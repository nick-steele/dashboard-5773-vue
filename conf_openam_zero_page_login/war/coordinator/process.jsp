<%@page import="org.jasypt.util.text.*" %>

<%
    //
    // Decrypt the parameters and pass to zero page loging URL
    //
    BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
    String key = "k33lser2l@ngler";
    textEncryptor.setPassword(key);

    String userId = request.getParameter("a");
    String p = request.getParameter("b");
    String gotoUrl = request.getParameter("c");
%>

<jsp:forward page="/UI/Login">
    <jsp:param name="IDToken1" value="<%= textEncryptor.decrypt(userId)%>" />
    <jsp:param name="IDToken2" value="<%= textEncryptor.decrypt(p)%>" />
    <jsp:param name="goto"     value="<%= textEncryptor.decrypt(gotoUrl)%>" />        
</jsp:forward>
