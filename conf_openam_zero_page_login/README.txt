The dashboard app uses the OpenAM zero page login. We added a preprocessor to encrypt user id and password. See attached diagram for implementation details.


The files in this directory are required on the OpenAM server. These files handle the decryption of user and password. For deployment copy the following files to the OpenAM server.


war/coordinator/process.jsp

war/WEB-INF/lib/jasypt-1.9.0.jar

