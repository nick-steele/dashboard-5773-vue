<%-- 
    Use Zero Page Login Establish session with Pentaho 
    to write session in browser
--%>

<%@page import="com.gsihealth.dashboard.entity.dto.LoginResult"%>
<%@page import="org.jasypt.util.text.*" %>
<%@page import="com.gsihealth.dashboard.server.WebConstants" %>
<%@page import="java.util.logging.Logger" %>
<%@page import="java.net.URLEncoder" %>

<html>
    <%
        String PentahoServer = ((LoginResult) session.getAttribute(WebConstants.LOGIN_RESULT_KEY)).getSsoPentahoServer();
       //Dynamic Pentaho User Id
       //use user_id instead of Report_Role_Key
        String userId = (String) session.getAttribute(WebConstants.USER_ID_KEY);
        String pw = encode((String) session.getAttribute(WebConstants.PW_KEY));
        session.removeAttribute(WebConstants.PW_KEY); 
        //String pw = userId + "123";
     
        //Default Static Pentaho User Id
        //String userId = (String) application.getAttribute(WebConstants.SSO_PENTAHO_USER_ID_KEY);
        // String pw = (String) application.getAttribute(WebConstants.SSO_PENTAHO_PW_KEY);
        // application.removeAttribute(WebConstants.SSO_PENTAHO_PW_KEY);     
        String pentahoUrl = PentahoServer + "/pentaho/Logout";
        
        System.out.println("Pentaho Logout url:" + pentahoUrl);
        System.out.println("userId:" + userId + "\nPentaho Password: *** ");
        
    /*    BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        String key = "k33lser2l@ngler"; // do not change this key
        textEncryptor.setPassword(key);   
        -- may need to be encrypted like above
       
        
        */
        Logger logger = Logger.getLogger(getClass().getName());
        
        logger.info("Logging out of Pentaho Server");
       
       
    %>
    <%!
         public String encode(String value) throws Exception {
                 if (value != null) {
                     value = URLEncoder.encode(value, "UTF-8");
                 } else {
                     value = "";
                 }
                 return value;
             }
        
        %>
    
    <body>
      
        <form  id="the_form" action="<%= pentahoUrl %>" method="GET">
            <input type="hidden" name="userid" value="<%= userId %>" /> <!-- textEncryptor.encrypt(userId) -->
            <input type="hidden" name="password" value="<%= pw %>" /> <!-- textEncryptor.encrypt(pw) -->
            
        </form>
        
        <script type="text/javascript">
           window.resizeTo(1,1);
           window.moveBy(0,1000);
         //   function autoSubmit() {
              //   var theForm = document.getElementById("the_form");
              //  theForm.submit();
                setTimeout(function(){window.close();},5000);          
      //    }   
          
        //    window.onload = autoSubmit;
        </script>    
        
        
         <iframe src ="<%= pentahoUrl %>?userid=<%= userId %>&password=<%= pw %>" frameborder ="0">
        </iframe>
    </body>
</html>
