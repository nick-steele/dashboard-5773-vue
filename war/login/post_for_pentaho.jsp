<%-- 
    Use Post to send tokenid to Avado for Single Sign On 
    
--%>


<%@page import="com.gsihealth.dashboard.common.ReferenceData"%>
<%@page import="java.util.Map"%>
<%@page import="com.gsihealth.dashboard.common.ClientApplicationProperties"%>
<%@page import="com.gsihealth.dashboard.entity.dto.LoginResult"%>
<%@page import="com.gsihealth.dashboard.server.WebConstants" %>
<%@page import="java.net.URLEncoder" %>




<html>
    <%
        
      String protocol = request.getParameter("protocol");
      String server = request.getParameter("server");  
      String j_username = request.getParameter("j_username");
      String j_password =request.getParameter("j_password");       
    %>
    
    <body>
        <form id="the_form" action="<%=protocol%>://<%=server%>/pentaho/j_spring_security_check" method="POST">
           
            <input type="hidden" name="j_username" value= <%=j_username%> /> 
            <input type="hidden" name="j_password" value= <%=j_password%> />
        </form>
        
        <script type="text/javascript">
            function autoSubmit() {               
                var theForm = document.getElementById("the_form");
                theForm.submit();
            }
            
            window.onload = autoSubmit;
        </script>
    </body>
</html>
