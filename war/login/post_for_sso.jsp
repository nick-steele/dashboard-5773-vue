<%-- 
    Use Zero Page Login Establish federated session with OpenAM 
    to write session in browser
--%>

<%@page import="com.gsihealth.dashboard.entity.dto.LoginResult"%>
<%@page import="org.jasypt.util.text.*" %>
<%@page import="com.gsihealth.dashboard.server.WebConstants" %>

<html>
    <%
        LoginResult loginResult = (LoginResult)session.getAttribute(WebConstants.LOGIN_RESULT_KEY);
        String sessionUrl = loginResult.getSessionUrl();
        String sessionGoto = loginResult.getSessionGoto();

        String userId = (String) session.getAttribute(WebConstants.USER_ID_KEY);

        String pw = (String) session.getAttribute(WebConstants.PW_KEY);
        //keep PW in session for Pentaho Login
   //     session.removeAttribute(WebConstants.PW_KEY);        //
        
        BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
        String key = "k33lser2l@ngler"; // do not change this key
        textEncryptor.setPassword(key);        
        
    %>
    
    <body>
        <form id="the_form" action="<%= sessionUrl %>" method="GET">
            <input type="hidden" name="a" value="<%= textEncryptor.encrypt(userId) %>" />
            <input type="hidden" name="b" value="<%= textEncryptor.encrypt(pw) %>" />
            <input type="hidden" name="c" value="<%= textEncryptor.encrypt(sessionGoto) %>" />
        </form>
        
        <script type="text/javascript">
           window.resizeTo(1,1);
           window.moveBy(0,1000);;
            function autoSubmit() {
                var theForm = document.getElementById("the_form");
                theForm.submit();
            }
            
            window.onload = autoSubmit;
        </script>
    </body>
</html>
