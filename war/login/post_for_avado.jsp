<%-- 
    Use Post to send tokenid to Avado for Single Sign On 
    
--%>


<%@page import="com.gsihealth.dashboard.entity.dto.LoginResult"%>
<%@page import="com.gsihealth.dashboard.server.WebConstants" %>


<html>
    <%
        LoginResult loginResult = (LoginResult)session.getAttribute(WebConstants.LOGIN_RESULT_KEY);
        String avadoFormUrl = loginResult.getAvadoUrl();
        String tokenId = (String) session.getAttribute(WebConstants.SSO_TOKEN_KEY);
        
    %>
    
    <body>
        <form id="the_form" action="<%= avadoFormUrl %>" method="POST">
            <input type="hidden" name="token" value="<%= tokenId %>" />          
        </form>
        
        <script type="text/javascript">
            function autoSubmit() {
                var theForm = document.getElementById("the_form");
                theForm.submit();
            }
            
            window.onload = autoSubmit;
        </script>
    </body>
</html>
