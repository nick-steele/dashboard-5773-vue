<%-- 
    Use Post to send tokenid to Avado for Single Sign On 
    
--%>


<%@page import="com.gsihealth.dashboard.common.ReferenceData"%>
<%@page import="java.util.Map"%>
<%@page import="com.gsihealth.dashboard.common.ClientApplicationProperties"%>
<%@page import="com.gsihealth.dashboard.entity.dto.LoginResult"%>
<%@page import="com.gsihealth.dashboard.server.WebConstants" %>



<html>
    <%
        
        String tokenId = (String) session.getAttribute(WebConstants.SSO_TOKEN_KEY);
        LoginResult loginResult= (LoginResult) session.getAttribute(WebConstants.LOGIN_RESULT_KEY);
        Long communityId = loginResult.getCommunityId();
        ClientApplicationProperties clientProps =
        ((Map<Long, ReferenceData>)application.getAttribute(WebConstants.REFERENCE_DATA_KEY))
                    .get(communityId).getClientApplicationProperties();
        String messagesFormUrl = clientProps.getMessageUrl();
        String emailid=loginResult.getDirectAddress();
        
    %>
    
    <body>
        <form id="the_form" action="<%= messagesFormUrl %>" method="POST">
            <input type="hidden" name="tokenid" value="<%= tokenId %>" />   
            <input type="hidden" name="communityid" value="<%= communityId %>" />   
            <input type="hidden" name="emailid" value="<%= emailid %>" />   
        </form>
        
        <script type="text/javascript">
            function autoSubmit() {
                var theForm = document.getElementById("the_form");
                theForm.submit();
            }
            
            window.onload = autoSubmit;
        </script>
    </body>
</html>
