<%--
    Use Post to send tokenid to Avado for Single Sign On

--%>

<%@page import="com.gsihealth.dashboard.entity.dto.LoginResult"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.gsihealth.dashboard.server.WebConstants" %>
<%@ page import="com.gsihealth.dashboard.common.Constants" %>


<html>
    <%


        String tokenId = (String) session.getAttribute(WebConstants.SSO_TOKEN_KEY);
        LoginResult loginResult = (LoginResult) session.getAttribute(WebConstants.LOGIN_RESULT_KEY);

        String treatFormUrl = loginResult.getTreatUrl();
        String oid = loginResult.getCommunityOid();
        String openAMinstance = loginResult.getOpenAMinstance();
        String destination = request.getParameter(Constants.DESTINATION_HTTP_PARAM_NAME);
        if (StringUtils.isBlank(destination)){
            destination = com.gsihealth.dashboard.client.util.Constants.CAREPLAN_HOMEPAGE;
        }

        String patientIdHttpParamName = com.gsihealth.dashboard.common.Constants.PATIENT_ID_HTTP_PARAM_NAME;


        String patientId = request.getParameter(patientIdHttpParamName);

        if (StringUtils.isBlank(patientId)) {
            patientId = "-1";
        }
        
        //SPIRA 5847  add taskId to pass to CarePlan
        String taskIdHttpParamName = com.gsihealth.dashboard.common.Constants.TASK_ID_HTTP_PARAM_NAME;

        String taskId = request.getParameter("taskId");

        if (StringUtils.isBlank(taskId)) {
            taskId = "-1";
        }
    %>

    <body>

         <form id="the_form" action="<%= treatFormUrl %>" method="POST">
            <input type="hidden" name="tokenid" value="<%= tokenId %>" />
            <input type="hidden" name="ssotype" value="OPENAM" />
            <input type="hidden" name="communityid" value="<%= oid %>" />
            <input type="hidden" name="<%= patientIdHttpParamName %>" value="<%= patientId %>" />
            <input type="hidden" name="<%= taskIdHttpParamName %>" value="<%= taskId %>" />
            <input type="hidden" name="openAMinstance" value="<%= openAMinstance %>" />
            <input type="hidden" name="destination" value="<%= destination %>" />
         </form>

        <script type="text/javascript">
            function autoSubmit() {
                var theForm = document.getElementById("the_form");
                theForm.submit();
            }

            window.onload = autoSubmit;
        </script>
    </body>
</html>
