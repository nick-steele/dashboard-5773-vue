<!DOCTYPE html>

<html lang="en">
<head>
  <%@page import="com.gsihealth.dashboard.client.util.ApplicationContext"%>
  <%@page import="com.gsihealth.dashboard.common.ReferenceData"%>
  <%@page import="java.util.Map"%>
  <%@page import="com.gsihealth.dashboard.common.ClientApplicationProperties" %>
  <%@page import="com.gsihealth.dashboard.server.WebConstants" %>
  <%@page import="com.gsihealth.dashboard.server.dao.ConfigurationDAO" %>
  <%@page import="com.gsihealth.dashboard.server.dao.ApplicationDAO" %>
  <%@page import="com.gsihealth.dashboard.server.service.ClientApplicationPropertiesUtils" %>
  <%@page import="org.apache.commons.lang.StringUtils" %>
  <%
    response.addHeader("X-Frame-Options", "DENY"); // Added to satisfy clickjacking issue in Spira 5566
      response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
      response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
      response.setDateHeader("Expires", 0); // Proxies.
  
      java.util.Date theDate = new java.util.Date();
  
      java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy");
      String theCurrentYear = formatter.format(theDate);
  
      // Get the build version...
      String buildVersion = (String) application.getAttribute(WebConstants.APPLICATION_PROPERTIES_BUILD_VERSION_KEY);
  %>
    <meta charset="utf-8">
    <title>GWT-Bridge index</title>
</head>
<body style="background-color: #77f;color:#fff">
  <center>
    <h1>Child iframe</h1>
    <button id="message_button">Send message</button><br/><br/>
    <span>Received: </span><span id="results"></span>
  </center>
  <script>
    var payload = {
      server	:	"<%= request.getServerName() %>",
      port    : "<%= request.getServerPort() %>",
      build   :	"<%= buildVersion %>",
      nodePort: null
	  };
    window.addEventListener('message', function (e) {
      document.getElementById('results').innerHTML = e.data
    })
    window.parent.postMessage(payload, '*')
  </script>
</body>
</html>
