/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.entity.AccessLevel;
import com.gsihealth.dashboard.entity.Organization;
import com.gsihealth.dashboard.entity.User;
import java.util.List;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Chad Darby
 */
public class UserDAOImplTest {

    public UserDAOImplTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    // @Test
    public void testSpeedGetTotalCountForReportUsers() {
        UserDAO userDAO = new UserDAOImpl();

        List<User> users2 = userDAO.findUserEntities();

        long start = System.currentTimeMillis();
        int count = userDAO.getTotalCountForUserEntities();
        long stop = System.currentTimeMillis();

        System.out.println("time = " + (stop - start) / 1000 + " secs.");
    }

    @Test
    public void findOrphanedUsers() {
        UserDAO userDAO = new UserDAOImpl();

        List<User> users = userDAO.findUserEntities();

        int count = 1;
        for (User tempUser : users) {
            Organization organization = null;

            try {
                    organization = tempUser.getUserCommunityOrganization(1L).getOrganization();

            } catch (Exception exc) {
                    System.out.println("Bad data for user organization: ");
                    System.out.println(count + ": " + tempUser.getLastName() + ", " + tempUser.getFirstName() + ", " + tempUser.getUserId());

                    // System.out.println("organization = " + organization.getOrganizationName() + ",  " + organization.getOrgId());
            }

            System.out.println("\n");
            count++;
        }
    }
}
