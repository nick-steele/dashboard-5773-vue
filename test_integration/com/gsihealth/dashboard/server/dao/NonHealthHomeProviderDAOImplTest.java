package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.entity.NonHealthHomeProvider;
import java.util.List;
import junit.framework.Assert;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Chad Darby
 */
public class NonHealthHomeProviderDAOImplTest {

    public NonHealthHomeProviderDAOImplTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /*
    @Test
    public void testFindEntities() {
        NonHealthHomeProviderDAOImpl dao = new NonHealthHomeProviderDAOImpl();

        List<NonHealthHomeProvider> results = dao.findNonHealthHomeProviderEntities(22, 1, 10);

        System.out.println(results);
    }
    */
    
    @Test
    public void testFindEntitiesCount() {
        NonHealthHomeProviderDAOImpl dao = new NonHealthHomeProviderDAOImpl();

        int count = dao.getNonHealthHomeProviderCount(22);

        System.out.println("Count = " + count);
    }

    // @Test
    /*
    public void addProviders() throws Exception {
        NonHealthHomeProviderDAOImpl dao = new NonHealthHomeProviderDAOImpl();

        NonHealthHomeProvider provider1 = new NonHealthHomeProvider();
        provider1.setFirstName("Test");
        provider1.setLastName("TestLast");
        provider1.setPatientId(22);
        provider1.setConsentObtainedByUserId(40);
        
        dao.add(provider1);

        NonHealthHomeProvider provider2 = new NonHealthHomeProvider();
        provider2.setFirstName("TestSecond");
        provider2.setLastName("TestSecondLast");
        provider2.setPatientId(22);
        provider2.setConsentObtainedByUserId(40);
        
        dao.add(provider2);
        
    }
    */
    
    @Test
    public void isDuplicateEmail_False() throws Exception {
        NonHealthHomeProviderDAOImpl dao = new NonHealthHomeProviderDAOImpl();

        String email = "bush@abc.fake";
        
        boolean actual = dao.isDuplicateEmail(email);
        
        Assert.assertFalse(actual);
    }    

    @Test
    public void isDuplicateEmail_True() throws Exception {
        NonHealthHomeProviderDAOImpl dao = new NonHealthHomeProviderDAOImpl();

        String email = "test@test.com";
        
        boolean actual = dao.isDuplicateEmail(email);
        
        Assert.assertTrue(actual);
    }    

}
