/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.entity.ProgramName;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Chad Darby
 */
public class ReferenceDataDAOImplTest {
    
    //             List<ProgramName> programNames = referenceDataDAO.findProgramNameEntities(communityId);

    private static ReferenceDataDAO referenceDataDAO;
    
    public ReferenceDataDAOImplTest() {
    }

    @BeforeClass
    public static void setup() throws Exception {
        referenceDataDAO = new ReferenceDataDAOImpl();
    }

    @Test
    public void testGetProgramNames() {

        List<ProgramName> programNames = referenceDataDAO.findProgramNameEntities(1L);
        
        for (ProgramName tempProgramName : programNames) {
            System.out.println(tempProgramName.getValue() + ",  " + tempProgramName.getHealthHomeId().getHealthHomeValue());
        }        
    }

}