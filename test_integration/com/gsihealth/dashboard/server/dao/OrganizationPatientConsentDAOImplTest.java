package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.entity.OrganizationPatientConsent;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class OrganizationPatientConsentDAOImplTest {
    
    public OrganizationPatientConsentDAOImplTest() {
    }


    @Test
    public void findEntitiesByOrg() {
        OrganizationPatientConsentDAO dao = new OrganizationPatientConsentDAOImpl();
        
        List<OrganizationPatientConsent> results = dao.findOrganizationPatientConsentEntitiesByOrg(9);
        
        System.out.println("findEntitiesByOrg results = " + results);
    }
    
    @Test
    public void findEntities() {
        OrganizationPatientConsentDAO dao = new OrganizationPatientConsentDAOImpl();
        
        List<OrganizationPatientConsent> results = dao.findOrganizationPatientConsentEntities(12, 1, 10);
        
        System.out.println("results = " + results);
    }


    @Test
    public void countEntities() {
        OrganizationPatientConsentDAO dao = new OrganizationPatientConsentDAOImpl();
        
        int count = dao.getOrganizationPatientConsentCount(12);
        
        System.out.println("count = " + count);
    }
    
    @Test
    public void hasConsent_PERMIT() {
        OrganizationPatientConsentDAO dao = new OrganizationPatientConsentDAOImpl();
        
        boolean consent = dao.hasConsent(3, 32);

        assertTrue(consent);
    }

    @Test
    public void hasConsent_NONE() {
        OrganizationPatientConsentDAO dao = new OrganizationPatientConsentDAOImpl();
        
        boolean consent = dao.hasConsent(3, 9999);

        assertFalse(consent);
    }

    @Test
    public void hasConsent_UserOrgDoesNotHaveConsent() {
        OrganizationPatientConsentDAO dao = new OrganizationPatientConsentDAOImpl();
        
        long patientId = 36;
        long orgId = 48;
        
        boolean consent = dao.hasConsent(patientId, orgId);

        assertFalse(consent);
    }
    
}
