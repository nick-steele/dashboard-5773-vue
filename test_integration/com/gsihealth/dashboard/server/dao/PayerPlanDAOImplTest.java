package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.entity.insurance.PayerPlan;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class PayerPlanDAOImplTest {

    private static PayerPlanDAOImpl payerPlanDAO;

    public PayerPlanDAOImplTest() {
    }

    @BeforeClass
    public static void setup() throws Exception {
        payerPlanDAO = new PayerPlanDAOImpl();
    }

    @Test
    public void testIsDuplicate_sameMmisId_newObject() {

        // setup
        PayerPlan payerPlan = new PayerPlan();
        payerPlan.setMmisId("11111");

        // execute
        boolean result = payerPlanDAO.isDuplicate(payerPlan);

        // assert
        assertTrue(result);
    }

    @Test
    public void testIsDuplicate_sameName_newObject() {

        // setup
        PayerPlan payerPlan = new PayerPlan();
        payerPlan.setName("Aetna, Inc.");

        // execute
        boolean result = payerPlanDAO.isDuplicate(payerPlan);

        // assert
        assertTrue(result);
    }

    @Test
    public void testIsDuplicate_sameNameAndMmisId_newObject() {

        // setup
        PayerPlan payerPlan = new PayerPlan();
        payerPlan.setName("Academy Life Insurance Company");
        payerPlan.setMmisId("11111");

        // execute
        boolean result = payerPlanDAO.isDuplicate(payerPlan);

        // assert
        assertTrue(result);
    }

    @Test
    public void testIsDuplicate_noMatch_newObject() {

        // setup
        PayerPlan payerPlan = new PayerPlan();
        payerPlan.setName("FooBar");
        payerPlan.setMmisId("1913");

        // execute
        boolean result = payerPlanDAO.isDuplicate(payerPlan);

        // assert
        assertFalse(result);
    }

    @Test
    public void testIsDuplicate_settingSameMmisId_existingObject_changeToSameObject() {

        // setup
        PayerPlan payerPlan = payerPlanDAO.findPayerPlan(1L);
        payerPlan.setMmisId("11111");

        // execute
        boolean result = payerPlanDAO.isDuplicate(payerPlan);

        // assert
        assertFalse(result);
    }

    @Test
    public void testIsDuplicate_settingSameName_existingObject_changeToSameObject() {

        // setup
        PayerPlan payerPlan = payerPlanDAO.findPayerPlan(1L);
        payerPlan.setName("Academy Life Insurance Company");

        // execute
        boolean result = payerPlanDAO.isDuplicate(payerPlan);

        // assert
        assertFalse(result);
    }

    @Test
    public void testIsDuplicate_settingSameNameAndMmisId_existingObject_changeToSameObject() {

        // setup
        PayerPlan payerPlan = payerPlanDAO.findPayerPlan(1L);
        payerPlan.setName("Academy Life Insurance Company");
        payerPlan.setMmisId("11111");

        // execute
        boolean result = payerPlanDAO.isDuplicate(payerPlan);

        // assert
        assertFalse(result);
    }

    @Test
    public void testIsDuplicate_sameMmisId_existingObject_duplicateOfOtherObject() {

        // setup
        PayerPlan payerPlan = payerPlanDAO.findPayerPlan(1L);
        payerPlan.setMmisId("22222");

        // execute
        boolean result = payerPlanDAO.isDuplicate(payerPlan);

        // assert
        assertTrue(result);
    }

    @Test
    public void testIsDuplicate_sameName_existingObject_duplicateOfOtherObject() {

        // setup
        PayerPlan payerPlan = payerPlanDAO.findPayerPlan(1L);
        payerPlan.setName("Admar Group, Inc., The");

        // execute
        boolean result = payerPlanDAO.isDuplicate(payerPlan);

        // assert
        assertTrue(result);
    }

    @Test
    public void testIsDuplicate_sameNameAndMmisId_existingObject_duplicateOfOtherObject() {

        // setup
        PayerPlan payerPlan = payerPlanDAO.findPayerPlan(1L);
        payerPlan.setName("Admar Group, Inc., The");
        payerPlan.setMmisId("22222");

        // execute
        boolean result = payerPlanDAO.isDuplicate(payerPlan);

        // assert
        assertTrue(result);
    }

    @Test
    public void testHasPatientsAssignedToThisPayerPlan() {

        // setup
        PayerPlan payerPlan = payerPlanDAO.findPayerPlan(1L);
        payerPlan.setName("foo");
        payerPlan.setMmisId("1111");
        
        // execute
        boolean actual = payerPlanDAO.hasPatientsAssignedToThisPayerPlan(payerPlan); 

        // assert
        assertTrue(actual);
    }
}
