/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.entity.FacilityType;
import com.gsihealth.dashboard.entity.Organization;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class OrganizationDAOImplTest {

    public OrganizationDAOImplTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void isUniqueOrganization_UniqueIsTrue() {
        OrganizationDAO dao = new OrganizationDAOImpl();

        Organization org = new Organization();
        String orgName = "AlphaBetaCharlie_" + System.currentTimeMillis();
        org.setOrganizationName(orgName);

        boolean expected = true;
        boolean actual = dao.isUniqueOrganizationName(org);

        assertEquals(expected, actual);
    }

    /*
    @Test
    public void isUniqueOrganization_UniqueIsFalse() throws Exception {
        OrganizationDAO dao = new OrganizationDAOImpl();

        Organization org = new Organization();
        String orgName = "JUnit Test Org";
        org.setOrganizationName(orgName);
        dao.create(org);

        Organization orgDuper = new Organization();
        orgDuper.setOrganizationName(orgName);

        boolean expected = false;
        boolean actual = dao.isUniqueOrganizationName(orgDuper);

        dao.permDelete(org.getOrgId());

        assertEquals(expected, actual);
    }

    @Test
    public void isUniqueOrganization_UniqueIsTrueForUpdate() throws Exception {
        OrganizationDAO dao = new OrganizationDAOImpl();

        Organization org = new Organization();
        String orgName = "JUnit Test Org";
        org.setOrganizationName(orgName);
        dao.create(org);

        boolean expected = true;
        boolean actual = dao.isUniqueOrganizationName(org);

        dao.permDelete(org.getOrgId());

        assertEquals(expected, actual);
    }

    @Test
    public void isUniqueOid_UniqueIsTrue() {
        OrganizationDAO dao = new OrganizationDAOImpl();

        Organization org = new Organization();
        org.setOID("1.2.3.4.5.6.7." + System.currentTimeMillis());

        boolean expected = true;
        boolean actual = dao.isUniqueOid(org);

        assertEquals(expected, actual);
    }

    @Test
    public void isUniqueOid_UniqueIsFalse() throws Exception {
        OrganizationDAO dao = new OrganizationDAOImpl();

        Organization org = new Organization();
        String oid = "junit.test.1.2.3.4.5.6";
        org.setOID(oid);
        dao.create(org);

        Organization orgDuper = new Organization();
        orgDuper.setOID(oid);

        boolean expected = false;
        boolean actual = dao.isUniqueOid(orgDuper);

        dao.permDelete(org.getOrgId());

        assertEquals(expected, actual);
    }

    @Test
    public void isUniqueOid_UniqueIsTrueForUpdate() throws Exception {
        OrganizationDAO dao = new OrganizationDAOImpl();

        Organization org = new Organization();
        String oid = "junit.test.1.2.3.4.5.6";
        org.setOID(oid);
        dao.create(org);

        boolean expected = true;
        boolean actual = dao.isUniqueOid(org);

        dao.permDelete(org.getOrgId());

        assertEquals(expected, actual);
    }
    
    @Test
    public void getOrganizationName() throws Exception {
        OrganizationDAO dao = new OrganizationDAOImpl();
        FacilityTypeDAO facilityTypeDAO = new FacilityTypeDAOImpl();
        
        FacilityType tempFacilityType = facilityTypeDAO.findFacilityType(1, 2);
        
        Organization org = new Organization();
        String orgName = "junit.test.Test Organization";
        org.setOrganizationName(orgName);
        org.setFacilityType(tempFacilityType);
        dao.create(org);

        String actual = dao.getOrganizationName(org.getOrgId());

        dao.permDelete(org.getOrgId());

        assertEquals(orgName, actual);
    }
    */

    /*
    @Test
    public void createOrg() throws Exception {
        OrganizationDAO dao = new OrganizationDAOImpl();
        FacilityTypeDAO facilityTypeDAO = new FacilityTypeDAOImpl();
        
        FacilityType tempFacilityType = facilityTypeDAO.findFacilityType(1, 2);
        
        Organization org = new Organization();
        String orgName = "junit.test.Test Organization";
        org.setOrganizationName(orgName);
        org.setFacilityType(tempFacilityType);
        dao.create(org);

        String actual = dao.getOrganizationName(org.getOrgId());

        // dao.permDelete(org.getOrgId());

        assertEquals(orgName, actual);
    }
    */
    
    @Test
    public void getTotalCountForGsiCommunity() throws Exception {
        OrganizationDAO dao = new OrganizationDAOImpl();

        long count = dao.getTotalCount(1);

        // test
        assertTrue(count > 0);
    }

    @Test
    public void getTotalCountForNonExistentCommunity() throws Exception {
        OrganizationDAO dao = new OrganizationDAOImpl();

        long count = dao.getTotalCount(1969);

        // test
        assertTrue(count == 0);
    }
    
}