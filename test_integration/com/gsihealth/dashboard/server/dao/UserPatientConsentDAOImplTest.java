/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.entity.User;
import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class UserPatientConsentDAOImplTest {
    
    public UserPatientConsentDAOImplTest() {
    }

    @Test
    public void getUsers() {
        UserPatientConsentDAOImpl dao = new UserPatientConsentDAOImpl();
        
        List<User> users = dao.getUsers(1, 1);
        int actual = users.size();
        int expected = 177;
        
        assertEquals(expected, actual);
    }

    @Test
    public void hasConsent() {
        UserPatientConsentDAOImpl dao = new UserPatientConsentDAOImpl();
        
        long userId = 31;
        long patientId = 1;
        
        boolean actual = dao.hasConsent(userId, patientId);
        
        assertTrue(actual);
    }

    @Test
    public void hasConsent_false() {
        UserPatientConsentDAOImpl dao = new UserPatientConsentDAOImpl();
        
        long userId = 31;
        long patientId = 999;
        
        boolean actual = dao.hasConsent(userId, patientId);
        
        assertFalse(actual);
    }
    
}
