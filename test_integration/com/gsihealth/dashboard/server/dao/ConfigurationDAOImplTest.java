/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad.Darby
 */
public class ConfigurationDAOImplTest {
    
    public ConfigurationDAOImplTest() {
    }
    
    @Test
    public void getConfig() throws Exception {
        
        ConfigurationDAOImpl dao = new ConfigurationDAOImpl();
        
        String dir = dao.getProperty(1, "reports.patient.dir");                
        
        assertNotNull(dir);
    }

    @Test
    public void getConfig_unknown() throws Exception {
        
        ConfigurationDAOImpl dao = new ConfigurationDAOImpl();
        
        String dir = dao.getProperty(1, "bubba");                
        
        assertNull(dir);
    }

}