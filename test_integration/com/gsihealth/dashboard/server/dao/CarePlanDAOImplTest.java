package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.server.reports.EncounterDate;
import com.gsihealth.dashboard.server.reports.EncounterMode;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Chad Darby
 */
public class CarePlanDAOImplTest {
    
    public CarePlanDAOImplTest() {
    }

    @Test
    public void findEncounterDates() throws Exception {
        CarePlanDAOImpl dao = new CarePlanDAOImpl();
        
        Map<Long, EncounterDate> encounterDates = dao.findEncounterDates(1);
        
        System.out.println("encounterDates=" + encounterDates);
        
        Set<Long> keys = encounterDates.keySet();
        
        for (Long tempPatientId : keys) {
            
            EncounterDate tempEncounterDate = encounterDates.get(tempPatientId);
            
            System.out.println("patient id=" + tempPatientId + ",  encounter dates=" + ReflectionToStringBuilder.reflectionToString(tempEncounterDate));            
        }
        
        assertNotNull(encounterDates);
    }

    @Test
    public void findInitialAssessmentDates() throws Exception {
        CarePlanDAOImpl dao = new CarePlanDAOImpl();
        
        Map<Long, Date> theDates = dao.findInitialAssessmentDates(1);
        
        System.out.println("theDates=" + theDates);

        System.out.println(theDates);
        
        assertNotNull(theDates);
    }
    
}

