package com.gsihealth.dashboard.server.dao;

import java.util.Map;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class MdmDAOImplTest {

    public MdmDAOImplTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testGetSystemCodes() {
        MdmDAO dao = new MdmDAOImpl();

        Map<String, String> codes = dao.getSystemCodes();

        System.out.println(codes);

        assertNotNull(codes);
    }
}
