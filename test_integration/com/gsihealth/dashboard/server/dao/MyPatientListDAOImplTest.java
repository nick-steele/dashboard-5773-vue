package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.entity.MyPatientInfo;
import com.gsihealth.dashboard.entity.dto.MyPatientInfoDTO;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import com.gsihealth.dashboard.server.patientmanagement.PatientManagerImpl;
import com.gsihealth.entity.MyPatientInfo;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import org.apache.commons.lang.time.StopWatch;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Chad.Darby
 */
public class MyPatientListDAOImplTest {

    private Logger logger = Logger.getLogger(getClass().getName());
    private static MyPatientListDAO myPatientListDAO;
    private static ConfigurationDAO configurationDAO;
    private static PatientEnrollmentDAO patientEnrollmentDAO;
    private static PatientManager patientManager;

    @BeforeClass
    public static void setup() throws Exception {
        myPatientListDAO = new MyPatientListDAOImpl();
        configurationDAO = new ConfigurationDAOImpl();

        String mdmEndpoint = configurationDAO.getProperty(1, "patient.mdm.endpoint");

        String defaultOid = "2.16.840.1.113883.3.1358";
        String gsiOid = configurationDAO.getProperty(1, "gsihealth.oid", defaultOid);
        patientManager = new PatientManagerImpl(new HashMap<String, String>());

        patientEnrollmentDAO = new PatientEnrollmentDAOImpl(patientManager, configurationDAO);

    }

    // @Test
    public void testAddToList() throws Exception {

        // setup
        MyPatientListDAOImpl dao = new MyPatientListDAOImpl();

        MyPatientInfo info = null;
        int userId = 31;
        int communityId = 1;

        // execute
        info = new MyPatientInfo(userId, 71, communityId);
        dao.add(info);

        info = new MyPatientInfo(userId, 78, communityId);
        dao.add(info);

        info = new MyPatientInfo(userId, 79, communityId);
        dao.add(info);

        List<MyPatientInfo> result = dao.findMyPatientInfoEntities(userId, communityId);

        // assert
        assertFalse(result.isEmpty());
        assertEquals(result.size(), 3);

    }

    

    // @Test
    public void testVersionTwo() throws Exception {

        long communityId = 1;
        long userId = 31;

        StopWatch stopWatch = new StopWatch();

        stopWatch.start();

        List<MyPatientInfoDTO> dtoResults = myPatientListDAO.findMyPatientInfoList(userId, communityId);

        for (MyPatientInfoDTO temp : dtoResults) {
            String euid = temp.getEuid();

            // get patient from mdm
            Person person = patientManager.getPatient(euid);
            temp.setFirstName(person.getFirstName());
            temp.setLastName(person.getLastName());
        }

        stopWatch.stop();
        logger.info("Version 2: Time to get MyPatientList: " + stopWatch.getTime() / 1000.0 + " secs");
    }
    
    @Test
    public void testIsPatientAlreadyOnList_yesTheyAre() throws Exception {
     
        // setup
        boolean expected = true;
        long userId = 31;
        long communityId = 1;
        long patientId = 201;
                
        // execute
        boolean actual = myPatientListDAO.isAlreadyOnList(userId, patientId, communityId);
        
        // assert
        assertEquals(expected, actual);   
    }

    @Test
    public void testIsPatientAlreadyOnList_nope() throws Exception {
     
        // setup
        boolean expected = false;
        long userId = 31;
        long communityId = 1;
        long patientId = 99999;
                
        // execute
        boolean actual = myPatientListDAO.isAlreadyOnList(userId, patientId, communityId);
        
        // assert
        assertEquals(expected, actual);   
    }

    @Test
    public void testIsListFull() throws Exception {
     
        // setup
        boolean expected = false;
        long userId = 31;
        long communityId = 1;
                
        // execute
        boolean actual = myPatientListDAO.isListFull(userId, communityId);
        
        // assert
        assertEquals(expected, actual);   
    }
    
}
