/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.PatientEnrollment;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class ReportDAOImplTest {

    public ReportDAOImplTest() {
    }


    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
  //  @Test
    public void findAll() throws Exception {
        ReportDAO dao = new ReportDAOImpl();

        Map<String, PatientEnrollment> results = dao.findAll();
        Set<Entry<String, PatientEnrollment>> entrySet = results.entrySet();

        for (Entry<String, PatientEnrollment> tempEntry : entrySet) {
            PatientEnrollment tempPatientEnrollment = tempEntry.getValue();
            String organizationName = tempPatientEnrollment.getOrganizationName();
            System.out.println(organizationName);
            assertNotNull(organizationName);
        }

    }

    @Test
    public void getTotalCountForEnrolledPatients(){
        ReportDAO dao = new ReportDAOImpl();
        
        long userId = 31L;
        long accessLevelId = 1L;
        
       int count  = dao.getTotalCountForEnrolledPatients(userId, accessLevelId);
         assertEquals(1466,count) ;
              
      
    }
        
     @Test
     public void getRexExTest(){
        
         String query = "SELECT * FROM patient_community_enrollment,VIEW_USER_PATIENT_CONSENT WHERE user_id = 31 and "
                + "patient_community_enrollment.patient_id=view_user_patient_consent.patient_id";
        
        String countQuery =  "SELECT COUNT(*) FROM patient_community_enrollment,VIEW_USER_PATIENT_CONSENT WHERE user_id = 31 and "
                + "patient_community_enrollment.patient_id=view_user_patient_consent.patient_id";
        
        String testQuery = query.replaceFirst("SELECT.*\\* ", "SELECT COUNT(*) ");
        
        System.out.println("before query RegEx: " + query);
        System.out.println("after testQuery RegEx: " + testQuery);
        
        assertEquals(countQuery, testQuery);
    }
    }
    