/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import java.util.List;
import org.junit.Test;

public class CareteamDAOTest {

    @Test
    public void testGetDuplicateRoles() throws Exception {

        System.out.println(">>>>>>>>>>>>> TESTING ");
        
        // setup
        CommunityCareteamDAOImpl dao = new CommunityCareteamDAOImpl();

        // execute
        List<String> duplicateRoles = dao.getDuplicateRolesForCareteam(1);

        // assert
        // assertFalse(duplicateRoles.isEmpty());

        for (String temp : duplicateRoles) {
            System.out.println(temp);
        }
    }
}
