package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.entity.User;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import com.gsihealth.dashboard.entity.UserAlert;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class UserAlertDAOImplTest {

    public UserAlertDAOImplTest() {
    }

    /*
    @Test
    public void getUserAlerts_NoneExist() {
        UserAlertDAOImpl dao = new UserAlertDAOImpl();

        List<UserAlert> alerts = dao.findUserAlertEntities(9999999);

        assertTrue(alerts.isEmpty());
    }

    @Test
    public void getUserAlerts_HaveSome() {
        UserAlertDAOImpl dao = new UserAlertDAOImpl();

        List<UserAlert> alerts = dao.findUserAlertEntities(26);

        System.out.println("size = " + alerts.size());

        assertFalse(alerts.isEmpty());
    }
    */
    
    @Test
    public void getUserAlerts_Page() {
        UserAlertDAOImpl dao = new UserAlertDAOImpl();

        int pageNumber = 2;
        int pageSize = 3;

        List<UserAlert> alerts = dao.findUserAlertEntities(26, pageNumber, pageSize);

        for (UserAlert tempAlert : alerts) {
            System.out.println(ReflectionToStringBuilder.toString(tempAlert));
        }

        System.out.println(">>>>> page results size = " + alerts.size());

        assertFalse(alerts.isEmpty());
    }

    @Test
    public void getTotalCount() {
        UserAlertDAOImpl dao = new UserAlertDAOImpl();

        int count = dao.getTotalCount(26);

        System.out.println(">>>>> page results size = " + count);

        assertTrue(count > 0);
    }

    /*
    @Test
    public void updateUserAlerts() throws Exception {
        UserAlertDAOImpl dao = new UserAlertDAOImpl();

        
        List<UserAlert> alerts = dao.findUserAlertEntities(26);

        UserAlert badAlert = alerts.get(0);
        badAlert.setIsVisible(false);
        dao.update(badAlert);

        alerts = dao.findUserAlertEntities(26);

        System.out.println("size = " + alerts.size());

        assertFalse(alerts.isEmpty());
    }
    */
    
    @Test
    public void searchUserAlertsByDate() throws Exception {
        UserAlertDAOImpl dao = new UserAlertDAOImpl();

        String from = "07-19-2012";
        SimpleDateFormat dateFormatter = new SimpleDateFormat("MM-dd-yyyy");
        Date fromDate = dateFormatter.parse(from);

        Date toDate = new Date();

        List<UserAlert> alerts = dao.searchUserAlertsByDate(31, fromDate, toDate, 1, 50);

        System.out.println("size = " + alerts.size());

        assertFalse(alerts.isEmpty());
    }

    @Test
    public void searchUserAlertsByFirstName_Page() throws Exception {
        UserAlertDAOImpl dao = new UserAlertDAOImpl();

        int count = dao.getTotalCountForSearchByPatientFirstName(31, "Eric");
        System.out.println("TOTAL COUNT: Searching by patient first name: 'Eric'  size = " + count);
        
        List<UserAlert> alerts = dao.searchUserAlertsByPatientFirstName(31, "Eric", 1, 10);

        System.out.println("Searching by patient first name: 'Eric'  size = " + alerts.size());
        System.out.println(alerts);

        assertFalse(alerts.isEmpty());
        assertEquals(count, alerts.size());
    }

    @Test
    public void searchUserAlertsByLastName_Page() throws Exception {
        UserAlertDAOImpl dao = new UserAlertDAOImpl();

        int count = dao.getTotalCountForSearchByPatientLastName(31, "CollinsTest");
        System.out.println("TOTAL COUNT: Searching by patient last name: 'CollinsTest'  size = " + count);
        
        List<UserAlert> alerts = dao.searchUserAlertsByPatientLastName(31, "CollinsTest", 1, 10);

        System.out.println("Searching by patient last name: 'CollinsTest'  size = " + alerts.size());
        System.out.println(alerts);

        assertFalse(alerts.isEmpty());
        assertEquals(count, alerts.size());
    }
    
}
