/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.entity.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class ApplicationDAOImplTest {

    public ApplicationDAOImplTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    // @Test
    public void testFindApplication_validApp() {
        ApplicationDAO dao = new ApplicationDAOImpl();
        String appName = "GSI_ENROLLMENT_APP";

        /*
        Application theApp = dao.findApplication(appName);

        assertNotNull(theApp);
        assertEquals(theApp.getApplicationName(), appName);
        */
    }

    // @Test(expected = javax.persistence.NoResultException.class)
    public void testFindApplication_unknonwApp() {
        ApplicationDAO dao = new ApplicationDAOImpl();

        /*
        Application theApp = dao.findApplication("foobarxyz");

        fail("should not have made it here");
        */
    }

     @Test
    public void testAddUserCommunityApplication() throws Exception {
        ApplicationDAOImpl dao = new ApplicationDAOImpl();

        UserCommunityApplication userCommApp = new UserCommunityApplication();
        UserCommunityApplicationPK userCommunityApplicationPK = new UserCommunityApplicationPK();
        userCommApp.setUserCommunityApplicationPK(userCommunityApplicationPK);
        userCommunityApplicationPK.setApplicationId(6); //10 = messaging app, 6= enrollment app
        userCommunityApplicationPK.setCommunityId(1);
        userCommunityApplicationPK.setUserId(170);  //edwinapp2@test.com userid=170
        userCommApp.setStartDate(new Date());
        userCommApp.setEndDate(new Date());

        // execute
        dao.addUserCommunityApplication(userCommApp);

        UserCommunityApplication actual = dao.findUserCommunityApplication(userCommunityApplicationPK);
        
        // cleanup
        dao.deleteUserCommunityApplication(userCommApp);
        
        // assert
        UserCommunityApplicationPK actualKey = actual.getUserCommunityApplicationPK();
        assertEquals(userCommunityApplicationPK.getApplicationId(), actualKey.getApplicationId());
        assertEquals(userCommunityApplicationPK.getCommunityId(), actualKey.getCommunityId());
        assertEquals(userCommunityApplicationPK.getUserId(), actualKey.getUserId());        
    }
    
    // @Test
    public void testHasMessagingApp() throws Exception {
        ApplicationDAOImpl dao = new ApplicationDAOImpl();

        // execute
        boolean actual = dao.hasMessagingApp(66, 1);
                
        // assert
        assertTrue(actual);        
    }

    // @Test
    public void testGetApplicationNamesApp() throws Exception {
        ApplicationDAOImpl dao = new ApplicationDAOImpl();

        // execute
        List<String> appNames = dao.getUserApplicationNames(18, 1);
           
        System.out.println(appNames);
        
        // assert
        assertFalse(appNames.isEmpty());        
    }
    
      @Test
    public void testGetOrganizationCommunityApplications() throws Exception {
        ApplicationDAOImpl dao = new ApplicationDAOImpl();

        // execute
        List<OrganizationCommunityApplication> appNames = dao.getOrganizationApplications(1L,1L);  //orgId=9 is Harrisburg which works
           
        System.out.println(appNames);
        
        for (OrganizationCommunityApplication temp : appNames) {
            System.out.println("App Id: " +temp.getOrganizationCommunityApplicationPK().getApplicationId() + ", Role Id: " + temp.getOrganizationCommunityApplicationPK().getRoleId());
        }
        
        // assert
        assertFalse(appNames.isEmpty());        
    }

    @Test
    public void testGetApplications() throws Exception {
        ApplicationDAOImpl dao = new ApplicationDAOImpl();

        // execute
        List<Application> appNames = dao.getUserApplications(31, 1);
           
        System.out.println(appNames);
        
        for (Application temp : appNames) {
            System.out.println(temp.getDisplayOrder() + ": " + temp.getScreenName());
        }
        
        // assert
        assertFalse(appNames.isEmpty());        
    }
    @Test
    public void testGetSSO() throws Exception {
        ApplicationDAOImpl dao = new ApplicationDAOImpl();

        // execute
        List<Application> appNames = dao.getUserApplications(31, 1);
           
        System.out.println(appNames);
        
        for (Application temp : appNames) {
            System.out.println(temp.getDisplayOrder() + ": " + temp.getScreenName() + " sso:" 
                    + temp.getSsoEnabled()+ " showpopup:" + temp.getShowInPopupWindow()
                    + " clickToOpen:" + temp.getClickToOpenEnabled());
        }
        
        // assert
        assertFalse(appNames.isEmpty());        
    }
}
