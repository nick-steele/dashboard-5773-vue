/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.patientmanagement;

import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.server.common.Person;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class PatientManagerIntegrationImplTest {
    
    private Logger logger = Logger.getLogger(getClass().getName());
    
    private String gsiOid = "2.16.840.1.113883.3.1358";

    @Test
    public void testSearchByState_DEV() throws Exception {

        logger.info("\n\nstarting: testSearchByState_DEV()");
        String mdmEndPoint = "http://10.110.210.60:52501/PatientMDMEJBService/PatientMDMEJB?wsdl";

        List<Person> patients = search(mdmEndPoint);
        
        assertFalse(patients.isEmpty());
        
        logger.info("finished: testSearchByState_DEV()");
    }

    // @Test
    public void testSearchByState_QA() throws Exception {

        logger.info("\n\nstarting: testSearchByState_QA()");
        String mdmEndPoint = "http://10.110.210.85:52501/PatientMDMEJBService/PatientMDMEJB?wsdl";

        List<Person> patients = search(mdmEndPoint);
        
        assertFalse(patients.isEmpty());
        
        logger.info("finished: testSearchByState_QA()");
    }

    // @Test
    public void testSearchByState_STAGING() throws Exception {

        logger.info("\n\nstarting: testSearchByState_STAGING()");
        String mdmEndPoint = "http://10.110.210.94:52501/PatientMDMEJBService/PatientMDMEJB?wsdl";

        List<Person> patients = search(mdmEndPoint);
        
        assertFalse(patients.isEmpty());
        
        logger.info("finished: testSearchByState_STAGING()");
    }

    // @Test
    public void testSearchByState_MMC_PRODUCTION() throws Exception {

        logger.info("\n\nstarting: testSearchByState_MMC_PRODUCTION()");
        String mdmEndPoint = "http://10.128.65.114:52501/PatientMDMEJBService/PatientMDMEJB?wsdl";

        List<Person> patients = search(mdmEndPoint);
        
        assertFalse(patients.isEmpty());
        
        logger.info("finished: testSearchByState_MMC_PRODUCTION()");
    }
    
    private List<Person> search(String mdmEndPoint) throws Exception {
        logger.info("mdmEndPoint: " + mdmEndPoint);
        PatientManager patientManager = new PatientManagerImpl(new HashMap<String,String>());
        
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setState("NJ");
        
        List<Person> patients = patientManager.findPatient(searchCriteria);
        logger.info("found: " + patients.size());
        return patients;
    }
}