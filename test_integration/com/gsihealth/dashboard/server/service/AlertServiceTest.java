package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.common.SearchResults;
import java.util.ArrayList;
import com.gsihealth.dashboard.entity.UserAlert;
import com.gsihealth.dashboard.entity.AlertMessage;
import com.gsihealth.dashboard.entity.dto.UserAlertDTO;
import java.util.List;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import com.gsihealth.dashboard.server.dao.UserAlertDAO;
import com.gsihealth.dashboard.server.dao.UserAlertDAOImpl;
import java.math.BigInteger;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class AlertServiceTest {
    
    public AlertServiceTest() {
    }

    // @Test
    public void getUserAlerts_HaveSome() throws Exception {
        // setup
        AlertServiceImpl alertService = new AlertServiceImpl();        
        UserAlertDAO dao = new UserAlertDAOImpl();
        alertService.setUserAlertDAO(dao);

        // execute
        SearchResults<UserAlertDTO> searchResults = alertService.getUserAlertsPartial(26);
        List<UserAlertDTO> alerts = searchResults.getData();
        for (UserAlertDTO tempAlert : alerts) {
            System.out.println(ReflectionToStringBuilder.toString(tempAlert));
        }
        
        System.out.println("size = " + alerts.size());
        
        assertFalse(alerts.isEmpty());        
    }
    
    @Test
    public void convert() {
        // setup
        AlertServiceImpl alertService = new AlertServiceImpl();        
        AlertMessage alertMessage = new AlertMessage();
        
        alertMessage.setAlertDesc("Test");
        alertMessage.setAlertName("Big Alert");
        
        UserAlert userAlert = new UserAlert();
        userAlert.setUserId(BigInteger.valueOf(13));
        userAlert.setIsVisible(false);
        userAlert.setAlertMessageId(alertMessage);
        
        List<UserAlert> alerts = new ArrayList<UserAlert>();
        alerts.add(userAlert);
        
        // execute
        List<UserAlertDTO> dtos = alertService.convert(alerts);
        
        // assert
        
    }
}
