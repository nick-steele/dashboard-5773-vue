/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.CommunityOrganization;
import com.gsihealth.dashboard.entity.Organization;
import com.gsihealth.dashboard.entity.OrganizationPatientConsent;
import com.gsihealth.dashboard.entity.PatientEnrollment;
import com.gsihealth.dashboard.entity.User;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;
import com.gsihealth.dashboard.server.common.NoSoupForYouException;
import com.gsihealth.dashboard.server.common.PatientLoadResult;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.dao.OrganizationDAOImpl;
import com.gsihealth.dashboard.server.dao.AuditDAOImpl;
import com.gsihealth.dashboard.server.dao.OrganizationPatientConsentDAOImpl;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

/**
 *
 * @author Chad Darby
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({Persistence.class,AuditDAOImpl.class, LoginResult.class})
public class EnrollmentServiceHelperTest {

    private EnrollmentServiceHelper helper;
    private AuditDAOImpl auditDAO;
    private OrganizationPatientConsentDAOImpl organizationPatientConsentDAO;
    private OrganizationDAOImpl organizationDAO;
    
    public EnrollmentServiceHelperTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    
    @Before
    public void setUp() throws Exception {
        Properties props = loadProperties();
//        
//        emf = mock(EntityManagerFactory.class);
//        PowerMockito.mockStatic(Persistence.class);
//	when(Persistence.createEntityManagerFactory(Mockito.anyString())).thenReturn(emf);
        
        auditDAO = mock(AuditDAOImpl.class);
        organizationPatientConsentDAO = mock(OrganizationPatientConsentDAOImpl.class);
        organizationDAO = mock(OrganizationDAOImpl.class);
        helper = new EnrollmentServiceHelper(props);
    }

    @After
    public void tearDown() throws Exception {
    }
//
//    @Test
//    public void sendXacmlConsent() throws Exception {
//        
//        Properties props = loadProperties();
//        
//        AuditDAOImpl auditDAO = new AuditDAOImpl();
//        OrganizationPatientConsentDAOImpl organizationPatientConsentDAO = new OrganizationPatientConsentDAOImpl();
//        OrganizationDAOImpl organizationDAO = new OrganizationDAOImpl();
//        
//        EnrollmentServiceHelper helper = new EnrollmentServiceHelper(props, auditDAO, organizationPatientConsentDAO, organizationDAO);
//        
//        Person thePerson = new Person();
//        thePerson.setLocalId("10");
//        
//        LoginResult theLoginResult = new LoginResult();
//        boolean hasConsent = false;
//        
//        helper.sendXacmlConsentToBHIX(thePerson, theLoginResult, hasConsent);
//    }
    
    /**
     * Helper method to load properties file
     *
     * @param application
     * @throws IOException
     */
    private static Properties loadProperties() throws IOException {
        String propsFileName = "C:/gsihealth/dashboard/config/dashboard.properties";
        Properties theProps = new Properties();
        theProps.load(new FileReader(propsFileName));

        return theProps;
    }

    /**
     * user can't add a patient to a different community than their own
     */
    @Test(expected=NoSoupForYouException.class)
    public void testAddPatientHelper_bad_user_patient() throws Exception {
        System.out.println("addPatientHelper");
        PersonDTO personDTO = mock(PersonDTO.class);
        LoginResult loginResult = mock(LoginResult.class);
        PatientEnrollmentDTO patientEnrollmentDTO = new PatientEnrollmentDTO();
        patientEnrollmentDTO.setCommunityId(1);
        boolean runInBackgroundNotificationsAndAlerts = false;
        long communityCareteamId = 0L;
        when(loginResult.getCommunityId()).thenReturn(Long.parseLong("2"));
        when(personDTO.getPatientEnrollment()).thenReturn(patientEnrollmentDTO);
        helper.addPatientHelper(personDTO, communityCareteamId, loginResult, runInBackgroundNotificationsAndAlerts);
    }
 

    /**
     * Test 
     * user can't edit a patient in a different community.
     */
    @Test(expected=NoSoupForYouException.class)
    public void testUpdatePatientHelper() throws Exception {
        System.out.println("updatePatientHelper");
        String oldEnrollmentStatus = "";
        boolean checkForDuplicate = false;
        boolean checkPatientUserUpdate = false;
        boolean checkForDuplicateSsn = false;
        boolean primaryMedicaidcareIdBln = false;
        boolean secondaryMedicaidcareIdBln = false;
        boolean checkEnableCareteam = false;
        PatientInfo patientInfo = null;
        String newCareTeamId = "";
        String oldCareTeamId = "";
        String oldProgramLevelConsentStatus = "";
        long oldOrganizationId = 0L;
        Person updatedPerson = null;
        
        PersonDTO personDTO = mock(PersonDTO.class);
        PatientEnrollmentDTO patientEnrollmentDTO = new PatientEnrollmentDTO();
        patientEnrollmentDTO.setCommunityId(1);
        when(personDTO.getPatientEnrollment()).thenReturn(patientEnrollmentDTO);
        
        LoginResult loginResult = mock(LoginResult.class);
        when(loginResult.getCommunityId()).thenReturn(Long.parseLong("2"));
        
        helper = new EnrollmentServiceHelper();
        helper.updatePatientHelper(loginResult, personDTO, oldEnrollmentStatus, checkForDuplicate, checkPatientUserUpdate, checkForDuplicateSsn, 
                primaryMedicaidcareIdBln, secondaryMedicaidcareIdBln, checkEnableCareteam, patientInfo, newCareTeamId, oldCareTeamId, 
                oldProgramLevelConsentStatus, oldOrganizationId, updatedPerson);
        
       
    }

}
