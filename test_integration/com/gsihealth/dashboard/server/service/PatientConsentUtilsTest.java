/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.entity.PatientEnrollment;
import com.gsihealth.dashboard.entity.dto.OrganizationPatientConsentDTO;
import com.gsihealth.dashboard.server.dao.OrganizationPatientConsentDAO;
import com.gsihealth.dashboard.server.dao.UserPatientConsentDAO;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Edwin.Montgomery
 */
public class PatientConsentUtilsTest {
    
    public PatientConsentUtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of saveOrganizationPatientConsentList method, of class PatientConsentUtils.
     */
    //@Test
    public void testSaveOrganizationPatientConsentList_4args_1() throws Exception {
        System.out.println("saveOrganizationPatientConsentList");
        OrganizationPatientConsentDAO organizationPatientConsentDAO = null;
        UserPatientConsentDAO userPatientConsentDAO = null;
        long organizationId = 0L;
        PatientEnrollment patientEnrollment = null;
        PatientConsentUtils.saveOrganizationPatientConsentList(organizationPatientConsentDAO, userPatientConsentDAO, organizationId, patientEnrollment);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of saveOrganizationPatientConsentList method, of class PatientConsentUtils.
     */
    //@Test
    public void testSaveOrganizationPatientConsentList_4args_2() throws Exception {
        System.out.println("saveOrganizationPatientConsentList");
        OrganizationPatientConsentDAO organizationPatientConsentDAO = null;
        UserPatientConsentDAO userPatientConsentDAO = null;
        List<OrganizationPatientConsentDTO> orgDtos = null;
        PatientEnrollment patientEnrollment = null;
        PatientConsentUtils.saveOrganizationPatientConsentList(organizationPatientConsentDAO, userPatientConsentDAO, orgDtos, patientEnrollment);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of convertFromDtos method, of class PatientConsentUtils.
     */
    //@Test
    public void testConvertFromDtos() {
        System.out.println("convertFromDtos");
        List<OrganizationPatientConsentDTO> dtos = null;
        List expResult = null;
        List result = PatientConsentUtils.convertFromDtos(dtos);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of updateOrganizationPatientConsent method, of class PatientConsentUtils.
     */
   // @Test
    public void testUpdateOrganizationPatientConsent() throws Exception {
        System.out.println("updateOrganizationPatientConsent");
        OrganizationPatientConsentDAO organizationPatientConsentDAO = null;
        UserPatientConsentDAO userPatientConsentDAO = null;
        long newOrganizationId = 0L;
        long oldOrganizationId = 0L;
        long patientId = 0L;
        long communityId = 0L;
        PatientConsentUtils.updateOrganizationPatientConsent(organizationPatientConsentDAO, userPatientConsentDAO, newOrganizationId, oldOrganizationId, patientId, communityId);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buildUserPatientConsentQuery method, of class PatientConsentUtils.
     */
    @Test
    public void testBuildUserPatientConsentQuery() {
        System.out.println("buildUserPatientConsentQuery");
        long userId = 31L;
        String expResult = "SELECT * FROM patient_community_enrollment AS t0,VIEW_USER_PATIENT_CONSENT AS t1"
                    +" WHERE t1.user_id = 31 and (t0.patient_id=t1.patient_id) and t0.STATUS IN ('Enrolled', 'Assigned')";
        String result = PatientConsentUtils.buildUserPatientConsentQuery(userId);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of buildTotalUserPatientConsentQuery method, of class PatientConsentUtils.
     */
    //@Test
    public void testBuildTotalUserPatientConsentQuery() {
        System.out.println("buildTotalUserPatientConsentQuery");
        long userId = 0L;
        long communityId = 0L;
        String expResult = "";
        String result = PatientConsentUtils.buildTotalUserPatientConsentQuery(userId, communityId);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buildNativeSQLForUsersWithConsent method, of class PatientConsentUtils.
     */
    //@Test
    public void testBuildNativeSQLForUsersWithConsent() {
        System.out.println("buildNativeSQLForUsersWithConsent");
        long patientId = 0L;
        String expResult = "";
        String result = PatientConsentUtils.buildNativeSQLForUsersWithConsent(patientId);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buildNativeSQLForEnrolledConsentedPatientsCount method, of class PatientConsentUtils.
     */
    //@Test
    public void testBuildNativeSQLForEnrolledConsentedPatientsCount() {
        System.out.println("buildNativeSQLForEnrolledConsentedPatientsCount");
        long userId = 0L;
        String expResult = "";
        String result = PatientConsentUtils.buildNativeSQLForEnrolledConsentedPatientsCount(userId);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buildJpqlForHasConsent method, of class PatientConsentUtils.
     */
    //@Test
    public void testBuildJpqlForHasConsent() {
        System.out.println("buildJpqlForHasConsent");
        long userId = 0L;
        long patientId = 0L;
        long communityId = 0L;
        String expResult = "";
        String result = PatientConsentUtils.buildJpqlForHasConsent(userId, patientId, communityId);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of createNativeOrJPQLQuery method, of class PatientConsentUtils.
     */
   // @Test
    public void testCreateNativeOrJPQLQuery_EntityManager_String() {
        System.out.println("createNativeOrJPQLQuery");
        EntityManager em = null;
        String queryString = "";
        Query expResult = null;
        Query result = PatientConsentUtils.createNativeOrJPQLQuery(em, queryString);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of createNativeOrJPQLQuery method, of class PatientConsentUtils.
     */
    //@Test
    public void testCreateNativeOrJPQLQuery_3args() {
        System.out.println("createNativeOrJPQLQuery");
        EntityManager em = null;
        String queryString = "";
        Class targetClass = null;
        Query expResult = null;
        Query result = PatientConsentUtils.createNativeOrJPQLQuery(em, queryString, targetClass);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
}