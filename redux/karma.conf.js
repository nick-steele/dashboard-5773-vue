
//var module = angular.module('AngularSampleApp', []);

module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['mocha','chai','sinon'],
    files: [
      'https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js',
     // 'https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.js',
      'https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-mocks.js',
      'https://ajax.googleapis.com/ajax/libs/angular_material/0.8.3/angular-material.min.js',
      'compiled/controller.js',
      'unit/*.js'
    ],
    exclude: [
    ],

    'plugins' : [
    'karma-mocha',
    'karma-chrome-launcher',
    'karma-phantomjs-launcher',
    'phantomjs-prebuilt' ,
    'karma-coverage',
    'karma-chai',
    'karma-sinon'
],

  preprocessors: {
  'compiled/*.js': ['coverage']
},
reporters: ['progress', 'coverage'],
coverageReporter: {
  type : 'html',
  dir : 'coverage/'
},
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['Chrome'] 
    
  });
};