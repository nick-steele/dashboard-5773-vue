module.exports = {
	missingMode			: 'You must tell Simplex which mode to run in.  Add a command-line argument (d/p/c for development/production/compile) or edit app-config.js and set this in app.mode',
	invalidArgument		: 'Invalid argument.  Please use d/p/c for development/production/compile.',
	debugPoint			: 'Debug Point Reached',
	mode				: 'Using mode',
	say					: function (msg, end) {
		if (end)
			end = ': ' + end;
		else
			end = '';
		return this[msg] + end;
	}
}