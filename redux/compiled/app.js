var app=angular.module('redux',[]);// An application event factory... 2nd pass (first was to use window.QA.app.event)
app.factory('$appEvent', function ($model) {
	return function (eventName, dataOrCallback, tracer) {
			$model.get('app').event(eventName, dataOrCallback, tracer)
	};
});

/*
	$trace by Nick Steele
	Copyright (C) 2016 GSI Health, LLC.

	Simple yet precise logging factory that gets right to the point! Trace through an entire
	application in a single line on the console!
	
	Time specific things you're interested in, even through promises, socket events, a bunch of other traces, you got an idea? It'll work.
	
	Launch as many traces as you like.
	
	Flush them whenever you want and the console will then show when the trace started, along with all timing information it contains.

	USAGE:
	
		Want to take a peek at what's currently being traced? That's easy! just call the active method on any trace like so:
		
			currentTraces = myTrace.active();
		
		Access details about a specific active trace like so:
		
			myTraceDetails = myTrace.active()['myTraceName'];
			
		Create a trace like so:
		
			myTrace = new $trace('myTraceId');
		
		Add a trace event like so (it will log the time since the last event in the trace):
		
			myTrace.did('theThingIJustDid');
		
		Complete a trace event like so (it will flush the trace, take it out of the active trace registry, and display timing informaiton):
		
			myTrace.done();
		
		...or...
		
			myTrace.success();
			
		...or...
		
			myTrace.fail();
		
*/

// Holds all the active traces...
app.service('$traceService', function($log,$filter) {
	var traces = [];
	return {
		// Adds a trace...
		add: function( key ) {
			traces[key] = {
				trace	: '',				// The trace name
				start	: new Date(),		// The time the trace was started
				last	: new Date(),		// The last time traced
				times	: []				// The times for each trace
			};
		},
		
		// Continues a trace...
		did: function( key, text ) {
			try {
				// Record the time since we last recieved text...
				var now = new Date();
				// Record the time passed...
				var ms = now - traces[key].last;
				// Store now as last...
				traces[key].last = now;
				// Record the time since we were last called...
				traces[key].times.push({text:text, now: now});
				// Record the text passed and the time since the last call to the current trace...
				traces[key].trace += '.' + text + '(' + ms + 'ms)';
			} catch(e) {
				$log.warn("$trace: "+ key +": Tried to add to it but it doesn't exist!");
			}
		},
		
		// Lists a trace...
		active: function() {
			return traces;
		},

		// Returns the time elapsed between events...
		offset: function (key,a,b) {
			var times = traces[key].times;
			return times[b].now-times[a].now;
		},

		//Added this to show current time tracing
		now: function ( key ) {
			var times = traces[key].times;
			if(!times||times.length==0) return;
			var log = '';
			var prev = times[0].now;
			var start = times[0].now;
			angular.forEach(times,function (val,index) {
				log+=('['+$filter('date')(val.now,'hh:mm:ss:sss')+'(+'+(val.now-start)+'/Δ'+(val.now-prev)+')]['+index+']'+val.text+'\n');
				prev = val.now;
			});
			return log;
		},
		
		// Flushes a trace (ends it)...
		flush: function( key ) {
			try {
				// Record the time since we last recieved text...
				total = new Date() - traces[key].start;
				var text = '[' + key + ']' + traces[key].trace + ' ' + total + 'ms total.';
				delete traces[key];
				return text;
			} catch(e) {
				$log.warn("$trace: "+ key +": Tried to end it but it doesn't exist!");
			}
		}
	}
});

app.factory('$trace', function ($log, $traceService) {
	// The factory to generate (start a new trace) an object...
    return function (key) {
		var key = key;
		$traceService.add(key);
		
		return {

			// Return all open traces...
			active: function () { return $traceService.active(); },

			// Return history of traces...
			//history: function () { return history; },
			
			did: function (text) { $traceService.did(key, text); },
			
			// Complete a trace...
			done: function (text, status) { 
				// Default to an indifferent color...
				var color = '#444';
				// If a status was provided, set the color to be green if success, red if fail...
				if (typeof status !== 'undefined') color = status ? '#272' : '#722';				
				// Dump it to the screen...
				$log.debug('%c ' + $traceService.flush(key), 'background: #eee; color: ' + color);
			},
			offset: function (a,b) {
				return $traceService.offset(key,a,b);
			},
			now: function () {
				return $traceService.now(key);
			},
			
			// Shortcuts to success/fail traces...
			success: function ( text ) { this.done(text, true); },
			fail: function ( text ) { this.done(text, false); }
		}
    };
});
app.service('constants', (function() {
    function isIE(userAgent) {
        userAgent = userAgent || navigator.userAgent;
        return userAgent.indexOf("MSIE ") > -1 || userAgent.indexOf("Trident/") > -1 || userAgent.indexOf("Edge/") > -1;
    }

    var isIE = isIE();

    return {
        VIEW: {
            MY_VIEW_DISPLAY_TEXT: 'My View’s Data',
            MY_ORG_VIEW_DISPLAY_TEXT: 'My Org’s Data',
            MY_TEAM_VIEW_SOME_DISPLAY_TEXT: 'My Selected Team Member’s Data',
            MY_TEAM_VIEW_DISPLAY_TEXT: 'My Team’s Data',

            getUserViewDisplayText: function (firstName, lastName) {
                return lastName + ', ' + firstName + '’s Data';
            }
        },
        VIEW_TYPE: {
          MY_VIEW: 1,
          ORG_VIEW: 2,
          TEAM_VIEW: 3,
          TEAM_VIEW_ONE: 31,
          TEAM_VIEW_SOME: 32
        },

        isIE: isIE
    }
}));

// A Simplex event factory for Angular (abstraction for WebSocket and evented systems)...
app.factory('event', function ($rootScope, socket) {
    // Split the event into it's parts
    var handleEvent = function (event, data) {
        try {
            var parts = event.split(':');
        } catch(error) {
            console.log('Malformed or missing event name: ' + event);
        }
        
        // Make it easier to read below...
        var location = parts[0];
        var what = parts[1];
        var data = data ? data : parts[2];
        
        // Represent the data for the log (console)...
        datalog = (typeof parts[2] === 'undefined') ? data : parts[2];
        if (dataLog) {
            if (dataLog.length > 50) dataLog = dataLog.length + ' bytes';
                else dataLog = '"' + dataLog + '"';					
        }
        
        // Handle local and remote events differently...
        switch (location) {
            case 'local':
                // Notify the console...
                console.log('<-GUI EVENT-local', what + '(' + dataLog + ')');
                // Call the event...
                events[what](data);
                break;
            case 'remote':
                GWT.activity();
                // Notify the console...
                console.log('APP-> EVENT-remote', what + '(' + dataLog + ')');
                // Call the event...
                socket.emit(what, data);
                break;
            case 'server':
                // Incoming events from the server (server-client)				
                console.log('<-SVR EVENT-server', what + '(' + dataLog + ')');
                
                // These are server pushes, TODO: Move these out into a strategy router...
                switch (what) {
                    case 's':
                        // Decompress and decode the state...
                        var c = JSON.parse(LZString.decompressFromBase64(data));
                        console.log(c);
                        // CSS...
                        if ( c.c ) document.getElementById('appStyle').innerHTML = c.c;
                        // HTML (angular code is recompiled with the dynamic $watch directive above)...
                        if ( c.h ) {
                            // TODO: Find the DOM element it belongs in... (ID)
                            if ( c.x ) {
                                console.log('Compiling', c.x.HTML );
                                var htmlId = c.x.HTML;
                                var ele = document.getElementById(htmlId);
                                ele.innerHTML = c.h;
                                var content = $compile(ele)($scope);
                            } else {
                                console.log('unknown location for HTML, not compiling');
                            }
                        }
                        // javascript can invoke new controllers/directives/services by calling angularize()...
                        if ( c.j ) {
                            eval( 'window.d = ' + c.j );
                            console.log ('controlerized', c.name);
                            angularize('controller', c.name, 'appContent', window.d);
                        }
                        break;
                    case 'd':
                        // Data change...
                        data = JSON.parse(data);
                        console.log('new data recieved');
                        if (data.LoginResult) {
                            updateLogin(data.User);
                        }
                        break;
                    case 'a':
                        my.note(data);
                        break;
                    case 'patch':
                        switch (data) {
                            case 'reboot':
                                location.reload();
                                break;
                            case 'css':
                                $('#css').replaceWith('<link id="css" rel="stylesheet" href="/dashboard/app.css?t=' + Date.now() + '"></link>');
                                break;
                            case 'state':
                                $appEvent('local:state:appTaskList');
                                break;
                        }
                        break;
                    case 'request':
                        console.log('requests');
                        switch(data) {
                            case 'login':
                                console.log('here');
                                if (typeof my.login !== 'undefined') my.event('remote:data', {type:'login', data: my.login});
                                break;
                        }
                    case 'debug':
                        console.log('Server DEBUG:', data);
                }
                break;
            default:
                // Unknown event types...
                console.log('.DIE. EVENT-unknown (' + location + ')', what + '(' + data + ')');
                break;
        }        
    };
        
    return {
    call: function (eventName, dataOrCallback) {
        var args = arguments;

    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
            var args = arguments;
            $rootScope.$apply(function () { if (callback) callback.apply(socket, args); });
      })
    }
    };
});

app.service('gsiCppWidget', 
  function() {
    
    this.getCcpWidgetSortFields = function(chartType, chartLabel) {
    	var fields = [];

    	chartType = chartType.toLowerCase();
    	chartLabel = chartLabel.toLowerCase();
    	switch(chartType) {
    		case 'issue': 
    			switch(chartLabel) {
    				case 'status' :
    					fields.push('label2');
    					break;
    				case 'domain' :
    					fields.push('label1');
    					break;
    				case 'numberperpatient' :
    					fields.push('label2')
    					break;
    			}
    			break;
    		case 'goal':
    			switch(chartLabel) {
    				case 'status' :
    					fields.push('label1');
    					break;
    				case 'numberperpatient' :
    					fields.push('label1')
    					break;
    			}
    			break;
    		case 'intervention':
    			switch(chartLabel) {
    				case 'status' :
    					fields.push('label1');
    					break;
    				case 'numberperpatient' :
    					fields.push('label1')
    					break;
    			}
    			break;
    		case 'assessment':
    			fields.push('label1');
    			fields.push('label3')
    			break;
    	}
       	return fields;
    };

    this.getCcpSortLabel = function(chartLabel, inputLabel, priorityStatusList) {
		var sortLabelValues = [];
    	if (chartLabel.toLowerCase() === 'numberperpatient' && priorityStatusList !== undefined && priorityStatusList != '') {
			return priorityStatusList.split('|');
    	}
    	else {
    		return inputLabel;
    	}
    }
  }
)
app.provider('gsiConfig', gsiConfigProvider);

    /** @ngInject */
    function gsiConfigProvider() {
        // Default configuration
        var gsiConfiguration = {
            'disableCustomScrollbars'        : false,
            'disableMdInkRippleOnMobile'     : true,
            'disableCustomScrollbarsOnMobile': true
        };

        // Methods
        this.config = config;

        //////////

        /**
         * Extend default configuration with the given one
         *
         * @param configuration
         */
        function config(configuration)
        {
            gsiConfiguration = angular.extend({}, gsiConfiguration, configuration);
        }

        /**
         * Service
         */
        this.$get = function ()
        {
            var service = {
                getConfig: getConfig,
                setConfig: setConfig
            };

            return service;

            //////////

            /**
             * Returns a config value
             */
            function getConfig(configName)
            {
                if ( angular.isUndefined(gsiConfiguration[configName]) )
                {
                    return false;
                }

                return gsiConfiguration[configName];
            }

            /**
             * Creates or updates config object
             *
             * @param configName
             * @param configValue
             */
            function setConfig(configName, configValue)
            {
                gsiConfiguration[configName] = configValue;
            }
        };
    }
app.factory('gsiGenerator', gsiGeneratorService);

    /** @ngInject */
    function gsiGeneratorService(gsiTheming, $log)
    {
        // Storage for simplified themes object
        var themes = {};

        var service = {
            generate: generate,
            rgba    : rgba
        };

        return service;

        //////////

        /**
         * Generate less variables for each theme from theme's
         * palette by using material color naming conventions
         */
        function generate()
        {
            var registeredThemes = gsiTheming.getRegisteredThemes();
            var registeredPalettes = gsiTheming.getRegisteredPalettes();

            // First, create a simplified object that stores
            // all registered themes and their colors

            // Iterate through registered themes
            angular.forEach(registeredThemes, function (registeredTheme)
            {
                themes[registeredTheme.name] = {};

                // Iterate through color types (primary, accent, warn & background)
                angular.forEach(registeredTheme.colors, function (colorType, colorTypeName)
                {
                    themes[registeredTheme.name][colorTypeName] = {
                        'name'  : colorType.name,
                        'levels': {
                            'default': {
                                'color'    : rgba(registeredPalettes[colorType.name][colorType.hues.default].value),
                                'contrast1': rgba(registeredPalettes[colorType.name][colorType.hues.default].contrast, 1),
                                'contrast2': rgba(registeredPalettes[colorType.name][colorType.hues.default].contrast, 2),
                                'contrast3': rgba(registeredPalettes[colorType.name][colorType.hues.default].contrast, 3),
                                'contrast4': rgba(registeredPalettes[colorType.name][colorType.hues.default].contrast, 4)
                            },
                            'hue1'   : {
                                'color'    : rgba(registeredPalettes[colorType.name][colorType.hues['hue-1']].value),
                                'contrast1': rgba(registeredPalettes[colorType.name][colorType.hues['hue-1']].contrast, 1),
                                'contrast2': rgba(registeredPalettes[colorType.name][colorType.hues['hue-1']].contrast, 2),
                                'contrast3': rgba(registeredPalettes[colorType.name][colorType.hues['hue-1']].contrast, 3),
                                'contrast4': rgba(registeredPalettes[colorType.name][colorType.hues['hue-1']].contrast, 4)
                            },
                            'hue2'   : {
                                'color'    : rgba(registeredPalettes[colorType.name][colorType.hues['hue-2']].value),
                                'contrast1': rgba(registeredPalettes[colorType.name][colorType.hues['hue-2']].contrast, 1),
                                'contrast2': rgba(registeredPalettes[colorType.name][colorType.hues['hue-2']].contrast, 2),
                                'contrast3': rgba(registeredPalettes[colorType.name][colorType.hues['hue-2']].contrast, 3),
                                'contrast4': rgba(registeredPalettes[colorType.name][colorType.hues['hue-2']].contrast, 4)
                            },
                            'hue3'   : {
                                'color'    : rgba(registeredPalettes[colorType.name][colorType.hues['hue-3']].value),
                                'contrast1': rgba(registeredPalettes[colorType.name][colorType.hues['hue-3']].contrast, 1),
                                'contrast2': rgba(registeredPalettes[colorType.name][colorType.hues['hue-3']].contrast, 2),
                                'contrast3': rgba(registeredPalettes[colorType.name][colorType.hues['hue-3']].contrast, 3),
                                'contrast4': rgba(registeredPalettes[colorType.name][colorType.hues['hue-3']].contrast, 4)
                            }
                        }
                    };
                });
            });

            // Process themes one more time and then store them in the service for external use
            processAndStoreThemes(themes);

            // Iterate through simplified themes
            // object and create style variables
            var styleVars = {};

            // Iterate through registered themes
            angular.forEach(themes, function (theme, themeName)
            {
                styleVars = {};
                styleVars['@themeName'] = themeName;

                // Iterate through color types (primary, accent, warn & background)
                angular.forEach(theme, function (colorTypes, colorTypeName)
                {
                    // Iterate through color levels (default, hue1, hue2 & hue3)
                    angular.forEach(colorTypes.levels, function (colors, colorLevelName)
                    {
                        // Iterate through color name (color, contrast1, contrast2, contrast3 & contrast4)
                        angular.forEach(colors, function (color, colorName)
                        {
                            styleVars['@' + colorTypeName + ucfirst(colorLevelName) + ucfirst(colorName)] = color;
                        });
                    });
                });

                // Render styles
                render(styleVars);
            });
        }

        // ---------------------------
        //  INTERNAL HELPER FUNCTIONS
        // ---------------------------

        /**
         * Process and store themes for global use
         *
         * @param _themes
         */
        function processAndStoreThemes(_themes)
        {
            // Here we will go through every registered theme one more time
            // and try to simplify their objects as much as possible for
            // easier access to their properties.
            var themes = angular.copy(_themes);

            // Iterate through themes
            angular.forEach(themes, function (theme)
            {
                // Iterate through color types (primary, accent, warn & background)
                angular.forEach(theme, function (colorType, colorTypeName)
                {
                    theme[colorTypeName] = colorType.levels;
                    theme[colorTypeName].color = colorType.levels.default.color;
                    theme[colorTypeName].contrast1 = colorType.levels.default.contrast1;
                    theme[colorTypeName].contrast2 = colorType.levels.default.contrast2;
                    theme[colorTypeName].contrast3 = colorType.levels.default.contrast3;
                    theme[colorTypeName].contrast4 = colorType.levels.default.contrast4;
                    delete theme[colorTypeName].default;
                });
            });

            // Store themes and set selected theme for the first time
            gsiTheming.setThemesList(themes);

            // Remember selected theme.
            var selectedTheme = null; //$cookies.get('selectedTheme');

            if ( selectedTheme )
            {
                gsiTheming.setActiveTheme(selectedTheme);
            }
            else
            {
                gsiTheming.setActiveTheme('default');
            }
        }


        /**
         * Render css files
         *
         * @param styleVars
         */
        function render(styleVars)
        {
            var cssTemplate = '[md-theme="@themeName"] a {\n    color: @accentDefaultColor;\n}\n\n[md-theme="@themeName"] .secondary-text,\n[md-theme="@themeName"] .icon {\n    color: @backgroundDefaultContrast2;\n}\n\n[md-theme="@themeName"] .hint-text,\n[md-theme="@themeName"] .disabled-text {\n    color: @backgroundDefaultContrast3;\n}\n\n[md-theme="@themeName"] .fade-text,\n[md-theme="@themeName"] .divider {\n    color: @backgroundDefaultContrast4;\n}\n\n/* Primary */\n[md-theme="@themeName"] .md-primary-bg {\n    background-color: @primaryDefaultColor;\n    color: @primaryDefaultContrast1;\n}\n\n[md-theme="@themeName"] .md-primary-bg .secondary-text,\n[md-theme="@themeName"] .md-primary-bg .icon {\n    color: @primaryDefaultContrast2;\n}\n\n[md-theme="@themeName"] .md-primary-bg .hint-text,\n[md-theme="@themeName"] .md-primary-bg .disabled-text {\n    color: @primaryDefaultContrast3;\n}\n\n[md-theme="@themeName"] .md-primary-bg .fade-text,\n[md-theme="@themeName"] .md-primary-bg .divider {\n    color: @primaryDefaultContrast4;\n}\n\n/* Primary, Hue-1 */\n[md-theme="@themeName"] .md-primary-bg.md-hue-1 {\n    background-color: @primaryHue1Color;\n    color: @primaryHue1Contrast1;\n}\n\n[md-theme="@themeName"] .md-primary-bg.md-hue-1 .secondary-text,\n[md-theme="@themeName"] .md-primary-bg.md-hue-1 .icon {\n    color: @primaryHue1Contrast2;\n}\n\n[md-theme="@themeName"] .md-primary-bg.md-hue-1 .hint-text,\n[md-theme="@themeName"] .md-primary-bg.md-hue-1 .disabled-text {\n    color: @primaryHue1Contrast3;\n}\n\n[md-theme="@themeName"] .md-primary-bg.md-hue-1 .fade-text,\n[md-theme="@themeName"] .md-primary-bg.md-hue-1 .divider {\n    color: @primaryHue1Contrast4;\n}\n\n/* Primary, Hue-2 */\n[md-theme="@themeName"] .md-primary-bg.md-hue-2 {\n    background-color: @primaryHue2Color;\n    color: @primaryHue2Contrast1;\n}\n\n[md-theme="@themeName"] .md-primary-bg.md-hue-2 .secondary-text,\n[md-theme="@themeName"] .md-primary-bg.md-hue-2 .icon {\n    color: @primaryHue2Contrast2;\n}\n\n[md-theme="@themeName"] .md-primary-bg.md-hue-2 .hint-text,\n[md-theme="@themeName"] .md-primary-bg.md-hue-2 .disabled-text {\n    color: @primaryHue2Contrast3;\n}\n\n[md-theme="@themeName"] .md-primary-bg.md-hue-2 .fade-text,\n[md-theme="@themeName"] .md-primary-bg.md-hue-2 .divider {\n    color: @primaryHue2Contrast4;\n}\n\n/* Primary, Hue-3 */\n[md-theme="@themeName"] .md-primary-bg.md-hue-3 {\n    background-color: @primaryHue3Color;\n    color: @primaryHue3Contrast1;\n}\n\n[md-theme="@themeName"] .md-primary-bg.md-hue-3 .secondary-text,\n[md-theme="@themeName"] .md-primary-bg.md-hue-3 .icon {\n    color: @primaryHue3Contrast1;\n}\n\n[md-theme="@themeName"] .md-primary-bg.md-hue-3 .hint-text,\n[md-theme="@themeName"] .md-primary-bg.md-hue-3 .disabled-text {\n    color: @primaryHue3Contrast3;\n}\n\n[md-theme="@themeName"] .md-primary-bg.md-hue-3 .fade-text,\n[md-theme="@themeName"] .md-primary-bg.md-hue-3 .divider {\n    color: @primaryHue3Contrast4;\n}\n\n/* Primary foreground */\n[md-theme="@themeName"] .md-primary-fg {\n    color: @primaryDefaultColor !important;\n}\n\n/* Primary foreground, Hue-1 */\n[md-theme="@themeName"] .md-primary-fg.md-hue-1 {\n    color: @primaryHue1Color !important;\n}\n\n/* Primary foreground, Hue-2 */\n[md-theme="@themeName"] .md-primary-fg.md-hue-2 {\n    color: @primaryHue2Color !important;\n}\n\n/* Primary foreground, Hue-3 */\n[md-theme="@themeName"] .md-primary-fg.md-hue-3 {\n    color: @primaryHue3Color !important;\n}\n\n\n/* Accent */\n[md-theme="@themeName"] .md-accent-bg {\n    background-color: @accentDefaultColor;\n    color: @accentDefaultContrast1;\n}\n\n[md-theme="@themeName"] .md-accent-bg .secondary-text,\n[md-theme="@themeName"] .md-accent-bg .icon {\n    color: @accentDefaultContrast2;\n}\n\n[md-theme="@themeName"] .md-accent-bg .hint-text,\n[md-theme="@themeName"] .md-accent-bg .disabled-text {\n    color: @accentDefaultContrast3;\n}\n\n[md-theme="@themeName"] .md-accent-bg .fade-text,\n[md-theme="@themeName"] .md-accent-bg .divider {\n    color: @accentDefaultContrast4;\n}\n\n/* Accent, Hue-1 */\n[md-theme="@themeName"] .md-accent-bg.md-hue-1 {\n    background-color: @accentHue1Color;\n    color: @accentHue1Contrast1;\n}\n\n[md-theme="@themeName"] .md-accent-bg.md-hue-1 .secondary-text,\n[md-theme="@themeName"] .md-accent-bg.md-hue-1 .icon {\n    color: @accentHue1Contrast2;\n}\n\n[md-theme="@themeName"] .md-accent-bg.md-hue-1 .hint-text,\n[md-theme="@themeName"] .md-accent-bg.md-hue-1 .disabled-text {\n    color: @accentHue1Contrast3;\n}\n\n[md-theme="@themeName"] .md-accent-bg.md-hue-1 .fade-text,\n[md-theme="@themeName"] .md-accent-bg.md-hue-1 .divider {\n    color: @accentHue1Contrast4;\n}\n\n/* Accent, Hue-2 */\n[md-theme="@themeName"] .md-accent-bg.md-hue-2 {\n    background-color: @accentHue2Color;\n    color: @accentHue2Contrast1;\n}\n\n[md-theme="@themeName"] .md-accent-bg.md-hue-2 .secondary-text,\n[md-theme="@themeName"] .md-accent-bg.md-hue-2 .icon {\n    color: @accentHue2Contrast2;\n}\n\n[md-theme="@themeName"] .md-accent-bg.md-hue-2 .hint-text,\n[md-theme="@themeName"] .md-accent-bg.md-hue-2 .disabled-text {\n    color: @accentHue2Contrast3;\n}\n\n[md-theme="@themeName"] .md-accent-bg.md-hue-2 .fade-text,\n[md-theme="@themeName"] .md-accent-bg.md-hue-2 .divider {\n    color: @accentHue2Contrast4;\n}\n\n/* Accent, Hue-3 */\n[md-theme="@themeName"] .md-accent-bg.md-hue-3 {\n    background-color: @accentHue3Color;\n    color: @accentHue3Contrast1;\n}\n\n[md-theme="@themeName"] .md-accent-bg.md-hue-3 .secondary-text,\n[md-theme="@themeName"] .md-accent-bg.md-hue-3 .icon {\n    color: @accentHue3Contrast1;\n}\n\n[md-theme="@themeName"] .md-accent-bg.md-hue-3 .hint-text,\n[md-theme="@themeName"] .md-accent-bg.md-hue-3 .disabled-text {\n    color: @accentHue3Contrast3;\n}\n\n[md-theme="@themeName"] .md-accent-bg.md-hue-3 .fade-text,\n[md-theme="@themeName"] .md-accent-bg.md-hue-3 .divider {\n    color: @accentHue3Contrast4;\n}\n\n/* Accent foreground */\n[md-theme="@themeName"] .md-accent-fg {\n    color: @accentDefaultColor !important;\n}\n\n/* Accent foreground, Hue-1 */\n[md-theme="@themeName"] .md-accent-fg.md-hue-1 {\n    color: @accentHue1Color !important;\n}\n\n/* Accent foreground, Hue-2 */\n[md-theme="@themeName"] .md-accent-fg.md-hue-2 {\n    color: @accentHue2Color !important;\n}\n\n/* Accent foreground, Hue-3 */\n[md-theme="@themeName"] .md-accent-fg.md-hue-3 {\n    color: @accentHue3Color !important;\n}\n\n\n/* Warn */\n[md-theme="@themeName"] .md-warn-bg {\n    background-color: @warnDefaultColor;\n    color: @warnDefaultContrast1;\n}\n\n[md-theme="@themeName"] .md-warn-bg .secondary-text,\n[md-theme="@themeName"] .md-warn-bg .icon {\n    color: @warnDefaultContrast2;\n}\n\n[md-theme="@themeName"] .md-warn-bg .hint-text,\n[md-theme="@themeName"] .md-warn-bg .disabled-text {\n    color: @warnDefaultContrast3;\n}\n\n[md-theme="@themeName"] .md-warn-bg .fade-text,\n[md-theme="@themeName"] .md-warn-bg .divider {\n    color: @warnDefaultContrast4;\n}\n\n/* Warn, Hue-1 */\n[md-theme="@themeName"] .md-warn-bg.md-hue-1 {\n    background-color: @warnHue1Color;\n    color: @warnHue1Contrast1;\n}\n\n[md-theme="@themeName"] .md-warn-bg.md-hue-1 .secondary-text,\n[md-theme="@themeName"] .md-warn-bg.md-hue-1 .icon {\n    color: @warnHue1Contrast2;\n}\n\n[md-theme="@themeName"] .md-warn-bg.md-hue-1 .hint-text,\n[md-theme="@themeName"] .md-warn-bg.md-hue-1 .disabled-text {\n    color: @warnHue1Contrast3;\n}\n\n[md-theme="@themeName"] .md-warn-bg.md-hue-1 .fade-text,\n[md-theme="@themeName"] .md-warn-bg.md-hue-1 .divider {\n    color: @warnHue1Contrast4;\n}\n\n/* Warn, Hue-2 */\n[md-theme="@themeName"] .md-warn-bg.md-hue-2 {\n    background-color: @warnHue2Color;\n    color: @warnHue2Contrast1;\n}\n\n[md-theme="@themeName"] .md-warn-bg.md-hue-2 .secondary-text,\n[md-theme="@themeName"] .md-warn-bg.md-hue-2 .icon {\n    color: @warnHue2Contrast2;\n}\n\n[md-theme="@themeName"] .md-warn-bg.md-hue-2 .hint-text,\n[md-theme="@themeName"] .md-warn-bg.md-hue-2 .disabled-text {\n    color: @warnHue2Contrast3;\n}\n\n[md-theme="@themeName"] .md-warn-bg.md-hue-2 .fade-text,\n[md-theme="@themeName"] .md-warn-bg.md-hue-2 .divider {\n    color: @warnHue2Contrast4;\n}\n\n/* Warn, Hue-3 */\n[md-theme="@themeName"] .md-warn-bg.md-hue-3 {\n    background-color: @warnHue3Color;\n    color: @warnHue3Contrast1;\n}\n\n[md-theme="@themeName"] .md-warn-bg.md-hue-3 .secondary-text,\n[md-theme="@themeName"] .md-warn-bg.md-hue-3 .icon {\n    color: @warnHue3Contrast1;\n}\n\n[md-theme="@themeName"] .md-warn-bg.md-hue-3 .hint-text,\n[md-theme="@themeName"] .md-warn-bg.md-hue-3 .disabled-text {\n    color: @warnHue3Contrast3;\n}\n\n[md-theme="@themeName"] .md-warn-bg.md-hue-3 .fade-text,\n[md-theme="@themeName"] .md-warn-bg.md-hue-3 .divider {\n    color: @warnHue3Contrast4;\n}\n\n/* Warn foreground */\n[md-theme="@themeName"] .md-warn-fg {\n    color: @warnDefaultColor !important;\n}\n\n/* Warn foreground, Hue-1 */\n[md-theme="@themeName"] .md-warn-fg.md-hue-1 {\n    color: @warnHue1Color !important;\n}\n\n/* Warn foreground, Hue-2 */\n[md-theme="@themeName"] .md-warn-fg.md-hue-2 {\n    color: @warnHue2Color !important;\n}\n\n/* Warn foreground, Hue-3 */\n[md-theme="@themeName"] .md-warn-fg.md-hue-3 {\n    color: @warnHue3Color !important;\n}\n\n/* Background */\n[md-theme="@themeName"] .md-background-bg {\n    background-color: @backgroundDefaultColor;\n    color: @backgroundDefaultContrast1;\n}\n\n[md-theme="@themeName"] .md-background-bg .secondary-text,\n[md-theme="@themeName"] .md-background-bg .icon {\n    color: @backgroundDefaultContrast2;\n}\n\n[md-theme="@themeName"] .md-background-bg .hint-text,\n[md-theme="@themeName"] .md-background-bg .disabled-text {\n    color: @backgroundDefaultContrast3;\n}\n\n[md-theme="@themeName"] .md-background-bg .fade-text,\n[md-theme="@themeName"] .md-background-bg .divider {\n    color: @backgroundDefaultContrast4;\n}\n\n/* Background, Hue-1 */\n[md-theme="@themeName"] .md-background-bg.md-hue-1 {\n    background-color: @backgroundHue1Color;\n    color: @backgroundHue1Contrast1;\n}\n\n[md-theme="@themeName"] .md-background-bg.md-hue-1 .secondary-text,\n[md-theme="@themeName"] .md-background-bg.md-hue-1 .icon {\n    color: @backgroundHue1Contrast2;\n}\n\n[md-theme="@themeName"] .md-background-bg.md-hue-1 .hint-text,\n[md-theme="@themeName"] .md-background-bg.md-hue-1 .disabled-text {\n    color: @backgroundHue1Contrast3;\n}\n\n[md-theme="@themeName"] .md-background-bg.md-hue-1 .fade-text,\n[md-theme="@themeName"] .md-background-bg.md-hue-1 .divider {\n    color: @backgroundHue1Contrast4;\n}\n\n/* Background, Hue-2 */\n[md-theme="@themeName"] .md-background-bg.md-hue-2 {\n    background-color: @backgroundHue2Color;\n    color: @backgroundHue2Contrast1;\n}\n\n[md-theme="@themeName"] .md-background-bg.md-hue-2 .secondary-text,\n[md-theme="@themeName"] .md-background-bg.md-hue-2 .icon {\n    color: @backgroundHue2Contrast2;\n}\n\n[md-theme="@themeName"] .md-background-bg.md-hue-2 .hint-text,\n[md-theme="@themeName"] .md-background-bg.md-hue-2 .disabled-text {\n    color: @backgroundHue2Contrast3;\n}\n\n[md-theme="@themeName"] .md-background-bg.md-hue-2 .fade-text,\n[md-theme="@themeName"] .md-background-bg.md-hue-2 .divider {\n    color: @backgroundHue2Contrast4;\n}\n\n/* Background, Hue-3 */\n[md-theme="@themeName"] .md-background-bg.md-hue-3 {\n    background-color: @backgroundHue3Color;\n    color: @backgroundHue3Contrast1;\n}\n\n[md-theme="@themeName"] .md-background-bg.md-hue-3 .secondary-text,\n[md-theme="@themeName"] .md-background-bg.md-hue-3 .icon {\n    color: @backgroundHue3Contrast1;\n}\n\n[md-theme="@themeName"] .md-background-bg.md-hue-3 .hint-text,\n[md-theme="@themeName"] .md-background-bg.md-hue-3 .disabled-text {\n    color: @backgroundHue3Contrast3;\n}\n\n[md-theme="@themeName"] .md-background-bg.md-hue-3 .fade-text,\n[md-theme="@themeName"] .md-background-bg.md-hue-3 .divider {\n    color: @backgroundHue3Contrast4;\n}\n\n/* Background foreground */\n[md-theme="@themeName"] .md-background-fg {\n    color: @backgroundDefaultColor !important;\n}\n\n/* Background foreground, Hue-1 */\n[md-theme="@themeName"] .md-background-fg.md-hue-1 {\n    color: @backgroundHue1Color !important;\n}\n\n/* Background foreground, Hue-2 */\n[md-theme="@themeName"] .md-background-fg.md-hue-2 {\n    color: @backgroundHue2Color !important;\n}\n\n/* Background foreground, Hue-3 */\n[md-theme="@themeName"] .md-background-fg.md-hue-3 {\n    color: @backgroundHue3Color !important;\n}';

            var regex = new RegExp(Object.keys(styleVars).join('|'), 'gi');
            var css = cssTemplate.replace(regex, function (matched)
            {
                return styleVars[matched];
            });

            var headEl = angular.element('head');
            var styleEl = angular.element('<style type="text/css"></style>');
            styleEl.html(css);
            headEl.append(styleEl);
        }

        /**
         * Convert color array to rgb/rgba
         * Also apply contrasts if needed
         *
         * @param color
         * @param _contrastLevel
         * @returns {string}
         */
        function rgba(color, _contrastLevel)
        {
            var contrastLevel = _contrastLevel || false;

            // Convert 255,255,255,0.XX to 255,255,255
            // According to Google's Material design specs, white primary
            // text must have opacity of 1 and we will fix that here
            // because Angular Material doesn't care about that spec
            if ( color.length === 4 && color[0] === 255 && color[1] === 255 && color[2] === 255 )
            {
                color.splice(3, 4);
            }

            // If contrast level provided, apply it to the current color
            if ( contrastLevel )
            {
                color = applyContrast(color, contrastLevel);
            }

            // Convert color array to color string (rgb/rgba)
            if ( color.length === 3 )
            {
                return 'rgb(' + color.join(',') + ')';
            }
            else if ( color.length === 4 )
            {
                return 'rgba(' + color.join(',') + ')';
            }
            else
            {
                $log.error('Invalid number of arguments supplied in the color array: ' + color.length + '\n' + 'The array must have 3 or 4 colors.');
            }
        }

        /**
         * Apply given contrast level to the given color
         *
         * @param color
         * @param contrastLevel
         */
        function applyContrast(color, contrastLevel)
        {
            var contrastLevels = {
                'white': {
                    '1': '1',
                    '2': '0.7',
                    '3': '0.3',
                    '4': '0.12'
                },
                'black': {
                    '1': '0.87',
                    '2': '0.54',
                    '3': '0.26',
                    '4': '0.12'
                }
            };

            // If white
            if ( color[0] === 255 && color[1] === 255 && color[2] === 255 )
            {
                color[3] = contrastLevels.white[contrastLevel];
            }
            // If black
            else if ( color[0] === 0 && color[1] === 0, color[2] === 0 )
            {
                color[3] = contrastLevels.black[contrastLevel];
            }

            return color;
        }

        /**
         * Uppercase first
         */
        function ucfirst(string)
        {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }
    }

app.factory('gsiSideNav', gsiSideNav);

function gsiSideNav($log) {

    var navs = {};

    return {
        get: get,
        open: open,
        close: close,
        toggle: toggle,
        go: go,
        quit:quit,
        isInitialized:isInitialized
    };


    // Initialize
    function get(name, isOpen, defaultApp) {
        if(!navs[name]){
            navs[name]={
                isOpen: !!isOpen,
                active: defaultApp,
                menus:{},
                selectedTabIndex:0
             };
         }
         navs[name].menus[defaultApp]=true;
        return navs[name]
    }

    //Open/close side menu
    function open(name) {
        get(name).isOpen = true;
    }
    function close(name) {
        get(name).isOpen = false;
    }
    function toggle(name) {
        get(name).isOpen = !navs[name].isOpen;
    }

    function isInitialized(name,appName){
        return !!get(name).menus[appName];
    }

    //Switch active apps
    function go(name, appName,data){


        if (typeof(data) == "undefined") {
            // auto-detect tab index via name, called from GWT event
            for (var app in QA.app.userApps) {
                if (QA.app.userApps[app].name === appName) {
                    data = QA.app.userApps[app];
                    break;
                }
            }
        }

        //Currently, the care plan app is using popup, it should not be an active app.
        if (isTabApp(appName,data)) {
            $log.debug(appName+ " is Activated.");
            get(name).active = appName;
            QA.monitor.route(appName);
        }

        if (appName==='appHome') {
            //Dirty patch... maybe we should conside adding appHome to userApp
            get(name).selectedTabIndex = 0;
            return true;
        } else if(appName==='appCareBookSearch'){
            get(name).selectedTabIndex = 1;

        } else if (data&&data.index&&!data.hasWindow) {
            get(name).selectedTabIndex = data.index;
        }
        //Check if app is already initialized
        if(isTabApp(appName,data)&&!isInitialized(name,appName)){
            $log.debug(appName+ " is not initialized.");
            get(name).menus[appName]=true;
            return false;
        }else{
            return true;
        }
    }

    function quit(name, appName, data) {
        $log.debug("Quitting "+appName);
        if(isTabApp(appName,data)){
            get(name).selectedTabIndex = 0;
            get(name).menus[appName]=false;
            return true;
        }
        $log.debug("Did not quit "+appName+". This app is likely to open another tab/window")
        return false;
    }

    //helper function
    function isTabApp(appName,data){
        return data&&!data.hasWindow || appName==='appCareBookSearch';
    }

}

app.provider('gsiTheming', gsiThemingProvider);

    /** @ngInject */
    function gsiThemingProvider()
    {

        // Inject $log service
        var $log = angular.injector(['ng']).get('$log');

        var registeredPalettes,
            registeredThemes;

        // Methods
        this.setRegisteredPalettes = setRegisteredPalettes;
        this.setRegisteredThemes = setRegisteredThemes;

        //////////

        /**
         * Set registered palettes
         *
         * @param _registeredPalettes
         */
        function setRegisteredPalettes(_registeredPalettes)
        {
            registeredPalettes = _registeredPalettes;
        }

        /**
         * Set registered themes
         *
         * @param _registeredThemes
         */
        function setRegisteredThemes(_registeredThemes)
        {
            registeredThemes = _registeredThemes;
        }

        /**
         * Service
         */
        this.$get = function ()
        {
            var service = {
                getRegisteredPalettes: getRegisteredPalettes,
                getRegisteredThemes  : getRegisteredThemes,
                setActiveTheme       : setActiveTheme,
                setThemesList        : setThemesList,
                themes               : {
                    list  : {},
                    active: {
                        'name' : '',
                        'theme': {}
                    }
                }
            };

            return service;

            //////////

            /**
             * Get registered palettes
             *
             * @returns {*}
             */
            function getRegisteredPalettes()
            {
                return registeredPalettes;
            }

            /**
             * Get registered themes
             *
             * @returns {*}
             */
            function getRegisteredThemes()
            {
                return registeredThemes;
            }

            /**
             * Set active theme
             *
             * @param themeName
             */
            function setActiveTheme(themeName)
            {
                // If theme does not exist, fallback to the default theme
                if ( angular.isUndefined(service.themes.list[themeName]) )
                {
                    // If there is no theme called "default"...
                    if ( angular.isUndefined(service.themes.list.default) )
                    {
//                        $log.error('You must have at least one theme named "default"');
                        return;
                    }

                    $log.warn('The theme "' + themeName + '" does not exist! Falling back to the "default" theme.');

                    // Otherwise set theme to default theme
                    service.themes.active.name = 'default';
                    service.themes.active.theme = service.themes.list.default;
                    $cookies.put('selectedTheme', service.themes.active.name);

                    return;
                }

                service.themes.active.name = themeName;
                service.themes.active.theme = service.themes.list[themeName];
                $cookies.put('selectedTheme', themeName);
            }

            /**
             * Set available themes list
             *
             * @param themeList
             */
            function setThemesList(themeList)
            {
                service.themes.list = themeList;
            }
        };
    }

app.factory('gsiUtils', gsiUtils);
		
	function gsiUtils() {
		// Private variables
		var mobileDetect = new MobileDetect(window.navigator.userAgent),
			browserInfo = null;

		var service = {
			exists       : exists,
			detectBrowser: detectBrowser,
			guidGenerator: guidGenerator,
			isMobile     : isMobile,
			toggleInArray: toggleInArray
		};

		return service;

		//////////

		/**
		 * Check if item exists in a list
		 *
		 * @param item
		 * @param list
		 * @returns {boolean}
		 */
		function exists(item, list)
		{
			return list.indexOf(item) > -1;
		}

		/**
		 * Returns browser information
		 * from user agent data
		 *
		 * Found at http://www.quirksmode.org/js/detect.html
		 * but modified and updated to fit for our needs
		 */
		function detectBrowser()
		{
			// If we already tested, do not test again
			if ( browserInfo )
			{
				return browserInfo;
			}

			var browserData = [
				{
					string       : window.navigator.userAgent,
					subString    : 'Edge',
					versionSearch: 'Edge',
					identity     : 'Edge'
				},
				{
					string   : window.navigator.userAgent,
					subString: 'Chrome',
					identity : 'Chrome'
				},
				{
					string       : window.navigator.userAgent,
					subString    : 'OmniWeb',
					versionSearch: 'OmniWeb/',
					identity     : 'OmniWeb'
				},
				{
					string       : window.navigator.vendor,
					subString    : 'Apple',
					versionSearch: 'Version',
					identity     : 'Safari'
				},
				{
					prop    : window.opera,
					identity: 'Opera'
				},
				{
					string   : window.navigator.vendor,
					subString: 'iCab',
					identity : 'iCab'
				},
				{
					string   : window.navigator.vendor,
					subString: 'KDE',
					identity : 'Konqueror'
				},
				{
					string   : window.navigator.userAgent,
					subString: 'Firefox',
					identity : 'Firefox'
				},
				{
					string   : window.navigator.vendor,
					subString: 'Camino',
					identity : 'Camino'
				},
				{
					string   : window.navigator.userAgent,
					subString: 'Netscape',
					identity : 'Netscape'
				},
				{
					string       : window.navigator.userAgent,
					subString    : 'MSIE',
					identity     : 'Explorer',
					versionSearch: 'MSIE'
				},
				{
					string       : window.navigator.userAgent,
					subString    : 'Trident/7',
					identity     : 'Explorer',
					versionSearch: 'rv'
				},
				{
					string       : window.navigator.userAgent,
					subString    : 'Gecko',
					identity     : 'Mozilla',
					versionSearch: 'rv'
				},
				{
					string       : window.navigator.userAgent,
					subString    : 'Mozilla',
					identity     : 'Netscape',
					versionSearch: 'Mozilla'
				}
			];

			var osData = [
				{
					string   : window.navigator.platform,
					subString: 'Win',
					identity : 'Windows'
				},
				{
					string   : window.navigator.platform,
					subString: 'Mac',
					identity : 'Mac'
				},
				{
					string   : window.navigator.platform,
					subString: 'Linux',
					identity : 'Linux'
				},
				{
					string   : window.navigator.platform,
					subString: 'iPhone',
					identity : 'iPhone'
				},
				{
					string   : window.navigator.platform,
					subString: 'iPod',
					identity : 'iPod'
				},
				{
					string   : window.navigator.platform,
					subString: 'iPad',
					identity : 'iPad'
				},
				{
					string   : window.navigator.platform,
					subString: 'Android',
					identity : 'Android'
				}
			];

			var versionSearchString = '';

			function searchString(data)
			{
				for ( var i = 0; i < data.length; i++ )
				{
					var dataString = data[i].string;
					var dataProp = data[i].prop;

					versionSearchString = data[i].versionSearch || data[i].identity;

					if ( dataString )
					{
						if ( dataString.indexOf(data[i].subString) !== -1 )
						{
							return data[i].identity;

						}
					}
					else if ( dataProp )
					{
						return data[i].identity;
					}
				}
			}

			function searchVersion(dataString)
			{
				var index = dataString.indexOf(versionSearchString);

				if ( index === -1 )
				{
					return;
				}

				return parseInt(dataString.substring(index + versionSearchString.length + 1));
			}

			var browser = searchString(browserData) || 'unknown-browser';
			var version = searchVersion(window.navigator.userAgent) || searchVersion(window.navigator.appVersion) || 'unknown-version';
			var os = searchString(osData) || 'unknown-os';

			// Prepare and store the object
			browser = browser.toLowerCase();
			version = browser + '-' + version;
			os = os.toLowerCase();

			browserInfo = {
				browser: browser,
				version: version,
				os     : os
			};

			return browserInfo;
		}

		/**
		 * Generates a globally unique id
		 *
		 * @returns {*}
		 */
		function guidGenerator()
		{
			var S4 = function ()
			{
				return (((1 + Math.random()) * 0x10000) || 0).toString(16).substring(1);
			};
			return (S4() + S4() + S4() + S4() + S4() + S4());
		}

		/**
		 * Return if current device is a
		 * mobile device or not
		 */
		function isMobile() {
			return mobileDetect.mobile();
		}

		/**
		 * Toggle in array (push or splice)
		 *
		 * @param item
		 * @param array
		 */
		function toggleInArray(item, array) {
			if ( array.indexOf(item) === -1 )
			{
				array.push(item);
			}
			else
			{
				array.splice(array.indexOf(item), 1);
			}
		}
	}

app.filter('keyboardShortcut', function($window) {
    return function(str) {
      if (!str) return;
      var keys = str.split('-');
      var isOSX = /Mac OS X/.test($window.navigator.userAgent);
      var seperator = (!isOSX || keys.length > 2) ? '+' : '';
      var abbreviations = {
        M: isOSX ? '?' : 'Ctrl',
        A: isOSX ? 'Option' : 'Alt',
        S: 'Shift'
      };
      return keys.map(function(key, index) {
        var last = index == keys.length - 1;
        return last ? key : abbreviations[key];
      }).join(seperator);
    };
});
// Global application model factory...
app.factory('$model', function ($rootScope) {
    var model = {};
    return {
        // Register a scope on the model...
        store: function (key, value) { model[key] = value; },
        // Returns the scope's model...
        get: function (key) { return model[key][key]; },
        // Return the entire scope...
        scope: function (key) { return model[key]; }
    };
});
(function ()
{
    'use strict';

    angular
        .module('app.core',
            [
                'ngAnimate',
                'ngAria',
                'ngCookies',
                'ngMessages',
                'ngResource',
                'ngSanitize',
                'ngMaterial',
                'angular-chartist',
                'chart.js',
                'datatables',
                'gridshore.c3js.chart',
                'nvd3',
                'pascalprecht.translate',
                'timer',
                'ui.router',
                'uiGmapgoogle-maps',
                'textAngular',
                'ui.sortable',
                'ng-sortable',
                'xeditable',
                'moment-picker'
            ]);
})();
// We ONLY need this for IE11... It caches XHR otherwise, which is invalid, but it does it.
app.config(['$httpProvider', function($httpProvider) {
    if (!$httpProvider.defaults.headers.get) $httpProvider.defaults.headers.get = {};    
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
}]);
// An IO socket factory for Angular...
app.factory('socket', function ($rootScope) {
	//var socket = io.connect();
        QA.nodePort = window.client.vars.nodePort;
	QA.protocol = location.protocol;
	// Connect on the same security level as the page (WS or WSS)...
        QA.wsProtocol = QA.protocol;
        var wsURL = QA.wsProtocol + '//' + QA.server;
        // Connect with a port number if there is a port number defined (not in DEV)...
        if (QA.port !== "80") wsURL += ':' + QA.nodePort;
	var socket = io(wsURL, {transports: ['websocket']});
	
	// Allow catching * events... 
	var onevent = socket.onevent;
	socket.onevent = function (packet) {
		var args = packet.data || [];
		onevent.call (this, packet);    // original call
		packet.data = ["*"].concat(args);
		onevent.call(this, packet);      // additional call to catch-all
	};
	
	return {
	on: function (eventName, callback) {
	  socket.on(eventName, function () {  
		var args = arguments;
		$rootScope.$apply(function () { callback.apply(socket, args); });
	  });
	},
	emit: function (eventName, data, callback) {
	  socket.emit(eventName, data, function () {
		var args = arguments;
		$rootScope.$apply(function () { if (callback) callback.apply(socket, args); });
	  })
	}
	};
});
app.factory('taskBadgeFilter', function (constants) {
    var data = {};
    data.isFiltered = false;

    return {
        data: data,

        set: function (badgeType, currentMember, model, isMyTeam) {
            data.descriptionText = '';
            data.isFiltered = true;
            data.isMyTeam = isMyTeam;
            switch(badgeType) {
                case "overdue":
                    data.badgeValue = model.overdueTask;
                    //overdue tasks only be calculated for today
                    data.badgeValue.interval = 0;
                    data.descriptionText += model.taskBadgeDescription.overdue.capitalize();
                    data.taskType = 'tasksoverdue';
                    break;
                case "todo":
                    data.badgeValue = model.todoTask;
                    data.descriptionText += model.taskBadgeDescription.due.capitalize();
                    data.taskType = 'taskstodo';
                    break;
                case "careplan":
                    data.badgeValue = model.cpNonassessTask
                    data.descriptionText += model.taskBadgeDescription.careplan.capitalize();
                    data.taskType = 'taskscpnonasses';
                    break;
                case "assessments":
                    data.badgeValue = model.cpAssessTask;
                    data.descriptionText += model.taskBadgeDescription.assessment.capitalize();
                    data.taskType = 'taskscpasses';
                    break;
                case "noBadgeFilter":
                    data.isFiltered = false;
                    data.taskType = undefined;
                    data.supervisorFlag = true;
                    break;
            }

            data.email = currentMember.email;
            data.firstName = currentMember.firstName;
            data.lastName = currentMember.lastName;

            var text = '';
            switch (isMyTeam) {
              case constants.VIEW_TYPE.MY_VIEW:
                text = data.lastName + ", " + data.firstName + "'s Task Page";
                break;
              case constants.VIEW_TYPE.TEAM_VIEW:
                text = "My Team Members 's Task Page";
                break;
              case constants.VIEW_TYPE.TEAM_VIEW_ONE:
                text = data.lastName + ", " + data.firstName + "'s Task Page";
                break;
              case constants.VIEW_TYPE.TEAM_VIEW_SOME:
                text = "My Selected Team Members 's Task Page";
                break;
              default:
                break;
            }

            if (data.isFiltered === true) {
                //var text = data.isMyTeam === true ? 'My Selected Team Members' : data.lastName + ', ' + data.firstName + "'s Task Page";
                data.descriptionText += " " +
                (data.badgeValue.interval === 0 ? "Today" : "Next " + data.badgeValue.interval + " days") +
                 " for " + text;
                data.descriptionText = 'Showing ' + data.descriptionText;
            } else {
              data.descriptionText += text;
            }

            /*else if (data.isMyTeam === true) {
                data.descriptionText += text;
            } else {
                data.descriptionText += data.lastName + ', ' + data.firstName + "'s Task Page";
            }*/
        },

        clear: function() {
            data.badgeValue = undefined;
            data.descriptionText = undefined;
            data.taskType = undefined;
            data.isFiltered = false;
            data.email = undefined;
            data.isMyTeam = undefined;
        }
    };
});

app.directive('alertEncounterWidget', ['$model', '$appEvent', '$q', '$mdDialog', 'constants', function ($model, $appEvent, $q, $mdDialog, constants) {

    var template = '<ms-widget layout="row" flex="100" class="nopadding-top">' +
        '<ms-widget-front class="white-bg">' +
        '<div class="ph-16 pv-8 border-bottom" layout="row" layout-align="space-between center" layout-wrap>' +
        '<div class="pv-8" layout="row" layout-align="start center" layout-align-sm="end" flex-sm="100">' +
        '<md-select id="drpRangePicker" class="simplified font-size-16" ng-model="ctrl.currentfilter.currentchartrange" ' +
        'aria-label="Change date" ng-change="ctrl.requestChartData()">' +
        '<md-option ng-repeat="(key, range) in ctrl.chartRanges" ng-value="key">{{::range}}</md-option>' +
        '</md-select>' +
        '</div>' +
        '</div>' +
        '<div layout="row" layout-wrap>' +
        '<div class="p-16 error-wrapper" layout="row" flex="100" flex-gt-sm="35" flexible-div="ctrl.drawAlertChart()">' +
        '<nvd3 id="alertChart" options="ctrl.alertChartOpts" data="ctrl.alertchartdata.chartData" api="ctrl.alertChartApi"></nvd3>' +
        '<error ng-if="ctrl.isalerterror === true" ' +
        'msg="\'Error Occurred: We encountered a problem Alert chart. Please try again later.\'" ' +
        'reload="ctrl.loadAlertData()">' +
        '</error>' +
        '</div>' +
        '<div class="p-16 error-wrapper" layout="row" flex="100" flex-gt-sm="65" style="position: relative" flexible-div="ctrl.drawCarePlanChart()">' +
        '<nvd3 id="careplanChart" options="ctrl.carePlanChartOpts" data="ctrl.encounters.data" api="ctrl.carePlanChartApi"></nvd3>' +
        '<span id="lblNoEncounter">' +
        '<span class="badge-count" ng-click="ctrl.noDataLabelClick()">{{ctrl.patientnoencounter.patients.size()}}</span>' +
        'Patient(s) with No Encounters' +
        '</span>' +
        '<error ng-if="ctrl.iscareplanerror === true || ctrl.isnoencountererror === true" ' +
        'msg="\'Error Occurred: We encountered a problem Care Plan chart. Please try again later.\' " ' +
        'reload="ctrl.loadCarePlanData()">' +
        '</error>' +
        '</div>' +
        '</div>' +
        '</ms-widget-front>' +
        '</ms-widget>';

    var controllerFn = function ($scope, $timeout) {

        var self = this;

        this.chartRanges = {
            7: "Last 7 Days",
            14: "Last 14 Days",
            30: "Last 30 Days"
        };

        this.alertChartOpts = {
            chart: {
                type: 'pieChart',
                height: 420,
                donut: true,
                x: function (d) { return d.label; },
                y: function (d) { return d.value; },
                showLabels: true,
                labelType: 'value',
                valueFormat: function (d) {
                    return d3.format(',.0f')(d);
                },
                pie: {
                    startAngle: function (d) { return d.startAngle / 2 - Math.PI / 2 },
                    endAngle: function (d) { return d.endAngle / 2 - Math.PI / 2 },
                    dispatch: {
                        elementClick: function (e) {
                            var currentChartRange = self.chartRanges[self.currentfilter.currentchartrange];
                            var title = e.data.label + ' Alerts - ' + self.capitalize(self.getDisplayName($model.get('home').currentMember)) + ' - ' + currentChartRange;
                            self.handleChartClick({popupType: 'ALERT'}, title, e.data, 'Alert');
                        }
                    }
                },
                margin: {},
                duration: 500,
                showLegend: true,
                legend: {
                    margin: {
                        top: 12
                    }
                },
                title: 'Severity',
                titleOffset: -10
            },
            title: {
                enable: true,
                text: 'Patients by Alerts Severity'
            },
            noData: {
                enable: true,
                text: 'No Alerts for the patients in this timeframe'
            }
        };

        this.alertchartdata = {
            chartData: [],
            alertList: {}
        };

        $scope.$watch(angular.bind(this, function () {
            return self.alertchartdata;
        }), function (newValue, oldValue) {
            if (typeof newValue != 'undefined') {
                self.alertChartOpts.chart.margin.bottom = newValue.chartData.length === 0 ? 0 : -150;
                $timeout(function() {
                    self.alertChartApi.refresh();
                }, 500);
            }
        });

        this.encounters = {
            data: [],
            xLabel: 'Core Service Types',
            yLabel: 'Patients'
        };

        this.carePlanChartOpts = {
            chart: {
                type: 'discreteBarChart',
                color: ['#03A9F4'],
                height: 500,
                margin: {
                    top: 50,
                    bottom: 100,
                    left: 100
                },
                noData: 'No Care Plan Encounters for the patients in this timeframe',
                x: function (d) { return d.label; },
                y: function (d) { return d.value; },
                showValues: true,
                valueFormat: function (d) {
                    return d3.format('d')(d);
                },
                duration: 500,
                xAxis: {
                    axisLabel: this.encounters.xLabel,
                    axisLabelDistance: -10,
                    rotateLabels: -23,
                    fontSize: 11
                },
                yAxis: {
                    axisLabel: this.encounters.yLabel,
                    axisLabelDistance: -10,
                    tickFormat: function (n) {
                        return d3.format('d')(n);
                    }
                },
                discretebar: {
                    dispatch: {
                        elementClick: function (e) {
                            var currentChartRange = self.chartRanges[self.currentfilter.currentchartrange];
                            var title = 'Care Plan Encounter: ' + e.data.label + ' - ' + self.getDisplayName($model.get('home').currentMember) + ' - ' + currentChartRange;
                            self.handleChartClick({popupType: 'ENCOUNTER'}, self.capitalize(title), e.data, 'Encounter');
                        }
                    }
                }
            },
            title: {
                enable: true,
                text: 'Care Plan Encounters'
            }
        };

        this.patientnoencounter = {};

        this.noDataLabelClick = function() {
            var title = self.capitalize('Patients with No Encounter' + ' - ' +
                        self.getDisplayName($model.get('home').currentMember) + ' - ' + self.chartRanges[self.currentfilter.currentchartrange]);
            self.handleChartClick({
                popupType: 'NO_ENCOUNTER',
                noToolTip: true
            }, title, self.patientnoencounter, 'No Encounter');
        };

        this.getDisplayName = function (subordinate) {
            if(subordinate.type === constants.VIEW_TYPE.MY_VIEW) {
                return constants.VIEW.MY_VIEW_DISPLAY_TEXT;
            }
            else if(subordinate.type === constants.VIEW_TYPE.ORG_VIEW) {
                return constants.VIEW.MY_ORG_VIEW_DISPLAY_TEXT;
            }
            else {
                var selectedSubordinates = $model.get('home').selectedSubordinates;
                if (selectedSubordinates.size() === 1)
                    return constants.VIEW.getUserViewDisplayText(selectedSubordinates[0].firstName, selectedSubordinates[0].lastName);
                else
                    return constants.VIEW.MY_TEAM_VIEW_DISPLAY_TEXT;
            }
        };

        this.capitalize = function(str, lower) {
            return (lower ? str.toLowerCase() : str).replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
        };

        this.handleChartClick = function (popupInfo, title, data, patientSearchPopupTitle) {
            GWT.activity();
            var homeCtrl = $model.get('home');
            homeCtrl.popupTitle = title;

            homeCtrl.alertPopupChartData = angular.copy(self.alertchartdata);
            homeCtrl.alertPopupChartData.label = data.label;
            homeCtrl.encountersPopupData = angular.copy(self.encounters);
            homeCtrl.encountersPopupData.label = data.label;

            homeCtrl.already_added = self.getMessageBundle(homeCtrl.default_already_added, "popup.tooltip.already_added", function (configMsg) {
                return configMsg;
            });

            if (data.patients.size() !== 0) {
                homeCtrl.alertPatientList = [];
                $model.get('app').showDialog('appViewPatients', popupInfo, null, function () {
                });
                if (data.patients.size() <= 500) {
                    homeCtrl.patientCount = -1;
                    $appEvent('remote:appHome:loadPatientList', { patients: data.patients });
                } else {
                    $timeout(function () {
                        homeCtrl.patientCount = data.patients.size();
                        homeCtrl.showSearchPatientDialog(patientSearchPopupTitle, data.patients, popupInfo.popupType);
                    });
                }
            } else {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Message Box')
                        .textContent('There is no patient data')
                        .ariaLabel('There is no patient data')
                        .ok('OK')
                );
            }
        };

        this.getMessageBundle = function (defaultMsg, msgKey, replaceFunc) {
            if (self.messageBundle && (self.messageBundle[msgKey])) {
                return replaceFunc(self.messageBundle[msgKey]);
            } else {
                return defaultMsg;
            }
        };

        this.drawAlertChart = function () {
            this.alertChartApi.update();
        };

        this.drawCarePlanChart = function () {
            this.carePlanChartApi.update();
        };

        this.changeOrg = function (data) {
            this.currentfilter.currentpatienttoggle = data;
            this.requestChartData();
        };
        this.changeRange = function (data) {
            this.currentfilter.currentchartrange = data;
            this.requestChartData();
        };
        this.requestChartData = function () {
            this.loadAlertData();
            this.loadCarePlanData();
        };

        this.loadAlertData = function () {
            $appEvent('remote:appHome:loadAlertCount', { type: this.currentfilter.currentpatienttoggle, interval: this.currentfilter.currentchartrange, userList: this.subordinates });
        }

        this.loadCarePlanData = function () {
            $appEvent('remote:appHome:loadEncounterCount', { type: this.currentfilter.currentpatienttoggle, interval: this.currentfilter.currentchartrange, userList: this.subordinates });
            $appEvent('remote:appHome:loadNoEncounterCount', { type: this.currentfilter.currentpatienttoggle, interval: this.currentfilter.currentchartrange, userList: this.subordinates });
        }
        this.api.refresh = function (data) {
            self.subordinates = data.subordinates;
            self.changeOrg(data.member.type);
        };

        this.requestChartData();

    }

    return {
        restrict: 'E',
        template: template,
        scope: false,
        bindToController: {
            alertchartdata: '=',
            encounters: '=',
            patientnoencounter: '=',
            isalerterror: '=',
            iscareplanerror: '=',
            isnoencountererror: '=',
            subordinates: '=',
            currentfilter: '=',
            name: '@',
            api: '='
        },
        controller: controllerFn,
        controllerAs: 'ctrl'
    }
}]);

app.directive('assessmentWidget', ['$model', '$appEvent', '$q', '$mdDialog', '$timeout', 'constants', 'gsiCppWidget', function ($model, $appEvent, $q, $mdDialog, $timeout, constants, gsiCppWidget) {

    var template = '<ms-widget layout="row" flex="100" class="nopadding-top">' +
        '<ms-widget-front class="white-bg">' +
        '<div class="progress-container no-delay backdrop" layout="row" layout-align="center center" ng-if="ctrl.data.isLoading || ctrl.patientnodata.isLoading">' +
        '<md-progress-circular md-mode="indeterminate"></md-progress-circular>' +
        '</div>' +
        '<div class="ph-16 pv-8 border-bottom" style="font-size: 1.0rem;" layout="row" layout-align="center center" layout-wrap ng-bind="ctrl.chartIns.title">' +
        '</div>' +
        '<div class="ph-16 pv-8" layout="row" layout-align="center center" layout-wrap>' +
        '<div style="width: 147px;">' +
        '<md-select  md-container-class=" custom-assessment-type-box" placeholder="Options" md-on-close="ctrl.revertCurrentState()" md-on-open="ctrl.revertCurrentState()" multiple class="simplified font-size-16" ng-model="ctrl.currentType"' +
        'aria-label="Change assessment type">' +
        '<md-content>' +
        '<md-option ng-click="ctrl.shouldCheck($event, opt.name)" ng-selected="$index<ctrl.MAX_OPTIONS" ng-repeat="opt in ::ctrl.data.options" ng-value="opt.name">{{opt.name + " (" + opt.abbrv + ")"}}</md-option>' +
        '</md-content>' +
        '<md-select-header class="custom-select-footer" ng-if="ctrl.data.options && ctrl.data.options.length>0" layout="row">' +
        '<span class="txt-footer-assessment" flex>Selected: &nbsp;<div class="\{\{ctrl.redAssessmentText\}\}" ng-bind="(ctrl.currentType? ctrl.currentType.length : 0) + \'/\' + ctrl.MAX_OPTIONS"></div></span>' +
        '<md-button ng-disabled="!ctrl.currentType || ctrl.currentType.length===0" class="md-primary md-raised" ng-click="ctrl.applyChange()">Apply</md-button>' +
        '</md-select-header>' +
        '</md-select>' +
        '</div>' +
        '<div style="width: 147px" ng-if="ctrl.chartIns.isShowTimeRange">' +
        '<md-select class="simplified font-size-16" ng-model="ctrl.filter.currentTime" ng-change="ctrl.chartIns.updateData()" ' +
        'aria-label="Change time range">' +
        '<md-option ng-repeat="time in ctrl.chartIns.timeRange" ng-selected="$first" ng-value="time" ng-bind="time.text"></md-option>' +
        '</md-select>' +
        '</div>' +
        '</div>' +
        '<div layout="row" layout-wrap class="error-wrapper" flexible-div="ctrl.onResize()" layout-align="center center">' +
        '<span style="font-size: 12px">' +
        '<span class="badge-count" ng-click="ctrl.chartIns.noDataLabelClick()">{{ctrl.patientnodata.patients.length}}</span>' +
        '{{::ctrl.chartIns.noDataLabel}}' +
        '</span>' +
        '<nvd3 class="hide-zero" options="ctrl.chartIns.options" data="ctrl.filteredData" api="ctrl.chartIns.chartApi"></nvd3>' +
        '<error ng-if="ctrl.data.isError === true || ctrl.patientnodata.isError" ' +
        'msg="\'Error Occurred: We encountered a problem. Please try again later.\'" ' +
        'reload="ctrl.chartIns.updateData()">' +
        '</error>' +
        '</div>' +
        '</ms-widget-front>' +
        '</ms-widget>';

    var controllerFn = function ($scope, $timeout, $mdSelect) {
        var self = this;
        this.MAX_OPTIONS = 5;
        this.FULL_ASSESSMENT_CLASS = 'red-assessment-text';
        this.data = {};
        this.patientnodata = {};
        this.filteredData = [];
        this.firstTime = true;
        this.redAssessmentText = '';
        this.shouldCheck = function (event, name) {
            if (this.currentType.length === this.MAX_OPTIONS && this.currentType.indexOf(name)===-1) {
                event.preventDefault();
                event.stopPropagation();

                $mdDialog
                    .show({
                        targetEvent: event,
                        clickOutsideToClose: true,
                        template:
                            '<md-dialog md-theme="default" class="_md md-default-theme md-transition-in assessment-alert" role="alertdialog" tabindex="-1">' +

                            '  <md-dialog-content class="md-dialog-content" role="document" tabindex="-1" id="dialogContent_60">' +
                            '     <h2 class="md-title ng-binding">WARNING</h2>' +
                            '     <p class="ng-binding">You may only select a maximum of 5 assessments to display</p>' +
                            '  </md-dialog-content>' +

                            '  <md-dialog-actions >' +
                            '    <md-button ng-click="closeDialog()" class="md-primary">' +
                            '      OK' +
                            '    </md-button>' +
                            '  </md-dialog-actions>' +
                            '</md-dialog>',
                        controller: AlertController,
                        onShowing: adjustZIndex
                    })
                    .finally(function() {
                        alert = undefined;

                    });
                // change default z-index of angular mdDialog to make it popup over mdSelect
                function adjustZIndex() {
                    $timeout(function() {
                        $('.assessment-alert').parent().css('z-index', '91');
                    });
                }
                function AlertController($scope) {
                    $scope.closeDialog = function() {
                        $mdDialog.hide();
                    }
                }
            }
        };
        this.applyChange = function() {
            this.selectedType = angular.copy(this.currentType);
            this.chartIns.updateData();
            $mdSelect.hide();
        };
        this.changeAssessmentText = function() {
            if(this.currentType) {
                if (this.currentType.length >= this.MAX_OPTIONS) {
                    this.redAssessmentText = this.FULL_ASSESSMENT_CLASS;
                }
                else {
                    this.redAssessmentText = '';
                }
            }
        }
        this.filterData = function(currentType) {
            var newAssessments = this.data.chartData ? angular.copy(this.data.chartData) : [];
            var filteredData = [];
            for (var i = 0; i < newAssessments.length; i++) {
                var hasValue = false;
                var newCol = newAssessments[i].values.filter(function(value) {
                    var idx = self.selectedType.indexOf(value.name);
                    if (idx > -1 && value.value > 0)
                        hasValue = true;
                    return idx > -1;
                });
                newAssessments[i].values = angular.copy(newCol);
                if (hasValue)
                    filteredData.push(newAssessments[i]);
            }
            this.filteredData = filteredData;
            this.chartIns.getPatientNoData(this.chartIns.getRequestPayload());

        };
        this.revertCurrentState = function() {
            self.currentType = angular.copy(self.selectedType);
        };
        var deleteWatch = $scope.$watch(function() {
            return self.data.options;
        }, function(newVal) {
            if (newVal && newVal.length>0)  {
                var firstElems = getFieldsValue(newVal, 'name').slice(0, self.MAX_OPTIONS);
                self.selectedType = angular.copy(firstElems);
                self.chartIns.getPatientNoData(self.chartIns.getRequestPayload());
                deleteWatch();
            }
        });
         $scope.$watch(function() {
            return self.currentType;
        }, function(newVal) {
            self.changeAssessmentText();
        });
        $scope.$watch(function() {
            return self.data.chartData;
        }, function(newVal) {
            self.filterData();
        });
        var myNamespace = {};
        myNamespace.ccpChart = function(option) {
            var that = {};
            that.type = '';
            that.title = '';
            that.criterias = [];
            that.timeRange = [];
            that.isShowTimeRange = self.isshowtimerange === "false" ? false : true;

            that.options = {
                chart: {
                    type: 'multiBarChart',
                    height: 500,
                    stacked: true,
                    x: function(d){return d.label;},
                    y: function(d){return d.value;},
                    showLabels: true,
                    showValues: true,
                    labelType: 'value',
                    valueFormat: function (d) {
                        return d3.format(',.0f')(d);
                    },
                    showLegend: true,
                    duration: 500,
                    showControls: false,
                    legend: {
                        margin: {
                            left: 0
                        }
                    },
                    legendPosition: 'top',
                    xAxis: {
                        fontSize: 11,
                        rotateLabels: -23
                    },
                    yAxis: {
                        tickFormat: function (n) {
                            return d3.format('d')(n);
                        },
                        axisLabelDistance: -25,
                        ticks: 10,
                        axisLabel: 'Number of Patients'
                    },
                    tooltip: {
                        contentGenerator: function(data) {
                        var str = '<table style="max-width:350px; white-space: normal"><thead><tr><td colspan="3"><strong>' + data.data.name +
                                    '</strong></td></tr></thead>';
                        if(data.series.length !== 0) {
                            str = str + '<tbody><tr><td></td></tr>';
                            data.series.forEach(function(d){
                                str = str + '<tr><td class="legend-color-guide" width="10%"><div style="background-color:' + d.color + '"></div></td><td width="30%" class="key">' + d.key +
                                        '</td><td>' + d.value + '</td></tr>';
                            });
                            str = str + '</tbody>';
                        }
                        str = str + '<tr><td colspan="3">' + data.data.desc + '</td></tr>'
                        str = str + '</table>';
                        return str;
                        }
                    },
					groupSpacing: 0.4,
                    reduceXTicks: false,
                    multibar: {
                        dispatch: {
                            elementClick: function (e) {
                                var homeCtrl = $model.get('home');
                                var title = '';
                                title = e.data.key + ' - ' + e.data.label + ' ' + self.type + ' - ' +
                                self.getDisplayName(homeCtrl.currentMember) + self.getTimeText(" - ");

                                title = self.capitalize(title);

                                var fields = gsiCppWidget.getCcpWidgetSortFields(self.type, self.filter.currentCrit.value);
                                var selectedLabels = gsiCppWidget.getCcpSortLabel(self.filter.currentCrit.value, [e.data.name, e.data.key]);

                                that.handleChartClick({
                                    popupType: 'CCP',
                                    tooltip: self.data.patientData,
                                    tooltipTitles: that.tooltipTitles,
                                    selectedFields: fields,
                                    selectedLabels: selectedLabels,
                                }, title, e.data, self.capitalize(self.type));
                            }
                        }
                    }
                },
                noData: {
                    enable: true,
                    text: 'No data in this timeframe'
                }
            };
            that.noDataLabelClick = function() {
                var title = self.capitalize('Patients with No' + ' ' + self.type + ' - ' +
                            self.getDisplayName($model.get('home').currentMember) + self.getTimeText(" - "));
                that.handleChartClick({
                    popupType: 'CCP',
                    noToolTip: true
                }, title, self.patientnodata, self.capitalize('No ' + self.type));
            };

            that.changeChartType = function() {
                that.options.chart.type = 'multiBarChart';
                that.options.chart.margin = { bottom: 70 };
                that.tooltipTitle1 = that.type + ' Type';
                that.tooltipTitle2 = that.type + ' Visit Type';
                that.tooltipTitle3 = that.type + ' Status';
                that.tooltipTitles = [that.tooltipTitle1, that.tooltipTitle2, that.tooltipTitle3];
                that.options.chart.showLegend = true;
            };
            that.getRequestPayload = function() {
                that.changeChartType();
                var homeCtrl = $model.get('home');
                var userIds = [];
                $model.get('home').selectedSubordinates.forEach(function(user) {
                    userIds.push(user.userId)
                });
                var dashboards = QA.app.settings.apps.home.dashboards;
                var statusFilter = '';
                for (var i in dashboards) {
                    if (dashboards[i].id === 'CCPDashboard') {
                        var widgets = dashboards[i].widgets;
                        for (var j in widgets) {
                            var widget = widgets[j];
                            if (widget.name.toUpperCase().indexOf(that.type) >= 0) {
                                statusFilter = widget.parameter.status;
                                break;
                            }
                        }
                    }
                }

                var requestData = {
                    entity: that.type,
                    groupBy: 'TYPE',
                    period: self.filter.currentTime !== undefined ? self.filter.currentTime.value : -1,
                    type: homeCtrl.currentMember.type,
                    subordinateIds: userIds,
                    status: statusFilter,
                    entityFilterNames: self.selectedType? self.selectedType : []
                }
                return requestData;
            };

            $scope.$watch(function() {
                return self.data.isLoading;
            }, function(newVal) {
                if (!newVal) {
                    that.options.noData.enable = true;
                }
            });

            that.updateData = function() {
                $timeout(function() {
                    that.chartApi.refresh();
                });
                self.filter.currentCrit = that.criterias[0];
                if (self.filter.currentCrit) {
                    that.options.noData.enable = false;
                    var requestData = that.getRequestPayload();
                    that.getChartData(requestData);
                    if(!self.firstTime) {
                        that.getPatientNoData(requestData);
                        self.firstTime = false;
                    }
                }
            };
            that.getPatientNoData = function(payload) {
                self.patientnodata.isLoading = true;
                $appEvent('remote:ccpDashboard:' + that.noData, payload);
            };
             that.getChartData = function(payload) {
                 self.data.isLoading = true;
                $appEvent('remote:ccpDashboard:' + that.type, payload);
            };
            that.handleChartClick = function (popupInfo, title, data, patientSearchPopupTitle) {
                GWT.activity();
                var homeCtrl = $model.get('home');
                homeCtrl.popupTitle = title;
                homeCtrl.already_added = self.getMessageBundle(homeCtrl.default_already_added, "popup.tooltip.already_added", function (configMsg) {
                    return configMsg;
                });
                if (data.patients.size() !== 0) {
                    homeCtrl.alertPatientList = [];
                    $model.get('app').showDialog('appViewPatients', popupInfo, null, function () {
                    });
                    if (data.patients.size() <= 500) {
                        homeCtrl.patientCount = -1;
                        $appEvent('remote:appHome:loadPatientList', { patients: data.patients });
                    } else {
                        $timeout(function () {
                            homeCtrl.patientCount = data.patients.size();
                            homeCtrl.showSearchPatientDialog(patientSearchPopupTitle, data.patients, popupInfo.popupType);
                        });
                    }
                } else {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('Message Box')
                            .textContent('There is no patient data')
                            .ariaLabel('There is no patient data')
                            .ok('OK')
                    );
                }
            };

            return that;
        }

        myNamespace.assessment = function () {
            var that = myNamespace.ccpChart();
            that.type = 'ASSESSMENT';
            that.title = 'PATIENTS WITH ASSESSMENTS';
            that.noData = 'patientNoAssessment';
            that.noDataLabel = 'Patient(s) with No Selected Assessments';
            that.criterias = [{value: 'TYPE', text: 'By Type'}];
            that.options.chart.donut = false;

            var d = moment().startOf('month');

            var dayDiffByMonth = function(lastMonthNum) {
                return moment().diff(moment(d).subtract({months: lastMonthNum - 1}), 'days') + 1
            }
            that.timeRange = [
                {value: dayDiffByMonth(1), text: 'Current Month'},
                {value: dayDiffByMonth(3), text: 'Last 3 Months'},
                {value: dayDiffByMonth(6), text: 'Last 6 Months'},
                {value: dayDiffByMonth(12), text: 'Last 12 Months'}];

            return that;
        }

        var chartIns = myNamespace[this.type]();
        this.chartIns = chartIns;
        this.api.refresh = this.chartIns.updateData;
        this.getDisplayName = function (subordinate) {
            if(subordinate.type === constants.VIEW_TYPE.MY_VIEW) {
                return constants.VIEW.MY_VIEW_DISPLAY_TEXT;
            }
            else if(subordinate.type === constants.VIEW_TYPE.ORG_VIEW) {
                return constants.VIEW.MY_ORG_VIEW_DISPLAY_TEXT;
            }
            else {
                var selectedSubordinates = $model.get('home').selectedSubordinates;
                if (selectedSubordinates.size() === 1)
                    return constants.VIEW.getUserViewDisplayText(selectedSubordinates[0].firstName, selectedSubordinates[0].lastName);
                else if(selectedSubordinates.size() < $model.get('app').subordinates.size())
                    return constants.VIEW.MY_TEAM_VIEW_SOME_DISPLAY_TEXT;
                else
                    return constants.VIEW.MY_TEAM_VIEW_DISPLAY_TEXT;
            }
        };

        this.getTimeText = function (separator) {
            if (self.filter.currentTime !== undefined) {
              return separator + self.filter.currentTime.text;
            }

            return "";
        }

        this.onResize = function() {
            if(constants.isIE) {
                this.chartIns.chartApi.refresh();
            }
        };

        this.capitalize = function(str, lower) {
            return (lower ? str.toLowerCase() : str).replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
        };
        this.getMessageBundle = function (defaultMsg, msgKey, replaceFunc) {
            if (self.messageBundle && (self.messageBundle[msgKey])) {
                return replaceFunc(self.messageBundle[msgKey]);
            } else {
                return defaultMsg;
            }
        };
        function getFieldsValue(input, field) {
            return input.map(function(o) {
                return o[field];
            });
        }
    }

    return {
        restrict: 'E',
        template: template,
        scope: false,
        bindToController: {
            data: '=',
            type: '@',
            filter: '=',
            patientnodata: '=',
            api: '=',
            isshowtimerange: '@'
        },
        controller: controllerFn,
        controllerAs: 'ctrl'
    }
}]);

app.directive('autofocus', ['$timeout', function($timeout) {
  return {
    restrict: 'A',
    link : function($scope, $element) {
      $timeout(function() {
        $element[0].focus();
        
        //trick to focus on the end of input on IE
        var val =  $element[0].value;
        $element[0].value = '';
        $element[0].value = val; 
      }, 0);
    }
  }
}]);
app.directive('ccpWidget', ['$model', '$appEvent', '$q', '$mdDialog', '$timeout', 'constants', 'gsiCppWidget', function ($model, $appEvent, $q, $mdDialog, $timeout, constants, gsiCppWidget) {

    var template = '<ms-widget layout="row" flex="100" class="nopadding-top">' +
        '<ms-widget-front class="white-bg">' +
        '<div class="progress-container no-delay backdrop" layout="row" layout-align="center center" ng-if="ctrl.data.isLoading">' +
        '<md-progress-circular md-mode="indeterminate"></md-progress-circular>' +
        '</div>' +
        '<div class="ph-16 pv-8 border-bottom" style="font-size: 1.0rem;" layout="row" layout-align="center center" layout-wrap ng-bind="ctrl.chartIns.title">' +
        '</div>' +
        '<div class="ph-16 pv-8" layout="row" layout-align="center center" layout-wrap>' +
        '<div style="width: 147px">' +
        '<md-select ng-change="ctrl.chartIns.updateData()" class="simplified font-size-16" ng-model="ctrl.filter.currentCrit"' +
        'aria-label="Change Criteria">' +
        '<md-option ng-repeat="crit in ctrl.chartIns.criterias" ng-selected="$first" ng-value="crit" ng-bind="crit.text"></md-option>' +
        '</md-select>' +
        '</div>' +
        '<div style="width: 147px" ng-if="ctrl.chartIns.isShowTimeRange">' +
        '<md-select class="simplified font-size-16" ng-model="ctrl.filter.currentTime" ng-change="ctrl.chartIns.updateData()" ' +
        'aria-label="Change time range">' +
        '<md-option ng-repeat="time in ctrl.chartIns.timeRange" ng-selected="$first" ng-value="time" ng-bind="time.text"></md-option>' +
        '</md-select>' +
        '</div>' +
        '</div>' +
        '<div layout="row" layout-wrap class="error-wrapper" flexible-div="ctrl.onResize()" layout-align="center center">' +
        '<span style="font-size: 12px">' +
        '<span class="badge-count" ng-click="ctrl.chartIns.noDataLabelClick()">{{ctrl.patientnodata.patients.length}}</span>' +
        '{{::ctrl.chartIns.noDataLabel}}' +
        '</span>' +
        '<nvd3 class="hide-zero" options="ctrl.chartIns.options" data="ctrl.data.chartData" api="ctrl.chartIns.chartApi"></nvd3>' +
        '<error ng-if="ctrl.data.isError === true || ctrl.patientnodata.isError" ' +
        'msg="\'Error Occurred: We encountered a problem. Please try again later.\'" ' +
        'reload="ctrl.chartIns.updateData()">' +
        '</error>' +
        '</div>' +
        '</ms-widget-front>' +
        '</ms-widget>';

    var controllerFn = function ($scope, $timeout) {
        var self = this;
        this.data = {};
        this.patientnodata = {};
        self.total = 1;
        var total = self.total;
        var myNamespace = {};
        self.priorityStatus = $scope.config.parameter.priorityStatus;
        myNamespace.ccpChart = function(option) {
            var that = {};
            that.type = '';
            that.title = '';
            that.criterias = [{value: 'STATUS', text: 'By Status'}, {value: 'DOMAIN', text: 'By Domain'}];

            that.timeRange = [{value: 15, text: 'Last 15 Days'}, {value: 30, text: 'Last 30 Days'}, {value: 90, text: 'Last 90 Days'}];

            that.isShowTimeRange = self.isshowtimerange === "false" ? false : true;

            that.options = {
                chart: {
                    type: 'pieChart',
                    height: 500,
                    stacked: true,
                    donut: true,
                    x: function(d){return d.label;},
                    y: function(d){return d.value;},
                    showLabels: true,
                    showValues: true,
                    labelType: 'value',
                    valueFormat: function (d) {
                        return d3.format(',.0f')(d);
                    },
                    showLegend: true,
                    duration: 500,
                    showControls: false,
                    legend: {
                        margin: {
                            bottom: 20,
                            left: 0
                        }
                    },
                    legendPosition: 'bottom',
                    xAxis: {
                        rotateLabels: -23,
                        fontSize: 11
                    },
                    yAxis: {
                        tickFormat: function (n) {
                            return d3.format('d')(n);
                        },
                        ticks: 10,
                        axisLabel: 'Number of Patients'
                    },
                    groupSpacing: 0.4,
                    reduceXTicks: false,
                    pie: {
                        dispatch: {
                            elementClick: function (e) {
                                var homeCtrl = $model.get('home');
                                var title = '';
                                title = e.data.label + ' - ' + self.type + ' - ' +
                                self.getDisplayName(homeCtrl.currentMember) + self.getTimeText(" - ");
                                title = self.capitalize(title);

                                var fields = gsiCppWidget.getCcpWidgetSortFields(self.type, self.filter.currentCrit.value);
                                var selectedLabels = gsiCppWidget.getCcpSortLabel(self.filter.currentCrit.value,  [e.data.label]);

                                that.handleChartClick({
                                    popupType: 'CCP',
                                    tooltip: self.data.patientData,
                                    tooltipTitles: that.tooltipTitles,
                                    selectedFields: fields,
                                    selectedLabels: selectedLabels,
                                }, title, e.data, self.capitalize(self.type));
                            }
                        }
                    },
                    multibar: {
                        dispatch: {
                            elementClick: function (e) {
                                var homeCtrl = $model.get('home');
                                var title = '';
                                title = e.data.key + ' - ' + self.type + ' - ' +
                                self.getDisplayName(homeCtrl.currentMember) + self.getTimeText(" - ");
                                title = self.capitalize(title);

                                var fields = gsiCppWidget.getCcpWidgetSortFields(self.type, self.filter.currentCrit.value);
                                var selectedLabels = gsiCppWidget.getCcpSortLabel(self.filter.currentCrit.value, [e.data.label], self.priorityStatus);

                                that.handleChartClick({
                                    popupType: 'CCP',
                                    tooltip: self.data.patientData,
                                    tooltipTitles: that.tooltipTitles,
                                    selectedFields: fields,
                                    selectedLabels: selectedLabels,
                                }, title, e.data, self.capitalize(self.type));
                            }
                        }
                    }
                },
                noData: {
                    enable: true,
                    text: 'No data in this timeframe'
                }
            };

            that.noDataLabelClick = function() {
                var title = self.capitalize('Patients with No' + ' ' + self.type + ' - ' +
                            self.getDisplayName($model.get('home').currentMember) + self.getTimeText(" - "));
                that.handleChartClick({
                    popupType: 'CCP',
                    noToolTip: true
                }, title, self.patientnodata, self.capitalize('No ' + self.type));
            };

            that.changeChartType = function() {
                if(self.filter.currentCrit.value === 'DOMAIN') {
                    that.options.chart.type = 'pieChart';
                    that.options.chart.margin = { top: 0 };
                    that.options.chart.showLegend = true;
                }
                else if (self.filter.currentCrit.value === 'TYPE') {
                    that.options.chart.type = 'pieChart';
                    that.options.chart.margin = { top: 0 };
                    that.options.chart.showLegend = true;
                }
                else if (self.filter.currentCrit.value === 'NUMBERPERPATIENT') {
                    that.options.chart.type = 'multiBarHorizontalChart';
                    that.options.chart.margin = { bottom: 70 };
                    that.options.chart.showLegend = false;
                    that.options.chart.yAxis.axisLabelDistance = -5;
                }
                else {
                    that.options.chart.type = 'multiBarChart';
                    that.options.chart.margin = { bottom: 70 };
                    that.options.chart.showLegend = false;
                    that.options.chart.yAxis.axisLabelDistance = -25;
                }

            };

            $scope.$watch(function() {
                return self.data.isLoading;
            }, function(newVal) {
                if (!newVal) {
                    if (constants.isIE) {
                        $timeout(function() {
                            that.chartApi.update();
                        }, 500);
                    }
                    that.options.noData.enable = true;
                }
            });

            that.updateData = function() {
                self.data.isLoading = true;
                self.patientnodata.isLoading = true;
                $timeout(function() {
                    that.chartApi.refresh();
                });
                if (self.filter.currentCrit) {
                    that.options.noData.enable = false;
                    self.data.chartData = [];
                    that.changeChartType();
                    var homeCtrl = $model.get('home');
                    var userIds = [];
                    $model.get('home').selectedSubordinates.forEach(function(user) {
                        userIds.push(user.userId)
                    });

                    var requestData = {
                        entity: that.type,
                        groupBy: self.filter.currentCrit.value,
                        period: self.filter.currentTime !== undefined ? self.filter.currentTime.value : -1,
                        type: homeCtrl.currentMember.type,
                        subordinateIds: userIds,
                        status: ''
                    }
                    $appEvent('remote:ccpDashboard:' + that.type, requestData);
                    $appEvent('remote:ccpDashboard:' + that.noData, requestData);
                }
            };

            that.handleChartClick = function (popupInfo, title, data, patientSearchPopupTitle) {
                GWT.activity();
                var homeCtrl = $model.get('home');
                homeCtrl.popupTitle = title;
                homeCtrl.already_added = self.getMessageBundle(homeCtrl.default_already_added, "popup.tooltip.already_added", function (configMsg) {
                    return configMsg;
                });
                if (data.patients.size() !== 0) {
                    homeCtrl.alertPatientList = [];
                    $model.get('app').showDialog('appViewPatients', popupInfo, null, function () {
                    });
                    if (data.patients.size() <= 500) {
                        homeCtrl.patientCount = -1;
                        $appEvent('remote:appHome:loadPatientList', { patients: data.patients });
                    } else {
                        $timeout(function () {
                            homeCtrl.patientCount = data.patients.size();
                            homeCtrl.showSearchPatientDialog(patientSearchPopupTitle, data.patients, popupInfo.popupType);
                        });
                    }
                } else {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('Message Box')
                            .textContent('There is no patient data')
                            .ariaLabel('There is no patient data')
                            .ok('OK')
                    );
                }
            };

            return that;
        }

        myNamespace.issue = function () {
            var that = myNamespace.ccpChart();
            that.type = 'ISSUE';
            that.title = 'PATIENTS WITH INCOMPLETE ISSUES';
            that.noData = 'patientNoIssue';
            that.noDataLabel = 'Patient(s) with No Issues';
            that.criterias = [{value: 'STATUS', text: 'By Status'}, {value: 'DOMAIN', text: 'By Domain'}, {value: 'NUMBERPERPATIENT', text: 'By Count Per Patient'}];
            that.tooltipTitles = ['Issue Domain', 'Issue Status', 'Issue/Need Title'];
            return that;
        }

        myNamespace.goal = function () {
            var that = myNamespace.ccpChart();
            that.type = 'GOAL';
            that.title = 'PATIENTS WITH GOALS';
            that.noData = 'patientNoGoal';
            that.noDataLabel = 'Patient(s) with No Goals';
            that.criterias = [{value: 'STATUS', text: 'By Status'}, {value: 'NUMBERPERPATIENT', text: 'By Count Per Patient'}];
            that.tooltipTitles = ['Goal Status', 'Goal/Objective'];
            return that;
        }

        myNamespace.intervention = function () {
            var that = myNamespace.ccpChart();
            that.type = 'INTERVENTION';
            that.title = 'PATIENTS WITH INTERVENTIONS';
            that.noData = 'patientNoIntervention';
            that.noDataLabel = 'Patient(s) with No Interventions';
            that.tooltipTitles = ['Intervention Status', 'Intervention Plan'];
            that.criterias = [{value: 'STATUS', text: 'By Status'}, {value: 'NUMBERPERPATIENT', text: 'By Count Per Patient'}];
            return that;
        }
        var chartIns = myNamespace[this.type]();
        this.chartIns = chartIns;
        this.api.refresh = this.chartIns.updateData;
        this.getDisplayName = function (subordinate) {
            if(subordinate.type === constants.VIEW_TYPE.MY_VIEW) {
                return constants.VIEW.MY_VIEW_DISPLAY_TEXT;
            }
            else if(subordinate.type === constants.VIEW_TYPE.ORG_VIEW) {
                return constants.VIEW.MY_ORG_VIEW_DISPLAY_TEXT;
            }
            else {
                var selectedSubordinates = $model.get('home').selectedSubordinates;
                if (selectedSubordinates.size() === 1)
                    return constants.VIEW.getUserViewDisplayText(selectedSubordinates[0].firstName, selectedSubordinates[0].lastName);
                else if(selectedSubordinates.size() < $model.get('app').subordinates.size())
                    return constants.VIEW.MY_TEAM_VIEW_SOME_DISPLAY_TEXT;
                else
                    return constants.VIEW.MY_TEAM_VIEW_DISPLAY_TEXT;
            }
        };

        this.getTimeText = function (separator) {
            if (self.filter.currentTime !== undefined) {
              return separator + self.filter.currentTime.text;
            }

            return "";
        };

        this.onResize = function() {
            if(constants.isIE) {
                if(this.chartIns.options.chart.type !== 'pieChart') {
                    this.chartIns.chartApi.refresh();
                }
            }
        };

        this.capitalize = function(str, lower) {
            return (lower ? str.toLowerCase() : str).replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
        };
        this.getMessageBundle = function (defaultMsg, msgKey, replaceFunc) {
            if (self.messageBundle && (self.messageBundle[msgKey])) {
                return replaceFunc(self.messageBundle[msgKey]);
            } else {
                return defaultMsg;
            }
        };
    }

    return {
        restrict: 'E',
        template: template,
        scope: false,
        bindToController: {
            data: '=',
            type: '@',
            filter: '=',
            patientnodata: '=',
            api: '=',
            isshowtimerange: '@'
        },
        controller: controllerFn,
        controllerAs: 'ctrl'
    }
}]);

app.directive('changeViewDropdown', ['$model', '$appEvent', '$q', '$mdDialog','$mdPanel', 'constants', function ($model, $appEvent, $q, $mdDialog, $mdPanel, constants) {

    var dropdownTemplate = '<div class="panel-menu-list">' +
		'<div md-ink-ripple ng-class="{\'selected\': dropdownCtrl.selectedView.type == defaultUserView.type}" ' +
        'ng-click="dropdownCtrl.changeViewMode(defaultUserView)" ng-repeat="defaultUserView in dropdownCtrl.app.defaultUserViews|filterViewMenu: {typeNotShown: dropdownCtrl.typeNotShown, hasChild: (dropdownCtrl.app.subordinates && dropdownCtrl.app.subordinates.length > 0)}" class="{{dropdownCtrl.theme[defaultUserView.type]}} panel-menu-item">' +
		'   <div>' +
        '		<md-icon md-svg-icon="{{dropdownCtrl.icons[defaultUserView.type]}}"></md-icon><span class="item-text">{{dropdownCtrl.displaySubordinateInMenu(defaultUserView)}}</span>' +
        '   </div>' +
		'   <md-tooltip md-direction="right" id="{{\'tooltip-\' + defaultUserView.idx}}" ng-if="defaultUserView.type === 1">' +
		'       Displays the patient data from the patients in your Patient Panel' +
		'   </md-tooltip>' +
		'   <md-tooltip md-direction="right" id="{{\'tooltip-\' + defaultUserView.idx}}" ng-if="defaultUserView.type === 2">' +
		'       Displays the patient data from all the patients in your organization to which you have access' +
		'   </md-tooltip>' +
		'   <md-tooltip md-direction="right" id="{{\'tooltip-\' + defaultUserView.idx}}" ng-if="defaultUserView.type === 3">' +
		'       Displays the patient data from the selected subordinates\' patient panels' +
		'   </md-tooltip>' +
		'</div>' +
        '<div ng-if="dropdownCtrl.app.subordinates.length>0">' +
		'   <md-divider style="margin: 5px 0 0 0;background-color: rgba(0,0,0,0.11);height: 1px;border: 0;"></md-divider>' +
        '   <md-content>' +
        '       <div class="panel-menu-item sub-item">' +
        '           <md-checkbox ng-checked="dropdownCtrl.isChecked()" ng-click="dropdownCtrl.toggleAll()" md-no-ink>' +
		'               Select All' +
		'           </md-checkbox>' +
        '       </div>' +
		'       <div class="panel-menu-item sub-item" ng-repeat="subordinateView in dropdownCtrl.app.subordinates">' +
		'           <md-checkbox ng-checked="dropdownCtrl.exists(subordinateView, dropdownCtrl.selectedSubordinates)" ng-click="dropdownCtrl.toggle(subordinateView, dropdownCtrl.selectedSubordinates)" md-no-ink>' +
		'               {{ dropdownCtrl.displaySubordinateInMenu(subordinateView) }}' +
		'           </md-checkbox>' +
		'       </div>' + 
        '   </md-content>' +
        '   <div layout="row" class="custom-select-footer"><span flex></span><md-button class="md-primary md-raised" ng-disabled="!dropdownCtrl.selectedSubordinates.length > 0" ng-click="dropdownCtrl.applyChange()">Apply</md-button></div>' +
	    '</div>' + 
    '</div>';

    var template = '<div class="dropdown-container">' +
                   '    <div class="dropdown-title" ng-if="!ctrl.hideDropDownTitle">' +
                   '        <md-icon md-svg-icon="{{ctrl.icons[ctrl.currentView.type]}}"></md-icon>' +
				   '        <span class="dropdown-txt">{{ctrl.displaySubordinateInMenu(ctrl.currentView)}}</span>' +
                   '    </div>' +
				   '    <span id="{{ctrl.btnId}}" ng-click="ctrl.showMenu($event)" class="dropdown-btn"><md-icon md-svg-icon="{{ctrl.btnIcon}}"></md-icon></span>' +
				   '</div>';
                   
    var displaySubordinateInMenu = function (subordinate) {
        if(subordinate === undefined) {
            return '';
        }

        if(subordinate.type === 1) {
            return constants.VIEW.MY_VIEW_DISPLAY_TEXT;
        } else if(subordinate.type === 2) {
            return constants.VIEW.MY_ORG_VIEW_DISPLAY_TEXT;
        } else if (subordinate.type === 3 && subordinate.userId === -3) {
            return constants.VIEW.MY_TEAM_VIEW_DISPLAY_TEXT;
        } else {
            return constants.VIEW.getUserViewDisplayText(subordinate.firstName, subordinate.lastName);
        }
    };
    var controllerFn = function ($scope, $timeout) {
        var self = this;
        self.icons = {
            1: 'account',
            2: 'city',
            3: 'account-group'
        };

        if (self.hideDropDownTitle === undefined)
            self.hideDropDownTitle = false;

        if (!self.btnIcon)
            self.btnIcon = "menu";

        function DropdownCtrl($scope, mdPanelRef, $window){
            var my = this;

            my.theme = {
                1: 'theme-bg-primary',
                2: 'theme-bg-secondary',
                3: 'theme-bg-grey'
            }
            my.selectedView = self.currentView;

            if (my.selectedView.type === 3) {
                my.selectedSubordinates = angular.copy(self.selectedSubordinates);
            } else {
                my.selectedSubordinates = [];
            }
            my._mdPanelRef = mdPanelRef;
            my.close = function(){
                var panelRef = my._mdPanelRef;
                panelRef && panelRef.close().then(function() {
                    panelRef.destroy();
                });
                self.currentView = angular.copy(my.selectedView);
            };
            my.displaySubordinateInMenu = displaySubordinateInMenu;
            my.selectUserView = function(userView) {
                my.close();
            }
            my.toggle = function (item, list) {
                my.selectedView = my.app.defaultUserViews[2];
                var idx = my.indexOfItem(item, list);
                if (idx > -1) {
                    list.splice(idx, 1);
                }
                else {
                    list.push(item);
                }
            };
            
            my.indexOfItem = function(item, list) {
                var index = -1;
                for (var i = 0; i < list.length; i++) {
                    if (list[i].email == item.email) {
                        index = i;
                        break;
                    }
                }
                return index;
            };

            my.exists = function (item, list) {
                return my.indexOfItem(item, list) > -1;
            };

            my.isChecked = function() {
                return my.selectedSubordinates.length === my.app.subordinates.length;
            };

            my.toggleAll = function() {
                my.selectedView = my.app.defaultUserViews[2];

                if (my.selectedSubordinates.length === my.app.subordinates.length) {
                    my.selectedSubordinates = [];
                } else if (my.selectedSubordinates.length === 0 || my.selectedSubordinates.length > 0) {
                    my.selectedSubordinates = my.app.subordinates.slice(0);
                }
            };
            
            my.changeViewMode = function(selectedView) {
                if (selectedView.type !== 3) {
                    my.selectedView = selectedView;
                    my.selectedSubordinates = [];
                    my.applyChange();
                }
                    
            };
            my.applyChange = function() {
                var data = {
                    selectedView: my.selectedView,
                    subordinates: my.selectedSubordinates
                }
                my.applyFn({data: data});
                self.selectedSubordinates = my.selectedSubordinates;
                my.close();
            };
            //Update position on window resize
            function onWinResize() {
                var position = $mdPanel.newPanelPosition()
                    .relativeTo('#' + this.btnId)
                    .addPanelPosition($mdPanel.xPosition.ALIGN_END, $mdPanel.yPosition.BELOW);
                my._mdPanelRef.updatePosition(position);
            }

            //Clean up watcher
            $scope.$on('$destroy', function () {
                angular.element($window)
                    .off('resize',onWinResize);
            });

        }
        this.app = $model.get('app');
        var menuConfig = {
            attachTo: angular.element(document.body),
            controller: DropdownCtrl,
            controllerAs: 'dropdownCtrl',
            template: dropdownTemplate,
            panelClass: 'demo-menu-example',
            clickOutsideToClose: true,
            escapeToClose: true,
            focusOnOpen: false,
            zIndex: 60
        };
        this.showMenu = function (ev) {
            var position = $mdPanel.newPanelPosition()
                .relativeTo('#' + this.btnId)
                .addPanelPosition($mdPanel.xPosition.ALIGN_START, $mdPanel.yPosition.BELOW);

            menuConfig.openFrom = ev;
            menuConfig.position = position;

            menuConfig.locals = {
                app: $model.get('app'), 
                typeNotShown: this.typeNotShown, 
                applyFn: this.applyFn,
                icons: this.icons
            };

            $mdPanel.open(menuConfig);

        };
        this.displaySubordinateInMenu = displaySubordinateInMenu;
        this.selectedSubordinates = [];
    }

    return {
        restrict: 'E',
        template: template,
        scope: false,
        bindToController: {
            applyFn: '&',
            typeNotShown: '=',
            currentView: '=',
            btnId: "@",
            hideDropDownTitle: '=?',
            btnIcon: '@',
        },
        controller: controllerFn,
        controllerAs: 'ctrl'
    }
}]);

app.directive('chartWidget',["$model","$appEvent", "$q", "$mdDialog", "$timeout", function($model,$appEvent,$q, $mdDialog, $timeout){
    var template = '<div class="p-16 error-wrapper"  flexible-div="drawChart()" width="98%" align="center">' +
                        '<div>' +
                            '{{title}}' +
                        '</div>' +
                        '<nvd3 id="id" options="chartOpts" data="chartdata" api="chartApi" width="100%">' +
                        '</nvd3>' +
                        /*<error ng-if="home.isAlertError === true"
                        msg="'Error Occurred: We encountered a problem Alert chart. Please try again later.'"
                        reload="home.loadAlertData()">
                        </error>*/
                    '</div>' ;

    /*var template1 = '<ms-widget layout="row" flex="95" class="nopadding-top" >' +
        '<ms-widget-front class="white-bg">' +
        
        '<div layout="row" layout-wrap>' +
        '<div class="p-16 error-wrapper" layout="row" flex="95" flex-gt-sm="100" flexible-div="drawChart()" >' +
        '<nvd3 id="idChart" options="chartOpts" data="chartdata" api="chartApi"  id="test456"></nvd3>' +
        '</div>' +
        '</div>' +
        '</ms-widget-front>' +
        '</ms-widget>' ;*/
                           
    return {
        restrict: 'E',
        replace: false,
        //bindToController: {
        scope: {
            id : '@',
            chartdata: '=',
            type: '=',
            title: '=',
            xlabel: '@',
            ylabel: '@'
        },
        link: function (scope, iElement, iAttrs) {
            scope.chartOpts = {
                chart: {
                    type: scope.type,
                    height: 500,
                    valueFormat: function (d) { return d3.format('d')(d);},
                    noData: 'No Care Plan Encounters for the patients in this timeframe 123',
                    x: function (d) { return d.label; },
                    y: function (d) { return d.value; },
                    showValues: true,
                    duration: 500
                },
                title: {
                    enable: false
                    //text: scope.title
                },
                noData: {
                    enable: true,
                    text: 'No Alerts for the patients in this timeframe'
                }
            };

            
            scope.drawChart = function() {
                scope.chartApi.update();
            };

            scope.initChart = function() {
                console.log('initChart:', scope.chartdata);
                
                if(scope.type === 'discreteBarChart') {
                    scope.chartOpts.chart.margin = {
                        top: 50,
                        bottom: 100,
                        left: 100
                    };
                    scope.chartOpts.chart.xAxis = {
                        axisLabel: scope.xlabel,
                        axisLabelDistance: -10,
                        rotateLabels: -23,
                        fontSize: 11
                    };
                    scope.chartOpts.chart.yAxis = {
                        axisLabel: scope.ylabel,
                        axisLabelDistance: -10,
                        tickFormat: function (n) {
                            return d3.format('d')(n);
                        }
                    }
                }
                else if(scope.type === 'pieChart') {
                    scope.chartOpts.chart.donut = true;
                    scope.chartOpts.chart.labelType = 'value';
                    scope.chartOpts.chart.valueFormat = function (d) { return d3.format(',.0f')(d);};
                    scope.chartOpts.chart.pie = {
                        startAngle: function (d) { return d.startAngle / 2 - Math.PI / 2 },
                        endAngle: function (d) { return d.endAngle / 2 - Math.PI / 2 }
                    };
                    scope.chartOpts.chart.margin = {};
                    scope.chartOpts.chart.showLegend = true;
                    scope.chartOpts.chart.legend = {
                        margin: {
                            top: 12
                        }
                    };
                    //scope.chartOpts.chart.titleOffset = -10;
                }
            };

            scope.initChart();
        },

        //controller:'@', //specify the controller is an attribute
        //name:'ctrl', //name of the attribute.
        //controllerAs: 'ctrl',
        /*controller: function ($scope) {
        },*/

        template: template

    }
}]);

app.directive('clickOutside', function($parse, $document) {
    var dir = {
        compile: function($element, attr) {
          // Parse the expression to be executed
          // whenever someone clicks off this element.
          var fn = $parse(attr["clickOutside"]);
          return function(scope, element, attr) {
            // add a click handler to the element that
            // stops the event propagation.
            element.bind("click", function(event) {
              event.stopPropagation();
            });
            angular.element($document[0].body).bind("click",                                                                 function(event) {
                scope.$apply(function() {
                    fn(scope, {$event:event});
                });
            });
          };
        }
      };
    return dir;
});
// Angluar directive to recompile the contents of dynamic areas when they get refreshed...
app.directive('dynamic-disabled', function ($compile) {
    console.log('dynamic loader checked in');
  return {
    restrict: 'A',
    replace: true,
    link: function (scope, ele, attrs) {
      scope.$watch(attrs.dynamic, function(html) {
        ele.html(html);
        $compile(ele.contents())(scope);
		console.log('Recompiled.');
      });
    }
  };
});
app.directive('error',["$model","$appEvent", "$q", "$mdDialog", function($model,$appEvent,$q, $mdDialog){
    var template = '<div class="error-container">' +
    '                   <md-icon style="color: #ff5722" ng-if="type!==\'warning\'" md-svg-icon="error"></md-icon>' +
    '                   <md-icon style="color: orange" ng-if="type===\'warning\'" md-svg-icon="warning"></md-icon>' +
    '                   <p ng-bind="msg"></p>' +
    '                   <md-button ng-if="reload" ng-click="reload()">Reload</md-button>' +
    '               </div>';

    return {
        restrict: 'E',
        replace: false,
        scope:{
            msg: '=',
            reload: '&?',
            type: '@'
        },
        template: template
    }
}]);

app.directive('flexibleDiv',['$timeout', function ($timeout) {
    return {
        link: function (scope, element, attr) {
            var timeoutPromise;
            var delayInMs = 300;
            // Watching height of parent div
            scope.$watchGroup([function () {
                return element[0].offsetWidth;
            }, function () {
                return element[0].offsetHeight;
            }], function (values) {
                $timeout.cancel(timeoutPromise);
                timeoutPromise = $timeout(function(){   //Set timeout
                    scope.$eval(attr.flexibleDiv);
                }, delayInMs);
            });

        }
    }
}]);
app.directive('gsiAlertChart', ["$model","$appEvent", function($model,$appEvent){
        return {
            restrict : "AE",
            scope: {
                home: '='
            },
            bindToController: true,
            template : "<ms-widget flippable=\"true\" flex=\"100\" class=\"nopadding-top\">\r\n"+
                        "<!-- Front -->\r\n" +
                        "<ms-widget-front class=\"white-bg\">\r\n"+
                        "<div layout=\"row\">\r\n"+
                        "<div flex layout-align=\"center center\" style=\" text-align:center\">"+
                        "{{ctrl.home.patientsToggle[ctrl.home.currentFilter.currentpatienttoggle]}}-{{ctrl.home.chartRanges[ctrl.home.currentFilter.currentchartrange]}}"+
                        "<\/div>\r\n"+
                        "<md-menu>\r\n"+
                        "<md-button aria-label=\"more\" class=\"md-icon-button\"\r\n"+
                        "ng-click=\"$mdOpenMenu($event)\">\r\n"+
                        "<img src=\"images\/redux\/icons\/dots-vertical.png\">\r\n"+
                        "<\/md-button>\r\n"+
                        "<md-menu-content width=\"2\">\r\n"+
                        "<md-menu-item>\r\n"+
                        "<md-button ng-click=\"flipWidget()\" aria-label=\"Flip widget\">\r\n"+
                        "Setting\r\n"+
                        "<\/md-button>\r\n"+
                        "<\/md-menu-item>\r\n"+
                        "<md-menu-item>\r\n"+
                        "<md-button ng-click=\"ctrl.requestChartData()\" aria-label=\"Refresh widget\">\r\nRefresh\r\n"+
                        "<\/md-button>\r\n"+
                        "<\/md-menu-item>\r\n"+
                        "<\/md-menu-content>\r\n"+
                        "<\/md-menu>\r\n"+
                        "<\/div>\r\n"+
                        "<div class=\"p-16 error-wrapper\" layout=\"row\" flexible-div=\"ctrl.drawAlertChart()\">\r\n"+
                        "<nvd3 id=\"alertChart\" options=\"ctrl.alertChartOpts\" data=\"ctrl.home.alertChartData.chartData\"\r\n"+
                        "api=\"ctrl.alertChartApi\"><\/nvd3>\r\n"+
                        "<error ng-if=\"ctrl.home.isAlertError === true\"\r\n"+
                        "msg=\"\'Error Occurred: We encountered a problem Alert chart. Please try again later.\'\"\r\n"+
                        "reload=\"ctrl.home.loadAlertData()\">\r\n"+
                        "<\/error>\r\n"+
                        "<\/div>\r\n"+
                        "<\/ms-widget-front>\r\n"+
                        "<ms-widget-back class=\"white-bg\">\r\n"+
                        "<div class=\"border-bottom\" layout-wrap layout=\"row\" flex=\"100\">\r\n"+
                        "<div flex layout=\"row\" layout-align=\"center center\">\r\n"+
                        "<span>Setting Alert Chart<\/span>\r\n"+
                        "<\/div>\r\n"+
                        "<md-button class=\"md-icon-button\" ng-click=\"flipWidget()\"\r\n"+
                        "aria-label=\"Flip widget\">\r\n"+
                        "<md-icon md-svg-icon=\"close\"><\/md-icon>\r\n"+
                        "<\/md-button>\r\n"+
                        "<\/div>\r\n"+
                        "<div class=\"ph-16 pv-8\" layout=\"row\" layout-align=\"center center\" flex=\"100\">\r\n"+
                        "<md-select id=\"drpRangePicker\" class=\"simplified font-size-16\" ng-model=\"ctrl.home.currentFilter.currentchartrange\"\r\n"+
                        "aria-label=\"Change date\"\r\n"+
                        "ng-change=\"ctrl.requestChartData()\">\r\n"+
                        "<md-option ng-repeat=\"(key, range) in ctrl.home.chartRanges\" ng-value=\"key\">\r\n{{::range}}\r\n"+
                        "<\/md-option>\r\n"+
                        "<\/md-select>\r\n"+
                        "<\/div>\r\n"+
                        "<div class=\"ph-16 pv-8\" layout=\"row\" layout-align=\"center center\" flex=\"100\">\r\n"+
                        "<md-select id=\"drpOrgPicker\" class=\"simplified font-size-16\"\r\n"+
                        "ng-model=\"ctrl.home.currentFilter.currentpatienttoggle\" aria-label=\"Change date\"\r\n "+
                        "ng-change=\"ctrl.requestChartData()\">\r\n"+
                        "<md-option ng-repeat=\"(key, range) in ctrl.home.patientsToggle\"\r\n"+
                        "id=\"patient-view-option-{{key}}\"  ng-value=\"key\">\r\n"+
                        "{{::range}}\r\n"+
                        "<\/md-option>\r\n"+
                        "<\/md-select>\r\n"+
                        "<\/div>\r\n"+
                        "<\/ms-widget-back>\r\n"+
                        "<\/ms-widget>",
            controllerAs: 'ctrl',

            controller: function($scope, $mdDialog) {
                var my =this;

                my.alertChartOpts = {
                    chart: {
                        type: 'pieChart',
                        height: 420,
                        donut: true,
                        x: function (d) { return d.label; },
                        y: function (d) { return d.value; },
                        showLabels: true,
                        labelType: 'value',
                        noData: 'No Alerts for the patients in this timeframe',
                        valueFormat: function (d) {
                            return d3.format(',.0f')(d);
                        },
                        pie: {
                            startAngle: function (d) { return d.startAngle / 2 - Math.PI / 2 },
                            endAngle: function (d) { return d.endAngle / 2 - Math.PI / 2 },
                            dispatch: {
                                elementClick: function (e) {
                                    var title = e.data.label +  ' Alerts - '
                                        + my.home.patientsToggle[my.home.currentFilter.currentpatienttoggle] + ' - ' + my.home.chartRanges[my.home.currentFilter.currentchartrange];
                                    my.handleChartClick('ALERT', title, e.data);
                                }
                            }
                        },
                        margin: {

                        },
                        duration: 500,
                        showLegend: true,
                        legend: {
                            margin: {
                                top: 12
                            }
                        },
                        title: 'Severity',
                        titleOffset: -10
                    },
                    title: {
                        enable: true,
                        text: 'Patients by Alerts Severity'
                    },
                    noData: {
                        enable: true,
                        text: 'No Alerts for the patients in this timeframe'
                    }
                };

                my.drawAlertChart = function () {
                    my.alertChartApi.update();
                };
              
                my.requestChartData = function () {
                    $appEvent('remote:appHome:loadAlertCount', {type : my.home.currentFilter.currentpatienttoggle, interval : my.home.currentFilter.currentchartrange, userList: my.home.subordinates});


                }
                my.getMessageBundle = function (defaultMsg, msgKey, replaceFunc) {
                    if(home.messageBundle && (home.messageBundle[msgKey])){
                        return replaceFunc(home.messageBundle[msgKey]);
                    } else {
                        return defaultMsg;
                    }

                };

                my.handleChartClick = function(popupType, title, data) {
                    GWT.activity();
                    var homeCtrl = $model.get('home');
                    homeCtrl.popupTitle = title;
                    my.alertPopupChartData =angular.copy(my.home.alertChartData);
                    my.encountersPopupData =angular.copy(my.home.encounters);

                    my.already_added = my.getMessageBundle(my.home.default_already_added, "popup.tooltip.already_added", function (configMsg) {
                        return configMsg;
                    });
                    
                    if (data.patients.size() !== 0) {
                        my.alertPatientList = [];
                        $model.get('app').showDialog('appViewPatients', popupType, null, function() {
                        });
                        if (data.patients.size() <= 500) {
                            my.patientCount = -1;
                            $appEvent('remote:appHome:loadPatientList', {patients : data.patients});
                        }
                        else {
                            $timeout(function() {
                                my.patientCount = data.patients.size();
                                my.showSearchPatientDialog(data.patients, popupType);
                            });
                        }
                    }  else {
                        $mdDialog.show(
                          $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('Message Box')
                            .textContent('There is no patient data')
                            .ariaLabel('There is no patient data')
                            .ok('OK')
                        );
                    }
                };
            }
        }
    }])


/**
 * Created by shirano on 3/29/2017.
 */
app
    .directive('gsiHiddenFrame',['$model', '$log','$timeout', '$window', function ($model, $log, $timeout, $window) {

    function createForm(id, method, path, target, params, onSubmit) {
        var form = document.createElement("form");
        form.setAttribute('id',id);
        form.setAttribute("method", method);
        form.setAttribute("action", path);
        form.setAttribute('target', target);
        form.onsubmit = onSubmit || angular.noop;
        params = params || {};
        for(var key in params) {
            if(params.hasOwnProperty(key)) {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value", params[key]);
                form.appendChild(hiddenField);
            }
        }
        var submitBtn = document.createElement("input");
        submitBtn.setAttribute("type", "submit");
        submitBtn.setAttribute("style","display:none;");
        form.appendChild(submitBtn);
        return form;
    }

    function createIframe(name){
        var iframe = document.createElement("iframe");
        iframe.setAttribute("style","display:none;");
        iframe.setAttribute("id", name);
        iframe.setAttribute('name',name);
        iframe.setAttribute('sandbox','allow-same-origin');
        return iframe;
    }

    function linkFn(scope,ele) {


        scope.$on('gsiBackgroundPost',function (event, args) {
            if(args!=="pentaho"){
                $log.warn(args+' not supported yet.');
                return;
            }
            var idPentahoForm = 'bgPentahoForm',
                idPentahoFrame = 'bgPentahoFrame';
            var login = $model.get('app').login;
            var matches = login.ssoPentahoServer &&
                login.ssoPentahoServer.match(/^(https?\:\/\/[^\/?#]+)(?:[\/?#]|$)/i);
            var domain = matches && matches[1];
            if(!domain){
                $log.warn('pentaho server address was not found.');
                return;
            }

            //remove old elements if exist
            ele.find('#'+idPentahoForm) && ele.find('#'+idPentahoForm).remove();
            ele.find('#'+idPentahoFrame) && ele.find('#'+idPentahoFrame).remove();

            ele.append(createIframe(idPentahoFrame));

            ele.append(createForm(
                idPentahoForm,
                'post',
                domain+'/pentaho/j_spring_security_check',
                idPentahoFrame,
                {
                    'j_username':login.email,
                    'j_password':login.special
                }));

            //This won't work if not same domain
            try{
                document.getElementById(idPentahoFrame).contentWindow.onerror=function(e) {
                    $log.warn('Error occurred in children frame: '+ idPentahoFrame, e);
                    return false;
                }
            }catch(e){$log.warn(e);}
            ele.find('#'+idPentahoFrame).on('load',function () {
                $log.info('pentaho login attempted');
                try{
                    var redirectUrl = document.getElementById(idPentahoFrame).contentWindow.location.href;
                    $log.info('redirected to:' +redirectUrl);
                    if(redirectUrl.indexOf('login_error') > -1){
                        $log.warn('Error occurred during Pentaho login.');
                        $model.get('app').settings.apps.login.pentahoAuthorized = false;
                    }else{
                        $window.GWT.setPentahoCalled(true);
                        $model.get('app').settings.apps.login.pentahoAuthorized = true;
                    }
                    $timeout(function () {
                        ele.find('#'+idPentahoForm) && ele.find('#'+idPentahoForm).remove();
                        ele.find('#'+idPentahoFrame) && ele.find('#'+idPentahoFrame).remove();
                    },0);
                }catch(e){
                    if(window.location.href.indexOf("://localhost") > -1) {
                        $log.warn('You are running application on localhost and seeing this error highly because of ' +
                            'CORS policy. You may see authentication popup, however the popup won\'t be displayed in non-localhost environment.');
                        $model.get('app').settings.apps.login.pentahoAuthorized = true;
                    }else{
                        $log.warn('Error occurred during Pentaho login.', e);
                    }
                    $timeout(function () {
                        ele.find('#'+idPentahoForm) && ele.find('#'+idPentahoForm).remove();
                        ele.find('#'+idPentahoFrame) && ele.find('#'+idPentahoFrame).remove();
                    },0);
                }
            });
            ele.find('#'+idPentahoForm).find('input[type="submit"]').click();

        })
    }

    return {
        restrict: 'E',
        replace:true,
        template: '<div style="display:none;"></div>',
        link: linkFn
    }
}]);

app.provider('gsiScrollConfig', gsiScrollConfigProvider)
   .directive('gsiScroll', gsiScrollDirective);


    /** @ngInject */
    function gsiScrollConfigProvider() {
        // Default configuration
        var defaultConfiguration = {
            wheelSpeed            : 1,
            wheelPropagation      : false,
            swipePropagation      : true,
            minScrollbarLength    : null,
            maxScrollbarLength    : null,
            useBothWheelAxes      : false,
            useKeyboard           : true,
            suppressScrollX       : false,
            suppressScrollY       : false,
            scrollXMarginOffset   : 0,
            scrollYMarginOffset   : 0,
            stopPropagationOnClick: true
        };

        // Methods
        this.config = config;

		// Extends the configuration above with a provided configuration object...
        function config(configuration) { defaultConfiguration = angular.extend( {}, defaultConfiguration, configuration ); }

        /**
         * Service
         */
        this.$get = function () {
            var service = { getConfig: getConfig };

            return service;

            function getConfig() { return defaultConfiguration; }
        };
    }

    /** @ngInject */
    function gsiScrollDirective($timeout, gsiScrollConfig, gsiUtils, gsiConfig) {
        return {
            restrict: 'AE',
            compile : function (tElement) {
                // Do not replace scrollbars if
                // 'disableCustomScrollbars' config enabled
                if ( gsiConfig.getConfig('disableCustomScrollbars') ) return;

                // Do not replace scrollbars on mobile devices
                // if 'disableCustomScrollbarsOnMobile' config enabled
                if ( gsiConfig.getConfig('disableCustomScrollbarsOnMobile') && gsiUtils.isMobile() ) return;

                // Add class
                tElement.addClass('gsi-scroll');

                return function postLink(scope, iElement, iAttrs) {
                    var options = {};

                    // If options supplied, evaluate the given
                    // value. This is because we don't want to
                    // have an isolated scope but still be able
                    // to use scope variables.
                    // We don't want an isolated scope because
                    // we should be able to use this everywhere
                    // especially with other directives
                    if ( iAttrs.gsiScroll ) options = scope.$eval(iAttrs.gsiScroll);

                    // Extend the given config with the ones from provider
                    options = angular.extend({}, gsiScrollConfig.getConfig(), options);

                    // Initialize the scrollbar
                    $timeout(function () {
                        PerfectScrollbar.initialize(iElement[0], options);
                    }, 0);

                    // Update the scrollbar on element mouseenter
                    iElement.on('mouseenter', updateScrollbar);

                    // Watch scrollHeight and update
                    // the scrollbar if it changes
                    scope.$watch(function () {
                        return iElement.prop('scrollHeight');
                    }, function (current, old) {
                        if ( angular.isUndefined(current) || angular.equals(current, old) ) return;
                        updateScrollbar();
                    });

                    // Watch scrollWidth and update
                    // the scrollbar if it changes
                    scope.$watch(function () {
                        return iElement.prop('scrollWidth');
                    }, function (current, old) {
                        if ( angular.isUndefined(current) || angular.equals(current, old) ) return;
                        updateScrollbar();
                    });

                    /**
                     * Update the scrollbar
                     */
                    function updateScrollbar() { PerfectScrollbar.update(iElement[0]); }

                    // Cleanup on destroy
                    scope.$on('$destroy', function () {
                        iElement.off('mouseenter');
                        PerfectScrollbar.destroy(iElement[0]);
                    });
                };
            }
        };
    }
app.directive('gsiTaskItem',["$model","$appEvent", "$q", "$mdDialog", function($model,$appEvent,$q, $mdDialog){
    var styles = {
        1: 'todo',
        2: 'in-progress',
        3: 'complete',
        4: 'cancelled'
    };
    var template = "<div class=\"task-item\"\n" +
        "           ng-class=\"itemClass()\" ng-show=\"!filter||filter[item.taskProgressStatusId.taskProgressStatusId].show\"" +
        "             layout=\"row\" layout-align=\"space-between center\">\n" +
        "            \n" + "<div ng-if=\"item.meta.info.icon() && item.taskProgressStatusId.taskProgressStatusId !== 3\" " +
        "                           style=\"position: relative; display: block; width: 0px; height: 0px;\" ng-click=\"badgeClick($event)\">\n" +
        "                    <div aria-label=\"{{item.meta.info.text()}}\">\n" +
        "                        <img width=\"16px\" height=\"16px\" style=\"margin-left:-20px;margin-top: 5px;cursor:pointer;\" ng-src=\"images/redux/icons/16px_information.png\" src=\"images/redux/icons/16px_information.png\">\n" +
        "                        \n" +
        "                    </div>\n" +
        "                </div>"+
        "            <div layout=\"column\" layout-align=\"space-between center\" \n" +
        "                 style=\"cursor:pointer;\" ng-click=\"details($event)\" flex=\"none\">\n" +
        "                 <div>\n"+
        "                     <md-tooltip>{{item.meta.hoverTip}}</md-tooltip>\n" +
        "                        <img style=\"margin:5px;\" ng-src=\"images/redux/icons/{{item.meta.icon.due}}.png\" />\n" +
        "                 </div>\n"+
        "                 <div>\n"+
        "                     <md-tooltip>{{taskIconHoverTip()}}</md-tooltip>\n" +
        "                        <img style=\"margin:5px;\" ng-src=\"images/redux/icons/{{item.meta.icon.type}}.png\" />\n" +
        "                  </div>\n"+
        "            </div>    \n" +
        "            <div ng-drag-handle class='task-item-text' layout=\"column\" layout-align=\"start start\" flex>\n" +
        "                <h3>{{ item.userTaskTitle}}</h3>\n" +
        "                <p>{{ item.userTaskDescr}}\n" +
        "                </p>\n" +
        "                <h4 ng-if='item.patientInfo !== undefined && item.patientInfo.lastname !== undefined'>{{ item.patientInfo.lastname}}, {{ item.patientInfo.firstname}}</h4>\n" +
        "            </div>\n" +
        "            <div  flex=\"nogrow\" ng-click=\"details($event)\"\n" +
        "                 style=\"cursor:pointer\" layout=\"column\" layout-align=\"center center\" >\n" +
        "                <img src=\"images/redux/icons/dots-vertical.png\">\n" +
        "            </div>    \n" +
        "            \n" +
        "            </div>";

    return {
        restrict: 'E',
        replace: false,
        scope:{
            item:'=',
            filter:'=?'
        },
        template:template,
        link:function(scope){
            function SQLdate(time){
                if (typeof time === 'undefined') return moment(Date.now()).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
                else return moment(time).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            }
            function checkConsent(data) {
                $appEvent('remote:appTaskList:checkConsent', data);
            }

            function isSystem(task) {
                return (typeof task.systemTaskId === 'undefined') ? false : true;
            }
            function checkMinor(data) {
                if(data.patientInfo !== undefined && data.patientInfo.dob !== undefined) {
                    $appEvent('remote:appTaskList:checkMinor', data.patientInfo.dob);
                }
            }
            scope.taskIconHoverTip = function(){
              if (isSystem(scope.item)) {
                return "System Task";
              }
              return "User Task";
            }
            scope.itemClass = function(){
                if (scope.item.taskProgressStatusId === undefined) {
                    return "";
                }
                return styles[scope.item.taskProgressStatusId.taskProgressStatusId];
            };
            scope.details = function (ev) {
                
                GWT.activity();
                $model.get('task').comments = [];          
                $appEvent('remote:appTaskList:getComments', {userTaskId: scope.item.userTaskId, communityId: scope.item.communityId});

                if (!$model.get('app').dialog.isOpen || $model.get('app').dialog.allowNextPopup)  {
                    $model.get('app').dialog.isOpen = false;
                    $model.get('app').dialog.allowNextPopup = false;
                    if (isSystem(scope.item)) {
                        checkConsent(scope.item);
                    }
                    $model.get('task').minorWarning = undefined;
                    checkMinor(scope.item);
                    scope.item.dueDateTime = moment(scope.item.dueDateTime).toDate();
                    
                    $model.get('task').orgCareteamUserSearch.selectedItem = {};
                    $model.get('task').orgCareteamUserSearch.selectedItem.firstName = scope.item.userFirstName;
                    $model.get('task').orgCareteamUserSearch.selectedItem.lastName = scope.item.userLastName;
                    $model.get('task').orgCareteamUserSearch.selectedItem.email = scope.item.userEmailId;

                    var cloneTask = {};
                    angular.copy(scope.item, cloneTask);
                    if (cloneTask.patientInfo === undefined || cloneTask.patientInfo === null) {
                        cloneTask.patientInfo = null;
                    }
                    
                    $model.get('app').showDialog('appTaskDetails', cloneTask, ev, function (result) {
                        switch (result) {
                            case 'ok':
                                cloneTask.dueDateTime = SQLdate(cloneTask.dueDateTime);
                                
                                cloneTask.lastUpdateDateTime = SQLdate();

                                $model.get('task').addMeta(cloneTask);
                                $model.get('task').setAssignTaskFields(cloneTask);
                                $model.get('task').setLastUpdateFields(cloneTask);

                                $model.get('task').tempTasks['' + scope.item.userTaskId] = scope.item;
                                
                                delete cloneTask['$$hashKey'];
                                delete cloneTask.meta;
                                $appEvent('remote:appTaskList:update', cloneTask);
                                break;
                            case 'cancel':
                                break;
                        }
                    });
                }
            };
            scope.badgeClick=function (ev) {
                GWT.activity();
                // backup the task to an original state (to revert if cancel is clicked)
                $model.get('app').showDialog('calendarTaskList', taskList, ev, function(result) {
                    switch (result) {
                        case 'close':
                            break;
                    }
                });
            }
        }
    }
}]);

app.directive('iframeWidget', ['$model','$appEvent', '$q', '$sce', '$http', function($model, $appEvent, $q, $sce, $http) {

    var template = '<ms-widget layout="row" flex="100" class="nopadding-top">' +
                       '<ms-widget-front class="white-bg">' +
//                           '<div id="{{ctrl.name}}-container" ng-style="ctrl.iframeStyle" class="iframe-container {{ctrl.iframeClass}}">' +
                               '<iframe src="{{ctrl.trustSrc(ctrl.url)}}" frameborder="0" width="100%" style="height: 78vh;"></iframe>' +
//                           '</div>' +
                       '</ms-widget-front>' +
                   '</ms-widget>';

    var controllerFn = function($scope, $timeout) {
        var self = this;
        this.iframeStyle = this.height? {'height': this.height} : {};
        this.iframeClass = !this.height? 'iframe-container-' + this.ratio.replace(':', '-') : '';

        this.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
        }

    }

    return {
        restrict: 'E',
        scope: true,
        template: template,
        controller: controllerFn,
        controllerAs: 'ctrl',
        bindToController: {
            name: '@',
            ratio: '@',
            url: '@', 
            height: '@'
        }
    }
}]);

app.directive('parseWidget', function ($compile, $timeout) {
  var holderPattern = /#\{(.[^{]+)\}/ig;
  return {
    restrict: 'E',
    scope: {
      config: '='
    },
    replace: true,
    link: function (scope, ele, attrs) {
      scope.$watch(getTemplate, function (html) {
        ele.html(html);
        $compile(ele.contents())(scope);
      });
      if(scope.config.parameter) {   
        Object.keys(scope.config.parameter).forEach(function(key) {
          if (scope.config.parameter[key].match(holderPattern)) {
            var value = scope.config.parameter[key];
            scope.$parent.$watch(value.substring(2, value.length - 1), function (data) {
              scope[key] = data;
              if (key==='hidewidget') {
                scope.config.hidewidget = data;
              }
            }, false);
          }
        });
      }

      function getTemplate() {
        var parameter = ' name="' + scope.config.name + '"';
        for (var key in scope.config.parameter) {
          if (scope.config.parameter[key].match(holderPattern)) {
            parameter += ' '+ key + '="' + key + '" ';
          } else {
            parameter += ' '+ key + '="' + scope.config.parameter[key] + '" ';
          }
        }
        return '<' + scope.config.type + parameter + '>' + '</' + scope.config.type + '>';
      }
    }
  };
});
/*Form validation helper to compare a confirmation password to a new password*/
app.directive('passwordMatch', [function () {
    return {
        restrict: 'A',
        scope: true,
        require: ['^ngModel', '^form'],
        link: function (scope, elem, attrs, ctrls) {
            var ngModel = ctrls[0]; //confirmPwd
            var formCtrl = ctrls[1];

            var otherPasswordModel = formCtrl[attrs.passwordMatch]; //newPwd

            ngModel.$parsers.push(function (value) {
                var valid = (value === otherPasswordModel.$viewValue);
                ngModel.$setValidity('mismatch', valid);
                ngModel.$render();
                return value;
            });
            otherPasswordModel.$parsers.push(function (value) {
                var valid = (value === ngModel.$viewValue);
                ngModel.$setValidity('mismatch', valid);
                ngModel.$render();
                return value;
            });

            //For model -> DOM validation
            ngModel.$formatters.unshift(function (value) {
                var valid = (value === otherPasswordModel.$viewValue);
                ngModel.$setValidity('mismatch', valid);
                return value;
            });
            otherPasswordModel.$formatters.unshift(function (value) {
                var valid = (value === ngModel.$viewValue);
                ngModel.$setValidity('mismatch', valid);
                return value;
            });


        }
    };
}])
app.directive('pressEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13 && !event.shiftKey) {
                    scope.$apply(function(){
                        scope.$eval(attrs.pressEnter, {'event': event});
                    });
                    event.preventDefault();
                }
            });
        };
    });
//Temporary patch to call GWT function to resize GWT applications when opening side navs
app.directive('resizeGwtOnTransitionEnd', [
    '$log',
    function ( $log  ) {
        var transitions = {
            "transition"      : "transitionend",
            "OTransition"     : "oTransitionEnd",
            "MozTransition"   : "transitionend",
            "WebkitTransition": "webkitTransitionEnd"
        };

        var whichTransitionEvent = function () {
            var t,
                el = document.createElement("fakeelement");

            for (t in transitions) {
                if (el.style[t] !== undefined){
                    return transitions[t];
                }
            }
        };

        var transitionEvent = whichTransitionEvent();

        return {
            'restrict': 'A',
            'link': function (scope, element, attrs) {

                element.bind(transitionEvent, function (evt) {
                    if(evt.originalEvent.propertyName!=='width') return;
                    // $log.debug('got a css transition event', evt);
                    GWT.resizeTab();

                }).children().bind(transitionEvent,function (evt) {
                    return false;
                });
            }
        };
    }]);
app.directive('showButton', function($animate) {
    return function(scope, element, attrs) {
        scope.$watch(attrs.showButton, function(newVal) {
            if (newVal) {
                $animate.addClass(element, 'toolStripButtonOver');
            } else {
                $animate.removeClass(element, 'toolStripButtonOver');
            }
        })
    }
});
app.directive('simplex', function () {
    return {
        link: function ($scope, element, attrs) {
            element.bind('click', function () {
                element.html('You clicked me!');
            });
            element.bind('mouseenter', function () {
                element.css('background-color', 'yellow');
            });
            element.bind('mouseleave', function () {
                element.css('background-color', 'white');
            });
        }
    };
});
app.directive('sizeWatch', function () {
    function link(scope, element, attrs) {
        scope.$watch(function () {
            scope.tableWidth = {
                width: element[0].firstChild.childNodes[1].offsetWidth + 'px'
            };
        });
    }
    return {
        restrict: 'AE',
        link: link
    };
}); 
app.directive('taskBadgeCountWidget',["$model", "$appEvent", "$log", "taskBadgeFilter", "constants", function($model, $appEvent, $log, taskBadgeFilter, constants){

    var template =
                "<div class=\"widget-group\" layout=\"row\" flex=\"100\" layout-wrap>\n" +
                "        <ms-widget layout=\"row\" flex=\"100\">\n" +
                "            <ms-widget-front class=\"white-bg\">\n" +
                "                <div class=\"ph-16 pv-8 border-bottom\" layout=\"row\" layout-align=\"space-between center\" layout-wrap>\n" +
                "                    <div class=\"pv-8\" layout=\"row\" layout-align=\"start center\" layout-align-sm=\"end\" flex-sm=\"100\">\n" +
                "                        <md-select id=\"drpDatePicker\" class=\"simplified font-size-16\" ng-model=\"home.currentDatePicker\" aria-label=\"Change date\"\n" +
                "                            ng-change=\"changeTaskSelection()\">\n" +
                "                            <md-option ng-repeat=\"range in ::home.ranges\" ng-value=\"range\">\n" +
                "                                {{::range.label}}\n" +
                "                            </md-option>\n" +
                "                        </md-select>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "                <div class=\"widget-group layout-wrap ng-scope layout-row error-wrapper\" layout=\"row\" layout-wrap=\"\">\n" +
                "                    <error ng-if=\"home.isTaskBadgeError === true\"\n" +
                "                        msg=\"'Error Occurred: We encountered a problem Task Badges. Please try again later.'\"\n" +
                "                        reload=\"reloadTaskBadgeCount()\">\n" +
                "                    </error>\n" +
                "                    <error type=\"warning\" ng-if=\"hidewidget.value === true\"\n" +
                "                        msg=\"'This mode view is not supported in task badge count.'\">" +
                "                    </error>\n" +
                "                    <ms-widget id=\"overdueTaskBox\" class=\"ms-widget-custom\" flippable=\"true\" layout=\"column\" flex=\"100\" flex-gt-xs=\"50\" flex-gt-md=\"25\">\n" +
                "                        <!-- Front -->\n" +
                "                        <ms-widget-front class=\"white-bg\" ng-click=\"openTaskManager('overdue');\" style=\"cursor:pointer;\">\n" +
                "                            <div class=\"pt-32 pb-32\" layout=\"column\" layout-align=\"center center\">\n" +
                "                                <div class=\"red-fg font-size-72 line-height-72 ng-binding\" id=\"overdueTaskCount\">\n" +
                "                                    {{home.overdueTask.count}}\n" +
                "                                </div>\n" +
                "                                <div class=\"h3 secondary-text font-weight-500\" style=\"font-size:0.9rem\" id=\"overdueFooter\" align=\"center\">{{::home.taskBadgeDescription.overdue}} (TODAY)</div>\n" +
                "                            </div>\n" +
                "                        </ms-widget-front>\n" +
                "                    </ms-widget>\n" +
                "\n" +
                "                    <ms-widget id=\"todoTaskBox\" class=\"ms-widget-custom\" flippable=\"true\" layout=\"column\" flex=\"100\" flex-gt-xs=\"50\" flex-gt-md=\"25\">\n" +
                "\n" +
                "                        <!-- Front -->\n" +
                "                        <ms-widget-front class=\"white-bg\" ng-click=\"openTaskManager('todo');\" style=\"cursor:pointer;\">\n" +
                "                            <div class=\"pt-32 pb-32\" layout=\"column\" layout-align=\"center center\">\n" +
                "                                <div class=\"light-blue-fg font-size-72 line-height-72\" id=\"todoTaskCount\">\n" +
                "                                    {{home.todoTask.count}}\n" +
                "                                </div>\n" +
                "                                <div class=\"h3 secondary-text font-weight-500\" style=\"font-size:0.9rem\" id=\"todoTaskFooter\" align=\"center\">{{::home.taskBadgeDescription.due}}</div>\n" +
                "                            </div>\n" +
                "                        </ms-widget-front>\n" +
                "                    </ms-widget>\n" +
                "\n" +
                "\n" +
                "                    <ms-widget id=\"nonAssessTaskBox\" class=\"ms-widget-custom\" flippable=\"true\" layout=\"column\" flex=\"100\" flex-gt-xs=\"50\" flex-gt-md=\"25\">\n" +
                "\n" +
                "                        <!-- Front -->\n" +
                "                        <ms-widget-front class=\"white-bg\" ng-click=\"openTaskManager('careplan');\" style=\"cursor:pointer;\">\n" +
                "                            <div class=\"pt-32 pb-32\" layout=\"column\" layout-align=\"center center\">\n" +
                "                                <div class=\"orange-fg font-size-72 line-height-72\" id=\"nonAssessTaskCount\">\n" +
                "                                    {{home.cpNonassessTask.count}}\n" +
                "                                </div>\n" +
                "                                <div class=\"h3 secondary-text font-weight-500\" style=\"font-size:0.9rem\" id=\"nonAssessTaskFooter\" align=\"center\">{{::home.taskBadgeDescription.careplan}}</div>\n" +
                "                            </div>\n" +
                "                        </ms-widget-front>\n" +
                "                    </ms-widget>\n" +
                "\n" +
                "\n" +
                "                    <ms-widget id=\"assessTaskBox\" class=\"ms-widget-custom\" flippable=\"true\" layout=\"column\" flex=\"100\" flex-gt-xs=\"50\" flex-gt-md=\"25\">\n" +
                "\n" +
                "                        <!-- Front -->\n" +
                "                        <ms-widget-front class=\"white-bg\" ng-click=\"openTaskManager('assessments');\" style=\"cursor:pointer;\">\n" +
                "                            <div class=\"pt-32 pb-32\" layout=\"column\" layout-align=\"center center\">\n" +
                "                                <div class=\"blue-grey-fg font-size-72 line-height-72\" id=\"assessTaskCount\">\n" +
                "                                    {{home.cpAssessTask.count}}\n" +
                "                                </div>\n" +
                "                                <div class=\"h3 secondary-text font-weight-500\" style=\"font-size:0.9rem\" id=\"assessTaskFooter\" align=\"center\">{{::home.taskBadgeDescription.assessment}}</div>\n" +
                "                            </div>\n" +
                "                        </ms-widget-front>\n" +
                "                    </ms-widget>\n" +
                "                </div>\n" +
                "            </ms-widget-front>\n" +
                "        </ms-widget>\n" +
                "    </div>"

    return {
        restrict: 'E',
        replace: false,
        scope:{
            name: '@',
            api: '=',
            hidewidget: '='
        },
        template:template,
        link:function(scope){
            scope.home = $model.get("home");
            scope.app = $model.get("app");
            scope.home.todoTask = {
              count: 0,
              tasks: [],
              email: '',
              interval: 0
          };

          scope.home.overdueTask = {
              count: 0,
              tasks: [],
              email: '',
              interval: 0
          };

          scope.home.cpNonassessTask = {
              count: 0,
              tasks: [],
              email: '',
              interval: 0
          };

          scope.home.cpAssessTask = {
              count: 0,
              tasks: [],
              email: '',
              interval: 0
          };

          scope.home.ranges = [
              { index: 0, label: "Today" },
              { index: 7, label: "Next 7 Days" },
              { index: 30, label: "Next 30 Days" }
          ];

          scope.home.patientsToggle = {
              1: "My Patient Panel",
              2: "My Org Patients"
          };

          scope.home.taskBadgeDescription = {
              overdue : 'OVERDUE TASKS',
              due : 'DUE TASKS',
              careplan : 'CARE PLAN TASKS',
              assessment : 'ASSESSMENT TASKS'
          };

          scope.home.currentRange = 'DY';

          scope.home.currentDatePicker =  scope.home.ranges[0];

          scope.reloadTaskBadgeCount = function(isFirstLoad) {
            if (scope.home.currentMember === undefined)
              $appEvent('remote:appHome:loadTaskCount', {interval :  scope.home.currentDatePicker.index, type : constants.VIEW_TYPE.MY_VIEW});
            else
               $appEvent('remote:appHome:loadTaskCount', {interval :  scope.home.currentDatePicker.index, type: scope.home.currentMember.type,  userList: scope.home.selectedSubordinates });
          }

          scope.reloadTaskBadgeCount(true);

          scope.changeTaskSelection = function () {
              scope.reloadTaskBadgeCount();
          };

          scope.openTaskManager = function(badgeType) {
              if (scope.home.currentMember.type === constants.VIEW_TYPE.MY_VIEW) {
                  taskBadgeFilter.set(badgeType, scope.home.currentMember, scope.home,  constants.VIEW_TYPE.MY_VIEW);
              } else if (scope.home.selectedSubordinates.size() === 1) {
                  taskBadgeFilter.set(badgeType, scope.home.selectedSubordinates[0], scope.home, constants.VIEW_TYPE.TEAM_VIEW_ONE);
              } else if (scope.home.selectedSubordinates.size() < scope.app.subordinates.size()){
                  taskBadgeFilter.set(badgeType, scope.home.currentMember, scope.home, constants.VIEW_TYPE.TEAM_VIEW_SOME);
              } else {
                  taskBadgeFilter.set(badgeType, scope.home.currentMember, scope.home, constants.VIEW_TYPE.TEAM_VIEW);
              }

              var taskScope = $model.scope("task");

              if (taskScope !== undefined) {
                  taskScope.task.switchToTaskBadgeFilterMode();
              }

              var userApps = $model.get("app").userApps;
              taskManagerApp = userApps.filter(function(item) {
                  if (item.applicationName === "GSI_TASK_MANAGER_APP")
                      return true;

                  return false;
              })[0];

              $appEvent(taskManagerApp.event, taskManagerApp);
          };

          scope.api.refresh = function (isOrgMode) {
            scope.hidewidget.value = isOrgMode;
            scope.reloadTaskBadgeCount();
          };
        }
    }
}]);

app.filter('gender', function() {
  return function(sex) {
    switch(sex.toUpperCase()) {
        case 'M':
        case 'MALE':
            return 'Male';
            break;
        case 'F':
        case 'FEMALE':
            return 'Female';
            break;
        case 'MF':
        case 'FM':
        case 'TRANS':
        case 'TRANSGENDER':
            return 'Transgender';
            break;
    }
  };
});

app.filter('ssn', function() {
  return function(ssn) {
      return 'xxx-xxx-' + ssn.slice(5);
  };
});

app.filter('ifNotNull', function() {
  return function(input) {
    if (typeof input === 'undefined' || input === null || input === '') return '';
    switch(input.toUpperCase()) {
        case 'NULL':
        case 'NULL_VALUE':
            return '';
            break;
        default:
            return input;
            break;
    }
  };
});
app.filter('filterPatients',['$filter', function($filter) {
  return function(patients, query) {
    query = query.toLowerCase();
    return patients.filter(function(patient) {
      if (!query
      || (patient.firstname && patient.firstname.toLowerCase().indexOf(query)!=-1)
      || (patient.lastname && patient.lastname.toLowerCase().indexOf(query) !=-1)
      || (patient.patientId && patient.patientId.toString().indexOf(query)!=-1)
      || (patient.gender && patient.gender.toLowerCase().indexOf(query) !=-1)
      || (patient.dob && $filter('date')(patient.dob, 'MM/dd/yyyy').indexOf(query)!=-1)
      || (patient.primaryPayerClass && patient.primaryPayerClass.toLowerCase().indexOf(query)!=-1)
      || (patient.primaryPayerMedicaidMedicareId && patient.primaryPayerMedicaidMedicareId.toLowerCase().indexOf(query)!=-1)
      || (patient.orgName && patient.orgName.toLowerCase().indexOf(query)!=-1)
      || (patient.status && patient.status.toLowerCase().indexOf(query)!=-1)) {
          return true;
      }
      return false;
    });
  };
}]);

var checkWidget = function(widget, model) {
  if (widget.hidewidget && widget.hidewidget.value === true)
    return false;

  if (widget.depends && !model.get('app').settings.apps[widget.depends.split('.')[0]][widget.depends.split('.')[1]])
    return false;

  if(!widget.permissions || widget.permissions.length <= 0)
      return true;

  for(var i=0; i<widget.permissions.length; i++) {
      if(model.get('app').can(widget.permissions[i]) === true)
          return true;
  }
  return false;
}

app.filter('filterTabs', ['$model', function($model) {
  return function(tabs) {
    return tabs.filter(function(tab) {
            if (tab.isHide === true)
              return false;

            var widCount = 0;
            for(var i=0; i<tab.widgets.length; i++) {
              if (checkWidget(tab.widgets[i], $model) === false)
                widCount = widCount + 1;
            }
            
            if(widCount === tab.widgets.length)
              return false;

            if(!tab.permissions || tab.permissions.length <= 0) {
                return true;
            }
            for(var i=0; i<tab.permissions.length; i++) {
                if($model.get('app').can(tab.permissions[i]) === true )
                    return true;
            }
            return false;
    });
  };
}]);

app.filter('filterWidget', ['$model', function($model) {
  return function(widgets) {
    return widgets.filter(function(widget) {
        return checkWidget(widget, $model);
        /*if(!widget.permissions || widget.permissions.length <= 0) {
            return true;
        }

        for(let i=0; i<widget.permissions.length; i++) {
            if($model.get('app').can(widget.permissions[i]) === true)
                return true;
        }
        return false;*/
    });
  };
}]);

app.filter('statusFilter', function() {
  return function(tasks, filter) {
    return tasks.filter(function(task) {
      return filter[task.taskProgressStatusId.taskProgressStatusId].show;
    });
  };
});

app.filter('taskFilter',['$filter', function($filter) {
  return function(tasks, query) {
    query = query.toLowerCase();

    return tasks.filter(function(task) {

        var patient = '';

        if (task.patientInfo) {
            if (task.patientInfo.firstname)
                patient = patient + task.patientInfo.firstname + ' ';
            if (task.patientInfo.lastname)
                patient = patient + task.patientInfo.lastname;
        }

        if (!query
            || (task.userTaskTitle && task.userTaskTitle.toLowerCase().indexOf(query)!=-1)
            || (task.userTaskDescr && task.userTaskDescr.toLowerCase().indexOf(query) !=-1)
            || (patient.toLowerCase().indexOf(query)!=-1)) {
                return true;
        }
        return false;
    });
  };
}]);

app.filter('filterViewMenu', ['$model', function($model) {
  return function(menuViews, filterObj) {
    return menuViews.filter(function(menuView) {
        if(filterObj.typeNotShown) {
          return menuView.type !== filterObj.typeNotShown;
        }
        if(filterObj.hasChild === false) {
          return menuView.type !== 3;
        }
        return true;
    });
  };
}]);

app.filter('orderWithPriority',['$filter', function($filter) {
  return function(items, fields, priorityFields) {
    var filtered = [];
    var rest = [];
    
    if (fields.length === priorityFields.length) {
      angular.forEach(items, function(item) {
        var matched = true;

        for(var i = 0; i < fields.length; i++) {
          if(item[fields[i]].toLowerCase() !== priorityFields[i].toLowerCase()) {
            matched = false;
          }   
        }
        if (matched) {
          filtered.push(item)
        }
        else {
          rest.push(item);
        }
      });

      // sort the rest of order list follow next fields
      if (fields.length > 1) {
        for(var i = 0; i < fields.length; i++) {
          var result = orderRestItems(rest, fields[i], priorityFields[i]);
          filtered = filtered.concat(result.ordered);
          rest = result.rest;
        }
      }
      filtered = filtered.concat(rest);
      
    }
    else {
      filtered = items;
    }
    
    return filtered;
  };
}]);

// sort by each item when there is more than 1 order fields
function orderRestItems(restList, field, priority) {
  var result = {
    ordered: [],
    rest: []
  };
  angular.forEach(restList, function(item) {
    var matched = false;
    if (item[field].toLowerCase() === priority.toLowerCase()) {
      result.ordered.push(item);
    }
    else {
      result.rest.push(item);
    }
  });
  return result;
}
