var app=angular.module('redux',[]);app.controller('AppReportController', function($mdDialog, $mdMedia, $model, $scope, $appEvent, $filter, $trace, $log) {var my = this;$model.store('report', $scope);if (typeof boot == 'function') boot();function boot() {
	$log.info('Booting Reports App.');

	// Reports...
    my.reports = {

			Patients: [
				{id: 'patientNotConsented',		title: 'Patients Not Consented', color: '#FFE082',	description: 'A list of those patients in your organization who have not yet provided consent.'},
				{id: 'patientConsented',		title: 'Consented Patients', color: '#FFCC80',	description: 'A list of those patients for whom your organization has consent.'},
				{id: 'patientTotals',			title: 'Total Patient Count', color: '#90CAF9', description: 'A summary count of patients in various categories in the GSIHealthCoordinator.'},
				{id: 'patientProgramReports',	title: 'Patient Program Report', color: '#80DEEA',	description: 'A report by patient of participation in programs and program status.'}
			]
	};

	var accesslevel = $model.get('app').login.accessLevelId;
	if(accesslevel === 1 || accesslevel === 2 || accesslevel === 4){
		my.reports.Administration = [
			{id: 'adminUserList', title: 'Administrative Reports User List', color: '#80DEEA', description: 'See a list of users who are on GSIHealthCoordinator.'}
		];
		my.reports.Patients.unshift({id: 'patientTrackingSheet',	title: 'Patient Tracking Sheet', color: '#FFAB91', description: 'An Excel summary of patients in your organization together with demographic data, enrollment status and encounter counts.', button: 'Download Report'});
	}

	// App feature list (for use in the future)...
	my.features = [
		{ name: 'email'		, icon: 'email'				, description: 'eMail this' },
		{ name: 'word'		, icon: 'file-excel-box'	, description: 'Create a Word file' },
		{ name: 'excel'		, icon: 'file-excel-box'	, description: 'Create an Excel file' },
		{ name: 'pdf'		, icon: 'file-pdf-box'		, description: 'Create a PDF' }
	];
}
});app.controller('AppTaskController', function($mdDialog, $mdMedia, $model, $scope, $rootScope, $appEvent, $timeout, $q, $interval, $filter, $trace, taskBadgeFilter, $log, constants) {var my = this;$model.store('task', $scope);if (typeof boot == 'function') boot();function boot() {
	var trace = new $trace('task.boot');
	my.appCtrl = $model.get('app');
	my.loadTasks = function(event, data){
		my.tasks = [false];
		$timeout(function() {
			if(my.isLoading)
				my.showProgress = true;
		}, 500);
		$appEvent(event, data);
	};

	// Store our own reference to devmode...
	my.devmode = {
		enabled: $model.get('app').devmode,
		toggles: {}
	};

	var bootStart = new Date();

	// Boot the app (populate the model)...
	my.tasks = [false];
	my.timeTasks = [];
	my.search = {
		time:'',
		status: '',
		list:   ''
	};
	my.tempTasks = {};

    my.taskBadgeFilter = taskBadgeFilter;

	my.searchCalendarTask = function () {
		$model.get('calendarTemplate').updateSarchString(my.search.time);
	};

	// Currently selected tab. Default is Home tab
	my.selectedTab = 0;
	// Track which tab has been loaded...
	my.loadedTabs = [];
	// Keeps track of available tabs here to lazy load tabs.
	// Please comment out tab-name when you want to disable a tab
	my.appTabs = [
		//'home',
		//'status',
		'multiStatus',
		'time'
		//'list'
	];

    //my.currentMember = my.appCtrl.subordinates[0];
    my.loginedUser = my.appCtrl.login;
    //taskBadgeFilter.set("noBadgeFilter", my.loginedUser, undefined );
    my.taskBadgeData = my.taskBadgeFilter.data;

    my.minorWarning = undefined;

	trace.did('vars');

	// Request data models (getting this started ASAP)...
	my.loadTasks('remote:appTaskList:boot', my.taskBadgeFilter.data);

	trace.did('bootEvent');

	// Define pre-calculated reference dates for comparisons...
	var REFERENCE = moment();
	my.refDate = {
		today:              REFERENCE.clone().startOf('day'),
		tomorrow:           REFERENCE.clone().add(1, 'days').startOf('day'),
		dayAfterTomorrow:   REFERENCE.clone().add(2, 'days').startOf('day'),
		nextWeek:           REFERENCE.clone().add(1, 'week').startOf('day'),
		nextMonth:          REFERENCE.clone().add(1, 'months').startOf('day')
	};

	my.localTimeZone = moment(Date.now()).format(".SSSZ");

	trace.did('moment');


	var todoColor = '#bbdefb',
		inProgressColor = '#fff9c4',
		completedColor = '#dcedc8',
		cancelledColor = '#f5f5f5';

	// The filter shows/hides items in each tab...
	my.filter = {
		1: { show: true,    css:'todo',          color: todoColor       },
		2: { show: true,    css:'in-progress',  color: inProgressColor },
		3: { show: taskBadgeFilter.data.isFiltered === true ? false: true,    css:'complete', color: completedColor},
		4: { show: false,    css:'cancelled',    color: cancelledColor}
	};

	// Returns status filter
	my.statusFilterFn = function(){
		return function(item){
			var id = item.taskProgressStatusId.taskProgressStatusId;
			return my.filter[id].show;
		}
	};
	my.showProgress= false;

	trace.did('watcher');

	my.listHeading = [
		{name: 'past',    show: true,  empty: true, title: 'Past Due'},
		{name: 'today',   show: true,  empty: true, title: 'Due today'},
		{name: 'tomorrow',show: true,  empty: true, title: 'Due tomorrow'},
		{name: 'week',    show: true,  empty: true, title: 'Due this week'},
		{name: 'far',     show: true,  empty: true, title: 'Due next week and beyond'},
		{name: 'none',    show: true,  empty: true, title: 'No due date'}
	];

	// For time view...
	my.time = {
		view: 'week',
		date: new Date(),
		title: 'title here',
		events:   [
			{
				title: 'My event title', // The title of the event
				type: 'info', // The type of the event (determines its color). Can be important, warning, info, inverse, success or special
				startsAt: new Date(2016,4,1,1), // A javascript date object for when the event starts
				endsAt: new Date(2016,5,26,15), // Optional - a javascript date object for when the event ends
				editable: false, // If edit-event-html is set and this field is explicitly set to false then dont make it editable.
				deletable: false, // If delete-event-html is set and this field is explicitly set to false then dont make it deleteable
				draggable: true, //Allow an event to be dragged and dropped
				resizable: true, //Allow an event to be resizable
				incrementsBadgeTotal: true, //If set to false then will not count towards the badge total amount on the month and year view
				recursOn: 'year', // If set the event will recur on the given period. Valid values are year or month
				cssClass: 'a-css-class-name' //A CSS class (or more, just separate with spaces) that will be added to the event when it is displayed on each view. Useful for marking an event as selected / active etc
			}
		]
	};

	my.minDate = new Date('1/1/1880');
	my.maxDate = new Date('1/31/2130');

	var loginInfo = $model.get("app").login;
    my.currentView = {
		email: loginInfo.email,
		firstName: loginInfo.firstName,
		lastName: loginInfo.lastName,
		userId: loginInfo.userId,
		type: 1,
		selected: true
	};

    trace.success();
}

// When a user clicks a task in time view
my.badgeClick = function(taskList, ev) {
    GWT.activity();
    $model.get('app').dialog.allowNextPopup = true;
    
    // backup the task to an original state (to revert if cancel is clicked)  
    $model.get('app').showDialog('calendarTaskList', taskList, ev, function(result) {      
        switch (result) {
            case 'close':
              // console.log('close');
              break;
        }
    });
}
my.addComment = function(task) {
    if (my.txtComment) {
        var data = {
            userTask: task.userTaskId,
            comment: my.txtComment
        };
        $appEvent('remote:appTaskList:addComment', data);
        my.txtComment = '';
    }
}
my.addCommentCallback = function(data) {
    data.UserTaskComment.userFirstName = $model.get('app').login.firstName;
    data.UserTaskComment.userLastName = $model.get('app').login.lastName;
    data.UserTaskComment.fromCurrentUser = true;
    data.UserTaskComment.userTaskId = data.UserTaskComment.userTask.userTaskId
    my.comments.push(data.UserTaskComment);
}
// Adds meta data to each task item...
 my.addMeta = function(task) {
     try {

         //"YYYY-MM-DDTHH:mm:ss".length
        if (task.dueDateTime.length === 19) {
            task.dueDateTime = task.dueDateTime  + my.localTimeZone;
        }

        due = moment(task.dueDateTime);

         // Define functions used in the meta object below (outline to reduce memory / increase speed)...
         var meta = {
             tipType:    function() { return (my.isSystem(task)) ? 'System task' : 'User task'; },
             iconType:   function() { return (my.isSystem(task)) ? 'task-system' : 'task-user'; },

             dueRange:   function() {
                 if (typeof task.dueDateTime === 'undefined') return 'none';

                 if ( due.isBefore(my.refDate.today) ) return 'past';
                 if ( due.isBefore(my.refDate.tomorrow) ) return 'today';
                 if ( due.isBefore(my.refDate.dayAfterTomorrow) ) return 'tomorrow';
                 if ( due.isBetween(my.refDate.today, my.refDate.nextWeek) ) return 'week';
                 return 'far';
             },
             iconDue:    function() {
                 // Complete exception...
                 if (task.taskProgressStatusId.taskProgressStatusId === 3) return 'due-complete';
                 if (task.taskProgressStatusId.taskProgressStatusId === 4) return 'due-complete';
                 if ( due.isBefore(my.refDate.today) ) return 'due-late';
                 if ( due.isBetween(my.refDate.today, my.refDate.nextWeek) ) return 'due-close';
                 if ( due.isAfter(my.refDate.nextWeek) ) return 'due-ok';
                 // This plugs the missing due date...
                 return 'due-close';
             },
             hoverTip:   function() {
                 // Complete exception...
                 if (task.taskProgressStatusId.taskProgressStatusId === 3) return 'Completed';
                 if (task.taskProgressStatusId.taskProgressStatusId === 4) return 'Canceled';
                 if ( due.isBefore(my.refDate.today) ) return 'Past due';
                 if ( due.isBefore(my.refDate.tomorrow) ) return 'Due today';
                 if ( due.isBefore(my.refDate.dayAfterTomorrow) ) return 'Due tomorrow';
                 return 'Due ' + moment(task.dueDateTime).add(1, 'days').fromNow();
             }
         };

         // Add the meta data to the task...
         task.meta = {
             tip:    {
                 due:    moment(task.dueDateTime).fromNow(),
                 type:   meta.tipType()
             },
             hoverTip:   meta.hoverTip(),
             color:      function() {
                     if(typeof task.taskProgressStatusId === 'undefined')
                             return '#000000';
                     else
                             return my.filter[ task.taskProgressStatusId.taskProgressStatusId ].color;
             },
             show:      function() {
                     if(typeof task.taskProgressStatusId === 'undefined')
                             return false;
                     else
                             return my.filter[ task.taskProgressStatusId.taskProgressStatusId ].show;
             },
             icon:   {
                 due:    meta.iconDue(),
                 type:   meta.iconType()
             },
             due:        meta.dueRange()
         };

		 task.title = task.userTaskTitle;
		 task.startsAt = new Date(task.dueDateTime);
         // task.endsAt = new Date(task.startsAt);
         // task.endsAt.setMinutes(task.startsAt.getMinutes() + 30);

         switch (task.taskProgressStatusId.taskProgressStatusId) {
            case 1:
                task.type = 'info';
                break;
            case 2:
                task.type = 'warning';
                break;
            case 3:
                task.type = 'success';
                break;
            case 4:
                task.type = 'cancel';
                break;
            default:
                task.type = 'unknow';
         }

		 //task.type = 'info';
		 task.editable = true;
		 task.draggable = true;
		 task.resizable = false;

         my.listHeading.forEach(function(heading) {
             if(heading.name === task.meta.due) heading.empty = false;
         });
     } catch(e) { if(task!==false) $log.warn('error adding metadata', e, task); }
 };

my.changeUserView = function(data) {
    console.log(data);
    if (data.selectedView.type === constants.VIEW_TYPE.MY_VIEW) {
        taskBadgeFilter.set("noBadgeFilter", data.selectedView, undefined, constants.VIEW_TYPE.MY_VIEW);
        console.log(data.selectedView);
    } else {
        if (data.subordinates.size() === 1) {
            taskBadgeFilter.set("noBadgeFilter", data.subordinates[0], undefined, constants.VIEW_TYPE.TEAM_VIEW_ONE);
        } else if(data.subordinates.size() < $model.get('app').subordinates.size()) {
            taskBadgeFilter.set("noBadgeFilter", data.subordinates[0], undefined, constants.VIEW_TYPE.TEAM_VIEW_SOME);
        }
        else {
            var tmp = angular.copy(data.selectedView);
            tmp.email = data.subordinates[0].email;
            for(var i = 1; i< data.subordinates.length; i++) {
                tmp.email += ";" + data.subordinates[i].email;
            }

            taskBadgeFilter.set("noBadgeFilter", tmp, undefined, constants.VIEW_TYPE.TEAM_VIEW);
        }
    }

    var resetTaskStatusFilter = function() {
        my.filter[1].show=true;
        my.filter[2].show=true;
        my.filter[3].show=true;
        my.filter[4].show=false;
    };
    resetTaskStatusFilter();

    my.loadTasks('remote:appTaskList:boot', {email: my.taskBadgeData.email ? my.taskBadgeData.email : my.loginedUser.email});
};

// returns true if the user is consent to the task...
my.checkConsent = function(task) { 
	QA.app.event('remote:appTaskList:checkConsent', task);
};


my.checkingCons = function(data) {
	my.isConsent = data;
};

my.clearTaskBadgeFilter = function(ev) {
    my.currentView = angular.copy($model.get("app").defaultUserViews[0]);
    my.switchSubordinate(my.loginedUser, ev);
};

my.deleteComment = function(comment) {
    $appEvent('remote:appTaskList:deleteComment', {id: comment.userTaskCommentId});
};
my.deleteCommentCallback = function(id) {
    my.comments = my.comments.filter(function(comment) {
        return comment.userTaskCommentId !== id;
    });
}
my.toggleEditComment = function(comment) {
    comment.editMode = !comment.editMode;
}
my.editCommentCallback = function(data) {
    my.currentCommentActive.lastUpdateDateTime = data.UserTaskComment.lastUpdateDateTime;
    my.currentCommentActive.txtComment = my.currentCommentActive.txtComment.trim();
    my.currentCommentActive.comment = data.UserTaskComment.comment;
    my.currentCommentActive.editMode = false;
}
my.getTaskList = function(data) {
    my.tasks = data;
    my.isLoadTaskError = false;
    my.tasksMetadata();
    $timeout(function() {
        $model.scope('task').$broadcast('refreshCalendar');
    });  
}
my.isConsent = true;
// Returns true if the task is a patient task...
my.isPatient = function(task) { return (typeof task.patientId === 'undefined') ? false : true; };
// returns true if the task is a system task...
my.isSystem = function(task) { return (typeof task.systemTaskId === 'undefined') ? false : true; };
// Loads the careplan for the task...
my.loadCarePlan = function(task) {
    var url = location.href.substring(0, location.href.lastIndexOf("/") + 1);
    $log.debug(task);
    GWT.openAppWindow(url + 'login/post_for_treat.jsp?taskId='+ task.sendingSystemTaskId,$model.get("app").login.treatName, $model.get("app").login.treatFeatures);
};

my.loadTaskError = function() {
    my.isLoadTaskError = true;
};
my.loadTaskSuccess = function() {
    my.showProgress = false;
    my.isLoading = false;
};
// Create a task...
my.new = function(ev) {
	GWT.activity();

	var task = {
		taskCategoryId: 1,
		userTaskTitle: '',
		userTaskDescr: '',
		taskAction: '',
		dueDateTime: new Date(),
		creationDateTime: my.SQLdate(),
		lastUdateDateTime: my.SQLdate(),
		taskTypeId: my.types === undefined || my.types.length === 0 ? undefined : my.types[0],
		taskProgressStatusId: { taskProgressStatusId: 1 }
		//email: my.taskBadgeFilter.data ? my.taskBadgeFilter.data.email : undefined
	};
	// Reset the patient search...
	my.patientSearch.selectedItem = null;
	my.patientSearch.searchText = null;


	my.orgCareteamUserSearch.selectedItem = {};

	var isSelected = my.taskBadgeData.email && !my.taskBadgeData.email.contains(";") ;
	my.orgCareteamUserSearch.selectedItem.email = isSelected ? my.taskBadgeData.email : my.loginedUser.email;
	my.orgCareteamUserSearch.selectedItem.firstName =isSelected ? my.taskBadgeData.firstName : my.loginedUser.firstName;
    my.orgCareteamUserSearch.selectedItem.lastName = isSelected ? my.taskBadgeData.lastName : my.loginedUser.lastName;

	my.orgCareteamUserSearch.searchText = null;
	$model.get('app').showDialog('appTaskNew', task, ev, function(result) {
		switch (result) {
			case 'ok':
                $log.debug(task);
				task.dueDateTime = my.SQLdate(task.dueDateTime);
				// Set the patientId...
				if (my.patientSearch.selectedItem)
					task.patientId = {patientId: my.patientSearch.selectedItem.patientId};
				else {
					delete(task.patientId);
				}
				my.setAssignTaskFields(task);
                my.setLastUpdateFields(task);
				// We're set, create a new update date/time and append metadata...
				//my.tempTask = data;
				QA.app.event('remote:appTaskList:new', task);
				break;
			case 'cancel':
				break;
		}
	});
};

// Called from the server when a new task creation finishes...
my.newResponse = function(data) {

    $appEvent('remote:appTaskList:taskFilter', {taskType: my.taskBadgeData.taskType, email: my.taskBadgeData.email, interval: $model.get('home').currentDatePicker.index, callFuncName : 'addTaskToList', task: data.UserTask});
};

my.addTaskToList = function(data) {
	// Add the task data to the list...
    $log.debug('addTaskToList', data, data.Map.isInList);
    if (data.Map.isInList === true) {
        if(data.Map.task !== undefined) {
    		$log.debug('Adding task:', data.Map.task);
            my.tasks.push(data.Map.task);
    		my.addMeta(my.tasks[my.tasks.length - 1]);
            $model.scope('task').$broadcast('refreshCalendar');
    	}
    }
    $model.get('home').taskApi.refresh($model.get('home').currentMember.type === 2);
    //my.tempTask = undefined;
};

// The patient search handler..
my.orgCareteamUserSearch = {
    selectedItem: {},
    searchText: null,
    promise: null,
    query: function(query) {
        selectedItem = {};
        // Setup a promise...
        my.orgCareteamUserSearch.promise = $q.defer();
        // Send the search event...
        QA.app.event('remote:appTaskList:searchOrgCareteamUser', query);
        //return my.userItems;
        return my.orgCareteamUserSearch.promise.promise;
    }
};
// Called from the patientSearch event...
my.orgCareteamUserSearchResolve = function(data) {
    if (my.orgCareteamUserSearch.searchText === data.query) {
        my.orgCareteamUserSearch.promise.resolve( data.response );
    }
};
// The patient search handler..
my.patientSearch = {
    selectedItem: null,
    searchText: null,
    promise: null,
    query: function(query) {
        // Setup a promise...
        my.patientSearch.promise = $q.defer();
        // Send the search event...
        QA.app.event('remote:appTaskList:patientSearch', query);
        return my.patientSearch.promise.promise;
    }
};
// Refresh tasks...
my.refresh = function(ev) {
	GWT.activity();
	// Request data models...

	if (taskBadgeFilter.data.isFiltered === true) {
		my.loadTasks('remote:appTaskList:loadTasksFromBadges', taskBadgeFilter.data);
	} else if (taskBadgeFilter.data.supervisorFlag === true) {
		my.loadTasks('remote:appTaskList:boot', {email: my.taskBadgeData.email ? my.taskBadgeData.email : my.loginedUser.email});
	} else {
		my.loadTasks('remote:appTaskList:boot');
	}
};

// Called from the patientSearch event...
my.searching = function (data) {
    if (my.patientSearch.searchText == data.query) {
        my.patientSearch.promise.resolve(data.response);
    }
};
my.selectedPatientChange = function(item) {
    $log.debug('selected patient change:', item);
    my.minorWarning = undefined;
    if(item && item.dob) {
        QA.app.event('remote:appTaskList:checkMinor', item.dob);
    }
}
my.setAssignTaskFields = function(task) {
    var newAssignTo = my.orgCareteamUserSearch.selectedItem;

    if (newAssignTo && newAssignTo.email) {
        if (newAssignTo.email !== task.userEmailId) {
            task.assignedByEmailId = my.loginedUser.email;
        }
        $log.debug('Assigned by user:', task.assignedByEmailId, task.assignedByFirstName, task.assignedByLastName);

        task.userEmailId = newAssignTo.email;
    } else if (!task.userEmailId) {
        task.userEmailId = my.taskBadgeData.email && !my.taskBadgeData.email.contains(";") ? my.taskBadgeData.email : my.loginedUser.email;
    }

    $log.debug('Assigned user:', task.userEmailId, task.userFirstName, task.userLastName);
}
my.setLastUpdateFields = function(task) {
	var loginedUser = $model.get('app').login;
	task.lastUpdatedByEmailId = loginedUser.email;
	$log.debug('Last update user:', task.lastUpdatedByEmailId);
}
// The times are in 'YYYY/MM/DD HH:mm:ss' format, but we need to account for timezones and other stuff, so do it all in one spot...
my.SQLdate = function(time) {
    // if (typeof time === 'undefined') return moment(Date.now()).format("YYYY-MM-DDTHH:mm:ss");
    // else return moment(time).format("YYYY-MM-DDTHH:mm:ss");
    if (typeof time === 'undefined') return moment(Date.now()).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
    else return moment(time).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
};
// What's called when an item is dropped in status view...
my.statusChange = function(item, status){
    // don't do anything for the weird bug when nothing gets passed to this (weird rare/browser/angular/material bug)...
    if (item === null) return;
    // don't do anything if it's been dropped back into the same card...
    if (item.taskProgressStatusId.taskProgressStatusId === status) return;
    // Change the status type...
    item.taskProgressStatusId.taskProgressStatusId = status;
    // Update the task...
	my.update(item);
};
// switch user
my.switchSubordinate = function(user, ev) {
    taskBadgeFilter.set("noBadgeFilter", user, undefined, user.email.contains(";") ? true : false);
    console.log("Switch to user: " + user.email);
    my.filter[1].show=true;
    my.filter[2].show=true;
    my.filter[3].show=true;
    my.filter[4].show=false;

    my.loadTasks('remote:appTaskList:boot', {email: my.taskBadgeData.email ? my.taskBadgeData.email : my.loginedUser.email});
}
my.switchToTaskBadgeFilterMode = function() {
    my.filter[1].show=true;
    my.filter[2].show=true;
    my.filter[3].show=false;
    my.filter[4].show=false;

    my.currentView.type = -1;

    if (taskBadgeFilter.data.badgeValue.tasks[0].length !==0)
        my.loadTasks('remote:appTaskList:loadTasksFromBadges', taskBadgeFilter.data);
    else {
        my.tasks = [];
        $timeout(function() {
            $model.scope('task').$broadcast('refreshCalendar');
        });
    }
}

my.tasksMetadata = function() {
        var start = new Date();      
        // Clear the headings in list view (say they are empty by default; we don't know what the new meadata will do)...
        my.listHeading.forEach(function(heading) { heading.empty = true; });
        
        // Go through each task and append metadata to it...
        my.tasks.forEach(my.addMeta);
        
        // For testing, record how long it took... can we get this under 100ms for 500 tasks?
        var time = new Date() - start;
        console.log('Task sorting took', time + 'ms');    
};
// Update a task...
//copied to gsiTaskItem directive
//TODO: still called from my.statusChange consider removing this when drag&drop is ready
my.update = function(task) {
    task.lastUpdateDateTime = my.SQLdate();
    my.addMeta(task);
    my.tempTasks['' + task.userTaskId] = task;
    var sendItem = angular.copy(task);
    my.modifiedTask = angular.copy(task);
    delete sendItem['$$hashKey'];
    delete sendItem.meta;
    console.log('Start update task', sendItem);
    QA.app.event('remote:appTaskList:update', sendItem);
};
my.updateComment = function (comment) {
    if (comment.txtComment.trim().length>0) {
        var data = {
            commentId: comment.userTaskCommentId,
            commentInfo: {
                UserTaskComment: {
                    userTask: comment.userTaskId,
                    userTaskCommentId: comment.userTaskCommentId,
                    communityId: comment.communityId,
                    userEmailId: comment.userEmailId,
                    comment: comment.txtComment.trim(),
                    creationDateTime: comment.creationDateTime
                }
            }
        }
        my.currentCommentActive = comment;
        $appEvent('remote:appTaskList:editComment', data);
    }
};
// Called when an update finishes...
my.updateResponse = function(taskId) {
	//$appEvent('remote:appHome:loadTaskCount', {interval : $model.get('home').currentDatePicker.index, email: $model.get('home').currentMember.email});
    console.log('updateResponse');
    $appEvent('remote:appTaskList:taskFilter', {taskType: my.taskBadgeData.taskType, email: my.taskBadgeData.email, interval: $model.get('home').currentDatePicker.index, callFuncName : 'updateTaskInList', task : my.tempTasks['' + taskId]});
};

my.updateTaskInList = function(data) {

    console.log('updateTaskInList', data.Map.isInList);

    if (data.Map.isInList === false) {
    	var removedTaskId = data.Map.task ? data.Map.task.userTaskId : undefined;
        if (removedTaskId !== undefined  && my.tempTasks['' + removedTaskId] !== undefined) {
            var removedTaskIdx =  my.tasks.indexOf(my.tempTasks['' + removedTaskId]);
            console.log('Removing task:', removedTaskId);
            my.tasks.splice(removedTaskIdx, 1);
            delete my.tempTasks['' + removedTaskId];
        }

    }
    else if (data.Map.isInList === true) {
        //console.log('updateTaskInList2', data.Map.isInList, data.Map.task);
        var updatedTaskId = data.Map.task ? data.Map.task.userTaskId : undefined;
        if (updatedTaskId !== undefined  && my.tempTasks['' + updatedTaskId] !== undefined) {
            var updatedTaskIdx = my.tasks.indexOf(my.tempTasks['' + updatedTaskId]);
            console.log('Updating task:', updatedTaskId);
            my.addMeta(data.Map.task);
            my.tasks[updatedTaskIdx] = data.Map.task;
        }

    }
    $model.scope('task').$broadcast('refreshCalendar');
    $model.get('home').taskApi.refresh($model.get('home').currentMember.type === 2);
};

//called from monthly view

// When a user clicks a task in time view
my.timeClick = function( task ) {
    console.log ('task clicked in time view', task)

    GWT.activity();
    // backup the task to an original state (to revert if cancel is clicked)
    var originalTask = {};
    angular.copy(task, originalTask);
    task.dueDateTime = moment(task.dueDateTime).toDate();

    $model.get('app').showDialog('appTaskDetails', task, {}, function(result) {
        switch (result) {
            case 'ok':
              //task.dueDateTime = task.dueDateTime.toISOString().slice(0, 19);
              task.dueDateTime = my.SQLdate(task.dueDateTime);
              my.update(task);
              break;
            case 'cancel':
              angular.copy(originalTask, task);
              break;
        }
    });
};
});app.controller('AsideController-MyPatientList', function($model, $scope, $appEvent, $log, $timeout, $mdPanel,$window,$mdDialog,$element) {var my = this;$model.store('myPatientList', $scope);if (typeof boot == 'function') boot();// Called on boot...
function boot() {

     my.backUpData=[];
    var tagArray=[];
    my.tagEnabled=[false,false,false,false,false];



        /* size... Number of patients per page( = scroll visually)
        * Note that the logic loads two pages, previous+current pages or current+next pages
        * depending on which index of the list user is looking at.*/
    var size = 15,
        /* debounce ... Millisecond pause between user scroll and data fetch */
        debounce = 300;

    $model.get('app').globalTrace.did('mypatientlist boot');
    $model.get('app').myPatientListInit = true;



    my.listTypes = [{
        name:'My Patient Panel',
        value:'user'
    }];

    if($model.get('app').login.canViewOrgPP){
        my.listTypes.push({
            name:'My Org Patients',
            value:'org'
        });
    }
// &&app.login.canViewOrgPP===true

    my.selectedTags = [];
    my.nullTagSelected = false;
    my.listType='my:user';
    my.selectedMenuId=null;

    my.listTypeNoOption = "My Patient Panel";

    $appEvent('local:myPatientList:open');
    $appEvent('remote:myPatientList:tags');



    function unselectAllTags(tags) {
        for(var i=0;i<tags.length;i++){
            tags[i].selected=false;
        }
    };
    my.clearAllTagFilter = function () {
      if(my.tags) unselectAllTags(my.tags);
      my.selectedTags=[];
      my.nullTagSelected = false;
      reloadListSearch();
    };

    my.toggleSelectedTag = function (tag) {
        if(tag.selected){
            my.nullTagSelected=false;
        }
        reloadListSearch();
    };
    my.toggleNoTags = function () {
        if(my.nullTagSelected){
            if(my.tags) unselectAllTags(my.tags);
            my.selectedTags=[];
        }
        reloadListSearch();
    };

    my.onListTypeChange = function () {
        reloadListSearch();
    };



    function reloadListSearch(){
        my.dynamicPatientList.reload();
    }


    /* Private function which parses selected tags */
    function getSelectedTagArray() {
        var tagArr = [];
        if(my.nullTagSelected) tagArr= ['null'];
        else if(my.tags){
            for(var i=0;i<my.tags.length;i++){
                if(my.tags[i].selected)
                    tagArr.push(my.tags[i].tagId);
            }
        }
        my.tagFilterActive = tagArr.length>0;
        return tagArr;
    }

    my.ppTimestamp=null;
    function getPayload(page,size,pCount,getCount){
        var typeArr = my.listType.split(':');
        return {
            page:page,
            size:size,
            mode:typeArr[0],
            type:typeArr[1],
            tags: getSelectedTagArray(),
            search:my.searchString,
            timestamp: my.ppTimestamp,
            pCount:pCount||1,
            getCount:!!getCount
        };
    }
var searchTimeoutPromise;
    my.keywordSearch = function(){
        if(searchTimeoutPromise){
            $timeout.cancel(searchTimeoutPromise);
        }
        reloadListSearch();
    };
    my.resetSearch = function () {
        my.searchString = null;
        if(searchTimeoutPromise){
            $timeout.cancel(searchTimeoutPromise);
        }
        reloadListSearch();
    };
    my.onKeywordChange = function(){

        var delay = 2000;
        if(!my.searchString||my.searchString.length>=3){
            delay = 1000;
        }
        if(searchTimeoutPromise){
            $timeout.cancel(searchTimeoutPromise);
        }
        searchTimeoutPromise = $timeout(function () {
                    reloadListSearch();
                    searchTimeoutPromise=null;
        },delay);

    };


    my.getPage = function (page) {
        var payload = getPayload(page-1,my.pageSize);
        $appEvent('remote:myPatientList:get',payload);
    };


    /* === DynamicPatientList implementation for virtual scrolling list=== */

    var DynamicPatientList = function () {

        /** @const {number} Number of items to fetch per request. */
        this.PAGE_SIZE = size;

        /**
         * @type {!Object<?Array>} Data pages, keyed by page number (0-index).
         */
        this.loadedPages = {};
        /** @type {number} Total number of items. */
        this.numItems = 0;

        my.notFound=false;

        /* Generate new timestamp on every search criteria update (tag, type, keyword)
        * so delayed response data does not get rendered after new search criteria has been created. */
        my.ppTimestamp = (new Date()).getTime();
        this.timestamp = my.ppTimestamp;

        /* Get first and second page with total count */
        this.fetchPage_(0,1,true);

    };

    DynamicPatientList.prototype.reload = function () {

        //Duplicated code with constructor... clean up later
        this.loadedPages = {};
        this.numItems = 0;
        my.notFound=false;

        my.ppTimestamp = (new Date()).getTime();
        this.timestamp = my.ppTimestamp;

        // $appEvent('remote:myPatientList:count', getPayload());
        this.fetchPage_(0,1,true);
    };

    /*Required function by Virtual Repeat onDemand*/
    DynamicPatientList.prototype.getItemAtIndex = function(index) {
        var pageNumber = Math.floor(index / this.PAGE_SIZE);
        var page = this.loadedPages[pageNumber];

        if (page) {
            //If the page is already loaded, return
            return page[index % this.PAGE_SIZE];
        } else if (!page && page !== null) {

            /* Find if user is looking at beginning/ending of a page */
            var isEarly = (this.PAGE_SIZE/2)>(index % this.PAGE_SIZE);
            var extraPageNumber = pageNumber + (isEarly?-1:+1);

            /* If computed adjacent page exists AND not loaded, then fetch it too.*/
            if(extraPageNumber>0){
                var extraPage = this.loadedPages[extraPageNumber];
                if(!extraPage&&extraPage!==null){
                    this.requestPage(pageNumber,extraPageNumber,index);
                    return;
                }
            }

            /* Fetch single page */
            this.requestPage(pageNumber, null, index);
        }

    };

    /*Required function by Virtual Repeat onDemand*/
    DynamicPatientList.prototype.getLength = function() {
        return this.numItems;
    };

    /* Cushion function before actually fetching page.
     * Cancel unnecessary requests until user stops scrolling */
    DynamicPatientList.prototype.requestPage = function(pageNumber, extraPage, index) {

        this.requestedPage = pageNumber;
        this.extraRequestedPage = extraPage;
        this.requestedIndex = index;

        var _this = this;

        //If there is queued request, cancel
        if(this.queuedRequest){
            var cancelled = $timeout.cancel(this.queuedRequest);
            // $log.debug(cancelled?"The request was cancelled.":"The request was already processed.");
            this.queuedRequest=null;
        }

        //Queue new request
        this.queuedRequest = $timeout(function (_this) {
                _this.fetchPage_(_this.requestedPage,_this.extraRequestedPage, false, _this.requestedIndex);
                _this.requestedPage = null;
                _this.extraRequestedPage=null;
        }, debounce, true, _this);

    };

    /* Actual function that fetches pages.
    * If getCount is true, we request total count. */
    DynamicPatientList.prototype.fetchPage_ = function(pageNumber,extraNumber,getCount,index) {

        // $log.debug("Fetching: ",pageNumber, extraNumber, "Requested index:", index);

        // Set the page to null so we know it is already being fetched.
        this.loadedPages[pageNumber] = null;

        var payload;
        // If extra page(adjacent page) is requested and the extra page data is not loaded yet, request two pages
        if(extraNumber&&!this.loadedPages[extraNumber]&&this.loadedPages[extraNumber]!==null){
            // Set the page to null so we know it is already being fetched.
            this.loadedPages[extraNumber] = null;
            payload = getPayload(extraNumber<pageNumber?extraNumber:pageNumber, this.PAGE_SIZE, 2, getCount);
        }else{
            payload = getPayload(pageNumber, this.PAGE_SIZE, 1, getCount);
        }

        // Fetch page
        $appEvent('remote:myPatientList:get',payload);

    };

    DynamicPatientList.prototype.processTotalCount = function(totalCount) {
        $log.debug(totalCount);
        this.numItems = totalCount;
    };
    DynamicPatientList.prototype.processNewPage = function (pageNumber,list) {
        $log.debug("Fetched: "+pageNumber);
        this.loadedPages[pageNumber] = list;
         my.backUpData.push(list);

    };



    /* ===================================================================== */

    //
    // // Triggered from server
    // my.processInitList = function () {
    //     my.dynamicPatientList = new DynamicPatientList();
    // };

    // Triggered from server
    my.processNewPage = function (data) {


        if(!data){
            $log.warn("ERROR: processNewPage received null data.");
            return;
        }
        if(data.timestamp!=my.ppTimestamp){
            $log.debug("This data is outdated.",data, data.timestamp);
            return;
        }
        if(data.notFound){
            $log.debug("Result not found");
            my.notFound = true;
            return;
        }

        var list = data.list;
        for(var i = 0 ; i<data.pCount;i++){
            if(list.length<=size){
                // $log.debug(data.page+i,list);
                my.dynamicPatientList.processNewPage(data.page+i,list);
                return;
            }else{
                var _list = list.splice(0,size);
                // $log.debug(data.page+i,_list);
                my.dynamicPatientList.processNewPage(data.page+i,_list);
            }
        }
    };

    // Triggered from server
    my.processTotalCount = function (data) {
        if(!data){
            $log.debug("processTotalCount received null data.");
            my.notFound = true;
        }else{
            my.notFound=false;
            // my.totalCount = data.count;
            my.dynamicPatientList.processTotalCount(data.count);
        }
            //my.dynamicPatientList.processTotalCount(data.count);
    };

    // Triggered from server
    //need to refresh Alert and Encounter chart after Remove Patient
    my.reloadAfterRemoveOrAdd = function () {
        my.reload();
        $model.get('home').reloadPatientPanelView();
    };



    my.reload = function(){
        // my.initialized = false;
        // $appEvent('remote:myPatientList:init', {type:my.listType});
        // my.searchString = null;
        my.selected=2;
        reloadListSearch();
    };

    my.reloadApp = function () {
        $appEvent('local:state:patientPanel');
    };
    my.onError = function (data) {
        $log.warn("myPatientPanel",data);
        my.isError = true;
    };



   //filter The tag Menu  and hide  tags
     my.filterTagsMenu=function(person,availableTags){
        angular.forEach(person,function(value){
         angular.forEach(availableTags,function(tagValue){
            if(tagValue.tagAbbr===value.tagAbbr){
               value.tagEnabled=tagValue.tagEnabled;
            }

         })

        })
}



    //init
    my.dynamicPatientList = new DynamicPatientList();


}

// Moved to userAction.js

var tagMenuConfig = {
    attachTo: angular.element(document.body),
    controller:['$scope','mdPanelRef','$mdPanel','$window',
        function($scope,mdPanelRef,$mdPanel,$window){
            var my = this;
            my._mdPanelRef = mdPanelRef;
            my.close = function(){
                var panelRef = my._mdPanelRef;
                panelRef && panelRef.close().then(function() {
                    panelRef.destroy();
                });
            };

            //Update position on window resize
            function onWinResize() {
                var position = $mdPanel.newPanelPosition()
                    .relativeTo('#MyPatientListTagMenuBtn')
                    .addPanelPosition($mdPanel.xPosition.ALIGN_END, $mdPanel.yPosition.BELOW);
                my._mdPanelRef.updatePosition(position);
            }
            angular.element($window)
                .on('resize',onWinResize);

            //Clean up watcher
            $scope.$on('$destroy', function () {
                angular.element($window)
                    .off('resize',onWinResize);
            });

        }],
    controllerAs: 'tagMenuCtrl',
    templateUrl: 'mplTagMenu.tpl.html',
    panelClass: 'demo-menu-example',
    clickOutsideToClose: true,
    escapeToClose: true,
    focusOnOpen: false,
    zIndex: 60
};

my.showTagMenu = function (ev) {

    var position = $mdPanel.newPanelPosition()
        .relativeTo('#MyPatientListTagMenuBtn')
        .addPanelPosition($mdPanel.xPosition.ALIGN_END, $mdPanel.yPosition.BELOW);

    tagMenuConfig.openFrom = ev;
    tagMenuConfig.position = position;
    tagMenuConfig.locals = {'myPatientList': my};

    $mdPanel.open(tagMenuConfig);

};
var sortMenuConfig = {
    attachTo: angular.element(document.body),
    controller:['$scope','mdPanelRef','$mdPanel','$window','$filter',
        function($scope,mdPanelRef,$mdPanel,$window,$filter){
            var my = this;
            my.myPatientList.sortOptions=[{order:'Ascending',sortBy:'lastName'},{order:'Descending',sortBy:'-lastName'},{order:'Most Recent',sortBy:'recentData'}];
            my._mdPanelRef = mdPanelRef;
            my.close = function(){
                var panelRef = my._mdPanelRef;
                panelRef && panelRef.close().then(function() {
                    panelRef.destroy();
                });
            };
   
            //Update position on window resize
            function onWinResize() {
                var position = $mdPanel.newPanelPosition()
                    .relativeTo('#MyPatientListSortMenuBtn')
                    .addPanelPosition($mdPanel.xPosition.ALIGN_END, $mdPanel.yPosition.BELOW);
                my._mdPanelRef.updatePosition(position);
            }
            angular.element($window)
                .on('resize',onWinResize);

            //Clean up watcher  
            $scope.$on('$destroy', function () {
                angular.element($window)
                    .off('resize',onWinResize);
            });
        
            //sort the patient panel
            my.myPatientList.sortPatientPanel=function(originalData,sortOption,index){
                    if(sortOption==='recentData'){
                        angular.extend(my.myPatientList.dynamicPatientList.loadedPages,my.myPatientList.backUpData)

                }else{

                    angular.forEach(originalData.loadedPages,function(value,key){
                    my.myPatientList.dynamicPatientList.loadedPages[key] = $filter('orderBy')(my.myPatientList.dynamicPatientList.loadedPages[key], sortOption);

                                    })

                }
                  
                 my.myPatientList.selected=parseInt(index);
            }
        }],

    controllerAs: 'sortMenuCtrl',
    templateUrl: 'mplSortMenu.tpl.html',
    panelClass: 'demo-menu-example',
    clickOutsideToClose: true,
    escapeToClose: true,
    focusOnOpen: false,
    zIndex: 60
};


my.selected=2;
 
my.showSortMenu=function(ev){
var position = $mdPanel.newPanelPosition()
        .relativeTo('#MyPatientListSortMenuBtn')
        .addPanelPosition($mdPanel.xPosition.ALIGN_END, $mdPanel.yPosition.BELOW);

    sortMenuConfig.openFrom = ev;
    sortMenuConfig.position = position;
    sortMenuConfig.locals = {'myPatientList': my};

    $mdPanel.open(sortMenuConfig);

}

var ppMenuCtrl = ['$scope','mdPanelRef','$mdPanel','$window',
    function($scope,mdPanelRef,$mdPanel,$window){
        var my = this;
        my._mdPanelRef = mdPanelRef;
        my.close = function(){
            var panelRef = my._mdPanelRef;
            panelRef && panelRef.close().then(function() {
                panelRef.destroy();
            });

        };

        //Copied from appHome
        function displayConfirmation(title, msg, success, cancel){
            var position = $mdPanel.newPanelPosition()
                .absolute()
                .center();

            var confirmCtrl = function (mdPanelRef) {
                var panel = this;
                panel.msg = msg;
                panel.title = title;
                panel._mdPanelRef = mdPanelRef;
                this.close = function() {
                    var panelRef = panel._mdPanelRef;
                    panelRef && panelRef.close().then(function () {
                        panelRef.destroy();
                    });
                };
                this.ok = function () {
                    success();
                    var panelRef = panel._mdPanelRef;
                    panelRef && panelRef.close().then(function () {
                        panelRef.destroy();
                    });

                };
                this.no = function () {
                    cancel();
                    var panelRef = panel._mdPanelRef;
                    panelRef && panelRef.close().then(function () {
                        panelRef.destroy();
                    });
                }
            };

            var config = {
                attachTo: angular.element($element[0]),
                controllerAs: 'panelCtrl',
                controller: confirmCtrl,
                template: '<div id="confirmPopup" class="panel-dialog">' +
                '   <md-toolbar class="panel-title">' +
                '       <div class="md-toolbar-tools">' +
                '           <h2>'+ title + '</h2>' +
                '           <span flex></span>' +
                '           <md-button class="md-icon-button" ng-click="panelCtrl.close()" aria-label="cancel"><md-icon md-svg-icon="close"></md-icon></md-button>' +
                '       </div>' +
                '   </md-toolbar>' +
                '   <div class="panel-content">' +
                '       <p>' + msg + '</p>' +
                '   </div>' +
                '   <div class="panel-action">' +
                '       <md-button id="btnConfirmOk" ng-click="panelCtrl.ok()">Yes</md-button>' +
                '       <md-button id="btnConfirmNo" ng-click="panelCtrl.no()" md-autofocus>No</md-button>' +
                '   </div>' +
                '</div>',
                disableParentScroll: true,
                hasBackdrop: true,
                panelClass: 'mpl-confirm-dialog',
                position: position,
                trapFocus: true,
                zIndex: 250,
                clickOutsideToClose: true,
                escapeToClose: true,
                focusOnOpen: true
            };

            $mdPanel.open(config);
        }


        my.openCarebook = function (patientId, panelRef) {
            $appEvent('local:patientDrillDown:openCarebook', patientId);
            my.close();

        };
        my.openEdit = function (patientId) {
            $appEvent('local:patientDrillDown:edit',patientId);
            my.close();
        };
        my.openCareplan = function (patientId) {
            $appEvent('local:patientDrillDown:openCarePlanDirect', patientId);
            my.close();
        };

        //TODO: copied from appHome
        my.removeFromList  = function (patient) {
            my.close();
            var title = 'Confirmation';
            var defaultMsg = 'Remove '+ patient.lastname + ', ' + patient.firstname + ' from My Patient Panel?';
            var msg = my.myPatientList.getMessageBundle(defaultMsg, "popup.confirmation.remove.msg", function (configMsg) {
                return configMsg.replace('[0]', patient.lastName + ', ').replace('[1]', patient.firstName);
            });
            var success = function () {
                $appEvent('remote:myPatientList:remove',{patientId: patient.patientId});
            };
            displayConfirmation(title, msg, success, function(){});

        };

        //Update position on window resize
        function onWinResize() {
            var position = $mdPanel.newPanelPosition()
                .relativeTo('#PPMenuBtn_'+my.index)
                .addPanelPosition($mdPanel.xPosition.ALIGN_END, $mdPanel.yPosition.ALIGN_TOPS);
            my._mdPanelRef.updatePosition(position);
        }
        angular.element($window)
            .on('resize',onWinResize);

        //Clean up watcher
        $scope.$on('$destroy', function () {
            angular.element($window)
                .off('resize',onWinResize);
        });

}];

var ppMenuConfig = {
    attachTo: angular.element(document.body),
    controller:ppMenuCtrl,
    controllerAs: 'vm',
    templateUrl: 'ppMenu.tpl.html',
    panelClass:'mpl-item-menu',
    clickOutsideToClose: true,
    escapeToClose: true,
    focusOnOpen: false,
    zIndex: 60,
    onRemoving: function () {
        my.selectedMenuId=null;
    }
};

my.clickMenuOpen = function (ev, patient, index) {

    my.selectedMenuId = patient.patientId;

    var position = $mdPanel.newPanelPosition()
        .relativeTo('#PPMenuBtn_'+index)
        .addPanelPosition($mdPanel.xPosition.ALIGN_END, $mdPanel.yPosition.ALIGN_TOPS);

    ppMenuConfig.openFrom = ev;
    ppMenuConfig.position = position;
    ppMenuConfig.locals = {'patient': patient,
        'index':index,
        'myPatientList':my};

    $mdPanel.open(ppMenuConfig);

};

//Copied from my.popupMenu.js


my.getMessageBundle = function (defaultMsg, msgKey, replaceFunc) {
    if(my.messageBundle && (my.messageBundle[msgKey])){
        return replaceFunc(my.messageBundle[msgKey]);
    } else {
        $log.debug('Cannot get messageBundle with key: '+ msgKey);
        return defaultMsg;
    }

};

});app.controller('adminAppController', function($model, $scope, $appEvent, $log, $timeout, $mdPanel,$window,$mdDialog,$element) {var my = this;$model.store('adminApp', $scope);if (typeof boot == 'function') boot();function boot() {


}

my.showButton=function(){

	console.log('ho kya raha hai ye');
}



});app.controller('AppController', function($mdSidenav, $mdDialog, $appEvent, $mdToast, $http, $scope, $mdMedia, $timeout, $compile, socket, $rootScope, $model, $log, $trace, gsiSideNav, $window, taskBadgeFilter, $interval, $location, $mdPanel, $window) {var my = this;$model.store('app', $scope);if (typeof boot == 'function') boot();/**
 * @kind function
 * @name boot
 *
 * @description
 * This private boot function is the first thing that gets called when application is initialized.
 * Any independent app setups can be done here.
 * */
function boot() {
    // Finish the boot trace...
    QA.trace.finish('system.boot');

    // Start an app.boot trace...
    QA.trace.add('app.boot');

        window.QA.app = my;
        window.QA.app.scope = function() { return $scope };

		// Hoist the app settings...
        my.settings = client.settings;
		console.log(my.settings)
		// Hoist the tenant settings based on the host...

		// Common Login - Find the correct tenant based on the URL...
		var host = window.location.hostname.toLowerCase();
		my.settings.tenant = my.settings.hosts[host].id;

		// Defaults...
		my.settings.apps.login.background = 'assets/tenant/' + my.settings.tenant + '/login-bg'
		my.settings.apps.login.logo = 'assets/tenant/' + my.settings.tenant + '/main-logo.png'
		my.settings.components.titleBar.logo = 'assets/tenant/' + my.settings.tenant + '/nav-logo.png'

		// Get the tenant settings and layer them...
		$http.get(my.settings.components.app.cdn + 'assets/tenant/' + my.settings.tenant + '/settings.json').success(function(data) {
			$.extend(true, my.settings, data);
			// app.login has some special features...

			// If no background name is defined, use the default expected name...
			if (typeof my.settings.apps.login.background === 'undefined') my.settings.apps.login.background = 'assets/tenant/' + my.settings.tenant + '/login-bg'

			// If more than one background to choose from)...
			if (typeof my.settings.apps.login.backgrounds !== 'undefined') {
				if (my.settings.apps.login.backgrounds > 0)
					my.settings.apps.login.background = my.settings.apps.login.background + Math.floor(Math.random() * (my.settings.apps.login.backgrounds) + 1);
			}

			// If quotes exist, randomize them and place them in the tagLine...
			if (typeof my.settings.apps.login.text.quotes !== 'undefined') {
				my.settings.apps.login.text.tagLine = my.settings.apps.login.text.quotes[Math.floor(Math.random() * my.settings.apps.login.text.quotes.length)]
			}
			$appEvent('remote:login:setting', {settings: my.settings});
		});


    QA.trace.log('app.boot', 'settings');

    my.$trace = $trace;

    //Adding for IE performance debug...
    my.globalTrace = new $trace('global');
    // Not logged in by default...
    my.isGWTCheckingToken = true;
    my.loggedIn = false;
    // Not in devmode by default...
    my.devmode = false;
    // Get the build version...
    my.build = window.QA.build;

    my.reduxReady = false;


    my.closeMenu=function(){

    angular.element(document.querySelector('#menu_container_3')).hide();
    angular.element(document.getElementsByTagName('md-backdrop')).remove();
    angular.element(document.querySelector('.md-scroll-mask')).removeClass('md-scroll-mask');


      } 

    // App logger...
    my.log = function( msg ) { console.log(msg); };
    // For determining the media size in the template...
    my.media = function(e) { return $mdMedia(e); };


    my.nodeDefault = {
        version: '',
        status: 'connecting...',
        connected: false
    };
    my.node = my.nodeDefault;

    // Build dialog model...
    my.dialog = {data: {}, choice: null};


    // Let the QA object touch the global model for testing/debugging...
    my.model = $model;
    my.$scope = $scope;
    // Hoist the generated eventServer...
    my.events = {push: {}, local: {}};

    my.$window = $window;
    my.openGlobalSearch = false;
    my.toggleClass = '';

    QA.trace.log('app.boot', 'vars');

    // Store the routers and JS payload...
    localStorage.setItem('cache.router.push',		JSON.stringify( {version: '1.0.1', data: window.client.routers.push }) );
    localStorage.setItem('cache.router.client',		JSON.stringify( {version: '1.0.1', data: window.client.routers.client }) );
    localStorage.setItem('cache.app.js',			JSON.stringify( {version: '1.0.1', data: 'empty' }) );
    localStorage.setItem('cache.app.css',			JSON.stringify( {version: '1.0.1', data: 'empty' }) );
    localStorage.setItem('cache.app.graphics',		JSON.stringify( {version: '1.0.1', data: 'empty' }) );
    localStorage.setItem('cache.app.states',		JSON.stringify( {version: '1.0.1', data: 'empty' }) );
    localStorage.setItem('cache.app.user',			JSON.stringify( {version: '1.0.1', data: 'empty' }) );
    localStorage.setItem('cache.app.log',			JSON.stringify( {version: '1.0.1', data: 'empty' }) );
    localStorage.setItem('cache.app.state',			JSON.stringify( {version: '1.0.1', data: 'empty' }) );

    QA.trace.log('app.boot', 'localStorage');

    // TODO: Store all views using lazy-loading (the server will then hot-swap changes if they happen, this will lead to a usable offline app, and full app caching)...

    // Load the routers...
    // TODO: Get rid of the push router (push routing can be combined with a client router using a single push function for pass through, this will make code 1/3rd cleaner (only 2 routers instead of 3)
    code = 'my.events.local = ' + LZString.decompressFromBase64(window.client.routers.client);
    try { eval( code ); } catch (e) { $log.error('Client router is invalid.', e, code); }

    // TODO: remove this once the routers have been combined...
    code = 'my.events.push = ' + LZString.decompressFromBase64(window.client.routers.push);
    try { eval( code ); } catch (e) { $log.error('Push router is invalid.', e, code); }
}

function getQueryVariable(variable)
{
    var query = $window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if(pair[0] == variable){return pair[1]||true;}
    }
    return(false);
}

//Called when socketConnected
/**
 * @kind function
 * @name my.appBoot
 *
 * @description
 * This appBoot function is called by socketConnected which receives app.settings from Node server.
 * All app configuration dependent on this app.settings should be done in this function.
 * */
my.appBoot = function() { // FIXME: currently gets called everytime socket is reconnected. maybe just call once

    // We get the key and screenName from my.login, everything else (icon and event) from this array...
		//TODO: hasCount value
		my.appDetails = {
				"HOME":								{css: "admin-menu",					name: "appHome"             },
				"GSI_ADMIN_APP":					{css: "admin-menu",					name: "appAdmin"            },
				"GSI_ENROLLMENT_APP":				{css: "enrollments-menu",			name: "appEnrollment"       },
				"GSI_CARETEAM_APP":					{css: "careteam-menu",				name: "appCareteams"        },
				"GSI_REPORTS_APP":					{css: "reports-menu",				name: "appReports"          },
				"TREAT":							{css: "treat-menu",					name: "appCareplan"         ,hasWindow:true},
				"GSI_MESSAGING_APP":				{css: "message-menu",				name: "appMessages"         , hasCount:true, hasWindow:true},
				"GSI_ALERTING_APP":					{css: "alerts-menu",				name: "appAlerts"           , hasCount:true},
				"GSI_TASK_LIST":					{css: "patient-summary-menu",		name: "appTaskList"         },
				"GSI_CAREBOOK_APP":					{css: "care-book-menu",				name: "appCarebook"         },
				"GSI_PATIENT_LIST_UTILITY":			{css: "patient-summary-menu",		name: "appPatientList"      },
				"GSI_CARE_COORDINATION":			{css: "treat-menu",					name: "appCareplan"         },
				"GSI_PATIENT_ENGAGEMENT":			{css: "patient-engagement-menu",	name: "appEngagement"       },
				"GSI_UHC_ENROLLMENT_REPORT_APP":	{css: "enrollments-menu",			name: "appUHCEnrollment"    , hasWindow:true, isPentaho: true},
				"GSI_POPULATION_MANAGER_APP":		{css: "population-manager-menu",	name: "appPopManager"       , hasWindow:true, isPentaho: true},
				"GSI_TASK_MANAGER_APP":				{css: "patient-summary-menu",		name: "appTaskList"          , hasCount:true}
		};

		// Default context menu...
		my.defaultContextMenu = [
		];

		my.userMenu = [
			{ text: 'Preferences',					icon: 'settings',					event: 'local:prefs:show'               ,		elementId: 'btnPrefs'},
			{ text: 'Help',							icon: 'help',						event: 'local:help:show`'               , 		elementId: 'btnHelp'},
			{ text: 'Resource Center',				icon: 'book-open',					event: 'local:resourceCenter:show'      , 		elementId: 'btnResource'}
		];
		my.globalMenu = [
			{ text: 'Search',						icon: 'magnify',					event: 'local:search:global'       		},
			{ text: 'Patient Panel',				icon: 'view-list',					event: 'local:myPatientList:show'       }
		];
		my.contextMenu = my.defaultContextMenu;
		my.appWindows = {};


        my.patientListNav=gsiSideNav.get('PatientList',false);


	// QA.trace.log('app.boot', 'Routers');

    // If we get here, the application is ready to rock...
    my.currentMenuTheme = 'theme-bg-primary';
    QA.trace.finish('app.boot');

    //Rendering completed
        my.loaded = true;

    loadComplete();

    my.consoleLog = function (e) {
        console.log(e);
    }
    //utils

    if(!my.identity||!my.identity.token){ // if identity is null user is not logged in yet; If we could strip key maybe we don't need this
        var otpKey = getQueryVariable('key');
        $location.search('key', null).replace();
        if(otpKey){
            $appEvent("local:login:token",otpKey);
        }else{
            if(!my.loggedIn) $appEvent("local:login:show");
            //This is temporary workaround for common login forgot password
            var forgotPasswordUserName = getQueryVariable('fp');
            if(forgotPasswordUserName){
                var _email = decodeURIComponent(forgotPasswordUserName);
                handleForgotPasswordRedirect(_email);
                $location.search('fp', null).replace();
            }
        }
    }
};

// use QA.app.update() to have Angular refresh for changes...
my.update = function () {
	
};

function handleForgotPasswordRedirect(forgotPasswordEmail) {
    var gwtWatcher = $scope.$watch(function(){return angular.isFunction($window.GWT.runForgotPassword);},function(n){
        if(n){
            gwtWatcher();
            if($window.GWT.ready === true){
                $timeout(function () {
                    $window.GWT.runForgotPassword(forgotPasswordEmail, my.settings.tenant);
                },100);
            }else{
                var _gwtReadyWatcher = $scope.$watch(function(){return my.settings.apps.login.isGwtReady;},function(n){
                    if($window.GWT.ready === true){
                        _gwtReadyWatcher();
                        $timeout(function () {
                            $window.GWT.runForgotPassword(forgotPasswordEmail, my.settings.tenant);
                        },100);
                    }
                });
            }
        }
    });
}

    // (private) Process local events
    function eventLocal(what, where) {
		my.events[what](where);
    }
	// (private) Process local events
    function eventRemote(what, where, data) {
        //console.log('eventRemote', what, where, data);
        var send = {
            payload: data,
            event: where
        }
        socket.emit(what, send);
    }
// Built-in router... eventually break this out into a compiled strategy...
function eventServer(what, data) {
    var firstKey = function(obj) {
		for (var key in obj)
			if (obj.hasOwnProperty(key)) break;
			return key;
    };
    var firstVal = function(obj) {return obj[Object.keys(obj)[0]];};
    
    switch (what) {
        case 's':
            // Decompress and decode the state...
            var c = JSON.parse(LZString.decompressFromBase64(data));
            //console.log(c);
            // CSS...
            if ( c.c ) document.getElementById('appStyle').innerHTML = c.c;
            // HTML (angular code is recompiled with the dynamic $watch directive above)...
            if ( c.h ) {
                // TODO: Find the DOM element it belongs in... (ID)
                if ( c.x ) {
                    if (c.x.dialog) {
                        // Get a list of camel cased variables out of the dialog name...
                        //var args = c.d.replace(/([A-Z])/g, ' $1').split(" ");
                        console.log('DIALOG scope:', c.x.dialog.scope, 'name:', c.x.dialog.name);
                        // The 1st variable should be 'app', 2nd variable is the scope, the 3rd variable is the name...
                        // Draw the actual dialog...
                        my.dialog.scope = c.x.dialog.scope;
                        $mdDialog.show({
                            controller: universalDialogController,
                            template: c.h,
                            parent: angular.element(document.body),
                            targetEvent: my.dialog.sourceElement,
                            clickOutsideToClose: true,
                            fullscreen: true // TODO: Calculate this
                          })
                          .then(function() { my.dialog.callback(my.dialog.choice); });

                        //my.showDialog( { scope: args[1], data: my.dialog.data, template: c.h, sourceElement: my.dialog.sourceElement, callback: my.dialog.callback} );
                    }else {
                        console.log('Compiling', c.x.HTML );
                        var htmlId = c.x.HTML;
                        var ele = document.getElementById(htmlId);
                        ele.innerHTML = c.h;
                        var content = $compile(ele)($scope);
                    } 
                } else {
                    console.log('unknown, Auto-compiling to taskTab');
                    my.html.task = c.h;   
                }
            }
            // javascript can invoke new controllers/directives/services by calling angularize()...
            if ( c.j ) {
                eval( 'window.d = ' + c.j );
                console.log ('controlerized', c.name);
                angularize('controller', c.name, 'appContent', window.d);
            }
            break;
        case 'd':
            // Data change...
            try {
                // Try to parse it, if it's possible...
                data = JSON.parse(data);            
            }
            catch(e) {
                // otherwise, it's already JS data...
            }
            if (data.LoginResult) {
                console.log('Updated Login');
                updateLogin(data.User);
            } else {
                // Update the application model, and through it, the proper respective scope (as long as it's been loaded)...
                var key = firstKey(data);
                var secondKey = firstKey(data[key]);
                console.log('<-SVR DATA:'+ key +'['+ secondKey +']');
                $model.get(key)[secondKey] = data[key][secondKey];
            }
            break;
        case 'f':
        console.log('shouldnt get here');
            // function call...
            try {
                // Try to parse it, if it's possible...
                data = JSON.parse(data);            
            }
            // otherwise, it's already JS data...
            catch(e) {}

            // Get the application model, and through it, the proper respective scope (as long as it's been loaded)...
            var key = firstKey(data);
            var secondKey = firstKey(data[key]);
            console.log('<-SVR CALL:'+ data.scope +':' + data.func + '('+ data.args +')');
            
            // Use the magic model service to call the requested function...
            $model.get(data.scope)[data.func](data.args);
            break;
        case 'a':
            my.note(data);
            break;
        case 'patch':
            switch (data) {
                case 'reboot':
                    location.reload();
                    break;
                case 'css':
                    $('#css').replaceWith('<link id="css" rel="stylesheet" href="/dashboard/app.css?t=' + Date.now() + '"></link>');
                    break;
                case 'state':
                    $appEvent('remote:state:appTaskList');
                    break;
            }
            break;
        case 'dialog':
            break;
        case 'request':
            console.log('Request handled:', data);
            switch(data) {
                case 'login':
                    if (typeof my.login !== 'undefined') $appEvent('remote:data', {type:'login', data: my.login});
                    break;
            }
        case 'debug':
            console.log('Server DEBUG:', data);
    }
}
// // XHR interceptor...
// var s_ajaxListener = new Object();
// s_ajaxListener.tempOpen = XMLHttpRequest.prototype.open;
// s_ajaxListener.tempSend = XMLHttpRequest.prototype.send;
//
// my.callback = function () {
//     var service = my.url.split('/').last();
//     // this.method :the ajax method used
//     // this.url    :the url of the requested script (including query string, if any) (urlencoded)
//     // this.data   :the data sent, if any ex: foo=bar&a=b (urlencoded)
//     // Process intercepted services...
//     switch ( service ) {
//         // When GWT loads the mypatientlistservice, activate...
//         case 'messagecountservice':
//             console.log('Detected GWT login',(new Date()).getTime());
//             //
//             // my.loggedIn = true;
//             // // Must apply changes because this is a callback that wasn't expected...
//             // console.log('applying changes');
//             // $scope.$apply( function() {$appEvent('local:login:get');});
//             break;
//         default:
//             //console.log('Intercepted:', service);
//             break;
//     }
// }
//
// XMLHttpRequest.prototype.open = function(a,b) {
//     if (!a) var a='';
//     if (!b) var b='';
//     s_ajaxListener.tempOpen.apply(this, arguments);
//     //s_ajaxListener.method = a;
//     my.url = b;
//     //if (a.toLowerCase() == 'get') {
// //	s_ajaxListener.data = b.split('?');
// //	s_ajaxListener.data = s_ajaxListener.data[1];
//     // }
// }
//
// XMLHttpRequest.prototype.send = function(a,b) {
//     if (!a) var a='';
//     if (!b) var b='';
//     s_ajaxListener.tempSend.apply(this, arguments);
//     //if(s_ajaxListener.method.toLowerCase() == 'post')s_ajaxListener.data = a;
//     my.callback();
// }
// Fade out the loading screen 2 seconds after the app is loaded for a smooth transition...
function loadComplete() {
//	$timeout(function() {
		my.loaded = true;
		$timeout(function() {
			$('#LoadingScreen').css('display', 'none');
		}, 500);
//	}, 2000);
}
my.can = function (permission) {
  if (my.login.permissions.indexOf(permission) !== -1) return true;
  return false;
};
// Fires when the my.dialog object is ready to be populated ('local:dialog:[name]' was called and pushed a dialog event)... an event, data is optional...
my.showDialog = function(name, data, ev, callback, boot) {
    if (!my.dialog.isOpen) {
        // Transfer the passed data/scope to the app (my) scope... (scope: scope, data: data, template: template, sourceElement: element, callback: callback(choice))
        my.dialog.name = name;
        my.dialog.data = data;
		my.dialog.boot = boot;
        my.dialog.sourceElement = ev;
        my.dialog.callback = callback;
        my.dialog.isOpen = true;
        // Call the correct application state... (the actual dialog gets drawn in eventServer, when the template gets returned as an application (dialog) state)
        $appEvent('local:state:' + name);
    }
    
};
my.displaySubordinateInMenu = function (subordinate) {
	if(subordinate === undefined) {
		return '';
	}
	if(subordinate.type === 1) {
		return 'My Data';
	}
	else if(subordinate.type === 2) {
		return 'My Org’s Data';
	}
	else if (subordinate.type === 3 && subordinate.userId === -3) {
		return 'My Team’s Data';
	}
	else {
		return subordinate.lastName + ', ' + subordinate.firstName + '’s Data';
	}
}

    // Displays an error message...
    my.err = function( msg ) {
        console.log('ERROR:', msg);
    };
// Fires an event, data is optional...
my.event = function(event, data, tracer) {
	// Prevent errors...
	if (typeof tracer === 'undefined') tracer = { log: function() {$log.debug('Tracer: should never be here')} }
	
    // Split the event into its components...
    try {
        var parts = event.split(':');
    } catch(error) {
        my.err('Malformed or missing event name: ' + event);
    }
	var type = parts[0];
	var what = parts[1];
	var where = parts[2];

	switch (type) {
        case 'local':
			//tracer.log('local');
            // Events happening internally (client-client)
            if (what.substring(0,3) === 'app') {
                // TODO: Build the app type into the app list...
				var appType = (what === 'appTaskList') ? 'Angular' : 'GWT';
				$log.debug('<-GUI EVENT-local', what + '(' + where + ') [' + appType + ']');
            } else
                $log.debug('<-GUI EVENT-local', what + '(' + where + ')');
            if(angular.isFunction(my.events.local[what])){
                QA.monitor.event(event);
                my.events.local[what](where, data);
            }else{
                var errmssg = "local."+what+" is not a function.";
                QA.monitor.error(event, errmssg);
                my.err(errmssg);
            }
            break;
        case 'remote':
            // Events happening internally (client-server)
			//tracer.log('remote');
            QA.monitor.event(event);
            debug.info('Remote:' + what + '(' + (where ? where : data) + ')');
			socket.emit(what, { payload: data, event: where });
            break;
        case 'push':
			// $log.debug('push');
			tracer.log('push[' + what + ']');
            // Incoming events from the server (server-client)
            my.events.push[what](where, data, tracer);
            break;
        default:
			//tracer.log('unknown');
            // Unknown event types...
            QA.monitor.error(event, "Unknown event");
            $log.debug('.DIE. EVENT-unknown (' + type + ')', what + '(' + where + ')');
            break;
    }
}
//Created only for logging GWT login
my.loginTimerLog = {logs:[]};
my.GwtLoginTimer = function (msg) {
    var now = new Date();
    switch(msg){
        case 'login_start':
            my.loginTimerLog = {logs:['start']};
            my.loginTimerLog.start = now;
            break;
        case 'login_failure':
            my.loginTimerLog.logs.push('['+(now-my.loginTimerLog.last)+'].error');
            $appEvent('remote:log:gwtLogin', {
                result:false,
                ms:now-my.loginTimerLog.start,
                log:my.loginTimerLog.logs.join('.')
            });
            break;
        case 'login_success':
            my.loginTimerLog.logs.push('['+(now-my.loginTimerLog.last)+'].success');
            my.gwtLoginLog = {
                result:true,
                ms:now-my.loginTimerLog.start,
                log:my.loginTimerLog.logs.join('.')
            };
            break;
        default:
            my.loginTimerLog.logs.push('['+(now-my.loginTimerLog.last)+'].'+msg);

    }
    my.loginTimerLog.last = now;
};
//This will be called by Redux.
//Put everything needs to be run to initialize the application
my.initApp = function () {


    my.globalTrace.did('initApp start');
    // Todo: use state files instead of calling remote events here
    $log.debug('Redux received login object. Initializing application...');
    //patientPanel init
    $appEvent('local:state:patientPanel');

    //appHome init
    $appEvent('local:state:appHome');

    function checkCountUpdate(name){

        var watcher = $scope.$watch(
            function (my,badgeVar) {
                return my["count_"+badgeVar]
            }.bind(null,my,badgeVar),
            function (name,n,o) {
                if(typeof o === 'undefined') return;
                if(o<n){
                    my.showNotifToast(name);
                    watcher();
                }
            }.bind(null,name));
    }

    if(my.settings.apps.login.pentahoEnabled){
        $appEvent('local:login:pentaho');
    }


    // should be called after node is ready
    if(my.countSubscribedApps) {
        //TODO: dirty workaround

        for (var i = 0; i < my.countSubscribedApps.length; i++) {
            if (my.countSubscribedApps[i] == 'appTaskList') continue; // No need to show app task notification yet
            var badgeVar = my.countSubscribedApps[i];
            checkCountUpdate(badgeVar)
        }

        $appEvent('remote:appGlobal:count', my.countSubscribedApps);
    }

    // Timer
    // !! pooling is not mistyped... server object has the misspelling
    if(my.login.poolingEnabled){
        $log.debug("Enabling badge count timer...");
        var countTimer = $interval(function () {
            if (my.countSubscribedApps && my.isAuthenticated === true) $appEvent('remote:appGlobal:count', my.countSubscribedApps);
        }, (my.login.pollingIntervalInSeconds||300)*1000);
    }

    my.globalTrace.did('initApp done');

    if(my.gwtLoginLog){
        $appEvent('remote:log:gwtLogin', my.gwtLoginLog);
    }
    //optional flag
    my.reduxReady = true;


};
my.loadMemberList = function(data) {
    my.subordinates = data;
    my.defaultUserViews = [];

    var user = {};
    user.email = my.login.email;
    user.firstName = my.login.firstName;
    user.lastName = my.login.lastName;
    user.userId = my.login.userId;
    user.type = 1;
    user.selected = true;
    my.defaultUserViews.push(user);

    var org = {};
    org.userId = -2;
    org.type = 2;
    my.defaultUserViews.push(org);

    if ( my.subordinates &&  my.subordinates.length > 0) {
        var team = {};
        team.email = '';
        team.userId = -3;
        team.type = 3;
        my.defaultUserViews.push(team);

        for (var i = 0; i < my.subordinates.length; i++) {
            if(my.subordinates[i].email !== user.email) {
                my.subordinates[i].type = 3;
                team.email = team.email + my.subordinates[i].email + ';';
            }
        }


        $model.get('home').patientsToggle[3] = "My Team View";
    }
    $model.get('home').currentMember = angular.copy(my.defaultUserViews[0]);
    $model.get('home').selectedSubordinates = [];
};

//Used by GWT-Bridge
my.logDebug = function (msg) {
    $log.debug(msg);
};
    my.nodeFill = function( obj ) {
        if (typeof obj === 'undefined') {
            obj =   my.nodeDefault;
        }
        my.node = obj;
        QA.node = my.node;
    };
    // (public) app.note - Displays a brief note to the user
    my.note = function( msg, pos, delay ) {
        if ( !pos	) pos = 'bottom left';
		if ( !delay ) delay = 3000;
		var toast = $mdToast.simple()
            .hideDelay(delay)
			.textContent( msg )
            .action('OK')
            .highlightAction(false)
            .position(pos);

        $mdToast.show(toast).then(function(response) {
            if ( response === 'ok' )
                my.log('clicked note ok button');
        });
    };
//Count models are called count_{{nameOfApp}}
//When a count goes to error state, an object is assigned to the model
my.onCountError = function (data) {
    $log.warn(data.message);
    if(data.model){
        var prevCount = !my[data.model]?"?":
            angular.isObject(my[data.model])?my[data.model].prevValue:my[data.model];
        my[data.model] = {prevValue : prevCount, isError : true};
    }
};
my.onIdentity =function (data) {
    my.identity=data;
    //GWT login
    if(my.identity && my.identity.token){
        var gwtWatcher = $scope.$watch(function(){return angular.isFunction($window.GWT.checkToken);},function(n){
            if(n){
                gwtWatcher();
                if($window.GWT.ready === true){
                    $timeout(function () {
                        $window.GWT.checkToken(my.identity.token);
                    },100);
                }else{
                    gwtReadyWatcher();
                }
            }
        });
    }
};

//Dirty workaround to make sure GWT is ready
function gwtReadyWatcher(){
    var _gwtReadyWatcher = $scope.$watch(function(){return my.settings.apps.login.isGwtReady;},function(n){
        if($window.GWT.ready === true){
            _gwtReadyWatcher();
            $timeout(function () {
                $window.GWT.checkToken(my.identity.token);
            },100);
        }
    });
}

my.onLoginError = function (message) {

    $appEvent('local:login:failed');
    var alert = $mdDialog.alert({
        title: 'Login Failed',
        textContent: message,
        ok:'OK'
    });

    $mdDialog
        .show( alert )
        .finally(function() {
            alert = undefined;
        });
};

my.onTokenSyncError = function (message) {
    //TODO if there is server error handle this differently
    $appEvent('local:session:expired');
};

my.onCommSwitchError = function (message) {

    var alert = $mdDialog.alert({
        title: 'Community Switch Failed',
        textContent: message,
        ok:'OK'
    });

    $mdDialog
        .show( alert )
        .finally(function() {
            alert = undefined;
        });
};


my.postAppBoot = function(data) {
    my.subordinates = data.subordinates;
    
    if (my.subordinates && my.subordinates.length > 0) {
        var user = {};
        user.email = my.login.email;
        user.firstName = my.login.firstName;
        user.lastName = my.login.lastName;
        user.userId = my.login.userId;
        my.subordinates.unshift(user);
        $model.get('home').currentMember = my.subordinates[0];
    }
}
my.resizeWindow = function() {
    $timeout(function() {
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent('resize', true, false);
        window.dispatchEvent(evt);
    }, 500);
}
    // Once Node establishes a connection, it will call this function, giving us status and version information...
    my.socketConnected = function( payload, trace ) {
		payload.status = 'connected';
        payload.connected = true;
        trace.log('calling my.nodeFill()');
        my.nodeFill( payload.node );
        //console.log('Node connection established. Server version:', my.node.version);
        my.isConnected = true;
        // my.settings = payload
        my.appBoot();
    };

    // TODO: deprecated / moved into webSocket.js
    // Once Node establishes a connection, it will call this function, giving us status and version information...
    my.socketDisconnected = function( p ) {
        // Fill the node object with defaults...
        my.nodeFill();
        // Yell we've been disconnected...
        $log.debug('Node disconnected.');
        //my.note('Lost connection to Redux');
        my.isConnected = false;
    };
my.confirmCommunitySwitch = function (tenant) {
    var _this = this;
    if(my.settings.apps.login.switching.showConfirmation){
        var message = my.settings.apps.login.text.switchingMessage,
            title = my.settings.apps.login.text.switchingTitle;
        var alert = $mdDialog.confirm({
            title: title,
            textContent: message,
            ok:'OK',
            cancel: 'Cancel',
            css:'notify-dialog',
            flex:66
        });

        $mdDialog
            .show( alert )
            .then(requestCommunitySwitch.bind(_this, tenant),angular.noop);
    }else{
        requestCommunitySwitch(tenant);
    }
};

function requestCommunitySwitch(tenant) {
    $appEvent('remote:community:switch',tenant.id);
}

my.onSwitchCommunity = function (data) {
    angular.forEach(my.appWindows, function (value) {
        value && angular.isFunction(value.close) && value.close();
    });
    $window.location.href = data.redirect;
};
//Reorganized switchTab flows using events for efficiency... see openTab.js and gsiSideNav for details.

// my.closeTab = function(data, event){
// 	console.log("closeTab " + data);
// 	my.activeApps.splice(my.activeApps.indexOf(data), 1);
// }
//
// my.openTab = function(what, data){
// 	var needToRender = true;
// 	if((my.listTabs.indexOf(what)!= -1 && typeof(data) != 'undefined')) {
//         if(my.activeApps.indexOf(data) === -1){
//             my.activeApps.push(data);
//             needToRender = true;
//             console.log("tab length: " + my.activeApps.length);
//         } else{
//             my.selectedTab=my.activeApps.indexOf(data) + 1;
//             console.log("selected tab: " + my.selectedTab);
//             needToRender = false;
//         }
//
//     }
//     return needToRender;
// }
my.trace = function (msg) {
  my.globalTrace.did(msg);
};
var toastConfig = {
    template:'<md-toast><div class="md-toast-content" layout="row" layout-align="center center" layout-padding>' +
    '<div flex-nogrow><md-icon md-svg-icon="{{ctrl.icon}}"></md-icon></div><div layout="column">' +
    '<div class="notif-date">{{ctrl.date|date:"MM/dd/yyyy h:mm a"}}</div>' +
    '<div class="notif-message">{{ctrl.message}}</div>' +
    '</div></div></md-toast>',
        bindToController:true,
    controllerAs:'ctrl',
    controller:function (){},
    hideDelay:6000,
        toastClass:'notif-toast',
    position:'bottom right'
};
my.showNotifToast = function (type) {
    var message, icon;
    switch(type){
        case 'appMessages':
            message = 'New messages received.';
            icon = 'icon_appMessages';
            break;
        case 'appAlerts':
            message = 'New alerts received.';
            icon = 'icon_appAlerts';
            break;
        // case 'appTaskList':
        //     message = 'New tasks received.';
        //     icon = 'icon_appTaskList';
        //     break;
        default:
            return;
    }
    toastConfig.locals = {message:message, date:new Date(),icon:icon};
    $mdToast.show(toastConfig);
};

// Unused in 5.3... Global Search is only Carebook Patient Search for now...
my.toggleGlobalSearch = function () {
    my.openGlobalSearch = !my.openGlobalSearch;
    
    if (my.openGlobalSearch) {
        my.toggleClass = 'expanded';
        my.globalSearchText = '';
        my.$window.onclick = function (event) {
            closeSearchWhenClickingElsewhere(event, my.toggleGlobalSearch);
        };
    } else {
        my.toggleClass = '';
        my.openGlobalSearch = false;
        my.$window.onclick = null;
        my.$scope.$apply();
    }
};
function closeSearchWhenClickingElsewhere(event, callbackOnClose) {
    var clickedElement = event.target;
    if (!clickedElement) return;

    var elementClasses = clickedElement.classList;
    var clickedOnSearchDrawer = elementClasses.contains('global-search-button') || elementClasses.contains('txt-global-search') ||
        (clickedElement.parentElement !== null && clickedElement.parentElement.classList.contains('global-search-button'));

    if (!clickedOnSearchDrawer) {
        callbackOnClose();
        return;
    }
}
// Apply new user settings to the logged in user...
function updateLogin(data) {
    // Inject the client-side app event data/etc...

   if(my.userAppInitialized){return;}

    my.globalTrace.did('updateLogin() start');
    my.devmode = GWT.isDevmode();
    my.countSubscribedApps = [];

    my.login = data;
    var hasPentaho = false;
    //For monitoring(New Relic)
    QA.monitor.login(data);

    my.login.userApps.forEach( function(item,index) {
       var tmpApp = my.appDetails[item.applicationName.trim()];
       //Starting with 2 (0:appHome, 1:appCareBookSearch)
       item.index=index+2;
       if (typeof tmpApp != 'undefined') {
           item.css = tmpApp.css;
           item.name = tmpApp.name;
           item.event = tmpApp.event?tmpApp.event:"local:openApp:"+tmpApp.name;
           if(tmpApp.hasCount){
               item.count="count_"+tmpApp.name;
               my.countSubscribedApps.push(item.name);
           }
           item.hasWindow = !!tmpApp.hasWindow;
           if(tmpApp.isPentaho){
               hasPentaho = true;
           }
       } else {
           $log.warn("App name could not be identified:",item.applicationName);
           item.css = 'help-icon';
           item.event = 'local:unknown:app';
       }
   });

    if(hasPentaho){
        my.settings.apps.login.pentahoEnabled = true;
    }
    my.userApps = my.login.userApps;

    // Notify Node of the new login data...
    // Changed callback function to app.initApp. Panda's reloadHomePanel is called within app.initApp
    $appEvent('remote:sync', {data: data, callback: {scope : 'app', func : 'initApp', args : undefined}});

    //main menu init
    $appEvent('local:state:mainMenu');


    // Change the home id div...
   //document.getElementById("reduxPanel").innerHTML = 'GWT footer';
   // Bit 1 indicates if Redux is enabled...
   // TODO: This UI selector system is being retired in 5.5...
	   my.ui = {
		   redux:(my.login.userInterfaceType & 1) ? true : false,
		   reduxSelectable: (my.login.userInterfaceType & 512) ? true : false, // TODO: Get rid of reduxSelectable in the view and here (replace with selectable)
		   selectable: (my.login.userInterfaceType & 512) ? true : false
	   };

	   // Allow editing from the console for debug purposes...
	   window.QA.ui = my.ui;

   my.userAppInitialized = true;
   $log.debug('->Login initialized');
    my.globalTrace.did('updateLogin() done');

	
	
	
	// Translate the old loginObject role and level to permissions in the new loginObject...
	// TODO: Remove this after migrating to RBAC/getting rid of GWT...
	
	// Takes any string and turns it into camelCase (support for permissions shim below)...
	var camelCase = function(str) {
	  return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
		return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
	  }).replace(/\s+/g, '');
	}
	
	// Takes an array of strings and removes any dupes (support for permissions shim below)...
	var unique = function(a) {
		return a.sort().filter(function(item, pos, ary) {
			return !pos || item != ary[pos - 1];
		})
	}
	
	my.login.permissions = [];
	
	if (my.login.canConsentEnrollMinors)
		my.login.permissions.push( 'special_canConsentEnrollMinors');

	if (my.login.canManagePowerUser == 'Y')
		my.login.permissions.push( 'special_canManagePowerUser');

	if (my.login.reportingRole !== 'undefined')
		my.login.permissions.push( 'special_' + camelCase(my.login.reportingRole) + 'ReportingRole' );

	if (my.login.userLevel !== 'undefined')
		my.login.permissions.push( 'special_' + camelCase(my.login.userLevel) + 'Level' );

	if (my.login.accessLevel !== 'undefined')
		my.login.permissions.push( 'special_' + camelCase(my.login.accessLevel) + 'Access');
	

	// Make sure all permissions are unique...
	my.login.permissions = unique(my.login.permissions);
}
/**
 * @kind ControllerFunction
 * @name app.webSockets
 *
 * @description
 * This sets up the WebSocket connect, disconnect, and * event (something came from the server)
 *
 **/

    // All messages from the server...
    socket.on('*', function (event, data) {
		// Calculate the size of the data...
		var dataSize = JSON.stringify(data).length + event.length + 7;

		// Create a new tracer for every socket event...
		var tracer = new QA.tracer(dataSize + ' bytes->' + event);
		
		// Send it to the push router...
        $appEvent('push:' + event, data, tracer);
		tracer.done();
    });
   
    // Called when a connection is established...
    socket.on('connect', function() {
		// Create a new tracer for every socket event...
		var tracer = new QA.tracer('connected');
		var sync = {};
		
        // If we're logged in with GWT...
        if ( my.login && my.login.userId ) sync.data = my.login;
		// If the token exists, send it too...
		if ( my.identity && my.identity.token ) sync.token = my.identity.token;
		
		// Send the event...
		$appEvent( 'remote:sync', sync );
        tracer.log('Synchronization sent');

		// If the identity token is present, repopulate the identity...
		//if ( my.identity && my.identity.token ) $appEvent('remote:login:sync', { token:my.identity.token });
		tracer.done();
    });
   
    // Called when a connection is lost...
    socket.on('disconnect', function() {
		// Create a new tracer for every socket event...
		var tracer = new QA.tracer('disconnected');
		
        // Fill the node object with defaults...
        my.nodeFill();

		// Handle token storage on disconnect...
		if ( my.identity && my.identity.token )
			localStorage.setItem("disconnect", JSON.stringify( { time: new Date(), token: my.identity.token }) );

		my.isAuthenticated = false;
		tracer.done();
    });
});app.controller('MainMenuController', function($model, $scope, $appEvent, $log, gsiSideNav) {var my = this;$model.store('mainMenu', $scope);if (typeof boot == 'function') boot();function boot() {

    //Mpved this to app.initApp
    //$appEvent('remote:appGlobal:boot');

    my.mainMenuExpand=true;
    $model.get('app').menu=my;
    $model.get('app').mainMenu=gsiSideNav.get('MainMenu',false,'appHome');
}
});app.controller('appHomeController', function($mdDialog, $mdPanel, $mdMedia, $model, $scope, $rootScope, $appEvent, $timeout, $q, $interval, $filter, $trace, gsiSideNav, taskBadgeFilter,$log) {var my = this;$model.store('home', $scope);if (typeof boot == 'function') boot();function boot() {
    var trace = new $trace('home.boot');
    my.appCtrl = $model.get('app');

    //just for test rendering tab and widget
    my.taskBadgeFilter = {};

    my.ranges = [
        { index: 0, label: "Today" },
        { index: 7, label: "Next 7 Days" },
        { index: 30, label: "Next 30 Days" }
    ];
    my.chartRanges = {
        7: "Last 7 Days",
        14: "Last 14 Days",
        30: "Last 30 Days"
    };

    my.taskBadgeDescription = {
        overdue: 'OVERDUE TASKS',
        due: 'DUE TASKS',
        careplan: 'CARE PLAN TASKS',
        assessment: 'ASSESSMENT TASKS'
    };

    my.patientsToggle = {
        1: "My Patient Panel",
        2: "My Org Patients"
    };

    my.currentPatientToggle = 1;

    my.currentChartRange = 7;

    my.currentFilter = {
        currentpatienttoggle: 1,
        currentchartrange: 7
    };

    my.currentRange = 'DY';

    my.currentMemberName = 'My Tasks Summary';

    my.numberOfTooltipRows = 5;

    my.currentDatePicker = my.ranges[0]

    my.todoTask = {
        count: 0,
        tasks: [],
        email: '',
        interval: 0
    };

    my.overdueTask = {
        count: 0,
        tasks: [],
        email: '',
        interval: 0
    };

    my.cpNonassessTask = {
        count: 0,
        tasks: [],
        email: '',
        interval: 0
    };

    my.cpAssessTask = {
        count: 0,
        tasks: [],
        email: '',
        interval: 0
    };
    my.messageBundle = {};
    my.default_already_added = "Patient is already on your Patient Panel";

    $timeout(function () {
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent('resize', true, false);
        window.dispatchEvent(evt);
    }, 1000);
    $appEvent('remote:appHome:getMessageBundle');

    my.loadOverdueTaskInNavigationBar = function () {
        $appEvent('remote:appGlobal:count', ['appTaskList']);
    }

    my.reloadTaskBadgeCount = function (isFirstLoad) {
        if (!isFirstLoad) {
            my.loadOverdueTaskInNavigationBar();
        }
    }
    $model.get('app').homeCtrl = my;
    my.reloadTaskBadgeCount(true);

    $appEvent('remote:appGlobal:loadMemberList');

    trace.success();

    my.alertEncounterApi = {};
    my.taskApi = {};
    my.issueApi = {};
    my.assessmentApi = {};
    my.goalApi = {};
    my.interventionApi = {};
    my.hideTaskBadgeCount = {};
    my.barTaskLabels = {
        x: 'Task type',
        y: 'Tasks'
    };
    my.currentPermissions = ['AllReport'];
    $timeout(function() {
        var email = $model.get('app').login.email;
    });

    my.issueFilter = {};
    my.assessmentFilter = {};
    my.goalFilter = {};
    my.interventionFilter = {};
    my.patientNoIssue = {};
    my.patientNoAssessment = {};
    my.patientNoGoal = {};
    my.patientNoIntervention = {};
    my.dashboards = $model.get('app').settings.apps.home.dashboards;
}

my.changeSortColumn = function (columnName, event) {
    if(my.currentSortElement)
        $(my.currentSortElement).removeClass('sorting');
    $(event.target).addClass('sorting');
    my.currentSortElement = event.target; 
    if (my.sortColumn === columnName) {
        my.sortOrder = my.sortOrder === 'asc' ? 'desc' : 'asc';
        my.reverse = !my.reverse;
    } 
    else {
        my.reverse = false;
        my.sortColumn = columnName;
        my.sortOrder = 'asc';
    }
}
my.reloadPatientPanelView = function(){
    if(my.currentPatientToggle == 1){
        my.changeUserView();
    }
};
my.changeTaskSelection = function () {
    if (my.currentMember.email === my.appCtrl.login.email) {
        my.currentMemberName = 'My Tasks Summary';
    }
    else if (my.currentMember.email.contains(";") > 0) {
        my.currentMemberName = 'My Team Tasks Summary';
    }
    else {
        my.currentMemberName = 'Tasks Summary for ' + my.currentMember.lastName + ', ' + my.currentMember.firstName;
    }
    my.reloadTaskBadgeCount();
};

my.changeUserView = function(data) {
  var alertApiData = {};

  if(data !== undefined) {
    my.currentMember = data.selectedView;
    my.selectedSubordinates = data.subordinates;
  }

  switch(my.currentMember.type) {
      case 1:
          $model.get('app').currentMenuTheme = 'theme-bg-primary';
          break;
      case 2:
          $model.get('app').currentMenuTheme = 'theme-bg-secondary';
          break;
      default:
          $model.get('app').currentMenuTheme = 'theme-bg-grey';
  };

  alertApiData.member = my.currentMember;
  alertApiData.subordinates = my.selectedSubordinates;
  if (typeof my.alertEncounterApi.refresh==='function')
    my.alertEncounterApi.refresh(alertApiData);
  if (typeof my.taskApi.refresh==='function')
      my.taskApi.refresh(my.currentMember.type === 2);
  if (typeof my.issueApi.refresh==='function')
      my.issueApi.refresh();
  if (typeof my.assessmentApi.refresh==='function')
      my.assessmentApi.refresh();
  if (typeof my.goalApi.refresh==='function')
      my.goalApi.refresh();
  if (typeof my.interventionApi.refresh==='function')
      my.interventionApi.refresh();
};

my.displayConfirmation = function(title, msg, success, cancel){
    var position = $mdPanel.newPanelPosition()
        .absolute()
        .center();

    var config = {
        attachTo: angular.element(document.body),
        controllerAs: 'panelCtrl',
        controller: function (mdPanelRef) {
            var panel = this;
            panel.msg = msg;
            panel.title = title;
            panel._mdPanelRef = mdPanelRef;
            this.close = function() {
                var panelRef = panel._mdPanelRef;
                panelRef && panelRef.close().then(function () {
                    panelRef.destroy();
                });
            };
            this.ok = function () {
                success();
                var panelRef = panel._mdPanelRef;
                panelRef && panelRef.close().then(function () {
                    panelRef.destroy();
                });

            };
            this.no = function () {
                cancel();
                var panelRef = panel._mdPanelRef;
                panelRef && panelRef.close().then(function () {
                    panelRef.destroy();
                });
            }
        },
        template: '<div id="confirmPopup" class="panel-dialog">' +
        '   <md-toolbar class="panel-title">' +
        '       <div class="md-toolbar-tools">' +
        '           <h2>'+ title + '</h2>' +
        '           <span flex></span>' +
        '           <md-button class="md-icon-button" ng-click="panelCtrl.close()" arial-label="cancel"><md-icon md-svg-icon="close"></md-icon></md-button>' +
        '       </div>' +
        '   </md-toolbar>' +
        '   <div class="panel-content">' +
        '       <p>' + msg + '</p>' +
        '   </div>' +
        '   <div class="panel-action">' +
        '       <md-button id="btnConfirmOk" ng-click="panelCtrl.ok()">Yes</md-button>' +
        '       <md-button id="btnConfirmNo" ng-click="panelCtrl.no()" md-autofocus>No</md-button>' +
        '   </div>' +
        '</div>',
        disableParentScroll: true,
        hasBackdrop: true,
        panelClass: 'demo-dialog-example',
        position: position,
        trapFocus: true,
        zIndex: 250,
        clickOutsideToClose: true,
        escapeToClose: true,
        focusOnOpen: true
    };

    $mdPanel.open(config);
}

my.getObjectLength = function(item) {
    return Object.keys(item).length;
};
// my.handleChartClick = function(popupType, title, data) {
//     GWT.activity();
//     my.popupTitle = title;
//     my.alertPopupChartData =angular.copy(my.alertChartData);
//     my.encountersPopupData =angular.copy(my.encounters);

//     my.already_added = my.getMessageBundle(my.default_already_added, "popup.tooltip.already_added", function (configMsg) {
//         return configMsg;
//     });
    
//     if (data.patients.size() !== 0) {
//         my.alertPatientList = [];
//         $model.get('app').showDialog('appViewPatients', popupType, null, function() {
//         });
//         if (data.patients.size() <= 500) {
//             my.patientCount = -1;
//             $appEvent('remote:appHome:loadPatientList', {patients : data.patients});
//         }
//         else {
//             $timeout(function() {
//                 my.patientCount = data.patients.size();
//                 my.showSearchPatientDialog(data.patients, popupType);
//             });
//         }
//     }  else {
//         $mdDialog.show(
//           $mdDialog.alert()
//             .parent(angular.element(document.querySelector('#popupContainer')))
//             .clickOutsideToClose(true)
//             .title('Message Box')
//             .textContent('There is no patient data')
//             .ariaLabel('There is no patient data')
//             .ok('OK')
//         );
//     }
// };

my.hideTooltip = function() {
    if(my.previousTooltip && my.previousTooltip.parentElement)
        my.previousTooltip.parentElement.removeChild(my.previousTooltip);
};
/*my.loadMemberList = function (data) {
    my.teamMembers = data;

    // add logged-in user
    var loginResult = $model.get("app").login;
    var user = {};
    user.email = loginResult.email;
    user.firstName = 'Me';
    user.lastName = '';
    user.userId = loginResult.userId;
    my.teamMembers.unshift(user);

    // set default selected user
    my.currentMember = my.teamMembers[0];
};*/
my.loadPatientList = function (data) {
    my.alertPatientList = data.sort(function (a, b) {
        return (a.patientId - b.patientId)
    });
};
my.onError = function (data) {
    GWT.activity();
    $log.warn("Home Panel", data[1]);

    if (data[0] === "TaskBadge") {
        my.isTaskBadgeError = true;
    }

    if (data[0] === "Alert") {
        my.isAlertError = true;
    }

    if (data[0] === "CarePlan") {
        my.isCarePlanError = true;
    }

    if (data[0] === "NoEncounter") {
        my.isNoEncounterError = true;
    }

    if (data[0] === "PatientList") {
        my.isPatientListError = true;
    }
};

my.onSuccess = function (data) {
    GWT.activity();
    if (data[0] === "TaskBadge") {
        my.isTaskBadgeError = false;
    }

    if (data[0] === "Alert") {
        my.isAlertError = false;
    }

    if (data[0] === "CarePlan") {
        my.isCarePlanError = false;
    }

    if (data[0] === "NoEncounter") {
        my.isNoEncounterError = false;
    }
};

my.openCarebook = function (patientId) {
    GWT.activity();
    $appEvent('local:patientDrillDown:openCarebook', patientId);
    $mdDialog.hide();
};
my.openEdit = function (patientId) {
    GWT.activity();
    $appEvent('local:patientDrillDown:edit', patientId);
    $mdDialog.hide();
};
my.openCareplan = function (patientId) {
    GWT.activity();
    $appEvent('local:patientDrillDown:openCarePlanDirect', patientId);
    $mdDialog.hide();
};
my.addToPatientList = function (index, patient) {
    GWT.activity();
    var title = 'Confirmation';
    var defaultMsg = 'Add '+ patient.lastname + ', ' + patient.firstname + ' to My Patient Panel?';
    var msg = my.getMessageBundle(defaultMsg, "popup.confirmation.add.msg", function (configMsg) {
        return configMsg.replace('[0]', patient.lastname + ', ').replace('[1]', patient.firstname);
    });
    var success =function () {
        my.currentPatientActive = patient;
        $appEvent('remote:myPatientList:add', {MyPatientInfoDTO: patient});
    };
    my.displayConfirmation(title, msg, success, function(){});

};
my.removeFromPatientList  = function (index, patient) {
    GWT.activity();
    var title = 'Confirmation';
    var defaultMsg = 'Remove '+ patient.lastname + ', ' + patient.firstname + ' from My Patient Panel?';
    var msg = my.getMessageBundle(defaultMsg, "popup.confirmation.remove.msg", function (configMsg) {
        return configMsg.replace('[0]', patient.lastname + ', ').replace('[1]', patient.firstname);
    });
    var success = function () {
        my.currentPatientActive = patient;
        $appEvent('remote:myPatientList:removeFromPopup',{patientId: patient.patientId});
    };
    my.displayConfirmation(title, msg, success, function(){});

};
my.getMessageBundle = function (defaultMsg, msgKey, replaceFunc) {
    if(my.messageBundle && (my.messageBundle[msgKey])){
        return replaceFunc(my.messageBundle[msgKey]);
    } else {
        $log.debug('Cannot get messageBundle with key: '+ msgKey);
        return defaultMsg;
    }

};

// my.drawAlertChart = function() {
//     my.alertChartApi.update();
// };

// my.drawCarePlanChart = function() {
//     my.carePlanChartApi.update();
// };

my.reloadHomePanel = function () {
    //Boot the main application...
    //$appEvent('remote:appHome:boot');
    my.reloadTaskBadgeCount();
    my.request();
};
my.searchPatientListCallBack = function(data){
    if (data.count>0 && data.count<=500) {
        my.alertPatientList = data.patientList;
        my.searchDialog.close();
    } else {
        my.alertPatientList = [];
    }
    my.patientCount = data.count;
}
my.setTooltip = function (event, tooltipId) {
    var elem = angular.element(document.getElementById(tooltipId))[0];
    if (elem) {
      elem.style.left = (event.clientX - elem.clientWidth/2) + 'px';
    }
    my.previousTooltip = elem;
}
my.showSearchPatientDialog = function (title, data, popupType) {
    var position = $mdPanel.newPanelPosition()
        .absolute()
        .center();

    function PanelController(mdPanelRef, parentScope) {
        parentScope.searchDialog = this;
        var panel = this;
        this.popupType = popupType;
        panel._mdPanelRef = mdPanelRef;
        this.close = function () {
            var panelRef = panel._mdPanelRef;
            panelRef && panelRef.close().then(function () {
                panelRef.destroy();
            });
        };
        this.search = function () {
            if (this.patientSearchText) {
                GWT.activity();
                this.parentScope.patientSearchText = this.patientSearchText;
                this.parentScope.alertPatientList = [false];
                var panelRef = panel._mdPanelRef;
                $appEvent('remote:appHome:searchPatientList', { patients: data, searchText: this.patientSearchText });
            }
        };
        this.cancel = function () {
            this.close();
            $mdDialog.hide();
        };
        this.close = function () {
            var panelRef = panel._mdPanelRef;
            panelRef && panelRef.close().then(function () {
                panelRef.destroy();
            });
        }
    }
    var config = {
        attachTo: angular.element(document.body),
        controllerAs: 'panelCtrl',
        controller: PanelController,
        template: '<div id="searchPopup" class="panel-dialog">' +
        '   <md-toolbar class="panel-title">' +
        '       <div class="md-toolbar-tools">' +
        '           <h2 style="text-transform: capitalize;">' + title.toLowerCase() + '</h2>' +
        '           <span flex></span>' +
        '           <md-button class="md-icon-button" ng-click="panelCtrl.cancel()" arial-label="cancel"><md-icon md-svg-icon="close"></md-icon></md-button>' +
        '       </div>' +
        '   </md-toolbar>' +
        '   <div class="panel-content">' +
        '       <p ng-if="!panelCtrl.parentScope.patientCount">No matches found! Please try again.</p>' +
        '       <p ng-if="panelCtrl.parentScope.patientCount">{{panelCtrl.parentScope.patientCount}} patients <span ng-show="panelCtrl.popupType===\'NO_ENCOUNTER\'">with no encounters </span>exceeds 500 patients display limit. Please enter patient name to refine your search</p>' +
        '       <div>' +
        '           <div style="width: 20px; height: 20px; display: inline-block"></div>' +
        '           <input type="text" press-enter="panelCtrl.search()" ng-model="panelCtrl.patientSearchText" id="txtPatientSearch">' +
        '           <div style="position: relative; display:inline-block; width:20px; height:20px; vertical-align: middle"><div ng-if="panelCtrl.parentScope.alertPatientList[0]===false" class="progress-container" layout="row" layout-align="center center">' +
        '               <md-progress-circular md-diameter="20px" md-mode="indeterminate"></md-progress-circular>' +
        '           </div></div>' +
        '       </div>' +
        '   </div>' +
        '   <div class="panel-action">' +
        '       <md-button id="btnConfirmOk" ng-click="panelCtrl.search()" ng-disabled="!panelCtrl.patientSearchText">Search</md-button>' +
        '       <md-button id="btnConfirmNo" ng-click="panelCtrl.cancel()">Cancel</md-button>' +
        '   </div>' +
        '</div>',
        disableParentScroll: true,
        hasBackdrop: true,
        panelClass: 'demo-dialog-example',
        position: position,
        trapFocus: true,
        zIndex: 250,
        clickOutsideToClose: false,
        escapeToClose: true,
        focusOnOpen: true,
        locals: { parentScope: my }
    };

    $mdPanel.open(config);
};
my.showToolTipInfo = function(data, patientInfo) {
    var position = $mdPanel.newPanelPosition()
        .absolute()
        .center();

    var popupType = data.popupType;
    var patientId = patientInfo.patientId;
    var patientName = patientInfo.firstname + ' ' + patientInfo.lastname;
    var dataDetail = {};

    function PanelController(mdPanelRef) {
        var panel = this;
        this.data = data;
        panel._mdPanelRef = mdPanelRef;

        dataDetail.detailItems = [];
        switch(popupType) {            
            case 'ALERT':
                dataDetail.title1 = 'Severity';
                dataDetail.title2 = 'Alert Type';
                
                if(my.alertPopupChartData && my.alertPopupChartData.alertList && my.alertPopupChartData.alertList[patientId]) {
                    var sourceList = my.alertPopupChartData.alertList[patientId];                
                    angular.forEach(sourceList, function(value, key) {
                        var item = {
                            label1: value.severity,
                            label2: value.alertName
                        };
                        dataDetail.detailItems.push(item);
                    });
                }
                dataDetail.detailItems = $filter('orderWithPriority')(dataDetail.detailItems, ['label1'], [my.alertPopupChartData.label]);
                this.popupTitle = patientName + '\'s ' + 'Alert';
                break;
            case 'ENCOUNTER':
                dataDetail.title1 = 'Frequency';
                dataDetail.title2 = 'Care Plan Encounter';
                if(my.encountersPopupData && my.encountersPopupData.serviceList && my.encountersPopupData.serviceList[patientId]) {
                    var sourceList = my.encountersPopupData.serviceList[patientId];                
                    angular.forEach(sourceList, function(value, key) {
                        var item = {
                            label1: value.label1,
                            label2: value.label2
                        };
                        dataDetail.detailItems.push(item);
                    });
                }
                dataDetail.detailItems = $filter('orderWithPriority')(dataDetail.detailItems, ['label2'], [my.encountersPopupData.label]);
                this.popupTitle = patientName + '\'s ' + 'Encounter';
                break;
            case 'CCP':
                if (data.tooltipTitles) {
                    dataDetail.title1 = data.tooltipTitles[0].toLowerCase();
                    dataDetail.title2 = data.tooltipTitles[1].toLowerCase();
                    if(data.tooltipTitles[2]) {
                        dataDetail.title3 = data.tooltipTitles[2].toLowerCase();
                    }
                }
                
                if(data.tooltip && data.tooltip[patientId]) {
                    var sourceList = data.tooltip[patientId];                
                    angular.forEach(sourceList, function(value, key) {
                        var item = {
                            label1: value.label1,
                            label2: value.label2
                        };
                        if (value.label3) {
                            item.label3 = value.label3;
                        }
                        dataDetail.detailItems.push(item);
                    });
                }
                dataDetail.detailItems = $filter('orderWithPriority')(dataDetail.detailItems, data.selectedFields, data.selectedLabels);
                this.popupTitle = patientName + '\'s ' + dataDetail.title1.split(' ')[0];
                break;

        }
        this.dataDetail = dataDetail;

        this.close = function () {
            var panelRef = panel._mdPanelRef;
            panelRef && panelRef.close().then(function () {
                panelRef.destroy();
            });
        };
        this.cancel = function () {
            this.close();
        };
        this.close = function () {
            var panelRef = panel._mdPanelRef;
            panelRef && panelRef.close().then(function () {
                panelRef.destroy();
            });
        }
    }



    var config = {
        attachTo: angular.element(document.body),
        controllerAs: 'panelCtrl',
        controller: PanelController,
        template: '<div>' +
        '   <md-toolbar class="panel-title">' +
        '       <div class="md-toolbar-tools">' +
        '           <h2 style="text-transform: capitalize;" ng-bind="panelCtrl.popupTitle">' + '</h2>' +
        '           <span flex></span>' +
        '           <md-button class="md-icon-button" ng-click="panelCtrl.cancel()" arial-label="cancel"><md-icon md-svg-icon="close"></md-icon></md-button>' +
        '       </div>' +
        '   </md-toolbar>' +
        '   <div class="panel-content">' +
        '       <table class="fixed_headers">' +
        '           <thead>' +
        '               <tr>' +
        '                   <th style="text-transform: capitalize;" ng-bind="panelCtrl.dataDetail.title1.toLowerCase()"></th>' +
        '                   <th style="text-transform: capitalize;" ng-bind="panelCtrl.dataDetail.title2.toLowerCase()"></th>' +
        '                   <th style="text-transform: capitalize;" ng-if="panelCtrl.dataDetail.title3" ng-bind="panelCtrl.dataDetail.title3.toLowerCase()"></th>' +
        '               </tr>' +
        '           </thead>' +
        '           <tbody>' +
        '               <tr ng-repeat="tooltipData in panelCtrl.dataDetail.detailItems">' +
        '                   <td ng-bind="tooltipData.label1"></td>' +
        '                   <td ng-bind="tooltipData.label2"></td>' +
        '                   <td ng-if="tooltipData.label3" ng-bind="tooltipData.label3"></td>' +
        '               </tr>' +
        '           </tbody>' +
        '       </table>' +
        '   </div>' +
        '   <div class="panel-action" layout="row">' +
        '       <span flex></span>' +
        '       <md-button id="btnConfirmNo" ng-click="panelCtrl.cancel()">Close</md-button>' +
        '   </div>' +
        '</div>',
        disableParentScroll: true,
        hasBackdrop: true,
        panelClass: 'tooltip-dialog',
        position: position,
        trapFocus: true,
        zIndex: 250,
        clickOutsideToClose: true,
        escapeToClose: true,
        focusOnOpen: true
    };

    $mdPanel.open(config);
}
my.updatePatientList = function(data){
    //TODO: reload Patient Panel
    my.currentPatientActive.alreadyInList = true;
    $model.get('myPatientList').reloadAfterRemoveOrAdd();
};

my.updateRemovedPatient = function(data){
    my.currentPatientActive.alreadyInList = false;
    $model.get('myPatientList').reloadAfterRemoveOrAdd();
};
});app.controller('CalendarTemplate', function($mdDialog, $mdMedia, $scope, $rootScope, $model, $timeout, $q, $interval, $filter, calendarConfig) {var my = this;$model.store('calendarTemplate', $scope);if (typeof boot == 'function') boot();// Called on boot...
function boot() {
    // Store our own reference to devmode...
    var vm = this;
    calendarConfig.templates.calendarWeekCell = 'customWeekCell.html';
    calendarConfig.templates.calendarDayItem = 'customDayItem.html';
    calendarConfig.templates.calendarMonthCell = 'customCalendarMonthCell.html';
    var taskCtrl = $model.get('task');
    calendarConfig.filter = taskCtrl.filter;
    calendarConfig.status = taskCtrl.status;
    calendarConfig.searchStr = taskCtrl.search.time;
    my.updateSarchString = function (str) {
            calendarConfig.searchStr = str;
    }
    //console.log(this.filter);
    //vm.events = [];
    //vm.calendarView = 'month';
    //vm.viewDate = moment().startOf('month').toDate();

    //$scope.$on('$destroy', function() {
    //  calendarConfig.templates.calendarMonthCell = 'mwl/calendarMonthCell.html';
    //});
   
};
});