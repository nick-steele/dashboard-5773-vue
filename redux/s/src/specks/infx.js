module.exports = function(buf, opts) {
	var s = require('gsiSimplex');
	var id = s.dom.nextID();
	s.dom.speck('infx'); // Let simplex know we've been used.
	return '<span class="input input--'+ opts.type +'">'+
				'<input class="input__field input__field--'+ opts.type +'" type="text" id="' + id + '" />' +
				'<label class="input__label input__label--'+ opts.type +'" for="' + id + '">' +
					'<span class="input__label-content input__label-content--'+ opts.type +'">' + buf + '</span>' +
				'</label>' +
			'</span>';
}
