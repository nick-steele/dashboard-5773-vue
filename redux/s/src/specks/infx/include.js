/* Simplex Speck: infx (Input FX) - v0.1
 * Creates a bunch of cool text box types and other special effects that you can optionally use (like all specks - if you don't use it, it doesn't get compiled into your code!)
 */

(function() {
	console.log('speck INFX loaded');

	// Go through each input item with input__field in the class...
	[].slice.call( document.querySelectorAll( 'input.input__field' ) ).forEach( function( inputEl ) {
		console.log('input__field loop');
		// in case the input is already filled...
		if( inputEl.value.trim() !== '' ) classie.add( inputEl.parentNode, 'input--filled' );
		// events:
		inputEl.addEventListener( 'focus', onInputFocus );
		inputEl.addEventListener( 'blur', onInputBlur );
	} );

	function onInputFocus( ev ) {
		console.log('input focus');
		classie.add( ev.target.parentNode, 'input--filled' );
	}

	function onInputBlur( ev ) {
		console.log('input blur');
		console.log('INFX blured');
		if( ev.target.value.trim() === '' ) classie.remove( ev.target.parentNode, 'input--filled' );
	}
})();