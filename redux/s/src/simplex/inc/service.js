
// ### This maps service names to endpoints and logic functionality.  Supports any protocol, you just have to create the protocol handler....



// Global so service functions can share the selected service...
var service = {};

// The main function...
var handleService = function(name, socket, data, success, fail) {
	
	// Append the service settings to the login object...
	// TODO: Pull this from the configuration endpoint
	s.settings.service = {
		endpoints: {
			test:		"http://localhost:8001/test",
			task:		"http://10.110.210.59:8080/TaskManager-1.0/tasking/"
		},
		services: {
			login:		{	endpoint:	"test",			protocol:	"REST.GET",		response:	"model;app.login",		enclosure:	"data",			format:	":data.username/:data.password"				},
			test:		{	endpoint:	"test",			protocol:	"REST.GET",		response:	"log",					enclosure:	"",				format:	""											},
			taskList:	{	endpoint:	"task",			protocol:	"REST.GET",		response:	"model;task.tasks",		enclosure:	"UserTaskList",	format:	"usertask/:login.email/:login.commId"		}
		}
	};
	
	s.settings.service.endpoints = s.settings.env.endpoints;
	s.settings.service.services = s.settings.services;

	// Trace that we've launched a service...
	s.logger.trace('Service(' + name + ')');

	// If the service is defined...
	if (s.settings.service.services[name]) {
		service = s.settings.service.services[name]; // ### modifies global
		// If the service endpoint is defined...
		if (s.settings.service.endpoints[service.endpoint]) {
			
			// Append the name...
			service.name = name;

			// Replace the endpoint...
			service.endpoint = s.settings.service.endpoints[service.endpoint];

			// Build the URI...
			service.uri = buildURI(service.format, socket);
			
			// If success / fail are present, replace defaults...
			service.success = (success ? success : successResponse);
			service.fail = (fail ? fail : failResponse);

			// Get the service type...
			var p = service.protocol.split(".");

			// If it exists...
			if (typeof protocols[p[0]][p[1]] === 'function') {
				// Run it...
				protocols[p[0]][p[1]](data);					
			} else {
				error('Missing protocol ' + service.protocol);
			}
		} else {
			error('Missing endpoint "' + service.endpoint + '"');			
		}
	} else {
		error('Service is undefined');
	}
}

// Service protocol handlers...
var protocols = {
	REST: {
		GET: function(data) {
			console.log(service.uri)
			s.restful.rest( 'GET', service.uri, service.success, service.fail, service.name );			
		} 
	}
} 

// Success handler...
function successResponse(data) {
	// Automate a successful response...
	var response = service.response.split(";");	
	console.log('Service "' + service.name + '" worked.');
}

// Fail handler...
function failResponse(data) {
	// Automate a failed response...
	console.log('Service "' + service.name + '" failed.');
}

// URI builder...
function buildURI(format, socket, data) {
	// RegEx to inject correct stuff...
	format = format.replace(":login.email", socket.clientData.login.email);
	format = format.replace(":login.commId", socket.clientData.login.commId);
	format = format.replace(":login.userId", socket.clientData.login.userId);
	//format = format.replace(":var.search", data.search);
	return service.endpoint + format;
}

// Error handler...
function error(msg) {
	s.logger.trace("SERVICE ERROR: " + msg + " (Service not processed!)");
}

module.exports = handleService;