var s = require('./../../simplex');

module.exports = function() {
	var startTime = new Date();

	s.compileService( 'users-1.0' );

	// We're done!
	var compileTime = new Date() - startTime;
	s.log('\nServices compiled successfully!', compileTime / 1000, 'seconds');
};

// Compile a router (specify a router name, provide a folder structure, define if it's to be compiled for server or device (Multi-user webSocket or single-user functional) and this handles the rest)...
s.compileService = function( name ) {

	s.log( '\n' + name + '...');

	// Service defaults...
	var service =	{
						name: name,
						base: '',
						preCode: '',
						postCode: '',
						code: '',
						folder: s.settings.folders.services,
						components: 0
					}

	// #####################################################################
	// ###     Loop through each service and build it's component...     ###
	// #####################################################################
	
	s.folders( s.settings.folders.root + service.folder ).forEach( function( componentName ) {

		service.components++;

		// Component defaults...
		var component = {
							name: componentName,	// From loop above
							commonCode: '',
							defaultCode: buildSwitchBlock(componentName, 'default', 'trace("Missing " + method);'),
							code: '',
							folder: service.folder + componentName + '/',
							methods: 0,
							methodNames: '',
							debug: '//# sourceURL=event.' + componentName
						}

		// Get common code (if there is any)...
		component.commonCode = s.file( component.folder + service.name + '.js' );

		// If there is common code, make sure it has a new line at the end of it...
		if ( component.commonCode.length > 0 ) component.commonCode += ( component.commonCode.substr(component.commonCode.length - 1) === '\n' ? '' : '\n' );
		
		// Build the start of method router (components with child events can share code, and therefore need an internal switch so they can execute in the same scope)...
		component.code = 'switch(method) {';
		
		// #####################################################################
		// ### Loop through each method (subfolders in component folders)... ###
		// #####################################################################

		s.folders( s.settings.folders.root + service.folder ).forEach( function( methodName ) {

			// Method defaults...
			var method =	{
								name: methodName,
								folder: service.folder + methodName + '/',
								code: ''
							}
			// Get the file...
			method.code = s.file( method.folder + method.name + '.js' );

			// If it's not empty, create the method...
			if (method.code.length > 0 ) {
				// Count the methods...
				service.methods++;

				// Append the method name...
				service.methodNames += (service.methodNames.length > 0 ? ', ' : '') + methodName;

				method.code = buildSwitchBlock(component.name, method.name, method.code);
			}				

			// Either save it to the default method, or add it to the running component code...
			if (method.name === 'default')
				component.defaultCode = '\n' + indent(method.code, 1);
			else {
				if (method.name === 'connect' && service.server) {
					//component.code = '';
				} else {
					component.code += method.code;		
				}
			}
		});
		
		// Log how many methods we just did in this component...
		if (component.methods != 0) {
			s.log( ' ' + ucFirst(component.name) + ': ' + component.methodNames );
		}
		
		// Combine component.code into a finished state (inject common code, the code, and the default code and finish the component switch block)...
		service.code	+=	(service.components > 1 ? ',\n' : '') + 
							component.name + ': function(method, data, trace) {\n' +
							indent(
								'audit("' + component.name + '", trace);\n' +
								component.commonCode +
								component.code +
								component.defaultCode + '\n}', 1) +
							'\n}';

		// If it's run at the server, add WebSocket code (many users run the same router)...
		if (service.server) service.postCode += '\tsocket.on("' + component.name + '", s.events.' + component.name + ');\n';
	});

	s.log('writing ' + service.name + ' event router to', s.settings.folders.root + s.settings.folders.compiled + service.name + '-service.js');
	// js = '{\n' + src + js + '\n}';
	

		// Build the wrap for client/server...
		var init	=	'let start = new Date();\n' +
						'var [undefined, method, ...args] = request.url.split("/"); // get the method and args\n' +
						'var log = console.log;\n' +
						'var query = s.data.query;\n' +
						'var simpleQuery = s.simpleQuery;\n' +
						'let functionName ( ...vars ) => { functionContents } socket.clientData.view;\n'; 
		
		// Every strategy is loaded inside the connect function, build the router header accordingly...
		var startup =	'// ATTENTION: This is a compiled file; it will change automatically. The source files are in s/src/services/' + service.name + '\n'+
						'module.exports = ( request, response ) =>\n' +
						indent(init, 1) +
						's.logger.start(socket, "connect");\n' +
						'\t// ### Compiled router (This is a single object that can be swapped out in real-time)\n'+
						'\ts.events = {\n' +
						indent(service.code, 2) + '\n\t};\n\n' +
						'\t// connect/' + service.name + '.js code will be injected here...\n' +
						indent(s.file( s.settings.folders.events + 'connect/' + service.name + '.js' ), 1) + '\n' +
						'\ts.logger.done(socket);' +
						'\n\n';
		
		// Finally, build the javascript...
		service.code = startup + '});\n\n';


	 // Compile the regular and compressed versions...
	s.write( s.settings.folders.compiled + service.name + '-services.js',  service.code );
}


// Adds tab indent(s)...
function indent(text, tabs) {
	return '\t'.repeat(tabs) + text.replace(new RegExp('\n', 'g'), '\n' + '\t'.repeat(tabs) );
}

function buildSwitchBlock(component, method, code) {
	var switchName	= (method === 'default' ? 'default:\n': 'case "' + method + '":\n');
	var debug		= '//# sourceURL=event.' + component + '.' + method;
	return '\n' + indent( switchName +
			'\taudit("' + component + "." + method + '")\n' +
			indent(code, 1) + '\n' +
			'\t' + debug + '\n' +
			'\tbreak;',
			1);	
}

// Upper-cases the first letter of a string...
function ucFirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
