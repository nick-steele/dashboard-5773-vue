
// Prototype changes...


// Get a key/value array of objects in this format "var=blah; var=blah" (like the browser cookies string)...
String.prototype.toKeyValue = function() {
	var ret = {};
	this.split('; ').forEach( function(item) {
		var i = item.split('=');
		var key = i[0];
		var value = i[1];
		ret[key] = value;
	});
	return ret;
}


// ### Universal functions...
var s = require('./../../simplex');
exports = module.exports = s;

// Return the string with the first char uppercase...
String.prototype.ucFirst = function(str) {
    return this.charAt(0).toUpperCase() + (this.slice(1)).toLowerCase();
}

// Horrible, awful error that requires the end of the universe...
exports.die = function(msg) {
	console.log(msg);
	process.exit(-1);
};

// Complete replacement for console.log...
exports.log = function() {

	if (s.settings.app.logging === false) return;

	var out = '';
	for ( var i = 0; i < arguments.length; i++ )
		out += out ? ' ' + arguments[i] : arguments[i];
	console.log(out);
};

exports.err = function(msg, end) {
	s.say(msg, end);
};

// Production Logger
exports.Log = function(data, type, socket) {
	if (socket)
		s.log( socket.id + ':' + s.getColor(type) + data + s.getColor() );
	else
		s.log( 'system:' + s.getColor(type) + data + s.getColor() );
};

// Production Errors
exports.Err = function(data, type, socket) {
	s.log('error:' + data, type, socket);
	if (socket) socket.emit('error', { message: data, type: type} );
};


// Color list for logger and error handler...
exports.getColor = function(type) {
    switch (type) {
        case 'user'     : return s.color('bold') + s.color('blue')
        case 'bug'      : return s.color('green');
        case 'system'   : return s.color('cyan');
        case 'protocol' : return s.color('yellow');
        default         : return s.color('reset');
    }
};

// Returns consol colors...
exports.color = function(color) {
    switch(color) {
        case 'bold'             :
        case 'bright'           : return s.color() + "\33[1m";
        case 'black'            : return s.color() + "\033[30m";
        case 'red'              : return s.color() + "\033[31m";
        case 'green'            : return s.color() + "\033[32m";
        case 'yellow'           : return s.color() + "\033[33m";
        case 'blue'             : return s.color() + "\033[34m";
        case 'magenta'          : return s.color() + "\033[35m";
        case 'cyan'             : return s.color() + "\033[36m";
        case 'white'            : return s.color() + "\033[37m";
        case 'bright red'       : return s.color('bright') + "\033[31m";
        case 'bright green'     : return s.color('bright') + "\033[32m";
        case 'bright yellow'    : return s.color('bright') + "\033[33m";
        case 'bright blue'      : return s.color('bright') + "\033[34m";
        case 'bright magenta'   : return s.color('bright') + "\033[35m";
        case 'bright cyan'      : return s.color('bright') + "\033[36m";
        case 'bright white'     : return s.color('bright') + "\033[37m";
        case 'reset'            :
        default                 : return "\033[0m";
    }
};

// Extrapolation on a folder name...
exports.rootFolder = function(name) {
  return s.settings.folders.root + s.settings.folders[name];
};

// Gets a file's contents if it exists, otherwise, return an empty string (as if the file existed but were empty)...
exports.file = function(src) {
	// log('loading ' + src);
	if ( s.fs.existsSync( s.settings.folders.root + src ) ) return s.fs.readFileSync(s.settings.folders.root + src, "utf8");
	return '';
};

exports.isFile = function(src) {
    if ( s.fs.existsSync ( src ) ) return true;
    return false;
}

// Writes a file's contents...
exports.write = function(src, contents) {
	return s.fs.writeFileSync(s.settings.folders.root + src, contents);
};

// Get a list of directories...
exports.folders = function(srcPath) {
  return s.fs.readdirSync(srcPath).filter(function(file) {
    return s.fs.statSync(s.path.join(srcPath, file)).isDirectory();
  });
};

// Makes sure a path exists...
exports.ensurePath = function(p, opts, made) {
    if (!opts || typeof opts !== 'object') opts = { mode: opts };
    var mode = opts.mode;
    if (mode === undefined) mode = parseInt('0777', 8) & (~process.umask());
    if (!made) made = null;
    p = s.path.resolve(p);

    try {
        s.fs.mkdirSync(p, mode);
        made = made || p;
    }
    catch (e) {
        switch (e.code) {
            case 'ENOENT' :
                made = s.ensurePath(s.path.dirname(p), opts, made);
                s.ensurePath(p, opts, made);
                break;
            default:
                var stat;
                try {
                    stat = s.fs.statSync(p);
                }
                catch (e2) {
                    throw e;
                }
                if (!stat.isDirectory()) throw e;
                break;
        }
    }
    return made;
};

// Removes the contents of a directory...
exports.emptyFolder = function(path) {
  if( s.fs.existsSync(path) ) {
    s.fs.readdirSync(path).forEach(function(file, index){
      var curPath = path + "/" + file;
      if(s.fs.lstatSync(curPath).isDirectory()) { // recurse
        s.emptyFolder(curPath);
      } else { // delete file
        s.fs.unlinkSync(curPath);
      }
    });
    //s.fs.rmdirSync(path);
  }
};

// TODO: improve this and put it into use:
// Thinking: Can events execute the same code on the client side as the server side? Write once, work anywhere?
// Like Meteor? Is there a good point to that?
// 
// Fires an event, data is optional...
exports.event = function(event, data) {
    // Split the event into its components...
    try { var parts = event.split(':'); } catch(error) { s.logger.trace('malformed-event:' + event); }
	var type = parts[0];
	var what = parts[1];
	var where = parts[2];
    
	switch (type) {
        case 'server':
            // Events happening server-server
            s.events[what]( { event: where, payload: data } );
            break;
        case 'client':
            // Events happening server-client
            s.events.push( { event: 'event', payload: 'local:' + what + where } );
            break;
		case 'push':
			var payload = where;
			if (typeof where === 'undefined') payload = data;
            s.events.push( { event: what, payload: payload } );
            break;
        default:
            // Events invoking
            s.logger.trace('unknown-event-type:' + event);
            break;
    }
};

exports.getURL = function( url ) {
    s.http.get(url, function(res){
        var body = '';

        res.on('data', function(chunk){ body += chunk; });

        res.on('end', function(){
            //var fbResponse = JSON.parse(body);
            console.log("Got a response: ", body);
        });
    }).on('error', function(e){
          console.log("getURL got an error:", e);
    });
};

// Gets something from a connected client...
exports.fromClient = function ( socket, event, data) {

    var dataRep = '';
    if (data) dataRep = (data.length > 40) ? data.length + ' bytes' : data;
    s.log('->:' + socket.id + ':' + event + ':' + dataRep);
};

// Send's the response of a rest call to a socket when it arrives...
exports.tunnel = function(socket, url ) {
    s.http.get(url, function(res){
        var body = '';

        res.on('data', function(chunk){ body += chunk; });

        res.on('end', function(){
            console.log('response recieved', body);
            s.toClient(socket, 'd', body);
        });
    }).on('error', function(e){
            s.toClient(socket, 'e', e);
    });
};

exports.rest = {
    get: function(url, socket) {
        s.con.to('REST', url, socket);
        s.http.get(url, function(res){
            var body = '';
            res.on('data', function(chunk){ body += chunk; });
            res.on('end', function(){
                s.con.to(url, body, socket);
                return body;
            });
        }).on('error', function(e){
            s.con.err(e, socket);
        });
    },
    post: function(url, socket, data) {
        console.log('rest.post started');
        s.con.to('REST', url, socket);
        var parsed = s.url.parse(url);
        var options = {
            host:   parsed.hostname,
            path:   parsed.path,
            port:   parsed.port,
            method: 'POST'
        };
        s.http.request(options, function(r) {
            var body = '';
            r.on('data', function(chunk) { body += chunk; });
            r.on('end', function() {
               s.con.to(url, body, socket);
               console.log('rest.put success', body);
               return body;
            });
        }).on('error', function(e){
            console.log('rest.put error');
            s.con.err(e, socket);
        });
    },
    put: function(url, socket, data) {
        console.log('rest.put started');
        s.con.to('REST', url, socket);
        var parsed = s.url.parse(url);
        var options = {
            host:   parsed.hostname,
            path:   parsed.path,
            port:   parsed.port,
            method: 'PUT'
        };
        s.http.request(options, function(r) {
            var body = '';
            r.on('data', function(chunk) { body += chunk; });
            r.on('end', function() {
               s.con.to(url, body, socket);
               console.log('rest.put success', body);
               return body;
            });
        }).on('error', function(e){
            console.log('rest.put error');
            s.con.err(e, socket);
        });
    }
};

// make a REST call...
exports.REST = function(method, base, dat, success, fail) {
    var parsed = s.url.parse(base);
    if (method === 'GET') {
        var headers = {};
        // No data to pass, so move args up by one...
        fail = success;
        success = dat;
    } else {
        var dataString = JSON.stringify(dat);
        var headers = {
            'Content-Type': 'application/json',
            'Content-Length': dataString.length
        };
    }
    var options = {
        host: parsed.hostname,
        path: parsed.path,
        port: parsed.port,
        method: method,
        headers: headers
    };
    console.log(options.method, options.host + ':' + options.port + options.path);

    var req = s.http.request(options, function(res) {
        var responseString = '';
        res.on('data', function(data) { responseString += data; });
        res.on('end', function() {
            try {
                if (responseString.length > 0) var responseObject = JSON.parse(responseString);
                if (typeof success === 'function') success(responseObject);
            }
            catch (e) {
				// An error occured in the response...
                if (typeof fail === 'function') fail('Unable to convert response to JSON');
            }
        });
    });

    if (typeof dataString !== 'undefined') req.write(dataString);
    req.end();
};

// Logging system...
exports.con = {
    to: function(event, data, socket) {
        this.log('<-', event, data, socket);
    },
    from: function(event, data, socket) {
        this.log('->', event, data, socket);
    },
    err: function(data, socket) {
        this.log('--', 'ERROR', data, socket);
    },
    id: function(socket) {
        return (socket) ? socket.id : '________SYSTEM________';
    },
    log: function(way, event, data, socket) {
        var over = 64;
        data = (data.length > over) ? data.substring(0, over) + ' (+' + data.length - over + ' bytes)' : data;
        s.log(way + ':' + this.id(socket) + ':' + event + ':' + data);
    }
};

// Sends a state to a user...
exports.state = function(name, socket) {
		s.logger.trace('sendState:' + name);
        //s.Log('send.state:' + name, 'user', socket);
        socket.emit('s', s.file( 's/compiled/states/' + name + '.state' ) );
};

// Sends something to a connected client...
exports.toClient = function ( socket, event, data) {
    s.logger.trace('push:' + event);
	var dataRep = (data.length > 40) ? data.length + ' bytes' : data;
    //s.log('<-:' + socket.id + ':' + event + ':' + dataRep);
    socket.emit(event, data);
};


// flexible sort...
exports.sortBy = function(field, reverse, primer){

   var key = primer ? function(x) {return primer(x[field])} : function(x) {return x[field]};
   reverse = !reverse ? 1 : -1;
   return function (a, b) {
       return a = key(a), b = key(b), reverse * ((a > b) - (b > a));
   };
};
