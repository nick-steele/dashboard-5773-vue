module.exports = {
	"modes": {
		"d": {
			bootserver  : "localhost",
			min         : false
		},
		"p": {
			bootserver  : "localhost",
			min         : false
		},
		"c": {
			bootserver	: "localhost",
			min         : false
		}
	},
	"folders":	{
		"compiled"	: 's/compiled/',
		"source"	: 's/src/',
		"wrappers"	: 's/src/wrappers',
		"services"	: 's/src/services/',
		"events"	: 's/src/events/',
		"app"		: 's/src/app/',
		"modules"	: 's/src/modules/',
		"ngApp"		: 's/src/app/js/active/',
		"backup"	: 's/time-travel/'
	}
};