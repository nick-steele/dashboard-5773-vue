var get = function(endPoint, win, fail) {
    rest('GET', endPoint, win, fail);
};

var _delete = function(endPoint, win, fail) {
    rest('DELETE', endPoint, win, fail);
};


var post = function(endPoint, body, win, fail) {
    rest('POST', endPoint, body, win, fail);
};

// The main rest caller (everything goes through this function)...
var rest = function(method, base, dat, success, fail, name, opts) {
    //s.logger.trace('\n ' + method + ':' + base);
    var parsed = s.url.parse(base);


    if (s.logger.socket().clientData.login) {
        var email = s.logger.socket().clientData.login.email;
    } else {
        var email = 'empty';
    }

    var headers;

    // Handle different methods...
    switch (method) {
        case 'GET':
            // NOTE: No break here, i.e. GET/DELETE share same code in DELETE! :)
        case 'DELETE':
            // No data to pass, so move args up by one...
            opts = name;
            name = fail;
            fail = success;
            success = dat;
            opts = opts || {};
            headers = opts.header ? opts.header : {};
            headers['GSI-email'] = email;
            break;
        case 'DELETEWITHBODY':
            // patch for DELETE operation with request body. Recent specifications allow DELETE to send body.
            method = 'DELETE';
        default:
            opts = opts || {};
            headers = opts.header ? opts.header : {};
            var dataString = JSON.stringify(dat);
            headers['Content-Type'] = 'application/json';
            headers['Content-Length'] = Buffer.byteLength(dataString, 'utf-8');
            headers['GSI-email'] = email;
            break;
    }


    //Adding security token...
    if(opts.secured){
        if(s.logger.socket().clientData.identity){
            s.logger.trace('secured');
            headers['token'] = s.logger.socket().clientData.identity.token;
            headers['commId'] = s.logger.socket().clientData.identity.tenant.id;
            headers['community'] = s.logger.socket().clientData.identity.tenant.instance;
        }else{
            s.logger.trace('[WARN] missing identity');
        }
    }

    var protocol = parsed.protocol;

    var options = {
        host: parsed.hostname,
        path: parsed.path,
        port: parsed.port,
        method: method,
        headers: headers
    };

    // Log what we're doing...
    //console.log(options.method, options.host + ':' + options.port + options.path);

    // Trace that we've made a REST call...
    s.logger.trace('REST:' + (name ? name : 'unnamed'));

    // Log the start time...
    //var startTime = process.hrtime();
    var startTime = Date.now();
    // Get the source key...
    var originLogKey = s.logger.key();
    // Make the request...

    //Switch request module per protocol
    var requestModule = (protocol === 'https:') ? s.https : s.http ;

    var req = requestModule.request(options, function(res) {
        var responseString = '';
        // When we get data, add it to the responseString...
        res.on('data', function(data) { responseString += data; });
        // Called when the rest call is done...
        var done = function() {

            //var endTime = process.hrtime();
            //var seconds = endTime[0] - startTime[0];
            //var milis = ( (endTime[1] - startTime[1]) / 1000000 ).toFixed(0);

            // Log the end time...
            var endTime = Date.now();
            //console.log(s.logger.stamp() + ':' + method, base, seconds + ':' + milis + 'ms');
            var milis = endTime - startTime;
            //s.logger.trace(seconds + '.' + milis);
            console.log(s.logger.stamp('service', milis + 'ms', originLogKey, res.statusCode) + (name ? name : 'unnamed') + '|' + method, base);

            // Handles REST call slowdown detection...
            registerTiming(milis, base);
            var responseObject;
            try {
                if (responseString.length > 0) var _json = JSON.parse(responseString);
                responseObject = _json;
            } catch (e) {
                // It worked, but the response is not JSON...
                responseObject = responseString;
            }

            if (res.statusCode === 200 || res.statusCode === 204) {
                if (typeof success === 'function') success(responseObject);
            } else if (res.statusCode >= 300 && res.statusCode < 400 && res.headers['location'] && opts.captureRedirect) { //FIXME: all redirect response are captured
                success({ "redirect": res.headers['location'] });
            } else {
                fail(res.statusCode, responseObject);
            }
        };
        // When data is done being transported...
        res.on('end', done);
    });

    // Error with the actual call itself (Network error, malformed URL, etc)...
    req.on('error', function(error) {
        // Log the end time...
        var endTime = Date.now();
        //console.log(s.logger.stamp() + ':' + method, base, seconds + ':' + milis + 'ms');
        var milis = endTime - startTime;
        console.log(s.logger.stamp('service', milis + 'ms', originLogKey, 'Network Error') + (name ? name : 'unnamed') + '|' + method, base + ' ' + error);
        // Tell the event what went wrong...
        fail(error);
    });

    if (typeof dataString !== 'undefined') req.write(dataString);
    req.end();
};

// TODO: Have this function register the last 20 REST times and check for growth, if so, setup throttling mechanism...
var registerTiming = function(timing, call) {
    var calls = [];
    var entry = { call: null, timing: null, flags: null };
    // TODO: Check in the performance of this call, and check for flags...
    checkFlags(entry);
};

// Checks an entry to see if a throttling strategy is nessisary...
var checkFlags = function(entry) {

};

// TODO: Throttling mechanism...
var throttle = function(strategy) {

};

module.exports = {
    get: get,
    post: post,
    rest: rest,
    _delete: _delete
};