// Makes sure the path exists...
s.ensurePath = function(p, opts, made) {
    if (!opts || typeof opts !== 'object') opts = { mode: opts };
    var mode = opts.mode;
    if (mode === undefined) mode = parseInt('0777', 8) & (~process.umask());
    if (!made) made = null;
    p = s.path.resolve(p);

    try {
        s.fs.mkdirSync(p, mode);
        made = made || p;
    }
    catch (e) {
        switch (e.code) {
            case 'ENOENT' :
                made = s.ensurePath(path.dirname(p), opts, made);
                s.ensurePath(p, opts, made);
                break;
            default:
                var stat;
                try {
                    stat = s.fs.statSync(p);
                }
                catch (e2) {
                    throw e;
                }
                if (!stat.isDirectory()) throw e;
                break;
        }
    }
    return made;
};