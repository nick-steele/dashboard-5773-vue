
var eventRoute = function (name, data) {
    s.newrelic.createWebTransaction('/websocket/' + name + '/' + (data.event || ''),function(){
        s.newrelic.endTransaction();
    })();
};

module.exports = eventRoute;

