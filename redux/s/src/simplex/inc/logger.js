var formatDate = require('./formatDate').formatDate;

// Private variables related to logging...
var log = {
	trace: [],
	type: null,
	socket: null,
	started: null,
	source: null,
	key: null,
	when: null,
	session: null,
	response: 'OK',
	last: null,			// The ns taken by the last trace.
	tracing: false,		// Upon startup, we aren't tracing.
	depth: 0,			// No depth at first.
	asyncRegistry: []	// List of asyncronous calls this is waiting on.
};

// Record an event...
function trace(event, data) {
	push(event);
	// If details were provided, log them...
	//if (typeof data !== 'undefined') console.log(data);
};

// TODO: Registers an async event... 
function registerAsync(event, token) {
	log.trace.push( { event: event, time: timeSince('last'), token: token } );
}

function logSignature() {
	var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var length = 6;
	var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

function sessionSignature() {
	var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var length = 10;
	var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}

// Start a logging trace (stuff that happens in an event; only allow start to be called after done)...
function start(socket, event) {
	
	// If this is a new trace, start it, otherwise just continue the existing trace...
	if (!log.tracing) {		
		// Generate a new log signature...
		log.key = logSignature();
		// Register the source...
		log.source = event;
		// Reset the trace...
		log.trace = [];
		// Load the sessionId...
		log.session = socket.logId
		// Set the start time...
		timeSince('now');
		// Store the socket...
		log.socket = socket;
		// Indicate we're running a trace...
		log.tracing = true;
		// Highlight this event as a root...
		event = event + '|';
		// Register this log type...
		log.type = 'event';
	} else {
		// Increase the depth...
		log.depth++;
		// Decorate the event as a child...
		event = '{' + event;
	}
	// Record the event...
	push(event);
	//console.log('start', event);
	//log.trace.push( {event: event, time: log.time.getTime() } );
}

// Ends the trace and writes it out...
function end(event) {
	// Reduce the depth...
	log.depth--;

	// If we're at the root, actually end the trace...
	if (log.depth < 0) {
		// Record the event...
		push(event);
		log.depth = 0;
		// End the trace...
		log.tracing = false;
		// Flush the log...
		flush();
	} else {
		push('}');
	}
}

// Ends the trace if it hasn't already been ended (used by the compiler to ensure logger.end() gets called in every event)...
function done() {
	//console.log('ended called');
	end('done');
}

// Saves a new event on the trace stack...
function push(what) {
	//console.log(what);
	log.trace.push( { event: what, time: timeSince('last') } );	
}

// Returns time since the trace started since a particular event...
function timeSince(when) {
	switch (when) {
		case 'now':
			// Save the current time if sent...
			log.started = process.hrtime();
			log.when = formatDate(new Date(), "yyyy-mm-dd hh:mm:ss:ffft");
			break;
		case 'started':
			// Return total time since started...
			var curTime = process.hrtime();
			var seconds = curTime[0] - log.started[0];
			var milis = ( (curTime[1] - log.started[1]) / 1000000 ).toFixed(3);
			// If we're dealing with less than a second...
			if (seconds === 0) {
				return milis + 'ms';
			} else {
				return seconds + '.' + milis;
			}
			break;
		case 'last':
			// Return the time since the last event...
			if (log.last === null) log.last = log.started;
			var curTime = process.hrtime();
			var seconds = curTime[0] - log.last[0];
			var milis = ( (curTime[1] - log.last[1]) / 1000000 ).toFixed(3);
			log.last = curTime;
			// If we're dealing with less than a second...
			if (seconds === 0) {
				return milis + 'ms';
			} else {
				return seconds + '.' + milis;
			}
			break;
	}
}

// Flushes the trace stack to the console (this is where the logic goes to flush another type, i.e. JSON, etc).
// In other words: This writes a log line...
function flush() {
	// Attach the socket details...
	var line = stamp();

	//// Convert everything to a single line...
	log.trace.forEach( function( trace ) {
		line += toString(trace);
	});

	// Get rid of orphaned dots in the tree...
	line = replaceAll(line, '.}', '}');

	// Send it to output...
	echo(line);
}

// Returns a log stamp for use in something (you can generate this outside of any tracing too, like a promised response to a REST call, for example...
function stamp( providedType, providedTime, providedKey, providedResponse) {
	return	formatDate(new Date(), "yyyy-MM-dd hh:mm:ss:ffft") + '|' +
			log.session + '|' +
			( providedTime ? providedTime : timeSince('started') ) + '|' +
			s.io.engine.clientsCount + '|' +
			( providedType ? providedType : log.type) + '|' +
			( providedResponse ? providedResponse : log.response) + '|' +
			(providedKey ? providedKey : log.key) + '|' +
			clientData() + '|';
}

function replaceAll(target, search, replacement) {
    return target.split(search).join(replacement);
};

// Returns the current log key...
function key() { return log.key; }

// Converts a log event to a string...
function toString(event) {
	// Don't add a dot if we're scaling up the tree...
	//if (event.event === '}') return event.event;
	// Add a dot...
	return event.event + '.'; // + '[' + event.time + '].';
}

// Sends a line to the console...
function echo(line) {
	console.log(line);
}

// Returns a string containing client data (IP address if not logged in, otherwise username)...
function clientData() {
	var data = log.socket.handshake.address + '|0';
	var userId = 'None';
	if (typeof log.socket.clientData !== 'undefined') {
		if (typeof log.socket.clientData.login !== 'undefined') {
			userId = log.socket.clientData.login.userId;
			if (typeof log.socket.clientData.login.email !== 'undefined') {
				data = log.socket.clientData.login.email + '|' + log.socket.clientData.login.communityId;
			}
		}
	}

	return data + '|' + userId;
}

function socketData() {
	return log.socket;
}


// Stuff we expose to the outside world...
module.exports = {
	start: start,
	trace: trace,
	key: key,
	done: done,
	stamp: stamp,
	socket: socketData,
	sessionSignature: sessionSignature
};