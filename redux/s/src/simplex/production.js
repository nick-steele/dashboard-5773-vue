// ### Simplex Production Mode Manager ###
s = require('../simplex');
exports = module.exports = s;

exports.run = function() {
	// Create minimal web server that we can upgrade to WebSocket...
	 s.server = s.http.createServer(function (req, res) {
		 res.writeHead(200, {'Content-Type' : 'text/html'});
		 res.end("OK to upgrade url " + req.url);
	 });

	 // Listen for WebSocket connections...
	 s.io = require('socket.io').listen(s.server);

	 // Load the compiled strategy router...
	 console.log(__dirname);
	 s.router = require('../../compiled/server-router');

	 // Patch settings...
	 s.settings.app.patching = false;

	 // Load final configuration information and start listening for connections...
	 s.bootstrap();
};
