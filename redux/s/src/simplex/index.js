/*
 *
 *	Simplex has 4 parts: the mode router (simplex), compiler (simplex-compiler), production mode manager (simplex-production), development mode manager (simplex-development)
 *	Simplex modules are launched by run(), the mode router below will check the command-line, if no arguments are found (dev/prod/comp), it will check app.json under app.mode
 *	for the same directive.  If nothing is found, it returns a failure to launch. (-1)
 *
 *	It's important to note that the command-line will override anything in app.json.
 *
 *
 *		### MODE ROUTER ###
 *
 *
 */

var startTime = new Date();

// Define the root s object so we don't get lint warnings...
var s			= require('./inc/functions');

 // Used by the universal functions in this file and s.mode...
s.fs            = require('fs');
s.path			= require('path');
s.http          = require('http');
s.https          = require('https');
s.url           = require('url');
s.uid           = require('./inc/md5').md5;
s.extend		= require('./inc/extend');
s.restful		= require('./inc/restful');
s.logger		= require('./inc/logger');
s.eventRoute	= require('./inc/eventRoute');
s.service		= require('./inc/service');
//s.ua			= require('./inc/ua');
//s.rollbar		= require('rollbar');
s.events		= {};								// Server events are not defined yet; server-router.js (compiled) will define them.

//s.rollbar.init("e6bc7f04b18b4267a378e5863c70957a");

// record a generic message and send to rollbar
//s.rollbar.reportMessage("Server started.");

// Shorthand to export everything in s...
exports = module.exports = s;

// Load all the settings in multiple stages from multiple locations...
s.getSettings = function(repressSettings) {
	
	// Client settings...
	s.client = {
		settings: {}
	}
	
	// Master base settings...
	s.settings = {
		folders: {
			root: s.path.dirname(GLOBAL.process.mainModule.filename) + '/'
		}
	};
	
	// Load the config file (must exist)...
	s.extend(true, s.settings, require('./inc/config.js'));
	
	// Load the app configuration file (must exists)...
	s.extend(true, s.settings, require(s.settings.folders.root + 'app-config.js') );
	
	// Load the server configuration (optional)...
	try { s.extend(true, s.settings, require(s.settings.folders.root + '../server-config.js') ); } catch (e) { s.die('Missing or invalid server-config file: ' + e); }
	
	// Load local.properties if it exists...
	s.extend(true, s.settings,  s.devProperties() );

	// Make sure a human language is defined...
	s.settings.language = s.settings.language || 'en';

	// Emulate any environment variables that are set...
	Object.keys(s.settings.env.vars).forEach( function(key) {
		process.env[key] = s.settings.env.vars[key];
		if (!repressSettings) console.log("ENV Variable set:", key, '=', s.settings.env.vars[key]);
	});
	
	if (!repressSettings) console.log("AGGREGATED SETTINGS FOR THIS ENVIRONMENT\n", s.settings);
};

// Handles local.properties and stuff that happens on development environments...
s.devProperties = function() {
	// Dummy holder...
	var dp = { folders: {} };
	
	// Get the local.properties file and break it apart into an array and subdivide it into objects based on the equals sign...
	s.file('/../local.properties').split('\n').forEach( function(item, index) {
		var kv = item.split('=');
		// If any items match something we're interested in, process it...
		if (kv[0] === 'dev.dir') dp.folders.devwar = s.removeBreaks(kv[1]);
		if (kv[0] === 'prod.dir') dp.folders.war = s.removeBreaks(kv[1]);
	});
	// Return the dummy holder so the settings object can be extended by it...
	return dp;
};

// Remove line breaks and stuff...
s.removeBreaks = function(text) { return text.replace(/(\r\n|\n|\r)/gm, ""); };

// Handle command-line arguments (entry point for Simplex mode router (this file))...
s.run = function(root) {

	// Draw the logo...
	s.logo();
	s.getSettings();

	var mode = process.argv[2];

	if ( !mode ) {
		if ( !s.settings.app.mode ) s.die('You must tell Simplex which mode to run in.  Add a command-line argument (d/p/c for development/production/compile) or edit app-config.js and set this in app.mode');
		mode = s.settings.app.mode;
	} else {
            s.settings.app.mode = mode;
        }

	// Prevent the world from ending because of ignorance (insensitivity to case)...
	mode = mode.toLowerCase();

	// Define modes...
	var modes = {'d': 'development', 'p': 'production', 'c': 'compile', 'a': 'patch'};
	console.log('Using mode', modes[mode]);
	// If a mode has been defined, invoke it...
        if (modes[mode])
			s.mode = require('./' + modes[mode]).run(); // This magic line loads the simplex mode file in the same folder as this one.
	else
		s.die('Invalid argument.  Please use d/p/c for development/production/compile.');

	var totalTime = new Date() - startTime;
	s.log( 'Run time:', totalTime / 1000, 'seconds');
};

// Draws a nifty advanced logo, nice visual breaker instead of manually clearing the screen...
exports.logo = function() {
    var logo =  " _____       _         _____ _         \n"+
                "| __  |___ _| |_ _ _ _|  _  | |_ _ ___ \n"+
                "|    -| -_| . | | |_'_|   __| | | |_ -|\n"+
                "|__|__|___|___|___|_,_|__|  |_|___|___|\n"+
                " Copyright (C) 2016 GSI Health, LLC. All Rights Reserved.";
    console.log(logo);
};

// These can be replaced...
exports.preBoot = function(callback) {callback()};
exports.postBoot = function() {};
exports.errorHandler = function() {
	switch(e.code) {
		case 'EADDRINUSE':
			console.log(e.port + " is already in use.");
			process.exit();
			break;
		default:
			console.log('An unhandled error occured:', + e, ' The server should probably be restarted at this point.');
			break;
	}
};

exports.bootstrap = function() {

	console.log('Patching:', s.settings.app.patching);
	// Hook for preBoot functions...
	s.preBoot(function() {
		s.server.on( 'error', s.errorHandler );
		s.server.listen(
			s.settings.client.vars.nodePort,
			"0.0.0.0",
			function () {
				s.log('\n \033[36m', s.settings.client.vars.name + '\033[0m listening on port ' + s.settings.client.vars.nodePort + '... \n');
				// Hook for postBoot functions...
				s.postBoot();
			});		
	});

};


