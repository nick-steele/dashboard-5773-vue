// ### Simplex Compiler ###

s							= require('../simplex');
s.jade						= require('jade');
s.lz						= require('./inc/lzw');
s.sass						= require('node-sass');
//s.ts						= require('typescript');
s.path						= require('path');
s.compileServices			= require('./inc/compile-services');
s.locals					= {};
s.compiled					= {js: '', css: '', html: '', routers: {}};
s.util						= require('util');


// Wrap the annoyingly complex minification tools in something universally simple for string processing...
s.min = {
    css: require('./inc/min-css').processString,
    js: function(src) {
        var min = require('uglify-js').minify;
        return min(src, {fromString: true}).code;
    }
};

exports = module.exports = s;

exports.run = function() {
    s.fullCompile();
};


// Compress and compile everything...
exports.fullCompile = function() {
	var startTime = new Date();

        // Determine if we're compiling pretty or not... (devmode = pretty, otherwise not)
		getCompileSettings();

		// Clear all compiled buffers...
		s.compiled = {js: '', css: '', html: '', routers: {}};

        s.log('\nServices...');
        s.compileServices();

        s.log('\nServer events...');
        s.compileServerRouter();

        s.log('\nClient events...');
        s.compileClientRouter();

        s.log('\nPush events...');
        s.compilePushRouter();

        s.log('\nModules...');
        s.compileModules();

		// Compile the static states...
        s.log('\nStates...');
		s.compileStates();

		// Compile everything related to JavaScript into client.js...
        s.log('\nBuilding app.js...');
		s.compileJS();

		// Compile each CSS file into client.css...
        s.log(s.color('green') + '\nBuilding app.css...' + s.color() );
		s.compileCSS();

		// Compile the app HTML file into /index.html...
        s.log(s.color('green') + '\nBuilding index...' + s.color() );
		s.compileHTML();

		// Compress everything...
		s.log('\nCompressing everything...');
		s.compressEverything();

	// We're done!
	var compileTime = new Date() - startTime;
	s.log('\nCompile successful!', compileTime / 1000, 'seconds');
};

// Sets up settings used by the compiler only (so they aren't added to the main index)...
function getCompileSettings() {
	s.pretty = s.settings.app.mode === 'd' ? true : false;
	s.warfolder = (s.settings.app.mode === 'd') ? s.settings.folders.devwar : s.settings.folders.war;
	if (typeof s.warfolder === 'undefined') s.warfolder = 'p/';
	console.log (s.warfolder);

	// Determine if things should be minified...
	s.settings.min = s.settings.modes[s.settings.app.mode].min;
};

// Creates /s/compiled/router.js...
s.compileServerRouter = function () {
	var js = '';
	var routeTable = '';

	// Build a list of every event except connect...
	s.folders( s.settings.folders.root + s.settings.folders.events + 'server/' ).forEach( function(name) {
		if ( name !== 'connect') {

			// s.log what we're doing...
			s.log(' ' + name);

			// Get the js source...
			src = s.file( s.settings.folders.events + 'server/' + name + '/' + name + '.js' );

			// Tab-indent it by one level (to indicate it's inside the connect event)...
			dest = '\t' + src.replace(new RegExp('\n', 'g'), '\n\t\t');

			// Drop the contents of this event with a SocketIO event function wrapper of the same name, and add a tab indent...
			js			+=	'\tsocket.on("' + name + '", s.events.' + name + ');\n';
			
			// Add the code to the route table:
			routeTable	+=	'\t' + name + ': function(data) {\n' +
							'\t\ts.logger.start(socket, "' + name + '", data);\n' +
							dest + '\n' +
                            '\t\ts.logger.trace("nr");' +
                            '\t\ts.eventRoute( "' + name + '", data);' +
							'\t\ts.logger.done();' + '\n'+
							'\n\t},\n';
		}
	});

	// Every strategy is loaded inside the connect function, build the router header accordingly...
	var connect =	'// ### Compiled file (This will change with every compile; source files are in s/src/events/server)\n'+
						's = require("../../s/src/simplex");\n' +
						'console.log("\\nServer Event Router Loaded");\n\n' +
                        's.io.on("connect", function(socket) {\n' +
                        '\tsocket.logId = s.logger.sessionSignature();\n' +                        
						'\ts.logger.start(socket, "connect");\n' +
						'\tvar trace = s.logger.trace;\n' +
						'\tvar my = socket.clientData;\n' +
						'\tvar send = function(name, data) { s.logger.trace("send:" + name);socket.emit(name, data); };\n' +
						'\tvar event = function(name, data) { s.logger.trace("event:" + name);s.event(name, data); };\n' +
						'\tvar service = function(name, data, success, fail) {\n' +
						'\t\ts.service(name, socket, data, success, fail);\n' +
						'\t};\n\n' +
						's.events = {\n' + routeTable + '};\n\n' +
						s.file( s.settings.folders.events + 'server/connect/connect.js' ) + '\n' +
						'\ts.logger.done();' +
						'\n\n';

	s.log('writing server event router to', s.settings.folders.root + s.settings.folders.compiled + 'server-router.js');
	// Write the strategy router...
        js = connect + js + '});\n\n';

        // Save the file...
	s.write( s.settings.folders.compiled + 'server-router.js', js );
	// Don't minify the server code because it's not exposed or transferred...
        //s.write( s.settings.folders.compiled + 'server-router.min.js', s.min.js(js) );
};

s.compileClientRouter = function() {
	var js = '';
        var events = 0;

	// Every strategy is loaded inside the connect function, build the router header accordingly...

	// Build a list of every event except connect...
	s.folders( s.settings.folders.root + s.settings.folders.events + 'client/' ).forEach( function(name) {
            // s.log what we're doing...
            s.log(' ' + name);

            // Get the js source...
            src = s.file( s.settings.folders.events + 'client/' + name + '/' + name + '.js' );

            // Tab-indent it by one level (to indicate it's inside the connect event)...
            dest = '\t\t' + src.replace(new RegExp('\n', 'g'), '\n\t\t');

            // Add commas if an event's been defined yet...
            if (events > 0) comma = ', '; else comma = '\t';
            events++;

            // Drop the contents of this event with a SocketIO event function wrapper of the same name, and add a tab indent...
            js +=   comma + name + ':\t function(what, data) {\n' +
                    dest + '\n\t}';
	});

	s.log('writing client event router to', s.settings.folders.root + s.settings.folders.compiled + 'client-router.js');
        js = '{\n' + js + '\n}';

        // Compile the regular and compressed versions...
	s.write( s.settings.folders.compiled + 'client-router.js',  js );
	s.compiled.routers.client = js;

        // Don't minify the client router because we're going to build it into AppController.js and minify that (the below will currently break anyway)
	//s.write( s.settings.folders.compiled + 'client-router.min.js', s.min.js(js) );
};

s.compilePushRouter = function() {
	var js = '';
    var events = 0;

	// Build a list of every event except connect...
	s.folders( s.settings.folders.root + s.settings.folders.events + 'push/' ).forEach( function(name) {
            // s.log what we're doing...
            s.log(' ' + name);

            // Get the js source...
            src = s.file( s.settings.folders.events + 'push/' + name + '/' + name + '.js' );

            // Tab-indent it by one level (to indicate it's inside the connect event)...
            dest = '\t\t' + src.replace(new RegExp('\n', 'g'), '\n\t\t');

            // Add commas if an event's been defined yet...
            if (events > 0) comma = ', '; else comma = '\t';
            events++;

            // Drop the contents of this event with a SocketIO event function wrapper of the same name, and add a tab indent...
            js +=   comma + name + ':\t function(what, data, trace) {\n' +
                    dest + '\n\t}';
	});

	s.log('writing push event router to', s.settings.folders.root + s.settings.folders.compiled + 'push-router.js');
    js = '{\n' + js + '\n}';

        // Compile the regular and compressed versions...
	s.write( s.settings.folders.compiled + 'push-router.js',  js );
	s.compiled.routers.push = js;
};

// Compiles the push router string (Uses eval, for anyone that wants to hate it :)
s.compilePushRouterOld = function() {
	var js = '';
    var events = 0;
	var defaultEvent;

	// Build the header of the evaluated function...

	var header =	"// This file is generated. Do not edit.\n"+
					"function eventServer(what, data) {\n"+
					"\tvar firstKeyz = function(obj) {\n"+
					"\t	for (var key in obj)\n"+
					"\t		if (obj.hasOwnProperty(key)) break;\n"+
					"\t		return key;\n"+
					"\t};\n\n"+
					"\tvar firstVal = function(obj) {return obj[Object.keys(obj)[0]];};\n\n"+
					"\tswitch (what) {\n";


	// Every strategy is loaded inside the connect function, build the router header accordingly...

	// Build a list of every event except connect...
	s.folders( s.settings.folders.root + s.settings.folders.events + 'push/' ).forEach( function(name) {
            // s.log what we're doing...
            s.log(' ' + name);

            // Get the js source...
            src = s.file( s.settings.folders.events + 'push/' + name + '/' + name + '.js' );

            // Tab-indent it by one level (to indicate it's inside the connect event)...
            dest = '\t\t\t' + src.replace(new RegExp('\n', 'g'), '\n\t\t\t');

            events++;

            // Drop the contents of this event with a SocketIO event function wrapper of the same name, and add a tab indent...
            if (name === 'default') {
				defaultEvent = '\t\tdefault:\n' + dest + '\n\t\t\tbreak;\n';
			} else {
				js +=   '\t\tcase "' + name + '":\n' + dest + '\n\t\t\tbreak;\n';
			}
	});

	if (defaultEvent) js += defaultEvent;

	s.log('writing push event router to', s.settings.folders.root + s.settings.folders.compiled + 'push-router.js');
    js = header + js + '\t}\n}';

        // Compile the regular and compressed versions...
	s.write( s.settings.folders.compiled + 'push-router.js',  js );
	s.compiled.routers.push = js;
        // Don't minify the client router because we're going to build it into AppController.js and minify that (the below will currently break anyway)
	//s.write( s.settings.folders.compiled + 'client-router.min.js', s.min.js(js) );
};

// Sets up in-memory infrastructure for the specks (so that when they are called by a state, they can be included in app.css and app.js)...
s.processSpecks = function() {
	// This is used for pre-compiling specks into app states...
	s.dom = {
		count		: 0,
		prefix		: 'speck',
		speckList	: [],
		speck		: function( name ) {
						// Add unique names onto the css list when called...
						//s.log('Checked-in Speck:' + name, this.speckList.indexOf( name ));
						if ( this.speckList.indexOf( name ) === -1 ) this.speckList.push ( name );
					},
		nextID		: function() { return this.prefix + this.count++; }
	};

	/* TODO: Remove shortcodes
	// Add all installed specks (shortcodes that automatically add their own js/css dependancies to the compiled script, and also inject HTML each time used)...
	s.fs.readdirSync( s.settings.folders.root + '/s/src/specks/').forEach( function( file ) {
		var name = file.substr(0, file.lastIndexOf('.') );
		if ( name === '' ) return;  // Don't do folders
		s.shortcode.add( name, require( s.settings.folders.root + '/s/src/specks/' + file ) );
		//s.log( 'Added speck "' + name + '"' );
	});
	*/
};

// Builds the states that are ultimately pushed to the client as needed...
s.compileStates = function() {

	// clear memory states...
	s.settings.states = [];

	// Make sure s/compiled/states/ exists...
	console.log('ensuring ', s.settings.folders.root + s.settings.folders.compiled + 'states/');
    s.ensurePath(s.settings.folders.root + s.settings.folders.compiled + 'states/');

	// Add all installed app states (states are automatically added if they are placed in s/src/states/)...
	s.folders( s.settings.folders.root + s.settings.folders.source + 'states/' ).forEach( function(name) {
		var path = s.settings.folders.source +  'states/' + name;
		var html = s.file( path + '/index.html' );
        var fullPath = s.settings.folders.root + s.settings.folders.source + 'states/' + name + '/';

            // Allow Jade to compile if there is no HTML version...
            if (html.length === 0) {
                var fileName = fullPath + 'index.jade';
                try {
                    html = s.jade.compileFile( fileName, { filename: fileName, pretty: s.pretty } )( s.settings );
                }
                catch(e) {
                    // If the file doesn't exist in jade, it's fine, so don't report it...
                    if ( e.code !== 'ENOENT' ) console.log('Could not compile jade file', fileName, '(' + e + ')');
                }
            }

		var css = s.file( path + '/index.css' );

            // Allow Sass to compile if there is no css version...
            if (css.length === 0 && s.isFile(fullPath + 'index.scss') ) {
                var fileName = fullPath + 'index.scss';
                try {
                    css = s.toSass(fileName);
                }
                catch(e) {
                    console.log('Could not compile Sass file', fileName, '(' + e + ')');
                }
            }

		var js = s.file( path + '/index.js' );
                var ctrl = s.file( path + '/state.js' );
                if (ctrl.length > 0 ) ctrl = JSON.parse(ctrl);
		var state = { name: name };
		if (html.length > 0) {
			// Parse any shortcodes...
			//html = s.shortcode.parse(html);
			state.h = html;
		} else { // Check for Jade...
			html = s.file( path + '/index.jade' );
			// If there is Jade...
			if (html.length > 0) {
				// Parse shortcodes...
				//html = s.shortcode.parse(html);
				// Parse the Jade...
				var html = s.jade.compile( html, { pretty: s.pretty } )( s.settings.app );
				// Add it to the frame...
				state.h = html;
			}
		}
                // Check for additional information...
                if (ctrl) state.x = ctrl;

		if (css.length > 0) state.c = css;
		if (js.length > 0) state.j = js;

		// Display each state in a nice looking format...
		var showName = name + '            '.substring(0, 12 - name.length);
		s.log('\033[32m', showName, '\033[0m', 'h ' + html.length + '\tc ' + css.length + '\tj ' + js.length);
		var compressed = s.lz.compressToBase64( JSON.stringify( state ) );
		s.write( s.settings.folders.compiled + 'states/' + name + '.state', compressed);
		// Write the state to memory too...
		s.settings.states += name + ':' + '"' + compressed + '",';
	});
};

// Wrapper to prettify the sass compiler...
s.toSass = function(file) { return s.sass.renderSync({ file: file }).css.toString('utf8'); }

// Returns the variables definition (window.client)...
s.compileJSVariables = function(modules) {
    // Create the client var of installed angular dependancies and variables we need to pass to the client.

    // Tab-indent the vars by one level (to indicate it's inside the vars object)...
    var vars = ' ' + JSON.stringify(s.settings.client.vars, null, 2).replace(new RegExp('\n', 'g'), '\n\t');

    var angularDependancies = [];
    // Pull out Angular modules...
    s.settings.modules.forEach( function ( module ) { if ( module.type === 'angular' ) angularDependancies.push(module.name); });

	// Acquire the application settings for the UI...
	s.compiled.settings = require(s.settings.app.webSettings);
	
    var quote = '';
    if ( angularDependancies.length ) quote = '"';

    var modQuote = '';
    if ( modules.length ) modQuote = '"';

    var client =	'var client = {\n' +
                        '\tdependancies: ['+ quote + angularDependancies.join('","') + quote +'],\n' +
                        '\tstates: {' + s.settings.states.replace(/,\s*$/, "") + '},\n' +
                        '\tmodules: ['+ modQuote + modules.join('","') + modQuote +'],\n' +
						'\trouters: {\n'+
						'\t\tpush: "' + s.lz.compressToBase64(s.compiled.routers.push) + '",\n'+
						'\t\tclient: "' + s.lz.compressToBase64(s.compiled.routers.client) + '"\n'+
						'\t},\n'+
                        '\tvars:' + vars + ',\n'+
						'\tsettings: ' + JSON.stringify(s.compiled.settings) + '\n'+
                        '};\n';
    // Minify it if we're supposed to...
    if (s.settings.min) client = s.min.js(client);
    return client;
};

// ### Creates /p/app.js
s.compileJS = function(hide) {

    // Add JS for modules...
    var js = {
        system      : {order: [], code: ''},
        core        : {order: [], code: ''},
        standard    : {order: [], code: ''},
        angular     : {order: [], code: ''},
        controller  : {order: [], code: ''},
        main        : {order: [], code: ''},
        app         : {order: [], code: ''}
    };

    // Sort the modules by their order...
    s.settings.modules.sort( s.sortBy('order', false, parseInt) );

    // Build up the JS code queue...
    s.settings.modules.forEach( function( module ) {
        js[module.type].code += module.js.compiled;
        js[module.type].order.push(module.id);
    });

    // drop the sorted module arrays in the right order...
    js.modules = [].concat(js.system.order).concat(js.core.order).concat(js.standard.order).concat(js.angular.order);

    js.vars = s.compileJSVariables(js.modules);



    // Drop the code in the right order...
    s.compiled.js = js.vars + js.system.code + js.core.code + js.standard.code + js.angular.code + js.main.code + js.controller.code + js.app.code;

	// Write the final client JavaScript...
	s.announceAndWrite(s.warfolder + "/app.js", s.compiled.js);
};

// Announces and writes a file...
s.announceAndWrite = function(filename, contents) {


	console.log('Writing ' + filename + '...');
    s.fs.writeFileSync(filename, contents);
};

// Gets a list of all active modules...
s.modulesActive = function() {
    // Clear our modules list...
    s.settings.modules = [];

    // Loop through the modules directory and load each module.json file...
    s.getFolders( s.settings.folders.root + s.settings.folders.modules + 'enabled/' ).forEach( function( file ) {
        // Get the file contents...
        //console.log('parsing',s.settings.folders.modules + file);
        try {
            var module = JSON.parse( s.file( s.settings.folders.modules + 'enabled/' +  file + '/module.json' ) );
        } catch(e) {
            console.log("Couldn't parse " + s.settings.folders.root + s.settings.folders.modules + file + "/module.json. Make sure the file exists and is formatted correctly or remove the module.");
            process.exit();
        }

        // Set defaults...
        if (typeof module.checksum  === 'undefined') module.checksum = '';
        if (typeof module.revision  === 'undefined') module.revision = 1;
        if (typeof module.type      === 'undefined') module.type = 'standard';
        if (typeof module.version   === 'undefined') module.version = '0.0.1';
        module.id = file;

        // If the module is enabled, add it to our list to process...
        module.enabled = true;
        if (typeof module.enabled !== 'undefined') if (module.enabled) s.settings.modules.push(module);
    });
};

// Compile all modules...
s.compileModules = function() {
    // Get all active modules...
    s.modulesActive();
    // Go through each module and compile it...
    s.settings.modules.forEach( function ( module ) { s.compileModule(module); });
};

// Compiles a module
s.compileModule = function( module ) {

    var path = s.settings.folders.modules + 'enabled/' + module.id + '/';
    // Get the source code...
    var src = {
        js: s.concatFolderContents(module.id, path + 'js/')
    };

    // Handler for Sass...
    var sassFile = s.settings.folders.root + path + 'scss/index.scss';
    if ( s.isFile( sassFile ) ) {
        src.css = s.toSass( sassFile );
    // No Sass, so just concatenate the contents of a possible css/ folder...
    } else {
        src.css = s.concatFolderContents(module.id, path + 'css/');
    }

    // Special additional preprocessing for controller modules...
    if (module.type === 'controller') {
        var data = '';
        var variable = ''
        // Process injection requests...
        if (module.inject) {
            module.inject.forEach(function(inject) {
                switch (inject) {
                    case 'events-inject':
                        // Load the client router...
                        data = s.file( s.settings.folders.compiled + 'client-router.js' );
                        variable = 'my.events';
                        src.js += variable + '=' + data + ';'
                        break;
                }
            });
        }
        src.js  = "app.controller('" + module.id + "', function(" + module.dependancies + ") {" +
                  "var my = this;" +
                  "$model.store('" + module.name + "', $scope);" +
                  "if (typeof boot == 'function') boot();" +
                  src.js +
                  "});";
    }

    // Store the source and generate UIDs (for cache hits to avoid recompiling)...
    module.css = { src: src.css, uid: s.uid( src.css ) };
    module.js = { src: src.js, uid: s.uid( src.js ) };

    // Build the file and folder targets for easy reading below...
    var folder = s.settings.folders.compiled + 'modules/' + module.id + '/';
    var cssFile = folder + 'css/' + module.css.uid + '.css';
    var jsFile = folder + 'js/' + module.js.uid + '.js';

    // Colors for cache hits...
    var cssDisp = {color: s.getColor('bug'), text: 'unchanged'};
    var jsDisp  = {color: s.getColor('bug'), text: 'unchanged'};

    // If we're minifying, use a cache to speed things up (delete s/compiled/modules to clear the cache)...
    if (s.settings.min && module.type !== 'controller') {

        // Try to get the disk version of the compiled file...
        module.css.compiled = s.file( cssFile );
        module.js.compiled  = s.file( jsFile );

        // If they are missing but not empty, wipe out the folder (any current compiled version) and compile them...
        if ( !module.js.compiled && module.js.uid !== 'd41d8cd98f00b204e9800998ecf8427e' ) {
            s.ensurePath(s.settings.folders.root + folder + 'js');
            jsDisp = {color: s.getColor('protocol'), text: 'modified '};
            s.emptyFolder(s.settings.folders.root + folder + 'js/');
            module.js.compiled = s.min.js( module.js.src);
            s.write( jsFile, module.js.compiled );
        }
        if ( !module.css.compiled && module.css.uid !== 'd41d8cd98f00b204e9800998ecf8427e' ) {
            s.ensurePath(s.settings.folders.root + folder + 'css');
            cssDisp = {color: s.getColor('protocol'), text: 'modified '};
            s.emptyFolder(s.settings.folders.root + folder + 'css/');
            module.css.compiled = s.min.css( module.css.src );
            s.write( cssFile, module.css.compiled );
        }
    } else {
        // Nothing to do really...
        module.css.compiled = module.css.src;
        module.js.compiled  = module.js.src;
    }
    // Explain what we did...
    console.log (jsDisp.color + jsDisp.text + s.getColor() + ':' + cssDisp.color + cssDisp.text + s.getColor() + ':' + module.id);
};


// Returns an array of all the directories in a folder...
s.getFolders = function(src) {
  return s.fs.readdirSync(src).filter(function(file) {
    return s.fs.statSync(s.path.join(src, file)).isDirectory();
  });
};

// Compiles an Angular controller from a Simplex controller module folder...
s.compileController = function() {

};

s.compileApp = function() {

	// Add everything in /s/src/js/active/includes/
        //s.compiled.js += s.concatFolderContents('include', s.settings.folders.ngApp + 'includes/');

        // Add functions...
	//s.compiled.js += s.concatFolderContents('Function', s.settings.folders.ngApp + 'functions/' );

	// Add the main app code...
	//s.compiled.js += s.file( s.settings.folders.app + 'js/app.js' );

	// Add all filters...
	//s.compiled.js += s.concatFolderContents('Filter', s.settings.folders.ngApp + 'filters/' );
	// Add all services...
	//s.compiled.js += s.concatFolderContents('Service', s.settings.folders.ngApp + 'services/' );
	// Add all directives...
	//s.compiled.js += s.concatFolderContents('Directive', s.settings.folders.ngApp + 'directives/' );

        // Add AppController...
        //var AppController = s.file(s.settings.folders.ngApp + 'AppController/AppController.js' );
        //var functions = s.concatFolderContents('AppController', s.settings.folders.ngApp + 'AppController/functions/' );
        //var AppControllerEvents = s.file( s.settings.folders.compiled + 'client-router.js' );

        //var AppControllerCompiled = AppController + functions + 'client.events = ' + AppControllerEvents + ';\n});';

        //console.log('Writing AppController.js to', s.settings.folders.root + s.settings.folders.compiled + 'AppController.js');
        //s.write( s.settings.folders.compiled + 'AppController.js', AppControllerCompiled);
        //s.compiled.js += AppControllerCompiled;

	// Add all controllers...
	//s.compiled.js += s.concatFolderContents('Controller', s.settings.folders.ngApp + 'controllers/' );
};

// Returns the concatenated contents of a folder...
s.concatFolderContents = function(what, folder) {
	var concat = '';
        try {
            s.fs.readdirSync( s.settings.folders.root + folder).forEach( function( file ) {
                    contents = s.file( folder + file );
                    concat += contents + '\n';
                    //s.log( 'Added ' + what + ' "' + file + '"');
            });
        } catch(e) {
            // If the folder doesn't exist, return an empty string (no folder returns no content)...
            if (e.code === 'ENOENT') return '';
            console.log(e);
            process.exit();
        }
	return concat;
};

s.compileCSS = function() {

	s.compiled.css = '';
	// Handle all css for specks that are actually accessable...
	//s.log(' Specks...');
	//s.dom.speckList.forEach( function(name) { s.compiled.css += s.file( s.settings.folders.source + 'specks/' + name + '/include.css'); });

	// Handle all CSS requested by the modules...
	s.log(' Application CSS...');
        // Add JS for modules...
        s.settings.modules.forEach( function( module ) { s.compiled.css += module.css.compiled; });

	// Compress css...
	s.announceAndWrite(s.warfolder + "/app.min.css", s.min.css(s.compiled.css));

	// Write the almighty compiled client stylesheet...
	s.announceAndWrite(s.warfolder + "/app.css", s.compiled.css);
};

s.compileHTML = function() {
	// Jade handles all includes in this case...

    var hosts = "window.QA.hostmap = " + JSON.stringify(s.compiled.settings.hosts)+ ";";
    hosts += "window.QA.tenantId = window.QA.hostmap[window.location.hostname].id;";
    var _settings = s.settings;
    _settings.hostmap = hosts;
    s.compiled.html = s.jade.compileFile( s.settings.folders.root + s.settings.folders.wrappers + '/' + s.settings.app.wrapper + '/index.jade', { pretty: s.pretty } )( _settings );
	// Write the almighty compiled index.html...
    s.announceAndWrite(s.warfolder + s.settings.app.index, s.compiled.html);
};

s.compressEverything = function() {
	// Take the client files and compress them...
        //s.uglify.minify(s.warfolder + 'app.js');
};
