// ### Simplex Development Mode Manager ###

s							= require('../simplex');
//s.engine					= require('express');
s.fs 						= require('fs');
s.path						= require('path');
//s.con						= require("mysql");
//s.compress				= require('compression'); // For g-zipping stuff express hosts...
s.watcher					= require('chokidar');
s.compile					= require('./compile');
//s.app						= s.engine();
//s.server					= require('http').Server(s.app);
//s.io 						= require('socket.io')(s.server);

exports = module.exports = s;

exports.run = function() {

	// Server setup...
	//s.app.use(s.compress());

	//s.app.use('/', s.engine.static(s.settings.folders.root + 'p'));

  // static route anything requested to the public folder...

  /* TODO: Remove
  s.app.get('/API/*', function ( req, res ) {
     res.writeHead(200, {'Content-Type' : 'text/html'});
		 res.end("OK to upgrade url " + req.url);
  });
*/

	// Create minimal web server that we can upgrade to WebSocket...
	s.server = s.http.createServer(function (req, res) {
		 switch (req.url) {
			case '/foo':
				res.writeHead(200, {'Content-Type' : 'text/html'});
				res.end("FOO");
				break;
			case '/test':
				res.writeHead(200, {'Content-Type' : 'text/html'});
				res.end("TEST");
				break;
			default:
				res.writeHead(200, {'Content-Type' : 'text/html'});
				res.end("OK to upgrade url " + req.url);
				break;
		 }
	});

	// Listen for WebSocket connections...
	s.io = require('socket.io').listen(s.server);

	// Setup file watchers...
	s.watching = s.watcher.watch( s.settings.folders.root + s.settings.folders.source, { persistent: true } );

	// When a file changes, back it up and tell the client to reload (partially if possible, otherwise entirely)...
	s.watching.on('change', fileChanged);

	// Make sure we're using a freshly compiled system...
	s.compile.fullCompile();

	// Load the compiled strategy router...
	require(s.rootFolder('compiled') + 'server-router');

  // Load final configuration information and start listening for connections...
  s.bootstrap();

	// Return true to the console if we're in a batch file...
  return true;
}

// When a file has changed...
function fileChanged( path, stats ) {
	// Time-machine backup...
	s.timeMachine(path, stats);

	s.log('Source change:', path);

	// Recompile the part of the app affected (TODO: make this much more effective)...
	if (path.indexOf('css') > 0 || path.indexOf('scss') > 0) {
		s.log('Recompiling CSS.');
		s.settings.app.logging = false;
		s.compile.compileModules();
		s.compile.compileCSS();
		s.settings.app.logging = true;
		s.log('Hot reloading.');
		s.io.emit('patch', { what: 'css' });
	// Recompile push events...
  } else if (path.indexOf('events\\push') > 0) {
		// Send it to all connected clients...
		s.io.emit('patch', { what: 'event', where: 'push', name: 'poo', payload: '"path"' } );
	// Recompile server events...
	} else if (path.indexOf('events\\server') > 0) {
		s.log('Recompiling Server Event Router.');
		s.settings.app.logging = false;
		s.compile.compileServerRouter();
		s.settings.app.logging = true;
    // Load the compiled strategy router...
    require('../../compiled/server-router');
  // Just recompile the states...
  } else if (path.indexOf('src\\states') > 0) {
    s.compile.fullCompile();
		s.io.emit('patch', {what: 'reboot'} );
  } else {
		// Recompile everything without details...
		s.log('Recompiling app.');
		s.settings.app.logging = false;
		s.compile.fullCompile();
		s.settings.app.logging = true;
		// Tell all clients to refresh...
		s.log('rebooting clients.');
		s.io.emit('patch', {what: 'reboot'} );
	};
}

s.timeMachine = function(path) {

		// Remove invalid chars from the file name (TODO: make this a single regexp)
		var backup = path.replace(/\\/g, '_');
		backup = backup.replace(/\//g, '_');
		backup = backup.replace(/\:/g, '');

		// Add current unix epoch in miliseconds...
		var time = (new Date).getTime();

		// Copy the file...
		s.fs.createReadStream(path).pipe(s.fs.createWriteStream(s.settings.folders.root + s.settings.folders.backup + time + backup));
};
