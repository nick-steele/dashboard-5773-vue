// Called on boot...
function boot() {
    // Store our own reference to devmode...
    var vm = this;
    calendarConfig.templates.calendarWeekCell = 'customWeekCell.html';
    calendarConfig.templates.calendarDayItem = 'customDayItem.html';
    calendarConfig.templates.calendarMonthCell = 'customCalendarMonthCell.html';
    var taskCtrl = $model.get('task');
    calendarConfig.filter = taskCtrl.filter;
    calendarConfig.status = taskCtrl.status;
    calendarConfig.searchStr = taskCtrl.search.time;
    my.updateSarchString = function (str) {
            calendarConfig.searchStr = str;
    }
    //console.log(this.filter);
    //vm.events = [];
    //vm.calendarView = 'month';
    //vm.viewDate = moment().startOf('month').toDate();

    //$scope.$on('$destroy', function() {
    //  calendarConfig.templates.calendarMonthCell = 'mwl/calendarMonthCell.html';
    //});
   
};