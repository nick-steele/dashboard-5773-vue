// An application event factory... 2nd pass (first was to use window.QA.app.event)
app.factory('$appEvent', function ($model) {
	return function (eventName, dataOrCallback, tracer) {
			$model.get('app').event(eventName, dataOrCallback, tracer)
	};
});
