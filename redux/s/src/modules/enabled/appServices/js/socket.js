// An IO socket factory for Angular...
app.factory('socket', function ($rootScope) {
	//var socket = io.connect();
        QA.nodePort = window.client.vars.nodePort;
	QA.protocol = location.protocol;
	// Connect on the same security level as the page (WS or WSS)...
        QA.wsProtocol = QA.protocol;
        var wsURL = QA.wsProtocol + '//' + QA.server;
        // Connect with a port number if there is a port number defined (not in DEV)...
        if (QA.port !== "80") wsURL += ':' + QA.nodePort;
	var socket = io(wsURL, {transports: ['websocket']});
	
	// Allow catching * events... 
	var onevent = socket.onevent;
	socket.onevent = function (packet) {
		var args = packet.data || [];
		onevent.call (this, packet);    // original call
		packet.data = ["*"].concat(args);
		onevent.call(this, packet);      // additional call to catch-all
	};
	
	return {
	on: function (eventName, callback) {
	  socket.on(eventName, function () {  
		var args = arguments;
		$rootScope.$apply(function () { callback.apply(socket, args); });
	  });
	},
	emit: function (eventName, data, callback) {
	  socket.emit(eventName, data, function () {
		var args = arguments;
		$rootScope.$apply(function () { if (callback) callback.apply(socket, args); });
	  })
	}
	};
});