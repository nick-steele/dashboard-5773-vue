app.factory('taskBadgeFilter', function (constants) {
    var data = {};
    data.isFiltered = false;

    return {
        data: data,

        set: function (badgeType, currentMember, model, isMyTeam) {
            data.descriptionText = '';
            data.isFiltered = true;
            data.isMyTeam = isMyTeam;
            switch(badgeType) {
                case "overdue":
                    data.badgeValue = model.overdueTask;
                    //overdue tasks only be calculated for today
                    data.badgeValue.interval = 0;
                    data.descriptionText += model.taskBadgeDescription.overdue.capitalize();
                    data.taskType = 'tasksoverdue';
                    break;
                case "todo":
                    data.badgeValue = model.todoTask;
                    data.descriptionText += model.taskBadgeDescription.due.capitalize();
                    data.taskType = 'taskstodo';
                    break;
                case "careplan":
                    data.badgeValue = model.cpNonassessTask
                    data.descriptionText += model.taskBadgeDescription.careplan.capitalize();
                    data.taskType = 'taskscpnonasses';
                    break;
                case "assessments":
                    data.badgeValue = model.cpAssessTask;
                    data.descriptionText += model.taskBadgeDescription.assessment.capitalize();
                    data.taskType = 'taskscpasses';
                    break;
                case "noBadgeFilter":
                    data.isFiltered = false;
                    data.taskType = undefined;
                    data.supervisorFlag = true;
                    break;
            }

            data.email = currentMember.email;
            data.firstName = currentMember.firstName;
            data.lastName = currentMember.lastName;

            var text = '';
            switch (isMyTeam) {
              case constants.VIEW_TYPE.MY_VIEW:
                text = data.lastName + ", " + data.firstName + "'s Task Page";
                break;
              case constants.VIEW_TYPE.TEAM_VIEW:
                text = "My Team Members' Task Page";
                break;
              case constants.VIEW_TYPE.TEAM_VIEW_ONE:
                text = data.lastName + ", " + data.firstName + "'s Task Page";
                break;
              case constants.VIEW_TYPE.TEAM_VIEW_SOME:
                text = "My Selected Team Members' Task Page";
                break;
              default:
                break;
            }

            if (data.isFiltered === true) {
                //var text = data.isMyTeam === true ? 'My Selected Team Members' : data.lastName + ', ' + data.firstName + "'s Task Page";
                data.descriptionText += " " +
                (data.badgeValue.interval === 0 ? "Today" : "Next " + data.badgeValue.interval + " days") +
                 " for " + text;
                data.descriptionText = 'Showing ' + data.descriptionText;
            } else {
              data.descriptionText += text;
            }

            /*else if (data.isMyTeam === true) {
                data.descriptionText += text;
            } else {
                data.descriptionText += data.lastName + ', ' + data.firstName + "'s Task Page";
            }*/
        },

        clear: function() {
            data.badgeValue = undefined;
            data.descriptionText = undefined;
            data.taskType = undefined;
            data.isFiltered = false;
            data.email = undefined;
            data.isMyTeam = undefined;
        }
    };
});
