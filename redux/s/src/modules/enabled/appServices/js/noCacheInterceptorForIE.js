// We ONLY need this for IE11... It caches XHR otherwise, which is invalid, but it does it.
app.config(['$httpProvider', function($httpProvider) {
    if (!$httpProvider.defaults.headers.get) $httpProvider.defaults.headers.get = {};    
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
}]);