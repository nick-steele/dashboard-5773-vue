app.service('gsiCppWidget', 
  function() {
    
    this.getCcpWidgetSortFields = function(chartType, chartLabel) {
    	var fields = [];

    	chartType = chartType.toLowerCase();
    	chartLabel = chartLabel.toLowerCase();
    	switch(chartType) {
    		case 'issue': 
    			switch(chartLabel) {
    				case 'status' :
    					fields.push('label2');
    					break;
    				case 'domain' :
    					fields.push('label1');
    					break;
    				case 'numberperpatient' :
    					fields.push('label2')
    					break;
    			}
    			break;
    		case 'goal':
    			switch(chartLabel) {
    				case 'status' :
    					fields.push('label1');
    					break;
    				case 'numberperpatient' :
    					fields.push('label1')
    					break;
    			}
    			break;
    		case 'intervention':
    			switch(chartLabel) {
    				case 'status' :
    					fields.push('label1');
    					break;
    				case 'numberperpatient' :
    					fields.push('label1')
    					break;
    			}
    			break;
    		case 'assessment':
    			fields.push('label1');
    			fields.push('label3')
    			break;
    	}
       	return fields;
    };

    this.getCcpSortLabel = function(chartLabel, inputLabel, priorityStatusList) {
		var sortLabelValues = [];
    	if (chartLabel.toLowerCase() === 'numberperpatient' && priorityStatusList !== undefined && priorityStatusList != '') {
			return priorityStatusList.split('|');
    	}
    	else {
    		return inputLabel;
    	}
    }
  }
)