app.service('constants', (function() {
    function isIE(userAgent) {
        userAgent = userAgent || navigator.userAgent;
        return userAgent.indexOf("MSIE ") > -1 || userAgent.indexOf("Trident/") > -1 || userAgent.indexOf("Edge/") > -1;
    }

    var isIE = isIE();

    return {
        VIEW: {
            MY_VIEW_DISPLAY_TEXT: 'My View’s Data',
            MY_ORG_VIEW_DISPLAY_TEXT: 'My Org’s Data',
            MY_TEAM_VIEW_SOME_DISPLAY_TEXT: 'My Selected Team Member’s Data',
            MY_TEAM_VIEW_DISPLAY_TEXT: 'My Team’s Data',

            getUserViewDisplayText: function (firstName, lastName) {
                return lastName + ', ' + firstName + '’s Data';
            }
        },
        VIEW_TYPE: {
          MY_VIEW: 1,
          ORG_VIEW: 2,
          TEAM_VIEW: 3,
          TEAM_VIEW_ONE: 31,
          TEAM_VIEW_SOME: 32
        },

        isIE: isIE,

        ENROLLMENT: {
            WIZARD: {
                ORG: {
                    ID: 1
                },
                RHIO: {
                    ID: 2
                },
                PROGRAM: {
                    ID: 3
                },
                PROVIDER_ACCESS: {
                    ID: 4
                },
                CARE_TEAM: {
                    ID: 5
                },
                ADVANCED: {
                    ID: 6
                }
            }
        }
    }
}));
