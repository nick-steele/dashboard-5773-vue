// A Simplex event factory for Angular (abstraction for WebSocket and evented systems)...
app.factory('event', function ($rootScope, socket) {
    // Split the event into it's parts
    var handleEvent = function (event, data) {
        try {
            var parts = event.split(':');
        } catch(error) {
            console.log('Malformed or missing event name: ' + event);
        }
        
        // Make it easier to read below...
        var location = parts[0];
        var what = parts[1];
        var data = data ? data : parts[2];
        
        // Represent the data for the log (console)...
        datalog = (typeof parts[2] === 'undefined') ? data : parts[2];
        if (dataLog) {
            if (dataLog.length > 50) dataLog = dataLog.length + ' bytes';
                else dataLog = '"' + dataLog + '"';					
        }
        
        // Handle local and remote events differently...
        switch (location) {
            case 'local':
                // Notify the console...
                console.log('<-GUI EVENT-local', what + '(' + dataLog + ')');
                // Call the event...
                events[what](data);
                break;
            case 'remote':
                GWT.activity();
                // Notify the console...
                console.log('APP-> EVENT-remote', what + '(' + dataLog + ')');
                // Call the event...
                socket.emit(what, data);
                break;
            case 'server':
                // Incoming events from the server (server-client)				
                console.log('<-SVR EVENT-server', what + '(' + dataLog + ')');
                
                // These are server pushes, TODO: Move these out into a strategy router...
                switch (what) {
                    case 's':
                        // Decompress and decode the state...
                        var c = JSON.parse(LZString.decompressFromBase64(data));
                        console.log(c);
                        // CSS...
                        if ( c.c ) document.getElementById('appStyle').innerHTML = c.c;
                        // HTML (angular code is recompiled with the dynamic $watch directive above)...
                        if ( c.h ) {
                            // TODO: Find the DOM element it belongs in... (ID)
                            if ( c.x ) {
                                console.log('Compiling', c.x.HTML );
                                var htmlId = c.x.HTML;
                                var ele = document.getElementById(htmlId);
                                ele.innerHTML = c.h;
                                var content = $compile(ele)($scope);
                            } else {
                                console.log('unknown location for HTML, not compiling');
                            }
                        }
                        // javascript can invoke new controllers/directives/services by calling angularize()...
                        if ( c.j ) {
                            eval( 'window.d = ' + c.j );
                            console.log ('controlerized', c.name);
                            angularize('controller', c.name, 'appContent', window.d);
                        }
                        break;
                    case 'd':
                        // Data change...
                        data = JSON.parse(data);
                        console.log('new data recieved');
                        if (data.LoginResult) {
                            updateLogin(data.User);
                        }
                        break;
                    case 'a':
                        my.note(data);
                        break;
                    case 'patch':
                        switch (data) {
                            case 'reboot':
                                location.reload();
                                break;
                            case 'css':
                                $('#css').replaceWith('<link id="css" rel="stylesheet" href="/dashboard/app.css?t=' + Date.now() + '"></link>');
                                break;
                            case 'state':
                                $appEvent('local:state:appTaskList');
                                break;
                        }
                        break;
                    case 'request':
                        console.log('requests');
                        switch(data) {
                            case 'login':
                                console.log('here');
                                if (typeof my.login !== 'undefined') my.event('remote:data', {type:'login', data: my.login});
                                break;
                        }
                    case 'debug':
                        console.log('Server DEBUG:', data);
                }
                break;
            default:
                // Unknown event types...
                console.log('.DIE. EVENT-unknown (' + location + ')', what + '(' + data + ')');
                break;
        }        
    };
        
    return {
    call: function (eventName, dataOrCallback) {
        var args = arguments;

    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
            var args = arguments;
            $rootScope.$apply(function () { if (callback) callback.apply(socket, args); });
      })
    }
    };
});
