/*
	$trace by Nick Steele
	Copyright (C) 2016 GSI Health, LLC.

	Simple yet precise logging factory that gets right to the point! Trace through an entire
	application in a single line on the console!
	
	Time specific things you're interested in, even through promises, socket events, a bunch of other traces, you got an idea? It'll work.
	
	Launch as many traces as you like.
	
	Flush them whenever you want and the console will then show when the trace started, along with all timing information it contains.

	USAGE:
	
		Want to take a peek at what's currently being traced? That's easy! just call the active method on any trace like so:
		
			currentTraces = myTrace.active();
		
		Access details about a specific active trace like so:
		
			myTraceDetails = myTrace.active()['myTraceName'];
			
		Create a trace like so:
		
			myTrace = new $trace('myTraceId');
		
		Add a trace event like so (it will log the time since the last event in the trace):
		
			myTrace.did('theThingIJustDid');
		
		Complete a trace event like so (it will flush the trace, take it out of the active trace registry, and display timing informaiton):
		
			myTrace.done();
		
		...or...
		
			myTrace.success();
			
		...or...
		
			myTrace.fail();
		
*/

// Holds all the active traces...
app.service('$traceService', function($log,$filter) {
	var traces = [];
	return {
		// Adds a trace...
		add: function( key ) {
			traces[key] = {
				trace	: '',				// The trace name
				start	: new Date(),		// The time the trace was started
				last	: new Date(),		// The last time traced
				times	: []				// The times for each trace
			};
		},
		
		// Continues a trace...
		did: function( key, text ) {
			try {
				// Record the time since we last recieved text...
				var now = new Date();
				// Record the time passed...
				var ms = now - traces[key].last;
				// Store now as last...
				traces[key].last = now;
				// Record the time since we were last called...
				traces[key].times.push({text:text, now: now});
				// Record the text passed and the time since the last call to the current trace...
				traces[key].trace += '.' + text + '(' + ms + 'ms)';
			} catch(e) {
				$log.warn("$trace: "+ key +": Tried to add to it but it doesn't exist!");
			}
		},
		
		// Lists a trace...
		active: function() {
			return traces;
		},

		// Returns the time elapsed between events...
		offset: function (key,a,b) {
			var times = traces[key].times;
			return times[b].now-times[a].now;
		},

		//Added this to show current time tracing
		now: function ( key ) {
			var times = traces[key].times;
			if(!times||times.length==0) return;
			var log = '';
			var prev = times[0].now;
			var start = times[0].now;
			angular.forEach(times,function (val,index) {
				log+=('['+$filter('date')(val.now,'hh:mm:ss:sss')+'(+'+(val.now-start)+'/Δ'+(val.now-prev)+')]['+index+']'+val.text+'\n');
				prev = val.now;
			});
			return log;
		},
		
		// Flushes a trace (ends it)...
		flush: function( key ) {
			try {
				// Record the time since we last recieved text...
				total = new Date() - traces[key].start;
				var text = '[' + key + ']' + traces[key].trace + ' ' + total + 'ms total.';
				delete traces[key];
				return text;
			} catch(e) {
				$log.warn("$trace: "+ key +": Tried to end it but it doesn't exist!");
			}
		}
	}
});

app.factory('$trace', function ($log, $traceService) {
	// The factory to generate (start a new trace) an object...
    return function (key) {
		var key = key;
		$traceService.add(key);
		
		return {

			// Return all open traces...
			active: function () { return $traceService.active(); },

			// Return history of traces...
			//history: function () { return history; },
			
			did: function (text) { $traceService.did(key, text); },
			
			// Complete a trace...
			done: function (text, status) { 
				// Default to an indifferent color...
				var color = '#444';
				// If a status was provided, set the color to be green if success, red if fail...
				if (typeof status !== 'undefined') color = status ? '#272' : '#722';				
				// Dump it to the screen...
				$log.debug('%c ' + $traceService.flush(key), 'background: #eee; color: ' + color);
			},
			offset: function (a,b) {
				return $traceService.offset(key,a,b);
			},
			now: function () {
				return $traceService.now(key);
			},
			
			// Shortcuts to success/fail traces...
			success: function ( text ) { this.done(text, true); },
			fail: function ( text ) { this.done(text, false); }
		}
    };
});