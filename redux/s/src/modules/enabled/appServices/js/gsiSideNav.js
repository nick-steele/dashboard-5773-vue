app.factory('gsiSideNav', gsiSideNav);

function gsiSideNav($log, $rootScope) {

    var navs = {};

    return {
        get: get,
        open: open,
        close: close,
        toggle: toggle,
        go: go,
        quit:quit,
        isInitialized:isInitialized
    };


    // Initialize
    function get(name, isOpen, defaultApp) {
        if(!navs[name]){
            navs[name]={
                isOpen: !!isOpen,
                active: defaultApp,
                menus:{},
                selectedTabIndex:0
             };
         }
         navs[name].menus[defaultApp]=true;
        return navs[name]
    }

    //Open/close side menu
    function open(name) {
        get(name).isOpen = true;
    }
    function close(name) {
        get(name).isOpen = false;
    }
    function toggle(name) {
        get(name).isOpen = !navs[name].isOpen;
    }

    function isInitialized(name,appName){
        return !!get(name).menus[appName];
    }

    //Switch active apps
    function go(name, appName,data){


        if (typeof(data) == "undefined") {
            // auto-detect tab index via name, called from GWT event
            for (var app in QA.app.userApps) {
                if (QA.app.userApps[app].name === appName) {
                    data = QA.app.userApps[app];
                    break;
                }
            }
        }

        //Currently, the care plan app is using popup, it should not be an active app.
        if (isTabApp(appName,data)) {
            $log.debug(appName+ " is Activated.");
            get(name).active = appName;
            QA.monitor.route(appName);
        }

        $rootScope.$evalAsync(function () {

            if (appName==='appHome') {
                //Dirty patch... maybe we should conside adding appHome to userApp
                get(name).selectedTabIndex = 0;
                return true;
            } else if(appName==='appCareBookSearch'){
                get(name).selectedTabIndex = 1;

            } else if (data&&data.index&&!data.hasWindow) {
                get(name).selectedTabIndex = data.index;
            }

        });

        //Check if app is already initialized
        if(isTabApp(appName,data)&&!isInitialized(name,appName)){
            $log.debug(appName+ " is not initialized.");
            get(name).menus[appName]=true;
            return false;
        }else{
            return true;
        }
    }



    function quit(name, appName, data) {
        $log.debug("Quitting "+appName);
        if(isTabApp(appName,data)){
            get(name).selectedTabIndex = 0;
            get(name).menus[appName]=false;
            return true;
        }
        $log.debug("Did not quit "+appName+". This app is likely to open another tab/window")
        return false;
    }

    //helper function
    function isTabApp(appName,data){
        return data&&!data.hasWindow || appName==='appCareBookSearch';
    }

}
