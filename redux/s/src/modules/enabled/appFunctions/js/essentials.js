// Adds .last() method to arrays...
if (!Array.prototype.last){
    Array.prototype.last = function(){
        return this[this.length - 1];
    };
};

String.prototype.capitalize = function(){
    if(this.length < 2) {
    	return this.charAt(0).toUpperCase();
    }
    var stringArray = this.split(/(\s+)/);
    var text = '';
    var i = 0;
    if (stringArray && stringArray.length > 0) {
    	for(i = stringArray.length - 1; i > -1; i--) {
        	if(stringArray[i].trim() === '') {
           	   stringArray.splice(i, 1);
        	}
    	}
    }
    if (stringArray && stringArray.length > 1) {
    	
    	var start = true;
		for (i = 0; i < stringArray.length; i++) { 
    		text += (start === true ? stringArray[i].capitalize() : ' ' + stringArray[i].capitalize());
    		start = false;
		}
		return text;
    }
    else {
    	return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase();
    }
    
};