
// Angular->GWT bridge (functions GWT can call from the server side)...


var GWT = {
    ready:false,
    event: function ( event , obj) {
        QA.app.logDebug('GWT emitted event', event);
        QA.app.event( event ,obj );
        QA.app.scope().$evalAsync();
        //angular.element(document.getElementById('mainApp')).scope().app.event( event );
        //angular.element(document.getElementById('mainApp')).scope().$evalAsync();
    },
    loginTimer: function (msg) {
        QA.app.GwtLoginTimer(msg);
    },
    log: function ( msg ) {
        //angular.element(document.getElementById('mainApp')).scope().app.log( 'GWT: ' + msg );
        console.info(QA.stamp()+ '(GWT)' + msg);
    },
    showAppWindow: function(url, name, features){
        var appWin = QA.app.appWindows[name];
        if(!appWin||appWin.closed === true){
            QA.app.appWindows[name] = window.open(url,name,features);
        }
        QA.app.appWindows[name].focus();
    },showAppTab: function(url, name, features){
        var appWin = QA.app.appWindows[name];
        if(!appWin||appWin.closed === true){
            QA.app.appWindows[name] = window.open(url,'_blank',features);
        }
        QA.app.appWindows[name].focus();
    },
    openAppWindow:function(url, name, features){
        QA.app.appWindows[name] = window.open(url,name,features);
        QA.app.appWindows[name].focus();
    },
    openAppTab:function(url, name, features){
        QA.app.appWindows[name] = window.open(url,'_blank',features);
        QA.app.appWindows[name].focus();
    },
    note: function ( msg ) {
        QA.app.logDebug('GWT note:' + msg);
        //angular.element(document.getElementById('mainApp')).scope().app.note( 'GWT: ' + msg );
    }
};

function GWTAngularUpdateAlerts( alerts ) {
        QA.app.alertValue( alerts );
        QA.app.scope().$apply();
	//angular.element(document.getElementById('mainApp')).scope().app.alertValue( alerts );
	//angular.element(document.getElementById('mainApp')).scope().$apply();
	console.log('GWT called "GWTAngularUpdateAlerts" with the value: ', alerts);
}