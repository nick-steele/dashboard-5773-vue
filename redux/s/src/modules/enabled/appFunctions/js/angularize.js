// Angluar 1.x hack to register dynamic controllers/directives/services properly...
function angularize(what, name, elem, func) {
		//return false;
		var queue = angular.module(client.vars.name)._invokeQueue;		// ...for readability; queue is the Angular Invoke Queue.
		var quit = false;
		// Go through each loaded module and quit if it already exists (this prevents duplicate modules from loading)...
		queue.forEach(function(module) { if ( name == module[2][0] ) quit = true; });
		if (quit) return false;
		
		console.log('adding...');
		// Load the new module...
		angular.module(client.vars.name)[what](name, func);
		
		// Now apply it...
		var module = queue[queue.length - 1];
		console.log('module',module);
		var provider = providers[module[0]];				// ...this line and above are for readability.
		console.log('module',module[0], module[1], module[2]);
		if (provider) provider[module[1]].apply(provider, module[2]);
/*
	// Re-apply all the controlers/directives/services...
	var queueLen = queue.length;
	for(var i = queueLen; i < queue.length; i++) {
		var call = queue[i]; // an entry in the invokeQueue (stuff that angular has loaded)
		// call is in the form [providerName, providerFunc, providerArguments]
		var provider = app.providers[call[0]];
		if (provider) {
			// e.g. $controllerProvider.register("Ctrl", function() { ... })
			provider[call[1]].apply(provider, call[2]);
		}
	}

	// compile the new element into the correct area...
	// (We don't need to compile it since I added the dynamic directive above)
	/*$('body').injector().invoke(function($compile, $rootScope) {
		$compile($('#' + elem))($rootScope);
		$rootScope.$apply();
	});*/
}