app.filter('filterPatients',['$filter', function($filter) {
  return function(patients, query) {
    query = query.toLowerCase();
    return patients.filter(function(patient) {
      if (!query
      || (patient.firstname && patient.firstname.toLowerCase().indexOf(query)!=-1)
      || (patient.lastname && patient.lastname.toLowerCase().indexOf(query) !=-1)
      || (patient.patientId && patient.patientId.toString().indexOf(query)!=-1)
      || (patient.gender && patient.gender.toLowerCase().indexOf(query) !=-1)
      || (patient.dob && $filter('date')(patient.dob, 'MM/dd/yyyy').indexOf(query)!=-1)
      || (patient.primaryPayerClass && patient.primaryPayerClass.toLowerCase().indexOf(query)!=-1)
      || (patient.primaryPayerMedicaidMedicareId && patient.primaryPayerMedicaidMedicareId.toLowerCase().indexOf(query)!=-1)
      || (patient.orgName && patient.orgName.toLowerCase().indexOf(query)!=-1)
      || (patient.status && patient.status.toLowerCase().indexOf(query)!=-1)) {
          return true;
      }
      return false;
    });
  };
}]);
