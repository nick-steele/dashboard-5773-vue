app.filter('gender', function() {
  return function(sex) {
    switch(sex.toUpperCase()) {
        case 'M':
        case 'MALE':
            return 'Male';
            break;
        case 'F':
        case 'FEMALE':
            return 'Female';
            break;
        case 'MF':
        case 'FM':
        case 'TRANS':
        case 'TRANSGENDER':
            return 'Transgender';
            break;
    }
  };
});

app.filter('ssn', function() {
  return function(ssn) {
      return 'xxx-xxx-' + ssn.slice(5);
  };
});

app.filter('ifNotNull', function() {
  return function(input) {
    if (typeof input === 'undefined' || input === null || input === '') return '';
    switch(input.toUpperCase()) {
        case 'NULL':
        case 'NULL_VALUE':
            return '';
            break;
        default:
            return input;
            break;
    }
  };
});

app.filter('address', function(){
    return function(data){
        if(!data) return '';
        var address = '';
        if(data.streetAddress1) address += (data.streetAddress1+', ');
        if(data.streetAddress2) address += (data.streetAddress2+', ');
        if(data.city) address += (data.city+', ');
        if(data.state) address += (data.state+' ');
        if(data.zipCode) address += data.zipCode;
        return address;
    }
});