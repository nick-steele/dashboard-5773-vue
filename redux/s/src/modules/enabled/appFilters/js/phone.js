app.filter('phone', [function() {
    function makePhone(value) {
        var result = value;

        var ssn = value ? value.toString() : '';
        if (ssn.length > 3) {
            result = ssn.substr(0, 3) + '-';
            if (ssn.length > 6) {
                result += ssn.substr(3, 3) + '-';
                result += ssn.substr(6, 4);
            } else {
                result += ssn.substr(3);
            }
        }

        return result;
    }
    return function(value) {
        return makePhone(value);
    };
}]);