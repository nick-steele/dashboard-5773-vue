app.filter('ssn', [function() {
    function makeSsn(value) {
        var result = value;

        var ssn = value ? value.toString() : '';
        if (ssn.length > 3) {
            result = ssn.substr(0, 3) + '-';
            if (ssn.length > 5) {
                result += ssn.substr(3, 2) + '-';
                result += ssn.substr(5, 4);
            } else {
                result += ssn.substr(3);
            }
        }

        return result;
    }
    return function(value) {
        return makeSsn(value);
    };
}]);