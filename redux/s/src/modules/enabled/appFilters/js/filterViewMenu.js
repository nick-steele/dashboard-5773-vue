app.filter('filterViewMenu', ['$model', function($model) {
  return function(menuViews, filterObj) {
    return menuViews.filter(function(menuView) {
        if(filterObj.typeNotShown) {
          return menuView.type !== filterObj.typeNotShown;
        }
        if(filterObj.hasChild === false) {
          return menuView.type !== 3;
        }
        return true;
    });
  };
}]);
