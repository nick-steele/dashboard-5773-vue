var checkWidget = function(widget, model) {
  if (widget.hidewidget && widget.hidewidget.value === true)
    return false;

  if (widget.depends && !model.get('app').settings.apps[widget.depends.split('.')[0]][widget.depends.split('.')[1]])
    return false;

  if(!widget.permissions || widget.permissions.length <= 0)
      return true;

  for(var i=0; i<widget.permissions.length; i++) {
      if(model.get('app').can(widget.permissions[i]) === true)
          return true;
  }
  return false;
}

app.filter('filterTabs', ['$model', function($model) {
  return function(tabs) {
    return tabs.filter(function(tab) {
            if (tab.isHide === true)
              return false;

            var widCount = 0;
            for(var i=0; i<tab.widgets.length; i++) {
              if (checkWidget(tab.widgets[i], $model) === false)
                widCount = widCount + 1;
            }
            
            if(widCount === tab.widgets.length)
              return false;

            if(!tab.permissions || tab.permissions.length <= 0) {
                return true;
            }
            for(var i=0; i<tab.permissions.length; i++) {
                if($model.get('app').can(tab.permissions[i]) === true )
                    return true;
            }
            return false;
    });
  };
}]);

app.filter('filterWidget', ['$model', function($model) {
  return function(widgets) {
    return widgets.filter(function(widget) {
        return checkWidget(widget, $model);
        /*if(!widget.permissions || widget.permissions.length <= 0) {
            return true;
        }

        for(let i=0; i<widget.permissions.length; i++) {
            if($model.get('app').can(widget.permissions[i]) === true)
                return true;
        }
        return false;*/
    });
  };
}]);
