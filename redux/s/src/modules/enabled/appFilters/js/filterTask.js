app.filter('statusFilter', function() {
  return function(tasks, filter) {
    return tasks.filter(function(task) {
      return filter[task.taskProgressStatusId.taskProgressStatusId].show;
    });
  };
});

app.filter('taskFilter',['$filter', function($filter) {
  return function(tasks, query) {
    query = query.toLowerCase();

    return tasks.filter(function(task) {

        var patient = '';

        if (task.patientInfo) {
            if (task.patientInfo.firstname)
                patient = patient + task.patientInfo.firstname + ' ';
            if (task.patientInfo.lastname)
                patient = patient + task.patientInfo.lastname;
        }

        if (!query
            || (task.userTaskTitle && task.userTaskTitle.toLowerCase().indexOf(query)!=-1)
            || (task.userTaskDescr && task.userTaskDescr.toLowerCase().indexOf(query) !=-1)
            || (patient.toLowerCase().indexOf(query)!=-1)) {
                return true;
        }
        return false;
    });
  };
}]);
