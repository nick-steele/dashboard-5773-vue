app.filter('orderWithPriority',['$filter', function($filter) {
  return function(items, fields, priorityFields) {
    var filtered = [];
    var rest = [];
    
    if (fields.length === priorityFields.length) {
      angular.forEach(items, function(item) {
        var matched = true;

        for(var i = 0; i < fields.length; i++) {
          if(item[fields[i]].toLowerCase() !== priorityFields[i].toLowerCase()) {
            matched = false;
          }   
        }
        if (matched) {
          filtered.push(item)
        }
        else {
          rest.push(item);
        }
      });

      // sort the rest of order list follow next fields
      if (fields.length > 1) {
        for(var i = 0; i < fields.length; i++) {
          var result = orderRestItems(rest, fields[i], priorityFields[i]);
          filtered = filtered.concat(result.ordered);
          rest = result.rest;
        }
      }
      filtered = filtered.concat(rest);
      
    }
    else {
      filtered = items;
    }
    
    return filtered;
  };
}]);

// sort by each item when there is more than 1 order fields
function orderRestItems(restList, field, priority) {
  var result = {
    ordered: [],
    rest: []
  };
  angular.forEach(restList, function(item) {
    var matched = false;
    if (item[field].toLowerCase() === priority.toLowerCase()) {
      result.ordered.push(item);
    }
    else {
      result.rest.push(item);
    }
  });
  return result;
}