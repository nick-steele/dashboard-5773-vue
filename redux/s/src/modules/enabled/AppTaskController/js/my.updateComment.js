my.updateComment = function (comment) {
    if (comment.txtComment.trim().length>0) {
        var data = {
            commentId: comment.userTaskCommentId,
            commentInfo: {
                UserTaskComment: {
                    userTask: comment.userTaskId,
                    userTaskCommentId: comment.userTaskCommentId,
                    communityId: comment.communityId,
                    userEmailId: comment.userEmailId,
                    comment: comment.txtComment.trim(),
                    creationDateTime: comment.creationDateTime
                }
            }
        }
        my.currentCommentActive = comment;
        $appEvent('remote:appTaskList:editComment', data);
    }
};