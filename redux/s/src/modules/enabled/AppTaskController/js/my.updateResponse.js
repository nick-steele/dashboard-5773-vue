// Called when an update finishes...
my.updateResponse = function(taskId) {
	//$appEvent('remote:appHome:loadTaskCount', {interval : $model.get('home').currentDatePicker.index, email: $model.get('home').currentMember.email});
    console.log('updateResponse');
    $appEvent('remote:appTaskList:taskFilter', {taskType: my.taskBadgeData.taskType, email: my.taskBadgeData.email, interval: $model.get('home').currentDatePicker.index, callFuncName : 'updateTaskInList', task : my.tempTasks['' + taskId]});
};

my.updateTaskInList = function(data) {

    console.log('updateTaskInList', data.Map.isInList);

    if (data.Map.isInList === false) {
        var removedTaskId = data.Map.task ? data.Map.task.userTaskId : undefined;
        var removedTaskIdStr = '' + removedTaskId;
        if (removedTaskId !== undefined  && my.tempTasks[removedTaskIdStr] !== undefined) {
            var removedTaskIdx =  my.tasks.indexOf(my.tempTasks[removedTaskIdStr]);
            console.log('Removing task:', removedTaskId);
            my.tasks.splice(removedTaskIdx, 1);
            if (taskBadgeFilter.data.isFiltered === true && taskBadgeFilter.data.badgeValue && taskBadgeFilter.data.badgeValue.tasks && taskBadgeFilter.data.badgeValue.tasks.length > 0) {
              var removedTaskInBadgeFilterIdx = -1;
              for(var taskIdx=0; taskIdx<taskBadgeFilter.data.badgeValue.tasks[0].length; taskIdx++) {
                if (removedTaskIdStr === taskBadgeFilter.data.badgeValue.tasks[0][taskIdx].id) {
                  removedTaskInBadgeFilterIdx = taskIdx;
                  break;
                }
              }
              if(removedTaskInBadgeFilterIdx > -1)
                taskBadgeFilter.data.badgeValue.tasks[0].splice(removedTaskInBadgeFilterIdx, 1);
            }
            delete my.tempTasks['' + removedTaskId];
        }

    }
    else if (data.Map.isInList === true) {
        //console.log('updateTaskInList2', data.Map.isInList, data.Map.task);
        var updatedTaskId = data.Map.task ? data.Map.task.userTaskId : undefined;
        if (updatedTaskId !== undefined  && my.tempTasks['' + updatedTaskId] !== undefined) {
            var updatedTaskIdx = my.tasks.indexOf(my.tempTasks['' + updatedTaskId]);
            console.log('Updating task:', updatedTaskId);
            my.addMeta(data.Map.task);
            my.tasks[updatedTaskIdx] = data.Map.task;
        }

    }
    $model.scope('task').$broadcast('refreshCalendar');
    $model.get('home').taskApi.refresh($model.get('home').currentMember.type === 2);
};
