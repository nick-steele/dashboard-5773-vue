my.deleteCommentCallback = function(id) {
    my.comments = my.comments.filter(function(comment) {
        return comment.userTaskCommentId !== id;
    });
}