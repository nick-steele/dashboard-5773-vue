my.tasksMetadata = function() {
        var start = new Date();      
        // Clear the headings in list view (say they are empty by default; we don't know what the new meadata will do)...
        my.listHeading.forEach(function(heading) { heading.empty = true; });
        
        // Go through each task and append metadata to it...
        my.tasks.forEach(my.addMeta);
        
        // For testing, record how long it took... can we get this under 100ms for 500 tasks?
        var time = new Date() - start;
        console.log('Task sorting took', time + 'ms');    
};