// Called from the server when a new task creation finishes...
my.newResponse = function(data) {

    $appEvent('remote:appTaskList:taskFilter', {taskType: my.taskBadgeData.taskType, email: my.taskBadgeData.email, interval: $model.get('home').currentDatePicker.index, callFuncName : 'addTaskToList', task: data.UserTask});
};

my.addTaskToList = function(data) {
	// Add the task data to the list...
    $log.debug('addTaskToList', data, data.Map.isInList);
    if (data.Map.isInList === true) {
        if(data.Map.task !== undefined) {
            $log.debug('Adding task:', data.Map.task);
            my.tasks.push(data.Map.task);
            my.addMeta(my.tasks[my.tasks.length - 1]);
            $model.scope('task').$broadcast('refreshCalendar');
            if (taskBadgeFilter.data.isFiltered === true && taskBadgeFilter.data.badgeValue && taskBadgeFilter.data.badgeValue.tasks && taskBadgeFilter.data.badgeValue.tasks.length > 0) {
              taskBadgeFilter.data.badgeValue.tasks[0].push({id: '' + data.Map.task.userTaskId});
            }
    	}
    }
    $model.get('home').taskApi.refresh($model.get('home').currentMember.type === 2);
    //my.tempTask = undefined;
};
