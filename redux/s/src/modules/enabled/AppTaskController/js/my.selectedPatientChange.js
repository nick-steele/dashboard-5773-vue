my.selectedPatientChange = function(item) {
    $log.debug('selected patient change:', item);
    my.minorWarning = undefined;
    if(item && item.dob) {
        QA.app.event('remote:appTaskList:checkMinor', item.dob);
    }
}