my.setAssignTaskFields = function(task) {
    var newAssignTo = my.orgCareteamUserSearch.selectedItem;

    if (newAssignTo && newAssignTo.email) {
        if (newAssignTo.email !== task.userEmailId) {
            task.assignedByEmailId = my.loginedUser.email;
        }
        $log.debug('Assigned by user:', task.assignedByEmailId, task.assignedByFirstName, task.assignedByLastName);

        task.userEmailId = newAssignTo.email;
    } else if (!task.userEmailId) {
        task.userEmailId = my.taskBadgeData.email && !my.taskBadgeData.email.contains(";") ? my.taskBadgeData.email : my.loginedUser.email;
    }

    $log.debug('Assigned user:', task.userEmailId, task.userFirstName, task.userLastName);
}