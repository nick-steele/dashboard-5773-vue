my.openFilterPanel = function(ev) {
    var position = $mdPanel.newPanelPosition()
        .relativeTo(ev.target)
        .addPanelPosition($mdPanel.xPosition.ALIGN_START, $mdPanel.yPosition.BELOW);

    var config = {
        attachTo: angular.element(document.body),
        controller: PanelMenuCtrl,
        controllerAs: 'vm',
        position: position,
        templateUrl: 'taskManager.filter.tpl.html',
        panelClass: 'filter-panel',
        locals: {
            'task': my
        },
        openFrom: ev,
        clickOutsideToClose: true,
        escapeToClose: true,
        focusOnOpen: false,
    };

    $mdPanel.open(config);
};

function PanelMenuCtrl(mdPanelRef, $timeout, $scope) {
    var self = this;
    this._mdPanelRef = mdPanelRef;
    this.watchSize = $scope.$watch(function() { return $mdMedia('gt-sm'); }, function(big) {
        if (big) {
            self.close();
        }
    });

    this.close = function() {
        var panelRef = this._mdPanelRef;
        panelRef && panelRef.close().then(function() {
            panelRef.destroy();
            self.watchSize();
        });
    };
}