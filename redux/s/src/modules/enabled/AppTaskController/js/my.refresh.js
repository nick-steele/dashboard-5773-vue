// Refresh tasks...
my.refresh = function(ev) {
	GWT.activity();
	// Request data models...

	if (taskBadgeFilter.data.isFiltered === true) {
		my.loadTasks('remote:appTaskList:loadTasksFromBadges', taskBadgeFilter.data);
	} else if (taskBadgeFilter.data.supervisorFlag === true) {
		my.loadTasks('remote:appTaskList:boot', {email: my.taskBadgeData.email ? my.taskBadgeData.email : my.loginedUser.email});
	} else {
		my.loadTasks('remote:appTaskList:boot');
	}
};
