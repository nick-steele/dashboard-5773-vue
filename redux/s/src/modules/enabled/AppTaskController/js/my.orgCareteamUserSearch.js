// The patient search handler..
my.orgCareteamUserSearch = {
    selectedItem: {},
    searchText: null,
    promise: null,
    query: function(query) {
        selectedItem = {};
        // Setup a promise...
        my.orgCareteamUserSearch.promise = $q.defer();
        // Send the search event...
        var patientData = $model.get('app').dialog.data;

        var patientId = undefined;
        if(patientData !== undefined) {
            patientId = patientData.patientId
        }
        console.log("Patientid: ", patientId, ";query:", query);
        QA.app.event('remote:appTaskList:searchOrgCareteamUser', {'query' : query, 'patientId': patientId});
        //return my.userItems;
        return my.orgCareteamUserSearch.promise.promise;
    }
};