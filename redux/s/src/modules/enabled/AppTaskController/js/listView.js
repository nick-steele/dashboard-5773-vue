// When a user clicks a task in time view
my.badgeClick = function(taskList, ev) {
    GWT.activity();
    $model.get('app').dialog.allowNextPopup = true;
    
    // backup the task to an original state (to revert if cancel is clicked)  
    $model.get('app').showDialog('calendarTaskList', taskList, ev, function(result) {      
        switch (result) {
            case 'close':
              // console.log('close');
              break;
        }
    });
}