my.addCommentCallback = function(data) {
    data.UserTaskComment.userFirstName = $model.get('app').login.firstName;
    data.UserTaskComment.userLastName = $model.get('app').login.lastName;
    data.UserTaskComment.fromCurrentUser = true;
    data.UserTaskComment.userTaskId = data.UserTaskComment.userTask.userTaskId
    my.comments.push(data.UserTaskComment);
}