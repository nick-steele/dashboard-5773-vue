my.switchToTaskBadgeFilterMode = function() {
    my.filter[1].show=true;
    my.filter[2].show=true;
    my.filter[3].show=false;
    my.filter[4].show=false;

    my.currentView.type = -1;

    if (taskBadgeFilter.data.badgeValue.tasks[0].length !==0)
        my.loadTasks('remote:appTaskList:loadTasksFromBadges', taskBadgeFilter.data);
    else {
        my.tasks = [];
        $timeout(function() {
            $model.scope('task').$broadcast('refreshCalendar');
        });
    }
}
