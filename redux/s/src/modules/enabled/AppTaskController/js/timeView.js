//called from monthly view

// When a user clicks a task in time view
my.timeClick = function( task ) {
    console.log ('task clicked in time view', task)

    GWT.activity();
    // backup the task to an original state (to revert if cancel is clicked)
    var originalTask = {};
    angular.copy(task, originalTask);
    task.dueDateTime = moment(task.dueDateTime).toDate();

    $model.get('app').showDialog('appTaskDetails', task, {}, function(result) {
        switch (result) {
            case 'ok':
              //task.dueDateTime = task.dueDateTime.toISOString().slice(0, 19);
              task.dueDateTime = my.SQLdate(task.dueDateTime);
              my.update(task);
              break;
            case 'cancel':
              angular.copy(originalTask, task);
              break;
        }
    });
};