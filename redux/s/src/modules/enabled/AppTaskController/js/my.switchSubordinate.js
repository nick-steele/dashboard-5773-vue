// switch user
my.switchSubordinate = function(user, ev) {
    taskBadgeFilter.set("noBadgeFilter", user, undefined, user.email.contains(";") ? true : false);
    console.log("Switch to user: " + user.email);
    my.filter[1].show=true;
    my.filter[2].show=true;
    my.filter[3].show=true;
    my.filter[4].show=false;

    my.loadTasks('remote:appTaskList:boot', {email: my.taskBadgeData.email ? my.taskBadgeData.email : my.loginedUser.email});
}