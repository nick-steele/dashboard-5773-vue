// The times are in 'YYYY/MM/DD HH:mm:ss' format, but we need to account for timezones and other stuff, so do it all in one spot...
my.SQLdate = function(time) {
    // if (typeof time === 'undefined') return moment(Date.now()).format("YYYY-MM-DDTHH:mm:ss");
    // else return moment(time).format("YYYY-MM-DDTHH:mm:ss");
    if (typeof time === 'undefined') return moment(Date.now()).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
    else return moment(time).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
};