// Update a task...
//copied to gsiTaskItem directive
//TODO: still called from my.statusChange consider removing this when drag&drop is ready
my.update = function(task) {
    task.lastUpdateDateTime = my.SQLdate();
    my.addMeta(task);
    my.tempTasks['' + task.userTaskId] = task;
    var sendItem = angular.copy(task);
    my.modifiedTask = angular.copy(task);
    delete sendItem['$$hashKey'];
    delete sendItem.meta;
    console.log('Start update task', sendItem);
    QA.app.event('remote:appTaskList:update', sendItem);
};