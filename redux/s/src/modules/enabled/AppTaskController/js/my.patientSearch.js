// The patient search handler..
my.patientSearch = {
    selectedItem: null,
    searchText: null,
    promise: null,
    query: function(query) {
        // Setup a promise...
        my.patientSearch.promise = $q.defer();
        // Send the search event...
        QA.app.event('remote:appTaskList:patientSearch', query);
        return my.patientSearch.promise.promise;
    }
};