my.editCommentCallback = function(data) {
    my.currentCommentActive.lastUpdateDateTime = data.UserTaskComment.lastUpdateDateTime;
    my.currentCommentActive.txtComment = my.currentCommentActive.txtComment.trim();
    my.currentCommentActive.comment = data.UserTaskComment.comment;
    my.currentCommentActive.editMode = false;
}