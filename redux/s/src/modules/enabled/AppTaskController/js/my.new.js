// Create a task...
my.new = function(ev) {
	GWT.activity();

	var task = {
		taskCategoryId: 1,
		userTaskTitle: '',
		userTaskDescr: '',
		taskAction: '',
		dueDateTime: new Date(),
		creationDateTime: my.SQLdate(),
		lastUdateDateTime: my.SQLdate(),
		taskTypeId: my.types === undefined || my.types.length === 0 ? undefined : my.types[0],
		taskProgressStatusId: { taskProgressStatusId: 1 }
		//email: my.taskBadgeFilter.data ? my.taskBadgeFilter.data.email : undefined
	};
	// Reset the patient search...
	my.patientSearch.selectedItem = null;
	my.patientSearch.searchText = null;


	my.orgCareteamUserSearch.selectedItem = {};

	var isSelected = my.taskBadgeData.email && !my.taskBadgeData.email.contains(";") ;
	my.orgCareteamUserSearch.selectedItem.email = isSelected ? my.taskBadgeData.email : my.loginedUser.email;
	my.orgCareteamUserSearch.selectedItem.firstName =isSelected ? my.taskBadgeData.firstName : my.loginedUser.firstName;
    my.orgCareteamUserSearch.selectedItem.lastName = isSelected ? my.taskBadgeData.lastName : my.loginedUser.lastName;

	my.orgCareteamUserSearch.searchText = null;
	$model.get('app').showDialog('appTaskNew', task, ev, function(result) {
		switch (result) {
			case 'ok':
                $log.debug(task);
				task.dueDateTime = my.SQLdate(task.dueDateTime);
				// Set the patientId...
				if (my.patientSearch.selectedItem)
					task.patientId = {patientId: my.patientSearch.selectedItem.patientId};
				else {
					delete(task.patientId);
				}
				my.setAssignTaskFields(task);
                my.setLastUpdateFields(task);
				// We're set, create a new update date/time and append metadata...
				//my.tempTask = data;
				QA.app.event('remote:appTaskList:new', task);
				break;
			case 'cancel':
				break;
		}
	});
};
