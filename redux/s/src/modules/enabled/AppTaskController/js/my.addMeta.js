// Adds meta data to each task item...
 my.addMeta = function(task) {
     try {

         //"YYYY-MM-DDTHH:mm:ss".length
        if (task.dueDateTime.length === 19) {
            task.dueDateTime = task.dueDateTime  + my.localTimeZone;
        }

        due = moment(task.dueDateTime);

         // Define functions used in the meta object below (outline to reduce memory / increase speed)...
         var meta = {
             tipType:    function() { return (my.isSystem(task)) ? 'System task' : 'User task'; },
             iconType:   function() { return (my.isSystem(task)) ? 'task-system' : 'task-user'; },

             dueRange:   function() {
                 if (typeof task.dueDateTime === 'undefined') return 'none';

                 if ( due.isBefore(my.refDate.today) ) return 'past';
                 if ( due.isBefore(my.refDate.tomorrow) ) return 'today';
                 if ( due.isBefore(my.refDate.dayAfterTomorrow) ) return 'tomorrow';
                 if ( due.isBetween(my.refDate.today, my.refDate.nextWeek) ) return 'week';
                 return 'far';
             },
             iconDue:    function() {
                 // Complete exception...
                 if (task.taskProgressStatusId.taskProgressStatusId === 3) return 'due-complete';
                 if (task.taskProgressStatusId.taskProgressStatusId === 4) return 'due-complete';
                 if ( due.isBefore(my.refDate.today) ) return 'due-late';
                 if ( due.isBetween(my.refDate.today, my.refDate.nextWeek) ) return 'due-close';
                 if ( due.isAfter(my.refDate.nextWeek) ) return 'due-ok';
                 // This plugs the missing due date...
                 return 'due-close';
             },
             hoverTip:   function() {
                 // Complete exception...
                 if (task.taskProgressStatusId.taskProgressStatusId === 3) return 'Completed';
                 if (task.taskProgressStatusId.taskProgressStatusId === 4) return 'Canceled';
                 if ( due.isBefore(my.refDate.today) ) return 'Past due';
                 if ( due.isBefore(my.refDate.tomorrow) ) return 'Due today';
                 if ( due.isBefore(my.refDate.dayAfterTomorrow) ) return 'Due tomorrow';
                 return 'Due ' + moment(task.dueDateTime).add(1, 'days').fromNow();
             }
         };

         // Add the meta data to the task...
         task.meta = {
             tip:    {
                 due:    moment(task.dueDateTime).fromNow(),
                 type:   meta.tipType()
             },
             hoverTip:   meta.hoverTip(),
             color:      function() {
                     if(typeof task.taskProgressStatusId === 'undefined')
                             return '#000000';
                     else
                             return my.filter[ task.taskProgressStatusId.taskProgressStatusId ].color;
             },
             show:      function() {
                     if(typeof task.taskProgressStatusId === 'undefined')
                             return false;
                     else
                             return my.filter[ task.taskProgressStatusId.taskProgressStatusId ].show;
             },
             icon:   {
                 due:    meta.iconDue(),
                 type:   meta.iconType()
             },
             due:        meta.dueRange()
         };

		 task.title = task.userTaskTitle;
		 task.startsAt = new Date(task.dueDateTime);
         // task.endsAt = new Date(task.startsAt);
         // task.endsAt.setMinutes(task.startsAt.getMinutes() + 30);

         switch (task.taskProgressStatusId.taskProgressStatusId) {
            case 1:
                task.type = 'info';
                break;
            case 2:
                task.type = 'warning';
                break;
            case 3:
                task.type = 'success';
                break;
            case 4:
                task.type = 'cancel';
                break;
            default:
                task.type = 'unknow';
         }

		 //task.type = 'info';
		 task.editable = true;
		 task.draggable = true;
		 task.resizable = false;

         my.listHeading.forEach(function(heading) {
             if(heading.name === task.meta.due) heading.empty = false;
         });
     } catch(e) { if(task!==false) $log.warn('error adding metadata', e, task); }
 };
