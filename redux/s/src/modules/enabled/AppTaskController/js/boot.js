function boot() {
	var trace = new $trace('task.boot');
	my.appCtrl = $model.get('app');
	my.loadTasks = function(event, data){
		my.tasks = [false];
		$timeout(function() {
			if(my.isLoading)
				my.showProgress = true;
		}, 500);
		$appEvent(event, data);
	};

	// Store our own reference to devmode...
	my.devmode = {
		enabled: $model.get('app').devmode,
		toggles: {}
	};

	var bootStart = new Date();

	// Boot the app (populate the model)...
	my.tasks = [false];
	my.timeTasks = [];
	my.search = {
		time:'',
		status: '',
		list:   ''
	};
	my.tempTasks = {};

    my.taskBadgeFilter = taskBadgeFilter;

	my.searchCalendarTask = function () {
		$model.get('calendarTemplate').updateSarchString(my.search.time);
	};

	// Currently selected tab. Default is Home tab
	my.selectedTab = 0;
	// Track which tab has been loaded...
	my.loadedTabs = [];
	// Keeps track of available tabs here to lazy load tabs.
	// Please comment out tab-name when you want to disable a tab
	my.appTabs = [
		//'home',
		//'status',
		'multiStatus',
		'time'
		//'list'
	];

    //my.currentMember = my.appCtrl.subordinates[0];
    my.loginedUser = my.appCtrl.login;
    //taskBadgeFilter.set("noBadgeFilter", my.loginedUser, undefined );
    my.taskBadgeData = my.taskBadgeFilter.data;

    my.minorWarning = undefined;

	trace.did('vars');

	// Request data models (getting this started ASAP)...
	my.loadTasks('remote:appTaskList:boot', my.taskBadgeFilter.data);

	trace.did('bootEvent');

	// Define pre-calculated reference dates for comparisons...
	var REFERENCE = moment();
	my.refDate = {
		today:              REFERENCE.clone().startOf('day'),
		tomorrow:           REFERENCE.clone().add(1, 'days').startOf('day'),
		dayAfterTomorrow:   REFERENCE.clone().add(2, 'days').startOf('day'),
		nextWeek:           REFERENCE.clone().add(1, 'week').startOf('day'),
		nextMonth:          REFERENCE.clone().add(1, 'months').startOf('day')
	};

	my.localTimeZone = moment(Date.now()).format(".SSSZ");

	trace.did('moment');


	var todoColor = '#bbdefb',
		inProgressColor = '#fff9c4',
		completedColor = '#dcedc8',
		cancelledColor = '#f5f5f5';

	// The filter shows/hides items in each tab...
	my.filter = {
		1: { show: true,    css:'todo',          color: todoColor       },
		2: { show: true,    css:'in-progress',  color: inProgressColor },
		3: { show: taskBadgeFilter.data.isFiltered === true ? false: true,    css:'complete', color: completedColor},
		4: { show: false,    css:'cancelled',    color: cancelledColor}
	};

	// Returns status filter
	my.statusFilterFn = function(){
		return function(item){
			var id = item.taskProgressStatusId.taskProgressStatusId;
			return my.filter[id].show;
		}
	};
	my.showProgress= false;

	trace.did('watcher');

	my.listHeading = [
		{name: 'past',    show: true,  empty: true, title: 'Past Due'},
		{name: 'today',   show: true,  empty: true, title: 'Due today'},
		{name: 'tomorrow',show: true,  empty: true, title: 'Due tomorrow'},
		{name: 'week',    show: true,  empty: true, title: 'Due this week'},
		{name: 'far',     show: true,  empty: true, title: 'Due next week and beyond'},
		{name: 'none',    show: true,  empty: true, title: 'No due date'}
	];

	// For time view...
	my.time = {
		view: 'week',
		date: new Date(),
		title: 'title here',
		events:   [
			{
				title: 'My event title', // The title of the event
				type: 'info', // The type of the event (determines its color). Can be important, warning, info, inverse, success or special
				startsAt: new Date(2016,4,1,1), // A javascript date object for when the event starts
				endsAt: new Date(2016,5,26,15), // Optional - a javascript date object for when the event ends
				editable: false, // If edit-event-html is set and this field is explicitly set to false then dont make it editable.
				deletable: false, // If delete-event-html is set and this field is explicitly set to false then dont make it deleteable
				draggable: true, //Allow an event to be dragged and dropped
				resizable: true, //Allow an event to be resizable
				incrementsBadgeTotal: true, //If set to false then will not count towards the badge total amount on the month and year view
				recursOn: 'year', // If set the event will recur on the given period. Valid values are year or month
				cssClass: 'a-css-class-name' //A CSS class (or more, just separate with spaces) that will be added to the event when it is displayed on each view. Useful for marking an event as selected / active etc
			}
		]
	};

	my.minDate = new Date('1/1/1880');
	my.maxDate = new Date('1/31/2130');

	var loginInfo = $model.get("app").login;
    my.currentView = {
		email: loginInfo.email,
		firstName: loginInfo.firstName,
		lastName: loginInfo.lastName,
		userId: loginInfo.userId,
		type: 1,
		selected: true
	};

    trace.success();
}
