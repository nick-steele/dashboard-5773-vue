// Called from the patientSearch event...
my.searching = function (data) {
    if (my.patientSearch.searchText == data.query) {
        my.patientSearch.promise.resolve(data.response);
    }
};