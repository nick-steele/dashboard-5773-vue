my.changeUserView = function(data) {
    console.log(data);
    if (data.selectedView.type === constants.VIEW_TYPE.MY_VIEW) {
        taskBadgeFilter.set("noBadgeFilter", data.selectedView, undefined, constants.VIEW_TYPE.MY_VIEW);
        console.log(data.selectedView);
    } else {
        if (data.subordinates.size() === 1) {
            taskBadgeFilter.set("noBadgeFilter", data.subordinates[0], undefined, constants.VIEW_TYPE.TEAM_VIEW_ONE);
        } else {
            var viewType = '';
            if(data.subordinates.size() < $model.get('app').subordinates.size()) {
              viewType = constants.VIEW_TYPE.TEAM_VIEW_SOME;
            }
            else {
              viewType = constants.VIEW_TYPE.TEAM_VIEW;
            }

            var tmp = angular.copy(data.selectedView);
            tmp.email = data.subordinates[0].email;
            for(var i = 1; i< data.subordinates.length; i++) {
                tmp.email += ";" + data.subordinates[i].email;
            }

            taskBadgeFilter.set("noBadgeFilter", tmp, undefined, viewType);
        }
    }

    var resetTaskStatusFilter = function() {
        my.filter[1].show=true;
        my.filter[2].show=true;
        my.filter[3].show=true;
        my.filter[4].show=false;
    };
    resetTaskStatusFilter();

    my.loadTasks('remote:appTaskList:boot', {email: my.taskBadgeData.email ? my.taskBadgeData.email : my.loginedUser.email});
};
