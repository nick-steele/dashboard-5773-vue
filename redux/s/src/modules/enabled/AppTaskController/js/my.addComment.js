my.addComment = function(task) {
    if (my.txtComment) {
        var data = {
            userTask: task.userTaskId,
            comment: my.txtComment
        };
        $appEvent('remote:appTaskList:addComment', data);
        my.txtComment = '';
    }
}