// Called from the patientSearch event...
my.orgCareteamUserSearchResolve = function(data) {
    if (my.orgCareteamUserSearch.searchText === data.query.query) {
        my.orgCareteamUserSearch.promise.resolve( data.response );
    }
};