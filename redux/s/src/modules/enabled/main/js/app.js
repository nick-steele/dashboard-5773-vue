/*
 * This is the entry point for the entire client application...
 */
// Expose the Angular providers to allow dynamic module loading
var providers = {};

QA.trace.log('system.boot', 'Scripts loaded');

// Creates the main application...
var app = angular.module(client.vars.name, client.dependancies, function($controllerProvider, $compileProvider, $provide) {
	QA.trace.log('system.boot', 'Angular loaded');
    providers = {
        $controllerProvider: $controllerProvider,
        $compileProvider: $compileProvider,
        $provide: $provide
    };
})

// Configures when the application is booted...
.config(function($mdIconProvider, $mdThemingProvider, $locationProvider) {
    $mdIconProvider.defaultIconSet('app.svg?' + client.vars.version, 24);
	$mdThemingProvider.theme('default')
		.primaryPalette('blue')
		.accentPalette('deep-orange');
    $locationProvider.html5Mode(true);
	QA.trace.log('system.boot', 'Angular configured');

})

// Runs when the application is booted...
.run(function(gsiUtils, gsiGenerator, gsiConfig,$mdIcon) {

    //Preload svg icon file
	$mdIcon('app.svg?' + client.vars.version);

	// Generate extra classes based on registered themes so we can use same colors with non-angular-material elements
	gsiGenerator.generate();

	// Disable md-ink-ripple effects on mobile if 'disableMdInkRippleOnMobile' config enabled
	if ( gsiConfig.getConfig('disableMdInkRippleOnMobile') && gsiUtils.isMobile() ) angular.element('body').attr('md-no-ink', true);

	// Add isMobile as a class...
	if ( gsiUtils.isMobile() ) angular.element('html').addClass('is-mobile');

	// Add browser info as classes...
	var browserInfo = gsiUtils.detectBrowser();
	if ( browserInfo ) angular.element('html').addClass( browserInfo.browser + ' ' + browserInfo.version + ' ' + browserInfo.os );
	
	// Drop the data into the QA object...
	QA.client = browserInfo;
	QA.client.isMobile = gsiUtils.isMobile();
	if(window.newrelic){
		Object.keys(QA.client).forEach(function(key) {
			newrelic.setCustomAttribute(key, QA.client[key]);
		});
	}

	QA.trace.log('system.boot', 'Angular booted ' + browserInfo.browser + ' on ' + browserInfo.os);
})


// The Vue app entry point...
Vue.use(Quasar)
var vueApp = new Vue({
	el: '#vue-app',
	data: {
	  message: 'Hello Vue!'
	}
  })

Vue.prototype.$identity = {
	data: QA
}

Vue.prototype.$action = function(name, payload) {
	QA.event(name, payload)
	this.$q.emit(name, payload)
}

var VComponent = Vue.component('gsi-roster', {
	data: function () {
		return {
			message: 'Working!'
		}
	},
	template: "<center><div>This is a component in Vue! Is it working? {{ $idetity.name }}<v-btn :name=\"\" @click=\"$action('menuClick', payload)\"></v-btn></div></center>",
	reactions: {
		menuClick: function(payload) {

		}
	}
})

app.value('gsi-roster', VComponent)
