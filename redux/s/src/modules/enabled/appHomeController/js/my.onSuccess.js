my.onSuccess = function (data) {
    GWT.activity();
    if (data[0] === "TaskBadge") {
        my.isTaskBadgeError = false;
    }

    if (data[0] === "Alert") {
        my.isAlertError = false;
    }

    if (data[0] === "CarePlan") {
        my.isCarePlanError = false;
    }

    if (data[0] === "NoEncounter") {
        my.isNoEncounterError = false;
    }
};
