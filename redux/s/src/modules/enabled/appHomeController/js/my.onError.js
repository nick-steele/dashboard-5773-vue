my.onError = function (data) {
    GWT.activity();
    $log.warn("Home Panel", data[1]);

    if (data[0] === "TaskBadge") {
        my.isTaskBadgeError = true;
    }

    if (data[0] === "Alert") {
        my.isAlertError = true;
    }

    if (data[0] === "CarePlan") {
        my.isCarePlanError = true;
    }

    if (data[0] === "NoEncounter") {
        my.isNoEncounterError = true;
    }

    if (data[0] === "PatientList") {
        my.isPatientListError = true;
    }
};
