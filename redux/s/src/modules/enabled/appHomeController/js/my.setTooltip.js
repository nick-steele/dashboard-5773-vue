my.setTooltip = function (event, tooltipId) {
    var elem = angular.element(document.getElementById(tooltipId))[0];
    if (elem) {
      elem.style.left = (event.clientX - elem.clientWidth/2) + 'px';
    }
    my.previousTooltip = elem;
}