my.reloadPatientPanelView = function(){
    if(my.currentPatientToggle == 1){
        my.changeUserView();
    }
};
my.changeTaskSelection = function () {
    if (my.currentMember.email === my.appCtrl.login.email) {
        my.currentMemberName = 'My Tasks Summary';
    }
    else if (my.currentMember.email.contains(";") > 0) {
        my.currentMemberName = 'My Team Tasks Summary';
    }
    else {
        my.currentMemberName = 'Tasks Summary for ' + my.currentMember.lastName + ', ' + my.currentMember.firstName;
    }
    my.reloadTaskBadgeCount();
};

my.changeUserView = function(data) {
  var alertApiData = {};

  if(data !== undefined) {
    my.currentMember = data.selectedView;
    my.selectedSubordinates = data.subordinates;
  }

  switch(my.currentMember.type) {
      case 1:
          $model.get('app').currentMenuTheme = 'theme-bg-primary';
          break;
      case 2:
          $model.get('app').currentMenuTheme = 'theme-bg-secondary';
          break;
      default:
          $model.get('app').currentMenuTheme = 'theme-bg-grey';
  };

  alertApiData.member = my.currentMember;
  alertApiData.subordinates = my.selectedSubordinates;
  if (typeof my.alertEncounterApi.refresh==='function')
    my.alertEncounterApi.refresh(alertApiData);
  if (typeof my.taskApi.refresh==='function')
      my.taskApi.refresh(my.currentMember.type === 2);
  if (typeof my.issueApi.refresh==='function')
      my.issueApi.refresh();
  if (typeof my.assessmentApi.refresh==='function')
      my.assessmentApi.refresh();
  if (typeof my.goalApi.refresh==='function')
      my.goalApi.refresh();
  if (typeof my.interventionApi.refresh==='function')
      my.interventionApi.refresh();
};
