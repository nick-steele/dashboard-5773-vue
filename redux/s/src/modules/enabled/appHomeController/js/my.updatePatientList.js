my.updatePatientList = function(data){
    //TODO: reload Patient Panel
    my.currentPatientActive.alreadyInList = true;
    $model.get('myPatientList').reloadAfterRemoveOrAdd();
};

my.updateRemovedPatient = function(data){
    my.currentPatientActive.alreadyInList = false;
    $model.get('myPatientList').reloadAfterRemoveOrAdd();
};