my.openCarebook = function (patientId) {
    GWT.activity();
    $appEvent('local:patientDrillDown:openCarebook', patientId);
    $mdDialog.hide();
};
my.openEdit = function (patientId) {
    GWT.activity();
    $appEvent('local:patientDrillDown:edit', patientId);
    $mdDialog.hide();
};
my.openWizard = function(patientId, step) {
    // $mdDialog.hide();
    $appEvent('local:appPatientEnrollment:openWizard', { 'patientId': patientId, 'step':step });
};
my.openCareplan = function (patientId) {
    GWT.activity();
    $appEvent('local:patientDrillDown:openCarePlanDirect', patientId);
    $mdDialog.hide();
};
my.addToPatientList = function (index, patient) {
    GWT.activity();
    var title = 'Confirmation';
    var defaultMsg = 'Add '+ patient.lastname + ', ' + patient.firstname + ' to My Patient Panel?';
    var msg = my.getMessageBundle(defaultMsg, "popup.confirmation.add.msg", function (configMsg) {
        return configMsg.replace('[0]', patient.lastname + ', ').replace('[1]', patient.firstname);
    });
    var success =function () {
        my.currentPatientActive = patient;
        $appEvent('remote:myPatientList:add', {MyPatientInfoDTO: patient});
    };
    my.displayConfirmation(title, msg, success, function(){});

};
my.removeFromPatientList  = function (index, patient) {
    GWT.activity();
    var title = 'Confirmation';
    var defaultMsg = 'Remove '+ patient.lastname + ', ' + patient.firstname + ' from My Patient Panel?';
    var msg = my.getMessageBundle(defaultMsg, "popup.confirmation.remove.msg", function (configMsg) {
        return configMsg.replace('[0]', patient.lastname + ', ').replace('[1]', patient.firstname);
    });
    var success = function () {
        my.currentPatientActive = patient;
        $appEvent('remote:myPatientList:removeFromPopup',{patientId: patient.patientId});
    };
    my.displayConfirmation(title, msg, success, function(){});

};
my.getMessageBundle = function (defaultMsg, msgKey, replaceFunc) {
    if(my.messageBundle && (my.messageBundle[msgKey])){
        return replaceFunc(my.messageBundle[msgKey]);
    } else {
        $log.debug('Cannot get messageBundle with key: '+ msgKey);
        return defaultMsg;
    }

};
