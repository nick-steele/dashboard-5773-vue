my.displayConfirmation = function(title, msg, success, cancel){
    var position = $mdPanel.newPanelPosition()
        .absolute()
        .center();

    var config = {
        attachTo: angular.element(document.body),
        controllerAs: 'panelCtrl',
        controller: function (mdPanelRef) {
            var panel = this;
            panel.msg = msg;
            panel.title = title;
            panel._mdPanelRef = mdPanelRef;
            this.close = function() {
                var panelRef = panel._mdPanelRef;
                panelRef && panelRef.close().then(function () {
                    panelRef.destroy();
                });
            };
            this.ok = function () {
                success();
                var panelRef = panel._mdPanelRef;
                panelRef && panelRef.close().then(function () {
                    panelRef.destroy();
                });

            };
            this.no = function () {
                cancel();
                var panelRef = panel._mdPanelRef;
                panelRef && panelRef.close().then(function () {
                    panelRef.destroy();
                });
            }
        },
        template: '<div id="confirmPopup" class="panel-dialog">' +
        '   <md-toolbar class="panel-title">' +
        '       <div class="md-toolbar-tools">' +
        '           <h2>'+ title + '</h2>' +
        '           <span flex></span>' +
        '           <md-button class="md-icon-button" ng-click="panelCtrl.close()" arial-label="cancel"><md-icon md-svg-icon="close"></md-icon></md-button>' +
        '       </div>' +
        '   </md-toolbar>' +
        '   <div class="panel-content">' +
        '       <p>' + msg + '</p>' +
        '   </div>' +
        '   <div class="panel-action">' +
        '       <md-button id="btnConfirmOk" ng-click="panelCtrl.ok()">Yes</md-button>' +
        '       <md-button id="btnConfirmNo" ng-click="panelCtrl.no()" md-autofocus>No</md-button>' +
        '   </div>' +
        '</div>',
        disableParentScroll: true,
        hasBackdrop: true,
        panelClass: 'demo-dialog-example',
        position: position,
        trapFocus: true,
        zIndex: 250,
        clickOutsideToClose: true,
        escapeToClose: true,
        focusOnOpen: true
    };

    $mdPanel.open(config);
}
