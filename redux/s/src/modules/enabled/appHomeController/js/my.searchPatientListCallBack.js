my.searchPatientListCallBack = function(data){
    if (data.count>0 && data.count<=500) {
        my.alertPatientList = data.patientList;
        my.searchDialog.close();
    } else {
        my.alertPatientList = [];
    }
    my.patientCount = data.count;
}