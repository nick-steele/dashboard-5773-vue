my.changeSortColumn = function (columnName, event) {
    if(my.currentSortElement)
        $(my.currentSortElement).removeClass('sorting');
    $(event.target).addClass('sorting');
    my.currentSortElement = event.target; 
    if (my.sortColumn === columnName) {
        my.sortOrder = my.sortOrder === 'asc' ? 'desc' : 'asc';
        my.reverse = !my.reverse;
    } 
    else {
        my.reverse = false;
        my.sortColumn = columnName;
        my.sortOrder = 'asc';
    }
}