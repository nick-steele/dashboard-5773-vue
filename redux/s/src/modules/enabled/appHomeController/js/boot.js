function boot() {
    var trace = new $trace('home.boot');
    my.appCtrl = $model.get('app');
    my.constants = constants;
    //just for test rendering tab and widget
    my.taskBadgeFilter = {};

    my.ranges = [
        { index: 0, label: "Today" },
        { index: 7, label: "Next 7 Days" },
        { index: 30, label: "Next 30 Days" }
    ];
    my.chartRanges = {
        7: "Last 7 Days",
        14: "Last 14 Days",
        30: "Last 30 Days"
    };

    my.taskBadgeDescription = {
        overdue: 'OVERDUE TASKS',
        due: 'DUE TASKS',
        careplan: 'CARE PLAN TASKS',
        assessment: 'ASSESSMENT TASKS'
    };

    my.patientsToggle = {
        1: "My Patient Panel",
        2: "My Org Patients"
    };

    my.currentPatientToggle = 1;

    my.currentChartRange = 7;

    my.currentFilter = {
        currentpatienttoggle: 1,
        currentchartrange: 7
    };

    my.currentRange = 'DY';

    my.currentMemberName = 'My Tasks Summary';

    my.numberOfTooltipRows = 5;

    my.currentDatePicker = my.ranges[0]

    my.todoTask = {
        count: 0,
        tasks: [],
        email: '',
        interval: 0
    };

    my.overdueTask = {
        count: 0,
        tasks: [],
        email: '',
        interval: 0
    };

    my.cpNonassessTask = {
        count: 0,
        tasks: [],
        email: '',
        interval: 0
    };

    my.cpAssessTask = {
        count: 0,
        tasks: [],
        email: '',
        interval: 0
    };
    my.messageBundle = {};
    my.default_already_added = "Patient is already on your Patient Panel";

    $timeout(function () {
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent('resize', true, false);
        window.dispatchEvent(evt);
    }, 1000);
    $appEvent('remote:appHome:getMessageBundle');

    my.loadOverdueTaskInNavigationBar = function () {
        $appEvent('remote:appGlobal:count', ['appTaskList']);
    }

    my.reloadTaskBadgeCount = function (isFirstLoad) {
        if (!isFirstLoad) {
            my.loadOverdueTaskInNavigationBar();
        }
    }
    $model.get('app').homeCtrl = my;
    my.reloadTaskBadgeCount(true);

    $appEvent('remote:appGlobal:loadMemberList');

    trace.success();

    my.alertEncounterApi = {};
    my.taskApi = {};
    my.issueApi = {};
    my.assessmentApi = {};
    my.goalApi = {};
    my.interventionApi = {};
    my.hideTaskBadgeCount = {};
    my.barTaskLabels = {
        x: 'Task type',
        y: 'Tasks'
    };
    my.currentPermissions = ['AllReport'];
    $timeout(function() {
        var email = $model.get('app').login.email;
    });

    my.issueFilter = {};
    my.assessmentFilter = {};
    my.goalFilter = {};
    my.interventionFilter = {};
    my.patientNoIssue = {};
    my.patientNoAssessment = {};
    my.patientNoGoal = {};
    my.patientNoIntervention = {};
    my.dashboards = $model.get('app').settings.apps.home.dashboards;
}
