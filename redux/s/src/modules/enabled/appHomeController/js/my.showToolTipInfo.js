my.showToolTipInfo = function(data, patientInfo) {
    var position = $mdPanel.newPanelPosition()
        .absolute()
        .center();

    var popupType = data.popupType;
    var patientId = patientInfo.patientId;
    var patientName = patientInfo.firstname + ' ' + patientInfo.lastname;
    var dataDetail = {};

    function PanelController(mdPanelRef) {
        var panel = this;
        this.data = data;
        panel._mdPanelRef = mdPanelRef;

        dataDetail.detailItems = [];
        switch(popupType) {            
            case 'ALERT':
                dataDetail.title1 = 'Severity';
                dataDetail.title2 = 'Alert Type';
                
                if(my.alertPopupChartData && my.alertPopupChartData.alertList && my.alertPopupChartData.alertList[patientId]) {
                    var sourceList = my.alertPopupChartData.alertList[patientId];                
                    angular.forEach(sourceList, function(value, key) {
                        var item = {
                            label1: value.severity,
                            label2: value.alertName
                        };
                        dataDetail.detailItems.push(item);
                    });
                }
                dataDetail.detailItems = $filter('orderWithPriority')(dataDetail.detailItems, ['label1'], [my.alertPopupChartData.label]);
                this.popupTitle = patientName + '\'s ' + 'Alert';
                break;
            case 'ENCOUNTER':
                dataDetail.title1 = 'Frequency';
                dataDetail.title2 = 'Care Plan Encounter';
                if(my.encountersPopupData && my.encountersPopupData.serviceList && my.encountersPopupData.serviceList[patientId]) {
                    var sourceList = my.encountersPopupData.serviceList[patientId];                
                    angular.forEach(sourceList, function(value, key) {
                        var item = {
                            label1: value.label1,
                            label2: value.label2
                        };
                        dataDetail.detailItems.push(item);
                    });
                }
                dataDetail.detailItems = $filter('orderWithPriority')(dataDetail.detailItems, ['label2'], [my.encountersPopupData.label]);
                this.popupTitle = patientName + '\'s ' + 'Encounter';
                break;
            case 'CCP':
                if (data.tooltipTitles) {
                    dataDetail.title1 = data.tooltipTitles[0].toLowerCase();
                    dataDetail.title2 = data.tooltipTitles[1].toLowerCase();
                    if(data.tooltipTitles[2]) {
                        dataDetail.title3 = data.tooltipTitles[2].toLowerCase();
                    }
                }
                
                if(data.tooltip && data.tooltip[patientId]) {
                    var sourceList = data.tooltip[patientId];                
                    angular.forEach(sourceList, function(value, key) {
                        var item = {
                            label1: value.label1,
                            label2: value.label2
                        };
                        if (value.label3) {
                            item.label3 = value.label3;
                        }
                        dataDetail.detailItems.push(item);
                    });
                }
                dataDetail.detailItems = $filter('orderWithPriority')(dataDetail.detailItems, data.selectedFields, data.selectedLabels);
                this.popupTitle = patientName + '\'s ' + dataDetail.title1.split(' ')[0];
                break;

        }
        this.dataDetail = dataDetail;

        this.close = function () {
            var panelRef = panel._mdPanelRef;
            panelRef && panelRef.close().then(function () {
                panelRef.destroy();
            });
        };
        this.cancel = function () {
            this.close();
        };
        this.close = function () {
            var panelRef = panel._mdPanelRef;
            panelRef && panelRef.close().then(function () {
                panelRef.destroy();
            });
        }
    }



    var config = {
        attachTo: angular.element(document.body),
        controllerAs: 'panelCtrl',
        controller: PanelController,
        template: '<div>' +
        '   <md-toolbar class="panel-title">' +
        '       <div class="md-toolbar-tools">' +
        '           <h2 style="text-transform: capitalize;" ng-bind="panelCtrl.popupTitle">' + '</h2>' +
        '           <span flex></span>' +
        '           <md-button class="md-icon-button" ng-click="panelCtrl.cancel()" arial-label="cancel"><md-icon md-svg-icon="close"></md-icon></md-button>' +
        '       </div>' +
        '   </md-toolbar>' +
        '   <div class="panel-content">' +
        '       <table class="fixed_headers">' +
        '           <thead>' +
        '               <tr>' +
        '                   <th style="text-transform: capitalize;" ng-bind="panelCtrl.dataDetail.title1.toLowerCase()"></th>' +
        '                   <th style="text-transform: capitalize;" ng-bind="panelCtrl.dataDetail.title2.toLowerCase()"></th>' +
        '                   <th style="text-transform: capitalize;" ng-if="panelCtrl.dataDetail.title3" ng-bind="panelCtrl.dataDetail.title3.toLowerCase()"></th>' +
        '               </tr>' +
        '           </thead>' +
        '           <tbody>' +
        '               <tr ng-repeat="tooltipData in panelCtrl.dataDetail.detailItems">' +
        '                   <td ng-bind="tooltipData.label1"></td>' +
        '                   <td ng-bind="tooltipData.label2"></td>' +
        '                   <td ng-if="tooltipData.label3" ng-bind="tooltipData.label3"></td>' +
        '               </tr>' +
        '           </tbody>' +
        '       </table>' +
        '   </div>' +
        '   <div class="panel-action" layout="row">' +
        '       <span flex></span>' +
        '       <md-button id="btnConfirmNo" ng-click="panelCtrl.cancel()">Close</md-button>' +
        '   </div>' +
        '</div>',
        disableParentScroll: true,
        hasBackdrop: true,
        panelClass: 'tooltip-dialog',
        position: position,
        trapFocus: true,
        zIndex: 250,
        clickOutsideToClose: true,
        escapeToClose: true,
        focusOnOpen: true
    };

    $mdPanel.open(config);
}