my.showSearchPatientDialog = function (title, data, popupType) {
    var position = $mdPanel.newPanelPosition()
        .absolute()
        .center();

    function PanelController(mdPanelRef, parentScope) {
        parentScope.searchDialog = this;
        var panel = this;
        this.popupType = popupType;
        panel._mdPanelRef = mdPanelRef;
        this.close = function () {
            var panelRef = panel._mdPanelRef;
            panelRef && panelRef.close().then(function () {
                panelRef.destroy();
            });
        };
        this.search = function () {
            if (this.patientSearchText) {
                GWT.activity();
                this.parentScope.patientSearchText = this.patientSearchText;
                this.parentScope.alertPatientList = [false];
                var panelRef = panel._mdPanelRef;
                $appEvent('remote:appHome:searchPatientList', { patients: data, searchText: this.patientSearchText });
            }
        };
        this.cancel = function () {
            this.close();
            $mdDialog.hide();
        };
        this.close = function () {
            var panelRef = panel._mdPanelRef;
            panelRef && panelRef.close().then(function () {
                panelRef.destroy();
            });
        }
    }
    var config = {
        attachTo: angular.element(document.body),
        controllerAs: 'panelCtrl',
        controller: PanelController,
        template: '<div id="searchPopup" class="panel-dialog">' +
        '   <md-toolbar class="panel-title">' +
        '       <div class="md-toolbar-tools">' +
        '           <h2 style="text-transform: capitalize;">' + title.toLowerCase() + '</h2>' +
        '           <span flex></span>' +
        '           <md-button class="md-icon-button" ng-click="panelCtrl.cancel()" arial-label="cancel"><md-icon md-svg-icon="close"></md-icon></md-button>' +
        '       </div>' +
        '   </md-toolbar>' +
        '   <div class="panel-content">' +
        '       <p ng-if="!panelCtrl.parentScope.patientCount">No matches found! Please try again.</p>' +
        '       <p ng-if="panelCtrl.parentScope.patientCount">{{panelCtrl.parentScope.patientCount}} patients <span ng-show="panelCtrl.popupType===\'NO_ENCOUNTER\'">with no encounters </span>exceeds 500 patients display limit. Please enter patient name to refine your search</p>' +
        '       <div>' +
        '           <div style="width: 20px; height: 20px; display: inline-block"></div>' +
        '           <input type="text" press-enter="panelCtrl.search()" ng-model="panelCtrl.patientSearchText" id="txtPatientSearch">' +
        '           <div style="position: relative; display:inline-block; width:20px; height:20px; vertical-align: middle"><div ng-if="panelCtrl.parentScope.alertPatientList[0]===false" class="progress-container" layout="row" layout-align="center center">' +
        '               <md-progress-circular md-diameter="20px" md-mode="indeterminate"></md-progress-circular>' +
        '           </div></div>' +
        '       </div>' +
        '   </div>' +
        '   <div class="panel-action">' +
        '       <md-button id="btnConfirmOk" ng-click="panelCtrl.search()" ng-disabled="!panelCtrl.patientSearchText">Search</md-button>' +
        '       <md-button id="btnConfirmNo" ng-click="panelCtrl.cancel()">Cancel</md-button>' +
        '   </div>' +
        '</div>',
        disableParentScroll: true,
        hasBackdrop: true,
        panelClass: 'demo-dialog-example',
        position: position,
        trapFocus: true,
        zIndex: 250,
        clickOutsideToClose: false,
        escapeToClose: true,
        focusOnOpen: true,
        locals: { parentScope: my }
    };

    $mdPanel.open(config);
};