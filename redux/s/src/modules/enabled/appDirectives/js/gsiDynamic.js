app.directive( 'gsiDynamic', function ( $compile ) {
    return {
        restrict: 'A',
        replace: true,
        scope: true,
        link: function (scope, ele, attrs) {
          scope.$watch(attrs.template, function(html) {
            ele.html(html);
            $compile(ele.contents())(scope);
          });
        }
    }
});