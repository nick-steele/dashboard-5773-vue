/**
 * Created by shirano on 3/29/2017.
 */
app
    .directive('gsiHiddenFrame',['$model', '$log','$timeout', '$window', function ($model, $log, $timeout, $window) {

    function createForm(id, method, path, target, params, onSubmit) {
        var form = document.createElement("form");
        form.setAttribute('id',id);
        form.setAttribute("method", method);
        form.setAttribute("action", path);
        form.setAttribute('target', target);
        form.onsubmit = onSubmit || angular.noop;
        params = params || {};
        for(var key in params) {
            if(params.hasOwnProperty(key)) {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", key);
                hiddenField.setAttribute("value", params[key]);
                form.appendChild(hiddenField);
            }
        }
        var submitBtn = document.createElement("input");
        submitBtn.setAttribute("type", "submit");
        submitBtn.setAttribute("style","display:none;");
        form.appendChild(submitBtn);
        return form;
    }

    function createIframe(name){
        var iframe = document.createElement("iframe");
        iframe.setAttribute("style","display:none;");
        iframe.setAttribute("id", name);
        iframe.setAttribute('name',name);
        iframe.setAttribute('sandbox','allow-same-origin');
        return iframe;
    }

    function linkFn(scope,ele) {


        scope.$on('gsiBackgroundPost',function (event, args) {
            if(args!=="pentaho"){
                $log.warn(args+' not supported yet.');
                return;
            }
            var idPentahoForm = 'bgPentahoForm',
                idPentahoFrame = 'bgPentahoFrame';
            var login = $model.get('app').login;
            var matches = login.ssoPentahoServer &&
                login.ssoPentahoServer.match(/^(https?\:\/\/[^\/?#]+)(?:[\/?#]|$)/i);
            var domain = matches && matches[1];
            if(!domain){
                $log.warn('pentaho server address was not found.');
                return;
            }

            //remove old elements if exist
            ele.find('#'+idPentahoForm) && ele.find('#'+idPentahoForm).remove();
            ele.find('#'+idPentahoFrame) && ele.find('#'+idPentahoFrame).remove();

            ele.append(createIframe(idPentahoFrame));

            ele.append(createForm(
                idPentahoForm,
                'post',
                domain+'/pentaho/j_spring_security_check',
                idPentahoFrame,
                {
                    'j_username':login.email,
                    'j_password':login.special
                }));

            //This won't work if not same domain
            try{
                document.getElementById(idPentahoFrame).contentWindow.onerror=function(e) {
                    $log.warn('Error occurred in children frame: '+ idPentahoFrame, e);
                    return false;
                }
            }catch(e){$log.warn(e);}
            ele.find('#'+idPentahoFrame).on('load',function () {
                $log.info('pentaho login attempted');
                try{
                    var redirectUrl = document.getElementById(idPentahoFrame).contentWindow.location.href;
                    $log.info('redirected to:' +redirectUrl);
                    if(redirectUrl.indexOf('login_error') > -1){
                        $log.warn('Error occurred during Pentaho login.');
                        $model.get('app').settings.apps.login.pentahoAuthorized = false;
                    }else{
                        $window.GWT.setPentahoCalled(true);
                        $model.get('app').settings.apps.login.pentahoAuthorized = true;
                    }
                    $timeout(function () {
                        ele.find('#'+idPentahoForm) && ele.find('#'+idPentahoForm).remove();
                        ele.find('#'+idPentahoFrame) && ele.find('#'+idPentahoFrame).remove();
                    },0);
                }catch(e){
                    if(window.location.href.indexOf("://localhost") > -1) {
                        $log.warn('You are running application on localhost and seeing this error highly because of ' +
                            'CORS policy. You may see authentication popup, however the popup won\'t be displayed in non-localhost environment.');
                        $model.get('app').settings.apps.login.pentahoAuthorized = true;
                    }else{
                        $log.warn('Error occurred during Pentaho login.', e);
                    }
                    $timeout(function () {
                        ele.find('#'+idPentahoForm) && ele.find('#'+idPentahoForm).remove();
                        ele.find('#'+idPentahoFrame) && ele.find('#'+idPentahoFrame).remove();
                    },0);
                }
            });
            ele.find('#'+idPentahoForm).find('input[type="submit"]').click();

        })
    }

    return {
        restrict: 'E',
        replace:true,
        template: '<div style="display:none;"></div>',
        link: linkFn
    }
}]);
