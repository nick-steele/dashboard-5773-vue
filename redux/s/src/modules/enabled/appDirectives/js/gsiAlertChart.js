app.directive('gsiAlertChart', ["$model","$appEvent", function($model,$appEvent){
        return {
            restrict : "AE",
            scope: {
                home: '='
            },
            bindToController: true,
            template : "<ms-widget flippable=\"true\" flex=\"100\" class=\"nopadding-top\">\r\n"+
                        "<!-- Front -->\r\n" +
                        "<ms-widget-front class=\"white-bg\">\r\n"+
                        "<div layout=\"row\">\r\n"+
                        "<div flex layout-align=\"center center\" style=\" text-align:center\">"+
                        "{{ctrl.home.patientsToggle[ctrl.home.currentFilter.currentpatienttoggle]}}-{{ctrl.home.chartRanges[ctrl.home.currentFilter.currentchartrange]}}"+
                        "<\/div>\r\n"+
                        "<md-menu>\r\n"+
                        "<md-button aria-label=\"more\" class=\"md-icon-button\"\r\n"+
                        "ng-click=\"$mdOpenMenu($event)\">\r\n"+
                        "<img src=\"images\/redux\/icons\/dots-vertical.png\">\r\n"+
                        "<\/md-button>\r\n"+
                        "<md-menu-content width=\"2\">\r\n"+
                        "<md-menu-item>\r\n"+
                        "<md-button ng-click=\"flipWidget()\" aria-label=\"Flip widget\">\r\n"+
                        "Setting\r\n"+
                        "<\/md-button>\r\n"+
                        "<\/md-menu-item>\r\n"+
                        "<md-menu-item>\r\n"+
                        "<md-button ng-click=\"ctrl.requestChartData()\" aria-label=\"Refresh widget\">\r\nRefresh\r\n"+
                        "<\/md-button>\r\n"+
                        "<\/md-menu-item>\r\n"+
                        "<\/md-menu-content>\r\n"+
                        "<\/md-menu>\r\n"+
                        "<\/div>\r\n"+
                        "<div class=\"p-16 error-wrapper\" layout=\"row\" flexible-div=\"ctrl.drawAlertChart()\">\r\n"+
                        "<nvd3 id=\"alertChart\" options=\"ctrl.alertChartOpts\" data=\"ctrl.home.alertChartData.chartData\"\r\n"+
                        "api=\"ctrl.alertChartApi\"><\/nvd3>\r\n"+
                        "<error ng-if=\"ctrl.home.isAlertError === true\"\r\n"+
                        "msg=\"\'Error Occurred: We encountered a problem Alert chart. Please try again later.\'\"\r\n"+
                        "reload=\"ctrl.home.loadAlertData()\">\r\n"+
                        "<\/error>\r\n"+
                        "<\/div>\r\n"+
                        "<\/ms-widget-front>\r\n"+
                        "<ms-widget-back class=\"white-bg\">\r\n"+
                        "<div class=\"border-bottom\" layout-wrap layout=\"row\" flex=\"100\">\r\n"+
                        "<div flex layout=\"row\" layout-align=\"center center\">\r\n"+
                        "<span>Setting Alert Chart<\/span>\r\n"+
                        "<\/div>\r\n"+
                        "<md-button class=\"md-icon-button\" ng-click=\"flipWidget()\"\r\n"+
                        "aria-label=\"Flip widget\">\r\n"+
                        "<md-icon md-svg-icon=\"close\"><\/md-icon>\r\n"+
                        "<\/md-button>\r\n"+
                        "<\/div>\r\n"+
                        "<div class=\"ph-16 pv-8\" layout=\"row\" layout-align=\"center center\" flex=\"100\">\r\n"+
                        "<md-select id=\"drpRangePicker\" class=\"simplified font-size-16\" ng-model=\"ctrl.home.currentFilter.currentchartrange\"\r\n"+
                        "aria-label=\"Change date\"\r\n"+
                        "ng-change=\"ctrl.requestChartData()\">\r\n"+
                        "<md-option ng-repeat=\"(key, range) in ctrl.home.chartRanges\" ng-value=\"key\">\r\n{{::range}}\r\n"+
                        "<\/md-option>\r\n"+
                        "<\/md-select>\r\n"+
                        "<\/div>\r\n"+
                        "<div class=\"ph-16 pv-8\" layout=\"row\" layout-align=\"center center\" flex=\"100\">\r\n"+
                        "<md-select id=\"drpOrgPicker\" class=\"simplified font-size-16\"\r\n"+
                        "ng-model=\"ctrl.home.currentFilter.currentpatienttoggle\" aria-label=\"Change date\"\r\n "+
                        "ng-change=\"ctrl.requestChartData()\">\r\n"+
                        "<md-option ng-repeat=\"(key, range) in ctrl.home.patientsToggle\"\r\n"+
                        "id=\"patient-view-option-{{key}}\"  ng-value=\"key\">\r\n"+
                        "{{::range}}\r\n"+
                        "<\/md-option>\r\n"+
                        "<\/md-select>\r\n"+
                        "<\/div>\r\n"+
                        "<\/ms-widget-back>\r\n"+
                        "<\/ms-widget>",
            controllerAs: 'ctrl',

            controller: function($scope, $mdDialog) {
                var my =this;

                my.alertChartOpts = {
                    chart: {
                        type: 'pieChart',
                        height: 420,
                        donut: true,
                        x: function (d) { return d.label; },
                        y: function (d) { return d.value; },
                        showLabels: true,
                        labelType: 'value',
                        noData: 'No Alerts for the patients in this timeframe',
                        valueFormat: function (d) {
                            return d3.format(',.0f')(d);
                        },
                        pie: {
                            startAngle: function (d) { return d.startAngle / 2 - Math.PI / 2 },
                            endAngle: function (d) { return d.endAngle / 2 - Math.PI / 2 },
                            dispatch: {
                                elementClick: function (e) {
                                    var title = e.data.label +  ' Alerts - '
                                        + my.home.patientsToggle[my.home.currentFilter.currentpatienttoggle] + ' - ' + my.home.chartRanges[my.home.currentFilter.currentchartrange];
                                    my.handleChartClick('ALERT', title, e.data);
                                }
                            }
                        },
                        margin: {

                        },
                        duration: 500,
                        showLegend: true,
                        legend: {
                            margin: {
                                top: 12
                            }
                        },
                        title: 'Severity',
                        titleOffset: -10
                    },
                    title: {
                        enable: true,
                        text: 'Patients by Alerts Severity'
                    },
                    noData: {
                        enable: true,
                        text: 'No Alerts for the patients in this timeframe'
                    }
                };

                my.drawAlertChart = function () {
                    my.alertChartApi.update();
                };
              
                my.requestChartData = function () {
                    $appEvent('remote:appHome:loadAlertCount', {type : my.home.currentFilter.currentpatienttoggle, interval : my.home.currentFilter.currentchartrange, userList: my.home.subordinates});


                }
                my.getMessageBundle = function (defaultMsg, msgKey, replaceFunc) {
                    if(home.messageBundle && (home.messageBundle[msgKey])){
                        return replaceFunc(home.messageBundle[msgKey]);
                    } else {
                        return defaultMsg;
                    }

                };

                my.handleChartClick = function(popupType, title, data) {
                    GWT.activity();
                    var homeCtrl = $model.get('home');
                    homeCtrl.popupTitle = title;
                    my.alertPopupChartData =angular.copy(my.home.alertChartData);
                    my.encountersPopupData =angular.copy(my.home.encounters);

                    my.already_added = my.getMessageBundle(my.home.default_already_added, "popup.tooltip.already_added", function (configMsg) {
                        return configMsg;
                    });
                    
                    if (data.patients.size() !== 0) {
                        my.alertPatientList = [];
                        $model.get('app').showDialog('appViewPatients', popupType, null, function() {
                        });
                        if (data.patients.size() <= 500) {
                            my.patientCount = -1;
                            $appEvent('remote:appHome:loadPatientList', {patients : data.patients});
                        }
                        else {
                            $timeout(function() {
                                my.patientCount = data.patients.size();
                                my.showSearchPatientDialog(data.patients, popupType);
                            });
                        }
                    }  else {
                        $mdDialog.show(
                          $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('Message Box')
                            .textContent('There is no patient data')
                            .ariaLabel('There is no patient data')
                            .ok('OK')
                        );
                    }
                };
            }
        }
    }])

