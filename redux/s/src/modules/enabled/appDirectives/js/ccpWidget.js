app.directive('ccpWidget', ['$model', '$appEvent', '$q', '$mdDialog', '$timeout', 'constants', 'gsiCppWidget', function ($model, $appEvent, $q, $mdDialog, $timeout, constants, gsiCppWidget) {

    var template = '<ms-widget layout="row" flex="100" class="nopadding-top">' +
        '<ms-widget-front class="white-bg">' +
        '<div class="progress-container no-delay backdrop" layout="row" layout-align="center center" ng-if="ctrl.data.isLoading">' +
        '<md-progress-circular md-mode="indeterminate"></md-progress-circular>' +
        '</div>' +
        '<div class="ph-16 pv-8 border-bottom" style="font-size: 1.0rem;" layout="row" layout-align="center center" layout-wrap ng-bind="ctrl.chartIns.title">' +
        '</div>' +
        '<div class="ph-16 pv-8" layout="row" layout-align="center center" layout-wrap>' +
        '<div style="width: 147px">' +
        '<md-select ng-change="ctrl.chartIns.updateData()" class="simplified font-size-16" ng-model="ctrl.filter.currentCrit"' +
        'aria-label="Change Criteria">' +
        '<md-option ng-repeat="crit in ctrl.chartIns.criterias" ng-selected="$first" ng-value="crit" ng-bind="crit.text"></md-option>' +
        '</md-select>' +
        '</div>' +
        '<div style="width: 147px" ng-if="ctrl.chartIns.isShowTimeRange">' +
        '<md-select class="simplified font-size-16" ng-model="ctrl.filter.currentTime" ng-change="ctrl.chartIns.updateData()" ' +
        'aria-label="Change time range">' +
        '<md-option ng-repeat="time in ctrl.chartIns.timeRange" ng-selected="$first" ng-value="time" ng-bind="time.text"></md-option>' +
        '</md-select>' +
        '</div>' +
        '</div>' +
        '<div layout="row" layout-wrap class="error-wrapper" flexible-div="ctrl.onResize()" layout-align="center center">' +
        '<span style="font-size: 12px">' +
        '<span class="badge-count" ng-click="ctrl.chartIns.noDataLabelClick()">{{ctrl.patientnodata.patients.length}}</span>' +
        '{{::ctrl.chartIns.noDataLabel}}' +
        '</span>' +
        '<nvd3 class="hide-zero" options="ctrl.chartIns.options" data="ctrl.data.chartData" api="ctrl.chartIns.chartApi"></nvd3>' +
        '<error ng-if="ctrl.data.isError === true || ctrl.patientnodata.isError" ' +
        'msg="\'Error Occurred: We encountered a problem. Please try again later.\'" ' +
        'reload="ctrl.chartIns.updateData()">' +
        '</error>' +
        '</div>' +
        '</ms-widget-front>' +
        '</ms-widget>';

    var controllerFn = function ($scope, $timeout) {
        var self = this;
        this.data = {};
        this.patientnodata = {};
        self.total = 1;
        var total = self.total;
        var myNamespace = {};
        self.priorityStatus = $scope.config.parameter.priorityStatus;
        myNamespace.ccpChart = function(option) {
            var that = {};
            that.type = '';
            that.title = '';
            that.criterias = [{value: 'STATUS', text: 'By Status'}, {value: 'DOMAIN', text: 'By Domain'}];

            that.timeRange = [{value: 15, text: 'Last 15 Days'}, {value: 30, text: 'Last 30 Days'}, {value: 90, text: 'Last 90 Days'}];

            that.isShowTimeRange = self.isshowtimerange === "false" ? false : true;

            that.options = {
                chart: {
                    type: 'pieChart',
                    height: 500,
                    stacked: true,
                    donut: true,
                    x: function(d){return d.label;},
                    y: function(d){return d.value;},
                    showLabels: true,
                    showValues: true,
                    labelType: 'value',
                    valueFormat: function (d) {
                        return d3.format(',.0f')(d);
                    },
                    showLegend: true,
                    duration: 0,
                    showControls: false,
                    legend: {
                        margin: {
                            bottom: 20,
                            left: 0
                        }
                    },
                    legendPosition: 'bottom',
                    xAxis: {
                        rotateLabels: -23,
                        fontSize: 11
                    },
                    yAxis: {
                        tickFormat: function (n) {
                            return d3.format('d')(n);
                        },
                        ticks: 10,
                        axisLabel: 'Number of Patients'
                    },
                    groupSpacing: 0.4,
                    reduceXTicks: false,
                    pie: {
                        dispatch: {
                            elementClick: function (e) {
                                var homeCtrl = $model.get('home');
                                var title = '';
                                title = e.data.label + ' - ' + self.type + ' - ' +
                                self.getDisplayName(homeCtrl.currentMember) + self.getTimeText(" - ");
                                title = self.capitalize(title);

                                var fields = gsiCppWidget.getCcpWidgetSortFields(self.type, self.filter.currentCrit.value);
                                var selectedLabels = gsiCppWidget.getCcpSortLabel(self.filter.currentCrit.value,  [e.data.label]);

                                that.handleChartClick({
                                    popupType: 'CCP',
                                    tooltip: self.data.patientData,
                                    tooltipTitles: that.tooltipTitles,
                                    selectedFields: fields,
                                    selectedLabels: selectedLabels,
                                }, title, e.data, self.capitalize(self.type));
                            }
                        }
                    },
                    multibar: {
                        dispatch: {
                            elementClick: function (e) {
                                var homeCtrl = $model.get('home');
                                var title = '';
                                title = e.data.key + ' - ' + self.type + ' - ' +
                                self.getDisplayName(homeCtrl.currentMember) + self.getTimeText(" - ");
                                title = self.capitalize(title);

                                var fields = gsiCppWidget.getCcpWidgetSortFields(self.type, self.filter.currentCrit.value);
                                var selectedLabels = gsiCppWidget.getCcpSortLabel(self.filter.currentCrit.value, [e.data.label], self.priorityStatus);

                                that.handleChartClick({
                                    popupType: 'CCP',
                                    tooltip: self.data.patientData,
                                    tooltipTitles: that.tooltipTitles,
                                    selectedFields: fields,
                                    selectedLabels: selectedLabels,
                                }, title, e.data, self.capitalize(self.type));
                            }
                        }
                    }
                },
                noData: {
                    enable: true,
                    text: 'No data in this timeframe'
                }
            };

            that.noDataLabelClick = function() {
                var title = self.capitalize('Patients with No' + ' ' + self.type + ' - ' +
                            self.getDisplayName($model.get('home').currentMember) + self.getTimeText(" - "));
                that.handleChartClick({
                    popupType: 'CCP',
                    noToolTip: true
                }, title, self.patientnodata, self.capitalize('No ' + self.type));
            };

            that.changeChartType = function() {
                if(self.filter.currentCrit.value === 'DOMAIN') {
                    that.options.chart.type = 'pieChart';
                    that.options.chart.margin = { top: 0 };
                    that.options.chart.showLegend = true;
                }
                else if (self.filter.currentCrit.value === 'TYPE') {
                    that.options.chart.type = 'pieChart';
                    that.options.chart.margin = { top: 0 };
                    that.options.chart.showLegend = true;
                }
                else if (self.filter.currentCrit.value === 'NUMBERPERPATIENT') {
                    that.options.chart.type = 'multiBarHorizontalChart';
                    that.options.chart.margin = { bottom: 70 };
                    that.options.chart.showLegend = false;
                    that.options.chart.yAxis.axisLabelDistance = -5;
                }
                else {
                    that.options.chart.type = 'multiBarChart';
                    that.options.chart.margin = { bottom: 70 };
                    that.options.chart.showLegend = false;
                    that.options.chart.yAxis.axisLabelDistance = -25;
                }

            };

            $scope.$watch(function() {
                return self.data.isLoading;
            }, function(newVal) {
                if (!newVal) {
                    if (constants.isIE) {
                        $timeout(function() {
                            that.chartApi.update();
                        }, 500);
                    }
                    that.options.noData.enable = true;
                }
            });

            that.updateData = function() {
                self.data.isLoading = true;
                self.patientnodata.isLoading = true;
                $timeout(function() {
                    that.chartApi.refresh();
                });
                if (self.filter.currentCrit) {
                    that.options.noData.enable = false;
                    self.data.chartData = [];
                    that.changeChartType();
                    var homeCtrl = $model.get('home');
                    var userIds = [];
                    $model.get('home').selectedSubordinates.forEach(function(user) {
                        userIds.push(user.userId)
                    });

                    var requestData = {
                        entity: that.type,
                        groupBy: self.filter.currentCrit.value,
                        period: self.filter.currentTime !== undefined ? self.filter.currentTime.value : -1,
                        type: homeCtrl.currentMember.type,
                        subordinateIds: userIds,
                        status: ''
                    }
                    $appEvent('remote:ccpDashboard:' + that.type, requestData);
                    $appEvent('remote:ccpDashboard:' + that.noData, requestData);
                }
            };

            that.handleChartClick = function (popupInfo, title, data, patientSearchPopupTitle) {
                GWT.activity();
                var homeCtrl = $model.get('home');
                homeCtrl.popupTitle = title;
                homeCtrl.already_added = self.getMessageBundle(homeCtrl.default_already_added, "popup.tooltip.already_added", function (configMsg) {
                    return configMsg;
                });
                if (data.patients.size() !== 0) {
                    homeCtrl.alertPatientList = [];
                    $model.get('app').showDialog('appViewPatients', popupInfo, null, function () {
                    });
                    if (data.patients.size() <= 500) {
                        homeCtrl.patientCount = -1;
                        $appEvent('remote:appHome:loadPatientList', { patients: data.patients });
                    } else {
                        $timeout(function () {
                            homeCtrl.patientCount = data.patients.size();
                            homeCtrl.showSearchPatientDialog(patientSearchPopupTitle, data.patients, popupInfo.popupType);
                        });
                    }
                } else {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('Message Box')
                            .textContent('There is no patient data')
                            .ariaLabel('There is no patient data')
                            .ok('OK')
                    );
                }
            };

            return that;
        }

        myNamespace.issue = function () {
            var that = myNamespace.ccpChart();
            that.type = 'ISSUE';
            that.title = 'PATIENTS WITH INCOMPLETE ISSUES';
            that.noData = 'patientNoIssue';
            that.noDataLabel = 'Patient(s) with No Issues';
            that.criterias = [{value: 'STATUS', text: 'By Status'}, {value: 'DOMAIN', text: 'By Domain'}, {value: 'NUMBERPERPATIENT', text: 'By Count Per Patient'}];
            that.tooltipTitles = ['Issue Domain', 'Issue Status', 'Issue/Need Title'];
            return that;
        }

        myNamespace.goal = function () {
            var that = myNamespace.ccpChart();
            that.type = 'GOAL';
            that.title = 'PATIENTS WITH GOALS';
            that.noData = 'patientNoGoal';
            that.noDataLabel = 'Patient(s) with No Goals';
            that.criterias = [{value: 'STATUS', text: 'By Status'}, {value: 'NUMBERPERPATIENT', text: 'By Count Per Patient'}];
            that.tooltipTitles = ['Goal Status', 'Goal/Objective'];
            return that;
        }

        myNamespace.intervention = function () {
            var that = myNamespace.ccpChart();
            that.type = 'INTERVENTION';
            that.title = 'PATIENTS WITH INTERVENTIONS';
            that.noData = 'patientNoIntervention';
            that.noDataLabel = 'Patient(s) with No Interventions';
            that.tooltipTitles = ['Intervention Status', 'Intervention Plan'];
            that.criterias = [{value: 'STATUS', text: 'By Status'}, {value: 'NUMBERPERPATIENT', text: 'By Count Per Patient'}];
            return that;
        }
        var chartIns = myNamespace[this.type]();
        this.chartIns = chartIns;
        this.api.refresh = this.chartIns.updateData;
        this.getDisplayName = function (subordinate) {
            if(subordinate.type === constants.VIEW_TYPE.MY_VIEW) {
                return constants.VIEW.MY_VIEW_DISPLAY_TEXT;
            }
            else if(subordinate.type === constants.VIEW_TYPE.ORG_VIEW) {
                return constants.VIEW.MY_ORG_VIEW_DISPLAY_TEXT;
            }
            else {
                var selectedSubordinates = $model.get('home').selectedSubordinates;
                if (selectedSubordinates.size() === 1)
                    return constants.VIEW.getUserViewDisplayText(selectedSubordinates[0].firstName, selectedSubordinates[0].lastName);
                else if(selectedSubordinates.size() < $model.get('app').subordinates.size())
                    return constants.VIEW.MY_TEAM_VIEW_SOME_DISPLAY_TEXT;
                else
                    return constants.VIEW.MY_TEAM_VIEW_DISPLAY_TEXT;
            }
        };

        this.getTimeText = function (separator) {
            if (self.filter.currentTime !== undefined) {
              return separator + self.filter.currentTime.text;
            }

            return "";
        };

        this.onResize = function() {
            if(constants.isIE) {
                if(this.chartIns.options.chart.type !== 'pieChart') {
                    this.chartIns.chartApi.refresh();
                }
            }
        };

        this.capitalize = function(str, lower) {
            return (lower ? str.toLowerCase() : str).replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
        };
        this.getMessageBundle = function (defaultMsg, msgKey, replaceFunc) {
            if (self.messageBundle && (self.messageBundle[msgKey])) {
                return replaceFunc(self.messageBundle[msgKey]);
            } else {
                return defaultMsg;
            }
        };
    }

    return {
        restrict: 'E',
        template: template,
        scope: false,
        bindToController: {
            data: '=',
            type: '@',
            filter: '=',
            patientnodata: '=',
            api: '=',
            isshowtimerange: '@'
        },
        controller: controllerFn,
        controllerAs: 'ctrl'
    }
}]);
