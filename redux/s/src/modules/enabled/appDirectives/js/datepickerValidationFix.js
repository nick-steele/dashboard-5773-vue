;(function(angular, app){

    /**
     * A patch directive for Angular Material defect that md-datepicker does not run validation on minDate/maxDate
     * changes. Taken from {@link https://github.com/angular/material/issues/6086}
     */
    app.directive('datepickerValidationFix',datepickerValidationFix);

    function datepickerValidationFix() {
        return {
            restrict: 'A',
            require: ['mdDatepicker', 'ngModel'],
            link: function (scope, element, attrs, ctrls) {
                var mdDatepickerCtrl = ctrls[0];
                // Fix to run validation when a datepicker's minDate changes
                // Bug #5938
                mdDatepickerCtrl.$scope.$watch(function () { return mdDatepickerCtrl.minDate; }, function () {
                    if (mdDatepickerCtrl.dateUtil.isValidDate(mdDatepickerCtrl.date)) {
                        mdDatepickerCtrl.updateErrorState.call(mdDatepickerCtrl);
                    }
                });

                // Fix to clear error state when setting date programatically from null
                // Bug #6086
                mdDatepickerCtrl.$scope.$watch(function () { return mdDatepickerCtrl.date; }, function (newVal, oldVal) {
                    if (newVal && !oldVal) {
                        mdDatepickerCtrl.updateErrorState.call(mdDatepickerCtrl);
                    }
                });


            }
        };
    }

})(angular, app);