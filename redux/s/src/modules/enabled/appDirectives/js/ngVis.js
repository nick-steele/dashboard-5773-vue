(function(angular, vis, app){

    app.factory('VisDataSet', function () {
            'use strict';
            return function (data, options) {
                // Create the new dataSets
                return new vis.DataSet(data, options);
            };
        });

    app.directive('visTimeline', function(){
        return {
            restrict: 'EA',
            transclude: false,
            scope: {
                data: '=',
                options: '=',
                events: '='
            },
            link: function (scope, element, attr) {
                var timelineEvents = [
                    'rangechange',
                    'rangechanged',
                    'timechange',
                    'timechanged',
                    'select',
                    'doubleClick',
                    'click',
                    'contextmenu'
                ];

                // Declare the timeline
                var timeline = null;

                scope.$watch('data', function () {//FIXME no need to watch option
                    // Sanity check
                    if (!scope.data) {return;}

                    if (timeline !== null) {
                        timeline.destroy();
                    }

                    // Create the timeline object

                    timeline = new vis.Timeline(element[0], scope.data.items, scope.data.groups, scope.options);

                    // Attach an event handler if defined
                    angular.forEach(scope.events, function (callback, event) {
                        if (timelineEvents.indexOf(String(event)) >= 0) {
                            timeline.on(event, callback);
                        }
                    });

                    // onLoad callback
                    if (scope.events != null && scope.events.onload != null &&
                        angular.isFunction(scope.events.onload)) {
                        scope.events.onload(timeline);
                    }
                });

                scope.$watchCollection('options', function (options) {
                    if (timeline == null) {
                        return;
                    }
                    timeline.setOptions(options);
                });
            }
        };
    })

})(angular, vis, app);