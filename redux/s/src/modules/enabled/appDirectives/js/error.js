app.directive('error',["$model","$appEvent", "$q", "$mdDialog", function($model,$appEvent,$q, $mdDialog){
    var template = '<div class="error-container">' +
    '                   <md-icon style="color: #ff5722" ng-if="type!==\'warning\'" md-svg-icon="error"></md-icon>' +
    '                   <md-icon style="color: orange" ng-if="type===\'warning\'" md-svg-icon="warning"></md-icon>' +
    '                   <p ng-bind="msg"></p>' +
    '                   <md-button ng-if="reload" ng-click="reload()">Reload</md-button>' +
    '               </div>';

    return {
        restrict: 'E',
        replace: false,
        scope:{
            msg: '=',
            reload: '&?',
            type: '@'
        },
        template: template
    }
}]);
