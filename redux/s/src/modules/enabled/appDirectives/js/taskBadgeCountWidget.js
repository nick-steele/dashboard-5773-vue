app.directive('taskBadgeCountWidget',["$model", "$appEvent", "$log", "taskBadgeFilter", "constants", function($model, $appEvent, $log, taskBadgeFilter, constants){

    var template =
                "<div class=\"widget-group\" layout=\"row\" flex=\"100\" layout-wrap>\n" +
                "        <ms-widget layout=\"row\" flex=\"100\">\n" +
                "            <ms-widget-front class=\"white-bg\">\n" +
                "                <div class=\"ph-16 pv-8 border-bottom\" layout=\"row\" layout-align=\"space-between center\" layout-wrap>\n" +
                "                    <div class=\"pv-8\" layout=\"row\" layout-align=\"start center\" layout-align-sm=\"end\" flex-sm=\"100\">\n" +
                "                        <md-select id=\"drpDatePicker\" class=\"simplified font-size-16\" ng-model=\"home.currentDatePicker\" aria-label=\"Change date\"\n" +
                "                            ng-change=\"changeTaskSelection()\">\n" +
                "                            <md-option ng-repeat=\"range in ::home.ranges\" ng-value=\"range\">\n" +
                "                                {{::range.label}}\n" +
                "                            </md-option>\n" +
                "                        </md-select>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "                <div class=\"widget-group layout-wrap ng-scope layout-row error-wrapper\" layout=\"row\" layout-wrap=\"\">\n" +
                "                    <error ng-if=\"home.isTaskBadgeError === true\"\n" +
                "                        msg=\"'Error Occurred: We encountered a problem Task Badges. Please try again later.'\"\n" +
                "                        reload=\"reloadTaskBadgeCount()\">\n" +
                "                    </error>\n" +
                "                    <error type=\"warning\" ng-if=\"hidewidget.value === true\"\n" +
                "                        msg=\"'This mode view is not supported in task badge count.'\">" +
                "                    </error>\n" +
                "                    <ms-widget id=\"overdueTaskBox\" class=\"ms-widget-custom\" flippable=\"true\" layout=\"column\" flex=\"100\" flex-gt-xs=\"50\" flex-gt-md=\"25\">\n" +
                "                        <!-- Front -->\n" +
                "                        <ms-widget-front class=\"white-bg\" ng-click=\"openTaskManager('overdue');\" style=\"cursor:pointer;\">\n" +
                "                            <div class=\"pt-32 pb-32\" layout=\"column\" layout-align=\"center center\">\n" +
                "                                <div class=\"red-fg font-size-72 line-height-72 ng-binding\" id=\"overdueTaskCount\">\n" +
                "                                    {{home.overdueTask.count}}\n" +
                "                                </div>\n" +
                "                                <div class=\"h3 secondary-text font-weight-500\" style=\"font-size:0.9rem\" id=\"overdueFooter\" align=\"center\">{{::home.taskBadgeDescription.overdue}} (TODAY)</div>\n" +
                "                            </div>\n" +
                "                        </ms-widget-front>\n" +
                "                    </ms-widget>\n" +
                "\n" +
                "                    <ms-widget id=\"todoTaskBox\" class=\"ms-widget-custom\" flippable=\"true\" layout=\"column\" flex=\"100\" flex-gt-xs=\"50\" flex-gt-md=\"25\">\n" +
                "\n" +
                "                        <!-- Front -->\n" +
                "                        <ms-widget-front class=\"white-bg\" ng-click=\"openTaskManager('todo');\" style=\"cursor:pointer;\">\n" +
                "                            <div class=\"pt-32 pb-32\" layout=\"column\" layout-align=\"center center\">\n" +
                "                                <div class=\"light-blue-fg font-size-72 line-height-72\" id=\"todoTaskCount\">\n" +
                "                                    {{home.todoTask.count}}\n" +
                "                                </div>\n" +
                "                                <div class=\"h3 secondary-text font-weight-500\" style=\"font-size:0.9rem\" id=\"todoTaskFooter\" align=\"center\">{{::home.taskBadgeDescription.due}}</div>\n" +
                "                            </div>\n" +
                "                        </ms-widget-front>\n" +
                "                    </ms-widget>\n" +
                "\n" +
                "\n" +
                "                    <ms-widget id=\"nonAssessTaskBox\" class=\"ms-widget-custom\" flippable=\"true\" layout=\"column\" flex=\"100\" flex-gt-xs=\"50\" flex-gt-md=\"25\">\n" +
                "\n" +
                "                        <!-- Front -->\n" +
                "                        <ms-widget-front class=\"white-bg\" ng-click=\"openTaskManager('careplan');\" style=\"cursor:pointer;\">\n" +
                "                            <div class=\"pt-32 pb-32\" layout=\"column\" layout-align=\"center center\">\n" +
                "                                <div class=\"orange-fg font-size-72 line-height-72\" id=\"nonAssessTaskCount\">\n" +
                "                                    {{home.cpNonassessTask.count}}\n" +
                "                                </div>\n" +
                "                                <div class=\"h3 secondary-text font-weight-500\" style=\"font-size:0.9rem\" id=\"nonAssessTaskFooter\" align=\"center\">{{::home.taskBadgeDescription.careplan}}</div>\n" +
                "                            </div>\n" +
                "                        </ms-widget-front>\n" +
                "                    </ms-widget>\n" +
                "\n" +
                "\n" +
                "                    <ms-widget id=\"assessTaskBox\" class=\"ms-widget-custom\" flippable=\"true\" layout=\"column\" flex=\"100\" flex-gt-xs=\"50\" flex-gt-md=\"25\">\n" +
                "\n" +
                "                        <!-- Front -->\n" +
                "                        <ms-widget-front class=\"white-bg\" ng-click=\"openTaskManager('assessments');\" style=\"cursor:pointer;\">\n" +
                "                            <div class=\"pt-32 pb-32\" layout=\"column\" layout-align=\"center center\">\n" +
                "                                <div class=\"blue-grey-fg font-size-72 line-height-72\" id=\"assessTaskCount\">\n" +
                "                                    {{home.cpAssessTask.count}}\n" +
                "                                </div>\n" +
                "                                <div class=\"h3 secondary-text font-weight-500\" style=\"font-size:0.9rem\" id=\"assessTaskFooter\" align=\"center\">{{::home.taskBadgeDescription.assessment}}</div>\n" +
                "                            </div>\n" +
                "                        </ms-widget-front>\n" +
                "                    </ms-widget>\n" +
                "                </div>\n" +
                "            </ms-widget-front>\n" +
                "        </ms-widget>\n" +
                "    </div>"

    return {
        restrict: 'E',
        replace: false,
        scope:{
            name: '@',
            api: '=',
            hidewidget: '='
        },
        template:template,
        link:function(scope){
            scope.home = $model.get("home");
            scope.app = $model.get("app");
            scope.home.todoTask = {
              count: 0,
              tasks: [],
              email: '',
              interval: 0
          };

          scope.home.overdueTask = {
              count: 0,
              tasks: [],
              email: '',
              interval: 0
          };

          scope.home.cpNonassessTask = {
              count: 0,
              tasks: [],
              email: '',
              interval: 0
          };

          scope.home.cpAssessTask = {
              count: 0,
              tasks: [],
              email: '',
              interval: 0
          };

          scope.home.ranges = [
              { index: 0, label: "Today" },
              { index: 7, label: "Next 7 Days" },
              { index: 30, label: "Next 30 Days" }
          ];

          scope.home.patientsToggle = {
              1: "My Patient Panel",
              2: "My Org Patients"
          };

          scope.home.taskBadgeDescription = {
              overdue : 'OVERDUE TASKS',
              due : 'DUE TASKS',
              careplan : 'CARE PLAN TASKS',
              assessment : 'ASSESSMENT TASKS'
          };

          scope.home.currentRange = 'DY';

          scope.home.currentDatePicker =  scope.home.ranges[0];

          scope.reloadTaskBadgeCount = function(isFirstLoad) {
            if (scope.home.currentMember === undefined)
              $appEvent('remote:appHome:loadTaskCount', {interval :  scope.home.currentDatePicker.index, type : constants.VIEW_TYPE.MY_VIEW});
            else
               $appEvent('remote:appHome:loadTaskCount', {interval :  scope.home.currentDatePicker.index, type: scope.home.currentMember.type,  userList: scope.home.selectedSubordinates });
          }

          scope.reloadTaskBadgeCount(true);

          scope.changeTaskSelection = function () {
              scope.reloadTaskBadgeCount();
          };

          scope.openTaskManager = function(badgeType) {
              if (scope.home.currentMember.type === constants.VIEW_TYPE.MY_VIEW) {
                  taskBadgeFilter.set(badgeType, scope.home.currentMember, scope.home,  constants.VIEW_TYPE.MY_VIEW);
              } else if (scope.home.selectedSubordinates.size() === 1) {
                  taskBadgeFilter.set(badgeType, scope.home.selectedSubordinates[0], scope.home, constants.VIEW_TYPE.TEAM_VIEW_ONE);
              } else if (scope.home.selectedSubordinates.size() < scope.app.subordinates.size()){
                  taskBadgeFilter.set(badgeType, scope.home.currentMember, scope.home, constants.VIEW_TYPE.TEAM_VIEW_SOME);
              } else {
                  taskBadgeFilter.set(badgeType, scope.home.currentMember, scope.home, constants.VIEW_TYPE.TEAM_VIEW);
              }

              var taskScope = $model.scope("task");

              if (taskScope !== undefined) {
                  taskScope.task.switchToTaskBadgeFilterMode();
              }

              var userApps = $model.get("app").userApps;
              taskManagerApp = userApps.filter(function(item) {
                  if (item.applicationName === "GSI_TASK_MANAGER_APP")
                      return true;

                  return false;
              })[0];

              $appEvent(taskManagerApp.event, taskManagerApp);
          };

          scope.api.refresh = function (isOrgMode) {
            scope.hidewidget.value = isOrgMode;
            scope.reloadTaskBadgeCount();
          };
        }
    }
}]);
