
(function(angular, app) {

    /**
     * A controller for {@link gsiProgramDeleteDialog} service.
     */
    app.controller('GsiProgramDeleteDialogCtrl',GsiProgramDeleteDialogCtrl);
    GsiProgramDeleteDialogCtrl.$inject = ['$mdDialog', '$model', '$appEvent','$log'];
    function GsiProgramDeleteDialogCtrl($mdDialog, $model, $appEvent, $log) {
        this.$mdDialog = $mdDialog;
        this.app = $model.get('app');
        if(!$model.get('app').programDeleteReasons){
            $appEvent('remote:patient:getProgramDeleteReasons');
        }
    }

    GsiProgramDeleteDialogCtrl.prototype.getText = function(key){
        return this.app.settings.apps.patient.enrollment.program.text[key];
    };

    GsiProgramDeleteDialogCtrl.prototype.ok = function () {
        this.$mdDialog.hide(this.result);
    };

    GsiProgramDeleteDialogCtrl.prototype.no = function () {
        this.$mdDialog.cancel(this.result);
    };


    /**
     * A service to open program delete dialog. Currently called from <gsi-program-row>.
     */
    app.service('gsiProgramDeleteDialog', function ($mdDialog) {


        var defaultConfig = {
            template: '\
            <div id="programDeletePopup" class="panel-dialog" layout="column"> \
               <div class="panel-content" layout-padding layout="column" flex> \
                   <h3 ng-bind="::vm.getText(\'deleteProgramTitle\')"></h3> \
                   <div ng-if="vm.app.programDeleteReasons" flex layout="column">  \
                        <md-input-container> \
                            <label><span ng-bind="::vm.getText(\'deleteProgramReasonLabel\')"></span></label>  \
                            <md-select  \
                            ng-model="vm.result.deleteReason"  \
                            ng-init="!vm.deleteReason ? vm.result.deleteReason = vm.app.programDeleteReasons[0]:noop()"> \
                                <md-option ng-repeat="opt in vm.app.programDeleteReasons" ng-value="opt"> \
                                    {{opt.value}} \
                                </md-option> \
                            </md-select> \
                        </md-input-container> \
                   </div>\
               </div> \
               <div class="panel-action" layout="row" layout-align="end center" flex="nogrow"> \
                   <md-button id="btnCancelProgramDelete" ng-click="vm.no()">Cancel</md-button> \
                   <md-button id="btnConfirmProgramDelete" ng-click="vm.ok()">\
                    <span ng-bind="::vm.getText(\'deleteProgramButton\')"></span>\
                   </md-button> \
               </div> \
            </div>',
            controller: 'GsiProgramDeleteDialogCtrl',
            controllerAs: 'vm',
            disableParentScroll: true,
            hasBackdrop: true,
            panelClass: 'program-delete-dialog',
            trapFocus: true,
            clickOutsideToClose: true,
            escapeToClose: true,
            focusOnOpen: true,
            multiple:true
        };

        function open(config) {
            var _config = angular.extend({}, defaultConfig, config);
            return $mdDialog.show(_config);
        }

        return {
            open : open
        };

    });

})(angular, app);
