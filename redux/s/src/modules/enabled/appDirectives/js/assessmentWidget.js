app.directive('assessmentWidget', ['$model', '$appEvent', '$q', '$mdDialog', '$timeout', 'constants', 'gsiCppWidget', function ($model, $appEvent, $q, $mdDialog, $timeout, constants, gsiCppWidget) {

    var template = '<ms-widget layout="row" flex="100" class="nopadding-top">' +
        '<ms-widget-front class="white-bg">' +
        '<div class="progress-container no-delay backdrop" layout="row" layout-align="center center" ng-if="ctrl.data.isLoading || ctrl.patientnodata.isLoading">' +
        '<md-progress-circular md-mode="indeterminate"></md-progress-circular>' +
        '</div>' +
        '<div class="ph-16 pv-8 border-bottom" style="font-size: 1.0rem;" layout="row" layout-align="center center" layout-wrap ng-bind="ctrl.chartIns.title">' +
        '</div>' +
        '<div class="ph-16 pv-8" layout="row" layout-align="center center" layout-wrap>' +
        '<div style="width: 147px;">' +
        '<md-select  md-container-class=" custom-assessment-type-box" placeholder="Options" md-on-close="ctrl.revertCurrentState()" md-on-open="ctrl.revertCurrentState()" multiple class="simplified font-size-16" ng-model="ctrl.currentType"' +
        'aria-label="Change assessment type">' +
        '<md-content>' +
        '<md-option ng-click="ctrl.shouldCheck($event, opt.name)" ng-selected="$index<ctrl.MAX_OPTIONS" ng-repeat="opt in ::ctrl.data.options" ng-value="opt.name">{{opt.name + " (" + opt.abbrv + ")"}}</md-option>' +
        '</md-content>' +
        '<md-select-header class="custom-select-footer" ng-if="ctrl.data.options && ctrl.data.options.length>0" layout="row">' +
        '<span class="txt-footer-assessment" flex>' +
        '<div style="display: inherit" ng-if="ctrl.data.options.length > ctrl.MAX_OPTIONS">Selected: &nbsp;<div class="\{\{ctrl.redAssessmentText\}\}" ng-bind="(ctrl.currentType? ctrl.currentType.length : 0) + \'/\' + ctrl.MAX_OPTIONS"></div></div>' + 
        '</span>' +
        '<md-button ng-disabled="!ctrl.currentType || ctrl.currentType.length===0" class="md-primary md-raised" ng-click="ctrl.applyChange()">Apply</md-button>' +
        '</md-select-header>' +
        '</md-select>' +
        '</div>' +
        '<div style="width: 147px" ng-if="ctrl.chartIns.isShowTimeRange">' +
        '<md-select class="simplified font-size-16" ng-model="ctrl.filter.currentTime" ng-change="ctrl.chartIns.updateData()" ' +
        'aria-label="Change time range">' +
        '<md-option ng-repeat="time in ctrl.chartIns.timeRange" ng-selected="$first" ng-value="time" ng-bind="time.text"></md-option>' +
        '</md-select>' +
        '</div>' +
        '</div>' +
        '<div layout="row" layout-wrap class="error-wrapper" flexible-div="ctrl.onResize()" layout-align="center center">' +
        '<span style="font-size: 12px">' +
        '<span class="badge-count" ng-click="ctrl.chartIns.noDataLabelClick()">{{ctrl.patientnodata.patients.length}}</span>' +
        '{{::ctrl.chartIns.noDataLabel}}' +
        '</span>' +
        '<nvd3 class="hide-zero" options="ctrl.chartIns.options" data="ctrl.filteredData" api="ctrl.chartIns.chartApi"></nvd3>' +
        '<error ng-if="ctrl.data.isError === true || ctrl.patientnodata.isError" ' +
        'msg="\'Error Occurred: We encountered a problem. Please try again later.\'" ' +
        'reload="ctrl.chartIns.updateData()">' +
        '</error>' +
        '</div>' +
        '</ms-widget-front>' +
        '</ms-widget>';

    var controllerFn = function ($scope, $timeout, $mdSelect) {
        var self = this;
        this.MAX_OPTIONS = 5;
        this.FULL_ASSESSMENT_CLASS = 'red-assessment-text';
        this.data = {};
        this.patientnodata = {};
        this.filteredData = [];
        this.firstTime = true;
        this.redAssessmentText = '';
        this.shouldCheck = function (event, name) {
            if (this.currentType.length === this.MAX_OPTIONS && this.currentType.indexOf(name)===-1) {
                event.preventDefault();
                event.stopPropagation();

                $mdDialog
                    .show({
                        targetEvent: event,
                        clickOutsideToClose: true,
                        template:
                            '<md-dialog md-theme="default" class="_md md-default-theme md-transition-in assessment-alert" role="alertdialog" tabindex="-1">' +

                            '  <md-dialog-content class="md-dialog-content" role="document" tabindex="-1" id="dialogContent_60">' +
                            '     <h2 class="md-title ng-binding">WARNING</h2>' +
                            '     <p class="ng-binding">You may only select a maximum of 5 assessments to display</p>' +
                            '  </md-dialog-content>' +

                            '  <md-dialog-actions >' +
                            '    <md-button ng-click="closeDialog()" class="md-primary">' +
                            '      OK' +
                            '    </md-button>' +
                            '  </md-dialog-actions>' +
                            '</md-dialog>',
                        controller: AlertController,
                        onShowing: adjustZIndex
                    })
                    .finally(function() {
                        alert = undefined;

                    });
                // change default z-index of angular mdDialog to make it popup over mdSelect
                function adjustZIndex() {
                    $timeout(function() {
                        $('.assessment-alert').parent().css('z-index', '91');
                    });
                }
                function AlertController($scope) {
                    $scope.closeDialog = function() {
                        $mdDialog.hide();
                    }
                }
            }
        };
        this.applyChange = function() {
            this.selectedType = angular.copy(this.currentType);
            this.chartIns.updateData();
            $mdSelect.hide();
        };
        this.changeAssessmentText = function() {
            if(this.currentType) {
                if (this.currentType.length >= this.MAX_OPTIONS) {
                    this.showAssessmentCount = true;
                    this.redAssessmentText = this.FULL_ASSESSMENT_CLASS;
                }
                else {
                    this.redAssessmentText = '';
                    this.showAssessmentCount = false;
                }
            }
        }
        this.filterData = function(currentType) {
            var newAssessments = this.data.chartData ? angular.copy(this.data.chartData) : [];
            var filteredData = [];
            for (var i = 0; i < newAssessments.length; i++) {
                var hasValue = false;
                var newCol = newAssessments[i].values.filter(function(value) {
                    var idx = self.selectedType.indexOf(value.name);
                    if (idx > -1 && value.value > 0)
                        hasValue = true;
                    return idx > -1;
                });
                newAssessments[i].values = angular.copy(newCol);
                if (hasValue)
                    filteredData.push(newAssessments[i]);
            }
            this.filteredData = filteredData;
            this.chartIns.getPatientNoData(this.chartIns.getRequestPayload());

        };
        this.revertCurrentState = function() {
            self.currentType = angular.copy(self.selectedType);
        };
        var deleteWatch = $scope.$watch(function() {
            return self.data.options;
        }, function(newVal) {
            if (newVal && newVal.length>0)  {
                var firstElems = getFieldsValue(newVal, 'name').slice(0, self.MAX_OPTIONS);
                self.selectedType = angular.copy(firstElems);
                self.chartIns.getPatientNoData(self.chartIns.getRequestPayload());
                deleteWatch();
            }
        });
         $scope.$watch(function() {
            return self.currentType;
        }, function(newVal) {
            self.changeAssessmentText();
        });
        $scope.$watch(function() {
            return self.data.chartData;
        }, function(newVal) {
            self.filterData();
        });
        var myNamespace = {};
        myNamespace.ccpChart = function(option) {
            var that = {};
            that.type = '';
            that.title = '';
            that.criterias = [];
            that.timeRange = [];
            that.isShowTimeRange = self.isshowtimerange === "false" ? false : true;

            that.options = {
                chart: {
                    type: 'multiBarChart',
                    height: 500,
                    stacked: true,
                    x: function(d){return d.label;},
                    y: function(d){return d.value;},
                    showLabels: true,
                    showValues: true,
                    labelType: 'value',
                    valueFormat: function (d) {
                        return d3.format(',.0f')(d);
                    },
                    showLegend: true,
                    duration: 0,
                    showControls: false,
                    legend: {
                        margin: {
                            left: 0
                        }
                    },
                    legendPosition: 'top',
                    xAxis: {
                        fontSize: 11,
                        rotateLabels: -23
                    },
                    yAxis: {
                        tickFormat: function (n) {
                            return d3.format('d')(n);
                        },
                        axisLabelDistance: -25,
                        ticks: 10,
                        axisLabel: 'Number of Patients'
                    },
                    tooltip: {
                        contentGenerator: function(data) {
                        var str = '<table style="max-width:350px; white-space: normal"><thead><tr><td colspan="3"><strong>' + data.data.name +
                                    '</strong></td></tr></thead>';
                        if(data.series.length !== 0) {
                            str = str + '<tbody><tr><td></td></tr>';
                            data.series.forEach(function(d){
                                str = str + '<tr><td class="legend-color-guide" width="10%"><div style="background-color:' + d.color + '"></div></td><td width="30%" class="key">' + d.key +
                                        '</td><td>' + d.value + '</td></tr>';
                            });
                            str = str + '</tbody>';
                        }
                        str = str + '<tr><td colspan="3">' + data.data.desc + '</td></tr>'
                        str = str + '</table>';
                        return str;
                        }
                    },
					groupSpacing: 0.4,
                    reduceXTicks: false,
                    multibar: {
                        dispatch: {
                            elementClick: function (e) {
                                var homeCtrl = $model.get('home');
                                var title = '';
                                title = e.data.key + ' - ' + e.data.label + ' ' + self.type + ' - ' +
                                self.getDisplayName(homeCtrl.currentMember) + self.getTimeText(" - ");

                                title = self.capitalize(title);

                                var fields = gsiCppWidget.getCcpWidgetSortFields(self.type, self.filter.currentCrit.value);
                                var selectedLabels = gsiCppWidget.getCcpSortLabel(self.filter.currentCrit.value, [e.data.name, e.data.key]);

                                that.handleChartClick({
                                    popupType: 'CCP',
                                    tooltip: self.data.patientData,
                                    tooltipTitles: that.tooltipTitles,
                                    selectedFields: fields,
                                    selectedLabels: selectedLabels,
                                }, title, e.data, self.capitalize(self.type));
                            }
                        }
                    }
                },
                noData: {
                    enable: true,
                    text: 'No data in this timeframe'
                }
            };
            that.noDataLabelClick = function() {
                var title = self.capitalize('Patients with No' + ' ' + self.type + ' - ' +
                            self.getDisplayName($model.get('home').currentMember) + self.getTimeText(" - "));
                that.handleChartClick({
                    popupType: 'CCP',
                    noToolTip: true
                }, title, self.patientnodata, self.capitalize('No ' + self.type));
            };

            that.changeChartType = function() {
                that.options.chart.type = 'multiBarChart';
                that.options.chart.margin = { bottom: 70 };
                that.tooltipTitle1 = that.type + ' Type';
                that.tooltipTitle2 = that.type + ' Visit Type';
                that.tooltipTitle3 = that.type + ' Status';
                that.tooltipTitles = [that.tooltipTitle1, that.tooltipTitle2, that.tooltipTitle3];
                that.options.chart.showLegend = true;
            };
            that.getRequestPayload = function() {
                that.changeChartType();
                var homeCtrl = $model.get('home');
                var userIds = [];
                $model.get('home').selectedSubordinates.forEach(function(user) {
                    userIds.push(user.userId)
                });
                var dashboards = QA.app.settings.apps.home.dashboards;
                var statusFilter = '';
                for (var i in dashboards) {
                    if (dashboards[i].id === 'CCPDashboard') {
                        var widgets = dashboards[i].widgets;
                        for (var j in widgets) {
                            var widget = widgets[j];
                            if (widget.name.toUpperCase().indexOf(that.type) >= 0) {
                                statusFilter = widget.parameter.status;
                                break;
                            }
                        }
                    }
                }

                var requestData = {
                    entity: that.type,
                    groupBy: 'TYPE',
                    period: self.filter.currentTime !== undefined ? self.filter.currentTime.value : -1,
                    type: homeCtrl.currentMember.type,
                    subordinateIds: userIds,
                    status: statusFilter,
                    entityFilterNames: self.selectedType? self.selectedType : []
                }
                return requestData;
            };

            $scope.$watch(function() {
                return self.data.isLoading;
            }, function(newVal) {
                if (!newVal) {
                    that.options.noData.enable = true;
                }
            });

            that.updateData = function() {
                $timeout(function() {
                    that.chartApi.refresh();
                });
                self.filter.currentCrit = that.criterias[0];
                if (self.filter.currentCrit) {
                    that.options.noData.enable = false;
                    var requestData = that.getRequestPayload();
                    that.getChartData(requestData);
                    if(!self.firstTime) {
                        that.getPatientNoData(requestData);
                        self.firstTime = false;
                    }
                }
            };
            that.getPatientNoData = function(payload) {
                self.patientnodata.isLoading = true;
                $appEvent('remote:ccpDashboard:' + that.noData, payload);
            };
             that.getChartData = function(payload) {
                 self.data.isLoading = true;
                $appEvent('remote:ccpDashboard:' + that.type, payload);
            };
            that.handleChartClick = function (popupInfo, title, data, patientSearchPopupTitle) {
                GWT.activity();
                var homeCtrl = $model.get('home');
                homeCtrl.popupTitle = title;
                homeCtrl.already_added = self.getMessageBundle(homeCtrl.default_already_added, "popup.tooltip.already_added", function (configMsg) {
                    return configMsg;
                });
                if (data.patients.size() !== 0) {
                    homeCtrl.alertPatientList = [];
                    $model.get('app').showDialog('appViewPatients', popupInfo, null, function () {
                    });
                    if (data.patients.size() <= 500) {
                        homeCtrl.patientCount = -1;
                        $appEvent('remote:appHome:loadPatientList', { patients: data.patients });
                    } else {
                        $timeout(function () {
                            homeCtrl.patientCount = data.patients.size();
                            homeCtrl.showSearchPatientDialog(patientSearchPopupTitle, data.patients, popupInfo.popupType);
                        });
                    }
                } else {
                    $mdDialog.show(
                        $mdDialog.alert()
                            .parent(angular.element(document.querySelector('#popupContainer')))
                            .clickOutsideToClose(true)
                            .title('Message Box')
                            .textContent('There is no patient data')
                            .ariaLabel('There is no patient data')
                            .ok('OK')
                    );
                }
            };

            return that;
        }

        myNamespace.assessment = function () {
            var that = myNamespace.ccpChart();
            that.type = 'ASSESSMENT';
            that.title = 'PATIENTS WITH ASSESSMENTS';
            that.noData = 'patientNoAssessment';
            that.noDataLabel = 'Patient(s) with No Selected Assessments';
            that.criterias = [{value: 'TYPE', text: 'By Type'}];
            that.options.chart.donut = false;

            var d = moment().startOf('month');

            var dayDiffByMonth = function(lastMonthNum) {
                return moment().diff(moment(d).subtract({months: lastMonthNum - 1}), 'days') + 1
            }
            that.timeRange = [
                {value: dayDiffByMonth(1), text: 'Current Month'},
                {value: dayDiffByMonth(3), text: 'Last 3 Months'},
                {value: dayDiffByMonth(6), text: 'Last 6 Months'},
                {value: dayDiffByMonth(12), text: 'Last 12 Months'}];

            return that;
        }

        var chartIns = myNamespace[this.type]();
        this.chartIns = chartIns;
        this.api.refresh = this.chartIns.updateData;
        this.getDisplayName = function (subordinate) {
            if(subordinate.type === constants.VIEW_TYPE.MY_VIEW) {
                return constants.VIEW.MY_VIEW_DISPLAY_TEXT;
            }
            else if(subordinate.type === constants.VIEW_TYPE.ORG_VIEW) {
                return constants.VIEW.MY_ORG_VIEW_DISPLAY_TEXT;
            }
            else {
                var selectedSubordinates = $model.get('home').selectedSubordinates;
                if (selectedSubordinates.size() === 1)
                    return constants.VIEW.getUserViewDisplayText(selectedSubordinates[0].firstName, selectedSubordinates[0].lastName);
                else if(selectedSubordinates.size() < $model.get('app').subordinates.size())
                    return constants.VIEW.MY_TEAM_VIEW_SOME_DISPLAY_TEXT;
                else
                    return constants.VIEW.MY_TEAM_VIEW_DISPLAY_TEXT;
            }
        };

        this.getTimeText = function (separator) {
            if (self.filter.currentTime !== undefined) {
              return separator + self.filter.currentTime.text;
            }

            return "";
        }

        this.onResize = function() {
            if(constants.isIE) {
                this.chartIns.chartApi.refresh();
            }
        };

        this.capitalize = function(str, lower) {
            return (lower ? str.toLowerCase() : str).replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
        };
        this.getMessageBundle = function (defaultMsg, msgKey, replaceFunc) {
            if (self.messageBundle && (self.messageBundle[msgKey])) {
                return replaceFunc(self.messageBundle[msgKey]);
            } else {
                return defaultMsg;
            }
        };
        function getFieldsValue(input, field) {
            return input.map(function(o) {
                return o[field];
            });
        }
    }

    return {
        restrict: 'E',
        template: template,
        scope: false,
        bindToController: {
            data: '=',
            type: '@',
            filter: '=',
            patientnodata: '=',
            api: '=',
            isshowtimerange: '@'
        },
        controller: controllerFn,
        controllerAs: 'ctrl'
    }
}]);
