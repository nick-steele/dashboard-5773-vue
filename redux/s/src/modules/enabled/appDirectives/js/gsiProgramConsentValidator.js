(function(angular, app){

    /**
     * Validator directive for Program Consent.
     */
    app.directive('gsiProgramConsentValidator', function(gsiProgramUtils){
        return {
            require:'ngModel',
            scope:{ program: '=', ngModel: '=' },
            link: function(scope, elm, attrs, ctrl){

                function isValid(){
                    return gsiProgramUtils.isConsentValid(scope.program, scope.ngModel);
                }

                scope.$watch('program', function(){
                    ctrl.$setValidity('consent', isValid());
                }, true);

                scope.$watch('ngModel', function(){
                    ctrl.$setValidity('consent', isValid());
                }, true);

            }
        }
    });

})(angular, app);