/**
 * Temporary patch to call GWT function to resize GWT applications when opening side navs
 * (6.0.1) Added patch for nvd3 resize. When removing GWT, refactor this directive and keep nvd3 patch
 */
app.directive('resizeGwtOnTransitionEnd', [
    '$log',
    '$window',
    function ( $log  ,$window ) {
        var transitions = {
            "transition"      : "transitionend",
            "OTransition"     : "oTransitionEnd",
            "MozTransition"   : "transitionend",
            "WebkitTransition": "webkitTransitionEnd"
        };

        var whichTransitionEvent = function () {
            var t,
                el = document.createElement("fakeelement");

            for (t in transitions) {
                if (el.style[t] !== undefined){
                    return transitions[t];
                }
            }
        };

        var transitionEvent = whichTransitionEvent();

        return {
            'restrict': 'A',
            'link': function (scope, element, attrs) {

                element.bind(transitionEvent, function (evt) {
                    if(evt.originalEvent.propertyName!=='width') return;
                    // $log.debug('got a css transition event', evt);
                    GWT.resizeTab();
                    //patch for nvd3 widget resize
                    $window.dispatchEvent(new Event('resize'));
                }).children().bind(transitionEvent,function (evt) {
                    return false;
                });
            }
        };
    }]);