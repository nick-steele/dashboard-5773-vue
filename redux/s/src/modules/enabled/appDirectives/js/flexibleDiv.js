app.directive('flexibleDiv',['$timeout', function ($timeout) {
    return {
        link: function (scope, element, attr) {
            var timeoutPromise;
            var delayInMs = 300;
            // Watching height of parent div
            scope.$watchGroup([function () {
                return element[0].offsetWidth;
            }, function () {
                return element[0].offsetHeight;
            }], function (values) {
                $timeout.cancel(timeoutPromise);
                timeoutPromise = $timeout(function(){   //Set timeout
                    scope.$eval(attr.flexibleDiv);
                }, delayInMs);
            });

        }
    }
}]);