app.directive('chartWidget',["$model","$appEvent", "$q", "$mdDialog", "$timeout", function($model,$appEvent,$q, $mdDialog, $timeout){
    var template = '<div class="p-16 error-wrapper"  flexible-div="drawChart()" width="98%" align="center">' +
                        '<div>' +
                            '{{title}}' +
                        '</div>' +
                        '<nvd3 id="id" options="chartOpts" data="chartdata" api="chartApi" width="100%">' +
                        '</nvd3>' +
                        /*<error ng-if="home.isAlertError === true"
                        msg="'Error Occurred: We encountered a problem Alert chart. Please try again later.'"
                        reload="home.loadAlertData()">
                        </error>*/
                    '</div>' ;

    /*var template1 = '<ms-widget layout="row" flex="95" class="nopadding-top" >' +
        '<ms-widget-front class="white-bg">' +
        
        '<div layout="row" layout-wrap>' +
        '<div class="p-16 error-wrapper" layout="row" flex="95" flex-gt-sm="100" flexible-div="drawChart()" >' +
        '<nvd3 id="idChart" options="chartOpts" data="chartdata" api="chartApi"  id="test456"></nvd3>' +
        '</div>' +
        '</div>' +
        '</ms-widget-front>' +
        '</ms-widget>' ;*/
                           
    return {
        restrict: 'E',
        replace: false,
        //bindToController: {
        scope: {
            id : '@',
            chartdata: '=',
            type: '=',
            title: '=',
            xlabel: '@',
            ylabel: '@'
        },
        link: function (scope, iElement, iAttrs) {
            scope.chartOpts = {
                chart: {
                    type: scope.type,
                    height: 500,
                    valueFormat: function (d) { return d3.format('d')(d);},
                    noData: 'No Care Plan Encounters for the patients in this timeframe 123',
                    x: function (d) { return d.label; },
                    y: function (d) { return d.value; },
                    showValues: true,
                    duration: 500
                },
                title: {
                    enable: false
                    //text: scope.title
                },
                noData: {
                    enable: true,
                    text: 'No Alerts for the patients in this timeframe'
                }
            };

            
            scope.drawChart = function() {
                scope.chartApi.update();
            };

            scope.initChart = function() {
                console.log('initChart:', scope.chartdata);
                
                if(scope.type === 'discreteBarChart') {
                    scope.chartOpts.chart.margin = {
                        top: 50,
                        bottom: 100,
                        left: 100
                    };
                    scope.chartOpts.chart.xAxis = {
                        axisLabel: scope.xlabel,
                        axisLabelDistance: -10,
                        rotateLabels: -23,
                        fontSize: 11
                    };
                    scope.chartOpts.chart.yAxis = {
                        axisLabel: scope.ylabel,
                        axisLabelDistance: -10,
                        tickFormat: function (n) {
                            return d3.format('d')(n);
                        }
                    }
                }
                else if(scope.type === 'pieChart') {
                    scope.chartOpts.chart.donut = true;
                    scope.chartOpts.chart.labelType = 'value';
                    scope.chartOpts.chart.valueFormat = function (d) { return d3.format(',.0f')(d);};
                    scope.chartOpts.chart.pie = {
                        startAngle: function (d) { return d.startAngle / 2 - Math.PI / 2 },
                        endAngle: function (d) { return d.endAngle / 2 - Math.PI / 2 }
                    };
                    scope.chartOpts.chart.margin = {};
                    scope.chartOpts.chart.showLegend = true;
                    scope.chartOpts.chart.legend = {
                        margin: {
                            top: 12
                        }
                    };
                    //scope.chartOpts.chart.titleOffset = -10;
                }
            };

            scope.initChart();
        },

        //controller:'@', //specify the controller is an attribute
        //name:'ctrl', //name of the attribute.
        //controllerAs: 'ctrl',
        /*controller: function ($scope) {
        },*/

        template: template

    }
}]);
