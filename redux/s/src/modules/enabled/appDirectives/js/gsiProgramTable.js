;(function(angular, app, GWT){


    /**
     * Controller of gsiProgramTable directive.
     * */
    app.controller('GsiProgramTableCtrl',GsiProgramTableCtrl);

    GsiProgramTableCtrl.$inject = ['$scope', '$gsiSaveButton', '$filter', '$timeout', '$log','$mdDialog', '$model',
        '$appEvent', '$mdColors', 'gsiProgramUtils'
    ];
    function GsiProgramTableCtrl(
        $scope, $gsiSaveButton, $filter, $timeout, $log, $mdDialog, $model, $appEvent, $mdColors, gsiProgramUtils
    ) {

        //Initialize

        $model.store('gsiProgramTable', $scope);

        this.$scope = $scope;
        this.$model = $model;
        this.$gsiSaveButton = $gsiSaveButton;
        this.app = $model.get('app');
        this.$log = $log;
        this.$appEvent = $appEvent;
        this.$mdDialog = $mdDialog;
        this.$timeout = $timeout;
        this.$filter = $filter;
        this.$mdColors = $mdColors;
        this.utils = gsiProgramUtils;

        /** Store gsiProgramRowCtrl of each row */
        this.rowCtrls = [];

        /** Default column to sort by */
        this.currentOrder = this.app.settings.apps.patient.enrollment.program.defaultSortColumnId;
        /** Default sorting order */
        this.currentOrderDesc = false;

        /** Today's date; Used for validating date inputs
         * TODO should this be updated at midnight? */
        this.today = new Date();
        /** Patient's DoB.
         * TODO this new Date(dob) conversion is dependent on PM response date format... should be revisited */
        this.dob = new Date(this.patientDemog.dob);
        this.isMinor = this.patientDemog.minor;

        /** Styling schemas for modified fields and selected subordinate programs.
         *  Note that md-colors directive throws an error when md-colors object is empty,
         *  thus I'm using ng-style as a workaround. */
        this.modifiedFieldStyle = {color: $mdColors.getThemeColor('default-primary-600')};
        this.newFieldStyle= {background:$mdColors.getThemeColor('default-primary-50')} ;
        this.selectedSubBgColor = {background:$mdColors.getThemeColor('default-primary-500')};

        /** Specify element to attach popups or dialogs. Default is document.body */
        this.attachPopupToElement =
            this.attachPopupTo ? document.querySelector(this.attachPopupTo) : angular.element(document.body);

        /** Bind prototype filter search function to the controller instance.
         * This is required to make the function available in UI (ng-repeat) */
        this.filterSearch = angular.bind(this, this.filterSearchFn);

        /** Load consent styling configuration from tenant settings */
        this.consentConfigMap = getConsentConfigMap(this.app.settings.apps.patient.enrollment.program.consent.statuses);
        this.consentNotRequiredStyle = {color: $mdColors.getThemeColor(this.app.settings.apps.patient.enrollment.program.consent.notRequired.color)};
        this.consentNotRequiredIcon = this.app.settings.apps.patient.enrollment.program.consent.notRequired.icon;
        this.consentNotFilledStyle = {color: $mdColors.getThemeColor(this.app.settings.apps.patient.enrollment.program.consent.notFilled.color)};
        this.consentNotFilledIcon = this.app.settings.apps.patient.enrollment.program.consent.notFilled.icon;

        GWT.activity();

        /** Initializing available program list. See {@link loadAvailableProgramList} for more details. */


        initializeProgramList(this);
        this.loadAvailableProgramList();

        this.$scope.$on('patientProgram:save:attempt', angular.bind(this, this.onSaveAttemptListener));
        this.$scope.$on('patientProgram:delete:success', angular.bind(this, this.onProgramDeleteSuccessListener));
        this.$scope.$on('patientProgram:save:success', angular.bind(this, this.onSaveSuccessListener));

        this.backup = angular.copy(this.patientPrograms);

        var self = this;

        this.$scope.$watchCollection('gsiProgramTable.patientPrograms', angular.bind(this, this.flattenPatientPrograms));


        /**
         * Separates program list to single programs and non-single programs. (non-single == parent enabled)
         * Note that in client mixture of single and non-single programs.
         * {@link availableList} List of available programs with parents
         * {@link singleList} List of available single programs (without parents)
         * */
        function initializeProgramList(self){

            var singleList = [];
            var availableList = [];

            for(var pli=0;pli<self.programList.length;pli++){
                if(self.programList[pli].type==='single')singleList.push(self.programList[pli]);
                else availableList.push(self.programList[pli]);
            }

            self.availableList = availableList;
            self.singleList = singleList;

        }

    }

    // private function; Converts configuration from list to map
    function getConsentConfigMap(consentStatuses){
        var map = {};
        for(var i = 0; i < consentStatuses.length; i++){
            var status = consentStatuses[i];
            map[status.name] = status;
        }
        return map;
    }

    /** Called by child rows to register its controller to the parent*/
    GsiProgramTableCtrl.prototype.registerRow = function (rowCtrl) {
        this.rowCtrls.push(rowCtrl);
    };

    /** Called by child rows to unregister its controller to the parent*/
    GsiProgramTableCtrl.prototype.unregisterRow = function (rowCtrl) {
        for(var i=0;i<this.rowCtrls.length;i++){
            if(this.rowCtrls[i].formName === rowCtrl.formName){
                this.rowCtrls.splice(i,1);
                break;
            }
        }
    };

    /** Calculate parent form pristine status from child form pristine statuses
     *  Note: By default parent.$dirty is not updated by child.$dirty, so this is a workaround to let them sync */
    GsiProgramTableCtrl.prototype.checkParentPristine = function () {
        var allPristine = true;
        for (var k in this.form){
            if (!this.form.hasOwnProperty(k)||!k.startsWith("rowForm_")) continue;
            if(this.form[k].$dirty){
                allPristine=false;
                break;
            }
        }
        if(this.form && allPristine) this.form.$setPristine();
    };




    GsiProgramTableCtrl.prototype.moveToFirstLevel = function (data) {
        var index = this.patientPrograms.indexOf(data);
        if(index!==-1) return; // do nothing
        var found = false;
        for(var i = 0; i<this.patientPrograms.length;i++){
            var pp = this.patientPrograms[i];
            if(pp.program.type !=='complex_parent') continue;
            var sindex = pp.subordinates.indexOf(data);
            if(sindex===-1) continue;
            found = true;
            pp.subordinates.splice(sindex, 1);
            break;
        }
        if(found) this.patientPrograms.push(data);
        else this.$log.error('data was not found from patientPrograms. must be a bug', data, this.patientPrograms);
    };

    GsiProgramTableCtrl.prototype.moveIntoParent = function (data, parentId) {
        if(data.program.type !=='complex_subordinate') throw "program must be complex_subordinate.";
        var index = this.patientPrograms.indexOf(data);
        if(index!==-1){
            this.patientPrograms.splice(index,1);
        }
        var parent;
        for(var i = 0; i<this.patientPrograms.length;i++){
             pp = this.patientPrograms[i];
            if(pp.program.type !=='complex_parent' || pp.program.id!==parentId) continue;
            parent = pp;
            break;
        }

        if(parent){
            var sindex = parent.subordinates.indexOf(data);
            if(sindex===-1) {
                pp.subordinates.push(data);
            }
        }else{
            this.patientPrograms.push({program:{id:parentId, type:'complex_parent'}, subordinates:[data]});
        }
    };

    /** Attempts to save current programs */
    GsiProgramTableCtrl.prototype.onSaveAttemptListener = function () {
        GWT.activity();

        if(this.patientPrograms.length===0) return;

        var self = this;

        var ppList = [];


        angular.forEach(this.patientPrograms, function(value){
            if(value.program.type === 'complex_parent'){
                if(angular.isArray(value.subordinates) && value.subordinates.length){
                    var cp = {program:{id:value.program.id}, consentInfo: formatConsentForSave(value.consentInfo), requestType: 'complex', subordinates:[]};
                    angular.forEach(value.subordinates, function (sub) {
                        if(isModified(sub))
                            cp.subordinates.push(formatForSave(sub));
                    });
                    ppList.push(cp);
                }

            }else{

                if(isModified(value))
                    ppList.push(formatForSave(value));
            }
        });

        self.$log.debug(ppList);

        if(ppList.length === 0){
            self.$log.debug("No program change detected for save. Returning... ");
            return;
        }

        self.status = self.$gsiSaveButton.IN_PROGRESS;

        // Requests server to save
        self.$appEvent('remote:patient:savePatientPrograms', {
            patientId : self.patientDemog.patientId,
            data: ppList
        });


        /** Format and clean up data before sending to the server */
        function formatForSave(patientProgram){

            // If a simple program is terminated, subordinate programs should be emptied
            if(patientProgram.selectedPrograms && patientProgram.status && patientProgram.status.endReasonRequired){
                patientProgram.selectedPrograms = [];
            }

            // Make copy
            var _new = angular.copy(patientProgram);

            // Delete metadata
            delete _new._metadata;

            // Delete all unnecessary properties except its identity property
            _new.program = {id: _new.program.id};
            _new.status = {id: _new.status.id};
            if(_new.endReason)_new.endReason = {id: _new.endReason.id};


            //Delete statusEffectiveDate if it's not editable
            if(!self.statusEffectiveDateEditable){delete _new.statusEffective;}

            if(_new.consentInfo) _new.consentInfo = formatConsentForSave(_new.consentInfo);

            return _new;
        }

        function formatConsentForSave(consentInfo){
            if(!consentInfo) return null;
            var _new = angular.copy(consentInfo);
            if(_new.consentObtainedBy) _new.consentObtainedBy = {userId: _new.consentObtainedBy.userId};
            if(_new.minorConsentObtainedBy) _new.minorConsentObtainedBy = {userId: _new.minorConsentObtainedBy.userId};
            if(_new.consenter) _new.consenter = {code: _new.consenter.code};
            if(_new.consent) _new.consent = {id:_new.consent.id};
            return _new;
        }

        function isModified(pp) {
            return pp._metadata._isNew || self.form[pp._metadata.formName].$dirty;
        }

    };


    /** Called on program delete */
    GsiProgramTableCtrl.prototype.deleteProgramEpisode = function (program, deleteReason) {
        GWT.activity();
        this.$appEvent('remote:patient:deleteProgramEpisode', {
            programId: program.id,
            patientId: this.patientDemog.patientId,
            deleteReasonId: deleteReason.id
        });
    };

    GsiProgramTableCtrl.prototype.flattenPatientPrograms= function(){
        var self = this;

        self._flatPatientPrograms = refactor();

        function refactor(){
            if(!self.patientPrograms || self.patientPrograms.length === 0) return [];
            var arr = [];
            angular.forEach(self.patientPrograms, function(obj){
                if(obj.program && obj.program.type === 'complex_parent'){
                    if(obj.subordinates){
                        angular.forEach(obj.subordinates, function(sub){arr.push(sub)})
                    }
                }else{
                    arr.push(obj);
                }
            });
            return arr;
        }
    };

    /** Called on save success */
    GsiProgramTableCtrl.prototype.onSaveSuccessListener = function (event, data) {
        GWT.activity();
        this.form.$setPristine();
    };

    /** Called on delete success */
    GsiProgramTableCtrl.prototype.onProgramDeleteSuccessListener = function (event, data) {
        GWT.activity();

        // Look for successfully deleted program from list, and remove

        var self = this;

        var deletedPp = self.utils.findPatientProgramsById(data.deletedProgramId, self.patientPrograms,true);

        if(deletedPp) self.deleteRow(deletedPp);
        else self.$log.warn('Deleted program could not be discovered in the list, ', data.deletedProgramId);

        var hasWarning = false;


        if(data.prev && data.prev.errorDetail){         // Check if response data has error warning
            hasWarning = true;
            self.$log.warn(data.prev.errorDetail);
        }else{                            // Check if response data has previous episode data
            hasWarning = pushPreviousEpisode(data.prev)
        }

        // Show result popup
        self.$mdDialog.show(
            self.$mdDialog.alert()
                .clickOutsideToClose(true)
                .textContent(hasWarning?
                    self.app.settings.apps.patient.enrollment.program.text.deleteProgramWarning:
                    self.app.settings.apps.patient.enrollment.program.text.deleteProgramSuccess)
                .ariaLabel('Program Deleted')  //TODO
                .multiple(true)
                .ok('OK') //TODO
        );

        function pushPreviousEpisode(prev) {
            if(!prev) return false;
            if(!prev.program) return true;
            if (prev.program.type !== 'complex_parent') {
                self.patientPrograms.push(prev);
                return false;
            }

            if (!angular.isArray(prev.subordinates)) {
                self.$log.error('Invalid data was returned from service. Missing subordinates.', prev);
                return true;
            }else if(prev.subordinates.length !== 1){
                self.$log.warn('Invalid data was returned from service.', prev);
                return false;
            }

            var parent = self.utils.findPatientProgramsById(prev.program.id, self.patientPrograms);
            if (parent) {
                parent.subordinates.push(prev.subordinates[0]);
                self.flattenPatientPrograms() // need to call flatten
            } else{self.patientPrograms.push(prev);}
            return false;

        }


    };



    /** Called by child row when a row is new and user attempts to delete the row. */
    GsiProgramTableCtrl.prototype.deleteRow = function (data) {
        if(!data) return;
        if(data.program && data.program.type === 'complex_subordinate'){
            var parent =  this.utils.findPatientProgramsById(data.program.parentProgram.id, this.patientPrograms);
            var sindex = parent.subordinates.indexOf(data);
            if(sindex===-1){
                this.$log.error('attempted to delete but the data was not found.', data, this.patientPrograms);
                return;
            }
            parent.subordinates.splice(sindex, 1);
            if(parent.subordinates.length === 0) this.patientPrograms.splice(this.patientPrograms.indexOf(parent),1);
            else this.flattenPatientPrograms();
        }else{
            var index = this.patientPrograms.indexOf(data);
            if(index===-1){
                this.$log.error('attempted to delete but the data was not found.', data, this.patientPrograms);
                return;
            }
            this.patientPrograms.splice(index,1);
        }
    };

    /** Shortcut for accessing consent icon configurations */
    GsiProgramTableCtrl.prototype.getConsentIcon = function (consent) {
        if(!consent || !consent.value) return this.consentNotFilledIcon;
        var config = this.consentConfigMap[consent.value];
        return config ? config.icon : null;
    };

    /** Shortcut for accessing consent color configurations */
    GsiProgramTableCtrl.prototype.getConsentColor = function (consent) {
        if(!consent || !consent.value) return this.consentNotFilledStyle;
        var config = this.consentConfigMap[consent.value];
        return config ? {color: this.$mdColors.getThemeColor(config.color)} : null;
    };

    /** Adds new program */
    GsiProgramTableCtrl.prototype.addNew = function () {
        GWT.activity();
        this.patientPrograms.push({_metadata:{_isNew:true}});
    };




    /** Called on any program select */
    GsiProgramTableCtrl.prototype.onProgramSelect = function () {
        GWT.activity();
        this.loadAvailableProgramList();
    };

    /** Util to recalculate selectable program list to add. A program is not addable if the program already exists
     * in the table. */
    GsiProgramTableCtrl.prototype.loadAvailableProgramList = function () {

        var selectedProgramMap = {};

        //Add existing programs to the selectedProgramMap
        selectedProgramMap = isProgramSelected(selectedProgramMap, this.patientPrograms);

        for(var pli=0;pli<this.programList.length;pli++){
            var program = this.programList[pli];

            if(this.programList[pli].type==='complex_parent'){
                //"complex_parent" program should be disabled if all of its subordinate programs are selected
                var allSelected = true;
                //iterate through subordinate programs of "complex_parent"
                for(var ci =0; ci<this.programList[pli].programs.length;ci++){
                    var subProgram = this.programList[pli].programs[ci];
                    var subIsSelected = (selectedProgramMap[subProgram.id] === true);
                    program.programs[ci].selected = subIsSelected;
                    if(!subIsSelected){allSelected = false;}
                }
                program.selected = allSelected;
            }else{
                program.selected = (selectedProgramMap[program.id] === true)
            }

        }

        function isProgramSelected(map, list) {
            for(var i=0;i<list.length;i++){
                var pp = list[i];
                if(!pp.program) continue;
                if(pp.program.type === 'complex_parent'){
                    if(!angular.isArray(pp.subordinates)) continue;
                    for(var j = 0; j< pp.subordinates.length; j++){
                        if(!pp.subordinates[j] || !pp.subordinates[j].program) continue;
                        map[pp.subordinates[j].program.id] = true;
                    }
                }else{
                    map[pp.program.id] = true;
                }
            }
            return map;
        }

    };


    GsiProgramTableCtrl.prototype.getBackupByProgramId =function (id) {
        return this.utils.findPatientProgramsById(id, this.backup, true);
    };

    GsiProgramTableCtrl.prototype.announceConsentRevert = function(id){
        this.$scope.$broadcast('patientProgram:consent:reverted', id);
    };

    GsiProgramTableCtrl.prototype.announceConsentChanged = function(id){
        this.$scope.$broadcast('patientProgram:consent:changed', id);
    };


    //FIXME
    GsiProgramTableCtrl.prototype.getConsentInfo =function (patientProgram) {
        if(!patientProgram) throw "";
        if(this.utils.isConsentManagedByParent(patientProgram.program)){
            var parentProgramId = patientProgram.program.parentProgram.id;
            var parent = this.utils.findPatientProgramsById(parentProgramId, this.patientPrograms);
            if(!parent) {
                parent = {program:{id:parentProgramId, type:'complex_parent'}, consentInfo:{}, subordinates:[]};
                this.patientPrograms.push(parent);
            }
            return parent.consentInfo;
        }else{
            patientProgram.consentInfo = patientProgram.consentInfo || {};
           return patientProgram.consentInfo;
        }

    };



    /** Changes columns to sort the table by. If the same column is clicked, inverts the order (asc/desc) */
    GsiProgramTableCtrl.prototype.changeOrder = function (id) {
        GWT.activity();
        if(this.currentOrder === id){
            this.currentOrderDesc = ! this.currentOrderDesc;
        }else{
            this.currentOrder = id;
            this.currentOrderDesc = false;
        }
    };


    var MAX_INT = (Number && Number.MAX_SAFE_INTEGER) || 9007199254740991;
    var MIN_INT = (Number && Number.MIN_SAFE_INTEGER) || -9007199254740991


    /** orderBy function called from UI.
     * Don't call GWT.activity within this function.
     * TODO how to treat newly added programs in ordering */
    GsiProgramTableCtrl.prototype.orderByFn = function (ctrl) {
        var self = ctrl;
        return function(item){
            switch(self.currentOrder){
                case "parentProgramName":
                    return 3;
                // if(!item.program) return null;
                // else if(item.program.type==='complex_subordinate') return item.program.parentProgram.name;
                // else return item.program.name;
                // break;
                case "programName":
                    if(!item.program) return null;
                    else if(item.program.type==='simple_parent') return null;
                    else return item.program.name;
                    break;
                case "programEffective":
                    return item.programEffective;
                    break;
                case "programEnd":
                    return item.programEnd;
                    break;
                case "statusEffective":
                    return item.statusEffective;
                    break;
                // case "consentStatus":
                //     return item.consent && item.consent.value;
                case "status":
                    return item.status && item.status.name;
                case "endReason":
                    return item.endReason && item.endReason.value;
                    break;

            }
        }

    };

    function showNew(item){
        return !item || !item._metadata || item._metadata._isNew;
    }


    GsiProgramTableCtrl.prototype.showOld = function (item) {
        return !showNew(item);
    };

    GsiProgramTableCtrl.prototype.showNew = function (item) {
        return showNew(item)
    };

    GsiProgramTableCtrl.prototype.byRowCreated = function (item) {
        if(!item._metadata) return -1;
        return item._metadata.created;
    };

    // Disabled for 6.0. Will be revisited in next release
    GsiProgramTableCtrl.prototype.filterSearchFn = function (item) {
        var self = this;
        if (!self.searchQuery) return true;
        if (!item._metadata) return true;
        var query = self.searchQuery.toLowerCase();
        var arr = query.split(' ');
        var found = true;
        for (var i = 0; i < arr.length; i++) {
            var k = arr[i];
            found = searchForKeyword(item, k);
            if (!found) break;
        }
        return found;

         function searchForKeyword(item, keyword) {
            if(!item.program) return false;
            if(item.program.name && item.program.name.toLowerCase().indexOf(keyword)!= -1) return true;
            if(item.program.parentProgram && item.program.parentProgram.name.toLowerCase().indexOf(keyword)!= -1) return true;
            if(item.program.subPrograms){
                for(var pi=0;pi<item.program.subPrograms.length;pi++){
                    if(item.program.subPrograms[pi].name.toLowerCase().indexOf(keyword)!=-1) return true;
                }
            }
            if(item.status && item.status.name.toLowerCase().indexOf(keyword)!=-1) return true;
            if(item.endReason && item.endReason.value.toLowerCase().indexOf(keyword)!=-1) return true;
            if(item.programEffective && self.$filter('date')(item.programEffective, 'MM/d/yyyy').indexOf(keyword)!=-1) return true;
            if(item.programEnd && self.$filter('date')(item.programEnd, 'MM/d/yyyy').indexOf(keyword)!=-1) return true;
            if(item.statusEffective && self.$filter('date')(item.statusEffective, 'MM/d/yyyy').indexOf(keyword)!=-1) return true;
            // if(item.consent && item.consent.value && item.consent.value.toLowerCase().indexOf(keyword) != -1) return true;
            return false
        }
    }



    var SEARCH_FIELD = '<md-input-container \
                class="program-search-input-container" \
                md-no-float="" flex="">\
                    <md-icon md-svg-icon="{{::gsiProgramTable.utils.getIcon(\'search\')}}"></md-icon>\
                    <input \
                    placeholder="{{::gsiProgramTable.utils.getText(\'searchInputLabel\')}}" \
                    ng-model="gsiProgramTable.searchQuery" \
                    aria-label="{{::gsiProgramTable.utils.getText(\'searchInputLabel\')}}" />\
                </md-input-container>';

    var ADD_BUTTON = '<md-button \
                class="md-icon-button" \
                ng-click="gsiProgramTable.addNew()" \
                ng-disabled="gsiProgramTable.ngDisabled">\
                    <md-icon md-svg-icon="{{::gsiProgramTable.utils.getIcon(\'add\')}}"></md-icon>\
                    <md-tooltip>{{::gsiProgramTable.utils.getText(\'addButtonTooltip\')}}</md-tooltip>\
                </md-button>';

    var HEADER = '\
    <div class="table-header" layout="row" flex="100">\
        <div \
        class="gsi-editable-cell-wrapper icon-cell" \
        flex="nogrow" \
        layout="column" \
        layout-align="center center" \
        flex-order="-20"/>\
        \
        <div class="gsi-editable-cell-wrapper" \
        ng-repeat="(key,column) in gsiProgramTable.columnConfig" \
        ng-class="[{\'icon-cell\': column.iconOnly}, column.cssClass]"\
        flex="{{column.flex?column.flex:\'\'}}" \
        flex-order="{{column.order}}" \
        ng-if="gsiProgramTable.columnConfig[column.id].show"> \
            <md-button \
            aria-label="Sort by {{column.label}}"\
            ng-disabled="column.sortDisabled"\
            ng-click="gsiProgramTable.changeOrder(column.id)" \
            layout-fill="layout-fill" \
            md-no-ink="md-no-ink" \
            layout="row" \
            layout-align="start center">\
                <span class="column-title name" flex="initial" ng-if="column.label">{{column.label}}</span>\
                <md-icon ng-if="!column.label && column.iconOnly"></md-icon>\
                <md-icon \
                flex="none" \
                ng-if="!column.sortDisabled && gsiProgramTable.currentOrder === column.id"\
                md-svg-icon="{{gsiProgramTable.utils.getIcon(gsiProgramTable.currentOrderDesc?\'sortUp\':\'sortDown\')}}"/>\
                <span ng-if="!column.iconOnly" flex="flex"/>\
            </md-button>\
        </div>\
        \
        <div class="gsi-editable-cell-wrapper icon-cell" flex="nogrow" flex-order="20" />\
    </div>\
    ';

    var GSI_EDITABLE_TABLE_CONTENT =    '<form class="gsi-editable-table" layout="row" layout-wrap="" name="gsiProgramTable.form">'
        +HEADER+
        '<div \
        class="padding"\
        ng-if="gsiProgramTable.patientPrograms.length===0"\
        flex="flex">No programs found.\
        </div>\
        <gsi-program-row class="gsi-editable-row"\
        ng-repeat="d in gsiProgramTable._flatPatientPrograms|filter:gsiProgramTable.showNew|orderBy:gsiProgramTable.byRowCreated" data ="d" ></gsi-program-row>\
        <gsi-program-row class="gsi-editable-row"\
        ng-repeat="d in gsiProgramTable.filtered = (gsiProgramTable._flatPatientPrograms|filter:gsiProgramTable.showOld|filter:gsiProgramTable.filterSearch|orderBy:gsiProgramTable.orderByFn(gsiProgramTable):gsiProgramTable.currentOrderDesc)" data ="d" ></gsi-program-row>\
    </form>';


    /** Main directive */
    app.directive('gsiProgramTable', function () {
        return{
            restrict:'E',
            template: '<ng-transclude />',
            transclude:true,
            scope: {
                patientPrograms: '=',
                programList: '=',
                columnConfig: '=',
                statusEffectiveDateEditable : '=',
                patientDemog: '=',
                status: '=',
                ngDisabled: '=',
                form: '='
            },
            controller: 'GsiProgramTableCtrl',
            bindToController: true,
            controllerAs: 'gsiProgramTable'
        }
    });

    /**
     * Subordinate directives
     * Required to have <gsi-program-table> as parent element.
     * Separated these components to give flexibility to move/refactor views
     * */

    /** Add button directive*/
    app.directive('gsiProgramAddButton', function () {
        return{
            restrict:'E',
            template: ADD_BUTTON,
            require: '^^gsiProgramTable',
            link:function (scope, elem, attr, ctrl) {
                scope.gsiProgramTable = ctrl;
            }
        }
    });

    /** Search field directive */
    app.directive('gsiProgramSearchField', function () {
        return{
            restrict:'E',
            template: SEARCH_FIELD,
            require: '^^gsiProgramTable',
            link:function (scope, elem, attr, ctrl) {
                scope.gsiProgramTable = ctrl;
            }
        }
    });

    /** Content directive */
    app.directive('gsiProgramTableContent', function () {
        return{
            restrict:'E',
            template: GSI_EDITABLE_TABLE_CONTENT,
            require: '^^gsiProgramTable',
            link:function (scope, elem, attr, ctrl) {
                scope.gsiProgramTable = ctrl;
            }
        }
    });




})(angular, app, GWT);
