app.directive('changeViewDropdown', ['$model', '$appEvent', '$q', '$mdDialog','$mdPanel', 'constants', function ($model, $appEvent, $q, $mdDialog, $mdPanel, constants) {

    var dropdownTemplate = '<div class="panel-menu-list">' +
		'<div md-ink-ripple ng-class="{\'selected\': dropdownCtrl.selectedView.type == defaultUserView.type}" ' +
        'ng-click="dropdownCtrl.changeViewMode(defaultUserView)" ng-repeat="defaultUserView in dropdownCtrl.app.defaultUserViews|filterViewMenu: {typeNotShown: dropdownCtrl.typeNotShown, hasChild: (dropdownCtrl.app.subordinates && dropdownCtrl.app.subordinates.length > 0)}" class="{{dropdownCtrl.theme[defaultUserView.type]}} panel-menu-item">' +
		'   <div>' +
        '		<md-icon md-svg-icon="{{dropdownCtrl.icons[defaultUserView.type]}}"></md-icon><span class="item-text">{{dropdownCtrl.displaySubordinateInMenu(defaultUserView)}}</span>' +
        '   </div>' +
		'   <md-tooltip md-direction="right" id="{{\'tooltip-\' + defaultUserView.idx}}" ng-if="defaultUserView.type === 1">' +
		'       Displays the patient data from the patients in your Patient Panel' +
		'   </md-tooltip>' +
		'   <md-tooltip md-direction="right" id="{{\'tooltip-\' + defaultUserView.idx}}" ng-if="defaultUserView.type === 2">' +
		'       Displays the patient data from all the patients in your organization to which you have access' +
		'   </md-tooltip>' +
		'   <md-tooltip md-direction="right" id="{{\'tooltip-\' + defaultUserView.idx}}" ng-if="defaultUserView.type === 3">' +
		'       Displays the patient data from the selected subordinates\' patient panels' +
		'   </md-tooltip>' +
		'</div>' +
        '<div ng-if="dropdownCtrl.app.subordinates.length>0">' +
		'   <md-divider style="margin: 5px 0 0 0;background-color: rgba(0,0,0,0.11);height: 1px;border: 0;"></md-divider>' +
        '   <md-content>' +
        '       <div class="panel-menu-item sub-item">' +
        '           <md-checkbox ng-checked="dropdownCtrl.isChecked()" ng-click="dropdownCtrl.toggleAll()" md-no-ink>' +
		'               Select All' +
		'           </md-checkbox>' +
        '       </div>' +
		'       <div class="panel-menu-item sub-item" ng-repeat="subordinateView in dropdownCtrl.app.subordinates">' +
		'           <md-checkbox ng-checked="dropdownCtrl.exists(subordinateView, dropdownCtrl.selectedSubordinates)" ng-click="dropdownCtrl.toggle(subordinateView, dropdownCtrl.selectedSubordinates)" md-no-ink>' +
		'               {{ dropdownCtrl.displaySubordinateInMenu(subordinateView) }}' +
		'           </md-checkbox>' +
		'       </div>' + 
        '   </md-content>' +
        '   <div layout="row" class="custom-select-footer"><span flex></span><md-button class="md-primary md-raised" ng-disabled="!dropdownCtrl.selectedSubordinates.length > 0" ng-click="dropdownCtrl.applyChange()">Apply</md-button></div>' +
	    '</div>' + 
    '</div>';

    var template = '<div class="dropdown-container">' +
                   '    <div class="dropdown-title" ng-if="!ctrl.hideDropDownTitle">' +
                   '        <md-icon md-svg-icon="{{ctrl.icons[ctrl.currentView.type]}}"></md-icon>' +
				   '        <span class="dropdown-txt">{{ctrl.displaySubordinateInMenu(ctrl.currentView)}}</span>' +
                   '    </div>' +
				   '    <span id="{{ctrl.btnId}}" ng-click="ctrl.showMenu($event)" class="dropdown-btn"><md-icon md-svg-icon="{{ctrl.btnIcon}}"></md-icon></span>' +
				   '</div>';
                   
    var displaySubordinateInMenu = function (subordinate) {
        if(subordinate === undefined) {
            return '';
        }

        if(subordinate.type === 1) {
            return constants.VIEW.MY_VIEW_DISPLAY_TEXT;
        } else if(subordinate.type === 2) {
            return constants.VIEW.MY_ORG_VIEW_DISPLAY_TEXT;
        } else if (subordinate.type === 3 && subordinate.userId === -3) {
            return constants.VIEW.MY_TEAM_VIEW_DISPLAY_TEXT;
        } else {
            return constants.VIEW.getUserViewDisplayText(subordinate.firstName, subordinate.lastName);
        }
    };
    var controllerFn = function ($scope, $timeout) {
        var self = this;
        self.icons = {
            1: 'account',
            2: 'city',
            3: 'account-group'
        };

        if (self.hideDropDownTitle === undefined)
            self.hideDropDownTitle = false;

        if (!self.btnIcon)
            self.btnIcon = "menu";

        function DropdownCtrl($scope, mdPanelRef, $window){
            var my = this;

            // three themes when change the View in home page
            my.theme = {
                1: 'theme-bg-primary',
                2: 'theme-bg-secondary',
                3: 'theme-bg-grey'
            }
            my.selectedView = self.currentView;

            if (my.selectedView.type === 3) {
                my.selectedSubordinates = angular.copy(self.selectedSubordinates);
            } else {
                my.selectedSubordinates = [];
            }
            my._mdPanelRef = mdPanelRef;
            my.close = function(){
                var panelRef = my._mdPanelRef;
                panelRef && panelRef.close().then(function() {
                    panelRef.destroy();
                });
                self.currentView = angular.copy(my.selectedView);
            };
            my.displaySubordinateInMenu = displaySubordinateInMenu;
            my.selectUserView = function(userView) {
                my.close();
            }
            my.toggle = function (item, list) {
                my.selectedView = my.app.defaultUserViews[2];
                var idx = my.indexOfItem(item, list);
                if (idx > -1) {
                    list.splice(idx, 1);
                }
                else {
                    list.push(item);
                }
            };
            
            my.indexOfItem = function(item, list) {
                var index = -1;
                for (var i = 0; i < list.length; i++) {
                    if (list[i].email == item.email) {
                        index = i;
                        break;
                    }
                }
                return index;
            };

            my.exists = function (item, list) {
                return my.indexOfItem(item, list) > -1;
            };

            my.isChecked = function() {
                return my.selectedSubordinates.length === my.app.subordinates.length;
            };

            my.toggleAll = function() {
                my.selectedView = my.app.defaultUserViews[2];

                if (my.selectedSubordinates.length === my.app.subordinates.length) {
                    my.selectedSubordinates = [];
                } else if (my.selectedSubordinates.length === 0 || my.selectedSubordinates.length > 0) {
                    my.selectedSubordinates = my.app.subordinates.slice(0);
                }
            };
            
            my.changeViewMode = function(selectedView) {
                if (selectedView.type !== 3) {
                    my.selectedView = selectedView;
                    my.selectedSubordinates = [];
                    my.applyChange();
                }
                    
            };

            my.applyChange = function() {
                var data = {
                    selectedView: my.selectedView,
                    subordinates: my.selectedSubordinates
                }
                my.applyFn({data: data});
                self.selectedSubordinates = my.selectedSubordinates;
                my.close();
            };
            //Update position on window resize
            function onWinResize() {
                var position = $mdPanel.newPanelPosition()
                    .relativeTo('#' + this.btnId)
                    .addPanelPosition($mdPanel.xPosition.ALIGN_END, $mdPanel.yPosition.BELOW);
                my._mdPanelRef.updatePosition(position);
            }

            //Clean up watcher
            $scope.$on('$destroy', function () {
                angular.element($window)
                    .off('resize',onWinResize);
            });

        }
        this.app = $model.get('app');
        var menuConfig = {
            attachTo: angular.element(document.body),
            controller: DropdownCtrl,
            controllerAs: 'dropdownCtrl',
            template: dropdownTemplate,
            panelClass: 'demo-menu-example',
            clickOutsideToClose: true,
            escapeToClose: true,
            focusOnOpen: false,
            zIndex: 60
        };
        this.showMenu = function (ev) {
            var position = $mdPanel.newPanelPosition()
                .relativeTo('#' + this.btnId)
                .addPanelPosition($mdPanel.xPosition.ALIGN_START, $mdPanel.yPosition.BELOW);

            menuConfig.openFrom = ev;
            menuConfig.position = position;

            menuConfig.locals = {
                app: $model.get('app'), 
                typeNotShown: this.typeNotShown, 
                applyFn: this.applyFn,
                icons: this.icons
            };

            $mdPanel.open(menuConfig);

        };
        this.displaySubordinateInMenu = displaySubordinateInMenu;
        this.selectedSubordinates = [];
    }

    return {
        restrict: 'E',
        template: template,
        scope: false,
        bindToController: {
            applyFn: '&',
            typeNotShown: '=',
            currentView: '=',
            btnId: "@",
            hideDropDownTitle: '=?',
            btnIcon: '@',
        },
        controller: controllerFn,
        controllerAs: 'ctrl'
    }
}]);
