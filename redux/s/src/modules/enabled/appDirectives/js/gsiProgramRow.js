;(function(angular, app){

    /**
     * Controller of each program row in gsiProgramTable. gsiProgramRow must exist be the parent element.
     * @class GsiProgramRowCtrl
     * */
    app.controller('GsiProgramRowCtrl',GsiProgramRowCtrl);
    GsiProgramRowCtrl.$inject = [
        '$scope', '$element', '$attrs', '$filter', '$timeout', '$log','$mdPanel', '$model', '$appEvent',
        'gsiProgramConsentDialog', 'gsiProgramDeleteDialog', 'gsiProgramUtils'
        ];
    function GsiProgramRowCtrl(
        $scope, $element, $attrs, $filter, $timeout, $log, $mdPanel, $model, $appEvent, gsiProgramConsentDialog,
        gsiProgramDeleteDialog, gsiProgramUtils
    ) {

        /** Initialize **/

        this.$filter = $filter;
        this.$scope = $scope;
        this.$model = $model;
        this.$appEvent = $appEvent;
        this.$element = $element;
        this.$timeout = $timeout;
        this.$mdPanel = $mdPanel;
        this.$log = $log;
        this.gsiProgramConsentDialog = gsiProgramConsentDialog;
        this.gsiProgramDeleteDialog = gsiProgramDeleteDialog;
        this.utils = gsiProgramUtils;


        // Parent controller sets is-new="true" if a row is created by user.
        // This'd be false if a row is created by data loaded from server or after the save.


        // Stores metadata. Make sure to delete when copying to request payload
        this.data._metadata = this.data._metadata || {};

        this.isNew = $attrs.isNew === 'true' ||  this.data._metadata &&  this.data._metadata._isNew;

        //todo clean up
        this.data._metadata._isNew = this.isNew;
        this.data._metadata.created = new Date();

        // Flag to track if a program is in terminated status. Some fields are not editable when terminated.
        this.isProgramTerminated = false;

        this.initializeFieldData(this.data);


        // true if users don't have edit access to a program.
        // Such access is judged by rest service from patient and user match.
        this.readOnly = !this.isNew && this.data.program.enabled === false;

        // Backup data to let users revert their edits
        this.backup = angular.copy(this.data);


        var self = this;

        // Listeners
        self.$scope.$on('patientProgram:save:success', angular.bind(self, self.onSaveSuccessListener));

        self.$scope.$on('patientProgram:consent:reverted', function(ev,id){
           self.$log.debug('Received "patientProgram:consent:reverted"', id);
           if(self.data && self.utils.isConsentManagedByParent(self.data.program) && self.data.program.parentProgram.id === id){
               self.$log.debug('consent is reverted by other row', id);
               self.getRowForm().consent.$setPristine();
               self.checkFormPristine();
           }
        });


        self.$scope.$on('patientProgram:consent:changed', function(ev,id){
            self.$log.debug('Received "patientProgram:consent:changed"', id);
            if(self.data && self.utils.isConsentManagedByParent(self.data.program) && self.data.program.parentProgram.id === id){
                self.$log.debug('consent is changed by other row', id);
                self.getRowForm().consent.$setDirty();
            }
        });


        // Initialize menu
        self.menus = [
            {
                text: self.utils.getText('editConsentMenu'),
                icon: self.utils.getIcon('consentMenuItem'),
                action: angular.bind(self,self.consentMenuAction),
                ngIf: angular.bind(self,self.requiresConsent),
                cssClass: angular.bind(self,self.consentMenuClass),
                cssStyle: angular.bind(self, self.consentMenuMdColor)

            },
            {
                text: self.utils.getText('revertMenu'),
                icon: self.utils.getIcon('revertMenuItem'),
                action: angular.bind(self,self.revertMenuAction),
                disabled: angular.bind(self,self.revertMenuDisabled),
                ngIf: angular.bind(self,self.showRevertMenu)
            },
            {
                text: self.utils.getText('deleteMenu'),
                icon: self.utils.getIcon('deleteMenuItem'),
                action: angular.bind(self,self.deleteMenuAction)
            }
        ];

        this.$log.debug('initialized row...')

    }

    GsiProgramRowCtrl.prototype.checkFormPristine = function(){
        var allPristine = true;
        var rowForm = this.getRowForm();
        for(var k in rowForm){
            if (!rowForm.hasOwnProperty(k) || k.charAt(0) === '$' ) continue;
            if(rowForm[k].$dirty){
                allPristine = false;
                break;
            }

        }
        if(allPristine){
            rowForm.$setPristine();
            this.tableCtrl.checkParentPristine();
        }
    };

    /**
     * Listener called on successful save.
     */
    GsiProgramRowCtrl.prototype.onSaveSuccessListener = function () {
        var self = this;
        // todo
        // // If saved program was new and `complex` type, model needs some tweaks
        // if(self.isNew === true &&self._rawParentProgram && self._rawParentProgram.type === 'complex_parent'){
        //     self.data.program.type ='complex_subordinate';
        //     // self.data.program.parentProgram = self._rawParentProgram;
        // }
        // Ensure isNew flag is false
        self.isNew = false;
        self.data._metadata._isNew = false;

        // Update program termination flag.
        self.isProgramTerminated = self.data.status && self.data.status.endReasonRequired;

        //Set form to $pristine
        this.getRowForm().$setPristine();
    };


    /**
     * Populate models from original data.
     * @param original -- patient program data sent from server.
     */
    GsiProgramRowCtrl.prototype.initializeFieldData = function(original) {
        //FIXME should be simpler
        var self = this;

        //populate data from copy
        var data = angular.copy(original);

        self.programEffectiveGetSetFn(self.utils.parseDate(data.programEffective));
        self.statusEffectiveGetSetFn(self.utils.parseDate(data.statusEffective));
        // programEnd needs to be in $timeout since it's minDate depends on `programEffective` date
        self.$timeout(function () {
            self.programEndGetSetFn(self.utils.parseDate(data.programEnd));
        },0);

        self.data.status = data.status;
        self.isProgramTerminated = data.status && data.status.endReasonRequired;
        self.data.endReason = data.endReason;
        data.selectedPrograms ? self.data.selectedPrograms = data.selectedPrograms : delete self.data.selectedPrograms;

    };

    /**
     * @return {FormController} -- returns form controller of current row registered to the parent controller
     */
    GsiProgramRowCtrl.prototype.getRowForm = function () {
        if(this._rowFormRef) return this._rowFormRef;

        if(!this.tableCtrl || !this.tableCtrl.form) return null;
        else {
            this._rowFormRef = this.tableCtrl.form[this.formName];
            return this._rowFormRef;
        }
    };


    /*
     * Context menu functions
     */

    /**
     * @return {boolean} if true revert menu is shown
     */
    GsiProgramRowCtrl.prototype.showRevertMenu = function () {
        return !this.isNew;
    };

    /**
     * Called on revert menu click
     * @param data
     * @param $close
     */
    GsiProgramRowCtrl.prototype.revertMenuAction = function(data, $close) {
        GWT.activity();
        var backup = angular.copy(this.tableCtrl.getBackupByProgramId(this.data.program.id));
        this.initializeFieldData(backup);

        var consentInfo;
        if(this.utils.isConsentManagedByParent(this.data.program)){
            var parent = this.tableCtrl.getBackupByProgramId(this.data.program.parentProgram.id)
            consentInfo = parent.consentInfo;
        }else{
            consentInfo = backup.consentInfo;
        }
        angular.copy(consentInfo, this.consentInfo);

        this.getRowForm().$setPristine();
        if(this.utils.isConsentManagedByParent(this.data.program)) this.tableCtrl.announceConsentRevert(this.data.program.parentProgram.id);
        this.tableCtrl.checkParentPristine();
        $close();
    };

    /**
     * @return {*|boolean} if true revert menu is disabled
     */
    GsiProgramRowCtrl.prototype.revertMenuDisabled = function () {
        return this.tableCtrl&&this.tableCtrl.form&&this.getRowForm() && this.getRowForm().$pristine;
    };


    /**
     * Called on delete menu click
     * @param data
     * @param $close
     */
    GsiProgramRowCtrl.prototype.deleteMenuAction = function(data, $close) {
        GWT.activity();
        if(this.isNew){
            this.tableCtrl.deleteRow(this.data, this.formName);
        }else{
            this.openProgramDeletePanel();
        }
        $close();
    };

    /**
     * Called on consent menu click
     * @param data
     * @param $close
     */
    GsiProgramRowCtrl.prototype.consentMenuAction = function(data, $close){
        GWT.activity();
        this.openConsentPanel();
        $close && $close();
    };


    /**
     * Opens consent editor panel
     */
    GsiProgramRowCtrl.prototype.openConsentPanel = function(){
        GWT.activity();
        var config = {};
        config.position =  this.$mdPanel.newPanelPosition()
            .absolute()
            .center();
        config.locals = {
            data:this.data,
            consentInfo: this.consentInfo,
            dob: this.tableCtrl.dob,
            isMinor : this.tableCtrl.isMinor,
            today:this.tableCtrl.today,
            patientId: this.tableCtrl.patientDemog.patientId
        };
        config.attachTo = this.tableCtrl.attachPopupToElement;

        //Open dialog
        this.gsiProgramConsentDialog.open(config).then(onSuccess.bind(this));

        function onSuccess(data) {
            GWT.activity();
            var self = this;

            self.consentInfo.consentObtainedBy = data.consentObtainedBy;
            self.consentInfo.minorConsentObtainedBy = data.minorConsentObtainedBy;
            self.consentInfo.consenter = data.consenter;
            self.consentInfo.consent = data.consent;
            self.consentInfo.consentDate = data.consentDate? self.utils.formatDate(data.consentDate): null;
            self.consentInfo.minorConsentDate = data.minorConsentDate? self.utils.formatDate(data.minorConsentDate): null;

            self.getRowForm().consent.$setDirty();

            if(self.utils.isConsentManagedByParent(self.data.program)){self.tableCtrl.announceConsentChanged(self.data.program.parentProgram.id);}
        }
    };


    /**
     * Opens program delete panel
     */
    GsiProgramRowCtrl.prototype.openProgramDeletePanel = function(){
        GWT.activity();
        var config = {};
        config.position =  this.$mdPanel.newPanelPosition()
            .absolute()
            .center();
        config.locals = {
            isMinor : this.isMinor,
            patientId : this.tableCtrl.patientDemog.patientId
        };
        config.attachTo = this.tableCtrl.attachPopupToElement;
        this.gsiProgramDeleteDialog.open(config).then(onSuccess.bind(this));

        function onSuccess(data) {
            var self = this;
            GWT.activity();
            if(self.isNew){
                self.$log.warn('Something went wrong. You cannot let users request delete for newly added programs.');
                return;
            }
            self.tableCtrl.deleteProgramEpisode(self.data.program, data.deleteReason);
        }
    };

    GsiProgramRowCtrl.prototype.consentMenuClass = function(){
       return this.consentInvalid() ? 'consent-invalid': '';
    };

    GsiProgramRowCtrl.prototype.consentMenuMdColor = function(){
        return this.isChanged('consentGroup') ? this.tableCtrl.modifiedFieldStyle : {};
    };

    /**
     *
     * @return {boolean} true if user entered consent is invalid
     */
    GsiProgramRowCtrl.prototype.consentInvalid = function () {
        var rowForm = this.getRowForm();
        return rowForm && rowForm.consent && rowForm.consent.$invalid;
        // if(!this.data.program) return false;
        // if (this.utils.isConsentRequired(this.data.program) && (
        //         !this.data.consent || !this.data.consent.id ||
        //         !this.data.consentObtainedBy || !this.data.consentObtainedBy.userId ||
        //         !this.data.consentDate
        //     )){
        //     return true;
        // }
        // return this.utils.isConsenterRequired(this.data.program, this.consentInfo) && (!this.data.consenter || !this.data.consenter.code);
    };

    /**
     * @return {boolean} true if consent is required
     */
    GsiProgramRowCtrl.prototype.requiresConsent = function () {
        return this.utils.isConsentRequired(this.data.program);
    };


    /**
     * Function that returns name of parent program.
     * Used for read only purpose (after save, when isNew is false)
     * @return {string}
     */
    GsiProgramRowCtrl.prototype.getParentProgramName = function () {
        var self = this;
        if(self.data.program.type==='complex_subordinate'){
            return self.data.program.parentProgram.name;
        }else if(self.data.program.type==='single'){
            return '---';
        }else{
            return self.data.program.name;
        }
    };

    /**
     * Function that returns name of a program.
     * Used for read only purpose (after save, when isNew is false)
     * @return {string}
     */
    GsiProgramRowCtrl.prototype.getProgramName = function () {

        if(this.data.program && (this.data.program.type==='complex_subordinate' || this.data.program.type==='single')){
            return this.data.program.name
        }else return null;
    };

    /**
     *
     * @return {boolean} if current selected program is simple parent
     */
    GsiProgramRowCtrl.prototype.isSimpleProgramSelected = function () {
        var self = this;
        return self.isNew ?
            self._rawParentProgram && self._rawParentProgram.type==='simple_parent' :
            self.data.program && self.data.program.type==='simple_parent';
    };

    /**
     * Clean up fields
     */
    GsiProgramRowCtrl.prototype.clearFields= function () {
        var self = this;

        self.data.status = null;
        self.data.endReason = null;
        self.programEffectiveGetSetFn(null);
        self.programEndGetSetFn(null);
        self.statusEffectiveGetSetFn(null);
        self.consentInfo = null;
        self.onStatusChange();
    };

    /**
     * Getter-Setter function for parent program.
     * Must return the value of the model. If argument exists, update the value.
     * @param parentProgram
     * @return {*}
     */
    GsiProgramRowCtrl.prototype.parentProgramGetSetFn = function (parentProgram) {
        var self = this;

        // No argument found. Return existing value
        if(arguments.length===0) return self._rawParentProgram;

        // Value found. Empty all values
        self._rawSubProgram = null;
        self.data.program = null;
        // Clean up other metadata fields (dates, status, etc)
        self.clearFields();

        self.tableCtrl.moveToFirstLevel(self.data);

        self._rawParentProgram = parentProgram;

        // Single program selected.
        if(self._rawParentProgram==='None'){
            // Update subprogram options for child program to single program list
            self._subProgramOptions = self.tableCtrl.singleList;
            // subordinate program is mandatory
            self.programRequired=true;
            //Cleanup selected programs
            delete self.data.selectedPrograms;
            self.tableCtrl.onProgramSelect();
        }
        else if(self._rawParentProgram.type==='simple_parent'){
            // set this parent to data
            self.data.program = self._rawParentProgram;
            // subordinate program is not mandatory
            self.programRequired=false;
            // Update subprogram options to subordinates of this program
            self._subProgramOptions = self._rawParentProgram.subPrograms;
            // Reinitialize selected subordinate programs
            self.data.selectedPrograms=[];
            self.tableCtrl.onProgramSelect();
            // If consent is required, show consent editor panel
            if(self.requiresConsent()){
                self.consentInfo = self.consentInfo || self.tableCtrl.getConsentInfo(self.data);
                //Couldnt use consentValid as validator runs late
                if(!self.utils.isConsentValid(self.data.program, self.consentInfo)) self.openConsentPanel()
            }
        }else if(self._rawParentProgram.type==='complex_parent'){
            // Update subprogram options to subordinates of this program
            self._subProgramOptions = self._rawParentProgram.programs;
            // subordinate program is mandatory
            self.programRequired=true;
            // Delete selected programs
            delete self.data.selectedPrograms;
            self.tableCtrl.onProgramSelect();
        }else {
            self.$log.error("Unknown parent program type", self._rawParentProgram);
        }

        return self._rawParentProgram;
    };

    /**
     * Getter-Setter function for subordinate program.
     * Must return the value of the model. If argument exists, update the value.
     * @param program
     * @return {*}
     */
    GsiProgramRowCtrl.prototype.programGetSetFn = function (program) {

        var self = this;
        // No argument found. Return existing value
        if(!angular.isDefined(program)) return self._rawSubProgram;

        self._rawSubProgram = program;
        if(self._rawParentProgram === 'None' || self._rawParentProgram.type === 'complex_parent'){
            // Set this program to data
            self.data.program = self._rawSubProgram;
            // Clean up other metadata fields (dates, status, etc)
            self.clearFields();
            // If consent is required, show consent editor panel
            if(self.requiresConsent()) {
                self.consentInfo = self.consentInfo || self.tableCtrl.getConsentInfo(self.data);
                if(!self.utils.isConsentValid(self.data.program, self.consentInfo)) self.openConsentPanel()
            }
        }else{
            self.$log.error("Unknown subordinate program type", self._rawSubProgram);
        }

        if(self._rawParentProgram.type === 'complex_parent'){
            self.tableCtrl.moveIntoParent(self.data, self._rawParentProgram.id);
        }

        self.tableCtrl.onProgramSelect();
        return self._rawSubProgram;
    };

    /**
     * Finds out change of passed fields; If it's touched or not
     * Calls some required action on some field change
     * @param type -- name of the field
     */
    GsiProgramRowCtrl.prototype.onStatusChange = function (){
        var self = this,
            touched;

        if(!self.data.status && !self.backup.status) touched = false;
        else touched = !self.data.status || !self.backup.status || self.data.status.id!==self.backup.status.id;

        if(touched === true){       // If status is changed
            //update most recent status date
            self._mostRecentStatusDate = self.backup ? self.utils.parseDate(self.backup.statusEffective): null;

            if(self.data.status && !self.data.status.endReasonRequired){
                self.isProgramTerminated = false
            }
            //Update status effective date to current date
            self.statusEffectiveGetSetFn(new Date());
            self.getRowForm().statusEffective.$setDirty();

        }else if(self.backup){      // If status is unchanged or same as before and backup exists
            //Revert most recent status date
            self._mostRecentStatusDate =  self.utils.parseDate(self.backup.mostRecentStatusDate);
            if(self.data.status && self.data.status.endReasonRequired){
                self.isProgramTerminated = true;
                self.statusEffectiveGetSetFn(self.utils.parseDate(self.backup.statusEffective));
                self.getRowForm().statusEffective.$setPristine();
            }else if(!self.tableCtrl.statusEffectiveDateEditable){
                self.statusEffectiveGetSetFn(self.utils.parseDate(self.backup.statusEffective));
            }
        }else{                      // If status is unchanged and program is new
            self.statusEffectiveGetSetFn(new Date());
            self.getRowForm().statusEffective.$setDirty();
        }
    };

    /**
     *
     * @param type
     * @return {boolean} -- true if the field is $dirty
     */
    GsiProgramRowCtrl.prototype.isChanged = function (type) {

        if(!this.tableCtrl) return false;

        // `subPrograms` is subordinate programs of simple parent.
        // form controller of subPrograms are stored with name `subPrograms_${index}`
        if(type==='subPrograms'){
            var index = 0;
            // If any is $dirty return true
            while(!!this.getRowForm()[type+'_'+index]){
                if (this.getRowForm()[type+'_'+index].$dirty){
                    return true;
                }
                index++;
            }
            return false;
        }else if(type==='consentGroup'){ // if any of consent data is $dirty return true
            return getConsentHasChange(this.getRowForm());
        }
        else return this.getRowForm()[type].$dirty;

        /**
         *
         * @param form -- formController of current row
         * @return {boolean} -- true if any consent data has change
         */
        function getConsentHasChange(form) {

            if(!form) return false;

            // Name of consent form controls
            var consentFields = [
                'consentObtainedBy',
                'consentDate',
                'consenter',
                'minorConsentObtainedBy',
                'minorConsentDate'
            ];

            for(var i=0; i<consentFields.length; i++){
                var control = form[consentFields[i]];
                if(control && control.$dirty) return true;
            }

            return false;
        }

    };

    /**
     * Register parent table control to this row controller.
     * @param tableCtrl
     */
    GsiProgramRowCtrl.prototype.registerParent = function(tableCtrl){

        var self = this;

        self.$log.debug('registering parent table...');

        self.tableCtrl = tableCtrl;

        // Register self row controller to parent controller
        self.tableCtrl.registerRow(self);

        // If self program is new and single program list exists, set default of parent program to "None".
        if(self.isNew&&self.tableCtrl.singleList&&self.tableCtrl.singleList.length>0) self.parentProgramGetSetFn('None');

        // Give current form an unique id
        self.formName = getRandomFormId(self.tableCtrl);
        self.data._metadata.formName = self.formName;

        if(self.data.program){
            if(self.data.program.type==='complex_subordinate'){
                this.tableCtrl.moveIntoParent(self.data, self.data.program.parentProgram.id);
            }
            this.consentInfo = this.consentInfo || this.tableCtrl.getConsentInfo(this.data);
        }

        /**
         * Generatets random form id.
         * Required to register this form controller to parent controller without collision
         * */
        function getRandomFormId (tableCtrl) {
            var formName="rowForm" + randomId();
            while(tableCtrl.form.hasOwnProperty(formName)){
                formName="rowForm" + randomId();
            }
            return formName;

            function randomId(){
                return '_' + Math.random().toString(36).substr(2, 9);
            }
        }

    };


    /**
     * Getter-setter function for program effective date.
     * @param input
     * @return {*}
     */
    GsiProgramRowCtrl.prototype.programEffectiveGetSetFn = function(input){
        var self = this;
        if(angular.isDefined(input)){
            self._rawProgramEffective = input;
            self.data.programEffective = self.utils.formatDate(self._rawProgramEffective);
            self.$timeout(function () {
                if(self._rawProgramEnd<self._rawProgramEffective)
                    self.programEndGetSetFn(null);
            },0);
        }
        return self._rawProgramEffective;
    };

    /**
     * Returns raw programEffective model.
     * @return {*}
     */
    GsiProgramRowCtrl.prototype.getRawProgramEffective=function () {
        return this._rawProgramEffective
    };

    /**
     * Getter-setter function for program end date.
     * @param input
     * @return {*}
     */
    GsiProgramRowCtrl.prototype.programEndGetSetFn = function(input){
        if(angular.isDefined(input)){
            this._rawProgramEnd = input;
            this.data.programEnd =  this.utils.formatDate(this._rawProgramEnd);
        }
        return this._rawProgramEnd;
    };

    /**
     * Getter-setter function for status effective date.
     * @param input
     * @return {*}
     */
    GsiProgramRowCtrl.prototype.statusEffectiveGetSetFn = function(input){
        if(arguments.length!==0){
            this._rawStatusEffective = input;
            this.data.statusEffective = this.utils.formatDate(this._rawStatusEffective);
        }
        return this._rawStatusEffective;
    };

    /**
     * @return {Date} minimum date of status effective date.
     * It is earlier date of patient's DoB and most recent status effective date.
     */
    GsiProgramRowCtrl.prototype.statusEffectiveMinDate = function () {
        if(!this._mostRecentStatusDate) return this.tableCtrl.dob;
        var dob = this.tableCtrl.dob;
        return this._mostRecentStatusDate > dob ? this._mostRecentStatusDate : dob;
    };

    /**
     * Util function to return index of a program in programs
     * @param program
     * @return {number}
     */
    function getSubProgramIndex(programs, program){
        if(!programs) return -1;
        for(var i=0;i<programs.length;i++){
            if(program.id===programs[i].id) return i;
        }
        return -1;
    }

    /**
     *
     * @param p
     * @return {boolean} true if simple subordinate program is selected
     */
    GsiProgramRowCtrl.prototype.isSubProgramSelected = function (p) {
        return getSubProgramIndex(this.data.selectedPrograms, p)!==-1
    };

    /**
     * Toggle active/inactive of simple subordinate program
     * @param p
     */
    GsiProgramRowCtrl.prototype.toggleSubProgram = function (p) {
        GWT.activity();
        var self = this;
        // If terminated, do nothing;
        if(self.isProgramTerminated) return;
        if(!self.data.selectedPrograms) self.data.selectedPrograms = [];
        var index = getSubProgramIndex(self.data.selectedPrograms, p);
        (index===-1) ? self.data.selectedPrograms.push(p) : self.data.selectedPrograms.splice(index, 1);
    };

    /**
     *
     * @return {*} simple subordinate program options
     */
    GsiProgramRowCtrl.prototype.getSubProgramOptions = function () {
        return this._subProgramOptions;
    };

    /**
     *
     * @param opt -- parent program
     * @return {boolean} true if parent program option is not selectable
     */
    GsiProgramRowCtrl.prototype.isParentProgramOptionDisabled = function (opt) {
        if(!opt) return false;
        if(opt.enabled===false) return true;
        if(opt.selected!==true) return false;
        var parentProgram = this.parentProgramGetSetFn();
        return !parentProgram || (parentProgram.id !== opt.id);
    };

    /**
     *
     * @param opt -- program
     * @return {boolean} true if program option is not selectable
     */
    GsiProgramRowCtrl.prototype.isProgramOptionDisabled = function (opt) {
        if(!opt) return false;
        if(opt.enabled === false) return true;
        if(opt.selected!==true) return false;
        var program = this.programGetSetFn();
        return !program || (program.id !== opt.id);
    };


    // template

    var iconCellWrapper = '\
    class="icon-cell gsi-editable-cell-wrapper"\
    flex="nogrow"\
    layout="column"\
    layout-align="center center"\
    ';

    function getCellWrapper(type) {
        return '<div \
        class="gsi-editable-cell-wrapper column-info"\
        flex="{{ctrl.tableCtrl.columnConfig[\''+type+'\'].flex}}"\
        ng-class="ctrl.tableCtrl.columnConfig[\''+type+'\'].cssClass" \
        layout="column" \
        layout-align="center {{ctrl.tableCtrl.columnConfig[\''+type+'\'].center? \'center\' : \'start\'}}"\
        flex-order="{{ctrl.tableCtrl.columnConfig[\''+type+'\'].order}}"\
        ng-if="ctrl.tableCtrl.columnConfig[\''+type+'\'].show"\
        >'
    }


    var SIMPLE_PROGRAM_TOGGLE_BUTTON = '\
        <div \
        flex-order="-20"\
        '+iconCellWrapper+'\
        >\
        <md-button \
        class="md-icon-button sub-program-button" \
        ng-click="ctrl.isSimpleSubProgramsOpen=!ctrl.isSimpleSubProgramsOpen"\
        ng-if="ctrl.isSimpleProgramSelected()">\
            <md-icon \
            ng-style="ctrl.isChanged(\'subPrograms\')&& ctrl.tableCtrl.modifiedFieldStyle"\
            md-svg-icon="{{ctrl.utils.getIcon(ctrl.isSimpleSubProgramsOpen?\'subProgramCollapse\':\'subProgramOpen\')}}"\
            /> \
            <md-tooltip>{{ctrl.isSimpleSubProgramsOpen?ctrl.utils.getText(\'subRowCollapseTooltip\'):ctrl.utils.getText(\'subRowOpenTooltip\')}}</md-tooltip>\
        </md-button>\
        </div>';

    var PARENT_PROGRAM_CELL =
        getCellWrapper('parentProgramName') +'\
            <md-select \
            name="parentProgramName" \
            aria-label="{{::ctrl.tableCtrl.columnConfig[\'parentProgramName\']}}"\
            ng-style="ctrl.tableCtrl.modifiedFieldStyle" ng-if="ctrl.isNew" \
            ng-model-options="{trackBy:\'$value.id\', getterSetter:true}" \
            ng-model="ctrl.parentProgramGetSetFn" \
            >\
                <md-option ng-if="ctrl.tableCtrl.singleList.length>0">None</md-option>\
                    <md-option \
                    ng-disabled="ctrl.isParentProgramOptionDisabled(opt)"\
                    ng-repeat="opt in ctrl.tableCtrl.availableList" \
                    ng-value="opt" \
                    >\
                        {{opt.name}}\
                    </md-option>\
            </md-select>\
            <span ng-if="!ctrl.isNew" >\
                {{ctrl.getParentProgramName()}}\
            </span>\
        </div>';

    var SUB_PROGRAM_CELL =
        getCellWrapper('programName') +'\
            <md-select \
            name="programName" \
            aria-label="{{::ctrl.tableCtrl.columnConfig[\'programName\']}}"\
            ng-style="ctrl.tableCtrl.modifiedFieldStyle" \
            ng-if="ctrl.isNew && ctrl.parentProgramGetSetFn().type !==\'simple_parent\'" \
            ng-model-options="{trackBy:\'$value.id\', getterSetter:true}" \
            ng-model="ctrl.programGetSetFn" \
            ng-required="ctrl.programRequired"\
            >\
                <md-option \
                ng-disabled="ctrl.isProgramOptionDisabled(opt)"\
                ng-repeat="opt in ctrl.getSubProgramOptions()" \
                ng-value="opt" \
                >\
                    {{opt.name}}\
                </md-option>\
            </md-select>\
            <span ng-if="!ctrl.isNew" >{{ctrl.getProgramName()}}</span>\
        </div>';

    var CONSENT_STATUS_CELL =
        getCellWrapper('consentStatus') +'\
            <div \
            ng-click="ctrl.openConsentPanel()" \
            class="md-button md-icon-button" \
            style="cursor:pointer;"\
            ng-if="ctrl.requiresConsent()" \
            ng-class="{\'consent-invalid\': ctrl.consentInvalid()}"\
            aria-label="Edit Consent">\
                <md-tooltip ng-if="ctrl.consentInfo.consent.value">\
                    {{::ctrl.utils.getText(\'consentTooltipPrefix\')}}{{ctrl.consentInfo.consent.value}}\
                </md-tooltip>\
                <md-icon md-svg-icon="{{ctrl.tableCtrl.getConsentIcon(ctrl.consentInfo.consent)}}" \
                    ng-style="ctrl.tableCtrl.getConsentColor(ctrl.consentInfo.consent)"></md-icon>\
            </div>\
            <div class="md-icon-button" ng-if="!ctrl.requiresConsent()" aria-label="Consent not required">\
                <md-icon \
                    md-svg-icon="{{::ctrl.tableCtrl.consentNotRequiredIcon}}" \
                    ng-style="::ctrl.tableCtrl.consentNotRequiredStyle" />\
                <md-tooltip>\
                    {{::ctrl.utils.getText(\'consentTooltipPrefix\')}}{{::ctrl.utils.getText(\'consentNotRequired\')}}\
                </md-tooltip>\
            </div>\
        </div>';

    var PROGRAM_EFFECTIVE_CELL =
        getCellWrapper('programEffective') +'\
            <md-datepicker \
            aria-label="{{::ctrl.tableCtrl.columnConfig[\'programEffective\']}}"\
            datepicker-validation-fix \
            name="programEffective"\
            ng-model="ctrl.programEffectiveGetSetFn" \
            ng-style="ctrl.isChanged(\'programEffective\')&& ctrl.tableCtrl.modifiedFieldStyle" \
            ng-disabled="ctrl.readOnly || ctrl.isProgramTerminated"\
            md-hide-icons="calendar"\
            md-min-date="ctrl.tableCtrl.dob" \
            ng-if="ctrl.data.program.dateRequired" \
            ng-model-options="{getterSetter:true}" \
            required \
            />\
        </div>';


    var PROGRAM_END_CELL =
        getCellWrapper('programEnd') +'\
            <md-datepicker \
            aria-label="{{::ctrl.tableCtrl.columnConfig[\'programEnd\']}}"\
            datepicker-validation-fix \
            name="programEnd" \
            ng-style="ctrl.isChanged(\'programEnd\')&& ctrl.tableCtrl.modifiedFieldStyle" \
            ng-disabled="ctrl.readOnly || ctrl.isProgramTerminated"\
            md-hide-icons="calendar" \
            md-min-date="ctrl.getRawProgramEffective()" \
            ng-if="ctrl.data.program.dateRequired" \
            ng-model-options="{getterSetter:true}" \
            ng-model="ctrl.programEndGetSetFn" \
            ng-required="ctrl.data.status&&ctrl.data.status.endReasonRequired===true" \
            /> \
        </div>';

    var STATUS_CELL =
        getCellWrapper('status') +'\
            <md-select \
            name="status" \
            aria-label="{{::ctrl.tableCtrl.columnConfig[\'status\']}}"\
            ng-style="ctrl.isChanged(\'status\')&& ctrl.tableCtrl.modifiedFieldStyle" \
            ng-disabled="ctrl.readOnly"\
            ng-change="ctrl.onStatusChange()"\
            ng-if="ctrl.data.program" \
            ng-model-options="{trackBy: \'$value.id\'}" \
            ng-model="ctrl.data.status" \
            required \
            >\
                <md-option \
                ng-repeat="opt in ctrl.data.program.statuses" \
                ng-value="opt"\
                >\
                    {{opt.name}}\
                </md-option>\
            </md-select>\
        </div>' ;

    var STATUS_EFFECTIVE_CELL =
        getCellWrapper('statusEffective') +'\
            <md-datepicker \
            name="statusEffective" \
            aria-label="{{::ctrl.tableCtrl.columnConfig[\'statusEffective\']}}"\
            datepicker-validation-fix \
            ng-style="ctrl.isChanged(\'statusEffective\')&& ctrl.tableCtrl.modifiedFieldStyle" \
            ng-disabled="ctrl.readOnly || ctrl.isProgramTerminated"\
            md-hide-icons="calendar"\
            md-max-date="ctrl.tableCtrl.today" \
            md-min-date="ctrl.statusEffectiveMinDate()" \
            ng-if="ctrl.tableCtrl.statusEffectiveDateEditable && ctrl.data.program"\
            ng-model-options="{getterSetter:true}" \
            ng-model="ctrl.statusEffectiveGetSetFn" \
            ng-if=""\
            required \
            />\
            <span ng-if="!ctrl.tableCtrl.statusEffectiveDateEditable && ctrl.data.program">\
            {{ctrl.statusEffectiveGetSetFn() | date:\'MM/dd/yyyy\'}}\
            </span>\
        </div>';

    var END_REASON_CELL =
        getCellWrapper('endReason') +'\
            <md-select \
            name="endReason" \
            aria-label="{{::ctrl.tableCtrl.columnConfig[\'endReason\']}}"\
            ng-style="ctrl.isChanged(\'endReason\')&& ctrl.tableCtrl.modifiedFieldStyle" \
            ng-if="ctrl.data.status.endReasonRequired" \
            ng-model-options="{trackBy: \'$value.id\'}"\
            ng-model="ctrl.data.endReason" \
            required \
            >\
                <md-option \
                ng-repeat="opt in ctrl.data.program.endReasons" \
                ng-value="opt"\
                >\
                    {{opt.value}}\
                </md-option>\
            </md-select>\
        </div>' ;

    // var HIDDEN_FIELDS_CELL ='\
    //     <div style="display:none">\
    //         <input type="hidden" name="consentObtainedBy" ng-if="ctrl.data.program.consentRequired" \
    //             required ng-model="ctrl.data.consentObtainedBy.userId"/>\
    //         <input type="hidden" name="consent" ng-if="ctrl.data.program.consentRequired" \
    //             required ng-model="ctrl.data.consent.id"/>\
    //         <input type="hidden" ng-if="ctrl.data.program.consentRequired" name="consentDate" \
    //             required ng-model="ctrl.data.consentDate"/>\
    //         <input type="hidden" ng-if="ctrl.data.program.consenterRequired" name="consenter" \
    //             ng-required="ctrl.utils.isConsenterRequired(ctrl.data)" ng-model="ctrl.data.consenter.code"/>\
    //         <input type="hidden" name="minorConsentObtainedBy" ng-if="ctrl.data.program.minorConsentRequired" \
    //             ng-model="ctrl.data.minorConsentObtainedBy.userId"/>\
    //         <input type="hidden" ng-if="ctrl.data.program.minorConsentRequired" name="minorConsentDate" \
    //             ng-model="ctrl.data.minorConsentDate"/>\
    //     </div>';

    var HIDDEN_FIELDS_CELL ='\
        <div style="display:none">\
            <input type="hidden" name="consent" ng-if="ctrl.requiresConsent()" \
                gsi-program-consent-validator program="ctrl.data.program" ng-model="ctrl.consentInfo"/>\
        </div>';


    var CONTEXT_MENU_CELL =
        '<div flex-order="20" '+iconCellWrapper+'\
        ><gsi-context-menu \
        ng-hide="ctrl.readOnly"\
        menus="ctrl.menus" \
        class="program-context-menu"\
        opts="{panelClass:\'program-context-menu-panel\'}"\
        btn-style="ctrl.isChanged(\'consentGroup\')&& ctrl.tableCtrl.modifiedFieldStyle"\
        btn-icon="{{ctrl.utils.getIcon(\'contextMenu\')}}"\
        /></div>';

    var MAIN_ROW =

        '<div class="main-row" layout="row" layout-wrap flex="100" ng-style="ctrl.isNew&&ctrl.tableCtrl.newFieldStyle">' +

        SIMPLE_PROGRAM_TOGGLE_BUTTON +

        PARENT_PROGRAM_CELL +

        SUB_PROGRAM_CELL +

        CONSENT_STATUS_CELL +

        PROGRAM_EFFECTIVE_CELL +

        PROGRAM_END_CELL +

        STATUS_CELL +

        STATUS_EFFECTIVE_CELL +

        END_REASON_CELL +

        HIDDEN_FIELDS_CELL +

        CONTEXT_MENU_CELL +

        '</div>';

    var SIMPLE_SUB_PROGRAM_CONTAINER_ROW ='\
        <div \
        class="sub-program-selection-row" \
        ng-if="ctrl.isSimpleProgramSelected()" \
        ng-show="ctrl.isSimpleSubProgramsOpen"\
        ng-class="{\'disabled\':ctrl.readOnly || ctrl.isProgramTerminated}"\
        >\
            <label \
            class="sub-program-chip" \
            ng-style="!ctrl.isProgramTerminated && ctrl.isSubProgramSelected(p) && ctrl.tableCtrl.selectedSubBgColor" \
            ng-repeat="p in ctrl.data.program.subPrograms" \
            >\
            <input type="checkbox"\
            ng-disabled="ctrl.readOnly || ctrl.isProgramTerminated"\
            name="subPrograms_{{$index}}"\
            checklist-model="ctrl.data.selectedPrograms"\
            checklist-value="p">\
                {{p.name}} \
                <md-tooltip>\
                    {{(!ctrl.isProgramTerminated && ctrl.isSubProgramSelected(p)) ? \
                    ctrl.utils.getText(\'subRowActiveTooltip\') : \
                    ctrl.utils.getText(\'subRowInactiveTooltip\')}} \
                </md-tooltip>\
            </label>\
        </div>';


    var GSI_EDITABLE_PROGRAM_ROW_TMPL =
        '<form name="{{ctrl.formName}}" layout="column" flex="100" ng-class="{\'read-only\': ctrl.readOnly}">' +

        MAIN_ROW+
        SIMPLE_SUB_PROGRAM_CONTAINER_ROW +

        '</form>';


    app
        .directive('gsiProgramRow',['$filter','$timeout',function ($filter, $timeout) {

            return {
                restrict: 'E',
                scope: {
                    data: '='
                },
                link: function (scope,elem,attrs,ctrls){
                    var rowCtrl = ctrls[0];
                    var tableCtrl = ctrls[1];
                    rowCtrl.registerParent(tableCtrl);
                    elem.on('$destroy', function () {
                        tableCtrl.unregisterRow(rowCtrl);
                        $timeout(function () {
                            tableCtrl.checkParentPristine();
                            tableCtrl.loadAvailableProgramList();
                        },0);
                    })
                },
                controller: 'GsiProgramRowCtrl',
                controllerAs: 'ctrl',
                bindToController: true,
                require: ['gsiProgramRow', '^^gsiProgramTable'],
                replace: true,
                template: GSI_EDITABLE_PROGRAM_ROW_TMPL
            }
        }]);


})(angular, app);

