app.directive('sizeWatch', function () {
    function link(scope, element, attrs) {
        scope.$watch(function () {
            scope[attrs.sizeWatch] = {
                width: element[0].firstChild.childNodes[1].offsetWidth + 'px'
            };
        });
    }
    return {
        restrict: 'AE',
        link: link
    };
}); 