app.directive('parseWidget', function ($compile, $timeout) {
  var holderPattern = /#\{(.[^{]+)\}/ig;
  return {
    restrict: 'E',
    scope: {
      config: '='
    },
    replace: true,
    link: function (scope, ele, attrs) {
      scope.$watch(getTemplate, function (html) {
        ele.html(html);
        $compile(ele.contents())(scope);
      });
      if(scope.config.parameter) {   
        Object.keys(scope.config.parameter).forEach(function(key) {
          if (scope.config.parameter[key].match(holderPattern)) {
            var value = scope.config.parameter[key];
            scope.$parent.$watch(value.substring(2, value.length - 1), function (data) {
              scope[key] = data;
              if (key==='hidewidget') {
                scope.config.hidewidget = data;
              }
            }, false);
          }
        });
      }

      function getTemplate() {
        var parameter = ' name="' + scope.config.name + '"';
        for (var key in scope.config.parameter) {
          if (scope.config.parameter[key].match(holderPattern)) {
            parameter += ' '+ key + '="' + key + '" ';
          } else {
            parameter += ' '+ key + '="' + scope.config.parameter[key] + '" ';
          }
        }
        return '<' + scope.config.type + parameter + '>' + '</' + scope.config.type + '>';
      }
    }
  };
});