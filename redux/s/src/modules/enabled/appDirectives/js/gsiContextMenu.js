app
    .directive('gsiContextMenu', ['$mdPanel', function ($mdPanel) {
        return{
            scope: {
                menus: '=',
                data: '=',
                opts: '=?',
                btnStyle:'=?',
                btnIcon:'@?'
            },
            replace:true,
            template:'<button class="md-icon-button md-button" layout="column" layout-align="center center" ' +
            'ng-click="openMenu($event)">' +
                '<md-icon ng-style="btnStyle" md-svg-icon="{{btnIcon}}"/>' +
            '</button>',
            link:function (scope) {
                scope.btnIcon = scope.btnIcon || 'dots-vertical';
                scope.opts = scope.opts || {};
                var menuConfig = {
                    attachTo: angular.element(document.body),
                    controller:['mdPanelRef',function (mdPanelRef) {
                        var self = this;
                        self._mdPanelRef = mdPanelRef;
                        self.close = function(){
                            var panelRef = self._mdPanelRef;
                            panelRef && panelRef.close().then(function() {
                                panelRef.destroy();
                            });
                        };


                        self.showMenu = function () {
                            if(!self.menus) return false;
                            for(var i = 0; i< self.menus.length; i++){
                                if(!self.menus[i].ngIf) return true;
                                if(!angular.isFunction(self.menus[i].ngIf)&&self.menus[i].ngIf) return true;
                                if(angular.isFunction(self.menus[i].ngIf)&&self.menus[i].ngIf()) return true;
                            }
                            return false;
                        };
                    }],
                    controllerAs: 'menuCtrl',
                    template: '<div class="md-whiteframe-z2 gsi-context-menu md-body-1" md-colors="{background: \'default-background\'}">' +
                        '<md-menu-item ng-repeat="menu in menuCtrl.menus" ng-if="!menu.ngIf || menu.ngIf()" ng-class="menu.cssClass()" ng-style="menu.cssStyle()">' +
                            '<md-button ng-click="menu.action(menuCtrl.data, menuCtrl.close)" ng-disabled="menu.disabled(menuCtrl.data)">' +
                            '   <md-icon ng-if="menu.icon" md-svg-icon="{{menu.icon}}"  ng-style="menu.cssStyle()"></md-icon>' +
                            '   <span>{{menu.text}}</span></md-button>' +
                            '</md-menu-item>'+
                        '<md-menu-item ng-if="menuCtrl.showMenu()!==true" disabled>' +
                    '       <md-button disabled><span>No Menu</span></md-button>' +
                        '</md-menu-item>'+
                        '</div>',
                    clickOutsideToClose: true,
                    escapeToClose: true,
                    focusOnOpen: false
                };

                scope.openMenu = function () {
                    var position = $mdPanel.newPanelPosition()
                        .relativeTo(event.target)
                        .addPanelPosition($mdPanel.xPosition.ALIGN_START, $mdPanel.yPosition.BELOW);

                    menuConfig.openFrom = event;
                    menuConfig.position = position;
                    menuConfig.locals = {
                        data:scope.data,
                        menus:scope.menus
                    };
                    menuConfig.panelClass = scope.opts.panelClass;

                    $mdPanel.open(menuConfig);
                }

            }
        }
    }]);