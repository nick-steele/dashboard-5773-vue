(function(angular, app){

    /**
     * Validator directive for RHIO.
     * If sharing is true and no RHIO family or endpoint is selected, sets validity to false.
     */
    app.directive('rhioValidator', function(){
       return {
           require: 'ngModel',
           scope:{ ngModel: '=' },
           link: function(scope, elm, attrs, ctrl){

               function isValid(rhio){
                   if (!rhio.sharing) return true;
                   if(!rhio.endpoints && !rhio.rhioFamilies) return true;

                   for (var i = 0; i < rhio.rhioFamilies.length; i++) {
                       if(rhio.rhioFamilies[i].selected) return true;
                   }

                   for (var j = 0; j < rhio.endpoints.length; j++) {
                       if (rhio.endpoints[j].enabled) return true;
                   }
                   return false;
               }

               scope.$watch('ngModel', function(n){
                   ctrl.$setValidity('sharing', isValid(n));
               }, true);

           }
       }
    });

})(angular, app);