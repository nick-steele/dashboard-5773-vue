app.directive('autofocus', ['$timeout', function($timeout) {
  return {
    restrict: 'A',
    link : function($scope, $element) {
      $timeout(function() {
        $element[0].focus();
        
        //trick to focus on the end of input on IE
        var val =  $element[0].value;
        $element[0].value = '';
        $element[0].value = val; 
      }, 0);
    }
  }
}]);