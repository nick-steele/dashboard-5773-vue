/**
 * This is a patch directive for nvd3. It calls resize event with 500 ms delay when dimension of the directive changes.
 * Apply this to wrapper div block
 */
app.directive('nvd3ResizePatch',function($window,$timeout){
    return function(scope,elem){
        var nvd3ResizePatchWatcher = scope.$watch(function(){
            return [elem[0].offsetWidth, elem[0].offsetHeight].join('x');
        },function(){
            //500 ms delay is the key
            $timeout(function(){
                $window.dispatchEvent(new Event('resize'));
            },500);
        });

        //Clean up
        scope.$on('destroy', function () {
            nvd3ResizePatchWatcher();
        })

    }
});