

app.directive('gsiSaveMdButton', function(){
    return{
        restrict:'E',
        template:'<md-button ng-class="[buttonClass,vm.ngClass]" ng-click="gsiSaveClick()" md-colors="vm.mdColor" ng-disabled="ngDisabled || status === vm.INPROGRESS">{{vm.text}}</md-button>',
        scope:{
            'status' : '=',
            'ngDisabled': '=',
            'buttonClass':'@',
            'gsiSaveClick':'&'
        },
        controller: 'GsiSaveMdButtonCtrl',
        controllerAs: 'vm'
    }
});

app.controller('GsiSaveMdButtonCtrl',GsiSaveMdButtonCtrl);

GsiSaveMdButtonCtrl.$inject = ['$scope','$gsiSaveButton', '$element', '$timeout', '$animate'];
function GsiSaveMdButtonCtrl($scope, $gsiSaveButton, $element, $timeout, $animate){
    var self = this;

    this.INPROGRESS = $gsiSaveButton.IN_PROGRESS;

    var opt = {
        basic:{
            text:'Save',
            style:{}
        },
        success:{
            text: 'Saved!',
            cls: 'gsi-save-button-saved',
            msec: 1500,
            style:{'background':'default-primary-600'}
        },
        running:{
            text: 'Saving...',
            cls:null,
            style:{},
        },
        error:{
            text: 'Error',
            cls: 'gsi-save-button-error',
            msec: 3000,
            style:{'background':'default-warn-600'}
        }
    };


    //TODO refacor
    var custom = this.options || {};
    opt = angular.extend(opt,custom);

    this.text = opt.basic.text;
    this.mdColor = opt.basic.style;

    var el = $element;

    $scope.$watch('status', function(n,o){
        switch(n){
            case $gsiSaveButton.IN_PROGRESS:

                self.mdColor = opt.running.style;
                self.text = opt.running.text;
                self.inProgress=true;
                $animate.addClass(el, opt.running.cls);

                break;
            case $gsiSaveButton.SUCCESS:

                self.inProgress=false;
                self.text = opt.success.text;
                self.mdColor = opt.success.style;
                $animate.removeClass(el, opt.running.cls);
                $animate.removeClass(el, opt.error.cls);
                $animate.addClass(el, opt.success.cls)
                    .then(function () {
                        $timeout(function () {
                            self.mdColor = opt.basic.style;
                            el.removeClass(opt.success.cls);
                            self.text = opt.basic.text;
                        }, opt.success.msec);
                    });
                break;
            case $gsiSaveButton.ERROR:

                self.inProgress=false;
                self.text = opt.error.text;
                self.mdColor = opt.error.style;

                $animate.removeClass(el, opt.running.cls);
                $animate.removeClass(el, opt.success.cls);

                $animate.addClass(el, opt.error.cls)
                    .then(function () {
                        $timeout(function () {
                            self.mdColor = opt.basic.style;
                            self.text = opt.basic.text;
                            el.removeClass(opt.error.cls);
                        }, opt.error.msec);
                    });
                break;

        }
    });
}


//@deprecated
app.directive('gsiSaveButton', function($animate, $timeout, $gsiSaveButton){
    return {
        restrict:'A',
        scope: {
            'gsiSaveButton' : '=',
            'gsiSaveButtonOptions' : '@?',
            'btnDisable':'='
            // ,
            // 'gsiAnimateOnRunning' : '@?',
            // 'gsiAnimateOnDone' : '@?',
            // 'gsiAnimateOnMsec' : '@?',
        },
        compile: function(el,attr){
            var orig = el.html();

            return {
                pre: function(scope,el,attr){

                    var custom = scope.gsiSaveButtonOptions || {};
                    // var text = scope.gsiAnimateOnDone.text || 'Saved!';
                    // var msec = scope.gsiAnimateOnMsec || 1000;


                    var opt = {
                        success:{
                            text: 'Saved!',
                            cls: 'md-primary',
                            msec: 1500
                        },
                        running:{
                            text: 'Saving...',
                            cls:null
                        },
                        error:{
                            text: 'Error',
                            cls: 'md-warn',
                            msec: 2000
                        }
                    }

                    opt = angular.extend(opt,custom);


                    scope.$watch('gsiSaveButton', function(n,o){
                        switch(n){
                            case $gsiSaveButton.IN_PROGRESS:
                                el.html(opt.running.text);
                                $animate.addClass(el, opt.running.cls);
                                break;
                            case $gsiSaveButton.SUCCESS:
                                $animate.removeClass(el, opt.running.cls);
                                $animate.removeClass(el, opt.error.cls);
                                el.html(opt.success.text);
                                $animate.addClass(el, opt.success.cls)
                                    .then(function () {
                                        $timeout(function () {
                                            el.removeClass(opt.success.cls);
                                            el.html(orig);
                                            scope.btnDisable=true;

                                        }, opt.success.msec);
                                    });
                                break;
                            case $gsiSaveButton.ERROR:
                                $animate.removeClass(el, opt.running.cls);
                                $animate.removeClass(el, opt.success.cls);
                                el.html(opt.error.text);
                                $animate.addClass(el, opt.error.cls)
                                    .then(function () {
                                        $timeout(function () {
                                            el.removeClass(opt.error.cls);
                                            el.html(orig);
                                        }, opt.error.msec);
                                    });
                                break;
                        }
                    });
                }
            }

        }
    };
})

app.service('$gsiSaveButton', function () {
    this.IN_PROGRESS = 0;
    this.SUCCESS = 1;
    this.ERROR = 2;
});