// directive that add class to md-chip element from md-chip-template

app.directive('customChip', function() {
    return {
        restrict: 'EA',
        link: function(scope, elem, attrs) {
            scope.$watch(function() {
                return elem.attr('class');
            }, function(newValue) {
                addClass(newValue);
            });

            function addClass(value) {
                var chipTemplateClass = value;
                var mdChip = elem.parent().parent(); // md-chip element
                mdChip.addClass(chipTemplateClass); // add class to md-chip element
            }
        }
    }
});