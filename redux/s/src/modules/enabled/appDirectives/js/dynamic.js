// Angluar directive to recompile the contents of dynamic areas when they get refreshed...
app.directive('dynamic-disabled', function ($compile) {
    console.log('dynamic loader checked in');
  return {
    restrict: 'A',
    replace: true,
    link: function (scope, ele, attrs) {
      scope.$watch(attrs.dynamic, function(html) {
        ele.html(html);
        $compile(ele.contents())(scope);
		console.log('Recompiled.');
      });
    }
  };
});