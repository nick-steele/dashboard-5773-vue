(function(angular, app, moment){

    // Added for debugging user session timer so has least functionality.
    // Should be enhanced if it needs to be used in other purpose
    app.directive('simpleTimer', function ($interval, $log) {
        return{
            scope: {start:'='},
            link: function(scope, element, attrs) {
                if(!moment) {
                    $log.warn('moment is not loaded.');
                    return;
                }

                var stopTime; // so that we can cancel the time updates

                // used to update the UI
                function updateTime() {
                    try{
                        var d = moment.duration((moment().unix() - moment(scope.start).unix()) * 1000, 'milliseconds');
                        var text = (d.hours()?d.hours()+'h':'')+d.minutes()+'m'+d.seconds()+'s';
                        element.text(text);
                    }catch(e){element.text('');}//TODO
                }

                // watch the expression, and update the UI on change.
                scope.$watch('start', function () {
                    updateTime();
                });

                stopTime = $interval(updateTime, 1000);

                // listen on DOM destroy (removal) event, and cancel the next UI update
                // to prevent updating time after the DOM element was removed.
                element.on('$destroy', function () {
                    $interval.cancel(stopTime);
                });
            }
        }

    })

})(angular, app, moment);