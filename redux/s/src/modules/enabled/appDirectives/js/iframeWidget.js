app.directive('iframeWidget', ['$model','$appEvent', '$q', '$sce', '$http', function($model, $appEvent, $q, $sce, $http) {

    var template = '<ms-widget layout="row" flex="100" class="nopadding-top">' +
                       '<ms-widget-front class="white-bg">' +
//                           '<div id="{{ctrl.name}}-container" ng-style="ctrl.iframeStyle" class="iframe-container {{ctrl.iframeClass}}">' +
                               '<iframe src="{{ctrl.trustSrc(ctrl.url)}}" frameborder="0" width="100%" style="height: 78vh;"></iframe>' +
//                           '</div>' +
                       '</ms-widget-front>' +
                   '</ms-widget>';

    var controllerFn = function($scope, $timeout) {
        var self = this;
        this.iframeStyle = this.height? {'height': this.height} : {};
        this.iframeClass = !this.height? 'iframe-container-' + this.ratio.replace(':', '-') : '';

        this.trustSrc = function(src) {
            return $sce.trustAsResourceUrl(src);
        }

    }

    return {
        restrict: 'E',
        scope: true,
        template: template,
        controller: controllerFn,
        controllerAs: 'ctrl',
        bindToController: {
            name: '@',
            ratio: '@',
            url: '@', 
            height: '@'
        }
    }
}]);
