app.directive('alertEncounterWidget', ['$model', '$appEvent', '$q', '$mdDialog', 'constants', function ($model, $appEvent, $q, $mdDialog, constants) {

    var template = '<ms-widget layout="row" flex="100" class="nopadding-top">' +
        '<ms-widget-front class="white-bg">' +
        '<div class="ph-16 pv-8 border-bottom" layout="row" layout-align="space-between center" layout-wrap>' +
        '<div class="pv-8" layout="row" layout-align="start center" layout-align-sm="end" flex-sm="100">' +
        '<md-select id="drpRangePicker" class="simplified font-size-16" ng-model="ctrl.currentfilter.currentchartrange" ' +
        'aria-label="Change date" ng-change="ctrl.requestChartData()">' +
        '<md-option ng-repeat="(key, range) in ctrl.chartRanges" ng-value="key">{{::range}}</md-option>' +
        '</md-select>' +
        '</div>' +
        '</div>' +
        '<div layout="row" layout-wrap>' +
        '<div class="p-16 error-wrapper" layout="row" flex="100" flex-gt-sm="35" flexible-div="ctrl.drawAlertChart()">' +
        '<nvd3 id="alertChart" config="{disabled: true}" options="ctrl.alertChartOpts" data="ctrl.alertchartdata.chartData" api="ctrl.alertChartApi"></nvd3>' +
        '<error ng-if="ctrl.isalerterror === true" ' +
        'msg="\'Error Occurred: We encountered a problem Alert chart. Please try again later.\'" ' +
        'reload="ctrl.loadAlertData()">' +
        '</error>' +
        '</div>' +
        '<div class="p-16 error-wrapper" layout="row" flex="100" flex-gt-sm="65" style="position: relative" flexible-div="ctrl.drawCarePlanChart()">' +
        '<nvd3 id="careplanChart" config="ctrl.config" options="ctrl.carePlanChartOpts" data="ctrl.encounters.data" api="ctrl.carePlanChartApi"></nvd3>' +
        '<span id="lblNoEncounter">' +
        '<span class="badge-count" ng-click="ctrl.noDataLabelClick()">{{ctrl.patientnoencounter.patients.size()}}</span>' +
        'Patient(s) with No Encounters' +
        '</span>' +
        '<error ng-if="ctrl.iscareplanerror === true || ctrl.isnoencountererror === true" ' +
        'msg="\'Error Occurred: We encountered a problem Care Plan chart. Please try again later.\' " ' +
        'reload="ctrl.loadCarePlanData()">' +
        '</error>' +
        '</div>' +
        '</div>' +
        '</ms-widget-front>' +
        '</ms-widget>';

    var controllerFn = function ($scope, $timeout) {

        var self = this;
        this.config = {};
        this.chartRanges = {
            7: "Last 7 Days",
            14: "Last 14 Days",
            30: "Last 30 Days"
        };

        this.alertChartOpts = {
            chart: {
                type: 'pieChart',
                height: 420,
                donut: true,
                x: function (d) { return d.label; },
                y: function (d) { return d.value; },
                showLabels: true,
                labelType: 'value',
                valueFormat: function (d) {
                    return d3.format(',.0f')(d);
                },
                pie: {
                    startAngle: function (d) { return d.startAngle / 2 - Math.PI / 2 },
                    endAngle: function (d) { return d.endAngle / 2 - Math.PI / 2 },
                    dispatch: {
                        elementClick: function (e) {
                            var currentChartRange = self.chartRanges[self.currentfilter.currentchartrange];
                            var title = e.data.label + ' Alerts - ' + self.capitalize(self.getDisplayName($model.get('home').currentMember)) + ' - ' + currentChartRange;
                            self.handleChartClick({popupType: 'ALERT'}, title, e.data, 'Alert');
                        }
                    }
                },
                margin: {},
                duration: 0,
                showLegend: true,
                legend: {
                    margin: {
                        top: 12
                    }
                },
                title: 'Severity',
                titleOffset: -10
            },
            title: {
                enable: true,
                text: 'Patients by Alerts Severity'
            },
            noData: {
                enable: true,
                text: 'No Alerts for the patients in this timeframe'
            }
        };

        this.alertchartdata = {
            chartData: [],
            alertList: {}
        };

        $scope.$watch(angular.bind(this, function () {
            return self.alertchartdata;
        }), function (newValue, oldValue) {
            if (typeof newValue != 'undefined') {
                self.alertChartOpts.chart.margin.bottom = newValue.chartData.length === 0 ? 0 : -150;
                self.alertChartApi.refreshWithTimeout(0);
            }
        });

        if(constants.isIE) {
            this.config = {
                disabled: true
            };
            $scope.$watch(angular.bind(this, function () {
                return self.encounters;
            }), function (newValue, oldValue) {
                if (typeof newValue !== 'undefined') {
                    self.carePlanChartApi.refreshWithTimeout(0);
                }
            });
        };
        
        this.encounters = {
            data: [],
            xLabel: 'Core Service Types',
            yLabel: 'Patients'
        };

        this.carePlanChartOpts = {
            chart: {
                type: 'discreteBarChart',
                color: ['#03A9F4'],
                height: 500,
                margin: {
                    top: 50,
                    bottom: 100,
                    left: 100
                },
                noData: 'No Care Plan Encounters for the patients in this timeframe',
                x: function (d) { return d.label; },
                y: function (d) { return d.value; },
                showValues: true,
                valueFormat: function (d) {
                    return d3.format('d')(d);
                },
                duration: 0,
                xAxis: {
                    axisLabel: this.encounters.xLabel,
                    axisLabelDistance: -10,
                    rotateLabels: -23,
                    fontSize: 11
                },
                yAxis: {
                    axisLabel: this.encounters.yLabel,
                    axisLabelDistance: -10,
                    tickFormat: function (n) {
                        return d3.format('d')(n);
                    }
                },
                discretebar: {
                    dispatch: {
                        elementClick: function (e) {
                            var currentChartRange = self.chartRanges[self.currentfilter.currentchartrange];
                            var title = 'Care Plan Encounter: ' + e.data.label + ' - ' + self.getDisplayName($model.get('home').currentMember) + ' - ' + currentChartRange;
                            self.handleChartClick({popupType: 'ENCOUNTER'}, self.capitalize(title), e.data, 'Encounter');
                        }
                    }
                }
            },
            title: {
                enable: true,
                text: 'Care Plan Encounters'
            }
        };

        this.patientnoencounter = {};

        //click on No Data label
        this.noDataLabelClick = function() {
            var title = self.capitalize('Patients with No Encounter' + ' - ' +
                        self.getDisplayName($model.get('home').currentMember) + ' - ' + self.chartRanges[self.currentfilter.currentchartrange]);
            self.handleChartClick({
                popupType: 'NO_ENCOUNTER',
                noToolTip: true
            }, title, self.patientnoencounter, 'No Encounter');
        };

        this.getDisplayName = function (subordinate) {
            if(subordinate.type === constants.VIEW_TYPE.MY_VIEW) {
                return constants.VIEW.MY_VIEW_DISPLAY_TEXT;
            }
            else if(subordinate.type === constants.VIEW_TYPE.ORG_VIEW) {
                return constants.VIEW.MY_ORG_VIEW_DISPLAY_TEXT;
            }
            else {
                var selectedSubordinates = $model.get('home').selectedSubordinates;
                if (selectedSubordinates.size() === 1)
                    return constants.VIEW.getUserViewDisplayText(selectedSubordinates[0].firstName, selectedSubordinates[0].lastName);
                else
                    return constants.VIEW.MY_TEAM_VIEW_DISPLAY_TEXT;
            }
        };

        this.capitalize = function(str, lower) {
            return (lower ? str.toLowerCase() : str).replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
        };

        this.handleChartClick = function (popupInfo, title, data, patientSearchPopupTitle) {
            GWT.activity();
            var homeCtrl = $model.get('home');
            homeCtrl.popupTitle = title;

            homeCtrl.alertPopupChartData = angular.copy(self.alertchartdata);
            homeCtrl.alertPopupChartData.label = data.label;
            homeCtrl.encountersPopupData = angular.copy(self.encounters);
            homeCtrl.encountersPopupData.label = data.label;

            homeCtrl.already_added = self.getMessageBundle(homeCtrl.default_already_added, "popup.tooltip.already_added", function (configMsg) {
                return configMsg;
            });

            if (data.patients.size() !== 0) {
                homeCtrl.alertPatientList = [];
                $model.get('app').showDialog('appViewPatients', popupInfo, null, function () {
                });
                if (data.patients.size() <= 500) {
                    homeCtrl.patientCount = -1;
                    $appEvent('remote:appHome:loadPatientList', { patients: data.patients });
                } else {
                    $timeout(function () {
                        homeCtrl.patientCount = data.patients.size();
                        homeCtrl.showSearchPatientDialog(patientSearchPopupTitle, data.patients, popupInfo.popupType);
                    });
                }
            } else {
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title('Message Box')
                        .textContent('There is no patient data')
                        .ariaLabel('There is no patient data')
                        .ok('OK')
                );
            }
        };

        this.getMessageBundle = function (defaultMsg, msgKey, replaceFunc) {
            if (self.messageBundle && (self.messageBundle[msgKey])) {
                return replaceFunc(self.messageBundle[msgKey]);
            } else {
                return defaultMsg;
            }
        };

        this.drawAlertChart = function () {
            this.alertChartApi.update();
        };

        this.drawCarePlanChart = function () {
            this.carePlanChartApi.update();
        };

        this.changeOrg = function (data) {
            this.currentfilter.currentpatienttoggle = data;
            this.requestChartData();
        };
        this.changeRange = function (data) {
            this.currentfilter.currentchartrange = data;
            this.requestChartData();
        };
        this.requestChartData = function () {
            this.loadAlertData();
            this.loadCarePlanData();
        };

        this.loadAlertData = function () {
            $appEvent('remote:appHome:loadAlertCount', { type: this.currentfilter.currentpatienttoggle, interval: this.currentfilter.currentchartrange, userList: this.subordinates });
        }

        this.loadCarePlanData = function () {
            $appEvent('remote:appHome:loadEncounterCount', { type: this.currentfilter.currentpatienttoggle, interval: this.currentfilter.currentchartrange, userList: this.subordinates });
            $appEvent('remote:appHome:loadNoEncounterCount', { type: this.currentfilter.currentpatienttoggle, interval: this.currentfilter.currentchartrange, userList: this.subordinates });
        }
        this.api.refresh = function (data) {
            self.subordinates = data.subordinates;
            self.changeOrg(data.member.type);
        };

        this.requestChartData();

    }

    return {
        restrict: 'E',
        template: template,
        scope: false,
        bindToController: {
            alertchartdata: '=',
            encounters: '=',
            patientnoencounter: '=',
            isalerterror: '=',
            iscareplanerror: '=',
            isnoencountererror: '=',
            subordinates: '=',
            currentfilter: '=',
            name: '@',
            api: '='
        },
        controller: controllerFn,
        controllerAs: 'ctrl'
    }
}]);
