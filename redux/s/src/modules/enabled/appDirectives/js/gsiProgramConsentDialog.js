
(function(angular, app){

    /**
     * A controller for {@link gsiProgramConsentDialog} service.
     */
    app.controller('GsiProgramConsentDialogCtrl',GsiProgramConsentDialogCtrl);
    GsiProgramConsentDialogCtrl.$inject = [
        '$scope','$mdDialog', '$model', '$appEvent', '$q', '$log', '$timeout', '$filter', 'gsiProgramUtils'];
    function GsiProgramConsentDialogCtrl($scope, $mdDialog, $model, $appEvent, $q, $log, $timeout, $filter, gsiProgramUtils) {

        this.$model = $model;
        this.$appEvent = $appEvent;
        this.app = this.$model.get('app');
        this.$mdDialog = $mdDialog;
        this.$scope = $scope;
        this.$q = $q;
        this.$log = $log;
        this.$filter = $filter;
        this.$timeout = $timeout;
        this.utils = gsiProgramUtils;
        this.result = this.consentInfo && angular.copy(this.consentInfo) || {};

        this.$model.store('programConsentPanel', this.$scope);
        if(this.utils.isConsenterRequiredByProgram(this.data.program) && !this.app.consenterOptions) this.$appEvent('remote:patient:getConsenterOptions');
        this.patientEnrollment = this.$model.get('patientEnrollment');
        if(this.consentInfo.minorConsentObtainedBy && this.consentInfo.minorConsentDate) this.giveMinorConsent = true;

        this.currentUser = {
            firstName: this.app.login.firstName,
            lastName: this.app.login.lastName,
            userId: this.app.login.userId
        };

        this.consentDateGetSetFn(this.utils.parseDate(this.consentInfo.consentDate));
        this.minorConsentDateGetSetFn(this.utils.parseDate(this.consentInfo.minorConsentDate));

        this._consentSearchResult = {timestamp: null, deferred: null};
        this._minorConsentSearchResult = {timestamp: null, deferred: null};

    }


    GsiProgramConsentDialogCtrl.prototype.ok = function () {
        this.$mdDialog.hide(this.result);
    };

    GsiProgramConsentDialogCtrl.prototype.no = function () {
        this.$mdDialog.cancel(this.result);
    };


    /**
     * Getter-setter function for consent date
     * @param input
     * @return {*}
     */
    GsiProgramConsentDialogCtrl.prototype.consentDateGetSetFn = function (input) {
        if(arguments.length!==0){
            this._rawConsentDate = input;
            this.result.consentDate =  this.utils.formatDate(this._rawConsentDate);
        }
        return this._rawConsentDate;
    };

    /**
     * Getter-setter function for minor consent date
     * @param input
     * @return {*}
     */
    GsiProgramConsentDialogCtrl.prototype.minorConsentDateGetSetFn = function (input) {
        if(arguments.length!==0){
            this._rawMinorConsentDate = input;
            this.result.minorConsentDate =  this.utils.formatDate(this._rawMinorConsentDate);
        }
        return this._rawMinorConsentDate;
    };


    /**
     * Called on consent option change (Yes, No, etc...);
     */
    GsiProgramConsentDialogCtrl.prototype.onGiveConsentChange = function () {
        var self = this;
        if(self.result.consent){    // user selected consent
            // If date is empty populate current date
            if(!self.result.consentDate) self.consentDateGetSetFn(new Date());
            // If obtained by user is empty populate current user
            if(!self.result.consentObtainedBy || !self.result.consentObtainedBy.userId){
                self.result.consentObtainedBy = self.currentUser;
            }
        }else{                      // user cleared consent
            //Clear consent fields
            self.result.consentObtainedBy = null;
            self.consentDateGetSetFn(null);
            self._consentSearchResult = {timestamp: null, deferred: null};
            self.consentSearchKeyword = null;
            self.$timeout(function () {
                self.result.consenter = null;
            },0);
        }
    };

    /**
     * Called on minor consent switch change
     */
    GsiProgramConsentDialogCtrl.prototype.onGiveMinorConsentChange = function () {
        if(this.giveMinorConsent){
            if(!this.result.minorConsentDate) this.minorConsentDateGetSetFn(new Date());
            if(!this.result.minorConsentObtainedBy || !this.result.minorConsentObtainedBy.userId){
                this.result.minorConsentObtainedBy = this.currentUser;
            }
        }else{
            this.result.minorConsentObtainedBy = null;
            this.minorConsentDateGetSetFn(null);
            this._minorConsentSearchResult = {timestamp: null, deferred: null};
            this.minorConsentSearchKeyword = null;
        }
    };

    /**
     *
     * @param type -- consent or minorConsent.
     * @param keyword -- search keyword to look for users.
     * @return {Promise|null}
     */
    GsiProgramConsentDialogCtrl.prototype.consentSearchQuery = function (type, keyword) {
        if(!keyword || keyword.length ===0 ) return null;
        var forMinor = false;
        var timestamp = (new Date()).getTime();
        var record = {
            timestamp : timestamp,
            deferred: this.$q.defer()
        };
        if(type==='consent') {
            //Cancel existing request
            if(this._consentSearchResult.deferred) this._consentSearchResult.deferred.reject();
            this._consentSearchResult = record;

        }else if(type==='minorConsent'){
            //Cancel existing request
            if(this._minorConsentSearchResult.deferred) this._minorConsentSearchResult.deferred.reject();
            this._minorConsentSearchResult = record;
            forMinor = true;
        }

        //Send Request
        this.$appEvent(
            'remote:patient:searchConsentUsers',
            {patientId: this.patientId, keyword: keyword, forMinor: forMinor, timestamp: timestamp}
            );

        if(type==='consent') {
            return this._consentSearchResult.deferred.promise;

        }else if(type==='minorConsent'){
            return this._minorConsentSearchResult.deferred.promise;
        }
    };

    /**
     * called on receiving consentObtainedBy user search result
     * @param data
     */
    GsiProgramConsentDialogCtrl.prototype.onConsentUserSearchResult = function (data) {
        if(this._consentSearchResult.timestamp!==data.timestamp){
            this.$log.info("timestamp did not match", this._consentSearchResult.timestamp,  data.timestamp);
            return;
        }
        this._consentSearchResult.deferred.resolve(data.result);
    };

    /**
     * called on receiving minorConsentObtainedBy user search result
     * @param data
     */
    GsiProgramConsentDialogCtrl.prototype.onMinorConsentUserSearchResult = function (data) {
        if(this._minorConsentSearchResult.timestamp!==data.timestamp){
            this.$log.info("timestamp did not match");
            return;
        }
        this._minorConsentSearchResult.deferred.resolve(data.result);
    };

    /**
     * Filter function for consenter option
     */
    GsiProgramConsentDialogCtrl.prototype.showConsentOption = function (isMinor) {
        var _isMinor = isMinor;
        return function(opt){
            if(opt.type === 'all') return true;
            else if(_isMinor && opt.type ==='minor') return true;
            else if(!_isMinor && opt.type ==='adult') return true;
            return false;
        }
    };


    /**
     * A service to open program consent editor dialog. Currently called from <gsi-program-row>.
     */
    app.service('gsiProgramConsentDialog',function($mdDialog){

        var CONSENT_TMPL = '\
            <div ng-if="programConsentPanel.utils.isConsentRequired(programConsentPanel.data.program)" flex layout="column"> \
                <div layout="row" layout-align="center center">\
                    <h3 flex="66" ng-bind="::programConsentPanel.utils.getText(\'consentTitle\')"></h3>\
                    <md-select flex="33" \
                    name="consent" \
                    ng-model="programConsentPanel.result.consent" \
                    aria-label="{{::programConsentPanel.utils.getText(\'consentTitle\')}}"\
                    ng-model-options="{trackBy:\'$value.id\'}" \
                    ng-change="programConsentPanel.onGiveConsentChange()"\
                    >\
                        <md-option ng-value="undefined">{{::programConsentPanel.utils.getText(\'txtBlankConsent\')}}</md-option>\
                        <md-option \
                        ng-repeat="opt in programConsentPanel.app.consentList" \
                        ng-value="opt" >\
                            {{opt.value}}\
                    </md-option>\
                    </md-select> \
                </div>\
                <md-autocomplete \
                flex="none" \
                md-no-cache="false" \
                md-input-name="consentObtainedBy" \
                ng-disabled="!programConsentPanel.result.consent" \
                md-require-match = "true"\
                md-selected-item="programConsentPanel.result.consentObtainedBy" \
                md-item-text = "item.firstName+\' \' + item.lastName" \
                md-floating-label= "{{::programConsentPanel.utils.getText(\'consentObtainedByLabel\')}}" \
                md-delay="250" required\
                md-search-text="programConsentPanel.consentSearchKeyword" \
                md-items="item in programConsentPanel.consentSearchQuery(\'consent\', programConsentPanel.consentSearchKeyword)"> \
                     <md-item-template>\
                         <span md-highlight-text="programConsentPanel.consentSearchKeyword">\
                            {{item.firstName + " " + item.lastName}}\
                        </span>\
                    </md-item-template>\
                </md-autocomplete>\
                <md-input-container>\
                    <label><span ng-bind="::programConsentPanel.utils.getText(\'consentDateLabel\')"></span></label>\
                    <md-datepicker \
                    name="consentDate"\
                    flex="none"\
                    md-min-date="programConsentPanel.dob" \
                    md-max-date="programConsentPanel.today" \
                    ng-disabled="!programConsentPanel.result.consent" required \
                    md-hide-icons="calendar" \
                    ng-model-options="{getterSetter:true}"\
                    ng-model="programConsentPanel.consentDateGetSetFn">\
                    </md-datepicker> \
                </md-input-container>\
            </div> \
            <div ng-if="programConsentPanel.utils.isConsenterRequiredByProgram(programConsentPanel.data.program)" flex layout="column"> \
                <md-input-container>\
                    <label><span ng-bind="::programConsentPanel.utils.getText(\'consenterLabel\')"></span></label>\
                    <md-select \
                    flex="none"\
                    name="consenter"\
                    aria-label="{{::programConsentPanel.utils.getText(\'consenterLabel\')}}"\
                    ng-model-options="{trackBy:\'$value.code\'}" \
                    ng-required="programConsentPanel.utils.isConsenterRequired(programConsentPanel.data.program, programConsentPanel.result)"\
                    ng-disabled="!programConsentPanel.result.consent || !programConsentPanel.app.consenterOptions" \
                    ng-model="programConsentPanel.result.consenter" \
                    >\
                            <md-option \
                                md-option-empty \
                                ng-value="undefined" \
                                ng-if="!programConsentPanel.utils.isConsenterRequired(programConsentPanel.data.program, programConsentPanel.result)" \
                            />\
                            <md-option \
                            ng-repeat="opt in programConsentPanel.app.consenterOptions | filter: programConsentPanel.showConsentOption(programConsentPanel.isMinor)" \
                            ng-value="opt" >\
                                {{opt.codeDescription}}\
                        </md-option>\
                    </md-select>\
                </md-input-container>\
            </div>';

        var MINOR_CONSENT_TMPL = '\
                    <p ng-bind-html="::programConsentPanel.utils.getText(\'minorSharingIntro\')"></p> \
                    <div layout="row" layout-align="center center">\
                        <h3 flex="66" ng-bind="::programConsentPanel.utils.getText(\'minorSharingTitle\')"></h3>\
                        <md-switch flex="33" \
                        ng-model="programConsentPanel.giveMinorConsent" \
                        aria-label="{{::programConsentPanel.utils.getText(\'minorSharingTitle\')}}"\
                        ng-change="programConsentPanel.onGiveMinorConsentChange()"\
                        >\
                        </md-switch> \
                    </div>\
                    <md-autocomplete \
                    flex="none" \
                    md-no-cache="false" \
                    md-input-name="minorConsentObtainedBy" \
                    ng-disabled="!programConsentPanel.giveMinorConsent" \
                    md-require-match = "true"\
                    md-selected-item="programConsentPanel.result.minorConsentObtainedBy" \
                    md-item-text = "item.firstName+\' \' + item.lastName" \
                    md-floating-label= "{{::programConsentPanel.utils.getText(\'minorSharingObtainedByLabel\')}}" \
                    md-delay="250" \
                    ng-required="programConsentPanel.giveMinorConsent" \
                    md-search-text="programConsentPanel.minorConsentSearchKeyword" \
                    md-items="item in programConsentPanel.consentSearchQuery(\'minorConsent\', programConsentPanel.minorConsentSearchKeyword)"> \
                         <md-item-template>\
                             <span md-highlight-text="programConsentPanel.minorConsentSearchKeyword">\
                                {{item.firstName + " " + item.lastName}}\
                            </span>\
                    </md-item-template>\
                </md-autocomplete>\
                <md-input-container>\
                    <label><span ng-bind="::programConsentPanel.utils.getText(\'minorSharingDateLabel\')"></span></label> \
                    <md-datepicker \
                    flex="none" \
                    name="minorConsentDate"\
                    md-min-date="programConsentPanel.dob" \
                    md-hide-icons="calendar" \
                    ng-model-options="{getterSetter:true}"\
                    md-max-date="programConsentPanel.today" \
                    ng-disabled="!programConsentPanel.giveMinorConsent"\
                    ng-model="programConsentPanel.minorConsentDateGetSetFn"/> \
                </md-input-container>';

        var CONSENT_MENU_TMPL = '\
        <md-dialog id="programConsentPopup" class="program-consent-dialog" layout="column"> \
            <md-dialog-content flex="auto" layout-padding class="panel-content" > \
                <form name="programConsentPanel.form" novalidate>\
                <p ng-bind-html="::programConsentPanel.utils.getText(\'consentIntro\')"></p>'+
            CONSENT_TMPL  +'\
                <div \
                ng-if="programConsentPanel.utils.isMinorConsentRequired(programConsentPanel.data.program) && programConsentPanel.isMinor " \
                flex \
                style="margin-top: 24px;"\
                layout="column">' +
            MINOR_CONSENT_TMPL +
            '</div>\
            </form>\
        </md-dialog-content> \
        <md-dialog-actions> \
            <md-button class="md-accent" id="btnCancelConsent" ng-click="programConsentPanel.no()">\
                <span ng-bind="::programConsentPanel.utils.getText(\'consentCancelButton\')"></span>\
            </md-button> \
            <md-button class="md-primary" id="btnSaveConsent" ng-click="programConsentPanel.ok()" ng-disabled="programConsentPanel.form.$invalid">\
            <span ng-bind="::programConsentPanel.utils.getText(\'consentOkButton\')"></span>\
            </md-button> \
        </md-dialog-actions> \
    </md-dialog>';


        var consentMenuConfig = {
            template: CONSENT_MENU_TMPL,
            controller:'GsiProgramConsentDialogCtrl',
            controllerAs: 'programConsentPanel',
            bindToController:true,
            disableParentScroll: true,
            hasBackdrop: true,
            panelClass: 'program-consent-dialog',
            trapFocus: true,
            clickOutsideToClose: false,
            escapeToClose: false,
            focusOnOpen: true,
            multiple:true
        };

        function open(config) {
            var _config = angular.extend({}, consentMenuConfig, config);
            return $mdDialog.show(_config);
        }

        return {
            open : open
        }

    });


})(angular, app);
