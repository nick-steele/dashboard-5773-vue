/*Form validation helper to compare a confirmation password to a new password*/
app.directive('passwordMatch', [function () {
    return {
        restrict: 'A',
        scope: true,
        require: ['^ngModel', '^form'],
        link: function (scope, elem, attrs, ctrls) {
            var ngModel = ctrls[0]; //confirmPwd
            var formCtrl = ctrls[1];

            var otherPasswordModel = formCtrl[attrs.passwordMatch]; //newPwd

            ngModel.$parsers.push(function (value) {
                var valid = (value === otherPasswordModel.$viewValue);
                ngModel.$setValidity('mismatch', valid);
                ngModel.$render();
                return value;
            });
            otherPasswordModel.$parsers.push(function (value) {
                var valid = (value === ngModel.$viewValue);
                ngModel.$setValidity('mismatch', valid);
                ngModel.$render();
                return value;
            });

            //For model -> DOM validation
            ngModel.$formatters.unshift(function (value) {
                var valid = (value === otherPasswordModel.$viewValue);
                ngModel.$setValidity('mismatch', valid);
                return value;
            });
            otherPasswordModel.$formatters.unshift(function (value) {
                var valid = (value === ngModel.$viewValue);
                ngModel.$setValidity('mismatch', valid);
                return value;
            });


        }
    };
}])