app.directive('gsiTaskItem',["$model","$appEvent", "$q", "$mdDialog", function($model,$appEvent,$q, $mdDialog){
    var styles = {
        1: 'todo',
        2: 'in-progress',
        3: 'complete',
        4: 'cancelled'
    };
    var template = "<div class=\"task-item\"\n" +
        "           ng-class=\"itemClass()\" ng-show=\"!filter||filter[item.taskProgressStatusId.taskProgressStatusId].show\"" +
        "             layout=\"row\" layout-align=\"space-between center\">\n" +
        "            \n" + "<div ng-if=\"item.meta.info.icon() && item.taskProgressStatusId.taskProgressStatusId !== 3\" " +
        "                           style=\"position: relative; display: block; width: 0px; height: 0px;\" ng-click=\"badgeClick($event)\">\n" +
        "                    <div aria-label=\"{{item.meta.info.text()}}\">\n" +
        "                        <img width=\"16px\" height=\"16px\" style=\"margin-left:-20px;margin-top: 5px;cursor:pointer;\" ng-src=\"images/redux/icons/16px_information.png\" src=\"images/redux/icons/16px_information.png\">\n" +
        "                        \n" +
        "                    </div>\n" +
        "                </div>"+
        "            <div layout=\"column\" layout-align=\"space-between center\" \n" +
        "                 style=\"cursor:pointer;\" ng-click=\"details($event)\" flex=\"none\">\n" +
        "                 <div>\n"+
        "                     <md-tooltip>{{item.meta.hoverTip}}</md-tooltip>\n" +
        "                        <img style=\"margin:5px;\" ng-src=\"images/redux/icons/{{item.meta.icon.due}}.png\" />\n" +
        "                 </div>\n"+
        "                 <div>\n"+
        "                     <md-tooltip>{{taskIconHoverTip()}}</md-tooltip>\n" +
        "                        <img style=\"margin:5px;\" ng-src=\"images/redux/icons/{{item.meta.icon.type}}.png\" />\n" +
        "                  </div>\n"+
        "            </div>    \n" +
        "            <div ng-drag-handle class='task-item-text' layout=\"column\" layout-align=\"start start\" flex>\n" +
        "                <h3>{{ item.userTaskTitle}}</h3>\n" +
        "                <p>{{ item.userTaskDescr}}\n" +
        "                </p>\n" +
        "                <h4 ng-if='item.patientInfo !== undefined && item.patientInfo.lastname !== undefined'>{{ item.patientInfo.lastname}}, {{ item.patientInfo.firstname}}</h4>\n" +
        "            </div>\n" +
        "            <div  flex=\"nogrow\" ng-click=\"details($event)\"\n" +
        "                 style=\"cursor:pointer\" layout=\"column\" layout-align=\"center center\" >\n" +
        "                <img src=\"images/redux/icons/dots-vertical.png\">\n" +
        "            </div>    \n" +
        "            \n" +
        "            </div>";

    return {
        restrict: 'E',
        replace: false,
        scope:{
            item:'=',
            filter:'=?'
        },
        template:template,
        link:function(scope){
            function SQLdate(time){
                if (typeof time === 'undefined') return moment(Date.now()).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
                else return moment(time).format("YYYY-MM-DDTHH:mm:ss.SSSZ");
            }
            function checkConsent(data) {
                $appEvent('remote:appTaskList:checkConsent', data);
            }

            function isSystem(task) {
                return (typeof task.systemTaskId === 'undefined') ? false : true;
            }
            function checkMinor(data) {
                if(data.patientInfo !== undefined && data.patientInfo.dob !== undefined) {
                    $appEvent('remote:appTaskList:checkMinor', data.patientInfo.dob);
                }
            }
            scope.taskIconHoverTip = function(){
              if (isSystem(scope.item)) {
                return "System Task";
              }
              return "User Task";
            }
            scope.itemClass = function(){
                if (scope.item.taskProgressStatusId === undefined) {
                    return "";
                }
                return styles[scope.item.taskProgressStatusId.taskProgressStatusId];
            };
            scope.details = function (ev) {
                
                GWT.activity();
                $model.get('task').comments = [];          
                $appEvent('remote:appTaskList:getComments', {userTaskId: scope.item.userTaskId, communityId: scope.item.communityId});

                if (!$model.get('app').dialog.isOpen || $model.get('app').dialog.allowNextPopup)  {
                    $model.get('app').dialog.isOpen = false;
                    $model.get('app').dialog.allowNextPopup = false;
                    if (isSystem(scope.item)) {
                        checkConsent(scope.item);
                    }
                    $model.get('task').minorWarning = undefined;
                    checkMinor(scope.item);
                    scope.item.dueDateTime = moment(scope.item.dueDateTime).toDate();
                    
                    $model.get('task').orgCareteamUserSearch.selectedItem = {};
                    $model.get('task').orgCareteamUserSearch.selectedItem.firstName = scope.item.userFirstName;
                    $model.get('task').orgCareteamUserSearch.selectedItem.lastName = scope.item.userLastName;
                    $model.get('task').orgCareteamUserSearch.selectedItem.email = scope.item.userEmailId;

                    var cloneTask = {};
                    angular.copy(scope.item, cloneTask);
                    if (cloneTask.patientInfo === undefined || cloneTask.patientInfo === null) {
                        cloneTask.patientInfo = null;
                    }
                    
                    $model.get('app').showDialog('appTaskDetails', cloneTask, ev, function (result) {
                        switch (result) {
                            case 'ok':
                                cloneTask.dueDateTime = SQLdate(cloneTask.dueDateTime);
                                
                                cloneTask.lastUpdateDateTime = SQLdate();

                                $model.get('task').addMeta(cloneTask);
                                $model.get('task').setAssignTaskFields(cloneTask);
                                $model.get('task').setLastUpdateFields(cloneTask);

                                $model.get('task').tempTasks['' + scope.item.userTaskId] = scope.item;
                                
                                delete cloneTask['$$hashKey'];
                                delete cloneTask.meta;
                                $appEvent('remote:appTaskList:update', cloneTask);
                                break;
                            case 'cancel':
                                break;
                        }
                    });
                }
            };
            scope.badgeClick=function (ev) {
                GWT.activity();
                // backup the task to an original state (to revert if cancel is clicked)
                $model.get('app').showDialog('calendarTaskList', taskList, ev, function(result) {
                    switch (result) {
                        case 'close':
                            break;
                    }
                });
            }
        }
    }
}]);
