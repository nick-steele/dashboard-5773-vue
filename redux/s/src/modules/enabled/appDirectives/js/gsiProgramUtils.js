;(function(angular,app){
    app.service('gsiProgramUtils',function($model, $filter){

        var app = $model.get('app');

        /**
         * Utility function that returns configuration text
         * @param key
         * @return {*}
         */
        function getText(key){
            return app.settings.apps.patient.enrollment.program.text[key];
        }

        /**
         * Utility function that returns configuration icon
         * @param key
         * @return {*}
         */
       function getIcon(key){
            return app.settings.apps.patient.enrollment.program.icons[key];
        }

        /**
         * Utility function to format date to string
         * @param date -- Date object
         * @return {string|null}
         */
        function formatDate(date){
            if(!date) return null;
            return $filter('date')(date, 'yyyy-MM-dd');
        }

        /**
         * Utility function to parse date from string
         * @param str -- date string
         * @return {Date}
         */
        function parseDate(str){
            if(!str) return null;
            var arr = str.split('-');
            if(arr.length!==3) return null;
            return new Date(arr[0],arr[1]-1,arr[2]);
        };


        function isConsentManagedByParent(program){
            if(!program) return false;
            return (program.type === 'complex_subordinate' && program.parentProgram && program.parentProgram.consentRequired);
        };


        /**
         * Consenter value is required if program requires it & consent value is Yes or No
         * @return {boolean} true if user entered consenter is required
         */
        function isConsenterRequired(program, consentInfo){
            if(!program) return false;
            if(!isConsenterRequiredByProgram(program)) return false;
            return (
                consentInfo.consent &&
                consentInfo.consent.value &&
                (consentInfo.consent.value.toUpperCase() === 'YES' || consentInfo.consent.value.toUpperCase() === 'NO' )
            );
        }


        /**
         */
        function isConsenterRequiredByProgram(program){
            if(!program) return false;
            if(isConsentManagedByParent(program)) return program.parentProgram.consenterRequired;
            else return program.consenterRequired;
        }


        function isConsentRequired(program){
            if(!program) return false;
            if(isConsentManagedByParent(program)) return program.parentProgram.consentRequired;
            else return program.consentRequired;
        }

        function isMinorConsentRequired(program){
            if(!program) return false;
            if(isConsentManagedByParent(program)) return program.parentProgram.minorConsentRequired;
            else return program.minorConsentRequired;
        }

        function isConsentValid(program, consentInfo){
            if(!program) return true;
            if(!isConsentRequired(program)) return true;
            if(!consentInfo) return false;
            if (isConsentRequired(program) && (
                    !consentInfo.consent || !consentInfo.consent.id ||
                    !consentInfo.consentObtainedBy || !consentInfo.consentObtainedBy.userId ||
                    !consentInfo.consentDate
                )) return false;
            if(isConsenterRequired(program, consentInfo) &&
                (!consentInfo.consenter || !consentInfo.consenter.code)) return false;
            return true;
        }

        function findPatientProgramsById(id, list, deep){
            if(!list || !list) return null;
            for(var i = 0; i< list.length; i++){
                var pp = list[i];
                if(pp.program && pp.program.id === id) return pp;
                if(deep && angular.isArray(pp.subordinates)){
                    for(var j = 0;j<pp.subordinates.length; j++){
                        var sub = pp.subordinates[j];
                        if(sub.program && sub.program.id === id) return sub;
                    }
                }
            }
            return null;
        }

        return {
            getText: getText,
            getIcon: getIcon,
            formatDate: formatDate,
            parseDate: parseDate,
            isConsentManagedByParent: isConsentManagedByParent,
            isConsenterRequiredByProgram: isConsenterRequiredByProgram,
            isConsenterRequired: isConsenterRequired,
            isConsentRequired: isConsentRequired,
            isMinorConsentRequired:isMinorConsentRequired,
            isConsentValid: isConsentValid,
            findPatientProgramsById: findPatientProgramsById
        };
    })
})(angular,app);