function boot() {
	$log.info('Booting Reports App.');

	// Reports...
    my.reports = {

			Patients: [
				//{id: 'patientNotConsented',		title: 'Patients Not Consented', color: '#FFE082',	description: 'A list of those patients in your organization who have not yet provided consent.'},
				//{id: 'patientConsented',		title: 'Consented Patients', color: '#FFCC80',	description: 'A list of those patients for whom your organization has consent.'},
				//{id: 'patientTotals',			title: 'Total Patient Count', color: '#90CAF9', description: 'A summary count of patients in various categories in the GSIHealthCoordinator.'},
				{id: 'patientProgramReports',	title: 'Patient Program Report', color: '#80DEEA',	description: 'A report by patient of participation in programs and program status.'}
			]
	};

	var accesslevel = $model.get('app').login.accessLevelId;
	if(accesslevel === 1 || accesslevel === 2 || accesslevel === 4){
		my.reports.Administration = [
			{id: 'adminUserList', title: 'Administrative Reports User List', color: '#80DEEA', description: 'See a list of users who are on GSIHealthCoordinator.'}
		];
		my.reports.Patients.unshift({id: 'patientTrackingSheet',	title: 'Patient Tracking Sheet', color: '#FFAB91', description: 'An Excel summary of patients in your organization together with demographic data, enrollment status and encounter counts.', button: 'Download Report'});
	}

	// App feature list (for use in the future)...
	my.features = [
		{ name: 'email'		, icon: 'email'				, description: 'eMail this' },
		{ name: 'word'		, icon: 'file-excel-box'	, description: 'Create a Word file' },
		{ name: 'excel'		, icon: 'file-excel-box'	, description: 'Create an Excel file' },
		{ name: 'pdf'		, icon: 'file-pdf-box'		, description: 'Create a PDF' }
	];
}
