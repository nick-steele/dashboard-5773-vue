    // (public) app.note - Displays a brief note to the user
    my.note = function( msg, pos, delay ) {
        if ( !pos	) pos = 'bottom left';
		if ( !delay ) delay = 3000;
		var toast = $mdToast.simple()
            .hideDelay(delay)
			.textContent( msg )
            .action('OK')
            .highlightAction(false)
            .position(pos);

        $mdToast.show(toast).then(function(response) {
            if ( response === 'ok' )
                my.log('clicked note ok button');
        });
    };