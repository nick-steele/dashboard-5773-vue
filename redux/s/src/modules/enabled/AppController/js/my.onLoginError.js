
my.onLoginError = function (message) {

    $appEvent('local:login:failed');
    var alert = $mdDialog.alert({
        title: 'Login Failed',
        textContent: message,
        ok:'OK'
    });

    $mdDialog
        .show( alert )
        .finally(function() {
            alert = undefined;
        });
};

my.onTokenSyncError = function (message) {
    //TODO if there is server error handle this differently
    $appEvent('local:session:expired');
};

my.onCommSwitchError = function (message) {

    var alert = $mdDialog.alert({
        title: 'Community Switch Failed',
        textContent: message,
        ok:'OK'
    });

    $mdDialog
        .show( alert )
        .finally(function() {
            alert = undefined;
        });
};

