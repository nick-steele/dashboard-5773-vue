my.openCarebookSearch = function(data) {
    if (!my.consentMsg) {
        my.event('remote:carebookSearch:messagebundle');
    }
    if (!my.userCrossCommunity) {
        my.event('remote:carebookSearch:userCrossCommunity');
    }
    if (!my.consentAccess) {
        my.event('remote:carebookSearch:consentAccess');
    }
    if (!my.enabledCounty) {
        my.event('remote:carebookSearch:enabledCounty');
    }
    my.showDialog('carebookSearch', data, null, function() {});
};