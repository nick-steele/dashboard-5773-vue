// Apply new user settings to the logged in user...
function updateLogin(data) {
    // Inject the client-side app event data/etc...

   if(my.userAppInitialized){return;}

    my.globalTrace.did('updateLogin() start');
    my.devmode = GWT.isDevmode();
    my.countSubscribedApps = [];

    my.login = data;
    var hasPentaho = false;
    //For monitoring(New Relic)
    QA.monitor.login(data);

    my.login.userApps.forEach( function(item,index) {
       var tmpApp = my.appDetails[item.applicationName.trim()];
       //Starting with 2 (0:appHome, 1:appCareBookSearch)
       item.index=index+2;
       if (typeof tmpApp != 'undefined') {
           item.css = tmpApp.css;
           item.name = tmpApp.name;
           item.event = tmpApp.event?tmpApp.event:"local:openApp:"+tmpApp.name;
           if(tmpApp.hasCount){
               item.count="count_"+tmpApp.name;
               my.countSubscribedApps.push(item.name);
           }
           item.hasWindow = !!tmpApp.hasWindow;
           if(tmpApp.isPentaho){
               hasPentaho = true;
           }
       } else {
           $log.warn("App name could not be identified:",item.applicationName);
           item.css = 'help-icon';
           item.event = 'local:unknown:app';
       }
   });

    if(hasPentaho){
        my.settings.apps.login.pentahoEnabled = true;
    }
    my.userApps = my.login.userApps;

    // Notify Node of the new login data...
    // Changed callback function to app.initApp. Panda's reloadHomePanel is called within app.initApp
    $appEvent('remote:sync', {data: data, callback: {scope : 'app', func : 'initApp', args : undefined}});

    //main menu init
    $appEvent('local:state:mainMenu');


    // Change the home id div...
   //document.getElementById("reduxPanel").innerHTML = 'GWT footer';
   // Bit 1 indicates if Redux is enabled...
   // TODO: This UI selector system is being retired in 5.5...
	   my.ui = {
		   redux:(my.login.userInterfaceType & 1) ? true : false,
		   reduxSelectable: (my.login.userInterfaceType & 512) ? true : false, // TODO: Get rid of reduxSelectable in the view and here (replace with selectable)
		   selectable: (my.login.userInterfaceType & 512) ? true : false
	   };

	   // Allow editing from the console for debug purposes...
	   window.QA.ui = my.ui;

   my.userAppInitialized = true;
   $log.debug('->Login initialized');
    my.globalTrace.did('updateLogin() done');

	
	
	
	// Translate the old loginObject role and level to permissions in the new loginObject...
	// TODO: Remove this after migrating to RBAC/getting rid of GWT...
	
	// Takes any string and turns it into camelCase (support for permissions shim below)...
	var camelCase = function(str) {
	  return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
		return index == 0 ? letter.toLowerCase() : letter.toUpperCase();
	  }).replace(/\s+/g, '');
	}
	
	// Takes an array of strings and removes any dupes (support for permissions shim below)...
	var unique = function(a) {
		return a.sort().filter(function(item, pos, ary) {
			return !pos || item != ary[pos - 1];
		})
	}
	
	my.login.permissions = [];
	
	if (my.login.canConsentEnrollMinors)
		my.login.permissions.push( 'special_canConsentEnrollMinors');

	if (my.login.canManagePowerUser == 'Y')
		my.login.permissions.push( 'special_canManagePowerUser');

	if (my.login.reportingRole !== 'undefined')
		my.login.permissions.push( 'special_' + camelCase(my.login.reportingRole) + 'ReportingRole' );

	if (my.login.userLevel !== 'undefined')
		my.login.permissions.push( 'special_' + camelCase(my.login.userLevel) + 'Level' );

	if (my.login.accessLevel !== 'undefined')
		my.login.permissions.push( 'special_' + camelCase(my.login.accessLevel) + 'Access');
	

	// Make sure all permissions are unique...
	my.login.permissions = unique(my.login.permissions);

    //fixme
    $appEvent('remote:carebookSearch:messagebundle');

}