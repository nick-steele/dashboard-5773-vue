my.displaySubordinateInMenu = function (subordinate) {
	if(subordinate === undefined) {
		return '';
	}
	if(subordinate.type === 1) {
		return 'My Data';
	}
	else if(subordinate.type === 2) {
		return 'My Org’s Data';
	}
	else if (subordinate.type === 3 && subordinate.userId === -3) {
		return 'My Team’s Data';
	}
	else {
		return subordinate.lastName + ', ' + subordinate.firstName + '’s Data';
	}
}
