// Fires when the my.dialog object is ready to be populated ('local:dialog:[name]' was called and pushed a dialog event)... an event, data is optional...
my.showDialog = function(name, data, ev, callback, boot) {
    // Transfer the passed data/scope to the app (my) scope... (scope: scope, data: data, template: template, sourceElement: element, callback: callback(choice))
    my.dialog.name = name;
    my.dialog.data = data;
    my.dialog.boot = boot;
    my.dialog.sourceElement = ev;
    my.dialog.callback = callback;
    my.dialog.isOpen = true;
    // Call the correct application state... (the actual dialog gets drawn in eventServer, when the template gets returned as an application (dialog) state)
    $appEvent('local:state:' + name);
};