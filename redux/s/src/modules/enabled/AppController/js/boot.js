/**
 * @kind function
 * @name boot
 *
 * @description
 * This private boot function is the first thing that gets called when application is initialized.
 * Any independent app setups can be done here.
 * */
function boot() {
    // Fix bug cannot use window.location.origin in IE for carebook URL
    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
    }
    // Finish the boot trace...
    QA.trace.finish('system.boot');

    // Start an app.boot trace...
    QA.trace.add('app.boot');

        window.QA.app = my;
        window.QA.app.scope = function() { return $scope };

        // For dynamic copyright footer, and other date related issues...
        my.date = new Date();

		// Hoist the app settings...
        my.settings = client.settings;
		console.log(my.settings)
		// Hoist the tenant settings based on the host...

		// Common Login - Find the correct tenant based on the URL...
		var host = window.location.hostname.toLowerCase();
		my.settings.tenant = my.settings.hosts[host].id;

		// Defaults...
		my.settings.apps.login.background = 'assets/tenant/' + my.settings.tenant + '/login-bg'
		my.settings.apps.login.logo = 'assets/tenant/' + my.settings.tenant + '/main-logo.png'
        my.settings.components.titleBar.logo = 'assets/tenant/' + my.settings.tenant + '/nav-logo.png'

		// Get the tenant settings and layer them...
		$http.get(my.settings.components.app.cdn + 'assets/tenant/' + my.settings.tenant + '/settings.json').success(function(data) {
			$.extend(true, my.settings, data);
			// app.login has some special features...

			// If no background name is defined, use the default expected name...
			if (typeof my.settings.apps.login.background === 'undefined') my.settings.apps.login.background = 'assets/tenant/' + my.settings.tenant + '/login-bg'

			// If more than one background to choose from)...
			if (typeof my.settings.apps.login.backgrounds !== 'undefined') {
				if (my.settings.apps.login.backgrounds > 0)
					my.settings.apps.login.background = my.settings.apps.login.background + Math.floor(Math.random() * (my.settings.apps.login.backgrounds) + 1);
			}

			// If quotes exist, randomize them and place them in the tagLine...
			if (typeof my.settings.apps.login.text.quotes !== 'undefined') {
				my.settings.apps.login.text.tagLine = my.settings.apps.login.text.quotes[Math.floor(Math.random() * my.settings.apps.login.text.quotes.length)]
			}
            $appEvent('remote:login:setting', {settings: my.settings});
            
            // Handle activity setup...
            
                // Reflow settings now that tenant settings have been applied...
                gsiActivity.options = my.settings.components.activity.options;

                // Call GWT.activity() when the user is active...
                gsiActivity.on('activity', function() {
                    // On slower machines, GWT.activity won't be defined for a few seconds while GWT sets up it's infrastructure, so make sure it's a function before we call it...
                    if (typeof GWT.activity === "function") GWT.activity();
                })

                // Warn the user when they are about to become inactive...
                function myWarning() {
                    $mdToast.show(
                        $mdToast.simple()
                          .textContent(my.settings.components.activity.text.warning)
                          .hideDelay(3000)
                      )
                }
                
                // Publish the warning...
                gsiActivity.on('warning', myWarning)

                // Turn it on...
                gsiActivity.enable()

            // Set the background image...
            console.log('Setting background image...')
            let bgImage = "url(" + (my.settings.components.cdn ? my.settings.components.cdn : '') + my.settings.apps.login.background + ".jpg)";
            document.getElementById("LoginScreen").style.backgroundImage = bgImage;
		});


    QA.trace.log('app.boot', 'settings');

    my.$trace = $trace;

    //Adding for IE performance debug...
    my.globalTrace = new $trace('global');
    // Not logged in by default...
    my.isGWTCheckingToken = true;
    my.loggedIn = false;
    // Not in devmode by default...
    my.devmode = false;
    // Get the build version...
    my.build = window.QA.build;

    my.reduxReady = false;

    my.sessionTimer = {};

    // App logger...
    my.log = function( msg ) { console.log(msg); };
    // For determining the media size in the template...
    my.media = function(e) { return $mdMedia(e); };


    my.nodeDefault = {
        version: '',
        status: 'connecting...',
        connected: false
    };
    my.node = my.nodeDefault;

    // Build dialog model...
    my.dialog = {data: {}, choice: null};


    // Let the QA object touch the global model for testing/debugging...
    my.model = $model;
    my.$scope = $scope;
    // Hoist the generated eventServer...
    my.events = {push: {}, local: {}};

    my.$window = $window;
    my.openGlobalSearch = false;
    my.toggleClass = '';

    QA.trace.log('app.boot', 'vars');

    // Store the routers and JS payload...
    localStorage.setItem('cache.router.push',		JSON.stringify( {version: '1.0.1', data: window.client.routers.push }) );
    localStorage.setItem('cache.router.client',		JSON.stringify( {version: '1.0.1', data: window.client.routers.client }) );
    localStorage.setItem('cache.app.js',			JSON.stringify( {version: '1.0.1', data: 'empty' }) );
    localStorage.setItem('cache.app.css',			JSON.stringify( {version: '1.0.1', data: 'empty' }) );
    localStorage.setItem('cache.app.graphics',		JSON.stringify( {version: '1.0.1', data: 'empty' }) );
    localStorage.setItem('cache.app.states',		JSON.stringify( {version: '1.0.1', data: 'empty' }) );
    localStorage.setItem('cache.app.user',			JSON.stringify( {version: '1.0.1', data: 'empty' }) );
    localStorage.setItem('cache.app.log',			JSON.stringify( {version: '1.0.1', data: 'empty' }) );
    localStorage.setItem('cache.app.state',			JSON.stringify( {version: '1.0.1', data: 'empty' }) );

    QA.trace.log('app.boot', 'localStorage');

    // TODO: Store all views using lazy-loading (the server will then hot-swap changes if they happen, this will lead to a usable offline app, and full app caching)...

    // Load the routers...
    // TODO: Get rid of the push router (push routing can be combined with a client router using a single push function for pass through, this will make code 1/3rd cleaner (only 2 routers instead of 3)
    code = 'my.events.local = ' + LZString.decompressFromBase64(window.client.routers.client);
    try { eval( code ); } catch (e) { $log.error('Client router is invalid.', e, code); }

    // TODO: remove this once the routers have been combined...
    code = 'my.events.push = ' + LZString.decompressFromBase64(window.client.routers.push);
    try { eval( code ); } catch (e) { $log.error('Push router is invalid.', e, code); }

    my.carebookSearchResult = {};
}

function getQueryVariable(variable)
{
    var query = $window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if(pair[0] == variable){return pair[1]||true;}
    }
    return(false);
}

//Called when socketConnected
/**
 * @kind function
 * @name my.appBoot
 *
 * @description
 * This appBoot function is called by socketConnected which receives app.settings from Node server.
 * All app configuration dependent on this app.settings should be done in this function.
 * */
my.appBoot = function() { // FIXME: currently gets called everytime socket is reconnected. maybe just call once

    // We get the key and screenName from my.login, everything else (icon and event) from this array...
		//TODO: hasCount value
		my.appDetails = {
				"HOME":								{css: "admin-menu",					name: "appHome"             },
				"GSI_ADMIN_APP":					{css: "admin-menu",					name: "appAdmin"            },
				"GSI_ENROLLMENT_APP":				{css: "enrollments-menu",			name: "appEnrollment"       },
				"GSI_CARETEAM_APP":					{css: "careteam-menu",				name: "appCareteams"        },
				"GSI_REPORTS_APP":					{css: "reports-menu",				name: "appReports"          },
				"TREAT":							{css: "treat-menu",					name: "appCareplan"         ,hasWindow:true},
				"GSI_MESSAGING_APP":				{css: "message-menu",				name: "appMessages"         , hasCount:true, hasWindow:true},
				"GSI_ALERTING_APP":					{css: "alerts-menu",				name: "appAlerts"           , hasCount:true},
				"GSI_TASK_LIST":					{css: "patient-summary-menu",		name: "appTaskList"         },
				"GSI_CAREBOOK_APP":					{css: "care-book-menu",				name: "appCarebook"         },
				"GSI_PATIENT_LIST_UTILITY":			{css: "patient-summary-menu",		name: "appPatientList"      },
				"GSI_CARE_COORDINATION":			{css: "treat-menu",					name: "appCareplan"         },
				"GSI_PATIENT_ENGAGEMENT":			{css: "patient-engagement-menu",	name: "appEngagement"       },
				"GSI_UHC_ENROLLMENT_REPORT_APP":	{css: "enrollments-menu",			name: "appUHCEnrollment"    , hasWindow:true, isPentaho: true},
				"GSI_POPULATION_MANAGER_APP":		{css: "population-manager-menu",	name: "appPopManager"       , hasWindow:true, isPentaho: true},
				"GSI_TASK_MANAGER_APP":				{css: "patient-summary-menu",		name: "appTaskList"          , hasCount:true}
		};

		// Default context menu...
		my.defaultContextMenu = [
		];

		my.userMenu = [
			{ text: 'Preferences',					icon: 'settings',					event: 'local:prefs:show'               ,		elementId: 'btnPrefs'},
			{ text: 'Help',							icon: 'help',						event: 'local:help:show`'               , 		elementId: 'btnHelp'},
			{ text: 'Resource Center',				icon: 'book-open',					event: 'local:resourceCenter:show'      , 		elementId: 'btnResource'},
            { text: 'Training',				        icon: 'school',					    event: 'local:openWindow:training'      , 		elementId: 'btnTraining'}

        ];
		my.globalMenu = [
			{ text: 'Search',						icon: 'magnify',					event: 'local:search:global'       		},
			{ text: 'Patient Panel',				icon: 'view-list',					event: 'local:myPatientList:show'       }
		];
		my.contextMenu = my.defaultContextMenu;
		my.appWindows = {};


        my.patientListNav=gsiSideNav.get('PatientList',false);

        my.minDate = new Date('1/1/1880');
	    my.maxDate = new Date('1/31/2130');
	// QA.trace.log('app.boot', 'Routers');

    // If we get here, the application is ready to rock...
    my.currentMenuTheme = 'theme-bg-primary';
    QA.trace.finish('app.boot');

    //Rendering completed
        my.loaded = true;

    loadComplete();

    my.consoleLog = function (e) {
        console.log(e);
    }
    //utils

    if(!my.identity||!my.identity.token){ // if identity is null user is not logged in yet; If we could strip key maybe we don't need this
        var otpKey = getQueryVariable('key');
        $location.search('key', null).replace();
        if(otpKey){
            $appEvent("local:login:token",otpKey);
        }else{
            if(!my.loggedIn) $appEvent("local:login:show");
            //This is temporary workaround for common login forgot password
            var forgotPasswordUserName = getQueryVariable('fp');
            if(forgotPasswordUserName){
                var _email = decodeURIComponent(forgotPasswordUserName);
                handleForgotPasswordRedirect(_email);
                $location.search('fp', null).replace();
            }
        }
    }
};

// use QA.app.update() to have Angular refresh for changes...
my.update = function () {
	
};

function handleForgotPasswordRedirect(forgotPasswordEmail) {
    var gwtWatchPromise = $interval(function(){
        if($window.GWT&&angular.isFunction($window.GWT.runForgotPassword)){
            $interval.cancel(gwtWatchPromise);
            $window.GWT.runForgotPassword(forgotPasswordEmail);
        }
    },200);
}