my.postAppBoot = function(data) {
    my.subordinates = data.subordinates;
    
    if (my.subordinates && my.subordinates.length > 0) {
        var user = {};
        user.email = my.login.email;
        user.firstName = my.login.firstName;
        user.lastName = my.login.lastName;
        user.userId = my.login.userId;
        my.subordinates.unshift(user);
        $model.get('home').currentMember = my.subordinates[0];
    }
}