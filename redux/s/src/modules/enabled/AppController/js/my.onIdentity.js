my.onIdentity =function (data) {
    my.identity=data;
    //GWT login
    if(my.identity && my.identity.token){
        var gwtWatchPromise = $interval(function(){
            if($window.GWT)console.log($window.GWT.checkToken);
            if($window.GWT&&angular.isFunction($window.GWT.checkToken)){
                $interval.cancel(gwtWatchPromise);
                $window.GWT.checkToken(my.identity.token);
            }
        },200);
        // var gwtWatcher = $scope.$watch(function(){
        //     if($window.GWT)console.log($window.GWT.checkToken);
        //     return $window.GWT&&$window.GWT.checkToken&&angular.isFunction($window.GWT.checkToken);},function(n){
        //     if(n){
        //         gwtWatcher();
        //         $timeout(function () {
        //             $window.GWT.checkToken(my.identity.token);
        //         },100);
        //     }
        // });
    }
};

//
// //Dirty workaround to make sure GWT is ready
// function gwtReadyWatcher(){
//     var _gwtReadyWatcher = $scope.$watch(function(){return my.settings.apps.login.isGwtReady;},function(n){
//         if($window.GWT.ready === true){
//             _gwtReadyWatcher();
//             $timeout(function () {
//                 $window.GWT.checkToken(my.identity.token);
//             },100);
//         }
//     });
// }