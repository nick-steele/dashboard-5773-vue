my.displayImportPopup = function(patient) {
    var position = $mdPanel.newPanelPosition()
        .absolute()
        .center();

    function PanelController(mdPanelRef) {
        var panel = this;
        this.genders = my.genders;
        this.patient = patient;
        this.enabledCounty = my.enabledCounty;
        if (this.patient.middleName)
            this.patientName = this.patient.firstName + ' ' + this.patient.middleName + ' ' + this.patient.lastName;
        else 
            this.patientName = this.patient.firstName + ' ' + this.patient.lastName;            
        this._mdPanelRef = mdPanelRef;
        this.close = function () {
            var panelRef = this._mdPanelRef;
            panelRef && panelRef.close().then(function () {
                panelRef.destroy();
            });
        };
        this.cancel = function () {
            var payload = {
                patient: this.prepairPayload(patient),
                action: {
                    name: 'Import',
                    choice: 'Y'
                }
            };
            // my.event('local:appCareBookSearch:auditImport', payload);
            this.close();
        };
        this.ok = function () {
            var payload = {
                patient: this.prepairPayload(patient),
                action: {
                    name: 'Import',
                    choice: 'Y'
                }
            };
            my.event('local:appCareBookSearch:auditImport', payload);
            this.close();
        };
        this.prepairPayload = function (patientInfo){
            var patientToEnrollment = angular.copy(patientInfo);
            delete patientToEnrollment.programs;
            delete patientToEnrollment.status;
            delete patientToEnrollment.organizationName;
            delete patientToEnrollment.otherCommunities;
            return patientToEnrollment;
        }


        // Flatten nested programs
        // Simple subordinate programs need to be shown as single program
        this.flattenedProgramList = flattenPrograms(this.patient.programs);

        function flattenPrograms(programs) {
            var arr = [];
            if(!programs) return arr;
            angular.forEach(programs, function(p){
                if(p.type === 'simple_parent' && p.childPrograms) {
                    angular.forEach(p.childPrograms, function(pc){ arr.push(pc) })
                }else{
                    arr.push(p);
                }
            });
            return arr;
        }

    }

    var config = {
        attachTo: angular.element(document.body),
        controllerAs: 'panelCtrl',
        controller: PanelController,
        template: '<div id="import-popup">' +
        '   <md-toolbar class="panel-title">' +
        '       <div class="md-toolbar-tools">' +
        '           <h2 style="text-transform: capitalize;">Patient Import</h2>' +
        '           <span flex></span>' +
        '           <md-button class="md-icon-button" ng-click="panelCtrl.cancel()" arial-label="cancel"><md-icon md-svg-icon="close"></md-icon></md-button>' +
        '       </div>' +
        '   </md-toolbar>' +
        '   <div class="panel-content padding-32" layout="column">' +
        '       <span class="profile-title" ng-bind="panelCtrl.patientName"></span>' +
        '       <span class="profile-subtitle">From community {{panelCtrl.patient.communityName}}</span>' + 
        '       <md-divider style="margin-top: 25px"></md-divider>' +
        '       <ul class="profile-list" layout="row" layout-wrap>' +
        '           <li class="profile-item" flex="50" layout="row">' +
        '               <strong class="title">Gender</strong>' +
        '               <span flex class="infor" ng-bind="panelCtrl.genders[panelCtrl.patient.gender]"></span>' +
        '           </li>' +
        '           <li class="profile-item" flex="50" layout="row">' +
        '               <strong class="title">Address 1</strong>' +
        '               <span flex class="infor">{{panelCtrl.patient.address1}}</span>' +
        '           </li>' +
        '           <li class="profile-item" flex="50" layout="row">' +
        '               <strong class="title">Birthday</strong>' +
        '               <span flex class="infor">{{panelCtrl.patient.dob | date:"MM/dd/yyyy"}}</span>' +
        '           </li>' +
        '           <li class="profile-item" flex="50" layout="row">' +
        '               <strong class="title">Address 2</strong>' +
        '               <span flex class="infor">{{panelCtrl.patient.address2}}</span>' +
        '           </li>' +
        '           <li class="profile-item" flex="50" layout="row">' +
        '               <strong class="title">SSN</strong>' +
        '               <span flex class="infor" ng-bind="panelCtrl.patient.ssn | ssn"></span>' +
        '           </li>' +
        '           <li class="profile-item" flex="50" layout="row">' +
        '               <strong class="title">City</strong>' +
        '               <span flex class="infor">{{panelCtrl.patient.city}}</span>' +
        '           </li>' +
        '           <li class="profile-item" flex="50">' +
        '               <strong class="title" ng-bind="!panelCtrl.patient.medicareId ? \'Medicaid Id\' : \'Medicare Id\'"></strong>' +
        '               <span flex class="infor" ng-bind="panelCtrl.patient.medicaidId || panelCtrl.patient.medicareId"></span>' +
        '           </li>' +
        '           <li class="profile-item" flex="50" layout="row">' +
        '               <strong class="title">State</strong>' +
        '               <span flex class="infor">{{panelCtrl.patient.state}}</span>' +
        '           </li>' +
        '           <li class="profile-item" flex="50" layout="row">' +
        '               <strong class="title">Zip Code</strong>' +
        '               <span flex class="infor">{{panelCtrl.patient.zipcode}}</span>' +
        '           </li>' +
        '           <li class="profile-item" ng-if="panelCtrl.enabledCounty === \'true\'" flex="50" layout="row">' +
        '               <strong class="title">County</strong>' +
        '               <span flex class="infor">{{panelCtrl.patient.county}}</span>' +
        '           </li>' +
        '           <li class="profile-item" layout="row" flex="100">' +
        '               <strong class="title">Programs</strong>' +
        '               <div flex ng-if="panelCtrl.patient.programs && panelCtrl.patient.programs.length>0" style="margin-top: -10px">' +
        '                   <md-chips class="small-chip" ng-model="panelCtrl.flattenedProgramList" readonly="true">' +
        '                       <md-chip-template>' +
        '                           {{$chip.programName}}' +
        '                       </md-chip-template>' +
        '                   </md-chips>' +       
        '               </div>' + 
        '           </li>' +
        '       </ul>' +
        '   </div>' +
        '   <div class="panel-action" layout="row">' +
        '       <span flex></span>' +
        '       <md-button id="btnConfirmYes" class="md-primary md-raised" ng-click="panelCtrl.ok()">Yes - Import Patient</md-button>' +        
        '       <md-button id="btnConfirmNo" ng-click="panelCtrl.cancel()">Cancel</md-button>' +
        '   </div>' +
        '</div>',
        disableParentScroll: true,
        hasBackdrop: true,
        panelClass: 'tooltip-dialog',
        position: position,
        trapFocus: true,
        zIndex: 250,
        clickOutsideToClose: true,
        escapeToClose: true,
        focusOnOpen: true
    };

    $mdPanel.open(config);
};