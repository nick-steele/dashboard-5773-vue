// Fires an event, data is optional...
my.event = function(event, data, tracer) {
	// Prevent errors...
	if (typeof tracer === 'undefined') tracer = { log: function() {$log.debug('Tracer: should never be here')} }
	if($window.QA.eventHistory.isEnabled()) $window.QA.eventHistory.add(event,data);
    // Split the event into its components...
    try {
        var parts = event.split(':');
    } catch(error) {
        my.err('Malformed or missing event name: ' + event);
    }
	var type = parts[0];
	var what = parts[1];
	var where = parts[2];

	switch (type) {
        case 'local':
			//tracer.log('local');
            // Events happening internally (client-client)
            if (what.substring(0,3) === 'app') {
                // TODO: Build the app type into the app list...
				var appType = (what === 'appTaskList') ? 'Angular' : 'GWT';
				$log.debug('<-GUI EVENT-local', what + '(' + where + ') [' + appType + ']');
            } else
                $log.debug('<-GUI EVENT-local', what + '(' + where + ')');
            if(angular.isFunction(my.events.local[what])){
                QA.monitor.event(event);
                my.events.local[what](where, data);
            }else{
                var errmssg = "local."+what+" is not a function.";
                QA.monitor.error(event, errmssg);
                my.err(errmssg);
            }
            break;
        case 'remote':
            // Events happening internally (client-server)
			//tracer.log('remote');
            QA.monitor.event(event);
            debug.info('Remote:' + what + '(' + (where ? where : data) + ')');
			socket.emit(what, { payload: data, event: where });
            break;
        case 'push':
			// $log.debug('push');
			tracer.log('push[' + what + ']');
            // Incoming events from the server (server-client)
            my.events.push[what](where, data, tracer);
            break;
        default:
			//tracer.log('unknown');
            // Unknown event types...
            QA.monitor.error(event, "Unknown event");
            $log.debug('.DIE. EVENT-unknown (' + type + ')', what + '(' + where + ')');
            break;
    }
}