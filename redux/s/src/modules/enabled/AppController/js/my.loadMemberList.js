my.loadMemberList = function(data) {
    my.subordinates = data;
    my.defaultUserViews = [];

    var user = {};
    user.email = my.login.email;
    user.firstName = my.login.firstName;
    user.lastName = my.login.lastName;
    user.userId = my.login.userId;
    user.type = 1;
    user.selected = true;
    my.defaultUserViews.push(user);

    var org = {};
    org.userId = -2;
    org.type = 2;
    my.defaultUserViews.push(org);

    if ( my.subordinates &&  my.subordinates.length > 0) {
        var team = {};
        team.email = '';
        team.userId = -3;
        team.type = 3;
        my.defaultUserViews.push(team);

        for (var i = 0; i < my.subordinates.length; i++) {
            if(my.subordinates[i].email !== user.email) {
                my.subordinates[i].type = 3;
                team.email = team.email + my.subordinates[i].email + ';';
            }
        }


        $model.get('home').patientsToggle[3] = "My Team View";
    }
    $model.get('home').currentMember = angular.copy(my.defaultUserViews[0]);
    $model.get('home').selectedSubordinates = [];
};
