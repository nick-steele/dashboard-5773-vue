//Reorganized switchTab flows using events for efficiency... see openTab.js and gsiSideNav for details.

// my.closeTab = function(data, event){
// 	console.log("closeTab " + data);
// 	my.activeApps.splice(my.activeApps.indexOf(data), 1);
// }
//
// my.openTab = function(what, data){
// 	var needToRender = true;
// 	if((my.listTabs.indexOf(what)!= -1 && typeof(data) != 'undefined')) {
//         if(my.activeApps.indexOf(data) === -1){
//             my.activeApps.push(data);
//             needToRender = true;
//             console.log("tab length: " + my.activeApps.length);
//         } else{
//             my.selectedTab=my.activeApps.indexOf(data) + 1;
//             console.log("selected tab: " + my.selectedTab);
//             needToRender = false;
//         }
//
//     }
//     return needToRender;
// }