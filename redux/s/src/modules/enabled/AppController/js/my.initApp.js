//This will be called by Redux.
//Put everything needs to be run to initialize the application
my.initApp = function () {


    my.globalTrace.did('initApp start');
    // Todo: use state files instead of calling remote events here
    $log.debug('Redux received login object. Initializing application...');
    //patientPanel init
    $appEvent('local:state:patientPanel');

    //appHome init
    $appEvent('local:state:appHome');

    function checkCountUpdate(name){

        var watcher = $scope.$watch(
            function (my,badgeVar) {
                return my["count_"+badgeVar]
            }.bind(null,my,badgeVar),
            function (name,n,o) {
                if(typeof o === 'undefined') return;
                if(o<n){
                    my.showNotifToast(name);
                    watcher();
                }
            }.bind(null,name));
    }

    if(my.settings.apps.login.pentahoEnabled){
        $appEvent('local:login:pentaho');
    }


    // should be called after node is ready
    if(my.countSubscribedApps) {
        //TODO: dirty workaround

        for (var i = 0; i < my.countSubscribedApps.length; i++) {
            if (my.countSubscribedApps[i] == 'appTaskList') continue; // No need to show app task notification yet
            var badgeVar = my.countSubscribedApps[i];
            checkCountUpdate(badgeVar)
        }

        $appEvent('remote:appGlobal:count', my.countSubscribedApps);
    }

    // Timer
    // !! pooling is not mistyped... server object has the misspelling
    if(my.login.poolingEnabled){
        $log.debug("Enabling badge count timer...");
        var countTimer = $interval(function () {
            if (my.countSubscribedApps && my.isAuthenticated === true) $appEvent('remote:appGlobal:count', my.countSubscribedApps);
        }, (my.login.pollingIntervalInSeconds||300)*1000);
    }

    my.globalTrace.did('initApp done');

    if(my.gwtLoginLog){
        $appEvent('remote:log:gwtLogin', my.gwtLoginLog);
    }
    //optional flag
    my.reduxReady = true;

    if(my.identity.patientId && isFinite(parseInt(my.identity.patientId))){
        $log.debug('-> found patient context', my.identity.patientId);
        $appEvent('remote:carebookSearch:checkConsentOnLogin', parseInt(my.identity.patientId));
    }


};