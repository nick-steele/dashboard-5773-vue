    // TODO: deprecated / moved into webSocket.js
    // Once Node establishes a connection, it will call this function, giving us status and version information...
    my.socketDisconnected = function( p ) {
        // Fill the node object with defaults...
        my.nodeFill();
        // Yell we've been disconnected...
        $log.debug('Node disconnected.');
        //my.note('Lost connection to Redux');
        my.isConnected = false;
    };