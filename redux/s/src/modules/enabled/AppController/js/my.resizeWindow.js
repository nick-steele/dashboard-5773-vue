my.resizeWindow = function() {
    $timeout(function() {
        var evt = document.createEvent("HTMLEvents");
        evt.initEvent('resize', true, false);
        window.dispatchEvent(evt);
    }, 500);
}