//Created only for logging GWT login
my.loginTimerLog = {logs:[]};
my.GwtLoginTimer = function (msg) {
    var now = new Date();
    switch(msg){
        case 'login_start':
            my.loginTimerLog = {logs:['start']};
            my.loginTimerLog.start = now;
            break;
        case 'login_failure':
            my.loginTimerLog.logs.push('['+(now-my.loginTimerLog.last)+'].error');
            $appEvent('remote:log:gwtLogin', {
                result:false,
                ms:now-my.loginTimerLog.start,
                log:my.loginTimerLog.logs.join('.')
            });
            break;
        case 'login_success':
            my.loginTimerLog.logs.push('['+(now-my.loginTimerLog.last)+'].success');
            my.gwtLoginLog = {
                result:true,
                ms:now-my.loginTimerLog.start,
                log:my.loginTimerLog.logs.join('.')
            };
            break;
        default:
            my.loginTimerLog.logs.push('['+(now-my.loginTimerLog.last)+'].'+msg);

    }
    my.loginTimerLog.last = now;
};