//Count models are called count_{{nameOfApp}}
//When a count goes to error state, an object is assigned to the model
my.onCountError = function (data) {
    $log.warn(data.message);
    if(data.model){
        var prevCount = !my[data.model]?"?":
            angular.isObject(my[data.model])?my[data.model].prevValue:my[data.model];
        my[data.model] = {prevValue : prevCount, isError : true};
    }
};