my.confirmCommunitySwitch = function (tenant) {
    var _this = this;
    if(my.settings.apps.login.switching.showConfirmation){
        var message = my.settings.apps.login.text.switchingMessage,
            title = my.settings.apps.login.text.switchingTitle;
        var alert = $mdDialog.confirm({
            title: title,
            textContent: message,
            ok:'OK',
            cancel: 'Cancel',
            css:'notify-dialog md-body-1',
            flex:66
        });

        $mdDialog
            .show( alert )
            .then(requestCommunitySwitch.bind(_this, tenant),angular.noop);
    }else{
        requestCommunitySwitch(tenant);
    }
};

function requestCommunitySwitch(tenant) {
    $appEvent('remote:community:switch',tenant.id);
}

my.onSwitchCommunity = function (data) {
    angular.forEach(my.appWindows, function (value) {
        value && angular.isFunction(value.close) && value.close();
    });
    $window.location.href = data.redirect;
};