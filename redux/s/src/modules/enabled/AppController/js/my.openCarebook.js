my.openCarebook = function(patient) {
    var okFn = function(audit, cancel) { //TODO refactor
        //second popup
        $model.get('home').displayConfirmation('Warning', my.consentMsg.consentLanguage, audit, cancel? cancel : function() {});
    };
    if (my.isPatientFromMT(patient)) {
        if (my.isPatientFromLocal(patient)) {
            var auditRemote = function() {
                if (patient.consented  === 'false') {
                    var redirectedToCarebook = true;
                    GWT.handleSelectedPatientInSearch(patient.patientId.toString());
                }
                my.event('local:appCareBookSearch:auditSearch', {
                    patient: patient,
                    action: {
                        name: 'Search',
                        choice: 'Y'
                    }
                });
                if (!redirectedToCarebook) {
                    my.event('local:appCareBookSearch:toCareBookByPatient', patient);
                }
            };
            var deniedAccess = function() {
                my.event('local:appCareBookSearch:auditSearch', {
                    patient: patient,
                    action: {
                        name: 'Search',
                        choice: 'N'
                    }
                });
            };

            var showRemoteWarningPopup = function(){
                $model.get('home').displayConfirmation('Warning', my.settings.apps.carebook.remoteCommunityWarning, auditRemote, deniedAccess);
            };

            if (patient.consented === 'true') { showRemoteWarningPopup(); }
            else if (my.consentAccess === 'true') {
                $model.get('home').displayConfirmation('Warning', my.consentMsg.consentWarning, showRemoteWarningPopup, angular.noop());
            } else {
                $model.get('home').displayConfirmation('Warning', my.consentMsg.consentDenied, function() {}, function() {});
            }
        }
        else {
            //import console.log
            if (!patient.programs) {
                my.event('remote:carebookSearch:activePrograms', {
                    patientId: patient.patientId,
                    communityId: patient.communityId
                });
            }

            //Warn that this patient is remote; If Yes proceed to import.
            $model.get('home').displayConfirmation(
                'Warning',
                my.settings.apps.carebook.remoteCommunityWarning,
                function () { my.displayImportPopup(patient); },
                function() {}
                );
            
        }
    } else { //only local
        if (patient.consented === 'true') {
            my.event('local:appCareBookSearch:toCareBookByPatient', patient);
            $mdDialog.hide();
        } else {
            if (my.consentAccess === 'true') {
                var auditLocal = function () {
                    GWT.handleSelectedPatientInSearch(patient.patientId.toString());
                };
                $model.get('home').displayConfirmation('Warning', my.consentMsg.consentWarning, okFn.bind(null, auditLocal), function() {});
            } else {
                $model.get('home').displayConfirmation('Warning', my.consentMsg.consentDenied, function() {}, function() {});
            }
        }
    }
};
my.getTenantInstance = function(communityId) {
    for(var i=0; i<my.identity.tenants.length; i++) {
        if (my.identity.tenants[i].id === communityId)
            return my.identity.tenants[i].instance;
    }
    return false;
}
my.buildCarebookUrl = function(patient) {
    var carebookUrl = my.login.carebookSummaryURL || (window.location.origin + '/carebook/login');
    carebookUrl += '?community=' + encodeURIComponent(my.identity.tenant.instance) + '&jwtToken=' + my.identity.token ;
    if (my.isPatientFromMT(patient)) {
        var dataObj = {};
        dataObj[patient.communityId] = {
            patientId: patient.patientId + '',
            userEmail: my.login.email
        };
        for (var i = 0; i < patient.otherCommunities.length; i++) {
            dataObj[patient.otherCommunities[i].communityId] = {
                patientId: patient.otherCommunities[i].patientId + '',
                userEmail: my.login.email
            };
        }
        var multitenantData = {
            localCommunity: my.login.communityId + '',
            data: dataObj
        };
        carebookUrl += '&tokenId=' + my.login.token + '&multitenantData=' + JSON.stringify(multitenantData);
    } else {
        carebookUrl += '&patientId=' + patient.patientId + '&userId=' + my.login.email + '&communityId=' + my.login.communityId + '&tokenId=' + my.login.token;
    }
    return carebookUrl;
};
my.isPatientFromMT = function(patient) {
    return (patient.communityId && patient.communityId !== my.login.communityId) || (patient.otherCommunities && patient.otherCommunities.length > 0);
};
my.isPatientFromLocal = function(patient) {
    return !patient.communityId || (patient.communityId === my.login.communityId) || (patient.otherCommunities && !patient.otherCommunities.every(function(val) { return val.communityId !== my.login.communityId}));
};
my.prepairDataAudit = function (patient, action) {
    var auditInfo = {
        patientId: patient.patientId,
        userId: my.login.userId,
        communityId: my.login.communityId
    };
    auditInfo.communities = [];
    auditInfo.communities.push({
        communityId: patient.communityId,
        patientId: patient.patientId,
        consented: patient.consented
    });
    for (var i = 0; i < patient.otherCommunities.length; i++) {
        var community = patient.otherCommunities[i];
        var communityObj = {
            communityId: community.communityId, 
            patientId: community.patientId,
            consented: (typeof community.consented !== 'undefined') ? community.consented.toString() : 'false'
        };
        auditInfo.communities.push(communityObj);
    }
    auditInfo.action = action;
    return auditInfo;
};