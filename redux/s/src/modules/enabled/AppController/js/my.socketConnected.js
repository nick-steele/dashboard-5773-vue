    // Once Node establishes a connection, it will call this function, giving us status and version information...
    my.socketConnected = function( payload, trace ) {
		payload.status = 'connected';
        payload.connected = true;
        trace.log('calling my.nodeFill()');
        my.nodeFill( payload.node );
        //console.log('Node connection established. Server version:', my.node.version);
        my.isConnected = true;
        // my.settings = payload
        my.appBoot();
    };
