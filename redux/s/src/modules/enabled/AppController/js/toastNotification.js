var toastConfig = {
    template:'<md-toast><div class="md-toast-content" layout="row" layout-align="center center" layout-padding>' +
    '<div flex-nogrow><md-icon md-svg-icon="{{ctrl.icon}}"></md-icon></div><div layout="column">' +
    '<div class="notif-date">{{ctrl.date|date:"MM/dd/yyyy h:mm a"}}</div>' +
    '<div class="notif-message">{{ctrl.message}}</div>' +
    '</div></div></md-toast>',
        bindToController:true,
    controllerAs:'ctrl',
    controller:function (){},
    hideDelay:6000,
        toastClass:'notif-toast',
    position:'bottom right'
};
my.showNotifToast = function (type) {
    var message, icon;
    switch(type){
        case 'appMessages':
            message = 'New messages received.';
            icon = 'icon_appMessages';
            break;
        case 'appAlerts':
            message = 'New alerts received.';
            icon = 'icon_appAlerts';
            break;
        // case 'appTaskList':
        //     message = 'New tasks received.';
        //     icon = 'icon_appTaskList';
        //     break;
        default:
            return;
    }
    toastConfig.locals = {message:message, date:new Date(),icon:icon};
    $mdToast.show(toastConfig);
};
