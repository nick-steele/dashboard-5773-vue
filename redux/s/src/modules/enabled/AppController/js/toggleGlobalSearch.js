// Unused in 5.3... Global Search is only Carebook Patient Search for now...
my.toggleGlobalSearch = function () {
    my.openGlobalSearch = !my.openGlobalSearch;
    
    if (my.openGlobalSearch) {
        my.toggleClass = 'expanded';
        my.globalSearchText = '';
        my.$window.onclick = function (event) {
            closeSearchWhenClickingElsewhere(event, my.toggleGlobalSearch);
        };
    } else {
        my.toggleClass = '';
        my.openGlobalSearch = false;
        my.$window.onclick = null;
        my.$scope.$apply();
    }
};
function closeSearchWhenClickingElsewhere(event, callbackOnClose) {
    var clickedElement = event.target;
    if (!clickedElement) return;

    var elementClasses = clickedElement.classList;
    var clickedOnSearchDrawer = elementClasses.contains('global-search-button') || elementClasses.contains('txt-global-search') ||
        (clickedElement.parentElement !== null && clickedElement.parentElement.classList.contains('global-search-button'));

    if (!clickedOnSearchDrawer) {
        callbackOnClose();
        return;
    }
}