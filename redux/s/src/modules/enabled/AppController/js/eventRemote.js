	// (private) Process local events
    function eventRemote(what, where, data) {
        //console.log('eventRemote', what, where, data);
        var send = {
            payload: data,
            event: where
        }
        socket.emit(what, send);
    }