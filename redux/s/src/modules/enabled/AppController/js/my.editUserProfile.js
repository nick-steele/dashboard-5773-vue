my.editUserProfile = function (userProfile) {
    
    my.event('remote:provider:getMetaData');
    var IS_EDITMODE = userProfile ? true : false;
    if(IS_EDITMODE && userProfile.roleId)
        userProfile.roleId = userProfile.roleId + '';
    var position = $mdPanel.newPanelPosition()
        .absolute()
        .center();

    function PanelController(mdPanelRef) {
        this.IS_EDITMODE = IS_EDITMODE;
        // this.gender = constants.GENDER;
        this.app = my;
        this.stringToColor = my.stringToColor;
        this.user = userProfile ? angular.copy(userProfile) : {};
        var panel = this;           
        this._mdPanelRef = mdPanelRef;
        this.onSelectOpen = function() 
        {
             $timeout(function() {
                $('.md-select-backdrop').css('z-index', '83');
            }, 200);
        };
        this.close = function () {
            var panelRef = this._mdPanelRef;
            panelRef && panelRef.close().then(function () {
                panelRef.destroy();
            });
        };
        this.cancel = function () {
            this.close();
        };
        this.ok = function () {
            console.log(this.user.dob );
            this.user.dob = $filter('date')(this.user.dob, 'yyyy-MM-dd'),
            console.log(this.user.dob);
            console.log(this.user);
            if (IS_EDITMODE) {
                my.event('remote:provider:update', this.user);
            }
            else {
                my.event('remote:provider:addProvider', this.user);                
            }
            this.close();
        };
    }

    var config = {
        attachTo: angular.element(document.body),
        controllerAs: 'panelCtrl',
        controller: PanelController,
        templateUrl: 'editUserProfile.tpl.html', 
        disableParentScroll: true,
        hasBackdrop: true,
        panelClass: 'tooltip-dialog',
        position: position,
        trapFocus: true,
        zIndex: 81,
        clickOutsideToClose: true,
        escapeToClose: true,
        focusOnOpen: true
    };

    $mdPanel.open(config);
}