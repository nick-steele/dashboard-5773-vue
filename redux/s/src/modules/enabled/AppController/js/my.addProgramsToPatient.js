my.addProgramsToPatient = function(data) {
    for (var i = 0; i < my.carebookSearchResult.patients.length; i++) {
        if (my.carebookSearchResult.patients[i].patientId == data.patientId)  {
            my.carebookSearchResult.patients[i].programs = data.programs;
        }
    }
};