// Built-in router... eventually break this out into a compiled strategy...
function eventServer(what, data) {
    var firstKey = function(obj) {
		for (var key in obj)
			if (obj.hasOwnProperty(key)) break;
			return key;
    };
    var firstVal = function(obj) {return obj[Object.keys(obj)[0]];};
    
    switch (what) {
        case 's':
            // Decompress and decode the state...
            var c = JSON.parse(LZString.decompressFromBase64(data));
            //console.log(c);
            // CSS...
            if ( c.c ) document.getElementById('appStyle').innerHTML = c.c;
            // HTML (angular code is recompiled with the dynamic $watch directive above)...
            if ( c.h ) {
                // TODO: Find the DOM element it belongs in... (ID)
                if ( c.x ) {
                    if (c.x.dialog) {
                        // Get a list of camel cased variables out of the dialog name...
                        //var args = c.d.replace(/([A-Z])/g, ' $1').split(" ");
                        console.log('DIALOG scope:', c.x.dialog.scope, 'name:', c.x.dialog.name);
                        // The 1st variable should be 'app', 2nd variable is the scope, the 3rd variable is the name...
                        // Draw the actual dialog...
                        my.dialog.scope = c.x.dialog.scope;
                        $mdDialog.show({
                            controller: universalDialogController,
                            template: c.h,
                            parent: angular.element(document.body),
                            targetEvent: my.dialog.sourceElement,
                            clickOutsideToClose: true,
                            fullscreen: true // TODO: Calculate this
                          })
                          .then(function() { my.dialog.callback(my.dialog.choice); });

                        //my.showDialog( { scope: args[1], data: my.dialog.data, template: c.h, sourceElement: my.dialog.sourceElement, callback: my.dialog.callback} );
                    }else {
                        console.log('Compiling', c.x.HTML );
                        var htmlId = c.x.HTML;
                        var ele = document.getElementById(htmlId);
                        ele.innerHTML = c.h;
                        var content = $compile(ele)($scope);
                    } 
                } else {
                    console.log('unknown, Auto-compiling to taskTab');
                    my.html.task = c.h;   
                }
            }
            // javascript can invoke new controllers/directives/services by calling angularize()...
            if ( c.j ) {
                eval( 'window.d = ' + c.j );
                console.log ('controlerized', c.name);
                angularize('controller', c.name, 'appContent', window.d);
            }
            break;
        case 'd':
            // Data change...
            try {
                // Try to parse it, if it's possible...
                data = JSON.parse(data);            
            }
            catch(e) {
                // otherwise, it's already JS data...
            }
            if (data.LoginResult) {
                console.log('Updated Login');
                updateLogin(data.User);
            } else {
                // Update the application model, and through it, the proper respective scope (as long as it's been loaded)...
                var key = firstKey(data);
                var secondKey = firstKey(data[key]);
                console.log('<-SVR DATA:'+ key +'['+ secondKey +']');
                $model.get(key)[secondKey] = data[key][secondKey];
            }
            break;
        case 'f':
        console.log('shouldnt get here');
            // function call...
            try {
                // Try to parse it, if it's possible...
                data = JSON.parse(data);            
            }
            // otherwise, it's already JS data...
            catch(e) {}

            // Get the application model, and through it, the proper respective scope (as long as it's been loaded)...
            var key = firstKey(data);
            var secondKey = firstKey(data[key]);
            console.log('<-SVR CALL:'+ data.scope +':' + data.func + '('+ data.args +')');
            
            // Use the magic model service to call the requested function...
            $model.get(data.scope)[data.func](data.args);
            break;
        case 'a':
            my.note(data);
            break;
        case 'patch':
            switch (data) {
                case 'reboot':
                    location.reload();
                    break;
                case 'css':
                    $('#css').replaceWith('<link id="css" rel="stylesheet" href="/dashboard/app.css?t=' + Date.now() + '"></link>');
                    break;
                case 'state':
                    $appEvent('remote:state:appTaskList');
                    break;
            }
            break;
        case 'dialog':
            break;
        case 'request':
            console.log('Request handled:', data);
            switch(data) {
                case 'login':
                    if (typeof my.login !== 'undefined') $appEvent('remote:data', {type:'login', data: my.login});
                    break;
            }
        case 'debug':
            console.log('Server DEBUG:', data);
    }
}