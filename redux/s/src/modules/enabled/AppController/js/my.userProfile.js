my.displayUserProfile = function (userProfile) {
    var position = $mdPanel.newPanelPosition()
        .absolute()
        .center();

    function PanelController(mdPanelRef) {
        this.gender = constants.GENDER;
        this.stringToColor = my.stringToColor;
        this.user = userProfile;
        var panel = this;           
        this._mdPanelRef = mdPanelRef;
        this.close = function () {
            var panelRef = this._mdPanelRef;
            panelRef && panelRef.close().then(function () {
                panelRef.destroy();
            });
        };
        this.cancel = function () {
            this.close();
        };
        this.ok = function () {
            this.close();
            my.editUserProfile(userProfile);
            this.close();
        };
    }

    var config = {
        attachTo: angular.element(document.body),
        controllerAs: 'panelCtrl',
        controller: PanelController,
        templateUrl: 'userProfile.tpl.html', 
        disableParentScroll: true,
        hasBackdrop: true,
        panelClass: 'tooltip-dialog',
        position: position,
        trapFocus: true,
        zIndex: 81,
        clickOutsideToClose: true,
        escapeToClose: true,
        focusOnOpen: true
    };

    $mdPanel.open(config);
}