/**
 * @kind ControllerFunction
 * @name app.webSockets
 *
 * @description
 * This sets up the WebSocket connect, disconnect, and * event (something came from the server)
 *
 **/

    // All messages from the server...
    socket.on('*', function (event, data) {
		// Calculate the size of the data...
		var dataSize = JSON.stringify(data).length + event.length + 7;

		// Create a new tracer for every socket event...
		var tracer = new QA.tracer(dataSize + ' bytes->' + event);
		
		// Send it to the push router...
        $appEvent('push:' + event, data, tracer);
		tracer.done();
    });
   
    // Called when a connection is established...
    socket.on('connect', function() {
		// Create a new tracer for every socket event...
		var tracer = new QA.tracer('connected');
		var sync = {};
		
        // If we're logged in with GWT...
        if ( my.login && my.login.userId ) sync.data = my.login;
		// If the token exists, send it too...
		if ( my.identity && my.identity.token ) sync.token = my.identity.token;
		
		// Send the event...
		$appEvent( 'remote:sync', sync );
        tracer.log('Synchronization sent');

		// If the identity token is present, repopulate the identity...
		//if ( my.identity && my.identity.token ) $appEvent('remote:login:sync', { token:my.identity.token });
		tracer.done();
    });
   
    // Called when a connection is lost...
    socket.on('disconnect', function() {

		// Inform the user their session expired (fixes sync issues - temporarappy removing sync for security reasons)
		$appEvent('local:session:expired');
		/*
		// Create a new tracer for every socket event...
		var tracer = new QA.tracer('disconnected');
		
        // Fill the node object with defaults...
        my.nodeFill();

		// Handle token storage on disconnect...
		if ( my.identity && my.identity.token )
			localStorage.setItem("disconnect", JSON.stringify( { time: new Date(), token: my.identity.token }) );

		my.isAuthenticated = false;
		tracer.done();
		*/
    });