// Called on boot...
function boot() {

     my.backUpData=[];
    var tagArray=[];
    my.tagEnabled=[false,false,false,false,false];



        /* size... Number of patients per page( = scroll visually)
        * Note that the logic loads two pages, previous+current pages or current+next pages
        * depending on which index of the list user is looking at.*/
    var size = 15,
        /* debounce ... Millisecond pause between user scroll and data fetch */
        debounce = 300;

    $model.get('app').globalTrace.did('mypatientlist boot');
    $model.get('app').myPatientListInit = true;



    my.listTypes = [{
        name:'My Patient Panel',
        value:'user'
    }];

    if($model.get('app').login.canViewOrgPP){
        my.listTypes.push({
            name:'My Org Patients',
            value:'org'
        });
    }
// &&app.login.canViewOrgPP===true
    my.selectedTags = [];
    my.nullTagSelected = false;
    my.listType='my:user';
    my.selectedMenuId=null;

    my.listTypeNoOption = "My Patient Panel";

    $appEvent('local:myPatientList:open');
    $appEvent('remote:myPatientList:tags');


    function unselectAllTags(tags) {
        for(var i=0;i<tags.length;i++){
            tags[i].selected=false;
        }
    };
    my.clearAllTagFilter = function () {
      if(my.tags) unselectAllTags(my.tags);
      my.selectedTags=[];
      my.nullTagSelected = false;
      reloadListSearch();
    };

    my.toggleSelectedTag = function (tag) {
        if(tag.selected){
            my.nullTagSelected=false;
        }
        reloadListSearch();
    };
    my.toggleNoTags = function () {
        if(my.nullTagSelected){
            if(my.tags) unselectAllTags(my.tags);
            my.selectedTags=[];
        }
        reloadListSearch();
    };

    my.onListTypeChange = function () {
        reloadListSearch();
    };



    function reloadListSearch(){
        my.dynamicPatientList.reload();
    }


    /* Private function which parses selected tags */
    function getSelectedTagArray() {
        var tagArr = [];
        if(my.nullTagSelected) tagArr= ['null'];
        else if(my.tags){
            for(var i=0;i<my.tags.length;i++){
                if(my.tags[i].selected)
                    tagArr.push(my.tags[i].tagId);
            }
        }
        my.tagFilterActive = tagArr.length>0;
        return tagArr;
    }

    my.ppTimestamp=null;
    function getPayload(page,size,pCount,getCount){
        var typeArr = my.listType.split(':');
        return {
            page:page,
            size:size,
            mode:typeArr[0],
            type:typeArr[1],
            tags: getSelectedTagArray(),
            search:my.searchString,
            timestamp: my.ppTimestamp,
            pCount:pCount||1,
            getCount:!!getCount
        };
    }
var searchTimeoutPromise;
    my.keywordSearch = function(){
        if(searchTimeoutPromise){
            $timeout.cancel(searchTimeoutPromise);
        }
        reloadListSearch();
    };
    my.resetSearch = function () {
        my.searchString = null;
        if(searchTimeoutPromise){
            $timeout.cancel(searchTimeoutPromise);
        }
        reloadListSearch();
    };
    my.onKeywordChange = function(){

        var delay = 2000;
        if(!my.searchString||my.searchString.length>=3){
            delay = 1000;
        }
        if(searchTimeoutPromise){
            $timeout.cancel(searchTimeoutPromise);
        }
        searchTimeoutPromise = $timeout(function () {
                    reloadListSearch();
                    searchTimeoutPromise=null;
        },delay);

    };


    my.getPage = function (page) {
        var payload = getPayload(page-1,my.pageSize);
        $appEvent('remote:myPatientList:get',payload);
    };


    /* === DynamicPatientList implementation for virtual scrolling list=== */

    var DynamicPatientList = function () {

        /** @const {number} Number of items to fetch per request. */
        this.PAGE_SIZE = size;

        /**
         * @type {!Object<?Array>} Data pages, keyed by page number (0-index).
         */
        this.loadedPages = {};
        /** @type {number} Total number of items. */
        this.numItems = 0;

        my.notFound=false;

        /* Generate new timestamp on every search criteria update (tag, type, keyword)
        * so delayed response data does not get rendered after new search criteria has been created. */
        my.ppTimestamp = (new Date()).getTime();
        this.timestamp = my.ppTimestamp;

        /* Get first and second page with total count */
        this.fetchPage_(0,1,true);

    };

    DynamicPatientList.prototype.reload = function () {

        //Duplicated code with constructor... clean up later
        this.loadedPages = {};
        this.numItems = 0;
        my.notFound=false;

        my.ppTimestamp = (new Date()).getTime();
        this.timestamp = my.ppTimestamp;

        // $appEvent('remote:myPatientList:count', getPayload());
        this.fetchPage_(0,1,true);
    };

    /*Required function by Virtual Repeat onDemand*/
    DynamicPatientList.prototype.getItemAtIndex = function(index) {
        var pageNumber = Math.floor(index / this.PAGE_SIZE);
        var page = this.loadedPages[pageNumber];

        if (page) {
            //If the page is already loaded, return
            return page[index % this.PAGE_SIZE];
        } else if (!page && page !== null) {

            /* Find if user is looking at beginning/ending of a page */
            var isEarly = (this.PAGE_SIZE/2)>(index % this.PAGE_SIZE);
            var extraPageNumber = pageNumber + (isEarly?-1:+1);

            /* If computed adjacent page exists AND not loaded, then fetch it too.*/
            if(extraPageNumber>0){
                var extraPage = this.loadedPages[extraPageNumber];
                if(!extraPage&&extraPage!==null){
                    this.requestPage(pageNumber,extraPageNumber,index);
                    return;
                }
            }

            /* Fetch single page */
            this.requestPage(pageNumber, null, index);
        }

    };

    /*Required function by Virtual Repeat onDemand*/
    DynamicPatientList.prototype.getLength = function() {
        return this.numItems;
    };

    /* Cushion function before actually fetching page.
     * Cancel unnecessary requests until user stops scrolling */
    DynamicPatientList.prototype.requestPage = function(pageNumber, extraPage, index) {

        this.requestedPage = pageNumber;
        this.extraRequestedPage = extraPage;
        this.requestedIndex = index;

        var _this = this;

        //If there is queued request, cancel
        if(this.queuedRequest){
            var cancelled = $timeout.cancel(this.queuedRequest);
            // $log.debug(cancelled?"The request was cancelled.":"The request was already processed.");
            this.queuedRequest=null;
        }

        //Queue new request
        this.queuedRequest = $timeout(function (_this) {
                _this.fetchPage_(_this.requestedPage,_this.extraRequestedPage, false, _this.requestedIndex);
                _this.requestedPage = null;
                _this.extraRequestedPage=null;
        }, debounce, true, _this);

    };

    /* Actual function that fetches pages.
    * If getCount is true, we request total count. */
    DynamicPatientList.prototype.fetchPage_ = function(pageNumber,extraNumber,getCount,index) {

        // $log.debug("Fetching: ",pageNumber, extraNumber, "Requested index:", index);

        // Set the page to null so we know it is already being fetched.
        this.loadedPages[pageNumber] = null;

        var payload;
        // If extra page(adjacent page) is requested and the extra page data is not loaded yet, request two pages
        if(extraNumber&&!this.loadedPages[extraNumber]&&this.loadedPages[extraNumber]!==null){
            // Set the page to null so we know it is already being fetched.
            this.loadedPages[extraNumber] = null;
            payload = getPayload(extraNumber<pageNumber?extraNumber:pageNumber, this.PAGE_SIZE, 2, getCount);
        }else{
            payload = getPayload(pageNumber, this.PAGE_SIZE, 1, getCount);
        }

        // Fetch page
        $appEvent('remote:myPatientList:get',payload);

    };

    DynamicPatientList.prototype.processTotalCount = function(totalCount) {
        $log.debug(totalCount);
        this.numItems = totalCount;
    };
    DynamicPatientList.prototype.processNewPage = function (pageNumber,list) {
        $log.debug("Fetched: "+pageNumber);
        this.loadedPages[pageNumber] = list;
         my.backUpData.push(list);

    };



    /* ===================================================================== */

    //
    // // Triggered from server
    // my.processInitList = function () {
    //     my.dynamicPatientList = new DynamicPatientList();
    // };

    // Triggered from server
    my.processNewPage = function (data) {


        if(!data){
            $log.warn("ERROR: processNewPage received null data.");
            return;
        }
        if(data.timestamp!=my.ppTimestamp){
            $log.debug("This data is outdated.",data, data.timestamp);
            return;
        }
        if(data.notFound){
            $log.debug("Result not found");
            my.notFound = true;
            return;
        }

        var list = data.list;
        for(var i = 0 ; i<data.pCount;i++){
            if(list.length<=size){
                // $log.debug(data.page+i,list);
                my.dynamicPatientList.processNewPage(data.page+i,list);
                return;
            }else{
                var _list = list.splice(0,size);
                // $log.debug(data.page+i,_list);
                my.dynamicPatientList.processNewPage(data.page+i,_list);
            }
        }
    };

    // Triggered from server
    my.processTotalCount = function (data) {
        if(!data){
            $log.debug("processTotalCount received null data.");
            my.notFound = true;
        }else{
            my.notFound=false;
            // my.totalCount = data.count;
            my.dynamicPatientList.processTotalCount(data.count);
        }
            //my.dynamicPatientList.processTotalCount(data.count);
    };

    // Triggered from server
    //need to refresh Alert and Encounter chart after Remove Patient
    my.reloadAfterRemoveOrAdd = function () {
        my.reload();
        $model.get('home').reloadPatientPanelView();
    };



    my.reload = function(){
        // my.initialized = false;
        // $appEvent('remote:myPatientList:init', {type:my.listType});
        // my.searchString = null;
        my.selected=2;
        reloadListSearch();
    };

    $model.store('reload',my.reload);
    my.reloadApp = function () {
        $appEvent('local:state:patientPanel');
    };
    my.onError = function (data) {
        $log.warn("myPatientPanel",data);
        my.isError = true;
    };



   //filter The tag Menu  and hide  tags
     my.filterTagsMenu=function(person,availableTags){
        angular.forEach(person,function(value){
         angular.forEach(availableTags,function(tagValue){
            if(tagValue.tagAbbr===value.tagAbbr){
               value.tagEnabled=tagValue.tagEnabled;
            }

         })

        })
}



    //init
    my.dynamicPatientList = new DynamicPatientList();


}
