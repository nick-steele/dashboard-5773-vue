var ppMenuCtrl = ['$scope','mdPanelRef','$mdPanel','$window','constants',
    function($scope,mdPanelRef,$mdPanel,$window, constants){
        var my = this;
        my.constants = constants;
        my._mdPanelRef = mdPanelRef;
        my.close = function(){
            var panelRef = my._mdPanelRef;
            panelRef && panelRef.close().then(function() {
                panelRef.destroy();
            });

        };

        //Copied from appHome
        function displayConfirmation(title, msg, success, cancel){
            var position = $mdPanel.newPanelPosition()
                .absolute()
                .center();

            var confirmCtrl = function (mdPanelRef) {
                var panel = this;
                panel.msg = msg;
                panel.title = title;
                panel._mdPanelRef = mdPanelRef;
                this.close = function() {
                    var panelRef = panel._mdPanelRef;
                    panelRef && panelRef.close().then(function () {
                        panelRef.destroy();
                    });
                };
                this.ok = function () {
                    success();
                    var panelRef = panel._mdPanelRef;
                    panelRef && panelRef.close().then(function () {
                        panelRef.destroy();
                    });

                };
                this.no = function () {
                    cancel();
                    var panelRef = panel._mdPanelRef;
                    panelRef && panelRef.close().then(function () {
                        panelRef.destroy();
                    });
                }
            };

            var config = {
                attachTo: angular.element($element[0]),
                controllerAs: 'panelCtrl',
                controller: confirmCtrl,
                template: '<div id="confirmPopup" class="panel-dialog">' +
                '   <md-toolbar class="panel-title">' +
                '       <div class="md-toolbar-tools">' +
                '           <h2>'+ title + '</h2>' +
                '           <span flex></span>' +
                '           <md-button class="md-icon-button" ng-click="panelCtrl.close()" aria-label="cancel"><md-icon md-svg-icon="close"></md-icon></md-button>' +
                '       </div>' +
                '   </md-toolbar>' +
                '   <div class="panel-content">' +
                '       <p>' + msg + '</p>' +
                '   </div>' +
                '   <div class="panel-action">' +
                '       <md-button id="btnConfirmOk" ng-click="panelCtrl.ok()">Yes</md-button>' +
                '       <md-button id="btnConfirmNo" ng-click="panelCtrl.no()" md-autofocus>No</md-button>' +
                '   </div>' +
                '</div>',
                disableParentScroll: true,
                hasBackdrop: true,
                panelClass: 'mpl-confirm-dialog',
                position: position,
                trapFocus: true,
                zIndex: 250,
                clickOutsideToClose: true,
                escapeToClose: true,
                focusOnOpen: true
            };

            $mdPanel.open(config);
        }


        my.openCarebook = function (patientId, panelRef) {
            $appEvent('local:patientDrillDown:openCarebook', patientId);
            my.close();

        };
        my.openEdit = function (patientId) {
            $appEvent('local:patientDrillDown:edit',patientId);
            my.close();
        };
        my.openCareplan = function (patientId) {
            $appEvent('local:patientDrillDown:openCarePlanDirect', patientId);
            my.close();
        };

        
        my.openWizard = function(patientId, step){
            my.close();
            $appEvent('local:appPatientEnrollment:openWizard',{'patientId':patientId, 'step':step});
        };

        //TODO: copied from appHome
        my.removeFromList  = function (patient) {
            my.close();
            var title = 'Confirmation';
            var defaultMsg = 'Remove '+ patient.lastname + ', ' + patient.firstname + ' from My Patient Panel?';
            var msg = my.myPatientList.getMessageBundle(defaultMsg, "popup.confirmation.remove.msg", function (configMsg) {
                return configMsg.replace('[0]', patient.lastName + ', ').replace('[1]', patient.firstName);
            });
            var success = function () {
                $appEvent('remote:myPatientList:remove',{patientId: patient.patientId});
            };
            displayConfirmation(title, msg, success, function(){});

        };

        //Update position on window resize
        function onWinResize() {
            var position = $mdPanel.newPanelPosition()
                .relativeTo('#PPMenuBtn_'+my.index)
                .addPanelPosition($mdPanel.xPosition.ALIGN_END, $mdPanel.yPosition.ALIGN_TOPS);
            my._mdPanelRef.updatePosition(position);
        }
        angular.element($window)
            .on('resize',onWinResize);

        //Clean up watcher
        $scope.$on('$destroy', function () {
            angular.element($window)
                .off('resize',onWinResize);
        });

}];

var ppMenuConfig = {
    attachTo: angular.element(document.body),
    controller:ppMenuCtrl,
    controllerAs: 'vm',
    templateUrl: 'ppMenu.tpl.html',
    panelClass:'mpl-item-menu',
    clickOutsideToClose: true,
    escapeToClose: true,
    focusOnOpen: false,
    zIndex: 60,
    onRemoving: function () {
        my.selectedMenuId=null;
    }
};

my.clickMenuOpen = function (ev, patient, index) {

    my.selectedMenuId = patient.patientId;

    var position = $mdPanel.newPanelPosition()
        .relativeTo('#PPMenuBtn_'+index)
        .addPanelPosition($mdPanel.xPosition.ALIGN_END, $mdPanel.yPosition.ALIGN_TOPS);

    ppMenuConfig.openFrom = ev;
    ppMenuConfig.position = position;
    ppMenuConfig.locals = {'patient': patient,
        'index':index,
        'myPatientList':my};

    $mdPanel.open(ppMenuConfig);

};

//Copied from my.popupMenu.js


my.getMessageBundle = function (defaultMsg, msgKey, replaceFunc) {
    if(my.messageBundle && (my.messageBundle[msgKey])){
        return replaceFunc(my.messageBundle[msgKey]);
    } else {
        $log.debug('Cannot get messageBundle with key: '+ msgKey);
        return defaultMsg;
    }

};
