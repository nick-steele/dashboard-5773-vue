var sortMenuConfig = {
    attachTo: angular.element(document.body),
    controller:['$scope','mdPanelRef','$mdPanel','$window','$filter',
        function($scope,mdPanelRef,$mdPanel,$window,$filter){
            var my = this;
            my.myPatientList.sortOptions=[{order:'Ascending',sortBy:'lastName'},{order:'Descending',sortBy:'-lastName'},{order:'Most Recent',sortBy:'recentData'}];
            my._mdPanelRef = mdPanelRef;
            my.close = function(){
                var panelRef = my._mdPanelRef;
                panelRef && panelRef.close().then(function() {
                    panelRef.destroy();
                });
            };
   
            //Update position on window resize
            function onWinResize() {
                var position = $mdPanel.newPanelPosition()
                    .relativeTo('#MyPatientListSortMenuBtn')
                    .addPanelPosition($mdPanel.xPosition.ALIGN_END, $mdPanel.yPosition.BELOW);
                my._mdPanelRef.updatePosition(position);
            }
            angular.element($window)
                .on('resize',onWinResize);

            //Clean up watcher  
            $scope.$on('$destroy', function () {
                angular.element($window)
                    .off('resize',onWinResize);
            });
        
            //sort the patient panel
            my.myPatientList.sortPatientPanel=function(originalData,sortOption,index){
                    if(sortOption==='recentData'){
                        angular.extend(my.myPatientList.dynamicPatientList.loadedPages,my.myPatientList.backUpData)

                }else{

                    angular.forEach(originalData.loadedPages,function(value,key){
                    my.myPatientList.dynamicPatientList.loadedPages[key] = $filter('orderBy')(my.myPatientList.dynamicPatientList.loadedPages[key], sortOption);

                                    })

                }
                  
                 my.myPatientList.selected=parseInt(index);
            }
        }],

    controllerAs: 'sortMenuCtrl',
    templateUrl: 'mplSortMenu.tpl.html',
    panelClass: 'demo-menu-example',
    clickOutsideToClose: true,
    escapeToClose: true,
    focusOnOpen: false,
    zIndex: 60
};


my.selected=2;
 
my.showSortMenu=function(ev){
var position = $mdPanel.newPanelPosition()
        .relativeTo('#MyPatientListSortMenuBtn')
        .addPanelPosition($mdPanel.xPosition.ALIGN_END, $mdPanel.yPosition.BELOW);

    sortMenuConfig.openFrom = ev;
    sortMenuConfig.position = position;
    sortMenuConfig.locals = {'myPatientList': my};

    $mdPanel.open(sortMenuConfig);

}
