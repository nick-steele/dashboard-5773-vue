
var tagMenuConfig = {
    attachTo: angular.element(document.body),
    controller:['$scope','mdPanelRef','$mdPanel','$window',
        function($scope,mdPanelRef,$mdPanel,$window){
            var my = this;
            my._mdPanelRef = mdPanelRef;
            my.close = function(){
                var panelRef = my._mdPanelRef;
                panelRef && panelRef.close().then(function() {
                    panelRef.destroy();
                });
            };

            //Update position on window resize
            function onWinResize() {
                var position = $mdPanel.newPanelPosition()
                    .relativeTo('#MyPatientListTagMenuBtn')
                    .addPanelPosition($mdPanel.xPosition.ALIGN_END, $mdPanel.yPosition.BELOW);
                my._mdPanelRef.updatePosition(position);
            }
            angular.element($window)
                .on('resize',onWinResize);

            //Clean up watcher
            $scope.$on('$destroy', function () {
                angular.element($window)
                    .off('resize',onWinResize);
            });

        }],
    controllerAs: 'tagMenuCtrl',
    templateUrl: 'mplTagMenu.tpl.html',
    panelClass: 'demo-menu-example',
    clickOutsideToClose: true,
    escapeToClose: true,
    focusOnOpen: false,
    zIndex: 60
};

my.showTagMenu = function (ev) {

    var position = $mdPanel.newPanelPosition()
        .relativeTo('#MyPatientListTagMenuBtn')
        .addPanelPosition($mdPanel.xPosition.ALIGN_END, $mdPanel.yPosition.BELOW);

    tagMenuConfig.openFrom = ev;
    tagMenuConfig.position = position;
    tagMenuConfig.locals = {'myPatientList': my};

    $mdPanel.open(tagMenuConfig);

};