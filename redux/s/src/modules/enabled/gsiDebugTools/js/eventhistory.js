(function(window){

    function EventHistory(){
        this.history = [];
        this.enabled = false;

        EventHistory.prototype.get = function () {
            return this.history;
        };

        EventHistory.prototype.add = function (event,data) {
            this.history.push({event:event,data:data, time: new Date()});
        };

        EventHistory.prototype.clear = function () {
            this.history = [];
        };

        EventHistory.prototype.enable = function () {
            this.enabled=true;
        };

        EventHistory.prototype.disable = function () {
            this.enabled=false;
        };

        EventHistory.prototype.isEnabled = function () {
            return this.enabled;
        };

        EventHistory.prototype.find = function (event) {
            var _history = this.history;
            for(var i=0;i<_history.length;i++){
                if(_history[i].event === event) return _history[i];
            }
            return null;
        };

        EventHistory.prototype.findAll = function (event) {
            var _history = this.history;
            var temp = [];
            for(var i=0;i<_history.length;i++){
                if(_history[i].event === event) temp.push(_history[i]);
            }
            return temp;
        };

    }

    window.QA.eventHistory = new EventHistory();

})(window);
