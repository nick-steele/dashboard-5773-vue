/*
	trace by Nick Steele
	Copyright (C) 2016 GSI Health, LLC.

	Simple yet precise logging factory that gets right to the point! Trace through an entire
	application in a single line on the console!
	
	Time specific things you're interested in, even through promises, socket events, a bunch of other traces, you got an idea? It'll work.
	
	Launch as many traces as you like.
	
	Flush them whenever you want and the console will then show when the trace started, along with all timing information it contains.

	USAGE:
	
		Want to take a peek at what's currently being traced? That's easy! just call the active method on any trace like so:
		
			currentTraces = myTrace.active();
		
		Access details about a specific active trace like so:
		
			myTraceDetails = myTrace.active()['myTraceName'];
			
		Create a trace like so:
		
			myTrace = new $trace('myTraceId');
		
		Add a trace event like so (it will log the time since the last event in the trace):
		
			myTrace.did('theThingIJustDid');
		
		Complete a trace event like so (it will flush the trace, take it out of the active trace registry, and display timing informaiton):
		
			myTrace.done();
		
		...or...
		
			myTrace.success();
			
		...or...
		
			myTrace.fail();
		
*/

window.QA.traces = {};


// Trace factory...
window.QA.tracer = function(key) {
	if (typeof key === 'undefined') key = QA.trace.sig();
	
	// Record our own copy of the key...
	var key = key;
	
	// Use the trace service to add it to the registry...
	QA.trace.add(key);
	
	// ### Factory methods...	
	this.log		= function(text)	{ QA.trace.log(key, text); }
	this.done		= function()		{ QA.log.trace(QA.trace.flush(key)); }	
	this.success	= function()		{ QA.log.success(QA.trace.flush(key)); }
	this.fail		= function()		{ QA.log.fail(QA.trace.flush(key)); }
}


// Trace Service...
window.QA.trace = {
	// Adds a trace...
	add: function( key ) {
		QA.traces[key] = {
			trace	: '',				// The trace name
			start	: new Date(),		// The time the trace was started
			last	: new Date(),		// The last time traced
			times	: []				// The times for each trace
		};
	},
	
	// Continues a trace...
	log: function( key, text ) {
		try {
			var trace = QA.traces[key]
		} catch(e) {
			debug.error("$trace: "+ key +": Tried to add to it but it doesn't exist!");
			return;
		}
		
		// Record the time since we last recieved text...
		var when = new Date();
		// Record the time passed...
		var ms = when - trace.last;
		// Store now as last...
		trace.last = when;
		// Record the time since we were last called...
		trace.times.push({text:text, when: when});
		// Record the text passed and the time since the last call to the current trace...
		trace.trace += '.' + text + '(' + ms + 'ms)';

	},
	
	// Flushes a trace (ends it)...
	flush: function( key ) {
		try {
			// Record the time since we last recieved text...
			total = new Date() - QA.traces[key].start;
			var text = '[' + key + ']' + QA.traces[key].trace + ' ' + total + 'ms';
			delete QA.traces[key];
			return text;
		} catch(e) {
			debug.error("$trace: "+ key +": Tried to end it but it doesn't exist!");
		}
	},
	
	// Produces a random signature...
	sig: function() {
		var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		var length = 6;
		var result = '';
		for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
		return result;
	},
	
	// Way to complete the function from the service itself...
	finish: function( key ) {
		debug.trace(QA.trace.flush(key));
	}
}

// Start the boot calculation...
QA.trace.add('system.boot');