// Logs something in QA (to the console and an array)...

QA.stamp = function() {
		// Build a nice timestamp...
		var pad = function(num, size){ return ('000000000' + num).substr(-size); }
		var now = new Date();
		return pad(now.getHours(), 2) + ':' + pad(now.getMinutes(), 2) + ':' + pad(now.getSeconds(), 2) + '.' + pad(now.getMilliseconds(), 3) + ' ';
}

QA.record = function(text) {
	QA.log.logs.push(text);	
}


QA.log = {
	log: function(text) {
		console.log(QA.stamp() + text);
		QA.record(QA.stamp() + '|L|' + text);
	},
	info: function(text) {
		console.info(QA.stamp() + text);
		QA.record(QA.stamp() + '|I|' + text);
	},
	warn: function(text) {
		console.warn(QA.stamp() + 'WARNING: ' + text);
		QA.record(QA.stamp() + '|W|' + text);
	},
	error: function(text) {
		console.error(QA.stamp() + 'ERROR: ' + text);
		QA.record(QA.stamp() + '|E|' + text);
	},
	success: function(text) {
		console.info(QA.stamp() + 'SUCCESS: ' + text);
		QA.record(QA.stamp() + '|S|' + text);
	},
	fail: function(text) {
		console.error(QA.stamp() + 'FAIL: ' + text);
		QA.record(QA.stamp() + '|F|' + text);
	},
	trace: function(text) {
		//console.info('%c ' + QA.stamp() + text, 'background: #eee; color: #f0f');
		console.info(QA.stamp() + text);
		QA.record(QA.stamp() + '|T|' + text);
	},
	// Holds a copy of all log entries...
	logs: []
}

// Alias...
window.debug = QA.log;

// Timestamp for first code execution...
debug.info('Client payload v' + client.vars.version);
