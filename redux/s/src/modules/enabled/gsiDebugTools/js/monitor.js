// Wrapper for New Relic Monitor. Does not do anything if NR is not loaded. Currently NR is loaded at Glassfish level.
QA.monitor = {
    login :function(loginObj){
        if(!window.newrelic||!loginObj)return;
        newrelic.setCustomAttribute("userId", loginObj.userId);
        newrelic.setCustomAttribute("communityId", loginObj.communityId);
    },
    route :function(routeName){
        if(!window.newrelic)return;
        newrelic.setCurrentRouteName("/"+routeName);
    },
    event :function(eventName){
        if(!window.newrelic)return;
        newrelic.interaction().setName(eventName).save().end();
    },
    error: function(errorName, message){
        if(!window.newrelic)return;
        newrelic.noticeError({name: errorName, message: message});
    }
};

