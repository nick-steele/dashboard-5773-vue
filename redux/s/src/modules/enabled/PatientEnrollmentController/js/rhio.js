
// Initialize
my.patientRhio = false;

/**
 * Reload rhio data from server.
 */
my.stepScopes.rhio.reloadRhio = function () {
    GWT.activity();
    my.getRhioError = false;
    $appEvent('remote:patient:getRhio', my.patientId);
};

/**
 * Clear Rhio data on permit sharing off. Store backup of current state
 */
my.stepScopes.rhio.resetRhio = function () {
    GWT.activity();
    //Store current state to revert it back when sharing is turned on again
    my.stepScopes.rhio.rhioEpBackup = angular.copy(my.patientRhio);
    for (var i = 0; i < my.patientRhio.endpoints.length; i++) {
        my.patientRhio.endpoints[i].enabled = false;
    }
      
    for (var i = 0; i < my.patientRhio.rhioFamilies.length; i++) {
        my.patientRhio.rhioFamilies[i].selected = null;
    }
};

/**
 * Revert RHIO state with backup. Called on permit sharing on
 */
my.stepScopes.rhio.revertRhio = function () {
    GWT.activity();
    if (my.stepScopes.rhio.rhioEpBackup) {
        my.patientRhio = my.stepScopes.rhio.rhioEpBackup;
        // my.patientRhio.rhioFamilies = my.stepScopes.rhio.rhioEpBackup;
    }
    my.patientRhio.sharing = true;
};

/**
 * Request RHIO save to the server
 */
my.stepScopes.rhio.saveRhio = function () {
    GWT.activity();
    var data = angular.copy(my.patientRhio);
    if (!data.sharing) { // If sharing is off, delete unnecessary data
        delete data.endpoints;
        delete data.rhioFamilies;
    } else if (data.rhioFamilies) {
        // If data rhio has family, update enabled flag
        // TODO this process is redundant. Need to be revise with service
        for (var i = 0; i < data.rhioFamilies.length; i++) {
            var rf = data.rhioFamilies[i];
            rf.enabled = !!(rf.selected);
        }
    }
    my.stepScopes.rhio.status = $gsiSaveButton.IN_PROGRESS;
    $appEvent('remote:patient:saveRhio', {patientId: my.patientId, data: data});
};

/**
 * Called on rhio save success
 * @param data -- save response sent from server
 */
my.onSaveRhioSuccess = function (data) {
    GWT.activity();
    my.stepScopes.rhio.status = $gsiSaveButton.SUCCESS;
     delete my.stepScopes.rhio.rhioEpBackup;
    my.rhioForm.$setPristine();
    my.originalRhioSharing = my.patientRhio.sharing;
};

/**
 * Called on get rhio data success
 * @param data -- RHIO data sent from server
 */
my.onGetRhioSuccess = function (data) {
    my.patientRhio = data;
    if (data)
        my.originalRhioSharing = data.sharing;
    my.isLoadedRhio = true;
};

/**
 * Force setting rhio form $dirty
 */
my.stepScopes.rhio.setDirty = function () {
    my.rhioForm.$setDirty();
};