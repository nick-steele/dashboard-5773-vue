my.onAddUpdateError = function (err) {
    $mdDialog.show(
        $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title('Error')
            .textContent(err.errorDetail)
            .multiple(true)
            .ok('OK')
    );
    my.isUpdating = false;
};
my.onAddUpdateError = function (err) {
    $mdDialog.show(
        $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title('Error')
            .textContent(err.errorDetail)
            .multiple(true)
            .ok('OK')
    );
    my.isUpdating = false;
};