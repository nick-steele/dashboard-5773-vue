//when update Provider Detail successfully
my.updateUserCallback = function(data) {
    my.isUpdating = false;
    if (my.searchedResult[data.index].userId === data.userInfo.userId)
    my.searchedResult[data.index] = data.userInfo;
    if (my.myCareData.listOfMember[data.index].userId === data.userInfo.userId)
        my.myCareData.listOfMember[data.index] = data.userInfo;

    //back to User Profile screen
    my.displayUserProfile(data.index, data.userInfo);
};

//when create Provider successfully
my.addProviderCallback = function(data) {
    my.isUpdating = false;

    //back to search Care Team
    my.backToSearchView();
};