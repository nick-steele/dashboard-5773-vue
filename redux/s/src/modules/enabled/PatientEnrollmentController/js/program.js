
my.stepScopes.program.columnMap = {};

//Convert program field configurations into map
for(var pci=0;pci< my.app.settings.apps.patient.enrollment.program.fields.length ;pci++){
    var columnConfig = my.app.settings.apps.patient.enrollment.program.fields[pci];
    my.stepScopes.program.columnMap[ columnConfig.id ] = columnConfig;
    if(columnConfig.id==='consentStatus') {
        my.stepScopes.program.columnMap[ columnConfig.id ].cssClass = 'consent-cell';
        my.stepScopes.program.columnMap[ columnConfig.id ].flex = 'nogrow';
        my.stepScopes.program.columnMap[ columnConfig.id ].center = true;
        my.stepScopes.program.columnMap[ columnConfig.id ].sortDisabled = true;
    }
}


my.onGetPatientPrograms = function(data){
  my.isLoadedProgram = true;
  my.patientPrograms = data;
};

my.stepScopes.program.statusEffectiveDateEditable = GWT.getStatusEffectiveDateEditable();

my.stepScopes.program.reload = function () {
    GWT.activity();
    my.getPatientProgramsError = false;
    $appEvent('remote:patient:getPatientPrograms', my.patientId);
    $appEvent('remote:patient:getProgramList', my.patientId);
};


my.stepScopes.program.save = function () {

    GWT.activity();
    my.stepScopes.program.errorMessage = null;
    $scope.$broadcast('patientProgram:save:attempt');
    
};


my.onSavePatientProgramsSuccess= function () {
    GWT.activity();
    my.stepScopes.program.status = $gsiSaveButton.SUCCESS;
    $scope.$broadcast('patientProgram:save:success');
    my.refreshCarebookIFrame();
};

my.onDeletePatientProgramSuccess = function (data) {

    $scope.$broadcast('patientProgram:delete:success', data);
    my.refreshCarebookIFrame();
};

my.onDeletePatientProgramEpisodeError = function (data) {

    $mdDialog.show(
        $mdDialog.alert()
            .clickOutsideToClose(true)
            .title('Error')
            .textContent(data.message || my.app.settings.apps.patient.enrollment.program.text.deleteProgramErrorDefault)
            .ariaLabel('Error')
            .multiple(true)
            .ok('OK')
    );
};

my.refreshCarebookIFrame = function () {
    var carebookIFrame = document.getElementById('carebook-iframe');
    if (carebookIFrame) {
        var iFrameBody;
        if ( carebookIFrame.contentWindow ) { //IE
            iFrameBody = carebookIFrame.contentWindow;//iFrame.contentWindow.document.getElementsByTagName('body')[0];
        }
        else if ( carebookIFrame.contentDocument ) { // FF
            iFrameBody = carebookIFrame;//iFrame.contentDocument.getElementsByTagName('body')[0];
        }
        if(typeof iFrameBody.refreshProgList === 'function') {
            iFrameBody.refreshProgList('refreshProgram');
        }
    }
};

my.stepScopes.program.toggleHistoryView = function(open){
    if(open){
        my.stepScopes.program.showHistory = true;
    }else{
        my.stepScopes.program.showHistory = false;
        my.stepScopes.program.historyData = null;
    }
}