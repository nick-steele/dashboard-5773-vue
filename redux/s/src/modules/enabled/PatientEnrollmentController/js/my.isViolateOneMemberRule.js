my.atLeastOneMemberBelongRequiredRoles = function(careteam) {
    if (!my.requiredroles || my.requiredroles.length === 0)
        return true;
    var requiredRoleIds = my.requiredroles.map(function(role) {
        return role.id;
     });
    for (var i=0; i<careteam.length; i++) {
        if (careteam[i].userType !== 'EXTERNAL' && careteam[i].userStatus !== 'INACTIVE') {
            if (requiredRoleIds.indexOf(careteam[i].roleId) > -1)
                return true;
        }  
    }
    return false;
};
my.isViolateOneMemberRule = function(careteam, title, message) {
    var ONE_TEAM_MEMBER = my.oneMemberInCareTeam;
    if (ONE_TEAM_MEMBER === true) {
        var haveRequiredRole = my.atLeastOneMemberBelongRequiredRoles(careteam);
        if (!careteam || careteam.length===0 || !haveRequiredRole) {
            if (!my.isOpenViolatePopup) {
                var ul = '';
                if (!message) {
                    if (!my.requiredroles || my.requiredroles.length === 0) {
                        message = my.careConfig.oneMemberRuleMsg1;
                    }
                    else {
                        message = my.careConfig.oneMemberRuleMsg3;
                        ul = '<ul>';
                        my.requiredroles.forEach(function(element) {
                            ul += '<li>' + element.name + '</li>'
                        });
                        ul += '</ul>';
                    }
                }
                message = message.replace(/\{0\}/g, ul);
                var content = '<div>' + message + '</div>';
                $mdDialog.show(
                    $mdDialog.alert()
                        .parent(angular.element(document.querySelector('#popupContainer')))
                        .clickOutsideToClose(true)
                        .title(title)
                        .htmlContent(content)
                        .multiple(true)
                        .ok('OK')
                ).finally(function() {
                    console.log('close violate popup');
                    my.isOpenViolatePopup = false;
                });
                my.isOpenViolatePopup = true;
            }
            return true;
        }
    }
    return false;
};