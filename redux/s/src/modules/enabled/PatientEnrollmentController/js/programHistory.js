

var loadHistoryBy = 'year';
var programHistoryDefaultMinDate = getDateLimitFromConfig('defaultMinDate');
var historyDataTimestamp;
var offsetMonth = my.app.settings.apps.patient.enrollment.program.history.options.preloadOffsetMonth;

/**
 * Initialize history widget and request data
 */
my.stepScopes.program.initHistory = function () {
    my.stepScopes.program.historyHasError = false;
    my.stepScopes.program.currentHistoryMinDate = moment(programHistoryDefaultMinDate).subtract(offsetMonth, 'month').startOf(loadHistoryBy);
    my.stepScopes.program.historyData = null;
    my.getPatientProgramHistoryError = false;
    loadHistoryData(my.stepScopes.program.currentHistoryMinDate);
};

/**
 * Event configuration map passed to visTimeline component
 */
my.stepScopes.program.historyEvents = {
    'rangechanged': function(event) {
        GWT.activity();
        if(my.stepScopes.program.currentHistoryMinDate.isSameOrBefore(my.stepScopes.program.historyOptions.min)) return;
        var offset = moment(event.start).subtract(offsetMonth, 'month').startOf(loadHistoryBy);
        if (!offset.isBefore(my.stepScopes.program.currentHistoryMinDate)) return;
        my.stepScopes.program.currentHistoryMinDate =  offset;
        loadHistoryData(my.stepScopes.program.currentHistoryMinDate);
    }
};

/**
 * private function to request data from remote.
 */
function loadHistoryData(start) {
    //Timestamp to track the latest request
    historyDataTimestamp = (new Date()).getTime();

    var _start = start.isBefore(my.stepScopes.program.historyOptions.min)? my.stepScopes.program.historyOptions.min : start;
    $appEvent('remote:patient:getPatientProgramHistory', {
        timestamp : historyDataTimestamp,
        patientId: my.patientId,
        start: _start.format("YYYY-MM-DD")
    });
}

/**
 *  Config options required by visTimeline
 */
my.stepScopes.program.historyOptions = {

    //Where to show time axis
    orientation: {
        axis:'both'
    },

    //Default visible time range
    start:programHistoryDefaultMinDate,
    end:getDateLimitFromConfig('defaultMaxDate'),

    //Maximum visible time range
    min:getDateLimitFromConfig('absMinDate'),
    max:getDateLimitFromConfig('absMaxDate'),

    //Max zoomable range
    zoomMin:86400000*3,

    stack:false,
    stackSubgroups:true,

    editable: false,

    margin:{
        item:10,
        axis:20
    },

    tooltip:{
        followMouse:true
    }
};

/**
 *  Reads time configuration from tenant config and returns moment object
 *  @return {Object} moment date obj
 */
function getDateLimitFromConfig(name){
    var field = my.app.settings.apps.patient.enrollment.program.history.options[name];
    if(!field) return null;

    //If string, return absolute date
    if(angular.isString(field)) return moment(field);

    //If object, return relative date
    if(angular.isObject(field)){
        var day = moment().startOf('day');
        if(angular.isNumber(field.day)) day = day.add(field.day, 'day');
        if(angular.isNumber(field.month)) day = day.add(field.month, 'month').startOf('month');
        if(angular.isNumber(field.year)) day = day.add(field.year, 'year').startOf('year');
        return day;
    }

    return null;

}


