my.getColor = function(cssClass) {
    if (cssClass) {    
        var palette = cssClass.split('-')[0];
        var shade = cssClass.split('-')[1] || '500';

        //get exact color in current theme based on cssClass
        var color = angular.copy($mdColorPalette[$mdTheming.THEMES[$mdTheming.defaultTheme()].colors[palette].name][shade]);
        color.rgbValue = (color.value.length === 3 ? 'rgb' : 'rgba') + '(' + color.value.toString() + ')';
        color.rgbContrast = (color.contrast.length === 3 ? 'rgb' : 'rgba') + '(' + color.contrast.toString() + ')';
        return color;
    }
    return null;
}