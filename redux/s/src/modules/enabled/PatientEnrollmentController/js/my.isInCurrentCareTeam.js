my.isInCurrentCareTeam = function(provider) {
    for(var i = 0; i < my.myCareData.listOfMember.length; i++) {
        if (my.myCareData.listOfMember[i].userId === provider.userId) 
            return true;
    }
    return false;
};