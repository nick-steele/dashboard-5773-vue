
// Slice off a piece of the config and point to it...
my.config = $model.get('app').settings.apps.patient;
my.enrollmentText = $model.get('app').settings.apps.patient.enrollment.text;;
my.inProgress = null;
my.app = $model.get('app');
my.stepScopes = {
    rhio: {},
    program: {},
    org: {},
    provider: {},
    care: {}
};
my.rhioForm ={};
my.programForm={};
$timeout(function() {
    my.changeStyle('#dynamicStyle');
});
my.gsiSaveInProgress = $gsiSaveButton.IN_PROGRESS;

my.stepConfigMap = {
    1:{
        templateUrl : "patientEnrollment.org.tpl.html",
        onSelect : onSelectOrg,
        isDirty: function () {
            return my.wizardStep1 && my.wizardStep1.$dirty;
        }
    },
    2:{
        templateUrl : "patientEnrollment.rhio.tpl.html",
        onSelect : onSelectRhio,
        isDirty: function () {
            return my.rhioForm && my.rhioForm.$dirty;
        },
        isNoData: function() {
            return !my.originalRhioSharing && my.patientRhio.readOnly === false && (my.patientRhio.rhioFamilies.length !== 0 || my.patientRhio.endpoints.length !== 0);
        }
    },
    3:{
        templateUrl : "patientEnrollment.program.tpl.html",
        onSelect : onSelectProgram,
        isDirty: function () {
            return my.programForm && my.programForm.$dirty;
        },
        isNoData: function() {
            if (!my.patientPrograms || my.patientPrograms.length === 0 )
                return true;
            return false;
        }
    },
    4:{
       templateUrl  :"patientEnrollment.provide.tpl.html",
        onSelect : onSelectProvider,
        isDirty: function () {
            return my.wizardStep5 && my.wizardStep5.$dirty;
        }
    },
    5:{
       templateUrl  :"patientEnrollment.careTeam.tpl.html",
       onSelect: onCareSelect,
       isDirty: function () {
            return my.wizardStep4 && my.wizardStep4.$dirty;
        },
        isNoData: function () {
            return (!my.originalCareTeam || my.originalCareTeam.length===0) && !my.originalCareTeamName;
        }
    }
};

my.onTabSelect=function (step) {
    my.step=step;
    GWT.activity();
    angular.isFunction(my.stepConfigMap[step.id].onSelect)&&my.stepConfigMap[step.id].onSelect(step);
};

my.onTabUnselect=function (step) {
    GWT.activity();
    angular.isFunction(my.stepConfigMap[step.id].onUnselect) && my.stepConfigMap[step.id].onUnselect(step);
};

my.addToPatientPanel=function(){

var patientInfo={MyPatientInfoDTO:{
    patientId : $scope.data.patientId,
    flagged : "N"
 }}
$appEvent('remote:patient:addToPanel',patientInfo);;
}

my.addSuccess=function(msg){
    my.alreadyInList=true;
     my.addToolTip=my.config.text.alreadyMember;
    $model.scope('reload')();
    $mdToast.show({
      template: '<md-toast><span flex>Success</span></md-toast>',
      hideDelay: 2000,
      position: 'top right'
    })
}


my.addFailed=function(){
  $model.scope('reload')();
    $mdToast.show({
      template: '<md-toast><span flex>Failed</span></md-toast>',
      hideDelay: 2000,
      position: 'top right'
    })

}
my.patientId = $scope.data.patientId;

if(!$scope.data.wizardStepId){
    $log.debug('wizard step is not set. taking to the first page...');
    my.wizardStep = 0
}else{

    $log.debug('requested step id', $scope.data.wizardStepId);
    var found;
    for(var wi=0;my.app.settings.apps.patient.enrollment.groups.length;wi++){
        var step = my.app.settings.apps.patient.enrollment.groups[wi];
        if(step.id===$scope.data.wizardStepId){
            my.wizardStep = step.order - 1;
            found = true;

            $log.debug('step id is set to:', my.wizardStep);
            break;
        }
    }
    if(!found){
        $log.debug('step id not found!', $scope.data.wizardStepId);
        my.wizardStep = 0;
    }
}


// is myData used anywhere else? or
// Is it one time use? If yes, can it be removed/refactored? e.g. use my.patientId
// If not, can you rename property name to be more specific?
my.myData={patientId:my.patientId };
//$appEvent('remote:patient:orgData',my.myData);
// FUTURE: Disabled for now
// $scope.saveAllData=function(){
//
// if(my.wizardStep4.$dirty){
//
//          my.saveCareTeam();
// }
//      my.saveOrgData();
//   if(arr2.every(function(elem) {return arr1.indexOf(elem) > -1})){
//
//         $mdDialog.hide();
//         $mdDialog.hide();
//
//   }else{
//
//             $mdDialog.hide();
//
//   }
//
//
// }


my.close=function(form){

    GWT.activity();

    //TODO could be clearner
    if (my.wizardStep4.$dirty !== true && my.wizardStep5.$dirty !== true && my.wizardStep1.$dirty !== true && my.rhioForm.$dirty !== true && my.programForm.$dirty !== true) {
        $mdDialog.cancel();
        return;
    }

    $mdDialog.show({
        locals: {
            enrollmentText: my.enrollmentText,
            steps: my.stepConfigMap,
            groups: my.app.settings.apps.patient.enrollment.groups,
            parentMdDialog: $mdDialog
        },
        clickOutsideToClose: true,

        controller: ['$mdDialog', function($mdDialog){
            var self = this;
            self.discard = function(){ $mdDialog.hide(); self.parentMdDialog.cancel(); };
            self.cancel = function(){ $mdDialog.cancel() };
        }],
        controllerAs: 'ctrl',
        bindToController: true,
        ok: "Ok",
        multiple: true,
        template:
        '<md-dialog>' +
        '<md-dialog-content class="md-dialog-content">' +
        '  <h3 md-colors="{color:\'default-accent\'}" ng-bind="::ctrl.enrollmentText.discardPopupTitle"></h3>' +
        '  <p ng-bind-html="::ctrl.enrollmentText.discardPopupText1"></p>' +
        '<ul><li ng-repeat="data in ctrl.groups track by $index" ng-show="ctrl.steps[data.id].isDirty()">{{data.text}}</li></ul>' +
        '<p ng-bind-html="::ctrl.enrollmentText.discardPopupText2"></p>' +
        '</md-dialog-content>' +
        '<md-dialog-actions>' +
        '<md-button id="cancel" ng-click="ctrl.cancel()" aria-label="{{::ctrl.enrollmentText.discardPopupCancelBtn}}" ng-bind="::ctrl.enrollmentText.discardPopupCancelBtn"></md-button>' +
        '<md-button id="discard" class="md-primary" ng-click="ctrl.discard()" aria-label="{{::ctrl.enrollmentText.discardPopupDiscardBtn}}" ng-bind="::ctrl.enrollmentText.discardPopupDiscardBtn"></md-button>' +
        '</md-dialog-actions>' +
        '</md-dialog>'
    });

};


$appEvent('remote:patient:alreadyInList',my.myData);
$appEvent('remote:patient:checkPatientConsent',my.myData);
$appEvent('remote:patient:orgData',my.myData);
$appEvent('remote:patient:getDemog', my.patientId);
$appEvent('remote:patient:getRequiredRoles', my.patientId);
$appEvent('remote:patient:getDuplicate', my.patientId);
onSelectProgram();
onCareSelect();
onSelectRhio();
function onSelectRhio(){
    if(!my.stepScopes.rhio.initialized){
        $appEvent('remote:patient:getRhio', my.patientId);
        my.stepScopes.rhio.initialized = true;
    }
}

function onSelectProgram(){
    if(!my.stepScopes.program.initialized){
        $appEvent('remote:patient:getPatientPrograms', my.patientId);
        $appEvent('remote:patient:getProgramList', my.patientId);
        $appEvent('remote:patient:getConsentList');
        my.stepScopes.program.initialized = true;
    }
}


my.ifConsented=function(status){
my.haveConsent=status.Boolean;
my.addToolTip=status.Boolean?my.addToolTip:my.config.text.notPermitedToAddToPatientPanel;
}

my.alreadyInList=function(status){
 my.alreadyInList=status.Boolean;
 my.addToolTip=status.Boolean?my.config.text.alreadyMember:my.config.text.AddPatient;
}

my.onSaveError = function (data) {
    GWT.activity();
    $log.error(data);
    var message = data && data.message || 'An error occurred while processing your request.';
    switch(data.type){
        case 'rhio':
            my.stepScopes.rhio.status = $gsiSaveButton.ERROR;
            my.stepScopes.rhio.errorMessage = message;
            break;
        case 'program':
            my.stepScopes.program.status = $gsiSaveButton.ERROR;
            my.stepScopes.program.errorMessage = message;
            break;
        case 'org':
            my.stepScopes.org.status = $gsiSaveButton.ERROR;
            break;

    }
};

function onSelectOrg(){
    if(!my.stepScopes.org.initialized) {
        
        my.stepScopes.org.initialized = true;
    }
}

function onSelectProvider(){
    
    if(!my.stepScopes.provider.initialized) {
        my.stepScopes.provider.initialized = true;        
    }
}

function onCareSelect(){
    if(!my.stepScopes.care.initialized) {
        $appEvent('remote:patient:getRoles',my.myData);

        my.stepScopes.care.initialized = true;
    }
}
my.minDate = new Date('1/1/1900');