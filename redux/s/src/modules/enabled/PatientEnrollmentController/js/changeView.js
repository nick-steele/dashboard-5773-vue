my.backToSearchView = function() {
    my.showSearch = true;
    my.showUserProfile = false;
    my.showEditUser = false;
}
my.backToUserProfile = function() {
    my.showSearch = false;
    my.showUserProfile = true;
    my.showEditUser = false;
    my.showExtWarning = false;
    my.showPrior = false;
}
my.backToCareteamView = function() {
    my.showPrior=false;
    my.showSearch=false;
    my.showUserProfile=false;
}