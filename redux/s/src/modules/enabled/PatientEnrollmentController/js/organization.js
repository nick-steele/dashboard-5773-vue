/*A patient can be assigned to some organation. Each patient will have a primary 
organization and can  have multiple secondary organization. It can be updated and modified*/
my.readonly = false;
my.removable=true;
my.querySearch   = querySearch;
my.searchTextChange   = searchTextChange;
var saveUpdateURI;
my.orgConfig = $model.get('app').settings.apps.patient.enrollment.organization.text;
my.orgIcon = $model.get('app').settings.apps.patient.enrollment.organization.icons;

my.otherCmaSelectedList = [];
var tempArray=[];
my.primaryData=[];
my.providersList=[];
my.providerSelected=[];
my.count=0;
my.orgDisable=true;
my.organizationData={};
my.selectedOrgGroups = [];


//Below function is responsible for bringing all the data from backend onload and show to UI
my.getOrSuccess=function(data){

    if (data.primaryOrganizationSelected) data.primaryOrganizationSelected.typeColor = 'primary';
    data.secondaryOrganizationSelected.forEach(function(val) {
        val.typeColor = 'secondary';
    });
    data.autoConsentedOrganizationList.forEach(function(val) {
        val.typeColor = 'auto';
    })
    my.organizationData=data;


angular.forEach(my.organizationData.cmaOrganizationList,function(value){

        if(value.type.toLowerCase()!=='group'){

           my.primaryData.push(value);

        }

      })
         
        angular.copy(my.primaryData,my.providersList);
        my.savedPrimary=my.organizationData.primaryOrganizationSelected;
        my.tempSelected=my.organizationData.primaryOrganizationSelected;
        saveUpdateURI=my.organizationData.primaryOrganizationSelected?'/updatePatientOrganizationThroughMPA':'/addPatientOrganization';
        my.providerSelected=my.organizationData.dynamicOrganizationSelected.concat(my.organizationData.secondaryOrganizationSelected).concat(my.organizationData.autoConsentedOrganizationList);
        my.providerSelected.push(my.organizationData.primaryOrganizationSelected);
        my.providerSelected=removeDuplicates(my.providerSelected,'name');

        my.wizardStep1.$setPristine();
};


//Responsible for actions on primary CMA change 
my.primaryCmaChange=function(){
    GWT.activity();
    my.wizardStep1.$setPristine();

      my.orgDisable=true;

   if(my.count>0){
      my.orgDisable=false;

  my.wizardStep1.$setDirty();
    var confirm=$mdDialog.confirm()
                    .title(my.orgConfig.warning)
                    .content(my.orgConfig.warnMsg)
                    .ok('yes')
                    .cancel('Cancel')                    
                    .multiple(true);
    $mdDialog.show(confirm).then(function(){
    if(my.savedPrimary){
    my.organizationData.secondaryOrganizationSelected.push(my.savedPrimary);
    my.organizationData.primaryOrganizationSelected.readOnlyOrgForMPA=true;
    my.organizationData.primaryOrganizationSelected.typeColor='primary';
    my.providerSelected=removeDuplicates(my.providerSelected,'name');

  }

    angular.forEach(my.providerSelected,function(value,key){

  if(my.tempSelected && value.name===my.tempSelected.name){


  }

  })
     my.tempSelected=my.organizationData.primaryOrganizationSelected;
     my.organizationData.secondaryOrganizationSelected=removeDuplicates(my.organizationData.secondaryOrganizationSelected,'name');

     angular.forEach(my.organizationData.secondaryOrganizationSelected,function(value,key){

      if(value.name===my.organizationData.primaryOrganizationSelected.name){

            my.organizationData.secondaryOrganizationSelected.splice(key,1);

         }

     })

    my.organizationData.secondaryOrganizationSelected.forEach(function(val) {
        val.typeColor = 'secondary';
    });
    },function(){

        my.organizationData.primaryOrganizationSelected=my.tempSelected;

    })
                    
}else{
    my.tempSelected=my.organizationData.primaryOrganizationSelected;
}
my.count=my.count+1;
    }



var patientId=$scope.data.patientId;

//Saves the all the chnges in data base
my.saveOrgData=function(panelType){
    GWT.activity();

    var payload={};

    my.searchText='';
    my.search='';
    payload.primaryOrganizationSelected=my.organizationData.primaryOrganizationSelected;
    payload.cmaOrganizationList=null;
    payload.allOrganizationList=null;
    payload.autoConsentedOrganizationList=null;
    payload.secondaryOrganizationSelected=my.organizationData.secondaryOrganizationSelected;
    payload.groupList=null;
    payload.patientId=patientId;
    my.stepScopes.org.status = $gsiSaveButton.IN_PROGRESS;
    if(panelType==='organization'){
        payload.uriEndpoints= '/updatePatientOrganization';
        payload.cmaGroupsSelected = my.selectedOrgGroups || [];
        $appEvent('remote:patient:updateOrg',payload);

    }else{
       payload.uriEndpoints='updatePatientOrganizationThroughMPA';
       payload.selectedGroups = my.selectedProviderGroups;
       payload.dynamicOrganizationSelected=my.organizationData.dynamicOrganizationSelected;
       $appEvent('remote:patient:updateMpa',payload);
    }
    my.providerSelected=removeDuplicates(my.providerSelected,'name');

};



my.onOrgSuccess=function(){
  
    my.orgSuccess=true;
    my.selectedOrgGroups = [];
    my.wizardStep1.$setPristine();
    my.stepScopes.org.status=$gsiSaveButton.SUCCESS;
    my.orgDisable=true;
    my.savedPrimary=my.tempSelected;
    $appEvent('remote:patient:provideData',my.myData);

};

//remove all the duplicacy from the list of data
function removeDuplicates(originalArray, objKey) {
  var trimmedArray = [];
  var values = [];
  var value;

  for(var i = 0; i < originalArray.length; i++) {
    value = originalArray[i][objKey];

    if(values.indexOf(value) === -1) {
      trimmedArray.push(originalArray[i]);
      values.push(value);
    }
  }

  return trimmedArray;

}

//actioned on secondary chip remove
my.removeSecondary=function(item){
      GWT.activity();

  my.orgDisable=false;

}

function querySearch (query) {
  if(my.allConfigData.groupEnabled.toLowerCase()=="false"){
          
          my.states=my.primaryData;

         }else{
          my.states = my.organizationData.cmaOrganizationList;

         }

    var results = query ? my.states.filter(createFilterFor(query) ) : my.states,
        deferred;
    return results;
}

function searchTextChange(event) {

document.querySelector('.md-autocomplete-suggestions-container').style.width='30%';
}


function createFilterFor(query) {
    var lowercaseQuery = angular.lowercase(query);

    return function filterFn(state) {
        return (state.name.toLowerCase().indexOf(lowercaseQuery) !== -1);
    };

}

my.onOrganizationAdd = function (item) {
    GWT.activity();
    my.orgDisable=false;

    if(!item) return null;

    if(item.type && item.type.toLowerCase()==='group'){
        if(!itemExists(my.selectedOrgGroups, item)) my.selectedOrgGroups.push(item);
        var orgList = angular.copy(item.organizationList);
        for(var i = 0; i<orgList.length; i++){
            var org = orgList[i];
            if(!org || itemExists(my.organizationData.secondaryOrganizationSelected, org)) continue;
            org.readOnlyOrgForMPA=true;
            org.typeColor = 'secondary';
            my.organizationData.secondaryOrganizationSelected.push(org);
        }
        return null;
    }else if(item.name === my.organizationData.primaryOrganizationSelected.name){
        return null;
    }else if(!itemExists(my.organizationData.secondaryOrganizationSelected, item)){
        item.typeColor = 'secondary';
        return item;
    }else{
        return null;
    }

    /**
     * Helper function to find out if added provider already exists in the list.
     * @param list
     * @param item
     * @return {boolean} true if item already exists in the list. Checks by item.id.
     */
    function itemExists(list, item){
        for(var i=0;i<list.length;i++){
            if(!list[i] || list[i].id!==item.id) continue;
            return true;
        }
        return false;
    }
};

