my.changeStyle = function(id) {
    $(id).html(
    'md-chips md-chip.accent:not(.md-focused) {' +
            'background:' + my.getColor('accent').rgbValue + ';' +
            'color: ' + my.getColor('accent').rgbContrast + ';' +
       '}' +
        'md-chips md-chip.accent:not(.md-focused) .md-chip-remove md-icon {' +
            'color:' + my.getColor('accent').rgbContrast + ';' +
        '}' +
        'md-chips md-chip.accent-300:not(.md-focused) {' +
            'background: ' + my.getColor('accent-300').rgbValue + ';'+
            'color: ' + my.getColor('accent-300').rgbContrast + ';' +       
        '}' +
        'md-chips md-chip.accent-300:not(.md-focused) .md-chip-remove md-icon {' +
            'color: ' + my.getColor('accent-300').rgbContrast + ';' +
        '}'
    );
}