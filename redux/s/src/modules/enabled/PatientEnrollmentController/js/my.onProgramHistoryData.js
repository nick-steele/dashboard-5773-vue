
var subGroupOrderer = function(a,b){return b.subgroupOrder-a.subgroupOrder};




/**
 * Called by remote event. Converts data to visTimeline format.
 * Make sure to directly update existing VisDataSet object. Vis library takes care of updating the timeline.
 * Note that assigning new data to the bound object will force visTimeline to reinitialize.
 *
 *  Currently , to work around some library bugs, on data update:
 *   * items are updated by calling clear() then add(item) for each item
 *   * groups are updated by calling update() for each item.
 *
 * One day events (An episode or a status segment that starts and ends on the same day) can not have a space in the grid.
 * To work around this, one day events are converted to "point" objects which displays details in its tooltip (title property).
 * Such events that occur on the same day are grouped into single "point" object. Details are merged into its tooltip content.
 * See {@link mergeSameDayEvents(eventMap, dataObj, historyData)} for more details.
 *
 * @param {Object} [payload] History data sent from server
 */
my.onProgramHistoryData = function(payload){

    GWT.activity();

    //Dismiss if new data request is sent to remote
    if(payload.timestamp !== historyDataTimestamp){
        $log.debug('timestamp not matched.' + payload.timestamp);
        return;
    }

    //initialize visDataSet
    if(!my.stepScopes.program.historyData){
        my.stepScopes.program.historyData = {
            items: VisDataSet(),
            groups: VisDataSet()
        };
    }else{
        my.stepScopes.program.historyData.items.clear();
    }

    var data = payload.data.records;

    my.stepScopes.program.historyHasError = payload.data.error;

    //Iterate programs:
    for(var i=0; i<data.length;i++){
        var programHistory = data[i];
        var group = {
            id: programHistory.program.id,
            content: (programHistory.program.parentProgram?programHistory.program.parentProgram.name + '&nbsp;/&nbsp;':'')+ programHistory.program.name,
            subgroupOrder:subGroupOrderer
        };

        if(programHistory.subProgramHistories) {
            //If the program is simple program, subordinate programs are parsed
            group.nestedGroups = [];
            group.showNested = false;
            var subGroup = {
                id: group.id + '_sub',
                content: '&nbsp;'
            };
            my.stepScopes.program.historyData.groups.update(subGroup);
            group.nestedGroups.push(subGroup.id);

            processSubProgramHistories(programHistory, subGroup);
        }

        my.stepScopes.program.historyData.groups.update(group);

        var oneDayEpMap = {};

        //Iterate episodes in the program
        for(var j=0; j<programHistory.episodes.length; j++){
            processEpisode(group, programHistory.episodes[j], oneDayEpMap);
        }

    }

    my.stepScopes.program.historyLastUpdated = new Date();

};

function processSubProgramHistories(programHistory, subGroup) {


    //Iterate simple subordinate programs
    for (var l = 0; l < programHistory.subProgramHistories.length; l++) {
        var subProgramHistory = programHistory.subProgramHistories[l];
        var subGroupProgramId = subGroup.id + '_' + subProgramHistory.program.id;
        var oneDaySubEpMap = {};

        //Iterate simple subordinate program episodes
        for (var m = 0; m < subProgramHistory.episodes.length; m++) {
            var subEpisodeObj = convertSubProgramEpisode(subProgramHistory.episodes[m], subProgramHistory, subGroup.id, subGroupProgramId);

            my.stepScopes.program.historyData.items.add(subEpisodeObj.data);
            if (subEpisodeObj.dayEvent) mergeSameDayEvents(oneDaySubEpMap, subEpisodeObj, my.stepScopes.program.historyData);
        }
    }

}


/**
 * Convert simple subordinate program episodes.
 */
function convertSubProgramEpisode(subEpisode, subProgramHistory, subGroupId, subGroupProgramId) {
    var isOneDay = subEpisode.start === subEpisode.end;

    var dateStr = getDateStr(subEpisode.start, subEpisode.end);

    var result = {};

    result.data = {
        className: 'subordinate-program-item' + (isOneDay?' one-day':''),
        content: isOneDay ? '&ngsp;' : subProgramHistory.program.name,
        start: moment(subEpisode.start),
        end: subEpisode.end ? moment(subEpisode.end) : moment(),
        group: subGroupId,
        id: subGroupProgramId + 'ep_' + subEpisode.id,
        subgroup: subGroupProgramId,
        title: isOneDay ? null : dateStr,
        type: 'range'
    };

    if(!isOneDay) return result;

    result.dayEvent = {};
    result.dayEvent.date = subEpisode.start;
    result.dayEvent.data = {
        className: 'subordinate-point-item' ,
        group: subGroupId,
        id: subGroupProgramId + 'ep_' + subEpisode.id+ '_day',
        start: moment(subEpisode.start),
        subgroup: subGroupProgramId,
        title: subProgramHistory.program.name + ' (' + dateStr + ')',
        type: 'point'
    };

    return result

}



function processEpisode(group, episode, oneDayEpMap) {
    var episodeObj = convertEpisodeData(group.id, episode);
    var idPrfx = episodeObj.data.id;

    my.stepScopes.program.historyData.items.add(episodeObj.data);

    var oneDaySegMap = {};

    //Iterate segments(statuses) in the episode
    for (var k = 0; k < episode.segments.length; k++) {
        var segmentObj = convertSegmentData(episode.segments[k], group.id, idPrfx);
        my.stepScopes.program.historyData.items.add(segmentObj.data);
        if (segmentObj.inactiveSpacer) my.stepScopes.program.historyData.items.add(segmentObj.inactiveSpacer);
        if (segmentObj.dayEvent) mergeSameDayEvents(oneDaySegMap, segmentObj, my.stepScopes.program.historyData);
    }
    if (episodeObj.dayEvent) mergeSameDayEvents(oneDayEpMap, episodeObj, my.stepScopes.program.historyData);
}


/**
 * Convert episode data
 */
function convertEpisodeData(groupId, episode) {
    var idPrfx = groupId + '_ep' + episode.id;

    var oneDay = episode.start === episode.end;
    var dateStr = getDateStr(episode.start, episode.end);
    var endReasonStr = episode.endReason ? '(' + episode.endReason.value + ')' : '&nbsp;';

    var result = {};
    result.data = {
        align: 'right',
        className: 'episode-item' + (oneDay?' one-day':''),
        content: oneDay ? '&ngsp;' : endReasonStr,
        start: moment(episode.start),
        end: episode.end ? moment(episode.end) : moment(),
        group: groupId,
        id: idPrfx,
        subgroup: 'episode',
        subgroupOrder: 0,
        title: oneDay ? null : dateStr,
        type: 'range'
    };

    if(!oneDay) return result;

    result.dayEvent = {
        date: episode.start,
        data: {
            className: 'episode-point-item',
            group: groupId,
            id: idPrfx+'_day',
            start: moment(episode.start),
            subgroup: 'episode',
            subgroupOrder: 0,
            title: (episode.endReason ? episode.endReason.value : '') + ' (' + dateStr + ')' ,
            type: 'point'
        }
    };
    return result;
}


/**
 * Convert segment(status) data
 */
function convertSegmentData(segment, groupId, idPrfx) {
    var isOneDay = segment.end === segment.start;
    var dateStr = getDateStr(segment.start, segment.end);
    var segmentId = idPrfx + '_seg' + segment.id;


    var result = {};
    result.data = {
        className: 'segment-item' + (segment.status.endReasonRequired ? ' inactive' : '') + (isOneDay?' one-day':''),
        content: isOneDay ? '&ngsp;' : segment.status.name,
        start: moment(segment.start),
        end: segment.end ? moment(segment.end) : moment(),
        group: groupId,
        id: segmentId,
        subgroup: 'status',
        subgroupOrder: 1,
        title: isOneDay ? null : dateStr,
        type: 'range'
    };

    if(segment.status.endReasonRequired){
        result.inactiveSpacer = {
            className:'episode-item inactive',
            start: moment(segment.start),
            end: segment.end ? moment(segment.end) : moment(),
            group: groupId,
            content:'&nbsp;',
            id: segmentId+'_inactive',
            subgroup:'episode',
            subgroupOrder: 0,
            type:'range'
        }
    }

    if(!isOneDay) return result;
    result.dayEvent = {};
    result.dayEvent.date = segment.start;

    result.dayEvent.data = {
        className: 'segment-point-item' + (segment.status.endReasonRequired ? ' inactive' : ''),
        group: groupId,
        id: segmentId+'_day',
        start: moment(segment.start),
        subgroup: 'status',
        subgroupOrder: 1,
        title: segment.status.name + ' (' + dateStr + ')',
        type: 'point'
    };
    return result;
}


/**
 * Merge one day events that occur on the same day.
 */
function mergeSameDayEvents(eventMap, dataObj, historyData) {
    if(!dataObj.dayEvent) return;
    if (!eventMap[dataObj.dayEvent.date]) {
        eventMap[dataObj.dayEvent.date] = dataObj.dayEvent.data;
        historyData.items.add(dataObj.dayEvent.data);
    } else {
        eventMap[dataObj.dayEvent.date].title += '<br />' + dataObj.dayEvent.data.title;
    }
}

/**
 * Generate date string from start and end dates.
 */
function getDateStr(start, end) {
    return $filter('date')(start, 'M/d/yyyy') + '&nbsp;-&nbsp;' + (end ? $filter('date')(end, 'M/d/yyyy') : '');
}
