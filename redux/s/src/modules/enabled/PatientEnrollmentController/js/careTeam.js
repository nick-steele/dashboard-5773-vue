/*Creation of new cacre team. Includes the removal and updation of existing care team */
my.pageModified = true; //FIXME var name is not appropriate
my.stepScopes.care = {};
my.filteredRole = {};
var arr2 = [],
    arr1 = [],
    arr3 = [],
    arr4 = [];
my.careBtnDisabled = true;

my.showConsentedFlag = false;
my.careConfig = $model.get('app').settings.apps.patient.enrollment.careTeam.text;
my.oneMemberInCareTeam = $model.get('app').settings.apps.patient.enrollment.careTeam.oneMemberInCareTeam;
my.careIcons = $model.get('app').settings.apps.patient.enrollment.careTeam.icons;
my.careIconColors = $model.get('app').settings.apps.patient.enrollment.careTeam.iconColors;

var template = '<md-dialog  aria-label="My Dialog" >' +
    '<md-dialog-content layout-align="end center" class="consented">' + '<div>' +
    my.careConfig['consentedLabel']+
    '</div>' +
    '<br><div>' +
    my.careConfig['permissionLabel'] +
    '</div>' + '</md-dialog-content>' + '</md-dialog>'

my.showPrior = false;
my.myData = { patientId: $scope.data.patientId };

$appEvent('remote:patient:getCare', my.myData);


//below function assign the data to list on load of prior
my.priorSuccess = function(response) {
    my.myPriorList = response;

    angular.forEach(my.myPriorList.listOfMember, function(value, key) {

        if (value.selected === true) {

            //my.myCareData.listOfMember.push(value);
            my.myPriorList.listOfMember.splice(key, 1);
            my.myPriorList.listOfMember = my.myPriorList.listOfMember.splice(key, 1);

        }

    });

}
var deleteCareWatch = $scope.$watch('patientEnrollment.myCareData', function(newVal) {
  if (newVal) {
    my.openDirectMessage();
    deleteCareWatch();
  }
});
my.directStatusSuccess = function(status) {
  my.directMessagingRequired = status;
}
my.openDirectMessage = function() {
    my.showDirectIcon = false;
    $appEvent('remote:patient:getDirectStatus', my.myData);
    if (my.allConfigData.showDirect === 'true') {
        if (my.allConfigData.disableMinor.toLowerCase() === 'false') {
            my.showDirectIcon = true;
        } else {
            if (getAge(my.patientDemog.dob) >= parseInt(my.allConfigData.directAge)) {
                my.showDirectIcon = true;
            }
        }
    }
}
function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}
my.getPriorList = function() {
    GWT.activity();
    $appEvent('remote:patient:getPrior', my.myData);
    my.showPrior = true;

}

my.countSelectedProvider = function(){
    return (my.myPriorList.listOfMember.filter(function(a) { return a.selected; }) || []).length;
}

//function fired when  checkbox is selected on prior
my.optionToggled = function(selected) {
    GWT.activity();

    my.isAllSelected = my.myPriorList.listOfMember.every(function(itm) { return itm.selected; })
    if (selected.consentStatus !== 'PERMIT' && selected.selected == true) {

        var confirm = $mdDialog.confirm()
            .title()
            .htmlContent(template)
            .ok(my.careConfig.yesLabel)
            .cancel(my.careConfig.noLabel)
            .multiple(true);
        $mdDialog.show(confirm).then(function() {
            var payload = { dynamicOrganizationSelected: [{ id: selected.organizationId }], patientId: my.myData.patientId }
             selected.consentStatus='PERMIT';          
            $appEvent('remote:patient:giveConsent', payload);
            $appEvent('remote:patient:provideData', my.myData);


        }, function() {
            selected.selected = false;
            my.isAllSelected = false;
        })
    }
}


//add the data to the list of cards from search n provider page
my.addtoList = function(role, backFn) {
    my.pageModified = false;
    if(my.changemodeStarted){

        my.removeCard();
        my.changemodeStarted=false;
    }

    my.backFn = backFn || function() { my.showSearch = true;
        my.showExtWarning = false; };
    my.careBtnDisabled = false;
    my.errorCare='';
    my.stepScopes.care.status = undefined;

    var payload = { dynamicOrganizationSelected: [{ id: role.organizationId }], patientId: my.myData.patientId }

    my.myCareData = my.myCareData || {listOfMember:[]};

    if (role.userType.toLowerCase() === 'internal') {
        if (role.consentStatus == 'PERMIT') {

            my.myCareData.listOfMember.push(role);
            my.myCareData.listOfMember = removeDuplicateData(my.myCareData.listOfMember, 'userId');
            my.showPrior = false;
            my.showSearch = false;
            role.selected = true;
            // support Provider Details screen -> view Careteam
            my.showUserProfile = false;
        } else {

            var confirm = $mdDialog.confirm()
                .title()
                .htmlContent(template)
                .ok(my.careConfig.yesLabel)
                .cancel(my.careConfig.noLabel)
                .multiple(true);
            $mdDialog.show(confirm).then(function() {                   
                role.consentStatus='PERMIT';
                $appEvent('remote:patient:giveConsent', payload);
                $appEvent('remote:patient:provideData', my.myData);

                my.myCareData.listOfMember.push(role);
                my.showPrior = false;
                my.showSearch = false;
                role.selected = true;
                // support Provider Details screen -> view Careteam
                my.showUserProfile = false;
                my.myCareData.listOfMember = removeDuplicateData(my.myCareData.listOfMember, 'userId');

            }, function() {

            })
        }

    } else {
        my.extData = role;
        my.showExtWarning = true;
        my.showSearch = false;
        // support Provider Details screen -> view Careteam
        my.showUserProfile = false;
    }
    my.wizardStep4.$setDirty();

}

//It adds the external data to the card list
my.addExternalData = function() {
    my.myCareData.listOfMember.push(my.extData);
    my.showPrior = false;
    my.showSearch = false;
    my.showExtWarning = false;
    my.myCareData.listOfMember=removeDuplicateData(my.myCareData.listOfMember,'userId');

}

my.toggleAll = function() {
    GWT.activity();
    var toggleStatus = my.isAllSelected;
    angular.forEach(my.myPriorList.listOfMember, function(itm) { itm.selected = toggleStatus; });

}



//Import the prior list selected data to the card page 
my.importCareMembers = function() {
    my.showPrior = false;
     my.stepScopes.care.status='';
    $appEvent('remote:patient:provideData', my.myData);


    angular.forEach(my.myPriorList.listOfMember, function(value, key) {

        if (value.selected === true) {
            my.myCareData.listOfMember.push(value);
            my.myPriorList.listOfMember.splice(key, 1);
            my.myPriorList.listOfMember = my.myPriorList.listOfMember.splice(key, 1);
            my.wizardStep4.$setDirty();
            my.careBtnDisabled = false;
        }

    })

    my.myCareData.listOfMember = removeDuplicateData(my.myCareData.listOfMember, 'userId');

}

var searchCTMemberPromise;

function requestCareTeamMemberSearch(searchString) {

    var param = angular.copy(my.myData);
    param.pageNo = 0;
    param.noOfData = 100;
    param.searchParam = searchString ? searchString : '%20';
    param.consentRequired = my.showConsentedFlag;
    param.roleId = my.filteredRole.id ? my.filteredRole.id : '%20';
    var timestamp = (new Date()).getDate();
    my.careteamSearchTimestamp = timestamp;
    param.timestamp = timestamp;

    $appEvent('remote:patient:getFilterdData', param);
}

var DELAY_DEFAULT = 500;
var DELAY_SHORT = 2000;

//it filters and get me the search data from backend in list
my.getSearchedData = function(searchString) {

    var delay = (!my.searchString||my.searchString.length>=3) ? DELAY_DEFAULT : DELAY_SHORT;

    if(searchCTMemberPromise){
        $timeout.cancel(searchCTMemberPromise);
    }
    searchCTMemberPromise = $timeout(function () {
        requestCareTeamMemberSearch(searchString);
        searchCTMemberPromise=null;
    }, delay);

};

my.directMessaging = function() {
    my.wizardStep4.$setDirty();
    my.careBtnDisabled = false;
    my.errorCare='';
}

var ppMenuCtrl = ['$scope', 'mdPanelRef', '$mdPanel', '$window', 'constants',
    function($scope, mdPanelRef, $mdPanel, $window, constants) {
        var my = this;
        my.constants = constants;
        my._mdPanelRef = mdPanelRef;

        my.close = function() {
            var panelRef = my._mdPanelRef;
            panelRef && panelRef.close().then(function() {
                panelRef.destroy();
            });

        };
        my.getPriorList = function() {
            mdPanelRef.close();
            my.patientEnrollment.getPriorList();
        }
        my.patientEnrollment.removePatientFromCareTeam = function() {

            mdPanelRef.close();
            var confirm = $mdDialog.confirm()
                .title(my.patientEnrollment.orgConfig.warning)
                .content(my.patientEnrollment.careConfig.removeFromCare)
                .ok(my.patientEnrollment.careConfig.yes)
                .cancel(my.patientEnrollment.orgConfig.btnCancel)
                .multiple(true);
            $mdDialog.show(confirm).then(function() {
                var removePayload = {};
                angular.copy(my.patientEnrollment.myCareData, removePayload);
                removePayload.listOfMember = null;
                removePayload.patientId = my.patientEnrollment.myData.patientId;
                $appEvent('remote:patient:saveCare', removePayload);

            })

        }
        my.addtoList = function() {

            my.patientEnrollment.myCareData.listOfMember.push(my.patientEnrollment.shownMember);
            my.patientEnrollment.showPrior = false;
            my.patientEnrollment.showSearch = false;
            my.patientEnrollment.careBtnDisabled = false;
            my.patientEnrollment.errorCare='';
            my.patientEnrollment.myCareData.listOfMember = removeDuplicateData(my.patientEnrollment.myCareData.listOfMember, 'userId');
            my.patientEnrollment.wizardStep4.$setDirty();
            my.close();

        }

        //Redirect to profile page
        my.goToProfile = function() {
            my.close();
            my.patientEnrollment.displayUserProfile(my.index, my.member);
        };

        //Redirect to profile page from card
        my.cardToProfile = function() {
            my.close();
            my.patientEnrollment.displayUserProfile(my.index, my.cardMember, my.patientEnrollment.backToCareteamView);
        };



        //It removes the selected card from the list
        my.patientEnrollment.removeCard = function(form) {

            my.patientEnrollment.myCareData.listOfMember = my.patientEnrollment.myCareData.listOfMember.filter(function(el) {
             mdPanelRef.close();
             return el.userId !== my.cardMember.userId;

            });
                 my.patientEnrollment.wizardStep4.$setDirty();
                my.patientEnrollment.wizardStep4.careTeam.$setDirty();
                my.patientEnrollment.careBtnDisabled = false;
                my.patientEnrollment.patientEnrollment='';
                my.patientEnrollment.pageModified = false;
        }

        //it modify the card and update it
        my.modifyRole = function() {
            my.patientEnrollment.pageModified = false;
            var param = my.patientEnrollment.myData;
            param.pageNo = 0;
            param.noOfData = 25;
            param.searchParam = '%20';
            param.consentRequired = my.showConsentedFlag;
            param.roleId = my.cardMember.roleId;
            my.patientEnrollment.filteredRole = my.cardMember;
            my.patientEnrollment.filteredRole.name = my.cardMember.roleName;
            my.patientEnrollment.filteredRole.id = my.cardMember.roleId;

            $appEvent('remote:patient:getFilterdData', param);
            my.patientEnrollment.changemodeStarted=true;
            mdPanelRef.close();

            my.patientEnrollment.showSearch = true;
            my.patientEnrollment.careBtnDisabled = false;
            my.patientEnrollment.errorCare='';
            my.patientEnrollment.wizardStep4.$setDirty();
            my.patientEnrollment.wizardStep4.careTeam.$setDirty();



        }

    }
];

var ppMenuConfig = {
    attachTo: angular.element(document.body),
    controller: ppMenuCtrl,
    controllerAs: 'vm',
    templateUrl: 'careMenu.tpl.html',
    panelClass: 'mpl-item-menu',
    clickOutsideToClose: true,
    escapeToClose: true,
    focusOnOpen: false,
    zIndex: 82,
    onRemoving: function() {
        my.selectedMenuId = null;
    }
};


var directMsgConfig = {
    attachTo: angular.element(document.body),
    controller: ppMenuCtrl,
    controllerAs: 'msg',
    templateUrl: 'directMsg.tpl.html',
    panelClass: 'mpl-item-menu',
    clickOutsideToClose: true,
    escapeToClose: true,
    focusOnOpen: false,
    zIndex: 82,
    onRemoving: function() {
        my.selectedMenuId = null;
    }
};

var cardMenuConfig = {
    attachTo: angular.element(document.body),
    controller: ppMenuCtrl,
    controllerAs: 'ctrl',
    templateUrl: 'cardMenu.tpl.html',
    panelClass: 'mpl-item-menu',
    clickOutsideToClose: true,
    escapeToClose: true,
    focusOnOpen: false,
    zIndex: 82,
    onRemoving: function() {
        my.selectedMenuId = null;
    }
};

//open the munu list panel

my.clickMenuOpen = function(ev, member, index) {

    my.careMember = member;
    var position = $mdPanel.newPanelPosition()
        .relativeTo('#careMenuBtn_' + index)
        .addPanelPosition($mdPanel.xPosition.OFFSET_END, $mdPanel.yPosition.ALIGN_TOPS);

    ppMenuConfig.openFrom = ev;
    ppMenuConfig.position = position;
    ppMenuConfig.locals = {
        'member': member,
        'index': index,
        'patientEnrollment': my
    };
    //ppMenuConfig.attachTo=angular.element(document.querySelector('.enrollment-wizard'));
    $mdPanel.open(ppMenuConfig);

};


//opens the panel of menu at caed ellipses
my.cardMenuOpen = function(ev, cardMember, index) {

    //my.shownMember=member;
    var position = $mdPanel.newPanelPosition()
        .relativeTo('#cardMenu_' + index)
        .addPanelPosition($mdPanel.xPosition.OFFSET_END, $mdPanel.yPosition.ALIGN_TOPS);

    cardMenuConfig.openFrom = ev;
    cardMenuConfig.position = position;
    cardMenuConfig.locals = {
        'cardMember': cardMember,
        'index': index,
        'patientEnrollment': my
    };
    // ppMenuConfig.attachTo=angular.element(document.querySelector('.enrollment-wizard'));
    $mdPanel.open(cardMenuConfig);

};

my.showConsented = function(consented) {
    my.tempVar = my.searchedResult
    if (consented) {
        my.tempVar = my.searchedResult
        my.searchedResult = $filter('filter')(my.tempVar, 'PERMIT');

    } else {
        my.searchedResult = my.tempVar;

    }

}

my.validateEmail = function() {

    $appEvent('remote:patient:validateEmail', my.myData);

}
my.validateCareName = function(name) {
    my.myData.query = name;
    $appEvent('remote:patient:validateName', my.myData);
}


//It does the validation of data before saving
my.validate = function(data) {
  my.prevStatus=data;
  //TODO move this to configuration
  var myTemplate ='<div>This Care Team is shared by other patients.</div>'+
                   '<div class="pre"><br><b>Editing will affect all other patients with this Care Team.</b></div>'+
                   '<div><br>Would you like to make changes for all patients with this Care Team, or create a separate team?</div>'

  if(my.prevStatus &&
      my.stepConfigMap[5].isDirty() &&
      my.myCareData.careTeamId &&
      !my.pageModified)
  {
   var dialog = $mdDialog.confirm()
          .title('Warning!')
          .htmlContent(myTemplate)
          .ok(my.careConfig.changeForAll)
          .cancel(my.careConfig.createNewSeparateTeam)
          .multiple(true);

        //TODO Will be removed after final testing 
       //dialog["_options"].clickOutsideToClose=true

        $mdDialog.show(dialog).then(function(){

          my.myCareData.createNewCareTeam=false;
           my.stepScopes.care.status = $gsiSaveButton.IN_PROGRESS
           $appEvent('remote:patient:saveCare',my.myCareData);

        },function(){
         
           my.myCareData.createNewCareTeam=true;
           $appEvent('remote:patient:validateNew',my.myData);
        });

  }else if(data && my.wizardStep4.careTeam.$dirty && !my.myCareData.careTeamId) { //FIXME is this in use?

     var dialog = $mdDialog.alert()
          .title()
          .htmlContent(my.careConfig.careValidate)
          .ok('ok')
          .multiple(true);
        $mdDialog.show(dialog).then(function(){
           my.getCount=0;


        });

  }else {
           my.stepScopes.care.status = $gsiSaveButton.IN_PROGRESS
           $appEvent('remote:patient:saveCare',my.myCareData);

  }

}

my.newSuccess = function(data) {
        my.newstatus = data;
        if (!data) {

          my.validate();

        } else {

            var dialog = $mdDialog.alert()
                .title()
                .content(my.careConfig.careValidate)
                .ok('ok')
                .multiple(true);
            $mdDialog.show(dialog).then(function() {
                my.getCount = 0;


            });
        }
    }
    //Saves all the changes at care team page
my.saveCareTeam = function() {
    arr3 = [],
    arr4 = [];

    my.myCareData.directMessagingRequired = my.directMessagingRequired;
    
    //FIXME variable name should be more self explanatory (arr1, arr2, arr3... doesn't make sense)
      angular.forEach(my.requiredroles,function(val){
      arr2.push(val.id);
      arr3.push(val.name);

    })

    angular.forEach(my.myCareData.listOfMember,function(value){

      arr1.push(value.roleId);
      arr4.push(value.roleName);
    })
    $scope.dataMissing=arr3.filter(function(x) {  // return elements in previousArray matching...
        return arr4.indexOf(x)===-1;  // "this element doesn't exist in currentArray"
    })

    my.myCareData.patientId=my.myData.patientId;
    $scope.dataMissing = $scope.dataMissing.filter(function(item, pos) {
            return $scope.dataMissing.indexOf(item) == pos;
        })

    //check One_Member_Rule_With_Any_Role, please contact Panda Team if you have concern
    if (my.careConfig.oneMember === true) {
      if(!my.isViolateOneMemberRule(my.myCareData.listOfMember, my.careConfig.oneMemberRuleTitle)) {

        checkMemberConsent(my.myCareData.careTeamName, my.myCareData.listOfMember, function(){$appEvent('remote:patient:saveCare', my.myCareData)});

      }
      return;
    }

    else if($scope.dataMissing.length>0){

        //my.stepScopes.care.status = $gsiSaveButton.IN_PROGRESS;

       $scope.missingMemberLabel=my.careConfig.missingMemberLabel;
       $scope.mandatoryMembers=my.careConfig.mandatoryMembers;

        $mdDialog.show({
          locals: {parent: $scope,ok: "Ok"},
          clickOutsideToClose: true,

          controller: angular.noop,
          controllerAs: 'ctrl',
          bindToController: true,
          ok: "Ok",
          multiple:true,
          template:
            '<md-dialog>' +
            '  <div style="font-size: 21px;margin-left: 11px;margin-top: 11px;color: rgb(33,150,243);">{{ctrl.parent.missingMemberLabel}}</div>' +
            '  <div style="margin-top:15px;margin-left:15px">{{ctrl.parent.mandatoryMembers}}</div>' +
            '<ul><li ng-repeat="data in ctrl.parent.dataMissing track by $index">{{data}}</li></ul>' +
            '<div><md-button class="md-primary" style="float:right" ng-click="ctrl.parent.closepopUp()">OK</md-button><div>' +

            '</md-dialog>'
        });
       my.stepScopes.care.status = $gsiSaveButton.ERROR;

    }else if(arr2.every(function(elem) {return arr1.indexOf(elem) > -1})){

        checkMemberConsent(my.myCareData.careTeamName, my.myCareData.listOfMember, my.validateCareName);

    }
    else{

        my.validateEmail();
    }

       // my.stepScopes.care.status = $gsiSaveButton.IN_PROGRESS;
}

//FIXME don't use $scope just to path function to dialog
$scope.closepopUp = function() {
    $mdDialog.hide();
}

function checkMemberConsent(careTeamName, members, callback){
    members = members || [];
    var deniedMembers = members.filter(function (a) {
        return a.consentStatus === 'DENY';
    });
    if(deniedMembers.length === 0){
        my.validateEmail();
        return;
    }

    var promise = $mdDialog.show({
        locals: {members: deniedMembers, careTeamName: careTeamName},
        clickOutsideToClose: false,
        escapeToClose:false,
        controller: ['$scope','$mdDialog', '$model',function($scope, $mdDialog, $model){
            $scope.ok = function(){$mdDialog.hide()};
            $scope.cancel = function(){$mdDialog.cancel()};
            var textConfig = $model.get('app').settings.apps.patient.enrollment.careTeam.text;
            $scope.getText = function(key){
              return textConfig[key];
            };
        }],
        title:my.careConfig.consentAdvisoryPopupTitle,
        controllerAs: 'ctrl',
        bindToController: true,
        multiple:true,
        template:
        '<md-dialog class="consent-advisory-dialog">' +
        '   <md-dialog-content class="md-dialog-content">' +
        '   <h3>{{::getText(\'consentAdvisoryPopupTitle\')}}{{::ctrl.careTeamName}}</h3>' +
        '   <div class="md-body-1">' +
        '   <p md-colors="{color:\'default-accent\'}">{{::getText(\'consentAdvisoryPopupText1\')}}</p>' +
        '   <ul>' +
        '       <li ng-repeat="member in ctrl.members">{{::member.firstName}} {{::member.lastName}}, {{::member.credential}} - {{member.organizationName}}</li>' +
        '   </ul>' +
        '   <span md-colors="{color:\'default-accent\'}">{{::getText(\'consentAdvisoryPopupText2\')}}</span>' +
        '   <p>{{::getText(\'consentAdvisoryPopupText3\')}}</p>' +
        '   <div md-colors="{color:\'default-accent\'}">{{::getText(\'consentAdvisoryPopupTextNote\')}}</div>' +
        '   </div>' +
        '   </md-dialog-content>' +
        '   <md-dialog-actions layout="row" layout-align="center center">' +
        '       <md-button class="md-accent" ng-click="cancel()">{{::getText(\'consentAdvisoryPopupCancelBtn\')}}</md-button>' +
        '       <md-button class="md-primary" ng-click="ok()">{{::getText(\'consentAdvisoryPopupOkBtn\')}}</md-button>' +
        '   </md-dialog-actions>' +
        '</md-dialog>'
    });

    promise.then(function(data){
        my.validateEmail();
    },angular.noop);
};

//functions is called when careteam save is successful
my.careSaveSuccess = function(data) {
    my.careSaved=true;
    $scope.warning=my.orgConfig.warning
  $scope.duplicationLabel=my.careConfig.duplicationLabel;
    $scope.duplicateData = my.duplicateroles;
    if (data.toLowerCase() === 'duplicateerror') {

        $scope.dialogOpen = true;
        $mdDialog.show({
            locals: { parent: $scope, ok: "Ok" },
            clickOutsideToClose: true,

            controller: angular.noop,
            controllerAs: 'ctrl',
            bindToController: true,
            ok: "Ok",
            multiple: true,
            template: '<md-dialog>' +
        '  <div style="font-size: 21px;margin-left: 11px;margin-top: 11px;color: rgb(33,150,243);">{{ctrl.parent.warning}}</div>' +
        '  <div style="margin-top:15px;margin-left:15px">{{ctrl.parent.duplicationLabel}}</div>' +
        '<ul><li ng-repeat="data in ctrl.parent.duplicateData">{{data.name}}</li></ul>' +
        '<div><md-button class="md-primary" style="float:right" ng-click="ctrl.parent.closepopUp()">OK</md-button><div>' +
        '</md-dialog>'
        });
        
        my.stepScopes.care.status = $gsiSaveButton.ERROR;
    } else {
            $appEvent('remote:patient:provideData', my.myData);

            $appEvent('remote:patient:getCare', my.myData);


        my.careBtnDisabled=true;
        my.stepScopes.care.status = $gsiSaveButton.SUCCESS;

        my.wizardStep4.$setPristine();
        my.originalCareTeam = angular.copy(my.myCareData.listOfMember);
        my.originalCareTeamName = my.myCareData.careTeamName;
    }

}
my.getCareSuccess = function(data) {
    my.myCareData = data;
    my.originalCareTeam = angular.copy(data.listOfMember);
    my.originalCareTeamName = data.careTeamName;
    my.isLoadedCareData = true;
}
my.getCareFailed = function() {
}
my.careSaveFailed = function() {

    alert('failed');
}


my.filterByName = function(name, allData) {

    my.myCareData.listOfMember = $filter('filter')(allData, name);

}

my.getCount = 0;

//when backend call for search data gives the success response below function is called
my.searchSuccess = function(payload) {
    if(payload.timestamp !== my.careteamSearchTimestamp){
        $log.info('the search is outdated.');
        return;
    }
    my.searchedResult = payload.data;

    if (my.searchedResult.length < 1 && my.getCount === 0) {
        my.getCount = my.getCount + 1;

        var dialog = $mdDialog.alert()
            .title()
            .content(my.careConfig.noResult)
            .ok('ok')
            .multiple(true);
        $mdDialog.show(dialog).then(function() {
            my.getCount = 0;


        });

    } else {
        my.showConsented(my.showConsentedFlag);
    }
}

function removeDuplicateData(originalArray, objKey) {
    var trimmedArray = [];
    var values = [];
    var value;

    for (var i = 0; i < originalArray.length; i++) {
        value = originalArray[i][objKey];

        if (values.indexOf(value) === -1) {
            trimmedArray.push(originalArray[i]);
            values.push(value);
        }
    }

    return trimmedArray;

}


//Checks if email from backend is blank or have some data
my.emailSuccess = function(email) {
    if (my.directMessagingRequired) {


        if (email == null || email == '') {
            var confirm = $mdDialog.alert()
                .title(my.orgConfig.warning)
                .content(my.careConfig.directMessaging)
                .ok('ok')
                .multiple(true);
            $mdDialog.show(confirm);

        } else {

            my.validateCareName(my.myCareData.careTeamName);

        }
    } else {

        my.validateCareName(my.myCareData.careTeamName);

    }


}


//open the menu ellpses for on care team page 
my.openCareTeamMenu = function(ev) {
    var position = $mdPanel.newPanelPosition()
        .relativeTo('#directMenu')
        .addPanelPosition($mdPanel.xPosition.OFFSET_END, $mdPanel.yPosition.ALIGN_TOPS);

    directMsgConfig.openFrom = ev;
    directMsgConfig.position = position;
    directMsgConfig.locals = {
        'patientEnrollment': my
    };
    $mdPanel.open(directMsgConfig);
}

my.onCareTeamSelect = function() {
    my.wizardStep4.careTeam.$setPristine();
    my.careBtnDisabled = false;
    my.pageModified = true; // Reset flag to track if a careteam is edited
    my.myCareData = {listOfMember:[]};
    if(!my.careTeamSearchSelected) return;
    $appEvent('remote:patient:getCareTeamDetails', {patientId: my.patientId, careTeamId: my.careTeamSearchSelected.careTeamId});
};


var careTeamSearchResult = {};

my.onCareTeamSearchResult = function (data) {
    if(careTeamSearchResult.timestamp!==data.timestamp){
        $log.info("timestamp did not match");
        return;
    }
    careTeamSearchResult.deferred.resolve(data.data);
};

var ctSearchDelayPromise;


my.careTeamSearchQuery = function(keyword){

    if(careTeamSearchResult.deferred) careTeamSearchResult.deferred.reject();
    if(ctSearchDelayPromise) $timeout.cancel(ctSearchDelayPromise);

    if(!keyword || keyword.trim().length === 0 ) return null;

    var delay = (keyword.length>=3) ? 1000 : 2000;

    careTeamSearchResult = {
        deferred: $q.defer()
    };

    //Delay firing the search
    ctSearchDelayPromise = $timeout(function () {

        var timestamp = (new Date()).getTime();
        careTeamSearchResult.timestamp = timestamp;
        //Send Request
        $appEvent(
            'remote:patient:searchCareTeam',
            {patientId: my.patientId, keyword: keyword, timestamp: timestamp}
        );
        ctSearchDelayPromise = null;

    }, delay);

    return careTeamSearchResult.deferred.promise;

};