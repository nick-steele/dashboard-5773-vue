my.displayUserProfile = function (index, userProfile, backFn) {

    // when click back button in View More User Provider Detail, the default behavior is back to search Care Team view
    my.backToPreviousScreen = backFn || my.backToSearchView;
    my.userIndex = index;
    my.userProfile = userProfile;
    my.showSearch = false;
    my.showUserProfile = true;
    my.showPrior = false;
    my.showEditUser = false; 
    my.showExtWarning = false;
}
my.displayEditUser = function (userProfile) {
    my.IS_EDITMODE = userProfile ? true : false;
    if(my.IS_EDITMODE && userProfile.roleId)
    {
        userProfile.roleId = userProfile.roleId + '';
    }
    my.backToPreviousScreen = my.IS_EDITMODE ? my.displayUserProfile.bind(null, my.userIndex, userProfile) : my.backToSearchView;    
    my.userEditing = userProfile ? angular.copy(userProfile) : {};
    my.showSearch = false;
    my.showPrior = false;
    my.showUserProfile = false;
    my.showEditUser = true;
    my.showExtWarning = false;
}
my.saveUserProfile = function () {
    my.isUpdating = true;
    my.userEditing.dob = $filter('date')(my.userEditing.dob, 'yyyy-MM-dd');
    if (my.IS_EDITMODE) {
        $appEvent('remote:provider:update', {
            index: my.userIndex,
            userInfo: my.userEditing
        });
    }
    else {
        $appEvent('remote:provider:addProvider', my.userEditing);                
    }
}