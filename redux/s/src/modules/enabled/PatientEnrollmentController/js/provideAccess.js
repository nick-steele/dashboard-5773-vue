/*A patient can be assigned to some organation. Each patient will have a primary 
organization and can  have multiple secondary organization. It can be updated and modified*/
my.provideConfig = $model.get('app').settings.apps.patient.enrollment.manageProvider;

my.disableChips=[];
var payload={};

my.provideSaveDisable=true;

//actioned on remove of chips from typeahead
my.remove=function(item,index,event){
    GWT.activity();
    my.provideSaveDisable=false;
    my.errorSaveProvider='';

    my.keyCode=event.keyCode;

    my.disabled =false;

    angular.forEach(my.organizationData.dynamicOrganizationSelected,function(value,key){

        if(value.id===item.id){
            my.organizationData.dynamicOrganizationSelected.splice(key,1);
        }

    })

}

my.providerTextChange=function(){

document.querySelector('.md-autocomplete-suggestions-container').style.width='30%'

}


//assign the onload data to the UI when page is loaded
my.providerSuccess=function(data){
        data.secondaryOrganizationSelected.forEach(function(val) {
            val.typeColor = 'secondary';
        });
        data.primaryOrganizationSelected.typeColor = 'primary';
        my.providerSelected=data.dynamicOrganizationSelected.concat(data.secondaryOrganizationSelected).concat(my.organizationData.autoConsentedOrganizationList);
        my.providerSelected.push(data.primaryOrganizationSelected);
        my.providerSelected=removeDuplicates(my.providerSelected,'name');

        my.wizardStep5.$setPristine();

}


/*it restricts the removal of chips on backspace*/
my.onMpaRemove=function(chip,event){
  my.provideSaveDisable=false;
  my.errorSaveProvider='';

    if(my.keyCode==8){
      my.providerSelected.push(chip);
    }

}

/**
 * Called by md-chips directive. Called when user prompts to add new provider.
 * Per official doc you can define funciton to return `undefined` to add the item as is and `null` to prevent adding.
 * Using such functionality this function handles adding grouped providers and preventing duplication of providers.
 * @param item
 * @return {null|undefined|Object}
 */
my.onProviderAdd=function(item){

    my.provideSaveDisable=false;
    my.errorSaveProvider='';
    GWT.activity();

    if(!item) return null;
    if(item.type && item.type.toLowerCase()==='group'){// If added item is group item
        if(!item.organizationList) return null;
        for(var i=0; i < item.organizationList.length; i++){
            var groupItem = item.organizationList[i];
            // If it already exists in list, skip
            if(!groupItem || itemExists(my.providerSelected, groupItem)) continue;
            // If it doesn't exist in list, add it
            my.organizationData.dynamicOrganizationSelected.push(groupItem);
            my.providerSelected.push(groupItem)
        }
        return null;
    }else if(!itemExists(my.providerSelected, item)){ // If added item does not exist in the list, add it
        my.providerSelected.push(item);
        my.organizationData.dynamicOrganizationSelected.push(item);
        return; // add the item as is
    }else{
        // If added item already exists in the list, don't add
        return null;
    }

    /**
     * Helper function to find out if added provider already exists in the list.
     * @param list
     * @param item
     * @return {boolean} true if item already exists in the list. Checks by item.id.
     */
    function itemExists(list, item){
        for(var i=0;i<list.length;i++){
            if(!list[i] || list[i].id!==item.id) continue;
            return true;
        }
        return false;
    }

};
my.mpaSuccess=function(date){
    my.stepScopes.org.status=$gsiSaveButton.SUCCESS;
    my.wizardStep5.$setPristine();
    my.provideSaveDisable=true;

}

my.providerLocalSearch = function(query){
    var isGroupDisabled = my.allConfigData.groupEnabled.toLowerCase()=="false";

    var list = isGroupDisabled ? my.organizationData.allOrganizationList.filter(groupFilter) :  my.organizationData.allOrganizationList;

    if(!query) return list;

    return list.filter(createFilterForProviderSearch(query));
};

//FIXME group should be filtered on server side... not UI
//FIXME this should be typeahead dynamic search
function groupFilter(item) {
    return item && item.type && item.type.toLowerCase() !== 'group';
}

function createFilterForProviderSearch(query) {
    var lowercaseQuery = angular.lowercase(query);

    return function filterFn(state) {
        return (state.name.toLowerCase().indexOf(lowercaseQuery) !== -1);
    };

}




