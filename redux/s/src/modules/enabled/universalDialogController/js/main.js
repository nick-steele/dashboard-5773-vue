// Single dialog controller for all dialogs...
function universalDialogController($scope, $mdDialog, $model, $log, $appEvent) {

    var self = this;
    // Capture the application dialogData...
    $scope.data = $model.get('app').dialog.data;

    // Make the right scope available to the dialog...
    $scope.scope = $model.get('app').dialog.scope && $model.get( $model.get('app').dialog.scope ) || {};

    // Choices get invoked here...
    $scope.choose = function(choice) { $mdDialog.hide(); $model.get('app').dialog.choice = choice; };
	
	// Allow running a function on dialog render...
	$scope.boot = $model.get('app').dialog.boot || angular.noop;

	$scope.stateBoot = $model.get('app').dialog.stateBoot;

    // Try to run the boot function if it exists (evalAsync makes it happen after the view has been rendered)...

	$scope.$evalAsync(function() { 
		try{
			$scope.boot();
            (function(ev){return ev && eval(ev) || angular.noop;}).call(self,$scope.stateBoot);
        } catch(e) {
            // It wasn't a function or the function failed.
			$log.error(e);
		}
	});
}
