// This comes from the client database: connect.configuration... (populated by a REST call when Node boots)
var userURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.user');

// UI event worked...
var success = function(data) {
    s.toClient(socket, 'a', 'Please wait...');
    s.toClient(socket, 'patch', {what: 'reboot'} );
}

// UI event failed...
var failure = function(data) {
    s.toClient(socket, 'a', 'it didnt work:' + data);    
}

var obj = { Map: { userId: socket.clientData.login.userId.toString(), userInterfaceType: "1" } };

switch(data.event) {
	case 'goRedux':
            // Check if they are logged in...
            if (typeof socket.clientData.login !== 'undefined') {
                obj.Map.userInterfaceType = 1;
                s.REST('POST', userURL + 'update', obj, success, failure);
            } else
                failure('login first');
            break;
	case 'goClassic':
            // Check if they are logged in...
            if (typeof socket.clientData.login !== 'undefined') {
                obj.Map.userInterfaceType = 0;
                s.REST('POST', userURL + 'update', obj, success, failure);
            } else
                failure('login first');
            break;
}