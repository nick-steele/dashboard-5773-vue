var patientUrl = s.getConfiguration(socket.clientData.login.communityId, 'patient.list.server') +'/' ;

var onErrorFn = function (args) {
    var pushFunc = {
        scope:  'myPatientList',
        func:   'onError',
        args:   args
    };
    s.toClient(socket, 'f', pushFunc );
};

var pushRET = function(name, endpoint, func, child ,transformFunc,base) {
    // Make the rest call..

	s.restful.rest('GET', endpoint, function(response) {
		var pushFunc;
		if (typeof child!='undefined'&& !response) {
			console.error("tried to get "+child+" from response but response was empty:", response);
			pushFunc = {
				scope:  'myPatientList',
				func:   func,
				args:   null
			};
		} else {
			if (typeof response == 'undefined') response = true;
			else response =  (typeof child == 'undefined'||child==null) ? response : response[child];

			if (typeof transformFunc == 'function') {
				response = transformFunc.call(this,base,response);
			}
			pushFunc = {
				scope:  'myPatientList',
				func:   func,
				args:   response
			};
			var json = JSON.stringify(pushFunc.args);
			var dataRep = (json.length > 40)?json.length + ' bytes' : json;
			trace('pushRET'+ func+ dataRep+ pushFunc.scope+ pushFunc.func);
			// Push the function call to the client, in Angular, in the compiled app controller, there is eventServer(event) that handles server pushes.
			// There is a switch that handles 'd' responses, which are data pushes, it magically falls into just the right place...
		}
		s.toClient(socket, 'f', pushFunc );
    }, function(data) {
		onErrorFn('Problem with ' + name + '(' + data + '), RET error: '+func);
    }, name);
};

var pushPOST = function(name, endpoint, payload, func, child ,transformFunc,base) {
    s.restful.rest("POST", endpoint, payload, function(response) {

        var pushFunc;
        if(typeof child!='undefined'&& !response){
            trace("tried to get "+child+" from response but response was empty:");
            pushFunc = {
                scope:  'myPatientList',
                func:   func,
                args:   null
            };
        } else  {
            if (typeof response == 'undefined') response = true;
            else response =  (typeof child == 'undefined'||child==null) ? response : response[child];

            if (typeof transformFunc == 'function'){
                response = transformFunc.call(this,base,response);
            }
            pushFunc = {
                scope:  'myPatientList',
                func:   func,
                args:   response
            };
            var json = JSON.stringify(pushFunc.args);
            var dataRep = (json.length > 40)?json.length + ' bytes' : json;
            trace('pushRET'+ func+ dataRep+ pushFunc.scope+ pushFunc.func);
            // Push the function call to the client, in Angular, in the compiled app controller, there is eventServer(event) that handles server pushes.
            // There is a switch that handles 'd' responses, which are data pushes, it magically falls into just the right place...

        }

        s.toClient(socket, 'f', pushFunc );
    }, function(data) {
        trace('error:' + data);
        onErrorFn('Problem with ' + name + '(' + data + '), SET error: '+func);
    }, name);
};



var pushSET = function(name, method, endpoint, payload, scope,  func) {

    s.restful.rest(method, endpoint, payload, function(data) {
        if(func !== 'undefined') {
            var pushFunc = {
                scope:  scope,
                func:   func,
                args:   payload
            };

            s.toClient(socket, 'f', pushFunc);
        }
    }, function(data) {
        trace('error:' + data);
        onErrorFn('Problem with ' + name + '(' + data + '), SET error: '+func);
    }, name);
};


var pushGET = function(name, endpoint, localName, subVar ) {

    s.restful.rest('GET', endpoint, function(data) {

        var pushModel = {myPatientList: {}};
        pushModel.myPatientList[localName] = (typeof subVar === 'undefined') ? data : data[subVar];
        trace(pushModel);
        s.toClient(socket, 'd', pushModel);
    }, function(data) {
        trace(endpoint+'\n error:' + data);
        onErrorFn('Problem with ' + name + '(' + data + '), GET error: '+localName);
    }, name);
};

var pushDELETE = function(name, endpoint, scope,  func) {

    s.restful.rest('DELETE', endpoint, function(data) {
        if(func !== 'undefined') {
            var pushFunc = {
                scope:  scope,
                func:   func
            };

            s.toClient(socket, 'f', pushFunc);
        }
    }, function(data) {
        trace('error:' + data);
        onErrorFn('Problem with ' + name + '(' + data + '), SET error: '+func);
    }, name);
};

//copied from appHome
var getMessageBundle = function (homeModel) {
    var pushModel = {myPatientList: {}};
    if (s.getConfiguration(socket.clientData.login.communityId, 'homepage.message.bundle')) {
        pushModel.myPatientList[homeModel] = JSON.parse(s.getConfiguration(socket.clientData.login.communityId, 'homepage.message.bundle'));
    }
    s.toClient(socket, 'd', pushModel);
};



var communityId,userId;
switch(data.event){

    case "get":
        userId = socket.clientData.login.userId;
        communityId = socket.clientData.login.communityId;
        var page = data.payload.page;
        var pCount =data.payload.pCount || 1;
        var size = data.payload.size;
        var tags = data.payload.tags.length!==0?data.payload.tags.join(","):'all';
        var start =  page*size,
            end = start+size*pCount-1;
        var timestamp = data.payload.timestamp;
        var getCount = data.payload.getCount;
        var keyword = data.payload.search;
        var mode =  data.payload.mode || 'my';

        var countApi,countEvName,listApi,listEvName;
        if (mode !== 'sub') {
            var type=data.payload.type;
            if(type === 'org' && socket.clientData.login.canViewOrgPP !== true){
                //FIXME: better way of handling unauthorized access
                onErrorFn('You are not allowed to access this panel.');
                break;
            }
            countApi = patientUrl + "count/" + userId + "/" + communityId + "/" + type + "/" + tags;
            countEvName = "patientListCount";
            listApi = patientUrl + userId + "/" + communityId + "/" + start + "/" + end + "/" + type + "/" + tags;
            listEvName = "patientListNewPageRet";

        } else {
            var subId = data.payload.type;
            countApi = patientUrl + "count/" + userId + "/" + communityId + "/team/" + subId + "/" + tags;
            countEvName = "subPatientListCount";
            listApi = patientUrl + userId + "/" + communityId + "/team/" + subId + "/" + start + "/" + end + "/" + tags;
            listEvName = "subPatientListNewPageRet";
        }

        if(keyword && keyword.length>0){
            var body = {"List":[{search:keyword}]};
            if(getCount){
                pushPOST(countEvName+'POST', countApi,
                    body,"processTotalCount",null,function(base, data){base.count=data;return base},{timestamp:timestamp});
            }
            pushPOST(listEvName+'POST', listApi,
                body,"processNewPage","List",function(base, data){base.list=data;return base},{page:page, timestamp:timestamp, pCount: pCount});

        }else{
            if(getCount){
                pushRET(countEvName, countApi, "processTotalCount",null,function(base, data){base.count=data;return base},{timestamp:timestamp});
            }
            pushRET(listEvName, listApi,"processNewPage","List",function(base, data){base.list=data;return base},{page:page, timestamp:timestamp, pCount: pCount});

        }



    break;
    case "tags":
        communityId = socket.clientData.login.communityId;
        pushGET('patientListTags',patientUrl+"tags/"+communityId,"tags","List");
        getMessageBundle('messageBundle');
        break;
    case 'add':
        var data = {
            MyPatientInfoDTO: {
                userId: socket.clientData.login.userId,
                patientId: data.payload.MyPatientInfoDTO.patientId ,
                communityId: socket.clientData.login.communityId,
                flagged: "N"
            }
        };
        pushSET('patientListAdd', 'POST', patientUrl + "addWithManualTag/", data, 'home', 'updatePatientList');
        break;
    case 'remove':
        var patientId = data.payload.patientId,
            userId = socket.clientData.login.userId;
            communityId = socket.clientData.login.communityId;
        pushDELETE('patientListRemove', patientUrl+'delete/'+userId+"/"+communityId+"/"+patientId,'myPatientList', 'reloadAfterRemoveOrAdd');

        break;
    case 'removeFromPopup':
        var patientId = data.payload.patientId,
            userId = socket.clientData.login.userId;
        communityId = socket.clientData.login.communityId;
        pushDELETE('patientListRemove(fromPopup)', patientUrl+'delete/'+userId+"/"+communityId+"/"+patientId,'home', 'updateRemovedPatient');

        break;

}
