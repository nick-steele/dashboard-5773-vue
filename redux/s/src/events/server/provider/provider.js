// configuration values
var careTeamURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.careteam');
var userId = socket.clientData.login.userId;
var userEmail = socket.clientData.login.email;
var commId = socket.clientData.login.communityId;

var transform = function(data) {
    return data;
};
var transformEditUser = function (index, userInfo) {
    return {
        index: index,
        userInfo: userInfo
    }
};
var transformAddUser = function (userInfo) {
    return userInfo;
};
var onAddUpdateError = function (error) {
    var pushFunc = {
        scope:  'patientEnrollment',
        func:   'onAddUpdateError',
        args:   error
    };
    s.toClient(socket, 'f', pushFunc);
};
var getData = function(name, endpoint, modelName, subVar, transformFn, callbackToClient, opts) {
    s.restful.rest('GET', endpoint, function(data) {
        console.log(data);
        transformFn = transformFn || transform;
        var modelData = transformFn((!subVar) ? data : data[subVar]);
        if (modelName)
            backToClient(modelData, modelName);
        if (callbackToClient) {
            var pushFunc = {
                scope:  'patientEnrollment',
                func:   callbackToClient,
                args:   modelData
            };
            s.toClient(socket, 'f', pushFunc);
        }
    }, function(data) {

    }, name, opts);
};
var pushData = function(method, name, endpoint, modelName, subVar, payload, transformFn, callbackToClient, onErrorFn, opts) {
    s.restful.rest(method, endpoint, payload, function(data) {
        transformFn = transformFn || transform;
        var modelData = transformFn((!subVar) ? data : data[subVar]);
        if (modelName)
            backToClient(modelData, modelName);
        if (callbackToClient) {
            var pushFunc = {
                scope:  'patientEnrollment',
                func:   callbackToClient,
                args:   modelData
            };
            s.toClient(socket, 'f', pushFunc);
        }
    }, function(code, data) {
        onErrorFn(data);
    }, name, opts);
};
var backToClient = function(data, modelName) {
    var pushModel = { patientEnrollment: {} };
    pushModel.patientEnrollment[modelName] = data;
    s.toClient(socket, 'd', pushModel);
};

var onError = function(modelName) {
    var pushModel = { patientEnrollment: {} };
    pushModel.patientEnrollment[modelName] = {
        isError: true,
        patients: [],
        isLoading: false
    };
    s.toClient(socket, 'd', pushModel);
};

// careTeamURL = 'http://10.153.1.16:9090';
switch (data.event) {
    case 'getMetaData':
        getData('getMetaData', careTeamURL + '/configurations/' + commId + '/genders', 'genders', null, null, null, {secured: true});
        getData('getMetaData', careTeamURL + '/configurations/' + commId + '/states', 'states', null, null, null, {secured: true}); 
        getData('getMetaData', careTeamURL + '/configurations/' + commId + '/prefixes', 'prefixes', null, null, null, {secured: true}); 
        getData('getMetaData', careTeamURL + '/configurations/' + commId + '/credentials', 'credentials', null, null, null, {secured: true});
        getData('getMetaData', careTeamURL + '/configurations/' + commId + '/roles', null, null, null, 'getRolesCallBack', {secured: true});
        break;
    case 'addProvider':
        pushData('POST', 'addProvider', careTeamURL + '/providers/' + commId, null, null, data.payload, transformAddUser.bind(null, data.payload), 'addProviderCallback', onAddUpdateError, {secured: true});
        break;
    case 'update':
        var providerId = data.payload.userInfo.userId;
        pushData('PUT', 'updateProvider', careTeamURL + '/providers/' + commId + '/' + providerId, null, null, data.payload.userInfo, transformEditUser.bind(null, data.payload.index, data.payload.userInfo), 'updateUserCallback', onAddUpdateError, {secured: true});        
        break;
}