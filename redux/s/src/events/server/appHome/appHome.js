// configuration values
var taskingURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.tasks');
var patientURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.patient');
var careplanURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.careplan');
var alertURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.alert');

var userURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.user');

var onErrorFn = function (type, msg) {
    var pushFunc = {
        scope:  'home',
        func:   'onError',
        args:   [type, msg]
    };
    s.toClient(socket, 'f', pushFunc );
};

var onSuccessFn = function (type) {
    var pushFunc = {
        scope:  'home',
        func:   'onSuccess',
        args:   [type]
    };
    s.toClient(socket, 'f', pushFunc );
};

var pushGET = function (name, endpoint, modelName, transform, addData) {
    s.restful.rest('GET', endpoint, function (data) {
        var modelData = transform(data, addData);
		if (modelName instanceof Array) {
			var modelArray = [];
            modelName.forEach(function (name) {
				var pushModel = {home: {}};
				pushModel.home[name] = modelData[name];
                modelArray.push(pushModel);
		        //s.toClient(socket, 'd', pushModel);
			});
            s.toClient(socket, 'arrayData', modelArray);
		} else {
			var pushModel = {home: {}};
			pushModel.home[modelName] = modelData;
	        s.toClient(socket, 'd', pushModel);
		}
        onSuccessFn(name);
    }, function (data) {
        onErrorFn(name, 'Problem with ' + name + '(' + data + ')');
    }, name);
};


var pushData = function (name, method, endpoint, modelName, transform, addData, payload) {
    if(method === 'GET') {
        s.restful.rest('GET', endpoint, function (data) {
            processReceivedData(transform, modelName, data, addData);
            onSuccessFn(name);
        }, function (data) {
            onErrorFn(name, 'Problem with ' + name + '(' + data + ')');
        }, name);
    }
    else {
        s.restful.rest(method, endpoint, payload, function (data) {
            processReceivedData(transform, modelName, data, addData);
            onSuccessFn(name);
        }, function (data) {
            onErrorFn(name, 'Problem with ' + name + '(' + data + ')');
        }, name);
    }

};

var processReceivedData = function(transform, modelName, data, addData) {
    var modelData = transform(data, addData);
    if (modelName instanceof Array) {
        var modelArray = [];
        modelName.forEach(function (name) {
            var pushModel = {home: {}};
            pushModel.home[name] = modelData[name];
            modelArray.push(pushModel);
        });
        s.toClient(socket, 'arrayData', modelArray);
    } else {
        var pushModel = {home: {}};
        pushModel.home[modelName] = modelData;
        s.toClient(socket, 'd', pushModel);
    }
}

var pushSET = function (name, method, endpoint, payload, homeModel, subVar, func) {
    s.restful.rest(method, endpoint, payload, function (data) {
        var pushModel = {home: {}};

        pushModel.home[homeModel] = (typeof subVar === 'undefined') ? data : data[subVar];
        s.toClient(socket, 'd', pushModel);
        if (func !== 'undefined') {
            var pushFunc = {
                scope: 'home',
                func: func,
                args: (typeof subVar === 'undefined') ? data : data[subVar]
            };
            s.toClient(socket, 'f', pushFunc);
        }

        onSuccessFn(name);
    }, function (data) {
        onErrorFn(name, 'Problem with ' + name + '(' + data + ')');
    }, name);
};

var getPatientList = function (model, subVar, func) {
    var commId = socket.clientData.login.communityId;
    var userId = socket.clientData.login.userId;
    var url = patientURL + "patientInfo/demog/" + commId + "/" + userId;
    pushSET('PatientList', 'POST', url, data.payload, model, subVar, func);
};

var searchPatientList = function (model, subVar, func) {
    var commId = socket.clientData.login.communityId;
    var userId = socket.clientData.login.userId;
    var email = socket.clientData.login.email;

    var search = data.payload.searchText;

    if (typeof search !== 'undefined') {
        //Make special characters become invalid lucene search characters
        search = search.trim().replace(/[^\w\s']/gi, '^#');
    }
    search = encodeURIComponent(search);

    var url = patientURL + "patientInfo/demog/search/" + commId + "/" + search + '/' + userId + "/" + email;
    pushSET('searchPatientList', 'POST', url, {patients: data.payload.patients}, model, subVar, func);
};


var getMessageBundle = function (homeModel) {
    var pushModel = {home: {}};
    if (s.getConfiguration(socket.clientData.login.communityId, 'homepage.message.bundle')) {
        pushModel.home[homeModel] = JSON.parse(s.getConfiguration(socket.clientData.login.communityId, 'homepage.message.bundle'));
    }
    s.toClient(socket, 'd', pushModel);

};

// build REST url
var buildTaskCountUrl = function (commId, email, type, interval) {
    return taskingURL + 'taskreport/' + email + '/' + commId + '/' + interval;
};

var buildAlertCountUrl = function (commId, userId, type, interval) {
    return alertURL + 'alertreport/report/' + userId + '/' + commId + '/' + interval + '/' + type;
};

var buildEncounterCountUrl = function (commId, userId, type, interval) {
    return careplanURL + "careplanreport/report/" + userId + "/" + commId;
};
var builNoEncouterCountUrl = function (commId, userId, type, interval) {
    return careplanURL + "careplan/nocount/" + userId + "/" + commId;
};

// transform REST data to chart data
var transformTasks = function (data, addData) {
    // transform data from REST to task count

    var taskData = {
		'todoTask': {
			'count': data.TaskReport.todoTask.axes[1].max,
			'tasks': data.TaskReport.todoTask.tasks,
			'email': addData.email,
			'interval': addData.interval
		},
		'overdueTask': {
			'count': data.TaskReport.overdueTask.axes[1].max,
			'tasks': data.TaskReport.overdueTask.tasks,
			'email': addData.email,
			'interval': addData.interval
		},
		'cpNonassessTask': {
			'count': data.TaskReport.ipcpnonAssessTask.axes[1].max,
			'tasks': data.TaskReport.ipcpnonAssessTask.tasks,
			'email': addData.email,
			'interval': addData.interval
		},
		'cpAssessTask': {
			'count': data.TaskReport.ipcpassessTask.axes[1].max,
			'tasks': data.TaskReport.ipcpassessTask.tasks,
			'email': addData.email,
			'interval': addData.interval
		}
	};

    return taskData;
};

var transformAlerts = function (data, addData) {
    // transform data from REST to alert count
    var severity = {
        CRITICAL: 1,
        HIGH: 2,
        MEDIUM: 3,
        LOW: 4
    };
    var baseColor = 0xff0000;
    var offSet = 0x004000;
    var chartData = [];
    var xValues = data.chart.axes[0].values;
    var yValues = data.chart.axes[1].values;
    var patients = data.chart.patients;
    //add color base on severity

    for (var i = 0; i < xValues.length; i++) {
        chartData = chartData.concat({
            label: xValues[i],
            value: yValues[i],
            color: "#" + (baseColor + offSet * (severity[xValues[i].toUpperCase()] - 1)).toString(16),
            patients: patients[i]
        })
    }

    //need to sort severity
    chartData = chartData.filter(function (item) {
        return item.value > 0;
    }).sort(function (a, b) {
        return (severity[a.label] - severity[b.label]);
    });

    var result = {
        chartData: chartData,
        alertList: data.chart.data? data.chart.data[0] : []
    };
    return result;
};

var transformEncounters = function (data, addData) {
    var labels = data.chart.axes[0].values;
    var values = data.chart.axes[1].values;
    var patients = data.chart.patients;
    var xLabel = data.chart.axes[0].name;
    var yLabel = data.chart.axes[1].name;
    var serviceList = data.chart.data? data.chart.data[0]:[];
    var data = [{}];
    data[0].key = "Encounters";
    data[0].values = [];

    for (var i = 0; i < labels.length; i++) {
        var item = {};
        item.label = labels[i];
        item.value = values[i];
        item.patients = patients[i];
        data[0].values[i] = item;
    }

    var encounterModel = {};
    encounterModel.data = data;
    encounterModel.xLabel = xLabel;
    encounterModel.yLabel = yLabel;
    encounterModel.serviceList = serviceList;
    return encounterModel;
};

var transformNoEncounters = function (data, addData) {
    return data;
};

var loadChartData = function (buildUrl, transform, model, name, payload) {
    // get parameters for REST url
    var email = '';
    if (data.payload.email && data.payload.email.indexOf(';') > 0) {
        email = socket.clientData.login.email;
    }
    else {
        email = data.payload.email ? data.payload.email : socket.clientData.login.email;
    }


    var commId = socket.clientData.login.communityId;

    var type = data.payload.type;
    var interval = data.payload.interval;
    var team = (data.payload.email && data.payload.email.indexOf(';') > 0) ? 'team' : data.payload.email ? data.payload.email : socket.clientData.login.email;
    var addData = {
        email: email,
        commId: commId,
        type: type,
        interval: interval,
        team: team
    };

    if (name === 'CarePlan' || name === 'NoEncounter' || name === "Alert") {
        email = socket.clientData.login.userId;
    }

    // build url
    var url = buildUrl(commId, email, type, interval);

    // call REST service, transform and push data to socket

    if (name === 'CarePlan' || name === 'NoEncounter') {
        var requestObj = {};
        requestObj.reportRequest = {};
        requestObj.reportRequest.entity = 'ENCOUNTER';
        requestObj.reportRequest.groupBy = 'SERVICE';
        requestObj.reportRequest.period = interval;
        requestObj.reportRequest.type = type;
        requestObj.reportRequest.subordinateIds = payload.userIds;

        payload = {};
        payload = requestObj;
    }

    if(payload === undefined) {
        pushData(name, 'GET', url, model, transform, addData);
    }
    else {
        pushData(name, 'POST', url, model, transform, addData, payload);
    }
}

var getUserIds = function(users) {
    var userIds = [];
    if(users !== undefined) {
        users.forEach(function(user) {
            userIds.push(user.userId)
        });
    }
    return userIds;
}

var getUserEmails = function(users) {
    var userEmails = [];
    if(users !== undefined) {
        users.forEach(function(user) {
            userEmails.push(user.email)
        });
    }
    return userEmails;
}


switch (data.event) {
    case "boot":
        //appBoot();
        break;
    case "loadTaskCount":
        var userList = [];
        if(data.payload.type === 3) {
            userList = getUserEmails(data.payload.userList);
        }
        else {
            userList.push(socket.clientData.login.email);
        }

        /*if (data.payload.email && data.payload.email.indexOf(';') > 0) {
            email = socket.clientData.login.email;
            userList =  data.payload.email.split(/;/);
        }
        else {
            var userEmail = data.payload.email ? data.payload.email : socket.clientData.login.email;
            userList.push(userEmail);
        }*/


        var payload = {users: userList};
		    loadChartData(buildTaskCountUrl, transformTasks, ['todoTask',
			     'overdueTask',
			     'cpNonassessTask',
			     'cpAssessTask',
           'pieTaskChart',
           'barTaskChart'], 'TaskBadge', payload);
        break;
    case "loadAlertCount":
        //console.log(socket.clientData.login.email);
        var userIds = getUserIds(data.payload.userList);
        loadChartData(buildAlertCountUrl, transformAlerts, 'alertChartData', 'Alert', {users: userIds});
        break;
    case "loadEncounterCount":
        //console.log(socket.clientData.login.email);
        var userIds = getUserIds(data.payload.userList);
        loadChartData(buildEncounterCountUrl, transformEncounters, 'encounters', 'CarePlan', {userIds: userIds});
        break;
    case "loadNoEncounterCount":
        var userIds = getUserIds(data.payload.userList);
        loadChartData(builNoEncouterCountUrl, transformNoEncounters, 'patientNoEncounter', 'NoEncounter', {userIds: userIds});
        break;
    case "loadPatientList":
        getPatientList('alertPatientList', 'PatientList', 'loadPatientList');
        break;
    case "searchPatientList":
        searchPatientList('searchPatientList', 'SearchResults', 'searchPatientListCallBack');
        break;
    case "getMessageBundle":
        getMessageBundle('messageBundle');
        break;

}
