

var orgUrl = s.getConfiguration(socket.clientData.login.communityId, 'organization.rest.url') + '/';
var groupEnabled = s.getConfiguration(socket.clientData.login.communityId, 'organization.group.enabled').toLowerCase();
const rhioUrl = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.rhio');
const patientServiceMTUrl = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.patient.service.mt');
const programUrl = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.program');


const patientManagerUrl = s.getConfiguration(socket.clientData.login.communityId, 'patient.manager.url');
var isNameRequired = s.getConfiguration(socket.clientData.login.communityId, 'careteam.name.mandatory');
var showDirect=s.getConfiguration(socket.clientData.login.communityId, 'patient.engagement.enable');
var disableMinor=s.getConfiguration(socket.clientData.login.communityId, 'disable.minor.direct.messaging');
var directAge=s.getConfiguration(socket.clientData.login.communityId, 'minor.age');
var patientUri=s.getConfiguration(socket.clientData.login.communityId, 'patient.list.server');


var careTeamUrl = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.careteam') + '/';

var globalUri = orgUrl + 'organizationrest/';


var pushCleanGET = function (name, endpoint, localName, prop) {

    s.restful.rest('GET', endpoint, function (data) {

        data.groupEnabled = groupEnabled;

        var pushModel = {patientEnrollment: {}};

        pushModel.patientEnrollment[localName] = (typeof prop !== 'undefined') ? data[prop] : data;
        trace(pushModel);
        s.toClient(socket, 'd', pushModel);

    }, function (data) {
    }, name);
};



var pushGET = function () {
    var myData = {};

    myData.groupEnabled = groupEnabled;
    myData.isNameRequired = isNameRequired;
    myData.showDirect=showDirect;
    myData.disableMinor=disableMinor;
    myData.directAge=directAge;

    var pushModel = {patientEnrollment: {}};
    pushModel.patientEnrollment['allConfigData'] = myData;
    trace(pushModel);
    s.toClient(socket, 'd', pushModel);

};

var pushCareGET = function (name, endpoint, localName) {



    s.restful.rest('GET', endpoint, function (data) {

        var pushModel = {patientEnrollment: {}};
        pushModel.patientEnrollment[localName] = data;
        trace(pushModel);
        s.toClient(socket, 'd', pushModel);

    }, function (data) {

    }, name, {secured: true});
};



var pushSET = function (name, method, endpoint, payload, scope, func) {
    var err = saveErrorHandler('org')
    var success = saveSuccessHandler('onOrgSuccess')


    delete payload['uriEndpoints'];

    s.restful.rest(method, endpoint, payload, success, err, name, {secured: true});
};


var getSuccessHandler = function (prop, scope) {
    var _prop = prop;
    scope = scope || 'patientEnrollment';
    return function (data) {
        var pushModel = {};
        pushModel[scope] = {};
        pushModel[scope][_prop] = data;
        trace(pushModel);
        s.toClient(socket, 'd', pushModel);
    }
};


var getErrorHandler = function (errorProp) {
    var _errorProp = errorProp;
    return function (status, data) {
        data && s.log('failure', JSON.stringify(data));
        var _obj = {patientEnrollment: {}};
        _obj.patientEnrollment[_errorProp] = true;
        s.toClient(socket, 'd', _obj);
    }
};

var saveErrorHandler = function (type) {
    var _type = type;
    return function (status, data) {
        s.log('save failed');
        data && s.log('mssg ', JSON.stringify(data));
        var errorMessage = data && data.clientMessage || 'An error occurred while processing your request.';
        var _obj = {
            scope: 'patientEnrollment',
            func: 'onSaveError',
            args: {type: _type, message: errorMessage}
        };
        s.toClient(socket, 'f', _obj);
    };
};

var saveSuccessHandler = function (func) {
    var _func = func;
    return function (data) {
        console.log('success');
        trace('save success');
        var _obj = {
            scope: 'patientEnrollment',
            func: _func,
            args: data
        };
        s.toClient(socket, 'f', _obj);
    }
}



var communityId = socket.clientData.login.communityId;
var uri, userId, successFn, errFn, endpoint;




switch (data.event) {
    case "getDemog":
        pushCleanGET('getPatientDemog',
                patientManagerUrl + communityId + '/' + data.payload,
                "patientDemog",
                "PatientDemogs"
                );
        break;

    case 'validateEmail':

        communityId = socket.clientData.login.communityId;
        userId = socket.clientData.login.userId;

        pushGET();

        successFn = saveSuccessHandler('emailSuccess');
        errFn = saveErrorHandler('searchFailed');
        endpoint = careTeamUrl + 'careteam/patientemail/' + communityId + '/' + data.payload.patientId;

        s.restful.rest('GET', endpoint, successFn, errFn, 'emailSuccess', {secured: true});
        break;

    case 'provideData':

        communityId = socket.clientData.login.communityId;
        userId = socket.clientData.login.userId;

        pushGET();

        successFn = saveSuccessHandler('providerSuccess');
        errFn = saveErrorHandler('searchFailed');
        endpoint = globalUri + 'getPatientOrganization/' + communityId + '/' + data.payload.patientId;

        s.restful.rest('GET', endpoint, successFn, errFn, 'getOrSuccess', {secured: true});
        break;

    case 'orgData':

        communityId = socket.clientData.login.communityId;
        userId = socket.clientData.login.userId;

        pushGET();


        successFn = saveSuccessHandler('getOrSuccess');
        errFn = saveErrorHandler('searchFailed');
        endpoint = globalUri + 'getPatientOrganization/' + communityId + '/' + data.payload.patientId;

        s.restful.rest('GET', endpoint, successFn, errFn, 'getOrSuccess', {secured: true});
        break;

    case 'saveOrg':
        data.payload.communityId = communityId;
        break;
    case 'updateOrg':
        data.payload.communityId = communityId;
        userId = socket.clientData.login.userId;

        data.payload.userId = userId;
        var type = data.payload.uriEndpoints == '/addPatientOrganization' ? 'POST' : 'PUT';


        pushSET('patientUpdate', type, globalUri + data.payload.uriEndpoints, data.payload, 'home', 'updatePatientList');

        break;
    case 'updateMpa':
        var type = data.payload.uriEndpoints == '/addPatientOrganization' ? 'POST' : 'PUT';
        successFn = saveSuccessHandler('mpaSuccess');
        errFn = saveErrorHandler('mpafail');
        endpoint = globalUri + data.payload.uriEndpoints;
        data.payload.communityId = communityId;
        data.payload.userId = socket.clientData.login.userId;
        var type = data.payload.uriEndpoints == '/addPatientOrganization' ? 'POST' : 'PUT';
        delete data.payload['uriEndpoints'];


        s.restful.rest(type, endpoint, data.payload, successFn, errFn, 'mpaUpdate', {secured: true});




        break;

    case "getCare":
        userId = socket.clientData.login.userId;

        //pushCareGET('getCare', careTeamUrl + 'careteam/member/' + communityId + '/' + data.payload.patientId, "myCareData");
        
        successFn = saveSuccessHandler('getCareSuccess');
        errFn = saveErrorHandler('getCareFailed');
        endpoint = careTeamUrl + 'careteam/member/' + communityId + '/' + data.payload.patientId;

        s.restful.rest('GET', endpoint, successFn, errFn, 'getCareData', {secured: true});
        break;

    // Searches careteam by keyword
    case "searchCareTeam":{
        let url = `${careTeamUrl}careteam/search/${communityId}`;
        let timestamp = data.payload.timestamp;
        let successFn = (data)=>{
            var _obj = {
                scope: 'patientEnrollment',
                func: "onCareTeamSearchResult",
                args: {data,timestamp}
            };
            s.toClient(socket, 'f', _obj);
        };

        s.restful.rest('POST', url, {keyword: data.payload.keyword}, successFn,
            function(status, data){
                data && s.log('failure', JSON.stringify(data));
            }, 'searchCareTeam', {secured:true});

        break;
    }
    // Get careteam info by id
    case "getCareTeamDetails":{
        let url = `${careTeamUrl}careteam/${communityId}/patient/${data.payload.patientId}/careteam/${data.payload.careTeamId}`;
        s.restful.rest('GET', url,
            getSuccessHandler("myCareData","patientEnrollment"),
            function(status, data){
                data && s.log('failure', JSON.stringify(data));
            }, 'getCareTeamDetails', {secured:true});
        break;
    }
    case "getPrior":  
        successFn = saveSuccessHandler('priorSuccess');
        errFn = saveErrorHandler('searchFailed');
        endpoint = careTeamUrl + 'careteam/priormember/' + communityId + '/' + data.payload.patientId;

        s.restful.rest('GET', endpoint, successFn, errFn, 'priorData', {secured: true});

        break;

    case "getserchResult":
        successFn = saveSuccessHandler('searchSuccess');
        errFn = saveErrorHandler('searchFailed');
        endpoint = careTeamUrl + 'careteam/searchUser/' + communityId + '/' + data.payload.searchParam + '/' + data.payload.patientId;

        s.restful.rest('GET', endpoint, successFn, errFn, 'searchResult', {secured: true});

        break;

    case "getDirectStatus":
        successFn = saveSuccessHandler('directStatusSuccess');
        errFn = saveErrorHandler('searchFailed');
        endpoint = careTeamUrl + 'careteam/directMessagingStatus/' + communityId + '/' + data.payload.patientId;

        s.restful.rest('GET', endpoint, successFn, errFn, 'msgStatus', {secured: true});

        break;

    case "getFilterdData" :{

        var timestamp = data.payload.timestamp;
        var successFn = function (data) {
            var _obj = {
                scope: 'patientEnrollment',
                func: 'searchSuccess',
                args: {timestamp, data}
            };
            s.toClient(socket, 'f', _obj);
        };
        errFn = saveErrorHandler('searchFailed');
        endpoint = careTeamUrl + 'careteam/searchUser/' + communityId + '/' + data.payload.patientId + '/' + data.payload.pageNo + '/' + data.payload.noOfData + '/' + data.payload.searchParam + '/' + data.payload.roleId + '/' + data.payload.consentRequired;

        s.restful.rest('GET', endpoint, successFn, errFn, 'searchResult', {secured: true});

        break;
    }
    case "giveConsent":
        successFn = saveSuccessHandler('consentSuccess');
        errFn = saveErrorHandler('consentfailed');
        endpoint = globalUri + 'addPatientConsentToOrg';
        data.payload.communityId = communityId;
        data.payload.userId = socket.clientData.login.userId;

        s.restful.rest('POST', endpoint, data.payload, successFn, errFn, 'giveConsent', {secured: true});
        break;

    case "validateName":
        successFn = saveSuccessHandler('validate');
        errFn = saveErrorHandler('validateFailed');
        endpoint = careTeamUrl + 'careteam/checkcareteamexist/' + communityId + '/' + data.payload.patientId + '?name=' + encodeURIComponent(data.payload.query);

        s.restful.rest('GET', endpoint, successFn, errFn, 'validateName', {secured: true});

        break;

    case "validateNew":
        successFn = saveSuccessHandler('newSuccess');
        errFn = saveErrorHandler('validateFailed');
        endpoint = careTeamUrl + 'careteam/checkcareteamexist/' + communityId +  '?name=' + encodeURIComponent(data.payload.query);

        s.restful.rest('GET', endpoint, successFn, errFn, 'validateNameNew', {secured: true});

        break;

    case "saveCare":
        successFn = saveSuccessHandler('careSaveSuccess');
        errFn = saveErrorHandler('careSaveFailed');
        endpoint = careTeamUrl + 'careteam/savecareteam';
        data.payload.communityId = communityId;
        console.log('my user id is ')
        console.log(socket.clientData.login.userId);
        data.payload.userId = socket.clientData.login.userId;
        s.restful.rest('POST', endpoint, data.payload, successFn, errFn, 'saveCare', {secured: true});

        break;

    case "getRoles":
        pushCareGET('getCare', careTeamUrl + 'careteam/allroles/' + communityId, "allRoles");
        break;

    case "getRequiredRoles" :

        pushCareGET('getCare', careTeamUrl + 'careteam/requiredroles/' + communityId, "requiredroles");
        break;

    case "getDuplicate" :

        pushCareGET('getCare', careTeamUrl + 'careteam/duplicateroles/' + communityId, "duplicateroles");
        break;


    case "getRhio":
    {
        successFn = saveSuccessHandler('onGetRhioSuccess');
        errFn = getErrorHandler('getRhioError');
        endpoint = rhioUrl + '/patients/rhio/' + data.payload + '/' + communityId;
        s.restful.rest('GET', endpoint, successFn, errFn, 'getPatientRhio', {secured: true});

        // getRhioError
        break;
    }
    case "saveRhio":

        successFn = saveSuccessHandler('onSaveRhioSuccess');
        errFn = saveErrorHandler('rhio');

        endpoint = rhioUrl + '/patients/rhio/' + communityId + '/' + data.payload.patientId;
        s.restful.rest('PUT', endpoint, data.payload.data, successFn, errFn, 'savePatientRhio', {secured: true});

        break;
    case "getProgramList":
    {

        var successFn = getSuccessHandler('programList');
        var errFn = getErrorHandler('getProgramListError');
        var endpoint = `${programUrl}/programs/patient/${data.payload}`;
        s.restful.rest('GET', endpoint, successFn, errFn, 'getPrograms', {secured: true});
        break;
    }
    case 'getConsentList':
        var successFn = getSuccessHandler('consentList', 'app');
        var errFn = getErrorHandler('getConsentListError');
        var endpoint = `${programUrl}/programs/levels`;
        s.restful.rest('GET', endpoint, successFn, errFn, 'getConsents', {secured: true});
        break;
    case "getPatientPrograms":
    {

        var successFn = function (data) {
            var _obj = {
                scope: 'patientEnrollment',
                func: 'onGetPatientPrograms',
                args: data
            };
            s.toClient(socket, 'f', _obj);
        };
        var errFn = getErrorHandler('getPatientProgramsError');
        var endpoint = `${programUrl}/patients/${data.payload}/programs`;
        s.restful.rest('GET', endpoint, successFn, errFn, 'getPatientPrograms', {secured: true});

        // getRhioError
        break;
    }
    case "getPatientProgramHistory":{

        var endpoint = `${programUrl}/patients/${data.payload.patientId}/programs/history?start=${data.payload.start}`;
        var errFn = getErrorHandler('getPatientProgramHistoryError');
        var timestamp = data.payload.timestamp;

        var successFn = function (data) {
            var _obj = {
                scope: 'patientEnrollment',
                func: 'onProgramHistoryData',
                args: {timestamp, data}
            };
            s.toClient(socket, 'f', _obj);
        };

        s.restful.rest('GET', endpoint, successFn, errFn, 'getPatientProgramHistory', {secured: true});
        break;
    }
    case "getProgramDeleteReasons":
    {

        var successFn = getSuccessHandler('programDeleteReasons', 'app');
        var errFn = getErrorHandler('getProgramDeleteReasonError');
        var endpoint = `${programUrl}/programs/deleteReasons`;
        s.restful.rest('GET', endpoint, successFn, errFn, 'getProgramDeleteReasons', {secured: true});

        break;

    }
    case "savePatientPrograms":
    {
        var successFn = saveSuccessHandler('onSavePatientProgramsSuccess');
        var errFn = saveErrorHandler('program');
        var userId = socket.clientData.login.userId;
        endpoint = `${programUrl}/patients/${data.payload.patientId}/programs`;
        s.restful.rest('PUT', endpoint, data.payload.data, successFn, errFn, 'savePatientPrograms', {secured: true});
        break;
    }
    case "deleteProgramEpisode":
    {
        var deletedProgramId = data.payload.programId;

        var successFn = function (prev) {
            var _obj = {
                scope: 'patientEnrollment',
                func: 'onDeletePatientProgramSuccess',
                args: {
                    deletedProgramId,
                    prev
                }
            };
            s.toClient(socket, 'f', _obj);
        };
        var errFn = function (data) {
            s.log('delete program episode failed');
            data && s.log('mssg ', JSON.stringify(data));
            var message = data && data.clientMessage;
            var _obj = {
                scope: 'patientEnrollment',
                func: 'onDeletePatientProgramEpisodeError',
                args: {message}
            };
            s.toClient(socket, 'f', _obj);
        };
        var userId = socket.clientData.login.userId;

        var endpoint = `${programUrl}/patients/${data.payload.patientId}/programs/${deletedProgramId}?getPreviousEpisode=true`;
        s.restful.rest('DELETEWITHBODY', endpoint, {id: data.payload.deleteReasonId}, successFn, errFn, 'deleteProgramEpisode', {secured: true});
        break;
    }

    case "searchConsentUsers":
    {


        var func = data.payload.forMinor ? 'onMinorConsentUserSearchResult' : 'onConsentUserSearchResult';
        var timestamp = data.payload.timestamp;
        var communityId = socket.clientData.login.communityId;

        var successFn = function (result) {
            var _obj = {
                scope: 'programConsentPanel',
                func,
                args: {
                    timestamp,
                    result
                }
            };
            s.toClient(socket, 'f', _obj);
        };

        var errFn = function (data) {
            s.log('consent user search failed');
            data && s.log('mssg ', JSON.stringify(data));
            // var _obj = {
            //     scope: 'programConsentPanel',
            //     func: errFunc,
            //     args: {timestamp}
            // };
            // s.toClient(socket, 'f', _obj);
        };

        var endpoint = `${patientServiceMTUrl}/consent/getConsentUserlist/${data.payload.patientId}/${communityId}`;
        s.restful.rest('POST', endpoint, {searchKeyText: data.payload.keyword}, successFn, errFn, 'searchConsentUsers', {secured: true});
        break;
    }
    case "getConsenterOptions":
    {

        var successFn = getSuccessHandler('consenterOptions', 'app');
        var errFn = getErrorHandler('getProgramConsenterOptionsError');
        var endpoint = `${patientServiceMTUrl}/consent/getConsenters/${communityId}`;

        s.restful.rest('GET', endpoint, successFn, errFn, 'getConsenterOptions', {secured: true});
        break;
    }

    case "addToPanel":
    {

          successFn = saveSuccessHandler('addSuccess');
          errFn = saveErrorHandler('addFailed');
          endpoint = patientUri + '/addWithManualTag';
          data.payload.MyPatientInfoDTO.communityId = socket.clientData.login.communityId;
          data.payload.MyPatientInfoDTO.userId = socket.clientData.login.userId;
          s.restful.rest('POST', endpoint, data.payload, successFn, errFn, 'addToPatientPanel', {secured: true});
        break;
       }
    case "checkPatientConsent":
    {
        communityId = socket.clientData.login.communityId;
        userId = socket.clientData.login.userId;

        pushGET();

        successFn = saveSuccessHandler('ifConsented');
        errFn = saveErrorHandler('searchFailed');
        endpoint = patientUri+'/hasConsent/'+userId+'/'+data.payload.patientId+'/'+communityId;

        s.restful.rest('GET', endpoint, successFn, errFn, 'checkConsentOfPatient', {secured: true});
        break;
     }
    case "alreadyInList":
    {
        communityId = socket.clientData.login.communityId;
        userId = socket.clientData.login.userId;

        pushGET();

        successFn = saveSuccessHandler('alreadyInList');
        errFn = saveErrorHandler('searchFailed');
        endpoint = patientUri+'/isOn/'+userId+'/'+data.payload.patientId+'/'+communityId;

        s.restful.rest('GET', endpoint, successFn, errFn, 'isOn', {secured: true});
        break;
     }
    default:
}
