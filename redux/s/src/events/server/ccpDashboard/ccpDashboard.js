// configuration values
var taskingURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.tasks');
var patientURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.patient');
var careplanURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.careplan');
var alertURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.alert');
var userURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.user');


var pushData = function (method, name, endpoint, modelName, transform, groupBy, subvar, payload, onErrorFn) {

    if (method==='GET') {
        s.restful.rest('GET', endpoint, function (data) {
            var modelData = transform((typeof subVar === 'undefined') ? data : data[subVar], groupBy, modelName);
            backToClient(modelData, modelName);
        }, function (data) {
            onErrorFn(modelName);
        }, name);
    }
    else {
        s.restful.rest('POST', endpoint, payload, function (data) {
            data = (typeof subVar === 'undefined') ? data : data[subVar];
            var modelData = transform((typeof subVar === 'undefined') ? data : data[subVar], groupBy, modelName);
            backToClient(modelData, modelName);
        }, function (data) {
            onErrorFn(modelName);
        }, name);
    }
};

var backToClient = function(data, modelName) {
    var pushModel = {home: {}};
    pushModel.home[modelName] = data;
    s.toClient(socket, 'd', pushModel);
};

var onChartError = function(modelName) {
    var pushModel = {home: {}};
    pushModel.home[modelName] = {
        isError : true,
        chartData: [],
        isLoading: false
    };
    s.toClient(socket, 'd', pushModel);
};

var onNoDataError = function(modelName) {
    var pushModel = {home: {}};
    pushModel.home[modelName] = {
        isError : true,
        isLoading: false
    };
    s.toClient(socket, 'd', pushModel);
};

var transformData = function(data, groupBy, event) {
    var result = {};

    if (event === 'ASSESSMENT' && groupBy === 'TYPE') {
        return transformAssessmentData(data);
    }

    var labels = data.chart.axes[0].values;
    var values = data.chart.axes[1].values;
    labels = labels.filter(function(item, index) {
        return values[index] != 0;
    });
    values = values.filter(function(item, index) {
        return item != 0;
    });
    var patients = data.chart.patients;
    patients = patients.filter(function(item, index) {
        return item.length != 0;
    });

    var chartData = [];
    if (groupBy === 'TYPE') {
        for (var i = 0; i < labels.length; i++) {
            chartData = chartData.concat({
                label: labels[i],
                value: values[i],
                patients: patients[i]
            });
        }
    }
    else if(groupBy === 'DOMAIN') {
      var domainLimit = undefined;
      var dashboards = socket.clientData.settings.apps.home.dashboards;
      dashboards.forEach(function (item, index) {
          if (item.id !== 'CCPDashboard') {
              return;
          }
          var widgets = item.widgets;
          for (var i in widgets) {
              if (widgets[i].name === 'issue-widget' && event === 'ISSUE') {
                  domainLimit = widgets[i].domainLimit;
                  break;
              }
              else if (widgets[i].name === 'intervention-widget' && event === 'INTERVENTION') {
                  domainLimit = widgets[i].domainLimit;
                  break;
              }
          }
      });
      if(domainLimit === undefined) {
        domainLimit = 20;
      }

      var topList = getTopPatientList(data, domainLimit);
      var indices = [];

      for (var i = 0; i < labels.length; i++) {
          if(topList.length > 0 && topList.indexOf(labels[i]) === -1) {
            continue;
          }
          chartData = chartData.concat({
              label: labels[i],
              value: undefined,
              patients: []
          });
          indices.push({
              idx: i,
              label: labels[i]
          });
      }

      for (var i = 0; i < chartData.length; i++) {
          for(var j = 0; j < indices.length; j++) {
              var tempVal = 0;
              var tempPatients = [];
              if (indices[j].label.toUpperCase() !== chartData[i].label.toUpperCase()) {
                continue;
              }
              if (indices[j].label.toUpperCase() === chartData[i].label.toUpperCase()) {
                  tempVal = values[indices[j].idx];
                  tempPatients = patients[indices[j].idx];
              }
              chartData[i].label = indices[j].label;
              chartData[i].value = tempVal;
              chartData[i].patients = tempPatients;
          }
      }
    }
    else if (groupBy === 'STATUS' || groupBy === 'NUMBERPERPATIENT') {
        var statusFilter = {};
        var exceptedStatusFilter = {};
        var hasStatusFilter = false;
        var color = '';
        if (groupBy  === 'STATUS') {
            hasStatusFilter = true;
            try {
                var dashboards = socket.clientData.settings.apps.home.dashboards;
                dashboards.forEach(function (item, index) {
                    if (item.id !== 'CCPDashboard') {
                        return;
                    }
                    var widgets = item.widgets;
                    for (var i in widgets) {
                        var status = widgets[i].parameter.status;
                        var exceptedStatus = widgets[i].parameter.exceptedStatus;
                        color = widgets[i].parameter.color;
                        if (typeof status === 'undefined') {
                            status = '';
                        }
                        var key = "";
                        if (widgets[i].name === 'issue-widget') {
                            key = "ISSUE";
                        }
                        if (widgets[i].name === 'assessment-widget') {
                            key = "ASSESSMENT";
                        }
                        if (widgets[i].name === 'goal-widget') {
                            key = "GOAL";
                        }
                        if (widgets[i].name === 'intervention-widget') {
                            key = "INTERVENTION";
                        }
                        statusFilter[key] = status;
                        exceptedStatusFilter[key] = exceptedStatus;
                    }
                });
            } catch(err) {
                console.log('Settings for dashboard not found', err);
                statusFilter = {
                    "ISSUE": "Unreviewed|In Progress|Error",
                    "ASSESSMENT": "COMPLETE|INCOMPLETE",
                    "GOAL": "In-Progress|Deferred|Entered in Error",
                    "INTERVENTION": "Unreviewed|In Progress|Complete|Entered In Error"
                }
            }
        }

        // var colorMap = {};
        // if (statusFilter[event] !== '' && color !== ''){
        //     var colorList = color.split('|');
        //     var statusList = statusFilter[event].split('|');
        //     for (var i = 0; i < statusList.length; i++ ){
        //         colorMap[statusList[i].toUpperCase()] = colorList[i];
        //     }
        // }

        var statuses = [];
        var exceptedStatuses = [];
        if (statusFilter[event] !== undefined && statusFilter[event] != ''){
            statuses = statusFilter[event].split('|');
            statuses.map(function(x) {
                return x.toUpperCase();
            });
        }
        if (exceptedStatusFilter[event] !== undefined && exceptedStatusFilter[event] != ''){
            exceptedStatuses = exceptedStatusFilter[event].split('|');
            exceptedStatuses.map(function(x) {
                return x.toUpperCase();
            });
        }

        var indices = [];
        for (var i = 0; i < labels.length; i++) {
            if (hasStatusFilter) {
                if (exceptedStatuses.length > 0) {
                    if (exceptedStatuses.indexOf(labels[i].toUpperCase()) !== -1) {
                        continue;
                    }
                } else if(statuses.indexOf(labels[i].toUpperCase()) === -1) {
                    continue;
                }
            }
            chartData = chartData.concat({
                key: labels[i],
                //color: colorMap[labels[i].toUpperCase()],
                values: []
            });
            indices.push({
                idx: i,
                label: labels[i]
            });
        }
        for (var i = 0; i < chartData.length; i++) {
            for(var j = 0; j < indices.length; j++) {
                var tempVal = 0;
                var tempPatients = [];
                if (indices[j].label.toUpperCase() === chartData[i].key.toUpperCase()) {
                    tempVal = values[indices[j].idx];
                    tempPatients = patients[indices[j].idx];
                }
                chartData[i].values.push({
                    label: indices[j].label,
                    value: tempVal,
                    patients : tempPatients
                });
            }
        }
    }

    result.chartData = chartData;
    result.patientData = data.chart.data? data.chart.data[0] : {};
    result.isError = false;
    result.isLoading = false;
    return result;
};

var transformAssessmentData = function(data) {
    var result = {};
    var labels = data.chart.axes[0].values;
    var values = data.chart.axes[1].values;
    labels = labels.filter(function(item, index) {
        return values[index] != 0;
    });
    values = values.filter(function(item, index) {
        return item != 0;
    });
    var patients = data.chart.patients;
    patients = patients.filter(function(item, index) {
        return item.length != 0;
    });
    var chartData = [];
    var indices = [];

    var dashboards = socket.clientData.settings.apps.home.dashboards;
    var options = [];
    var statusFilter = '';
    var colors = '';
    var statusDescription = '';
    dashboards.forEach(function (item, index) {
        if (item.id !== 'CCPDashboard') {
            return;
        }
        var widgets = item.widgets;
        for (var i in widgets) {
            if (widgets[i].name === 'assessment-widget') {
                statusFilter = widgets[i].parameter.status;
                colors = widgets[i].parameter.color;
                options = widgets[i].options;
                statusDescription = widgets[i].parameter.statusDescription;
                break;
            }
        }
    });

    var statusMap = {};
    if (statusFilter !== '') {
        var statusList = statusFilter.split('|');
        var colorList = colors.split('|');
        for (var i in statusList) {
            chartData = chartData.concat({
                key: statusList[i],
                values: [],
                color: colorList[i]
            });
        }
        var statusDescriptionList = statusDescription.split('|');
        if (statusList.length == statusDescriptionList.length){
            for (var i in statusList){
                statusMap[statusList[i].toUpperCase()] = statusDescriptionList[i];
            }
        }
    }
    var hasData = false;
    if (data.chart.data) {
        for (var id in data.chart.data[0]) {
            var arr = data.chart.data[0][id];
            for (var i in arr) {
                var obj = arr[i];
                var vals = chartData.filter(function(item, index) {
                    return item.key == obj.label3;
                });
                if (vals.length == 0) {
                    vals.push({
                        key: obj.label3,
                        values: []
                    });
                    chartData.push(vals[0]);
                }
                if (vals[0].values.length == 0) {
                    for (var j in options) {
                        vals[0].values.push({
                            label: options[j].abbrv,
                            name: options[j].name,
                            value: 0,
                            desc: statusMap[obj.label3.toUpperCase()],
                            patients: []
                        });
                    }
                }
                var v = vals[0].values.filter(function(item, index) {
                    return item.name == obj.label1;
                });
                if (v.length > 0) {
                    var isExist = false;
                    for (var i = 0; i < v[0].patients.length; i++) {
                      if(v[0].patients[i].id === id) {
                        isExist = true;
                        break;
                      }
                    }
                    if(isExist === false) {
                      v[0].value++;
                      v[0].patients.push({'id': id});
                      hasData = true;
                    }
                }
            }
        }
    }

    if (!hasData) {
        chartData = [];
    } else {
        chartData = chartData.filter(function(item, index) {
            return item.values.length > 0;
        });
    }
    result.chartData = chartData;
    result.patientData = data.chart.data? data.chart.data[0] : {};
    result.options = options;
    result.isError = false;
    result.isLoading = false;
    return result;
}

var getTopPatientList = function(data, top) {
  console.log('getTopPatientList');
  var resultMap = [];
  var labelResultList = [];
  //var resultList = [];
  //var valResultList = [];

  var labels = data.chart.axes[0].values;
  var values = data.chart.axes[1].values;

  //console.log('labels:', labels);
  //console.log('values:', values);

  for(i = 0; i < labels.length; i++) {
    resultMap.push({label: labels[i], value: values[i]});
  }

  resultMap.sort(function(x, y) {
    return y.value -x.value;
  });

  var stop = resultMap.length;
  if(resultMap.length > top) {
    stop = top;
  }
  for(i = 0; i < stop; i++) {
    labelResultList.push(resultMap[i].label);
  }
  return labelResultList;
}


var transformPatientNoData = function(data) {
    data.isError = false;
    data.isLoading = false;
    return data;
};
var userId = socket.clientData.login.userId;
var commId = socket.clientData.login.communityId;
switch (data.event) {
    case "ISSUE": case "ASSESSMENT": case "INTERVENTION": case "GOAL":
        pushData('POST', 'patientWithData', careplanURL + 'careplanreport/report/' + userId + '/' + commId, data.event, transformData, data.payload.groupBy, undefined, {reportRequest: data.payload}, onChartError);
        break;
    case "patientNoIssue": case "patientNoAssessment": case "patientNoIntervention":case "patientNoGoal":
        pushData('POST', 'patientWithData', careplanURL + 'careplan/nocount/' + userId + '/' + commId, data.event, transformPatientNoData, undefined, undefined, {reportRequest: data.payload}, onNoDataError);
        break;
}
