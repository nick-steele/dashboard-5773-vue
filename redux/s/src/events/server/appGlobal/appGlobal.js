var pushFunction = function(scope, func, response, child) {
    var pushFunc = {
        scope:  scope,
        func:   func,
        args:   (response === undefined || typeof child === 'undefined') ? response : response[child]
    };
    trace('Push function:', scope, func);
    s.toClient(socket, 'f', pushFunc);
};

// Pushes a GET REST call through to the client Task controller model (makes everything easier to understand below)...
var pushRET = function(name, buildUrlFunc, func, child ) {
    // Make the rest call..
    s.restful.rest('GET', buildUrlFunc, function(response) {
        if (typeof response === 'undefined') response = true;
            // Define the function we're looking to call...
            //console.log('pushRET');
            // Push the function call to the client, in Angular, in the compiled app controller, there is eventServer(event) that handles server pushes.
            // There is a switch that handles 'd' responses, which are data pushes, it magically falls into just the right place...
            //s.toClient(socket, 'f', pushFunc );
            pushFunction('app', func, response, child);

        }, function(data) {
            s.toClient(socket, 'a', {message:'Problem with ' + name + '(' + data + ')', log:'RET error: '+ func});
    }, name);
 };

//Just copy pasting from appTaskList...
// Pushes a GET REST call through to the client Task controller model (makes everything easier to understand below)...
var pushGET = function(name, endpoint, taskModel, subVar ,errFn ) {
    // Make the rest call..
    s.restful.rest('GET', endpoint, function(data) {
        // Define the data model as being in the task controller...
        var pushModel = { app: {}};
        // Now that we'll be in the right Angular controller above, push the data to the right model, using a sub value if present (this translates the REST parent
        // into whatever we need in Angular - they can be decoupled)...
        pushModel.app[taskModel] = (subVar === null || typeof subVar === 'undefined') ? data : data[subVar];
        // console.log(pushModel);

        // Push the data to the client, in Angular, in the compiled app controller, there is a eventServer function, that handles server pushes.
        // There is a switch that handles 'd' responses, which are data pushes, it magically falling into just the right place...
        s.toClient(socket, 'd', pushModel );
    }, function(data) {
        trace('error:' + data);
        if(errFn){
            errFn({model:taskModel, message:'Problem with ' + name + '(' + data + ') GET error: '+taskModel});
        }
    }, name);
};

var pushPOST = function(name, endpoint, body, taskModel, subVar , errFn) {
    // Make the rest call..
    s.restful.rest('POST', endpoint, body, function(data) {
        // Define the data model as being in the task controller...
        var pushModel = { app: {}};
        // console.log(data);
        // Now that we'll be in the right Angular controller above, push the data to the right model, using a sub value if present (this translates the REST parent
        // into whatever we need in Angular - they can be decoupled)...
        var value = data;
        if(subVar){
            var subVarArr = subVar.split(".");
            value = data[subVarArr[0]];
            for(var i=1;i<subVarArr.length;i++){
                if(typeof value[subVarArr[i]] == 'undefined') {
                    console.warn("Property "+ subVarArr[i]+ " of "+subVar+"  was not found in the data.", value);
                    value = null;
                    break;
                }
                else value = value[subVarArr[i]];
            }
        }
        pushModel.app[taskModel] = value;

        // console.log(pushModel);

        // Push the data to the client, in Angular, in the compiled app controller, there is a eventServer function, that handles server pushes.
        // There is a switch that handles 'd' responses, which are data pushes, it magically falling into just the right place...
        s.toClient(socket, 'd', pushModel );
    }, function(data) {
        if(errFn){
            errFn({model:taskModel,message:'Problem with ' + name + '(' + data + ') GET error: '+taskModel});
        }
    }, name);
};

var badgeErrorFn = function (data) {
    var pushFunc = {
        scope:  'app',
        func:   "onCountError",
        args:   data
    };
    s.toClient(socket, 'f', pushFunc);
};

var appCount = function(appList){
    if(!appList||appList.length===0){
        trace('No app with count');
        return;
    }

	// TEMPORARY fix to logging error in production... Panda: please fix this: this is being called when the user isn't logged in!
	try {
		var email = socket.clientData.login.email;
		var communityId = socket.clientData.login.communityId;
		
	} catch(e) {
		//console.log("MYSTERIOUS ERROR")
		//console.log(socket)
		trace('MYSTERIOUS ERROR WAS HERE')
		s.logger.done();
		return;
	}

    if(typeof appList === 'string'){
        appList = [appList];
    }
    for(var i=0;i<appList.length;i++){
        switch(appList[i]){
            case 'appTaskList':
                var taskingURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.tasks');
                pushGET('globalCountTasks', taskingURL+'usertask/count/' + email + '/' + communityId, 'count_appTaskList',null, badgeErrorFn);
                break;
            case 'appMessages':

                var directAddress = socket.clientData.login.directAddress;
                var token = socket.clientData.login.token,
                    mssgEndpoint = s.getConfiguration(socket.clientData.login.communityId, 'patientuser.webservice.client.url');

                var mssgApi = mssgEndpoint && mssgEndpoint.match(/(https?:\/\/(.*?)\/)/);
                if(!mssgApi){
                    trace("Error: could not find Message API");
                }else{
                    mssgApi = (mssgApi[0]+"msgapp/service/");
                    pushPOST('globalCountMessages', mssgApi+"fetchFolders/unreadInboxCount", {"serviceParam":{"tokenid":token,"communityid":communityId,"userDirectAddress":directAddress}},"count_appMessages","folders.0.unseeMessageCount",null, badgeErrorFn);
                }
                break;
            case 'appAlerts':
                var alertEndpoint = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.alert');
                var alertUrl = alertEndpoint+"useralert/"+email+"/true/"+communityId;
                pushGET('globalCountAlerts', alertUrl, "count_appAlerts",null, badgeErrorFn);
                break;
            default:
                trace('Not found: '+appList[i]);
        }
    }

};

var loadMemberList = function() {
    var commId = socket.clientData.login.communityId;
    var userId = socket.clientData.login.userId;
    var userURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.user');
    pushRET('globalListSubordinates', userURL + commId + "/" + userId + "/subordinates", 'loadMemberList', 'List');
};

switch(data.event){
    case "count":
        appCount(data.payload);
        break;
    case "loadMemberList":
        loadMemberList();
        break;
}