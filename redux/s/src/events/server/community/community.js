
var loginUrl = s.getConfiguration(socket.clientData.identity.tenant.id,'user.manager.server');


//community:switch event

var success = function(data){
    trace('success');
    s.toClient(socket,'f',
        {
            scope: 'app',
            func: 'onSwitchCommunity',
            args: data
        });
};

var failure = function (code, data) {
    trace('error:' + code);
    data.response && trace('message:' + data.response);
    var errorMessage = data.response ? data.response : 'An error occurred during authentication.';
    s.toClient(socket,'f',
        {
            scope: 'app',
            func: 'onCommSwitchError',
            args: errorMessage
        });
};

var eventName = 'GetCommunityToken';
var _url = loginUrl+'/token/'+data.payload;
var payload = {token: socket.clientData.identity.token};

s.restful.rest('POST', _url, payload, success, failure, eventName, {captureRedirect:true});

