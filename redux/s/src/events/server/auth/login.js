//Moved to login.js

// Performs a login for users, expects: my.event('remote:auth:login', {user: data.user, pass: data.pass});

// Construct the endpoint...
//
// var endpoint =	s.settings.endpoints.REST.auth.login +
// 				'/' +
// 				data.user +
// 				'/' +
// 				data.pass;


// Call it.  Expects ( url , [what to append to logs on success] , [what to append to logs on failure] )
// The rest function will automatically generate local events with the same event name:success or name:failure, so make sure they exist!
/*
 * Logs:
 *        On success: server.auth.success nick.steele@gsihealth.com
 *        On failure: server.auth.failure nick.steele@gsihealth.com:myPassword
 *
 * Logic:
 *        Will generate a push event that will call my.event( 'local:auth:[success/failure]' , [auth response] )
 *        You must create events/client/auth/success.js and events/client/auth/failure
 */

// s.restful.get( socket, endpoint, success, failure);
//
// var success = function(response) {
// 	s.logger.trace( 'success ' + response.username );
//     server.push( {event: 'auth', payload: 'success'} );
// };
//
// var failure = function() {
// 	s.logger.trace( 'failure ' + data.user + ':' + data.pass );
//     server.push( {event: 'auth', payload: 'failure'} );
// };