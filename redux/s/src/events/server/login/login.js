/**
 * @kind serverEvent
 * @name login
 * @returns identity
 *
 * @description
 * This login event is new way of authenticating user.
 * It requests UserManager for authentication and pushes identity object to client.
* */

var loginUrl = s.settings.env.endpoints.userManager+'user/usercommunity';

var loginSuccess = function(data){
    if(!data || !data.token){ // currently service does not return error code and instead returns blank object.
        loginFailure(-1);
    }else{
        trace('success:'+data.email);
        socket.clientData.identity = data;
        s.toClient(socket,'f',
            {
                scope: 'app',
                func: 'onIdentity',
                args: data
            });
    }

};


var loginFailure = function (code, data) {

    trace('error:' + code);
    var errorMessage = (data&&data.response) ? data.response : 'An error occurred during authentication.';
    data && data.response && trace('message:' + data.response);
    s.toClient(socket,'f',
        {
            scope: 'app',
            func: 'onLoginError',
            args: errorMessage
        });
};

var eventName, _url, payload;

switch(data.event){
    case 'setting':
        // update tenant settings from client to server
        trace('settings');
        socket.clientData.settings = data.payload.settings;
        break;

    /* Login by token (happens on community switch)
     * onSuccess -- set identity, call GWT token check
     * onFailure -- show error message, show login page*/
    //TODO: Should it be moved to "sync" event?
    case 'token': //login by token
        eventName = 'LoginWithToken';
        _url = loginUrl+'/token';
        payload = {token: data.payload.token};
        s.restful.rest('POST', _url, payload, loginSuccess, loginFailure, eventName);
        break;
    /* Refresh token (ex. on user internet disconnect)
    * onSuccess -- reset token FUTURE:resolve freeze
    * onFailure -- show error message, logout user */
    case 'sync':
        // This event has been moved to sync.js. Do not call
        console.log('Bypassing login:sync (SyncByToken)');
        break;
    /* Login by credential
    * onSuccess -- set identity, call GWT token check
    * onFailure -- show error message, (opt.) enable login button */
    default:
        eventName = 'LoginWithCred';
        trace('user:' + data.payload.user);
        _url = loginUrl+'/user';
        if(data.payload.tenant){
            _url = _url+'/'+data.payload.tenant;
        }
        payload = {
            user: data.payload.user,
            password: data.payload.pass
        };
        s.restful.rest('POST', _url, payload, loginSuccess, loginFailure, eventName);
}



