	// This comes from the client database: connect.configuration... (populated by a REST call when Node boots)
	var taskingURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.tasks');
    var patientURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.patient');
    var userURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.user');


	var pushFunction = function(scope, func, response, child, query) {
		var data = (response === undefined || typeof child === 'undefined') ? response : response[child]
		if (query){
			data = {
				query: query,
				response: data
			}
		}
	    var pushFunc = {
	        scope:  scope,
	        func:   func,
	        args:   data
	    };
	    //console.log('Push function:', scope, func);
		trace('Push function:'+ scope+ func)
	    s.toClient(socket, 'f', pushFunc);
	};

	// Event worked (return the model)...
	var success = function(data) {
		s.toClient(socket, 'd', {task: data} );
	};

	// Event failed...
	var failure = function(data) {
		s.toClient(socket, 'a', 'appTaskList: Failed:' + data);
	};

	var pushDELETE = function(name, endpoint, scope, func, callbackData) {
		s.restful.rest('DELETE', endpoint, function(data) {
			if(func !== 'undefined') {
				var pushFunc = {
					scope:  scope,
					func:   func,
					args: 	callbackData
				};

				s.toClient(socket, 'f', pushFunc);
			}
		}, function(data) {
			console.log('error:' + data);
		}, name);
	};
	// Pushes a GET REST call through to the client Task controller model (makes everything easier to understand below)...
	var pushGET = function(name, buildUrlFunc, taskModel, subVar, failureCallback) {
		// Make the rest call..
		s.restful.rest('GET', buildUrlFunc, function(data) {
			// Define the data model as being in the task controller...
			var pushModel = { task: {}};

			// Now that we'll be in the right Angular controller above, push the data to the right model, using a sub value if present (this translates the REST parent
			// into whatever we need in Angular - they can be decoupled)...
			pushModel.task[taskModel] = (typeof subVar === 'undefined') ? data : data[subVar];
			//console.log('pushGET PushModel:', pushModel);

			// Push the data to the client, in Angular, in the compiled app controller, there is a eventServer function, that handles server pushes.
			// There is a switch that handles 'd' responses, which are data pushes, it magically falling into just the right place...
			s.toClient(socket, 'd', pushModel );
		}, function(data) {
			trace('error:' + data);
			if(failureCallback) {
				var pushFunc = {
					scope:  'task',
					func:   failureCallback,
					args:   null
				};
				s.toClient(socket, 'f', pushFunc);
			}
			else {
				s.toClient(socket, 'a', {message:'Problem with ' + name + '(' + data + ')', log:'POST error: '+ taskModel});
			}
		}, name);
	};

	// Pushes a GET REST call through to the client Task controller model (makes everything easier to understand below)...
	var pushRET = function(name, buildUrlFunc, func, child, query) {
		s.restful.rest('GET', buildUrlFunc, function(response) {
			if (typeof response === 'undefined') response = true;
			// Define the function we're looking to call...
			trace('pushRET');
			// Push the function call to the client, in Angular, in the compiled app controller, there is eventServer(event) that handles server pushes.
			// There is a switch that handles 'd' responses, which are data pushes, it magically falls into just the right place...
			//s.toClient(socket, 'f', pushFunc );
			pushFunction('task', func, response, child, query);

		}, function(data) {
			s.toClient(socket, 'a', {message:'Problem with ' + name + '(' + data + ')', log: 'GET error: '+ func});
		}, name);
	};

	var pushPOST = function(name, buildUrlFunc, func, child, query, data) {
		s.restful.rest('POST', buildUrlFunc, data, function(response) {
			if (typeof response === 'undefined') response = true;
			console.log(response)
			pushFunction('task', func, response, child, query);

		}, function(data) {
			s.toClient(socket, 'a', {message:'Problem with ' + name + '(' + data + ')', log:method + ' error: '+ func});
		}, name);
	};

	// TODO: Pushes a PUT REST call through to the client Task controller model...
	var pushSET = function(name, method, buildUrlFunc, payload, func, fail, succCallbackFunc) {
		// Make the rest call..
		s.restful.rest(method, buildUrlFunc, payload, function(response) {
			trace('pushset response', response);
			if (typeof response === 'undefined') response = true;
			if (typeof func.response !== 'undefined') {
				response = func.response;
			} else {
				// if func.child and response[func.child] then response = response[func.child]
				if (typeof func.child !== 'undefined') {
					if (typeof response[func.child] !=='undefined') response = response[func.child];
				}
			}
			// Define the function we're looking to call...
			var pushFunc = {
				scope:  'task',
				func:   func.name,
				args:   response
			};
			trace('pushSET'+ func+ pushFunc);
			// Push the function call to the client, in Angular, in the compiled app controller, there is eventServer(event) that handles server pushes.
			// There is a switch that handles 'd' responses, which are data pushes, it magically falls into just the right place...

			s.toClient(socket, 'f', pushFunc );

			if (typeof succCallbackFunc === 'function') {
				trace('Successful callback function');
				succCallbackFunc();
			}
		}, function(data) {
			if (typeof fail === 'function') {
				fail('Error');
			} else {
				s.toClient(socket, 'a', {message:'Problem with ' + name + '(' + data + ')', log:'POST error: '+ func});
			}

		}, name);
	};

	var request = function(name, method, endpoint, payload, func, subVar, failureCallback ) {
		s.restful.rest(method, taskingURL + endpoint, payload, function(data) {
			var ressponse = (typeof subVar === 'undefined') ? data : data[subVar];
			var pushFunc = {
				scope:  'task',
				func:   func.name,
				args:   ressponse
			};
			s.toClient(socket, 'f', pushFunc);
		}, function(data) {
			trace('error:' + data);
			if(failureCallback) {
				var pushFunc = {
					scope:  'task',
					func:   failureCallback,
					args:   null
				};
				s.toClient(socket, 'f', pushFunc);
			}
			else {
				s.toClient(socket, 'a', {message:'Problem getting data, try refreshing your browser.', log:'POST error: '+ taskModel});
			}
		}, name);
	};

	var loadTasksFromBadges = function() {
		if(data.payload.badgeValue && data.payload.badgeValue.tasks[0] && data.payload.badgeValue.tasks[0].length > 0) {
			request('taskIds', 'POST','usertask/taskids/' + socket.clientData.login.communityId + '/', {tasks: data.payload.badgeValue.tasks[0]} , {name: 'getTaskList'}, 'UserTaskList', 'loadTaskError');
		}
		else {
			var pushFunc = {
					scope:  'task',
					func:   'getTaskList',
					args:   []
				};
			s.toClient(socket, 'f', pushFunc);
		}

	};

	var callTaskBadgeCountReload = function() {
		pushFunction('home', 'reloadTaskBadgeCount', undefined, undefined);
	};
	var buildUrl = function (baseUrl, endpoint) {
	    return baseUrl + endpoint;
    };

	s.logger.trace(data.event);
	switch(data.event) {
			// Return all the user data they need...
		case 'boot':
				// Get the task types...:
				pushGET('taskTypes', buildUrl(taskingURL, 'tasktype/' + socket.clientData.login.communityId), 'types', 'TaskTypeList');

				// Get the task status names...
				pushGET('taskStatus', buildUrl(taskingURL,'taskstatus'), 'status', 'List');

				// It should be redudant because we have it in appGlobal.js
				// Get the overdue tasks...
				// pushGET('usertask/count/' + socket.clientData.login.email + '/' + socket.clientData.login.communityId, 'overdue');
				if (data.payload && data.payload.isFiltered === true) {
					loadTasksFromBadges();
				} else {
				    var email = (data.payload && data.payload.email) ? data.payload.email : socket.clientData.login.email;
					request('taskList', 'POST','usertask/users/' + socket.clientData.login.communityId + '/', {users: email.split(/;/)} , {name: 'getTaskList'}, 'UserTaskList', 'loadTaskError');
				}

				break;
		case 'loadTasksFromBadges':
				loadTasksFromBadges();
				break;
			// Insert a task...
		case 'new':
				// bare bone user task...
				trace("New task");
				var Task = {
					taskCategoryId: 1,
					userTaskTitle: data.payload.userTaskTitle,
					userTaskDescr: data.payload.userTaskDescr,
					taskAction: data.payload.taskAction,
					dueDateTime: data.payload.dueDateTime,
					lastUpdateDateTime: data.payload.lastUpdateDateTime,
					creationDateTime: data.payload.creationDateTime,
					communityId: socket.clientData.login.communityId,
					userEmailId: data.payload.userEmailId ? data.payload.userEmailId : socket.clientData.login.email,
					taskTypeId: data.payload.taskTypeId === undefined ?  1 : data.payload.taskTypeId.taskTypeId,
					taskProgressStatusId: { taskProgressStatusId: data.payload.taskProgressStatusId.taskProgressStatusId },
					assignedByEmailId:  data.payload.assignedByEmailId,
					lastUpdatedByEmailId: socket.clientData.login.email
				};

				delete(data.payload.startsAt);
				delete(data.payload.title);
				delete(data.payload.type);
				delete(data.payload.editable);
				delete(data.payload.draggable);
				delete(data.payload.resizable);

				if (data.payload.patientId) Task.patientId = data.payload.patientId.patientId;
				// Just pass the rest call...
				pushSET('taskCreate', 'POST',buildUrl(taskingURL, 'usertask/createWithId'), {UserTask: Task} , {name: 'newResponse'}, function() {
					s.toClient(socket, 'a', "Couldn't create task");
				}, callTaskBadgeCountReload);


				break;
			case 'update':
				// Update the task...
				// Send the payload to the PUT endpoint, send the response to the task controller's response model...
				delete(data.payload.startsAt);
				delete(data.payload.title);
				delete(data.payload.type);
				delete(data.payload.editable);
				delete(data.payload.draggable);
				delete(data.payload.resizable);

                if(data.payload.patientInfo) {
                	data.payload.patientId = data.payload.patientInfo.patientId;
                } else {
                	data.payload.patientId = undefined;
                }
				delete(data.payload.patientInfo);
				delete(data.payload.startsAt);
				delete(data.payload.endsAt);
				delete(data.payload.daySpan);
                delete(data.payload.dayOffset);

				data.payload.lastUpdatedByEmailId = socket.clientData.login.email;

				pushSET('taskUpdate', 'PUT',buildUrl(taskingURL, 'usertask/' + socket.clientData.login.userId), {UserTask: data.payload} ,
					{name: 'updateResponse', response: data.payload.userTaskId}, function() {
						s.toClient(socket, 'a', "Couldn't update task");
				}, callTaskBadgeCountReload);
				break;
			case 'patientSearch':
				// Split the search if a space is provided (REST call supports firstname and lastname separated searches)...
				var limited = 30;
				var search = data.payload;

				//Make special characters become invalid lucene search characters
				if (typeof search !== 'undefined') {
                	search = search.trim().replace(/[^\w\s']/gi, '^#');
				}

				if(search === undefined || search.trim() === '') {
					search = ' ';
				}

				search = encodeURIComponent(search);
				// Get a list of patients matching the search...
				pushRET('patientSearch', buildUrl(patientURL, 'patientInfo/demog/limit/' + socket.clientData.login.communityId + '/' + search + '/' + socket.clientData.login.email + '/' + limited), 'searching', 'PatientList', data.payload);
				break;
		case 'checkConsent':
			pushRET('taskConsent', buildUrl(taskingURL, 'systemtask/' + socket.clientData.login.userId + '/' + data.payload.patientId + '/' + socket.clientData.login.communityId), 'checkingCons', 'Boolean');
			break;
		case 'taskFilter':
			var email = data.payload.email ? data.payload.email : socket.clientData.login.email;
			var taskType = data.payload.taskType
			if(taskType === undefined) {
				taskType = 'taskNonBadgeFilter';
			}
			pushPOST('taskReportFilter', buildUrl(taskingURL, 'taskreport/taskfilter/' + taskType + '/' + socket.clientData.login.communityId + '/' + data.payload.interval + '/' + data.payload.task.userTaskId), data.payload.callFuncName, undefined, undefined, {users: email.split(/;/)});

			// request('taskReportFilter', 'POST', 'taskreport/taskfilter/' + taskType + '/' + socket.clientData.login.communityId + '/' + data.payload.interval + '/' + data.payload.task.userTaskId, {users: email.split(/;/)} , {name: data.payload.callFuncName}, 'undefined', 'loadTaskError');

			break;
		case 'searchOrgCareteamUser':
			// Split the search if a space is provided (REST call supports firstname and lastname separated searches)...
			trace('appTaskList searchOrgCareteamUser');
			var search = data.payload.query;
			var patientId = data.payload.patientId;

			//For the error "Encoded slashes are not allowed" in Glassfish. Another option is config change in Glassfish:
			//com.sun.grizzly.util.buf.UDecoder.ALLOW_ENCODED_SLASH=true
			if (typeof search !== 'undefined') {
				search = search.trim().replace(/\/|\\/g, '#');
			}

			if(search === undefined || search.trim() === '') {
				search = ' ';
			}

			search = encodeURIComponent(search);

			if(patientId === undefined) {
				patientId = -1;
			}

			if(search === undefined || search.trim() === '') {
				pushFunction('task', 'orgCareteamUserSearchResolve', [], undefined, data.payload);
			}
			else {
				pushRET('taskSearch', buildUrl(userURL, socket.clientData.login.communityId + '/' + socket.clientData.login.userId + '/' + patientId + '/' + search), 'orgCareteamUserSearchResolve', 'List', data.payload);
			}

			break;
		case 'checkMinor':
			var userCommunityId = socket.clientData.login.communityId;
			var minorAge = s.getConfiguration(socket.clientData.login.communityId, 'minor.age');
			var minorWarning = s.getConfiguration(socket.clientData.login.communityId, 'minor.warning');
			var patientDOB = new Date(data.payload);
			var now = new Date();
			var timeDiff = now.getTime() - patientDOB.getTime();
			var years = timeDiff / (1000 * 3600 * 24 * 365.25);
			trace('Years:', years);
			if (years < minorAge) {
				if (minorWarning === undefined) {
					minorWarning = "This Patient is a Minor. Different consent and information disclosure rules may apply";
				}
			}
			else {
				minorWarning = undefined;
			}
			var pushModel = { task: {}};
			pushModel.task['minorWarning'] = minorWarning;
			s.toClient(socket, 'd', pushModel );
			break;
		case 'getComments':
			pushGET('getComments', buildUrl(taskingURL + 'comment/', data.payload.userTaskId + '/' + socket.clientData.login.email + '/' + data.payload.communityId), 'comments', 'CommentList');
			break;
		case 'addComment':
			var data = {
				UserTaskComment: {
					userTask: data.payload.userTask,
					communityId: socket.clientData.login.communityId,
					userEmailId: socket.clientData.login.email,
					comment: data.payload.comment
				}
			};
			pushSET('addComment', 'POST', taskingURL + 'comment/createWithId', data, {name: 'addCommentCallback'});
			break;
		case 'deleteComment':
			pushDELETE('deleteComment', buildUrl(taskingURL + 'comment/', + data.payload.id), 'task', 'deleteCommentCallback', data.payload.id);
			break;
		case 'editComment':
			pushSET('editComment','PUT' , buildUrl(taskingURL + 'comment/', data.payload.commentId),data.payload.commentInfo, {name: 'editCommentCallback'});
			break;
	}
