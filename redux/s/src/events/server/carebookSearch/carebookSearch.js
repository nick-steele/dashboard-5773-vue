// configuration values
var careTeamURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.careteam');
var taskingURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.tasks');
var patientURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.patient');
var careplanURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.careplan');
var alertURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.alert');
var userURL = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.user');
var userManagerUrl = s.getConfiguration(socket.clientData.login.communityId, 'user.manager.server');
var mtUrl = s.getConfiguration(socket.clientData.login.communityId, 'redux.rest.patient.service.mt');
// programUrl = 'http://10.153.0.136:9005';
// mtUrl = 'http://10.153.0.136:9005';
var auditUrl = mtUrl + '/auditMT/audit';
var searchCrossCommUrl = mtUrl + '/v1/patientServices/searchPatient/';
var programUrl = mtUrl + '/programMT/activeprogram/';
var userId = socket.clientData.login.userId;
var userEmail = socket.clientData.login.email;
var commId = socket.clientData.login.communityId;
var CONSENT_DENIED_TEXT = 'This patient has not yet granted consent. please contact your administrator.';
var transform = function(data) {
    return data;
};
var transformProgram = function(patientId, programs) {
    var response = {
        patientId: patientId,
        programs: programs
    };
    return response;
};
var getData = function(name, endpoint, modelName, subVar, transformFn, callbackToClient, opts) {
    s.restful.rest('GET', endpoint, function(data) {
        transformFn = transformFn ? transformFn : transform;
        var modelData = transformFn((!subVar) ? data : data[subVar]);
        if (modelName)
            backToClient(modelData, modelName);
        if (callbackToClient) {
            var pushFunc = {
                scope:  'app',
                func:   callbackToClient,
                args:   modelData
            };
            s.toClient(socket, 'f', pushFunc);
        }
    }, function(data) {;
    }, name, opts);
};
var pushData = function(method, name, endpoint, modelName, subVar, payload, transformFn, onErrorFn, opts) {
    s.restful.rest(method, endpoint, payload, function(data) {
        transformFn = transformFn ? transformFn : transform;
        var modelData = transformFn((!subVar) ? data : data[subVar]);
        if (modelName)
            backToClient(modelData, modelName);
    }, function(data) {
        if (modelName)
            onErrorFn(modelName);
    }, name, opts);
};
var transformCrossComm = function(patientList) {
    var response = {
        patients: patientList,
        isLoading: false,
    };
    for (var i = 0; i < patientList.length; i++) {
        if (patientList[i].communityId !== commId) {
            response.haveCrossCommPatient = true;
            break;
        }
    }
    return response;
};
var transformData = function(patientList) {
    return {
        patients: patientList,
        isLoading: false,
    };
};
var backToClient = function(data, modelName) {
    var pushModel = { app: {} };
    pushModel.app[modelName] = data;
    s.toClient(socket, 'd', pushModel);
};

var onError = function(modelName) {
    var pushModel = { app: {} };
    pushModel.app[modelName] = {
        isError: true,
        patients: [],
        isLoading: false
    };
    s.toClient(socket, 'd', pushModel);
};

var checkConsent = function (endpoint, patientId) {
    var payload = { PatientDemogs: { patientId: patientId } };
    s.restful.rest('PUT', endpoint, payload,
        function(data) {
            if(!data || !data.PatientDemogs || data.PatientDemogs.length !== 1) {
                s.log('patient provided by context was not found. patientId: '+ patientId);
                return;
            }

            var pushFunc = {
                scope:  'app',
                func:   'onPatientContextCheck',
                args:   data.PatientDemogs[0]
            };
            s.toClient(socket, 'f', pushFunc);

        }, function(data) {
        s.log('Error during patient context check. patientId:' + patientId);
        s.log(data);
            // const pushFunc = {
            //     scope:  'app',
            //     func:   'onPatientContextCheckError',
            //     args:   data
            // };
            // s.toClient(socket, 'f', pushFunc);

        }, 'checkPatientConsentByUser', {secured: true});
};

var tzo = -new Date().getTimezoneOffset();
dif = tzo >= 0 ? '+' : '-',
pad = function(num) {
    var norm = Math.abs(Math.floor(num));
    return (norm < 10 ? '0' : '') + norm;
};

iso8601TimeZone = (function() {
    return  dif + pad(tzo / 60) +
        ':' + pad(tzo % 60);
})();

switch (data.event) {
    case "searchCrossComm":
        if (data.payload.dob) {
            var time = "T10:00:00"; //for daylight saving time
            data.payload.dob = data.payload.dob.substr(0, 10) + time + iso8601TimeZone;
        }
        
        pushData('PUT', 'carebooksearch', searchCrossCommUrl + commId, 'carebookSearchResult', null, data.payload, transformData, onError, { secured: true });
        break;
    case "search":
        pushData('PUT', 'carebooksearch', patientURL + 'patientInfo/searchPatient/' + commId + '/' + userId, 'carebookSearchResult', 'PatientDemogs', { PatientDemogs: data.payload }, transformData, onError);
        break;

    case "checkConsentOnLogin":{
        checkConsent(patientURL + 'patientInfo/searchPatient/' + commId + '/' + userId, data.payload);
        break;
    }
    case 'messagebundle':
        var messagebundle = {
            consentWarning: s.getConfiguration(socket.clientData.login.communityId, 'patient.self.assert.warning.text'),
            consentDenied: s.getConfiguration(socket.clientData.login.communityId, 'patient.self.assert.denied.text') || CONSENT_DENIED_TEXT,
            consentLanguage: s.getConfiguration(socket.clientData.login.communityId, 'consent.language')
        };
        backToClient(messagebundle, 'consentMsg');
        break;
    case 'consentAccess':
        var consentAccess = s.getConfiguration(socket.clientData.login.communityId, 'patient.self.asserted.consent.access');
        backToClient(consentAccess, 'consentAccess');
        break;
    case 'enabledCounty':
        var enabledCounty = s.getConfiguration(socket.clientData.login.communityId, 'county.enabled');
        backToClient(enabledCounty, 'enabledCounty');
        break;
    case 'userCrossCommunity':
        getData('carebook searchcross', userManagerUrl + '/users/' + userEmail + '/communities', 'userCrossCommunity');
        break;
    case 'auditMT':
        pushData('POST', 'crossCommunityAudit', mtUrl + '/auditMT/audit', null, null, data.payload, null, null, { secured: true });
        break;
    case 'activePrograms':
        getData('getActivePrograms', programUrl + data.payload.communityId + '/' + data.payload.patientId, null, null, transformProgram.bind(null, data.payload.patientId), 'addProgramsToPatient', { secured: true }); 
        break;
    case 'getGenderMasterData': 
        getData('getGenderMasterData', careTeamURL + '/configurations/' + commId + '/genders', 'genders', null, null, null, {secured: true});
        break;
}