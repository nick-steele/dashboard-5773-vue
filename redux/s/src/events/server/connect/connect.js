
		// Called whenever a client first makes a connection...

        // Clear my data, because a fresh connection is taking place...
        my = {};
        socket.clientData = {};
		
        // Push the connect state...
//		s.event('server:state:connect');

		trace('Storing-Device-Info');
		// Store data about the device this user connected with...
			my.device = {
				origin: socket.handshake.headers.origin,
				cookies: socket.handshake.headers.cookie?socket.handshake.headers.cookie.toKeyValue():{},
				info: socket.handshake.headers['user-agent'],
				time: socket.handshake.time
			};

		// Log the Java session Id (TODO: Remove when we're rid of GWT)...
		if (my.device.cookies.JSESSIONID) trace('session:' + my.device.cookies.JSESSIONID);

		// Log the device info...
		trace('device:' + my.device.info);

		// Log the device time...
		trace('device time:' + my.device.time);

		// // Trigger a request for login info event...
		// Now client tries to push login obj in sync event
		// event('server:push:request', 'login');

        // Trigger a browser function call on the client (app.socketConnected) and include our server version and other stuff...
		event('server:push:function', {
			scope: 'app',
			func: 'socketConnected',
			args: {
				node:{
                    version: s.settings.client.vars.version,
                    status: 'connected',
                    connected: true,
                    sourceHash: ''
                }
			}
		});
		
		// Patching mode...
		if (s.settings.app.patching) {
			console.log('Patching');
			event('server:push:patch', {what: 'reboot'});
		}