// Log what's happening...
s.logger.trace(data.payload.type + '(' + data.payload.data?(JSON.stringify(data.payload.data).length / 1024).toFixed(3):0 + 'kb)', data.payload.data);
// Store the client data...
socket.clientData[data.payload.type] = data.payload.data;
if(data.payload.callback && data.payload.callback.scope && data.payload.callback.func) {

	trace('Function callback:' + data.payload.callback.scope  + ' ' + data.payload.callback.func);
	s.toClient(socket, 'f', data.payload.callback);
}