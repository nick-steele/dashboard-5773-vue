// Execute a function on the client...
try { data = JSON.parse(data); } catch(e) {}

// Get the application model, and through it, the proper respective scope (as long as it's been loaded)...

// Trace what function we're calling...
//trace.log('calling ' + data.scope + '.' + data.func + '(' + JSON.stringify(data.args) + ')');
trace.log('calling ' + data.scope + '.' + data.func + '()');

// Use the magic model service to call the requested function...
$model.get(data.scope)[data.func](data.args, trace);

trace.log('complete');