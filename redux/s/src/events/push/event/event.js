// Try to parse it if it's JSON, otherwise it's already JS data...
try { data = JSON.parse(data); } catch(e) {}

$log.info('Push Event:' + data);

// Use the magic model service to call the requested event...
$model.get('app').event(data);