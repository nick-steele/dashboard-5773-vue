// Modifies something in the client (a function in a controller, a controller, etc).  Creates it if it doesn't exist...
// i.e. modify { type: 'function', where: 'app', what: 'eventServer', payload: {} }
$log.info('Modification sent', data);

switch(data.type) {
	case 'function':
		$log.log('worked');
		my.model.get(data.where)[data.what] = eval(data.payload);
		break;
	case 'data':
		my.model.get(data.where)[data.what] = data.payload;
		break;
}