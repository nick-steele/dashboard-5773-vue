// Decompress and decode the state...
try {
	var c = JSON.parse(LZString.decompressFromBase64(data));	
} catch (e) {
	$log.error('could not decompress and parse the state payload because ', e);
	return;
}
// CSS...
if ( c.c ) document.getElementById('appStyle').innerHTML = c.c;
// HTML (angular code is recompiled with the dynamic $watch directive above)...
if ( c.h ) {
	// TODO: Find the DOM element it belongs in... (ID)
	if ( c.x ) {
		if (c.x.dialog) {
            // Get a list of camel cased variables out of the dialog name...
            //var args = c.d.replace(/([A-Z])/g, ' $1').split(" ");
            console.log('DIALOG scope:', c.x.dialog.scope, 'name:', c.x.dialog.name);
            // The 1st variable should be 'app', 2nd variable is the scope, the 3rd variable is the name...
            // Draw the actual dialog...
            my.dialog.scope = c.x.dialog.scope;
            //my.dialog.boot = c.x.dialog.boot;
            var options = {
                controller: universalDialogController,
                template: c.h,
                parent: angular.element(document.body),
                targetEvent: my.dialog.sourceElement,
                clickOutsideToClose: true,
                fullscreen: true // TODO: Calculate this
            };
            if(c.x.dialog.options) angular.extend(options,c.x.dialog.options);
            // javascript can invoke new controllers/directives/services by calling angularize()...
            if( c.j ) {
                my.dialog.stateBoot = c.j
            }
            $mdDialog.show(options)
              .then(function() {
                    my.dialog.allowNextPopup = false;
                    my.dialog.isOpen = false; 
                    if(my.dialog.callback) my.dialog.callback(my.dialog.choice);
                }, function() {
                    my.dialog.allowNextPopup = false;
                    my.dialog.isOpen = false;
                });

            //my.showDialog( { scope: args[1], data: my.dialog.data, template: c.h, sourceElement: my.dialog.sourceElement, callback: my.dialog.callback} );
        } else {
            try {
                var htmlId = c.x.HTML;
                var ele = document.getElementById(htmlId);
                if(ele==null) $log.error("#"+htmlId+" does not exist.");
                else{
                    my.globalTrace.did(htmlId+" received");
                    ele.innerHTML = c.h;
                    var content = $compile(ele)($scope);
                    my.globalTrace.did(htmlId+" compiled");
                }
            } catch(e) {
                console.error(e);
            }
        }
	} else {
		$log.error('HTML but no DOM element provider by the state model');
	}
}
