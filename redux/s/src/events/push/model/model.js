// Try to parse it if it's JSON, otherwise it's already JS data...
try { data = JSON.parse(data); } catch(e) {}

$log.info('Push Model:' + data.scope + ':' + data.model + '('+ data.data +')');

// Use the magic model service to call the requested function...
$model.get(data.scope)[data.model] = data.data;