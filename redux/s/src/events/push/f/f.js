// RPC function call by the server, to the client...
try { data = JSON.parse(data); } catch(e) {}
QA.monitor.event("push:f::"+data.scope +':' + data.func);
// Get the application model, and through it, the proper respective scope (as long as it's been loaded)...
$log.debug('push-RPC: '+ data.scope +':' + data.func + '('+ data.args +')');


trace.log(data.scope +':' + data.func);
// Use the magic model service to call the requested function...
$model.get(data.scope)[data.func](data.args);