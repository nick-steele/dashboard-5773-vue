try {
	// Try to parse it, if it's possible...
	data = JSON.parse(data);            
}
catch(e) {
	// otherwise, it's already JS data...
}

$log.debug('<-SVR DATA ARRAY START:');
if (data instanceof Array) {
	data.forEach(function (dataElement) {
	    var key = firstKey(dataElement);
		var secondKey = firstKey(dataElement[key]);

		$log.debug('<-SVR DATA ARRAY:'+ key +'['+ secondKey +']');
		QA.monitor.event("push:arrayData::"+key +'['+ secondKey +']');		
		trace.log(key +'['+ secondKey +']');
		$model.get(key)[secondKey] = dataElement[key][secondKey];
	});
}

