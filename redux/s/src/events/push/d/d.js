// Data change...
try {
	// Try to parse it, if it's possible...
	data = JSON.parse(data);            
}
catch(e) {
	// otherwise, it's already JS data...
}
if (data.LoginResult) {
	$log.debug('Updated Login');
	QA.monitor.event("push:d::login");
	updateLogin(data.User);
} else {
	// Update the application model, and through it, the proper respective scope (as long as it's been loaded)...
	var key = firstKey(data);
	var secondKey = firstKey(data[key]);
	$log.debug('<-SVR DATA:'+ key +'['+ secondKey +']');
	QA.monitor.event("push:d::"+key +'['+ secondKey +']');
	trace.log(key +'['+ secondKey +']');
	$model.get(key)[secondKey] = data[key][secondKey];
}