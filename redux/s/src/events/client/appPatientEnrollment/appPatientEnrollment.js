var openWizard = function(patientId, step){
    step && $log.debug('Directing to step id: ', step)
    var data = {patientId:patientId, wizardStepId: step};

    $model.get('app')
        .showDialog('patientEnrollment', data, null, function () {});

};

switch (what) {
    case 'openWizard':
        openWizard(data.patientId, data.step);
        break;
    case 'openCareTeamWizard':
        if (typeof data !== 'object') {
            data = JSON.parse(data);
        }
        openWizard(data.patientId, constants.ENROLLMENT.WIZARD.CARE_TEAM.ID);
        break;
    case 'openProgramWizard':
        openWizard(data.patientId, constants.ENROLLMENT.WIZARD.PROGRAM.ID);
        break;
    case 'openEnrollmentWizard':
        if (typeof data !== 'object') {
            data = JSON.parse(data);
        }
        openWizard(data.patientId);
        break;
}