
//Moved from GWT-Bridge
function _showAppWindow(url, name, features) {
    var appWin = $window.QA.app.appWindows[name];
    if(!appWin||appWin.closed === true){
        $window.QA.app.appWindows[name] = window.open(url,name,features);
    }
    $window.QA.app.appWindows[name].focus();
}

var url;
switch(what){
    case 'training':
        var subdomain = $location.host().split('.')[0];
        url = 'https://www.gsihealth.com/training/?token='+my.identity.token+'&env='+subdomain;
        _showAppWindow(url, 'training','_blank');
        break;
    default:
        $log.warn('Not supported', what);
}
