switch (what) {
    case 'show':
        gsiSideNav.isInitialized('MainMenu', 'appCareBookSearch') ? gsiSideNav.go('MainMenu', 'appCareBookSearch') : $appEvent('local:globalSearch:focus');
        break;
    case 'quit':
        GWT.appCareBookSearchClose();
        gsiSideNav.quit('MainMenu', 'appCareBookSearch');
        $appEvent('local:globalSearch:clear');
        $appEvent('local:openApp:appHome', { appName: 'appHome' });
        break;
    case 'toCareBookByPatient':
        var patientObj;
        if(typeof data === 'object') {
            patientObj = data;
        } else {
            var patientId = data;
            if(my.carebookSearchResult && my.carebookSearchResult.patients){
                for (var i = 0; i < my.carebookSearchResult.patients.length; i++) {
                    if (my.carebookSearchResult.patients[i].patientId == patientId) {
                        patientObj = my.carebookSearchResult.patients[i];
                        break;
                    }
                }
            }
            //Required for patient context login...
            if(!patientObj){
                $log.info('patient was not found from search result. creating object...');
                patientObj = { patientId:patientId };
            }
        }
        my.event('local:appCareBookSearch:toCareBook', my.buildCarebookUrl(patientObj));
        break;
    case 'toCareBookByUrl':
        var url = data;
        if (url.indexOf('jwtToken')===-1) {
            url += '&community=' + encodeURIComponent(my.identity.tenant.instance) + '&jwtToken=' + my.identity.token ;
        }
        my.event('local:appCareBookSearch:toCareBook', url);        
        break;
    case 'toCareBook':
        var url = data;
        var carebookIframe = document.getElementById('carebook-iframe');
        if (!carebookIframe) {
            carebookIframe = document.createElement("iframe");
            carebookIframe.setAttribute('id', 'carebook-iframe');
            carebookIframe.style.width = "100%";
            carebookIframe.style.height = "100%";
            carebookIframe.style.border = "0";
            var divAppCareBookSearch = document.getElementById('appCareBookSearch');
            divAppCareBookSearch.appendChild(carebookIframe);
        }
        carebookIframe.setAttribute("src", url);
        gsiSideNav.go('MainMenu', 'appCareBookSearch');
        $mdDialog.hide();
        break;
    case 'toEnrollment':
        var patient = data;
        $mdDialog.hide();
        GWT.openEnrollmentAdd('' + JSON.stringify(patient));
        break;
    case 'handleCarebookSearchError':
        $mdDialog.show(
            $mdDialog.alert()
            .parent(angular.element(document.querySelector('#popupContainer')))
            .clickOutsideToClose(true)
            .title('Error')
            .textContent('Cannot open CareBook')
            .ariaLabel('Cannot open CareBook')
            .ok('OK')
        );
        break;
    case 'auditSearch':
        var payload = my.prepairDataAudit(data.patient, data.action);
        my.event('remote:carebookSearch:auditMT', payload);
        break;
    case 'auditImport':
        GWT.openEnrollmentAdd(JSON.stringify(data.patient));
        $mdDialog.hide();
        break;
}