
switch (what) {
	//Always call GWT
	case 'boot':
	case 'show':
		GWT.appCareplan();
		break;
	case 'startup':
                // This .5 second timeout is VERY HACKY but GWT is so slow that it won't refresh in time otherwise...
		$timeout(function() { $appEvent('local:state:appCareplan'); }, 500);
		break;
}