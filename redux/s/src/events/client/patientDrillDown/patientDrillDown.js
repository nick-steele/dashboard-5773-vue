switch (what) {
    case 'openCarebook':
        // send patientId as data to event rather than storing it in app scope
        // $mdSidenav( 'MainMenu' ).open();
        console.log("open carebook" + data);
        GWT.openCarebook('' +data);
        $appEvent('local:globalSearch:clear');
        break;
    case 'edit':
        console.log("open Enrollment Edit" + data);
        GWT.openEnrollmentEdit('' + data);
        break;
    case 'openCarePlanDirect':
        GWT.openCarePlanDirect("carePlan",data);
        break;
}