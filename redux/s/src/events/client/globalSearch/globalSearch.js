// Handles global search.  Currently this is only Carebook Patient Search...

switch (what) {
    case 'toggle':
        // Shows/hides the search UI in small screen (phone) mode...
        $log.debug($mdMedia);
        my.globalSearchHidden = !my.globalSearchHidden;
        break;
    case 'clear':
        // Clears the contents of the search fields...
        my.carebookSearchFirstName = '';
        my.carebookSearchLastName = '';
        break;
    case 'focus':
        document.getElementById("txtSearchPatientLast").focus();
        break;
    case 'search':
        if (!my.genders)
            my.event('remote:carebookSearch:getGenderMasterData');
        if (my.carebookSearchLastName) {
            var payload = {
                firstName: my.carebookSearchFirstName,
                lastName: my.carebookSearchLastName
            };
            
            // Fixes bug in service found in pre-6.0 SIPRA 8921. Quicker to fix in UI than to discuss possible issue in service...
            if (payload.firstName === "") delete payload.firstName
            if (payload.lastName === "") delete payload.lastName
            
            my.event('remote:carebookSearch:search', payload);
            my.openCarebookSearch();
        }
        break;
    case 'searchCrossComm':
        if (!my.genders)
            my.event('remote:carebookSearch:getGenderMasterData');
        my.carebookSearchResult.isLoading = true;
        my.carebookSearchResult.patients = [];
        var lastName = my.carebookSearchName.split(',')[0];
        var firstName = my.carebookSearchName.split(',')[1];
        var payload = {
            firstName: firstName,
            lastName: lastName,
            ssn: my.carebookSearchSSN,
            dob: $filter('date')(my.carebookSearchDob, 'yyyy-MM-ddTHH:mm:ss.sss'),
            payer1: my.carebookSearchPayerId
        };
        my.event('remote:carebookSearch:searchCrossComm', payload);
        break;
    case 'clearCrossCommSearch':
        if(my.carebookSearchLastName && my.carebookSearchFirstName) {
             my.carebookSearchName = my.carebookSearchLastName + ', ' + my.carebookSearchFirstName;
        }
        else if(my.carebookSearchLastName) {
            my.carebookSearchName = my.carebookSearchLastName;
        }
        else if(my.carebookSearchFirstName) {
            my.carebookSearchName = ', ' + my.carebookSearchFirstName;
        }
        else {
            my.carebookSearchName = '';
        }
       
        my.carebookSearchSSN = my.carebookSearchPayerId = '';
        my.carebookSearchDob = undefined;
        break;

}