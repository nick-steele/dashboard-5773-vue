// // TODO: We dont need QA.server or QA.port if we move this to the server, correct?
//
// // Just translate this into a remote event; the remote event will populate the correct data model or generate an error if nessisary...
// //$appEvent('remote:auth:login', {user: data.user, pass: data.pass});
//
//         var loginURL = location.protocol + '//' + QA.server;
//         if (QA.port !== "80") loginURL += ':' + QA.port;
// // Gets the login data from the server (this is the only REST call that mut be done by the client)...
//         $http.get(loginURL + "/dashboard/rest/login").then(function (response) {
//             // Get the login result...
//             data = response.data.LoginResult;
//             // Update the login...
//             updateLogin(data);
//         });
switch(what){
    case "attempt": //Request login
        $appEvent('remote:login',{user:data.user,pass:data.pass, tenant: my.settings.tenant});
        my.isLoggingIn = true;
        break;
    case "token":
        $appEvent('remote:login:token',{token:data}); //FIXME
        break;
        //Deprecated
    case "gwtLogin":
        GWT.checkToken(data);
        break;
    //    Sync token which has been updated by GWT
    case "syncTokenFromGWT":
        my.identity.token = data;
        $appEvent( 'remote:sync', {token: my.identity.token} );
        break;
    case "forgotPassword":
        GWT.forgotPassword(data);
        break;
    case "pentaho":
        if(!my.pentahoLoggedin){
            $scope.$broadcast('gsiBackgroundPost','pentaho');
        }
        break;
    case "show":
    case "failed":
        my.isGWTCheckingToken = false;
        my.loggedIn = false;
        my.isLoggingIn = false;
        break;
    case "success": //Called from GWT
        my.trace("Login event");
        var loginURL = location.protocol + '//' + QA.server;
        if (QA.port !== "80") loginURL += ':' + QA.port;
// Gets the login data from the server (this is the only REST call that mut be done by the client)...
        $http.get(loginURL + "/dashboard/rest/login").then(function (response) {
            // Get the login result...
            my.trace("Login data received");
            data = response.data.LoginResult;
            // Update the login...
            my.loggedIn=true;
            my.isGWTCheckingToken = false;
            my.isLoggingIn = false;
            updateLogin(data);
        },function(){
            my.onLoginError('An error occurred during authentication.');
        });

        break;
}