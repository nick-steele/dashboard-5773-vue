
// Load dialog details into the appReportsDetails state...
var reportDetails = function(item) {
	if(item.id === 'patientTrackingSheet'){
		GWT.showReportDetails(item.id);
		return;
	}
	// showDialog will make a request for the dialog state and fill it with the correct model and the item below as data...
	$model.get('app').showDialog('appReportsDetails', item, null, function (result) {
		switch (result) {
			case 'ok':
				console.log('You clicked OK', result);
				break;
			case 'cancel':
				console.log('You didnt click', result);
				break;
		}
	// Boot function (called when the dialog has been booted)...
	}, function() {
		GWT.showReportDetails(item.id);
	});
	
}

switch (what) {
	case 'boot':
		$appEvent('local:state:appReports');
		break;
	case 'details':
		reportDetails(data);
		break;
	case 'email':
		alert('emailing', data);
		break;
	case 'word':
		alert('Creating Word doc', data);
		break;
	case 'excel':
		alert('Creating Excel', data);
		break;
	case 'pdf':
		alert('Creating pdf', data);
		break;
}
