// Show the patient list...
// Draw the aside...
switch(what) {
    case 'open':
        gsiSideNav.open("PatientList");
        break;
    case 'close':
        gsiSideNav.close("PatientList");
        break;
    case 'toggle':
        gsiSideNav.toggle("PatientList");
        break;
    case 'refresh':
        $model.get('myPatientList').reloadAfterRemoveOrAdd();
        break;
}
