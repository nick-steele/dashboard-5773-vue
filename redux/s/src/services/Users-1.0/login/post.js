(USERLAND_user, USERLAND_pass) => {
	
	// Check if the user/pass combo returns anything...
	let result = simpleQuery( 'SELECT * FROM user WHERE EMAIL = ? AND PASS = ?', [ USERLAND_user, hash( USERLAND_pass ) ] );
	
	// No result? The login is invalid...
	if (result === undefined) Throw 'Login for ' + user + ' failed.';

	// If we're here, the user/pass combo was correct...
	log('Login for ' + user + ' success.')
	return result;
}