
describe('test framework', function() {
 beforeEach(angular.mock.module('redux'));

  var $controller,provide,patientList,scope,fakeModelService;

      beforeEach(angular.mock.module(function (_$provide_) {

        provide = _$provide_;
        provide.value('$mdPanel',{});

        provide.value('$mdDialog',{});


        provide.value('$element',function(){});
        provide.value('$model',fakeModelService);

        provide.value('$appEvent',function(){})


    }));




  beforeEach(angular.mock.inject(function(_$controller_,$rootScope){


    $controller = _$controller_;
    scope=$rootScope.$new();

   patientList = $controller('AsideController-MyPatientList',{$scope:scope});

  }));

  describe('test patient panel controller functionality', function() {
   

    it('should return as expected on load of controller', function() {
    
    expect(patientList.tagEnabled[0]).to.equal(false);
    expect(patientList.tagEnabled[1]).to.equal(false)
    expect(patientList.tagEnabled[2]).to.equal(false)
    expect(patientList.tagEnabled[3]).to.equal(false)
    expect(patientList.tagEnabled[4]).to.equal(false)

    });


    it('reload functionality test : should return as expected', function() {

       patientList.reload();
       expect(patientList.selected).to.equal(2);

    });

   it('toggleSelectedTag functionality test : should return as expected', function() {

       patientList.toggleSelectedTag({selected:true});
       expect(patientList.nullTagSelected).to.equal(false);

    });


    
  });





  fakeModelService={

          store:sinon.stub(),
          get :function(){
            return {globalTrace:{did:sinon.stub()},login:{canViewOrgPP:true}};
          }
        }
});