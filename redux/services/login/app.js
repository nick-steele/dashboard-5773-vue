/*
 *	GSI Health login service 1.0.0
 *
 *		Copyright (C) 2017, GSI Health, LLC.  All rights reserved.
 *
 *		This is database (as long as it supports SQL syntax) and service protocol (HTTP/HTTP2/WebSocket/etc) agnostic: Just write your logic and forget what's underneith.
 *
 *		Access the datasource using s.data.  Access config using s.config.
 */
 
 
 //  generic asynchronous control-flow driver
function async (proc, ...params) {
    var iterator = proc(...params)
    return new Promise((resolve, reject) => {
        let loop = (value) => {
            let result
            try {
                result = iterator.next(value)
            }
            catch (err) {
                reject(err)
            }
            if (result.done)
                resolve(result.value)
            else if (   typeof result.value      === "object"
                     && typeof result.value.then === "function")
                result.value.then((value) => {
                    loop(value)
                }, (err) => {
                    reject(err)
                })
            else
                loop(result.value)
        }
        loop()
    })
}

//  application-specific asynchronous builder
function makeAsync (text, after) {
    return new Promise((resolve, reject) => {
        setTimeout(() => resolve(text), after)
    })
}

//  application-specific asynchronous procedure
async(function* (greeting) {
    let foo = yield makeAsync("foo", 300)
    let bar = yield makeAsync("bar", 200)
    let baz = yield makeAsync("baz", 100)
    return `${greeting} ${foo} ${bar} ${baz}`
}, "Hello").then((msg) => {
    console.log("RESULT:", msg) // "Hello foo bar baz"
})

 // This gets loaded by our engine...
module.exports = (request, response) => {
	let start = new Date();	
	let type = request.method.toLowerCase();
	let [undefined, method, ...args] = request.url.split("/"); // get the method and args
		
	switch ( method ) {
		case 'login':
			let methods = {
				get: (USERLAND_user, USERLAND_pass) => {					
					// Check if the user/pass combo returns anything...
					simpleQuery( 'select u.*, GROUP_CONCAT(c.community_id) AS communities FROM user u INNER JOIN user_community_organization c WHERE u.EMAIL = ? AND u.PASS = ? AND c.USER_ID = u.USER_ID', [ USERLAND_user, hash( USERLAND_pass )  ], result => {
						let msg = USERLAND_user + ' login ';

						// No result? The login is invalid...
						if (typeof result === 'undefined') {
							let took = ' ' + (new Date() - start) + 'ms';
							msg = msg + 'failed with password ' + USERLAND_pass + ' (hash: ' + hash( USERLAND_pass ) + ')';
							log(msg + took);
							response.end(msg + took);
						// Result? The login is valid...
						} else {
							// Convert our result into nifty meaningful JavaScript...
							result = userObject( result );
							let took = ' ' + (new Date() - start) + 'ms';								
							msg = msg + 'succeded.';
							log(msg + took);
							// Send it to the request...
							response.end( JSON.stringify( result ) );
						}
					});
				}
			}
			
			// Call the method...
			if (typeof methods[ type ] === 'function') methods[ type ]( ...args );
			else {
				log('Missing ' + type + ' ' + method);
				response.end('Missing.');
			}
			break;
		default:
			log('Missing ' + method);
			response.end('Missing.');
			break;
	}
}

var s = require('./simplex');
var [log, hash, query, simpleQuery, config, app] = [s.log, s.hash, s.query, s.simpleQuery, s.config, s.express];

// Parse the user table and return the new UI-friendly user object...
var userObject = str => {	
	try {
		// Catch any errors converting the login object...
		var obj = {
			id			: str.USER_ID,
			email		: str.EMAIL,
			dob			: str.DATE_OF_BIRTH,
			name:		{
				title	: str.TITLE,
				prefix	: str.PREFIX,
				display	: str.DISPLAY_NAME,
				first	: str.FIRST_NAME,
				middle	: str.MIDDLE_NAME,
				last	: str.LAST_NAME,
				suffix	: str.SUFFIX
			},
			address:	{
				line1	: str.STREET_ADDRESS_1,
				line2	: str.STREET_ADDRESS_2,
				city	: str.CITY,
				state	: str.STATE,
				zip		: str.ZIP_CODE
			},
			phone:		{
				main	: str.TELEPHONE_NUMBER,
				cell	: str.MOBILE_NUMBER,
				pager	: str.PAGER_NUMBER,
				fax		: str.FAX_NUMBER,
				ext		: str.PHONE_EXT
			},
			security:	{
				date:	{
					created	: str.CREATION_DATE,
					updated	: str.LAST_UPDATE_DATE,
					accessed: str.LAST_SUCCESSFUL_LOGIN_DATE,
					passwordChanged: str.LAST_PASSWORD_CHANGE_DATE				
				},
				enabled	: str.ENABLED,
				deleted	: str.DELETED,
				locked	: str.ACCOUNT_LOCKED_OUT,
				attempts: str.FAILED_ACCESS_ATTEMPTS,
				changePassword: str.MUST_CHANGE_PASSWORD,
				role	: str.REPORTING_ROLE
			},
			communities	: str.communities.split(",")
		}		
	} catch (e) {
		// If something went wrong creating the login object...
		log('Error building user object: ' + e);
	}
	try {
		obj.settings = JSON.parse( str.settings );
	} catch (e) {
		// If the settings string isn't valid JSON...
		log('user settings is not valid JSON: ' + e);
	}
	// Everything went through, return it...
	return obj;
}


/*

Problems with user table:
	LAST_UPDATE_DATE is the same as LAST_SUCCESSFUL_CHANGE_DATE
	MIDDLE_INITIAL is the same as left(1, MIDDLE_NAME) and allows two sources of truth for middle name.

The following probably belong in settings:
	LAST_ALERT_ACCESS_DATE_TIME (pointer to last read personal alert: never searched)
	eula_accepted (flag to personally show a UI element or not)
	eula_date (pointer to last eula acceptance date; should be a hash which can be verified)
	eula_id (?)
	disable_user_mfa (flag enabling/disabling a UI element)
	USER_INTERFACE_TYPE

I don't know what this stuff is for:
	AUTHY_ID
	HC_IDENTIFIER
	HC_PROFESSION
	HDP_PROVIDER_STATUS
	HDP_PROVIDER_LANGUAGE_SUPPORTED
	HC_SIGNING_CERTIFICATE
	LABELED_URI
	PHYSICAL_DELIVERY_OFFICE_NAME
	HDP_PROVIDER_MAILING_ADDRESS
	HDP_PROVIDER_BILLING_ADDRESS
	HDP_PROVIDER_PRACTICE_ADDRESS
	HC_PRACTICE_LOCATION
	CREDENTIAL_IDS
	HC_SPECIALIZATION
	EUID
	IN_HPD
	LAST_MFA_VERIFICATION_DATE
	NPI
	CAN_MANAGE_POWER_USER
	REPORTING_ROLE

The following can probably be turned into meta activity:
	DO_NOT_REQUIRE_NEW_PASSWORD_ON_CHANGE
	LAST_PASSWORD_CHANGE_DATE
*/