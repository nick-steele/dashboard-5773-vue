/*
	GSI Health Developers:

	This file is executed at COMPILE TIME.  This means if you include JavaScript, your code will execute
	at the time of COMPILE inside NODE not RUNTIME inside the BROWSER.

	Doing this in 2017:

				"copyright": "Copyright \u00A9 2012 - " + (new Date()).getFullYear() + " GSI Health, LLC. All Rights Reserved."

	Will yield this in 2018, hard-coded into app.js:

				"copyright": "Copyright \u00A9 2012 - 2017 GSI Health, LLC. All Rights Reserved."

	If you need to generate information at RUNTIME, do not use JavaScript, instead, just use a template like this:

				"copyright": "Copyright &copy; 2012 - {{app.copyright}} GSI Health, LLC. All Rights Reserved."

	And in your view code, use the gsi-dynamic directive like this:

				<div gsi-dynamic template="{{app.settings.components.app.copyright}}"></div>

	Presto!  A dynamicly compiling string template that can be overridden by tenants!! Woohoo! :)
	
	***ALWAYS NOTE WHICH STRINGS YOU'RE ALLOWING DYNAMIC TEMPLATING FOR***
	
*/
module.exports = {
	"beta": {
		"roster": false
	},
	"components": {
		"activity": {
			options: {
				enabled: true,				/* is the ActivityMonitor enabled? */
				keepAlive: 0,				/* keepAlive ping invterval (seconds) */
				inactive: 1800,				/* how long until user is considered inactive? (seconds) */
				warning: 300,				/* when to warn user when nearing inactive state (deducted from inactive in seconds) */
				monitor: 1,     			/* how frequently to check if the user is inactive (seconds) */
				disableOnInactive: false,	/* by default, once user becomes inactive, all listeners are detached */
				DOMevents: ['mouseup', 'keypress', 'touchstart'] /* list of DOM events to determine user's activity */	
			},
			"text": {
				"warning"	: "Your session will expire if you are inactive for 30 minutes..."
			}
		},
		"titleBar": {
			"title"			: "Coordinator",
			"logOut"		: "titleBar",
			"search"		: "careBook",
			"shrinkIcon"	: "true"
		},
		"patientPanel": {
			"searchHistory": []
		},
		"app": {
			"version": "6.0.0",
			"background": "/assets/0/background.jpg",
			"primaryColor": "blue",
			"secondaryColor": "orange",
			"cdn": "/dashboard/",
			"copyright": "Copyright &copy; 2012 - {{app.date | date:'yyyy'}} GSI Health, LLC. All Rights Reserved." // DYNAMIC TEMPLATING ENABLED
		}
	},
	"hosts": {
		"localhost"   						: { id: 1},
		"dev.gsihealth.net"					: { id: 1},
		"qa.gsihealth.net"					: { id: 102},
		"qb.gsihealth.net"					: { id: 104},
		"stg.gsihealth.net"					: { id: 301},
		"stgb.gsihealth.net"				: { id: 302},
		"beta.gsihealth.net"				: { id: 401},
		"escrow.gsihealth.net"				: { id: 850},
		"train1.gsihealth.net"				: { id: 601},
		"test.gsihealth.net"				: { id: 801},
		"nplogin.gsihealth.net"				: { id: 999},
		"demo.gsihealth.net"				: { id: 1001},
		"training.gsihealth.net"			: { id: 1101},
		"uat.gsihealth.net"	 				: { id: 1102},
		"gsihealthcoordinator.gsihealth.net": { id: 1103},
		"hhcuat.gsihealth.net"				: { id: 1003},
		"login.gsihealth.com"				: { id: 2000},
		"healthhome.gsihealth.com"			: { id: 2001},
		"uhc.gsihealth.com"					: { id: 2002},
		"cbc.gsihealth.com"					: { id: 2003},
		"cnyhhn.gsihealth.com"				: { id: 2004},
		"nfmmc.gsihealth.com"				: { id: 2005},
		"hhpwny.gsihealth.com"				: { id: 2006},
		"hhc.gsihealth.com"					: { id: 2007},
		"ccf.gsihealth.com"					: { id: 2008},
		"hvcc.gsihealth.com"				: { id: 2009},
		"ccc.gsihealth.com"					: { id: 2010},
		"ahi.gsihealth.com"					: { id: 2011},
		"bphc.gsihealth.com"				: { id: 2012},
		"crisp.gsihealth.com"				: { id: 2013}
	},
	"apps": {
		"home": {
			"text":	{
			},
			"requires": "valid_user",
			"gridWidth": {
				"xlarge": 6,
				"large": 4,
				"medium": 3,
				"small": 2,
				"xsmall": 1
			},
			"views": {
				"default": [{
					"name": "tasksCount",
					"width": 2,
					"height": 2,
					"order": 0
				}, {
					"name": "tasksDue",
					"width": 2,
					"height": 2,
					"order": 1
				}]
			},
			"widgets": {
				"tasksCount": {
					"requires": "view_own_tasks"
				},
				"tasksDue": {
					"requires": "view_own_tasks"
				}
			},
			"dashboards": [
		        {
		            "id": "tab1",
		            "title": "Summary Dashboard",
		            "layout": {
		                "md-cols": 12,
		                "md-row-height": "80px",
		                "md-gutter": "10px"
		            },
		            "widgets": [
		                {
		                    "layout": {
		                        "md-colspan": 12,
		                        "md-rowspan-gt-xs": 5,
		                        "md-rowspan-gt-md": 3,
		                        "md-rowspan": 9
		                    },
		                    "name": "task-widget-1",
		                    "type": "task-badge-count-widget",
		                    "parameter": {
		                        "api": "#{home.taskApi}",
														"hidewidget": "#{home.hideTaskBadgeCount}"
		                    }
		                },
		                {
		                    "layout": {
		                        "md-colspan": 12,
		                        "md-rowspan-gt-sm": 7,
		                        "md-rowspan": 13
		                    },
		                    "name": "alert-widget-1",
		                    "type": "alert-encounter-widget",
		                    "parameter": {
		                        "encounters": "#{home.encounters}",
		                        "patientnoencounter": "#{home.patientNoEncounter}",
		                        "isalerterror": "#{home.isAlertError}",
		                        "iscareplanerror": "#{home.isCarePlanError}",
		                        "isnoencountererror": "#{home.isNoEncounterError}",
		                        "subordinates": "#{home.selectedSubordinates}",
		                        "alertchartdata": "#{home.alertChartData}",
		                        "currentfilter": "#{home.currentFilter}",
		                        "api": "#{home.alertEncounterApi}"
		                    }
		                }
		            ]
		        },
						{
							"id": "CCPDashboard",
							"title": "CCP Dashboard",
							"layout": {
								"md-cols": 12,
								"md-row-height": "640px",
								"md-gutter": "5px"
							},
							"widgets": [
								{
									"layout": {
										"md-colspan-gt-md": 6,
										"md-colspan": 12,
										"md-rowspan": 1
									},
									"name": "issue-widget",
									"type": "ccp-widget",
										"styles": {
										"width": "95%",
										"max-width": "100%"
									},
									"parameter": {
										"type": "issue",
										"data": "#{home.ISSUE}",
										"filter": "#{home.issueFilter}",
										"patientnodata": "#{home.patientNoIssue}",
										"api": "#{home.issueApi}",
										"status": "Unreviewed|In Progress|Error",
										"exceptedStatus": "COMPLETE",
										"color" : "#F57C00|#1976D2|#D32F2F",
										"isshowtimerange": "false",
										"priorityStatus" : "In Progress"
									},
									"domainLimit": 20
								}, {
									"layout": {
										"md-colspan-gt-md": 6,
										"md-colspan": 12,
										"md-rowspan": 1
									},
									"name": "assessment-widget",
									"type": "assessment-widget",
										"styles": {
										"width": "95%",
										"max-width": "100%"
									},
									"parameter": {
										"type": "assessment",
										"data": "#{home.ASSESSMENT}",
										"filter": "#{home.assessmentFilter}",
										"patientnodata": "#{home.patientNoAssessment}",
										"api": "#{home.assessmentApi}",
										"status": "COMPLETE|INCOMPLETE",
										"statusDescription": "Complete = event occurred to completion in the selected time frame|Incomplete = event has been modified within the selected time frame, but not to completion",
										"color": "#1976D2|#F57C00"
									},
									"options": [
										{
												"name": "Functional Assessment",
												"abbrv": "FACT-GP"
										}
									]
								}, {
									"layout": {
										"md-colspan-gt-md": 6,
										"md-colspan": 12,
										"md-rowspan": 1
									},
									"name": "goal-widget",
									"type": "ccp-widget",
										"styles": {
										"width": "95%",
										"max-width": "100%"
									},
									"parameter": {
										"type": "goal",
										"data": "#{home.GOAL}",
										"filter": "#{home.goalFilter}",
										"patientnodata": "#{home.patientNoGoal}",
										"api": "#{home.goalApi}",
										"status": "Deferred|In-Progress|Entered in Error",
										"exceptedStatus": "ACHIEVED",
										"color": "#F57C00|#1976D2|#D32F2F",
										"isshowtimerange": "false",
										"priorityStatus" : "In-Progress"
									}
								}, {
									"layout": {
										"md-colspan-gt-md": 6,
										"md-colspan": 12,
										"md-rowspan": 1
									},
									"name": "intervention-widget",
									"type": "ccp-widget",
										"styles": {
										"width": "95%",
										"max-width": "100%"
									},
									"parameter": {
										"type": "intervention",
										"data": "#{home.INTERVENTION}",
										"filter": "#{home.interventionFilter}",
										"patientnodata": "#{home.patientNoIntervention}",
										"api": "#{home.interventionApi}",
										"status": "Unreviewed|In Progress|Entered In Error",
										"exceptedStatus": "COMPLETE",
										"color": "#F57C00|#1976D2|#D32F2F",
										"isshowtimerange": "false",
										"priorityStatus" : "In Progress"
									},
									"domainLimit": 20
								}
							]
						}
		    ]
		},
		"login": {
			"engine": "redux",
			"render": "window",
			"requires": "valid_user",
			"aside": "right",
			"dialog": "modal",
			"backgrounds": 1,
			"logo": "./assets/app/gsi.png",
			"primaryColor": "blue",
			"secondaryColor": "orange",
			"connected": [
				// Third parties are objects containing a name and needs that are included per tenant, like this...
				// {"name" : "facebook", "id" : 2298389843, "color": "#ffffff" }
				// For each third party chosen, it must of course be supported by the login app.  Assets come from
				// assets/apps/login/connected/
			],
			"text": {
				"connectWith"		: "OR",
				"tagLine"			: "Population health, personalized.",
				"forgotPassword"	: "Forgot Password?",
				"pleaseLogin"		: "Log in to your account",
				"dontHaveAccount"	: "Having problems?",
				"contactUs"			: "Contact us",
				"contactLink"		: "mailto:support@gsihealth.com",
				"loginButton"		: "LOGIN",
				"loginProgressButton"	:	"Please wait...",
				"invalidUsername"	: "Email must be a valid e-mail address",
				"disclosuresHTML"	: "<p>You are reminded that the data available here may be subject to a Federal SAMHSA Redisclosure Warning.</p><p>Patient data available on this site may be subject to the following regulations:</p><p><strong>Article 27-F of the N.Y. Public Health Law &sect; 2782</strong><br>This information has been disclosed to you from confidential records which are protected by state law. State law prohibits you from making any further disclosure of this information without the specific written consent of the person to whom it pertains, or as otherwise permitted by law. Any unauthorized further disclosure in violation of state law may result in a fine or jail sentence or both. A general authorization for the release of medical or other information is NOT sufficient authorization for further disclosure.</p><p><strong>NYS Mental Hygiene Law &sect; 33.13</strong><br>You shall ensure that any confidential health information shall remain confidential and shall only be stored, accessed, used or disclosed in accordance with applicable provisions of State and Federal law. Under the Mental Hygiene Law, further disclosure of such confidential mental health information is strictly limited to those circumstances in which consent of the patient is obtained, a court order is issued, or the recipient of the disclosed information is otherwise authorized to receive such information under Mental Hygiene Law section 33.13.</p>",
				"switchingMessage"	: "You are about to change to another client environment for which you are authorized. Please make sure you have saved any work that is in-progress.",
				"switchingTitle"	: "Confirm Switch Environments"
			},
			"switching": {
				"showConfirmation"	: true,
				"optOut"			: false
			},
			"dialogs": {
				"mfa": {
					"title": "We need to make sure you are still you",
					"order": 0,
					"triggers": "geolocation,ip,flags"
				},
				"eula1.0": {
					"title": "End User License Agreement",
					"order": 1
				},
				"samsha": {
					"title": "SAMSHA Agreement",
					"order": 2,
					"every": 24
				}
			}
		},
		"tasks": {
			"text":	{
			},
			"engine": "redux",
			"render": "app",
			"icon": "tasks",
			"name": "Task Manager",
			"resource": "tasks",
			"requires": "read_own_tasks",
			"badge": "app.tasks.overdue",
			"types": [
				{
					"id": 0,
					"name": "Backlog"
				}, {
					"id": 1,
					"name": "In the works"
				}, {
					"id": 2,
					"name": "Finished"
				}, {
					"id": 3,
					"name": "Archived"
				}
				],
			"views": [
				{
					"name": "status",
					"display": "Overview"
				}, {
					"name": "calendar",
					"display": "Time"
				}, {
					"name": "experimental",
					"display": "6.0 PREVIEW",
					"requires": "experimental"
				}

			]
		},
		"analitics": {
			"text":	{
			},
			"engine": "external",
			"render": "app",
			"icon": "analitics",
			"name": "Analitics",
			"resource": "analitics",
			"requires": "read_own_analitics"

		},
		"enrollment": {
			"text":	{
			},
			"engine": "gwt",
			"render": "app",
			"icon": "enrollment",
			"name": "Enrollment",
			"resource": "users",
			"requires": "create_any_users"
		},
		"careTeam": {
			"text":	{
			},
			"engine": "gwt",
			"render": "app",
			"icon": "careTeam",
			"name": "Care Teams",
			"resource": "careteams",
			"requires": "read_own_careteams"
		},
		"carePlan": {
			"text":	{
			},
			"engine": "external",
			"render": "app",
			"icon": "carePlan",
			"name": "Care Plan",
			"resource": "careplans",
			"requires": "read_own_careplans"
		},
		"alerts": {
			"text":	{
			},
			"engine": "gwt",
			"render": "app",
			"icon": "alerts",
			"name": "Alerts",
			"resource": "alerts",
			"requires": "read_own_alerts",
			"badge": "app.alerts.count"
		},
		"userSettings": {
			"text":	{
			},
			"engine": "gwt",
			"render": "userMenu",
			"icon": "settings",
			"name": "Preferences",
			"resource": "settings",
			"requires": "read_own_settings"
		},
		"messages": {
			"text":	{
			},
			"engine": "gwt",
			"icon": "messahes",
			"name": "Messages",
			"resource": "messages",
			"requires": "read_own_messages",
			"badge": "app.messages.count"
		},
		"reports": {
			"text":	{
			},
			"engine": "redux",
			"icon": "reports",
			"name": "Reports",
			"resource": "reports",
			"requires": "read_any_reports"
		},
		"admin": {
			"text":	{
			},
			"engine": "gwt",
			"icon": "admin",
			"name": "Admin",
			"resource": "users",
			"requires": "read_all_users"
		},
		"patient": {
			"text":	{
				"careTeamMemberSearchShowConsented": "Show only consented providers",
				"careTeamMemberSearchCurrentlySearching": "Currently searching for a",
				"notPermitedToAddToPatientPanel":"Patient has not granted access to your organization",
				"alreadyMember":"Patient is already on your Patient Panel",
				"AddPatient":"Add to patient Panel"

			},
			"icons": {
				"careTeamMemberSearchFilter": "filter-variant"
			},
			"enrollment":{
                "groups": [
                    { "id": 1,  "text": "Organization",     "order": 1, "show": true  },
                    { "id": 2,  "text": "RHIO Sharing",     "order": 2, "show": true  },
                    { "id": 3,  "text": "Programs",         "order": 3, "show": true  },
                    { "id": 4,  "text": "Provider Access",  "order": 4, "show": true  },
                    { "id": 5,  "text": "Care Team",        "order": 5, "show": true  }
                ],
                "text": {
					"leftNavHeader": "Incomplete/Unsaved Data",
					"unsavedTabHeader": "Unsaved data",
					"noDataTabHeader": "No data entered for these sections",
                    "discardPopupTitle": "Warning!",
                    "discardPopupText1": "There are unsaved changes on the following tabs: ",
                    "discardPopupText2": "Exiting now will cause you to lose all of these changes. <br> Do you still wish to continue?",
                    "discardPopupDiscardBtn": "Discard Changes",
                    "discardPopupCancelBtn": "Cancel",
                },
				"program": {
                    "fields":[
                        {   "id": "parentProgramName",   "label":  "Parent Name",          "order": 1   ,"show": true   },
                        {   "id": "programName",         "label":  "Program Name",         "order": 2   ,"show": true   },
                        {   "id": "consentStatus",       "label":  "Consent",              "order": 3   ,"show": true   },
                        {   "id": "programEffective",    "label":  "Program Effective ",   "order": 4   ,"show": false   },
                        {   "id": "programEnd",          "label":  "Program End",          "order": 5   ,"show": false   },
                        {   "id": "status",              "label":  "Patient Status",       "order": 6   ,"show": true   },
                        {   "id": "statusEffective",     "label":  "Status Effective",     "order": 7   ,"show": true   },
                        {   "id": "endReason",           "label":  "End Reason",           "order": 8   ,"show": true   }
                    ],
					"history":{
                    	"options":{
                    		"absMinDate": "2015-01-01",
							"absMaxDate": 		{ "day": 7 },
							"defaultMinDate":	{ "month" : -6 },
							"defaultMaxDate":	{ "day": 7 },
							"preloadOffsetMonth":	6
						}
					},
                    "defaultSortColumnId": "parentProgramName",
					"consent": {
                    	"notRequired":{
                    		"color": "default-grey-500",
							"icon": "lock-open-outline"
						},
						"notFilled":{
                    		"color": "default-grey-500",
							"icon": "lock"
						},
                    	"statuses": [
							{
								"name": "Yes",
								"color":"default-green-500",
								"icon":"lock"
							},
                            {
                                "name": "No",
                                "color":"default-red-500",
                                "icon":"lock"
                            },
                            {
                                "name": "Pending",
                                "color":"default-orange-500",
                                "icon":"lock"
                            },
                            {
                                "name": "Unknown",
                                "color":"default-grey-500",
                                "icon":"lock"
                            }
						]
					},
                    "text":{
                        "intro" :			"Select a program to edit or delete it. Click the plus icon to create a new program.",
                        "consentIntro": 	"Consent is required for this program.<br />Please fill out the following fields before continuing.",
                        "consentTitle":				"Give Consent",
                        "consentObtainedByLabel": 	"Consent Obtained By",
                        "consentDateLabel":			"Consent Date",
                        "consenterLabel":			"Consenter",
                        "minorSharingIntro": 			"This patient is a minor. To edit their information sharing, please fill out the following fields.",
                        "minorSharingTitle": 			"Information Sharing",
                        "minorSharingObtainedByLabel": 	"Consent Obtained By",
                        "minorSharingDateLabel":		"Consent Date",
                        "consentOkButton"	:			"Finish",
                        "consentCancelButton":			"Cancel",
                        "deleteProgramTitle": 			"Delete Program",
                        "deleteProgramReasonLabel": 	"Select a Reason",
                        "deleteProgramButton": 			"Delete",
                        "deleteProgramErrorDefault": 	'An error occurred while deleting a program.',
                        "deleteProgramSuccess": 		'Successfully deleted the most recent program episode.',
                        "deleteProgramWarning": 		'The program has been successfully deleted, but previous program data could not be retrieved. Please inform the help desk.',
                        "searchInputLabel":				"Search...",
						"loadErrorMessage":				"Error loading programs. Please try again later.",
						"reloadButton":					"Reload",
						"cancelButton":					"Close",
						"addButtonTooltip":				"Add a new program",
                        "historyButtonTooltip":			"View Program History",
						"subRowCollapseTooltip":		"Collapse",
						"subRowOpenTooltip":			"Expand to see child programs",
						"subRowActiveTooltip":			"Active program",
						"subRowInactiveTooltip":		"Inactive program",
						"consentTooltipPrefix":			"Consent: ",
						"txtBlankConsent": 				"Select Status",
						"historyCloseButton":			"Back",
						"historyErrorMessage":			"We encountered a problem with Program History Timeline. Please try again later.",
						"historyHasErrorMssg":			"We encountered a problem while processing Program History Timeline. Please contact the help desk.",
						"editConsentMenu":				"Edit Consent",
						"deleteMenu":					"Delete",
						"revertMenu":					"Revert",
						"consentNotRequired":			"N/A"
                    },
					"icons":{
                    	"add": "plus",
                    	"search": "magnify",
                    	"viewHistory":"clock",
						"sortUp": "menu-up",
						"sortDown": "menu-down",
						"subProgramCollapse": "minus",
						"subProgramOpen" :	"plus",
						"contextMenu": "dots-vertical",
						"deleteMenuItem" : "delete",
						"consentMenuItem" : "lock",
						"revertMenuItem" : "history"
                    }
				},
				"rhio":{
                    "text": {
                        "intro": "Flip the switch on permit RHIO Information Sharing. Select an organization to permit sharing. <br />De-select to revoke permissions. Flip the switch off to turn off all sharing.",
                        "readOnlyIntro": "The system automatically shares and receives information for this patient at appropriate times with the following entities. e.g. RHIOs or HIEs:",
                        "empty": "No RHIO Information Sharing configuration is available.",
                        "label": "Permit Sharing",
						"atLeastOneRequired": 	"At least one organization must be selected.",
                        "loadErrorMessage":		"Error loading RHIO Information Sharing. Please try again later.",
                        "reloadButton":			"Reload",
						"cancelButton":			"Close"
                    },
                    "icons":{

                    }
				},

               
                "organization":{
                	"text":{
					"chooseDropDown":"Choose a Primary CMA for the patient using the dropdown.",
					"primaryCma":"Primary CMA",
					"otherCma":"Other CMA(s)",
					"btnSave":"Save",
					"btnCancel":"Close",
					"addGroup":"Add a Group",
					"otherOrganization":"Search for organizations...",
					"noSearch":"No Match For",
					"searchCMA":"Type the name of an organization or group below to add it to the list of other CMAs.",
					"warning":"Warning!",
					"disableMsg":"Can not be saved",
					"warnMsg":"You are about to change the Primary CMA. Users from previous organization will continue to have access. Continue?"
                },
                "icons":{

                  "closeIcon":"close"

				}

            },
                "manageProvider":{
					"text1":"Type the name of the provider’s organization and select to add it in to list.  Click 'X' icon to remove it.",
					"text2":"CMA and Auto-consented organizations (shown colored) without 'X' cannot be removed on this page.  If groups are configured they may be found by searching for the word ‘Group-‘",
					"mpaLabel":"Provider Organizations with Access to Patient"
                },

               "careTeam":{
               	"text":{

					"text1":"Click a card's context menu to edit or remove it. Click the plus icon to add a new member.",
					"text2":"Click the action menu (ellipsis) and select the clock icon to check for any previously existing Care Teams for this patient.",
					"currentteamAssigned":"Current Care Team Assigned",
					"chooseNewTeam":"Current Care Team assigned",
					"priorCareText": "A previous Care Team was found. To add or remove members from the list to be imported, select or deselect the row with their information, and then press the \"Import\" button.",
					"clockToolTip":"Check for and load a previously existing team",
					"errorText1":"There's nothing here",
					"errorText2":"click the '+' icon add a member",
					"chooseRole":"Choose a new Role",
					"chooseTeam":"Choose a New Team",
					"filterConfig":"Filter by user role",
					"oneMemberRuleTitle": "Cannot Save",
					"oneMemberRuleMsg1": "Care Team must have at least one member",
					"oneMemberRuleMsg2": "Care Team must have at least one member before updating patient information",
					"oneMemberRuleMsg3": "Care Team must have at least one member with any role belong to required roles: {0}",
					"inActiveMsg":"This person is not an active user of GSIHealthCoordinator. They will not be able to see the record about the patient or interact with them about the platform. Adding them to the patient's Care Team will serve only as a reference to other GSIHealthCoordinator users.",
					"createNew":"Create New",
					"crateNewExternal":"Create new external provider",
					"createNow":"Would you like to create a new one now?",
					"addMember":"Add New Member",
					"careTeamAdd":"Add to Care Team",
					"removeCareTeam": "Remove patient from Care Team",
					"secureMessaging": "Add patient to Care Team with direct secure messaging",
					"priorCareTeam": "Previously existing Care Team",
					"changeForAll":"Change for All",
					"createNewSeparateTeam":"Create Separate Team",
					"directMessaging":"Direct messaging requires patient email address.  Please enter the email address through Demographics.",
					"removeFromCare":"This will unassign the Care Team from Patient. Continue?",
                    "yes":"yes",
                    "viewMore":"View more",
                    "change":"Change",
                    "remove":"Remove",
                    "mandatoryMembers":"The following care team members need to be added before the care team can be saved:",
                    "missingMemberLabel":"Missing members!",
                    "duplicationLabel":"Only the below roles can be duplicated:",
                    "consentedLabel":"This provider's organization does not currently have consent to view this patient's complete patient record.",
                    "permissionLabel":"Would you like to grant it now?",
                    "grantPermissionLabel":"Would you like to grant it now?",
                    "careValidate":"Care Team name already exists. Please enter a unique name",
                    "noResult":"No matching record found",
                    "noPrevious":"No previous Care Team was found for this patient.",
                    "importLabel":"Import",
                    "yesLabel":"Yes",
                    "noLabel":"No",
                    "continueLable":"Do you want to continue?",
                    "searchText":"Search...",
                    "cancelLabel":"Close",
                    "backLabel":"Back",
                    "editLabel":"Edit",
                    "consentAdvisoryPopupTitle": "Consent Advisory for Care Team: ",
					"consentAdvisoryPopupOkBtn": "Yes",
					"consentAdvisoryPopupCancelBtn": "Cancel",
					"consentAdvisoryPopupText1": "The assigned Care Team members: ",
					"consentAdvisoryPopupText2": "DO NOT have consent to access the patient's information.",
					"consentAdvisoryPopupText3": "If you would like to grant access to all the organizations listed, please click ‘YES’. Otherwise, please click ‘CANCEL’ and remove or replace individual members on the Care Team.",
					"prefix": "Prefix",
					"firstName": "First Name",
					"lastName": "Last Name",
					"credential": "Credential",
					"NPI": "NPI",
					"primPracitionerFacility": "Primary Pracitioner Facility",
					"gender": "Gender",
					"dob": "DOB",
					"phone": "Phone Number",
					"phoneExt": "Extension",
					"directAddress": "Direct Address",
					"address1": "Address",
					"address2": "Address Line 2",
					"city": "City",
					"state": "State",
					"zipCode": "Zip Code",
					"email": "Email",
					"specialty": "Specialty",
					"careTeamRole": "Care Team Role",
					"externalProvider":"External provider",
					"currentMember":"Current Member",
					"unconsentedProvider":"Unconsented Provider",
					"currentMmember":"Current Member",
					"noDataLabel": "(None)",
					"priorImportTextPrefix": "Currently importing ",
					"priorImportTextSuffix": " provider(s)...",

                },
				"oneMemberInCareTeam": false,
                "icons":{
                 "plusIcon":"plus",
                 "vertiCalDots":"dots-vertical",
                 "domain":"domain",
                 "phoneIcon":"cellphone-iphone",
                 "email":"email",
                 "marker":"map-marker",
                 "import":"redo",
                 "eye":"eye",
                 "clock":"clock",
                 "remove":"delete",
                 "account":"account",
                 "alertAccount":"account-alert",
                 "offAccount":"person-external",
                 "keyChange":"swap-horizontal",
				 "gender": "human-male-female",
				 "phone": "cellphone-android",
				 "directAddress": "email",
				 "address": "map-marker",
				 "addProvider": "person-add",
				 "org": "domain",
				 "personAdded":"person-added",
				 "notConsented":"person-not-consented"
                },

				"iconColors":{
               		"notConsented":"default-red-400",
				   "userInactive": "default-grey-400",
				   "currentMember": "default-green-400",
				   "userExternal": "default-blue-400"
				}

            }
			}
		},
		"carebook": {
			"showUniversalSearch": true,
			"remoteCommunityWarning": "You are about to access a consolidated patient’s record in remote community. Do you have consent?",
			"text":{
				"notConsentedTooltip":"Your organization has not been granted access to this patient."
			}
		}
	}

}

