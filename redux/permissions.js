// An brand admin with access to view, cancel, and void any order in his brand 
permissions = {

	viewSububordinantPatients: {
		concern: 'patients',
		match: {
			id: ':'
		},
		grant: {
			read: true
		}
	}
    match: {
        brandId: 'zcafe'
    },
    grant: {
        get: true,
        cancel: true,
        void: true
    }
} ];
var permSet = new PermissionSet(permArray);
var orderPermissionData = {
    brandId: 'zcafe',
    orderId: 'abcde12345'
};


event = getSubordinatePatients(data) => {
	
}


TenantRoles = {
	"user": [
		"all_own_resources",	// all = all permissions, own = self, resources = all items
	],
	"powerUser"
}


// Permissions in the database that do not meet these criteria are invalid...
"security": {
	"actions": 	[		// Services must check if the action being performed is authorized
		"all",
		"create",
		"read",
		"update",
		"delete",
		"impersonate"
	],
	"concerns":	[		// Services must create functions to determine the concern raised with each action
		"all",
		"own",
		"sub"
	],
	"resources": [		// Services only need to check for their resource type
		"resources",
		"tasks",
		"patients",
		"messages",
		"alerts",
		"telemetry",
		"users",
		"groups",
		"organizations",
		"tenants",
		"reports"
	]
}