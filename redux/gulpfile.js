// including plugins
var gulp = require('gulp');
var Server = require('karma').Server;

var run = require('gulp-run');
var  nodemon = require('gulp-nodemon')


// task
gulp.task('test', function (done) {
  new Server({
    configFile: require('path').resolve('karma.conf.js'),
    singleRun: true
  }, done).start();
});


gulp.task('start', function () {
  nodemon({ script: 'app.js', tasks: ['test'] })
    .on('restart', function () {
      console.log('restarted!')
    })
})