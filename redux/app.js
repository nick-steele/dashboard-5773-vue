var s = require('./s/src/simplex');

s.preBoot = function(callback) {
	s.newrelic = require('newrelic'); // For admin monitoring.
	
    s.setConfiguration = function(communityId, key, value) {
        s.settings.config[key + '_' + communityId] = value;
    }

    s.getConfiguration = function(communityId, key) {
        return s.settings.config[key + '_' + communityId];
    }

    // Get the configuration information...
    console.log('Getting configuration from', s.settings.env.endpoints.configuration );
	
	// Uncomment the following two lines to work on the train/etc...
	//callback();
	//return;
	
    s.REST('GET', s.settings.env.endpoints.configuration + 'dashboard/rest/configuration', function(data) {
        // Success...
        console.log('Configuration received.');
        s.settings.config = {};

        /*
        console.log(JSON.stringify(data))
        const fs = require('fs');
        const content = JSON.stringify(data);
        
        fs.writeFile("outsettings.json", content, 'utf8', function (err) {
            if (err) {
                return console.log(err);
            }
        
            console.log("The file was saved!");
        });
        */

        // Now convert it from configurationPK entries in a list to a tree...
        data.List.forEach(function(item) {
            if (item.active === 'Y') {
                s.setConfiguration(item.configurationPK.communityId, item.configurationPK.name, item.value);
            }
        });
        console.log('Configuration converted.');
        console.log('Tasking:' + s.settings.config['redux.rest.tasks']);
        // Ready, so listen for connections...
        s.server.on('error', function(e) {
            console.log("\n A SERVER ERROR OCCURED: " + e.code + "\n");
            switch(e.code) {
                case 'EADDRINUSE':
                    console.log("Something else is using port " + e.port + ". The most likely cause of this is that Redux is already running on this machine.\n" +
                                "If you are redeploying, please end all other instances of Node first.\nIf Node isn’t running, find out what is using port " +
                                e.port + " and reconfigure it.\nIf something else *MUST* use port " + e.port + " on this machine, you must configure Redux to " +
                                "use a different port number in D:/Node_Apps/Redux/app-config.js (client.vars.nodePort), and reconfigure the firewall to pass " +
                                "Node traffic through this port as well, then restart Redux.\n\nRedux cannot listen for traffic until this issue is fixed.");
                    process.exit();
                    break;
                default:
                    console.log('An unhandled Node server error occured:', + e, ' The server should probably be restarted at this point.');
                    break;
            }
            console.log(e);
        });
		callback();

    }, function(data) {
        // Failure...
        console.log("CATASTROPHIC FAILURE: Couldn't get configuration data from endpoints.localhost.  If the endpoint isn't localhost:8080, make sure you include it in D:\\Node_Apps\\server-config.js");
        process.exit();
    });
};

s.run();