//Make sure to run this after compiling code `node app c`


//Test dependency
let nock = require('nock'),
    expect = require('expect.js'),
    sinon = require('sinon'),
    ioClient = require('socket.io-client');
const userManager = 'http://usermanager/',
    port = 8002,
    socketURL = `http://localhost:${port}`;

//Least required server side code (Need review)
let s = require('../s/src/simplex');
s.newrelic = require('newrelic');
s.io = require('socket.io').listen(port);
s.router = require('../s/compiled/server-router');
s.settings = {
    client:{
        vars:{
            version: '0.0.0'
        }
    },
    app:{},
    env:{
        endpoints:{
            userManager:userManager
        }
    },
    config:{}
};

s.getConfiguration = function(communityId, key) {
    return s.settings.config[key + '_' + communityId];
}


describe('socket.io test suite', function() {

    let client1;

    function getSocketConnection() {
        return ioClient.connect(socketURL, {
            'reconnection delay': 0
            , 'reopen delay': 0
            , 'force new connection': true
            , transports: ['websocket']
        });
    }

    beforeEach(function(done) {
        // Setup
        client1 = getSocketConnection();

        client1.on('connect', () => {
            console.log('[client1] connected!');
        });

        client1.on('function', (message)=>{
            expect(message).to.have.property('func','socketConnected');
            done();
        });

        client1.on('disconnect', () => {
            console.log('[client1] disconnected...');
        });
    });

    afterEach((done) => {
        // Cleanup
        client1.emit('disconnect', { event: 'data' });
        done();
    });

    it('should sync token on reconnect', (done) => {

        //mock server
        nock(userManager)
            .post('/user/usercommunity/token')
            .reply(200, {token:'1234'});

        // Request sync
        client1.emit('sync', {payload: {token: 'token', data: {loginData: 'loginData' }}});
        client1.on('d',(message)=>{
            // identity data should come back to client
            expect(message).to.have.property('app');
            expect(message.app).to.have.property('identity');
            client1.emit('getData',{payload:'identity'});

            // identity data should be set in server
            client1.on('debug', (message)=>{
                expect(message).to.have.property('token');
                done();
            });
        });
    });

    it('should not mix sockets', (done) => {

        //mock server
        nock(userManager)
            .post('/user/usercommunity/token')
            .reply(200, {token:'1234'});

        //Connect second client
        let client2 = getSocketConnection();

        client2.on('connect', () => {
            console.log('[client2] connected');

            //client 1 sends sync request
            client1.emit('sync', {payload: {token: 'token', data: {loginData: 'loginData' }}});

            // identity data should come back to client 1, not 2
            client1.on('d',(message)=>{
                console.log("[client1] received", message);
                expect(message).to.have.property('app');
                expect(message.app).to.have.property('identity');
                client1.emit('getData',{payload:'identity'});
                client1.on('debug', (message)=>{
                    expect(message).to.have.property('token');
                    done();
                });
            });

            //If client 2 receives data, fail
            client2.on('d',(message)=>{
                console.log("[client2] received", message);
                if(message && message.app && message.app.identity){
                    expect().fail("client 2 should not receive this identity data.");
                    done();
                }
            });
        });
    });


    //Issue not fixed yet, will fail
    it('should log with correct signature', (done) => {

        //mock server
        //Add intentional delay
        let patientListManager = "http://pm.pm";
        nock(patientListManager)
            .get('/tags/1')
            .delay(500)
            .reply(200, {List:[{},{}]});

        s.settings.config['patient.list.server_1'] = patientListManager;


        let client2 = getSocketConnection();

        //Push identity data for client 1
        client1.emit('sync', {payload: { data: {userId:1111, email:'client1',communityId:1}}});
        //Make a http request (will respond with 500ms delay)
        client1.emit('myPatientList', {event:'tags'});


        let spy;
        //Connect second client
        client2.on('connect', () => {
            console.log('[client2] connected');
            client2.on('function', (message)=>{
                expect(message).to.have.property('func','socketConnected');
                //Push identity data for client 2
                client2.emit('sync', {payload: { data: {userId:2222, email:'client2',communityId:2}}});
                //Spy with little delay to capture log produced by http request callback only
                setTimeout(()=>{spy = sinon.spy(console,'log');},200);
            });
        });


        client1.on('d',(message)=>{
            //on sync http request complete
            if(message && message.myPatientList && message.myPatientList.tags) {
                //Captured values
                let logs = spy.args;
                spy.restore();
                //Log should contain correct signature
                expect(logs[0][0]).to.contain('client1|1|1111');
                done();
            }
        });

    });



});