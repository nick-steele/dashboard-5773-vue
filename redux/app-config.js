module.exports = {
	"app":	{
		"db"		: { host: "localhost", user: "root", password: "", database: "test" },
		"mode"		: "d",
		"wrapper"	: "redux",
		"concat"	: false,
		"uglify"	: false,
		"index"		: "/index.jsp",
		"webSettings": "./../../../app-config-web.js"
	},
	"services": {
		login:		{	endpoint:	"test",			protocol:	"REST.GET",		response:	"model;app.login",		enclosure:	"data",			format:	":data.username/:data.password"				},
		test:		{	endpoint:	"test",			protocol:	"REST.GET",		response:	"log",					enclosure:	"",				format:	""											},
		taskList:	{	endpoint:	"task",			protocol:	"REST.GET",		response:	"model;task.tasks",		enclosure:	"UserTaskList",	format:	"usertask/:login.email/:login.commId"		}
	},
	"apps": {
		"appTask": {
			"name"		: "Task Manager",
			"render"	: "angular",
			"badge"		: true
		},
		"appEnrollment": {
			"name"		: "Enrollment",
			"render"	: "angular"
		},
		"appCareteams": {
			"name"		: "Care Teams",
			"render"	: "angular"
		},
		"appCareplan": {
			"name"		: "Care Plan",
			"render"	: "angular"
		},
		"appAlerts": {
			"name"		: "Alerts",
			"render"	: "GWT",
			"badge"		: true
		},
		"appMessages": {
			"name"		: "Messages",
			"render"	: "GWT",
			"target"	: "window",
			"badge"		: true
		},
		"appPopmanager": {
			"name"		: "Population Manager",
			"access"	: "admin,power",
			"render"	: "GWT"
		},
		"appReports": {
			"name"		: "Reports",
			"access"	: "admin,power",
			"render"	: "angular"
		},
		"appAdmin": {
			"name"		: "Admin",
			"access"	: "admin",
			"render"	: "GWT"
		},
		"appUHCEnroll": {
			"name"		: "UHC Enrollment Report",
			"access"	: "admin",
			"community"	: "3",
			"render"	: "GWT"
		},
	},
	"client": {
		"vars": {
			"name"		: "coordinator",
			"version"	: "6.0.0",
			"title"     : "Health Home Dashboard",
			"nodePort"  : 8001
		}
	},
	"security": {		// Permissions default to : "user" ... types are "all,user,power,admin"
		"events": {
			"login"					: "all",
			"app.alert"				: "admin,power",
			"app.data"				: "admin",
			"appTask.subordinates"	: "admin"
		}
	}
};