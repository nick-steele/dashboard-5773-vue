/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.pixinterface;

import org.hl7.v3.PRPAIN201302UV02;

/**
 *
 * @author Chad Darby
 */
public class MessageUtil {

    public static PRPAIN201302UV02 getTestPRPAIN201302UV02() throws Exception {
        PIXSender ps = new PIXSender();

        PRPAIN201302UV02 test = ps.getTestPRPAIN201302UV02();
        
        return test;
    }
}
