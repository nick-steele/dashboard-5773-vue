package com.gsihealth.dashboard.tools.dbscripts;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import org.apache.commons.io.FileUtils;

/**
 * Read all of the *.sql files in a directory (in order) and generate a single file.
 * 
 * For example, input files in directory: database\db-updates-release-3.1
 * - db-updates-00-delete-legacy-tables.sql
 * - db-updates-01-notification.sql
 * - ...
 * 
 * All of the files will go into:
 * - db-updates-all.sql
 *
 * @author Chad Darby
 */
public class BuildSingleDbScript {

    public void run() throws Exception {
        // prompt for directory
        String directoryName = prompt();
        
        // read all files
        File[] files = readFiles(directoryName);
        
        // append the files
        StringBuilder totalData = appendFiles(files);
        
        // write out new file
        saveFile(directoryName, "db-updates-all.sql", totalData);
    }
    
    private String prompt() {

        System.out.println("BUILD SINGLE DB SCRIPT");
        System.out.println("======================\n\n");

        System.out.println("What is the directory for db script files?");
        Scanner scanner = new Scanner(System.in);
        String directoryName = scanner.nextLine();

        File theDir = new File(directoryName);
        
        if (!theDir.exists()) {
            System.out.println("\nError: The directory does not exist");
            System.exit(1);
        }
        
        return directoryName;
    }
    
    private File[] readFiles(String directoryName) {
        
        File theDir = new File(directoryName);

        File[] files = theDir.listFiles(new DbScriptFilenameFilter());
                
        Arrays.sort(files);

        for (File temp : files) {
            System.out.println(temp);
        }
        
        return files;
    }

    private StringBuilder appendFiles(File[] files) throws Exception {
    
        StringBuilder result = new StringBuilder();
        
        for (File temp : files) {
            System.out.println("Reading file: " + temp + "...");
            
            result.append("\n\n--\n");
            result.append("-- Source File: " + temp.getName() + "\n");
            result.append("--\n\n");
           
            List<String> lines = FileUtils.readLines(temp);
        
            for (String tempLine : lines) {
                result.append(tempLine + "\n");
            }
        }
        
        return result;
        
    }

    private void saveFile(String directoryName, String dbupdatesallsql, StringBuilder totalData) throws Exception {
        
        File totalFile = new File(directoryName, dbupdatesallsql);
        
        FileUtils.writeStringToFile(totalFile, totalData.toString());
        
        System.out.println("\nFinished writing file: " + totalFile + "\n");
    }

    public static void main(String[] args) throws Exception {
        BuildSingleDbScript app = new BuildSingleDbScript();
        app.run();
    }


}
