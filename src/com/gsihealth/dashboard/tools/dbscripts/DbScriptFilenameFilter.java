package com.gsihealth.dashboard.tools.dbscripts;

import java.io.File;
import java.io.FilenameFilter;

/**
 *
 * @author Chad Darby
 */
public class DbScriptFilenameFilter implements FilenameFilter {

    private static final String REGEX = "db-updates-[0-9][0-9].+\\.sql";
    
    public boolean accept(File dir, String name) {
        
        return name.matches(REGEX);
    }
    
}
