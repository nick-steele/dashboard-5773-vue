package com.gsihealth.dashboard.tools.messages;

import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.entity.PatientCommunityEnrollment;
import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.dao.CommunityDAO;
import com.gsihealth.dashboard.server.dao.CommunityDAOImpl;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.dashboard.server.dao.ConfigurationDAOImpl;
import com.gsihealth.dashboard.server.dao.NotificationAuditDAO;
import com.gsihealth.dashboard.server.dao.NotificationAuditDAOImpl;
import com.gsihealth.dashboard.server.dao.OrganizationDAO;
import com.gsihealth.dashboard.server.dao.OrganizationDAOImpl;
import com.gsihealth.dashboard.server.dao.OrganizationPatientConsentDAO;
import com.gsihealth.dashboard.server.dao.OrganizationPatientConsentDAOImpl;
import com.gsihealth.dashboard.server.dao.PatientEnrollmentDAO;
import com.gsihealth.dashboard.server.dao.PatientEnrollmentDAOImpl;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import com.gsihealth.dashboard.server.patientmanagement.PatientManagerImpl;
import com.gsihealth.dashboard.server.service.EnrollmentServiceHelper;
import com.gsihealth.dashboard.server.util.NotificationUtils;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

/**
 *
 * @author Chad Darby
 */
public class MessageTool {

    private final static String ADD = "ADD";
    private final static String UPDATE = "UPDATE";

    private static void validateProps(String fileName, String modeStr, String communityId) {

        if (StringUtils.isBlank(fileName)) {
            throw new IllegalArgumentException("Missing property for patient.data.filename");
        }

        if (StringUtils.isBlank(modeStr)) {
            throw new IllegalArgumentException("Missing property for mode");
        }
        
        if (StringUtils.isBlank(communityId)) {
            throw new IllegalArgumentException("Missing property for community id");
        }
    }
    
    private ConfigurationDAO configurationDAO;
    
    private OrganizationPatientConsentDAO organizationPatientConsentDAO;
    private NotificationAuditDAO notificationAuditDAO;
    private PatientEnrollmentDAO patientEnrollmentDAO;
    private OrganizationDAO organizationDAO;
    private CommunityDAO communityDAO;
    private PatientManager patientManager;
    private String fileName;
    private Logger logger = Logger.getLogger(getClass().getName());
    private String mode;
    private long communityId;
    
    public MessageTool(String theFileName, String theMode, String communityIdStr) {
        fileName = theFileName;
        mode = theMode;
        communityId = Long.parseLong(communityIdStr);
    }

    public void run() throws Exception {

        prompt();

        // return persons from database
        List<PersonDTO> persons = getPatientsFromDatabase();

        // create enrollment service helper
        EnrollmentServiceHelper enrollmentServiceHelper = buildEnrollmentServiceHelper();
        int count = 1;
        
        for (PersonDTO tempPerson : persons) {
            PatientEnrollmentDTO patientEnrollment = tempPerson.getPatientEnrollment();
            logger.info("\n\nProcessing for person id: " + patientEnrollment.getPatientId() + ",  lastName=" + tempPerson.getLastName());
            logger.info(count + " out of " + persons.size() + ".  Percent complete: " + getPercentage(count, persons.size()) + "\n\n");

            try {
                if (StringUtils.equalsIgnoreCase(ADD, mode)) {
                    enrollmentServiceHelper.sendAddNotificationsToNotificationManager(tempPerson,communityId);
                } else if (StringUtils.equalsIgnoreCase(UPDATE, mode)) {
                    enrollmentServiceHelper.sendUpdateNotificationToNotificationManager(tempPerson,communityId);
                } else {
                    throw new IllegalArgumentException("Unknown mode");
                }
            } catch (Exception exc) {
                logger.severe("\n\n>>>>>>>>>>   Failed for person id: " + patientEnrollment.getPatientId() + ",  patientEuid=" + patientEnrollment.getPatientEuid());
                exc.printStackTrace();
            }

            count++;
        }
    }

    private void prompt() {

        System.out.println("Performing message sync for fileName: " + fileName);
        System.out.println("Mode: " + mode);
        System.out.println("Community: " + communityId);

        System.out.println("Are you sure yes/[no]?");

        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        if (StringUtils.startsWithIgnoreCase(input, "yes")) {
            return;
        } else {
            System.out.println("\nOperation cancelled.\n");
            System.exit(0);
        }
    }

    private static Properties loadProperties(String propsFileName) throws IOException {
        Properties theProps = new Properties();
        System.out.println("Loading properties file: " + propsFileName);
        theProps.load(new FileReader(propsFileName));
        System.out.println("PROPERTIES:");
        theProps.list(System.out);
        

        return theProps;
    }

    public static void main(String[] args) {

        try {
            
            System.out.println("args 2: " + args[2]);
            if (args.length != 3) {
                System.out.println("Missing arguments. Usage: MessageTool <mode> <fileName> <community>");
                System.out.println("mode = Add or Update (case-insensitive)"); 
                System.out.println("ex: message-tool-qa update test.txt 1");
                System.exit(0);
            }
            
            String theMode = args[0];
            String fileName = args[1];
            String communityIdStr = args[2];
            
            MessageTool.validateProps(fileName, theMode, communityIdStr);

            MessageTool messageTool = new MessageTool(fileName, theMode, communityIdStr);
            messageTool.run();
            System.exit(0);
            
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    private List<PersonDTO> getPatientsFromDatabase() throws Exception {

        // read patient ids from file
        List<Long> patientIds = readPatientIdsFromFile();
        logger.info("patientIds: " + patientIds);
        
        // setup the DAOs
        logger.info("setting up daos...");
        setupDaos();
        
        // load MDM
        loadMdm(this.communityId);


        // now patients get from DAOs
        Map<String, PatientCommunityEnrollment> patientEnrollmentMap = new HashMap<String, PatientCommunityEnrollment>();

        logger.info("loading patients from connect DB...");
        int count = 1;
        for (Long tempPatientId : patientIds) {
            logger.info(count + " out of " + patientIds.size() + ".  Percent complete: " + getPercentage(count, patientIds.size()));

            PatientCommunityEnrollment patientEnrollment = patientEnrollmentDAO.findPatientEnrollment(tempPatientId, communityId);
            String euid = patientEnrollment.getPatientEuid();

            patientEnrollmentMap.put(euid, patientEnrollment);

            count++;
        }

        List<PersonDTO> persons = findGenericPatients(patientEnrollmentMap);

        logger.info("got the patients: " + persons);

        return persons;
    }

    private EnrollmentServiceHelper buildEnrollmentServiceHelper() {
        NotificationUtils.setCommunityDAO(communityDAO);
        //JIRA 629 - remove properties
        EnrollmentServiceHelper enrollmentServiceHelper = new EnrollmentServiceHelper(patientManager,
                notificationAuditDAO, organizationPatientConsentDAO,
                patientEnrollmentDAO, organizationDAO);

        return enrollmentServiceHelper;
    }

    private List<Long> readPatientIdsFromFile() throws IOException {

        List<String> lines = FileUtils.readLines(new File(fileName));

        List<Long> patientIds = new ArrayList<Long>();

        for (String temp : lines) {
            long val = Long.parseLong(temp);
            patientIds.add(val);
        }

        return patientIds;
    }

    private void setupDaos() {
        organizationDAO = new OrganizationDAOImpl();
        organizationPatientConsentDAO = new OrganizationPatientConsentDAOImpl();
        notificationAuditDAO = new NotificationAuditDAOImpl();
        configurationDAO = new ConfigurationDAOImpl();
        ConfigurationContext.getInstance().setConfigurationDAO(configurationDAO);
        patientEnrollmentDAO = new PatientEnrollmentDAOImpl(patientManager, configurationDAO);
        communityDAO = new CommunityDAOImpl();
    }

    private void loadMdm(long communityId) throws Exception {
        String mdmEndpoint = configurationDAO.getProperty(communityId, "patient.mdm.endpoint");
        logger.info("Loading MDM, endpoint = " + mdmEndpoint);

        String defaultOid = "2.16.840.1.113883.3.1358";
        String gsiOid = configurationDAO.getProperty(communityId, "gsihealth.oid");
        logger.info("getting gsioid: " + gsiOid + " for community: " + communityId);
        patientManager = new PatientManagerImpl(new HashMap<String, String>());
    }

    private List<PersonDTO> findGenericPatients(Map<String, PatientCommunityEnrollment> patientEnrollments) throws Exception {

        List<PersonDTO> searchResults = new ArrayList<PersonDTO>();
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        // get the patient enrollment
        Set<Map.Entry<String, PatientCommunityEnrollment>> entries = patientEnrollments.entrySet();

        logger.info("\n\nReading patient data from MDM\n\n");

        int count = 1;
        int size = entries.size();
        for (Map.Entry<String, PatientCommunityEnrollment> tempEntry : entries) {
            String euid = tempEntry.getKey();
            PatientCommunityEnrollment tempPatientEnrollment = tempEntry.getValue();

            Person tempPerson = null;

            logger.info(count + " out of " + size + ".  Percent complete: " + getPercentage(count, size));

            try {
                tempPerson = new Person(patientManager.getPatient(euid));
                tempPerson.setPatientCommunityEnrollment(tempPatientEnrollment);
                PersonDTO personDto = mapper.map(tempPerson, PersonDTO.class);
                searchResults.add(personDto);
            } catch (Exception exc) {
                // skip this patient
                String message = "Could not find patient ID in MDM: " + euid;
                logger.log(Level.SEVERE, message, exc);
                continue;
            } finally {
                count++;
            }

            
        }

        return searchResults;
    }

    private String getPercentage(int count, int size) {

        double val = (count / (float) size) * 100;

        return String.format("%.0f%%", val);
    }
}
