package com.gsihealth.dashboard.monitor;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.service.BasePortalServiceServlet;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
public class MonitorAspect {

	static final Logger log = Logger.getLogger("monitorLogger");

    /**
     *
     * @param pjp
     * @return
     * @throws Throwable
     */
    @Around("(execution(* com.gsihealth.dashboard.server.service.AdminServiceImpl.*(..)) || "
			+ "execution(* com.gsihealth.dashboard.server.service.AlertServiceImpl.*(..)) || "
                        + "execution(* com.gsihealth.dashboard.server.patientmanagement.PatientManagerImpl.*(..)) || "
                        + "execution(* com.gsihealth.patientmdminterface.PatientInterface.*(..)) || "
                        + "execution(* com.gsihealth.patientmdminterface.PatientInterface.*(..)) || "
                        + "execution(* com.gsihealth.dashboard.server.service.EnrollmentServiceHelper.*(..)) || "
                        + "execution(* com.gsihealth.dashboard.server.service.PatientConsentUtils.*(..)) || "
			+ "execution(* com.gsihealth.dashboard.server.service.CareteamServiceImpl.*(..)) || "
			+ "execution(* com.gsihealth.dashboard.server.service.DocumentResourceCenterServiceImpl.*(..)) || "
			+ "execution(* com.gsihealth.dashboard.server.service.EnrollmentServiceImpl.*(..)) || "
                        + "execution(* com.gsihealth.dashboard.server.util.EnrollmentUtils.*(..)) || "
                        + "execution(* com.gsihealth.patientmdminterface.PatientMDMInterface.*(..)) || "
                        + "execution(* com.gsihealth.patientmdminterface.PatientMirthImpl.*(..)) || "
                        + "execution(* com.gsihealth.dashboard.server.service.BasePatientService.*(..)) || "
			+ "execution(* com.gsihealth.dashboard.server.service.ManagePatientProgramServiceImpl.*(..)) || "
			+ "execution(* com.gsihealth.dashboard.server.service.MessageCountServiceImpl.*(..)) || "
			+ "execution(* com.gsihealth.dashboard.server.service.MyPatientListServiceImpl.*(..)) || "
			+ "execution(* com.gsihealth.dashboard.server.service.NonHealthHomeProviderServiceImpl.*(..)) || "
			+ "execution(* com.gsihealth.dashboard.server.service.OrganizationPatientConsentServiceImpl.*(..)) || "
			+ "execution(* com.gsihealth.dashboard.server.service.OrganizationServiceImpl.*(..)) || "
			+ "execution(* com.gsihealth.dashboard.server.service.PatientActivityTrackerServiceImpl.*(..)) || "
			+ "execution(* com.gsihealth.dashboard.server.service.PatientOtherIdsServiceImpl.*(..)) || "
			+ "execution(* com.gsihealth.dashboard.server.service.PayerPlanAdminServiceImpl.*(..)) || "
			+ "execution(* com.gsihealth.dashboard.server.service.ReportServiceImpl.*(..)) || "
			+ "execution(* com.gsihealth.dashboard.server.service.SamhsaServiceImpl.*(..)) || "
			+ "execution(* com.gsihealth.dashboard.server.service.SecurityServiceImpl.*(..)) || "
			+ "execution(* com.gsihealth.dashboard.server.service.SubcriptionManagerAlertServiceImpl.*(..)) || "
			+ "execution(* com.gsihealth.dashboard.server.service.TaskCountServiceImpl.*(..)) "
			+ ") && "
				+ "!execution(* com.gsihealth.dashboard.server.service.*.getAssociatedUser(..))")
	public Object aroundWithReturn(ProceedingJoinPoint pjp) throws Throwable {

		long start = System.currentTimeMillis();// record start time
		Object obj = pjp.proceed();// proceed to the method
		long end = System.currentTimeMillis();// record end time

		Object caller = pjp.getThis();

		String user = "[undetermined]";
		long commId = 0;
		if (caller instanceof BasePortalServiceServlet) {
			LoginResult result = ((BasePortalServiceServlet) caller).getAssociatedUser();
			if (result != null){
				user = result.getEmail();
				commId = result.getCommunityId();
			}
		}
                
		log.info("User: " + user + " Commid: "+commId+" :: Time taken to execute: class: "
				+ pjp.getSignature().getDeclaringType().getSimpleName() + ", method: " + pjp.getSignature().getName()
				+ " is: " + (end - start) + " msecs");

		return obj;
	}

	@Around("(execution(* com.gsihealth.dashboard.rest.service.MyPatientListRestService.*(..))"
			+ ") && "
			+ "!execution(* com.gsihealth.dashboard.server.service.*.getAssociatedUser(..))")
	public Object aroundRESTWithReturn(ProceedingJoinPoint pjp)throws Throwable {

		long start = System.currentTimeMillis();// record start time
		Object obj = pjp.proceed();// proceed to the method
		long end = System.currentTimeMillis();// record end time

		String user = "[undetermined]";
		String commId = "0";
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
		String userHeader = request.getHeader("GSI-email");
		if(userHeader!=null) user = userHeader;
        String commIdHeader = request.getHeader("GSI-commid");
        if(commIdHeader!=null) commId = commIdHeader;
		log.info("User: " + user + " Commid: "+commId+" :: Time taken to execute: class: "
				+ pjp.getSignature().getDeclaringType().getSimpleName() + ", method: " + pjp.getSignature().getName()
				+ " is: " + (end - start) + " msecs");


		return obj;
	}

}
