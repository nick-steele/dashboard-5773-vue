package com.gsihealth.dashboard.common;

import com.gsihealth.dashboard.entity.dto.UserAlertDTO;
import java.util.List;

/**
 * Returns a collection of alerts based on page size. Also includes the total count of all entries in database.
 * 
 * @author Chad Darby
 */
public class AlertSearchResults implements java.io.Serializable {
    
    private int totalCount;
    private List<UserAlertDTO> alerts;

    public AlertSearchResults() {
        
    }
    
    public AlertSearchResults(int totalCount, List<UserAlertDTO> alerts) {
        this.totalCount = totalCount;
        this.alerts = alerts;
    }

    public List<UserAlertDTO> getAlerts() {
        return alerts;
    }

    public void setAlerts(List<UserAlertDTO> alerts) {
        this.alerts = alerts;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
       
}
