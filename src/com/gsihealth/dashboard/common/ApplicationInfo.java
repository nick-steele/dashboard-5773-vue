package com.gsihealth.dashboard.common;

/**
 *
 * @author Chad Darby
 */
public class ApplicationInfo implements java.io.Serializable {

    private String iconFileName;
    private String screenName;
    private long applicationId;
    
    public ApplicationInfo() {        
    }

    public ApplicationInfo(String iconFileName, String theScreenName) {
        this(iconFileName, theScreenName, 0);
    }

    public ApplicationInfo(String iconFileName, String theScreenName, long applicationId) {
        this.iconFileName = iconFileName;
        this.screenName = theScreenName;
        this.applicationId = applicationId;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }
    

    public String getIconFileName() {
        return iconFileName;
    }

    public void setIconFileName(String iconFileName) {
        this.iconFileName = iconFileName;
    }

    public long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(long applicationId) {
        this.applicationId = applicationId;
    }
}
