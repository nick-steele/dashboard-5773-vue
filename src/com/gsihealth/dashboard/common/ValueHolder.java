package com.gsihealth.dashboard.common;

/**
 * Generic interface for retrieving a value
 * 
 * @author Chad Darby
 */
public interface ValueHolder {

    /**
     * Returns the value
     * 
     * @return
     */
    public String getValue();
}
