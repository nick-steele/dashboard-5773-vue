package com.gsihealth.dashboard.common;

/**
 *
 * @author Chad Darby
 */
public interface AccessLevelConstants {
    
    public static final long SUPER_USER_ACCESS_LEVEL_ID = 1;
    public static final long LOCAL_ADMIN_ACCESS_LEVEL_ID = 2;
    public static final long HH_TEAM_ACCESS_LEVEL_ID = 3;
    public static final long POWER_USER_ACCESS_LEVEL_ID = 4;
    
    public static final Character CHAR_YES= 'Y';
    public static final Character CHAR_NO= 'N';

}
