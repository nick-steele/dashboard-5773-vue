package com.gsihealth.dashboard.common;

import java.util.List;

/**
 * Returns search results based on page size. Also includes the total count of all entries in database.
 * 
 * @author Chad Darby
 */
public class SearchResults<T> implements java.io.Serializable {
    
    private int totalCount;
    private List<T> data;
    private boolean mdmMaxExceeded;
    
    public SearchResults() {
        
    }
    
    public SearchResults(int totalCount, List<T> theData) {
        this.totalCount = totalCount;
        this.data = theData;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> theData) {
        this.data = theData;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public boolean isMdmMaxExceeded() {
        return mdmMaxExceeded;
    }

    public void setMdmMaxExceeded(boolean mdmMaxExceeded) {
        this.mdmMaxExceeded = mdmMaxExceeded;
    }
       
    
}
