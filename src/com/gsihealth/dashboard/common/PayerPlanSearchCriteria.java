package com.gsihealth.dashboard.common;

/**
 *
 * @author Chad Darby
 */
public class PayerPlanSearchCriteria implements java.io.Serializable {
    
    private String payerPlanId;
    private String mmisid;
    
    public PayerPlanSearchCriteria() {
        
    }

    public PayerPlanSearchCriteria(String payerPlanId, String mmisid) {
        this.payerPlanId = payerPlanId;
        this.mmisid = mmisid;
    }

    public String getMmisid() {
        return mmisid;
    }

    public void setMmisid(String mmisid) {
        this.mmisid = mmisid;
    }

    public String getPayerPlanId() {
        return payerPlanId;
    }

    public void setPayerPlanId(String payerPlanId) {
        this.payerPlanId = payerPlanId;
    }
}
