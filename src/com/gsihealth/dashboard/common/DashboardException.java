package com.gsihealth.dashboard.common;

/**
 *
 * @author Chad Darby
 */
public class DashboardException extends Exception {

    public DashboardException(Throwable cause) {
        super(cause);
    }

    public DashboardException(String message, Throwable cause) {
        super(message, cause);
    }

    public DashboardException(String message) {
        super(message);
    }

    public DashboardException() {
    }

}
