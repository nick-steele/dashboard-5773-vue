package com.gsihealth.dashboard.common;

/**
 *
 * @author Chad Darby
 */
public interface AlertConstants {

    public static final String SEVERITY = "Severity";
    public static final String APPLICATION = "Application";
    public static final String DATE_RECEIVED = "Date Received";
    public static final String ALERT_DETAILS = "Alert Description";
    public static final String ALERT_TYPE_NAME = "Alert Type";
    public static final String PATIENT_FIRST_NAME = "Patient First Name";
    public static final String PATIENT_LAST_NAME = "Patient Last Name";
    public static final String PATIENT_ID = "Patient ID";
    public static final String USER_ORGANIZATION_NAME = "User Organization";

    public static final String NOT_APPLICABLE = "Not Applicable";

    // application names
    public static final String TREAT_APP = "TREAT";
    public static final String GSI_CARE_COORDINATION_APP = "GSI_CARE_COORDINATION";
    public static final String GSI_ADMIN_APP = "GSI_ADMIN_APP";
    public static final String GSI_CARETEAM_APP = "GSI_CARETEAM_APP";
    public static final String GSI_ENROLLMENT_APP = "GSI_ENROLLMENT_APP";
    public static final String GSI_PATIENT_CDR_APP = "GSI_PATIENT_CDR";

    public static final String GSI_DOCUMENT_REPOSITORY_APP = "GSI_DOCUMENT_REPOSITORY";
    public static final String GSI_ALERTING_APP = "GSI_ALERTING_APP";
    public static final String GSI_MESSAGING_APP = "GSI_MESSAGING_APP";
    
    public static final String GSIHEALTH_DASHBOARD_APP = "GSIHealth_Dashboard";
    
    // alert logical ids    
    public static final String ORGANIZATION_CREATED_ALERT_ID = "004";
    public static final String ORGANIZATION_DEACTIVATED_ALERT_ID = "002";
    public static final String USER_CREATED_ALERT_ID = "003";
    public static final String USER_INACTIVATED_ALERT_ID = "007";
    public static final String NEW_CARE_TEAM_CREATED_ALERT_ID = "008";
    public static final String CARE_TEAM_CHANGED_ALERT_ID = "009";
    public static final String PATIENT_ASSIGNED_TO_CARETEAM_ALERT_ID = "001";
    public static final String PATIENT_CARETEAM_REMOVED_ALERT_ID = "010";
    public static final String ENROLLMENT_STATUS_CHANGE_ALERT_ID = "011";
    public static final String CONSENT_STATUS_CHANGED_TO_YES_ALERT_ID = "012";
    public static final String CONSENT_STATUS_CHANGED_TO_NO_ALERT_ID = "013";
    public static final String PATIENT_CREATED_ALERT_ID = "006";
    public static final String PATIENT_DEMOGRAPHICS_CHANGED_ALERT_ID = "005";
    public static final String PATIENT_AGE_OF_MAJORITY = "300";
    
    // reason codes
    public static final String ALERT_NOTIFICATION = "ALERT_NOTIFICATION";
    
    //subcription manager
    

    
}
