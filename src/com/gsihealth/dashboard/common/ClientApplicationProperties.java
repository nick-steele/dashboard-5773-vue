package com.gsihealth.dashboard.common;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Chad Darby
 */
public class ClientApplicationProperties implements java.io.Serializable {

    private int sessionTimeoutInSeconds;
    private String helpUrl;

    private String messageUrl;
    private String consentLanguage;
    private String carebookUrl;
    private String populationManagerUrl;
    private String uhcEnrollmentReportUrl;
    private String taskListAppURL;
    private String patientListUtilityURL;

    private boolean headerLogoVisible;
    private String headerLogoFileName;
    private int headerLogoWidth;
    private int headerLogoHeight;
    private String headerTagLine;
    private String navLogoFilename;

    private boolean loginLogoVisible;
    private String loginLogoFileName;
    private int loginLogoHeight;
    private int loginLogoWidth;

    private boolean privacyPolicyVisible;
    private String privacyPolicyFooterFileName;

    private boolean enrollmentInsuranceEffectiveDateValidationEnabled;
    private boolean enrollmentInsuranceMedicaidIdValidationEnabled;

    private boolean patientSearchEnabled;
    
    //mirth
    private String mirthSearchVisible;

    private boolean intraHHComunityFieldVisible;
    
    private String healthHomeLabel;
    private boolean programNameRequired;
    
    private boolean showPatientOtherIdsCrud;
    
    //Patient Search -Suppress Intitial Screen Load
    private boolean candidatesEnrollmentDisableIntialLoad;
    private boolean enrolledPatientDisableIntialLoad;
    
    private boolean healthHomeRequired;

    private int maxPatientSearchResultsSize;
    
    private int rpcRetryMaxAttempts;
    private int rpcRetryIntervalInMillis;
    
    //Patient engagement 
    private String patientToCareTeamLabel;
    private boolean patientEngagementEnable;
    private String addPatientToCareTeamMsg;
    private String removePatientFromCareTeamMsg;
    private String attestLabel;
    private String federalMessage;
    private String articleLinkMessage;
    private String nysLinkMessage;
    private String federalLink;
    private String articleLink;
    private String nysLink;
    private boolean loginAttributesEnable;
    private boolean requireMedicaidID;
    private boolean requireMedicareID;
    private boolean manageConsent;
    private String hhRequiredLabel;
    private String programEndReasonLabel;
    private Long communityId;
    private boolean devmode;
    private boolean enableClientMFA;
    private String authyApiKey;
    private String countryCode;
    private boolean allowSubsequentLogin;
    
    //Children's Health Home
    private String minorWarning;
    private boolean disableMinorDirectMessaging;
    private boolean childrensHealthHome;
    private int minorAge;
    private String patientProgramButtonText;
    private String healthHomeProviderButtonText;
    private String activityTrackerWindowTitle;
    private String activityNameText;
    private String activityValueText;
    private String activityTrackerButtonText;
    private String reportGenerationDurationWindowTitle;
    private boolean patientSelfAssertedConsentAccess;
    private String patientSelfAssertWarningText;
    // For Redux
    private int userInterfaceType = 0;
    
    //JIRA 1024
    private boolean warnConsentStatusChangeFlag;
    private String warnConsentStatusChange;
    private String warnActivePrograms;
    private String warnConsentStatusChangeDialogTitle;
    private String warnActiveProgramsDialogTitle;
    private String warnActiveProgramsInactiveButtonText;
    
    
    //JIRA 1124
    private boolean obfuscateSearchResultsForConsent; 
            
    private String searchForAdditionalPatientsText;
    private String normalPatientsSearchText;
    private String searchForAdditionalPatientsInformationIconText;
    
    //JIRA 727
    private boolean multiTenantLoginPageFlag;

    private String supervisorFormTitle;
    
    //JIRA 2523
    private String daeCloseButtonText;
    private String daeDescLabelText;
    private String daeSelectButtonText;
    private String daeTitleLabelText;
    private String dseAddNowButtonText;
    private String dseDescLabelText;
    private String dseSelectButtonText;
    private String dseTitleLabelText;
    //Spira 7137
    private String dseUpdateDescLabelText;
    private String dseUpdateSaveButtonText;    
    private String dseUpdateCloseButtonText;    
    
    private boolean preventPatientInactivation;
    
    //Spira 7467
    private boolean mppStatusEffectiveDateEditable;
    private String mppStatusEffectiveDateMessage;

    //DASHBOARD-4126
    private String demographicSearchButton;
    private String demographicClearButton;
    private String demographicSelectButton;
    private String demographicModifyButton;
    private String demographicCheckDuplicateButton;
    private String demographicSaveButton;
    private String demographicCancelButton;
    private String demographicEnrollmentButton;
    private String demographicAddToPatientButton;

    //DASHBOARD-3752
    private LinkedHashMap<String, String> providerTypes;

    public ClientApplicationProperties() {

    }
    
	// For Redux optional showing interface...
    public int getUserInterfaceType() {
        return userInterfaceType;
    }
    
    public void setUserInterfaceType(int userInterfaceType) {
        this.userInterfaceType = userInterfaceType;
    }
    
    
    public boolean isDevmode() {
        return devmode;
    }

    public void setDevmode(boolean devmode) {
        this.devmode = devmode;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public int getSessionTimeoutInSeconds() {
        return sessionTimeoutInSeconds;
    }

    public void setSessionTimeoutInSeconds(int sessionTimeoutInSeconds) {
        this.sessionTimeoutInSeconds = sessionTimeoutInSeconds;
    }

    public String getHelpUrl() {
        return helpUrl;
    }

    public void setHelpUrl(String helpUrl) {
        this.helpUrl = helpUrl;
    }

    public String getConsentLanguage() {
        return consentLanguage;
    }

    public void setConsentLanguage(String consentLanguage) {
        this.consentLanguage = consentLanguage;
    }

    public String getMessageUrl() {
        return messageUrl;
    }

    public void setMessageUrl(String messageUrl) {
        this.messageUrl = messageUrl;
    }

    public String getCarebookUrl() {
        return carebookUrl;
    }

    public void setCarebookUrl(String carebookUrl) {
        this.carebookUrl = carebookUrl;
    }

    public boolean isLoginLogoVisible() {
        return loginLogoVisible;
    }

    public void setLoginLogoVisible(boolean loginLogoVisible) {
        this.loginLogoVisible = loginLogoVisible;
    }

    public String getLoginLogoFileName() {
        return loginLogoFileName;
    }

    public void setLoginLogoFileName(String loginLogoFileName) {
        this.loginLogoFileName = loginLogoFileName;
    }

    public int getLoginLogoHeight() {
        return loginLogoHeight;
    }

    public void setLoginLogoHeight(int loginLogoHeight) {
        this.loginLogoHeight = loginLogoHeight;
    }

    public int getLoginLogoWidth() {
        return loginLogoWidth;
    }

    public void setLoginLogoWidth(int loginLogoWidth) {
        this.loginLogoWidth = loginLogoWidth;
    }

    public boolean isPrivacyPolicyVisible() {
        return privacyPolicyVisible;
    }

    public void setPrivacyPolicyVisible(boolean privacyPolicyVisible) {
        this.privacyPolicyVisible = privacyPolicyVisible;
    }

    public String getPrivacyPolicyFooterFileName() {
        return privacyPolicyFooterFileName;
    }

    public void setPrivacyPolicyFooterFileName(String privacyPolicyFooterFileName) {
        this.privacyPolicyFooterFileName = privacyPolicyFooterFileName;
    }

    public boolean isHeaderLogoVisible() {
        return headerLogoVisible;
    }

    public void setHeaderLogoVisible(boolean headerLogoVisible) {
        this.headerLogoVisible = headerLogoVisible;
    }

    public String getHeaderLogoFileName() {
        return headerLogoFileName;
    }

    public void setHeaderLogoFileName(String headerLogoFileName) {
        this.headerLogoFileName = headerLogoFileName;
    }

    public int getHeaderLogoWidth() {
        return headerLogoWidth;
    }

    public void setHeaderLogoWidth(int headerLogoWidth) {
        this.headerLogoWidth = headerLogoWidth;
    }

    public int getHeaderLogoHeight() {
        return headerLogoHeight;
    }

    public void setHeaderLogoHeight(int headerLogoHeight) {
        this.headerLogoHeight = headerLogoHeight;
    }

    public String getHeaderTagLine() {
        return headerTagLine;
    }

    public void setHeaderTagLine(String headerTagLine) {
        this.headerTagLine = headerTagLine;
    }

    public String getNavLogoFilename() {
        return navLogoFilename;
    }

    public void setNavLogoFilename(String navLogoFilename) {
        this.navLogoFilename = navLogoFilename;
    }


    public boolean isEnrollmentInsuranceEffectiveDateValidationEnabled() {
        return enrollmentInsuranceEffectiveDateValidationEnabled;
    }

    public void setEnrollmentInsuranceEffectiveDateValidationEnabled(boolean enrollmentInsuranceEffectiveDateValidationEnabled) {
        this.enrollmentInsuranceEffectiveDateValidationEnabled = enrollmentInsuranceEffectiveDateValidationEnabled;
    }

    public boolean isEnrollmentInsuranceMedicaidIdValidationEnabled() {
        return enrollmentInsuranceMedicaidIdValidationEnabled;
    }

    public void setEnrollmentInsuranceMedicaidIdValidationEnabled(boolean enrollmentInsuranceMedicaidIdValidationEnabled) {
        this.enrollmentInsuranceMedicaidIdValidationEnabled = enrollmentInsuranceMedicaidIdValidationEnabled;
    }

    public boolean isPatientSearchEnabled() {
        return patientSearchEnabled;
    }

    public void setPatientSearchEnabled(boolean patientSearchEnabled) {
        this.patientSearchEnabled = patientSearchEnabled;
    }
    //mirth
    public String getMirthSearchVisible(){
        return mirthSearchVisible;
    }
    public void setMirthSearchVisible(String mirthSearchVisible){
        this.mirthSearchVisible = mirthSearchVisible;
    }
    
    public String getPopulationManagerUrl() {
        return populationManagerUrl;
    }

    public void setPopulationManagerUrl(String populationManagerUrl) {
        this.populationManagerUrl = populationManagerUrl;
    }

    public String getUhcEnrollmentReportUrl() {
        return uhcEnrollmentReportUrl;
    }

    public void setUhcEnrollmentReportUrl(String uhcEnrollmentReportUrl) {
        this.uhcEnrollmentReportUrl = uhcEnrollmentReportUrl;
    }

    public boolean isIntraHHComunityFieldVisible() {
        return intraHHComunityFieldVisible;
    }

    public void setIntraHHComunityFieldVisible(boolean intraHHComunityFieldVisible) {
        this.intraHHComunityFieldVisible = intraHHComunityFieldVisible;
    }
    
      public boolean getShowPatientOtherIdsCrud() {
        return showPatientOtherIdsCrud;
    }
    
        public void setShowPatientOtherIdsCrud(boolean showPatientOtherIdsCrud) {
        this.showPatientOtherIdsCrud = showPatientOtherIdsCrud;
    }

    /**
     * @return the healthHomeLabel
     */
    public String getHealthHomeLabel() {
        return healthHomeLabel;
    }

    /**
     * @param healthHomeLabel the healthHomeLabel to set
     */
    public void setHealthHomeLabel(String healthHomeLabel) {
        this.healthHomeLabel = healthHomeLabel;
    }

    /**
     * @return the programNameRequired
     */
    public boolean isProgramNameRequired() {
        return programNameRequired;
    }

    /**
     * @param programNameRequired the programNameRequired to set
     */
    public void setProgramNameRequired(boolean programNameRequired) {
        this.programNameRequired = programNameRequired;
    }

    public boolean isCandidatesEnrollmentDisableIntialLoad() {
        return candidatesEnrollmentDisableIntialLoad;
    }

    public void setCandidatesEnrollmentDisableIntialLoad(boolean candidatesEnrollmentDisableIntialLoad) {
        this.candidatesEnrollmentDisableIntialLoad = candidatesEnrollmentDisableIntialLoad;
    }

    public boolean isEnrolledPatientDisableIntialLoad() {
        return enrolledPatientDisableIntialLoad;
    }

    public void setEnrolledPatientDisableIntialLoad(boolean enrolledPatientDisableIntialLoad) {
        this.enrolledPatientDisableIntialLoad = enrolledPatientDisableIntialLoad;
    }

    public boolean isHealthHomeRequired() {
        return healthHomeRequired;
    }

    public void setHealthHomeRequired(boolean healthHomeRequired) {
        this.healthHomeRequired = healthHomeRequired;
    }

    public int getMaxPatientSearchResultsSize() {
        return maxPatientSearchResultsSize;
    }

    public void setMaxPatientSearchResultsSize(int maxPatientSearchResultsSize) {
        this.maxPatientSearchResultsSize = maxPatientSearchResultsSize;
    }

    public int getRpcRetryMaxAttempts() {
        return rpcRetryMaxAttempts;
    }

    public void setRpcRetryMaxAttempts(int rpcRetryMaxAttempts) {
        this.rpcRetryMaxAttempts = rpcRetryMaxAttempts;
    }

    public int getRpcRetryIntervalInMillis() {
        return rpcRetryIntervalInMillis;
    }

    public void setRpcRetryIntervalInMillis(int rpcRetryIntervalInMillis) {
        this.rpcRetryIntervalInMillis = rpcRetryIntervalInMillis;
    }   
    
     public String getPatientToCareTeamLabel() {
        return patientToCareTeamLabel;
    }

    public void setPatientToCareTeamLabel(String patientToCareTeamLabel) {
        this.patientToCareTeamLabel = patientToCareTeamLabel;
    }

    public boolean isPatientEngagementEnable() {
        return patientEngagementEnable;
    }

    public void setPatientEngagementEnable(boolean patientEngagementEnable) {
        this.patientEngagementEnable = patientEngagementEnable;
    }

    public String getAddPatientToCareTeamMsg() {
        return addPatientToCareTeamMsg;
    }

    public void setAddPatientToCareTeamMsg(String removePatientFromCareTeamMsg) {
        this.addPatientToCareTeamMsg = removePatientFromCareTeamMsg;
    }

    public String getRemovePatientFromCareTeamMsg() {
        return removePatientFromCareTeamMsg;
    }

    public void setRemovePatientFromCareTeamMsg(String removePatientFromCareTeamMsg) {
        this.removePatientFromCareTeamMsg = removePatientFromCareTeamMsg;
    }

    public String getAttestLabel() {
        return attestLabel;
    }

    public void setAttestLabel(String attestLabel) {
        this.attestLabel = attestLabel;
    }

    public String getFederalMessage() {
        return federalMessage;
    }

    public void setFederalMessage(String federalMessage) {
        this.federalMessage = federalMessage;
    }

    public String getArticleLinkMessage() {
        return articleLinkMessage;
    }

    public void setArticleLinkMessage(String articleLinkMessage) {
        this.articleLinkMessage = articleLinkMessage;
    }

    public String getNysLinkMessage() {
        return nysLinkMessage;
    }

    public void setNysLinkMessage(String nysLinkMessage) {
        this.nysLinkMessage = nysLinkMessage;
    }

    public String getFederalLink() {
        return federalLink;
    }

    public void setFederalLink(String federalLink) {
        this.federalLink = federalLink;
    }

    public String getArticleLink() {
        return articleLink;
    }

    public void setArticleLink(String articleLink) {
        this.articleLink = articleLink;
    }

    public String getNysLink() {
        return nysLink;
    }

    public void setNysLink(String nysLink) {
        this.nysLink = nysLink;
    }
 public boolean isLoginAttributesEnable() {
        return loginAttributesEnable;
    }

    public void setLoginAttributesEnable(boolean loginAttributesEnable) {
        this.loginAttributesEnable = loginAttributesEnable;
    }

    public boolean isManageConsent() {
        return manageConsent;
    }

    public void setManageConsent(boolean manageConsent) {
        this.manageConsent = manageConsent;
    }

    /**
     * @return the hhRequiredLabel
     */
    public String getHhRequiredLabel() {
        return hhRequiredLabel;
    }

    /**
     * @param hhRequiredLabel the hhRequiredLabel to set
     */
    public void setHhRequiredLabel(String hhRequiredLabel) {
        this.hhRequiredLabel = hhRequiredLabel;
    }

    /**
     * @return the programEndReasonLabel
     */
    public String getProgramEndReasonLabel() {
        return programEndReasonLabel;
    }

    /**
     * @param programEndReasonLabel the programEndReasonLabel to set
     */
    public void setProgramEndReasonLabel(String programEndReasonLabel) {
        this.programEndReasonLabel = programEndReasonLabel;
    }

    public String getTaskListAppURL() {
        return taskListAppURL;
    }

    public void setTaskListAppURL(String taskListAppURL) {
        this.taskListAppURL = taskListAppURL;
    }

    public String getPatientListUtilityURL() {
        return patientListUtilityURL;
    }

    public void setPatientListUtilityURL(String patientListUtilityURL) {
        this.patientListUtilityURL = patientListUtilityURL;
    }

    /**
     * @return the RequireMedicaidID
     */
    public boolean isRequireMedicaidID() {
        return requireMedicaidID;
    }

    /**
     * @param RequireMedicaidID the RequireMedicaidID to set
     */
    public void setRequireMedicaidID(boolean requireMedicaidID) {
        this.requireMedicaidID = requireMedicaidID;
    }

    /**
     * @return the RequireMedicareID
     */
    public boolean isRequireMedicareID() {
        return requireMedicareID;
    }

    /**
     * @param RequireMedicareID the RequireMedicareID to set
     */
    public void setRequireMedicareID(boolean requireMedicareID) {
        this.requireMedicareID = requireMedicareID;
    }

    /**
     * @return the enableClientMFA
     */
    public boolean isEnableClientMFA() {
        return enableClientMFA;
    }

    /**
     * @param enableClientMFA the enableClientMFA to set
     */
    public void setEnableClientMFA(boolean enableClientMFA) {
        this.enableClientMFA = enableClientMFA;
    }

    
    /**
     * @return the authyApiKey
     */
    public String getAuthyApiKey() {
        return authyApiKey;
    }

    /**
     * @param authyApiKey the authyApiKey to set
     */
    public void setAuthyApiKey(String authyApiKey) {
        this.authyApiKey = authyApiKey;
    }

    /**
     * @return the countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param countryCode the countryCode to set
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * @return the allowSubsequentLogin
     */
    public boolean isAllowSubsequentLogin() {
        return allowSubsequentLogin;
    }

    /**
     * @param allowSubsequentLogin the allowSubsequentLogin to set
     */
    public void setAllowSubsequentLogin(boolean allowSubsequentLogin) {
        this.allowSubsequentLogin = allowSubsequentLogin;
    }

    public String getMinorWarning() {
        return minorWarning;
    }

    public void setMinorWarning(String minorWarning) {
        this.minorWarning = minorWarning;
    }

    public boolean isDisableMinorDirectMessaging() {
        return disableMinorDirectMessaging;
    }

    public void setDisableMinorDirectMessaging(boolean disableMinorDirectMessaging) {
        this.disableMinorDirectMessaging = disableMinorDirectMessaging;
    }

    public boolean isChildrensHealthHome() {
        return childrensHealthHome;
    }

    public void setChildrensHealthHome(boolean childrensHealthHome) {
        this.childrensHealthHome = childrensHealthHome;
    }

    public int getMinorAge() {
        return minorAge;
    }

    public void setMinorAge(int minorAge) {
        this.minorAge = minorAge;
    }

    /**
     * @return the patientProgramButtonText
     */
    public String getPatientProgramButtonText() {
        return patientProgramButtonText;
    }

    /**
     * @param patientProgramButtonText the patientProgramButtonText to set
     */
    public void setPatientProgramButtonText(String patientProgramButtonText) {
        this.patientProgramButtonText = patientProgramButtonText;
    }

    /**
     * @return the healthHomeProviderButtonText
     */
    public String getHealthHomeProviderButtonText() {
        return healthHomeProviderButtonText;
    }

    /**
     * @param healthHomeProviderButtonText the healthHomeProviderButtonText to set
     */
    public void setHealthHomeProviderButtonText(String healthHomeProviderButtonText) {
        this.healthHomeProviderButtonText = healthHomeProviderButtonText;
    }

    /**
     * @return the activityTrackerWindowTitle
     */
    public String getActivityTrackerWindowTitle() {
        return activityTrackerWindowTitle;
    }

    /**
     * @param activityTrackerWindowTitle the activityTrackerWindowTitle to set
     */
    public void setActivityTrackerWindowTitle(String activityTrackerWindowTitle) {
        this.activityTrackerWindowTitle = activityTrackerWindowTitle;
    }

    /**
     * @return the activityNameText
     */
    public String getActivityNameText() {
        return activityNameText;
    }

    /**
     * @param activityNameText the activityNameText to set
     */
    public void setActivityNameText(String activityNameText) {
        this.activityNameText = activityNameText;
    }

    /**
     * @return the activityValueText
     */
    public String getActivityValueText() {
        return activityValueText;
    }

    /**
     * @param activityValueText the activityValueText to set
     */
    public void setActivityValueText(String activityValueText) {
        this.activityValueText = activityValueText;
    }

    /**
     * @return the activityTrackerButtonText
     */
    public String getActivityTrackerButtonText() {
        return activityTrackerButtonText;
    }

    /**
     * @param activityTrackerButtonText the activityTrackerButtonText to set
     */
    public void setActivityTrackerButtonText(String activityTrackerButtonText) {
        this.activityTrackerButtonText = activityTrackerButtonText;
    }
    
    
    public String getReportGenerationDurationWindowTitle() {
        return reportGenerationDurationWindowTitle;
    }

    public void setReportGenerationDurationWindowTitle(String reportGenerationDurationWindowTitle) {
        this.reportGenerationDurationWindowTitle = reportGenerationDurationWindowTitle;
    }
    

    public boolean isPatientSelfAssertedConsentAccess() {
        return patientSelfAssertedConsentAccess;
    }

    public void setPatientSelfAssertedConsentAccess(boolean patientSelfAssertedConsentAccess) {
        this.patientSelfAssertedConsentAccess = patientSelfAssertedConsentAccess;
    }

    public String getPatientSelfAssertWarningText() {
        return patientSelfAssertWarningText;
    }

    public void setPatientSelfAssertWarningText(String patientSelfAssertWarningText) {
        this.patientSelfAssertWarningText = patientSelfAssertWarningText;
    }

    /**
     * @return the warnConsentStatusChangeFlag
     */
    public boolean isWarnConsentStatusChangeFlag() {
        return warnConsentStatusChangeFlag;
    }

    /**
     * @param warnConsentStatusChangeFlag the warnConsentStatusChangeFlag to set
     */
    public void setWarnConsentStatusChangeFlag(boolean warnConsentStatusChangeFlag) {
        this.warnConsentStatusChangeFlag = warnConsentStatusChangeFlag;
    }

    /**
     * @return the warnConsentStatusChange
     */
    public String getWarnConsentStatusChange() {
        return warnConsentStatusChange;
    }

    /**
     * @param warnConsentStatusChange the warnConsentStatusChange to set
     */
    public void setWarnConsentStatusChange(String warnConsentStatusChange) {
        this.warnConsentStatusChange = warnConsentStatusChange;
    }

    /**
     * @return the warnActivePrograms
     */
    public String getWarnActivePrograms() {
        return warnActivePrograms;
    }

    /**
     * @param warnActivePrograms the warnActivePrograms to set
     */
    public void setWarnActivePrograms(String warnActivePrograms) {
        this.warnActivePrograms = warnActivePrograms;
    }
    
    public String getWarnConsentStatusChangeDialogTitle() {
        return warnConsentStatusChangeDialogTitle;
    }

    public void setWarnConsentStatusChangeDialogTitle(String warnConsentStatusChangeDialogTitle) {
        this.warnConsentStatusChangeDialogTitle = warnConsentStatusChangeDialogTitle;
    }

    public String getWarnActiveProgramsDialogTitle() {
        return warnActiveProgramsDialogTitle;
    }

    public void setWarnActiveProgramsDialogTitle(String warnActiveProgramsDialogTitle) {
        this.warnActiveProgramsDialogTitle = warnActiveProgramsDialogTitle;
    }

    public String getWarnActiveProgramsInactiveButtonText() {
        return warnActiveProgramsInactiveButtonText;
    }

    public void setWarnActiveProgramsInactiveButtonText(String warnActiveProgramsInactiveButtonText) {
        this.warnActiveProgramsInactiveButtonText = warnActiveProgramsInactiveButtonText;
    }
    
    public boolean isObfuscateSearchResultsForConsent() {
        return obfuscateSearchResultsForConsent;
    }

    public void setObfuscateSearchResultsForConsent(boolean obfuscateSearchResultsForConsent) {
        this.obfuscateSearchResultsForConsent = obfuscateSearchResultsForConsent;
    }

    public String getSearchForAdditionalPatientsText() {
        return searchForAdditionalPatientsText;
    }

    public void setSearchForAdditionalPatientsText(String searchForAdditionalPatientsText) {
        this.searchForAdditionalPatientsText = searchForAdditionalPatientsText;
    }

    /**
     * @return the normalPatientsSearchText
     */
    public String getNormalPatientsSearchText() {
        return normalPatientsSearchText;
    }

    /**
     * @param normalPatientsSearchText the normalPatientsSearchText to set
     */
    public void setNormalPatientsSearchText(String normalPatientsSearchText) {
        this.normalPatientsSearchText = normalPatientsSearchText;
    }

    public String getSearchForAdditionalPatientsInformationIconText() {
        return searchForAdditionalPatientsInformationIconText;
    }

    public void setSearchForAdditionalPatientsInformationIconText(String searchForAdditionalPatientsInformationIconText) {
        this.searchForAdditionalPatientsInformationIconText = searchForAdditionalPatientsInformationIconText;
    }

    /**
     * @return the multiTenantLoginPageFlag
     */
    public boolean isMultiTenantLoginPageFlag() {
        return multiTenantLoginPageFlag;
    }

    /**
     * @param multiTenantLoginPageFlag the multiTenantLoginPageFlag to set
     */
    public void setMultiTenantLoginPageFlag(boolean multiTenantLoginPageFlag) {
        this.multiTenantLoginPageFlag = multiTenantLoginPageFlag;
    }

    public String getSupervisorFormTitle() {
        return supervisorFormTitle;
    }

    public void setSupervisorFormTitle(String supervisorFormTitle) {
        this.supervisorFormTitle = supervisorFormTitle;
    }

    //JIRA 2523
    /**
     * @return the daeCloseButtonText
     */
    public String getDaeCloseButtonText() {
        return daeCloseButtonText;
    }

    /**
     * @param daeCloseButtonText the daeCloseButtonText to set
     */
    public void setDaeCloseButtonText(String daeCloseButtonText) {
        this.daeCloseButtonText = daeCloseButtonText;
    }

    /**
     * @return the daeDescLabelText
     */
    public String getDaeDescLabelText() {
        return daeDescLabelText;
    }

    /**
     * @param daeDescLabelText the daeDescLabelText to set
     */
    public void setDaeDescLabelText(String daeDescLabelText) {
        this.daeDescLabelText = daeDescLabelText;
    }

    /**
     * @return the daeSelectButtonText
     */
    public String getDaeSelectButtonText() {
        return daeSelectButtonText;
    }

    /**
     * @param daeSelectButtonText the daeSelectButtonText to set
     */
    public void setDaeSelectButtonText(String daeSelectButtonText) {
        this.daeSelectButtonText = daeSelectButtonText;
    }

    /**
     * @return the daeTitleLabelText
     */
    public String getDaeTitleLabelText() {
        return daeTitleLabelText;
    }

    /**
     * @param daeTitleLabelText the daeTitleLabelText to set
     */
    public void setDaeTitleLabelText(String daeTitleLabelText) {
        this.daeTitleLabelText = daeTitleLabelText;
    }

    /**
     * @return the dseAddNowButtonText
     */
    public String getDseAddNowButtonText() {
        return dseAddNowButtonText;
    }

    /**
     * @param dseAddNowButtonText the dseAddNowButtonText to set
     */
    public void setDseAddNowButtonText(String dseAddNowButtonText) {
        this.dseAddNowButtonText = dseAddNowButtonText;
    }

    /**
     * @return the dseDescLabelText
     */
    public String getDseDescLabelText() {
        return dseDescLabelText;
    }

    /**
     * @param dseDescLabelText the dseDescLabelText to set
     */
    public void setDseDescLabelText(String dseDescLabelText) {
        this.dseDescLabelText = dseDescLabelText;
    }

    /**
     * @return the dseSelectButtonText
     */
    public String getDseSelectButtonText() {
        return dseSelectButtonText;
    }

    /**
     * @param dseSelectButtonText the dseSelectButtonText to set
     */
    public void setDseSelectButtonText(String dseSelectButtonText) {
        this.dseSelectButtonText = dseSelectButtonText;
    }

    /**
     * @return the dseTitleLabelText
     */
    public String getDseTitleLabelText() {
        return dseTitleLabelText;
    }

    /**
     * @param dseTitleLabelText the dseTitleLabelText to set
     */
    public void setDseTitleLabelText(String dseTitleLabelText) {
        this.dseTitleLabelText = dseTitleLabelText;
    }

    /**
     * @return the preventPatientInactivation
     */
    public boolean isPreventPatientInactivation() {
        return preventPatientInactivation;
    }

    /**
     * @param preventPatientInactivation the preventPatientInactivation to set
     */
    public void setPreventPatientInactivation(boolean preventPatientInactivation) {
        this.preventPatientInactivation = preventPatientInactivation;
    }

    public String getDseUpdateDescLabelText() {
        return dseUpdateDescLabelText;
    }

    public void setDseUpdateDescLabelText(String dseUpdateDescLabelText) {
        this.dseUpdateDescLabelText = dseUpdateDescLabelText;
    }

    public String getDseUpdateSaveButtonText() {
        return dseUpdateSaveButtonText;
    }

    public void setDseUpdateSaveButtonText(String dseUpdateSaveButtonText) {
        this.dseUpdateSaveButtonText = dseUpdateSaveButtonText;
    }

    public String getDseUpdateCloseButtonText() {
        return dseUpdateCloseButtonText;
    }

    public void setDseUpdateCloseButtonText(String dseUpdateCloseButtonText) {
        this.dseUpdateCloseButtonText = dseUpdateCloseButtonText;
    }

    //Spira 7467
    public boolean getMppStatusEffectiveDateEditable() {
        return this.mppStatusEffectiveDateEditable;
    }
    
    public void setMppStatusEffectiveDateEditable(boolean mppStatusEffectiveDateEditable) {
        this.mppStatusEffectiveDateEditable = mppStatusEffectiveDateEditable;
    }
            
    public String getMppStatusEffectiveDateMessage() {
        return mppStatusEffectiveDateMessage;
    }

    public void setMppStatusEffectiveDateMessage(String mppStatusEffectiveDateMessage) {
        this.mppStatusEffectiveDateMessage = mppStatusEffectiveDateMessage;
    }

    public String getDemographicSearchButton() {
        return demographicSearchButton;
    }

    public void setDemographicSearchButton(String demographicSearchButton) {
        this.demographicSearchButton = demographicSearchButton;
    }

    public String getDemographicClearButton() {
        return demographicClearButton;
    }

    public void setDemographicClearButton(String demographicClearButton) {
        this.demographicClearButton = demographicClearButton;
    }

    public String getDemographicSelectButton() {
        return demographicSelectButton;
    }

    public void setDemographicSelectButton(String demographicSelectButton) {
        this.demographicSelectButton = demographicSelectButton;
    }

    public String getDemographicModifyButton() {
        return demographicModifyButton;
    }

    public void setDemographicModifyButton(String demographicModifyButton) {
        this.demographicModifyButton = demographicModifyButton;
    }

    public String getDemographicCheckDuplicateButton() {
        return demographicCheckDuplicateButton;
    }

    public void setDemographicCheckDuplicateButton(String demographicCheckDuplicateButton) {
        this.demographicCheckDuplicateButton = demographicCheckDuplicateButton;
    }

    public String getDemographicSaveButton() {
        return demographicSaveButton;
    }

    public void setDemographicSaveButton(String demographicSaveButton) {
        this.demographicSaveButton = demographicSaveButton;
    }

    public String getDemographicCancelButton() {
        return demographicCancelButton;
    }

    public void setDemographicCancelButton(String demographicCancelButton) {
        this.demographicCancelButton = demographicCancelButton;
    }

	public String getDemographicEnrollmentButton() {
		return demographicEnrollmentButton;
	}

	public void setDemographicEnrollmentButton(String demographicEnrollmentButton) {
		this.demographicEnrollmentButton = demographicEnrollmentButton;
	}

	public String getDemographicAddToPatientButton() {
		return demographicAddToPatientButton;
	}

	public void setDemographicAddToPatientButton(
			String demographicAddToPatientButton) {
		this.demographicAddToPatientButton = demographicAddToPatientButton;
	}

    public LinkedHashMap<String, String> getProviderTypes() {
        return providerTypes;
    }

    public void setProviderTypes(LinkedHashMap<String, String> providerTypes) {
        this.providerTypes = providerTypes;
    }
}
