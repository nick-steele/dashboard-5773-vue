package com.gsihealth.dashboard.common;

import com.gsihealth.patientmdminterface.entity.DuplicateCheckProcessResult;

/**
 *
 * @author Chad Darby
 */
public class PortalException extends Exception implements java.io.Serializable {

    private DuplicateCheckProcessResult duplicateCheckProcessResult;

    public PortalException() {
    }

    public PortalException(String message) {
        super(message);
    }

    public PortalException(String message, DuplicateCheckProcessResult duplicateCheckProcessResult) {
        super(message);
        this.duplicateCheckProcessResult = duplicateCheckProcessResult;
    }

    public DuplicateCheckProcessResult getDuplicateCheckProcessResult() {
        return duplicateCheckProcessResult;
    }

}
