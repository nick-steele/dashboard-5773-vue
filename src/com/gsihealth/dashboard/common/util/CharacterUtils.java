package com.gsihealth.dashboard.common.util;

public final class CharacterUtils {

    private final static String[] META_CHARACTERS = {"\\","^","$","{","}","[","]","(",")",".","*","+","?","|","<",">","-","&"};

    private CharacterUtils() {
    }

    public static String escapeMetaCharacters(final String input){
        String result = input;

        for (int i = 0 ; i < META_CHARACTERS.length ; i++){
            result = result.replace(META_CHARACTERS[i], "\\" + META_CHARACTERS[i]);
        }
        return result;
    }
}
