package com.gsihealth.dashboard.common.util;

/**
 *
 * @author Chad Darby
 */
public interface AppConfigConstants {

    public static final long ENROLLMENT_APP_ID = 6;
    public static final long CARETEAM_APP_ID = 5;
    public static final long ALERTS_APP_ID = 9;
    public static final long CAREPLAN_APP_ID = 2;
    public static final long ADMIN_APP_ID = 4;
    public static final long REPORTS_APP_ID = 12;
    public static final long MESSAGES_APP_ID = 10;
    public static final long PATIENT_ENGAGEMENT_APP_ID = 11;
    public static final long CAREBOOK_APP_ID = 14;
    public static final long POPULATION_MANAGER_APP_ID = 15;
    public static final long PATIENT_SUMMARY_APP_ID = 16;
    public static final long UHC_ENROLLMENT_REPORT_APP_ID = 18;
    public static final long PATIENT_LIST_UTILITY_APP_ID = 40;
    public static final long TASK_LIST_APP_ID = 48;
}
