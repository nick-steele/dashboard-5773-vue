package com.gsihealth.dashboard.common.util;

import com.gsihealth.dashboard.client.util.StringUtils;

/**
 *
 * @author Chad Darby
 */
public class SsnUtils {
    
    public static final int SSN_LENGTH = 9;
    private static final int LAST_FOUR_START_INDEX = 5;
    
    /**
     * Mask the SSN to only show last 4 digits
     * 
     * Input: 123456789
     * Output: xxx-xx-6789
     * 
     * also supports input that includes dashes
     * 
     * @param ssn
     * @return 
     */
    public static String getMaskedSsn(String ssn) {
        String result = "";
        
        if (StringUtils.isBlank(ssn)) {
            throw new IllegalArgumentException("SSN is empty/null.");
        }

        int length = ssn.length();
        
        if (length != SSN_LENGTH) {
            throw new IllegalArgumentException("Invalid SSN length. 9 digits required");
        }
        
        String lastFour = ssn.substring(LAST_FOUR_START_INDEX);
        
        result = "###-##-" + lastFour;
        
        return result;
    }


}
