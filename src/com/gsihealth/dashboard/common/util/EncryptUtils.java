package com.gsihealth.dashboard.common.util;

/**
 * Algorithms to encode the data. ONLY to be used for the URL line on reports
 *
 * @author Chad Darby
 */
public class EncryptUtils {

    public static String encode(String data) throws Exception {

        // for null string...noop
        if (data == null) {
            return data;
        }

        char[] chars = data.toCharArray();

        StringBuilder hex = new StringBuilder();
        for (int i = 0; i < chars.length; i++) {
            hex.append(Integer.toHexString((int) chars[i]));
        }

        return hex.toString();
    }

    public static String decode(String data) {

        // for null string...noop
        if (data == null) {
            return data;
        }

        StringBuilder out = new StringBuilder();

        // decode a hex string to ascii
        for (int i = 0; i < data.length(); i += 2) {
            String temp = data.substring(i, i + 2);
            out.append((char) Integer.parseInt(temp, 16));
        }

        return out.toString();
    }

    /*
     public static String process(String data) {

     StringBuffer buffer = new StringBuffer();
        
     // for null string...noop
     if (data == null) {
     return data;
     }
        
     for (int i = 0; i < data.length(); i++) {
     char c = data.charAt(i);
     if       (c >= 'a' && c <= 'm') c += 13;
     else if  (c >= 'A' && c <= 'M') c += 13;
     else if  (c >= 'n' && c <= 'z') c -= 13;
     else if  (c >= 'N' && c <= 'Z') c -= 13;
            
     buffer.append(c);
     }
        
     return buffer.toString();
     }
     */
}
