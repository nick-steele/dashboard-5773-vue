package com.gsihealth.dashboard.common;

/**
 *
 * @author Chad Darby
 */
public interface Constants {

    public static final String YES = "Yes";
    public static final String NO = "No";
    public static final String UNKNOWN = "Unknown";
    public static final char YES_CHAR = 'Y';
    public static final char NO_CHAR = 'N';
    public static final int MAX_LOGIN_FAILURES = 5;
    public static final String TREAT_PATIENT_ALERT_TAG = "@TREAT@";
    public static final String SOUTHWEST_BROOKLYN_ORG_ID_KEY = "SOUTHWEST_BROOKLYN_ORG_ID_KEY";
     //check Medicaid Medicare id 
   
    public static final String MSG = "This patient has the same ";
    public static final String ONEPATIENTMSG = " as one other patient. ";
     public static final String OTHERPATIENTMSG = " as other patients. ";
    public static final String ASSIGNMSG = "That patient is assigned to ";
    public static final String LASTMSG = " with an Enrollment Status of ";
    public static final String ADMINMSG = "Please contact your administrator for help in resolving this conflict.";
    public static final int TYPE_ONE = 1;
    public static final int TYPE_TWO = 2;
    public static final String MEDICAIDID ="Medicaid ID";
    public static final String MEDICAREID ="Medicare ID";
    public static final String SSN ="SSN";
    public static final String PERMIT="PERMIT";
    public static final String DENY="DENY";
    public static final String STATUS=" Status.";
    public static final String SSN_MSG="This patient has the same SSN as one other patient. That patient is assigned to ";
    public static final String DUPLICATEE_EMAILMAIL_MSG="This patient has the same email address as another patient. That patient is assigned to ";
    public static final String IN_PROVIDER="IN_PROVIDERS";
    public static final String OUT_PROVIDER="OUT_PROVIDERS";
    public static final String DUPLICATE_PROGRAM_MSG="This patient cannot be enrolled to same programs that are not finished/ended. Already enrolled to ";
    
    public static final String PATIENT_ID_HTTP_PARAM_NAME = "patientId";
    public static final String DESTINATION_HTTP_PARAM_NAME = "destination";
    public static final String COMMUNITY_SPECIFIC_DATA_KEY = "COMMUNITY_SPECIFIC_DATA_KEY";
    public static final String TASK_ID_HTTP_PARAM_NAME = "taskID";

    public static final String BLANK_CARETEAM_ID = "-1";
    String CMOH_TYPE_PRIMARY = "PRIMARY";
}
