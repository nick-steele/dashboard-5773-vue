package com.gsihealth.dashboard.common;

import com.gsihealth.dashboard.entity.dto.ProgramNameDTO;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class ReferenceData implements java.io.Serializable {

    private String[] postalStateCodes;
    private LinkedHashMap<String, String> genderCodes;
    private LinkedHashMap<String, String> raceCodes;
    private LinkedHashMap<String, String> ethnicCodes;
    private LinkedHashMap<String, String> languageCodes;
    private String[] programNames;
    private String[] enrollmentStatuses;
    private String[] programLevels;
    private String[] userCredentials;
    private String[] userPrefixes;    
//    private long southwestBrooklynOrgId;

    private Map<String, ApplicationInfo> applicationInfoMap;
    private String[] enrollmentReasonsForInactivation;
    private String[] allDisenrollmentReasons;
    private String[] disenrollmentReasonsForRefusal;
    private String[] disenrollmentReasonsForInactivation;
    private LinkedHashMap<Long, String> patientRelationship;
    private String[] duplicateCareteamRoles;
    private String[] requiredCareteamRoles;
    private LinkedHashMap<String, String> payerClass;
    private LinkedHashMap<String, String> payerPlan;
    private LinkedHashMap<String, String> eula;
     
    private LinkedHashMap<String, String> samhsa;
    private Map<Long, String> accessLevelMap;
    
    //health Home 
    private LinkedHashMap<Long, String> programHealthHome;
    private LinkedHashMap<Long, ProgramNameDTO> programNameHealthHome;
    private LinkedHashMap<Long, String> userRole;
    private LinkedHashMap<String, String> InProviderName;
    private LinkedHashMap<String, String> OutProviderName;
    private LinkedHashMap<Long, String> programNameMap;
    private LinkedHashMap<Long, String> patientStatus;
    //JIRA 1023
    private LinkedHashMap<Long, String> healthHomeCodes;
    private LinkedHashMap<Long, List<Long>> programForHealthHomeCodes;
    private String [] terminationReasonCodes;
    private LinkedHashMap<String, String[]> reasonForPatientStatus;
    private LinkedHashMap<Long, String> hhRequiredForProgram;
    private LinkedHashMap<String, String> county;
    private LinkedHashMap<String, String[]> healthHomeForProgram;
    private String[] deleteReasons;
    private LinkedHashMap<Long, String> statusWithReason;
    private ClientApplicationProperties clientApplicationProperties;
    private LinkedHashMap<Long, String> activityNamesMap;
    private LinkedHashMap<Integer, String> programLevelMap;

    public ReferenceData() {
        
    }

    public String[] getEnrollmentStatuses() {
        return enrollmentStatuses;
    }

    public void setEnrollmentStatuses(String[] enrollmentStatuses) {
        this.enrollmentStatuses = enrollmentStatuses;
    }

    public LinkedHashMap<String, String> getGenderCodes() {
        return genderCodes;
    }

    public void setGenderCodes(LinkedHashMap<String, String> genderCodes) {
        this.genderCodes = genderCodes;
    }

    public String[] getProgramLevels() {
        return programLevels;
    }

    public void setProgramLevels(String[] programLevels) {
        this.programLevels = programLevels;
    }

    public String[] getProgramNames() {
        return programNames;
    }

    public void setProgramNames(String[] programNames) {
        this.programNames = programNames;
    }

    public String[] getUserCredentials() {
        return userCredentials;
    }

    public void setUserCredentials(String[] userCredentials) {
        this.userCredentials = userCredentials;
    }

    public String[] getUserPrefixes() {
        return userPrefixes;
    }

    public void setUserPrefixes(String[] userPrefixes) {
        this.userPrefixes = userPrefixes;
    }

    public String[] getPostalStateCodes() {
        return postalStateCodes;
    }

    public void setPostalStateCodes(String[] postalStateCodes) {
        this.postalStateCodes = postalStateCodes;
    }

    public LinkedHashMap<String, String> getEthnicCodes() {
        return ethnicCodes;
    }

    public void setEthnicCodes(LinkedHashMap<String, String> ethnicCodes) {
        this.ethnicCodes = ethnicCodes;
    }

    public LinkedHashMap<String, String> getLanguageCodes() {
        return languageCodes;
    }

    public void setLanguageCodes(LinkedHashMap<String, String> languageCodes) {
        this.languageCodes = languageCodes;
    }

    public LinkedHashMap<String, String> getRaceCodes() {
        return raceCodes;
    }

    public void setRaceCodes(LinkedHashMap<String, String> raceCodes) {
        this.raceCodes = raceCodes;
    }

//    public long getSouthwestBrooklynOrgId() {
//        return southwestBrooklynOrgId;
//    }

//    public void setSouthwestBrooklynOrgId(long southwestBrooklynOrgId) {
//        this.southwestBrooklynOrgId = southwestBrooklynOrgId;
//    }

    public Map<String, ApplicationInfo> getApplicationInfoMap() {
        return applicationInfoMap;
    }

    public void setApplicationInfoMap(Map<String, ApplicationInfo> applicationInfoMap) {
        this.applicationInfoMap = applicationInfoMap;
    }

    public String[] getEnrollmentReasonsForInactivation() {
        return enrollmentReasonsForInactivation;
    }

    public void setEnrollmentReasonsForInactivation(String[] enrollmentReasonsForInactivation) {
        this.enrollmentReasonsForInactivation = enrollmentReasonsForInactivation;
    }
    

    /**
     * @return the disenrollmentReasonsForInactivation
     */
    public String[] getDisenrollmentReasonsForInactivation() {
        return disenrollmentReasonsForInactivation;
    }

    /**
     * @param disenrollmentReasonsForInactivation the disenrollmentReasonsForInactivation to set
     */
    public void setDisenrollmentReasonsForInactivation(String[] disenrollmentReasonsForInactivation) {
        this.disenrollmentReasonsForInactivation = disenrollmentReasonsForInactivation;
    }

    /**
     * @return the allDisenrollmentReasons
     */
    public String[] getAllDisenrollmentReasons() {
        return allDisenrollmentReasons;
    }

    /**
     * @param allDisenrollmentReasons the allDisenrollmentReasons to set
     */
    public void setAllDisenrollmentReasons(String[] allDisenrollmentReasons) {
        this.allDisenrollmentReasons = allDisenrollmentReasons;
    }

    
    /**
     * @return the patientRelationship
     */
    public LinkedHashMap<Long, String> getPatientRelationship() {
        return patientRelationship;
    }

    /**
     * @param patientRelationship the patientRelationship to set
     */
    public void setPatientRelationship(LinkedHashMap<Long, String> patientRelationship) {
        this.patientRelationship = patientRelationship;
    }

     /**
     * @return the disenrollmentReasonsForRefusal
     */
    public String[] getDisenrollmentReasonsForRefusal() {
        return disenrollmentReasonsForRefusal;
    }
    
     /**
     * @param disenrollmentReasonsForRefusal the disenrollmentReasonsForRefusal to set
     */
    public void setDisenrollmentReasonsForRefusal(String[] disenrollmentReasonsForRefusal) {
        this.disenrollmentReasonsForRefusal = disenrollmentReasonsForRefusal;
    }

    public String[] getDuplicateCareteamRoles() {
        return duplicateCareteamRoles;
    }

    public void setDuplicateCareteamRoles(String[] duplicateCareteamRoles) {
        this.duplicateCareteamRoles = duplicateCareteamRoles;
    }

    public String[] getRequiredCareteamRoles() {
        return requiredCareteamRoles;
    }

    public void setRequiredCareteamRoles(String[] requiredCareteamRoles) {
        this.requiredCareteamRoles = requiredCareteamRoles;
    }

    public LinkedHashMap<String, String> getPayerClass() {
        return payerClass;
    }

    public void setPayerClass(LinkedHashMap<String, String> payerClass) {
        this.payerClass = payerClass;
    }

    public LinkedHashMap<String, String> getPayerPlan() {
        return payerPlan;
    }

    public void setPayerPlan(LinkedHashMap<String, String> payerPlan) {
        this.payerPlan = payerPlan;
    }

    public LinkedHashMap<String, String> getEula() {
        return eula;
    }

    public void setEula(LinkedHashMap<String, String> eula) {
        this.eula = eula;
    }

    public LinkedHashMap<String, String> getSamhsa() {
        return samhsa;
    }

    public void setSamhsa(LinkedHashMap<String, String> samhsa) {
        this.samhsa = samhsa;
    }
    
    public Map<Long, String> getAccessLevelMap() {
        return accessLevelMap;
    }

    public void setAccessLevelMap(Map<Long, String> accessLevelMap) {
        this.accessLevelMap = accessLevelMap;
    }

    /**
     * @return the programHealthHome
     */
    public LinkedHashMap<Long, String> getProgramHealthHome() {
        return programHealthHome;
    }

    /**
     * @param programHealthHome the programHealthHome to set
     */
    public void setProgramHealthHome(LinkedHashMap<Long, String> programHealthHome) {
        this.programHealthHome = programHealthHome;
    }

    /**
     * @return the programNameHealthHome
     */
    public LinkedHashMap<Long, ProgramNameDTO> getProgramNameHealthHome() {
        return programNameHealthHome;
    }

    /**
     * @param programNameHealthHome the programNameHealthHome to set
     */
    public void setProgramNameHealthHome(LinkedHashMap<Long, ProgramNameDTO> programNameHealthHome) {
        this.programNameHealthHome = programNameHealthHome;
    }

    public LinkedHashMap<Long, String> getUserRole() {
        return userRole;
    }

    public void setUserRole(LinkedHashMap<Long, String> userRole) {
        this.userRole = userRole;
    }

//    /**
//     * @return the InProviderName
//     */
//    public LinkedHashMap<String, String> getInProviderName() {
//        return InProviderName;
//    }
//
//    /**
//     * @param InProviderName the InProviderName to set
//     */
//    public void setInProviderName(LinkedHashMap<String, String> InProviderName) {
//        this.InProviderName = InProviderName;
//    }
//
//    /**
//     * @return the OutProviderName
//     */
//    public LinkedHashMap<String, String> getOutProviderName() {
//        return OutProviderName;
//    }
//
//    /**
//     * @param OutProviderName the OutProviderName to set
//     */
//    public void setOutProviderName(LinkedHashMap<String, String> OutProviderName) {
//        this.OutProviderName = OutProviderName;
//    }

    /**
     * @return the programNameMap
     */
    public LinkedHashMap<Long, String> getProgramNameMap() {
        return programNameMap;
    }

    /**
     * @param programNameMap the programNameMap to set
     */
    public void setProgramNameMap(LinkedHashMap<Long, String> programNameMap) {
        this.programNameMap = programNameMap;
    }

    /**
     * @return the patientStatus
     */
    public LinkedHashMap<Long, String> getPatientStatus() {
        return patientStatus;
    }

    /**
     * @param patientStatus the patientStatus to set
     */
    public void setPatientStatus(LinkedHashMap<Long, String> patientStatus) {
        this.patientStatus = patientStatus;
    }

    /**
     * @return the healthHomeCodes
     */
    public LinkedHashMap<Long, String> getHealthHomeCodes() {
        return healthHomeCodes;
    }

    /**
     * @param healthHomeCodes the healthHomeCodes to set
     */
    public void setHealthHomeCodes(LinkedHashMap<Long, String> healthHomeCodes) {
        this.healthHomeCodes = healthHomeCodes;
    }

    public LinkedHashMap<Long, List<Long>> getProgramForHealthHomeCodes() {
        return programForHealthHomeCodes;
    }

    public void setProgramForHealthHomeCodes(LinkedHashMap<Long, List<Long>> programForHealthHomeCodes) {
        this.programForHealthHomeCodes = programForHealthHomeCodes;
    }

    /**
     * @return the terminationReasonCodes
     */
    public String[] getTerminationReasonCodes() {
        return terminationReasonCodes;
    }

    /**
     * @param terminationReasonCodes the terminationReasonCodes to set
     */
    public void setTerminationReasonCodes(String[] terminationReasonCodes) {
        this.terminationReasonCodes = terminationReasonCodes;
    }

    /**
     * @return the reasonForPatientStatus
     */
    public LinkedHashMap<String, String[]> getReasonForPatientStatus() {
        return reasonForPatientStatus;
    }

    /**
     * @param reasonForPatientStatus the reasonForPatientStatus to set
     */
    public void setReasonForPatientStatus(LinkedHashMap<String, String[]> reasonForPatientStatus) {
        this.reasonForPatientStatus = reasonForPatientStatus;
    }

    /**
     * @return the hhRequiredForProgram
     */
    public LinkedHashMap<Long, String> getHhRequiredForProgram() {
        return hhRequiredForProgram;
    }

    /**
     * @param hhRequiredForProgram the hhRequiredForProgram to set
     */
    public void setHhRequiredForProgram(LinkedHashMap<Long, String> hhRequiredForProgram) {
        this.hhRequiredForProgram = hhRequiredForProgram;
    }

    /**
     * @return the healthHomeForProgram
     */
    public LinkedHashMap<String, String[]> getHealthHomeForProgram() {
        return healthHomeForProgram;
    }

    /**
     * @param healthHomeForProgram the healthHomeForProgram to set
     */
    public void setHealthHomeForProgram(LinkedHashMap<String, String[]> healthHomeForProgram) {
        this.healthHomeForProgram = healthHomeForProgram;
    }

    /**
     * @return the deleteReasons
     */
    public String[] getDeleteReasons() {
        return deleteReasons;
    }

    /**
     * @param deleteReasons the deleteReasons to set
     */
    public void setDeleteReasons(String[] deleteReasons) {
        this.deleteReasons = deleteReasons;
    }

    /**
     * @return the statusWithReason
     */
    public LinkedHashMap<Long, String> getStatusWithReason() {
        return statusWithReason;
    }

    /**
     * @param statusWithReason the statusWithReason to set
     */
    public void setStatusWithReason(LinkedHashMap<Long, String> statusWithReason) {
        this.statusWithReason = statusWithReason;
    }

    public ClientApplicationProperties getClientApplicationProperties() {
        return clientApplicationProperties;
    }

    public void setClientApplicationProperties(ClientApplicationProperties clientApplicationProperties) {
        this.clientApplicationProperties = clientApplicationProperties;
    }

    /**
     * @return the activityNamesMap
     */
    public LinkedHashMap<Long, String> getActivityNamesMap() {
        return activityNamesMap;
    }

    /**
     * @param activityNamesMap the activityNamesMap to set
     */
    public void setActivityNamesMap(LinkedHashMap<Long, String> activityNamesMap) {
        this.activityNamesMap = activityNamesMap;
    }

    public void setProgramLevelMap(LinkedHashMap<Integer, String> programLevelMap) {
        this.programLevelMap = programLevelMap;
    }

    public LinkedHashMap<Integer, String> getProgramLevelMap() {
        return programLevelMap;
    }

    public LinkedHashMap<String, String> getCounty() {
        return county;
    }

    public void setCounty(LinkedHashMap<String, String> county) {
        this.county = county;
    }
}
