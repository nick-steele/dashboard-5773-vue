/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.common;

import com.gsihealth.dashboard.common.dto.PersonDTO;

/**
 *
 * @author rsundar
 */
public class SearchPatientResults implements java.io.Serializable{
    private Integer patientCount;
    private SearchResults<PersonDTO> searchResults;

    public Integer getPatientCount() {
        return patientCount;
    }

    public void setPatientCount(Integer patientCount) {
        this.patientCount = patientCount;
    }

    public SearchResults<PersonDTO> getSearchResults() {
        return searchResults;
    }

    public void setSearchResults(SearchResults<PersonDTO> searchResults) {
        this.searchResults = searchResults;
    }
}
