package com.gsihealth.dashboard.common;

import com.gsihealth.dashboard.entity.dto.RoleDTO;
import java.util.List;

/**
 * Container for form data used by the Manage Care Team Window
 * 
 * @author Chad Darby
 */
public class ManageCareTeamFormData implements java.io.Serializable {
   
    private List<String> requiredRolesForCareTeam;
    private List<String> duplicateRoles;
    
    private List<RoleDTO> userRoles;
    
    public ManageCareTeamFormData() {
        
    }

    public List<String> getRequiredRolesForCareTeam() {
        return requiredRolesForCareTeam;
    }

    public void setRequiredRolesForCareTeam(List<String> requiredRolesForCareTeam) {
        this.requiredRolesForCareTeam = requiredRolesForCareTeam;
    }

    public List<RoleDTO> getUserRoles() {
        return userRoles;
    }

    public void setUserRoles(List<RoleDTO> userRoles) {
        this.userRoles = userRoles;
    }

    public List<String> getDuplicateRoles() {
        return duplicateRoles;
    }

    public void setDuplicateRoles(List<String> duplicateRoles) {
        this.duplicateRoles = duplicateRoles;
    }        
}
