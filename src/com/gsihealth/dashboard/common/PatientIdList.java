package com.gsihealth.dashboard.common;

import com.fasterxml.jackson.annotation.JsonRootName;
import java.util.ArrayList;

/**
 *
 * @author vlewis
 */

@JsonRootName(value = "patients")
public class PatientIdList extends ArrayList<Patient>{
    
}
