package com.gsihealth.dashboard.common;

/**
 *
 * @author Chad Darby
 */
public interface AppKeyConstants {
  
    public static String MESSAGING_APP_KEY = "GSI_MESSAGING_APP";
    
}
