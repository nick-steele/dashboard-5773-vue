package com.gsihealth.dashboard.common;

import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class SearchCriteria implements java.io.Serializable {

    private String firstName;
    private String middleName;
    private String lastName;
    private String dateOfBirthStr;
    private String gender;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zipCode;
    private String phone;
    private String careteamId;
    private String programLevelConsentStatus;
    private String programNameId;
    private String enrollmentStatus;
    private String minor;
    private boolean searchAllPatients;
    private long organizationId;
    private String ssn;
    private String orgName;
    private String mediCaidCareId;
    private String patientID;
    private String email;
    private String healthHome;
    private String activePatient;
    private String NetworkProviderId;
    private String providerType;
    private String age;
    private List<SearchRange> searchRange;  // subclass for range searches
    
    private boolean isCarebookPatientSearch = false;
    private static final int NO_ID = -99;

    public SearchCriteria() {
        firstName = "";
        lastName = "";
        gender = "";
        dateOfBirthStr = null;
        middleName = "";
        address1 = "";
        address2 = "";
        city = "";
        state = "";
        zipCode = "";
        phone = "";
        ssn = "";

        careteamId = "";

        programLevelConsentStatus = "";
        programNameId = "";
        enrollmentStatus = "";
        minor = "";
        searchAllPatients = false;
        orgName = "";
        mediCaidCareId = "";
        healthHome = "";
        activePatient = "";

        organizationId = NO_ID;
        NetworkProviderId = "";
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public List<SearchRange> getSearchRange() {
        return searchRange;
    }

    public void setSearchRange(List<SearchRange> searchRange) {
        this.searchRange = searchRange;
    }

    public String getDateOfBirthStr() {
        return dateOfBirthStr;
    }

    public void setDateOfBirthStr(String dateOfBirthStr) {
        this.dateOfBirthStr = dateOfBirthStr;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCareteamId() {
        return careteamId;
    }

    public void setCareteamId(String careteamId) {
        this.careteamId = careteamId;
    }

    public String getEnrollmentStatus() {
        return enrollmentStatus;
    }

    public void setEnrollmentStatus(String enrollmentStatus) {
        this.enrollmentStatus = enrollmentStatus;
    }
    
    public String getMinor() {
        return minor;
    }
     public void setMinor(String minor) {
        this.minor = minor;
    }

    public long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(long organizationId) {
        this.organizationId = organizationId;
    }

    public String getProgramLevelConsentStatus() {
        return programLevelConsentStatus;
    }

    public void setProgramLevelConsentStatus(String programLevelConsentStatus) {
        this.programLevelConsentStatus = programLevelConsentStatus;
    }

    public String getProgramNameId() {
        return programNameId;
    }

    public void setProgramNameId(String programNameId) {
        this.programNameId = programNameId;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    /**
     * @return the orgName
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * @param orgName the orgName to set
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    /**
     * @return the mediCaidCareId
     */
    public String getMediCaidCareId() {
        return mediCaidCareId;
    }

    /**
     * @param mediCaidCareId the mediCaidCareId to set
     */
    public void setMediCaidCareId(String mediCaidCareId) {
        this.mediCaidCareId = mediCaidCareId;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public String getHealthHome() {
        return healthHome;
    }

    public void setHealthHome(String healthHome) {
        this.healthHome = healthHome;
    }

    public String getActivePatient() {
        return activePatient;
    }

    public void setActivePatient(String activePatient) {
        this.activePatient = activePatient;
    }

    /**
     * @return the NetworkProviderId
     */
    public String getNetworkProviderId() {
        return NetworkProviderId;
    }

    /**
     * @param NetworkProviderId the NetworkProviderId to set
     */
    public void setNetworkProviderId(String NetworkProviderId) {
        this.NetworkProviderId = NetworkProviderId;
    }

    /**
     * @return the providerType
     */
    public String getProviderType() {
        return providerType;
    }

    /**
     * @param providerType the providerType to set
     */
    public void setProviderType(String providerType) {
        this.providerType = providerType;
    }

    public boolean isCarebookPatientSearch() {
        return isCarebookPatientSearch;
    }

    public void setIsCarebookPatientSearch(boolean isCarebookPatientSearch) {
        this.isCarebookPatientSearch = isCarebookPatientSearch;
    }

    public boolean isSearchAllPatients() {
        return searchAllPatients;
    }

    public void setSearchAllPatients(boolean searchAllPatients) {
        this.searchAllPatients = searchAllPatients;
    }
    
    
}
