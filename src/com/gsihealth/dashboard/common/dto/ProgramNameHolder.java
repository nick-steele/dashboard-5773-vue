package com.gsihealth.dashboard.common.dto;

/**
 *
 * @author Chad Darby
 */
public class ProgramNameHolder implements java.io.Serializable {
    
    private String programName;
    private String healthHome;
    private long communityId;

    public ProgramNameHolder(String programName, String healthHome, long communityId) {
        this.programName = programName;
        this.healthHome = healthHome;
        this.communityId = communityId;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }

    public String getHealthHome() {
        return healthHome;
    }

    public void setHealthHome(String healthHome) {
        this.healthHome = healthHome;
    }
        
}
