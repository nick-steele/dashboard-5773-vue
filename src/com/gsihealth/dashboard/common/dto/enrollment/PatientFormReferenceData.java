package com.gsihealth.dashboard.common.dto.enrollment;

import com.gsihealth.dashboard.entity.dto.ConsenterDTO;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class PatientFormReferenceData implements Serializable {

    private LinkedHashMap<String, String>  organizationNames;
    private LinkedHashMap<String, String>  payerPlanNames;
    private LinkedHashMap<String, String>  payerClassNames;
    private LinkedHashMap<String, String>  careteamNames;
    private List<UserDTO> userDTOs;
    private List<OrganizationDTO> organizationDTOs;
    private LinkedHashMap<String, String> consenters;

    public PatientFormReferenceData() {        
    }
    
    public LinkedHashMap<String, String> getCareteamNames() {
        return careteamNames;
    }

    public void setCareteamNames(LinkedHashMap<String, String> careteamNames) {
        this.careteamNames = careteamNames;
    }

    public LinkedHashMap<String, String> getOrganizationNames() {
        return organizationNames;
    }

    public void setOrganizationNames(LinkedHashMap<String, String> organizationNames) {
        this.organizationNames = organizationNames;
    }

    public LinkedHashMap<String, String> getPayerPlanNames() {
        return payerPlanNames;
    }

    public void setPayerPlanNames(LinkedHashMap<String, String> payerPlans) {
        this.payerPlanNames = payerPlans;
    }

    public LinkedHashMap<String, String> getPayerClassNames() {
        return payerClassNames;
    }

    public void setPayerClassNames(LinkedHashMap<String, String> payerClassNames) {
        this.payerClassNames = payerClassNames;
    }
     
    public List<UserDTO> getUserDTOs() {
        return userDTOs;
    }

    public void setUserDTOs(List<UserDTO> users) {
        this.userDTOs = users;
    }
     public List<OrganizationDTO> getOrganizationDTOs() {
        return organizationDTOs;
    }

    public void setOrganizationDTOs(List<OrganizationDTO> organizations) {
        this.organizationDTOs = organizations;
    }

    public LinkedHashMap<String, String> getConsenters() {
        return consenters;
    }

    public void setConsenters(LinkedHashMap<String, String> consenters) {
        this.consenters = consenters;
    }
    
    
}
