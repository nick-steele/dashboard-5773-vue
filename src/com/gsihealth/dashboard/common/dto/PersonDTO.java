package com.gsihealth.dashboard.common.dto;

import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;
import com.gsihealth.patientmdminterface.entity.SimplePerson;

import java.util.Date;

/**
 * Data transfer object for Person
 * 
 * @author Chad Darby
 */
public class PersonDTO implements java.io.Serializable {
    SimplePerson simplePerson;
   
    private String email;
    private String source;    
    private String careteam;
    private String consentTime;
    private String consentAMPM;
    
    private String minorConsentTime;
    private String minorConsentAMPM;
    
    private Date endDate;
    private Date createdDate;
    private PatientEnrollmentDTO patientEnrollment;
    
    private String patientUserActive;
    private String originalPatientUserActive;
    private Date enrollmentChangeDate;

    private String mode;
    private String programDeleteReason;

    private String additionalConsentedOrganizations; //TODO should these be in the constructor
    private boolean selfAssertEligible;
    
    //Spira 5438 : To hold date in string format. Date object was changeing during GWT RPC call .
    private String dobAsString;
    
    private boolean notFoundInMirth = false;

    private String patientStatus;

    

    public PersonDTO() {
        patientEnrollment = new PatientEnrollmentDTO();
        simplePerson = new SimplePerson();
    }
    
    public PersonDTO(SimplePerson simplePerson, PatientEnrollmentDTO patientEnrollment ){
        
    }

    public PersonDTO(String euid, String firstName, String lastName, String middleName, Date dateOfBirth, String gender, 
            String streetAddress1, String streetAddress2, String city, String state , String zipCode, String telephone,
            String telephoneExtension, String careteam, String title, String phoneBus, String mothermn, String languageCode, 
            String age, String ageUnits, String genderCode, String county, String country, String localId, String localOrg, 
            String ssn, String driversLicense, String nationality, String language, String dateOfdeath, String mStatus, 
            String motherName, String race, String ethnic, String email, String source, String consentTime, String consentAMPM, 
            Date endDate, Date createdDate, PatientEnrollmentDTO patientEnrollment, String patientUserActive, String originalPatientUserActive, 
            Date enrollmentChangeDate, String mode, String programDeleteReason, String minor, String additionalConsentedOrganizations) {
        this.simplePerson = new SimplePerson();
        this.simplePerson.setPatientEuid(euid);
        
        this.simplePerson.setFirstName(firstName);
        this.simplePerson.setLastName(lastName);
        this.simplePerson.setMiddleName(middleName);
        this.simplePerson.setDateOfBirth(dateOfBirth);
        this.simplePerson.setGenderCode(gender);
        this.simplePerson.setStreetAddress1(streetAddress1);
        this.simplePerson.setStreetAddress2(streetAddress2);
        this.simplePerson.setCity(city);
        this.simplePerson.setState(state);
        this.simplePerson.setZipCode(zipCode);
        this.simplePerson.setPhoneHome(telephone);
        this.simplePerson.setTelephoneExtension(telephoneExtension);
        
        this.simplePerson.setPhoneBus(phoneBus);
        this.simplePerson.setMothermn(mothermn);
        this.simplePerson.setLanguageCode(languageCode);
        this.simplePerson.setAge(age);
        this.simplePerson.setAgeUnits(ageUnits);
        this.simplePerson.setGenderCode(genderCode);
        this.simplePerson.setMinor(minor) ;
        this.simplePerson.setCounty(county);
        this.simplePerson.setCountry(country);
        this.simplePerson.setLocalId(localId);
        this.simplePerson.setLocalOrg(localOrg);
        this.simplePerson.setSSN(ssn);
        this.simplePerson.setLicenseNum(driversLicense);
        this.simplePerson.setNationality(nationality);
        this.simplePerson.setLanguage(language);
        this.simplePerson.setDateOfdeath(dateOfdeath);
        this.simplePerson.setmStatus(mStatus);
        this.simplePerson.setMotherName(motherName);
        this.simplePerson.setRace(race);
        this.simplePerson.setEthnicityCode(ethnic);
        this.simplePerson.setTitle(title);
        
        this.careteam = careteam;
        this.email = email;
        this.source = source;
        this.consentTime = consentTime;
        this.consentAMPM = consentAMPM;
        this.endDate = endDate;
        this.createdDate = createdDate;
        this.patientEnrollment = patientEnrollment;
        this.patientUserActive = patientUserActive;
        this.originalPatientUserActive = originalPatientUserActive;
        this.enrollmentChangeDate = enrollmentChangeDate;
        this.mode = mode;
        this.programDeleteReason = programDeleteReason;
        
        this.additionalConsentedOrganizations = additionalConsentedOrganizations;
    }
    
    public boolean isNotFoundInMirth() {
        return notFoundInMirth;
    }

    public void setNotFoundInMirth(boolean notFoundInMirth) {
        this.notFoundInMirth = notFoundInMirth;
    }
    
    public String getCity() {
        return this.simplePerson.getCity();
    }

    public void setCity(String city) {
        this.simplePerson.setCity(city);
    }

    public Date getDateOfBirth() {
        return this.simplePerson.getDateOfBirth() ;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.simplePerson.setDateOfBirth(dateOfBirth);
    }
    
    //Spira 5438 : To get date in string format. Value of date in date object was changeing during GWT RPC call .
    public String getDateOfBirthAsString() {
        return this.dobAsString ;
    }

    //Spira 5438 : To set date in string format. Value of date in date object was changeing during GWT RPC call .
    public void setDateOfBirthAsString(String dateOfBirth) {
        this.dobAsString = dateOfBirth;
    }

    public String getFirstName() {
        return this.simplePerson.getFirstName() ;
    }

    public void setFirstName(String firstName) {
        this.simplePerson.setFirstName(firstName);
    }

    public String getGender() {
        return this.simplePerson.getGenderCode() ;
    }

    public void setGender(String gender) {
        this.simplePerson.setGenderCode(gender);
    }

    public String getLastName() {
        return this.simplePerson.getLastName() ;
    }

    public void setLastName(String lastName) {
        this.simplePerson.setLastName(lastName);
    }

    public String getState() {
        return this.simplePerson.getState()  ;
    }

    public void setState(String state) {
        this.simplePerson.setState(state);
    }

    public String getStreetAddress1() {
        return this.simplePerson.getStreetAddress1()  ;
    }

    public void setStreetAddress1(String streetAddress1) {
        this.simplePerson.setStreetAddress1(streetAddress1);
    }

    public String getStreetAddress2() {
        return this.simplePerson.getStreetAddress2() ;
    }

    public void setStreetAddress2(String streetAddress2) {
        this.simplePerson.setStreetAddress2(streetAddress2);
    }

    public String getZipCode() {
        return this.simplePerson.getZipCode();
    }

    public void setZipCode(String zipCode) {
        this.simplePerson.setZipCode(zipCode);
    }

    public String getEuid() {
        return this.simplePerson.getPatientEuid() ;
    }

    public void setEuid(String euid) {
        this.simplePerson.setPatientEuid(euid);
    }

    public String getMiddleName() {
        return this.simplePerson.getMiddleName()  ;
    }

    public void setMiddleName(String middleName) {
        this.simplePerson.setMiddleName(middleName);
    }

    public String getTelephone() {
        return this.simplePerson.getPhoneHome()  ;
    }

    public void setTelephone(String telephone) {
        this.simplePerson.setPhoneHome(telephone);
    }
    
       public String getMinor() {
        return this.simplePerson.getMinor() ;
    }

    public void setMinor(String minor) {
        this.simplePerson.setMinor(minor);
    }

    //Add getter and setters from Simple Person Class

    public String getAge() {
        return this.simplePerson.getAge() ;
    }

    public void setAge(String age) {
        this.simplePerson.setAge(age);
    }

    public String getAgeUnits() {
        return this.simplePerson.getAgeUnits() ;
    }

    public void setAgeUnits(String ageUnits) {
        this.simplePerson.setAgeUnits(ageUnits);
    }

    public String getCountry() {
        return this.simplePerson.getCountry() ;
    }

    public void setCountry(String country) {
        this.simplePerson.setCountry(country);
    }

    public String getCounty() {
        return this.simplePerson.getCounty()  ;
    }

    public void setCounty(String county) {
        this.simplePerson.setCounty(county);
    }


    public String getDateOfdeath() {
        return this.simplePerson.getDateOfdeath() ;
    }

    public void setDateOfdeath(String dateOfdeath) {
        this.simplePerson.setDateOfdeath(dateOfdeath);
    }


    public String getGenderCode() {
        return this.simplePerson.getGenderCode() ;
    }

    public void setGenderCode(String genderCode) {
        this.simplePerson.setGenderCode(genderCode);
    }

    public String getLanguage() {
        return this.simplePerson.getLanguage()  ;
    }

    public void setLanguage(String language) {
        this.simplePerson.setLanguage(language);
    }

    public String getLanguageCode() {
        return this.simplePerson.getLanguageCode() ;
    }

    public void setLanguageCode(String languageCode) {
        this.simplePerson.setLanguageCode(languageCode);
    }

    public String getLocalId() {
        return this.simplePerson.getLocalId()  ;
    }

    public void setLocalId(String localId) {
        this.simplePerson.setLocalId(localId);
    }

    public String getLocalOrg() {
        return this.simplePerson.getLocalOrg() ;
    }

    public void setLocalOrg(String localOrg) {
        this.simplePerson.setLocalOrg(localOrg);
    }

    public String getmStatus() {
        return this.simplePerson.getmStatus() ;
    }

    public void setmStatus(String mStatus) {
        this.simplePerson.setmStatus(mStatus);
    }

    public String getMotherName() {
        return this.simplePerson.getMotherName() ;
    }

    public void setMotherName(String motherName) {
        this.simplePerson.setMotherName(motherName);
    }

    public String getMothermn() {
        return this.simplePerson.getMothermn() ;
    }

    public void setMothermn(String mothermn) {
        this.simplePerson.setMothermn(mothermn);
    }

    public String getNationality() {
        return this.simplePerson.getNationality() ;
    }

    public void setNationality(String nationality) {
        this.simplePerson.setNationality(nationality);
    }


    public String getPhoneBus() {
        return this.simplePerson.getPhoneBus() ;
    }

    public void setPhoneBus(String phoneBus) {
        this.simplePerson.setPhoneBus(phoneBus);
    }

    public String getRace() {
        return this.simplePerson.getRace()  ;
    }

    public void setRace(String race) {
        this.simplePerson.setRace(race);
    }

    public String getSsn() {
        return this.simplePerson.getSSN()  ;
    }

    public void setSsn(String ssn) {
        this.simplePerson.setSSN(ssn);
    }

    public String getTitle() {
        return this.simplePerson.getTitle() ;
    }

    public void setTitle(String title) {
        this.simplePerson.setTitle(title);
    }

    public String getDriversLicense() {
        return this.simplePerson.getLicenseNum() ;
    }

    public void setDriversLicense(String driversLicense) {
        this.simplePerson.setLicenseNum(driversLicense);
    }
    
    public String getEthnic() {
        return this.simplePerson.getEthnicityCode() ;
    }

    public void setEthnic(String ethnic) {
        this.simplePerson.setEthnicityCode(ethnic);
    }
    
    public String getTelephoneExtension() {
        return this.simplePerson.getTelephoneExtension() ;
    }

    public void setTelephoneExtension(String telephoneExtension) {
        this.simplePerson.setTelephoneExtension(telephoneExtension);
    }


    public String getCareteam() {
        return careteam;
    }

    public void setCareteam(String careteam) {
        this.careteam = careteam;
    }
    
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public PatientEnrollmentDTO getPatientEnrollment() {
        return patientEnrollment;
    }

        public void setPatientEnrollment(PatientEnrollmentDTO patientEnrollment) {
        this.patientEnrollment = patientEnrollment;
    }

    public String getConsentAMPM() {
        return consentAMPM;
    }

    public void setConsentAMPM(String consentAMPM) {
        this.consentAMPM = consentAMPM;
    }

    public String getConsentTime() {
        return consentTime;
    }

    public void setConsentTime(String consentTime) {
        this.consentTime = consentTime;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
     public String getPatientUserActive() {
        return patientUserActive;
    }

    public void setPatientUserActive(String patientUserActive) {
        this.patientUserActive = patientUserActive;
    }

    public String getOriginalPatientUserActive() {
        return originalPatientUserActive;
    }

    public void setOriginalPatientUserActive(String originalPatientUserActive) {
        this.originalPatientUserActive = originalPatientUserActive;
    }

    /**
     * @return the createdDate
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * @return the enrollmentChangeDate
     */
    public Date getEnrollmentChangeDate() {
        return enrollmentChangeDate;
    }

    /**
     * @param enrollmentChangeDate the enrollmentChangeDate to set
     */
    public void setEnrollmentChangeDate(Date enrollmentChangeDate) {
        this.enrollmentChangeDate = enrollmentChangeDate;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    /**
     * @return the programDeleteReason
     */
    public String getProgramDeleteReason() {
        return programDeleteReason;
    }

    /**
     * @param programDeleteReason the programDeleteReason to set
     */
    public void setProgramDeleteReason(String programDeleteReason) {
        this.programDeleteReason = programDeleteReason;
    }

    /**
     * @return the minorConsentTime
     */
    public String getMinorConsentTime() {
        return minorConsentTime;
    }

    /**
     * @param minorConsentTime the minorConsentTime to set
     */
    public void setMinorConsentTime(String minorConsentTime) {
        this.minorConsentTime = minorConsentTime;
    }

    /**
     * @return the minorConsentAMPM
     */
    public String getMinorConsentAMPM() {
        return minorConsentAMPM;
    }

    /**
     * @param minorConsentAMPM the minorConsentAMPM to set
     */
    public void setMinorConsentAMPM(String minorConsentAMPM) {
        this.minorConsentAMPM = minorConsentAMPM;
    }

    public String getAdditionalConsentedOrganizations() {
        return additionalConsentedOrganizations;
    }

    public void setAdditionalConsentedOrganizations(String additionalConsentedOrganizations) {
        this.additionalConsentedOrganizations = additionalConsentedOrganizations;
    }
    public boolean isSelfAssertEligible() {
        return selfAssertEligible;
    }

    public void setSelfAssertEligible(boolean selfAssertEligible) {
        this.selfAssertEligible = selfAssertEligible;
    }

    public SimplePerson getSimplePerson() {
        return simplePerson;
    }
    public void setSimplePerson(SimplePerson simplePerson) {
        this.simplePerson = simplePerson;
    }

    public String getPatientStatus() {
        return patientStatus;
    }

    public void setPatientStatus(String patientStatus) {
        this.patientStatus = patientStatus;
    }
}
