/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.common.dto;

import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import java.util.Date;

/**
 *
 * @author beth.boose
 */
public class PatientOtherIdsDTO implements java.io.Serializable {

    private Long patientOtherIdsId;
    private long patientId;
    private String patientOtherId;
    private String patientOtherOrgOid;
    private OrganizationDTO organizationDTO;
    private String organizationName;
    private String patientOtherOrgName;
    private long communityId;
    private Date creationDateTime;
    
    public Long getPatientOtherIdsId() {
        return patientOtherIdsId;
    }

    public void setPatientOtherIdsId(Long patientOtherIdsId) {
        this.patientOtherIdsId = patientOtherIdsId;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public String getPatientOtherId() {
        return patientOtherId;
    }

    public void setPatientOtherId(String patientOtherId) {
        this.patientOtherId = patientOtherId;
    }

    public String getPatientOtherOrgOid() {
        return patientOtherOrgOid;
    }

    public void setPatientOtherOrgOid(String patientOtherOrgOid) {
        this.patientOtherOrgOid = patientOtherOrgOid;
    }

     public OrganizationDTO getOrganizationDTO() {
        return organizationDTO;
    }

    public void setOrganizationDTO(OrganizationDTO organizationDTO) {
        this.organizationDTO = organizationDTO;
    }
    
     public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getPatientOtherOrgName() {
        return patientOtherOrgName;
    }

    public void setPatientOtherOrgName(String patientOtherOrgName) {
        this.patientOtherOrgName = patientOtherOrgName;
    }
    
    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }
    
     public Date getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(Date creationDateTime) {
        this.creationDateTime = creationDateTime;
    }
}
