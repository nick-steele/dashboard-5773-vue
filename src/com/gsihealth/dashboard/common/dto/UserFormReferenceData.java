package com.gsihealth.dashboard.common.dto;

import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class UserFormReferenceData implements java.io.Serializable {
    
    private List<OrganizationDTO> organizationDTOs;
    private LinkedHashMap<String, String> accessLevels;
    private LinkedHashMap<String, String> roles;
    
    public UserFormReferenceData() {
        
    }
    
    public List<OrganizationDTO> getOrganizationDTOs() {
        return organizationDTOs;
    }

    public void setOrganizationDTOs(List<OrganizationDTO> organizations) {
        this.organizationDTOs = organizations;
    }
   
    public LinkedHashMap<String, String> getAccessLevels() {
        return accessLevels;
    }

    public void setAccessLevels(LinkedHashMap<String, String> accessLevels) {
        this.accessLevels = accessLevels;
    }

    public LinkedHashMap<String, String> getRoles() {
        return roles;
    }

    public void setRoles(LinkedHashMap<String, String> roles) {
        this.roles = roles;
    }
        
}
