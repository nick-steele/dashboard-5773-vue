package com.gsihealth.dashboard.common;
import java.io.Serializable;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author vlewis
 */

@XmlRootElement

public class ViewPatientDemog implements Serializable {
    private static final long serialVersionUID = 1L;
 
    private String firstname;
    
    private String lastname;
   
    private String middlename;
   
    private String gender;
 
    private String ssn;
    
    private Date dob;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
   
    private String  age;
  
   
    private long communityId;
   
    private long patientId;
 
    private String patientEuid;
    
    private Date startDate;
 
    private String status;
    
    private String programName;
   
    private Date programNameEffectiveDate;
   
    private Date programNameEndDate;
   
    private String oid;
   
    private String programLevelConsentStatus;
  
    private String acuityScore;
   
    private long orgId;
    
    private String orgName;
   
    private String reasonForInactivation;
    
    private String patientMrn;
   
    private Date consentDateTime;
 
    private long consentFormId;
 
    private long consentObtainedByUserId;
   
    private String informationSharingMinor;

    private Date minorConsentDateTime;
   
    private long minorConsentObtainedByUserId;
  
    private String reasonForRefusal;
    
    private String primaryPayerClass;
  
    private String primaryPayerPlan;
   
    private String primaryPayerMedicaidMedicareId;
    
  
    private Date primaryPlanEffectiveDate;
 
    private String secondaryPayerClass;
  
    private String secondaryPayerPlan;
   
    private String secondaryPayerMedicaidMedicareId;
    
    private Date secondaryPlanEffectiveDate;
    
    private Date createdDate;
  
    private Date lastModifiedDate;
  
    private String emailAddress;
  
    private String  emergencyContact;
    
    private String  programHealthHome;
   
    private String directAddress;
 
    private long roleId;
   
    private String patientUserActive;
 
    private Date lastPasswordChangeDate;
    
    private String patientLoginId;
    
    private long patientSourceId;
   
    private Character lockBoxActive;

    private boolean alreadyInList;

    public ViewPatientDemog() {
    }
  
     public ViewPatientDemog(Long patientId) {
        this.patientId = patientId;
       
    }
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public String getPatientEuid() {
        return patientEuid;
    }

    public void setPatientEuid(String patientEuid) {
        this.patientEuid = patientEuid;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public Date getProgramNameEffectiveDate() {
        return programNameEffectiveDate;
    }

    public void setProgramNameEffectiveDate(Date programNameEffectiveDate) {
        this.programNameEffectiveDate = programNameEffectiveDate;
    }

    public Date getProgramNameEndDate() {
        return programNameEndDate;
    }

    public void setProgramNameEndDate(Date programNameEndDate) {
        this.programNameEndDate = programNameEndDate;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getProgramLevelConsentStatus() {
        return programLevelConsentStatus;
    }

    public void setProgramLevelConsentStatus(String programLevelConsentStatus) {
        this.programLevelConsentStatus = programLevelConsentStatus;
    }

    public String getAcuityScore() {
        return acuityScore;
    }

    public void setAcuityScore(String acuityScore) {
        this.acuityScore = acuityScore;
    }

    public long getOrgId() {
        return orgId;
    }

    public void setOrgId(long orgId) {
        this.orgId = orgId;
    }

    public String getReasonForInactivation() {
        return reasonForInactivation;
    }

    public void setReasonForInactivation(String reasonForInactivation) {
        this.reasonForInactivation = reasonForInactivation;
    }

    public String getPatientMrn() {
        return patientMrn;
    }

    public void setPatientMrn(String patientMrn) {
        this.patientMrn = patientMrn;
    }

    public Date getConsentDateTime() {
        return consentDateTime;
    }

    public void setConsentDateTime(Date consentDateTime) {
        this.consentDateTime = consentDateTime;
    }

    public long getConsentFormId() {
        return consentFormId;
    }

    public void setConsentFormId(long consentFormId) {
        this.consentFormId = consentFormId;
    }

    public long getConsentObtainedByUserId() {
        return consentObtainedByUserId;
    }

    public void setConsentObtainedByUserId(long consentObtainedByUserId) {
        this.consentObtainedByUserId = consentObtainedByUserId;
    }

    public String getInformationSharingMinor() {
        return informationSharingMinor;
    }

    public void setInformationSharingMinor(String informationSharingMinor) {
        this.informationSharingMinor = informationSharingMinor;
    }

    public Date getMinorConsentDateTime() {
        return minorConsentDateTime;
    }

    public void setMinorConsentDateTime(Date minorConsentDateTime) {
        this.minorConsentDateTime = minorConsentDateTime;
    }

    public long getMinorConsentObtainedByUserId() {
        return minorConsentObtainedByUserId;
    }

    public void setMinorConsentObtainedByUserId(long minorConsentObtainedByUserId) {
        this.minorConsentObtainedByUserId = minorConsentObtainedByUserId;
    }

    public String getReasonForRefusal() {
        return reasonForRefusal;
    }

    public void setReasonForRefusal(String reasonForRefusal) {
        this.reasonForRefusal = reasonForRefusal;
    }

    public String getPrimaryPayerClass() {
        return primaryPayerClass;
    }

    public void setPrimaryPayerClass(String primaryPayerClass) {
        this.primaryPayerClass = primaryPayerClass;
    }

    public String getPrimaryPayerPlan() {
        return primaryPayerPlan;
    }

    public void setPrimaryPayerPlan(String primaryPayerPlan) {
        this.primaryPayerPlan = primaryPayerPlan;
    }

    public String getPrimaryPayerMedicaidMedicareId() {
        return primaryPayerMedicaidMedicareId;
    }

    public void setPrimaryPayerMedicaidMedicareId(String primaryPayerMedicaidMedicareId) {
        this.primaryPayerMedicaidMedicareId = primaryPayerMedicaidMedicareId;
    }

    public Date getPrimaryPlanEffectiveDate() {
        return primaryPlanEffectiveDate;
    }

    public void setPrimaryPlanEffectiveDate(Date primaryPlanEffectiveDate) {
        this.primaryPlanEffectiveDate = primaryPlanEffectiveDate;
    }

    public String getSecondaryPayerClass() {
        return secondaryPayerClass;
    }

    public void setSecondaryPayerClass(String secondaryPayerClass) {
        this.secondaryPayerClass = secondaryPayerClass;
    }

    public String getSecondaryPayerPlan() {
        return secondaryPayerPlan;
    }

    public void setSecondaryPayerPlan(String secondaryPayerPlan) {
        this.secondaryPayerPlan = secondaryPayerPlan;
    }

    public String getSecondaryPayerMedicaidMedicareId() {
        return secondaryPayerMedicaidMedicareId;
    }

    public void setSecondaryPayerMedicaidMedicareId(String secondaryPayerMedicaidMedicareId) {
        this.secondaryPayerMedicaidMedicareId = secondaryPayerMedicaidMedicareId;
    }

    public Date getSecondaryPlanEffectiveDate() {
        return secondaryPlanEffectiveDate;
    }

    public void setSecondaryPlanEffectiveDate(Date secondaryPlanEffectiveDate) {
        this.secondaryPlanEffectiveDate = secondaryPlanEffectiveDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getEmergencyContact() {
        return emergencyContact;
    }

    public void setEmergencyContact(String emergencyContact) {
        this.emergencyContact = emergencyContact;
    }

    public String getProgramHealthHome() {
        return programHealthHome;
    }

    public void setProgramHealthHome(String programHealthHome) {
        this.programHealthHome = programHealthHome;
    }

    public String getDirectAddress() {
        return directAddress;
    }

    public void setDirectAddress(String directAddress) {
        this.directAddress = directAddress;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public String getPatientUserActive() {
        return patientUserActive;
    }

    public void setPatientUserActive(String patientUserActive) {
        this.patientUserActive = patientUserActive;
    }

    public Date getLastPasswordChangeDate() {
        return lastPasswordChangeDate;
    }

    public void setLastPasswordChangeDate(Date lastPasswordChangeDate) {
        this.lastPasswordChangeDate = lastPasswordChangeDate;
    }

    public String getPatientLoginId() {
        return patientLoginId;
    }

    public void setPatientLoginId(String patientLoginId) {
        this.patientLoginId = patientLoginId;
    }

    public long getPatientSourceId() {
        return patientSourceId;
    }

    public void setPatientSourceId(long patientSourceId) {
        this.patientSourceId = patientSourceId;
    }

    public Character getLockBoxActive() {
        return lockBoxActive;
    }

    public void setLockBoxActive(Character lockBoxActive) {
        this.lockBoxActive = lockBoxActive;
    }

    /**
     * @return the orgName
     */
    public String getOrgName() {
        return orgName;
    }

    /**
     * @param orgName the orgName to set
     */
    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public boolean isAlreadyInList() {
        return alreadyInList;
    }

    public void setAlreadyInList(boolean alreadyInList) {
        this.alreadyInList = alreadyInList;
    }
}
