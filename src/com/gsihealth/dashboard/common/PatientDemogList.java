package com.gsihealth.dashboard.common;

import com.fasterxml.jackson.annotation.JsonRootName;
import java.util.ArrayList;

/**
 *
 * @author vlewis
 */

@JsonRootName(value = "PatientList")
public class PatientDemogList extends ArrayList<ViewPatientDemog>{
    
}
