package com.gsihealth.dashboard.common;

/**
 *
 * @author Chad Darby
 */
public interface ConsentConstants {
    
    public static final String CONSENT_PERMIT = "PERMIT";
    public static final String CONSENT_DENY = "DENY";
    public static final String CONSENT_NOTIFICATION_STREAMLINE = "consent.notification.streamline";
    
}
