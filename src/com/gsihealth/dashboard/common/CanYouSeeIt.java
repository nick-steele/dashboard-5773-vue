/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.common;

import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import java.util.logging.Logger;

/**
 *
 * @author Beth.Boose
 */
public class CanYouSeeIt {
    private static final Logger logger = Logger.getLogger("CanYouSeeIt");
    
    /**
     * users can't edit/access a patient in a different community 
     * @param user
     * @param personDTO
     * @param imaSuperDuperUser
     * @return true if this user can see this person or imaSuperDuperUser is true
     */
    public static boolean thisUserCanSeeThisPatient(LoginResult userLogin, PersonDTO personDTO, boolean imaSuperDuperUser) {
        boolean canSeeIt = false;
        if(userLogin == null || personDTO == null) {
            return false;
        }
        
        if(imaSuperDuperUser || (userLogin.getCommunityId() == personDTO.getPatientEnrollment().getCommunityId())){
            canSeeIt = true;
        }
        logger.info("can this user see this patient: " + canSeeIt);
        return canSeeIt;
    }
    
    /**
     * A user cant add/update/access a user in another community – NOTE, we’ll need some sort of super-duper admin access 
     * @param user
     * @param user
     * @return 
     */
    public static boolean thisUserCanSeeThisUser(LoginResult userLogin, UserDTO editUser){
        boolean canSeeIt = false;
        if(userLogin == null || editUser == null){
            return false;
        }
        if(userLogin.getCommunityId() == editUser.getCommunityId()) {
            canSeeIt = true;
        }
        logger.info("can this user see this user: " + canSeeIt);
        return canSeeIt;
    } 
}
