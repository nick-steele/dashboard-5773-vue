package com.gsihealth.dashboard.common;

import java.io.Serializable;

/**
 *
 * @author Vince Lewis
 */
public class Patient implements Serializable{
      private String id;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }  
}
