package com.gsihealth.dashboard.client.mypatientlist;

/**
 *
 * @author Chad.Darby
 */
public interface MyPatientListConstants {

    public static final String PATIENT_ID = "Patient ID";
    public static final String FIRST_NAME = "First Name";
    public static final String LAST_NAME = "Last Name";
    public static final String FULL_NAME = "Name";
    public static final String CM_ORGANIZATION = "CM Organization";
    public static final String ENROLLMENT_STATUS = "Enrollment Status";
    
    public static final String ACTIONS = "Actions";    
    public static final String ROW_NUM = "Row Num";
}
