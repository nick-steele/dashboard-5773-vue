package com.gsihealth.dashboard.client.mypatientlist;

import com.gsihealth.dashboard.entity.dto.MyPatientInfoDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Chad.Darby
 */
public class MyPatientListUtils {

    public static ListGridRecord[] convert(List<MyPatientInfoDTO> myPatientInfoCollection) {
        ListGridRecord[] records = new ListGridRecord[myPatientInfoCollection.size()];
        
        for (int index = 0; index < records.length; index++) {
            MyPatientInfoDTO temp = myPatientInfoCollection.get(index);
            
            records[index] = new MyPatientRecord(index, temp);
        }
        
        return records;
    }    
}
