package com.gsihealth.dashboard.client.mypatientlist;

import com.gsihealth.dashboard.entity.dto.MyPatientInfoDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 *
 * @author Chad.Darby
 */
public class MyPatientRecord extends ListGridRecord {

    public MyPatientRecord(int rowNum, long patientId, String firstName, String lastName, String cmOrg, String enrollmentStatus) {

        setAttribute(MyPatientListConstants.ROW_NUM, rowNum);

        setAttribute(MyPatientListConstants.PATIENT_ID, Long.toString(patientId));
        setAttribute(MyPatientListConstants.FIRST_NAME, firstName);
        setAttribute(MyPatientListConstants.LAST_NAME, lastName);

        setAttribute(MyPatientListConstants.FULL_NAME, lastName + ", " + firstName);
        
        setAttribute(MyPatientListConstants.CM_ORGANIZATION, cmOrg);
        setAttribute(MyPatientListConstants.ENROLLMENT_STATUS, enrollmentStatus);
        setAttribute(MyPatientListConstants.ACTIONS, "");        
    }

    public MyPatientRecord(int rowNum, MyPatientInfoDTO temp) {
        this(rowNum, temp.getPatientId(), temp.getFirstName(), temp.getLastName(), temp.getCmOrganizationName(), temp.getEnrollmentStatus());
    }
}
