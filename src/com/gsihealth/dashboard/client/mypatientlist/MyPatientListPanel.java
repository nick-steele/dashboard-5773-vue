package com.gsihealth.dashboard.client.mypatientlist;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.service.MyPatientListService;
import com.gsihealth.dashboard.client.service.MyPatientListServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.MyPatientInfoDTO;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.AutoFitEvent;
import com.smartgwt.client.types.FetchMode;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
@Deprecated
public class MyPatientListPanel extends VLayout {

    private MyPatientListServiceAsync myPatientListService = GWT.create(MyPatientListService.class);
    private ListGrid listGrid;

    public MyPatientListPanel() {

        buildGui();
        DashBoardApp.restartTimer();

        loadMyPatientList();
    }
    
    private void buildGui() {

        HLayout hlayout = new HLayout();
        hlayout.setWidth100();
        
        Label title = new Label("My Patient List");
        title.setHeight(20);
        title.setStyleName("myListTitleLabel");
        
        hlayout.addMember(title);
        
        ImgButton refreshImage = createGridButton("mypatientlist/refresh-blue.png", "Refresh");
        refreshImage.setStyleName("refreshButton");
        
        refreshImage.addClickHandler(new RefreshClickHandler());
        
        hlayout.addMember(refreshImage);
        
        addMember(hlayout);
        
        listGrid = buildListGrid();

        addMember(listGrid);

    }

    /** Helper method to build a ListGrid
     *
     * @return
     */
    private ListGrid buildListGrid() {

        String[] columnNames = {MyPatientListConstants.FULL_NAME, 
            MyPatientListConstants.CM_ORGANIZATION, MyPatientListConstants.ENROLLMENT_STATUS, MyPatientListConstants.ACTIONS};
        
        ListGrid theListGrid = new MyPatientListGrid();
        
        theListGrid.setWidth100();
        theListGrid.setHeight100();
        
        theListGrid.setShowFilterEditor(true);
        theListGrid.setFilterOnKeypress(true);
        theListGrid.setDataFetchMode(FetchMode.LOCAL);
        theListGrid.setAutoFetchData(true);  

        theListGrid.setVirtualScrolling(false);          
        theListGrid.setShowRecordComponents(true);          
        theListGrid.setShowRecordComponentsByCell(true);  
        
        // add fields
        ListGridField[] fields = buildListGridFields(columnNames);
        theListGrid.setFields(fields);

        theListGrid.setEmptyMessage("...");
        theListGrid.setCanResizeFields(true);

        theListGrid.setHeaderAutoFitEvent(AutoFitEvent.NONE);
        
        return theListGrid;
    }

    private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];

        for (int i = 0; i < columnNames.length; i++) {
            String tempColumnName = columnNames[i];
            ListGridField tempField = buildListGridField(tempColumnName);
            fields[i] = tempField;            
        }

        return fields;
    }

    private ListGridField buildListGridField(String title) {
        ListGridField tempField = new ListGridField(title, title);

        tempField.setCanEdit(false);

        return tempField;
    }

    public void populateListGrid(ListGridRecord[] data) {
       
        // scroll to first row
        listGrid.scrollToRow(1);
        final DataSource dataSource = new DataSource();
        dataSource.setID("dataSource");
        dataSource.setClientOnly(true);

        DataSourceTextField fullName = new DataSourceTextField(MyPatientListConstants.FULL_NAME, MyPatientListConstants.FULL_NAME);
        DataSourceTextField cmOrganization = new DataSourceTextField(MyPatientListConstants.CM_ORGANIZATION, MyPatientListConstants.CM_ORGANIZATION);
        DataSourceTextField enrollmentStatus = new DataSourceTextField(MyPatientListConstants.ENROLLMENT_STATUS, MyPatientListConstants.ENROLLMENT_STATUS);
        
        DataSourceTextField actions = new DataSourceTextField(MyPatientListConstants.ACTIONS, MyPatientListConstants.ACTIONS);
        actions.setCanFilter(false);
        
        dataSource.setFields(fullName, cmOrganization, enrollmentStatus, actions);
        dataSource.setCacheData(data);
        
        listGrid.setDataSource(dataSource);
        listGrid.setData(data);
        listGrid.markForRedraw();   
    }

    class RefreshClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            loadMyPatientList();
        }
    }
    
    public void loadMyPatientList() {
        LoadMyPatientListCallback loader = new LoadMyPatientListCallback();
        loader.attempt();
    }

    class LoadMyPatientListCallback extends RetryActionCallback<List<MyPatientInfoDTO>> {

        @Override
        public void attempt() {
            LoginResult loginResult = ApplicationContextUtils.getLoginResult();
            long userId = loginResult.getUserId();
            long communityId= loginResult.getCommunityId();
            
            myPatientListService.getMyPatientList(userId, communityId, this);
        }
        
        @Override
        public void onCapture(List<MyPatientInfoDTO> data) {            
            
            ListGridRecord[] records = MyPatientListUtils.convert(data);
            
            populateListGrid(records);
        }
    }

    protected ImgButton createGridButton(String imageSrc, String prompt) {
        ImgButton theImageButton = new ImgButton();

        theImageButton.setSrc(imageSrc);
        theImageButton.setPrompt(prompt);

        theImageButton.setShowDown(false);
        theImageButton.setShowRollOver(false);
        theImageButton.setAlign(Alignment.CENTER);
        theImageButton.setHeight(16);
        theImageButton.setWidth(16);

        return theImageButton;
    }
    
}

