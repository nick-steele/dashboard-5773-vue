package com.gsihealth.dashboard.client.mypatientlist;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.WindowManager;
import com.gsihealth.dashboard.client.common.DashboardTitles;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.enrollment.EnrollmentPanel;
import com.gsihealth.dashboard.client.enrollment.ViewPatientPanel;
import com.gsihealth.dashboard.client.service.MyPatientListService;
import com.gsihealth.dashboard.client.service.MyPatientListServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.common.AccessLevelConstants;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.MyPatientInfoDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;

/**
 *
 * @author Chad.Darby
 */
public class MyPatientListGrid extends ListGrid {

    private MyPatientListServiceAsync myPatientListService = GWT.create(MyPatientListService.class);
    private boolean patientSearchEnabled;
    private static final int PATIENT_SEARCH_CANVAS_SIZE = 5;
    private static final int NON_PATIENT_SEARCH_CANVAS_SIZE = 4;

    // Redux
    private ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
    private int userInterfaceType = props.getUserInterfaceType();
            
    public MyPatientListGrid() {
        patientSearchEnabled = ApplicationContextUtils.getClientApplicationProperties().isPatientSearchEnabled();
        // Redux - this gets called when the list grid is instantiated.
        //exportJSNI();
    }

    @Override
    protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {

        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        long userId = loginResult.getUserId();

        String fieldName = this.getFieldName(colNum);

        if (fieldName.equals(MyPatientListConstants.ACTIONS)) {

            long patientId = record.getAttributeAsLong(MyPatientListConstants.PATIENT_ID);
            long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();

            String firstName = record.getAttributeAsString(MyPatientListConstants.FIRST_NAME);
            String lastName = record.getAttributeAsString(MyPatientListConstants.LAST_NAME);

            HLayout recordCanvas = null;

            //
            // Do not show carebook icon when client implementation doesn't have carebook subscription 
            // (based on property setting for carebook (same property that drives visibility of carebook 
            // patient search ribbon)
            //
            if (patientSearchEnabled) {
                recordCanvas = new HLayout(PATIENT_SEARCH_CANVAS_SIZE);
            } else {
                recordCanvas = new HLayout(NON_PATIENT_SEARCH_CANVAS_SIZE);
            }

            recordCanvas.setHeight(22);
            recordCanvas.setWidth100();
            recordCanvas.setAlign(Alignment.CENTER);

            ImgButton carePlanImg = createGridButton("mypatientlist/careplan-list-grid-image-v2.png", "Careplan");
            carePlanImg.addClickHandler(new CarePlanClickHandler(userId, patientId, communityId, firstName, lastName));

            ImgButton enrollmentImg = createGridButton("mypatientlist/enrollment-list-grid-image-v2.png", "Enrollment");
            enrollmentImg.addClickHandler(new EnrollmentClickHandler(userId, patientId, communityId, firstName, lastName));

            ImgButton removeImage = createGridButton("mypatientlist/remove.png", "Remove");
            removeImage.addClickHandler(new RemoveClickHandler(record, userId, patientId, communityId, firstName, lastName));

            //
            // Do not show carebook icon when client implementation doesn't have carebook subscription 
            // (based on property setting for carebook (same property that drives visibility of carebook 
            // patient search ribbon)
            //
            if (patientSearchEnabled) {
                ImgButton careBookImg = createGridButton("mypatientlist/carebook-list-grid-image-v2.png", "Carebook");
                careBookImg.addClickHandler(new CareBookClickHandler(userId, patientId, communityId, firstName, lastName));
                recordCanvas.addMember(careBookImg);
            }

            recordCanvas.addMember(carePlanImg);
            recordCanvas.addMember(enrollmentImg);
            recordCanvas.addMember(removeImage);

            return recordCanvas;

        } else {
            return null;
        }
    }

    protected ImgButton createGridButton(String imageSrc, String prompt) {
        ImgButton theImageButton = new ImgButton();

        theImageButton.setSrc(imageSrc);
        theImageButton.setPrompt(prompt);

        theImageButton.setShowDown(false);
        theImageButton.setShowRollOver(false);
        theImageButton.setAlign(Alignment.CENTER);
        theImageButton.setHeight(16);
        theImageButton.setWidth(16);

        return theImageButton;
    }

    // Redux
    public void handleEnrollmentClick(final long userId, final long patientId, final long communityId, final String firstName, final String lastName) {
        // check consent
        myPatientListService.hasConsent(userId, patientId, communityId, new AsyncCallback<Boolean>() {

            @Override
            public void onSuccess(Boolean result) {

                boolean hasConsent = result.booleanValue();
                boolean powerUser = isPowerUser();

                if (hasConsent || powerUser) {
                    //
                    // go directly to the patient view in the enrollment app
                    //

                    EnrollmentPanel enrollmentPanel = new EnrollmentPanel();
                    enrollmentPanel.removeMainContent();

                    ViewPatientPanel viewPatientPanel = new ViewPatientPanel();
                    enrollmentPanel.addMember(viewPatientPanel);

                    WindowManager.updateWindow(DashboardTitles.ENROLLMENT, enrollmentPanel);

                    // call myPatientListService.getPatient(patientId, commId);
                    GetPersonCallback getPersonCallback = new GetPersonCallback(patientId, communityId, viewPatientPanel);
                    getPersonCallback.attempt();
                } else {
                    SC.warn("You no longer have consent for this patient: " + firstName + " " + lastName + ".", new BooleanCallback() {
                        @Override
                        public void execute(Boolean value) {
                            refreshMyPatientListGui();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Throwable exc) {
                SC.warn("Error checking for patient consent.");
            }

        });
    }
    
    class EnrollmentClickHandler implements ClickHandler {

        private long patientId;
        private long communityId;
        private long userId;
        private String firstName;
        private String lastName;

        EnrollmentClickHandler(long theUserId, long thePatientId, long theCommunityId, String theFirstName, String theLastName) {
            userId = theUserId;
            patientId = thePatientId;
            communityId = theCommunityId;
            firstName = theFirstName;
            lastName = theLastName;
        }

        public void onClick(ClickEvent event) {
            // Redux - Need to functionalize this for manual RPC...
            handleEnrollmentClick(userId, patientId, communityId, firstName, lastName);
        }
    }

    class GetPersonCallback extends RetryActionCallback<PersonDTO> {

        long patientId;
        long communityId;
        ViewPatientPanel viewPatientPanel;

        GetPersonCallback(long thePatientId, long theCommunityId, ViewPatientPanel theViewPatientPanel) {
            patientId = thePatientId;
            communityId = theCommunityId;
            viewPatientPanel = theViewPatientPanel;
        }

        @Override
        public void attempt() {
            myPatientListService.getPatient(patientId, communityId, this);
        }

        @Override
        public void onCapture(PersonDTO thePerson) {
            viewPatientPanel.populate(thePerson);
        }
    }

    class CareBookClickHandler implements ClickHandler {

        private long patientId;
        private long communityId;
        private long userId;
        private String firstName;
        private String lastName;

        CareBookClickHandler(long theUserId, long thePatientId, long theCommunityId, String theFirstName, String theLastName) {
            userId = theUserId;
            patientId = thePatientId;
            communityId = theCommunityId;
            firstName = theFirstName;
            lastName = theLastName;
        }

        public void onClick(ClickEvent event) {

            // check consent
            //
            myPatientListService.hasConsent(userId, patientId, communityId, new AsyncCallback<Boolean>() {
                
                @Override
                public void onSuccess(Boolean result) {

                    boolean hasConsent = result.booleanValue();
                    boolean powerUser = isPowerUser();
                    
                    if (hasConsent || powerUser) {

                        //
                        // open carebook app for given patient id
                        //

                        // get some info from login result
                        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
                        String tokenId = loginResult.getToken();
                        String email = loginResult.getEmail();

                        // construct carebook URL
                        String careBookUrl = ApplicationContextUtils.getCarebookURL();
                        careBookUrl += "?patientId=" + patientId + "&userId=" + email + "&communityId=" + communityId + "&tokenId=" + tokenId;
                        String features = "location=no,status=yes,scrollbars=yes,width=1200,height=800,resizable=yes";

                        /*
                         * Internet Explorer does not like spaces in window name when calling window.open(), therefore removed spaces from name.
                         */
                        String name = "CareBook";

                        // open carebook in a new window
                        com.google.gwt.user.client.Window.open(careBookUrl, name, features);
                    } else {
                        SC.warn("You no longer have consent for this patient: " + firstName + " " + lastName + ".", new BooleanCallback() {
                            @Override
                            public void execute(Boolean value) {
                                refreshMyPatientListGui();
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Throwable exc) {
                    SC.warn("Error checking for patient consent.");
                }
            });

        }
    }

    class CarePlanClickHandler implements ClickHandler {

        private long patientId;
        private long communityId;
        private long userId;
        private String firstName;
        private String lastName;

        CarePlanClickHandler(long theUserId, long thePatientId, long theCommunityId, String theFirstName, String theLastName) {
            userId = theUserId;
            patientId = thePatientId;
            communityId = theCommunityId;
            firstName = theFirstName;
            lastName = theLastName;
        }

        public void onClick(ClickEvent event) {

            // check consent
            //
            myPatientListService.hasConsent(userId, patientId, communityId, new AsyncCallback<Boolean>() {
                
                @Override
                public void onSuccess(Boolean result) {

                    boolean hasConsent = result.booleanValue();
                    boolean powerUser = isPowerUser();
                    
                    if (hasConsent || powerUser) {

                        //
                        // Open TREAT app.  TODO: send patient id to TREAT
                        //

                        LoginResult loginResult = ApplicationContextUtils.getLoginResult();

                        String treatUrl = loginResult.getTreatUrl();
                        final String treatName = loginResult.getTreatName();
                        final String treatFeatures = loginResult.getTreatFeatures();
                        final boolean treatRestfulSSOEnabled = loginResult.isTreatRestfulSSOEnabled();

                        if (treatRestfulSSOEnabled) {
                            //change url settings to use  POST to send tokenid to Messages App
                            String protocol = com.google.gwt.user.client.Window.Location.getProtocol();
                            String hostAndPort = com.google.gwt.user.client.Window.Location.getHost();

                            String currentPage = com.google.gwt.user.client.Window.Location.getPath();

                            String path = "";
                            if (currentPage.startsWith("/dashboard")) {
                                path = "/dashboard";
                            }

                            String patientIdHttpParamName = com.gsihealth.dashboard.common.Constants.PATIENT_ID_HTTP_PARAM_NAME;
                            
                            path += "/login/post_for_treat.jsp?" + patientIdHttpParamName + "=" + patientId;
                            String queryString = com.google.gwt.user.client.Window.Location.getQueryString();
                            treatUrl = protocol + "//" + hostAndPort + path + queryString;
                        }

                        com.google.gwt.user.client.Window.open(treatUrl, treatName, treatFeatures);

                    } else {
                        SC.warn("You no longer have consent for this patient: " + firstName + " " + lastName + ".", new BooleanCallback() {
                            @Override
                            public void execute(Boolean value) {
                                refreshMyPatientListGui();
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Throwable exc) {
                    SC.warn("Error checking for patient consent.");
                }
            });

        }
    }

    class RemoveClickHandler implements ClickHandler {

        private ListGridRecord myRecord;
        private long patientId;
        private long communityId;
        private long userId;
        private String firstName;
        private String lastName;

        RemoveClickHandler(ListGridRecord theRecord, long theUserId, long thePatientId, long theCommunityId, String theFirstName, String theLastName) {
            myRecord = theRecord;
            userId = theUserId;
            patientId = thePatientId;
            communityId = theCommunityId;
            firstName = theFirstName;
            lastName = theLastName;
        }

        @Override
        public void onClick(ClickEvent clickEvent) {

            // check consent
            //
            myPatientListService.hasConsent(userId, patientId, communityId, new AsyncCallback<Boolean>() {
                
                @Override
                public void onSuccess(Boolean result) {

                    boolean hasConsent = result.booleanValue();
                    boolean powerUser = isPowerUser();
                    
                    if (hasConsent || powerUser) {

                        String message = "Are you sure you want to remove patient: " + firstName + " " + lastName + "?"
                                + "\nPlease note that this does not remove patient from the system.";

                        SC.ask("Remove Patient from My Patient List", message, new RemoveConfirmationCallback(myRecord, patientId, communityId));
                        
                    } else {
                        SC.warn("You no longer have consent for this patient: " + firstName + " " + lastName + ".", new BooleanCallback() {
                            @Override
                            public void execute(Boolean value) {
                                refreshMyPatientListGui();
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Throwable exc) {
                    SC.warn("Error checking for patient consent.");
                }
            });
        }
    }

    class RemoveConfirmationCallback implements BooleanCallback {

        ListGridRecord myRecord;
        long patientId;
        long communityId;

        RemoveConfirmationCallback(ListGridRecord theRecord, long thePatientId, long theCommunityId) {
            myRecord = theRecord;
            patientId = thePatientId;
            communityId = theCommunityId;
        }

        @Override
        public void execute(Boolean value) {

            boolean confirmed = value.booleanValue();

            if (confirmed) {

                LoginResult loginResult = ApplicationContextUtils.getLoginResult();
                long userId = loginResult.getUserId();

                MyPatientInfoDTO myPatientInfoDTO = new MyPatientInfoDTO(userId, patientId, communityId);

                // make remote call
                myPatientListService.delete(myPatientInfoDTO, new RemovePatientCallback(myRecord));

            }
        }
    }

    class RemovePatientCallback implements AsyncCallback {

        ListGridRecord myRecord;

        RemovePatientCallback(ListGridRecord theRecord) {
            myRecord = theRecord;
        }

        @Override
        public void onSuccess(Object t) {
            refreshMyPatientListGui();
        }

        @Override
        public void onFailure(Throwable thrwbl) {
            SC.warn("Error removing patient.");
        }
    }

    // Modified for Redux...
    protected void refreshMyPatientListGui() {
        // refresh gui

        // TODO: Deprecated
//        MyPatientListPanel myPatientListPanel = (MyPatientListPanel) ApplicationContext.getInstance().getAttribute(Constants.MY_PATIENT_LIST_PANEL_KEY);
//        myPatientListPanel.loadMyPatientList();
    }

    private boolean isPowerUser() {
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        
        long accessLevelId = loginResult.getAccessLevelId();
        
        return AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID == accessLevelId; 
    }
    
    // Redux - exports functions callable by Angular...
    public static native void exportJSNI()/*-{
       // $wnd.GWTplEnrollment    = @com.gsihealth.dashboard.client.mypatientlist.MyPatientListGrid::handleEnrollmentClick(*);
            
    }-*/;

}
