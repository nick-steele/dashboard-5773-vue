package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.common.PayerPlanSearchCriteria;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import java.util.LinkedHashMap;

/**
 *
 * @author Chad Darby
 */
public interface PayerPlanAdminServiceAsync {
    
    public void getTotalCount(AsyncCallback<Integer> callback);
    
    public void getTotalCount(PayerPlanSearchCriteria searchCriteria, AsyncCallback<Integer> callback);

    public void findPayerPlans(int pageNumber, int pageSize, AsyncCallback<SearchResults<PayerPlanDTO>> callback);

    public void findPayerPlans(PayerPlanSearchCriteria searchCriteria, int pageNumber, int pageSize, AsyncCallback<SearchResults<PayerPlanDTO>> callback);
    
    public void getPayerPlans(AsyncCallback<LinkedHashMap<String, String>> callback);
  
    public void addPayerPlan(PayerPlanDTO payerPlan, AsyncCallback<Void> callback);

    public void updatePayerPlan(PayerPlanDTO payerPlan, AsyncCallback<Void> callback);

    public void hasPatientsAssignedToThisPayerPlan(PayerPlanDTO payerPlan, AsyncCallback<Boolean> callback);
    
    public void deletePayerPlan(PayerPlanDTO payerPlan, AsyncCallback<Void> callback);
    
}
