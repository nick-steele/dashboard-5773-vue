package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderAssignedPatientDTO;
import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderDTO;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
@RemoteServiceRelativePath("nonhealthhomeproviderservice")
public interface NonHealthHomeProviderService extends RemoteService {
 
    //public SearchResults<NonHealthHomeProviderDTO> getProviders(long patientId, long communityId, int pageNumber, int pageSize) throws PortalException;

    public int getTotalCountOfProviders(long patientId) throws PortalException;
    
    public void addProvider(NonHealthHomeProviderDTO provider, long patientId,long communityId) throws PortalException;

    public void updateProvider(NonHealthHomeProviderDTO provider, boolean checkForDuplicateEmail, boolean checkForDuplicateNPI,long communityId) throws PortalException;
        
    public void removeProvider(NonHealthHomeProviderDTO provider,long communityId) throws PortalException;
    
    public List<NonHealthHomeProviderDTO> getProviderList(long patientId, long communityId) throws PortalException;
    
    public List<NonHealthHomeProviderDTO> getProvidersByCommunityId(long communityId, long patientId) throws PortalException;
    
    public int getTotalCountOfProvidersByCommunityId(long communityId) throws PortalException;
    
    public void assignProvider(NonHealthHomeProviderAssignedPatientDTO assignedPatientDTO) throws PortalException;
    
    public void unAssignProvider(NonHealthHomeProviderAssignedPatientDTO assignedPatientDTO) throws PortalException;
    
    public int getCountofPatientsAssignedToProvider(long providerId, long communityId) throws PortalException;
 
    public List<NonHealthHomeProviderDTO> getProvidersByCommunityId(long communityId) throws PortalException;
}
