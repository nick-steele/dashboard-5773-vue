/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.AlertSearchResults;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.entity.dto.UserAlertDTO;
import java.util.Date;
import java.util.List;
import javax.naming.directory.SearchResult;

/**
 * Provides access to alerts
 * 
 * @author Chad Darby
 */
@RemoteServiceRelativePath("alertservice")
public interface AlertService extends RemoteService {

    /**
     * Get the total count of alerts for this user
     * 
     * @param userId
     * @return
     * @throws PortalException 
     */
    public int getTotalCount(long userId) throws PortalException;
    
    /**
     * Get user alerts. Has the total count and only content for the first page size
     * 
     * @param userId
     * @return
     * @throws PortalException 
     */
    public SearchResults<UserAlertDTO> getUserAlertsPartial(long userId) throws PortalException;

    /**
     * Get user alerts
     * 
     * @param userId
     * @return
     * @throws PortalException 
     */
    public SearchResults<UserAlertDTO> getUserAlerts(long userId, int pageNumber, int pageSize) throws PortalException;
    
    /**
     * Update user alert
     * 
     * @param userAlertDTO
     * @throws PortalException 
     */
    public void updateUserAlert(UserAlertDTO userAlertDTO) throws PortalException;

    /**
     * Update user alerts
     * 
     * @param userAlertDTOs
     * @throws PortalException 
     */
    public void updateUserAlerts(List<UserAlertDTO> userAlertDTOs) throws PortalException;

    /**
     * Search for user alerts
     * 
     * @param fieldKey
     * @param searchValue
     * @return
     * @throws PortalException 
     */
    public SearchResults<UserAlertDTO> searchUserAlertsByPatientFirstName(long userId, String searchValue, int pageNumber, int pageSize) throws PortalException;

    /**
     * 
     * @param userId
     * @param searchValue
     * @param pageNumber
     * @param pageSize
     * @return
     * @throws PortalException 
     */
    public SearchResults<UserAlertDTO> searchUserAlertsByPatientId(long userId, String searchValue, int pageNumber, int pageSize) throws PortalException;
    
    /**
     * Search for user alerts
     * 
     * @param fieldKey
     * @param searchValue
     * @return
     * @throws PortalException 
     */
    public SearchResults<UserAlertDTO> searchUserAlertsByPatientLastName(long userId, String searchValue, int pageNumber, int pageSize) throws PortalException;

    /**
     * Search for user alerts
     * 
     * @param fieldKey
     * @param searchValue
     * @return
     * @throws PortalException 
     */
    public SearchResults<UserAlertDTO> searchUserAlertsByApplication(long userId, String searchValue, int pageNumber, int pageSize) throws PortalException;
    
    /**
     * Search for user alerts
     * 
     * @param fieldKey
     * @param searchValue
     * @return
     * @throws PortalException 
     */
    public SearchResults<UserAlertDTO> searchUserAlertsBySeverity(long userId, String searchValue, int pageNumber, int pageSize) throws PortalException;
    
    /**
     * Search for user alerts
     * 
     * @param fromDate
     * @param lastDate
     * @return
     * @throws PortalException 
     */
    public SearchResults<UserAlertDTO> searchUserAlertsByDate(long userId, Date fromDate, Date lastDate, int pageNumber, int pageSize) throws PortalException;

}
