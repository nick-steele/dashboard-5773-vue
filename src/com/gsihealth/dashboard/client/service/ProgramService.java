package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.entity.dto.ProgramDTO;
import com.gsihealth.dashboard.entity.dto.ProgramSearchOptionsDTO;
import com.gsihealth.dashboard.entity.dto.ProgramStatusDTO;

import java.util.List;

@RemoteServiceRelativePath("programservice")
public interface ProgramService extends RemoteService {

    List<ProgramDTO> getSubordinatePrograms(long communityId) throws PortalException;

    List<ProgramDTO> getParentPrograms(long communityId) throws PortalException;

    List<ProgramStatusDTO> getProgramStatuses(long communityId) throws PortalException;

    ProgramSearchOptionsDTO filterProgramOptionsByParent(long communityId, long parentId) throws PortalException;
    ProgramSearchOptionsDTO filterProgramOptionsByProgram(long communityId, long programId) throws PortalException;

    List<ProgramStatusDTO> getSystemProgramStatuses(long communityId) throws PortalException;
}
