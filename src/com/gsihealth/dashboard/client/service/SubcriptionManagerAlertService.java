/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.ApplicationInfo;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.entity.dto.AlertFilterPreferenceDTO;
import com.gsihealth.dashboard.entity.dto.AlertPreferenceDTO;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Vinay
 */
@RemoteServiceRelativePath("subcriptionmanageralertservice")
public interface SubcriptionManagerAlertService  extends RemoteService{
    
    public List<AlertPreferenceDTO> getAlerts( Long communityID,Map<String, ApplicationInfo> applicationInfoMap, long user_id) throws PortalException;
    public List<AlertFilterPreferenceDTO> getAlertFilterSelections(long userId,AlertPreferenceDTO apdto, Long communityId) throws PortalException;
    public void updateUserAlertFilter(List<AlertFilterPreferenceDTO> alertFilterUsers)throws PortalException;
     
}
