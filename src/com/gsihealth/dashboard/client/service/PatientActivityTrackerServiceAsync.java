/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.PatientActivityTrackerDTO;
import java.util.List;

/**
 *
 * @author ssingh
 */
public interface PatientActivityTrackerServiceAsync {
    
    public void getPatientActivities(long communityId, long patientId, AsyncCallback<List<PatientActivityTrackerDTO>> callback);
    
    public void addUpdatePatientActivities(List<PatientActivityTrackerDTO> dtos, PersonDTO personDTO, AsyncCallback callback);
    
    public void delete(long id, long communityId, AsyncCallback callback);
}
