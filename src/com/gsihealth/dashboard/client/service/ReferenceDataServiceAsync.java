package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.ReferenceData;

/**
 *
 * @author Satyendra Singh
 */
public interface ReferenceDataServiceAsync {

    public void getClientApplicationProperties(Long communityId, AsyncCallback<ClientApplicationProperties> callback);

    public void getReferenceData(Long communityId, AsyncCallback<ReferenceData> callback);

}
