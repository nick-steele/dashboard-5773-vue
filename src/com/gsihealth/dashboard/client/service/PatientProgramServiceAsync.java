/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.entity.PatientProgramName;

/**
 *
 * @author emontgomery
 */

public interface PatientProgramServiceAsync{

    public void addProgram(Long userId, PatientProgramName entity,AsyncCallback<String> callback);

    
    public void edit( Long userId, PatientProgramName entity, boolean isAddtoHistory,AsyncCallback<String> callback);

    
    public void remove(Long id, Long userId, int reasonId,AsyncCallback<String> callback);
    
    public void sendPatientProgramInfo(PatientProgramName entity,AsyncCallback<String> callback);
    
}
