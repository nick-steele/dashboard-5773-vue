package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface TaskCountServiceAsync {

    public void getOverdueTaskCount(AsyncCallback<Integer> callback);
    
}
