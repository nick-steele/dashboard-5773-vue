package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.common.AlertSearchResults;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.entity.dto.UserAlertDTO;
import java.util.Date;
import java.util.List;
import javax.naming.directory.SearchResult;

/**
 * Provides access to alerts
 *
 * @author Chad Darby
 */
public interface AlertServiceAsync {

    /**
     * 
     * @param userId
     * @param callback 
     */
    public void getTotalCount(long userId, AsyncCallback<Integer> callback);
    
    /**
     * Get the user alerts
     * 
     * @param userId
     * @param callback 
     */
    public void getUserAlertsPartial(long userId, AsyncCallback<SearchResults<UserAlertDTO>> callback);

    
    /**
     * Get the user alerts
     * 
     * @param userId
     * @param callback 
     */
    public void getUserAlerts(long userId, int pageNumber, int pageSize, AsyncCallback<SearchResults<UserAlertDTO>> callback);

        /**
     * Update an alert
     * 
     * @param userAlertDTO
     * @param callback 
     */
    public void updateUserAlert(UserAlertDTO userAlertDTO, AsyncCallback callback);

    /**
     * Update a collection of alerts
     * 
     * @param userAlertDTOs
     * @param callback 
     */
    public void updateUserAlerts(List<UserAlertDTO> userAlertDTOs, AsyncCallback callback);

    /**
     * Search for user alerts
     * 
     * @param fieldKey
     * @param searchValue
     * @return
     * @throws PortalException 
     */
    public void searchUserAlertsByPatientFirstName(long userId, String searchValue, int pageNumber, int pageSize, AsyncCallback<SearchResults<UserAlertDTO>> callback);

    /**
     * 
     * @param userId
     * @param searchValue
     * @param pageNumber
     * @param pageSize
     * @param callback 
     */
    public void searchUserAlertsByPatientId(long userId, String searchValue, int pageNumber, int pageSize, AsyncCallback<SearchResults<UserAlertDTO>> callback);
    
    /**
     * 
     * @param userId
     * @param searchValue
     * @param callback 
     */
    public void searchUserAlertsByPatientLastName(long userId, String searchValue, int pageNumber, int pageSize, AsyncCallback<SearchResults<UserAlertDTO>> callback);

    /**
     * 
     * @param userId
     * @param searchValue
     * @param callback 
     */
    public void searchUserAlertsByApplication(long userId, String searchValue, int pageNumber, int pageSize, AsyncCallback<SearchResults<UserAlertDTO>> callback);

    /**
     * 
     * @param userId
     * @param searchValue
     * @param callback 
     */
    public void searchUserAlertsBySeverity(long userId, String searchValue, int pageNumber, int pageSize, AsyncCallback<SearchResults<UserAlertDTO>> callback);

    /**
     * Search for user alerts
     * 
     * @param fromDate
     * @param lastDate
     * @return
     * @throws PortalException 
     */
    public void searchUserAlertsByDate(long userId, Date fromDate, Date lastDate, int pageNumber, int pageSize, AsyncCallback<SearchResults<UserAlertDTO>> callback);

    
}
