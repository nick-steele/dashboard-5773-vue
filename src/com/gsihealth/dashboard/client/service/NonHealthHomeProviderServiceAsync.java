package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderAssignedPatientDTO;
import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderDTO;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public interface NonHealthHomeProviderServiceAsync {
    
    //public void getProviders(long patientId, long communityId, int pageNumber, int pageSize, AsyncCallback<SearchResults<NonHealthHomeProviderDTO>> callback);

    public void getTotalCountOfProviders(long patientId, AsyncCallback<Integer> callback);
    
    public void addProvider(NonHealthHomeProviderDTO provider, long patientId, long communityId,AsyncCallback callback);

    public void updateProvider(NonHealthHomeProviderDTO provider, boolean checkForDuplicateEmail, boolean checkForDuplicateNPI,long communityId, AsyncCallback callback);
        
    public void removeProvider(NonHealthHomeProviderDTO provider, long communityId,AsyncCallback callback);
    
    public void getProviderList(long patientId, long communityId, AsyncCallback<List<NonHealthHomeProviderDTO>> callback);
    
    public void getProvidersByCommunityId(long communityId, long patientId, AsyncCallback<List<NonHealthHomeProviderDTO>> callback);
    
    public void getTotalCountOfProvidersByCommunityId(long communityId, AsyncCallback<Integer> callback);
    
    public void assignProvider(NonHealthHomeProviderAssignedPatientDTO assignedPatientDTO, AsyncCallback callback);
    
    public void unAssignProvider(NonHealthHomeProviderAssignedPatientDTO assignedPatientDTO, AsyncCallback callback);
    
    public void getCountofPatientsAssignedToProvider(long providerId, long communityId, AsyncCallback<Integer> callback);
    
    public void getProvidersByCommunityId(long communityId, AsyncCallback<List<NonHealthHomeProviderDTO>> callback);
}
