package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.PatientProgramDTO;
import java.util.List;

/**
 *
 * @author ssingh
 */
public interface ManagePatientProgramServiceAsync {

    public void getPatientProgramList(long patientId, long communityId, int pageNumber, int pageSize, AsyncCallback<SearchResults<PatientProgramDTO>> callback);

    public void getTotalCountOfPrograms(long patientId, long communityId, AsyncCallback<Integer> callback);

    //JIRA 1024
    public void getTotalCountOfActivePrograms(long patientId, long communityId, AsyncCallback<Integer> callback);
    
    public void addPatientProgram(List<PatientProgramDTO> dtos, PersonDTO personDTO, AsyncCallback callback);

    public void deletePrograms(List<PatientProgramDTO> dtos, PersonDTO personDTO, AsyncCallback callback);

    public void addPatientProgramFromPatientLoader(LoginResult loginResult, PatientProgramDTO patientProgramDTO, PersonDTO personDTO, AsyncCallback callback);
}
