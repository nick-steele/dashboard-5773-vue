/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PatientOtherIdsDTO;
import java.util.LinkedHashMap;

/**
 *
 * @author beth.boose
 */
public interface PatientOtherIdsServiceAsync {

    public void getPatientOtherIds(long patientId, int pageNumber, int pageSize,long communityId, AsyncCallback<SearchResults<PatientOtherIdsDTO>> callback);

    public void getTotalCountOfOtherIds(long patientId,long communityId, AsyncCallback<Integer> callback);

    public void addPatientOtherId(PatientOtherIdsDTO patientOtherIdsDTO, AsyncCallback callback);

    public void updatePatientOtherId(PatientOtherIdsDTO patientOtherIdsDTO, boolean checkForDuplicates, AsyncCallback callback);

    public void deletePatientOtherId(PatientOtherIdsDTO patientOtherIdsDTO, AsyncCallback callback);

    public void getOrganizations(long communityId,AsyncCallback<LinkedHashMap<String, String>> callback);

    public void isDuplicate(PatientOtherIdsDTO patientOtherIdsDTO, AsyncCallback<Boolean> callback);
}
