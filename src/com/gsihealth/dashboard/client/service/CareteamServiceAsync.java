package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.common.ManageCareTeamFormData;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchPatientResults;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.common.dto.enrollment.PatientFormReferenceData;
import com.gsihealth.dashboard.entity.dto.CareTeamDTO;
import com.gsihealth.dashboard.entity.dto.CommunityCareteamDTO;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public interface CareteamServiceAsync {

    public void getCareteamNames(Long communityId, AsyncCallback<LinkedHashMap<String, String>> callback);

    public void countPatients(long communityId,
            SearchCriteria searchCriteria,
            boolean enrolledMode,
            Long currentUserOrgId,
            long userId, AsyncCallback<SearchPatientResults> callback);
    
        public void findPatients(
            long communityId,
            SearchCriteria searchCriteria,
            boolean enrolledMode,
            Long currentUserOrgId,
            int pageNumber,
            int pageSize,
            long userId, 
            AsyncCallback<SearchResults<PersonDTO>> callback);

    public void getCareteamForPatient(long patientId, AsyncCallback<CommunityCareteamDTO> callback);

    //JIRA 1178 - Spira 5201
    public void getCareteam(String careteamName, long communityId, AsyncCallback<CommunityCareteamDTO> callback);

    //public void savePatientInfo(PatientInfo patientInfo,boolean checkEnableCareteam,boolean checkPatientUserActive, AsyncCallback callback);
    public void savePatientInfo(PersonDTO personDTO, PatientInfo patientInfo,boolean checkEnableCareteam,boolean checkPatientUserActive, long communityId, AsyncCallback callback);

     public void getUsersForCareteam(long communityCareteamId, AsyncCallback<CareTeamDTO> callback);

    public void getUsersForCareteam(long communityCareteamId, long patientId, long patientOrgId,  AsyncCallback<CareTeamDTO> callback);

    public void getUsersForCareteamWithoutPermitConsent(long communityCareteamId, long patientId, long patientOrgId, AsyncCallback<List<UserDTO>> callback);

    public void saveCareteam(long communityCareteamId, String careteamName, List<UserDTO> users,long communityId, AsyncCallback callback);

    public void saveNewCareteam(String careteamName, long communityId, List<UserDTO> users, AsyncCallback callback);

    public void hasPatientsAssignedToCareteam(long communityCareteamId, AsyncCallback<Boolean> callback);

    public void hasOtherPatientsAssignedToCareteam(long communityCareteamId, long patientId, AsyncCallback<Boolean> callback);

    public void getAvailableUsers(String role, List<Long> userIdsToExclude, AsyncCallback<List<UserDTO>> callback);

    public void getAvailableUsers(AsyncCallback<List<UserDTO>> callback);
    
    public void getConsenterTable(long communityId, AsyncCallback<LinkedHashMap<String, String>> callback);

    public void getTotalCountForAvailableUsers(String role, List<Long> userIdsToExclude, AsyncCallback<Integer> callback);

    public void getManageCareTeamFormData(long communityId,AsyncCallback<ManageCareTeamFormData> callback);

    public void getPatientFormReferenceData(long communityId,AsyncCallback<PatientFormReferenceData> callback);
    
// JIRA - 1193 ---Satyendra--- this method will no longer needed to search patient for carebook. Common searching method is being used now.
//    public void findDashboardPatient(long communityId, long userId, SearchCriteria searchCriteria, AsyncCallback<List<PersonDTO>> callback);
    
    public void findTotalNoOfUser(long communityId, SearchCriteria searchCriteria, long userId, AsyncCallback<Integer> callback);
    
    public void getOrganizationDTOs(long communityId,
            AsyncCallback<List<OrganizationDTO>> callback);

    public void performPatientEngagement(Long communityId,
            Long patientId, boolean directMessagngRequired,
            AsyncCallback<String> callback);
    
    public void updatePatientBySelfAssertion(PersonDTO thePerson, AsyncCallback<PersonDTO> asyncCallback);
}
