/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import java.util.LinkedHashMap;

/**
 *
 * @author Satyendra Singh
 */
@RemoteServiceRelativePath("organizationservice")
public interface OrganizationService extends RemoteService {
    
    /**
     * Add the Organization
     * 
     * @param orgDTO
     * @throws PortalException
     */
    public void addOrg(long communiyId, OrganizationDTO orgDTO, String token) throws PortalException;

    /**
     * Update the Organization
     * 
     * @param orgDTO
     * @throws PortalException
     */
    public void updateOrg(long communityId, OrganizationDTO orgDTO, String token) throws PortalException;
    
    /**
     * Get the list of Organizations
     *
     * @param 
     * @return
     * @throws PortalException
     */
    public SearchResults<OrganizationDTO> getOrgs(long communityId, int pageNumber, int pageSize) throws PortalException;

    /**
     * Get total count of organizations
     * 
     * @return
     * @throws PortalException 
     */
    public int getTotalCount(long communityId) throws PortalException;
    
    /**
     * Delete the Organization
     * 
     * @param orgDTO
     * @param callback
     */
    public void deleteOrg(OrganizationDTO userDTO, String token) throws PortalException;
    
    /**
     * Get all Organization
     * 
     * @param 
     * @param callback
     */
   public LinkedHashMap<String, String> getOrganizationNames(long communityId) throws PortalException;
   
   public LinkedHashMap<String, String> getOrganizationNamesForSearchForm(long communityId) throws PortalException;

   public String getOrganizationName(long orgId) throws PortalException;

   public SearchResults<OrganizationDTO> findCandidates(long communityId,SearchCriteria searchCriteria) throws PortalException;
   
   public int findTotalNoOrgs(long communityId,SearchCriteria searchCriteria) throws PortalException;
   
   public int getTotalCount(long communityId, int pageNumber, int pageSize)throws PortalException;

}
