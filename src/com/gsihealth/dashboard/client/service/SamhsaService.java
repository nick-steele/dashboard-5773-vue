
package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.PortalException;

/**
 *
 * @author rsharan
 */
@RemoteServiceRelativePath("samhsaservice")
public interface SamhsaService extends RemoteService {
    
    public String getActiveSamhsaContent(Long communityId) throws PortalException;

//    public void updateUserSamhsaStatus(String email, boolean flag) throws PortalException;
    
     public void updateUserSamhsaStatus(String email, Long community, boolean flag) throws PortalException;
    
}




