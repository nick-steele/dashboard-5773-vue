/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.common.ApplicationInfo;
import com.gsihealth.dashboard.entity.dto.AlertFilterPreferenceDTO;
import com.gsihealth.dashboard.entity.dto.AlertPreferenceDTO;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Vinay
 */
public interface SubcriptionManagerAlertServiceAsync {
    public void getAlerts(Long communityID,Map<String, ApplicationInfo> applicationInfoMap, long user_id, AsyncCallback<List<AlertPreferenceDTO>> callback);
    public void getAlertFilterSelections(long userId,AlertPreferenceDTO apdto, Long communityId, AsyncCallback<List<AlertFilterPreferenceDTO>> callback);
    public void updateUserAlertFilter(List<AlertFilterPreferenceDTO> alertFilterUsers, AsyncCallback callback);
   
}
