package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.entity.dto.LoginResult;

/**
 * Async interface for security service
 * 
 */
public interface SecurityServiceAsync {

    public void forgotPassword(String portalUserId,long communityId, AsyncCallback<Boolean> callback);

    /**
     * Change the user's password
     *
     * @param portalUserId
     * @param password
     */
    public void changePassword(String portalUserId, String password, AsyncCallback<String> callback);


    /**
     * Authenticate the claimToken
     *
     * @param claimToken
     * @return LoginResult
     */
    void authenticate(String claimToken, AsyncCallback<LoginResult> async);

    /**
     * Authenticate user id and password
     * 
     * @param portalUserId
     * @param portalPassword
     * @param callback
     */
    public void authenticate(String portalUserId, String portalPassword, Long communityId,String token, AsyncCallback<LoginResult> callback);
    
    //JIRA 1122
    public void logout(String portalUserId, String tokenId, Long communityId, AsyncCallback<Void> callback);

}
