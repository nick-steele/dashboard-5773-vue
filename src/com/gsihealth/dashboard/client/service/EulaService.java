package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.PortalException;

/**
 *
 * @author Chad Darby
 */
@RemoteServiceRelativePath("eulaservice")
public interface EulaService extends RemoteService {

    public String getActiveEulaContent(Long communityId) throws PortalException;

    public void updateUserEulaStatus(String email, Long community, boolean flag) throws PortalException;
}
