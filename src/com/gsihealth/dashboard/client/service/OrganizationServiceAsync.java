/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import java.util.LinkedHashMap;

/**
 *
 * @author User
 */
public interface OrganizationServiceAsync extends BasePortalServiceAsync {

    public void addOrg(long communityId, OrganizationDTO orgDTO, String token, AsyncCallback callback);

    public void updateOrg(long communityId, OrganizationDTO orgDTO, String token, AsyncCallback callback);

    public void getOrgs(long communityId, int pageNumber, int pageSize, AsyncCallback<SearchResults<OrganizationDTO>> callback);

    public void getTotalCount(long communityId, AsyncCallback<Integer> callback);
    
    public void deleteOrg(OrganizationDTO orgDTO, String token, AsyncCallback callback);

    /**
     * Get a collection of organization names and ids
     *
     * @return
     */
    public void getOrganizationNames(long communityId,AsyncCallback<LinkedHashMap<String, String>> callback);
    
    public void getOrganizationNamesForSearchForm(long communityId,AsyncCallback<LinkedHashMap<String, String>> callback);

    public void getOrganizationName(long orgId, AsyncCallback<String> callback);
    
    public void findCandidates(long communityId, SearchCriteria searchCriteria ,AsyncCallback<SearchResults<OrganizationDTO>> callback);
    
    public void findTotalNoOrgs(long communityId,SearchCriteria searchCriteria, AsyncCallback<Integer> callback);
    
     public void getTotalCount(long communityId, int pageNumber, int pageSize,AsyncCallback<Integer> callback);
}
