package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.UserFormReferenceData;
import com.gsihealth.dashboard.entity.dto.CommunityDTO;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author User
 */
public interface AdminServiceAsync extends BasePortalServiceAsync {

    /**
     * Add user
     * 
     * @param userDTO
     * @param userApps
     * @param callback 
     */
    public void addUser(UserDTO userDTO, List<String> userApps,boolean enableClientMfa,String authyApiKey,long communityID,boolean isThroughUserLoader,AsyncCallback callback);

    /**
     * Update user
     */
    public void updateUser(UserDTO userDTO, List<String> userApps, boolean checkForExistingEmailAddress,boolean enableClientMfa,String authyApiKey,long communityId,boolean isThroughUserLoader,
            AsyncCallback<String> callback);
    
    public void addOrUpdateUser(UserDTO userDTO, List<String> userApps, boolean checkForExistingEmailAddress, 
            boolean isExistingUser,boolean enableClientMfa,String authyApiKey,long communityId, boolean isThroughUserLoader, AsyncCallback callback);
    
    /**
     * update user preferences
     * @param userDTO
     * @param userApps
     * @param callback 
     */
    public void updateUserPreferences (UserDTO userDTO, List<String> userApps,long communityId, 
            AsyncCallback<String> callback);

    /**
     * Get users
     */
   // public void getUsers(long accessLevelId, int pageNumber, int pageSize, long communityId,AsyncCallback<SearchResults<UserDTO>> callback);

    /**
     * Get users
     */
    public void getTotalCount(long accessLevelId,long communityId, AsyncCallback<Integer> callback);

    /**
     * 
     */
    public void getLocalAdminTotalCountForUsers(long accessLevelId,long communityId, AsyncCallback<Integer> callback);

    /**
     * Get list of apps for user
     */
    public void getUserApps(UserDTO userDTO, AsyncCallback<List<String>> callback);

    /**
     * Get list of apps for user
     */
    public void getUserApps_Legacy(UserDTO userDTO, AsyncCallback<List<String>> callback);

    /**
     * Delete the user
     * 
     * @param userDTO
     * @param callback
     */
    public void deleteUser(UserDTO userDTO, String token, AsyncCallback callback);

    /**
     * Get Access Levels
     * 
     * @return
     */
    public void getAccessLevel(long communityId,AsyncCallback<LinkedHashMap<String, String>> callback);

    /**
     * Get Roles
     * 
     * @return
     */
    public void getRoles(long communityId,AsyncCallback<LinkedHashMap<String, String>> callback);

    /**
     * Get Facility Types
     * 
     * @return
     */
    public void getFacilityTypes(long communityId, AsyncCallback<LinkedHashMap<String, String>> callback);

    /**
     * Get Organizations
     * 
     * @return
     */
    public void getOrganization(long communityId,AsyncCallback<LinkedHashMap<String, String>> callback);

    public void getOrganizationDTOs(long communityId,AsyncCallback<List<OrganizationDTO>> callback);
    
    public void getOrganizationByOid(String oid, long communityId, AsyncCallback<OrganizationDTO> callback);
    /**
     * Get user by email
     * 
     * @param email
     * @param callback 
     */
    public void getUser(String email, Long communityId, AsyncCallback<UserDTO> callback);

     /** 
     * @param email
     * @return userDTO populated with user specific data, not community stuff
     * @throws PortalException 
     */
    public void getDemographicUser(String email, Long communityId, AsyncCallback<UserDTO> callback);
    
    /**
     * Get user by id
     * 
     * @param userId
     * @param callback 
     */
    public void getUser(long userId, Long communityId, AsyncCallback<UserDTO> callback);

    public void getUserFormReferenceData(AsyncCallback<UserFormReferenceData> callback);

    public void getDaysSinceLastChange(Date old, Date now, AsyncCallback<Integer> callback);

/**
     * 
     * @param searchCriteria
     * @param accessLevelId
     * @param callback 
     */
    public void findCandidates(SearchCriteria searchCriteria, long accessLevelId,
            long communityId, int pageNumber, int pageSize, AsyncCallback<SearchResults<UserDTO>> callback);
/**
     * 
     * @param searchCriteria
     * @param accessLevelId
     * @param callback 
     */
    public void findTotalNoOfUser(SearchCriteria searchCriteria, long communityId, AsyncCallback<Integer> callback);

  

    public void getBuildVersion(AsyncCallback<String> callback);
    
    public void  getUsers(Long communityId, AsyncCallback<List<UserDTO>> callback);
    
    public void requestSms(String email,long communityId,String key,String countryCode, AsyncCallback callback);
    
    public void verifyToken(String token,String email,long communityId, String key,String countryCode,AsyncCallback callback);
 
    public void updateUserContexts(long communityId,AsyncCallback<List<UserDTO>> callback);
    
    //JIRA 727
    public void getCommunitiesForUser(Long userId ,AsyncCallback<List<CommunityDTO>> callback);

    void getSupervisors(Long communityId, String orgId, AsyncCallback<List<UserDTO>> callback);

}
