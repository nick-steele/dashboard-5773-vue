package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.PortalException;

@RemoteServiceRelativePath("taskcountservice")
public interface TaskCountService extends RemoteService {

    public int getOverdueTaskCount() throws PortalException;
}
