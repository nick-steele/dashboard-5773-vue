package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.ListTagDTO;
import com.gsihealth.dashboard.entity.dto.MyPatientInfoDTO;
import com.gsihealth.dashboard.entity.dto.PatientListLabelDTO;
import java.util.List;

/**
 *
 * @author Chad.Darby
 */
public interface MyPatientListServiceAsync {

    public void add(MyPatientInfoDTO myPatientInfo, AsyncCallback callback);

    public void update(MyPatientInfoDTO myPatientInfo, AsyncCallback callback);

    public void delete(MyPatientInfoDTO myPatientInfo, AsyncCallback callback);

    public void updateTags(long userId, long communityId, String type, String id, AsyncCallback callback) ;

    public void getMyPatientList(long userId, long communityId, AsyncCallback<List<MyPatientInfoDTO>> callback);

    public void getMyPatientListCount(long userId, long communityId,String levl, String tags, List patients,AsyncCallback<String> callback);

    public void getMyPatientListScroll(long userId, long communityId,long start, long stop,String level, String tags,List patients, AsyncCallback<List<MyPatientInfoDTO>> callback);

      public void getListTags(long communityId,AsyncCallback<List<ListTagDTO>> callback);

      public void getPatient(long patientId, long communityId, AsyncCallback<PersonDTO> callback);

    public void isAlreadyOnList(long userid, long patientId, long communityId, AsyncCallback<Boolean> callback);
    
    public void myPatientListSelectionDisplayOptions(long userid, long patientId, long communityId, AsyncCallback<PatientListLabelDTO> callback);

    public void isListFull(long userid, long communityId, AsyncCallback<Boolean> callback);

    public void hasConsent(long userid, long patientId, long communityId, AsyncCallback<Boolean> callback);

    public void getConfigurationValue(long communityId, String defaultValue, String key, String subKey,
                                        AsyncCallback<String> callback);

    public void getTagByType(long communityId, String type, AsyncCallback<ListTagDTO> callback);
}
