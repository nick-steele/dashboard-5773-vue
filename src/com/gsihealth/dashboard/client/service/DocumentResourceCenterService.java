/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.entity.dto.DocumentResourceCenterDTO;
import java.util.List;

/**
 *
 * @author User
 */
@RemoteServiceRelativePath("docresourcecenterservice")
public interface DocumentResourceCenterService extends RemoteService{
    
    public List<DocumentResourceCenterDTO> getDocuments(long communityId) throws PortalException;
    
}
