/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.PatientActivityTrackerDTO;
import java.util.List;

/**
 *
 * @author ssingh
 */
@RemoteServiceRelativePath("activitytrackerservice")
public interface PatientActivityTrackerService extends RemoteService {
    
   public List<PatientActivityTrackerDTO> getPatientActivities(long communityId, long patientId) throws PortalException;
    
   public void addUpdatePatientActivities(List<PatientActivityTrackerDTO> dtos, PersonDTO personDTO) throws PortalException;
   
    public void delete(long id, long communityId) throws PortalException;
}
