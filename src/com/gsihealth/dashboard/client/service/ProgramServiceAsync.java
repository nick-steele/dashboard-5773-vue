package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.entity.dto.ProgramDTO;
import com.gsihealth.dashboard.entity.dto.ProgramSearchOptionsDTO;
import com.gsihealth.dashboard.entity.dto.ProgramStatusDTO;

import java.util.List;

public interface ProgramServiceAsync {
    void getSubordinatePrograms(long communityId, AsyncCallback<List<ProgramDTO>> async);
    void getParentPrograms(long communityId, AsyncCallback<List<ProgramDTO>> async);
    void getProgramStatuses(long communityId, AsyncCallback<List<ProgramStatusDTO>> async);
    void getSystemProgramStatuses(long communityId, AsyncCallback<List<ProgramStatusDTO>> async);
    void filterProgramOptionsByParent(long communityId, long parentId, AsyncCallback<ProgramSearchOptionsDTO> async);
    void filterProgramOptionsByProgram(long communityId, long programId, AsyncCallback<ProgramSearchOptionsDTO> async);
}
