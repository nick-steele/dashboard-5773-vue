package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchPatientResults;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.client.common.PersonInfo;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.common.PatientLoadResult;
import com.gsihealth.patientmdminterface.entity.DuplicateCheckProcessResult;
import com.gsihealth.patientmdminterface.entity.SimplePerson;

import java.util.LinkedHashMap;

/**
 *
 * @author Chad Darby
 */
public interface EnrollmentServiceAsync {

    //JIRA 2523
    public void checkForDuplicates(SimplePerson person, Long communityId, AsyncCallback<DuplicateCheckProcessResult> callback);
    
    public void findPatient(SimplePerson sp, long communityId, Long currentUserOrgId, AsyncCallback<PersonDTO> callback);
            
    public void addPatient(PersonDTO thePerson, AsyncCallback<PersonDTO> callback);

    public void addPatient(PersonDTO thePerson, long communityCareteamId, AsyncCallback<PersonDTO> callback);

    public void updatePatientFromPatientLoader(LoginResult thePatientLoaderLoginResult, PersonDTO personDTO, boolean checkEnableCareteam, boolean checkForDuplicate, boolean checkForDuplicateSsn, boolean primaryMedicaidcareIdBln, boolean secondaryMedicaidcareIdBln, String oldProgramLevelConsentStatus, String oldEnrollmentStatus, String newCareTeamId, String oldCareTeamId, long oldOrganizationId, boolean checkPatientUserUpdate,String oldAcuityScore, AsyncCallback callback) throws PortalException;
    
    public void updatePatient(PersonDTO thePerson,boolean checkEnableCareteam, boolean checkForDuplicate, boolean checkForDuplicateSsn, boolean primaryMedicaidcareId,boolean secondaryMedicaidcareId, String oldProgramLevelConsentStatus, String oldEnrollmentStatus, String newCareTeamId, String oldCareTeamId, long oldOrganizationId,boolean checkPatientUserUpdate,String oldAcuityScore, AsyncCallback callback);
    
   public void updatePatientBySelfAssertion(PersonDTO thePerson, AsyncCallback<PersonDTO> asyncCallback);
    
    public void updatePatientCareteam(long communityCareteamId, long patientId, AsyncCallback callback);

    public void removePatientCareteam(long patientId,long communityId, AsyncCallback callback);

    public void updatePatientForCareteamAssignment(PersonDTO person,boolean checkEnableCareteam, boolean checkForDuplicate, boolean checkForDuplicateSsn, boolean primaryMedicaidcareId,boolean secondaryMedicaidcareId,String oldProgramLevelConsentStatus, String oldEnrollmentStatus, String careTeamIdNew, String careTeamIdOld, long oldOrganizationId, PatientInfo patientInfo,boolean checkPatientUserUpdate,String oldAcuityScore, AsyncCallback savePatientCallback);
    
    public void checkIfDuplicatePatient(PersonDTO personDTO,long communityId, AsyncCallback callback);
    
    public void checkIfDuplicatePatientModify(PersonDTO thePerson,boolean checkForDuplicate,long communityId, AsyncCallback callback);
    
    public void getTotalCountForZero(AsyncCallback<Integer> callback);
    
    public void findPatients(long communityId, SearchCriteria searchCriteria,
            boolean enrolledMode,Long currentUserOrgId,
            int pageNumber, int pageSize,
            long userId, AsyncCallback<SearchResults<PersonDTO>> callback);
    
    public void findPatientsWithNoService(SearchResults<PersonDTO> results, AsyncCallback<SearchResults<PersonDTO>> callback);
    
    public void countPatients(long communityId, 
            SearchCriteria searchCriteria, 
            boolean enrolledMode, 
            Long currentUserOrgId, 
            long userId,
            AsyncCallback<SearchPatientResults> callback);

	public void parseStringToPerson(String data, AsyncCallback<PersonInfo> callback);
	public void getCounty(long communityId,AsyncCallback<LinkedHashMap<String, String>> callback);
}
