package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.PayerPlanSearchCriteria;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import java.util.LinkedHashMap;

/**
 *
 * @author Chad Darby
 */
@RemoteServiceRelativePath("payerplanadminservice")
public interface PayerPlanAdminService extends RemoteService {
    
    public int getTotalCount() throws PortalException;

    public int getTotalCount(PayerPlanSearchCriteria searchCriteria) throws PortalException;
    
    public SearchResults<PayerPlanDTO> findPayerPlans(int pageNumber, int pageSize) throws PortalException;

    public SearchResults<PayerPlanDTO> findPayerPlans(PayerPlanSearchCriteria searchCriteria, int pageNumber, int pageSize) throws PortalException;
    
    public LinkedHashMap<String, String> getPayerPlans() throws PortalException;    
    
    public void addPayerPlan(PayerPlanDTO payerPlan) throws PortalException;

    public void updatePayerPlan(PayerPlanDTO payerPlan) throws PortalException;

    public boolean hasPatientsAssignedToThisPayerPlan(PayerPlanDTO payerPlan) throws PortalException;
    
    public void deletePayerPlan(PayerPlanDTO payerPlan) throws PortalException;
    
}
