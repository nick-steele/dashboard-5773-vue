package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.ReferenceData;

/**
 * Reads reference data from the server
 * 
 * @author Chad Darby
 */
@RemoteServiceRelativePath("referencedataservice")
public interface ReferenceDataService extends RemoteService {

    public ClientApplicationProperties getClientApplicationProperties(Long communityId) throws PortalException;

    public ReferenceData getReferenceData(Long communityId) throws PortalException;


}
