package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.entity.dto.OrganizationPatientConsentDTO;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public interface OrganizationPatientConsentServiceAsync {
    
   public void getOrganizationPatientConsentList(long patientId, int pageNumber, int pageSize,long communityId, AsyncCallback<SearchResults<OrganizationPatientConsentDTO>> callback);
  
   public void getOrganizationPatientConsentList(long patientId, long communityId,AsyncCallback<List<OrganizationPatientConsentDTO>> callback);

    public void saveOrganizationPatientConsentList(long patientId, List<OrganizationPatientConsentDTO> dtos, long communityId,AsyncCallback callback);
    
}
