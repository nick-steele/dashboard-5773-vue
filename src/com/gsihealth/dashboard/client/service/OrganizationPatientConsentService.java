package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.entity.dto.OrganizationPatientConsentDTO;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
@RemoteServiceRelativePath("organizationpatientconsentservice")
public interface OrganizationPatientConsentService extends RemoteService {
    
    public SearchResults<OrganizationPatientConsentDTO> getOrganizationPatientConsentList(long patientId, int pageNumber, int pageSize,long communityId) throws PortalException;

    public List<OrganizationPatientConsentDTO> getOrganizationPatientConsentList(long patientId,long communityId) throws PortalException;

    public void saveOrganizationPatientConsentList(long patientId, List<OrganizationPatientConsentDTO> dtos,long communityId) throws PortalException;
    
}
