package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.entity.dto.LoginResult;

/**
 * Interface for security service.
 * 
 */
@RemoteServiceRelativePath("securityservice")
public interface SecurityService extends RemoteService {


    /**
     * User forgot their password. Create a new one and send to them.
     *
     * @param portalUserId
     * @param communityId
     * @return true if valid portal user id, else false
     * @throws com.gsihealth.dashboard.common.PortalException
     */
    public boolean forgotPassword(String portalUserId,long communityId) throws PortalException;

    /**
     * Change the user's password
     * 
     * @param portalUserId
     * @param password
     */
    public String changePassword(String portalUserId, String password) throws PortalException;


    /**
     * Authenticate the claimToken
     *
     * @param claimToken
     * @return LoginResult
     */
    public LoginResult authenticate(String claimToken);


    /**
     * Authenticate the user id and password
     *
     * @param portalUserId
     * @param portalPassword
     * @return
     */
    public LoginResult authenticate(String portalUserId, String portalPassword, Long communityId,String tokenId);
        
    //JIRA 1122
    public void logout(String portalUserId, String tokenId, Long communityId);
}
