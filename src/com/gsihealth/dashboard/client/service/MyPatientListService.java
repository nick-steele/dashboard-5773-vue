package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.ListTagDTO;
import com.gsihealth.dashboard.entity.dto.MyPatientInfoDTO;
import com.gsihealth.dashboard.entity.dto.PatientListLabelDTO;

import java.util.List;


/**
 *
 * @author Chad.Darby
 */

@RemoteServiceRelativePath("mypatientlistservice")
public interface MyPatientListService extends RemoteService {

    public void add(MyPatientInfoDTO myPatientInfo) throws PortalException;

    public void update(MyPatientInfoDTO myPatientInfo) throws PortalException;

    public void delete(MyPatientInfoDTO myPatientInfo) throws PortalException;

    public void updateTags(long userId, long communityId, String type, String id) throws PortalException;

    public List<MyPatientInfoDTO> getMyPatientList(long userId, long communityId) throws PortalException;

    public String getMyPatientListCount(long userId, long communityId, String level, String tags, List patients) throws PortalException;

    public List<MyPatientInfoDTO> getMyPatientListScroll(long userId, long communityId, long start, long stop, String level, String tags, List patients) throws PortalException;

    public List<ListTagDTO> getListTags(long communityId) throws Exception;

    public PersonDTO getPatient(long patientId, long communityId) throws PortalException;

    public boolean isAlreadyOnList(long userid, long patientId, long communityId) throws PortalException;
    
    public PatientListLabelDTO myPatientListSelectionDisplayOptions(long userid, long patientId, long communityId) throws PortalException;

    public boolean isListFull(long userid, long communityId) throws PortalException;

    public boolean hasConsent(long userid, long patientId, long communityId) throws PortalException;

    public String getConfigurationValue(long communityId, String defaultValue, String key, String subKey);

    public ListTagDTO getTagByType(long communityId, String type) throws Exception;
}
