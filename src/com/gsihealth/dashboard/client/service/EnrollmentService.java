package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchPatientResults;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.client.common.PersonInfo;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.common.PatientLoadResult;
import com.gsihealth.patientmdminterface.entity.DuplicateCheckProcessResult;
import com.gsihealth.patientmdminterface.entity.SimplePerson;

import java.util.LinkedHashMap;

/**
 *
 * @author Chad Darby
 */
@RemoteServiceRelativePath("enrollmentservice")
public interface EnrollmentService extends RemoteService {

    //JIRA 2523
    public DuplicateCheckProcessResult checkForDuplicates(SimplePerson person, Long communityId) throws PortalException;
    
    public PersonDTO findPatient(SimplePerson sp, long communityId, Long currentUserOrgId) throws PortalException;
    
    public PersonDTO addPatient(PersonDTO thePerson) throws PortalException;

    public PersonDTO addPatient(PersonDTO thePerson, long communityCareteamId) throws PortalException;

    public void updatePatientFromPatientLoader(LoginResult thePatientLoaderLoginResult, PersonDTO personDTO, boolean checkEnableCareteam, boolean checkForDuplicate, boolean checkForDuplicateSsn, boolean primaryMedicaidcareIdBln, boolean secondaryMedicaidcareIdBln, String oldProgramLevelConsentStatus, String oldEnrollmentStatus, String newCareTeamId, String oldCareTeamId, long oldOrganizationId, boolean checkPatientUserUpdate,String oldAcuityScore) throws PortalException;
    
    public void updatePatient(PersonDTO thePerson,boolean checkEnableCareteam, boolean checkForDuplicate, boolean checkForDuplicateSsn, boolean primaryMedicaidcareId,boolean secondaryMedicaidcareId,String oldProgramLevelConsentStatus, String oldEnrollmentStatus, String careTeamIdNew, String careTeamIdOld, long oldOrganizationId,boolean checkPatientUserUpdate,String oldAcuityScore) throws PortalException;

    public PersonDTO updatePatientBySelfAssertion(PersonDTO thePerson) throws PortalException;
    
    public void updatePatientCareteam(long communityCareteamId, long patientId) throws PortalException;

    public void removePatientCareteam(long patientId,long communityId) throws PortalException;

    public void updatePatientForCareteamAssignment(PersonDTO thePerson,boolean checkEnableCareteam, boolean checkForDuplicate, boolean checkForDuplicateSsn, boolean primaryMedicaidcareId,boolean secondaryMedicaidcareId,String oldProgramLevelConsentStatus, String oldEnrollmentStatus, String careTeamIdNew, String careTeamIdOld, long oldOrganizationId, PatientInfo patientInfo,boolean checkPatientUserUpdate,String oldAcuityScore) throws PortalException;
  
    public void checkIfDuplicatePatient(PersonDTO personDTO,long communityId) throws PortalException;
    
    public void checkIfDuplicatePatientModify(PersonDTO personDTO ,boolean checkForDuplicate,long communityId) throws PortalException;
    
    public int getTotalCountForZero()throws PortalException;
    
    /**
     * @param communityId
     * @param searchCriteria
     * @param enrolledMode
     * @param currentUserOrgId
     * @param pageNumber
     * @param pageSize
     * @param userId
     * @return paginated list of patients meeting the contained params
     */
    public SearchResults<PersonDTO> findPatients(
            long communityId,
            SearchCriteria searchCriteria,
            boolean enrolledMode,
            Long currentUserOrgId,
            int pageNumber,
            int pageSize,
            long userId) throws PortalException;
    
    /**
     * TODO - too many db searches, any way to get rid of some of them?
     * @param communityId
     * @param searchCriteria
     * @param enrolledMode
     * @param currentUserOrgId
     * @param userId
     * @return total number of patients that meet searchCriteria
     * @throws PortalException
     */
    public SearchPatientResults countPatients(long communityId, 
            SearchCriteria searchCriteria, 
            boolean enrolledMode, 
            Long currentUserOrgId,
            long userId) throws PortalException;
    
    public SearchResults<PersonDTO> findPatientsWithNoService(SearchResults<PersonDTO> results) throws PortalException;

    public PersonInfo parseStringToPerson(String data) throws PortalException;

    public LinkedHashMap<String, String> getCounty(long communityId) throws PortalException;
}
