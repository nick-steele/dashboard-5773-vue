/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.entity.PatientProgramName;

/**
 *
 * @author emontgomery
 */
@RemoteServiceRelativePath("patientprogramservice")
public interface PatientProgramService extends RemoteService{

    public String addProgram(Long userId, PatientProgramName entity);

  

    public String edit( Long userId, PatientProgramName entity, boolean isAddtoHistory);


    public String remove(Long id, Long userId, int reasonId);
    
    public String sendPatientProgramInfo(PatientProgramName entity) throws Exception;
    
}
