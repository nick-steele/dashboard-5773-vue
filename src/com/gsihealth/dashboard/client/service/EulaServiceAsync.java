package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 *
 * @author Chad Darby
 */
public interface EulaServiceAsync {

    public void getActiveEulaContent(Long communityId, AsyncCallback<String> callback);

    public void updateUserEulaStatus(String userEmail, Long communityId, boolean flag, AsyncCallback callback);
}
