package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 *
 
 */
public interface SamhsaServiceAsync {

    public void getActiveSamhsaContent(Long communityId, AsyncCallback<String> callback);

//    public void updateUserSamhsaStatus(String userEmail, boolean flag, AsyncCallback callback);
    
    public void updateUserSamhsaStatus(String userEmail, Long communityId, boolean flag, AsyncCallback callback);
}
