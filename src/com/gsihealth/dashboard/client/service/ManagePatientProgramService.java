package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.PatientProgramDTO;
import java.util.List;

/**
 *
 * @author ssingh
 */
@RemoteServiceRelativePath("managepatientprogramservice")
public interface ManagePatientProgramService extends RemoteService {

    public SearchResults<PatientProgramDTO> getPatientProgramList(long patientId, long communityId, int pageNumber, int pageSize) throws PortalException;

    public int getTotalCountOfPrograms(long patientId, long communityId) throws PortalException;
    
    //JIRA 1024
    public int getTotalCountOfActivePrograms(long patientId, long communityId) throws PortalException;

    public void addPatientProgram(List<PatientProgramDTO> dtos, PersonDTO personDTO) throws PortalException;

    public void deletePrograms(List<PatientProgramDTO> dtos, PersonDTO personDTO) throws PortalException;

    public void addPatientProgramFromPatientLoader(LoginResult loginResult, PatientProgramDTO patientProgramDTO, PersonDTO personDTO) throws PortalException;
}
