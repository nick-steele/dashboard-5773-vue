/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.PortalException;

/**
 *
 * @author User
 */

    @RemoteServiceRelativePath("messagecountservice")
public interface MessageCountService extends RemoteService {

    
    
    public int getUnreadMailCount() throws PortalException;
    
    
}
