package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.UserFormReferenceData;
import com.gsihealth.dashboard.entity.dto.CommunityDTO;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author Satyendra
 */
@RemoteServiceRelativePath("adminservice")
public interface AdminService extends RemoteService {
    
    /**
     * Add the user
     * 
     * @param userDTO
     * @throws PortalException
     */
    public void addUser(UserDTO userDTO, List<String> userApps,boolean enableClientMfa,String authyApiKey,long communityID, boolean isThroughUserLoader) throws PortalException;

    /**
     * Update the user
     * 
     * @param userDTO
     * @throws PortalException
     */
    public String updateUser(UserDTO userDTO, List<String> userApps, boolean checkForExistingEmailAddress,boolean enableClientMfa,String authyApiKey,long communityId, boolean isThroughUserLoader) throws PortalException;
    
    public void addOrUpdateUser(UserDTO userDTO, List<String> userApps, boolean checkForExistingEmailAddress, 
            boolean isExistingUser,boolean enableClientMfa,String authyApiKey,long communityId, boolean isThroughUserLoader) throws PortalException;
    
    
    public String updateUserPreferences (UserDTO userDTO, List<String> userApps,long communityId) throws PortalException;

    public OrganizationDTO getOrganizationByOid(String oid, long communityId) throws PortalException;

    /**
     * 
     * @param accessLevelId
     * @return
     * @throws PortalException 
     */
    public int getTotalCount(long accessLevelId,long communityId) throws PortalException;
    
    /**
     * 
     * @param accessLevelId
     * @return
     * @throws PortalException 
     */
    public int getLocalAdminTotalCountForUsers(long accessLevelId,long communityId) throws PortalException;
    
    /**
     * Get list of apps for user
     */
    public List<String> getUserApps(UserDTO userDTO) throws PortalException;
     
    /**
     * Get list of apps for user
     */
    public List<String> getUserApps_Legacy(UserDTO userDTO) throws PortalException;
        /**
     * Delete the user
     * 
     * @param userDTO
     * @param callback
     */
    public void deleteUser(UserDTO userDTO, String token) throws PortalException;
    
    /**
     * Get Access Levels
     * 
     * @return
     */
    public LinkedHashMap<String, String> getAccessLevel(long communityId) throws PortalException;
     
    /**
     * Get Roles 
     * 
     * @return
     */
    public LinkedHashMap<String, String> getRoles(long communityId) throws PortalException;
    
     /**
     * Get Facility Types
     * 
     * @return
     */
    public LinkedHashMap<String, String> getFacilityTypes(long communityId) throws PortalException;
        
     /**
     * Get Organization
     * 
     * @return
     */
    public LinkedHashMap<String, String> getOrganization(long communityId) throws PortalException;

    public List<OrganizationDTO> getOrganizationDTOs(long communityId) throws PortalException;
    
    /**
     * Get the user
     * 
     * @param email
     * @return
     * @throws PortalException
     */
    public UserDTO getUser(String email, Long communityId) throws PortalException;

    /** 
     * @param email
     * @return userDTO populated with user specific data, not community stuff
     * @throws PortalException 
     */
    public UserDTO getDemographicUser(String email, Long communityId) throws PortalException;
    
    /**
     * Get the user
     * 
     * @param email
     * @return
     * @throws PortalException
     */
    public UserDTO getUser(long userId, Long communityId) throws PortalException;
/**
     * 
     * @return
     * @throws PortalException 
     */
    public UserFormReferenceData getUserFormReferenceData() throws PortalException;
    
/**
     * 
     * @param searchCriteria
     * @param accessLevelId
     * @param pageNumber
     * @param pageSize
     * @return
     * @throws PortalException 
     */
    //public SearchResults<UserDTO> findCandidates(SearchCriteria searchCriteria, long accessLevelId, int pageNumber, int pageSize) throws PortalException;    
    public SearchResults<UserDTO> findCandidates(SearchCriteria searchCriteria, long accessLevelId,
            long communityId, int pageNumber, int pageSize) throws PortalException;    
    
    /**
     * 
     * @param searchCriteria
     * @param accessLevelId
     * @return
     * @throws PortalException 
     */
    public int findTotalNoOfUser(SearchCriteria searchCriteria, long communityId) throws PortalException;
    
    /**
     * 
     * @param old
     * @param now
     * @return 
     */
    public int getDaysSinceLastChange(Date old, Date now); 
    
    public String getBuildVersion();
    
    public List<UserDTO> getUsers(Long communityId) throws PortalException; 
    
    public void requestSms(String email,long communityId,String key,String countryCode)throws PortalException;
    
    public void verifyToken(String token,String email,long communityId,String key,String countryCode)throws PortalException;
    
    public List<UserDTO> updateUserContexts(long communityId);
    
    //JIRA 727
    public List<CommunityDTO> getCommunitiesForUser(Long userId);

    List<UserDTO> getSupervisors(Long communityId, String orgId) throws PortalException;
}

