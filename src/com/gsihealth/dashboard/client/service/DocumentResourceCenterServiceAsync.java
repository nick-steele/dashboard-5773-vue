/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.entity.dto.DocumentResourceCenterDTO;
import java.util.List;

/**
 *
 * @author User
 */
public interface DocumentResourceCenterServiceAsync {
    
     public void getDocuments(long communityId, AsyncCallback<List<DocumentResourceCenterDTO>> callback);
    
}
