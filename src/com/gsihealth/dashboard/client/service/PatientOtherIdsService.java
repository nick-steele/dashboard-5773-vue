/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PatientOtherIdsDTO;
import java.util.LinkedHashMap;

/**
 *
 * @author beth.boose
 */
@RemoteServiceRelativePath("patientOtherIdsService")
public interface PatientOtherIdsService extends RemoteService {

    public SearchResults<PatientOtherIdsDTO> getPatientOtherIds(long patientId, int pageNumber, int pageSize,long communityId) throws PortalException;

    public int getTotalCountOfOtherIds(long patientId,long communityId) throws PortalException;

    public void addPatientOtherId(PatientOtherIdsDTO patientOtherIdsDTO) throws PortalException;

    public void updatePatientOtherId(PatientOtherIdsDTO patientOtherIdsDTO, boolean checkForDuplicates) throws PortalException;

    public void deletePatientOtherId(PatientOtherIdsDTO patientOtherIdsDTO) throws PortalException;

    public LinkedHashMap<String, String> getOrganizations(long getOrganizations) throws PortalException;

    public boolean isDuplicate(PatientOtherIdsDTO patientOtherIdsDTO) throws PortalException;

}
