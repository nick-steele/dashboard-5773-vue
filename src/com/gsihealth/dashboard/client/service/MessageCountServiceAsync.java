/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 *
 * @author User
 */
public interface MessageCountServiceAsync {

    /**
     * 
     * @param userId
     * @param callback 
     */
    public void getUnreadMailCount(AsyncCallback<Integer> callback);
    
}
