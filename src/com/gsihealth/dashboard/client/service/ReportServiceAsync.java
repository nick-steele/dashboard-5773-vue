package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public interface ReportServiceAsync {

    /**
     * Get users to show in report
     * 
     * @param userDTO
     * @param callback
     */
    public void getReportUsers(AsyncCallback<List<UserDTO>> callback);

    public void getTotalCountForReportUsers(AsyncCallback<Integer> callback);
    
    public void getReportUsers(int pageNumber, int pageSize, AsyncCallback<SearchResults<UserDTO>> callback);
    
    public void getAggregatePatientCount(long communityId,AsyncCallback callback);

    public void getTotalCountForEnrolledPatients(long communityId,long userId, long accessLevelId, AsyncCallback<Integer> callback);

    public void getEnrolledPatients(long communityId,long userId, long accessLevelId, int pageNumber, int pageSize, AsyncCallback<SearchResults<PersonDTO>> callback);

    // public void getTotalCountForCandidatePatientsNotEnrolled(long communityId,long currentUserOrgId, AsyncCallback<Integer> callback);

    // public void getCandidatePatientsNotEnrolled(long communityId,long currentUserOrgId, int pageNumber, int pageSize, AsyncCallback<SearchResults<PersonDTO>> notEnrolledPatientCallback);

    public void getCandidatePatientsNotEnrolled(long communityId,int pageNumber, int pageSize, AsyncCallback<SearchResults<PersonDTO>> notEnrolledPatientCallback);

    public void getTotalCountForCandidatePatientsNotEnrolled(long communityId,AsyncCallback<Integer> callback);

    public void getPatientPrograms(long userId, long accessLevelId, long currentUserOrgId, long communityId, SearchCriteria searchCriteria, int pageNumber, int pageSize, AsyncCallback<SearchResults<PersonDTO>> callback);

    public void getTotalCountOfPrograms(long userId, long accessLevelId, long currentUserOrgId, long communityId,SearchCriteria searchCriteria, AsyncCallback<Integer> callback);

    public void getEnrolledPatients(long userId, long accessLevelId, AsyncCallback<List<PersonDTO>> callback);

    public void findPatientWithPrograms(long communityId, long userId, SearchCriteria searchCriteria, AsyncCallback<SearchResults<PersonDTO>> callback);
      public void findPTSRecordsCount(long communityId, long userId,long accessLevelId, AsyncCallback<Long> callback) ;
}
