package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.AggregatePatientCountDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
@RemoteServiceRelativePath("reportservice")
public interface ReportService extends RemoteService {

    /**
     * Get the list of users for report
     *
     * @param
     * @return
     * @throws PortalException
     */
    public List<UserDTO> getReportUsers() throws PortalException;

    public int getTotalCountForReportUsers() throws PortalException;
    
    public SearchResults<UserDTO> getReportUsers(int pageNum, int pageSize) throws PortalException;
    
    public AggregatePatientCountDTO getAggregatePatientCount(long communityId) throws PortalException;

    public int getTotalCountForEnrolledPatients(long communityId,long userId, long accessLevelId) throws PortalException;

    public SearchResults<PersonDTO> getEnrolledPatients(long communityId,long userId, long accessLevelId, int pageNum, int pageSize) throws PortalException;

    // public int getTotalCountForCandidatePatientsNotEnrolled(long communityId,long currentUserOrgId) throws PortalException;

    // public SearchResults<PersonDTO> getCandidatePatientsNotEnrolled(long communityId,long currentUserOrgId, int pageNum, int pageSize) throws PortalException;
  
    public SearchResults<PersonDTO> getCandidatePatientsNotEnrolled(long communityId,int pageNum, int pageSize) throws PortalException;

    public int getTotalCountForCandidatePatientsNotEnrolled(long communityId) throws PortalException;

    public SearchResults<PersonDTO> getPatientPrograms(long userId, long accessLevelId, long currentUserOrgId, long communityId,SearchCriteria searchCriteria, int pageNum, int pageSize) throws PortalException;

    public int getTotalCountOfPrograms(long userId, long accessLevelId, long currentUserOrgId, long communityId,SearchCriteria searchCriteria) throws PortalException;

    public List<PersonDTO> getEnrolledPatients(long userId, long accessLevelId) throws PortalException;

    public SearchResults<PersonDTO> findPatientWithPrograms(long communityId, long userId, SearchCriteria searchCriteria) throws PortalException ;
    
    public Long findPTSRecordsCount(long communityId, long userId,long accessLevelId)throws PortalException ;
         
}
