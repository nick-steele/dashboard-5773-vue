package com.gsihealth.dashboard.client.service;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.common.ManageCareTeamFormData;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchPatientResults;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.common.dto.enrollment.PatientFormReferenceData;
import com.gsihealth.dashboard.entity.dto.CareTeamDTO;
import com.gsihealth.dashboard.entity.dto.CommunityCareteamDTO;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
@RemoteServiceRelativePath("careteamservice")
public interface CareteamService extends RemoteService {

    public LinkedHashMap<String, String> getCareteamNames(Long communityId) throws PortalException;

    public SearchPatientResults countPatients(long communityId,
            SearchCriteria searchCriteria,
            boolean enrolledMode,
            Long currentUserOrgId,
            long userId) throws PortalException;
    
    /**
     * @param communityId
     * @param searchCriteria
     * @param enrolledMode
     * @param currentUserOrgId
     * @param pageNumber
     * @param pageSize
     * @param userId
     * @return paginated list of patients meeting the contained params
     */
    public SearchResults<PersonDTO> findPatients(
            long communityId,
            SearchCriteria searchCriteria,
            boolean enrolledMode,
            Long currentUserOrgId,
            int pageNumber,
            int pageSize,
            long userId) throws PortalException;

    public CommunityCareteamDTO getCareteamForPatient(long patientId) throws PortalException;

    //JIRA 1178 - Spira 5201
    public CommunityCareteamDTO getCareteam(String careteamName, long communityId) throws PortalException;

    //public void savePatientInfo(PatientInfo patientInfo,boolean checkEnableCareteam,boolean checkPatientUserActive) throws PortalException;

    public void savePatientInfo(PersonDTO personDTO, PatientInfo patientInfo,boolean checkEnableCareteam,boolean checkPatientUserActive,long communityId) throws PortalException;

    public CareTeamDTO getUsersForCareteam(long communityCareteamId) throws PortalException;

    public CareTeamDTO getUsersForCareteam(long communityCareteamId, long patientId, long patientOrgId) throws PortalException;

    public List<UserDTO> getUsersForCareteamWithoutPermitConsent(long communityCareteamId, long patientId, long patientOrgId) throws PortalException;

    public void saveCareteam(long communityCareteamId, String careteamName, List<UserDTO> users,long communityId ) throws PortalException;

    public void saveNewCareteam(String careteamName, long communityId, List<UserDTO> users) throws PortalException;

    public boolean hasPatientsAssignedToCareteam(long communityCareteamId) throws PortalException;

    public boolean hasOtherPatientsAssignedToCareteam(long communityCareteamId, long patientId) throws PortalException;

    public List<UserDTO> getAvailableUsers(String role, List<Long> userIdsToExclude) throws PortalException;

    public List<UserDTO> getAvailableUsers() throws PortalException;
    
    public LinkedHashMap<String, String> getConsenterTable(long communityId) throws PortalException;
    
    public List<OrganizationDTO> getOrganizationDTOs(long communityId) throws PortalException;

    public int getTotalCountForAvailableUsers(String role, List<Long> userIdsToExclude) throws PortalException;

    public ManageCareTeamFormData getManageCareTeamFormData(long communityId) throws PortalException;

    public PatientFormReferenceData getPatientFormReferenceData(long communityId) throws PortalException;

//    public List<PersonDTO> findDashboardPatient(long communityId, long userId, SearchCriteria searchCriteria) throws PortalException;

    public int findTotalNoOfUser(long communityId, SearchCriteria searchCriteria,
            long userId) throws PortalException;

    public String performPatientEngagement(Long communityId,
            Long patientId, boolean directMessagngRequired) throws Exception;
    
    public PersonDTO updatePatientBySelfAssertion(PersonDTO thePerson) throws PortalException;

    
}
