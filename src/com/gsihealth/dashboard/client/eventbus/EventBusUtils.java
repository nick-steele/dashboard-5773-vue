package com.gsihealth.dashboard.client.eventbus;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.SimpleEventBus;

/**
 * Collection of utils for the event bus
 * 
 * @author Chad Darby
 */
public class EventBusUtils {
    
    private static EventBus eventBus = GWT.create(SimpleEventBus.class);
    
    public static EventBus getEventBus() {
        return eventBus;
    }
}
