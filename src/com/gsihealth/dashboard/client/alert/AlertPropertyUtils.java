package com.gsihealth.dashboard.client.alert;

import com.gsihealth.dashboard.entity.dto.AlertMessageDTO;
import com.gsihealth.dashboard.entity.dto.UserAlertDTO;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class AlertPropertyUtils {
    
    /**
     * Convert DTO to record
     * 
     * @param index
     * @param userAlertDTO
     * @return 
     */
    public static UserAlertRecord convert(int index, UserAlertDTO userAlertDTO) {
        AlertMessageDTO alertMessage = userAlertDTO.getAlertMessageId();
        
        String severity = alertMessage.getSeverity();
        String applicationName = userAlertDTO.getApplicationName();
        Date dateReceived = userAlertDTO.getCreationDateTime();
        String alertDetails = alertMessage.getMessage();
        
        String patientFirstName = alertMessage.getPatientFirstName();
        String patientLastName = alertMessage.getPatientLastName();
        String patientId = alertMessage.getPatientId();        
        String alertTypeName = alertMessage.getAlertTypeName();        
        
        UserAlertRecord record = new UserAlertRecord(index, severity, applicationName, dateReceived, alertDetails, alertTypeName, patientFirstName, patientLastName, patientId);

        return record;
    }
    
    /**
     * Convert list of DTOs to records
     * 
     * @param userAlertDTOs
     * @return 
     */
    public static UserAlertRecord[] convert(List<UserAlertDTO> userAlertDTOs) {
        UserAlertRecord[] records = new UserAlertRecord[userAlertDTOs.size()];
        
        for (int i=0; i < userAlertDTOs.size(); i++) {
            UserAlertRecord temp = convert(i, userAlertDTOs.get(i));
            records[i] = temp;
        }
        
        return records;
    }
}
