package com.gsihealth.dashboard.client.alert;

import com.gsihealth.dashboard.common.AlertConstants;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import java.util.Date;

/**
 *
 * @author Chad Darby
 */
public class UserAlertRecord extends ListGridRecord {

    private int index;

    public UserAlertRecord(int index, String severity, String applicationName, Date dateReceived, String alertDetails, String alertTypeName, String patientFirstName, String patientLastName, String patientId) {
        setIndex(index);
        
        setSeverity(severity);
        setApplicationName(applicationName);
        setDateReceived(dateReceived);
        setAlertDetails(alertDetails);
        setAlertTypeName(alertTypeName);
        setPatientFirstName(patientFirstName);
        setPatientLastName(patientLastName);
        setPatientId(patientId);        
    }

    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

    public String getAlertDetails() {
        return getAttribute(AlertConstants.ALERT_DETAILS);
    }

    public void setAlertDetails(String alertDescription) {
        setAttribute(AlertConstants.ALERT_DETAILS, alertDescription);
    }

    public String getAlertTypeName() {
        return getAttribute(AlertConstants.ALERT_TYPE_NAME);
    }
    
    public void setAlertTypeName(String alertTypeName) {
        setAttribute(AlertConstants.ALERT_TYPE_NAME, alertTypeName);
    }
    
    public String getApplicationName() {
        return getAttribute(AlertConstants.APPLICATION);
    }

    public void setApplicationName(String applicationName) {
        setAttribute(AlertConstants.APPLICATION, applicationName);
    }

    public Date getDateReceived() {
        return getAttributeAsDate(AlertConstants.DATE_RECEIVED);
    }

    public void setDateReceived(Date dateReceived) {
        setAttribute(AlertConstants.DATE_RECEIVED, dateReceived);

    }

    public String getPatientFirstName() {
        return getAttribute(AlertConstants.PATIENT_FIRST_NAME);
    }

    public void setPatientFirstName(String patientFirstName) {        
        patientFirstName = StringUtils.defaultIfEmpty(patientFirstName, AlertConstants.NOT_APPLICABLE);        
        setAttribute(AlertConstants.PATIENT_FIRST_NAME, patientFirstName);
    }

    public String getPatientLastName() {        
        return getAttribute(AlertConstants.PATIENT_LAST_NAME);
    }

    public void setPatientLastName(String patientLastName) {
        patientLastName = StringUtils.defaultIfEmpty(patientLastName, AlertConstants.NOT_APPLICABLE);
        setAttribute(AlertConstants.PATIENT_LAST_NAME, patientLastName);
    }

    public String getSeverity() {
        return getAttribute(AlertConstants.SEVERITY);
    }

    public void setSeverity(String severity) {
        setAttribute(AlertConstants.SEVERITY, severity);
    }
    
    public String getPatientId() {        
        return getAttribute(AlertConstants.PATIENT_ID);
    }
    
    public void setPatientId(String patientId) {
        patientId = StringUtils.defaultIfEmpty(patientId, AlertConstants.NOT_APPLICABLE);
        setAttribute(AlertConstants.PATIENT_ID, patientId);        
    }    
}
