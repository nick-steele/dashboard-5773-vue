package com.gsihealth.dashboard.client.alert;

/**
 *
 * @author Chad Darby
 */
public enum AlertSearchType {
    PATIENT_FIRST_NAME, PATIENT_LAST_NAME, DATE_RECEIVED, APPLICATION, SEVERITY, PATIENT_ID}
