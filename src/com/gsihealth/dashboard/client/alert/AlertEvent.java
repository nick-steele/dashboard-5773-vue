package com.gsihealth.dashboard.client.alert;

import com.google.gwt.event.shared.GwtEvent;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.entity.dto.UserAlertDTO;
import java.util.List;

/**
 * 
 * @author Chad Darby
 */
public class AlertEvent extends GwtEvent<AlertEventHandler> {

    public static Type<AlertEventHandler> TYPE = new Type<AlertEventHandler>();

    private List<UserAlertDTO> alerts;
    private int totalCount;
    
    public AlertEvent(SearchResults<UserAlertDTO> searchResults) {
        totalCount = searchResults.getTotalCount();
        alerts = searchResults.getData();
    }
    
    @Override
    public Type<AlertEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(AlertEventHandler handler) {
        handler.onAlert(this);
    }

    public List<UserAlertDTO> getAlerts() {
        return alerts;
    }

    public int getTotalCount() {
        return totalCount;
    }        
    
}
