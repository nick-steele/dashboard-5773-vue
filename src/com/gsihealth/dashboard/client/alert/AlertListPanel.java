package com.gsihealth.dashboard.client.alert;

import com.google.gwt.user.client.Window;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.common.ApplicationInfo;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.common.AlertConstants;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.gsihealth.dashboard.client.WindowManager;
import com.gsihealth.dashboard.client.admintools.AdminToolsPanel;
import com.gsihealth.dashboard.client.careteam.CareTeamPanel;
import com.gsihealth.dashboard.client.common.DashboardTitles;
import com.gsihealth.dashboard.client.common.GridPager;
import com.gsihealth.dashboard.client.dashboard.DashboardPanel;
import com.gsihealth.dashboard.client.enrollment.EnrollmentPanel;
import com.gsihealth.dashboard.client.reports.MainReportPanel;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.DateFieldCellFormatter;
import com.gsihealth.dashboard.common.util.AppConfigConstants;
import com.gsihealth.dashboard.entity.dto.ApplicationDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.redux.Redux;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.gsihealth.dashboard.client.util.Constants.REPORTS_LIST_GRID_HEIGHT;

/**
 *
 * @author Chad Darby
 */
public class AlertListPanel extends VLayout {

    private ListGrid listGrid;
    private final int APPLICATION_ICON_COLUMN = 1;
    private final int DATE_COLUMN = 2;
    private final int DETAILS_COLUMN = 3;
    private final static String[] columnNames = {AlertConstants.SEVERITY, AlertConstants.APPLICATION, AlertConstants.DATE_RECEIVED, AlertConstants.ALERT_DETAILS, AlertConstants.ALERT_TYPE_NAME, AlertConstants.PATIENT_FIRST_NAME, AlertConstants.PATIENT_LAST_NAME, AlertConstants.PATIENT_ID};
    private final static int[] columnWidths = {50, 140, 150, 400, 200, 200, 200, 150};
    private Map<String, ApplicationInfo> applicationInfoMap;
    private GridPager pager;
    private List<ApplicationDTO> userApps;
    private List<ApplicationDTO> allApps;
    private Map<String, ApplicationDTO> alertApplicationMap;
    private Map<String, ApplicationDTO> allApplicationMap;
    private String externalWindowFeatures;
  
    public AlertListPanel() {
        
        LoginResult loginResult = ApplicationContextUtils.getLoginResult(); 
        userApps = loginResult.getUserApps();
        allApps = loginResult.getAllApps();
        externalWindowFeatures = loginResult.getTreatFeatures();
              
        buildGui();

        applicationInfoMap = new HashMap<String, ApplicationInfo>();      
        loadApplicationInfoMap();
        setupAlertApplicationMap();
    }

    protected long getUserLevelId() {
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();

        return loginResult.getUserLevelId();
    }
      
    private void buildGui() {

        setHeight(REPORTS_LIST_GRID_HEIGHT);
        listGrid = buildListGrid();

        setPadding(5);
        addMember(listGrid);

        pager = new GridPager(0);
        addMember(pager);
    }

    public void populateListGrid(ListGridRecord[] pagedData, int totalCount) {

        // scroll to first row
        listGrid.scrollToRow(1);

        listGrid.setData(pagedData);
        listGrid.markForRedraw();
        pager.setTotalCount(totalCount);
    }

    /**
     * Helper method to build a ListGrid
     *
     * @return
     */
    private ListGrid buildListGrid() {

        ListGrid theListGrid = new ListGrid();

        theListGrid.setCanReorderFields(true);

        theListGrid.setCellHeight(54);

        theListGrid.setWidth100();
        theListGrid.setHeight(REPORTS_LIST_GRID_HEIGHT);

        theListGrid.setAlternateRecordStyles(true);
        theListGrid.setShowAllRecords(false);
        theListGrid.setSelectionType(SelectionStyle.SIMPLE);
        theListGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);

        theListGrid.setCanEdit(false);

        // copy alert text
        theListGrid.setCanSelectText(true);
        theListGrid.setCanDragSelectText(true);
                
        // add fields
        ListGridField[] fields = buildListGridFields(columnNames);
        theListGrid.setFields(fields);

        // handle date clumn
        ListGridField dateField = fields[DATE_COLUMN];
        setDateFieldCellFormatter(dateField);

        // application icon column
        ListGridField applicationField = fields[APPLICATION_ICON_COLUMN];
        applicationField.setAlign(Alignment.CENTER);
        applicationField.setType(ListGridFieldType.TEXT);
        applicationField.setCellFormatter(new ImageTextCellFormatter());

        // list grid description
        ListGridField detailsField = fields[DETAILS_COLUMN];
        setdDetailsFieldCellFormatter(detailsField);

        theListGrid.setEmptyMessage("...");
        theListGrid.setCanResizeFields(true);

        // wrap text
        theListGrid.setFixedRecordHeights(false);
        theListGrid.setWrapCells(true);
        
        //add click handler
        theListGrid.addRecordDoubleClickHandler(new AlertDoubleClickHandler());
        
        return theListGrid;
    }

    private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];

        for (int i = 0; i < columnNames.length; i++) {
            String tempColumnName = columnNames[i];
            int tempColumnWidth = columnWidths[i];
            ListGridField tempField = buildListGridField(tempColumnName, tempColumnWidth);
            tempField.setCanEdit(true);
            fields[i] = tempField;
        }

        return fields;
    }

    private ListGridField buildListGridField(String title, int width) {
        ListGridField tempField = new ListGridField(title, title, width);

        tempField.setCanEdit(false);

        return tempField;
    }

    public void handleNavigationButtons(int thePageNum) {
        pager.handleNavigationButtons(thePageNum);
    }

    public void gotoPage(int pageNumber) {
        pager.gotoPage(pageNumber);
    }

    public ListGrid getListGrid() {
        return listGrid;
    }

    public int getCurrentPageNumber() {
        return pager.getCurrentPageNumber();
    }

    public void setCurrentPageNumber(int num) {
        pager.setCurrentPageNumber(num);
    }

    public void setPageEventHandler(PageEventHandler handler) {
        pager.setPageEventHandler(handler);
    }

    public void refreshPageLabels(int pageNum) {
        pager.refreshPageLabels(pageNum);
    }

    public void setTotalCount(int totalCount) {
        pager.setTotalCount(totalCount);
    }

    public int getPageSize() {
        return pager.getPageSize();
    }

    /**
     * Add cell formatter for date field
     *
     * @param dateField
     */
    private void setDateFieldCellFormatter(ListGridField dateField) {

        final DateTimeFormat dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy h:mm a");
        dateField.setCellFormatter(new DateFieldCellFormatter(dateFormatter));
    }

    private void loadApplicationInfoMap() {
        applicationInfoMap = (Map<String, ApplicationInfo>) ApplicationContext.getInstance().getAttribute(Constants.APPLICATION_INFO_MAP);
    }

    void clearPageLabels() {
        pager.clearPageLabels();
        pager.setTotalCount(0);
        pager.refreshPageLabels(1);
    }

    private void setdDetailsFieldCellFormatter(ListGridField detailsField) {
        detailsField.setCellFormatter(new DetailsFieldCellFormatter());

    }

    class ImageTextCellFormatter implements CellFormatter {

        public String format(Object value, ListGridRecord record, int rowNum, int colNum) {

            UserAlertRecord userAlertRecord = (UserAlertRecord) record;

            String key = (String) value;

            ApplicationInfo appInfo = null;

            boolean specialTreatAlert = isSpecialTreatAlert(userAlertRecord);

            if (specialTreatAlert) {
                appInfo = applicationInfoMap.get(AlertConstants.TREAT_APP);
            } else {
                appInfo = applicationInfoMap.get(key);
            }

            if (appInfo == null) {
                appInfo = new ApplicationInfo("alerts.jpg", "App");
            }

            return "<center><img src='images/32x32/" + appInfo.getIconFileName() + "' style='padding: 5px;' /><br>" + appInfo.getScreenName() + "</center>";
        }
    }

    /**
     * Remove special TREAT marker in alert details
     */
    class DetailsFieldCellFormatter implements CellFormatter {

        public String format(Object value, ListGridRecord record, int rowNum, int colNum) {

            UserAlertRecord userAlertRecord = (UserAlertRecord) record;

            boolean specialTreatAlert = isSpecialTreatAlert(userAlertRecord);

            String details = userAlertRecord.getAlertDetails();

            // if special treat alert then string the marker from details
            if (specialTreatAlert) {
                details = details.replace(com.gsihealth.dashboard.common.Constants.TREAT_PATIENT_ALERT_TAG, "");
            }

            return details;
        }
    }

    private boolean isSpecialTreatAlert(UserAlertRecord userAlertRecord) {      
        
        String desc = "   ";
        if (userAlertRecord.getAlertDetails() != null) {
         desc = userAlertRecord.getAlertDetails();
        }
        
        return desc.endsWith(com.gsihealth.dashboard.common.Constants.TREAT_PATIENT_ALERT_TAG);
    }
    
    class AlertDoubleClickHandler implements RecordDoubleClickHandler {

        public void onRecordDoubleClick(RecordDoubleClickEvent event) {

            UserAlertRecord userAlertRecord = (UserAlertRecord) event.getRecord();

            try {
                if (userAlertRecord == null) {
                    SC.warn("At least one alert must be selected.  Please select an alert and try again.");
                    return;
}
                System.out.println("there is a selected record:" + userAlertRecord.getApplicationName() + "--");

                ApplicationInfo appInfo = applicationInfoMap.get(userAlertRecord.getApplicationName());
                ApplicationDTO app = alertApplicationMap.get(String.valueOf(appInfo.getApplicationId()));
                openApplication(appInfo,app);
                   
            } catch (Exception e) {
                SC.warn("Exception: " + e.getMessage());
                e.printStackTrace(System.out);
            }
        }
    }
    
    public void openApplication(ApplicationInfo appInfo, ApplicationDTO app){
        //Check to see if app is click to enabled
        ApplicationDTO fullAppInfo = (ApplicationDTO) allApplicationMap.get(String.valueOf(appInfo.getApplicationId()));
       // System.out.println("Get full app info for : " +fullAppInfo.getApplicationId()+": "+ fullAppInfo.getScreenName());
        if (!fullAppInfo.getClickToOpenEnabled()) {
            SC.say("Opening " + fullAppInfo.getScreenName() + " from this screen is not currently supported");
            return;
        }

        if (app == null) {
            SC.say("The " + appInfo.getScreenName() + " application is not in your user profile");
            return;
        }

        if (app.getNativeApp()) {
            openInternalApp(appInfo, app);
            //alertClickHandlerMap.get(appInfo.getScreenName())
        } else {
            openExternalAppWindows(appInfo, app);

        }
    }
    
    
    public void openInternalApp(ApplicationInfo appInfo, ApplicationDTO app) {
        if (appInfo.getScreenName().equals(DashboardTitles.DEMOGRAPHICS)) {
            //closes existing tab , so default start screen will show when new panel is opened.
            //WindowManager.closeTab(appInfo.getScreenName());
            //WindowManager.addWindow(appInfo.getScreenName(), new EnrollmentPanel());
            Redux.jsEvent("local:openApp:appEnrollment");
            return;
        }
        if (appInfo.getScreenName().equals(DashboardTitles.CARE_TEAM_ASSIGNMENT)) {
            //WindowManager.closeTab(appInfo.getScreenName());
            //WindowManager.addWindow(appInfo.getScreenName(), new CareTeamPanel());
            Redux.jsEvent("local:openApp:appCareteams");
            return;
        }

        if (appInfo.getScreenName().equals(DashboardTitles.ADMIN)) {
            //WindowManager.closeTab(appInfo.getScreenName());
            //WindowManager.addWindow(appInfo.getScreenName(), new AdminToolsPanel());
            Redux.jsEvent("local:openApp:appAdmin");
            return;
        }

        if (appInfo.getScreenName().equals(DashboardTitles.REPORTS)) {
            //WindowManager.closeTab(appInfo.getScreenName());
            //WindowManager.addWindow(appInfo.getScreenName(), new MainReportPanel());
            Redux.jsEvent("local:openApp:appReport");
            return;
        }
                  
    }
    public void openExternalAppWindows(ApplicationInfo appinfo,ApplicationDTO app) {
        
        if (appinfo.getApplicationId() == AppConfigConstants.CAREPLAN_APP_ID ) {
            DashboardPanel.showTreatWindow(Constants.CAREPLAN_HOMEPAGE, "", true);
            return;
        }
        
        if (appinfo.getApplicationId() == AppConfigConstants.MESSAGES_APP_ID ) {
            DashboardPanel.showMessageAppWindow();
            return;
        }
        
        if (appinfo.getApplicationId() == AppConfigConstants.POPULATION_MANAGER_APP_ID ) {
            DashboardPanel.showPopulationManagerWindow();
            return;
        }
        
        if (appinfo.getApplicationId() == AppConfigConstants.UHC_ENROLLMENT_REPORT_APP_ID ) {
            DashboardPanel.showUhcEnrollmentReportAppWindow();
            return;
        }
        
         if (appinfo.getApplicationId() == AppConfigConstants.CAREBOOK_APP_ID ) {
            DashboardPanel.showCarebookAppWindow();
            return;
        }
        
        String theUrl = app.getExternalApplicationUrl();
        String features = externalWindowFeatures;
        String name = appinfo.getScreenName();;

        System.out.println("Default External Window for " + name+ ": " + theUrl);
        Redux.showAppWindow(theUrl, "default_"+name, features);
        

    }    
    
    public void setupAlertApplicationMap() {
          
        // key: app id, ApplicationDTO
        alertApplicationMap = new HashMap<String, ApplicationDTO>();

        for (ApplicationDTO app : userApps) {
            //multi tenant vinay  
            long appId = app.getApplicationId();
            alertApplicationMap.put(String.valueOf(appId), app);
        }
        System.out.println("AlertApplicationMap has been created");
        System.out.println("=====================================");
        System.out.println("Creating Map of All " + allApps.size() + " applications");
        allApplicationMap = new HashMap<String, ApplicationDTO>();

        for (ApplicationDTO app : allApps) {

            long appId = app.getApplicationId();
            allApplicationMap.put(String.valueOf(appId), app);

        }
        System.out.println("AllApplicationMap has been created");

    }
}