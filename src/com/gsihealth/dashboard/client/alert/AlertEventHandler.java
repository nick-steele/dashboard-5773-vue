package com.gsihealth.dashboard.client.alert;

import com.google.gwt.event.shared.EventHandler;

/**
 *
 * @author Chad Darby
 */
public interface AlertEventHandler extends EventHandler {

    /**
     * Called when an alert event is fired
     * 
     * @param alertEvent 
     */
    public void onAlert(AlertEvent alertEvent);
    
}
