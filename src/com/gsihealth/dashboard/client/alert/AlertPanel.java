package com.gsihealth.dashboard.client.alert;

import com.gsihealth.dashboard.common.ApplicationInfo;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.common.AlertConstants;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.redux.Redux;
import com.smartgwt.client.widgets.Button;
import com.gsihealth.dashboard.client.DashBoardApp;
import static com.gsihealth.dashboard.client.alert.AlertSearchType.APPLICATION;
import static com.gsihealth.dashboard.client.alert.AlertSearchType.DATE_RECEIVED;
import static com.gsihealth.dashboard.client.alert.AlertSearchType.PATIENT_FIRST_NAME;
import static com.gsihealth.dashboard.client.alert.AlertSearchType.PATIENT_ID;
import static com.gsihealth.dashboard.client.alert.AlertSearchType.PATIENT_LAST_NAME;
import static com.gsihealth.dashboard.client.alert.AlertSearchType.SEVERITY;
import com.gsihealth.dashboard.client.common.CustomButtonPanel;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.eventbus.EventBusUtils;
import com.gsihealth.dashboard.client.service.AlertService;
import com.gsihealth.dashboard.client.service.AlertServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.UserAlertDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.gsihealth.dashboard.client.service.SubcriptionManagerAlertService;
import com.gsihealth.dashboard.client.service.SubcriptionManagerAlertServiceAsync;
import com.gsihealth.dashboard.entity.dto.AlertPreferenceDTO;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.grid.ListGrid;

import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Chad Darby
 */
public class AlertPanel extends VLayout {

    public static final String SELECT_A_VALUE = "- Select a Value -";
    private AlertListPanel alertListPanel;
    private DynamicForm headerForm;
    private SelectItem searchBySelectItem;
    private TextItem searchTextItem;
    private Button removeAlertButton;
    private final AlertServiceAsync alertService = GWT.create(AlertService.class);
    private List<UserAlertDTO> alerts;
    private HandlerRegistration handlerRegistration;
    private SelectItem appNamesSelectItem;
    private String[] severityLevels = {"CRITICAL", "HIGH", "MEDIUM", "LOW"};
    private SelectItem severitySelectItem;
    private DateItem fromDateItem;
    private DateItem toDateItem;
    private long userId;
    private boolean searchMode;
    private AlertSearchType alertSearchType;
    private Label header;
    private LinkedHashMap<String, String> applicationMap;
    private static final String ADMIN_APP_ID = "4";
    private ProgressBarWindow progressBarWindow;
    private String[] searchByFields = {SELECT_A_VALUE, AlertConstants.APPLICATION, AlertConstants.DATE_RECEIVED, AlertConstants.PATIENT_FIRST_NAME, AlertConstants.PATIENT_LAST_NAME, AlertConstants.PATIENT_ID, AlertConstants.SEVERITY};
    private DateTimeFormat dateFormatter;

    public AlertPanel() {

        dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");

        buildGui();
        DashBoardApp.restartTimer();

        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        userId = loginResult.getUserId();

        // register with the bus
        subscribeToBus();
    }

    protected DateItem buildDateItem(String name, String title) {
        DateItem theDateItem = new DateItem(name, title);
        theDateItem.setWrapTitle(false);
        theDateItem.setTextAlign(Alignment.LEFT);
        theDateItem.setUseMask(true);
        theDateItem.setEnforceDate(true);
        theDateItem.setDisplayFormat(DateDisplayFormat.TOUSSHORTDATE);
        theDateItem.setWidth(90);

        theDateItem.setStartDate(dateFormatter.parse("01/01/1900"));

        return theDateItem;
    }

    private void buildGui() {

        // toolstrip
        ToolStrip toolStrip = createToolStrip();
        addMember(toolStrip);

        // header

        header = new Label("<b>Alerts</b>");
        header.setHeight(10);
        header.setMargin(10);
        header.setWidth(300);
        addMember(header);

        // layout for search button bar
        HLayout hLayout = new HLayout();
        hLayout.setHeight(14);
        hLayout.setAlign(Alignment.CENTER);
        hLayout.setWidth100();

        // form for build search button bar
        headerForm = buildSearchButtonBar();
        hLayout.addMember(headerForm);

        // list grid panel
        alertListPanel = new AlertListPanel();
        addMember(hLayout);
        addMember(alertListPanel);

        alertListPanel.setPageEventHandler(new AlertPageEventHandler());

        // layout spacer b/w layout lables and layout of remover button
        LayoutSpacer topSpacer = new LayoutSpacer();
        topSpacer.setHeight(20);
        addMember(topSpacer);

        // layout for remove button
        Layout buttonBar = buildButtonBar();
        addMember(buttonBar);

        progressBarWindow = new ProgressBarWindow("Loading", "Loading...");
        setPadding(5);
        setWidth100();
        setHeight100();
    }

    protected ToolStrip createToolStrip() {
        ToolStripButton alertsButton = new ToolStripButton("Alerts");

        ToolStrip toolStrip = new ToolStrip();
        toolStrip.setWidth100();
        toolStrip.addButton(alertsButton);

        alertsButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                setSearchMode(false);
                resetSearchFields();
                gotoPage(1);
            }
        });

        /*
         * not scoped for micro-release 2 ToolStripButton alertPreferencesButton
         * = new ToolStripButton("Alert Preferences"); toolStrip.addSeparator();
         * toolStrip.addButton(alertPreferencesButton);
         */

        return toolStrip;
    }

    private DynamicForm buildSearchButtonBar() {
        // form
        DynamicForm dynaForm = new DynamicForm();
        dynaForm.setWidth(300);
        dynaForm.setWrapItemTitles(false);
        dynaForm.setCellPadding(5);
        dynaForm.setNumCols(10);

        // drop down list
        searchBySelectItem = new SelectItem("searchby", "Search By");

        String[] theSearchByFields = null;

        theSearchByFields = searchByFields;

        searchBySelectItem.setValueMap(theSearchByFields);
        searchBySelectItem.addChangedHandler(new SearchByStatusChangedHandler());
        searchBySelectItem.setDefaultToFirstOption(true);

        PickerIcon searchPickerIcon = new PickerIcon(PickerIcon.SEARCH);
        PickerIcon clearPickerIcon = new PickerIcon(PickerIcon.CLEAR);

        final SearchClickHandler searchClickHandler = new SearchClickHandler();
        searchPickerIcon.addFormItemClickHandler(searchClickHandler);
        clearPickerIcon.addFormItemClickHandler(new ClearClickHandler());

        // search text item
        searchTextItem = new TextItem();
        searchTextItem.setShowTitle(false);
        searchTextItem.setIcons(searchPickerIcon, clearPickerIcon);

        searchTextItem.addKeyPressHandler(new KeyPressHandler() {
            @Override
            public void onKeyPress(KeyPressEvent event) {
                if (event.getKeyName().equals("Enter")) {
                    searchClickHandler.onFormItemClick(null);
                }
            }
        });

        severitySelectItem = new SelectItem();
        severitySelectItem.setShowTitle(false);
        severitySelectItem.setValueMap(severityLevels);
        severitySelectItem.setIcons(searchPickerIcon, clearPickerIcon);
        severitySelectItem.setVisible(false);

        appNamesSelectItem = new SelectItem();
        appNamesSelectItem.setWidth(200);
        appNamesSelectItem.setShowTitle(false);
        appNamesSelectItem.setIcons(searchPickerIcon, clearPickerIcon);
        appNamesSelectItem.setVisible(false);

        fromDateItem = buildDateItem("from", "From");
        fromDateItem.setWidth(150);
        fromDateItem.setVisible(false);

        toDateItem = buildDateItem("to", "To");
        toDateItem.setVisible(false);
        toDateItem.setWidth(150);
        toDateItem.setIcons(searchPickerIcon, clearPickerIcon);

        // adding field item to form
        dynaForm.setFields(searchBySelectItem, searchTextItem, appNamesSelectItem, severitySelectItem, fromDateItem, toDateItem);

        return dynaForm;
    }

    private Layout buildButtonBar() {
        // layout for button bar
        HLayout buttonLayout = new HLayout(10);
        buttonLayout.setWidth100();
        buttonLayout.setAlign(Alignment.CENTER);

        // remove button
        removeAlertButton = new Button("Remove Selected Alerts");
        removeAlertButton.setWidth(140);
        removeAlertButton.addClickHandler(new RemoveClickHandler());
        buttonLayout.addMember(removeAlertButton);

        return buttonLayout;
    }

    public void gotoPage(final int pageNumber) {

        alertService.getTotalCount(userId, new PortalAsyncCallback<Integer>() {
            public void onSuccess(Integer total) {

                if (total > 0) {
                    alertListPanel.setTotalCount(total);
                    alertListPanel.refreshPageLabels(pageNumber);
                    alertListPanel.gotoPage(pageNumber);
                } else {
                    clearListGrid();
                }
            }
        });
    }

    /**
     * Subscribe as a handler for the event bus. Analogous to signing up as a
     * ClickHandler
     */
    private void subscribeToBus() {
        EventBus eventBus = EventBusUtils.getEventBus();

        AlertEventHandler handler = new ListAlertEventHandler();
        handlerRegistration = eventBus.addHandler(AlertEvent.TYPE, handler);
    }

    protected void populate(List<UserAlertDTO> theAlerts, int totalCount) {

        alerts = theAlerts;

        if (!alerts.isEmpty()) {
            ListGridRecord[] records = AlertPropertyUtils.convert(alerts);
            alertListPanel.populateListGrid(records, totalCount);

            int currentPageNumber = alertListPanel.getCurrentPageNumber();
            alertListPanel.refreshPageLabels(currentPageNumber);

            alertListPanel.handleNavigationButtons(currentPageNumber);

            if (!searchMode) {
                // update main alert icon button count
                CustomButtonPanel alertButton =
                        (CustomButtonPanel) ApplicationContext.getInstance().getAttribute(Constants.ALERT_BUTTON_KEY);
                /* FIXME workaround, this is not set in dashboardPanel.showAlertIcon()
                if gwt is turned off, but still needed find a better way to handle this*/
                if (alertButton != null) {
                    alertButton.setAlertText(totalCount);
                }
            }
        } else {
            clearListGrid();


            if (searchMode) {
                SC.warn("Search did not return any results. Please modify your search criteria.");
            }
        }
    }

    public void clearListGrid() {
        ListGrid listGrid = alertListPanel.getListGrid();
        ListGridRecord[] records = new ListGridRecord[0];
        listGrid.setData(records);

        alertListPanel.clearPageLabels();
    }

    private void setupApplicationMap() {
        
        applicationMap = new LinkedHashMap<String, String>();
        Map<String, ApplicationInfo> applicationInfoMap = (Map<String, ApplicationInfo>) ApplicationContext.getInstance().getAttribute(Constants.APPLICATION_INFO_MAP);
        System.out.println("applicationInfoMap=" + applicationInfoMap + "\n\n");
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        long communityId = loginResult.getCommunityId();
        AlertAppFilterCallback appFilter = new AlertAppFilterCallback(communityId, applicationInfoMap, userId);
        appFilter.attempt();
        System.out.println("applicationMap=" + applicationMap + "\n\n");
    }
    
    class AlertAppFilterCallback extends RetryActionCallback<List<AlertPreferenceDTO>> {

        private  SubcriptionManagerAlertServiceAsync alertService = GWT.create(SubcriptionManagerAlertService.class);
        private long communityId;
        private Map<String, ApplicationInfo> applicationInfoMap;
        private long userId;

        public AlertAppFilterCallback(long communityId, Map<String, ApplicationInfo> applicationInfoMap, long userId) {
            this.applicationInfoMap = applicationInfoMap;
            this.communityId = communityId;
            this.userId = userId;
        }
        public void attempt() {
            alertService.getAlerts(communityId, applicationInfoMap, userId, this);
        }

        public void onCapture(List<AlertPreferenceDTO> docs) {
            for(AlertPreferenceDTO temp : docs){
                applicationMap.put(String.valueOf(temp.getApplicationID()), temp.getAlertName());
            }
            appNamesSelectItem.setValueMap(applicationMap);
        }
    }

    class FormItemGetPageAlertsCallback extends RetryActionCallback<SearchResults<UserAlertDTO>> {

        @Override
        public void attempt() {
            setSearchMode(true);

            // GET THE SEARCH type
            String searchType = searchBySelectItem.getValueAsString();

            if (searchType.equals(SELECT_A_VALUE)) {
                SC.warn("Invalid entry. Please enter a valid search value and attempt the search again.");
                return;
            }

            int pageSize = alertListPanel.getPageSize();
            alertListPanel.setCurrentPageNumber(1);

            boolean patientIdSearch = searchType.equals(AlertConstants.PATIENT_ID);
            boolean patientFirstNameSearch = searchType.equals(AlertConstants.PATIENT_FIRST_NAME);
            boolean patientLastNameSearch = searchType.equals(AlertConstants.PATIENT_LAST_NAME);

            if (patientIdSearch || patientFirstNameSearch || patientLastNameSearch) {
                String searchValue = StringUtils.trim(searchTextItem.getValueAsString());

                if (StringUtils.isBlank(searchValue)) {
                    SC.warn("Invalid entry. Please enter a valid search value and attempt the search again.");
                    return;
                }

                if (patientFirstNameSearch) {
                    alertSearchType = AlertSearchType.PATIENT_FIRST_NAME;
                    alertService.searchUserAlertsByPatientFirstName(userId, searchValue, 1, pageSize, this);
                } else if (patientLastNameSearch) {
                    alertSearchType = AlertSearchType.PATIENT_LAST_NAME;
                    alertService.searchUserAlertsByPatientLastName(userId, searchValue, 1, pageSize, this);
                } else if (patientIdSearch) {
                    alertSearchType = AlertSearchType.PATIENT_ID;
                    alertService.searchUserAlertsByPatientId(userId, searchValue, 1, pageSize, this);
                }
            } else if (searchType.equals(AlertConstants.SEVERITY)) {
                alertSearchType = AlertSearchType.SEVERITY;
                String searchValue = severitySelectItem.getValueAsString();

                if (StringUtils.isBlank(searchValue)) {
                    SC.warn("Invalid entry. Please enter a valid search value and attempt the search again.");
                    return;
                }

                alertService.searchUserAlertsBySeverity(userId, searchValue, 1, pageSize, this);
            } else if (searchType.equals(AlertConstants.APPLICATION)) {
                alertSearchType = AlertSearchType.APPLICATION;
                String searchValue = appNamesSelectItem.getValueAsString();

                if (StringUtils.isBlank(searchValue)) {
                    SC.warn("Invalid entry. Please enter a valid search value and attempt the search again.");
                    return;
                }

                alertService.searchUserAlertsByApplication(userId, searchValue, 1, pageSize, this);
            } else if (searchType.equals(AlertConstants.DATE_RECEIVED)) {
                alertSearchType = AlertSearchType.DATE_RECEIVED;

                Date fromDate = fromDateItem.getValueAsDate();
                Date toDate = toDateItem.getValueAsDate();

                if (fromDate == null || toDate == null) {
                    SC.warn("Invalid entry. Please enter a valid search value and attempt the search again.");
                    return;
                }

                if (toDate.before(fromDate)) {
                    SC.warn("<b>From Date</b> must come before <b>To Date</b>. Please enter a valid search value and attempt the search again.");
                    return;
                }

                alertService.searchUserAlertsByDate(userId, fromDate, toDate, 1, pageSize, this);
            }

        }

        @Override
        public void onCapture(SearchResults<UserAlertDTO> alertSearchResults) {

            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            List<UserAlertDTO> theAlerts = alertSearchResults.getData();
            int totalCount = alertSearchResults.getTotalCount();

            populate(theAlerts, totalCount);
        }

        @Override
        public void onFailure(Throwable thrwbl) {
            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            super.onFailure(thrwbl);
        }
    }

    class SearchByGetPageAlertsCallback extends RetryActionCallback<SearchResults<UserAlertDTO>> {

        private int pageNum;
        private int pageSize;

        SearchByGetPageAlertsCallback(int thePageNum, int thePageSize) {
            pageNum = thePageNum;
            pageSize = thePageSize;
        }

        @Override
        public void attempt() {

            if (pageNum != 1) {
                progressBarWindow.show();
            }

            if (searchMode) {

                String searchValue = null;

                switch (alertSearchType) {
                    case PATIENT_FIRST_NAME:
                        searchValue = searchTextItem.getValueAsString();

                        if (StringUtils.isBlank(searchValue)) {
                            SC.warn("Invalid entry. Please enter a valid search value and attempt the search again.");
                            return;
                        }

                        alertService.searchUserAlertsByPatientFirstName(userId, searchValue, pageNum, pageSize, this);
                        break;
                    case PATIENT_LAST_NAME:
                        searchValue = searchTextItem.getValueAsString();

                        if (StringUtils.isBlank(searchValue)) {
                            SC.warn("Invalid entry. Please enter a valid search value and attempt the search again.");
                            return;
                        }

                        alertService.searchUserAlertsByPatientLastName(userId, searchValue, pageNum, pageSize, this);
                        break;
                    case PATIENT_ID:
                        searchValue = searchTextItem.getValueAsString();

                        if (StringUtils.isBlank(searchValue)) {
                            SC.warn("Invalid entry. Please enter a valid search value and attempt the search again.");
                            return;
                        }

                        alertService.searchUserAlertsByPatientId(userId, searchValue, pageNum, pageSize, this);
                        break;
                    case SEVERITY:
                        searchValue = severitySelectItem.getValueAsString();

                        if (StringUtils.isBlank(searchValue)) {
                            SC.warn("Invalid entry. Please enter a valid search value and attempt the search again.");
                            return;
                        }

                        alertService.searchUserAlertsBySeverity(userId, searchValue, pageNum, pageSize, this);
                        break;
                    case APPLICATION:
                        searchValue = appNamesSelectItem.getValueAsString();

                        if (StringUtils.isBlank(searchValue)) {
                            SC.warn("Invalid entry. Please enter a valid search value and attempt the search again.");
                            return;
                        }

                        alertService.searchUserAlertsByApplication(userId, searchValue, pageNum, pageSize, this);
                        break;
                    case DATE_RECEIVED:
                        Date fromDate = fromDateItem.getValueAsDate();
                        Date toDate = toDateItem.getValueAsDate();

                        if (fromDate == null || toDate == null) {
                            SC.warn("Invalid entry. Please enter a valid search value and attempt the search again.");
                            return;
                        }

                        if (toDate.before(fromDate)) {
                            SC.warn("<b>From Date</b> must come before <b>To Date</b>. Please enter a valid search value and attempt the search again.");
                            return;
                        }

                        alertService.searchUserAlertsByDate(userId, fromDate, toDate, pageNum, pageSize, this);
                        break;
                }

            } else {
                alertService.getUserAlerts(userId, pageNum, pageSize, this);
            }

        }

        @Override
        public void onCapture(SearchResults<UserAlertDTO> alertSearchResults) {

            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            List<UserAlertDTO> theAlerts = alertSearchResults.getData();
            int totalCount = alertSearchResults.getTotalCount();
            Redux.jsEvent("local:gwtEvent:alertCount");

            populate(theAlerts, totalCount);
        }

        @Override
        public void onFailure(Throwable thrwbl) {
            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            super.onFailure(thrwbl);
        }
    }

    private List<UserAlertDTO> markRecordsForInvisible(ListGridRecord[] records) {
        List<UserAlertDTO> theAlerts = new ArrayList<UserAlertDTO>();

        for (ListGridRecord tempRecord : records) {
            UserAlertRecord userAlertRecord = (UserAlertRecord) tempRecord;
            int index = userAlertRecord.getIndex();
            UserAlertDTO tempAlert = alerts.get(index);
            tempAlert.setIsVisible(false);
            theAlerts.add(tempAlert);
        }

        return theAlerts;
    }

    class RemoveClickHandler implements ClickHandler {

        public void onClick(ClickEvent clickEvent) {

            ListGrid listGrid = alertListPanel.getListGrid();

            ListGridRecord[] records = listGrid.getSelectedRecords();

            if (records.length == 0) {
                SC.warn("At least one alert must be selected.  Please select an alert to delete and try again.");
                return;
            }

            String message = "Are you sure you want to remove the selected alert";

            if (records.length > 1) {
                message += "s";
            }

            message += ".";

            SC.ask("Remove Selected Alerts", message, new RemoveConfirmationCallback());



        }
    }

    class RemoveConfirmationCallback implements BooleanCallback {

        @Override
        public void execute(Boolean value) {

            if (value.booleanValue()) {
                ListGrid listGrid = alertListPanel.getListGrid();
                ListGridRecord[] records = listGrid.getSelectedRecords();

                // convert records to dtos
                List<UserAlertDTO> selectedAlerts = markRecordsForInvisible(records);

                alertService.updateUserAlerts(selectedAlerts, new RemoveAlertsCallback());



            }
        }
    }

    class RemoveAlertsCallback implements AsyncCallback {

        public void onSuccess(Object t) {
            gotoPage(1);

        }

        public void onFailure(Throwable thrwbl) {
            SC.warn("Error removing alerts");
        }
    }

    @Override
    public void destroy() {
        super.destroy();

        // remove our handler
        if (handlerRegistration != null) {
            handlerRegistration.removeHandler();
        }
    }

    class ListAlertEventHandler implements AlertEventHandler {

        @Override
        public void onAlert(AlertEvent alertEvent) {

            ListGrid listGrid = alertListPanel.getListGrid();
            final List<UserAlertDTO> theNewAlerts = alertEvent.getAlerts();

            // only populate GUI if nothing is selected in the list
            // prevents app from wiping out user selections
            if (!listGrid.anySelected() && !searchMode && alertListPanel.getCurrentPageNumber() == 1) {

                // get the proper sublist
                int endRecordNumber = Math.min(alertListPanel.getPageSize(), theNewAlerts.size());

                List<UserAlertDTO> sub = theNewAlerts.subList(0, endRecordNumber);

                if (!sub.equals(alerts)) {
                    gotoPage(1);
                }

                alerts = sub;
            }
        }
    }

    class SearchByStatusChangedHandler implements ChangedHandler {

        public void onChanged(ChangedEvent event) {

            clearSearchFields();
            
            String value = searchBySelectItem.getValueAsString();

            if (value.equals(AlertConstants.PATIENT_ID) || value.equals(AlertConstants.PATIENT_FIRST_NAME) || value.equals(AlertConstants.PATIENT_LAST_NAME)) {
                searchTextItem.show();

                fromDateItem.hide();
                toDateItem.hide();
                appNamesSelectItem.hide();
                severitySelectItem.hide();
            } else if (value.equals(AlertConstants.APPLICATION)) {
                appNamesSelectItem.show();
                setupApplicationMap();
                fromDateItem.hide();
                toDateItem.hide();
                severitySelectItem.hide();
                searchTextItem.hide();
            } else if (value.equals(AlertConstants.SEVERITY)) {
                severitySelectItem.show();

                fromDateItem.hide();
                toDateItem.hide();
                searchTextItem.hide();
                appNamesSelectItem.hide();
            } else if (value.equals(AlertConstants.DATE_RECEIVED)) {
                fromDateItem.show();
                toDateItem.show();

                searchTextItem.hide();
                appNamesSelectItem.hide();
                severitySelectItem.hide();
            } else {
                searchTextItem.show();

                fromDateItem.hide();
                toDateItem.hide();

                appNamesSelectItem.hide();
                severitySelectItem.hide();
            }
        }
    }

    protected void clearSearchFields() {
        // clear
        searchTextItem.clearValue();
        fromDateItem.clearValue();
        toDateItem.clearValue();
        appNamesSelectItem.clearValue();
        severitySelectItem.clearValue();
    }

    protected void resetSearchFields() {

        searchBySelectItem.setValue(SELECT_A_VALUE);
        searchTextItem.show();

        fromDateItem.hide();
        toDateItem.hide();
        appNamesSelectItem.hide();
        severitySelectItem.hide();

        // clear
        searchTextItem.clearValue();
        fromDateItem.clearValue();
        toDateItem.clearValue();
        appNamesSelectItem.clearValue();
        severitySelectItem.clearValue();

    }

    private void setSearchMode(boolean theMode) {

        searchMode = theMode;

        if (searchMode) {
            header.setContents("<h3>Alerts - <font color=red>Search Results</font></h3>");
        } else {
            header.setContents("<h3>Alerts</h3>");
        }
    }

    class SearchClickHandler implements FormItemClickHandler {

        @Override
        public void onFormItemClick(FormItemIconClickEvent event) {

            FormItemGetPageAlertsCallback formItemGetPageAlertsCallback  = new FormItemGetPageAlertsCallback();
            formItemGetPageAlertsCallback.attempt();
        }
    }

    class ClearClickHandler implements FormItemClickHandler {

        @Override
        public void onFormItemClick(FormItemIconClickEvent event) {
            setSearchMode(false);
            gotoPage(1);

            searchTextItem.show();
            searchBySelectItem.setValue(SELECT_A_VALUE);
            searchTextItem.clearValue();

            fromDateItem.hide();
            fromDateItem.clearValue();

            toDateItem.hide();
            toDateItem.clearValue();

            appNamesSelectItem.hide();
            appNamesSelectItem.setValue(ADMIN_APP_ID);

            severitySelectItem.hide();
            severitySelectItem.setValue(severityLevels[0]);
        }
    }

    class AlertPageEventHandler implements PageEventHandler {

        public void handlePage(int pageNum, int pageSize) {

            SearchByGetPageAlertsCallback searchByGetPageAlertsCallback = new SearchByGetPageAlertsCallback(pageNum, pageSize);
            searchByGetPageAlertsCallback.attempt();
        }
    }
}
