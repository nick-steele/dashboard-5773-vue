package com.gsihealth.dashboard.client.careplan;

import com.google.gwt.user.client.ui.FlowPanel;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.redux.Redux;
import com.smartgwt.client.widgets.layout.VLayout;

public class AppCareplanPanel extends VLayout {

    private static final int MAIN_CONTENT_POSITION = 1;

    public AppCareplanPanel() {
        buildUI();
        // Restart the activity timer (so the client doesn't logout)...
        DashBoardApp.restartTimer();
        // Notify Angular that we've drawn the task list panel (so Angular can inject the page)...
		
		// Build TREAT information for the iFrame...
		LoginResult loginResult = ApplicationContextUtils.getLoginResult();

        String treatUrl = loginResult.getTreatUrl();
        final String treatName = loginResult.getTreatName();
        final String treatFeatures = loginResult.getTreatFeatures();
        final boolean treatRestfulSSOEnabled = loginResult.isTreatRestfulSSOEnabled();
        System.out.println("Treat SSO Enabled Boolean: " +String.valueOf(treatRestfulSSOEnabled));

        if (treatRestfulSSOEnabled) {
            System.out.println("Treat SSO Enabled");
            //change url settings to use  POST to send tokenid to Messages App
            String protocol = com.google.gwt.user.client.Window.Location.getProtocol();
            String hostAndPort = com.google.gwt.user.client.Window.Location.getHost();

            String currentPage = com.google.gwt.user.client.Window.Location.getPath();

            String path = "";
            if (currentPage.startsWith("/dashboard")) {
                path = "/dashboard";
            }

            path += "/login/post_for_treat.jsp";
            String queryString = com.google.gwt.user.client.Window.Location.getQueryString();
            treatUrl = protocol + "//" + hostAndPort + path + queryString;
                System.out.println("Treat URL -"+ treatUrl );
        }

        //com.google.gwt.user.client.Window.open(treatUrl, treatName, treatFeatures);
        Redux.jsEvent("local:appCareplan:startup:" + treatUrl);
    }

    // Put things you need to build the initial UI here.
    private void buildUI() {       
        FlowPanel panel = new FlowPanel();	
		setHeight100();
        setWidth100();
        panel.getElement().setId("appCareplan");
        panel.getElement().setAttribute("dynamic", "app.html.careplan");
        addMember(panel);
        panel.setWidth("100%");
        panel.setHeight("100%");
    }
}
