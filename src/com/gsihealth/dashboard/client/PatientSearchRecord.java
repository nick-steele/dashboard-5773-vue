/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client;

import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 *
 * @author User
 */
public class PatientSearchRecord extends ListGridRecord {
  
    private int index;
    public static String LAST_NAME = "Last Name";
    public static String FIRST_NAME = "First Name";
    public static String MIDDLE_NAME = "Middle Name";
    public static String DOB = "DOB";
    public static String MINOR = "Minor";
    public static String GENDER = "Gender";
    public static String PATIENT_ID = "Patient ID";
    public static String CM_ORG = "CM Org";
    public static String ENROLLMENT_STATUS= "Enrollment Status";
    
  
    

    public PatientSearchRecord() {
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }
    
    public String getFirstName() {
        return getAttribute(FIRST_NAME);
    }

    public void setFirstName(String firstName) {
        setAttribute(FIRST_NAME, firstName);
    }

    public String getLastName() {
        return getAttribute(LAST_NAME);
    }

    public void setLastName(String lastName) {
        setAttribute(LAST_NAME, lastName);
    }
    
     public String getMiddleName() {
        return getAttribute(MIDDLE_NAME);
    }

    public void setMiddleName(String middleName) {
        setAttribute(MIDDLE_NAME, middleName);
    }

    public String getDOB() {
        return getAttribute(DOB);
    }

    public void setDOB(String dob) {
        setAttribute(DOB, dob);
    }
    
    
     public String getGender() {
        return getAttribute(GENDER);
    }

    public void setGender(String dob) {
        setAttribute(GENDER, dob);
    }
    
    
     public String getPatientId() {
        return getAttribute(PATIENT_ID);
    }

    public void setPatientId(String PatientId) {
        setAttribute(PATIENT_ID, PatientId);
    }
    
    
      public String getOrgId() {
        return getAttribute(CM_ORG);
    }

    public void setOrgId(String orgId) {
        setAttribute(CM_ORG, orgId);
    }
    
    
      public String getEnrollmentStatus() {
        return getAttribute(ENROLLMENT_STATUS);
    }

    public void getEnrollmentStatus(String status) {
        setAttribute(ENROLLMENT_STATUS, status);
    }
    
     public String getMinor() {
        return getAttribute(MINOR);
    }


    public void setMinor(String minor) {
        setAttribute(MINOR, minor);
    }
    
}
