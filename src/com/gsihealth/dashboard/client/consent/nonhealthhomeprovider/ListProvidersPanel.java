package com.gsihealth.dashboard.client.consent.nonhealthhomeprovider;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.common.GridPager;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.service.NonHealthHomeProviderService;
import com.gsihealth.dashboard.client.service.NonHealthHomeProviderServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.DateFieldCellFormatter;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderDTO;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceBooleanField;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.FetchMode;
import com.smartgwt.client.types.ListGridEditEvent;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class ListProvidersPanel extends VLayout {

    private static final int DATE_COLUMN = 10;
    private PersonDTO personDTO;
    private ListGrid listGrid;
    private Label label = null;
    private Button resetButton;
//    private GridPager pager;
    private NonHealthHomeProviderServiceAsync nonHealthHomeProviderService = GWT.create(NonHealthHomeProviderService.class);
    private ProgressBarWindow progressBarWindow;
//    private GetPageProvidersCallback getPageProvidersCallback;
    private MainPanel mainPanel;
    LoginResult loginResult = ApplicationContextUtils.getLoginResult();
    
    public ListProvidersPanel(MainPanel theMainPanel, PersonDTO thePersonDTO) {
        mainPanel = theMainPanel;
        personDTO = thePersonDTO;

        buildGui();

        progressBarWindow = new ProgressBarWindow("Loading", "Loading...");
        loadOutOfNetworkProviderData();
//        getPageProvidersCallback = new GetPageProvidersCallback();

//        gotoPage(1);
    }

    private void buildGui() {

        setPadding(5);
        setMembersMargin(5);

        Label label = new Label("<h3>List Providers</h3>");
        label.setWidth(300);
        label.setHeight(30);
        addMember(label);
        addMember(buildResetBar());

        listGrid = buildListGrid();

        setPadding(5);
        addMember(listGrid);
       
//        pager = new GridPager(0,100);
//        pager.setPageEventHandler(new ProviderPageEventHandler());
//        addMember(pager);

        // add double click support
        listGrid.addRecordDoubleClickHandler(new ProviderRecordDoubleClickHandler());        
        listGrid.addRecordClickHandler(new ProviderRecordClickHandler());        
    }
    
    
     private Layout buildResetBar() {
        // layout for button bar
        HLayout buttonLayout = new HLayout(10);
        buttonLayout.setWidth100();
        buttonLayout.setAlign(Alignment.CENTER);
//        buttonLayout.setHeight(25);
        label = new Label("");
//        label.setHeight(18);
        label.setWidth100();
        label.setAlign(Alignment.CENTER);
        buttonLayout.addMember(label);
        // reset button
        resetButton = new Button("Reset Filters");
        resetButton.setID("ResetFilters");
//        resetButton.setWidth(100);
//        resetButton.setHeight(20);
        resetButton.addClickHandler(new ClickHandler() {

            @Override                
                        

            public void onClick(ClickEvent event) {
         loadOutOfNetworkProviderData();
         label.setContents("");

            } });
        resetButton.setAlign(Alignment.CENTER);
        buttonLayout.addMember(resetButton);
        return buttonLayout;

     }
  

    /** Helper method to build a ListGrid
     *
     * @return
     */
    private ListGrid buildListGrid() {

        String[] columnNames = {ProviderConstants.LAST_NAME, ProviderConstants.FIRST_NAME, ProviderConstants.CARE_TEAM_ROLE,
            ProviderConstants.CREDENTIAL, ProviderConstants.SPECIALTY, ProviderConstants.PRIMARY_PRACTITIONER_FACILITY, ProviderConstants.STREET,
            ProviderConstants.CITY, ProviderConstants.STATE, ProviderConstants.ZIP_CODE,
            ProviderConstants.PHONE_NUMBER, ProviderConstants.CONSENT,
            ProviderConstants.ATTESTATION_DATE, ProviderConstants.RECORDED_BY};

        ListGrid theListGrid = new ListGrid();

        theListGrid.setCanReorderFields(true);
        theListGrid.setCanReorderRecords(true);

        theListGrid.setWidth100();
        theListGrid.setHeight100();

        theListGrid.setAlternateRecordStyles(true);
        theListGrid.setShowAllRecords(false);
        theListGrid.setSelectionType(SelectionStyle.SINGLE);

        theListGrid.setCanEdit(false);
        theListGrid.setDataFetchMode(FetchMode.LOCAL);
        theListGrid.setCanEdit(false);
        theListGrid.setAutoFetchData(true);  
        theListGrid.setShowFilterEditor(true);
        theListGrid.setFilterByCell(true);
        theListGrid.setFilterOnKeypress(true);
        theListGrid.setEditEvent(ListGridEditEvent.CLICK);
        theListGrid.setEditByCell(true);

        // add fields
        ListGridField[] fields = buildListGridFields(columnNames);
        theListGrid.setFields(fields);

        ListGridField dateField = fields[DATE_COLUMN];
        setDateFieldCellFormatter(dateField);

        theListGrid.setEmptyMessage("...");
        theListGrid.setCanResizeFields(true);

        return theListGrid;
    }

    private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];

        for (int i = 0; i < columnNames.length; i++) {
            String tempColumnName = columnNames[i];
            ListGridField tempField = buildListGridField(tempColumnName);
            fields[i] = tempField;
        }

        return fields;
    }

    private ListGridField buildListGridField(String title) {
        ListGridField tempField = new ListGridField(title, title);

        tempField.setCanEdit(false);

        return tempField;
    }

    /**
     * Add cell formatter for date field
     *
     * @param dateField
     */
    private void setDateFieldCellFormatter(ListGridField dateField) {

        final DateTimeFormat dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
        dateField.setCellFormatter(new DateFieldCellFormatter(dateFormatter));
    }

//    public void gotoPage(final int pageNumber) {
//
//        long patientId = personDTO.getPatientEnrollment().getPatientId();
//        nonHealthHomeProviderService.getTotalCountOfProvidersByCommunityId(loginResult.getCommunityId(), new PortalAsyncCallback<Integer>() {
//
//            public void onSuccess(Integer total) {
//
//                if (total > 0) {
//                    pager.setTotalCount(total);
//                    pager.refreshPageLabels(pageNumber);
//                    pager.gotoPage(pageNumber);
//                } else {
//                    clearListGrid();
//                }
//            }
//        });
//    }

    public void clearListGrid() {
        ListGridRecord[] records = new ListGridRecord[1];
        listGrid.setData(records);

//        pager.clearPageLabels();
    }

//    class ProviderPageEventHandler implements PageEventHandler {
//
//        public void handlePage(int pageNum, int pageSize) {
//            
//            progressBarWindow.show();
//            long patientId = personDTO.getPatientEnrollment().getPatientId();
//            nonHealthHomeProviderService.getProvidersByCommunityId(loginResult.getCommunityId(), patientId, pageNum, pageSize, getPageProvidersCallback);
//        }
//    }

     public void loadOutOfNetworkProviderData() {
       
       LoadOutOfNetworkProviderDataCallback loadOutOfNetworkProviderDataCallback = new LoadOutOfNetworkProviderDataCallback();
        loadOutOfNetworkProviderDataCallback.attempt();
    }
     
     class LoadOutOfNetworkProviderDataCallback extends RetryActionCallback<List<NonHealthHomeProviderDTO>> {
        
        @Override
        public void attempt() {
            long patientId = personDTO.getPatientEnrollment().getPatientId();
            long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
            nonHealthHomeProviderService.getProvidersByCommunityId(loginResult.getCommunityId(), patientId, this);
          
        }
        
        @Override
        public void onCapture(List<NonHealthHomeProviderDTO> data) {
           
            if (!data.isEmpty()) {
            ListGridRecord[] records = NonHealthHomeProviderPropertyUtils.convert(data);
            populateListGrid(records);

//            int currentPageNumber = pager.getCurrentPageNumber();
//            pager.refreshPageLabels(currentPageNumber);
//
//            pager.handleNavigationButtons(currentPageNumber);
        } else {
            clearListGrid();
        
        }
    } }
     
//    class GetPageProvidersCallback implements AsyncCallback<SearchResults<NonHealthHomeProviderDTO>> {
//
//        public void onSuccess(SearchResults<NonHealthHomeProviderDTO> searchResults) {
//
//            if (progressBarWindow != null) {
//                progressBarWindow.hide();
//            }
//
//            List<NonHealthHomeProviderDTO> theProviders = searchResults.getData();
//            int totalCount = searchResults.getTotalCount();
//            populate(theProviders, totalCount);
//        }
//
//        public void onFailure(Throwable thrwbl) {
//            if (progressBarWindow != null) {
//                progressBarWindow.hide();
//            }
//
//            SC.say("Error loading providers. Try again.");
//        }
//    }

    protected void populate(List<NonHealthHomeProviderDTO> theProviders) {

        if (!theProviders.isEmpty()) {
            ListGridRecord[] records = NonHealthHomeProviderPropertyUtils.convert(theProviders);
            populateListGrid(records);

//            int currentPageNumber = pager.getCurrentPageNumber();
//            pager.refreshPageLabels(currentPageNumber);
//
//            pager.handleNavigationButtons(currentPageNumber);
        } else {
            clearListGrid();
        }
    }

    public void populateListGrid(ListGridRecord[] pagedData) {

        // scroll to first row
        listGrid.scrollToRow(1);
      
         final DataSource dataSource = new DataSource();
        dataSource.setID("dataSource");
        dataSource.setClientOnly(true);
        DataSourceTextField lastName = new DataSourceTextField(ProviderConstants.LAST_NAME, "Last Name");
        DataSourceTextField firstName = new DataSourceTextField(ProviderConstants.FIRST_NAME, "First Name");
        DataSourceTextField careTeamRole = new DataSourceTextField(ProviderConstants.CARE_TEAM_ROLE, "Care Team Role");
        DataSourceTextField credential = new DataSourceTextField(ProviderConstants.CREDENTIAL, "Credential");
        DataSourceTextField specialty = new DataSourceTextField(ProviderConstants.SPECIALTY, "Specialty");
        DataSourceTextField primaryPractitionerFacility = new DataSourceTextField(ProviderConstants.PRIMARY_PRACTITIONER_FACILITY, "Primary Practitioner Facility");
        DataSourceTextField Street = new DataSourceTextField(ProviderConstants.STREET, "Street");
        DataSourceTextField city = new DataSourceTextField(ProviderConstants.CITY, "City");
        DataSourceTextField state = new DataSourceTextField(ProviderConstants.STATE, "State");
        DataSourceTextField zipCode = new DataSourceTextField(ProviderConstants.ZIP_CODE, "Zip Code");
        DataSourceTextField phoneNumber = new DataSourceTextField(ProviderConstants.PHONE_NUMBER, "Phone Number");
        DataSourceTextField consent = new DataSourceTextField(ProviderConstants.CONSENT, "Consent");
        DataSourceTextField attestationDate = new DataSourceTextField(ProviderConstants.ATTESTATION_DATE, "Attestation Date");
        DataSourceTextField recordedBy = new DataSourceTextField(ProviderConstants.RECORDED_BY, "Recorded By");


        dataSource.setFields(lastName, firstName, careTeamRole, credential,specialty,primaryPractitionerFacility,Street,city,state,zipCode,phoneNumber,consent,attestationDate,recordedBy);
        dataSource.setCacheData(pagedData);
        listGrid.setDataSource(dataSource);

        listGrid.setData(pagedData);
        listGrid.markForRedraw();
//        pager.setTotalCount(totalCount);
    }
    
    public NonHealthHomeProviderDTO getSelectedProvider() {
        NonHealthHomeProviderDTO provider = null;
        ListGridRecord tempRecord = listGrid.getSelectedRecord();

        if (tempRecord instanceof NonHealthHomeProviderRecord) {
            NonHealthHomeProviderRecord record = (NonHealthHomeProviderRecord) listGrid.getSelectedRecord();
            if (record != null) {
                provider = record.getProvider();

            }
        } else {
            NonHealthHomeProviderDTO tempUser = (NonHealthHomeProviderDTO) tempRecord.getAttributeAsObject("theDTO");
            provider = tempUser;

        }
        return provider;
    }

    /**
     * For each record clicked, populate form w/ user data
     */
    class ProviderRecordDoubleClickHandler implements RecordDoubleClickHandler {

        @Override
        public void onRecordDoubleClick(RecordDoubleClickEvent event) {
            mainPanel.showUpdateProviderPanel();            
        }
    }

    /**
     * For each record clicked, populate form w/ user data
     */
    class ProviderRecordClickHandler implements RecordClickHandler {

        public void onRecordClick(RecordClickEvent event) {
            mainPanel.enableRemoveProviderButton(true);
            mainPanel.enableUpdateProviderButton(true);
        }

    }
    
}
