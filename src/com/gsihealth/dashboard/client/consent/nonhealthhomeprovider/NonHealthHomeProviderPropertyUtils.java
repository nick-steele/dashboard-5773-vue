package com.gsihealth.dashboard.client.consent.nonhealthhomeprovider;

import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderDTO;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class NonHealthHomeProviderPropertyUtils {

    public static NonHealthHomeProviderRecord[] convert(List<NonHealthHomeProviderDTO> theProviders) {
        NonHealthHomeProviderRecord[] records = new NonHealthHomeProviderRecord[theProviders.size()];
        
        int index = 0;
        for (NonHealthHomeProviderDTO tempProvider : theProviders) {
            records[index] = convert(tempProvider);
            index++;
        }
        
        return records;
    }

    
    /**
     * Convert DTO to record
     * 
     * @param index
     * @param providerDTO
     * @return 
     */
    public static NonHealthHomeProviderRecord convert(NonHealthHomeProviderDTO providerDTO) {
        
        NonHealthHomeProviderRecord record = new NonHealthHomeProviderRecord(providerDTO);
        
        return record;
    }
    
    
}
