package com.gsihealth.dashboard.client.consent.nonhealthhomeprovider;

import com.gsihealth.dashboard.common.ConsentConstants;

/**
 *
 * @author Chad Darby
 */
public interface ProviderConstants {
    
    public static final String PREFIX = "Prefix";
    public static final String LAST_NAME = "Last Name";
    public static final String FIRST_NAME = "First Name";
    public static final String CREDENTIAL = "Credential";
    public static final String STREET = "Street";
    public static final String CITY = "City";
    public static final String STATE = "State";
    public static final String ZIP_CODE = "Zip Code";

    public static final String PHONE_NUMBER = "Phone Number";
    public static final String EMAIL = "Email";
    public static final String CONSENT = "Consent";
    
    public static final String ATTESTATION_DATE = "Consent Date";
    public static final String RECORDED_BY = "Recorded By";
    public static final String ASSIGNED_STATUS= "Assigned";
    public static final String[] CONSENT_STATUS= {ConsentConstants.CONSENT_PERMIT, ConsentConstants.CONSENT_DENY};
    public static final String DIRECT_ADDRESS="Direct Address";
    public static final String CARE_TEAM_ROLE="Care Team Role";
    public static final String SPECIALTY="Specialty";
    public static final String PRIMARY_PRACTITIONER_FACILITY="Primary Pracititioner Facility";
    public static final String NPI="NPI";
    
}
