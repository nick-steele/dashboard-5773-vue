package com.gsihealth.dashboard.client.consent.nonhealthhomeprovider;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.service.NonHealthHomeProviderService;
import com.gsihealth.dashboard.client.service.NonHealthHomeProviderServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.common.Constants;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderAssignedPatientDTO;
import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderDTO;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;
import java.util.Date;

/**
 *
 * @author Chad Darby
 */
public class MainPanel extends VLayout {

    private static int MAIN_CONTENT_POSITION = 1;
    private PersonDTO personDTO;
    private ToolStripButton listProvidersButton;
    private ToolStripButton addProviderButton;
    private ToolStripButton updateProviderButton;
    private ToolStripButton removeProviderButton;
    private ToolStripButton assignProviderButton;
    private ToolStripButton unassignProviderButton;
    private ListProvidersPanel listProvidersPanel;
    private NonHealthHomeProviderServiceAsync nonHealthHomeProviderService = GWT.create(NonHealthHomeProviderService.class);
    private boolean countFlag= false;

    public MainPanel(PersonDTO thePersonDTO) {
        personDTO = thePersonDTO;

        buildGui();
        DashBoardApp.restartTimer();

        // show providers list by default
        showListProvidersPanel();
    }

    private void buildGui() {

        ToolStrip toolStrip = createToolStrip();
        addMember(toolStrip);

        setMargin(5);
    }

    protected ToolStrip createToolStrip() {
        listProvidersButton = new ToolStripButton("List Providers");
        addProviderButton = new ToolStripButton("Add Provider");
        updateProviderButton = new ToolStripButton("Update Provider");
        removeProviderButton = new ToolStripButton("Remove Provider");
        assignProviderButton = new ToolStripButton("Assign Provider");
        unassignProviderButton = new ToolStripButton("Unassign Provider");

        ToolStrip toolStrip = new ToolStrip();
        toolStrip.setWidth100();
        toolStrip.addButton(listProvidersButton);
        toolStrip.addSeparator();
        toolStrip.addButton(addProviderButton);
        toolStrip.addSeparator();
        toolStrip.addButton(updateProviderButton);
        toolStrip.addSeparator();
        toolStrip.addButton(removeProviderButton);
        toolStrip.addSeparator();
        toolStrip.addButton(assignProviderButton);
        toolStrip.addSeparator();
        toolStrip.addButton(unassignProviderButton);

        listProvidersButton.addClickHandler(new ListProvidersClickHandler());
        addProviderButton.addClickHandler(new AddProviderClickHandler());
        updateProviderButton.addClickHandler(new UpdateProviderClickHandler());
        removeProviderButton.addClickHandler(new RemoveProviderClickHandler());
        assignProviderButton.addClickHandler(new AssignProviderClickHandler());
        unassignProviderButton.addClickHandler(new UnassignProviderClickHandler());

        return toolStrip;
    }

    private void removeMainContent() {

        Canvas[] members = getMembers();

        if (members.length > 1) {
            Canvas mainContent = members[MAIN_CONTENT_POSITION];
            this.removeMember(mainContent);
        }
    }

    class ListProvidersClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            showListProvidersPanel();
        }
    }

    public void showListProvidersPanel() {

        updateProviderButton.disable();
        removeProviderButton.disable();
        
        listProvidersPanel = new ListProvidersPanel(this, personDTO);

        removeMainContent();
        addMember(listProvidersPanel);
    }

    class AddProviderClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            updateProviderButton.disable();
            removeProviderButton.disable();
            removeMainContent();
            addMember(new AddProviderPanel(personDTO));
        }
    }

    class UpdateProviderClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            NonHealthHomeProviderDTO provider = listProvidersPanel.getSelectedProvider();

            if (provider == null) {
                SC.warn("A provider must be selected.  Please select a provider and try again.");
                return;
            }

            showUpdateProviderPanel();
        }
    }
    
    class AssignProviderClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            NonHealthHomeProviderDTO provider = listProvidersPanel.getSelectedProvider();

            if (provider == null) {
                SC.warn("A provider must be selected.  Please select a provider and try again.");
                return;
            } 
            
            if (null != provider.getConsentStatus() && provider.getConsentStatus().equals(Constants.PERMIT)) {
                SC.warn("The selected provider is already assigned.");
                return;
            }

            showAssignProviderPanel(provider);
        }
    }
    
    class UnassignProviderClickHandler implements ClickHandler {
        public void onClick(ClickEvent event) {

            NonHealthHomeProviderDTO provider = listProvidersPanel.getSelectedProvider();

            if (provider == null) {
                SC.warn("A provider must be selected.  Please select a provider and try again.");
                return;
            }
            
            if (provider.getConsentStatus().equals(Constants.DENY)) {
                SC.warn("The selected provider is not assigned.");
                return;
            }

            
            provider.setPatientId(personDTO.getPatientEnrollment().getPatientId());
            // prompt the user
            String message = "Are you sure you want to uassign provider " + provider.getFirstName() + " " + provider.getLastName() +"?";
            
            SC.confirm("UnAssign Provider", message, new UnAssignConfirmationCallback(provider));
        }
    }
    
    public void showAssignProviderPanel(NonHealthHomeProviderDTO dTO) {
       NonHealthHomeProviderDTO provider = dTO;

       removeMainContent();
       
       addMember(new AssignProviderPanel(provider, personDTO));
        
    }

    public void showUpdateProviderPanel() {

        removeProviderButton.disable();

        NonHealthHomeProviderDTO provider = listProvidersPanel.getSelectedProvider();

        removeMainContent();

        addMember(new UpdateProviderPanel(this, personDTO, provider));
    }
    
    class RemoveProviderClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            NonHealthHomeProviderDTO provider = listProvidersPanel.getSelectedProvider();
            
            if (provider == null) {
                SC.warn("A provider must be selected.  Please select a provider and try again.");
                return;
            }
            nonHealthHomeProviderService.getCountofPatientsAssignedToProvider(provider.getProviderId(), provider.getCommunityId(), new RemoveCallback(provider));
           
            countFlag= false;
            
           
        }
    }

    class RemoveConfirmationCallback implements BooleanCallback {

        NonHealthHomeProviderDTO provider;

        RemoveConfirmationCallback(NonHealthHomeProviderDTO theProvider) {
            provider = theProvider;
        }

        @Override
        public void execute(Boolean value) {

            // if the user said yes, then remove provider
            if (value.booleanValue()) {
                long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
                
                nonHealthHomeProviderService.removeProvider(provider,communityId, new RemoveAlertsCallback());
            }
        }
    }

    class RemoveAlertsCallback implements AsyncCallback {

        @Override
        public void onSuccess(Object t) {
            listProvidersPanel.loadOutOfNetworkProviderData();

        }

        @Override
        public void onFailure(Throwable thrwbl) {
            SC.warn("Error removing provider.");
        }
    }
    
    public void enableUpdateProviderButton(boolean flag) {
        if (flag) {
            updateProviderButton.enable();
        }
        else {
            updateProviderButton.hide();
        }
    }

    public void enableRemoveProviderButton(boolean flag) {
        if (flag) {
            removeProviderButton.enable();
        }
        else {
            removeProviderButton.hide();
        }
    }
    
    class UnAssignConfirmationCallback implements BooleanCallback {

        NonHealthHomeProviderDTO provider;

        UnAssignConfirmationCallback(NonHealthHomeProviderDTO theProvider) {
            provider = theProvider;
        }

        @Override
        public void execute(Boolean value) {

            // if the user said yes, then remove provider         
            
            if (value.booleanValue()) {
                
               nonHealthHomeProviderService.unAssignProvider(getAssignedDTO(provider.getProviderId(), provider.getPatientId(), provider.getCommunityId()), new UnAssignCallback());
            }
        }  
        
    }
    
     class UnAssignCallback implements AsyncCallback {

        @Override
        public void onSuccess(Object t) {
            listProvidersPanel.loadOutOfNetworkProviderData();

        }

        @Override
        public void onFailure(Throwable thrwbl) {
            SC.warn("Error unassigning provider.");
        }
    }
     
     class RemoveCallback implements AsyncCallback<Integer> {

        NonHealthHomeProviderDTO provider;

        RemoveCallback(NonHealthHomeProviderDTO theProvider) {
            provider = theProvider;
        }

        public void onFailure(Throwable thrwbl) {
           
        }

        public void onSuccess(Integer t) {

            if(Constants.PERMIT.equalsIgnoreCase(provider.getConsentStatus())) {
                // prompt the user
            String message = "The provider " + provider.getFirstName() + " " + provider.getLastName() +" is assigned. Please unassign before removing.";
            
            SC.say(message);
            return;
            } else if (t > 0) {
                
                SC.say("The provider is assigned to "+t+" other patients. Hence, it cannot be removed unless all the patients are unassigned.");
                return;
            }
            
             // prompt the user
            String message = "Are you sure you want to remove provider " + provider.getFirstName() + " " + provider.getLastName() +"?";
            
            SC.confirm("Remove Provider", message, new RemoveConfirmationCallback(provider));
        }

       
    }
     
     private NonHealthHomeProviderAssignedPatientDTO getAssignedDTO(long providerId, long patientId, long communityId) {
    NonHealthHomeProviderAssignedPatientDTO assignedPatientDTO = new NonHealthHomeProviderAssignedPatientDTO();
        
        
        assignedPatientDTO.setConsentDateTime(new Date());
        assignedPatientDTO.setConsentStatus(Constants.DENY);
        assignedPatientDTO.setConsentObtainedByUserId(ApplicationContextUtils.getLoginResult().getUserId());
        assignedPatientDTO.setCommunityId(communityId);
        assignedPatientDTO.setPatientId(patientId);
        assignedPatientDTO.setProviderId(providerId);
        
        return assignedPatientDTO;   
}
    
}
