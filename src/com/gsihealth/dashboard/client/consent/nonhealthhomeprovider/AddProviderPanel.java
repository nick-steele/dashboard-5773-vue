package com.gsihealth.dashboard.client.consent.nonhealthhomeprovider;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.callbacks.BasicPortalAsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.service.NonHealthHomeProviderService;
import com.gsihealth.dashboard.client.service.NonHealthHomeProviderServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 * @author Chad Darby
 */
public class AddProviderPanel extends VLayout {

    private static final int FORM_WIDTH = 800;
    
    private PersonDTO personDTO;
    private ProviderForm providerForm;
    private final NonHealthHomeProviderServiceAsync providerService = GWT.create(NonHealthHomeProviderService.class);

    public AddProviderPanel(PersonDTO thePersonDTO) {
        personDTO = thePersonDTO;

        buildGui();
    }

    private void buildGui() {

        setPadding(5);
        setMembersMargin(5);

        Label label = new Label("<h3>Add Provider</h3>");
        label.setWidth(200);
        label.setHeight(30);
        addMember(label);

        providerForm = new ProviderForm(personDTO);
        addMember(providerForm);

        Layout buttonBar = buildButtonBar();
        addMember(buttonBar);
        this.setOverflow(Overflow.AUTO);
    }

    private Layout buildButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        buttonLayout.setPadding(10);

        buttonLayout.setWidth(FORM_WIDTH);
        buttonLayout.setAlign(Alignment.CENTER);

        Button saveButton = new Button("Save");
        Button cancelButton = new Button("Cancel");
        buttonLayout.addMember(saveButton);
        buttonLayout.addMember(cancelButton);

        // register handler
        saveButton.addClickHandler(new AddButtonHandler());
        cancelButton.addClickHandler(new CancelButtonHandler());

        return buttonLayout;
    }

    class AddButtonHandler implements ClickHandler {

        NonHealthHomeProviderDTO nonHealthHomeProviderDTO = new NonHealthHomeProviderDTO();

        public void onClick(ClickEvent event) {
            if (providerForm.validate()) {
                nonHealthHomeProviderDTO = providerForm.getProvider();
                long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
                

                ProgressBarWindow progressBarWindow = new ProgressBarWindow();
                progressBarWindow.show();
                String confirmationMessage = "Save successful.";
                providerService.addProvider(nonHealthHomeProviderDTO, personDTO.getPatientEnrollment().getPatientId(),communityId, new AddProviderCallback(progressBarWindow, confirmationMessage));
            }

        }
    }

    class CancelButtonHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            providerForm.clearErrors();
            providerForm.clearValues();
            providerForm.setConsentObtainedByToCurrentUser();
        }
    }

    class AddProviderCallback extends BasicPortalAsyncCallback {

        public AddProviderCallback(ProgressBarWindow theProgressBarWindow, String confirmationMessage) {
            super(theProgressBarWindow, confirmationMessage);
        }

        @Override
        public void onSuccess(Object obj) {
            super.onSuccess(obj);
            providerForm.clearValues();
            providerForm.setConsentObtainedByToCurrentUser();
        }
    }
}
