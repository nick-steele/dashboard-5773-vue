package com.gsihealth.dashboard.client.consent.nonhealthhomeprovider;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.common.FormUtils;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.service.*;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.common.dto.UserFormReferenceData;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderDTO;
import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.validator.DateRangeValidator;
import com.smartgwt.client.widgets.form.validator.LengthRangeValidator;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.layout.VLayout;
import com.gsihealth.dashboard.client.util.*;
import java.util.*;

/**
 *
 * @author Satyendra Singh
 */
public class ProviderForm extends VLayout {

    public static final int FORM_WIDTH = 800;
    public static final String DATE_ITEM = "dateItem";
    private DynamicForm demographicForm;
    private DynamicForm consentForm;
    private TextItem firstNameTextItem;
    private TextItem lastNameTextItem;
    private DateItem attestationDateItem;
    private SelectItem prefixSelectItem;
    private TextItem address1TextItem;
    private TextItem address2TextItem;
    private TextItem phoneTextItem;
    private TextItem emailTextItem;
    private TextItem cityTextItem;
    private SelectItem credentialsSelectItem;
    private SelectItem stateSelectItem;
    private TextItem zipcodeTextItem;
    private SelectItem consentSelectItem;
    private SelectItem recordedBySelectItem;
    private RegExpValidator theAlphaNumberSpecialSymbolsRegExpValidator;
    private RegExpValidator theAlphaOnlyRegExpValidator;
    private RegExpValidator theAlphaHGAARegExValidator;
    private LengthRangeValidator phoneValidator;
    private RegExpValidator emailRegExpValidator;
    private LengthRangeValidator zipCodeValidator;
    private DateRangeValidator rangeDateValidator;
    private NonHealthHomeProviderDTO nonHealthHomeProviderDTO;
    private PersonDTO personDTO;
    private CareteamServiceAsync careteamService = GWT.create(CareteamService.class);
    private LinkedHashMap<String, String> availableUsersMap;
    private String oldProviderEmail;
    private DateTimeFormat dateFormatter;
    private TextItem specialtyTextItem;
    private ReferenceDataServiceAsync referenceDataService = GWT.create(ReferenceDataService.class);
    private AdminServiceAsync adminService = GWT.create(AdminService.class);
    private LinkedHashMap<String, String> roleList;
    private SelectItem userLevelSelectItem;
    private TextItem practitionerFacilityTextItem;
    private TextItem directAddressTextItem;
    private TextItem NPITextItem;
    private String oldNPI;
    LoginResult loginResult = ApplicationContextUtils.getLoginResult();

    public ProviderForm(PersonDTO thePersonDTO) {
        personDTO = thePersonDTO;

        dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");

        availableUsersMap = new LinkedHashMap<String, String>();
        buildGui();

        loadProvidersForConsentObtainedBy();
        // loadUserRoles();
    }

    private void buildGui() {

        setMembersMargin(10);

        // Demographic form
        demographicForm = buildDemographicForm();

        // Consent form
        consentForm = buildConsentForm();
        // displaying consent form for add provider panel.
        consentForm.setVisible(true);

        // add Demographic form
        addMember(demographicForm);

        // add Consent form
        addMember(consentForm);
    }

    public void clearErrors() {
        demographicForm.clearErrors(true);
        consentForm.clearErrors(true);
    }

    public void clearValues() {
        demographicForm.clearValues();
        consentForm.clearValues();
    }

    private DynamicForm buildDemographicForm() {
        DynamicForm theDemographicForm = new DynamicForm();
        theDemographicForm.setGroupTitle("Demographics");
        theDemographicForm.setHeight(160);
        theDemographicForm.setWidth(FORM_WIDTH);
        theDemographicForm.setCellPadding(2);
        theDemographicForm.setNumCols(6);
        theDemographicForm.setIsGroup(true);

        theDemographicForm.setRequiredTitlePrefix(FormUtils.REQUIRED_TITLE_PREFIX);
        theDemographicForm.setBrowserSpellCheck(false);

        // validators 
        theAlphaOnlyRegExpValidator = ValidationUtils.getAlphaOnlyRegExpValidator();
        theAlphaHGAARegExValidator = ValidationUtils.getAlphaHGAARegExValidator();
        theAlphaNumberSpecialSymbolsRegExpValidator = ValidationUtils.getAlphaNumberSpecialSymbolsRegExpValidator();

        prefixSelectItem = new SelectItem("prefixSelectItem", "Prefix");
        prefixSelectItem.setEndRow(false);
        prefixSelectItem.setValueMap(ApplicationContextUtils.getUserPrefixCodes());
        prefixSelectItem.setWrapTitle(false);
        prefixSelectItem.setTabIndex(1);
        prefixSelectItem.setWidth(90);
        prefixSelectItem.setAllowEmptyValue(true);

        address1TextItem = new TextItem("address1TextItem", "Address 1");
        address1TextItem.setTabIndex(6);
        address1TextItem.setWrapTitle(false);
        address1TextItem.setRequired(true);
        ValidationUtils.setValidators(address1TextItem, theAlphaNumberSpecialSymbolsRegExpValidator, true);

        phoneTextItem = new TextItem("phoneTextItem", "Phone Number");
        phoneTextItem.setTabIndex(11);
        phoneTextItem.setWrapTitle(false);
        phoneTextItem.setEndRow(true);
        phoneTextItem.setMask("###-###-####");
        phoneValidator = new LengthRangeValidator();
        phoneValidator.setMax(10);
        phoneValidator.setMin(10);
        phoneValidator.setErrorMessage("Invalid length");
        phoneTextItem.setValidators(phoneValidator);

        firstNameTextItem = new TextItem("fNameTextItem", "First Name");
        firstNameTextItem.setTabIndex(2);
        firstNameTextItem.setWrapTitle(false);
        firstNameTextItem.setRequired(true);
        ValidationUtils.setValidators(firstNameTextItem, theAlphaHGAARegExValidator, true);

        address2TextItem = new TextItem("address2TextItem", "Address 2");
        address2TextItem.setTabIndex(7);
        address2TextItem.setWrapTitle(false);
        ValidationUtils.setValidators(address2TextItem, theAlphaNumberSpecialSymbolsRegExpValidator, true);

        emailTextItem = new TextItem("emailTextItem", "Email");
        emailTextItem.setTabIndex(12);
        emailTextItem.setWrapTitle(false);
        emailTextItem.setRequired(false);
        emailTextItem.setEndRow(true);
        emailRegExpValidator = getEmailRegExpValidator();
        emailTextItem.setValidateOnExit(true);
        ValidationUtils.setValidators(emailTextItem, emailRegExpValidator, true);

        lastNameTextItem = new TextItem("lNameTextItem", "Last Name");
        lastNameTextItem.setTabIndex(3);
        lastNameTextItem.setWrapTitle(false);
        lastNameTextItem.setRequired(true);
        ValidationUtils.setValidators(lastNameTextItem, theAlphaHGAARegExValidator, true);

        cityTextItem = new TextItem("cityTextItem", "City");
        cityTextItem.setTabIndex(8);
        cityTextItem.setWrapTitle(false);
        cityTextItem.setRequired(true);
        ValidationUtils.setValidators(cityTextItem, theAlphaOnlyRegExpValidator, true);

        credentialsSelectItem = new SelectItem("credentialsSelectItem", "Credential");
        credentialsSelectItem.setEndRow(false);
        credentialsSelectItem.setValueMap(ApplicationContextUtils.getUserCredentialCodes());
        credentialsSelectItem.setWrapTitle(false);
        credentialsSelectItem.setTabIndex(13);
        credentialsSelectItem.setWidth(90);
        credentialsSelectItem.setRequired(false);
        credentialsSelectItem.setAllowEmptyValue(true);

        stateSelectItem = new SelectItem("stateSelectItem", "State");
        stateSelectItem.setEndRow(false);
        stateSelectItem.setValueMap(ApplicationContextUtils.getStateCodes());
        stateSelectItem.setWrapTitle(false);
        stateSelectItem.setTabIndex(4);
        stateSelectItem.setWidth(90);
        stateSelectItem.setRequired(true);
        stateSelectItem.setEndRow(false);

        zipcodeTextItem = new TextItem("zipcodeTextItem", "Zip Code");
        zipcodeTextItem.setTabIndex(9);
        zipcodeTextItem.setWrapTitle(false);
        zipcodeTextItem.setRequired(true);
        zipcodeTextItem.setEndRow(false);
        String zipCodeMask = "#####";
        zipcodeTextItem.setMask(zipCodeMask);
        zipCodeValidator = new LengthRangeValidator();
        zipCodeValidator.setMax(5);
        zipCodeValidator.setMin(5);
        zipCodeValidator.setErrorMessage("Invalid length");
        zipcodeTextItem.setValidators(zipCodeValidator);


        userLevelSelectItem = new SelectItem("userLevelSelectItem", "Care Team Role");
        userLevelSelectItem.setWrapTitle(false);
        userLevelSelectItem.setValue(FormUtils.EMPTY);
        userLevelSelectItem.setRequired(true);
        userLevelSelectItem.setTabIndex(5);

        LinkedHashMap<String, String> userRolesMap = buildUserRolesMap(ApplicationContextUtils.getUserRoles());
        userLevelSelectItem.setValueMap(userRolesMap);


        specialtyTextItem = new TextItem("specialtyTextItem", "Specialty");
        specialtyTextItem.setTabIndex(14);
        specialtyTextItem.setWrapTitle(false);
        //specialtyTextItem.setRequired(true);
        ValidationUtils.setValidators(specialtyTextItem, theAlphaOnlyRegExpValidator, true);

        practitionerFacilityTextItem = new TextItem("practitionerFacilityTextItem", "Primary Pracitioner Facility");
        practitionerFacilityTextItem.setTabIndex(10);
        practitionerFacilityTextItem.setWrapTitle(false);


        directAddressTextItem = new TextItem("directAddressTextItem", "Direct Address");
        directAddressTextItem.setTabIndex(15);
        directAddressTextItem.setWrapTitle(false);
        directAddressTextItem.setEndRow(true);

        NPITextItem = new TextItem("NPITextItem", "NPI");
        NPITextItem.setTabIndex(16);
        NPITextItem.setWrapTitle(false);
        NPITextItem.setEndRow(true);
        NPITextItem.setRequired(true);

        // validation for 10 digits
        String npiCodeMask = "##########";
        NPITextItem.setMask(npiCodeMask);
        LengthRangeValidator npiValidator = new LengthRangeValidator();
        npiValidator.setMax(10);
        npiValidator.setMin(10);
        npiValidator.setErrorMessage("Length must be 10 digit");
        NPITextItem.setValidators(npiValidator);

        SpacerItem spacerItem1 = new SpacerItem();
        spacerItem1.setWidth(90);
        spacerItem1.setColSpan(2);

        theDemographicForm.setFields(prefixSelectItem, address1TextItem, phoneTextItem, firstNameTextItem, address2TextItem, emailTextItem, lastNameTextItem, cityTextItem, credentialsSelectItem, stateSelectItem, zipcodeTextItem, specialtyTextItem, userLevelSelectItem, practitionerFacilityTextItem, directAddressTextItem, spacerItem1, NPITextItem);

        return theDemographicForm;
    }

    private DynamicForm buildConsentForm() {

        DynamicForm theConsentForm = new DynamicForm();
        theConsentForm.setGroupTitle("Consent");
        theConsentForm.setNumCols(6);
        theConsentForm.setIsGroup(true);
        theConsentForm.setWidth(FORM_WIDTH);
        theConsentForm.setHeight(60);
        theConsentForm.setCellPadding(2);

        theConsentForm.setRequiredTitlePrefix(FormUtils.REQUIRED_TITLE_PREFIX);
        theConsentForm.setBrowserSpellCheck(false);

        consentSelectItem = new SelectItem("consentSelectItem", "Consent");
        consentSelectItem.setValueMap(ProviderConstants.CONSENT_STATUS);
        consentSelectItem.setWrapTitle(false);
        consentSelectItem.setTabIndex(11);
        //consentSelectItem.setWidth(90);
        

        attestationDateItem = new DateItem(DATE_ITEM, "Consent Date");
        attestationDateItem.setTabIndex(12);
        attestationDateItem.setEndRow(false);
        attestationDateItem.setWrapTitle(false);
        //attestationDateItem.setTextAlign(Alignment.LEFT);
        attestationDateItem.setUseMask(true);
        attestationDateItem.setEnforceDate(true);
        attestationDateItem.setDisplayFormat(DateDisplayFormat.TOUSSHORTDATE);
        //attestationDateItem.setWidth(90);
        attestationDateItem.setStartDate(dateFormatter.parse("01/01/1900"));



        // add date validation rules
        rangeDateValidator = new DateRangeValidator();
        Date today = new java.util.Date();
        today.setHours(23);
        today.setMinutes(59);
        today.setSeconds(59);
        rangeDateValidator.setMax(today);

        rangeDateValidator.setErrorMessage("Date must be today's date or prior.");
        attestationDateItem.setValidators(rangeDateValidator);
        attestationDateItem.setValidateOnChange(true);

        recordedBySelectItem = new SelectItem("RecordedBySelectItem", "Recorded by");
        recordedBySelectItem.setEndRow(true);
        recordedBySelectItem.setWrapTitle(false);
        recordedBySelectItem.setTabIndex(13);
        //recordedBySelectItem.setWidth(150);
        
        theConsentForm.setFields(consentSelectItem, attestationDateItem, recordedBySelectItem);

        return theConsentForm;
    }

    private RegExpValidator getEmailRegExpValidator() {
        RegExpValidator theValidator = new RegExpValidator();
        theValidator.setExpression(Constants.EMAIL_VALIDATION_REGEX);
        theValidator.setErrorMessage("Email must be a valid email address.");
        return theValidator;
    }

    public String getOldProviderEmail() {
        return oldProviderEmail;
    }

    public void setOldProviderEmail(String email) {
        oldProviderEmail = email;
    }
    
    public String getOldNPI() {
        return oldNPI;
    }

    public void populate(NonHealthHomeProviderDTO theProvider) {
        // setting consent form values to not required.
        
                
        oldProviderEmail = theProvider.getEmail();
        oldNPI = theProvider.getNPI();
        nonHealthHomeProviderDTO = theProvider;

        // demographics
        firstNameTextItem.setValue(nonHealthHomeProviderDTO.getFirstName());
        lastNameTextItem.setValue(nonHealthHomeProviderDTO.getLastName());
        attestationDateItem.setValue(nonHealthHomeProviderDTO.getConsentDateTime());
        prefixSelectItem.setValue(nonHealthHomeProviderDTO.getTitle());
        address1TextItem.setValue(nonHealthHomeProviderDTO.getStreetAddress1());
        address2TextItem.setValue(nonHealthHomeProviderDTO.getStreetAddress2());
        phoneTextItem.setValue(nonHealthHomeProviderDTO.getTelephoneNumber());
        emailTextItem.setValue(nonHealthHomeProviderDTO.getEmail());
        cityTextItem.setValue(nonHealthHomeProviderDTO.getCity());
        credentialsSelectItem.setValue(nonHealthHomeProviderDTO.getCredentials());
        stateSelectItem.setValue(nonHealthHomeProviderDTO.getState());
        zipcodeTextItem.setValue(nonHealthHomeProviderDTO.getZipCode());

        // consent
        consentSelectItem.setValue(nonHealthHomeProviderDTO.getConsentStatus());
        attestationDateItem.setValue(nonHealthHomeProviderDTO.getConsentDateTime());

        // consentSelectItem.setDisabled(true);

        loadProvidersForConsentObtainedBy();
        // Long userId = nonHealthHomeProviderDTO.getConsentObtainedByUserId();
        Long userId = ApplicationContextUtils.getLoginResult().getUserId();
        String userName = availableUsersMap.get(Constants.DELIMITER_SELECT_ITEM + userId.toString());

        recordedBySelectItem.setValueMap(availableUsersMap);
        recordedBySelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + userId.toString());

        specialtyTextItem.setValue(nonHealthHomeProviderDTO.getSpecialty());
        practitionerFacilityTextItem.setValue(nonHealthHomeProviderDTO.getPrimaryPracitionerFacility());
        directAddressTextItem.setValue(nonHealthHomeProviderDTO.getDirectAddress());
        NPITextItem.setValue(nonHealthHomeProviderDTO.getNPI());

        String role = nonHealthHomeProviderDTO.getRole();
        if (role != null) {
            userLevelSelectItem.setValue(Long.parseLong(role));
        }

        // hiding consent form for update provider panel.
             
        consentForm.setVisible(false);
    }

    public NonHealthHomeProviderDTO getProvider() {
        if (nonHealthHomeProviderDTO == null) {
            nonHealthHomeProviderDTO = new NonHealthHomeProviderDTO();
        }

        // demographics
        nonHealthHomeProviderDTO.setFirstName(firstNameTextItem.getValueAsString());
        nonHealthHomeProviderDTO.setLastName(lastNameTextItem.getValueAsString());
        nonHealthHomeProviderDTO.setTitle(prefixSelectItem.getValueAsString());
        nonHealthHomeProviderDTO.setStreetAddress1(address1TextItem.getValueAsString());
        nonHealthHomeProviderDTO.setStreetAddress2(address2TextItem.getValueAsString());
        nonHealthHomeProviderDTO.setTelephoneNumber(phoneTextItem.getValueAsString());
        nonHealthHomeProviderDTO.setEmail(emailTextItem.getValueAsString());
        nonHealthHomeProviderDTO.setCity(cityTextItem.getValueAsString());
        nonHealthHomeProviderDTO.setCredentials(credentialsSelectItem.getValueAsString());
        nonHealthHomeProviderDTO.setState(stateSelectItem.getValueAsString());
        nonHealthHomeProviderDTO.setZipCode(zipcodeTextItem.getValueAsString());

        // consent
        nonHealthHomeProviderDTO.setConsentStatus(consentSelectItem.getValueAsString());
        nonHealthHomeProviderDTO.setConsentDateTime(attestationDateItem.getValueAsDate());

        // get consent obtained by
        String recordedByStr = recordedBySelectItem.getValueAsString();

        if (StringUtils.isNotBlank(recordedByStr)) {
            // convert from delimited format
            String str = recordedByStr;
            recordedByStr = recordedByStr.split(Constants.DELIMITER_SELECT_ITEM)[1];
            nonHealthHomeProviderDTO.setConsentObtainedByUserId(Long.parseLong(recordedByStr));
        } else {
            nonHealthHomeProviderDTO.setConsentObtainedByUserId(Constants.EMPTY_USER);
        }
        String specialty = specialtyTextItem.getValueAsString();
        nonHealthHomeProviderDTO.setSpecialty(specialty);

        String primaryPracitionerFacility = practitionerFacilityTextItem.getValueAsString();
        nonHealthHomeProviderDTO.setPrimaryPracitionerFacility(primaryPracitionerFacility);

        String directAddress = directAddressTextItem.getValueAsString();
        nonHealthHomeProviderDTO.setDirectAddress(directAddress);

        String role = userLevelSelectItem.getValueAsString();
        nonHealthHomeProviderDTO.setRole(role);

        // patient Id
        nonHealthHomeProviderDTO.setPatientId(personDTO.getPatientEnrollment().getPatientId());

        // communityId same as user's communityId
        nonHealthHomeProviderDTO.setCommunityId(loginResult.getCommunityId());

        nonHealthHomeProviderDTO.setNPI(NPITextItem.getValueAsString());

        return nonHealthHomeProviderDTO;
    }

    public boolean validate() {
        boolean validDemographics = demographicForm.validate();
        boolean validConsents = consentForm.validate();

        return validDemographics && validConsents;
    }

    private void loadProvidersForConsentObtainedBy() {

        boolean cachedLocally = isCachedProvidersForConsentObtainedBy();

        if (cachedLocally) {
            List<UserDTO> users = ApplicationContextUtils.getProvidersForConsentObtainedBy();
            availableUsersMap = buildUsersMap(users);
            recordedBySelectItem.setValueMap(availableUsersMap);

            setConsentObtainedByToCurrentUser();
        } else {
            // get from the server
            careteamService.getAvailableUsers(new GetAvailableUsersCallback());
        }
    }

    public void loadUserRoles() {

        adminService.getUserFormReferenceData(new GetUserFormReferenceDataCallback());
    }

    private boolean isCachedProvidersForConsentObtainedBy() {
        ApplicationContext context = ApplicationContext.getInstance();

        boolean cached = context.containsKey(Constants.PROVIDERS_FOR_CONSENT_OBTAINED_BY);

        return cached;
    }

    class GetAvailableUsersCallback implements AsyncCallback<List<UserDTO>> {

        public void onSuccess(List<UserDTO> users) {
            if (!users.isEmpty()) {
                availableUsersMap = buildUsersMap(users);

                recordedBySelectItem.setValueMap(availableUsersMap);

                setConsentObtainedByToCurrentUser();

                // update the cache
                ApplicationContext context = ApplicationContext.getInstance();
                context.putAttribute(Constants.PROVIDERS_FOR_CONSENT_OBTAINED_BY, users);

            } else {
                SC.warn("Providers not found.");
            }
        }

        public void onFailure(Throwable exc) {
            SC.warn("Error loading providers.");
        }
    }

    private void populateConsentObtainedBy(PatientEnrollmentDTO patient) {
        final long consentObtainedByUserId = patient.getConsentObtainedByUserId();

        long theUserId;

        if (consentObtainedByUserId > Constants.EMPTY_USER) {
            theUserId = consentObtainedByUserId;
        } else {
            // default to current user
            LoginResult loginResult = ApplicationContextUtils.getLoginResult();
            theUserId = loginResult.getUserId();
        }

        recordedBySelectItem.setValue(Long.toString(theUserId));
    }

    protected LinkedHashMap<String, String> buildUsersMap(List<UserDTO> users) {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

        for (UserDTO tempUser : users) {

            String userId = tempUser.getUserId().toString();
            String label = tempUser.getLastName() + ", " + tempUser.getFirstName();
            data.put(Constants.DELIMITER_SELECT_ITEM + userId, label.toString());

        }

        return data;
    }

    public void setConsentObtainedByToCurrentUser() {
        // default to current user
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        long theUserId = loginResult.getUserId();
        recordedBySelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + theUserId);
    }

    class GetUserFormReferenceDataCallback extends RetryActionCallback<UserFormReferenceData> {

        @Override
        public void attempt() {
            adminService.getUserFormReferenceData(this);
        }

        @Override
        public void onCapture(UserFormReferenceData userFormReferenceData) {
            // load roles
            LinkedHashMap<String, String> theRoles = userFormReferenceData.getRoles();
            populateRoles(theRoles);

        }
    }

    private void populateRoles(LinkedHashMap<String, String> data) {

        roleList = data;
        userLevelSelectItem.setValueMap(data);
    }

    private LinkedHashMap<String, String> buildUserRolesMap(LinkedHashMap<Long, String> baseData) {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

        for (Long tempKey : baseData.keySet()) {
            String tempValue = baseData.get(tempKey);

            String theKey = Long.toString(tempKey);
            data.put(theKey, tempValue);
        }

        return data;
    }
}
