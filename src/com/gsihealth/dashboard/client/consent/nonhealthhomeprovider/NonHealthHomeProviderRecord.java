package com.gsihealth.dashboard.client.consent.nonhealthhomeprovider;

import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import java.util.Map;

/**
 *
 * @author Chad Darby
 */
public class NonHealthHomeProviderRecord extends ListGridRecord {

    private NonHealthHomeProviderDTO provider;
    private Map<Long, String> roleMap = ApplicationContextUtils.getUserRoles();
    
    public NonHealthHomeProviderRecord(NonHealthHomeProviderDTO theProvider) {
        provider = theProvider;       
 
        setAttribute(ProviderConstants.PREFIX, provider.getTitle());
        setAttribute(ProviderConstants.FIRST_NAME, provider.getFirstName());
        setAttribute(ProviderConstants.LAST_NAME, provider.getLastName());
        setAttribute(ProviderConstants.CREDENTIAL, provider.getCredentials());
        
        setAttribute(ProviderConstants.SPECIALTY, provider.getSpecialty());
        setAttribute(ProviderConstants.PRIMARY_PRACTITIONER_FACILITY, provider.getPrimaryPracitionerFacility());
        setAttribute(ProviderConstants.STREET, getCompleteStreetAddress());
        setAttribute(ProviderConstants.CITY, provider.getCity());
        setAttribute(ProviderConstants.STATE, provider.getState());
        setAttribute(ProviderConstants.ZIP_CODE, provider.getZipCode());

        setAttribute(ProviderConstants.PHONE_NUMBER, provider.getTelephoneNumber());

        setAttribute(ProviderConstants.EMAIL, provider.getEmail());
        setAttribute(ProviderConstants.CONSENT, provider.getConsentStatus());
        setAttribute(ProviderConstants.ATTESTATION_DATE, provider.getConsentDateTime());
        
        setAttribute(ProviderConstants.RECORDED_BY, provider.getConsentObtainedByName());
        
        setAttribute(ProviderConstants.DIRECT_ADDRESS, provider.getDirectAddress());
        setAttribute(ProviderConstants.NPI, provider.getNPI());
        
        String roleId= provider.getRole();
        String roleName="";
        if(roleId!= null) {
            roleName = getRoleName(roleId);
        }
        setAttribute(ProviderConstants.CARE_TEAM_ROLE, roleName);
        setAttribute("theDTO", theProvider);
        
    }

    public String getCompleteStreetAddress() {
        
        StringBuilder completeStreetAddress = new StringBuilder(provider.getStreetAddress1());
        
        String streetAddress2 = provider.getStreetAddress2();
        
        if (StringUtils.isNotBlank(streetAddress2)) {
            completeStreetAddress.append(", " + streetAddress2);
        }
        
        return completeStreetAddress.toString();
    }

    public NonHealthHomeProviderDTO getProvider() {

        return provider;
    }
    
    public String getRoleName(String id) {
        String roleName = roleMap.get(Long.parseLong(id));
        
        return roleName;
    }
}
