package com.gsihealth.dashboard.client.consent.nonhealthhomeprovider;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.service.NonHealthHomeProviderService;
import com.gsihealth.dashboard.client.service.NonHealthHomeProviderServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 * @author Chad Darby
 */
public class UpdateProviderPanel extends VLayout {

    private static final int FORM_WIDTH = 800;
    
    private PersonDTO personDTO;
    private ProviderForm providerForm;
    private NonHealthHomeProviderDTO provider;
    private final NonHealthHomeProviderServiceAsync providerService = GWT.create(NonHealthHomeProviderService.class);
    private MainPanel mainPanel;

    public UpdateProviderPanel(MainPanel theMainPanel, PersonDTO thePersonDTO, NonHealthHomeProviderDTO theProvider) {
        mainPanel = theMainPanel;
        personDTO = thePersonDTO;
        provider = theProvider;
        
        buildGui();
    }

    private void buildGui() {

        setPadding(5);
        setMembersMargin(5);

        Label label = new Label("<h3>Update Provider</h3>");
        label.setWidth(300);
        label.setHeight(30);
        addMember(label);

        providerForm = new ProviderForm(personDTO);
        providerForm.populate(provider);
        addMember(providerForm);

        Layout buttonBar = buildButtonBar();
        addMember(buttonBar);
        this.setOverflow(Overflow.AUTO);
    }

    private Layout buildButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        buttonLayout.setPadding(10);

        buttonLayout.setWidth(FORM_WIDTH);
        buttonLayout.setAlign(Alignment.CENTER);

        Button saveButton = new Button("Save");
        Button cancelButton = new Button("Cancel");
        buttonLayout.addMember(saveButton);
        buttonLayout.addMember(cancelButton);

        saveButton.addClickHandler(new SaveButtonHandler());
        cancelButton.addClickHandler(new CancelButtonHandler());

        return buttonLayout;
    }

    class SaveButtonHandler implements ClickHandler {

        NonHealthHomeProviderDTO nonHealthHomeProviderDTO = new NonHealthHomeProviderDTO();

        public void onClick(ClickEvent event) {
            if (providerForm.validate()) {
          
                nonHealthHomeProviderDTO = providerForm.getProvider();
                
                ProgressBarWindow progressBarWindow = new ProgressBarWindow();
                progressBarWindow.show();
                String confirmationMessage = "Save successful.";
                
                String oldProviderEmail = providerForm.getOldProviderEmail();
                String newProviderEmail = nonHealthHomeProviderDTO.getEmail();
                boolean checkForDuplicateEmail = !StringUtils.equals(newProviderEmail, oldProviderEmail);
                boolean checkForDuplicateNPI = !StringUtils.equalsIgnoreCase(providerForm.getOldNPI(), nonHealthHomeProviderDTO.getNPI());

                long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
                              
                providerService.updateProvider(nonHealthHomeProviderDTO, checkForDuplicateEmail, checkForDuplicateNPI, communityId,new UpdateProviderCallback(progressBarWindow, confirmationMessage, newProviderEmail));
            }
        }
    }

    class UpdateProviderCallback implements AsyncCallback {

        ProgressBarWindow progressBarWindow;
        String message;
        String newProviderEmail;
        
        UpdateProviderCallback(ProgressBarWindow theProgressBarWindow, String theMessage, String theNewProviderEmail) {
            progressBarWindow = theProgressBarWindow;
            message = theMessage;
            newProviderEmail = theNewProviderEmail;
        }

        public void onSuccess(Object t) {
            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            providerForm.setOldProviderEmail(newProviderEmail);
            
            SC.say(message);
        }

        public void onFailure(Throwable thrwbl) {
            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            SC.warn(thrwbl.getMessage());
        }
    }

    class CancelButtonHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            mainPanel.showListProvidersPanel();
        }
    }
}