/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.consent.nonhealthhomeprovider;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.callbacks.BasicPortalAsyncCallback;
import com.gsihealth.dashboard.client.common.FormUtils;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.service.CareteamService;
import com.gsihealth.dashboard.client.service.CareteamServiceAsync;
import com.gsihealth.dashboard.client.service.NonHealthHomeProviderService;
import com.gsihealth.dashboard.client.service.NonHealthHomeProviderServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderAssignedPatientDTO;
import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.validator.DateRangeValidator;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author Satyendra Singh
 */
public class AssignProviderPanel extends VLayout {
    private NonHealthHomeProviderDTO providerDTO;
    private PersonDTO personDTO;
    private DynamicForm form;
    private SelectItem consentSelectItem;
    private DateItem attestationDateItem;
    private SelectItem recordedBySelectItem;
    private SelectItem userLevelSelectItem;
    public static final String DATE_ITEM = "dateItem";
    public static final int FORM_WIDTH = 800;
    private LinkedHashMap<String, String> availableUsersMap;
    private LinkedHashMap<String, String> roleList;
    private CareteamServiceAsync careteamService = GWT.create(CareteamService.class);
    private final NonHealthHomeProviderServiceAsync providerService = GWT.create(NonHealthHomeProviderService.class);
    private LoginResult loginResult;
    LinkedHashMap<String, String> userRolesMap;
    
    public AssignProviderPanel(NonHealthHomeProviderDTO theProviderDTO, PersonDTO thePersonDTO) {
        providerDTO = theProviderDTO;
        personDTO = thePersonDTO;
        buildGui();
        populate();
    }
    
    public void buildGui() {
        
        loginResult = ApplicationContextUtils.getLoginResult();
        
        setPadding(5);
        setMembersMargin(5);

        Label label = new Label("<h3>Assign Provider</h3>");
        label.setWidth(200);
        label.setHeight(30);
        addMember(label);

        form = getAssignProviderForm();
        addMember(form);

        Layout buttonBar = buildButtonBar();
        addMember(buttonBar);
        this.setOverflow(Overflow.AUTO);
    }
    
    private DynamicForm getAssignProviderForm() {
        
        DynamicForm theConsentForm = new DynamicForm();
        theConsentForm.setGroupTitle("Consent");
        theConsentForm.setNumCols(6);
        theConsentForm.setIsGroup(true);
        theConsentForm.setWidth(FORM_WIDTH);
        theConsentForm.setHeight(60);
        theConsentForm.setCellPadding(2);

        theConsentForm.setRequiredTitlePrefix(FormUtils.REQUIRED_TITLE_PREFIX);
        theConsentForm.setBrowserSpellCheck(false);

        DateTimeFormat dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
        
        consentSelectItem = new SelectItem("consentSelectItem", "Consent");
        consentSelectItem.setValueMap(ProviderConstants.CONSENT_STATUS);
        consentSelectItem.setDefaultValue(ProviderConstants.CONSENT_STATUS[0]);
        consentSelectItem.setDisabled(true);
        consentSelectItem.setWrapTitle(false);
        consentSelectItem.setTabIndex(11);
        consentSelectItem.setWidth(90);
        consentSelectItem.setRequired(true);
        
     
        
        attestationDateItem = new DateItem(DATE_ITEM, "Consent Date");
        attestationDateItem.setTabIndex(12);
        attestationDateItem.setEndRow(false);
        attestationDateItem.setWrapTitle(false);
        attestationDateItem.setTextAlign(Alignment.LEFT);
        attestationDateItem.setUseMask(true);
        attestationDateItem.setEnforceDate(true);
        attestationDateItem.setDisplayFormat(DateDisplayFormat.TOUSSHORTDATE);
        attestationDateItem.setWidth(90);
        attestationDateItem.setRequired(true);
        attestationDateItem.setStartDate(dateFormatter.parse("01/01/1900"));
    
       

        // add date validation rules
        DateRangeValidator rangeDateValidator = new DateRangeValidator();
        Date today = new java.util.Date();
        today.setHours(23);
        today.setMinutes(59);
        today.setSeconds(59);
        rangeDateValidator.setMax(today);

        rangeDateValidator.setErrorMessage("Date must be today's date or prior.");
        attestationDateItem.setValidators(rangeDateValidator);
        attestationDateItem.setValidateOnChange(true);

        recordedBySelectItem = new SelectItem("RecordedBySelectItem", "Recorded by");
        recordedBySelectItem.setEndRow(false);
        recordedBySelectItem.setWrapTitle(false);
        recordedBySelectItem.setTabIndex(13);
        recordedBySelectItem.setWidth(150);
        recordedBySelectItem.setRequired(true);
        
        userLevelSelectItem = new SelectItem("userLevelSelectItem", "Care Team Role");
        userLevelSelectItem.setWrapTitle(false);
        userLevelSelectItem.setValue(FormUtils.EMPTY);
        userLevelSelectItem.setRequired(true);
        userLevelSelectItem.setTabIndex(14);
        
        userRolesMap = buildUserRolesMap(ApplicationContextUtils.getUserRoles());
        userLevelSelectItem.setValueMap(userRolesMap);
        

        theConsentForm.setFields(consentSelectItem, attestationDateItem, recordedBySelectItem,userLevelSelectItem);

        return theConsentForm;
    }
    
    private Layout buildButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        buttonLayout.setPadding(10);

        buttonLayout.setWidth(FORM_WIDTH);
        buttonLayout.setAlign(Alignment.CENTER);

        Button saveButton = new Button("Save");
        Button cancelButton = new Button("Cancel");
        buttonLayout.addMember(saveButton);
        buttonLayout.addMember(cancelButton);

        // register handler
        saveButton.addClickHandler(new AddButtonHandler());
        cancelButton.addClickHandler(new CancelButtonHandler());

        return buttonLayout;
    }
    
    public NonHealthHomeProviderAssignedPatientDTO getAssingPatientDTO() {
        NonHealthHomeProviderAssignedPatientDTO assignedPatientDTO = new NonHealthHomeProviderAssignedPatientDTO();
        
        assignedPatientDTO.setConsentDateTime(attestationDateItem.getValueAsDate());
        assignedPatientDTO.setConsentStatus(consentSelectItem.getValueAsString());
         String recordedByStr = recordedBySelectItem.getValueAsString();

        if (StringUtils.isNotBlank(recordedByStr)) {
            // convert from delimited format
            String str = recordedByStr;
            recordedByStr = recordedByStr.split(Constants.DELIMITER_SELECT_ITEM)[1];
            assignedPatientDTO.setConsentObtainedByUserId(Long.parseLong(recordedByStr));
        } else {
            assignedPatientDTO.setConsentObtainedByUserId(Constants.EMPTY_USER);
        }
        
        assignedPatientDTO.setCommunityId(providerDTO.getCommunityId());
        assignedPatientDTO.setPatientId(providerDTO.getPatientId());
        assignedPatientDTO.setProviderId(providerDTO.getProviderId());
        
        String roleId = userLevelSelectItem.getValueAsString();
        assignedPatientDTO.setRoleId(roleId);
        assignedPatientDTO.setRoleName(userRolesMap.get(Long.parseLong(roleId)));
        
        return assignedPatientDTO;   
    }
    
    private void populate() {
        loadProvidersForConsentObtainedBy();
        Long userId = ApplicationContextUtils.getLoginResult().getUserId();
         NonHealthHomeProviderAssignedPatientDTO assignedPatientDTO = new NonHealthHomeProviderAssignedPatientDTO();

        
        String userName = availableUsersMap.get(Constants.DELIMITER_SELECT_ITEM + userId.toString());

        recordedBySelectItem.setValueMap(availableUsersMap);
        recordedBySelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + userId.toString());
        //recordedBySelectItem.setValueMap(availableUsersMap);
       // recordedBySelectItem.setValue(userId);
        
        String role = assignedPatientDTO.getRoleId();
        if (role != null) {
            userLevelSelectItem.setValue(Long.parseLong(role));
        }

    }
    
     private void loadProvidersForConsentObtainedBy() {

        boolean cachedLocally = isCachedProvidersForConsentObtainedBy();

        if (cachedLocally) {
            List<UserDTO> users = ApplicationContextUtils.getProvidersForConsentObtainedBy();
            availableUsersMap = buildUsersMap(users);
            recordedBySelectItem.setValueMap(availableUsersMap);

            setConsentObtainedByToCurrentUser();
        } else {
            // get from the server
            careteamService.getAvailableUsers(new GetAvailableUsersCallback());
        }
    }

    private boolean isCachedProvidersForConsentObtainedBy() {
        ApplicationContext context = ApplicationContext.getInstance();

        boolean cached = context.containsKey(Constants.PROVIDERS_FOR_CONSENT_OBTAINED_BY);

        return cached;
    }

    class GetAvailableUsersCallback implements AsyncCallback<List<UserDTO>> {

        public void onSuccess(List<UserDTO> users) {
            if (!users.isEmpty()) {
                availableUsersMap = buildUsersMap(users);
                recordedBySelectItem.setValueMap(availableUsersMap);

                setConsentObtainedByToCurrentUser();

                // update the cache
                ApplicationContext context = ApplicationContext.getInstance();
                context.putAttribute(Constants.PROVIDERS_FOR_CONSENT_OBTAINED_BY, users);

            } else {
                SC.warn("Providers not found.");
            }
        }

        public void onFailure(Throwable thrwbl) {
            throw new UnsupportedOperationException("Not supported yet.");
        }

}
    
    protected LinkedHashMap<String, String> buildUsersMap(List<UserDTO> users) {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

        for (UserDTO tempUser : users) {
            String userId = tempUser.getUserId().toString();
            String label = tempUser.getLastName() + " " + tempUser.getFirstName();
            data.put(Constants.DELIMITER_SELECT_ITEM + userId, label.toString());
        }

        return data;
    }

    public void setConsentObtainedByToCurrentUser() {
        // default to current user
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        long theUserId = loginResult.getUserId();
        recordedBySelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + theUserId);
    }
    
    class AddButtonHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            if (form.validate()) {
                long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
                               
                NonHealthHomeProviderAssignedPatientDTO assignedPatientDTO = getAssingPatientDTO();
                assignedPatientDTO.setPatientId(personDTO.getPatientEnrollment().getPatientId());
                assignedPatientDTO.setCommunityId(communityId);
                ProgressBarWindow progressBarWindow = new ProgressBarWindow();
                progressBarWindow.show();
                String confirmationMessage = "Save successful.";
                
                providerService.assignProvider(assignedPatientDTO, new AssignProviderCallback(progressBarWindow, confirmationMessage));
            }

        }
    }

    class CancelButtonHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            form.clearErrors(true);
            form.clearValues();
            setConsentObtainedByToCurrentUser();
        }
    }
    
    class AssignProviderCallback extends BasicPortalAsyncCallback {

        public AssignProviderCallback(ProgressBarWindow theProgressBarWindow, String confirmationMessage) {
            super(theProgressBarWindow, confirmationMessage);
        }

        @Override
        public void onSuccess(Object obj) {
            super.onSuccess(obj);
            form.clearValues();
            setConsentObtainedByToCurrentUser();
        }
    }
     private void populateRoles(LinkedHashMap<String, String> data) {

        roleList = data;
        userLevelSelectItem.setValueMap(data);
    }
     private LinkedHashMap<String, String> buildUserRolesMap(LinkedHashMap<Long, String> baseData) {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

        for (Long tempKey : baseData.keySet()) {
            String tempValue = baseData.get(tempKey);

            String theKey = Long.toString(tempKey);
            data.put(theKey, tempValue);
        }

        return data;
    }
}