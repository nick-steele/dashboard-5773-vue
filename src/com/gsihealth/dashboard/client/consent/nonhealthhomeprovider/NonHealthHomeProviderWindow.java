package com.gsihealth.dashboard.client.consent.nonhealthhomeprovider;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.smartgwt.client.widgets.Window;

/**
 *
 * @author Chad Darby
 */
public class NonHealthHomeProviderWindow extends Window {
    
    private PersonDTO personDTO;
    
    public NonHealthHomeProviderWindow(PersonDTO thePersonDTO) {
        personDTO = thePersonDTO;
        
        buildGui();
    }
    
    private void buildGui() {
        DashBoardApp.restartTimer();

        setWindowProps();
        
        MainPanel mainPanel = new MainPanel(personDTO);
        addItem(mainPanel);        
    }
    
    private void setWindowProps() {
        setPadding(10);
        setWidth(1080);
        setHeight(650);
        
        String patientName = personDTO.getFirstName() + " " + personDTO.getLastName();
        setTitle("Add/Assign Out of Network Providers :" + patientName);
        setShowMinimizeButton(false);
        setIsModal(true);
        setShowModalMask(true);
        
        centerInPage();
    }
    
}
