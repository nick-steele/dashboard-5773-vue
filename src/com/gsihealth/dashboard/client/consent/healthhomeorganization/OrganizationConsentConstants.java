/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.consent.healthhomeorganization;

/**
 *
 * @author Chad Darby
 */
public interface OrganizationConsentConstants {
    
    public static final String COMMUNITY_ID = "Community Id";
    public static final String ORGANIZATION_NAME = "Organization Name";
     public static final String ORGANIZATION_ID = "Organization Id";
    public static final String CITY = "City";
    public static final String STATE = "State";
    public static final String PERMIT_CONSENT = "Consented";
    public static final String ACTION = "Consent Action";
       public static final String SHOW_CONSENTED = "Show Consented";
       public static final String SHOW_ALL = "Show All";
}
