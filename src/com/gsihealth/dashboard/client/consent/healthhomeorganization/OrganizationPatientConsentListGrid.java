/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.consent.healthhomeorganization;

import com.gsihealth.dashboard.client.WindowManager;
import com.gsihealth.dashboard.client.common.DashboardTitles;
import com.gsihealth.dashboard.client.enrollment.EnrollmentPanel;
import com.gsihealth.dashboard.client.enrollment.ViewPatientPanel;
import com.gsihealth.dashboard.client.mypatientlist.MyPatientListConstants;
import com.gsihealth.dashboard.client.mypatientlist.MyPatientListGrid;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.entity.dto.OrganizationPatientConsentDTO;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;

/**
 *
 * @author rsharan
 */
public class OrganizationPatientConsentListGrid extends ListGrid {
private Label label=null;
private String MSG = "<font color='red'>You must click the 'Save Consent' button to save any changes you have made.</font>";
    public OrganizationPatientConsentListGrid() {
        System.out.println("OPC............................");

    }

    @Override
    protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {

        System.out.println("OPC" + colNum);
        String fieldName = this.getFieldName(colNum);
        System.out.println("OPC2" + fieldName);
        if (fieldName.equals(OrganizationConsentConstants.ACTION)) {
            System.out.println("OPC3" + fieldName);

            String consentStatus = record.getAttributeAsString(OrganizationConsentConstants.PERMIT_CONSENT);

            HLayout recordCanvas = null;

            String buttonText = null;

            recordCanvas = new HLayout(5);

            recordCanvas.setHeight(22);
            recordCanvas.setWidth100();
            recordCanvas.setAlign(Alignment.LEFT);
            if (consentStatus.equals("true")) {
                buttonText = "Deny";
            } else {
                buttonText = "Permit";
            }
            System.out.println("OPC4" + buttonText);
            Button permit = createGridButton(buttonText);
            permit.addClickHandler(new ConsentClickHandler(record, consentStatus));

            recordCanvas.addMember(permit);

            return recordCanvas;

        } else {
            return null;
        }
    }

    private Button createGridButton(String permit) {

        Button theButton = new Button();
        theButton.setShowDown(false);
        theButton.setShowRollOver(false);
        theButton.setAlign(Alignment.LEFT);
        theButton.setHeight(18);
        theButton.setWidth(56);
        theButton.setTitle(permit);

        return theButton;
    }

    class ConsentClickHandler implements ClickHandler {

        private ListGridRecord record;
        private String consentStatus;

        ConsentClickHandler(ListGridRecord record, String consentStatus) {
            this.record = record;
            this.consentStatus = consentStatus;
            System.out.println("OPC5.......................");
        }

        public void onClick(ClickEvent event) {
            Button b = (Button) event.getSource();
            ListGridRecord[] listGridRecords = getRecords();
            DataSource dataSource = getDataSource();
            int rowNo = getRowNum(record);
            if (consentStatus != null && consentStatus.equals("true")) {

                record.setAttribute(OrganizationConsentConstants.PERMIT_CONSENT, false);
                b.setTitle("Permit");

                System.out.println("OPC6.......PERMIT................");

            } else {

                record.setAttribute(OrganizationConsentConstants.PERMIT_CONSENT, true);
                b.setTitle("Deny");
                System.out.println("OPC7........DENY...............");

            }

            /*  setVirtualScrolling(false);          
             setShowRecordComponents(true);          
             setShowRecordComponentsByCell(true); */
//         
            listGridRecords[rowNo] = record;
            
            dataSource.setCacheData(listGridRecords);
            setDataSource(dataSource);
            setData(listGridRecords);
            
           
               label=new Label(MSG);
                 label.setHeight(18);
        label.setWidth100();
               label.setAlign(Alignment.CENTER);
               addMember(label,0);
           
           
            markForRedraw();
           

        }

    }

}
