package com.gsihealth.dashboard.client.consent.healthhomeorganization;

import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.ConsentConstants;
import com.gsihealth.dashboard.entity.dto.OrganizationPatientConsentDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 *
 * @author Chad Darby
 */
public class OrganizationPatientConsentRecord extends ListGridRecord {

    OrganizationPatientConsentDTO organizationPatientConsentDto;
    
    public OrganizationPatientConsentRecord(OrganizationPatientConsentDTO theDto) {
        organizationPatientConsentDto = theDto;
        setAttribute(OrganizationConsentConstants.ORGANIZATION_ID, theDto.getOrganizationId());
        setAttribute(OrganizationConsentConstants.ORGANIZATION_NAME, theDto.getOrganizationName());
        setAttribute(OrganizationConsentConstants.CITY, theDto.getCity());
        setAttribute(OrganizationConsentConstants.STATE, theDto.getState());
        setAttribute(OrganizationConsentConstants.COMMUNITY_ID, theDto.getCommunityId());
        setAttribute(OrganizationConsentConstants.PERMIT_CONSENT, isChecked(theDto));
        setAttribute("Dto", theDto);
     //setAttribute(OrganizationConsentConstants.ACTION, theDto.getConsentStatus());
    }

    protected boolean isChecked(OrganizationPatientConsentDTO theDto) {
        System.out.println("Statis:"+theDto.getConsentStatus());
        return StringUtils.equalsIgnoreCase(ConsentConstants.CONSENT_PERMIT, theDto.getConsentStatus());
    }
    
    public void setPermitConsent(boolean flag) {
        setAttribute(OrganizationConsentConstants.PERMIT_CONSENT, flag);        
    }
    
    public OrganizationPatientConsentDTO getOrganizationPatientConsentDto() {
        // read the attributes
        Boolean theStatus = getAttributeAsBoolean(OrganizationConsentConstants.PERMIT_CONSENT);
        
        String statusStr = getStatus(theStatus);
        
        organizationPatientConsentDto.setConsentStatus(statusStr);
        
        return organizationPatientConsentDto;
    }
    
    private String getStatus(boolean flag) {
        
        return flag ? ConsentConstants.CONSENT_PERMIT : ConsentConstants.CONSENT_DENY;
    }
}
