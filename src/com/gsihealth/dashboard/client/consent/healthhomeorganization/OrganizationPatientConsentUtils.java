package com.gsihealth.dashboard.client.consent.healthhomeorganization;

import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.ConsentConstants;
import com.gsihealth.dashboard.entity.dto.OrganizationPatientConsentDTO;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class OrganizationPatientConsentUtils {
        
    public static OrganizationPatientConsentRecord[] convert(List<OrganizationPatientConsentDTO> theOrgs, long currentPatientOrgId, String programLevelConsentStatus) {
        OrganizationPatientConsentRecord[] records = new OrganizationPatientConsentRecord[theOrgs.size()];
//    
//        long southwestBrooklynOrgId = getSouthwestBrooklynOrgId();
        
        int index = 0;
        for (OrganizationPatientConsentDTO tempOrgPatientConsent : theOrgs) {
            OrganizationPatientConsentRecord tempListGridRecord = convert(tempOrgPatientConsent);
            long tempOrganizationId = tempOrgPatientConsent.getOrganizationId();
            
            // automatically check the record if the same org and make it disabled
            boolean sameOrgId = tempOrganizationId == currentPatientOrgId;
            if (sameOrgId) {
                tempListGridRecord.setPermitConsent(true);
                tempListGridRecord.setEnabled(false);
            }
            
            /** JIRA - 958 SWB-OID is replaced by list of auto-consented orgs and disable them
            /* auto-consented orgs are picked from community_org table
            **/
            if(tempOrgPatientConsent.isAutoConsent()){
                  // In BHH when PLCS is changed from yes to no without save, the auto consented orgs should be shown checked on UI in manage acess provider grid.
                  boolean permitConsent = StringUtils.equalsIgnoreCase("YES", programLevelConsentStatus);
                  boolean disableBhhMode=ApplicationContextUtils.getClientApplicationProperties().isManageConsent();
                  if(!disableBhhMode){
                        // In BHH, when PLCS is changed from yes to no without save, the auto consented orgs should be shown checked on UI in manage acess provider grid.
                      tempListGridRecord.setPermitConsent(permitConsent);
                  }
                  else{
                      // Spira - 5864 UHC Mode
                      tempListGridRecord.setPermitConsent(StringUtils.equalsIgnoreCase(ConsentConstants.CONSENT_PERMIT, tempOrgPatientConsent.getConsentStatus()));
                  }
                 
                tempListGridRecord.setEnabled(false);
            }
            records[index] = tempListGridRecord;
            
            index++;
        }
        
        return records;
    }
    
    
    /**
     * Convert DTO to record
     * 
     * @param index
     * @param theDto
     * @return 
     */
    public static OrganizationPatientConsentRecord convert(OrganizationPatientConsentDTO theDto) {
        
        OrganizationPatientConsentRecord record = new OrganizationPatientConsentRecord(theDto);
        
        return record;
    }

}
