package com.gsihealth.dashboard.client.consent.healthhomeorganization;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.smartgwt.client.widgets.Window;

/**
 *
 * @author Chad Darby
 */
public class OrganizationPatientConsentWindow extends Window {
    
    private PersonDTO personDTO;
    
    public OrganizationPatientConsentWindow(PersonDTO thePersonDTO) {
        personDTO = thePersonDTO;
        
        buildGui();
    }
    
    private void buildGui() {
        DashBoardApp.restartTimer();

        setWindowProps();
        
        OrganizationPatientConsentPanel organizationPatientConsentPanel = new OrganizationPatientConsentPanel(personDTO);
        addItem(organizationPatientConsentPanel);
        
    }
    
    private void setWindowProps() {
        setPadding(10);
        setWidth(1080);
        setHeight(650);
        
        String patientName = personDTO.getFirstName() + " " + personDTO.getLastName();
        setTitle("Manage Provider Organization Level Consent for Patient: " + patientName);
        setShowMinimizeButton(false);
        setIsModal(true);
        setShowModalMask(true);
       
        centerInPage();
    }
    
}
