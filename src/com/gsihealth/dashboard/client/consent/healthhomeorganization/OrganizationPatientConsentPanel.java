package com.gsihealth.dashboard.client.consent.healthhomeorganization;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
//import com.gsihealth.dashboard.client.mypatientlist.MyPatientListConstants;
import com.gsihealth.dashboard.client.service.OrganizationPatientConsentService;
import com.gsihealth.dashboard.client.service.OrganizationPatientConsentServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.ConsentConstants;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.OrganizationPatientConsentDTO;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceBooleanField;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridEditEvent;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.CellSavedEvent;
import com.smartgwt.client.widgets.grid.events.CellSavedHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class OrganizationPatientConsentPanel extends VLayout {
    
    private PersonDTO personDTO;
    private OrganizationPatientConsentServiceAsync organizationPatientConsentService = GWT.create(OrganizationPatientConsentService.class);
    private Button saveButton;
    private ListGrid listGrid;
    private Label label = null;
    private ListGridRecord[] orignalListGridRecord = null;
     
    private Button resetButton;
    private Button showConsented;
    private String MSG = "<font color='red'>You must click the 'Save Consent' button to save any changes you have made.</font>";
    
    private static final int CONSENT_COLUMN = 4;
    
    private ProgressBarWindow progressBarWindow;
    
    public OrganizationPatientConsentPanel(PersonDTO thePersonDTO) {
        personDTO = thePersonDTO;
        
        buildGui();
        DashBoardApp.restartTimer();
        
        loadOrganizationPatientConsentData();
    }
    
    private void buildGui() {
        
        setMargin(5);
        
        listGrid = buildListGrid();
        
        addMember(buildResetBar(), 0);
        
        setPadding(5);
        addMember(listGrid);

        // layout spacer b/w layout lables and layout of remover button
        LayoutSpacer topSpacer = new LayoutSpacer();
        topSpacer.setHeight(20);
        addMember(topSpacer);

        // layout for remove button
        Layout buttonBar = buildButtonBar();
        addMember(buttonBar);
        
        progressBarWindow = new ProgressBarWindow("Saving", "Saving...");
        
    }

    /**
     * Helper method to build a ListGrid
     *
     * @return
     */
    private ListGrid buildListGrid() {
        
        String[] columnNames = {OrganizationConsentConstants.ORGANIZATION_ID, OrganizationConsentConstants.ORGANIZATION_NAME, OrganizationConsentConstants.CITY, OrganizationConsentConstants.STATE, OrganizationConsentConstants.PERMIT_CONSENT};
        
        ListGrid theListGrid = new ListGrid();

//        theListGrid.setCanReorderFields(true);
//        theListGrid.setCanReorderRecords(true);
//        theListGrid.setWidth100();
//        theListGrid.setHeight100();
//
//        theListGrid.setAlternateRecordStyles(true);
//        theListGrid.setShowAllRecords(false);
//        theListGrid.setSelectionType(SelectionStyle.SINGLE);
//
//        theListGrid.setCanEdit(true);
//        theListGrid.setDataFetchMode(FetchMode.LOCAL);
//        theListGrid.setCanEdit(false);
//        theListGrid.setAutoFetchData(true);  
//        theListGrid.setShowFilterEditor(true);
//        theListGrid.setFilterByCell(true);
//        theListGrid.setFilterOnKeypress(true);
//        theListGrid.setEditEvent(ListGridEditEvent.CLICK);
//        theListGrid.setEditByCell(true);
        // theListGrid.setCanReorderFields(true);
        // theListGrid.setCanReorderRecords(true);
        theListGrid.setWidth100();
        theListGrid.setHeight100();
        
        theListGrid.setAlternateRecordStyles(true);
        theListGrid.setShowAllRecords(true);//
        theListGrid.setSelectionType(SelectionStyle.MULTIPLE);
        
        theListGrid.setCanEdit(true);
        theListGrid.setAutoSaveEdits(true);
        
        theListGrid.addCellSavedHandler(new CellSavedHandler() {
            
            @Override
            public void onCellSaved(CellSavedEvent event) {
                
                label.setContents(MSG);
                
                OrganizationPatientConsentRecord opcr = (OrganizationPatientConsentRecord) event.getRecord();
                
                System.out.println(opcr.getOrganizationPatientConsentDto().getConsentStatus());
                ListGridRecord copyOfOrignalRecord[] = orignalListGridRecord;
                for (int i = 0; i < copyOfOrignalRecord.length; i++) {
                    
                    OrganizationPatientConsentRecord tempOrgRecord = (OrganizationPatientConsentRecord) copyOfOrignalRecord[i];
                    
                    if (tempOrgRecord.getOrganizationPatientConsentDto().getOrganizationId() == opcr.getOrganizationPatientConsentDto().getOrganizationId()) {
                        orignalListGridRecord[i] = opcr;
                    }
                    
                }
            }
            
        });

        //theListGrid.setDataFetchMode(FetchMode.LOCAL);
        //theListGrid.setCanEdit(false);
        theListGrid.setShowFilterEditor(true);
        theListGrid.setFilterByCell(true);
        
        theListGrid.setFilterOnKeypress(true);
        theListGrid.setEditEvent(ListGridEditEvent.CLICK);
        theListGrid.setEditByCell(true);
        
        theListGrid.setAutoFetchData(true);

        //theListGrid.setVirtualScrolling(false);
        // theListGrid.setShowRecordComponents(true);
        //  theListGrid.setShowRecordComponentsByCell(true);
//        
        // add fields
        ListGridField[] fields = buildListGridFields(columnNames);
        theListGrid.setFields(fields);
        
        theListGrid.setEmptyMessage("...");
        theListGrid.setCanResizeFields(true);
        theListGrid.hideField(OrganizationConsentConstants.ORGANIZATION_ID);
        
        return theListGrid;
    }
    
    private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];
        
        for (int i = 0; i < columnNames.length; i++) {
            String tempColumnName = columnNames[i];
            ListGridField tempField = buildListGridField(tempColumnName);
            fields[i] = tempField;
            
            if (i == CONSENT_COLUMN) {
                fields[i].setCanEdit(true);
                fields[i].setCanFilter(false);
              //  fields[i].setCanToggle(true);
                
                fields[i].setType(ListGridFieldType.BOOLEAN);
                
            }
        }
        
        return fields;
    }
    
    private ListGridField buildListGridField(String title) {
        ListGridField tempField = new ListGridField(title, title);
        
        tempField.setCanEdit(false);
        
        return tempField;
    }
    
    private Layout buildButtonBar() {
        // layout for button bar
        HLayout buttonLayout = new HLayout(10);
        buttonLayout.setWidth100();
        buttonLayout.setAlign(Alignment.CENTER);

        // remove button
        saveButton = new Button("Save Consent");
        saveButton.setID("SaveConsent");
        saveButton.setWidth(140);
        saveButton.addClickHandler(new SaveClickHandler());
        buttonLayout.addMember(saveButton);
        
        return buttonLayout;
    }
    
    private Layout buildResetBar() {
        // layout for button bar
        HLayout buttonLayout = new HLayout(10);
        buttonLayout.setWidth100();
        buttonLayout.setAlign(Alignment.CENTER);
        buttonLayout.setHeight(25);
        label = new Label("");
        label.setHeight(18);
        label.setWidth100();
        label.setAlign(Alignment.CENTER);
        buttonLayout.addMember(label);
        // remove button
        resetButton = new Button("Reset Filters");
        resetButton.setID("ResetFilters");
        resetButton.setWidth(100);
        resetButton.setHeight(20);
        resetButton.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                
                        
         loadOrganizationPatientConsentData();
         label.setContents("");
         showConsented.setTitle(OrganizationConsentConstants.SHOW_CONSENTED);
            }
        });
        resetButton.setAlign(Alignment.CENTER);
        buttonLayout.addMember(resetButton);
        
        showConsented = new Button(OrganizationConsentConstants.SHOW_CONSENTED);
        showConsented.setID("showConsented");
        showConsented.setWidth(100);
        showConsented.setHeight(20);
        showConsented.addClickHandler(new ClickHandler() {

            @Override
            public void onClick(ClickEvent event) {
                
                   displayConsentedRecords(showConsented.getTitle());    
                   
         
            }

          
        });
        showConsented.setAlign(Alignment.CENTER);
        buttonLayout.addMember(showConsented);
      
        
        return buttonLayout;
    }
      private void displayConsentedRecords(String title) {
              if(title!=null && title.equals(OrganizationConsentConstants.SHOW_CONSENTED)){
                  List<ListGridRecord> listGridRecords=new ArrayList<ListGridRecord>();
               ListGridRecord[] filteredGridRecords=null;
               for(ListGridRecord record:orignalListGridRecord){
                     OrganizationPatientConsentRecord tempOrgRecord = (OrganizationPatientConsentRecord) record;
                    
                    if(tempOrgRecord.getOrganizationPatientConsentDto().getConsentStatus().equals(ConsentConstants.CONSENT_PERMIT)){
                        listGridRecords.add(record);
                    }
                    
               }
             filteredGridRecords=new ListGridRecord[listGridRecords.size()];
             for(int i=0;i<listGridRecords.size();i++){
                 filteredGridRecords[i]=listGridRecords.get(i);
             }
             populateListGrid(filteredGridRecords);
             showConsented.setTitle(OrganizationConsentConstants.SHOW_ALL);
              }
              else if(title!=null && title.equals(OrganizationConsentConstants.SHOW_ALL)){
                   populateListGrid(orignalListGridRecord);
                   showConsented.setTitle(OrganizationConsentConstants.SHOW_CONSENTED);
              }
            }
    private void loadOrganizationPatientConsentData() {
       
        LoadOrganizationPatientConsentDataCallback loadOrganizationPatientConsentDataCallback = new LoadOrganizationPatientConsentDataCallback();
        loadOrganizationPatientConsentDataCallback.attempt();
    }
    
    public void populateListGrid(ListGridRecord[] data) {
     //   orignalListGridRecord = data;
       // ListGridRecord[] records = listGrid.getRecords();

        // scroll to first row
        listGrid.scrollToRow(1);
        final DataSource dataSource = new DataSource();
        dataSource.setID("dataSource");
        dataSource.setClientOnly(true);
//        dataSource.setShowPrompt(true);
        DataSourceTextField communityId = new 
        DataSourceTextField(OrganizationConsentConstants.COMMUNITY_ID, OrganizationConsentConstants.COMMUNITY_ID);
        DataSourceTextField org_id = new DataSourceTextField(OrganizationConsentConstants.ORGANIZATION_ID, OrganizationConsentConstants.ORGANIZATION_ID);
        DataSourceTextField name = new DataSourceTextField("Organization Name", "Organization Name");
        DataSourceTextField city = new DataSourceTextField("City", "City");
        DataSourceTextField state = new DataSourceTextField("State", "State");
        DataSourceBooleanField permit = new DataSourceBooleanField("Consented", "Consented");

        communityId.setCanEdit(false);
        communityId.setPrimaryKey(true);
        org_id.setCanEdit(false);
        org_id.setPrimaryKey(true);
        permit.setCanEdit(true);
        permit.setCanFilter(false);

        //Permit.setCanSave(true);
        name.setCanEdit(false);
        // name.setPrimaryKey(false);  
        city.setCanEdit(false);
        //city.setPrimaryKey(false);  
        state.setCanEdit(false);
        // state.setPrimaryKey(false);  
        dataSource.setFields(communityId, org_id, name, city, state, permit);
        dataSource.setCacheData(data);
        listGrid.setDataSource(dataSource);
        listGrid.hideField(OrganizationConsentConstants.ORGANIZATION_ID);
        listGrid.hideField(OrganizationConsentConstants.COMMUNITY_ID);
        listGrid.setData(data);
        listGrid.redraw();
    }
    
    class LoadOrganizationPatientConsentDataCallback extends RetryActionCallback<List<OrganizationPatientConsentDTO>> {
        
        @Override
        public void attempt() {
            long patientId = personDTO.getPatientEnrollment().getPatientId();
            long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
            organizationPatientConsentService.getOrganizationPatientConsentList(patientId, communityId, this);
        }
        
        @Override
        public void onCapture(List<OrganizationPatientConsentDTO> data) {
            long patientOrgId = personDTO.getPatientEnrollment().getOrgId();
            String programLevelConsentStatus = personDTO.getPatientEnrollment().getProgramLevelConsentStatus();
            ListGridRecord[] records = OrganizationPatientConsentUtils.convert(data, patientOrgId, programLevelConsentStatus);
            orignalListGridRecord=records;
            populateListGrid(records);
        }
    }
    
    class SaveClickHandler implements ClickHandler {
        
        @Override
        public void onClick(ClickEvent clickEvent) {
            
            progressBarWindow.show();
            
            ListGridRecord[] records = orignalListGridRecord;
            
            List<OrganizationPatientConsentDTO> dtos = getCheckedItems(records);
            
            long patientId = personDTO.getPatientEnrollment().getPatientId();
            long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
            organizationPatientConsentService.saveOrganizationPatientConsentList(patientId, dtos, communityId, new SaveAsyncCallback());
        }
        
        private List<OrganizationPatientConsentDTO> getCheckedItems(ListGridRecord[] records) {
            
            List<OrganizationPatientConsentDTO> dtos = new ArrayList<OrganizationPatientConsentDTO>();
            
            for (ListGridRecord tempRecord : records) {
                OrganizationPatientConsentDTO tempDto = null;
                if (tempRecord instanceof OrganizationPatientConsentRecord) {
                    System.out.println("Inside OPCR");
                    OrganizationPatientConsentRecord tempOrgRecord = (OrganizationPatientConsentRecord) tempRecord;
                    
                    tempDto = tempOrgRecord.getOrganizationPatientConsentDto();
                } //in case of filtering
                else {
                    System.out.println("Inside LGR");
                    tempDto = (OrganizationPatientConsentDTO) tempRecord.getAttributeAsObject("Dto");
                    Boolean theStatus = tempRecord.getAttributeAsBoolean(OrganizationConsentConstants.PERMIT_CONSENT);
                    
                    String statusStr = theStatus ? ConsentConstants.CONSENT_PERMIT : ConsentConstants.CONSENT_DENY;
                    
                    tempDto.setConsentStatus(statusStr);
                }
                
                String consentStatus = tempDto.getConsentStatus();
                
                System.out.println("Saving status:" + consentStatus);
                
                boolean hasPermit = StringUtils.equalsIgnoreCase(consentStatus, ConsentConstants.CONSENT_PERMIT);
                if (hasPermit) {
                    dtos.add(tempDto);
                }
            }
            
            return dtos;
        }
    }
    
    class SaveAsyncCallback implements AsyncCallback {
        
        @Override
        public void onSuccess(Object t) {
            progressBarWindow.hide();
            label.setContents("");
            SC.say("Save successful.");
            
        }
        
        @Override
        public void onFailure(Throwable thrwbl) {
            progressBarWindow.hide();
            
            SC.warn("Error during save. Please try again.");
        }
    }
}
