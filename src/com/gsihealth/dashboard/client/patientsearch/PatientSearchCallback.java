package com.gsihealth.dashboard.client.patientsearch;

import com.gsihealth.dashboard.client.WindowManager;
import java.util.List;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.smartgwt.client.util.SC;

/**
 *
 * @author User
 */
public class PatientSearchCallback extends PortalAsyncCallback<SearchResults<PersonDTO>> {

    private SearchCriteria searchCriteria;
    private List<PersonDTO> users;
    private ProgressBarWindow progressBar;
    private final WindowManager windowManager;
    private final PatientSearchWindow window ;

    public PatientSearchCallback() {
        this.windowManager = null;
        this.window = null;
    }

    public PatientSearchCallback(SearchCriteria theSearchCriteria, ProgressBarWindow theProgressBarWindow , WindowManager windowManager) {
        searchCriteria = theSearchCriteria;
        progressBar = theProgressBarWindow;
        this.windowManager = windowManager;
        window = new PatientSearchWindow(progressBar , this.windowManager);   
    }
    
 @ Override
    public void onSuccess(SearchResults<PersonDTO> users) {
      
        //window = new PatientSearchWindow(progressBar , this.windowManager);
        //JIRA - 1193 --Satyendra -- No need to get the count seperately. SearchResults already has the patient count. 
        //Changed the the existing class to extend PortalAsyncCallback<SearchResults<PersonDTO>>, so that the existing search functinality can be used in carebook patient search. 

        //        window.gotoPage(1, searchCriteria, progressBarWindow);
        window.populatePatientUsers(users.getData());

        window.show();
         if((users.getData() == null) ||(users.getData() != null && users.getData().size() == 0 )){
             SC.warn("Search did not return any results. Please modify your search criteria.");
        }

        if (progressBar != null) {
            progressBar.hide();
        }

    }

    @Override
    public void onFailure(Throwable thrwbl) {
        SC.warn("Error retrieving Patient.");

    }
}
