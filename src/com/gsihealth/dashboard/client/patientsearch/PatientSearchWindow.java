/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.patientsearch;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.RootPanel;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.PatientSearchRecord;
import com.gsihealth.dashboard.client.WindowManager;
import com.gsihealth.dashboard.client.common.MiniGridPager;
import com.gsihealth.dashboard.client.common.PersonRecord;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.service.CareteamService;
import com.gsihealth.dashboard.client.service.CareteamServiceAsync;
import com.gsihealth.dashboard.client.service.EnrollmentService;
import com.gsihealth.dashboard.client.service.EnrollmentServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.PropertyUtils;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.redux.Redux;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.Date;
import java.util.List;

/**
 *
 * @author User
 */
public class PatientSearchWindow extends Window {

    //private DynamicForm form;
    private ListGrid patientListGrid;
    private MiniGridPager pager;
    private ProgressBarWindow progressBarWindow;
    private int currentPageNumber;
    private CareteamServiceAsync careteamService = GWT.create(CareteamService.class);
    private final EnrollmentServiceAsync enrollmentService = GWT.create(EnrollmentService.class);
    private long userId;
    private long communityId;

    private List<PersonDTO> persons;
    private boolean requireChh;
    private final WindowManager windowManager;

    public PatientSearchWindow(ProgressBarWindow theProgressBarWindow, WindowManager windowManager) {
        this.windowManager = windowManager;
        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();

        requireChh = props.isChildrensHealthHome();
        buildGui();
        DashBoardApp.restartTimer();

    }

    private void buildGui() {
        DashBoardApp.restartTimer();

        setWindowProps();
        // dismissing result window on outside click
        setDismissOnOutsideClick(true);
        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(5);
        addItem(spacer);


        Canvas listGridLayout = buildListGridLayout();
        addItem(listGridLayout);

        patientListGrid.addRecordDoubleClickHandler(new PatientRecordClickHandler(this));

    }


    private void setWindowProps() {
        setPadding(10);
        setWidth(600);
        setHeight(350);
        setTitle("Patient Search");
        //setShowMinimizeButton(false);
        setIsModal(true);
        setShowModalMask(true);
        centerInPage();
    }

    private Canvas buildListGridLayout() {

        HStack listGridLayout = new HStack();
        listGridLayout.setPadding(5);

        patientListGrid = buildListGrid();
        pager = new MiniGridPager(0);

        // temp hide it
        pager.hide();

        VLayout patientLayout = createListGridLayout(patientListGrid, pager);
        listGridLayout.addMember(patientLayout);

        return listGridLayout;
    }

    private ListGrid buildListGrid() {
        if (requireChh) {
            String[] columnNames = {PatientSearchRecord.LAST_NAME, PatientSearchRecord.FIRST_NAME, PatientSearchRecord.MIDDLE_NAME, PatientSearchRecord.DOB, PatientSearchRecord.MINOR, PatientSearchRecord.GENDER, PatientSearchRecord.PATIENT_ID, PatientSearchRecord.CM_ORG, PatientSearchRecord.ENROLLMENT_STATUS};
            ListGrid theListGrid = new ListGrid();

            theListGrid.setWidth(570);
            theListGrid.setHeight(270);

            ListGridField[] fields = buildListGridFields(columnNames);

            theListGrid.setFields(fields);
            theListGrid.setEmptyMessage("...");

            theListGrid.setCanResizeFields(true);

            return theListGrid;

        } else {

            String[] columnNames = {PatientSearchRecord.LAST_NAME, PatientSearchRecord.FIRST_NAME, PatientSearchRecord.MIDDLE_NAME, PatientSearchRecord.DOB, PatientSearchRecord.GENDER, PatientSearchRecord.PATIENT_ID, PatientSearchRecord.CM_ORG, PatientSearchRecord.ENROLLMENT_STATUS};
            ListGrid theListGrid = new ListGrid();

            theListGrid.setWidth(570);
            theListGrid.setHeight(270);

            ListGridField[] fields = buildListGridFields(columnNames);

            theListGrid.setFields(fields);
            theListGrid.setEmptyMessage("...");

            theListGrid.setCanResizeFields(true);

            return theListGrid;
        }
    }

    private VLayout createListGridLayout(ListGrid theListGrid, MiniGridPager theGridPager) {
        VLayout theLayout = new VLayout();

        theListGrid.setWidth(570);
        theLayout.addMember(theListGrid);

        theGridPager.setWidth(570);
        theLayout.addMember(theGridPager);

        return theLayout;
    }

    private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];

        for (int i = 0; i < columnNames.length; i++) {
            String tempColumnName = columnNames[i];
            ListGridField tempField = buildListGridField(tempColumnName);
            fields[i] = tempField;
        }

        return fields;
    }

    private ListGridField buildListGridField(String title) {
        ListGridField tempField = new ListGridField(title, title);

        tempField.setCanEdit(false);

        return tempField;
    }

    public void populatePatientUsers(List<PersonDTO> users) {
        ListGridRecord[] records = convert(users);
        persons = users;

        patientListGrid.scrollToRow(1);
        patientListGrid.setData(records);
        patientListGrid.markForRedraw();
        //patientListGrid.setData(records);
    }

    private ListGridRecord[] convert(List<PersonDTO> users) {
        ListGridRecord[] records = new ListGridRecord[users.size()];

        // convert dtos to records
        for (int index = 0; index < users.size(); index++) {

            PersonDTO tempUser = users.get(index);
            records[index] = PropertyUtils.convertDashBoard(index, tempUser);
        }

        return records;
    }

//    public void gotoPage(final int pageNumber, SearchCriteria searchCriteria, ProgressBarWindow progressBarWindow) {
//
//        // progressBarWindow.setVisible(true);
//        //System.out.println(" gotoPage progressBarWindow ::" + progressBarWindow);
//        currentPageNumber = pageNumber;
//
//        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
//        userId = loginResult.getUserId();
//        communityId = loginResult.getCommunityId();
//
//        TotalCountsCallback totalCountsCallback  = new TotalCountsCallback(searchCriteria);
//        totalCountsCallback.attempt();
//        
//    }
//
//    class TotalCountsCallback extends RetryActionCallback<Integer> {
//
//        private SearchCriteria searchCriteria;
//        
//        TotalCountsCallback(SearchCriteria searchCriteria) {
//            this.searchCriteria = searchCriteria;
//        }
//        
//        @Override
//        public void attempt() {
//            careteamService.findTotalNoOfUser(communityId, searchCriteria, userId, this);
//        }
//        
//        @Override
//        public void onCapture(Integer total) {
//            pager.setTotalCount(total);
//            pager.gotoPage(currentPageNumber);
//
//            if (progressBarWindow != null) {
//                progressBarWindow.hide();
//            }
//
//            if (total == 0) {
//                if (progressBarWindow != null) {
//                    progressBarWindow.setVisible(false);
//                }
//
//                SC.warn(Constants.SEARCH_DID_NOT_RETURN_ANY_RESULTS);
//                //clear list grid
//                ListGridRecord[] records = new ListGridRecord[1];
//                patientListGrid.setData(records);
//            }
//        }
//
//        @Override
//        public void onFailure(Throwable exc) {
//            progressBarWindow.setVisible(false);
//            super.onFailure(exc);
//        }
//    }

    /**
     * For each record clicked, populate form w/ user data
     */
    class PatientRecordClickHandler implements RecordDoubleClickHandler {

        private PatientSearchWindow searchWindow;

        PatientRecordClickHandler(PatientSearchWindow searchWindow){
            this.searchWindow=searchWindow;
        }
        @Override
        public void onRecordDoubleClick(RecordDoubleClickEvent event) {
            handleSelectedPatient(searchWindow);
        }
    }

    private void handleSelectedPatient(final PatientSearchWindow searchWindow) {
        PersonRecord theRecord = (PersonRecord) patientListGrid.getSelectedRecord();

        if (theRecord == null) {
            SC.warn("Nothing selected.");
            return;
        }

        int index = theRecord.getIndex();

        // get selected DTO
        final PersonDTO thePerson = persons.get(index);

        final LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        communityId = loginResult.getCommunityId();
        final String tokenId = loginResult.getToken();

        String careBookUrl = ApplicationContextUtils.getCarebookURL();

        careBookUrl += "?patientId=" + thePerson.getPatientEnrollment().getPatientId() + "&userId=" + loginResult.getEmail() + "&communityId=" + communityId + "&tokenId=" + tokenId;

        final String features = "location=no,status=yes,scrollbars=yes,width=1200,height=800,resizable=yes";
        
        /*
         * Internet Explorer does not like spaces in window name when calling window.open(), therefore removed spaces from name.
         */
        final String name = "CareBook";
        // now show the panel
        if (thePerson.isSelfAssertEligible()) {
            final ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
            if (ApplicationContextUtils.getClientApplicationProperties().isPatientSelfAssertedConsentAccess()) {
                SC.confirm("Warning", props.getPatientSelfAssertWarningText(), new BooleanCallback() {
                    @Override
                    public void execute(Boolean value) {
                        if (value.booleanValue()) {

                            final String consentLanguage = props.getConsentLanguage();
                            SC.ask("Consent Confirmation", consentLanguage, new BooleanCallback() {
                                public void execute(Boolean value) {
                                    if (value.booleanValue()) {
                                        thePerson.getPatientEnrollment().setSelfAssertedConsentDate(new Date());
                                        enrollmentService.updatePatientBySelfAssertion(thePerson, new AsyncCallback<PersonDTO>() {
                                            @Override
                                            public void onSuccess(PersonDTO updatedPersonDtoAfterSelfAssertion) {

                                                String careBookUrlUpdatedUser = ApplicationContextUtils.getCarebookURL();
                                                careBookUrlUpdatedUser += "?patientId=" + thePerson.getPatientEnrollment().getPatientId() + "&userId=" + loginResult.getEmail() + "&communityId=" + communityId + "&tokenId=" + tokenId;
                                                RootPanel mainPanel = RootPanel.get("appCareBookSearch");
                                                mainPanel.clear();
                                                searchWindow.hide();
                                                Frame frame = new Frame(careBookUrlUpdatedUser);
                                                mainPanel.add(frame);
                                                Redux.jsEvent("local:openApp:appCareBookSearch");
                                                 
                                                // SPIRA 7188
                                              
                                                //PatientSearchWindow.this.windowManager.reloadScreenWithSearchCriteria();

                                            }

                                            @Override
                                            public void onFailure(Throwable cause) {
                                                SC.say("Error launching carebook, Please try again.");
                                            }
                                        });
                                    }
                                }
                            });

                        } else {
                            SC.say("Do nothing");
                        }
                    }

                });
            } else {
                SC.say("This patient has not yet granted access to your organization.  Please contact your administrator.");
            }
        } else {
            //com.google.gwt.user.client.Window.open(careBookUrl, name, features);

            RootPanel mainPanel = RootPanel.get("appCareBookSearch");
            mainPanel.clear();
            searchWindow.hide();
            Frame frame = new Frame(careBookUrl);
            mainPanel.add(frame);
            Redux.jsEvent("local:openApp:appCareBookSearch");


        }

    }


}
