/*
 * This is a generic dashboard application shell. Use it to create new apps in GWT.
 * 
 */
package com.gsihealth.dashboard.client.TaskList;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.smartgwt.client.widgets.layout.VLayout;
import com.google.gwt.user.client.ui.FlowPanel;
import com.gsihealth.dashboard.redux.Redux;

/**
 *
 * @author Nick Steele
 */
public class TaskListPanel extends VLayout {

    private static final int MAIN_CONTENT_POSITION = 1;

    public TaskListPanel() {
        buildUI();
        // Restart the activity timer (so the client doesn't logout)...
        DashBoardApp.restartTimer();
        // Notify Angular that we've drawn the task list panel (so Angular can inject the page)...
        Redux.jsEvent("local:appTaskList:startup");
    }

    // Put things you need to build the initial UI here.
    private void buildUI() {       
        FlowPanel panel = new FlowPanel();	
		setHeight100();
        setWidth100();
        panel.getElement().setId("appTasks");
        panel.getElement().setAttribute("dynamic", "app.html.task");
        addMember(panel);

        panel.setWidth("100%");
        panel.setHeight("100%");
    }
}
