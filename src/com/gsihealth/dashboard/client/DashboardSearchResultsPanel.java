/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client;

import com.smartgwt.client.widgets.Label;
import com.gsihealth.dashboard.client.common.GridPager;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.client.util.DateFieldCellFormatter;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import java.util.List;

/**
 *
 * @author User
 */
public class DashboardSearchResultsPanel extends VLayout {

    private ListGrid listGrid;
    private GridPager pager;
    private static final int DATE_COLUMN = 3;
    private static final String[] columnNamesCandidates = {"Last Name", "First Name", "Middle Name", "DOB", "Gender", "Patient ID"};
    private Label consentLabel;
    private List<PersonDTO> persons;

    public DashboardSearchResultsPanel() {

        buildGui();


    }

    private void buildGui() {

        listGrid = buildListGrid();

        setPadding(2);

        // header
        Layout headerBar = buildHeaderBar();
        addMember(headerBar);

        // list grid
        addMember(listGrid);

        // pager
        pager = new GridPager(0);
        pager.setWidth(590);
        addMember(pager);

        LayoutSpacer topSpacer = new LayoutSpacer();

        addMember(topSpacer);
        // setWidth100();
        // setHeight100();
    }

//    public void populateListGrid(ListGridRecord[] data, int totalCount) {
//        // scroll to first row
//        listGrid.scrollToRow(1);
//
//        listGrid.setData(data);
//        listGrid.markForRedraw();
//        pager.setTotalCount(totalCount);
//
//        pager.updatePage(totalCount);
//
//    }

    /** Helper method to build a ListGrid
     *
     * @return
     */
    private ListGrid buildListGrid() {

        ListGrid theListGrid = new ListGrid();

        theListGrid.setCanReorderFields(true);
        theListGrid.setCanReorderRecords(true);

        theListGrid.setWidth(590);
        theListGrid.setHeight100();

        theListGrid.setAlternateRecordStyles(true);
        theListGrid.setShowAllRecords(false);
        theListGrid.setSelectionType(SelectionStyle.SINGLE);

        theListGrid.setCanEdit(false);

        // add fields
        ListGridField[] fields = buildListGridFields(columnNamesCandidates);
        theListGrid.setFields(fields);

        ListGridField dateField = fields[DATE_COLUMN];
        setDateFieldCellFormatter(dateField);

        theListGrid.setEmptyMessage("...");
        //theListGrid.setCanResizeFields(true);

        return theListGrid;
    }

    private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];

        for (int i = 0; i < columnNames.length; i++) {
            String tempColumnName = columnNames[i];
            ListGridField tempField = buildListGridField(tempColumnName);
            fields[i] = tempField;
        }

        return fields;
    }

    /**
     * Add cell formatter for date field
     *
     * @param dateField
     */
    private void setDateFieldCellFormatter(ListGridField dateField) {

        final DateTimeFormat dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
        dateField.setCellFormatter(new DateFieldCellFormatter(dateFormatter));
    }

    private ListGridField buildListGridField(String title) {
        ListGridField tempField = new ListGridField(title, title);

        tempField.setCanEdit(false);

        return tempField;
    }

    public void setPageEventHandler(PageEventHandler handler) {
        pager.setPageEventHandler(handler);
    }

    private Layout buildHeaderBar() {
        HLayout layout = new HLayout(10);

        layout.setWidth100();
        layout.setAlign(Alignment.CENTER);

        consentLabel = new Label();
        //consentLabel.setHeight(30);
        consentLabel.setWidth100();
        consentLabel.setAlign(Alignment.CENTER);

        layout.addMember(consentLabel);

        return layout;
    }

    public ListGrid getListGrid() {
        return listGrid;
    }

    public void populateListGrid(ListGridRecord[] data, List<PersonDTO> thePersons, int totalCount) {
       
        // scroll to first row
        listGrid.scrollToRow(1);

        listGrid.setData(data);
        listGrid.markForRedraw();
        persons = thePersons;

        pager.setTotalCount(totalCount);
       // pager.updatePage(totalCount);

    }

    public void clearListGrid() {
        ListGridRecord[] records = new ListGridRecord[1];
        listGrid.setData(records);
    }

    public void setTotalCount(int totalCount) {
        pager.setTotalCount(totalCount);
    }
    
     public void gotoPage(int pageNumber) {
        pager.gotoPage(pageNumber);
    }
}
