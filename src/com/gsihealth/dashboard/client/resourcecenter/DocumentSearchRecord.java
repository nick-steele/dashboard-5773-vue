/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.resourcecenter;

import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 *
 * @author User
 */
public class DocumentSearchRecord extends ListGridRecord {

    private int index;
    public static String DOCUMENT_TITLE = "Document Title";
    public static String DESCRIPTION = "Description";
    public static String MIME_TYPE = "Mime Type";
    public static String FILE_PATH = "File Path";
    public static String UPLOAD_DATE = "Date Uploaded";
    public static String DOWNLOAD = "Download";

    public DocumentSearchRecord() {
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

    public String getDocumentTitle() {
        return getAttribute(DOCUMENT_TITLE);
    }

    public void setDocumentTitle(String docName) {
        setAttribute(DOCUMENT_TITLE, docName);
    }

    public String getDescription() {
        return getAttribute(DESCRIPTION);
    }

    public void setDescription(String des) {
        setAttribute(DESCRIPTION, des);
    }

    public String getMimeType() {
        return getAttribute(MIME_TYPE);
    }

    public void setMimeType(String mimeType) {
        setAttribute(MIME_TYPE, mimeType);
    }

    public String getUploadedDate() {
        return getAttribute(UPLOAD_DATE);
    }

    public void setUploadedDate(String date) {
        setAttribute(UPLOAD_DATE, date);
    }

    public String getDownload() {
        return getAttribute(DOWNLOAD);
    }

    public void setDownload(String dd) {
        setAttribute(DOWNLOAD, dd);
    }

    public String getDocFilePath() {
        return getAttribute(FILE_PATH);
    }

    public void setDocFilePath(String path) {
        setAttribute(FILE_PATH, path);
    }
}
