/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.resourcecenter;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.util.PropertyUtils;
import com.gsihealth.dashboard.entity.dto.DocumentResourceCenterDTO;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.List;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.gsihealth.dashboard.common.ApplicationInfo;
import com.gsihealth.dashboard.client.reports.ReportUtils;
import com.gsihealth.dashboard.client.util.DateFieldCellFormatter;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.types.Encoding;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;

/**
 *
 * @author Vinay
 */
public class ResourceCenterWindow extends Window {

    private ListGrid documentListGrid;
    // private static final int DATE_COLUMN = 3;
    private static final int DOCDESC_COLUMN = 2;
    private static final int DOWNLOAD_COLUMN = 3;

    public ResourceCenterWindow() {
        buildGui();
        DashBoardApp.restartTimer();
    }

    private void buildGui() {

        setWindowProps();

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(5);
        addItem(spacer);

        Canvas listGridLayout = buildListGridLayout();
        addItem(listGridLayout);
    }

    private void setWindowProps() {

        VLayout layout = new VLayout();
        layout.setMembersMargin(10);
        setWidth("700px");
        setHeight("500px");

        setTitle("Document Resource Center");
        setShowMinimizeButton(false);
        setShowCloseButton(true);
        setShowMaximizeButton(false);

        setIsModal(true);
        setShowModalMask(true);
        centerInPage();

        addItem(layout);
    }

    private Canvas buildListGridLayout() {
        HStack listGridLayout = new HStack();
        listGridLayout.setPadding(5);

        documentListGrid = buildListGrid();

        Canvas patientLayout = createListGridLayout(documentListGrid);
        listGridLayout.addMember(patientLayout);

        return listGridLayout;
    }

    private Canvas createListGridLayout(ListGrid theListGrid) {
        VLayout theLayout = new VLayout();

        theListGrid.setWidth(670);
        theLayout.addMember(theListGrid);

        return theLayout;
    }

    private ListGrid buildListGrid() {

        String[] columnNames = {DocumentSearchRecord.DOCUMENT_TITLE, DocumentSearchRecord.DESCRIPTION, DocumentSearchRecord.UPLOAD_DATE, DocumentSearchRecord.DOWNLOAD};
        ListGrid theListGrid = new ListGrid();

        theListGrid.setWidth(670);
        theListGrid.setHeight(450);

        ListGridField[] fields = buildListGridFields(columnNames);

        theListGrid.setFields(fields);
        theListGrid.setCellHeight(35);

        ListGridField descField = fields[DOCDESC_COLUMN];
        descField.setWrap(true);

        Canvas canvas = new Canvas();

        final DynamicForm uploadForm = new DynamicForm();
        uploadForm.setEncoding(Encoding.MULTIPART);

        ListGridField downloadField = fields[DOWNLOAD_COLUMN];
        downloadField.setAlign(Alignment.CENTER);
        downloadField.setCellFormatter(new ImageTextCellFormatter());
        downloadField.setType(ListGridFieldType.ICON);

        canvas.addChild(uploadForm);

        theListGrid.addChild(canvas);

        downloadField.addRecordClickHandler(new RecordClickHandler() {

            StringBuffer url = null;

            public void onRecordClick(RecordClickEvent event) {
                url = new StringBuffer(ReportUtils.getCurrentPageUrl());
                DocumentRecord docRecord = (DocumentRecord) event.getRecord();

                // add reference to export servlet            
                url.append("/DocumentCenterServlet" + "?param=" + docRecord.getDocFilePath());
                uploadForm.setAction(url.toString());

                uploadForm.submitForm();
            }
        });


        return theListGrid;
    }


    class ImageTextCellFormatter implements CellFormatter {

        public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
            ApplicationInfo appInfo = null;

            if (appInfo == null) {
                appInfo = new ApplicationInfo("download.png", "");
            }

            return "<center><img src='images/24x24/" + appInfo.getIconFileName() + "' style='padding: 3px;' /><br>" + appInfo.getScreenName() + "</center>";
        }
    }


    private ListGridField[] buildListGridFields(String[] columnNames) {

        ListGridField[] fields = new ListGridField[columnNames.length];

        for (int i = 0; i < columnNames.length; i++) {
            String tempColumnName = columnNames[i];
            ListGridField tempField = buildListGridField(tempColumnName);
            fields[i] = tempField;
        }

        return fields;
    }

    private ListGridField buildListGridField(String title) {
        ListGridField tempField = new ListGridField(title, title);
        tempField.setCanEdit(false);

        return tempField;
    }

    public void populate(List<DocumentResourceCenterDTO> users) {
        ListGridRecord[] docRecords = convert(users);

        documentListGrid.setData(docRecords);
        documentListGrid.markForRedraw();
    }

    private ListGridRecord[] convert(List<DocumentResourceCenterDTO> docs) {

        ListGridRecord[] records = new ListGridRecord[docs.size()];

        // convert dtos to records
        for (int index = 0; index < docs.size(); index++) {
            DocumentResourceCenterDTO tempDoc = docs.get(index);
            records[index] = PropertyUtils.convertDocumentData(index, tempDoc);
        }

        return records;
    }

    private void setDateFieldCellFormatter(ListGridField dateField) {

        final DateTimeFormat dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
        dateField.setCellFormatter(new DateFieldCellFormatter(dateFormatter));
    }
}
