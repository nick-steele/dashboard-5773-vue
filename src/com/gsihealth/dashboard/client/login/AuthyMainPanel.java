
package com.gsihealth.dashboard.client.login;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.gsihealth.dashboard.server.authy.client.TwoFactorAuthClient;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.gsihealth.dashboard.client.eventbus.EventBusUtils;
import com.gsihealth.dashboard.client.service.AdminService;
import com.gsihealth.dashboard.client.service.AdminServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.LinkItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.BlurEvent;
import com.smartgwt.client.widgets.form.fields.events.BlurHandler;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.validator.LengthRangeValidator;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 * @author ssingh
 */
public class AuthyMainPanel  extends VLayout {
    
    
    private static final String DIRECTIONS_TEXT = "<h3>Please enter your verification code from the Authy app or click 'request code via SMS'</h3>";
    private static final String REQUEST_LINK_TEXT = "<U>request code via SMS";
    private DynamicForm verificationForm;
    protected TextItem codeTextItem;
    private Button verifyButton;
    private Button cancelButton;
    private LinkItem requestCodeLinkItem;
    private LoginResult userInfo;
    private final AdminServiceAsync adminService = GWT.create(AdminService.class);
    private long communityId;
    private String email;
    private DynamicForm form;
    private AuthyVerificationWindow window;
    private String authyApikey;
    private String countryCode;
    
    
    public AuthyMainPanel(LoginResult loginResult,AuthyVerificationWindow authyVerificationWindow){
        
        window=authyVerificationWindow;
        userInfo = ApplicationContextUtils.getLoginResult();
        communityId=userInfo.getCommunityId();
        email=userInfo.getEmail();
        countryCode=userInfo.getCountryCode();
        setValuesFromApplicationProperties();
        buildGui();
    }
     private void buildGui() {
        setPadding(10);
        setMembersMargin(2);

        Label label = new Label(DIRECTIONS_TEXT);
        label.setWidth(360);
        label.setHeight(20);
//        label.setStyleName("AuthyTitleLabel");
        addMember(label);
        
     
        verificationForm=displayWindow();
        addMember(verificationForm);

        Layout buttonBar = buildButtonBar();
        buttonBar.setAlign(Alignment.CENTER);
        buttonBar.setPadding(5);
              
        addMember(buttonBar);

     }
      private DynamicForm displayWindow() {
        // form
        form = new DynamicForm();
        form.setAutoHeight();
        form.setAutoWidth();
        form.setPadding(10);
        form.setTop(30);
        form.setWrapItemTitles(true);
        form.setNumCols(2);
     
      
        codeTextItem = new TextItem("verficationCode","Enter Code");
        codeTextItem.setAlign(Alignment.CENTER);
        codeTextItem.setWrapTitle(false);
        codeTextItem.setRequired(true);
        LengthRangeValidator codeLengthValidator = new LengthRangeValidator();
        codeLengthValidator.setMax(7);
        codeLengthValidator.setErrorMessage("Invalid length. It should have 7-digits");
        codeTextItem.setValidators(codeLengthValidator);
//        codeTextItem.setValidateOnExit(true);
//        codeTextItem.addBlurHandler(new UIBlurHandler());
        codeTextItem.setEndRow(true);
       
        // request SMS link
        requestCodeLinkItem = new LinkItem();
        requestCodeLinkItem.setValue(REQUEST_LINK_TEXT);
        requestCodeLinkItem.setName("ManagePatientPrograms");
        requestCodeLinkItem.setShowTitle(false);
        requestCodeLinkItem.setEndRow(true);
        requestCodeLinkItem.addClickHandler(new RequestCodeLinkClickHandler());
        
          SpacerItem space= new SpacerItem();
          space.setWidth(60);

        form.setFields( codeTextItem,space,requestCodeLinkItem);
       return form;
    }
//      class UIBlurHandler implements BlurHandler {
//
//        @Override
//        public void onBlur(BlurEvent event) {
//            codeTextItem.validate();
//        }
//    }
//   
   
   
    private Layout buildButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        verifyButton = new Button("Verify Code");
        cancelButton = new Button("Cancel");
        verifyButton.addClickHandler(new VerifyCodeClickHandler());
        cancelButton.addClickHandler(new CancelClickHandler());
        buttonLayout.addMember(verifyButton);
        buttonLayout.addMember(cancelButton);
        return buttonLayout;
    }
    
     class RequestCodeLinkClickHandler implements ClickHandler {

        public void onClick(ClickEvent ce) {
         
           adminService.requestSms(email, communityId, authyApikey,countryCode, new RequestSmsCallback("SMS sent successfully "));
        }
    }
     
     class VerifyCodeClickHandler implements com.smartgwt.client.widgets.events.ClickHandler{
         
         public void onClick(com.smartgwt.client.widgets.events.ClickEvent ce){
             if(form.validate()){
             String token= codeTextItem.getValueAsString();
             adminService.verifyToken(token, email, communityId,authyApikey,countryCode, new verfiyTokenCallback());
         }}
     }

    public class RequestSmsCallback<T> extends PortalAsyncCallback<T> {
   
    protected String confirmationMessage;
       
    public RequestSmsCallback( String theConfirmationMessage) {
       
        confirmationMessage = theConfirmationMessage;
       
    }

        @Override
        public void onSuccess(Object t) {

            SC.say(confirmationMessage);

        }

        @Override
        public void onFailure(Throwable exc) {

            SC.warn(exc.getMessage());

        }
    }

    
    public class verfiyTokenCallback<T> extends PortalAsyncCallback<T> {
   
    @Override
    public void onSuccess(Object t) {
  
        // fire event
            EventBus eventBus = EventBusUtils.getEventBus();
            eventBus.fireEvent(new VerifyEvent(userInfo));
            window.destroy();

    }
        @Override
        public void onFailure(Throwable exc) {
             
            SC.warn(exc.getMessage());
            verifyButton.setTitle("Re-verify code");
            codeTextItem.clearValue();

        }

}
    class CancelClickHandler implements com.smartgwt.client.widgets.events.ClickHandler {

        @Override
        public void onClick(com.smartgwt.client.widgets.events.ClickEvent event) {
            // fire event
            EventBus eventBus = EventBusUtils.getEventBus();
            eventBus.fireEvent(new CancelEvent());
            window.destroy();
           
        }
    }
    public void setValuesFromApplicationProperties(){
         ClientApplicationProperties props= ApplicationContextUtils.getClientApplicationProperties();
         authyApikey=props.getAuthyApiKey();
         
    }
            
}
