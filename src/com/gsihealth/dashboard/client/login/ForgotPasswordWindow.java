package com.gsihealth.dashboard.client.login;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.service.SecurityServiceAsync;
import com.gsihealth.dashboard.client.util.Constants;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
/**
 *
 * @author Chad Darby
 */
public class ForgotPasswordWindow extends Window {

    private SecurityServiceAsync securityService;
    private TextItem portalUserIdTextItem;
    private DynamicForm form;
    private ProgressBarWindow progressBarWindow;
    private ForgotPasswordWindow forgotPasswordWindow;
    private long communityId;

    public ForgotPasswordWindow(SecurityServiceAsync theSecurityService, long communityId,String defaultPortalUserId) {

        forgotPasswordWindow = this;
        securityService = theSecurityService;
        this.communityId = communityId;
        buildGui();
        portalUserIdTextItem.setValue(defaultPortalUserId);
    }
     
 

    private void buildGui() {

        VLayout layout = new VLayout();
         

        setWidth(350);
        setHeight(115);
        setTitle("Forgot Password");
        setShowMinimizeButton(false);
        setIsModal(true);
        setShowModalMask(true);
        centerInPage();
        addCloseClickHandler(new CloseClickHandler() {

         public void onCloseClick(CloseClickEvent event) {
                destroy();
            }
        });

        form = new DynamicForm();
        form.setTop(20);
        form.setHeight(50);
        form.setWidth(100);
        form.setPadding(5);
        portalUserIdTextItem = new TextItem();
        portalUserIdTextItem.setTitle("Enter Email Address");
        portalUserIdTextItem.setWrapTitle(false);
        portalUserIdTextItem.setWidth(200);
      
        portalUserIdTextItem.addKeyPressHandler(new KeyPressHandler() {
            @Override
            public void onKeyPress(KeyPressEvent event) {
                
                if (event.getKeyName().equals("Enter")) {
                      
                   SendButtonClickHandler handler = new SendButtonClickHandler();
                   handler.onClick(null);
                }
            }
        });
      

        // setup validation
        portalUserIdTextItem.setRequired(true);
        RegExpValidator userIdRegExpValidator = getPortalUserIdRegExpValidator();
        portalUserIdTextItem.setValidators(userIdRegExpValidator);

        form.setFields(portalUserIdTextItem);
        layout.addMember(form);

        IButton sendButton = new IButton("Send");
        sendButton.setWidth(75);
        sendButton.setHeight(25);
        sendButton.addClickHandler(new SendButtonClickHandler());

        HLayout hlayout = new HLayout(10);
        LayoutSpacer spacer1 = new LayoutSpacer();
        spacer1.setWidth(100);
        hlayout.addMember(spacer1);
        hlayout.addMember(sendButton);

        IButton cancelButton = new IButton("Cancel");
        cancelButton.setWidth(75);
        cancelButton.setHeight(25);
        cancelButton.addClickHandler(new CancelButtonClickHandler());
        hlayout.addMember(cancelButton);

        layout.addMember(hlayout);

        addItem(layout);
    }



    private RegExpValidator getPortalUserIdRegExpValidator() {
        RegExpValidator validator = new RegExpValidator();
        validator.setExpression(Constants.EMAIL_VALIDATION_REGEX);
        validator.setErrorMessage("User ID must be an email address.");
        return validator;
    }

    class SendButtonClickHandler implements ClickHandler {


        @Override
        public void onClick(ClickEvent event) {

            if (form.validate()) {
                
                progressBarWindow = new ProgressBarWindow("Sending email...");
                progressBarWindow.show();
                String portalUserId = (String) portalUserIdTextItem.getValue();
            
                securityService.forgotPassword(portalUserId,communityId, new ForgotPasswordCallback(portalUserId, progressBarWindow, forgotPasswordWindow));
            }
        }
    }

    class CancelButtonClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            destroy();
        }
    }

    public static class ForgotPasswordCallback implements AsyncCallback<Boolean> {

        private String portalUserId;
        private ForgotPasswordWindow forgotPasswordWindow;
        private ProgressBarWindow progressBarWindow;

        ForgotPasswordCallback(String portalUserId, ProgressBarWindow progressBarWindow, ForgotPasswordWindow forgotPasswordWindow){
            super();
            this.portalUserId = portalUserId;
            this.forgotPasswordWindow = forgotPasswordWindow;
            this.progressBarWindow = progressBarWindow;
        }

        @Override
        public void onSuccess(Boolean result) {

            progressBarWindow.hide();

            if (result.booleanValue()) {
                SC.say("An email has been sent to <b>" + portalUserId + "</b>.");
                if(forgotPasswordWindow != null) forgotPasswordWindow.destroy();
            } else {
                SC.say("Sorry. We can not find the user ID: <b>" + portalUserId + "</b>.");
            }
        }

        @Override
        public void onFailure(Throwable exc) {
            progressBarWindow.hide();
            SC.say("An error has occurred.");
        }
    }
    
    
}
