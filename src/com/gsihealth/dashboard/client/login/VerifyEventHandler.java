/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.login;

import com.google.gwt.event.shared.EventHandler;

/**
 *
 * @author ssingh
 */
public interface VerifyEventHandler extends EventHandler{
    
    /**
     * Called when an verify event is fired
     * 
     * @param verifyEvent 
     */
    public void onVerify(VerifyEvent verifyEvent);
    
}
