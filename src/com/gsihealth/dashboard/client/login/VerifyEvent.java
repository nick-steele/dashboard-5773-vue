/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.login;

import com.google.gwt.event.shared.GwtEvent;
import com.gsihealth.dashboard.entity.dto.LoginResult;

/**
 *
 * @author ssingh
 */
public class VerifyEvent  extends GwtEvent<VerifyEventHandler>{
    
     public static Type<VerifyEventHandler> TYPE = new Type<VerifyEventHandler>();
     private LoginResult loginResult;
    
     public VerifyEvent(LoginResult login){
     this.loginResult=login;   
        
    }
    
     @Override
    public GwtEvent.Type<VerifyEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(VerifyEventHandler handler) {
        handler.onVerify(this);
    }
    
     public LoginResult getLoginResult() {
         return loginResult;
     }
}
