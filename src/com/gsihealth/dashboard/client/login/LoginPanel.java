package com.gsihealth.dashboard.client.login;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.eventbus.EventBusUtils;
import com.gsihealth.dashboard.client.service.AdminService;
import com.gsihealth.dashboard.client.service.AdminServiceAsync;
import com.gsihealth.dashboard.client.service.SecurityService;
import com.gsihealth.dashboard.client.service.SecurityServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.gsihealth.dashboard.redux.Redux;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;

import java.util.Date;
//import javax.ws.rs.core.Response;

/**
 * @author Chad Darby
 */
public class LoginPanel extends VLayout {

    private final SecurityServiceAsync securityService = GWT.create(SecurityService.class);
    private final AdminServiceAsync adminService = GWT.create(AdminService.class);
    private DynamicForm loginForm;
    private TextItem portalUserIdTextItem;
    private PasswordItem portalPasswordItem;
    private Button loginButton;
    private Button cancelButton;
    private Img loginLogo;
    private StaticTextItem attestTextItem;
    private boolean checkEnable = true;
    private String attestLabel = "<font color='red'></font>";
    private static int comparison;
    private Date date;
    private Date date2;
    private LoginResult loginResult;
    private UserDTO userDto;
    private String sams;
    private Long communityId;
    private Date lastMfaVerificationDate;
    private static int compare;
    private boolean enableClientMFA;
    private boolean allowSubsequentUserLogin;
    private HandlerRegistration handlerRegistration;

//    private DynamicForm midForm;
//    private LinkItem federalCheckLinkItem, articleCheckLinkItem, nysCheckLinkItem;
    public LoginPanel() {

        buildGui();
        subscribeToBus();
    }

    @Deprecated
    private void buildGui() {

        setBackgroundColor("white");
        setBorder("1px solid #c0c0c0");
        setHeight(150);
//        setLeft(200);

        this.setPadding(30);
        setKeepInParentRect(true);

        HLayout topLayout = new HLayout();
        topLayout.setHeight(10);

        loginLogo = new Img("gsihealthcoordinator.jpg", 288, 38);
        loginLogo.hide();
        topLayout.addMember(loginLogo);
        addMember(topLayout);

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(25);
        addMember(spacer);

    }

    protected void popUpsBeforeSuccessfulLogin(LoginResult login) {

        if (login.isAuthenticated()) {
            handleSuccessfulLogin(login);
        } else {
            // clean up
            Redux.loginTimer("login_failure");
            loginButton.setDisabled(false);
            SC.say("Error", login.getErrorMessage());
        }

    }

    /**
     * Handle the successful login
     *
     */
    protected void handleSuccessfulLogin(LoginResult theLoginResult) {

        Redux.log("Login successful");
        DashBoardApp.completeAppPrerequisite(theLoginResult);

        // clean up
        hide();

        RootPanel loginPanelWrapper = RootPanel.get("gwtLoginPanelWrapper");
        if (loginPanelWrapper != null) {
            loginPanelWrapper.getElement().getStyle().setDisplay(Style.Display.NONE);
        }

        loginResult = ApplicationContextUtils.getLoginResult();

        long userId = loginResult.getUserId();
        communityId = loginResult.getCommunityId();
        Redux.loginTimer("samhsa_start");
        adminService.getUser(userId, communityId, new GetUsersSamhsaCallback());
    }

    private static String getDate(Date programDate) {
        DateTimeFormat dtf = DateTimeFormat.getFormat("MM/dd/yyyy");
        String programDateStr = dtf.format(programDate);
        return programDateStr;
    }

    public void showChangePasswordWindow(String email, boolean showSamsha) {
        Redux.loginTimer("change_password");
        // show modal popup window
        ChangePasswordWindow changePasswordWindow = new ChangePasswordWindow(securityService, email, showSamsha);
        changePasswordWindow.show();
    }

    public class GetUsersSamhsaCallback implements AsyncCallback<UserDTO> {
            private String sams;

        @Override
        public void onSuccess(UserDTO userDto) {
            Redux.loginTimer("samhsa_success");
            date = userDto.getSamhsaDate();

            Date currentDate = new java.util.Date();
            String cd = getDate(currentDate);

            if (date != null) {
                String sams = getDate(date);
                comparison = (cd).compareTo(sams);
            }
            String email = loginResult.getEmail();
            boolean showSamsha = false;
            // CurrentDate > SamhsaDate -> this is the first time user logging in today. Doesn't matter Samhsa_accepted = 'Y" or 'N' -> it must appear
            if (comparison == 1){
                showSamsha = true;
            }
            // CurrentDate == SamhsaDate ->
            else if (comparison == 0){
                if (loginResult.hasSamhsa()) {
                    showSamsha = true;
                }
                else{
                    showSamsha = false;
                }
            }
            boolean showEula = loginResult.hasEula() ? false : true;
            boolean changePwd = loginResult.isMustChangePassword() ? true : false;
            if (!showEula && !showSamsha && !changePwd) {  // no eula, no cpwd, no samsha
                DashBoardApp.showMainApplication();
            } else if (!showEula && changePwd) { // no eula, cpwd, samsha
                showChangePasswordWindow(email, showSamsha);
            } else if (!showEula && !changePwd && showSamsha) { // no eula, no cpwd, samsha
                DashBoardApp.showSamhsaWindow();
            } else { // eula
                DashBoardApp.showEulaWindow(showSamsha, email, changePwd);
            }
        }

        @Override
        public void onFailure(Throwable exc) {
            Redux.loginTimer("login_failure");
            SC.warn("No Record Found.");
        }
    }

    public void init(boolean loginLogoVisible, String loginLogoFileName, int loginLogoWidth,
            int loginLogoHeight, String attest, boolean enable, boolean enableClientMfa, boolean allowSubsequentLogin, Long communityId) {

        enableClientMFA = enableClientMfa;
        allowSubsequentUserLogin = allowSubsequentLogin;
        if (loginLogoVisible) {
            loginLogo.show();
            loginLogo.setSrc(loginLogoFileName);
            loginLogo.setWidth(loginLogoWidth);
            loginLogo.setHeight(loginLogoHeight);

            loginLogo.markForRedraw();
        } else {
            loginLogo.hide();
        }
        System.out.println("enableL:::::::" + enable);

        loginForm = new DynamicForm();
        portalUserIdTextItem = new TextItem("portalUserId", "User ID/Email");
        portalUserIdTextItem.setName("UserID");
        portalUserIdTextItem.setWidth(220);
        portalUserIdTextItem.setEndRow(true);

        portalPasswordItem = new PasswordItem("portalPassword", "Password");
        portalPasswordItem.setName("Password");
        portalPasswordItem.setWidth(220);
        this.communityId = communityId;
        portalPasswordItem.addKeyPressHandler(new KeyPressHandler() {

            @Override
            public void onKeyPress(KeyPressEvent event) {
                if (event.getKeyName().equals("Enter") && !loginButton.isDisabled()) {
                    ClickHandler handler = new LoginClickHandler();
                    handler.onClick(null);
                }
            }
        });

        loginForm = new DynamicForm();
        loginForm.setWidth(200);
        loginForm.setWrapItemTitles(false);
        loginForm.setCellPadding(5);
        loginForm.setNumCols(8);

//        LinkItem forgotPasswordLinkItem = new LinkItem("linkItem");
//        forgotPasswordLinkItem.setLinkTitle("Forgot Password?");
//        forgotPasswordLinkItem.setName("ForgotPassword");
//        forgotPasswordLinkItem.setWidth(100);
//        forgotPasswordLinkItem.setShowTitle(false);
//        forgotPasswordLinkItem.addClickHandler(new ForgotPasswordClickHandler());
//        forgotPasswordLinkItem.setEndRow(true);

        if (enable == true) {
            attestTextItem = new StaticTextItem("attestTextItem", attestLabel);
//             attestTextItem.setLeft(15);
//             attestTextItem.setWidth(10);
//             attestTextItem.setEndRow(true);
            attestTextItem.setVisible(true);
            attestTextItem.setVisible(enable);
            attestTextItem.setColSpan(5);
            attestTextItem.setShowTitle(false);
            attestTextItem.setDefaultValue(attest);

            loginForm.setFields(portalUserIdTextItem, portalPasswordItem, attestTextItem);
        } else {
            loginForm.setFields(portalUserIdTextItem, portalPasswordItem);
        }
        addMember(loginForm);

        HLayout midLayout = new HLayout();
        midLayout.setHeight(10);

//        midForm = new DynamicForm();
//        midForm.setNumCols(4);
//        midForm.setWidth(100);
//        midForm.setWrapItemTitles(false);
//      
        HLayout buttonLayout = new HLayout(10);

        LayoutSpacer buttonSpacer = new LayoutSpacer();
        buttonSpacer.setWidth(73);
        buttonLayout.addMember(buttonSpacer);

        loginButton = new Button("Login");
        loginButton.setID("Login");

        loginButton.addClickHandler(new LoginClickHandler());
        buttonLayout.addMember(loginButton);

        cancelButton = new Button("Cancel");
        cancelButton.setID("Cancel");
        cancelButton.addClickHandler(new CancelClickHandler());
        buttonLayout.addMember(cancelButton);

        if (enable) {
            loginButton.setDisabled(false);
            addMember(midLayout);
//            addMember(spacer2);
            addMember(buttonLayout);
        } else {
            loginButton.setDisabled(false);
//            addMember(spacer2);
            addMember(buttonLayout);
        }

        //Should create JNDI functions after loginPanel init
        Redux.declareLoginPanelMethod(this);

        Redux.log("LoginPanel init done");
        Redux.setGwtReady();
    }

    @Deprecated
    class LoginClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            loginButton.setDisabled(false);
            Redux.log("Login Button clicked");
            String portalUserId = portalUserIdTextItem.getValueAsString();
            String portalPassword = portalPasswordItem.getValueAsString();

            doLogin(portalUserId, portalPassword);
        }
    }

    public void doLogin(String portalUserId, String portalPassword) {
        if (!StringUtils.isBlank(portalUserId) && !StringUtils.isBlank(portalPassword)) {
            portalUserId = portalUserId.replaceAll("\\s+", "");
            // authenticate user
            loginButton.setDisabled(true);

            Redux.loginTimer("login_start");
            securityService.authenticate(portalUserId, portalPassword, communityId,null,
                    new LoginCallback());
            return;

        }

        if (StringUtils.isBlank(portalUserId) && StringUtils.isBlank(portalPassword)) {
            SC.warn("Please re-enter your UserID/Email and password and try to log in again.");

            clearValues();
        } else if (!StringUtils.isBlank(portalUserId)) {
            SC.warn("Please re-enter your password and try to log in again.");
            clearValues();
        } else if (!StringUtils.isBlank(portalPassword)) {
            SC.warn("Please re-enter your UserID/Email and try to log in again.");
            clearValues();
        }
    }


    private void openNewAuthyPopup(LoginResult loginUser) {
        AuthyVerificationWindow newWindow = new AuthyVerificationWindow(loginUser);
        newWindow.showSetFocus();
    }

    private void clearValues() {
//        articleCheckItem.setValue(false);
//        federalCheckItem.setValue(false);
//        nysCheckItem.setValue(false);
        loginButton.setDisabled(false);

//        nysCheck = false;
//        federalCheck = false;
//        articleCheck = false;
    }

    public class LoginCallback implements AsyncCallback<LoginResult> {

        @Override
        public void onFailure(Throwable thrwbl) {
            SC.warn("Error during login. : " + thrwbl.getMessage() + " : " + thrwbl.getCause());
            Redux.loginTimer("login_failure");
            // clean up
            loginButton.setDisabled(false);
            clearValues();
        }

        @Override
        public void onSuccess(LoginResult loginResult) {

            Redux.loginTimer("auth_success");
            ApplicationContext context = ApplicationContext.getInstance();
            context.putAttribute(Constants.LOGIN_RESULT, loginResult);
            if (loginResult.isAuthenticated()) {
                checkLoginPopups(loginResult);

            } else {
                // clean up
                loginButton.setDisabled(false);
                SC.say("Error", loginResult.getErrorMessage());
            }
        }
    }

    public void checkLoginPopups(LoginResult loginResult) {
        lastMfaVerificationDate = loginResult.getLastMfaVerificationDate();
        Date currentDate = new Date();
        String cd = getDate(currentDate);
        Boolean disableUserMfa = Boolean.parseBoolean(loginResult.getDisableUserMfa());

        if (lastMfaVerificationDate != null) {
            String lastMfaVerifiction = getDate(lastMfaVerificationDate);
            compare = (cd).compareTo(lastMfaVerifiction);

        }
        // checks if MFA is required
        if (enableClientMFA && !disableUserMfa) {
            // To show MFA for first login attempt
            if (lastMfaVerificationDate == null || compare != 0 && allowSubsequentUserLogin) {
                openNewAuthyPopup(loginResult);
            } else if (compare != 0 && !allowSubsequentUserLogin) {
                openNewAuthyPopup(loginResult);
            } // To show MFA on every login attempt
            else if (compare == 0 && !allowSubsequentUserLogin) {
                openNewAuthyPopup(loginResult);
            } else {
                // go ahead to dashboard
                popUpsBeforeSuccessfulLogin(loginResult);
            }
        } else {
            // go ahead to dashboard
            popUpsBeforeSuccessfulLogin(loginResult);
        }
    }

//    @Deprecated
//    class ForgotPasswordClickHandler implements com.smartgwt.client.widgets.form.fields.events.ClickHandler {
//
//        @Override
//        public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
//            String portalUserId = portalUserIdTextItem.getValueAsString();
//            doForgotPassword(portalUserId);
//        }
//    }

    public native String getTenantId() /*-{
        return ''+$wnd.QA.tenantId;
    }-*/;


    public void doForgotPassword(String portalUserId) {
        communityId = Long.parseLong(getTenantId());
        Window theWindow = new ForgotPasswordWindow(securityService, communityId, portalUserId);
        theWindow.show();
    }

    public void runForgotPassword(String portalUserId) {
        communityId = Long.parseLong(getTenantId());
        ProgressBarWindow progressBarWindow = new ProgressBarWindow("Sending email...");
        progressBarWindow.show();
        securityService.forgotPassword(portalUserId,communityId, new ForgotPasswordWindow.ForgotPasswordCallback(portalUserId, progressBarWindow, null));
    }

    class CancelClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            loginForm.clearErrors(true);
            loginForm.clearValues();
        }
    }

    public void clearErrors(boolean flag) {
        this.loginForm.clearErrors(flag);
    }

    public void clearFormValues() {
        this.portalPasswordItem.clearValue();
        this.portalUserIdTextItem.clearValue();
        this.loginButton.setDisabled(false);
    }

    /**
     * Subscribe as a handler for the event bus. Analogous to signing up as a
     * ClickHandler
     */
    private void subscribeToBus() {
        EventBus eventBus = EventBusUtils.getEventBus();

        VerifyEventHandler handler = new SuccessEventHandler();
        handlerRegistration = eventBus.addHandler(VerifyEvent.TYPE, handler);

        CancelHandler cancelHandler = new CancelHandler();
        handlerRegistration = eventBus.addHandler(CancelEvent.TYPE, cancelHandler);
    }

    class SuccessEventHandler implements VerifyEventHandler {

        @Override
        public void onVerify(VerifyEvent verifyEvent) {
            LoginResult result = verifyEvent.getLoginResult();
            popUpsBeforeSuccessfulLogin(result);
        }
    }

    class CancelHandler implements CancelEventHandler {

        @Override
        public void onCancel(CancelEvent cancelEvent) {
//            loginButton.setDisabled(false);
        }
    }
}
