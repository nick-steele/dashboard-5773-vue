
package com.gsihealth.dashboard.client.login;

import com.google.gwt.event.shared.GwtEvent;
import com.gsihealth.dashboard.entity.dto.LoginResult;

/**
 *
 * @author ssingh
 */
public class CancelEvent  extends GwtEvent<CancelEventHandler>{
    
     public static Type<CancelEventHandler> TYPE = new Type<CancelEventHandler>();
        
     public CancelEvent(){    
        
    }
    
     @Override
    public GwtEvent.Type<CancelEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(CancelEventHandler handler) {
        handler.onCancel(this);
    }
    
    
}
