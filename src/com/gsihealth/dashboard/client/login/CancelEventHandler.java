
package com.gsihealth.dashboard.client.login;

import com.google.gwt.event.shared.EventHandler;

/**
 *
 * @author ssingh
 */
public interface CancelEventHandler  extends EventHandler {
  
    /**
     * Called when an cancel event is fired
     * 
     * @param CancelEvent 
     */
    public void onCancel(CancelEvent cancelEvent);
}
