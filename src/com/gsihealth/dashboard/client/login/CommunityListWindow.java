package com.gsihealth.dashboard.client.login;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.callbacks.BasicPortalAsyncCallback;
import com.gsihealth.dashboard.client.common.FormUtils;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.eventbus.EventBusUtils;
import com.gsihealth.dashboard.client.service.ManagePatientProgramService;
import com.gsihealth.dashboard.client.service.ManagePatientProgramServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.entity.dto.CommunityDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author Ranga
 */
public class CommunityListWindow extends Window {

    private LoginResult loginResult;
    private List<CommunityDTO> communityDtoList;
    private DynamicForm communitySelectForm;
    private SelectItem communitySelectItem;

    public CommunityListWindow(LoginResult loginResult, List<CommunityDTO> communityDtoList) {
        this.loginResult = loginResult;
        this.communityDtoList = communityDtoList;

        buildGui();
    }

    private void buildGui() {
        DashBoardApp.restartTimer();

        setWindowProps();
        VLayout userlayout = new VLayout();
        communitySelectForm = getCommunityForm();
        userlayout.addMember(communitySelectForm);
        addItem(userlayout);

        VLayout buttonlayout = new VLayout();
        Canvas buttonBar = buildButtonBar();
        buttonlayout.addMember(buttonBar);
        addItem(buttonlayout);
    }

    private void setWindowProps() {
        setPadding(10);
        setWidth(350);
        setHeight(150);

        setTitle("Please select a Community");
        setShowMinimizeButton(false);
        setIsModal(true);
        setShowModalMask(true);

        centerInPage();
    }

    private DynamicForm getCommunityForm() {

        DynamicForm theForm = new DynamicForm();
        theForm.setPadding(10);
        theForm.setBrowserSpellCheck(false);

        communitySelectItem = new SelectItem("communitySelectItem", "Select Community");
        communitySelectItem.setRequired(false);
        communitySelectItem.setWrapTitle(false);
        communitySelectItem.setRequired(true);
        communitySelectItem.setWidth(200);
        communitySelectItem.setTabIndex(1);
        communitySelectItem.setValueMap(getCommunityCodes());

        theForm.setFields(communitySelectItem);
        return theForm;
    }

    private HLayout buildButtonBar() {
        Button okButton = new Button("OK");
        okButton.setID("OK");
        okButton.addClickHandler(new OkButtonClickHandler());

        HLayout buttonBar = new HLayout();
        buttonBar.setWidth(310);
        buttonBar.setAlign(Alignment.RIGHT);

        buttonBar.addMember(okButton);

        return buttonBar;
    }

    class OkButtonClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            if (communitySelectForm.validate()) {
                //set community here
                loginResult.setCommunityId((Long)communitySelectItem.getValue());

                hide();
                destroy();
                DashBoardApp.showMainApplication();                
            }
        }
    }

    private LinkedHashMap<Long, String> getCommunityCodes() {
        LinkedHashMap<Long, String> valueMap = new LinkedHashMap<Long, String>();
        for (CommunityDTO communityDTO : communityDtoList) {
            valueMap.put(communityDTO.getCommunityId(), communityDTO.getCommunityName());
        }

        return valueMap;
    }

}
