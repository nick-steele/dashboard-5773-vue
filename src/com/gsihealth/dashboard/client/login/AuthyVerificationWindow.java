
package com.gsihealth.dashboard.client.login;

import com.gsihealth.dashboard.client.common.Cancelable;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;

/**
 *
 * @author ssingh
 */
public class AuthyVerificationWindow extends Window implements Cancelable{

    private static final String WINDOW_TITLE_TEXT = "Authy Verification";
    private  AuthyMainPanel panel;
    private LoginResult userInfo;
    
    public AuthyVerificationWindow (LoginResult loginResult){
        this.buildGui();
        this.userInfo=loginResult;
       
    }
    
    private void buildGui() {
        setPadding(10);
        setWidth(400);
        setHeight(230);
        setTitle(WINDOW_TITLE_TEXT);
        setShowMinimizeButton(false);
        setIsModal(true);
        setShowModalMask(true);
        centerInPage();
       
        panel= new AuthyMainPanel(userInfo,this);
        addItem(panel);
        this.addCloseClickHandler(new VerficationWindowCloseClickHandler());
       
    }
     
     
  
    @Override
    public void clearErrors(boolean flag) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clearValues() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
    public void showSetFocus() {
        this.show();
        this.panel.codeTextItem.focusInItem();
    }
    
     class VerficationWindowCloseClickHandler implements CloseClickHandler {

        @Override
        public void onCloseClick(CloseClickEvent event) {
            destroy();
           
        }
    }

}
