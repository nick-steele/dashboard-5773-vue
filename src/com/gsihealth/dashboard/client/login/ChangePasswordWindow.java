package com.gsihealth.dashboard.client.login;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.login.form.SecurityForm;
import com.gsihealth.dashboard.client.service.SecurityServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.redux.Redux;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;


/**
 *
 * @author Chad Darby
 */
public class ChangePasswordWindow extends Window {

    private SecurityServiceAsync securityService;
    private String portalUserId;
    private boolean showSamsha;
    private SecurityForm securityForm;
    private SendButtonClickHandler searchClickHandler;
    private Button sendButton;

    public ChangePasswordWindow(SecurityServiceAsync theSecurityService, String thePortalUserId, boolean showSamsha) {
        securityService = theSecurityService;
        portalUserId = thePortalUserId;
        this.showSamsha = showSamsha;
        buildGui();
    }

    private void buildGui() {


        VLayout layout = new VLayout();
        layout.setMembersMargin(10);
        setWidth(425);
        setHeight(200);
        setTitle("Change Password ");
        setShowMinimizeButton(false);
        setShowCloseButton(true);
        setShowMaximizeButton(false);

        setIsModal(true);
        setShowModalMask(true);
        centerInPage();

        searchClickHandler=new SendButtonClickHandler();

        securityForm = new SecurityForm();
        securityForm.setTop(20);
        securityForm.setPadding(5);

        TextItem userIdTextItem = securityForm.getPortalUserIdTextItem();
        userIdTextItem.setValue(portalUserId);
        userIdTextItem.disable();

        layout.addMember(securityForm);

        securityForm.getConfirmPasswordTextItem().addKeyPressHandler(new KeyPressHandler() {

            @Override
            public void onKeyPress(KeyPressEvent event) {

                if (event.getKeyName().equals("Enter") && !sendButton.isDisabled()) {
                    SendButtonClickHandler handler = new SendButtonClickHandler();
                    handler.onClick(null);
                }
            }
        });

        sendButton = new Button("Save");
        sendButton.setWidth(75);
        sendButton.setHeight(25);
        sendButton.addClickHandler(new SendButtonClickHandler());

        HLayout buttonBar = new HLayout(10);
        buttonBar.setWidth100();
        buttonBar.setAlign(Alignment.CENTER);
        buttonBar.addMember(sendButton);

        layout.addMember(buttonBar);

        addItem(layout);

        this.addCloseClickHandler(new WinCloseClickHandler());
    }

    class WinCloseClickHandler implements CloseClickHandler {

        public void onCloseClick(CloseClickEvent event) {

            hide();

            String message = "<b>You did not change your password as requested.</b><br><b>You will not be able to access the application until you do so.</b><br><b>The application will return you to the loging screen.</b>";
            SC.warn(message, new SendToLogoutPageCallback());
        }
    }

    class SendToLogoutPageCallback implements BooleanCallback {

        @Override
        public void execute(Boolean value) {
            //Spira 6164
            DashBoardApp.doLogout();
        }
    }

    class SendButtonClickHandler implements ClickHandler {
        @Override
        public void onClick(ClickEvent event) {

            if (securityForm.validate()) {
                sendButton.disable();
                String password = securityForm.getPortalPassword();
                securityService.changePassword(portalUserId, password, new ChangePasswordCallback());
//                SC.say("Your password has been changed.", new ConfirmPasswordCallback());
                SC.say("Your password has been changed.",new ConfirmPasswordCallback());
            }
        }
    }
    class ConfirmPasswordCallback implements BooleanCallback {

        @Override
        public void execute(Boolean value) {
            if (!showSamsha) {
                DashBoardApp.showMainApplication();
            } else {
                DashBoardApp.showSamhsaWindow();
            }
        }
    }

    class ChangePasswordCallback implements AsyncCallback<String> {

        @Override
        public void onSuccess(String result) {
            Redux.jsEvent("local:login:syncTokenFromGWT", result);
            completeProcess();
        }

        @Override
        public void onFailure(Throwable exc) {
            SC.warn(exc.getMessage());
        }

        private void completeProcess() {
            sendButton.enable();

//           SC.say("Your password has been changed.");

            LoginResult loginResult = ApplicationContextUtils.getLoginResult();
            loginResult.setMustChangePassword(false);

            // clean up
            ChangePasswordWindow.this.hide();
            ChangePasswordWindow.this.destroy();

            //load application
//            DashBoardApp.showMainApplication();


        }
    }
}
