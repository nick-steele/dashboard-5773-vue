package com.gsihealth.dashboard.client.login;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.service.AdminService;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.layout.VLayout;
import com.gsihealth.dashboard.client.service.AdminServiceAsync;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.widgets.HTMLFlow;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;

/**
 *
 * @author Satyendra Singh
 */
public class BottomPanel extends VLayout {

    private StaticTextItem buildnumberItem;  
    private AdminServiceAsync adminService = GWT.create(AdminService.class);
    
    private boolean localPrivacyPolicyVisible;
    private String localPrivacyPolicyFooterFileName;
    
    private VLayout bottomLayout= new VLayout();
    private HLayout bottomLayout1stRow= new HLayout();
    private Label copyrightItem = new Label();
    private Label spacerItem = new Label();
    DynamicForm buildNumberForm = new DynamicForm();
    private HTMLFlow privacyItem = new HTMLFlow();   
    private Img logo = new Img();
    private String copyrightMessage = "";
    
   
      
    public BottomPanel() {
        
    }

    
    public void init(boolean privacyPolicyVisible,String privacyPolicyFooterFileName ) {
        
        localPrivacyPolicyVisible = privacyPolicyVisible;
        localPrivacyPolicyFooterFileName = privacyPolicyFooterFileName;   
         buildGui();

        // Service call to get the build release version
//        adminService.getBuildVersion(new GetBuildVersionAction());
          adminService.getBuildVersion(new GetBuildVersionAction());
      
    }

    
    public void buildGui() {
        
        java.util.Date theDate = new java.util.Date();
        DateTimeFormat formatter =  DateTimeFormat.getFormat("yyyy");
        String theCurrentYear =  formatter.format(theDate);
        copyrightMessage = "Copyright © 2012 - " + theCurrentYear + " GSI Health, Inc. <br/>All Rights Reserved.";
        
        //Bottom Layout mutlple rows
         // Build number layout 
 
        setTop(750);
        setLeft(200);
        bottomLayout.setHeight(30);
        bottomLayout.setAlign(VerticalAlignment.CENTER);
            
        // Build number layout 
       // HLayout bottomLayout1stRow= new HLayout();
        setTop(750);
        setLeft(200);
        bottomLayout1stRow.setHeight(70);
        bottomLayout1stRow.setAlign(VerticalAlignment.CENTER);
        SpacerItem spacerItem1 = new SpacerItem();
        spacerItem1.setColSpan(3);

        // Build label
        buildnumberItem = new StaticTextItem("build", "Build:");
        
        // Build Number Form
        buildNumberForm.setHeight(10);
        buildNumberForm.setWidth(200);
        // add build label to form
        buildNumberForm.setFields(spacerItem1, buildnumberItem);
        
        //Spacer Item to offset Builder Number om Login Page
        spacerItem.setContents(" ");
        spacerItem.setWidth(300);
        
        //Copyright Notice
        copyrightItem.setContents(copyrightMessage);
        copyrightItem.setWidth(300);
        copyrightItem.setValign(VerticalAlignment.CENTER);
        copyrightItem.setAlign(Alignment.CENTER);
        
        //Add GSI logo        
        logo.setWidth(128);
        logo.setHeight(70);
        logo.setSrc("gsi_logo.jpg");
        
        bottomLayout1stRow.addMember(copyrightItem);
        
        //Privacy Notice
        if (localPrivacyPolicyVisible && StringUtils.isNotBlank(localPrivacyPolicyFooterFileName)) {
         
            String contextPath = Window.Location.getPath().replace("/index.jsp", "");         
            localPrivacyPolicyFooterFileName = contextPath + localPrivacyPolicyFooterFileName;
            
           
            privacyItem.setAlign(Alignment.CENTER);
            // privacyItem.setWidth(200);
           
            System.out.println(" Context Path: " + contextPath
                    + " contextPath +  localPrivacyPolicyFooterFileName:" + localPrivacyPolicyFooterFileName);
        
            privacyItem.setContentsURL(localPrivacyPolicyFooterFileName);
            bottomLayout1stRow.addMember(privacyItem);
            
        }
        bottomLayout1stRow.addMember(spacerItem);
        bottomLayout1stRow.addMember(buildNumberForm);
        bottomLayout1stRow.addMember(logo);
     
        //connect BottomPanel to LoginPabel instead of bottomLayout
        //otherwise it will be overwritten when screen is resized.
        bottomLayout.addMember(bottomLayout1stRow);
        
        //Only show build number when LoginPanel is displayed
         logo.setVisible(false);
         copyrightItem.setContents("");
         privacyItem.setVisible(false);
        
        
        //Align and Display the Bottom Panel
        bottomLayout.setTop(700);
        bottomLayout.setLeft(600);
        bottomLayout.draw();
        
        addMember(bottomLayout);
        
    }
    
    public void showDashboardBottomPanelMembers(){
          
        copyrightItem.setContents(copyrightMessage);
        privacyItem.setVisible(true);
        spacerItem.setVisible(false);
        buildNumberForm.setVisible(false);
        logo.setVisible(true);
        
    }
    
    class GetBuildVersionAction extends RetryActionCallback<String> {

        @Override
        public void attempt() {
            adminService.getBuildVersion(this);            
        }

        @Override
        public void onCapture(String buildVersion) {
            if (buildVersion != null || !buildVersion.equalsIgnoreCase("")) {
                buildnumberItem.setValue(buildVersion);
            }
        }
        
    }
}
