package com.gsihealth.dashboard.client.login.form;

import com.gsihealth.dashboard.client.common.Cancelable;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.PasswordStrengthValidator;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.BlurEvent;
import com.smartgwt.client.widgets.form.fields.events.BlurHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.smartgwt.client.widgets.form.validator.MatchesFieldValidator;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.form.validator.Validator;
import com.smartgwt.client.widgets.layout.VLayout;
//import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
//import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
//import com.smartgwt.client.widgets.events.ClickHandler;


/**
 *
 * @author Chad Darby
 */
public class SecurityForm extends VLayout implements Cancelable {

    private static final int FIELD_WIDTH = 220;
    private DynamicForm form;
    private TextItem portalUserIdTextItem;
    private PasswordItem passwordItem;
    private PasswordItem confirmPasswordItem;

     public SecurityForm() {
         
        buildGui();
    }
    private void buildGui() {
        form = new DynamicForm();
        form.setGroupTitle("<b>User ID & Password</b>");
        form.setIsGroup(true);
        form.setWidth(400);
        form.setWrapItemTitles(false);
        form.setCellPadding(5);
        
        setHeight(100);

        portalUserIdTextItem = buildTextItem("User ID / E-mail", true);
        RegExpValidator userIdRegExpValidator = getPortalUserIdRegExpValidator();

        portalUserIdTextItem.setValidators(userIdRegExpValidator);
        portalUserIdTextItem.setValidateOnExit(true);        
        portalUserIdTextItem.addBlurHandler(new UserIdBlurHandler());

        passwordItem = buildPasswordItem("Password");
        passwordItem.setName("chip_password");
        Validator passwordStrengthValidator = new PasswordStrengthValidator();
        passwordItem.setValidateOnExit(true);
        passwordItem.setValidators(passwordStrengthValidator);        
        passwordItem.addChangeHandler(new PasswordChangeHandler());
        
        confirmPasswordItem = buildPasswordItem("Confirm Password");
        MatchesFieldValidator matchValidator = new MatchesFieldValidator();
        matchValidator.setOtherField("chip_password"); // refs back to orig password field
        matchValidator.setErrorMessage("Passwords do not match.");
        confirmPasswordItem.setValidators(matchValidator);
        confirmPasswordItem.setValidateOnExit(true);
        
        form.setFields(portalUserIdTextItem, passwordItem, confirmPasswordItem);

        addMember(form);
    }

    private PasswordItem buildPasswordItem(String title) {
        PasswordItem thePasswordItem = new PasswordItem();
        thePasswordItem.setTitle(title);
        thePasswordItem.setWidth(FIELD_WIDTH);
        thePasswordItem.setRequired(true);
        return thePasswordItem;
    }

    private RegExpValidator getPortalUserIdRegExpValidator() {
        RegExpValidator validator = new RegExpValidator();
        validator.setExpression(Constants.EMAIL_VALIDATION_REGEX);
        validator.setErrorMessage("User ID must be an email address.");
        return validator;
    }

    public TextItem getPortalUserIdTextItem() {
        return portalUserIdTextItem;
    }

    public TextItem getConfirmPasswordTextItem() {
        return confirmPasswordItem;
    }
    
    private TextItem buildTextItem(String title, boolean required) {
        TextItem item = new TextItem();

        item.setTitle(title);
        item.setRequired(required);
        item.setWidth(FIELD_WIDTH);

        return item;
    }

    public String getPortalUserId() {
        return portalUserIdTextItem.getValue().toString();
    }

    public String getPortalPassword() {
        return passwordItem.getValue().toString();
    }

    public void setPortalUserId(String portalUserId) {
        portalUserIdTextItem.setValue(portalUserId);
    }

    public void setPortalPassword(String data) {
        passwordItem.setValue(data);
    }

    public boolean validate() {
        return form.validate();
    }

    @Override
    public void clearErrors(boolean flag) {
        form.clearErrors(flag);
    }

    @Override
    public void clearValues() {
        form.clearValues();
    }

    /**
     * Helper class to clear the confirm password once the main password field is selected
     */
    class PasswordChangeHandler implements ChangeHandler {

        @Override
        public void onChange(ChangeEvent event) {
            confirmPasswordItem.clearValue();

        }
        
    }

    class UserIdBlurHandler implements BlurHandler {

        @Override
        public void onBlur(BlurEvent event) {
            portalUserIdTextItem.validate();
        }
        
    }
    
}
