package com.gsihealth.dashboard.client.admintools;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.common.PageEventHandler;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.callbacks.GetAdminUsersCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.service.AdminService;
import com.gsihealth.dashboard.client.service.AdminServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;

/**
 *
 * @author User
 */
public class AdminSearchPanel extends VLayout {

    private boolean searchMode;
    private long organizationId;
    private long userId;
    private SearchClickHandler searchClickHandler;
    private DynamicForm navigationForm;
    private SelectItem navigationSelectItem;
    private AdminSearchForm adminSearchForm;
    private AdminSearchResultsPanel adminSearchResultsPanel;
    private Button searchButton;
    private Button clearButton;
    private final AdminServiceAsync adminService = GWT.create(AdminService.class);
    private ProgressBarWindow progressBarWindow;
    private static final String ENROLLED_PATIENTS = "Enrolled Patients";
    private GetAdminUsersCallback getUsersCallback;
    private static final int SUPER_USER = 1;
    private static final int LOCAL_ADMIN = 2;
    private int currentPageNumber;

    public AdminSearchPanel() {

        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        organizationId = loginResult.getOrganizationId();
        userId = loginResult.getUserId();

        buildAdminGui();
        DashBoardApp.restartTimer();
    }

    private void buildAdminGui() {
        searchClickHandler = new SearchClickHandler();

        navigationForm = buildNavigationForm();

        adminSearchForm = new AdminSearchForm(searchClickHandler);
        HLayout hLayout = new HLayout();
        hLayout.setWidth100();

        VLayout adminSearchLayout = buildSideSearchLayout();
        hLayout.addMember(adminSearchLayout);

        adminSearchResultsPanel = new AdminSearchResultsPanel();
        adminSearchResultsPanel.setPageEventHandler(new AdminListPageEventHandler());
        hLayout.addMember(adminSearchResultsPanel);

        addMember(hLayout);
        this.setOverflow(Overflow.AUTO);


        progressBarWindow = new ProgressBarWindow("Loading", "Loading...");
        getUsersCallback = new GetAdminUsersCallback(adminSearchResultsPanel, progressBarWindow);
        gotoPage(1);
    }

    class AdminListPageEventHandler implements PageEventHandler {

        public void handlePage(int pageNum, int pageSize) {
            long accessLevelId = ApplicationContextUtils.getLoginResult().getAccessLevelId();
            long communityId= ApplicationContextUtils.getLoginResult().getCommunityId();
            progressBarWindow.show();
            adminService.findCandidates(null, accessLevelId, communityId, pageNum, pageSize, getUsersCallback);
           
        }
    }

    class SearchClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            DashBoardApp.restartTimer();
            searchMode = true;

            if (adminSearchForm.validate()) {
                SearchCriteria searchCriteria = adminSearchForm.getSearchCriteria();

                gotoPageForCandidates(1, searchCriteria);

            }

        }
    }

    public void gotoPageForCandidates(final int pageNumber, final SearchCriteria searchCriteria) {

        progressBarWindow.setVisible(true);

        currentPageNumber = pageNumber;

        progressBarWindow.show();
    }

    class ClearClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            searchMode = false;
            DashBoardApp.restartTimer();
            adminSearchForm.reset();
        }
    }

    private DynamicForm buildNavigationForm() {

        navigationForm = new DynamicForm();
        navigationForm.setNumCols(0);

        return navigationForm;
    }

    private VLayout buildSideSearchLayout() {
        VLayout sideSearchLayout = new VLayout();

        sideSearchLayout.setHeight100();
        sideSearchLayout.setWidth(250);
        sideSearchLayout.setShowResizeBar(true);

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(10);
        sideSearchLayout.addMember(spacer);

        sideSearchLayout.addMember(navigationForm);
        sideSearchLayout.addMember(adminSearchForm);

        Layout searchButtonBar = buildSearchButtonBar();
        sideSearchLayout.addMember(searchButtonBar);

        return sideSearchLayout;
    }

    /**
     * Helper method to build button bar
     *
     * @return
     */
    private Layout buildSearchButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        buttonLayout.setWidth100();
        buttonLayout.setAlign(Alignment.CENTER);

        searchButton = new Button("Search");
        clearButton = new Button("Clear");
        buttonLayout.addMember(searchButton);
        buttonLayout.addMember(clearButton);

        searchButton.addClickHandler(searchClickHandler);
        clearButton.addClickHandler(new ClearClickHandler());

        return buttonLayout;
    }

    protected boolean isEnrolledMode() {

        String navType = navigationSelectItem.getValueAsString();

        return navType.equals(ENROLLED_PATIENTS);
    }

    class NavigationChangedHandler implements ChangedHandler {

        public void onChanged(ChangedEvent event) {
            DashBoardApp.restartTimer();
            searchMode = false;
        }
    }

    public void gotoPage(final int pageNumber) {

        TotalCountsCallback totalCountsCallback = new TotalCountsCallback(pageNumber);
        totalCountsCallback.attempt();
    }

    private class TotalCountsCallback extends RetryActionCallback<Integer> {

        private final int pageNumber;

        public TotalCountsCallback(int pageNumber) {
            this.pageNumber = pageNumber;
        }

        @Override
        public void attempt() {
            long accessLevelId = ApplicationContextUtils.getLoginResult().getAccessLevelId();
            long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();

            if (accessLevelId == SUPER_USER) {
                adminService.getTotalCount(accessLevelId, communityId,this);
            } else if (accessLevelId == LOCAL_ADMIN) {
                adminService.getLocalAdminTotalCountForUsers(accessLevelId,communityId, this);
            }
        }

        @Override
        public void onCapture(Integer total) {
            adminSearchResultsPanel.setTotalCount(total);
            adminSearchResultsPanel.gotoPage(pageNumber);
        }
    }
}
