package com.gsihealth.dashboard.client.admintools.user;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.admintools.Mode;
import com.gsihealth.dashboard.client.admintools.handlers.CancelClickHandler;
import com.gsihealth.dashboard.client.callbacks.GetUsersCallback;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.eventbus.EventBusUtils;
import com.gsihealth.dashboard.client.service.AdminService;
import com.gsihealth.dashboard.client.service.AdminServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.AccessLevelConstants;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.gsihealth.dashboard.redux.Redux;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.List;

/**
 * Allows user to update an administrator
 *
 * @author Chad Darby
 */
public class UpdateUserPanel extends VLayout {

    private static final int SUPER_USER = 1;
    private static final int LOCAL_ADMIN = 2;
    private static final int POWER_USER = 4;
    private UserListPanel usersListPanel;
    private UserForm form;
    private final AdminServiceAsync adminService = GWT.create(AdminService.class);
    private ProgressBarWindow progressBarWindow;
    private GetUsersCallback getUsersCallback;
    private String originalEmailAddress;
    private UserSearchForm userSearchForm;
    private Button searchButton;
    private Button clearButton;
    private boolean searchMode;
    private SearchClickHandler searchClickHandler;
    private int currentPageNumber;
    private List<UserDTO> userDTOs;
    private boolean enableClientMfa;   
    private String authyApiKey;

    public UpdateUserPanel() {
        
      seValuesFromtClientApplicationProperties();
      buildGui();
      
      DashBoardApp.restartTimer();
    }

    private void buildGui() {
        Label label = new Label("<h3>Update User</h3>");
        label.setWidth(200);
        label.setHeight(30);
        addMember(label);

        searchClickHandler = new SearchClickHandler();
        HLayout hLayoutSearch = new HLayout();

        VLayout adminSearchVLayout = buildSideSearchLayout();
        VLayout searchLayout = new VLayout();

        userSearchForm = new UserSearchForm(searchClickHandler);

        userSearchForm.setWidth("248px");
        searchLayout.addMember(userSearchForm);
        searchLayout.addMember(adminSearchVLayout);

        hLayoutSearch.addMember(searchLayout);
        addMember(hLayoutSearch);

        VLayout vLayout = new VLayout();

        usersListPanel = new UserListPanel();
        usersListPanel.setPageEventHandler(new UserListPageEventHandler());

        vLayout.addMember(usersListPanel);

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(20);
        addMember(spacer);

       if(enableClientMfa){
        Label labelNote = new Label("<BR><font color='red'>This site uses 2-Factor Authentication requires a user to have a mobile phone that is used for registration.</font><BR><BR><b>* indicates required field</b>");
        labelNote.setWidth(300);
        labelNote.setHeight(30);
        vLayout.addMember(labelNote);

        }
       else{
        Label labelNote = new Label("<b>* indicates required field</b>");
        labelNote.setWidth(300);
        labelNote.setHeight(30);
        vLayout.addMember(labelNote);

       }
       form = new UserForm(Mode.UPDATE);
        vLayout.addMember(form);

        addMember(spacer);
        HLayout buttonLayout = new HLayout();
        buttonLayout.setHeight("14px");
        buttonLayout.setLayoutTopMargin(5);
        Button saveButton = new Button("Save");
        Button cancelButton = new Button("Cancel");

        buttonLayout.addMember(saveButton);
        buttonLayout.addMember(cancelButton);

        vLayout.addMember(buttonLayout);
        this.setOverflow(Overflow.AUTO);

        hLayoutSearch.addMember(vLayout);

        // setup listeners on list
        ListGrid userListGrid = usersListPanel.getListGrid();
        userListGrid.addRecordClickHandler(new AdminRecordClickHandler());

        // setup listener on Update button
        saveButton.addClickHandler(new UpdateUserClickHandler());
        cancelButton.addClickHandler(new CancelClickHandler(form, userListGrid));

        progressBarWindow = new ProgressBarWindow();
        getUsersCallback = new GetUsersCallback(usersListPanel, progressBarWindow);

        gotoPage(1);
    }

    class GetTotalCountCallback extends RetryActionCallback<Integer> {

        private int pageNumber;

        GetTotalCountCallback(int thePageNumber) {
            pageNumber = thePageNumber;
        }

        @Override
        public void attempt() {
            long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
            adminService.findTotalNoOfUser(null, communityId, this);
        }

        @Override
        public void onCapture(Integer total) {
            usersListPanel.setTotalCount(total);
            usersListPanel.gotoPage(pageNumber);
        }
        
        @Override
        public void onFailure(Throwable exc) {
            if (progressBarWindow != null) {
                progressBarWindow.setVisible(false);
            }
            
            super.onFailure(exc);
        }
    }

    public void gotoPage(final int pageNumber) {
        GetTotalCountCallback getTotalCountCallback = new GetTotalCountCallback(pageNumber);
        getTotalCountCallback.attempt();
    }

    /**
     * Load list grid with new data
     */
    private void loadListWithUsers() {


        progressBarWindow.show();

        TotalCountsCallback totalCountsCallback = new TotalCountsCallback();
        totalCountsCallback.attempt();
    }

    /**
     * Updates the administrator on the server
     */
    class UpdateUserClickHandler implements ClickHandler {

        private UserRecord userRecord;

        public void onClick(ClickEvent event) {

            ListGrid userListGrid = usersListPanel.getListGrid();

            userRecord = (UserRecord) userListGrid.getSelectedRecord();

            if (userRecord == null) {
                SC.warn("Nothing selected.");
                return;
            }

            boolean blankPassword = StringUtils.isBlank(form.getPasswordItem().getValueAsString());
            boolean blankConfirmPassword = StringUtils.isBlank(form.getConfirmPasswordItem().getValueAsString());

            if (form.validate()) {

                UserDTO theUserDTO = form.getUserDTO();

                if (blankPassword && blankConfirmPassword) {
                    theUserDTO.setPasswordNotSet(true);
                } else {
                    theUserDTO.setPasswordNotSet(false);
                }

                progressBarWindow = new ProgressBarWindow();
                progressBarWindow.show();

                List<String> userApps = form.getSelectedApps();
                String loggedInEmail = ApplicationContextUtils.getLoginResult().getEmail();
                boolean isSelf = (loggedInEmail.equals(originalEmailAddress));
                // originalEmailAddress will be userDTO email addy if this is an update
                boolean checkForExistingEmailAddress = !(
                        StringUtils.equalsIgnoreCase(originalEmailAddress, theUserDTO.getEmail()));
                System.out.println("check for original email addy? " + checkForExistingEmailAddress);
                // check for updating a power user
                Character canManagePowerUser = ApplicationContextUtils.getLoginResult().getCanManagePowerUser();

                long accessLevelId = Long.parseLong(theUserDTO.getAccessLevelId());
                boolean powerUser = accessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID;
                long communityId=ApplicationContextUtils.getLoginResult().getCommunityId();
                
                if (powerUser && (AccessLevelConstants.CHAR_NO.equals(canManagePowerUser))) {
                    progressBarWindow.hide();
                    SC.warn("You don't have rights to update a power user account!");

                } else if(enableClientMfa && theUserDTO.getDisableUserMfa().equalsIgnoreCase("false") && (theUserDTO.getMobileNo()==null) ){
                    progressBarWindow.hide();
                    SC.warn("Mobile phone number required");
                } else{
                    adminService.updateUser(theUserDTO, userApps, checkForExistingEmailAddress,
                            enableClientMfa,authyApiKey,communityId,false, new UpdateUserCallback(progressBarWindow, isSelf));
                }
            }
        }
    }

    /**
     * Handles the response of server-side update
     */
    class UpdateUserCallback extends PortalAsyncCallback<String> {

        private final boolean isSelf;

        UpdateUserCallback(ProgressBarWindow window, boolean isSelf) {
            super(window);
            this.isSelf = isSelf;
        }

        @Override
        public void onSuccess(String changePwdToken) {

            form.disableMessageAppCheckbox();

            form.clearValues();
            userSearchForm.clearValues();

            loadListWithUsers();
            
            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            Redux.jsEvent("remote:appGlobal:loadMemberList");

            if(isSelf && null != changePwdToken) {
                Redux.jsEvent("local:login:syncTokenFromGWT", changePwdToken);
            }
            
            SC.say("Save successful.");            
            //After updation of users update user in Application contaxts for client and server.
            adminService.updateUserContexts(ApplicationContextUtils.getLoginResult().getCommunityId() , new AsyncCallback<List<UserDTO>>(){

                @Override
                public void onSuccess(List<UserDTO> result){
                    ApplicationContext context = ApplicationContext.getInstance();
                    context.putAttribute(Constants.PROVIDERS_FOR_CONSENT_OBTAINED_BY, result);
                    // fire event
                     EventBus eventBus = EventBusUtils.getEventBus();
                     eventBus.fireEvent(new UpdateUserContextEvent());
                }

                @Override
                public void onFailure(Throwable th){

                }
            });
        }

        @Override
        public void onFailure(Throwable exc) {

            loadListWithUsers();
            SC.warn(exc.getMessage());
            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

        }
    }

    /**
     * For each record clicked, populate form w/ user data
     */
    class AdminRecordClickHandler implements RecordClickHandler {

        public void onRecordClick(RecordClickEvent event) {
            UserRecord theRecord = (UserRecord) event.getRecord();
            int index = theRecord.getIndex();

            List<UserDTO> userDTOs = getUsersCallback.getUserDTOs();

            // get selected DTO
            UserDTO theUserDTO = userDTOs.get(index);
            originalEmailAddress = theUserDTO.getEmail();
            if (theUserDTO.getAccountLockedOut().toString().equalsIgnoreCase("Y") || theUserDTO.getMustChangePassword().toString().equalsIgnoreCase("Y")) {
                form.setIsUserEnable("No");
            } else {
                form.setIsUserEnable("Yes");
            }

            // now populate the form
            form.populateForm(theUserDTO);
            // load user apps
            loadUserApps(theUserDTO);
        }
    }

    private void loadUserApps(UserDTO theUserDTO) {

        GetUserAppsCallback getUserAppsCallback = new GetUserAppsCallback(theUserDTO);
        getUserAppsCallback.attempt();
    }

    class GetUserAppsCallback extends RetryActionCallback<List<String>> {

        private UserDTO userDTO;

        public GetUserAppsCallback(UserDTO theUserDTO) {
            userDTO = theUserDTO;
        }

        @Override
        public void attempt() {
            adminService.getUserApps(userDTO, this);
        }

        @Override
        public void onCapture(List<String> userApps) {
            form.populateUserApps(userApps);
        }
    }

    class UserListPageEventHandler implements PageEventHandler {

        @Override
        public void handlePage(int pageNum, int pageSize) {

            long accessLevelId = ApplicationContextUtils.getLoginResult().getAccessLevelId();
             long communityId= ApplicationContextUtils.getLoginResult().getCommunityId();

            progressBarWindow.show();
            SearchCriteria searchCriteria = userSearchForm.getSearchCriteria();
            adminService.findCandidates(searchCriteria, accessLevelId, communityId, pageNum, pageSize, getUsersCallback);
            progressBarWindow.hide();
        }
    }

    class SearchClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            DashBoardApp.restartTimer();
            searchMode = true;

            if (userSearchForm.validate()) {

                SearchCriteria searchCriteria = userSearchForm.getSearchCriteria();


                if ((searchCriteria.getLastName() == null) && (searchCriteria.getFirstName() == null) && (searchCriteria.getEmail() == null)) {
                    //  SC.warn("Last Name is a required field. Please enter a value and search again.");
                    SC.warn("Please enter a value and search again.");
                } else {
                    searchAdminUsers(1, searchCriteria);
                }
            }
        }
    }

    public void searchAdminUsers(final int pageNumber, final SearchCriteria searchCriteria) {

        progressBarWindow.show();
        currentPageNumber = pageNumber;

        TotalCountsCallback totalCountsCallback = new TotalCountsCallback();
        totalCountsCallback.attempt();
    }

    class TotalCountsCallback extends RetryActionCallback<Integer> {

        @Override
        public void attempt() {
            long communityId= ApplicationContextUtils.getLoginResult().getCommunityId();

            SearchCriteria searchCriteria = userSearchForm.getSearchCriteria();
            adminService.findTotalNoOfUser(searchCriteria, communityId, this);
            
        }

        @Override
        public void onCapture(Integer total) {
            usersListPanel.setTotalCount(total);

            usersListPanel.gotoPage(currentPageNumber);

            progressBarWindow.hide();

            if (total == 0) {
                progressBarWindow.setVisible(false);
                usersListPanel.clearListGrid();
                SC.warn("Search did not return any results. Please modify your search criteria.");
            }
        }

        @Override
        public void onFailure(Throwable exc) {
            progressBarWindow.setVisible(false);
            super.onFailure(exc);
        }
    }

    private VLayout buildSideSearchLayout() {
        VLayout sideSearchLayout = new VLayout();

        sideSearchLayout.setWidth(230);

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(10);
        sideSearchLayout.addMember(spacer);

        Layout searchButtonBar = buildSearchButtonBar();
        sideSearchLayout.addMember(searchButtonBar);

        return sideSearchLayout;
    }

    /**
     * Helper method to build button bar
     *
     * @return
     */
    private Layout buildSearchButtonBar() {
        HLayout buttonLayout = new HLayout(10);

        buttonLayout.setWidth(230);
        buttonLayout.setAlign(Alignment.CENTER);

        searchButton = new Button("Search");
        clearButton = new Button("Clear");
        buttonLayout.addMember(searchButton);
        buttonLayout.addMember(clearButton);

        searchButton.addClickHandler(searchClickHandler);
        clearButton.addClickHandler(new ClearClickHandler());

        return buttonLayout;



    }

    class ClearClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            searchMode = false;
            DashBoardApp.restartTimer();

            userSearchForm.clearValues();
            userSearchForm.clearErrors(true);

            loadListWithUsers();

        }
    }
     public void seValuesFromtClientApplicationProperties(){
        ClientApplicationProperties props=ApplicationContextUtils.getClientApplicationProperties();
        enableClientMfa=props.isEnableClientMFA();
        authyApiKey=props.getAuthyApiKey();
                
    }
}
