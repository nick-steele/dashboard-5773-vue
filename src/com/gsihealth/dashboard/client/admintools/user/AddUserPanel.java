package com.gsihealth.dashboard.client.admintools.user;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.admintools.Mode;
import com.gsihealth.dashboard.client.admintools.callbacks.AddUserCallback;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.service.AdminService;
import com.gsihealth.dashboard.client.service.AdminServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class AddUserPanel extends VLayout {

    private UserForm form;
    private Button saveButton;
    private Button cancelButton;
    private final AdminServiceAsync adminService = GWT.create(AdminService.class);
    private String email;
    private boolean isExistingUser;
    private boolean enableClientMfa;    
    private String authyApiKey;
   
    public AddUserPanel(String email, UserDTO userDTO, boolean isExistingUser) {
        this.email = email;
        this.isExistingUser = isExistingUser;
        setValuesFromClientApplicationProperties();
        buildGui(userDTO);
        DashBoardApp.restartTimer();
    }

    private void buildGui(UserDTO userDTO) {
        setMembersMargin(5);
        Label label = new Label("<h3>Add User</h3>");
        label.setWidth(300);
        label.setHeight(20);
        addMember(label);
     
        if(enableClientMfa){
        Label mfalabel = new Label("<font color='red'>This site uses 2-Factor Authentication requires a user to have a mobile phone that is used for registration.</font>");
        mfalabel.setWidth(300);
        mfalabel.setHeight(20);
         addMember(mfalabel);
        }
        Label labelNote = new Label("<b>* indicates required field</b>");
        labelNote.setWidth(300);
        labelNote.setHeight(30);
        addMember(labelNote);

        if(userDTO.isFound()) {
            form = new UserForm(Mode.ADD, email, userDTO, isExistingUser);
            form.populateDemographicForm();
        }
        else {
            form = new UserForm(Mode.ADD, email, null, isExistingUser);
        }
        form.enableOrDisablePasswordFields(userDTO.isInActiveInOtherCommunities());
        
        addMember(form);

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(20);
        addMember(spacer);

        Layout buttonBar = buildButtonBar();
        addMember(buttonBar);
        saveButton.addClickHandler(new AddUserClickHandler());
        cancelButton.addClickHandler(new AddUserCancelClickHandler());

        this.setOverflow(Overflow.AUTO);
    }

    /**
     * Helper method to build button bar
     *
     * @return
     */
    private Layout buildButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        saveButton = new Button("Save");
        cancelButton = new Button("Cancel");
        buttonLayout.addMember(saveButton);
        buttonLayout.addMember(cancelButton);

        return buttonLayout;
    }

    class AddUserCancelClickHandler implements ClickHandler {
        @Override
        public void onClick(ClickEvent event) {
            destroy();
            //WindowManager.updateWindow(DashboardTitles.ADMIN, new AdminToolsPanel());
        }
    }

    class AddUserClickHandler implements ClickHandler {

        private ProgressBarWindow progressBarWindow;

        @Override
        public void onClick(ClickEvent event) {

            DashBoardApp.restartTimer();

            if (form.validate()) {
                UserDTO theUserDTO = form.getUserDTO();

                List<String> userApps = form.getSelectedApps();
                // this is a brand new user
                if (!isExistingUser) {
                    Date now = new Date();
                    theUserDTO.setEffectiveDate(now);
                    theUserDTO.setEndDate(null);
                    theUserDTO.setIsDeleted('N');
                    theUserDTO.setAccountLockedOut('N');
                    theUserDTO.setFailedAccessAttempts(0);
                    theUserDTO.setLoggedIn('N');
                    theUserDTO.setMustChangePassword('N');
                    theUserDTO.setSamhsaAccepted('N');
                } else {
                    boolean blankPassword = StringUtils.isBlank(form.getPasswordItem().getValueAsString());
                    boolean blankConfirmPassword = StringUtils.isBlank(form.getConfirmPasswordItem().getValueAsString());
                    theUserDTO.setPasswordNotSet(false);
                    if (blankPassword && blankConfirmPassword) {
                        theUserDTO.setPasswordNotSet(true);
                    }
                }

                if (enableClientMfa && (theUserDTO.getMobileNo() == null)) {
                    SC.warn("Mobile phone number required");
                } else {
                    long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();

                    progressBarWindow = new ProgressBarWindow();
                    progressBarWindow.show();
                    PortalAsyncCallback addUserCallback = new AddUserCallback(progressBarWindow, "Save successful.", form);

                    // call service
                    adminService.addOrUpdateUser(theUserDTO, userApps, false, isExistingUser, enableClientMfa, authyApiKey, communityId, false, addUserCallback);
                }
            }
        }
    }
    public void setValuesFromClientApplicationProperties(){
        ClientApplicationProperties props=ApplicationContextUtils.getClientApplicationProperties();
        enableClientMfa=props.isEnableClientMFA();
        authyApiKey=props.getAuthyApiKey();
    }
}
