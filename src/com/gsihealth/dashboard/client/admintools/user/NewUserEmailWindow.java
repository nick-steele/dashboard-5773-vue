/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.admintools.user;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.admintools.AdminToolsPanel;
import com.gsihealth.dashboard.client.common.Cancelable;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.service.AdminService;
import com.gsihealth.dashboard.client.service.AdminServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.BlurEvent;
import com.smartgwt.client.widgets.form.fields.events.BlurHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.validator.LengthRangeValidator;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;

/**
 * @author Beth.Boose
 */
public class NewUserEmailWindow extends Window implements Cancelable {

    private static final String WINDOW_TITLE_TEXT = "Create a new user";
    private static final String DIRECTIONS_TEXT = "Enter an email address";
    private static final String VALIDATE_BUTTON_TEXT = "Validate User";
    private static final String FIND_USER_ERROR_MSG = " Please select a different email address";

    private String email;
    private DynamicForm emailForm;
    private Button validateUserButton;
    private TextItem emailTextItem;
    private AdminToolsPanel adminToolsPanel;
    private final AdminServiceAsync adminService = GWT.create(AdminService.class);
    private UserDTO userDTO;
    private ProgressBarWindow progressBarWindow;
    private Long communityId;
    private ClickHandler validateUserClickHandler;

    public NewUserEmailWindow(String email) {
        this.email = email;
        this.setLoggedOnUserCommunityId();
        this.validateUserClickHandler = new ValidateUserClickHandler();
        this.buildGui();

    }

    public NewUserEmailWindow(AdminToolsPanel parent) {
        adminToolsPanel = parent;
        this.setLoggedOnUserCommunityId();
        this.validateUserClickHandler = new ValidateUserClickHandler();
        this.buildGui();
    }

    public NewUserEmailWindow() {
        this.setLoggedOnUserCommunityId();
        this.buildGui();
    }

    private void setLoggedOnUserCommunityId() {
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        communityId = loginResult.getCommunityId();
    }

    private void formatWindow() {
        // form
        KeyPressHandler validateKeyPressHandler = new ValidateKeyPressHandler();
        emailForm = new DynamicForm();
        emailForm.setAutoHeight();
        emailForm.setAutoWidth();
        emailForm.setPadding(5);
        emailForm.setCellPadding(5);
        emailForm.setWrapItemTitles(false);
        emailForm.setNumCols(2);
        StaticTextItem directions = new StaticTextItem("directions", NewUserEmailWindow.DIRECTIONS_TEXT);
        directions.setWrap(false);
        directions.setWrapTitle(false);
        directions.setColSpan(2);
        // email text stuff
        emailTextItem = new TextItem();
        emailTextItem.setAlign(Alignment.LEFT);
        emailTextItem.setTitle("User ID/Email");
        RegExpValidator userIdRegExpValidator = getEmailAddressRegExpValidator();
        emailTextItem.setRequired(true);
        LengthRangeValidator emailLengthValidator = new LengthRangeValidator();
        emailLengthValidator.setMax(50);
        emailLengthValidator.setErrorMessage("Invalid length");
        emailTextItem.setValidators(userIdRegExpValidator, emailLengthValidator);
        emailTextItem.addKeyPressHandler(validateKeyPressHandler);
        // save button
        Layout buttonBar = buildButtonBar();
        buttonBar.setAlign(Alignment.CENTER);
        validateUserButton.addClickHandler(validateUserClickHandler);

        emailForm.setFields(directions, emailTextItem);
        this.addItem(emailForm);
        this.addItem(buttonBar);
    }

    private Layout buildButtonBar() {
        HLayout buttonLayout = new HLayout();
        validateUserButton = new Button(NewUserEmailWindow.VALIDATE_BUTTON_TEXT);
        buttonLayout.addMember(validateUserButton);
        return buttonLayout;
    }

    private void buildGui() {
        setPadding(10);
        setWidth(400);
        setHeight(150);
        setTitle(NewUserEmailWindow.WINDOW_TITLE_TEXT);
        setShowMinimizeButton(false);
        setIsModal(true);
        setShowModalMask(true);
        centerInPage();
        formatWindow();
        this.addCloseClickHandler(new EmailCloseClickHandler());

    }

    public void showSetFocus() {
        this.show();
        this.emailTextItem.focusInItem();
    }

    @Override
    public void clearErrors(boolean flag) {
        this.emailForm.clearErrors(flag);
    }

    @Override
    public void clearValues() {
        this.emailTextItem.clearValue();
    }

    private RegExpValidator getEmailAddressRegExpValidator() {
        RegExpValidator validator = new RegExpValidator();
        validator.setExpression(Constants.EMAIL_VALIDATION_REGEX);
        validator.setErrorMessage(Constants.USER_VALID_EMAIL_ADDRESS);
        return validator;
    }

    public String getEmail() {
        return email;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    class ValidateKeyPressHandler implements KeyPressHandler {

        @Override
        public void onKeyPress(KeyPressEvent event) {
            if (event.getKeyName().equals(Constants.ENTER_KEY)) {
                validateUserClickHandler.onClick(null);
            }
        }
    }

    class ValidateUserClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            if (emailForm.validate()) {
                email = StringUtils.trim(emailTextItem.getValueAsString().toLowerCase());
                // does user exist?
                progressBarWindow = new ProgressBarWindow();
                progressBarWindow.show();
                FindUserEmailCallback findUserEmailCallback = new FindUserEmailCallback(progressBarWindow);
                adminService.getDemographicUser(email, communityId, findUserEmailCallback);
            }
        }
    }

    class EmailCloseClickHandler implements CloseClickHandler {

        @Override
        public void onCloseClick(CloseClickEvent event) {
            destroy();
            //WindowManager.updateWindow(DashboardTitles.ADMIN, new AdminToolsPanel());
        }
    }

    class FindUserEmailCallback<UserDTO> extends PortalAsyncCallback<UserDTO> {

        com.gsihealth.dashboard.entity.dto.UserDTO uDTO;

        public FindUserEmailCallback(ProgressBarWindow progressBarWindow) {
            this.progressBarWindow = progressBarWindow;
        }

        @Override
        public void onFailure(Throwable exc) {
            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            SC.warn(exc.getMessage() + NewUserEmailWindow.FIND_USER_ERROR_MSG);
            clearErrors(true);
            clearValues();
        }

        /**
         * @param foundUserDTO
         */
        @Override
        public void onSuccess(UserDTO foundUserDTO) {
            uDTO = (com.gsihealth.dashboard.entity.dto.UserDTO) foundUserDTO;
//            if(uDTO.isUserExistInCommunity()){
//                String msg ="User " + uDTO.getEmail() + " already exists.";
//                SC.warn(msg);
//            }
            /**  1. Add user AND already exists in this community AND user is Active in this community.
             Dialog message: This user (FN LN) already exists in this community
             On data input screen, password fields are open for data entry*/

             if (uDTO.isUserExistInCommunity() && uDTO.isUserActiveInCommunity()) {
                String msg = "This user " + uDTO.getFirstName() + "," + uDTO.getLastName() + " already exists in this community";
                SC.warn(msg);
            }
            /**  2.Add user AND already exists in this community AND user is Inactive this community AND user is active in at least one other community.
             Dialog message: This user (FN LN) already exists in this community but is ‘Inactive’. This user must have their status set to ‘Active’.  You do not need to reset the password
             On data input screen, password fields are greyed out*/

            else if (uDTO.isUserExistInCommunity() && !uDTO.isUserActiveInCommunity() && uDTO.isUserActiveAtleastInOneOther()) {
                String msg = "This user " + uDTO.getFirstName() + "," + uDTO.getLastName() + " already exists in this community but is ‘Inactive’. This user must have their status set to ‘Active’.You do not need to reset the password";
                SC.warn(msg);
            }

            /**
             * 3. Add user AND already exists in this community AND user is Inactive all communities.
             Dialog message: This user (FN LN) exists in the GSIH environment but is ‘Inactive’ in this community. This user must have their status set to ‘Active’ and their password reset
             On data input screen, password fields are open for data entry
             */

            else if (uDTO.isUserExistInCommunity() && (uDTO.isInActiveInOtherCommunities() && !uDTO.isUserActiveInCommunity() )&& uDTO.isUserExsistInOneOtherCommunity()){
                String msg = "This user " + uDTO.getFirstName() + "," + uDTO.getLastName() + " exists in the GSIH environment but is ‘Inactive’ in this community. This user must have their status set to ‘Active’ and their password reset";
                SC.warn(msg);
            }
            /**
             * 4. Add user AND already exists in this community AND user is Inactive this community AND user does not exist in any other community.
             Dialog message: This user (FN LN) exists in in this community but is ‘Inactive’. This user must have their status set to ‘Active’ and their password reset
             On data input screen, password fields are open for data entry
             */
             else if (uDTO.isUserExistInCommunity() && !uDTO.isUserActiveInCommunity() && !uDTO.isUserExsistInOneOtherCommunity()) {
                String msg = "This user " + uDTO.getFirstName() + "," + uDTO.getLastName() + " exists in this community but is ‘Inactive’. This user must have their status set to ‘Active’ and their password reset";
                SC.warn(msg);
            }
            /**
             * 5. Add user AND does not exist in any community
             *   No Dialog message.  Just take user to screen to input data, password fields are open for data entry.
             */
            else if (!uDTO.isFound()) {
                adminToolsPanel.createUserPanel(email, uDTO, false);
                hide();
            }
            /**
             * 6. Add user AND does not exist in this community AND user exists in any other community AND user is Inactive in all other communities.
             * Dialog message: This user (FN LN) exists in the GSIH environment. This user must have their password reset.
             * Then take user to screen input, password fields are open for data entry. (addresses Ranga's initial comment below)
             */
            else if (!uDTO.isUserExistInCommunity() && uDTO.isUserExsistInOneOtherCommunity() &&
                    uDTO.isInActiveInOtherCommunities()) {
                String msg = "This user " + uDTO.getFirstName() + "," + uDTO.getLastName() + " exists in the GSIH environment. This user must have their password reset.";
                SC.ask("Existing user message", msg, new UseExistingUserCallback(uDTO, NewUserEmailWindow.this));
            }
            /**
             * 7. Add user AND user is active in at least one other community.
             * Dialog message: This user (FN LN) already exists in this community.  You do not need to reset their password
             * Then take user to screen input, password fields are greyed out. (addresses Ranga's initial comment below)
             */

            //FIXME : Based on Requirement this needs to be change in future.
            else if(uDTO.isUserActiveAtleastInOneOther()){
                String msg = "This user " + uDTO.getFirstName() + "," + uDTO.getLastName() + " exists in the GSIH environment. You do not need to reset their password";
                 SC.ask("Existing user message", msg, new UseExistingUserCallback(uDTO, NewUserEmailWindow.this));
            }

            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }
        }
    }

    class UseExistingUserCallback implements BooleanCallback {
        UserDTO userDTO;
        NewUserEmailWindow newUserEmailWindow;

        UseExistingUserCallback(UserDTO userDTO, NewUserEmailWindow newUserEmailWindow) {
            this.userDTO = userDTO;
            this.newUserEmailWindow = newUserEmailWindow;
        }

        @Override
        public void execute(Boolean value) {
            // if the user said yes, use found user
            if (value) {
                adminToolsPanel.createUserPanel(email, userDTO, value);
                newUserEmailWindow.hide();
            } else {
                clearErrors(true);
                clearValues();
            }
        }
    }

    class UserIdBlurHandler implements BlurHandler {

        @Override
        public void onBlur(BlurEvent event) {
            emailTextItem.validate();
        }
    }

}
