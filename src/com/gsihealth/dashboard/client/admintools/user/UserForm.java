package com.gsihealth.dashboard.client.admintools.user;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.admintools.Mode;
import com.gsihealth.dashboard.client.common.Cancelable;
import com.gsihealth.dashboard.client.common.FormUtils;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.service.AdminService;
import com.gsihealth.dashboard.client.service.AdminServiceAsync;
import com.gsihealth.dashboard.client.util.*;
import com.gsihealth.dashboard.common.AccessLevelConstants;
import com.gsihealth.dashboard.common.AppKeyConstants;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.dto.UserFormReferenceData;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.*;
import com.smartgwt.client.widgets.form.fields.events.*;
import com.smartgwt.client.widgets.form.validator.*;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.*;

/**
 *
 * @author Chad Darby
 */
public class UserForm extends VLayout implements Cancelable {

    private static final String ACTIVE_STATUS_VALUE = "ACTIVE";
    private static final String INACTIVE_STATUS_VALUE = "INACTIVE";
    private static final String STATUS_VALUE_YES = "Yes";
    private static final String STATUS_VALUE_NO = "No";
    private DynamicForm contactForm;
    private DynamicForm securityForm;
    private DynamicForm serviceSubscriptionForm;
    private SupervisorForm supervisorForm;
    private TextItem firstNameTextItem;
    private TextItem middleNameTextItem;
    private TextItem lastNameTextItem;
    private SelectItem genderSelectItem;
    private SelectItem statusSelectItem;
    private SelectItem enableSelectItem;
    private SelectItem unlockAccountSelectItem;
    private SelectItem resetLoginDateSelectItem;
    private SelectItem resetPasswordDateSelectItem;
    private DateItem dateOfBirthDateItem;
    private TextItem streetAddress1TextItem;
    private TextItem streetAddress2TextItem;
    private SelectItem organizationSelectItem;
    private DataSource orgDataSource;
    private TextItem cityTextItem;
    private SelectItem stateSelectItem;
    private SelectItem userLevelSelectItem;
    private SelectItem accessLevelSelectItem;
    private TextItem zipCodeTextItem;
    private TextItem workPhoneTextItem;
    private TextItem workPhoneExtTextItem;
    private TextItem emailTextItem;
    private TextItem npiTextItem;
    private static final int FIELD_WIDTH = 300;
    private Mode mode;
    private UserDTO userDTO;
    private PasswordItem passwordItem;
    private PasswordItem confirmPasswordItem;
    private AdminServiceAsync adminService;
    private SelectItem userPrefixSelectItem;
    private SelectItem userCredentialSelectItem;
    private LinkedHashMap<String, String> roleList;
    private CheckboxItem intraCommunityCheckBox;
    private boolean userDeactivated;
    private String isUserEnable = "true";
    private LinkedHashMap<String, String> statusMap;
    private String enableRecord = null;
    private LinkedHashMap<String, String> accessLevelsMap;
    private DateTimeFormat dateFormatter;
    private String powerUserAccessLevelDescription;
    private String email;
    private boolean isExistingUser;
    private TextItem mobileItem,countryCodeTextItem;
 
    public UserForm(Mode theMode) {
        // get access levels reference data
        powerUserAccessLevelDescription = ApplicationContextUtils.getAccessLevelMap().get(AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID);
        dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
        mode = theMode;
        adminService = GWT.create(AdminService.class);
        buildGui();
    }
    
    public UserForm(Mode theMode, String email, UserDTO userDTO, boolean isExistingUser) {
        // get access levels reference data
        powerUserAccessLevelDescription = ApplicationContextUtils.getAccessLevelMap().get(AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID);
        dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
        mode = theMode;
        this.isExistingUser = isExistingUser;
        adminService = GWT.create(AdminService.class);
        this.email = email;
        this.userDTO = userDTO;
        buildGui();
    }
    
    

    private void buildGui() {

        contactForm = buildContactForm();

        // container for the forms
        HLayout hLayout = new HLayout();

        // add the main form
        hLayout.addMember(contactForm);

        // add spacer
        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setWidth(50);
        hLayout.addMember(spacer);

        VLayout vLayout = new VLayout();
        securityForm = buildSecurityForm();
        vLayout.addMember(securityForm);

        spacer.setWidth(500);

        hLayout.addMember(spacer);

        LayoutSpacer vlayoutSpacer = new LayoutSpacer();
        vlayoutSpacer.setHeight(15);
        vLayout.addMember(vlayoutSpacer);

        serviceSubscriptionForm = buildServiceSubscriptionForm();

        if (intraHHCommunityFieldVisible()) {
            vLayout.addMember(serviceSubscriptionForm);
        }

        vLayout.addMember(vlayoutSpacer);

        if (mode == Mode.ADD) {
            supervisorForm = new SupervisorForm(415, 320);
        } else if (mode == Mode.UPDATE) {
            supervisorForm = new SupervisorForm(448, 348);
        }

        vLayout.addMember(supervisorForm);

        hLayout.addMember(vLayout);
        addMember(hLayout);

        //
        loadStatus();
        loadGender();

        loadUserCredentials();
        loadUserPrefixes();

        loadUserFormReferenceData();
    }

    /**
     *
     */
    private void loadGender() {
        LinkedHashMap<String, String> genderMap = ApplicationContextUtils.getGenderCodes();
        genderSelectItem.setValueMap(genderMap);
    }

    /**
     *
     */
    private void loadStatus() {
        LinkedHashMap<String, String> statusMap = new LinkedHashMap<String, String>();
        statusMap.put(ACTIVE_STATUS_VALUE, "Active");
        statusMap.put(INACTIVE_STATUS_VALUE, "Inactive");
        statusSelectItem.setValueMap(statusMap);
        statusSelectItem.setDefaultValue(ACTIVE_STATUS_VALUE);
    }

    /**
     * Build the contact form
     *
     * @return
     */
    private DynamicForm buildContactForm() {

        RegExpValidator alphaNumberSpecialSymbolsRegExpValidator = ValidationUtils.getAlphaNumberSpecialSymbolsRegExpValidator();
        RegExpValidator alphaOnlyRegExpValidator = ValidationUtils.getAlphaOnlyRegExpValidator();
        RegExpValidator alphaHGAARegExValidator = ValidationUtils.getAlphaHGAARegExValidator();
        RegExpValidator numberRegExValidator=ValidationUtils.getNumbersOnlyRegExpValidator();
        
        DynamicForm tempForm = new DynamicForm();
        tempForm.setIsGroup(true);
        //tempForm.setWidth(200);
        tempForm.setWidth(170);
        tempForm.setWrapItemTitles(false);
        tempForm.setCellPadding(5);
        tempForm.setNumCols(4);
        tempForm.setGroupTitle("Demographics");
        tempForm.setRequiredTitlePrefix(FormUtils.REQUIRED_TITLE_PREFIX);

        userPrefixSelectItem = buildSelectItem("Prefix", false);
        userPrefixSelectItem.setColSpan(4);

        firstNameTextItem = buildTextItem("First Name", true);
        ValidationUtils.setValidators(firstNameTextItem, alphaHGAARegExValidator, true);
        firstNameTextItem.setColSpan(4);

        middleNameTextItem = buildTextItem("Middle Name", false);
        ValidationUtils.setValidators(middleNameTextItem, alphaHGAARegExValidator, false);
        middleNameTextItem.setColSpan(4);

        lastNameTextItem = buildTextItem("Last Name", true);
        ValidationUtils.setValidators(lastNameTextItem, alphaHGAARegExValidator, true);
        lastNameTextItem.setColSpan(4);

        genderSelectItem = buildSelectItem("Gender", true);
        genderSelectItem.setValidateOnExit(false);
        genderSelectItem.setColSpan(4);

        dateOfBirthDateItem = buildDateItem("Date of Birth", true);
        dateOfBirthDateItem.setValidateOnExit(false);
        dateOfBirthDateItem.setColSpan(4);

        // add date validation rules
        DateRangeValidator dateRangeValidator = new DateRangeValidator();
        Date today = new java.util.Date();
        dateRangeValidator.setMax(today);

        dateRangeValidator.setErrorMessage("Date must be today's date or prior.");
       
        Date before = new java.util.Date(-59,00,01);
        dateRangeValidator.setMin(before);
        dateRangeValidator.setErrorMessage("Date must be between 31st Dec 1840 and todays date.");
        
        dateOfBirthDateItem.setValidators(dateRangeValidator);
        dateOfBirthDateItem.setValidateOnChange(true);
        
        streetAddress1TextItem = buildTextItem("Address 1", true);
        ValidationUtils.setValidators(streetAddress1TextItem, alphaNumberSpecialSymbolsRegExpValidator, true);
        streetAddress1TextItem.setColSpan(4);

        streetAddress2TextItem = buildTextItem("Address 2", false);
        ValidationUtils.setValidators(streetAddress2TextItem, alphaNumberSpecialSymbolsRegExpValidator, true);
        streetAddress2TextItem.setColSpan(4);

        cityTextItem = buildTextItem("City", true);
        ValidationUtils.setValidators(cityTextItem, alphaOnlyRegExpValidator, true);
        cityTextItem.setColSpan(4);

        stateSelectItem = buildSelectItem("State", true);

        stateSelectItem.setValueMap(ApplicationContextUtils.getStateCodes());
        stateSelectItem.setValidateOnExit(false);
        stateSelectItem.setDefaultValue("NY");
        stateSelectItem.setColSpan(4);

        zipCodeTextItem = buildTextItem("Zip Code", true);
        zipCodeTextItem.setValidateOnExit(true);
        zipCodeTextItem.setWidth(100);
        String zipCodeMask = "#####";
        zipCodeTextItem.setMask(zipCodeMask);
        LengthRangeValidator zipCodeValidator = new LengthRangeValidator();
        zipCodeValidator.setMax(5);
        zipCodeValidator.setMin(5);
        zipCodeValidator.setErrorMessage("Invalid length");
        zipCodeTextItem.setValidators(zipCodeValidator);
        zipCodeTextItem.setEndRow(true);

        workPhoneTextItem = buildTextItem("Telephone", false);
        workPhoneTextItem.setValidateOnExit(true);
        workPhoneTextItem.setWidth(100);
        workPhoneTextItem.setMask("###-###-####");
        LengthRangeValidator workPhoneValidator = new LengthRangeValidator();
        workPhoneValidator.setMax(10);
        workPhoneValidator.setMin(10);
        workPhoneValidator.setErrorMessage("Invalid length");
        workPhoneTextItem.setValidators(workPhoneValidator);

        workPhoneExtTextItem = buildTextItem("Ext", false);
        workPhoneExtTextItem.setValidateOnExit(true);
        workPhoneExtTextItem.setWidth(100);
        workPhoneExtTextItem.setRequired(false);
        LengthRangeValidator workPhoneExtValidator = new LengthRangeValidator();
        workPhoneExtTextItem.setMask("######");
        workPhoneExtValidator.setMax(6);
        workPhoneExtValidator.setMin(1);
        workPhoneExtValidator.setErrorMessage("Invalid length");
        workPhoneExtTextItem.setValidators(workPhoneExtValidator);
        
        mobileItem = buildTextItem("Mobile No.", false);
        mobileItem.setValidateOnExit(true);
        mobileItem.setWidth(100);
        mobileItem.setMask("###-###-####");
        LengthRangeValidator mobilePhoneValidator = new LengthRangeValidator();
        mobilePhoneValidator.setMax(10);
        mobilePhoneValidator.setMin(10);
        mobilePhoneValidator.setErrorMessage("Invalid length");
        mobileItem.setValidators(workPhoneValidator);
        mobileItem.setEndRow(true);
        
        countryCodeTextItem = buildTextItem("Country Code", false);
        ValidationUtils.setValidators(countryCodeTextItem, numberRegExValidator, true);
        countryCodeTextItem.setValidateOnExit(true);
        countryCodeTextItem.setWidth(60);
        countryCodeTextItem.setEndRow(false);
       
        userCredentialSelectItem = buildSelectItem("Credential", true);
        userCredentialSelectItem.setColSpan(4);

        npiTextItem = buildTextItem("NPI", false);
        npiTextItem.setValidateOnExit(true);
        LengthRangeValidator npiValidator = new LengthRangeValidator();
        npiTextItem.setMask("##########");
        npiValidator.setMax(10);
        npiValidator.setMin(10);
        npiValidator.setErrorMessage("Invalid length. You must enter 10 digits.");
        npiTextItem.setValidators(npiValidator);
        npiTextItem.setColSpan(4);
        
        

        tempForm.setFields(userPrefixSelectItem, firstNameTextItem, middleNameTextItem, lastNameTextItem, 
                userCredentialSelectItem, npiTextItem, genderSelectItem, dateOfBirthDateItem, 
                streetAddress1TextItem, streetAddress2TextItem, cityTextItem, stateSelectItem, 
                zipCodeTextItem, workPhoneTextItem, workPhoneExtTextItem,countryCodeTextItem,mobileItem);

        return tempForm;
    }

    /**
     * Build the security form
     *
     * @return
     */
    private DynamicForm buildSecurityForm() {
        DynamicForm tempForm = new DynamicForm();

        tempForm.setIsGroup(true);
        tempForm.setGroupTitle("Security");
        tempForm.setWidth(170);
        tempForm.setWrapItemTitles(false);
        tempForm.setCellPadding(5);
        tempForm.setNumCols(2);
        tempForm.setRequiredTitlePrefix(FormUtils.REQUIRED_TITLE_PREFIX);

        statusMap = new LinkedHashMap<String, String>();
        statusMap.put("Yes", "Yes");
        statusMap.put("No", "No");

        emailTextItem = buildTextItem("User ID/Email", true);
        emailTextItem.setDisabled(true);

        // only require passwords if we are NOT in update mode or user already exists
        boolean passwordRequiredFlag = (mode != Mode.UPDATE && !isExistingUser);
        passwordItem = buildPasswordItem("Password", passwordRequiredFlag);
        passwordItem.setName("dashboard_password");
        Validator passwordStrengthValidator = new PasswordStrengthValidator();
        passwordItem.setValidateOnExit(true);
        passwordItem.setValidators(passwordStrengthValidator);
        passwordItem.addChangeHandler(new PasswordChangeHandler());

        confirmPasswordItem = buildPasswordItem("Confirm Password", passwordRequiredFlag);
        MatchesFieldValidator matchValidator = new MatchesFieldValidator();
        matchValidator.setOtherField("dashboard_password"); // refs back to orig password field
        matchValidator.setErrorMessage("Passwords do not match.");
        confirmPasswordItem.setValidators(matchValidator);
        confirmPasswordItem.setValidateOnExit(true);
        organizationSelectItem = buildOrgSelectItem("Organization", true);
        //spira 5389-user adding user without organization name will show "Field is Required"
        organizationSelectItem.setRequired(true);
        organizationSelectItem.setValidateOnExit(false);
        organizationSelectItem.setColSpan(4);

        organizationSelectItem.addChangedHandler(new ChangedHandler() {
            @Override
            public void onChanged(ChangedEvent event) {
                String orgId = (String) event.getValue();
                Long targetUserId = null;
                if (userDTO != null) {
                    targetUserId = userDTO.getUserId();
                }
                supervisorForm.populateSupervisorSelectItem(orgId.substring(1), targetUserId);
            }
        });
        
        userLevelSelectItem = buildSelectItem("User Level", true);

        accessLevelSelectItem = buildSelectItem("Access Level", true);

        statusSelectItem = buildSelectItem("Status", true);
        statusSelectItem.setValidateOnExit(false);
        statusSelectItem.setColSpan(4);

        if (mode == Mode.UPDATE) {
            statusSelectItem.addChangedHandler(new StatusConfirmationChangedHandler());
        } else if (mode == Mode.ADD) {
            statusSelectItem.addChangedHandler(new ChangedHandler() {
                @Override
                public void onChanged(ChangedEvent event) {
                    String status = event.getValue().toString();
                    if (status.equals(INACTIVE_STATUS_VALUE)) {
                        supervisorForm.getSupervisorSelectItem().clearValue();
                        supervisorForm.getSupervisorSelectItem().disable();
                    } else {
                        supervisorForm.getSupervisorSelectItem().enable();
                    }
                }
            });
        }

        // enable account
        enableSelectItem = buildSelectItem("Enable Account", false);
        enableSelectItem.setValueMap(statusMap);
        enableSelectItem.setValidateOnExit(false);
        enableSelectItem.setColSpan(4);

        // unlock account
        unlockAccountSelectItem = buildSelectItem("Unlock Account", false);
        unlockAccountSelectItem.setValueMap(statusMap);
        unlockAccountSelectItem.setValidateOnExit(false);
        unlockAccountSelectItem.setColSpan(4);

        // reset login date
        resetLoginDateSelectItem = buildSelectItem("Reset Last Login Date", false);
        resetLoginDateSelectItem.setValueMap(statusMap);
        resetLoginDateSelectItem.setValidateOnExit(false);
        resetLoginDateSelectItem.setColSpan(4);

        // reset password date
        resetPasswordDateSelectItem = buildSelectItem("Reset Last Password Date", false);
        resetPasswordDateSelectItem.setValueMap(statusMap);
        resetPasswordDateSelectItem.setValidateOnExit(false);
        resetPasswordDateSelectItem.setColSpan(4);

        if(mode == Mode.ADD) {
            enableSelectItem.setVisible(false);
            unlockAccountSelectItem.setVisible(false);
            resetLoginDateSelectItem.setVisible(false);
            resetPasswordDateSelectItem.setVisible(false);
            emailTextItem.setValue(email);
        }

        tempForm.setFields(emailTextItem, passwordItem, confirmPasswordItem, organizationSelectItem, 
                userLevelSelectItem, accessLevelSelectItem, statusSelectItem, enableSelectItem, 
                unlockAccountSelectItem, resetLoginDateSelectItem, resetPasswordDateSelectItem);

        return tempForm;
    }

    /**
     * Build the Service Subscription form
     *
     * @return
     */
    private DynamicForm buildServiceSubscriptionForm() {
        DynamicForm tempForm = new DynamicForm();

        tempForm.setIsGroup(true);
        tempForm.setGroupTitle(Constants.SERVICE_SUBSCRIPTION);
        if (mode == Mode.ADD) {
            tempForm.setWidth(FIELD_WIDTH);
        } else if (mode == Mode.UPDATE) {
            tempForm.setWidth(448);
        }
        tempForm.setHeight(50);
        tempForm.setWrapItemTitles(false);
        tempForm.setCellPadding(5);
        tempForm.setNumCols(2);
        tempForm.setRequiredTitlePrefix(FormUtils.REQUIRED_TITLE_PREFIX);

        // Intra-HH community changed to DIRECT Secured Messaging in 3.3

        intraCommunityCheckBox = buildCheckBox(Constants.DIRECT_SECURED_MESSAGING, false);
        intraCommunityCheckBox.setColSpan(5);
        tempForm.setFields(intraCommunityCheckBox);

        return tempForm;
    }

    private TextItem buildTextItem(String title, boolean required) {
        TextItem item = new TextItem();

        item.setTitle(title);
        item.setRequired(required);
        item.setWidth(FIELD_WIDTH);

        return item;
    }

    /**
     * we need a special selectItem for organizations, so we can filter on 
     * whether an org is active or not.
     * @param title
     * @param required
     * @return 
     */
    private SelectItem buildOrgSelectItem(String title, boolean required) {
        organizationSelectItem = new SelectItem(title, title);
        organizationSelectItem.setValueField("value");
        organizationSelectItem.setDisplayField("display");
        organizationSelectItem.setAllowEmptyValue(true);
        return organizationSelectItem;
    }
    
    private SelectItem buildSelectItem(String title, boolean required) {
        SelectItem tempSelectItem = new SelectItem();

        tempSelectItem.setTitle(title);
        tempSelectItem.setRequired(required);
        tempSelectItem.setWidth(FIELD_WIDTH);

       // tempSelectItem.setAnimatePickList(true);
        //tempSelectItem.setAnimationTime(600);
        tempSelectItem.setColSpan(4);

        return tempSelectItem;
    }

    private DateItem buildDateItem(String title, boolean required) {
        DateItem dateItem = new DateItem();

        dateItem.setTitle(title);
        dateItem.setRequired(required);

        dateItem.setTextAlign(Alignment.LEFT);
        dateItem.setUseMask(true);
        dateItem.setEnforceDate(true);
        dateItem.setDisplayFormat(DateDisplayFormat.TOUSSHORTDATE);
        dateItem.setWidth(90);

        dateItem.setStartDate(dateFormatter.parse("01/01/1900"));

        return dateItem;
    }

    private CheckboxItem buildCheckBox(String title, boolean requiredFlag) {
        CheckboxItem tempCheckBox = new CheckboxItem();
        tempCheckBox.setRequired(requiredFlag);
        tempCheckBox.setTitle(title);
        tempCheckBox.setWidth(400);
        return tempCheckBox;
    }

    /**
     * Read the form data
     *
     * @return the user
     */
    public UserDTO getUserDTO() {

        if (userDTO == null) {
            userDTO = new UserDTO();
        }

        // read from form fields

        String firstName = StringUtils.trim(firstNameTextItem.getValueAsString());
        String middleName = StringUtils.trim(middleNameTextItem.getValueAsString());
        String lastName = StringUtils.trim(lastNameTextItem.getValueAsString());
        String gender = genderSelectItem.getValueAsString();
        Date dateOfBirth = dateOfBirthDateItem.getValueAsDate();

        String organizationStr = organizationSelectItem.getValueAsString();

        if (StringUtils.isNotBlank(organizationStr)) {
            organizationStr = organizationStr.split(Constants.DELIMITER_SELECT_ITEM)[1];
        }
        
        String organization = organizationStr;
        String orgId = organizationStr;
        
        String streetAddress1 = StringUtils.trim(streetAddress1TextItem.getValueAsString());
        String streetAddress2 = StringUtils.trim(streetAddress2TextItem.getValueAsString());

        String city = StringUtils.trim(cityTextItem.getValueAsString());

        String state = stateSelectItem.getValueAsString();

        String zipCode = StringUtils.trim(zipCodeTextItem.getValueAsString());
        String workPhone = StringUtils.trim(workPhoneTextItem.getValueAsString());
        String workPhoneExt = StringUtils.trim(workPhoneExtTextItem.getValueAsString());

        // convert newEmail address from form to lowercase
        String newEmail = StringUtils.trim(emailTextItem.getValueAsString().toLowerCase());

        String password = passwordItem.getValue().toString();
        String confirmPassword = confirmPasswordItem.getValueAsString();
        String securityRole = userLevelSelectItem.getValueAsString();
        String accessLevel = accessLevelSelectItem.getValueAsString();
        String status = statusSelectItem.getValueAsString();

        String credentials = userCredentialSelectItem.getValueAsString();
        String prefix = userPrefixSelectItem.getValueAsString();
        String npi = npiTextItem.getValueAsString();
        String mobileNo=mobileItem.getValueAsString();
        String countryCode=countryCodeTextItem.getValueAsString();
       
        userDTO.setFirstName(firstName);
        userDTO.setMiddleName(middleName);
        userDTO.setLastName(lastName);
        userDTO.setGender(gender);
        userDTO.setDateOfBirth(dateOfBirth);
        userDTO.setStatus(status);
        userDTO.setOrganization(organization);
        userDTO.setOrgid(orgId);
        userDTO.setStreetAddress1(streetAddress1);
        userDTO.setStreetAddress2(streetAddress2);
        userDTO.setCity(city);
        userDTO.setState(state);
        userDTO.setZipCode(zipCode);
        userDTO.setWorkPhone(workPhone);
        userDTO.setWorkPhoneExt(workPhoneExt);
        userDTO.setEmail(newEmail);
        userDTO.setSecurityRole(securityRole);
        userDTO.setPassword(password);
        userDTO.setConfirmPassword(confirmPassword);
        userDTO.setSecurityRoleId(securityRole);
        userDTO.setAccessLevelId(accessLevel);

        userDTO.setCredentials(credentials);
        userDTO.setPrefix(prefix);
        userDTO.setNpi(npi);
        userDTO.setMobileNo(mobileNo);
        userDTO.setCountryCode(countryCode);
            
        // enable account
        //   setEnableRecord(enableSelectItem.getValueAsString());
        if (StringUtils.equalsIgnoreCase(enableSelectItem.getValueAsString(), STATUS_VALUE_YES)) {
            userDTO.setEnabled('Y');

        }

        // unlock account
        if (StringUtils.equalsIgnoreCase(unlockAccountSelectItem.getValueAsString(), STATUS_VALUE_YES)) {
            userDTO.setAccountLockedOut('N');
        }

        // reset password
        if (StringUtils.equalsIgnoreCase(resetPasswordDateSelectItem.getValueAsString(), STATUS_VALUE_YES)) {
            userDTO.setMustChangePassword('Y');
        }

        // reset login date
        if (StringUtils.equalsIgnoreCase(resetLoginDateSelectItem.getValueAsString(), STATUS_VALUE_YES)) {
            userDTO.setLastSuccessfulLoginDate(new Date());
        }

        if (mode == Mode.UPDATE) {
            userDTO.setUserDeactivated(userDeactivated);
        } else {
            if (status.equals(INACTIVE_STATUS_VALUE)) {
                userDTO.setUserDeactivated(true);
            }
        }

        // setting the CAN_MANAGE_POWER_USER column to 'N'
        //userDTO.setCanManagePowerUser('N');

        userDTO.setSupervisorId(supervisorForm.getSelectedSupervisor());

        return userDTO;
    }

    private PasswordItem buildPasswordItem(String title, boolean requiredFlag) {
        PasswordItem thePasswordItem = new PasswordItem();
        thePasswordItem.setTitle(title);
        thePasswordItem.setWidth(FIELD_WIDTH);
        thePasswordItem.setRequired(requiredFlag);
        return thePasswordItem;
    }

    public TextItem getConfirmPasswordItem() {
        return confirmPasswordItem;
    }

    public TextItem getPasswordItem() {
        return passwordItem;
    }

    public void populateForm(UserDTO theUserDTO) {

        userDTO = theUserDTO;
        populateDemographicForm();
        populateLoginInfoForm();

        /*
         * enabled account
         */
        if (userDTO.getEnabled() != null) {

            if (userDTO.getEnabled().equals('Y')) {
                enableSelectItem.setValue(STATUS_VALUE_NO);
            } else {
                enableSelectItem.setValue(STATUS_VALUE_YES);
            }

        } else {
            enableSelectItem.setValue(STATUS_VALUE_NO);
        }

        // uco stuff, varies by community
        handleAccessLevel(theUserDTO);
        statusSelectItem.setValue(userDTO.getStatus());
        userLevelSelectItem.setValue(userDTO.getSecurityRoleId());
        organizationSelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + userDTO.getOrgid());
        supervisorForm.populateSupervisorSelectItem(userDTO.getOrgid(), userDTO.getSupervisorId(), userDTO.getUserId());
        if (Constants.USER_STATUS_ACTIVE.equals(userDTO.getStatus())) {
            supervisorForm.getSupervisorSelectItem().enable();
        } else {
            supervisorForm.getSupervisorSelectItem().disable();
        }

    }

    public void populateLoginInfoForm() {
        // unlock account
        if (userDTO.getAccountLockedOut().equals('Y')) {
            unlockAccountSelectItem.setValue(STATUS_VALUE_YES);
        } else {
            unlockAccountSelectItem.setValue(STATUS_VALUE_NO);
        }
        
        if (userDTO.getLastPasswordChangeDate() != null) {
            adminService.getDaysSinceLastChange(userDTO.getLastPasswordChangeDate(), new Date(), new LastPasswordChangeCallback());
        } else {
            resetPasswordDateSelectItem.setValue(STATUS_VALUE_NO);
        }
        
        // reset last login
        if (userDTO.getLastSuccessfulLoginDate() != null) {
            adminService.getDaysSinceLastChange(userDTO.getLastSuccessfulLoginDate(), new Date(), new LastChangeCallback());
        } else {
            resetLoginDateSelectItem.setValue(STATUS_VALUE_NO);
        }
    }

    public void populateDemographicForm() {
        userPrefixSelectItem.setValue(userDTO.getPrefix());
        firstNameTextItem.setValue(userDTO.getFirstName());
        middleNameTextItem.setValue(userDTO.getMiddleName());
        lastNameTextItem.setValue(userDTO.getLastName());
        userCredentialSelectItem.setValue(userDTO.getCredentials());
        npiTextItem.setValue(userDTO.getNpi());
        genderSelectItem.setValue(userDTO.getGender());
        
        dateOfBirthDateItem.setValue(userDTO.getDateOfBirth());
        streetAddress1TextItem.setValue(userDTO.getStreetAddress1());
        streetAddress2TextItem.setValue(userDTO.getStreetAddress2());
        cityTextItem.setValue(userDTO.getCity());
        stateSelectItem.setValue(userDTO.getState());
        zipCodeTextItem.setValue(userDTO.getZipCode());
        workPhoneTextItem.setValue(userDTO.getWorkPhone());
        workPhoneExtTextItem.setValue(userDTO.getWorkPhoneExt());
        emailTextItem.setValue(userDTO.getEmail());
        populateMobileNo(userDTO);
        // set pw and confirm to blank for existing user
        passwordItem.setValue("");
        confirmPasswordItem.setValue("");
    }
    
    public void enableOrDisablePasswordFields(boolean isInActiveInOtherCommunities)
    {
        passwordItem.setDisabled(!isInActiveInOtherCommunities);
        confirmPasswordItem.setDisabled(!isInActiveInOtherCommunities);
        passwordItem.setRequired(isInActiveInOtherCommunities);
        confirmPasswordItem.setRequired(isInActiveInOtherCommunities);
        if(!isInActiveInOtherCommunities) {
            passwordItem.setValue("");
            confirmPasswordItem.setValue("");
            passwordItem.setValidators(null);
            confirmPasswordItem.setValidators(null);
        }
    }

    private void populateMobileNo(UserDTO userDto) {
        String number = userDto.getMobileNo();
        String countryCode=null;
        if (number != null && number.length()!=10) {
            String mobileNo = number.substring(number.length() - 10, number.length());
            countryCode = number.replace(mobileNo, "");
            mobileItem.setValue(mobileNo);
            countryCodeTextItem.setValue(countryCode);
        } else {
            String code=null;
            mobileItem.setValue(number);
            countryCodeTextItem.setValue(code);
        }
    }
    private void loadUserFormReferenceData() {
        adminService.getUserFormReferenceData(new GetUserFormReferenceDataCallback());
    }

    /**
     * Helper method to validate the form
     */
    public boolean validate() {
        boolean validateContactForm = contactForm.validate();
        boolean validateSecurityForm = securityForm.validate();
        boolean flag = validateContactForm && validateSecurityForm;
        return flag;
    }

    public void clearErrors(boolean flag) {
        contactForm.clearErrors(flag);
        securityForm.clearErrors(flag);
    }

    public void clearValues() {
        contactForm.clearValues();
        securityForm.clearValues();
        supervisorForm.clearValues();
    }
//
//    private RegExpValidator getEmailAddressRegExpValidator() {
//        RegExpValidator validator = new RegExpValidator();
//        validator.setExpression(Constants.EMAIL_VALIDATION_REGEX);
//        validator.setErrorMessage(Constants.USER_VALID_EMAIL_ADDRESS);
//        return validator;
//    }

    private void loadUserCredentials() {
        String[] codes = ApplicationContextUtils.getUserCredentialCodes();
        userCredentialSelectItem.setValueMap(codes);
    }

    private void loadUserPrefixes() {
        String[] codes = ApplicationContextUtils.getUserPrefixCodes();
        userPrefixSelectItem.setValueMap(codes);
    }

    public List<String> getSelectedApps() {
        List<String> selectedApps = new ArrayList<String>();

        boolean selected = intraCommunityCheckBox.getValueAsBoolean();

        if (selected) { // if direct messaging is selected add the messaging app
            selectedApps.add(AppKeyConstants.MESSAGING_APP_KEY);
        }

        return selectedApps;
    }

    public void populateUserApps(List<String> userApps) {

        if (userApps != null && !userApps.isEmpty()) {
            //SC.warn("User Apps List:  " + userApps.toString());
            // FIXME is this correct?  shouldn't this be disabled if the user has a direct email addy?
            boolean messageAppChecked = userApps.contains(AppKeyConstants.MESSAGING_APP_KEY);

            intraCommunityCheckBox.setValue(messageAppChecked);

            // disable the messaging app if it has already been selected
            intraCommunityCheckBox.setDisabled(messageAppChecked);
        }
    }

    /**
     * @return the isUserEnable
     */
    public String getIsUserEnable() {
        return isUserEnable;
    }

    /**
     * @param isUserEnable the isUserEnable to set
     */
    public void setIsUserEnable(String isUserEnable) {
        this.isUserEnable = isUserEnable;
    }

    /**
     * @return the enableRecord
     */
    public String getEnableRecord() {
        return enableRecord;
    }

    /**
     * @param enableRecord the enableRecord to set
     */
    public void setEnableRecord(String enableRecord) {
        this.enableRecord = enableRecord;
    }

    private void addPowerUser(SelectItem theAccessLevelSelectItem) {
        String powerUserAccessLevelIdStr = Long.toString(AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID);
        accessLevelsMap.put(powerUserAccessLevelIdStr, powerUserAccessLevelDescription);
        theAccessLevelSelectItem.setValueMap(accessLevelsMap);
    }

    private void removePowerUser(SelectItem theAccessLevelSelectItem) {
        String powerUserAccessLevelIdStr = Long.toString(AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID);
        accessLevelsMap.remove(powerUserAccessLevelIdStr);
        theAccessLevelSelectItem.setValueMap(accessLevelsMap);
    }
    
    /**
     * Handling power user access level display when viewing user does not have
     * right to modify the power user
     */
    protected void handleAccessLevel(UserDTO theUserDTO) throws NumberFormatException {
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
 
        Character canManagePowerUserVal = loginResult.getCanManagePowerUser();

        long theUserAccessLevelId = Long.parseLong(theUserDTO.getAccessLevelId());
        boolean viewingPowerUser = theUserAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID;

        boolean canManagePowerUser = AccessLevelConstants.CHAR_YES.equals(canManagePowerUserVal);
        boolean canNotManagePowerUser = !canManagePowerUser;

        // for the accessLevelSelectItem
        // 1. run thru the logic of adding "Power User" to the list
        // 2. also enable/disable based on permissions
        if (canManagePowerUser) {
            addPowerUser(accessLevelSelectItem);
            accessLevelSelectItem.enable();
        }

        if (canNotManagePowerUser && viewingPowerUser) {
            addPowerUser(accessLevelSelectItem);
            accessLevelSelectItem.disable();
        }

        if (canNotManagePowerUser && !viewingPowerUser) {
            removePowerUser(accessLevelSelectItem);
            accessLevelSelectItem.enable();
        }

        // now sett the user's access level
        accessLevelSelectItem.setValue(theUserAccessLevelId);

        // handle special case of Local Admin: they can't change user access level
        // and the leve defaults to HH Team User
        long loggedInUsersAccessLevelId = loginResult.getAccessLevelId();
        boolean localAdmin = loggedInUsersAccessLevelId == AccessLevelConstants.LOCAL_ADMIN_ACCESS_LEVEL_ID;
        //spira 4744--local admin should be restricted to update the security information of super user,
        String accessLevelId = theUserDTO.getAccessLevelId();
        emailTextItem.setDisabled(true);
        if (localAdmin && accessLevelId.equals(Long.toString(AccessLevelConstants.SUPER_USER_ACCESS_LEVEL_ID))) {
            enableDisableFormComponentsForLocalAdmin(true);
        } else if (localAdmin && !accessLevelId.equals(Long.toString(AccessLevelConstants.SUPER_USER_ACCESS_LEVEL_ID))) {
            String hhTeamAccessLevelIdStr = Long.toString(AccessLevelConstants.HH_TEAM_ACCESS_LEVEL_ID);
            accessLevelSelectItem.setDefaultValue(hhTeamAccessLevelIdStr);
            enableDisableFormComponentsForLocalAdmin(false);
        }
    }
    
    	/**
         * spira 5344
         * This method will enable disable of User Form components on the basis of access level for local admin .
         * Only Super user could not be editable when logged in user is local admin. Else all would be editable.
	 */
	private void enableDisableFormComponentsForLocalAdmin(boolean isVisible) {
		accessLevelSelectItem.disable();
		passwordItem.setDisabled(isVisible);
		confirmPasswordItem.setDisabled(isVisible);
		organizationSelectItem.setDisabled(isVisible);
		statusSelectItem.setDisabled(isVisible);
		userLevelSelectItem.setDisabled(isVisible);
		enableSelectItem.setDisabled(isVisible);
		unlockAccountSelectItem.setDisabled(isVisible);
		resetLoginDateSelectItem.setDisabled(isVisible);
		resetPasswordDateSelectItem.setDisabled(isVisible);
	}

    class PasswordChangeHandler implements ChangeHandler {

        public void onChange(ChangeEvent event) {
            confirmPasswordItem.clearValue();
        }
    }

    class StatusConfirmationChangedHandler implements ChangedHandler {

        public void onChanged(ChangedEvent event) {
            String status = event.getValue().toString();

            if (status.equals(INACTIVE_STATUS_VALUE)) {
                String firstNameLastName = getFirstNameLastName();
                String message = "You are about to deactivate the following user " + firstNameLastName + ". Do you want to continue?";
                SC.ask("Deactivate User", message, new StatusConfirmationCallback());
            } else {
                supervisorForm.getSupervisorSelectItem().enable();
            }
        }

        private String getFirstNameLastName() {
            String firstName = firstNameTextItem.getValueAsString();
            String lastName = lastNameTextItem.getValueAsString();
            String firstNameLastName = firstName + " " + lastName;
            return firstNameLastName;
        }
    }

    class StatusConfirmationCallback implements BooleanCallback {

        @Override
        public void execute(Boolean value) {

            userDeactivated = value;

            if (value.booleanValue()) {
                statusSelectItem.setValue(INACTIVE_STATUS_VALUE);
                userDeactivated = true;
                supervisorForm.getSupervisorSelectItem().clearValue();
                supervisorForm.getSupervisorSelectItem().disable();
            } else {
                statusSelectItem.setValue(ACTIVE_STATUS_VALUE);
            }
        }
    }

    public void disableMessageAppCheckbox() {
        intraCommunityCheckBox.disable();
    }

    /**
     * create a datasource this will allow for item filtering
     */
    private void createOrgDataSource() {
        orgDataSource = new DataSource();
        orgDataSource.setCacheAllData(true);
        orgDataSource.setClientOnly(true);
        orgDataSource.setFields(
            new DataSourceField("value", FieldType.TEXT), 
            new DataSourceField("display", FieldType.TEXT)
        );
    }
    
    /**
     * we have to show a users org if it's inactive, this will grey out
     * all inactive orgs while still allowing a preselected org to be visible
     * populate the dataSource with the filtered results
     * @param baseData - a list of all organizationDTOs
     */
    private void buildOrgNamesMap(List<OrganizationDTO> baseData) {
        System.out.println("building orgNamesMap");
        createOrgDataSource();
        
        List<ListGridRecord> reclist = new ArrayList<ListGridRecord>(baseData.size());
        for (OrganizationDTO each : baseData) {
            String status = each.getStatus();
            boolean active = StringUtils.equalsIgnoreCase(status, "ACTIVE");
            Long orgId = each.getOrganizationId();
            String key = Constants.DELIMITER_SELECT_ITEM + orgId.toString();
            ListGridRecord record = new ListGridRecord();
            
            record.setAttribute("value", key);
            record.setAttribute("display", each.getOrganizationName());
            record.setEnabled(active);
            reclist.add(record);
        }
        orgDataSource.setCacheData(reclist.toArray(new ListGridRecord[reclist.size()]));
        organizationSelectItem.setOptionDataSource(orgDataSource);
        organizationSelectItem.setShowOptionsFromDataSource(true);
    }
 
    private void populateAccessLevels(LinkedHashMap<String, String> data) {
        long accessLevelId = ApplicationContextUtils.getLoginResult().getAccessLevelId();
        Character canManagePowerUser = ApplicationContextUtils.getLoginResult().getCanManagePowerUser();
        System.out.println("populating access levels");
        boolean localAdmin = accessLevelId == AccessLevelConstants.LOCAL_ADMIN_ACCESS_LEVEL_ID;

        /*
         * remove power user from the list when canManagePowerUser is not 'Y'
         */
        if (!(AccessLevelConstants.CHAR_YES.equals(canManagePowerUser))) {
            data.remove(String.valueOf(AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID));
        }

        accessLevelSelectItem.setValueMap(data);
        
        if (localAdmin) {
            accessLevelSelectItem.setDefaultValue(Long.toString(AccessLevelConstants.HH_TEAM_ACCESS_LEVEL_ID));
            accessLevelSelectItem.disable();
            }
            
        // assign to our field in this class
        accessLevelsMap = data;
    }

    private void populateRoles(LinkedHashMap<String, String> data) {

        roleList = data;
        userLevelSelectItem.setValueMap(data);
    }

    class GetUserFormReferenceDataCallback extends RetryActionCallback<UserFormReferenceData> {

        @Override
        public void attempt() {
            adminService.getUserFormReferenceData(this);
        }

        @Override
        public void onCapture(UserFormReferenceData userFormReferenceData) {
            // load orgDTOs
             
            List<OrganizationDTO> orgDTOs = userFormReferenceData.getOrganizationDTOs();
            buildOrgNamesMap(orgDTOs);

            // load access levels
            LinkedHashMap<String, String> theAccessLevels = userFormReferenceData.getAccessLevels();
           
            populateAccessLevels(theAccessLevels);

            // load roles
            LinkedHashMap<String, String> theRoles = userFormReferenceData.getRoles();
            populateRoles(theRoles);


        }
    }

    class LastChangeCallback implements AsyncCallback<Integer> {

        public void onFailure(Throwable thrwbl) {
           
        }

        public void onSuccess(Integer t) {
            

            if (t >= 90) {
                resetLoginDateSelectItem.setValue(STATUS_VALUE_YES);
            } else {
                resetLoginDateSelectItem.setValue(STATUS_VALUE_NO);
            }

        }
    }

    class LastPasswordChangeCallback implements AsyncCallback<Integer> {

        public void onFailure(Throwable thrwbl) {
            
        }

        public void onSuccess(Integer t) {
             if (t >= 90) {
                resetPasswordDateSelectItem.setValue(STATUS_VALUE_YES);
            } else {
                resetPasswordDateSelectItem.setValue(STATUS_VALUE_NO);
            }

        }
    }

    private boolean intraHHCommunityFieldVisible() {

        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();

        return props.isIntraHHComunityFieldVisible();
    }

}
