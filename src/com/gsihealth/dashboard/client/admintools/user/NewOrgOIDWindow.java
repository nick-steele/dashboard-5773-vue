/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.admintools.user;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.WindowManager;
import com.gsihealth.dashboard.client.admintools.AdminToolsPanel;
import com.gsihealth.dashboard.client.common.Cancelable;
import com.gsihealth.dashboard.client.common.DashboardTitles;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.service.AdminService;
import com.gsihealth.dashboard.client.service.AdminServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.client.util.ValidationUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.BlurEvent;
import com.smartgwt.client.widgets.form.fields.events.BlurHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;

/**
 *
 * @author Beth.Boose
 */
public class NewOrgOIDWindow extends Window implements Cancelable {

	private static final String WINDOW_TITLE_TEXT = "Create a new organization";
	private static final String DIRECTIONS_TEXT = "Enter an OID";
	private static final String VALIDATE_BUTTON_TEXT = "Validate Org";
	private static final String FIND_ORG_ERROR_MSG = " Please select a different OID";

	private String oid;
	private DynamicForm oidForm;
	private Button validateOrgButton;
	private TextItem oidTextItem;
	private AdminToolsPanel adminToolsPanel;
	private final AdminServiceAsync adminService = GWT.create(AdminService.class);
	private OrganizationDTO orgDTO;
	private ProgressBarWindow progressBarWindow;
	private Long communityId;
	private ClickHandler validateOrgClickHandler;

	public NewOrgOIDWindow(String oid) {
		this.oid = oid;
		this.setLoggedOnUserCommunityId();
		this.validateOrgClickHandler = new ValidateOIDClickHandler();
		this.buildGui();

	}

	public NewOrgOIDWindow(AdminToolsPanel parent) {
		adminToolsPanel = parent;
		this.setLoggedOnUserCommunityId();
		this.validateOrgClickHandler = new ValidateOIDClickHandler();
		this.buildGui();
	}

	public NewOrgOIDWindow() {
		this.setLoggedOnUserCommunityId();
		this.buildGui();
	}

	private void setLoggedOnUserCommunityId() {
		LoginResult loginResult = ApplicationContextUtils.getLoginResult();
		communityId = loginResult.getCommunityId();
	}

	private void formatWindow() {
		// form
		KeyPressHandler validateKeyPressHandler = new ValidateKeyPressHandler();
		oidForm = new DynamicForm();
		oidForm.setAutoHeight();
		oidForm.setAutoWidth();
		oidForm.setPadding(5);
		oidForm.setCellPadding(5);
		oidForm.setWrapItemTitles(false);
		oidForm.setNumCols(2);
		StaticTextItem directions = new StaticTextItem("directions", NewOrgOIDWindow.DIRECTIONS_TEXT);
		directions.setWrap(false);
		directions.setWrapTitle(false);
		directions.setColSpan(2);
		// oid text stuff
		oidTextItem = new TextItem();
		oidTextItem.setAlign(Alignment.LEFT);
		oidTextItem.setTitle("OID");
		oidTextItem.setRequired(true);

		RegExpValidator numberPeriodsOnlyRegExpValidator = ValidationUtils.getNumberPeriodsOnlyRegExpValidator();
		ValidationUtils.setValidators(oidTextItem, numberPeriodsOnlyRegExpValidator, true);

		oidTextItem.addKeyPressHandler(validateKeyPressHandler);
		// save button
		Layout buttonBar = buildButtonBar();
		buttonBar.setAlign(Alignment.CENTER);
		validateOrgButton.addClickHandler(validateOrgClickHandler);

		oidForm.setFields(directions, oidTextItem);
		this.addItem(oidForm);
		this.addItem(buttonBar);
	}

	private Layout buildButtonBar() {
		HLayout buttonLayout = new HLayout();
		validateOrgButton = new Button(NewOrgOIDWindow.VALIDATE_BUTTON_TEXT);
		buttonLayout.addMember(validateOrgButton);
		return buttonLayout;
	}

	private void buildGui() {
		setPadding(10);
		setWidth(400);
		setHeight(150);
		setTitle(NewOrgOIDWindow.WINDOW_TITLE_TEXT);
		setShowMinimizeButton(false);
		setIsModal(true);
		setShowModalMask(true);
		centerInPage();
		formatWindow();
		this.addCloseClickHandler(new OIDCloseClickHandler());

	}

	public void showSetFocus() {
		this.show();
		this.oidTextItem.focusInItem();
	}

	@Override
	public void clearErrors(boolean flag) {
		this.oidForm.clearErrors(flag);
	}

	@Override
	public void clearValues() {
		this.oidTextItem.clearValue();
	}

	public String getOid() {
		return oid;
	}

	class ValidateKeyPressHandler implements KeyPressHandler {

		@Override
		public void onKeyPress(KeyPressEvent event) {
			if (event.getKeyName().equals(Constants.ENTER_KEY)) {
				validateOrgClickHandler.onClick(null);
			}
		}
	}

	class ValidateOIDClickHandler implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			if (oidForm.validate()) {
				oid = StringUtils.trim(oidTextItem.getValueAsString().toLowerCase());
				// does user exist?
				progressBarWindow = new ProgressBarWindow();
				progressBarWindow.show();
				FindCallback orgCallBack = new FindCallback(progressBarWindow);
				adminService.getOrganizationByOid(oid, communityId, orgCallBack);
			}
		}
	}

	class OIDCloseClickHandler implements CloseClickHandler {

		@Override
		public void onCloseClick(CloseClickEvent event) {
			destroy();
			//WindowManager.updateWindow(DashboardTitles.ADMIN, new AdminToolsPanel());
		}
	}

	class FindCallback<OrganizationDTO> extends PortalAsyncCallback<OrganizationDTO> {

		com.gsihealth.dashboard.entity.dto.OrganizationDTO dto;

		public FindCallback(ProgressBarWindow progressBarWindow) {
			this.progressBarWindow = progressBarWindow;
		}

		@Override
		public void onFailure(Throwable exc) {
			if (progressBarWindow != null) {
				progressBarWindow.hide();
			}

			SC.warn(exc.getMessage() + NewOrgOIDWindow.FIND_ORG_ERROR_MSG);
			clearErrors(true);
			clearValues();
		}

		@Override
		public void onSuccess(OrganizationDTO foundDTO) {
			dto = (com.gsihealth.dashboard.entity.dto.OrganizationDTO) foundDTO;
			if (foundDTO != null) {
				String txt = "Did you mean " + dto.getOID() + " : " + dto.getOrganizationName() + "?";
				SC.ask("Existing organization message", txt, new UseExistingOrgCallback(dto, NewOrgOIDWindow.this));
			} else { // user doesn't exist, just show the add page
				adminToolsPanel.createOrgPanel(oid, null, false);
				hide();
			}
			if (progressBarWindow != null) {
				progressBarWindow.hide();
			}
		}
	}

	class UseExistingOrgCallback implements BooleanCallback {
		OrganizationDTO orgDTO;
		NewOrgOIDWindow newUseroidWindow;

		UseExistingOrgCallback(OrganizationDTO orgDTO, NewOrgOIDWindow newUseroidWindow) {
			this.orgDTO = orgDTO;
			this.newUseroidWindow = newUseroidWindow;
		}

		@Override
		public void execute(Boolean value) {
			// if the user said yes, use found org
			if (value) {
				adminToolsPanel.createOrgPanel(oid, orgDTO, value);
				newUseroidWindow.hide();
			} else {
				clearErrors(true);
				clearValues();
			}
		}
	}

	class OIDBlurHandler implements BlurHandler {

		@Override
		public void onBlur(BlurEvent event) {
			oidTextItem.validate();
		}
	}

}
