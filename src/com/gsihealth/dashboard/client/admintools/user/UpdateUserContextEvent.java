/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.admintools.user;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.GwtEvent.Type;

/**
 *
 * @author nkumar
 */
public class UpdateUserContextEvent extends GwtEvent<UpdateUserContextEventHandler> {

    public static Type<UpdateUserContextEventHandler> TYPE = new Type<UpdateUserContextEventHandler>();
   
    
    @Override
    public Type<UpdateUserContextEventHandler> getAssociatedType() {
        return TYPE;
    }

    @Override
    protected void dispatch(UpdateUserContextEventHandler handler) {
        handler.onUpdateUserContext(this);
    }
     
    
}
