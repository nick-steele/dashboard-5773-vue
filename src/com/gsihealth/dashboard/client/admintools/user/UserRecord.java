package com.gsihealth.dashboard.client.admintools.user;

import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 *
 * @author Chad Darby
 */
public class UserRecord extends ListGridRecord {

    private int index;

    public UserRecord(int index, String firstName, String middleName, String lastName, String address, String org, String city, String state, String zipCode, String accessLevel, String userRole, String status, String dateOfBirth) {

        setIndex(index);
        setFirstName(firstName);
        setMiddleName(middleName);
        setLastName(lastName);
        setAddress(address);

        setOrg(org);
        setCity(city);
        setState(state);
        setZipCode(zipCode);
        setAccessLevel(accessLevel);
        setUserLevel(userRole);
        setStatus(status);
        setDateOfBirth(dateOfBirth);
    }

    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return getAttribute("First Name");
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        setAttribute("First Name", firstName);
    }

    /**
     * @return the middleName
     */
    public String getMiddleName() {
        return getAttribute("Middle Name");
    }

    /**
     * @param lastName the MiddleName to set
     */
    public void setMiddleName(String middleName) {
        setAttribute("Middle Name", middleName);
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return getAttribute("Last Name");
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        setAttribute("Last Name", lastName);
    }

    /**
     * @return the address1
     */
    public String getAddress() {
        return getAttribute("Address");
    }

    /**
     * @param lastName the address1 to set
     */
    public void setAddress(String address) {
        setAttribute("Address", address);
    }

    /**
     * @return the org
     */
    public String getOrg() {
        return getAttribute("Organization");
    }

    /**
     * @param org the org to set
     */
    public void setOrg(String org) {
        setAttribute("Organization", org);
    }

    /**
     * @return the dept
     */
    public String getDept() {
        return getAttribute("Department");
    }

    /**
     * @param dept the dept to set
     */
    public void setDept(String dept) {
        setAttribute("Department", dept);
    }

    /**
     * @return the city
     */
    public String getCity() {
        return getAttribute("City");
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        setAttribute("City", city);
    }

    /**
     * @return the state
     */
    public String getState() {
        return getAttribute("State");
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        setAttribute("State", state);

    }

    /**
     * @return the zipcode
     */
    public String getZipCode() {
        return getAttribute("ZipCode");
    }

    /**
     * @param state the ZipCode to set
     */
    public void setZipCode(String zipCode) {
        setAttribute("ZipCode", zipCode);

    }


    /**
     * @return the accessLevel
     */
    public String getAccessLevel() {
        return getAttribute("Access Level");
    }

    /**
     * @param accessLevel the accessLevel to set
     */
    public void setAccessLevel(String accessLevel) {
        setAttribute("Access Level", accessLevel);

    }

    /**
     * @return the UserRole
     */
    public String getUserLevel() {
        return getAttribute("User Level");
    }

    /**
     * @param accessLevel the UserRole to set
     */
    public void setUserLevel(String userLevel) {
        setAttribute("User Level", userLevel);

    }

    /**
     * @return the UserRole
     */
    public String getStatus() {
        return getAttribute("Status");
    }

    /**
     * @param accessLevel the UserRole to set
     */
    public void setStatus(String status) {
        setAttribute("Status", status);

    }

    public String getDateOfBirth() {
        return getAttribute("DOB");
    }

    public void setDateOfBirth(String dateOfBirth) {
        setAttribute("DOB", dateOfBirth);

    }
}
