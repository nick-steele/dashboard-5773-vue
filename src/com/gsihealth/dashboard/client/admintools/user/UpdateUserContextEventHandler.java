/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.admintools.user;

import com.google.gwt.event.shared.EventHandler;

/**
 *
 * @author nkumar
 */
public interface UpdateUserContextEventHandler extends EventHandler {

    /**
     * Called when an update user context event is fired
     * 
     * @param updateUserContextEvent 
     */
    public void onUpdateUserContext(UpdateUserContextEvent updateUserContextEvent);
    
}
