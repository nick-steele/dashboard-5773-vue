package com.gsihealth.dashboard.client.admintools.user;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.service.AdminService;
import com.gsihealth.dashboard.client.service.AdminServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by hnguyen on 6/3/2016.
 */
public class SupervisorForm extends DynamicForm {

    private final AdminServiceAsync adminService = GWT.create(AdminService.class);
    private SelectItem supervisorSelectItem;
    private List<UserDTO> supervisors;
    private Long communityId;
    private int fieldWidth;
    private int groupWidth;

    public SupervisorForm(int fieldWidth) {
        this(0, fieldWidth);
    }

    public SupervisorForm(int groupWidth, int fieldWidth) {
        communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
        this.groupWidth = groupWidth;
        this.fieldWidth = fieldWidth;
        buildGui();
    }

    private  void buildGui() {
        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
        String supervisorFormTitle = props.getSupervisorFormTitle();
        setIsGroup(true);
        setGroupTitle(supervisorFormTitle);
        if (groupWidth > 0) {
            setWidth(groupWidth);
        }
        setHeight(50);
        setCellPadding(5);
        setNumCols(2);
        setWrapItemTitles(false);

        supervisorSelectItem = new SelectItem();
        supervisorSelectItem.setTitle("Select User");
        supervisorSelectItem.setWidth(fieldWidth);
        supervisorSelectItem.setColSpan(4);
        supervisorSelectItem.setAllowEmptyValue(true);

        setFields(supervisorSelectItem);
    }

    private void loadSupervisors(String orgId, Long supervisorId, Long targetUserId) {
        supervisorSelectItem.clearValue();
        supervisorSelectItem.setValueMap(buildSupervisorMap(supervisors, orgId, targetUserId));
        if (supervisorId != null && supervisorId != -1) {
            setSelectedSupervisor(supervisorId);
        }
    }

    public void populateSupervisorSelectItem(String orgId, Long targetUserId) {
        populateSupervisorSelectItem(orgId, null, targetUserId);
    }

    public void populateSupervisorSelectItem(final String orgId, final Long supervisorId, final Long targetUserId) {
        if (supervisors != null) {
            loadSupervisors(orgId, supervisorId, targetUserId);
        } else {
            adminService.getSupervisors(communityId, orgId, new AsyncCallback<List<UserDTO>>() {
                @Override
                public void onFailure(Throwable caught) {
                    SC.warn("Failed to get available supervisor");
                }

                @Override
                public void onSuccess(List<UserDTO> result) {
                    if (supervisors == null) {
                        supervisors = new ArrayList<UserDTO>();
                        supervisors.addAll(result);
                    }
                    loadSupervisors(orgId, supervisorId, targetUserId);
                }
            });
        }
    }

    private LinkedHashMap<String, String> buildSupervisorMap(List<UserDTO> users, String orgId, Long targetUserId) {
        LinkedHashMap<String, String> result = new LinkedHashMap<String, String>();
        if (users != null) {
            for (UserDTO user : users) {
                if (user.getUserId().equals(targetUserId)) {
                    continue;
                }
                if (orgId.equals(user.getOrgid())) {
                    StringBuilder label = new StringBuilder();
                    String userId = user.getUserId().toString();
                    label.append(user.getLastName());
                    label.append(", ");
                    label.append(user.getFirstName());

                    String organization = user.getOrganization();

                    if (StringUtils.isNotBlank(organization)) {
                        label.append(" - ");
                        label.append(organization);
                    }

                    result.put(Constants.DELIMITER_SELECT_ITEM + userId, label.toString());
                }
            }
        }
        return result;
    }

    public Long getSelectedSupervisor() {
        String value = supervisorSelectItem.getValueAsString();
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        return Long.parseLong(value.split(Constants.DELIMITER_SELECT_ITEM)[1]);
    }

    public void setSelectedSupervisor(Long supervisorId) {
        supervisorSelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + supervisorId);
    }

    @Override
    public void clearValues() {
        super.clearValues();
        supervisors = null;
    }

    public SelectItem getSupervisorSelectItem() {
        return supervisorSelectItem;
    }
}
