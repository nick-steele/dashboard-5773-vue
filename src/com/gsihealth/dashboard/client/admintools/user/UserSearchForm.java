/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.admintools.user;

import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.ValidationUtils;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.RowSpacerItem;
import com.smartgwt.client.widgets.form.fields.SectionItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;

/**
 *
 * @author User
 */
public class UserSearchForm extends DynamicForm {

    private TextItem lastNameTextItem;
    private TextItem firstNameTextItem;
    private ClickHandler searchClickHandler;
    private TextItem emailTextItem;


    public UserSearchForm(ClickHandler theSearchClickHandler) {
        searchClickHandler = theSearchClickHandler;
        buildGui();
    }

    private void buildGui() {

         RegExpValidator alphaHGAARegExValidator = ValidationUtils.getAlphaHGAARegExValidator(); 
                                                                  //getAlphaHGAARegExValidator   
        KeyPressHandler searchKeyPressHandler = new SearchKeyPressHandler();
        SectionItem adminSectionItem = buildSectionItem("Name");
       
        lastNameTextItem = new TextItem("lastName", "Last Name");
        lastNameTextItem.setRequired(false);
        lastNameTextItem.addKeyPressHandler(searchKeyPressHandler);
        ValidationUtils.setValidators(lastNameTextItem, alphaHGAARegExValidator, false);

        firstNameTextItem = new TextItem("firstName", "First Name");
        firstNameTextItem.addKeyPressHandler(searchKeyPressHandler);
        ValidationUtils.setValidators(firstNameTextItem, alphaHGAARegExValidator, false);
        
        emailTextItem = new TextItem("EmailId", "User ID/Email");
        RegExpValidator userIdRegExpValidator = getEmailAddressRegExpValidator();
        ValidationUtils.setValidators(emailTextItem, userIdRegExpValidator, false);
        emailTextItem.addKeyPressHandler(searchKeyPressHandler);
        
        adminSectionItem.setItemIds(lastNameTextItem.getName(), firstNameTextItem.getName(), emailTextItem.getName());
        RowSpacerItem rowSpacerItem = new RowSpacerItem();

        this.setFields(adminSectionItem, lastNameTextItem, firstNameTextItem, emailTextItem, rowSpacerItem);
    }

    private SectionItem buildSectionItem(String title) {
        SectionItem sectionItem = new SectionItem();
        sectionItem.setDefaultValue(title);
        return sectionItem;
    }

    public SearchCriteria getSearchCriteria() {
        SearchCriteria searchCriteria = new SearchCriteria();

        searchCriteria.setLastName(lastNameTextItem.getValueAsString());
        searchCriteria.setFirstName(firstNameTextItem.getValueAsString());
        searchCriteria.setEmail(emailTextItem.getValueAsString());
        return searchCriteria;
    }

    class SearchKeyPressHandler implements KeyPressHandler {

        @Override
        public void onKeyPress(KeyPressEvent event) {
            if (event.getKeyName().equals(Constants.ENTER_KEY)) {
                searchClickHandler.onClick(null);
            }
        }
    }
    
    public void clearValues() {
        clearUserSearchForm();
    }
    
    private void clearUserSearchForm() {

        // clear values on form
        lastNameTextItem.clearValue();
        firstNameTextItem.clearValue();
        emailTextItem.clearValue();        
    }
    
     private RegExpValidator getEmailAddressRegExpValidator() {
        RegExpValidator validator = new RegExpValidator();
        validator.setExpression(Constants.EMAIL_VALIDATION_REGEX);
        validator.setErrorMessage("Email must be valid.");
        return validator;
    }
    
    
}
