package com.gsihealth.dashboard.client.admintools.organization;

import com.google.gwt.core.client.GWT;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.admintools.Mode;
import com.gsihealth.dashboard.client.admintools.handlers.CancelClickHandler;
import com.gsihealth.dashboard.client.callbacks.GetOrganizationsCallback;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.service.OrganizationService;
import com.gsihealth.dashboard.client.service.OrganizationServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.List;

/**
 * Allows user to update an administrator
 *
 * @author Chad Darby
 */
public class UpdateOrganizationPanel extends VLayout {

    private OrganizationListPanel organizationListPanel;
    private OrganizationForm form;
    private final OrganizationServiceAsync orgService = GWT.create(OrganizationService.class);
    private ProgressBarWindow progressBarWindow;
    private GetOrganizationsCallback getOrganizationsCallback;
    private UpdateOrganizationCallback updateOrganizationCallback;
    private OrganizationSearchForm organizationSearchForm;
    private Button searchButton;
    private Button clearButton;
    private boolean searchMode;
    private SearchClickHandler searchClickHandler;
    private int currentPageNumber;
    private long communityId;

    public UpdateOrganizationPanel() {

        communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
        buildGui();
        DashBoardApp.restartTimer();
    }

    private void buildGui() {

        progressBarWindow = new ProgressBarWindow();
        updateOrganizationCallback = new UpdateOrganizationCallback(progressBarWindow);

        Label label = new Label("<h2>Update Organization</h2>");
        label.setWidth(280);
        label.setHeight(40);
        addMember(label);


        searchClickHandler = new SearchClickHandler();

        HLayout hLayoutSearch = new HLayout();

        VLayout adminSearchVLayout = buildSideSearchLayout();

        VLayout searchLayout = new VLayout();

        organizationSearchForm = new OrganizationSearchForm(searchClickHandler);
        organizationSearchForm.setWidth("262px");

        searchLayout.addMember(organizationSearchForm);
        searchLayout.addMember(adminSearchVLayout);

        hLayoutSearch.addMember(searchLayout);
        addMember(hLayoutSearch);

        VLayout vLayout = new VLayout();
        organizationListPanel = new OrganizationListPanel();
        organizationListPanel.setPageEventHandler(new OrganizationListPageEventHandler());
        getOrganizationsCallback = new GetOrganizationsCallback(organizationListPanel, progressBarWindow);

        vLayout.addMember(organizationListPanel);

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(20);
        addMember(spacer);


        Label labelNote = new Label("<b>* indicates required field</b>");
        labelNote.setWidth(300);
        labelNote.setHeight(30);

        vLayout.addMember(labelNote);

        form = new OrganizationForm(Mode.UPDATE);
        vLayout.addMember(form);

        addMember(spacer);

        HLayout buttonLayout = new HLayout();
        buttonLayout.setHeight("14px");
        buttonLayout.setLayoutTopMargin(5);
        IButton updateButton = new IButton("Save");
        IButton cancelButton = new IButton("Cancel");
        buttonLayout.addMember(updateButton);
        buttonLayout.addMember(cancelButton);

        vLayout.addMember(buttonLayout);
        this.setOverflow(Overflow.AUTO);


        hLayoutSearch.addMember(vLayout);

        // setup listeners on list
        ListGrid userListGrid = organizationListPanel.getListGrid();
        userListGrid.addRecordClickHandler(new OrganizationRecordClickHandler());

        // setup listener on Update button
        updateButton.addClickHandler(new UpdateOrganizationClickHandler());
        cancelButton.addClickHandler(new CancelClickHandler(form, userListGrid));

        gotoPage(1);
    }

    public void gotoPage(final int pageNumber) {

        GetOrgTotalCountCallback getOrgTotalCountCallback = new GetOrgTotalCountCallback(pageNumber);
        getOrgTotalCountCallback.attempt();        
    }

    private void loadListWithOrganization() {
        int pageNumber = organizationListPanel.getCurrentPageNumber();
        int pageSize = organizationListPanel.getPageSize();

        // progressBarWindow.show();
        orgService.getOrgs(communityId, pageNumber, pageSize, getOrganizationsCallback);

        TotalCountsCallback totalCountsCallback = new TotalCountsCallback();
        totalCountsCallback.attempt();
    }

    /**
     * Updates the administrator on the server
     */
    class UpdateOrganizationClickHandler implements ClickHandler {

        private OrganizationRecord organizationRecord;

        //UserRecord userRecord;
        public void onClick(ClickEvent event) {

            ListGrid organizationListGrid = organizationListPanel.getListGrid();

            organizationRecord = (OrganizationRecord) organizationListGrid.getSelectedRecord();

            if (organizationRecord == null) {
                SC.warn("Nothing selected.");
                return;
            }

            if (form.validate()) {

                OrganizationDTO theOrgDTO = form.getOrgDTO();

                progressBarWindow.show();

                String token = "";

                long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
                orgService.updateOrg(communityId, theOrgDTO, token, updateOrganizationCallback);
                searchMode = false;
            }
        }
    }

    /**
     * Handles the response of server-side update
     */
    class UpdateOrganizationCallback extends PortalAsyncCallback {

        UpdateOrganizationCallback(ProgressBarWindow window) {
            super(window);
        }

        @Override
        public void onSuccess(Object t) {
            loadListWithOrganization();

            SC.say("Save successful.");
            form.clearValues();
            organizationSearchForm.clearValues();

            TotalCountsCallback totalCountsCallback = new TotalCountsCallback();
            totalCountsCallback.attempt();
        }
    }

    /**
     * For each record clicked, populate form w/ user data
     */
    class OrganizationRecordClickHandler implements RecordClickHandler {

        public void onRecordClick(RecordClickEvent event) {
            OrganizationRecord theRecord = (OrganizationRecord) event.getRecord();
            int index = theRecord.getIndex();

            List<OrganizationDTO> orgDTOs = getOrganizationsCallback.getOrgDTOs();

            // get selected DTO
            OrganizationDTO theOrgDTO = orgDTOs.get(index);

            // now populate the form
            form.populateForm(theOrgDTO);
        }
    }

    class OrganizationListPageEventHandler implements PageEventHandler {

        public void handlePage(int pageNum, int pageSize) {
//            
            if (searchMode) {
                SearchCriteria searchCriteria = organizationSearchForm.getSearchCriteria();
                searchCriteria.setOrgName(searchCriteria.getOrgName().trim());
                orgService.findCandidates(communityId,searchCriteria, getOrganizationsCallback);
            } else {

                orgService.getOrgs(communityId, pageNum, pageSize, getOrganizationsCallback);
            }

        }
    }

    /**
     * ClearClickHandler
     */
    class ClearClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            searchMode = false;
            DashBoardApp.restartTimer();
            // organizationSearchForm.reset();

            boolean blnClear = organizationSearchForm.getOrganizationSearchValues();


            if (!blnClear) {

                organizationSearchForm.clearValues();
                loadListWithOrganization();

            }
        }
    }

    /**
     * SearchClickHandler
     */
    class SearchClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            DashBoardApp.restartTimer();
            searchMode = true;
       
            if (organizationSearchForm.validate()) {
                // form.clearValues();
                SearchCriteria searchCriteria = organizationSearchForm.getSearchCriteria();
                if (searchCriteria.getOrgName() == null) {
                    SC.warn("You must enter Organizaton Name.");
                } else {
                    searchCriteria.setOrgName(searchCriteria.getOrgName().trim());
                    searchOrganization(1, searchCriteria);
                }

            }
        }
    }

    /**
     *
     * @param pageNumber
     * @param searchCriteria
     */
    public void searchOrganization(final int pageNumber, final SearchCriteria searchCriteria) {

        progressBarWindow.setVisible(true);
        currentPageNumber = pageNumber;

        FindTotalOrgsCountsCallback findTotalOrgsCountsCallback = new FindTotalOrgsCountsCallback(searchCriteria);
        findTotalOrgsCountsCallback.attempt();        
    }

    private VLayout buildSideSearchLayout() {
        VLayout sideSearchLayout = new VLayout();

        sideSearchLayout.setWidth("260px");

        sideSearchLayout.setShowResizeBar(false);

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(10);
        sideSearchLayout.addMember(spacer);

        Layout searchButtonBar = buildSearchButtonBar();
        sideSearchLayout.addMember(searchButtonBar);

        return sideSearchLayout;
    }

    /**
     * Helper method to build button bar
     *
     * @return
     */
    private Layout buildSearchButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        buttonLayout.setWidth("260px");
        buttonLayout.setAlign(Alignment.CENTER);

        searchButton = new Button("Search");
        clearButton = new Button("Clear");
        buttonLayout.addMember(searchButton);
        buttonLayout.addMember(clearButton);

        searchButton.addClickHandler(searchClickHandler);
        clearButton.addClickHandler(new ClearClickHandler());

        return buttonLayout;
    }

    class TotalCountsCallback extends RetryActionCallback<Integer> {

        @Override
        public void attempt() {
            orgService.getTotalCount(communityId, this);
        }

        @Override
        public void onCapture(Integer total) {
            organizationListPanel.setTotalCount(total);
            progressBarWindow.setVisible(false);

            if (total == 0) {
                organizationListPanel.clearListGrid();
                SC.warn(Constants.SEARCH_DID_NOT_RETURN_ANY_RESULTS);
            } else {
                organizationListPanel.gotoPage(currentPageNumber);
            }
        }

        @Override
        public void onFailure(Throwable exc) {
            progressBarWindow.setVisible(false);
            System.out.println("organization TotalCountsCallback fail for: " + exc);
            exc.printStackTrace();
            super.onFailure(exc.getCause());
        }
    }

    class FindTotalOrgsCountsCallback extends RetryActionCallback<Integer> {

        private SearchCriteria searchCriteria;
        
        FindTotalOrgsCountsCallback(SearchCriteria searchCriteria) {
            this.searchCriteria = searchCriteria;
        }
        
        @Override
        public void attempt() {
            orgService.findTotalNoOrgs(communityId,searchCriteria, this);
        }

        @Override
        public void onCapture(Integer total) {
            organizationListPanel.setTotalCount(total);
            progressBarWindow.setVisible(false);

            if (total == 0) {
                organizationListPanel.clearListGrid();
                SC.warn(Constants.SEARCH_DID_NOT_RETURN_ANY_RESULTS);
            } else {
                organizationListPanel.gotoPage(currentPageNumber);
            }
        }

        @Override
        public void onFailure(Throwable exc) {
            progressBarWindow.setVisible(false);
            super.onFailure(exc);
        }
    }

    private class GetOrgTotalCountCallback extends RetryActionCallback<Integer> {

        private final int pageNumber;

        public GetOrgTotalCountCallback(int pageNumber) {
            this.pageNumber = pageNumber;
        }

        @Override
        public void attempt() {
            orgService.getTotalCount(communityId, this);
        }
        
        @Override
        public void onCapture(Integer total) {
            organizationListPanel.setTotalCount(total);
            organizationListPanel.gotoPage(pageNumber);
        }
    }

}
