package com.gsihealth.dashboard.client.admintools.organization;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.admintools.Mode;
import com.gsihealth.dashboard.client.admintools.handlers.CancelClickHandler;
import com.gsihealth.dashboard.client.callbacks.BasicPortalAsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.service.OrganizationService;
import com.gsihealth.dashboard.client.service.OrganizationServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.Date;

/**
 *
 * @author Satyendra Singh
 */
public class AddOrganizationPanel extends VLayout {

    private OrganizationForm form;
    private IButton addButton;
    private IButton cancelButton;
    private OrganizationDTO theOrgDTO;
    private final OrganizationServiceAsync orgService = GWT.create(OrganizationService.class);

    private String oid;
    private OrganizationDTO orgDTO;
    private boolean isExistingOrg;

    
//    public AddUserPanel() {
//        buildGui();
//        DashBoardApp.restartTimer();
//
//    }
    public AddOrganizationPanel(String oid, OrganizationDTO orgDTO, boolean isExistingOrg) {
        this.oid = oid;
        this.orgDTO = orgDTO;
        this.isExistingOrg = isExistingOrg;

        buildGui();
        DashBoardApp.restartTimer();
    }

    private void buildGui() {

        Label label = new Label("<h2>Add Organization</h2>");
        label.setWidth(200);
        label.setHeight(40);
        addMember(label);

        Label labelNote = new Label("<b>* indicates required field</b>");
        labelNote.setWidth(300);
        labelNote.setHeight(30);
        addMember(labelNote);

//        if(isExistingOrg)
//        	form = new OrganizationForm(Mode.UPDATE, oid, orgDTO, isExistingOrg);
//        else
        form = new OrganizationForm(Mode.ADD, oid, orgDTO, isExistingOrg);
        addMember(form);
        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(20);
        addMember(spacer);

        Layout buttonBar = buildButtonBar();
        addMember(buttonBar);

        addButton.addClickHandler(new AddOrganizationClickHandler());
        cancelButton.addClickHandler(new CancelClickHandler(form));

        this.setOverflow(Overflow.AUTO);
    }

    /**
     * Helper method to build button bar
     * 
     * @return
     */
    private Layout buildButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        addButton = new IButton("Save");
        cancelButton = new IButton("Cancel");
        buttonLayout.addMember(addButton);
        buttonLayout.addMember(cancelButton);

        return buttonLayout;
    }

    public OrganizationDTO getTheOrgDTO() {
        return theOrgDTO;
    }

    public void setTheOrgDTO(UserDTO theUserDTO) {
        this.theOrgDTO = theOrgDTO;
    }

    class AddOrganizationClickHandler implements ClickHandler {

        private ProgressBarWindow progressBarWindow;

        public void onClick(ClickEvent event) {

            DashBoardApp.restartTimer();

            if (form.validate()) {

                theOrgDTO = form.getOrgDTO();

                Date now = new Date();
                theOrgDTO.setEffectiveDate(now);
                theOrgDTO.setEndDate(null);

                progressBarWindow = new ProgressBarWindow();
                progressBarWindow.show();

                String confirmationMessage = getConfirmationMessage();
                BasicPortalAsyncCallback basicPortalAsyncCallback = new AddOrganizationCallback(progressBarWindow, confirmationMessage);
                String token = "";
                // call service

                long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
//                if(isExistingOrg)
//                	orgService.updateOrg(communityId, theOrgDTO, token, basicPortalAsyncCallback);
//                else 
                	orgService.addOrg(communityId, theOrgDTO, token, basicPortalAsyncCallback);
            }
        }

        protected String getConfirmationMessage() {
            return "Save successful.";
        }
    }

    class AddOrganizationCallback extends BasicPortalAsyncCallback {

        AddOrganizationCallback(ProgressBarWindow theProgressBarWindow, String theConfirmationMessage) {
            super(theProgressBarWindow, theConfirmationMessage);
        }

        @Override
        public void onSuccess(Object t) {
            super.onSuccess(t);

            form.clearValues();
        }
    }
}
