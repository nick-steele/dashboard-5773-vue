package com.gsihealth.dashboard.client.admintools.organization;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.common.GridPager;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.List;

/**
 *
 * @author Satyendra Singh
 */
public class OrganizationListPanel extends VLayout {

    private ListGrid organizationListGrid;
    private GridPager pager;
    private List<OrganizationDTO> orgs;
    private static final int COMPONENT_WIDTH = 920;

    public OrganizationListPanel() {
        buildGui();
        DashBoardApp.restartTimer();
    }

    private void buildGui() {
       
        setHeight(100);
        organizationListGrid = buildOrganizationListGrid();

        addMember(organizationListGrid);

        pager = new GridPager(0);
        pager.setWidth(COMPONENT_WIDTH);
        pager.showLoading();
        addMember(pager);

    }

    public void populateListGrid(ListGridRecord[] data, List<OrganizationDTO> theOrg, int totalCount) {
       
       
         organizationListGrid.scrollToRow(1);
        orgs = theOrg;
        organizationListGrid.setData(data);
        organizationListGrid.markForRedraw();
        pager.setTotalCount(totalCount);
       
    }

    /** Helper method to build a ListGrid
     *
     * @return
     */
    private ListGrid buildOrganizationListGrid() {

        String[] columnNames = {"Organization Name", "OID", "Facility Type", "Address", "Telephone #"};
        ListGrid theListGrid = new ListGrid();

        theListGrid.setCanReorderFields(true);
        theListGrid.setCanReorderRecords(true);

        theListGrid.setWidth(COMPONENT_WIDTH);
        theListGrid.setHeight(150);

        theListGrid.setAlternateRecordStyles(true);
        theListGrid.setShowAllRecords(false);
        theListGrid.setSelectionType(SelectionStyle.SINGLE);

        // add fields
        ListGridField[] fields = buildListGridFields(columnNames);
        theListGrid.setFields(fields);

        theListGrid.setEmptyMessage("...");
        theListGrid.setCanResizeFields(true);

        return theListGrid;
    }

    private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];

        for (int i = 0; i < columnNames.length; i++) {
            String tempColumnName = columnNames[i];
            ListGridField tempField = buildListGridField(tempColumnName);
            fields[i] = tempField;
        }

        return fields;
    }

    private ListGridField buildListGridField(String title) {
        ListGridField tempField = new ListGridField(title, title);

        tempField.setCanEdit(false);

        return tempField;
    }

    public ListGrid getListGrid() {
        return organizationListGrid;
    }

    public void gotoPage(int pageNumber) {
        pager.gotoPage(pageNumber);
    }

    public int getCurrentPageNumber() {
        return pager.getCurrentPageNumber();
    }

    public void setPageEventHandler(PageEventHandler handler) {
        pager.setPageEventHandler(handler);
    }

    public void refreshPageLabels(int pageNum) {
        pager.refreshPageLabels(pageNum);
    }

    public void setTotalCount(int totalCount) {
        pager.setTotalCount(totalCount);
    }

    public int getPageSize() {
        return pager.getPageSize();
    }

    public void clearListGrid() {
        ListGridRecord[] records = new ListGridRecord[1];
        organizationListGrid.setData(records);
    }
}
