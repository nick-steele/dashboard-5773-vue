package com.gsihealth.dashboard.client.admintools.organization;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.admintools.Mode;
import com.gsihealth.dashboard.client.common.Cancelable;
import com.gsihealth.dashboard.client.common.FormUtils;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.service.AdminService;
import com.gsihealth.dashboard.client.service.AdminServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.client.util.ValidationUtils;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.validator.LengthRangeValidator;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.LinkedHashMap;

/**
 *
 * @author Satyendra Singh
 */
public class OrganizationForm extends VLayout implements Cancelable {

    private static final String ACTIVE_STATUS_VALUE = "ACTIVE";
    private static final String INACTIVE_STATUS_VALUE = "INACTIVE";
    private DynamicForm organizationForm;
    private TextItem organizationNameTextItem;
    private TextItem physicalStreetAddress1TextItem;
    private TextItem physicalStreetAddress2TextItem;
    private TextItem physicalCityTextItem;
    private SelectItem physicalStateSelectItem;
    private TextItem physicalZipCodeTextItem;
    private TextItem workPhoneTextItem;
    private static final int FIELD_WIDTH = 300;
    private Mode mode;
    private DynamicForm tempForm;
    private OrganizationDTO orgDTO;
    private SelectItem facilitySelectItem;
    private AdminServiceAsync adminService;
    private TextItem oidTextItem;
    private SelectItem statusSelectItem;
    
    private String oid;
    private boolean isExistingOrg;

    public OrganizationForm(Mode theMode) {
        mode = theMode;
        adminService = GWT.create(AdminService.class);
        buildGui();
    }

    public OrganizationForm(Mode theMode, String oid, OrganizationDTO orgDTO, boolean isExistingOrg) {
        mode = theMode;
        adminService = GWT.create(AdminService.class);
        
        this.oid = oid;
        this.orgDTO = orgDTO;
        this.isExistingOrg = isExistingOrg;
        
        buildGui();
    }

    private void buildGui() {

        organizationForm = buildOrganizationForm();

        // container for the forms
        HLayout hLayout = new HLayout();

        // add the main form
        hLayout.addMember(organizationForm);

        // add spacer
        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setWidth(50);
        hLayout.addMember(spacer);

        addMember(hLayout);
        if (mode == Mode.UPDATE) {
            tempForm.setGroupTitle("<b>Update Organization</b>");
        }

        if(orgDTO != null) {
        	populateForm(orgDTO);
        } else {
        	oidTextItem.setValue(this.oid);
        }
        
        // load facility types
        loadFacilityTypes();
    }

    private void clearOrganizationForm() {

        // clear values on form
        organizationNameTextItem.clearValue();
        oidTextItem.clearValue();
        physicalStreetAddress1TextItem.clearValue();
        physicalStreetAddress2TextItem.clearValue();
        physicalCityTextItem.clearValue();
        physicalStateSelectItem.clearValue();
        physicalZipCodeTextItem.clearValue();
        workPhoneTextItem.clearValue();
        facilitySelectItem.clearValue();
    }

    /**
     * Build the contact form
     *
     * @return
     */
    private DynamicForm buildOrganizationForm() {

        RegExpValidator alphaOnlyRegExpValidator = ValidationUtils.getAlphaOnlyRegExpValidator();
        RegExpValidator numberPeriodsOnlyRegExpValidator = ValidationUtils.getNumberPeriodsOnlyRegExpValidator();
        RegExpValidator alphaNumberSpecialSymbolsRegExpValidator = ValidationUtils.getAlphaNumberSpecialSymbolsRegExpValidator();

        tempForm = new DynamicForm();
        tempForm.setGroupTitle("<b>Add Organization</b>");
        tempForm.setIsGroup(true);
        tempForm.setWidth(120);
        tempForm.setWrapItemTitles(false);
        tempForm.setCellPadding(5);
        tempForm.setNumCols(6);
        tempForm.setRequiredTitlePrefix(FormUtils.REQUIRED_TITLE_PREFIX);

        organizationNameTextItem = buildTextItem("Organization Name", true);
        ValidationUtils.setValidators(organizationNameTextItem, alphaOnlyRegExpValidator, true);

        oidTextItem = buildTextItem("OID", true);
        ValidationUtils.setValidators(oidTextItem, numberPeriodsOnlyRegExpValidator, true);

        oidTextItem.setDisabled(true);

        facilitySelectItem = buildSelectItem("Facility Type", true);
        facilitySelectItem.setValidateOnExit(false);

        statusSelectItem = buildSelectItem("Status", true);
        statusSelectItem.setValidateOnExit(false);
        LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
        valueMap.put(ACTIVE_STATUS_VALUE, "Active");
        valueMap.put(INACTIVE_STATUS_VALUE, "Inactive");
        statusSelectItem.setValueMap(valueMap);
        statusSelectItem.setDefaultValue(ACTIVE_STATUS_VALUE);

        if (mode == Mode.UPDATE) {
            statusSelectItem.addChangedHandler(new StatusConfirmationChangedHandler());
        }

        physicalStreetAddress1TextItem = buildTextItem("Address1", true);
        ValidationUtils.setValidators(physicalStreetAddress1TextItem, alphaNumberSpecialSymbolsRegExpValidator, true);

        physicalStreetAddress2TextItem = buildTextItem("Address2", false);
        ValidationUtils.setValidators(physicalStreetAddress2TextItem, alphaNumberSpecialSymbolsRegExpValidator, true);

        physicalCityTextItem = buildTextItem("City", true);
        ValidationUtils.setValidators(physicalCityTextItem, alphaOnlyRegExpValidator, true);

        physicalStateSelectItem = buildSelectItem("State", true);
        physicalStateSelectItem.setValueMap(getStateCodes());
        physicalStateSelectItem.setValidateOnExit(false);
        physicalStateSelectItem.setDefaultValue("NY");

        physicalZipCodeTextItem = buildTextItem("Zip Code", true);
        physicalZipCodeTextItem.setValidateOnExit(false);
        String zipCodeMask = "#####";
        physicalZipCodeTextItem.setMask(zipCodeMask);
        LengthRangeValidator zipCodeValidator = new LengthRangeValidator();
        zipCodeValidator.setMax(5);
        zipCodeValidator.setMin(5);
        zipCodeValidator.setErrorMessage("Invalid length");
        physicalZipCodeTextItem.setValidators(zipCodeValidator);

        workPhoneTextItem = buildTextItem("Telephone #", false);
        workPhoneTextItem.setValidateOnExit(false);
        workPhoneTextItem.setWidth(100);
        workPhoneTextItem.setMask("###-###-####");
        LengthRangeValidator workPhoneValidator = new LengthRangeValidator();
        workPhoneValidator.setMax(10);
        workPhoneValidator.setMin(10);
        workPhoneValidator.setErrorMessage("Invalid length");
        workPhoneTextItem.setValidators(workPhoneValidator);

        tempForm.setFields(organizationNameTextItem, oidTextItem, facilitySelectItem, statusSelectItem, physicalStreetAddress1TextItem, physicalStreetAddress2TextItem, physicalCityTextItem, physicalStateSelectItem, physicalZipCodeTextItem,
                workPhoneTextItem);

        return tempForm;
    }

    private TextItem buildTextItem(String title, boolean required) {
        TextItem item = new TextItem();

        item.setTitle(title);
        item.setRequired(required);
        item.setWidth(FIELD_WIDTH);

        item.setColSpan(4);

        return item;
    }

    private SelectItem buildSelectItem(String title, boolean required) {
        SelectItem tempSelectItem = new SelectItem();

        tempSelectItem.setTitle(title);
        tempSelectItem.setRequired(required);
        tempSelectItem.setWidth(FIELD_WIDTH);

       // tempSelectItem.setAnimatePickList(true);
        //tempSelectItem.setAnimationTime(600);
        tempSelectItem.setColSpan(4);

        return tempSelectItem;
    }

    /**
     * Helper method for retrieving field values
     *
     * @param textItem
     * @return
     */
    private String getValue(TextItem textItem) {
        return StringUtils.trim(textItem.getValueAsString());
    }

    /**
     * Read the form data
     *
     * @return the organization
     */
    public OrganizationDTO getOrgDTO() {

        if (orgDTO == null) {
            orgDTO = new OrganizationDTO();
        }

        // read from form fields
        String facilityType = facilitySelectItem.getValueAsString();
        String organizationName = getValue(organizationNameTextItem);
        String OID = getValue(oidTextItem);
        String status = statusSelectItem.getValueAsString();

        String physicalStreetAddress1 = getValue(physicalStreetAddress1TextItem);
        String physicalStreetAddress2 = getValue(physicalStreetAddress2TextItem);
        String physicalCity = getValue(physicalCityTextItem);
        String physicalState = physicalStateSelectItem.getValueAsString();
        String physicalZipCode = getValue(physicalZipCodeTextItem);

        String workPhone = getValue(workPhoneTextItem);

        orgDTO.setOrganizationName(organizationName);
        orgDTO.setOID(OID);
        orgDTO.setStatus(status);
        orgDTO.setFacilityId(facilityType);
        orgDTO.setPhysicalStreetAddress1(physicalStreetAddress1);
        orgDTO.setPhysicalStreetAddress2(physicalStreetAddress2);
        orgDTO.setPhysicalCity(physicalCity);
        orgDTO.setPhysicalState(physicalState);
        orgDTO.setPhysicalZipCode(physicalZipCode);

        orgDTO.setWorkPhone(workPhone);

        return orgDTO;
    }

    /**
     * Loads the Facility Types
     */
    private void loadFacilityTypes() {
        GetFacilityTypeCallback getFacilityTypeCallback = new GetFacilityTypeCallback();
        getFacilityTypeCallback.attempt();
    }

    public void populateForm(OrganizationDTO theOrgDTO) {
        orgDTO = theOrgDTO;
        // demographic fields
        organizationNameTextItem.setValue(theOrgDTO.getOrganizationName());
        oidTextItem.setValue(theOrgDTO.getOID());
        statusSelectItem.setValue(theOrgDTO.getStatus());
        facilitySelectItem.setValue(theOrgDTO.getFacilityId());
        physicalStreetAddress1TextItem.setValue(theOrgDTO.getPhysicalStreetAddress1());
        physicalStreetAddress2TextItem.setValue(theOrgDTO.getPhysicalStreetAddress2());
        physicalCityTextItem.setValue(theOrgDTO.getPhysicalCity());
        physicalStateSelectItem.setValue(theOrgDTO.getPhysicalState());
        physicalZipCodeTextItem.setValue(theOrgDTO.getPhysicalZipCode());

        workPhoneTextItem.setValue(theOrgDTO.getWorkPhone());

    }

    private String[] getStateCodes() {

        ApplicationContext context = ApplicationContext.getInstance();
        String[] data = (String[]) context.getAttribute(Constants.POSTAL_STATE_CODES_KEY);

        return data;
    }

    public void clearErrors(boolean flag) {
        organizationForm.clearErrors(flag);
    }

    public void clearValues() {
        clearOrganizationForm();
    }

    public boolean validate() {
        boolean organizationFormValidate = organizationForm.validate();
        boolean flag = organizationFormValidate;
        return flag;
    }

    /**
     * Populate the facility type in drop down list w/ data from server
     *
     */
    class GetFacilityTypeCallback extends RetryActionCallback<LinkedHashMap<String, String>> {

        @Override
        public void attempt() {
            long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
            adminService.getFacilityTypes(communityId, this);
        }

        @Override
        public void onCapture(LinkedHashMap<String, String> data) {
            facilitySelectItem.setValueMap(data);
        }
    }

    class StatusConfirmationChangedHandler implements ChangedHandler {

        public void onChanged(ChangedEvent event) {
            String status = event.getValue().toString();

            if (status.equals(INACTIVE_STATUS_VALUE)) {
                String orgName = organizationNameTextItem.getValueAsString();
                String message = "You are about to deactivate the following organization " + orgName + ". Do you want to continue?";
                SC.ask("Deactivate Organization", message, new StatusConfirmationCallback());
            }
        }
    }

    class StatusConfirmationCallback implements BooleanCallback {

        @Override
        public void execute(Boolean value) {

            if (value.booleanValue()) {
                statusSelectItem.setValue(INACTIVE_STATUS_VALUE);
            } else {
                statusSelectItem.setValue(ACTIVE_STATUS_VALUE);
            }
        }
    }
}
