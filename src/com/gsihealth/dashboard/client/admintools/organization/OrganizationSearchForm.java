package com.gsihealth.dashboard.client.admintools.organization;

import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.client.util.ValidationUtils;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.RowSpacerItem;
import com.smartgwt.client.widgets.form.fields.SectionItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;

/**
 *
 * @author User
 */
public class OrganizationSearchForm extends DynamicForm {

    private TextItem orgNameTextItem;
    private ClickHandler searchClickHandler;

    public OrganizationSearchForm(ClickHandler theSearchClickHandler) {
        searchClickHandler = theSearchClickHandler;
        buildGui();

    }

    private void buildGui() {
        RegExpValidator alphaHGAARegExValidator = ValidationUtils.getAlphaHGAARegExValidator();

        KeyPressHandler searchKeyPressHandler = new SearchKeyPressHandler();

        SectionItem adminSectionItem = buildSectionItem("Organization");
        setOrgNameTextItem(new TextItem("orgName", "* Organization Name"));


        getOrgNameTextItem().setWrapTitle(false);
        getOrgNameTextItem().setAlign(Alignment.CENTER);
        getOrgNameTextItem().addKeyPressHandler(searchKeyPressHandler);
        ValidationUtils.setValidators(orgNameTextItem, alphaHGAARegExValidator, false);

        adminSectionItem.setItemIds(getOrgNameTextItem().getName());
        RowSpacerItem rowSpacerItem = new RowSpacerItem();
        rowSpacerItem.setVisible(false);
        this.setFields(adminSectionItem, getOrgNameTextItem(), rowSpacerItem);
    }

    private SectionItem buildSectionItem(String title) {
   
        SectionItem sectionItem = new SectionItem();
        
        sectionItem.setDefaultValue(title);
        return sectionItem;
    }

    public SearchCriteria getSearchCriteria() {
        SearchCriteria searchCriteria = new SearchCriteria();
        String orgName = StringUtils.trim(getOrgNameTextItem().getValueAsString());
        searchCriteria.setOrgName(orgName);
        return searchCriteria;
    }

    /**
     * @return the orgNameTextItem
     */
    public TextItem getOrgNameTextItem() {
        return orgNameTextItem;
    }

    /**
     * @param orgNameTextItem the orgNameTextItem to set
     */
    public void setOrgNameTextItem(TextItem orgNameTextItem) {
        this.orgNameTextItem = orgNameTextItem;
    }

    class SearchKeyPressHandler implements KeyPressHandler {

        @Override
        public void onKeyPress(KeyPressEvent event) {

            if (event.getKeyName().equals(Constants.ENTER_KEY)) {
                searchClickHandler.onClick(null);
            }
        }
    }

    public void clearValues() {
        clearOrgsSearchForm();
    }

    private void clearOrgsSearchForm() {

        // clear values on form
        orgNameTextItem.clearValue();

    }
    
     public boolean getOrganizationSearchValues() {
       
        boolean blnValue=false;
        if(orgNameTextItem.getValueAsString()==null){
            blnValue=true;
        }
        
        return blnValue;
    }
}
