package com.gsihealth.dashboard.client.admintools.organization;

import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 *
 * @author Chad Darby
 */
public class OrganizationRecord extends ListGridRecord {

    private int index;

    public OrganizationRecord(int index, String organizationName, String oid, String facilityType, String facilityAddress, String workPhone) {
        setIndex(index);
        setOrganizationName(organizationName);
        setOid(oid);
        setFacilityType(facilityType);
        setFacilityAddress(facilityAddress);
        setWorkPhone(workPhone);
    }

    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * @return the organizationName
     */
    public String getOrganizationName() {
        return getAttribute("Organization Name");
    }

    /**
     * @param firstName the organizationName to set
     */
    public void setOrganizationName(String organizationName) {
        setAttribute("Organization Name", organizationName);
    }

    /**
     * @return the oid
     */
    public String getOid() {
        return getAttribute("OID");
    }

    /**
     * @param oid the oid to set
     */
    public void setOid(String oid) {
        setAttribute("OID", oid);
    }

    /**
     * @return the facilityType
     */
    public String getFacilityType() {
        return getAttribute("Facility Type");
    }

    /**
     * @param firstName the facilityType to set
     */
    public void setFacilityType(String facilityType) {
        setAttribute("Facility Type", facilityType);
    }

    /**
     * @return the facilityAddress
     */
    public String getFacilityAddress() {
        return getAttribute("Address");
    }

    /**
     * @param facilityAddress the facilityAddress to set
     */
    public void setFacilityAddress(String facilityAddress) {
        setAttribute("Address", facilityAddress);
    }

    /**
     * @return the workPhone
     */
    public String getWorkPhone() {
        return getAttribute("Telephone #");
    }

    /**
     * @param org the workPhone to set
     */
    public void setWorkPhone(String workPhone) {
        setAttribute("Telephone #", workPhone);
    }
}
