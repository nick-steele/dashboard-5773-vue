package com.gsihealth.dashboard.client.admintools;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.admintools.organization.AddOrganizationPanel;
import com.gsihealth.dashboard.client.admintools.organization.UpdateOrganizationPanel;
import com.gsihealth.dashboard.client.admintools.payerplan.AddPayerPlanPanel;
import com.gsihealth.dashboard.client.admintools.payerplan.PayerPlansSearchPanel;
import com.gsihealth.dashboard.client.admintools.payerplan.SearchResultsPanel;
import com.gsihealth.dashboard.client.admintools.user.AddUserPanel;
import com.gsihealth.dashboard.client.admintools.user.NewOrgOIDWindow;
import com.gsihealth.dashboard.client.admintools.user.NewUserEmailWindow;
import com.gsihealth.dashboard.client.admintools.user.UpdateUserPanel;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.common.AccessLevelConstants;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

/**
 *
 * @author Chad Darby
 */
public class AdminToolsPanel extends VLayout {

    private static final int MAIN_CONTENT_POSITION = 1;

    public AdminToolsPanel() {
        buildGui();
        DashBoardApp.restartTimer();
    }

    private void buildGui() {
        ToolStrip toolStrip = createToolStrip();
        addMember(toolStrip);
        setPadding(5);
        
        
    }

    protected ToolStrip createToolStrip() {

        ToolStrip toolStrip = new ToolStrip();
        toolStrip.setWidth100();

        setupUserAdminButtons(toolStrip);

        if (isSuperUser() || isPowerUser()) {
            setupOrganizationAdminButtons(toolStrip);             
        }

        return toolStrip;
    }

    protected void setupOrganizationAdminButtons(ToolStrip toolStrip) {
        // setup Organization Admin buttons
        ToolStripButton addOrganizationButton = new ToolStripButton("Add Organization");
        addOrganizationButton.setID("AddOrganization");
        ToolStripButton updateOrganizationButton = new ToolStripButton("Update Organization");
        updateOrganizationButton.setID("UpdateOrganization");

        toolStrip.addSeparator();
        toolStrip.addButton(addOrganizationButton);
        toolStrip.addButton(updateOrganizationButton);       

        addOrganizationButton.addClickHandler(new AddOrganizationClickHandler());
        updateOrganizationButton.addClickHandler(new UpdateOrganizationClickHandler());
        toolStrip.addSeparator();
    }

    
    protected void setupPayerPlanAdminButtons(ToolStrip toolStrip) {
        // setup Payer Plan Admin buttons
        ToolStripButton searchPayerPlansButton = new ToolStripButton("Search Payer Plans");
        searchPayerPlansButton.addClickHandler(new SearchPayerPlansClickHandler());

        ToolStripButton addPayerPlanButton = new ToolStripButton("Add Payer Plan");
        addPayerPlanButton.addClickHandler(new AddPayerPlanClickHandler());

        ToolStripButton updatePayerPlanButton = new ToolStripButton("Update Payer Plan");
        updatePayerPlanButton.addClickHandler(new UpdatePayerPlanClickHandler());

        ToolStripButton deletePayerPlanButton = new ToolStripButton("Delete Payer Plan");
        deletePayerPlanButton.addClickHandler(new DeletePayerPlanClickHandler());

        toolStrip.addButton(searchPayerPlansButton);
        toolStrip.addButton(addPayerPlanButton);
        toolStrip.addButton(updatePayerPlanButton);
        toolStrip.addButton(deletePayerPlanButton);
    }

    protected void setupUserAdminButtons(ToolStrip toolStrip) {
        ToolStripButton addUserButton = new ToolStripButton("Add User");
        addUserButton.setID("AdminAddUser");
        ToolStripButton updateUserButton = new ToolStripButton("Update User");
        updateUserButton.setID("AdminUpdateUser");

        toolStrip.addButton(addUserButton);
        toolStrip.addButton(updateUserButton);

        addUserButton.addClickHandler(new AddUserClickHandler());
        updateUserButton.addClickHandler(new UpdateUserClickHandler());
    }

    private boolean isSuperUser() {
        long accessLevelId = ApplicationContextUtils.getLoginResult().getAccessLevelId();
        boolean superUser = accessLevelId == AccessLevelConstants.SUPER_USER_ACCESS_LEVEL_ID;

        return superUser;
    }

    private boolean isPowerUser() {
        long accessLevelId = ApplicationContextUtils.getLoginResult().getAccessLevelId();
        boolean powerUser = accessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID;

        return powerUser;
    }
    
    /**
     * get and validate email before showing user dialog
     */
    private void openNewOrgPopup(){
    	NewOrgOIDWindow newOrgWindow = new NewOrgOIDWindow(this);
    	newOrgWindow.showSetFocus();
    }
    
    /**
     * get and validate email before showing user dialog
     */
    private void openNewUserPopup(){
        NewUserEmailWindow newUserWindow = new NewUserEmailWindow(this);
        newUserWindow.showSetFocus();
    }

    public void showPanel(Canvas thePanel) {
        // remove old panel
        Canvas oldPanel = getMember(MAIN_CONTENT_POSITION);
       
        if(oldPanel!=null){
             removeMember(oldPanel);
        }
       

        addMember(thePanel, MAIN_CONTENT_POSITION);
    }
    
    public Canvas getPanel(){
        return getMember(MAIN_CONTENT_POSITION);
    }
    
    public void createUserPanel(String email, UserDTO userDTO, boolean isExistingUser){
        Canvas thePanel = new AddUserPanel(email, userDTO, isExistingUser);
        showPanel(thePanel);
    }

    public void createOrgPanel(String oid, OrganizationDTO orgDTO, boolean isExistingOrg){
        Canvas thePanel = new AddOrganizationPanel(oid, orgDTO, isExistingOrg);
        showPanel(thePanel);
    }

    class AddUserClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            openNewUserPopup();
        }
    }

    class UpdateUserClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            Canvas thePanel = new UpdateUserPanel();
            showPanel(thePanel);
        }
    }

    class AddOrganizationClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
        	openNewOrgPopup();
//            Canvas thePanel = new AddOrganizationPanel();
//            showPanel(thePanel);
        }
    }

    class UpdateOrganizationClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            Canvas thePanel = new UpdateOrganizationPanel();
            showPanel(thePanel);
        }
    }

    class SearchPayerPlansClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            Canvas thePanel = new PayerPlansSearchPanel();
            showPanel(thePanel);
        }
    }

    class AddPayerPlanClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            // get panel
            ApplicationContext applicationContext = ApplicationContext.getInstance();
            SearchResultsPanel searchResultsPanel = (SearchResultsPanel) applicationContext.getAttribute(Constants.PAYER_PLAN_SEARCH_RESULTS_PANEL_KEY);

            // clear old selections
            if (searchResultsPanel != null) {
                searchResultsPanel.getListGrid().deselectAllRecords();
            }

            // clear panel and show it
            Canvas thePanel = new AddPayerPlanPanel();
            showPanel(thePanel);
        }
    }

    class UpdatePayerPlanClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            // get panel
            ApplicationContext applicationContext = ApplicationContext.getInstance();
            SearchResultsPanel searchResultsPanel = (SearchResultsPanel) applicationContext.getAttribute(Constants.PAYER_PLAN_SEARCH_RESULTS_PANEL_KEY);

            // update payer plan
            if (searchResultsPanel != null) {
                searchResultsPanel.updateSelectedPayerPlan();
            } else {
                SC.warn("You must select a payer plan from the search results.");
            }
        }
    }

    class DeletePayerPlanClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            // get panel
            ApplicationContext applicationContext = ApplicationContext.getInstance();
            SearchResultsPanel searchResultsPanel = (SearchResultsPanel) applicationContext.getAttribute(Constants.PAYER_PLAN_SEARCH_RESULTS_PANEL_KEY);

            // update payer plan
            if (searchResultsPanel != null) {
                searchResultsPanel.deleteSelectedPayerPlan();
            } else {
                SC.warn("You must select a payer plan from the search results.");
            }
        }
    }
    
    
}
