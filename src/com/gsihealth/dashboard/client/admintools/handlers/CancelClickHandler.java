package com.gsihealth.dashboard.client.admintools.handlers;

import com.gsihealth.dashboard.client.common.Cancelable;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;

/**
 *
 * @author Satyendra
 */
public class CancelClickHandler implements ClickHandler {

    private Cancelable form;
    private ListGrid listGrid;
    
    public CancelClickHandler(Cancelable theForm) {
        this(theForm, null);
    }

    public CancelClickHandler(Cancelable theForm, ListGrid theListGrid) {
        form = theForm;
        listGrid = theListGrid;
    }

    /**
     * Remove the current panel
     * 
     * @param event
     */
    public void onClick(ClickEvent event) {

        if (form != null) {
            form.clearErrors(true);
            form.clearValues();
        }

        if (listGrid != null) {
            listGrid.deselectAllRecords();
        }
    }
}
