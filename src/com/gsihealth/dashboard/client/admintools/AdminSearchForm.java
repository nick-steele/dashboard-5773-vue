/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.admintools;

import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.gsihealth.dashboard.client.common.Cancelable;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.client.util.ValidationUtils;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.RowSpacerItem;
import com.smartgwt.client.widgets.form.fields.SectionItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;


/**
 *
 * @author User
 */
public class AdminSearchForm extends DynamicForm implements Cancelable {

    private ClickHandler searchClickHandler;
    private TextItem lastNameTextItem;
    private TextItem firstNameTextItem;

       
    public AdminSearchForm(ClickHandler theSearchClickHandler) {

         searchClickHandler = theSearchClickHandler;

        buildGui();
        
    }



    private void buildGui() {
      
        KeyPressHandler searchKeyPressHandler = new SearchKeyPressHandler();
        RegExpValidator alphaOnlyRegExpValidator = ValidationUtils.getAlphaOnlyRegExpValidator();
      
        

        
        SectionItem adminSectionItem = buildSectionItem("Search");

        lastNameTextItem = new TextItem("lastName", "Last Name");
        lastNameTextItem.setRequired(false);
       
        ValidationUtils.setValidators(lastNameTextItem, alphaOnlyRegExpValidator, false);
        lastNameTextItem.addKeyPressHandler(searchKeyPressHandler);


        firstNameTextItem = new TextItem("firstName", "First Name");
        ValidationUtils.setValidators(firstNameTextItem, alphaOnlyRegExpValidator, true);
        firstNameTextItem.addKeyPressHandler(searchKeyPressHandler);
    
        adminSectionItem.setItemIds(lastNameTextItem.getName(), firstNameTextItem.getName());

        RowSpacerItem rowSpacerItem = new RowSpacerItem();
        

        this.setFields(adminSectionItem, lastNameTextItem, firstNameTextItem, rowSpacerItem);
    }
    
    private SectionItem buildSectionItem(String title) {
        SectionItem sectionItem = new SectionItem();
        sectionItem.setDefaultValue(title);  // the title of section
       // sectionItem.setSectionExpanded(true);

        return sectionItem;
    }

    
    class SearchKeyPressHandler implements KeyPressHandler {

        @Override
        public void onKeyPress(KeyPressEvent event) {
//            if (event.getKeyName().equals(Constants.ENTER_KEY)) {
//                searchClickHandler.onClick(null);
//            }
        }
    }
    
    public SearchCriteria getSearchCriteria() {
        SearchCriteria searchCriteria = new SearchCriteria();

        searchCriteria.setLastName(StringUtils.trim(lastNameTextItem.getValueAsString()));
        searchCriteria.setFirstName(StringUtils.trim(firstNameTextItem.getValueAsString()));

        return searchCriteria;
    }
    
}
