package com.gsihealth.dashboard.client.admintools.callbacks;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.admintools.user.UpdateUserContextEvent;
import com.gsihealth.dashboard.client.admintools.user.UserForm;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.eventbus.EventBusUtils;
import com.gsihealth.dashboard.client.service.AdminService;
import com.gsihealth.dashboard.client.service.AdminServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.gsihealth.dashboard.redux.Redux;
import com.smartgwt.client.util.SC;

import java.util.List;

/**
 * @author Chad Darby
 */
public class AddUserCallback<T> extends PortalAsyncCallback<T> {

    private UserForm form;
    protected String confirmationMessage;
    protected ProgressBarWindow progressBarWindow;
    private final AdminServiceAsync adminService = GWT.create(AdminService.class);

    public AddUserCallback(ProgressBarWindow theProgressBarWindow, String theConfirmationMessage, UserForm theForm) {
        form = theForm;
        confirmationMessage = theConfirmationMessage;
        progressBarWindow = theProgressBarWindow;
    }

    @Override
    public void onSuccess(Object t) {

        SC.say(confirmationMessage);
        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

        form.clearValues();

        Redux.jsEvent("remote:appGlobal:loadMemberList");

        //After adding new user, update user in Application context for client and server.
        adminService.updateUserContexts(ApplicationContextUtils.getLoginResult().getCommunityId(),
            new AsyncCallback<List<UserDTO>>() {

                @Override
                public void onSuccess(List<UserDTO> result) {
                    ApplicationContext context = ApplicationContext.getInstance();
                    context.putAttribute(Constants.PROVIDERS_FOR_CONSENT_OBTAINED_BY, result);
                    // fire event
                    EventBus eventBus = EventBusUtils.getEventBus();
                    eventBus.fireEvent(new UpdateUserContextEvent());
                }

                @Override
                public void onFailure(Throwable th) {

                }
            }
        );
    }

    @Override
    public void onFailure(Throwable exc) {
        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

        SC.warn(exc.getMessage());
    }
}
