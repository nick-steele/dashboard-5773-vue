/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.admintools.payerplan;

import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 *
 * @author Chad Darby
 */
public class PayerPlanRecord  extends ListGridRecord {
    
    private PayerPlanDTO payerPlanDTO;
    
    public PayerPlanRecord(PayerPlanDTO thePayerPlanDTO) {
        
        setAttribute("Payer Plan Name", thePayerPlanDTO.getName());
        setAttribute("MMIS ID", thePayerPlanDTO.getMmisId());
        
        payerPlanDTO = thePayerPlanDTO;
    }

    public PayerPlanDTO getPayerPlanDTO() {
        return payerPlanDTO;
    }

}