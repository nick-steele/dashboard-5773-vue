package com.gsihealth.dashboard.client.admintools.payerplan;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.admintools.handlers.CancelClickHandler;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.service.PayerPlanAdminService;
import com.gsihealth.dashboard.client.service.PayerPlanAdminServiceAsync;
import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 * @author Chad Darby
 */
public class UpdatePayerPlanPanel extends VLayout {
    
    private PayerPlanForm form;
    private Button updateButton;
    private Button cancelButton;
    private ProgressBarWindow progressBarWindow;
    
    private final PayerPlanAdminServiceAsync payerPlanService = GWT.create(PayerPlanAdminService.class);

    public UpdatePayerPlanPanel() {
        buildGui();
        DashBoardApp.restartTimer();
    }

    private void buildGui() {

        setHeight(250);

        setPadding(5);
        setMembersMargin(5);
        
        progressBarWindow = new ProgressBarWindow();
        
        Label label = new Label("<h2>Update Payer Plan</h2>");
        label.setWidth(200);
        label.setHeight(40);
        addMember(label);

        Label labelNote = new Label("<b>* indicates required field</b>");
        labelNote.setWidth(300);
        labelNote.setHeight(30);
        addMember(labelNote);

        form = new PayerPlanForm();
        addMember(form);
        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(20);
        addMember(spacer);

        Layout buttonBar = buildButtonBar();
        addMember(buttonBar);

        updateButton.addClickHandler(new UpdatePayerPlanClickHandler());
        cancelButton.addClickHandler(new CancelClickHandler(form));

        this.setOverflow(Overflow.AUTO);
    }

    /**
     * Helper method to build button bar
     * 
     * @return
     */
    private Layout buildButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        updateButton = new Button("Update");
        cancelButton = new Button("Cancel");
        buttonLayout.addMember(updateButton);
        buttonLayout.addMember(cancelButton);

        return buttonLayout;
    }

    public void populate(PayerPlanDTO thePayerPlan) {
        form.clearValues();
        
        form.populate(thePayerPlan);
    }
    
    class UpdatePayerPlanClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            
            if (form.validate()) {
                PayerPlanDTO payerPlan = form.getPayerPlan();

                progressBarWindow.setVisible(true);
                payerPlanService.updatePayerPlan(payerPlan, new UpdatePayerPlanCallback());
            }
        }        
    }
    
    class UpdatePayerPlanCallback implements AsyncCallback<Void> {

        @Override
        public void onSuccess(Void t) {
            progressBarWindow.setVisible(false);
            
            SC.say("Payer plan updated successfully.");
        }
        
        @Override
        public void onFailure(Throwable exc) {
            progressBarWindow.setVisible(false);

            SC.warn(exc.getMessage());
        }
    }

}
