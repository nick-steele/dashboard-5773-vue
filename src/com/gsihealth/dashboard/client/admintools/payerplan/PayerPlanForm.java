package com.gsihealth.dashboard.client.admintools.payerplan;

import com.gsihealth.dashboard.client.common.Cancelable;
import com.gsihealth.dashboard.client.common.FormUtils;
import com.gsihealth.dashboard.client.util.ValidationUtils;
import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 * @author Chad Darby
 */
public class PayerPlanForm extends VLayout implements Cancelable {

    private DynamicForm tempForm;
    
    private TextItem payerPlanNameTextItem;
    private TextItem mmisIdTextItem;

    private Long payerPlanId;
    
    private static final int FIELD_WIDTH = 250;
    
    public PayerPlanForm() {
        buildGui();
    }

    protected void buildGui() {

        RegExpValidator alphaNumberSpecialSymbolsRegExpValidator = ValidationUtils.getAlphaNumberSpecialSymbolsIncludingAmpersandRegExpValidator();
        RegExpValidator numberOnlyRegExpValidator = ValidationUtils.getNumbersOnlyRegExpValidator();

        tempForm = new DynamicForm();
       
        tempForm.setWidth(120);
        
        tempForm.setWrapItemTitles(false);
        tempForm.setCellPadding(5);
        tempForm.setNumCols(6);
        tempForm.setRequiredTitlePrefix(FormUtils.REQUIRED_TITLE_PREFIX);
     
        payerPlanNameTextItem = buildTextItem("Payer Plan Name", true);
        ValidationUtils.setValidators(payerPlanNameTextItem, alphaNumberSpecialSymbolsRegExpValidator, true);

        mmisIdTextItem = buildTextItem("MMIS ID", false);
        ValidationUtils.setValidators(mmisIdTextItem, numberOnlyRegExpValidator, true);
        
        tempForm.setFields(payerPlanNameTextItem, mmisIdTextItem);
        
        addMember(tempForm);
    }
    
    private TextItem buildTextItem(String title, boolean required) {
        TextItem item = new TextItem();

        item.setTitle(title);
        item.setRequired(required);
        item.setWidth(FIELD_WIDTH);

        item.setColSpan(4);

        return item;
    }
    
    public PayerPlanDTO getPayerPlan() {
    
        PayerPlanDTO payerPlan = new PayerPlanDTO();
     
        payerPlan.setName(payerPlanNameTextItem.getValueAsString());
        payerPlan.setMmisId(mmisIdTextItem.getValueAsString());

        payerPlan.setId(payerPlanId);
        
        return payerPlan;
    }
    
    public void populate(PayerPlanDTO payerPlan) {
        payerPlanNameTextItem.setValue(payerPlan.getName());
        mmisIdTextItem.setValue(payerPlan.getMmisId());
        
        payerPlanId = payerPlan.getId();
    }
    
    public boolean validate() {
        return tempForm.validate();
    }
    
    @Override
    public void clearErrors(boolean flag) {
        tempForm.clearErrors(flag);
    }

    @Override
    public void clearValues() {
        tempForm.clearValues();
    }
    
}
