package com.gsihealth.dashboard.client.admintools.payerplan;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.service.PayerPlanAdminService;
import com.gsihealth.dashboard.client.service.PayerPlanAdminServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.PropertyUtils;
import com.gsihealth.dashboard.common.PayerPlanSearchCriteria;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class PayerPlansSearchPanel extends VLayout {

    private Button searchButton;
    private Button clearButton;
    private SearchResultsPanel payerPlanSearchResultsPanel;
    private SearchForm payerPlanSearchForm;
    private SearchClickHandler searchClickHandler;
    private ProgressBarWindow progressBarWindow;
    private final PayerPlanAdminServiceAsync payerPlanAdminService = GWT.create(PayerPlanAdminService.class);
    private boolean searchMode;
    private int currentPageNumber;

    public PayerPlansSearchPanel() {
        buildGui();
        DashBoardApp.restartTimer();

        // search result panel in context
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        applicationContext.putAttribute(Constants.PAYER_PLAN_SEARCH_RESULTS_PANEL_KEY, payerPlanSearchResultsPanel);
    }

    protected void buildGui() {

        searchClickHandler = new SearchClickHandler();

        payerPlanSearchForm = new SearchForm(searchClickHandler);

        HLayout hLayout = new HLayout();
        hLayout.setWidth100();

        VLayout sideSearchLayout = buildSideSearchLayout();
        hLayout.addMember(sideSearchLayout);

        payerPlanSearchResultsPanel = new SearchResultsPanel(this);
        payerPlanSearchResultsPanel.setPageEventHandler(new ResultsPageEventHandler());
        hLayout.addMember(payerPlanSearchResultsPanel);

        addMember(hLayout);
        this.setOverflow(Overflow.AUTO);

        progressBarWindow = new ProgressBarWindow();

        // Get payer plans from the server        
        gotoPage(1);
    }

    private VLayout buildSideSearchLayout() {
        VLayout sideSearchLayout = new VLayout();


        sideSearchLayout.setHeight100();
        sideSearchLayout.setWidth(250);
        sideSearchLayout.setShowResizeBar(true);

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(10);
        sideSearchLayout.addMember(spacer);

        sideSearchLayout.addMember(payerPlanSearchForm);

        Layout searchButtonBar = buildSearchButtonBar();
        sideSearchLayout.addMember(searchButtonBar);

        return sideSearchLayout;
    }

    public void gotoPage(final int pageNumber) {

        progressBarWindow.setVisible(true);

        currentPageNumber = pageNumber;

        TotalCountsCallback callback = new TotalCountsCallback();
        callback.attempt();

    }

    private void gotoPage(int pageNumber, PayerPlanSearchCriteria payerPlanSearchCriteria) {
        progressBarWindow.setVisible(true);

        currentPageNumber = pageNumber;

        TotalCountsCallback callback = new TotalCountsCallback(payerPlanSearchCriteria);
        callback.attempt();
    }

    /**
     * Helper method to build button bar
     *
     * @return
     */
    private Layout buildSearchButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        buttonLayout.setWidth100();
        buttonLayout.setAlign(Alignment.CENTER);

        searchButton = new Button("Search");
        clearButton = new Button("Clear");
        buttonLayout.addMember(searchButton);
        buttonLayout.addMember(clearButton);

        searchButton.addClickHandler(searchClickHandler);
        clearButton.addClickHandler(new ClearClickHandler());

        return buttonLayout;
    }

    public void setSearchMode(boolean flag) {
        searchMode = flag;
    }

    class SearchClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            DashBoardApp.restartTimer();
            searchMode = true;

            if (payerPlanSearchForm.validate()) {
                PayerPlanSearchCriteria payerPlanSearchCriteria = payerPlanSearchForm.getSearchCriteria();

                gotoPage(1, payerPlanSearchCriteria);
            }

        }
    }

    class ClearClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            searchMode = false;
            DashBoardApp.restartTimer();
            payerPlanSearchForm.reset();
        }
    }

    class ResultsPageEventHandler implements PageEventHandler {

        public void handlePage(int pageNum, int pageSize) {

            progressBarWindow.show();

            PayerPlanSearchCallback payerPlanSearchCallback = new PayerPlanSearchCallback(pageNum, pageSize);
            payerPlanSearchCallback.attempt();            
        }
    }

    class TotalCountsCallback extends RetryActionCallback<Integer> {

        private PayerPlanSearchCriteria payerPlanSearchCriteria;

        TotalCountsCallback() {
            this(null);
        }
        
        TotalCountsCallback(PayerPlanSearchCriteria payerPlanSearchCriteria) {
            this.payerPlanSearchCriteria = payerPlanSearchCriteria;
        }

        @Override
        public void attempt() {

            if (payerPlanSearchCriteria != null) {
                payerPlanAdminService.getTotalCount(payerPlanSearchCriteria, this);
            } else {
                payerPlanAdminService.getTotalCount(this);
            }
        }

        @Override
        public void onCapture(Integer total) {
            payerPlanSearchResultsPanel.setTotalCount(total);
            payerPlanSearchResultsPanel.gotoPage(currentPageNumber);

            if (total == 0) {
                progressBarWindow.setVisible(false);
                payerPlanSearchResultsPanel.clearListGrid();

                if (searchMode) {
                    SC.warn(Constants.SEARCH_DID_NOT_RETURN_ANY_RESULTS);
                }
            }
        }

        @Override
        public void onFailure(Throwable exc) {
            progressBarWindow.setVisible(false);
            super.onFailure(exc);
        }
    }

    class PayerPlanSearchCallback extends RetryActionCallback<SearchResults<PayerPlanDTO>> {

        private int pageNum;
        private int pageSize;
                
        private PayerPlanSearchCallback(int pageNum, int pageSize) {
            this.pageNum = pageNum;
            this.pageSize = pageSize;
        }

        @Override
        public void attempt() {
            if (searchMode) {
                PayerPlanSearchCriteria searchCriteria = payerPlanSearchForm.getSearchCriteria();

                payerPlanAdminService.findPayerPlans(searchCriteria, pageNum, pageSize, this);
            } else {
                payerPlanAdminService.findPayerPlans(pageNum, pageSize, this);
            }            
        }
        
        @Override
        public void onCapture(SearchResults<PayerPlanDTO> searchResults) {

            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            List<PayerPlanDTO> payerPlans = searchResults.getData();

            int totalCount = searchResults.getTotalCount();

            if (!payerPlans.isEmpty()) {
                // convert to list grid records
                ListGridRecord[] data = new ListGridRecord[payerPlans.size()];

                for (int index = 0; index < payerPlans.size(); index++) {
                    PayerPlanDTO tempPerson = payerPlans.get(index);
                    data[index] = PropertyUtils.convert(index, tempPerson);
                }

                payerPlanSearchResultsPanel.populateListGrid(data, payerPlans, totalCount);
            } else {
                payerPlanSearchResultsPanel.clearListGrid();

                if (searchMode) {
                    SC.warn("Search did not return any results. Please modify your search criteria.");
                }
            }
        }

        @Override
        public void onFailure(Throwable error) {
            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            super.onFailure(error); 
        }
    }
}
