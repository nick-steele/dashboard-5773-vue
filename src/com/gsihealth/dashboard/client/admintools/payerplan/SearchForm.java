package com.gsihealth.dashboard.client.admintools.payerplan;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.common.Cancelable;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.service.PayerPlanAdminService;
import com.gsihealth.dashboard.client.service.PayerPlanAdminServiceAsync;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.PayerPlanSearchCriteria;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.RowSpacerItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.form.fields.SectionItem;
import java.util.LinkedHashMap;

/**
 *
 * @author Chad Darby
 */
public class SearchForm extends DynamicForm implements Cancelable {

    private ComboBoxItem payerPlanNameComboBoxItem;
    private TextItem mmisIdTextItem;
    private final PayerPlanAdminServiceAsync payerPlanAdminService = GWT.create(PayerPlanAdminService.class);
    private ClickHandler searchClickHandler;

    public SearchForm(ClickHandler theSearchClickHandler) {

        searchClickHandler = theSearchClickHandler;

        buildGui();

        // load payer plan names
        loadPayerPlanNames();
    }

    private void buildGui() {

        KeyPressHandler searchKeyPressHandler = new SearchKeyPressHandler();

        setBrowserSpellCheck(false);

        payerPlanNameComboBoxItem = new ComboBoxItem("payerPlanNames", "Payer Plan");
        payerPlanNameComboBoxItem.setAddUnknownValues(false);

        mmisIdTextItem = new TextItem("mmisId", "MMIS ID");
        mmisIdTextItem.setRequired(false);
        mmisIdTextItem.addKeyPressHandler(searchKeyPressHandler);

        RowSpacerItem rowSpacerItem = new RowSpacerItem();

        SectionItem payerPlanSectionItem = buildSectionItem("Payer Plan");
        payerPlanSectionItem.setItemIds(payerPlanNameComboBoxItem.getName(), mmisIdTextItem.getName());

        setFields(payerPlanSectionItem, payerPlanNameComboBoxItem, mmisIdTextItem, rowSpacerItem);
    }

    private SectionItem buildSectionItem(String title) {
        SectionItem sectionItem = new SectionItem();
        sectionItem.setDefaultValue(title);  // the title of section
        sectionItem.setSectionExpanded(true);

        return sectionItem;
    }

    private String getPayerPlanId() {
        return payerPlanNameComboBoxItem.getValueAsString();
    }

    private String getMmisId() {
        return mmisIdTextItem.getValueAsString();
    }

    public PayerPlanSearchCriteria getSearchCriteria() {

        String payerPlanId = StringUtils.trim(getPayerPlanId());
        String mmisId = StringUtils.trim(getMmisId());

        PayerPlanSearchCriteria searchCriteria = new PayerPlanSearchCriteria(payerPlanId, mmisId);

        return searchCriteria;
    }

    @Override
    public boolean validate() {
        boolean valid = super.validate();

        boolean atLeastOneSearchCriteriaIsEntered = checkForData();
        if (!atLeastOneSearchCriteriaIsEntered) {
            SC.warn("You must enter at least one search criteria.");
        }

        return valid && atLeastOneSearchCriteriaIsEntered;
    }

    /**
     * Check if a form has data
     *
     * @param theForm
     * @return
     */
    private boolean checkForData() {

        boolean hasData = false;

        FormItem[] formItems = getFields();
        for (FormItem tempFormItem : formItems) {

            // skip section items
            if (tempFormItem instanceof SectionItem) {
                continue;
            }

            Object value = tempFormItem.getValue();

            if (value != null && !StringUtils.isBlank(value.toString())) {
                hasData = true;
                break;
            }
        }


        return hasData;
    }

    private void loadPayerPlanNames() {
        LoadPayerPlanNamesCallback loadPayerPlanNamesCallback = new LoadPayerPlanNamesCallback();
        loadPayerPlanNamesCallback.attempt();
    }

    class LoadPayerPlanNamesCallback extends RetryActionCallback<LinkedHashMap<String, String>> {

        @Override
        public void attempt() {
            payerPlanAdminService.getPayerPlans(this);
        }

        @Override
        public void onCapture(LinkedHashMap<String, String> data) {
            payerPlanNameComboBoxItem.setValueMap(data);
        }
    }

    class SearchKeyPressHandler implements KeyPressHandler {

        @Override
        public void onKeyPress(KeyPressEvent event) {
            if (event.getKeyName().equals(Constants.ENTER_KEY)) {
                searchClickHandler.onClick(null);
            }
        }
    }
}
