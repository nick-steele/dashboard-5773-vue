package com.gsihealth.dashboard.client.admintools.payerplan;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.admintools.AdminToolsPanel;
import com.gsihealth.dashboard.client.common.GridPager;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.service.PayerPlanAdminService;
import com.gsihealth.dashboard.client.service.PayerPlanAdminServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class SearchResultsPanel extends VLayout {

    private ListGrid listGrid;
    private static final String[] columnNamesCandidates = {"Payer Plan Name", "MMIS ID"};
    private GridPager pager;
    private UpdatePayerPlanPanel updatePayerPlanPanel;
    private ProgressBarWindow progressBarWindow;
    private final PayerPlanAdminServiceAsync payerPlanService = GWT.create(PayerPlanAdminService.class);

    private final PayerPlansSearchPanel payerPlansSearchPanel;
    
    public SearchResultsPanel(PayerPlansSearchPanel thePayerPlansSearchPanel) {
        
        payerPlansSearchPanel = thePayerPlansSearchPanel;
        
        buildGui();
    }

    private void buildGui() {

        progressBarWindow = new ProgressBarWindow();

        listGrid = buildListGrid();

        setPadding(5);

        // list grid
        addMember(listGrid);

        // pager
        pager = new GridPager(0);
        addMember(pager);

        LayoutSpacer topSpacer = new LayoutSpacer();
        topSpacer.setHeight(20);
        addMember(topSpacer);

        setWidth100();
        setHeight100();

        // update payer plan panel
        updatePayerPlanPanel = new UpdatePayerPlanPanel();

        // register listeners
        listGrid.addRecordDoubleClickHandler(new PayerPlanRecordDoubleClickHandler());
    }

    /** Helper method to build a ListGrid
     *
     * @return
     */
    private ListGrid buildListGrid() {

        ListGrid theListGrid = new ListGrid();

        theListGrid.setCanReorderFields(true);
        theListGrid.setCanReorderRecords(true);

        theListGrid.setWidth100();
        theListGrid.setHeight100();

        theListGrid.setAlternateRecordStyles(true);
        theListGrid.setShowAllRecords(false);
        theListGrid.setSelectionType(SelectionStyle.SINGLE);

        theListGrid.setCanEdit(false);

        // add fields
        ListGridField[] fields = buildListGridFields(columnNamesCandidates);
        theListGrid.setFields(fields);

        theListGrid.setEmptyMessage("...");
        theListGrid.setCanResizeFields(true);

        return theListGrid;
    }

    private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];

        for (int i = 0; i < columnNames.length; i++) {
            String tempColumnName = columnNames[i];
            ListGridField tempField = buildListGridField(tempColumnName);
            fields[i] = tempField;
        }

        return fields;
    }

    public void clearListGrid() {
        ListGridRecord[] records = new ListGridRecord[1];
        listGrid.setData(records);
    }

    public void populateListGrid(ListGridRecord[] data, List<PayerPlanDTO> thePayerPlans, int totalCount) {

        // scroll to first row
        listGrid.scrollToRow(1);

        listGrid.setData(data);
        listGrid.markForRedraw();

        pager.setTotalCount(totalCount);
    }

    private ListGridField buildListGridField(String title) {
        ListGridField tempField = new ListGridField(title, title);

        tempField.setCanEdit(false);

        return tempField;
    }

    public ListGrid getListGrid() {
        return listGrid;
    }

    public void gotoPage(int pageNumber) {
        pager.gotoPage(pageNumber);
    }

    public int getCurrentPageNumber() {
        return pager.getCurrentPageNumber();
    }

    public void setPageEventHandler(PageEventHandler handler) {
        pager.setPageEventHandler(handler);
    }

    public void refreshPageLabels(int pageNum) {
        pager.refreshPageLabels(pageNum);
    }

    public void setTotalCount(int totalCount) {
        pager.setTotalCount(totalCount);
    }

    public int getPageSize() {
        return pager.getPageSize();
    }

    /**
     * For each record clicked, populate form w/ user data
     */
    class PayerPlanRecordDoubleClickHandler implements RecordDoubleClickHandler {

        @Override
        public void onRecordDoubleClick(RecordDoubleClickEvent event) {
            updateSelectedPayerPlan();
        }
    }

    public void updateSelectedPayerPlan() {
        PayerPlanRecord theRecord = (PayerPlanRecord) listGrid.getSelectedRecord();

        if (theRecord == null) {
            SC.warn("You must select a payer plan from the search results.");
            return;
        }

        listGrid.deselectAllRecords();
        
        // get selected DTO
        PayerPlanDTO thePayerPlan = theRecord.getPayerPlanDTO();

        // populate update panel
        updatePayerPlanPanel.populate(thePayerPlan);

        // now show the panel
        AdminToolsPanel adminToolsPanel = ApplicationContextUtils.getAdminToolsPanel();
        adminToolsPanel.showPanel(updatePayerPlanPanel);
    }

    public void deleteSelectedPayerPlan() {
        PayerPlanRecord theRecord = (PayerPlanRecord) listGrid.getSelectedRecord();

        if (theRecord == null) {
            SC.warn("You must select a payer plan from the search results.");
            return;
        }

        listGrid.deselectAllRecords();
        
        // get selected DTO
        PayerPlanDTO thePayerPlan = theRecord.getPayerPlanDTO();

        // check if this panel is assigned to patients
        payerPlanService.hasPatientsAssignedToThisPayerPlan(thePayerPlan, new HasPatientsAssignedToThisPayerPlanCallback(thePayerPlan));
    }

    class HasPatientsAssignedToThisPayerPlanCallback implements AsyncCallback<Boolean> {

        PayerPlanDTO payerPlan;

        HasPatientsAssignedToThisPayerPlanCallback(PayerPlanDTO thePayerPlan) {
            payerPlan = thePayerPlan;
        }

        @Override
        public void onSuccess(Boolean hasPatientsAssigned) {

            if (hasPatientsAssigned) {
                // warn user
                SC.warn("You can not delete this payer plan. Currently there are patient(s) assigned to this payer plan.");
            } else {
                SC.ask("Delete Payer Plan", "Are you sure you want to delete the payer plan?", new RemoveConfirmationCallback(payerPlan));
            }
        }

        @Override
        public void onFailure(Throwable exc) {
            SC.warn(exc.getMessage());
        }
    }

    class DeletePayerPlanCallback implements AsyncCallback<Void> {

        @Override
        public void onSuccess(Void t) {
            SC.say("Payer plan deleted successfully.", new BooleanCallback() {

                @Override
                public void execute(Boolean value) {
                    payerPlansSearchPanel.setSearchMode(false);
                    gotoPage(1);
                }
            });
        }

        @Override
        public void onFailure(Throwable exc) {
            SC.warn(exc.getMessage());
        }
    }

    class RemoveConfirmationCallback implements BooleanCallback {

        PayerPlanDTO payerPlan;

        RemoveConfirmationCallback(PayerPlanDTO thePayerPlan) {
            payerPlan = thePayerPlan;
        }

        @Override
        public void execute(Boolean value) {

            if (value.booleanValue()) {
                payerPlanService.deletePayerPlan(payerPlan, new DeletePayerPlanCallback());
            }
        }
    }
}
