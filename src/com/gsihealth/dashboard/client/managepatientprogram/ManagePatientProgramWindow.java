package com.gsihealth.dashboard.client.managepatientprogram;


import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.smartgwt.client.widgets.Window;

/**
 *
 * @author ssingh
 */
public class ManagePatientProgramWindow extends Window {
     private PersonDTO personDTO;
      
     public ManagePatientProgramWindow(PersonDTO thePersonDTO) {
        personDTO = thePersonDTO;
        
        buildGui();
    }
    
private void buildGui() {
        DashBoardApp.restartTimer();

        setWindowProps();
        
        ManagePatientProgramPanel managePatientProgramPanel = new ManagePatientProgramPanel(personDTO);
        addItem(managePatientProgramPanel);
        
    }
    
    private void setWindowProps() {
        setPadding(10);
        setWidth(850);
        setHeight(300);
        
        String patientName = personDTO.getFirstName() + " " + personDTO.getLastName();
        setTitle("Manage Patient Programs : " + patientName);
        setShowMinimizeButton(false);
        setIsModal(true);
        setShowModalMask(true);
       
        centerInPage();
    }
    
}
