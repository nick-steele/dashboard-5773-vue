
package com.gsihealth.dashboard.client.managepatientprogram;

import com.gsihealth.dashboard.client.common.FormUtils;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.entity.dto.PatientProgramDTO;
import com.smartgwt.client.docs.Data;
import com.smartgwt.client.util.StringUtil;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author ssingh
 */
public class ManagePatientProgramRecord  {
    ListGridRecord listGridRecord;
    PatientProgramDTO patientProgramDTO;
    ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
    boolean healthHomeRequired = props.isHealthHomeRequired();
    String healthHomeLabel=props.getHealthHomeLabel();
    String programEndReasonLabel=props.getProgramEndReasonLabel();
    long patientId;
    long communityId;

    public ManagePatientProgramRecord(ListGridRecord listGridRecord, long patientId, long communityId) {
        this.listGridRecord = listGridRecord;
        this.patientId = patientId;
        this.communityId = communityId;
    }

    public ManagePatientProgramRecord() {
    }

    public ListGridRecord setListGridRecord(PatientProgramDTO thePatientProgramDto) {
        listGridRecord = new ListGridRecord();
        patientProgramDTO = thePatientProgramDto;
        this.listGridRecord = listGridRecord;
//         Map<String, String> hhReqMap = ApplicationContextUtils.getHHRequiredForProgramValue();
        //JIRA 1023
        String programNameId =Constants.DELIMITER_SELECT_ITEM + thePatientProgramDto.getProgramId();
        String patientStatus=Constants.DELIMITER_SELECT_ITEM + thePatientProgramDto.getPatientStatus();
//        String hhRequired=hhReqMap.get(programNameId.split(Constants.DELIMITER_SELECT_ITEM)[1]);
//        System.out.println("Value hhRequired"+hhRequired);
        this.listGridRecord.setAttribute("id", thePatientProgramDto.getPatientProgramNameId());
        //LinkedHashMap<Long, String> programNameValue = ApplicationContextUtils.getProgramNameValue();
        this.listGridRecord.setAttribute(ManagePatientProgramConstants.PROGRAM, programNameId);
        //this.listGridRecord.setAttribute(ManagePatientProgramConstants.PROGRAM, programNameValue.get(thePatientProgramDto.getProgramId()));
        
//        this.listGridRecord.setAttribute(ManagePatientProgramConstants.HHREQUIRED,  hhRequired);
        this.listGridRecord.setAttribute(healthHomeLabel, thePatientProgramDto.getHealthHome());
        this.listGridRecord.setAttribute(ManagePatientProgramConstants.PROGRAM_EFFECTIVE_DATE, thePatientProgramDto.getProgramEffectiveDate());
        this.listGridRecord.setAttribute(ManagePatientProgramConstants.END_DATE, thePatientProgramDto.getProgramEndDate());
        this.listGridRecord.setAttribute(ManagePatientProgramConstants.PATIENT_STATUS, patientStatus);
        this.listGridRecord.setAttribute(programEndReasonLabel, thePatientProgramDto.getTerminationReason());
        this.listGridRecord.setAttribute(ManagePatientProgramConstants.STATUS_EFFECTIVE, thePatientProgramDto.getStatusEffectiveDate()); 
        this.listGridRecord.setAttribute(ManagePatientProgramConstants.DELETE, false);
        this.listGridRecord.setAttribute("oldRecord", thePatientProgramDto.isOldRecord());
        return listGridRecord;
        
    }

    public PatientProgramDTO getPatientProgramDto() {
        // read the attributes
        patientProgramDTO = new PatientProgramDTO();
        String progId = listGridRecord.getAttribute(ManagePatientProgramConstants.PROGRAM);
//        String hhReq = listGridRecord.getAttribute(ManagePatientProgramConstants.HHREQUIRED);
        Date effectDate = listGridRecord.getAttributeAsDate(ManagePatientProgramConstants.PROGRAM_EFFECTIVE_DATE);
        Date endDate = listGridRecord.getAttributeAsDate(ManagePatientProgramConstants.END_DATE);
        Date statusEffectiveDate = listGridRecord.getAttributeAsDate(ManagePatientProgramConstants.STATUS_EFFECTIVE);
        String patientStatus = listGridRecord.getAttribute(ManagePatientProgramConstants.PATIENT_STATUS);
        String terminReason = listGridRecord.getAttribute(programEndReasonLabel);
        boolean isDelete = listGridRecord.getAttributeAsBoolean(ManagePatientProgramConstants.DELETE);
        boolean isOld=listGridRecord.getAttributeAsBoolean("oldRecord");
        System.out.println("Value of Delete "+isDelete);
        long programId = getId(progId);
        long patientStatusId = getId(patientStatus);
        String healthHome;
        //JIRA 1023
        if(isOld) {
            healthHome = listGridRecord.getAttribute(healthHomeLabel);
        }
        else {
            String healthHomeId = listGridRecord.getAttribute(healthHomeLabel);        
            long hhId = getId(healthHomeId);
            healthHome = ApplicationContextUtils.getHealthHomeCodes().get(hhId);
        }
        if(listGridRecord.getAttributeAsLong("id")!=null){
        patientProgramDTO.setPatientProgramNameId(listGridRecord.getAttributeAsLong("id"));
        }
        patientProgramDTO.setProgramId(programId);
        patientProgramDTO.setHealthHome(healthHome);   
        patientProgramDTO.setProgramEffectiveDate(effectDate);
        patientProgramDTO.setProgramEndDate(endDate);
        patientProgramDTO.setPatientStatus(patientStatusId);
        patientProgramDTO.setTerminationReason(terminReason);
        patientProgramDTO.setCommunityId(communityId);
        patientProgramDTO.setPatientId(patientId);
        patientProgramDTO.setOldRecord(isOld);
        patientProgramDTO.setStatusEffectiveDate(statusEffectiveDate);
        patientProgramDTO.setDelete(isDelete);

        if (isOld){
            patientProgramDTO.setOperation("Update");
        }
        else{
            patientProgramDTO.setOperation("Create");
        }
        
        return patientProgramDTO;
    }

    public long getId(String str) {

        PatientProgramDTO patientProgramDTO = new PatientProgramDTO();
        long Id = 0;

        if (StringUtils.isNotBlank(str)) {
            //for multiple health Home vaule in DB
            if (str.startsWith("_")) {

                Id = Long.parseLong(str.split(Constants.DELIMITER_SELECT_ITEM)[1]);
            } else {
                Id = Long.parseLong(str);

            }
        } else {
            Id = 0;
        }
        return Id;
    }
}