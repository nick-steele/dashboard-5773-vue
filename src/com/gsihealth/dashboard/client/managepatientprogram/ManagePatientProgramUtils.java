
package com.gsihealth.dashboard.client.managepatientprogram;

import com.gsihealth.dashboard.entity.dto.PatientProgramDTO;
import com.gsihealth.dashboard.entity.dto.ProgramDTO;
import com.gsihealth.dashboard.entity.dto.ProgramStatusDTO;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;

import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author ssingh
 */
public class ManagePatientProgramUtils {
   
      
     public static ListGridRecord[] convert(List<PatientProgramDTO> thePrograms) {
         ListGridRecord[] records = new ListGridRecord[thePrograms.size()];
        
        int index = 0;
        for (PatientProgramDTO tempPatientProgram : thePrograms) {
            records[index] = convert(tempPatientProgram);
            index++;
        }
        
        return records;
    }
    
       public static ListGridRecord convert(PatientProgramDTO providerDTO) {
        
           ListGridRecord record = new ManagePatientProgramRecord().setListGridRecord(providerDTO);
        
        return record;
    }


    public static LinkedHashMap<Long, String> convertStatusToMap(List<ProgramStatusDTO> programStatusDTOS) {
        if(programStatusDTOS==null) return null;
        LinkedHashMap<Long, String> valueMap = new LinkedHashMap<Long, String>();
        for(ProgramStatusDTO status: programStatusDTOS){
            valueMap.put(status.getId(), status.getName());
        }
        return valueMap;
    }

    public static LinkedHashMap<Long, String> convertToMap(List<ProgramDTO> programDTOS) {
        if(programDTOS==null) return null;
        LinkedHashMap<Long, String> valueMap = new LinkedHashMap<Long, String>();
        for(ProgramDTO program: programDTOS){
            valueMap.put(program.getId(), program.getName());
        }
        return valueMap;
    }
}
