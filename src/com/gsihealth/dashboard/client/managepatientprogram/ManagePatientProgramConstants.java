/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.managepatientprogram;

/**
 *
 * @author ssingh
 */
public interface ManagePatientProgramConstants {

    public static final String PROGRAM = "Program";
    public static final String HHREQUIRED = "HH Required";
//    public static final String HEALTH_HOME = "Health Home";
    public static final String PROGRAM_EFFECTIVE_DATE = "Program Effective";
    public static final String END_DATE = "Program End";
    public static final String PATIENT_STATUS = "Patient Status";
//    public static final String PROGRAM_END_REASON = "Program End Reason";
    public static final String DELETE = "Delete";
    public static final String STATUS_EFFECTIVE = "Status Effective";
}
