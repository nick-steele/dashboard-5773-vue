/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.managepatientprogram;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.callbacks.BasicPortalAsyncCallback;
import com.gsihealth.dashboard.client.common.FormUtils;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.eventbus.EventBusUtils;
import com.gsihealth.dashboard.client.service.ManagePatientProgramService;
import com.gsihealth.dashboard.client.service.ManagePatientProgramServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.PatientProgramDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.List;

/**
 *
 * @author ssingh
 */
public class DeletePopUpWindow extends Window {

    private PersonDTO personDTO;
    private List<PatientProgramDTO> patientProgramDTO;
    private DynamicForm deleteReasonForm;
    private SelectItem reasonSelectItem;
//    private String[] reasons = {"HouseKeeping", "Other"};
    private ManagePatientProgramServiceAsync managePatientProgramService = GWT.create(ManagePatientProgramService.class);
    private ListGridRecord[] recordsToDelete;
    private ListGrid theListGrid;

    public DeletePopUpWindow(PersonDTO thePersonDTO, List<PatientProgramDTO> dtos, ListGridRecord[] records, ListGrid grid) {
        personDTO = thePersonDTO;
        patientProgramDTO = dtos;
        recordsToDelete=records;
        theListGrid=grid;

        buildGui();
    }

    private void buildGui() {
        DashBoardApp.restartTimer();

        setWindowProps();
        VLayout userlayout = new VLayout();
        deleteReasonForm = getDeleteReasonForm();
        userlayout.addMember(deleteReasonForm);
        addItem(userlayout);
        
       VLayout buttonlayout =new VLayout();
        Canvas buttonBar = buildButtonBar();
        buttonlayout.addMember(buttonBar);
        addItem(buttonlayout);
    }
    
     private void setWindowProps() {
        setPadding(10);
        setWidth(250);
        setHeight(150);
        
        setTitle("Enter Reason for Deleting");
        setShowMinimizeButton(false);
        setIsModal(true);
        setShowModalMask(true);
       
        centerInPage();
    }
     
       private DynamicForm getDeleteReasonForm() {

        DynamicForm theForm = new DynamicForm();
//        theForm.setNumCols(2);
        theForm.setPadding(10);
        theForm.setBrowserSpellCheck(false);
        
        reasonSelectItem = new SelectItem("reasonSelectItem","Select Reason ");
        reasonSelectItem.setRequired(false);
        reasonSelectItem.setWrapTitle(true);
        reasonSelectItem.setRequired(true);
//        reasonSelectItem.setAllowEmptyValue(true);
        reasonSelectItem.setWidth(100);
        reasonSelectItem.setTabIndex(1);
        reasonSelectItem.setValueMap(getProgramDeleteReasonCodes());
        
        theForm.setFields(reasonSelectItem);
        return theForm;
       }
       
        private HLayout buildButtonBar() {
        Button okButton = new Button("OK");
        okButton.setID("OK");
        okButton.addClickHandler(new OkButtonClickHandler());

                
        HLayout buttonBar = new HLayout();
        buttonBar.setWidth(210);
        buttonBar.setAlign(Alignment.RIGHT);
       
        buttonBar.addMember(okButton);
       

        return buttonBar;
    }
        
         class OkButtonClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            if(deleteReasonForm.validate()){
            ProgressBarWindow progressBarWindow = new ProgressBarWindow();
            String selectedReason = reasonSelectItem.getValueAsString();
            personDTO.setProgramDeleteReason(selectedReason);
                       
            String confirmationMessage = "Delete successful.";
            managePatientProgramService.deletePrograms(patientProgramDTO, personDTO, new SaveAsyncCallback(progressBarWindow, confirmationMessage));
           
            hide();
            destroy();
            }
        }
    }

         class SaveAsyncCallback extends BasicPortalAsyncCallback {

        public SaveAsyncCallback(ProgressBarWindow theProgressBarWindow, String confirmationMessage) {
            super(theProgressBarWindow, confirmationMessage);
        }

        @Override
        public void onSuccess(Object t) {
            progressBarWindow.hide();
            SC.say("Delete successful.");
            personDTO.setProgramDeleteReason(null);
                       
             System.out.println("On deletePopUp >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
             for(int i=0;i<recordsToDelete.length;i++){
            theListGrid.removeData(recordsToDelete[i]);
             }
//              // fire event
//            EventBus eventBus = EventBusUtils.getEventBus();
//            eventBus.fireEvent(new DeleteEvent());
       }

        @Override
        public void onFailure(Throwable exc) {
            progressBarWindow.hide();

          SC.warn(exc.getMessage());
        }
    }
    
       private String[] getProgramDeleteReasonCodes() {
        String[] baseCodes = ApplicationContextUtils.getProgramDeleteReasonCodes();

        String[] updatedCodes = FormUtils.getCodesWithAddedBlankOption(baseCodes);

        return updatedCodes;
    }


}
