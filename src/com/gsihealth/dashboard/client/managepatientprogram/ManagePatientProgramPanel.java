package com.gsihealth.dashboard.client.managepatientprogram;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.callbacks.BasicPortalAsyncCallback;
import com.gsihealth.dashboard.client.common.FormUtils;
import com.gsihealth.dashboard.client.common.GridPager;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.eventbus.EventBusUtils;
import com.gsihealth.dashboard.client.service.ManagePatientProgramServiceAsync;
import com.gsihealth.dashboard.client.service.ManagePatientProgramService;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.PatientProgramDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.EditorValueMapFunction;
import com.smartgwt.client.widgets.grid.HoverCustomizer;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.EditorEnterEvent;
import com.smartgwt.client.widgets.grid.events.EditorEnterHandler;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
/**
 *
 * @author ssingh
 */
public class ManagePatientProgramPanel extends VLayout {

    private PersonDTO personDTO;
    private ManagePatientProgramServiceAsync managePatientProgramService = GWT.create(ManagePatientProgramService.class);
    private ListGrid listGrid;
    private ProgressBarWindow progressBarWindow;
    private GridPager pager;
    private ToolStripButton addNewProgramButton;
    private ToolStripButton saveButton;
    private ToolStripButton cancelButton;
    private ToolStripButton deleteButton;
    private  ListGrid theListGrid;
    private boolean healthHomeRequired;
    private String healthHomeLabel;
    private String programEndReasonLabel;
    private long communityId;
    private ManagePatientProgramPanel.GetPagePatientProgramCallback getPatientProgramCallback;
    private SelectItem healthHomeSelectItem;
    private SelectItem terminationReasonSelectItem;
    private CheckboxItem deleteSelectItem;
    //JIRA 1023
    private SelectItem programNameSelectItem;
    private static final int HEALTH_HOME_COLUMN =0;
    private static final int PROGRAM_COLUMN =1;
    private static final int PROGRAM_EFFECTIVE_DATE_COLUMN =2;
    private static final int END_DATE_COLUMN =3;
    private static final int PATIENT_STATUS_COLUMN =4;
    private static final int STATUS_EFFECTIVE_COLUMN =5;
    private static final int PROGRAM_END_REASON_COLUMN =6;
    private static final int DELETE_COLUMN =7;
    private boolean deleteReasonFlag=false;
    private boolean updateMode=false;
    private TextItem hhRequiredTextItem;
    private HandlerRegistration handlerRegistration;
    private String hhRequiredValue="false";
    
    //Spira 6467
    private boolean mppStatusEffectiveDateEditable = false;
    private String mppStatusEffectiveDateMessage="";

    public ManagePatientProgramPanel(PersonDTO thePersonDTO) {
        personDTO = thePersonDTO;
        setFormValidationRulesFromClientAppProperties();
        communityId= ApplicationContextUtils.getLoginResult().getCommunityId();
       
        buildGui();
        progressBarWindow = new ProgressBarWindow("Loading", "Loading...");
        getPatientProgramCallback = new ManagePatientProgramPanel.GetPagePatientProgramCallback();

        gotoPage(1);
        DashBoardApp.restartTimer();
    }

    private void buildGui() {

        ToolStrip toolStrip = createToolStrip();
        addMember(toolStrip);

        setMargin(5);
        setMargin(5);

        LayoutSpacer topSpacer = new LayoutSpacer();
        topSpacer.setHeight(20);
        addMember(topSpacer);

        listGrid = buildListGrid();
        setPadding(5);
        addMember(listGrid);

        LayoutSpacer topSpacer1 = new LayoutSpacer();
        topSpacer1.setHeight(20);
        addMember(topSpacer1);

        pager = new GridPager(0);
        pager.setPageEventHandler(new ManagePatientProgramPanel.PatientProgramsPageEventHandler());
        addMember(pager);
       
        progressBarWindow = new ProgressBarWindow("Saving", "Saving...");
        
    }
     
      private ListGrid buildListGrid() {
	//JIRA 1023
        String[] columnNames = {healthHomeLabel, ManagePatientProgramConstants.PROGRAM,ManagePatientProgramConstants.PROGRAM_EFFECTIVE_DATE,ManagePatientProgramConstants.END_DATE,ManagePatientProgramConstants.PATIENT_STATUS,ManagePatientProgramConstants.STATUS_EFFECTIVE,programEndReasonLabel,ManagePatientProgramConstants.DELETE};

        theListGrid = new ListGrid();

        theListGrid.setWidth100();
        theListGrid.setHeight100();

        theListGrid.setShowAllRecords(true);
        theListGrid.setSelectionType(SelectionStyle.SINGLE);
         
        theListGrid.setCanEdit(true);

        // add fields
          ListGridField[] fields = buildListGridFields(columnNames);
        theListGrid.setFields(fields);

        theListGrid.setEmptyMessage("...");
        theListGrid.setCanResizeFields(true);
        theListGrid.setAutoSaveEdits(false);
        
         
           
     
        return theListGrid;
    }
      private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];

          for (int i = 0; i < columnNames.length; i++) {
              String tempColumnName = columnNames[i];
              ListGridField tempField = buildListGridField(tempColumnName);
              fields[i] = tempField;

              if (i == PROGRAM_COLUMN) {
                  //JIRA 1023
                  fields[i].setCanEdit(false);
                  programNameSelectItem = new SelectItem();
                  programNameSelectItem.setAddUnknownValues(false);
                  LinkedHashMap<String, String> programNameWithCodeMap = FormUtils.buildProgramNameWithCodeMap(ApplicationContextUtils.getProgramNameValue());
                  //programNameSelectItem.setValueMap(programNameWithCodeMap);
                  programNameSelectItem.setValue(FormUtils.EMPTY);

                  //LinkedHashMap<String, String> programNameMap = FormUtils.buildProgramNameMap(ApplicationContextUtils.getProgramNameValue());
                  fields[i].setValueMap(programNameWithCodeMap);
                  fields[i].setAlign(Alignment.CENTER);
                  
                  fields[i].setEditorValueMapFunction(new EditorValueMapFunction() {
                    public Map getEditorValueMap(Map values, ListGridField field, ListGrid grid) {
                        if(healthHomeRequired) {
                            String selectedHh = (String) values.get(healthHomeLabel);
                            //if check needed, as during modify, home health id is not available
                            if(selectedHh.contains(Constants.DELIMITER_SELECT_ITEM)) {
                                LinkedHashMap<Long, List<Long>> programForHealthHomeCodesMap = ApplicationContextUtils.getProgramForHealthHomeCodes();
                                Long hhId = Long.valueOf(selectedHh.split(Constants.DELIMITER_SELECT_ITEM)[1]);
                                List<Long> programCodes = programForHealthHomeCodesMap.get(hhId);
                                LinkedHashMap<String, String> programMap = FormUtils.buildProgramCodeForHealthHomeMap(
                                        programCodes, ApplicationContextUtils.getProgramNameValue());
                                return programMap;                                
                            }
                        }
                        LinkedHashMap<String, String> programNameWithCodeMap = FormUtils.buildProgramNameWithCodeMap(ApplicationContextUtils.getProgramNameValue());
                        return programNameWithCodeMap;
                    }
                  });
                  fields[i].setEditorType(programNameSelectItem);
               
           } 
            else if (i == HEALTH_HOME_COLUMN) {
                fields[i].setCanEdit(false);
                healthHomeSelectItem = new SelectItem();
//                healthHomeSelectItem.setVisible(healthHomeRequired);
                healthHomeSelectItem.setValue(FormUtils.EMPTY);
                fields[i].setAlign(Alignment.CENTER);
                fields[i].setHidden(!healthHomeRequired);
		//JIRA 1023
                LinkedHashMap<String, String> healthHomeMap = FormUtils.buildHealthHomeMap(ApplicationContextUtils.getHealthHomeCodes());
                fields[i].setValueMap(healthHomeMap);                           
                healthHomeSelectItem.addChangedHandler(new ChangedHandler() {
                    public void onChanged(ChangedEvent event) {
                        LinkedHashMap<String, String> programNameWithCodeMap = FormUtils.buildProgramNameWithCodeMap(ApplicationContextUtils.getProgramNameValue());
                        //theListGrid.setValueMap(ManagePatientProgramConstants.PROGRAM, new LinkedHashMap());                        
                        theListGrid.setValueMap(ManagePatientProgramConstants.PROGRAM, programNameWithCodeMap);
                    }
                });
                fields[i].setEditorType(healthHomeSelectItem);
                
            } 
            else if (i ==PROGRAM_EFFECTIVE_DATE_COLUMN) {
                fields[i].setCanEdit(true);
                fields[i].setType(ListGridFieldType.DATE);
                fields[i].setAlign(Alignment.CENTER);
            } 
            else if (i == END_DATE_COLUMN) {
                fields[i].setCanEdit(true);
                fields[i].setType(ListGridFieldType.DATE);
                fields[i].setAlign(Alignment.CENTER);
            }
            else if (i == PATIENT_STATUS_COLUMN) {
                fields[i].setCanEdit(true);
                  SelectItem patientSelectItem = new SelectItem();
                  LinkedHashMap<String, String> patientStatusMap = FormUtils.buildPatientStatusMap(ApplicationContextUtils.getPatientStatusValue());
                  fields[i].setValueMap(patientStatusMap);
                  fields[i].setAlign(Alignment.CENTER);
                  patientSelectItem.addChangedHandler(new ChangedHandler() {
                      public void onChanged(ChangedEvent event) {
                          //calling 'setValueMap()' will force the 'getEditorValueMap' method to be re-evaluated for the End Reason field  
                          theListGrid.setValueMap(programEndReasonLabel, new LinkedHashMap());
                         
                      }
                  });
                  fields[i].setEditorType(patientSelectItem);

              } 
             else if (i == STATUS_EFFECTIVE_COLUMN) {
                fields[i].setCanEdit(mppStatusEffectiveDateEditable);
                fields[i].setShowHover(mppStatusEffectiveDateEditable);
                fields[i].setHoverCustomizer(new HoverCustomizer() {
                    @Override
                    public String hoverHTML(Object value, ListGridRecord record, int rowNum, int colNum) {
                        return mppStatusEffectiveDateMessage;
                    }
                });
                fields[i].setType(ListGridFieldType.DATE);
                DateItem statusEffectiveDateItem = new DateItem();
                statusEffectiveDateItem.setPrompt(mppStatusEffectiveDateMessage);
                fields[i].setEditorProperties(statusEffectiveDateItem);
                fields[i].setAlign(Alignment.CENTER);               
            }
            else if (i ==PROGRAM_END_REASON_COLUMN) {
                fields[i].setCanEdit(true);
                terminationReasonSelectItem = new SelectItem();
                terminationReasonSelectItem.setAddUnknownValues(false);
                terminationReasonSelectItem.setValue(FormUtils.EMPTY);
//                terminationReasonSelectItem.setValueMap(getTerminationReasonCodes());
                fields[i].setAlign(Alignment.CENTER);

                fields[i].setEditorValueMapFunction(new EditorValueMapFunction() {
                    public Map getEditorValueMap(Map values, ListGridField field, ListGrid grid) {

                        final Map<String, String[]> reasonMap = ApplicationContextUtils.getReasonForPatientStatusValue();
                        LinkedHashMap<String, String> patientStatusMap = FormUtils.buildPatientStatusMap(ApplicationContextUtils.getPatientStatusValue());

                        String status = (String) values.get(ManagePatientProgramConstants.PATIENT_STATUS);
                        Map<String, String> valueMap = new HashMap<String, String>();
                        if (status != null) {
                            String str = null;

                            for (String tempKey : patientStatusMap.keySet()) {
                                if (tempKey.equalsIgnoreCase(status)) {
                                    str = patientStatusMap.get(status);
                                }
                            }

                            String[] reasons = reasonMap.get(str);

                            //convert reasons into ValueMap. In this case we simply create a Map with same key -> value since  
                            //stored value is the same as user displayable value  

                            for (int i = 0; i < reasons.length; i++) {
                              String val = reasons[i];
                              valueMap.put(val, val);
                          }
                         }
                            return valueMap;
                      }
                     });
                  fields[i].setEditorType(terminationReasonSelectItem);
              } else if (i == DELETE_COLUMN) {
                  fields[i].setCanEdit(true);
                  fields[i].setType(ListGridFieldType.BOOLEAN);
            
        }
        }
        return fields;
    }
        private ListGridField buildListGridField(String title) {
           
        ListGridField tempField = new ListGridField(title, title);

        tempField.setCanEdit(false);

        return tempField;
    }
         
     protected ToolStrip createToolStrip() {   
         
        addNewProgramButton = new ToolStripButton("Add New Program");
        saveButton = new ToolStripButton("Save");
        cancelButton = new ToolStripButton("Cancel");
        deleteButton = new ToolStripButton("Delete");
     

        ToolStrip toolStrip = new ToolStrip();
        toolStrip.setWidth100();
        toolStrip.addButton(addNewProgramButton);
        toolStrip.addSeparator();
        toolStrip.addButton(saveButton);
        toolStrip.addSeparator();
        toolStrip.addButton(cancelButton);
        toolStrip.addSeparator();
        toolStrip.addButton(deleteButton);
     

        addNewProgramButton.addClickHandler(new ManagePatientProgramPanel.addNewProgramClickHandler());
        saveButton.addClickHandler(new ManagePatientProgramPanel.SaveClickHandler());
        cancelButton.addClickHandler(new ManagePatientProgramPanel.CancelClickHandler());
        deleteButton.addClickHandler(new ManagePatientProgramPanel.DeleteClickHandler());       

        return toolStrip;
    }

      class addNewProgramClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            Map defaultValues = new HashMap();  
            //JIRA 1023
            if(healthHomeRequired) {
                theListGrid.getField(healthHomeLabel).setCanEdit(true);
            }
            theListGrid.getField(ManagePatientProgramConstants.PROGRAM).setCanEdit(true);  
            theListGrid.startEditingNew();
                           
        }
    }
      class DeleteClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            
            long patientId = personDTO.getPatientEnrollment().getPatientId();
            int [] rows = theListGrid.getAllEditRows();
                                      
             ListGridRecord[] records=new ListGridRecord[rows.length];
                theListGrid.saveAllEdits();
            for(int i=0;i<rows.length;i++){
                 records[i] = theListGrid.getRecord(rows[i]); 
             }

            List<PatientProgramDTO> dtos = getPatientPrograms(records, patientId, communityId);
                        
            ProgressBarWindow progressBarWindow = new ProgressBarWindow();
             
            if(deleteReasonFlag && updateMode){
                //JIRA 1023
                //makeFieldsNonEditable();
                DeletePopUpWindow deletePopUpWindow=new DeletePopUpWindow(personDTO,dtos,records,theListGrid);
                deletePopUpWindow.show();
                deleteReasonFlag=false;
        }
           
    }}
      class CancelClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
//            EnrollmentPanel enrollmentPanel = ApplicationContextUtils.getEnrollmentPanel();
//            enrollmentPanel.showModifyPatientPanel(personDTO);
              //JIRA 1023
              makeFieldsNonEditable();
              gotoPage(1);
        }
    }


       class SaveClickHandler implements ClickHandler {

        PatientProgramDTO patientProgramDTO = new PatientProgramDTO();

        @Override
        public void onClick(ClickEvent event) {
           
            long patientId = personDTO.getPatientEnrollment().getPatientId();
            int [] rows = theListGrid.getAllEditRows();
                                      
            ListGridRecord[] records=new ListGridRecord[rows.length];
            theListGrid.saveAllEdits();
            for(int i=0;i<rows.length;i++){
                records[i] = theListGrid.getRecord(rows[i]); 
            }
                        
            List<PatientProgramDTO> dtos = getPatientPrograms(records, patientId, communityId);
                        
            ProgressBarWindow progressBarWindow = new ProgressBarWindow();
            if(deleteReasonFlag && !updateMode){
                
                String msg="You cannot delete the program which is not associated to patient.";
                SC.warn(msg);
                gotoPage(1);
                deleteReasonFlag=false;
             }
             
            else{
                String confirmationMessage = "Save successful.";
                if ((validateListGridRecords(dtos))&& (!dtos.isEmpty())) {

                    progressBarWindow.show();
                    managePatientProgramService.addPatientProgram(dtos, personDTO, new ManagePatientProgramPanel.SaveAsyncCallback(progressBarWindow, confirmationMessage));
    //                gotoPage(1);
                }
                
            }
             
        }
    }

    //JIRA 1023
    private void makeFieldsNonEditable() {
        if(healthHomeRequired) {
            theListGrid.getField(healthHomeLabel).setCanEdit(false);  
        }
        theListGrid.getField(ManagePatientProgramConstants.PROGRAM).setCanEdit(false);        
        //theListGrid.getField(ManagePatientProgramConstants.PROGRAM).setEditorType((SelectItem) null);        
    }
    
    public boolean validateListGridRecords(List<PatientProgramDTO> dtos) {
        boolean validate = true;
        
        for (PatientProgramDTO patientProgramDTOs : dtos) {
            
            long progID = patientProgramDTOs.getProgramId();
            long patientStatus = patientProgramDTOs.getPatientStatus();

            if (progID == 0 || patientProgramDTOs.getPatientStatus() == 0) {
                validate = false;
                String msg = "Program and Patient Status cannot be left blank!";
                SC.say(msg);
            } else if (patientProgramDTOs.getProgramEffectiveDate() != null && patientProgramDTOs.getProgramEndDate() != null && patientProgramDTOs.getProgramEffectiveDate().after(patientProgramDTOs.getProgramEndDate())) {
                validate = false;
                SC.warn("Program Effective Date can not be greater than  Program End Date!");
            } else if (patientProgramDTOs.getProgramEffectiveDate() == null && patientProgramDTOs.getProgramEndDate() != null) {
                validate = false;
                SC.warn("You cannot add Program End Date without Program Effective Date!");
            } else if((progID!=0) && (hhRequiredValue.equalsIgnoreCase("true")) && (healthHomeRequired)&& (patientProgramDTOs.getHealthHome()==null) ){
                validate = false; 
                SC.warn("For selected Program, " +healthHomeLabel+" is required. Please select "+healthHomeLabel); 
              
            } else if (patientProgramDTOs.isDelete()) {
                validate = false;
                SC.warn("If you want to delete a Program, then click on delete");

            }else if((patientStatus!=0) && (patientProgramDTOs.getTerminationReason()==null) ){
                
                LinkedHashMap<Long, String> statusWithTermination=ApplicationContextUtils.getPatientStatusEntities();
                if(statusWithTermination.get(patientStatus).equalsIgnoreCase("true")){
                    validate = false;
                    SC.warn("Please Select "+programEndReasonLabel);
                }   
            }
        }
        return validate;
    }

    public List<PatientProgramDTO> getPatientPrograms(ListGridRecord[] records, long patientId, long communityId) {

        List<PatientProgramDTO> dtos = new ArrayList<PatientProgramDTO>();

        for (ListGridRecord tempRecord : records) {
           
            PatientProgramDTO tempDto = new ManagePatientProgramRecord(tempRecord, patientId, communityId).getPatientProgramDto();
            if(tempDto.isDelete()){
                deleteReasonFlag=true;
                
            }
            if(tempDto.getOperation().equalsIgnoreCase("Update")){
                updateMode=true;
            }
                
            dtos.add(tempDto);
        }
       

        return dtos;
    }

    class SaveAsyncCallback extends BasicPortalAsyncCallback {

        public SaveAsyncCallback(ProgressBarWindow theProgressBarWindow, String confirmationMessage) {
            super(theProgressBarWindow, confirmationMessage);
        }

        @Override
        public void onSuccess(Object t) {
            progressBarWindow.hide();
            SC.say("Save successful.");
            personDTO.setProgramDeleteReason(null);
            makeFieldsNonEditable();
            gotoPage(1);
           
        }

        @Override
        public void onFailure(Throwable exc) {
            progressBarWindow.hide();

          SC.warn(exc.getMessage());
        }
    }
    
      public void gotoPage(final int pageNumber) {

        long patientId = personDTO.getPatientEnrollment().getPatientId();
        managePatientProgramService.getTotalCountOfPrograms(patientId,communityId, new PortalAsyncCallback<Integer>(){

            public void onSuccess(Integer total) {

                if (total > 0) {
                    pager.setTotalCount(total);
                    pager.refreshPageLabels(pageNumber);
                    pager.gotoPage(pageNumber);
                } else {
                    clearListGrid();
                }
            }
        });
    }

    public void clearListGrid() {
        ListGridRecord[] records = new ListGridRecord[0];
        listGrid.setData(records);

        pager.clearPageLabels();
    }
//    /**
//     * Subscribe as a handler for the event bus. Analogous to signing up as a
//     * ClickHandler
//     */
//    private void subscribeToBus() {
//        EventBus eventBus = EventBusUtils.getEventBus();
//
//        DeleteEventHandler handler = new ListAlertEventHandler();
//        handlerRegistration = eventBus.addHandler(DeleteEvent.TYPE, handler);
//    }
    
     class PatientProgramsPageEventHandler implements PageEventHandler {

        public void handlePage(int pageNum, int pageSize) {
            
//            progressBarWindow.show();
            long patientId = personDTO.getPatientEnrollment().getPatientId();
            System.out.println("PatientProgramsPageEventHandler patientId "+patientId);
            managePatientProgramService.getPatientProgramList(patientId, communityId, pageNum, pageSize,getPatientProgramCallback); 
        }
    }
     
//      class ListAlertEventHandler implements DeleteEventHandler {
//
//        public void onAlert(DeleteEvent deleteEvent) {
//            System.out.println("On DeleteEventHandler >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//            gotoPage(1);
//        }
//        }

     class GetPagePatientProgramCallback implements AsyncCallback<SearchResults<PatientProgramDTO>> {

        public void onSuccess(SearchResults<PatientProgramDTO> searchResults) {

            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            List<PatientProgramDTO> thePatientProgram = searchResults.getData();
            int totalCount = searchResults.getTotalCount();
            populate(thePatientProgram, totalCount);
        }

        public void onFailure(Throwable thrwbl) {
            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            SC.say("Error loading programs. Try again.");
        }
    }
     protected void populate(List<PatientProgramDTO> thePrograms, int totalCount) {
      
        if (!thePrograms.isEmpty()) {
            ListGridRecord[] records = ManagePatientProgramUtils.convert(thePrograms);
            populateListGrid(records, totalCount);

            int currentPageNumber = pager.getCurrentPageNumber();
            pager.refreshPageLabels(currentPageNumber);

            pager.handleNavigationButtons(currentPageNumber);
        } else {
            clearListGrid();
        }
    }
    public void populateListGrid(ListGridRecord[] pagedData, int totalCount) {

        // scroll to first row
        listGrid.scrollToRow(1);

        listGrid.setData(pagedData);
        listGrid.markForRedraw();
        pager.setTotalCount(totalCount);
    }

    protected void setFormValidationRulesFromClientAppProperties() {

        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
        healthHomeRequired = props.isHealthHomeRequired();
        healthHomeLabel=props.getHealthHomeLabel();
        programEndReasonLabel=props.getProgramEndReasonLabel();
        //Spira 6467
        mppStatusEffectiveDateEditable = props.getMppStatusEffectiveDateEditable();
        if(mppStatusEffectiveDateEditable) {
            mppStatusEffectiveDateMessage = props.getMppStatusEffectiveDateMessage();
        }
    }

    //JIRA 1023
    /*
    private String[] getHealthHomeCodes() {
        String[] baseCodes = ApplicationContextUtils.getHealthHomeCodes();

        String[] updatedCodes = FormUtils.getCodesWithAddedBlankOption(baseCodes);

        return updatedCodes;
    }
    */
    
    private String[] getTerminationReasonCodes() {
        String[] baseCodes = ApplicationContextUtils.getTerminationReasonCodes();

        String[] updatedCodes = FormUtils.getCodesWithAddedBlankOption(baseCodes);

        return updatedCodes;
    }
    
}
