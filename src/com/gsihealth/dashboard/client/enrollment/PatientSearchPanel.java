package com.gsihealth.dashboard.client.enrollment;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.service.CareteamService;
import com.gsihealth.dashboard.client.service.CareteamServiceAsync;
import com.gsihealth.dashboard.client.service.EnrollmentService;
import com.gsihealth.dashboard.client.service.EnrollmentServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.PropertyUtils;
import com.gsihealth.dashboard.client.util.SearchUtils;
import com.gsihealth.dashboard.common.AccessLevelConstants;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchPatientResults;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Chad Darby
 */
public class PatientSearchPanel extends VLayout {
    private final String PATIENT_COUNT="PatientCount";
    private final String PATIENT_LIST="PatientList"; 
    private SearchResults<PersonDTO> searchResults;
    private static final String CANDIDATES_FOR_ENROLLMENT = "Candidates for Enrollment";
    private static final String ENROLLED_PATIENTS = "Enrolled Patients";
    private static final String PATIENT_SEARCH_MIRTH="mirth";
    private DynamicForm navigationForm;
    private SelectItem navigationSelectItem;
    private PatientSearchForm patientSearchForm;
    private MirthPatientSearchForm mirthPatientSearchForm;
    private PatientSearchResultsPanel patientSearchResultsPanel;
    private Button searchButton;
    private Button clearButton;
    private ProgressBarWindow progressBarWindow;
    private final EnrollmentServiceAsync enrollmentService = GWT.create(EnrollmentService.class);
    private final CareteamServiceAsync careteamService = GWT.create(CareteamService.class);
    private SearchClickHandler searchClickHandler;
    private MirthSearchClickHandler mirthSearchClickHandler;
    private StaticTextItem queueSizeTextItem;
    private String[] programLevelConsentStatusCodesForCandidates = {"Yes", "No", "Unknown"};
    private String[] programLevelConsentStatusCodesForEnrolledPatients = {"Yes"};
    private boolean searchMode;
    private long organizationId;
    private PatientSearchCallback patientSearchCallback;
    private int currentPageNumber;
    private long userId;
    private long communityId;
    private long userAccessLevelId;
    private boolean candidatesEnrollmentDisableIntialLoad;
    private boolean enrolledPatientDisableIntialLoad;
    private int maxPatientSearchResultsSize;
    private boolean checkIntialFlg;
    private CheckboxItem searchMorePatientsCheckBoxItem = new CheckboxItem("searchMorePatientsCheckBoxItem", "<font color='#003168' size = '3'> Search all patients </font>");
    private CheckboxItem searchMorePatientsMirthCheckBoxItem = new CheckboxItem("searchMorePatientsMirthCheckBoxItem", "<font color='#003168' size = '3'> Search all patients </font>");
    
    ClientApplicationProperties props;
    
    public PatientSearchPanel() {

        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        userAccessLevelId = loginResult.getAccessLevelId();
        organizationId = loginResult.getOrganizationId();
        userId = loginResult.getUserId();
        communityId = loginResult.getCommunityId();
        
        props = ApplicationContextUtils.getClientApplicationProperties();
        
        setFormValidationRulesFromClientAppProperties();

        // get value of max patient search results
        ClientApplicationProperties clientApplicationProperties = ApplicationContextUtils.getClientApplicationProperties();
        maxPatientSearchResultsSize = clientApplicationProperties.getMaxPatientSearchResultsSize();

        buildGui();
        DashBoardApp.restartTimer();
        
    }
    public SearchResults<PersonDTO> getSearchResults() {
        return searchResults;
    }

    public void setSearchResults(SearchResults<PersonDTO> searchResults) {
        this.searchResults = searchResults;
    }
    private void buildGui() {

        checkIntialFlg = false;
        searchClickHandler = new SearchClickHandler();
        mirthSearchClickHandler = new MirthSearchClickHandler();
        ClientApplicationProperties clientApplicationProperties = ApplicationContextUtils.getClientApplicationProperties();
        String mpiSearch = clientApplicationProperties.getMirthSearchVisible();
//        navigationForm = buildNavigationForm();
        patientSearchForm = new PatientSearchForm(searchClickHandler, searchMorePatientsCheckBoxItem);
        mirthPatientSearchForm = new MirthPatientSearchForm(mirthSearchClickHandler, searchMorePatientsMirthCheckBoxItem);
        HLayout hLayout = new HLayout();
        hLayout.setWidth100();

        VLayout sideSearchLayout = buildSideSearchLayout();
        hLayout.addMember(sideSearchLayout);
        
             if (mpiSearch.equals("mdm")){
                    patientSearchResultsPanel = new PatientSearchResultsPanel(patientSearchForm);
            }else if (mpiSearch.equals("mirth") || mpiSearch.equals("demog")) {
                    patientSearchResultsPanel = new PatientSearchResultsPanel(mirthPatientSearchForm);
            }
                    patientSearchResultsPanel.setPageEventHandler(new ResultsPageEventHandler());
             
        hLayout.addMember(patientSearchResultsPanel);

        addMember(hLayout);
        this.setOverflow(Overflow.AUTO);

//        navigationSelectItem.addChangedHandler(new NavigationChangedHandler());

        progressBarWindow = new ProgressBarWindow();
        patientSearchCallback = new PatientSearchCallback(progressBarWindow);

        // Get Patients from the server    
        if (!candidatesEnrollmentDisableIntialLoad) {
            gotoNextPage(1, null);
        } else {
//            setIntialLoadQueueSize();
            patientSearchResultsPanel.setEnrolledMode1(false, false);

        }
    }

    /**
     * searches for enrolled or candidates based on flags - FIXME obsolete
     *
     * @param candidatesEnrollmentBlnFlag
     * @param enrolledPatientBlnFlag
     */
//    public void setIntialLoadQueueSize() {
//
//        TotalCountsQueueSizeCallback totalCountsQueueSizeCallback
//                = new TotalCountsQueueSizeCallback();
//        totalCountsQueueSizeCallback.attempt();
//    }

    protected final void setFormValidationRulesFromClientAppProperties() {

        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
        candidatesEnrollmentDisableIntialLoad = props.isCandidatesEnrollmentDisableIntialLoad();
        enrolledPatientDisableIntialLoad = props.isEnrolledPatientDisableIntialLoad();
    }

    private VLayout buildSideSearchLayout() {
        VLayout sideSearchLayout = new VLayout();
        ClientApplicationProperties clientApplicationProperties = ApplicationContextUtils.getClientApplicationProperties();
        String mpiSearch = clientApplicationProperties.getMirthSearchVisible();
        sideSearchLayout.setHeight(200);
        sideSearchLayout.setWidth(250);
        sideSearchLayout.setShowResizeBar(true);

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(10);
        sideSearchLayout.addMember(spacer);

//        sideSearchLayout.addMember(navigationForm);
        if (mpiSearch.equals("mdm")) {
            LayoutSpacer upperSpacer = new LayoutSpacer();
            upperSpacer.setHeight(10);
            Layout searchButtonBar = buildSearchButtonBar();
            sideSearchLayout.addMember(upperSpacer);

            sideSearchLayout.addMember(searchButtonBar);

            LayoutSpacer upperSpacerShowAllPatientsBar = new LayoutSpacer();
            upperSpacerShowAllPatientsBar.setHeight(10);
            sideSearchLayout.addMember(upperSpacerShowAllPatientsBar);

            Layout showAllPatientsBar = buildShowAllPatientsBar();
            sideSearchLayout.addMember(showAllPatientsBar);

            LayoutSpacer lowerSpacer = new LayoutSpacer();
            lowerSpacer.setHeight(10);
            sideSearchLayout.addMember(lowerSpacer);

            sideSearchLayout.addMember(patientSearchForm);

            Layout searchBottomButtonBar = buildBottomSearchButtonBar();
            sideSearchLayout.addMember(searchBottomButtonBar);
        }

        //mirth
        else if (mpiSearch.equals("mirth") || mpiSearch.equals("demog")) {
            LayoutSpacer upperSpacer1 = new LayoutSpacer();
            upperSpacer1.setHeight(10);
            Layout searchButtonBar1 = buildMirthSearchButtonBar();
            sideSearchLayout.addMember(upperSpacer1);

            sideSearchLayout.addMember(searchButtonBar1);

            LayoutSpacer upperSpacerShowAllPatientsBar1 = new LayoutSpacer();
            upperSpacerShowAllPatientsBar1.setHeight(10);
            sideSearchLayout.addMember(upperSpacerShowAllPatientsBar1);

            Layout showAllPatientsBar1 = buildMirthShowAllPatientsBar();
            sideSearchLayout.addMember(showAllPatientsBar1);

            LayoutSpacer lowerSpacer1 = new LayoutSpacer();
            lowerSpacer1.setHeight(10);
            sideSearchLayout.addMember(lowerSpacer1);

            sideSearchLayout.addMember(mirthPatientSearchForm);

            Layout searchBottomButtonBar1 = buildMirthBottomSearchButtonBar();
            sideSearchLayout.addMember(searchBottomButtonBar1);
        }
        return sideSearchLayout;
    }

    public void gotoNextPage(final int pageNumber, final SearchCriteria searchCriteria) {

        progressBarWindow.setVisible(true);
        currentPageNumber = pageNumber;

        PatientsTotalCountCallback patientsTotalCountCallback = new PatientsTotalCountCallback(searchCriteria);
        patientsTotalCountCallback.attempt();
    }

    /**
     * Helper method to build button bar
     *
     * @return
     */
    private Layout buildSearchButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        buttonLayout.setWidth100();
        buttonLayout.setAlign(Alignment.CENTER);

        searchButton = new Button(props.getDemographicSearchButton());
        searchButton.setID("Search");
        clearButton = new Button(props.getDemographicClearButton());
        clearButton.setID("Clear");
        buttonLayout.addMember(searchButton);
        buttonLayout.addMember(clearButton);

        searchButton.addClickHandler(searchClickHandler);
        clearButton.addClickHandler(new ClearClickHandler());

        return buttonLayout;
    }

    //mirth
    private Layout buildMirthSearchButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        buttonLayout.setWidth100();
        buttonLayout.setAlign(Alignment.CENTER);

        searchButton = new Button(props.getDemographicSearchButton());
        searchButton.setID("Search");
        clearButton = new Button(props.getDemographicClearButton());
        clearButton.setID("Clear");
        buttonLayout.addMember(searchButton);
        buttonLayout.addMember(clearButton);

        searchButton.addClickHandler(mirthSearchClickHandler);
        clearButton.addClickHandler(new MirthClearClickHandler());

        return buttonLayout;
    }

    /**
     * Helper method to build show all patients bar
     *
     * @return
     */
    private Layout buildShowAllPatientsBar() {
        
        DynamicForm searchAllPatientDynamicForm = new DynamicForm();
        HLayout showAllPatientsLayout = new HLayout(0);
        showAllPatientsLayout.setWidth100();
        showAllPatientsLayout.setAlign(Alignment.LEFT);
  
        Img img = new Img("mypatientlist/info.png");
        img.setHeight(22);
        img.setWidth(22);
        img.setPrompt(ApplicationContextUtils.getClientApplicationProperties().getSearchForAdditionalPatientsInformationIconText());
        img.setHoverWidth(350);
        showAllPatientsLayout.addMember(img);
        searchAllPatientDynamicForm.setFields(searchMorePatientsCheckBoxItem);
        showAllPatientsLayout.addMember(searchAllPatientDynamicForm);

        return showAllPatientsLayout;
    }
    private Layout buildMirthShowAllPatientsBar() {
        
        DynamicForm searchAllPatientDynamicForm = new DynamicForm();
        HLayout showAllPatientsLayout = new HLayout(0);
        showAllPatientsLayout.setWidth100();
        showAllPatientsLayout.setAlign(Alignment.LEFT);
  
        Img img = new Img("mypatientlist/info.png");
        img.setHeight(22);
        img.setWidth(22);
        img.setPrompt(ApplicationContextUtils.getClientApplicationProperties().getSearchForAdditionalPatientsInformationIconText());
        img.setHoverWidth(350);
        showAllPatientsLayout.addMember(img);
        searchAllPatientDynamicForm.setFields(searchMorePatientsMirthCheckBoxItem);
        showAllPatientsLayout.addMember(searchAllPatientDynamicForm);

        return showAllPatientsLayout;
    }
    /**
     * buildBottomSearchButtonBar
     * @return 
     */
     private Layout buildBottomSearchButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        buttonLayout.setWidth100();
        buttonLayout.setAlign(Alignment.CENTER);

        searchButton = new Button(props.getDemographicSearchButton());
        searchButton.setID("SearchButton");
        clearButton = new Button(props.getDemographicClearButton());
        clearButton.setID("ClearButton");
        buttonLayout.addMember(searchButton);
        buttonLayout.addMember(clearButton);

        searchButton.addClickHandler(searchClickHandler);
        clearButton.addClickHandler(new ClearClickHandler());

        return buttonLayout;
    }
    //mirth

    private Layout buildMirthBottomSearchButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        buttonLayout.setWidth100();
        buttonLayout.setAlign(Alignment.CENTER);

        searchButton = new Button("Search");
        searchButton.setID("SearchButton");
        clearButton = new Button("Clear");
        clearButton.setID("ClearButton");
        buttonLayout.addMember(searchButton);
        buttonLayout.addMember(clearButton);

        searchButton.addClickHandler(mirthSearchClickHandler);
        clearButton.addClickHandler(new MirthClearClickHandler());

        return buttonLayout;
    }

//    private DynamicForm buildNavigationForm() {
//
//        navigationForm = new DynamicForm();
//        navigationForm.setNumCols(4);
//        navigationSelectItem = new SelectItem();
//        navigationSelectItem.setShowTitle(false);
//        navigationSelectItem.setValueMap(CANDIDATES_FOR_ENROLLMENT, ENROLLED_PATIENTS);
//        navigationSelectItem.setDefaultToFirstOption(true);
//
//        queueSizeTextItem = new StaticTextItem("queueSize");
//        queueSizeTextItem.setShowTitle(false);
//        queueSizeTextItem.setWidth(100);
//        navigationForm.setFields(navigationSelectItem, queueSizeTextItem);
//
//        clearQueueSize();
//
//        return navigationForm;
//    }
//
//    public void setQueueSize(int size) {
//
//        queueSizeTextItem.setValue("Queue size: " + size);
//    }
//
//    public void clearQueueSize() {
//        queueSizeTextItem.setValue("Queue size:");
//    }

    class SearchClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            DashBoardApp.restartTimer();
            searchMode = true;
            if(patientSearchForm.isSearchMorePatientsCheckBoxItemSelected()){
                
            }
            
            if(!patientSearchForm.validateForExtraPatientSearch()){
                patientSearchForm.validateFirstNameAndLastName();
              SC.warn("Enter first name and last name for extra search.");
            }else {
                if(patientSearchForm.validate()){
                    SearchCriteria searchCriteria = patientSearchForm.getSearchCriteria();

                    boolean enrolledMode = false;
                    patientSearchResultsPanel.setEnrolledMode1(enrolledMode, true);
                    gotoNextPage(1, searchCriteria);
                }
            }
        }
    }

    class MirthSearchClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            DashBoardApp.restartTimer();
            searchMode = true;
            if (mirthPatientSearchForm.isSearchMorePatientsCheckBoxItemSelected()) {

            }

            if (!mirthPatientSearchForm.validateForExtraPatientSearch()) {
                mirthPatientSearchForm.validateFirstNameAndLastName();
                SC.warn("Enter first name and last name for extra search.");
            } else if (mirthPatientSearchForm.validate()) {
                SearchCriteria searchCriteria = mirthPatientSearchForm.getSearchCriteria();
                
                boolean enrolledMode = false;
                patientSearchResultsPanel.setEnrolledMode1(enrolledMode, true);
                gotoNextPage(1, searchCriteria);
            }
        }
    }

    class ClearClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            searchMode = false;
            DashBoardApp.restartTimer();
            patientSearchForm.clearSearchValues();
        }
    }
    class MirthClearClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            searchMode = false;
            DashBoardApp.restartTimer();
            mirthPatientSearchForm.clearSearchValues();
        }
    }
    
    class PatientSearchCallback extends PortalAsyncCallback<SearchResults<PersonDTO>> {

        public PatientSearchCallback(ProgressBarWindow theProgressBarWindow) {
            super(theProgressBarWindow);

        }

        @Override
        public void onSuccess(SearchResults<PersonDTO> searchResults) {

            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            if (!searchMode) {
//                setQueueSize(searchResults.getTotalCount());
                if (searchResults.getTotalCount() > 0) {
                    checkIntialFlg = true;
                } else {
                    SC.warn("No patient records found.");
                }
            }

            List<PersonDTO> patients = searchResults.getData();
            int totalCount = searchResults.getTotalCount();

            if (!patients.isEmpty()) {
                // convert to list grid records
                ListGridRecord[] data = new ListGridRecord[patients.size()];

                for (int index = 0; index < patients.size(); index++) {
                    PersonDTO tempPerson = patients.get(index);
                    data[index] = PropertyUtils.convert(index, tempPerson);
                }

                patientSearchResultsPanel.populateListGrid(data, patients, totalCount);
            } else {
                patientSearchResultsPanel.clearListGrid();

                if (searchMode) {
                    SC.warn("Search did not return any results. Please modify your search criteria.");
                }
            }

            // if search results exceed max, only show warning message on first page
            if (searchMode) {

                int thePageNumber = patientSearchResultsPanel.getCurrentPageNumber();
                boolean isFirstPage = thePageNumber == 1;
                boolean mdmMaxExceeded = searchResults.isMdmMaxExceeded();

                if (mdmMaxExceeded && isFirstPage) {
                    String message = SearchUtils.getMaxPatientSearchResultsWarningMessage(maxPatientSearchResultsSize);
                    SC.warn(message);
                }
            }
        }
    }

//    class NavigationChangedHandler implements ChangedHandler {
//
//        public void onChanged(ChangedEvent event) {
//            DashBoardApp.restartTimer();
//            clearQueueSize();
//            boolean enrolledMode = false;
//
//            patientSearchResultsPanel.setEnrolledMode1(enrolledMode, false);
//            patientSearchForm.clearValues();
//            patientSearchForm.clearEnrollmentStatus();
//            patientSearchForm.setEnrolledMode(enrolledMode);
//
//            patientSearchForm.getProgramNameSelectItem().setValueMap(FormUtils.buildProgramNameMap(ApplicationContextUtils.getProgramNameValue()));
//            patientSearchForm.setVisibleOrganizationSection(enrolledMode);
//            patientSearchForm.clearValueProgramLevelConsentStatus();
//            checkIntialFlg = false;
//
//            // the default is candidate
//            String[] enrollmentStatusCodes = PatientSearchForm.STATUS_CODES_FOR_CANDIDATES;
//            String[] progLvlStatusCodes = programLevelConsentStatusCodesForCandidates;
//            boolean displayResultsOnLoad = !enrolledMode && candidatesEnrollmentDisableIntialLoad;
//            
//            if (enrolledMode) {
//                enrollmentStatusCodes = PatientSearchForm.STATUS_CODES_FOR_ENROLLED_PATIENTS;
//                progLvlStatusCodes = programLevelConsentStatusCodesForEnrolledPatients;
//                patientSearchForm.selectProgramLevelConsentStatus("Yes");
//                displayResultsOnLoad = enrolledPatientDisableIntialLoad;
//            }
//
//            patientSearchForm.setEnrollmentStatusValues(enrollmentStatusCodes);
//            patientSearchResultsPanel.setColumnsByCandidateOrEnrolled(enrolledMode);
//            patientSearchForm.setProgramLevelConsentStatusValues(progLvlStatusCodes);
//
//            
//            if (displayResultsOnLoad) {
//                setIntialLoadQueueSize();
//            } else {
//                gotoNextPage(1, null);
//            }
//        }
//    }

    class PatientsTotalCountCallback extends RetryActionCallback<SearchPatientResults> {

        private SearchCriteria searchCriteria;
        
        PatientsTotalCountCallback() {
            this(null);
        }

        PatientsTotalCountCallback(SearchCriteria searchCriteria) {
            this.searchCriteria = searchCriteria;
        }

        @Override
        public void attempt() {
          
            //TODO clean up code 
            Long userOrg = organizationId;
            if (userAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
                userOrg = null;
            }
          
            enrollmentService.countPatients(communityId, searchCriteria, false, userOrg, userId, this);
        }

        @Override
        public void onSuccess(SearchPatientResults totalPatients) {
            try {
                onCapture(totalPatients);
            } catch (Exception error) {
                onFailure(error);
            }
        }

        @Override
        public void onCapture(SearchPatientResults totalPatients) {
            
            int patientCount= totalPatients.getPatientCount();
            setSearchResults((SearchResults<PersonDTO>) totalPatients.getSearchResults());
            patientSearchResultsPanel.setTotalCount(patientCount);
            patientSearchResultsPanel.gotoPage(currentPageNumber);
               
            if (patientCount == 0) {
                progressBarWindow.setVisible(false);
                patientSearchResultsPanel.clearListGrid();

                if (searchMode) {
                    SC.warn(Constants.SEARCH_DID_NOT_RETURN_ANY_RESULTS);
                }
            }
        }

        @Override
        public void onFailure(Throwable exc) {
            progressBarWindow.setVisible(false);
            super.onFailure(exc);
        }

    }

    class ResultsPageEventHandler implements PageEventHandler {

        @Override
        public void handlePage(int pageNum, int pageSize) {
            progressBarWindow.show();
            boolean enrolledMode =false;
            ClientApplicationProperties clientApplicationProperties = ApplicationContextUtils.getClientApplicationProperties();
            String mpiSearch = clientApplicationProperties.getMirthSearchVisible();
            
            SearchCriteria searchCriteria = null;
            
            if (mpiSearch.equals("mdm")){
                    searchCriteria= patientSearchForm.getSearchCriteria();
            }
            else if(mpiSearch.equals("mirth") || mpiSearch.equals("demog")){
                    searchCriteria= mirthPatientSearchForm.getSearchCriteria();
            }
            Long userOrg = organizationId;
            
            if (userAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
                userOrg = null;
            }
            if(getSearchResults() != null){
                enrollmentService.findPatientsWithNoService(getSearchResults(), patientSearchCallback);
            } else {
                enrollmentService.findPatients(communityId, searchCriteria, enrolledMode,
                    userOrg, pageNum, pageSize, userId, patientSearchCallback);
            }
        }
    }

    protected boolean isEnrolledMode() {

        String navType = navigationSelectItem.getValueAsString();
        return navType.equals(ENROLLED_PATIENTS);
    }

    class TotalCountsQueueSizeCallback extends RetryActionCallback<SearchPatientResults> {

        TotalCountsQueueSizeCallback() {
        }

        @Override
        public void attempt() {
           
            //TODO clean up code 
            Long userOrg = organizationId;
            if (userAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
                userOrg = null;
            }
           
            enrollmentService.countPatients(communityId, null,false , userOrg, userId, this);
        }

        @Override
        public void onCapture(SearchPatientResults totalPatients) {
             
//            setQueueSize(total);
            patientSearchResultsPanel.setTotalCount(0);
            patientSearchResultsPanel.populateListGrid1(0);
            patientSearchResultsPanel.clearListGrid();
            patientSearchResultsPanel.gotoPage(currentPageNumber);

            if (totalPatients.getPatientCount()> 0) {
                checkIntialFlg = true;
            } else {
                SC.warn("No patient records found.");
            }
        }

        @Override
        public void onFailure(Throwable exc) {
            progressBarWindow.setVisible(false);
            super.onFailure(exc);
        }
    }

}
