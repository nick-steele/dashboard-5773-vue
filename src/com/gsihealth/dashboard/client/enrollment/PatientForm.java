package com.gsihealth.dashboard.client.enrollment;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.gsihealth.dashboard.client.admintools.user.UpdateUserContextEvent;
import com.gsihealth.dashboard.client.admintools.user.UpdateUserContextEventHandler;
import com.gsihealth.dashboard.client.common.FormUtils;
import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.client.common.PatientOtherIdsWindow;
import com.gsihealth.dashboard.client.common.PersonInfo;
import com.gsihealth.dashboard.client.eventbus.EventBusUtils;
import com.gsihealth.dashboard.client.managepatientprogram.ManagePatientProgramWindow;
import com.gsihealth.dashboard.client.service.EnrollmentService;
import com.gsihealth.dashboard.client.service.EnrollmentServiceAsync;
import com.gsihealth.dashboard.client.service.ReferenceDataService;
import com.gsihealth.dashboard.client.service.ReferenceDataServiceAsync;
import com.gsihealth.dashboard.client.util.*;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.common.dto.UserFormReferenceData;
import com.gsihealth.dashboard.common.dto.enrollment.PatientFormReferenceData;
import com.gsihealth.dashboard.common.util.SsnUtils;
import com.gsihealth.dashboard.entity.dto.*;
import com.gsihealth.dashboard.entity.dto.insurance.PayerClassDTO;
import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.*;
import com.smartgwt.client.widgets.form.fields.events.*;
import com.smartgwt.client.widgets.form.validator.DateRangeValidator;
import com.smartgwt.client.widgets.form.validator.DoesntContainValidator;
import com.smartgwt.client.widgets.form.validator.LengthRangeValidator;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.grid.ListGridRecord;

import java.util.*;

/**
 *
 * @author Chad Darby
 */
public class PatientForm extends BaseFormPatient implements UpdateUserContextEventHandler{

    public static final String DATE_OF_BIRTH_DATE_ITEM = "dateOfBirthDateItem";
    public static final int NAME_LENGTH_MAX = 100;
    public static final int NAME_LENGTH_MIN = 0;
    public static final int FORM_WIDTH = 910;
    private static final String CONSENT_AMPM_NAME = "consent_ampm";
    private static final String CONSENT_DATE_ITEM_NAME = "consent_date_item";
    private static final String CONSENT_TIME_ITEM_NAME = "consent_time_item";
    private static final String CONSENT_VERSION_NAME = "consent_version";
    private static final String CONSENT_OBTAINED_BY = "consent_obtained_by";
    private static final String OBTAINED_BY = "obtained_by";
    private static final String CONSENTER = "consenter";
    private static final String ADD_ASSIGN_OUT_OF_NETWORK_PROVIDERS = "Add/Assign Out of Network Providers";
    private  String manageHealthHomeConsent ;
    private  String managePatientPrograms;
    public static final long EMPTY_RELATIONSHIP_CODE = -1L;
    private DynamicForm patientDemographicsForm;
    private DynamicForm addressForm;
    private DynamicForm emergencyContactForm;
    private StaticTextItem patientIDStaticTextItem, primaryInsuranceTextItem, secondaryInsuranceTextItem;
    private TextItem firstNameTextItem, fNameEmergencyTextItem, mNameEmergencyTextItem, lNameEmergencyTextItem;
    private TextItem middleNameTextItem;
    private TextItem lastNameTextItem;
    private DateItem dobDateItem;
    private SelectItem genderSelectItem;
    private TextItem ssnTextItem;
    private TextItem telephoneTextItem, telephoneEmergencyTextItem;
    private SelectItem stateSelectItem;
    private TextItem address1TextItem;
    private TextItem address2TextItem;
    private TextItem cityTextItem;
    private TextItem zipCodeTextItem;
    private SelectItem programLevelSelectItem;
    private SelectItem programNameSelectItem;
    private DateItem consentDateItem,dateItem;
   // private TextItem consentTimeTextItem, timeTextItem;
    //private TextItem timeTextItem;
    //private SelectItem consentAMPM,consentAMPMItem;
   // private SelectItem consentAMPMItem;
    private SelectItem reasonForRefusalSelectItem;
    private SelectItem reasonForInactivationSelectItem;
    private SelectItem enrollmentStatusSelectItem;
    private SelectItem organizationSelectItem;
    private SelectItem countySelectItem;
    private TextItem patientMrnTextItem, emailTextItem, primaryMedicaidIdTextItem, secondaryMedicaidIdTextItem, emergencyEmailTextItem;
    private RegExpValidator alphaNumberRegExpValidator;
    private RegExpValidator alphaOnlyRegExpValidator;
    private BasePatientPanel basePatientPanel;
    private DynamicForm insuranceForm;
    private SelectItem consentObtainedBySelectItem, obtainedBySelectItem;
    private SelectItem consenterSelectItem;
    private EnrollmentServiceAsync enrollmentService = GWT.create(EnrollmentService.class);
    private PersonDTO originalPerson;
    private PersonDTO person;
    private LinkedHashMap<String, String> usersMap;
    private String[] CONSENT_STATUSES = {"Yes", "No"};
    private String[] PLCS_NO_ENROLLMENT_STATUSES = {"Inactive"};
    private String[] PLCS_YES_ENROLLMENT_STATUSES = {"Pending", "Inactive", "Enrolled", "Assigned"};
    private long careteamIdOld = Long.MIN_VALUE;
    private boolean careteamIdOldIsNotSet = true;
    private LinkItem manageNonHealthHomeLinkItem;
    private ButtonItem manageHealthHomeButtonItem;
    private boolean updateMode;
    private String oldProgramLevelConsentStatus;
    private static long CARETEAM_NOT_SET = -1;
    private long communityCareteamId = CARETEAM_NOT_SET;
    private static final String ALL_ONES_SSN = "111111111";
    private static final String ALL_ZEROS_SSN = "000000000";
    private static final String ALL_NINES_SSN = "999999999";
    private String realSsnValue;
    private String enteredValue;
    private boolean validSsnEntered = true;
    private DoesntContainValidator allOnesValidator;
    private DoesntContainValidator allZerosValidator;
    private DoesntContainValidator allNinesValidator;
    private LengthRangeValidator ssnLengthValidator;
    private SelectItem ethnicitySelectItem;
    private SelectItem raceSelectItem;
    private SelectItem languageSelectItem;
    private SelectItem primaryPayerClassSelectItem;
    private SelectItem secondaryPayerClassSelectItem;
    private ComboBoxItem primaryPayerPlanComboBoxItem;
    private ComboBoxItem secondaryPayerPlanComboBoxItem;
    private DateItem secondaryEffectiveDateItem, primaryEffectiveDateItem, programEffectiveDateItem, programEndDateItem;
    private static final String MIN_DATE = "1/1/1900";
    private String[] enrollmentReasonsForInactivation;
    private boolean enrollmentInsuranceEffectiveDateValidationEnabled;
    private boolean enrollmentInsuranceMedicaidIdValidationEnabled;
    private String disenrollmentCodeTitle;
    private String plcsTitle;
    private SelectItem relationshipSelectItem;
    private ReferenceDataServiceAsync referenceDataService = GWT.create(ReferenceDataService.class);
    private DateTimeFormat dateFormatter;
    private String healthHomeLabel;
    private SelectItem healthHomeLabelSelectItem;
    private boolean programNameRequired;
    // private String[] programNameCodes;
    private LinkItem patientOtherIdsLinkItem;
    private boolean healthHomeRequired;
    boolean programNameBln = false;
    private long healthHomeId;
    private String programLevelValue;
    private Date consentDateTime;
    private boolean consentNone;
    private long consentingUser;
    private StaticTextItem patientUserNameStaticTextItem,enrollmentChangeDateStaticTextItem;
    private String patientToCareTeamLabel;
    private boolean patientEngagementEnable;
    private CheckboxItem addPatientSelectItem;
    private boolean uncheckedFlag = false;
    private boolean enableCareteamPanel = false;
    private long communityId;
    private boolean showManageHealthHomeLink;
    private ButtonItem managePatientProgramButtonItem;
    private PatientInfo patientInfo;
    private TextItem acuityScoreTextItem;
    private SelectItem informationSharingSelectItem;
    private static final String[] INFORMATION_SHARING_MINOR_CODES = {"Yes","N/A"};
    private boolean childrensHealthHome = false;
    private boolean minorFlag = false;
    private boolean canConsentEnrollMinors = false;
    private boolean disableMinorDirectMessg = false;
    private Date consentDate2 = new java.util.Date();
    private DataSource userDataSource;
    private DataSource consenterDataSource;
    private DataSource minorUserDataSource;
    private PatientFormReferenceData patientFormReferenceData;
    private UserFormReferenceData userFormReferenceData;
    private DataSource orgDataSource;
    private UserDTO userDTO;
    private HandlerRegistration handlerRegistration;
    private boolean countyEnabled;
    private boolean countyRequired;

    public PatientForm(BasePatientPanel theBasePatientPanel) {

        communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
        canConsentEnrollMinors=ApplicationContextUtils.getLoginResult().isCanConsentEnrollMinors();

        countyEnabled = ApplicationContextUtils.getLoginResult().isCountyEnabled();
        countyRequired = ApplicationContextUtils.getLoginResult().isCountyRequired();

        dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");

        updateMode = false;
        basePatientPanel = theBasePatientPanel;

        setFormValidationRulesFromClientAppProperties();

        buildGui();


        loadPatientFormReferenceData(communityId);
    }

 public PatientForm(PersonDTO thePerson) {
        updateMode = true;
        communityId=ApplicationContextUtils.getLoginResult().getCommunityId();

        countyEnabled = ApplicationContextUtils.getLoginResult().isCountyEnabled();
        countyRequired = ApplicationContextUtils.getLoginResult().isCountyRequired();

        dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");


        setFormValidationRulesFromClientAppProperties();

        buildGui();


        loadPatientFormReferenceData(thePerson,communityId);
    //    Window.alert("Reference data w/ person ");

    }

    private long getHealthHomeId(PersonDTO thePerson) {
        long healthHomeId = 0;
        ProgramHealthHomeDTO programHealthHomeDTO = thePerson.getPatientEnrollment().getProgramHealthHome();
        if (programHealthHomeDTO != null) {
            healthHomeId = programHealthHomeDTO.getId();
    }
        return healthHomeId;
    }




    protected void setFormValidationRulesFromClientAppProperties() {

        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();

        enrollmentInsuranceEffectiveDateValidationEnabled = props.isEnrollmentInsuranceEffectiveDateValidationEnabled();
        enrollmentInsuranceMedicaidIdValidationEnabled = props.isEnrollmentInsuranceMedicaidIdValidationEnabled();
        healthHomeLabel = props.getHealthHomeLabel();
        programNameRequired = props.isProgramNameRequired();
        healthHomeRequired = props.isHealthHomeRequired();

        patientToCareTeamLabel = props.getPatientToCareTeamLabel();
        patientEngagementEnable = props.isPatientEngagementEnable();
        showManageHealthHomeLink=props.isManageConsent();
        childrensHealthHome=props.isChildrensHealthHome();
        disableMinorDirectMessg=props.isDisableMinorDirectMessaging();
        manageHealthHomeConsent=props.getHealthHomeProviderButtonText();
        managePatientPrograms=props.getPatientProgramButtonText();
//        requireMedicaidID = props.isRequireMedicaidID();
//        requireMedicareID = props.isRequireMedicareID();
    }

    protected void setSsnMaskAndValidators() {
        ssnTextItem.setMask("###-##-####");
        ssnTextItem.setMaskOverwriteMode(true);
        ssnTextItem.setValidators(ssnLengthValidator, allOnesValidator, allZerosValidator, allNinesValidator);
    }

    protected void removeSsnMaskAndValidators() {

        ssnTextItem.setValidators((DoesntContainValidator[]) null);
        ssnTextItem.setMaskOverwriteMode(false);
        ssnTextItem.setMask("CCCCCCC####");
    }

    private void clearSsnValues() {
        realSsnValue = null;
    }

    protected void buildGui() {

        setMargin(10);
        setWidth(950);

        // getting dienrollment title and PLCS title from prop file.
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        disenrollmentCodeTitle = loginResult.getDisenrollmentCodeTitle();
        plcsTitle = loginResult.getPlcsTitle();
        enrollmentReasonsForInactivation = ApplicationContextUtils.getDisenrollmentReasons();

        allOnesValidator = new DoesntContainValidator();
        allOnesValidator.setSubstring(ALL_ONES_SSN);

        allZerosValidator = new DoesntContainValidator();
        allZerosValidator.setSubstring(ALL_ZEROS_SSN);

        allNinesValidator = new DoesntContainValidator();
        allNinesValidator.setSubstring(ALL_NINES_SSN);

        alphaNumberRegExpValidator = ValidationUtils.getAlphaNumberRegExpValidator();
        alphaOnlyRegExpValidator = ValidationUtils.getAlphaOnlyRegExpValidator();

        patientDemographicsForm = getPatientDemographicForm();
        addMember(patientDemographicsForm);

        addressForm = getAddressForm();
        addMember(addressForm);

        // Emergency Contact Form
        emergencyContactForm = getEmergencyContactForm();
        addMember(emergencyContactForm);


        insuranceForm = buildInsuranceForm();
        addMember(insuranceForm);
        
        subscribeToBus();
    }
    
     /**
     * Subscribe as a handler for the event bus. Analogous to signing up as a
     * ClickHandler
     */
    private void subscribeToBus() {
        EventBus eventBus = EventBusUtils.getEventBus();

        handlerRegistration = eventBus.addHandler(UpdateUserContextEvent.TYPE, this);
    }

    @Override
    public void destroy() {
        super.destroy();

        // remove our handler
        if (handlerRegistration != null) {
            handlerRegistration.removeHandler();
        }
    }
    
    private DynamicForm getPatientDemographicForm() {

        DynamicForm theForm = new DynamicForm();
        theForm.setNumCols(6);
        theForm.setIsGroup(true);
        theForm.setGroupTitle("Patient Demographics");
        setFormWidth(theForm);
        theForm.setRequiredTitlePrefix(FormUtils.REQUIRED_TITLE_PREFIX);
        theForm.setBrowserSpellCheck(false);

        RegExpValidator alphaHGAARegExValidator = ValidationUtils.getAlphaHGAARegExValidator();

        LengthRangeValidator lengthRangeValidator = new LengthRangeValidator();
        lengthRangeValidator.setMax(NAME_LENGTH_MAX);
        lengthRangeValidator.setMin(NAME_LENGTH_MIN);

        patientIDStaticTextItem = new StaticTextItem("patientIDStaticTextItem", "Patient ID");
        patientIDStaticTextItem.setWrapTitle(false);
        patientIDStaticTextItem.setEndRow(false);
        patientIDStaticTextItem.setVisible(updateMode);

        patientOtherIdsLinkItem = new LinkItem();
        patientOtherIdsLinkItem.setValue("Patient other IDs");
        patientOtherIdsLinkItem.setVisible(updateMode);
        patientOtherIdsLinkItem.setShowTitle(false);
        patientOtherIdsLinkItem.setColSpan(2);
        //patientOtherIdsLinkItem.setEndRow(true);
        patientOtherIdsLinkItem.addClickHandler(new OtherPatientIdsClickHandler());


        //Patient consentingUser Name Patient Username Patient consentingUser
        patientUserNameStaticTextItem = new StaticTextItem("patientUserName", "Patient Username");
        patientUserNameStaticTextItem.setWrapTitle(false);
        //patientUserNameStaticTextItem.setWidth(90);
        patientUserNameStaticTextItem.setVisible(updateMode);
        patientUserNameStaticTextItem.setEndRow(true);
        if (!patientEngagementEnable) {
            patientUserNameStaticTextItem.setVisible(false);
        }

        firstNameTextItem = new TextItem("firstNameTextItem", "*First Name");
        firstNameTextItem.setTabIndex(1);
        firstNameTextItem.setStartRow(true);

        firstNameTextItem.setWrapTitle(false);
        firstNameTextItem.setRequired(true);
        ValidationUtils.setValidators(firstNameTextItem, alphaHGAARegExValidator, true);

        middleNameTextItem = new TextItem("middleNameTextItem", "Middle Name");
        middleNameTextItem.setTabIndex(2);

        middleNameTextItem.setWrapTitle(false);
        ValidationUtils.setValidators(middleNameTextItem, alphaHGAARegExValidator, true);

        lastNameTextItem = new TextItem("lastNameTextItem", "*Last Name");
        lastNameTextItem.setTabIndex(3);

        lastNameTextItem.setWrapTitle(false);
        lastNameTextItem.setRequired(true);
        ValidationUtils.setValidators(lastNameTextItem, alphaHGAARegExValidator, true);

        patientMrnTextItem = new TextItem("patientMrn", "Patient MRN");
        patientMrnTextItem.setTabIndex(4);
        ValidationUtils.setValidators(patientMrnTextItem, alphaNumberRegExpValidator, true);

        // ssn
        ssnTextItem = new TextItem("ssnTextItem", "SSN");
        ssnTextItem.setTabIndex(5);
        ssnTextItem.setValidateOnExit(true);

        ssnTextItem.addBlurHandler(new SsnBlurHandler());
        ssnTextItem.addFocusHandler(new SsnFocusHandler());

        ssnLengthValidator = new LengthRangeValidator();
        ssnLengthValidator.setMax(SsnUtils.SSN_LENGTH);
        ssnLengthValidator.setMin(SsnUtils.SSN_LENGTH);
        ssnLengthValidator.setErrorMessage("Invalid length");

        removeSsnMaskAndValidators();


        dobDateItem = new DateItem(DATE_OF_BIRTH_DATE_ITEM, "*Date of Birth");

        dobDateItem.setTabIndex(6);
        dobDateItem.setEndRow(false);
        dobDateItem.setWrapTitle(false);
        dobDateItem.setTextAlign(Alignment.LEFT);
        dobDateItem.setUseMask(true);
        dobDateItem.setEnforceDate(true);
        dobDateItem.setDisplayFormat(DateDisplayFormat.TOUSSHORTDATE);
        dobDateItem.setStartDate(dateFormatter.parse("01/01/1900"));
        dobDateItem.setRequired(true);

        // add date validation rules
        DateRangeValidator dateRangeValidator = new DateRangeValidator();
        Date today = DateUtils.getDateAtEndOfToday();
        dateRangeValidator.setMax(today);

        Date minDate = getMinDate();
        String minDateStr = getDateStr(minDate);
        dateRangeValidator.setMin(minDate);

        dateRangeValidator.setErrorMessage("Date must be between " + minDateStr + " and today's date.");
        dobDateItem.setValidators(dateRangeValidator);
        dobDateItem.setValidateOnChange(true);
//        if(!updateMode){
//            dobDateItem.addBlurHandler(new dobBlurHandler());
//        }

        // Gender
        genderSelectItem = new SelectItem("genderSelectItem", "*Gender");
        genderSelectItem.setValueMap(ApplicationContextUtils.getGenderCodes());
        genderSelectItem.setWrapTitle(false);
        genderSelectItem.setTabIndex(7);
        genderSelectItem.setRequired(true);

        //Acuity

        acuityScoreTextItem = new TextItem("acuityScoreTextItem", "Acuity Score");
        acuityScoreTextItem.setTabIndex(8);
        acuityScoreTextItem.setWrapTitle(false);
        acuityScoreTextItem.setEndRow(true);
        acuityScoreTextItem.setColSpan(2);

        RegExpValidator decimalRegExValidator = ValidationUtils.getDecimalRegExValidator();
        ValidationUtils.setValidators(acuityScoreTextItem, decimalRegExValidator, true);

        // Ethnicity
        ethnicitySelectItem = new SelectItem("ethnicitySelectItem", "Ethnicity");
        ethnicitySelectItem.setValueMap(ApplicationContextUtils.getEthnicCodes());
        ethnicitySelectItem.setEndRow(true);
        ethnicitySelectItem.setWrapTitle(false);
        ethnicitySelectItem.setTabIndex(9);
        ethnicitySelectItem.setAllowEmptyValue(true);
        // Race
        raceSelectItem = new SelectItem("raceSelectItem", "Race");
        raceSelectItem.setValueMap(ApplicationContextUtils.getRaceCodes());
        raceSelectItem.setEndRow(true);
        raceSelectItem.setWrapTitle(false);
        raceSelectItem.setTabIndex(10);
        raceSelectItem.setAllowEmptyValue(true);


        // Preferred Language
        languageSelectItem = new SelectItem("languageSelectItem", "Preferred Language");
        languageSelectItem.setValueMap(ApplicationContextUtils.getLanguageCodes());
        languageSelectItem.setEndRow(true);
        languageSelectItem.setWrapTitle(false);


        languageSelectItem.setTabIndex(11);
        languageSelectItem.setAllowEmptyValue(true);

        SpacerItem spacerItem1 = new SpacerItem();
        spacerItem1.setName("spacer1");

        TextItem ssn1TextItem = new TextItem("ssnTextItem", "SSN");
        ssn1TextItem.setTabIndex(4);

        theForm.setFields(patientIDStaticTextItem, patientOtherIdsLinkItem, patientUserNameStaticTextItem,
                firstNameTextItem, ssnTextItem, ethnicitySelectItem,
                middleNameTextItem, dobDateItem, raceSelectItem,
                lastNameTextItem, genderSelectItem, languageSelectItem,
                patientMrnTextItem, acuityScoreTextItem);

        return theForm;
    }


//    private String[] getProgramNameCodesValues(long healthHomeId) {
//
//        String healthHomeWithDelimiter = Constants.DELIMITER_SELECT_ITEM + healthHomeId;
//        String healthHome = healthHomeWithDelimiter.split(Constants.DELIMITER_SELECT_ITEM)[1];
//        LinkedHashMap<Long, ProgramNameDTO> programNameHealthHomeMap = ApplicationContextUtils.getProgramNameForHealthHomeValue();
//        Collection<ProgramNameDTO> dto = programNameHealthHomeMap.values();
//        ArrayList<ProgramNameDTO> programList = new ArrayList<ProgramNameDTO>(dto);
//
//        String[] programName = FormUtils.convertProgramNameHealthHome(healthHome, programList);
//
//        String[] programNameCodes = FormUtils.getCodesWithAddedBlankOption(programName);
//
//        return programNameCodes;
//    }

//    class HealthHomeProgramChangedHandler implements ChangedHandler {
//
//        public void onChanged(ChangedEvent event) {
//            String healthHomeWithDelimiter = (String) event.getValue();
//
//            String healthHome = healthHomeWithDelimiter.split(Constants.DELIMITER_SELECT_ITEM)[1];
//
//            LinkedHashMap<Long, ProgramNameDTO> programNameHealthHomeMap = ApplicationContextUtils.getProgramNameForHealthHomeValue();
//
//            Collection<ProgramNameDTO> dto = programNameHealthHomeMap.values();
//            ArrayList<ProgramNameDTO> programList = new ArrayList<ProgramNameDTO>(dto);
//            programNameSelectItem.setValue(FormUtils.EMPTY);
//
//            if (programNameBln) {
//                String[] programName = FormUtils.convertProgramNameHealthHome(healthHome, programList);
//                String[] programNameCodes = FormUtils.getCodesWithAddedBlankOption(programName);
//                programNameSelectItem.setValueMap(programNameCodes);
//            } else {
//                String[] programName = FormUtils.convertProgramNameHealthHome(healthHome, programList);
//                String[] programNameCodes = FormUtils.getCodesWithAddedBlankOption(programName);
//                programNameSelectItem.setValueMap(programNameCodes);
//            }
//
//
//        }
//    }

    public long getCareteamIdOld() {
        return careteamIdOld;
    }

  private DynamicForm getEmergencyContactForm() {

        DynamicForm theForm = new DynamicForm();
        theForm.setGroupTitle("Emergency Contact");
        setFormWidth(theForm);
        theForm.setIsGroup(true);
        theForm.setNumCols(6);

        theForm.setRequiredTitlePrefix(FormUtils.REQUIRED_TITLE_PREFIX);
        theForm.setBrowserSpellCheck(false);

        RegExpValidator alphaHGAARegExValidator = ValidationUtils.getAlphaHGAARegExValidator();

        fNameEmergencyTextItem = new TextItem("fNameEmergencyTextItem", "First Name");
        fNameEmergencyTextItem.setTabIndex(1);
        fNameEmergencyTextItem.setWrapTitle(false);
        ValidationUtils.setValidators(fNameEmergencyTextItem, alphaHGAARegExValidator, true);

        mNameEmergencyTextItem = new TextItem("mNameEmergencyTextItem", "Middle Name");
        mNameEmergencyTextItem.setTabIndex(2);
        mNameEmergencyTextItem.setWrapTitle(false);
        ValidationUtils.setValidators(mNameEmergencyTextItem, alphaHGAARegExValidator, true);

        lNameEmergencyTextItem = new TextItem("lNameEmergencyTextItem", "Last Name");
        lNameEmergencyTextItem.setTabIndex(3);

        lNameEmergencyTextItem.setWrapTitle(false);
        lNameEmergencyTextItem.setStartRow(false);
        lNameEmergencyTextItem.setEndRow(true);
        ValidationUtils.setValidators(lNameEmergencyTextItem, alphaHGAARegExValidator, true);

        telephoneEmergencyTextItem = new TextItem("newTextItem_1", "Telephone");
        telephoneEmergencyTextItem.setTabIndex(4);

        telephoneEmergencyTextItem.setValidateOnExit(true);
        telephoneEmergencyTextItem.setMask("###-###-####");
        LengthRangeValidator workPhoneValidator = new LengthRangeValidator();
        workPhoneValidator.setMax(10);
        workPhoneValidator.setMin(10);
        workPhoneValidator.setErrorMessage("Invalid length");
        telephoneEmergencyTextItem.setValidators(workPhoneValidator);

        // email address
        emergencyEmailTextItem = new TextItem("emergencyEmailTextItem", "Email Address");
        emergencyEmailTextItem.setTabIndex(7);

        RegExpValidator emailRegExpValidator = getEmailAddressRegExpValidator();
        emergencyEmailTextItem.setValidateOnExit(true);
        LengthRangeValidator emailLengthValidator = new LengthRangeValidator();
        emailLengthValidator.setMax(50);
        emailLengthValidator.setErrorMessage("Invalid length");
        emergencyEmailTextItem.setValidators(emailRegExpValidator, emailLengthValidator);

        relationshipSelectItem = new SelectItem("relationSelectItem", "Relationship");
        relationshipSelectItem.setAllowEmptyValue(true);
        relationshipSelectItem.setWrapTitle(false);
        relationshipSelectItem.setTabIndex(8);
        relationshipSelectItem.setEndRow(true);
        LinkedHashMap<String, String> patientRelationshipMap = buildPatientRelationshipMap(ApplicationContextUtils.getPatientRelationship());
        relationshipSelectItem.setValueMap(patientRelationshipMap);

        theForm.setFields(fNameEmergencyTextItem, mNameEmergencyTextItem, lNameEmergencyTextItem, telephoneEmergencyTextItem, emergencyEmailTextItem, relationshipSelectItem);

        return theForm;
    }

    private DynamicForm getAddressForm() {

        DynamicForm theForm = new DynamicForm();
        theForm.setNumCols(6);
        theForm.setGroupTitle("Contact Information");
        setFormWidth(theForm);
        theForm.setIsGroup(true);
        theForm.setRequiredTitlePrefix(FormUtils.REQUIRED_TITLE_PREFIX);
        theForm.setBrowserSpellCheck(false);

        RegExpValidator alphaNumberSpecialSymbolsRegExpValidator = ValidationUtils.getAlphaNumberSpecialSymbolsRegExpValidator();

        address1TextItem = new TextItem("streetAddress1", "Address 1");
        address1TextItem.setTabIndex(1);
        address1TextItem.setRequired(true);
        address1TextItem.setWrapTitle(false);
        ValidationUtils.setValidators(address1TextItem, alphaNumberSpecialSymbolsRegExpValidator, true);

        address2TextItem = new TextItem("streetAddress2", "Address 2");
        address2TextItem.setTabIndex(2);
        ValidationUtils.setValidators(address2TextItem, alphaNumberSpecialSymbolsRegExpValidator, true);

        cityTextItem = new TextItem("newTextItem_3", "City");
        cityTextItem.setRequired(true);
        ValidationUtils.setValidators(cityTextItem, alphaOnlyRegExpValidator, true);

        stateSelectItem = new SelectItem("newSelectItem_8", "State");
        stateSelectItem.setTabIndex(4);
        stateSelectItem.setValueMap(ApplicationContextUtils.getStateCodes());
        stateSelectItem.setRequired(true);

        // email address
        emailTextItem = new TextItem("emailTextItem", "Email Address");
        emailTextItem.setTabIndex(7);
        emailTextItem.setValidateOnExit(true);
        emailTextItem.setWrapTitle(false);

        RegExpValidator emailRegExpValidator = getEmailAddressRegExpValidator();
        emailTextItem.setValidateOnExit(true);
        LengthRangeValidator emailLengthValidator = new LengthRangeValidator();
        emailLengthValidator.setMax(50);
        emailLengthValidator.setErrorMessage("Invalid length");
        emailTextItem.setValidators(emailRegExpValidator, emailLengthValidator);
        if(countyEnabled) {
            countySelectItem = new SelectItem("countySelectItem", "County");
            countySelectItem.setAllowEmptyValue(true);
            countySelectItem.setValueMap(ApplicationContextUtils.getCounty());
            if (countyRequired) {
                countySelectItem.setRequired(true);
            }
        }
        zipCodeTextItem = new TextItem("newTextItem_12", "Zip Code");
        zipCodeTextItem.setTabIndex(5);
        zipCodeTextItem.setRequired(true);
        zipCodeTextItem.setValidateOnExit(true);

        String zipCodeMask = "#####";
        zipCodeTextItem.setMask(zipCodeMask);
        LengthRangeValidator zipCodeValidator = new LengthRangeValidator();
        zipCodeValidator.setMax(5);
        zipCodeValidator.setMin(5);
        zipCodeValidator.setErrorMessage("Invalid length");
        zipCodeTextItem.setValidators(zipCodeValidator);
        zipCodeTextItem.setEndRow(true);

        telephoneTextItem = new TextItem("newTextItem_1", "Telephone");
        telephoneTextItem.setTabIndex(6);


        telephoneTextItem.setValidateOnExit(true);
        telephoneTextItem.setMask("###-###-####");
        LengthRangeValidator workPhoneValidator = new LengthRangeValidator();
        workPhoneValidator.setMax(10);
        workPhoneValidator.setMin(10);
        workPhoneValidator.setErrorMessage("Invalid length");
        telephoneTextItem.setValidators(workPhoneValidator);

        SpacerItem spacerItem = new SpacerItem();
        spacerItem.setColSpan(2);
        if(countySelectItem != null) {
            theForm.setFields(address1TextItem, cityTextItem, telephoneTextItem, address2TextItem, stateSelectItem, emailTextItem, countySelectItem, zipCodeTextItem);
        }
        else {
            theForm.setFields(address1TextItem, cityTextItem, telephoneTextItem, address2TextItem, stateSelectItem, emailTextItem, spacerItem, zipCodeTextItem);
        }
        return theForm;
    }
    private RegExpValidator getEmailAddressRegExpValidator() {
        RegExpValidator validator = new RegExpValidator();
        validator.setExpression(Constants.EMAIL_VALIDATION_REGEX);
        validator.setErrorMessage("email address must be valid.");
        return validator;
    }

   private DynamicForm buildInsuranceForm() {
        final DynamicForm theForm = new DynamicForm();
        theForm.setWrapItemTitles(true);
        theForm.setGroupTitle("Insurance");
        setFormWidth(theForm);
        theForm.setIsGroup(true);
        theForm.setNumCols(12);
        theForm.setRequiredTitlePrefix(FormUtils.REQUIRED_TITLE_PREFIX);
        theForm.setBrowserSpellCheck(false);

        primaryInsuranceTextItem = new StaticTextItem("primaryInsuranceTextItem", "Primary");
        primaryInsuranceTextItem.setWrapTitle(false);
        primaryInsuranceTextItem.setEndRow(true);
        primaryInsuranceTextItem.setWidth(10);

        secondaryInsuranceTextItem = new StaticTextItem("secondaryInsuranceTextItem", "Secondary");
        secondaryInsuranceTextItem.setWrapTitle(false);
        secondaryInsuranceTextItem.setEndRow(true);
        secondaryInsuranceTextItem.setWidth(10);

        primaryPayerClassSelectItem = new SelectItem("primaryPayerClassSelectItem", "Payer Class");
        primaryPayerClassSelectItem.setTabIndex(1);
        primaryPayerClassSelectItem.setWrapTitle(false);
        primaryPayerClassSelectItem.setAllowEmptyValue(true);
        primaryPayerClassSelectItem.setWidth(130);

        primaryPayerPlanComboBoxItem = new ComboBoxItem("primaryPayerPlanComboBoxItem", "Payer Plan");
        primaryPayerPlanComboBoxItem.setAddUnknownValues(false);
        primaryPayerPlanComboBoxItem.setTabIndex(2);
        primaryPayerPlanComboBoxItem.setWrapTitle(false);
        primaryPayerPlanComboBoxItem.setAllowEmptyValue(true);
        primaryPayerPlanComboBoxItem.setWidth(130);

        secondaryPayerClassSelectItem = new SelectItem("secondaryPayerClassSelectItem", "Payer Class");
        secondaryPayerClassSelectItem.setTabIndex(5);
        secondaryPayerClassSelectItem.setWrapTitle(false);
        secondaryPayerClassSelectItem.setAllowEmptyValue(true);
        secondaryPayerClassSelectItem.setWidth(130);

        secondaryPayerPlanComboBoxItem = new ComboBoxItem("secondaryPayerPlanComboBoxItem", "Payer Plan");
        secondaryPayerPlanComboBoxItem.setAddUnknownValues(false);
        secondaryPayerPlanComboBoxItem.setTabIndex(6);
        secondaryPayerPlanComboBoxItem.setWidth(130);

        primaryMedicaidIdTextItem = new TextItem("primaryMedicaidIdTextItem", "&#09;&#09;Medicaid/Medicare/Payer ID");
        primaryMedicaidIdTextItem.setTabIndex(3);
        primaryMedicaidIdTextItem.setWidth(120);
        primaryMedicaidIdTextItem.setEndRow(true);
        primaryMedicaidIdTextItem.setWrapTitle(false);
        primaryMedicaidIdTextItem.setAlign(Alignment.LEFT);

        if (enrollmentInsuranceMedicaidIdValidationEnabled) {
            primaryMedicaidIdTextItem.setValidateOnExit(true);
            primaryMedicaidIdTextItem.setValidators(new MedicaidIdValidator(primaryPayerClassSelectItem, primaryMedicaidIdTextItem));

            primaryPayerClassSelectItem.setValidateOnExit(true);
            primaryPayerClassSelectItem.setValidators(new MedicaidIdValidator(primaryPayerClassSelectItem, primaryMedicaidIdTextItem));
        }


        secondaryMedicaidIdTextItem = new TextItem("secondaryMedicaidIdTextItem", "&#09;&#09;Medicaid/Medicare/Payer ID");
        secondaryMedicaidIdTextItem.setTabIndex(7);
        secondaryMedicaidIdTextItem.setWidth(120);

        if (enrollmentInsuranceMedicaidIdValidationEnabled) {
            secondaryMedicaidIdTextItem.setValidateOnExit(true);
            secondaryMedicaidIdTextItem.setValidators(new MedicaidIdValidator(secondaryPayerClassSelectItem, secondaryMedicaidIdTextItem));

            secondaryPayerClassSelectItem.setValidateOnExit(true);
            secondaryPayerClassSelectItem.setValidators(new MedicaidIdValidator(secondaryPayerClassSelectItem, secondaryMedicaidIdTextItem));
        }

        secondaryMedicaidIdTextItem.setEndRow(true);

        if (enrollmentInsuranceMedicaidIdValidationEnabled) {
            primaryPayerClassSelectItem.addChangeHandler(new ChangeHandler() {
                public void onChange(ChangeEvent event) {

                    if (StringUtils.isNotBlank(primaryMedicaidIdTextItem.getValueAsString())) {
                        if (event.getValue() != null) {
                            primaryMedicaidIdTextItem.setValidators(new FalseValidator(event.getValue().toString(), primaryMedicaidIdTextItem));
                            primaryMedicaidIdTextItem.validate();
                        } else {
                            primaryMedicaidIdTextItem.setValidators(new FalseValidator("0", primaryMedicaidIdTextItem));
                            primaryMedicaidIdTextItem.validate();
                        }
                    }
                }
            });
        }

        if (enrollmentInsuranceMedicaidIdValidationEnabled) {
            secondaryPayerClassSelectItem.addChangeHandler(new ChangeHandler() {
                public void onChange(ChangeEvent event) {

                    if (StringUtils.isNotBlank(secondaryMedicaidIdTextItem.getValueAsString())) {
                        if (event.getValue() != null) {
                            secondaryMedicaidIdTextItem.setValidators(new FalseValidator(event.getValue().toString(), secondaryMedicaidIdTextItem));
                            secondaryMedicaidIdTextItem.validate();
                        } else {
                            secondaryMedicaidIdTextItem.setValidators(new FalseValidator("0", secondaryMedicaidIdTextItem));
                            secondaryMedicaidIdTextItem.validate();
                        }
                    }
                }
            });
        }

        primaryEffectiveDateItem = new DateItem("primaryEffectiveDateItem", "&#09;&#09;Effective Date");
        primaryEffectiveDateItem.setTabIndex(4);
        primaryEffectiveDateItem.setEndRow(true);
        primaryEffectiveDateItem.setWrapTitle(false);
        primaryEffectiveDateItem.setTextAlign(Alignment.LEFT);
        primaryEffectiveDateItem.setUseMask(true);
        primaryEffectiveDateItem.setEnforceDate(true);
        primaryEffectiveDateItem.setDisplayFormat(DateDisplayFormat.TOUSSHORTDATE);
        primaryEffectiveDateItem.setStartDate(dateFormatter.parse("01/01/1900"));
        primaryEffectiveDateItem.setWidth(90);

        if (enrollmentInsuranceEffectiveDateValidationEnabled) {

            // add date validation rules
            DateRangeValidator pridateRangeValidator = new DateRangeValidator();
            Date today = DateUtils.getDateAtEndOfToday();

            pridateRangeValidator.setMax(today);

            pridateRangeValidator.setErrorMessage("Date must be today's date or prior.");
            primaryEffectiveDateItem.setValidators(pridateRangeValidator);
            primaryEffectiveDateItem.setValidateOnChange(true);
        }

        secondaryEffectiveDateItem = new DateItem("secondaryEffectiveDateItem", "&#09;&#09;Effective Date");
        secondaryEffectiveDateItem.setTabIndex(8);
        secondaryEffectiveDateItem.setEndRow(true);
        secondaryEffectiveDateItem.setWrapTitle(false);
        secondaryEffectiveDateItem.setTextAlign(Alignment.LEFT);
        secondaryEffectiveDateItem.setUseMask(true);
        secondaryEffectiveDateItem.setEnforceDate(true);
        secondaryEffectiveDateItem.setDisplayFormat(DateDisplayFormat.TOUSSHORTDATE);
        secondaryEffectiveDateItem.setStartDate(dateFormatter.parse("01/01/1900"));
        secondaryEffectiveDateItem.setWidth(90);

        if (enrollmentInsuranceEffectiveDateValidationEnabled) {
            // add date validation rules
            DateRangeValidator secdateRangeValidator = new DateRangeValidator();
            Date today = DateUtils.getDateAtEndOfToday();
            secdateRangeValidator.setMax(today);

            secdateRangeValidator.setErrorMessage("Date must be today's date or prior.");
            secondaryEffectiveDateItem.setValidators(secdateRangeValidator);
            secondaryEffectiveDateItem.setValidateOnChange(true);
        }

        SpacerItem space = new SpacerItem();
        space.setColSpan(4);

        SpacerItem space2 = new SpacerItem();
        space2.setWidth(67);

        SpacerItem space3 = new SpacerItem();
        space3.setWidth(274);

        theForm.setFields(primaryInsuranceTextItem, space2, primaryPayerClassSelectItem, space3, primaryMedicaidIdTextItem, space2, primaryPayerPlanComboBoxItem, space3, primaryEffectiveDateItem, secondaryInsuranceTextItem, space2, secondaryPayerClassSelectItem, space3, secondaryMedicaidIdTextItem, space2, secondaryPayerPlanComboBoxItem, space3, secondaryEffectiveDateItem);

        return theForm;
    }

    public boolean validate() {
        boolean validPatientDemographicsForm = patientDemographicsForm.validate();
        boolean validAddressForm = addressForm.validate();
        boolean validEmergencyContactForm = emergencyContactForm.validate();
        boolean validInsuranceForm = insuranceForm.validate();
       return validPatientDemographicsForm && validAddressForm && validEmergencyContactForm && validInsuranceForm;
    }

    public void clearErrors() {
        patientDemographicsForm.clearErrors(true);
        addressForm.clearErrors(true);
        emergencyContactForm.clearErrors(true);
        insuranceForm.clearErrors(true);
    }

    public void clearValues() {
        this.clearSsnValues();
        patientDemographicsForm.clearValues();
        addressForm.clearValues();
        emergencyContactForm.clearValues();
        insuranceForm.clearValues();
    }

    public PersonDTO getPerson() {
        if (person == null) {
            person = new PersonDTO();
        }

        // demographics
        if(firstNameTextItem.getValueAsString() != null && !firstNameTextItem.getValueAsString().isEmpty()){
        	person.setFirstName(firstNameTextItem.getValueAsString().trim());
        }
        if(lastNameTextItem.getValueAsString() != null && !lastNameTextItem.getValueAsString().isEmpty()){
        	person.setLastName(lastNameTextItem.getValueAsString().trim());
        }

        //person.setFirstName(firstNameTextItem.getValueAsString());
        person.setMiddleName(middleNameTextItem.getValueAsString());
        //person.setLastName(lastNameTextItem.getValueAsString());
        person.setDateOfBirth(dobDateItem.getValueAsDate());

         // dob
        Date dateOfBirth = dobDateItem.getValueAsDate();
        //Spira 5438 : To compare duplicate patients we set again date in string format after capturing from date item.
        if (dateOfBirth != null) {
            DateTimeFormat dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
            String data = dateFormatter.format(dateOfBirth);
            person.setDateOfBirthAsString(data);
        }

        person.setGender(genderSelectItem.getValueAsString());

        // person.setSsn(ssnTextItem.getValueAsString());
        person.setSsn(realSsnValue);

        // address
        person.setStreetAddress1(address1TextItem.getValueAsString());
        person.setStreetAddress2(address2TextItem.getValueAsString());
        person.setCity(cityTextItem.getValueAsString());
        person.setState(stateSelectItem.getValueAsString());


        if(countyEnabled){
            String countyId = countySelectItem.getValueAsString();
            person.setCounty(ApplicationContextUtils.getCounty().get(countyId));
        }

        person.setZipCode(zipCodeTextItem.getValueAsString());
        person.setTelephone(telephoneTextItem.getValueAsString());
        //
        // patient enrollment
        //
        PatientEnrollmentDTO patientEnrollmentDTO = person.getPatientEnrollment();
        patientEnrollmentDTO.setCommunityId(communityId);

        patientEnrollmentDTO.setPatientMrn(patientMrnTextItem.getValueAsString());
         patientEnrollmentDTO.setAcuityScore(acuityScoreTextItem.getValueAsString());

         // ethnicity
        person.setEthnic(ethnicitySelectItem.getValueAsString());

        // Race
        person.setRace(raceSelectItem.getValueAsString());

        // preferred language
        person.setLanguage(languageSelectItem.getValueAsString());


        // email
        patientEnrollmentDTO.setEmailAddress(emailTextItem.getValueAsString());
        //set the email to person also to save to Mirth
        person.getSimplePerson().setEmail(emailTextItem.getValueAsString());


        // Emergency contacts
        PatientEmergencyContactDTO emergencyContact = patientEnrollmentDTO.getPatientEmergencyContact();

        if (emergencyContact == null) {
            emergencyContact = new PatientEmergencyContactDTO();
            patientEnrollmentDTO.setPatientEmergencyContact(emergencyContact);
        }


        emergencyContact.setFirstName(fNameEmergencyTextItem.getValueAsString());
        emergencyContact.setLastName(lNameEmergencyTextItem.getValueAsString());
        emergencyContact.setMiddleName(mNameEmergencyTextItem.getValueAsString());
        emergencyContact.setEmailAddress(emergencyEmailTextItem.getValueAsString());
        emergencyContact.setTelephone(telephoneEmergencyTextItem.getValueAsString());

        String relationshipStr = relationshipSelectItem.getValueAsString();

        if (StringUtils.isNotBlank(relationshipStr)) {

            //
            // work around for sorting by keys. prepend numeric key with a string
            // to preserved sorted order
            // http://code.google.com/p/smartgwt/issues/detail?id=569
            //
            // retrieve user id
            long relationshipId = Long.parseLong(relationshipStr.split(Constants.DELIMITER_SELECT_ITEM)[1]);

            emergencyContact.setPatientRelationshipId(relationshipId);
            emergencyContact.setPatientRelationshipDescription(ApplicationContextUtils.getPatientRelationship().get(relationshipId));
        } else {
            emergencyContact.setPatientRelationshipId(EMPTY_RELATIONSHIP_CODE);
            emergencyContact.setPatientRelationshipDescription(null);
        }

        // insurance
        // primary payer plan      
        PayerPlanDTO primaryPayerPlan = new PayerPlanDTO();
        String primaryPayerPlanIdStr = primaryPayerPlanComboBoxItem.getValueAsString();
        if (StringUtils.isNotBlank(primaryPayerPlanIdStr)) {
            String stripPrimaryPayerPlanIdStr = primaryPayerPlanIdStr.split(Constants.DELIMITER_SELECT_ITEM)[1];
            long primaryPayerPlanId = Long.parseLong(stripPrimaryPayerPlanIdStr);
            primaryPayerPlan.setId(primaryPayerPlanId);
            primaryPayerPlan.setName(ApplicationContextUtils.getPayerPlanNames().get(stripPrimaryPayerPlanIdStr));
            patientEnrollmentDTO.setPrimaryPayerPlan(primaryPayerPlan);
        } else {
            patientEnrollmentDTO.setPrimaryPayerPlan(null);
        }

        // secondary payer plan
        String secondaryPayerPlanIdStr = secondaryPayerPlanComboBoxItem.getValueAsString();
        PayerPlanDTO secondaryPayerPlan = new PayerPlanDTO();
        if (StringUtils.isNotBlank(secondaryPayerPlanIdStr)) {
        	String stripSecondaryPayerPlanIdStr = secondaryPayerPlanIdStr.split(Constants.DELIMITER_SELECT_ITEM)[1];
            long secondaryPayerPlanId = Long.parseLong(stripSecondaryPayerPlanIdStr);
            secondaryPayerPlan.setId(secondaryPayerPlanId);
            secondaryPayerPlan.setName(ApplicationContextUtils.getPayerPlanNames().get(stripSecondaryPayerPlanIdStr));
            patientEnrollmentDTO.setSecondaryPayerPlan(secondaryPayerPlan);

        } else {
            patientEnrollmentDTO.setSecondaryPayerPlan(null);
        }

        // primary payer class

        PayerClassDTO primaryPayerClass = new PayerClassDTO();
        String primaryPayerClassIdStr = primaryPayerClassSelectItem.getValueAsString();

        if (primaryPayerClassIdStr != null) {
            Long primaryPayerClassId = Long.parseLong(primaryPayerClassIdStr);
            primaryPayerClass.setId(primaryPayerClassId);
            primaryPayerClass.setName(ApplicationContextUtils.getPayerClassNames().get(primaryPayerClassIdStr));
            primaryPayerClass.setCommunityId(communityId);
            patientEnrollmentDTO.setPrimaryPayerClass(primaryPayerClass);
            
        } else {
            patientEnrollmentDTO.setPrimaryPayerClass(null);
        }

        // secondary payer class

        PayerClassDTO secondaryPayerClass = new PayerClassDTO();
        String secondaryPayerClassIdStr = secondaryPayerClassSelectItem.getValueAsString();

        if (secondaryPayerClassIdStr != null) {
            Long secondaryPayerClassId = Long.parseLong(secondaryPayerClassIdStr);
            secondaryPayerClass.setId(secondaryPayerClassId);
            secondaryPayerClass.setName(ApplicationContextUtils.getPayerClassNames().get(secondaryPayerClassIdStr));
            patientEnrollmentDTO.setSecondaryPayerClass(secondaryPayerClass);
        } else {
            patientEnrollmentDTO.setSecondaryPayerClass(null);
        }

        // primary payer medicaid id
        String primaryPayerMedicaidId = primaryMedicaidIdTextItem.getValueAsString();
        patientEnrollmentDTO.setPrimaryPayerMedicaidMedicareId(primaryPayerMedicaidId);

        // second payer medicaid id
        String secondaryPayerMedicaidId = secondaryMedicaidIdTextItem.getValueAsString();
        patientEnrollmentDTO.setSecondaryPayerMedicaidMedicareId(secondaryPayerMedicaidId);

        // primary effective date
        Date primaryEffectiveDate = primaryEffectiveDateItem.getValueAsDate();
        patientEnrollmentDTO.setPrimaryPlanEffectiveDate(primaryEffectiveDate);

        // secondary effective date
        Date secondaryEffectiveDate = secondaryEffectiveDateItem.getValueAsDate();
        patientEnrollmentDTO.setSecondaryPlanEffectiveDate(secondaryEffectiveDate);

        //Patient Active /Inactive for save new patient record
       // boolean patientActive = addPatientSelectItem.getValueAsBoolean();
        // TODO: is this correct? Setting the default value as true
        boolean patientActive = false;


        String existFlag = person.getPatientEnrollment().getPatientUserActive();
      // TODO: is this correct? Setting the default value as Active
      //  existFlag = "Active";

        if (patientActive) {
            person.setPatientUserActive(Constants.PATINET_ACTIVE);
        } else {
            if (existFlag != null && existFlag.equalsIgnoreCase(Constants.PATINET_ACTIVE)) {
                person.setPatientUserActive(Constants.PATINET_INACTIVE);
            } else if (existFlag != null && existFlag.equalsIgnoreCase(Constants.PATINET_INACTIVE)) {
                person.setPatientUserActive(Constants.PATINET_INACTIVE);
            } else {
                person.setPatientUserActive(Constants.PATINET_NONE);
            }
        }
        patientEnrollmentDTO.setPatientUserActive(person.getPatientUserActive());
        person.setPatientEnrollment(patientEnrollmentDTO);

        return person;
    }

    public void populate(PersonDTO thePerson) {
        person = thePerson;  
        setOriginalPerson(person);
        PatientEnrollmentDTO patientEnrollment = person.getPatientEnrollment();
        patientIDStaticTextItem.setValue(StringUtils.defaultString(String.valueOf(patientEnrollment.getPatientId())));
        firstNameTextItem.setValue(person.getFirstName());
        middleNameTextItem.setValue(person.getMiddleName());
        lastNameTextItem.setValue(person.getLastName());

        //pe work
        patientUserNameStaticTextItem.setValue(patientEnrollment.getPatientLoginId());
//       Date dob = person.getDateOfBirth();
//        if(dob.getYear() < 1946){
//         dob.setHours(13);
//        }
//       dobDateItem.setValue(dob);
        //Spira 5438 : populating date from string value not from date value because date value was changing between server to client.
        dobDateItem.setValue(person.getDateOfBirthAsString());

        genderSelectItem.setValue(person.getGender());

        // ethnic, race and language
        if(StringUtils.isNotBlank(person.getEthnic()))ethnicitySelectItem.setValue(person.getEthnic());
        if(StringUtils.isNotBlank(person.getRace()))raceSelectItem.setValue(person.getRace());
        if(StringUtils.isNotBlank(person.getLanguage()))languageSelectItem.setValue(person.getLanguage());

        // ssn
        realSsnValue = person.getSsn();

        if (!StringUtils.isBlank(realSsnValue)) {
            ssnTextItem.setValue(SsnUtils.getMaskedSsn(realSsnValue));
        }

        address1TextItem.setValue(person.getStreetAddress1());
        address2TextItem.setValue(person.getStreetAddress2());
        cityTextItem.setValue(person.getCity());
        stateSelectItem.setValue(person.getState());
        if(countyEnabled){
            populateCounty(person.getCounty());
        }
        zipCodeTextItem.setValue(person.getZipCode());
        telephoneTextItem.setValue(person.getTelephone());

        String patientMrn = patientEnrollment.getPatientMrn();

        if (patientMrn != null) {
            patientMrnTextItem.setValue(patientMrn);
        }

        // email
        emailTextItem.setValue(patientEnrollment.getEmailAddress());

        String acuityScore=patientEnrollment.getAcuityScore();
        
       if(acuityScore!=null){
        acuityScoreTextItem.setValue(acuityScore);
        }

       //Patient Active /Inactive for save new patient record

        // insurance
        populateInsuranceSection(patientEnrollment);
        // Emergency Contacts
        populateEmergencyContactSection(patientEnrollment);

    }

    public void populatePersonInfo(PersonInfo personInfo) {
        patientIDStaticTextItem.setValue(StringUtils.defaultString(String.valueOf(personInfo.getPatientId())));

        middleNameTextItem.setValue(personInfo.getMiddleName());
        firstNameTextItem.setValue(personInfo.getFirstName());
        lastNameTextItem.setValue(personInfo.getLastName());

        address1TextItem.setValue(personInfo.getAddress1());
        cityTextItem.setValue(personInfo.getCity());
        zipCodeTextItem.setValue(personInfo.getZipcode());
        stateSelectItem.setValue(personInfo.getState());
        if(countyEnabled){
            populateCounty(personInfo.getCounty());
        }

        if (personInfo.getDob() != null) {
            String dobAsString = "";
            DateTimeFormat dateFormatter = DateTimeFormat.getFormat("yyyy-MM-dd");
            Date date = dateFormatter.parse(personInfo.getDob());
            dobAsString = DateTimeFormat.getFormat("MM/dd/yyyy").format(date);
            dobDateItem.setValue(dobAsString);
        }
        dobDateItem.setValue(personInfo.getDob());
        genderSelectItem.setValue(personInfo.getGender());
        
        realSsnValue = personInfo.getSsn();
        if (!StringUtils.isBlank(realSsnValue)) {
            ssnTextItem.setValue(SsnUtils.getMaskedSsn(realSsnValue));
        }

        String medicareId = personInfo.getMedicareId();
        if (StringUtils.isNotBlank(medicareId)) {
        	final String medicarePayerClass = "Medicare"; 
        	
        	primaryMedicaidIdTextItem.setValue(medicareId);
        	
        	Long payerClassId = null;
        	for(Map.Entry<String, String> entry: ApplicationContextUtils.getPayerClassNames().entrySet()){
                if((entry.getValue()).equalsIgnoreCase(medicarePayerClass)){
                	payerClassId = Long.valueOf(entry.getKey());
                    break;
                }
            }
        	primaryPayerClassSelectItem.setValue(payerClassId);
        }
        
        String medicaidId = personInfo.getMedicaidId();
        if (StringUtils.isNotBlank(medicaidId)) {
        	final String medicaidPayerClass = "Medicaid"; 
        	
        	primaryMedicaidIdTextItem.setValue(medicaidId);
        	
        	Long payerClassId = null;
        	for(Map.Entry<String, String> entry: ApplicationContextUtils.getPayerClassNames().entrySet()){
                if((entry.getValue()).equalsIgnoreCase(medicaidPayerClass)){
                	payerClassId = Long.valueOf(entry.getKey());
                    break;
                }
            }
        	primaryPayerClassSelectItem.setValue(payerClassId);
        }
//        organizationSelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + personInfo.getOrganizationId());
//        organizationSelectItem.setValue(personInfo.getOrganizationName());

    }

    private void populateConsentDate(Date consentDate) {
        consentDateItem.setValue(consentDate);
    }

    private void populateConsentDate2(Date consentDate2) {
        consentDateItem.setValue(consentDate2);
    }

    private void populateConsentObtainedByUser(long User) {
        consentObtainedBySelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + User);
    }
     private void populateConsenter(PatientEnrollmentDTO patientEnrollment) {

        long consenterCode = patientEnrollment.getConsenter();
       // Window.alert("PatientForm consenterCode:"+ patientEnrollment.getConsenter());

        consenterSelectItem.setValue((consenterCode==0)?"":Constants.DELIMITER_SELECT_ITEM + consenterCode);
    }


//    private void populateConsentTime(Date consentDate) {
//        if (consentDate != null) {
//            DateTimeFormat dateFormatter = DateTimeFormat.getFormat("hh:mm");
//            String data = dateFormatter.format(consentDate);
//           consentTimeTextItem.setValue(data);
//
//            DateTimeFormat ampmFormatter = DateTimeFormat.getFormat("a");
//            String ampm = ampmFormatter.format(consentDate);
//
//            consentAMPM.setValue(ampm);
//        }
//    }
//
//    private void populateConsentTime2(Date consentDate2) {
//
//        if (consentDate2 != null) {
//            DateTimeFormat dateFormatter = DateTimeFormat.getFormat("hh:mm");
//            String data2 = dateFormatter.format(consentDate2);
//            consentTimeTextItem.setValue(data2);
//
//            DateTimeFormat ampmFormatter = DateTimeFormat.getFormat("a");
//            String ampm2 = ampmFormatter.format(consentDate2);
//
//            consentAMPM.setValue(ampm2);
//        }
//    }

    private void populateMinorConsentDate(Date consentDate) {
        dateItem.setValue(consentDate);
    }
//    private void populateMinorConsentTime(Date consentDate) {
//        if (consentDate != null) {
//            DateTimeFormat dateFormatter = DateTimeFormat.getFormat("hh:mm");
//            String data = dateFormatter.format(consentDate);
//            timeTextItem.setValue(data);
//
//            DateTimeFormat ampmFormatter = DateTimeFormat.getFormat("a");
//            String ampm = ampmFormatter.format(consentDate);
//
//            consentAMPMItem.setValue(ampm);
//        }
//    }

    public PersonDTO getOriginalPerson() {
        return originalPerson;
    }

    private PersonDTO copy(PersonDTO person) {
        PersonDTO temp = new PersonDTO();

        temp.setFirstName(person.getFirstName());
        temp.setLastName(person.getLastName());
        temp.setDateOfBirth(person.getDateOfBirth());
        //Spira 5438 : To compare duplicate patients we set date in string format. It will be used in comparison in case of update patient.
        temp.setDateOfBirthAsString(person.getDateOfBirthAsString());        
        temp.setGender(person.getGender());
        temp.setSsn(person.getSsn());
        temp.setCounty(person.getCounty());

        PatientEnrollmentDTO patientEnrollmentDTO = new PatientEnrollmentDTO();
        patientEnrollmentDTO.setOrgId(person.getPatientEnrollment().getOrgId());
        patientEnrollmentDTO.setStatus(person.getPatientEnrollment().getStatus());
//        patientEnrollmentDTO.setProgramName(person.getPatientEnrollment().getProgramName());
//        patientEnrollmentDTO.setHealthHome(person.getPatientEnrollment().getHealthHome());

        //pe
        System.out.println("ORA --1669:" + person.getPatientEnrollment().getPatientUserActive());
        temp.setOriginalPatientUserActive(person.getPatientEnrollment().getPatientUserActive());
        patientEnrollmentDTO.setPatientUserActive(person.getPatientEnrollment().getPatientUserActive());

        //set PrimaryPayerMedicaidMedicareId && SecondaryPayerMedicaidMedicareId
        patientEnrollmentDTO.setPrimaryPayerMedicaidMedicareId(person.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId());
        patientEnrollmentDTO.setSecondaryPayerMedicaidMedicareId(person.getPatientEnrollment().getSecondaryPayerMedicaidMedicareId());
        temp.setPatientEnrollment(patientEnrollmentDTO);


        return temp;
    }

    public void setOriginalPerson(PersonDTO person) {
        originalPerson = copy(person);
    }




   private void loadPatientFormReferenceData(long communityId) {
       loadPatientFormReferenceData(null, communityId);
    }

    private void loadPatientFormReferenceData(PersonDTO thePerson,long communityId) {
        LinkedHashMap<String, String> payerPlanNames = ApplicationContextUtils.getPayerPlanNames();
        LinkedHashMap<String, String> payerPlanNamesMap =  buildPayerPlanNamesMap(payerPlanNames);
        primaryPayerPlanComboBoxItem.setValueMap(payerPlanNamesMap);
        secondaryPayerPlanComboBoxItem.setValueMap(payerPlanNamesMap);

                     // payer classes
        LinkedHashMap<String, String> payerClassNamesPrimary = ApplicationContextUtils.getPayerClassNames();
        LinkedHashMap<String, String> payerClassNamesSecondary = ApplicationContextUtils.getPayerClassNames();

        primaryPayerClassSelectItem.setValueMap(payerClassNamesPrimary);
        secondaryPayerClassSelectItem.setValueMap(payerClassNamesSecondary);
         if (thePerson != null) {
            populate(thePerson);
         }
    }
    public String primaryPayerClassValues(){
        LinkedHashMap<String, String> primaryPayerClassNames = ApplicationContextUtils.getPayerClassNames();

        String s1 = primaryPayerClassSelectItem.getValueAsString();
        String primaryPayerClassName = primaryPayerClassNames.get(s1);

        return primaryPayerClassName;

    }

    public String secondaryPayerClassValues(){
        LinkedHashMap<String, String> secondaryPayerClassNames = ApplicationContextUtils.getPayerClassNames();

        String s1 = secondaryPayerClassSelectItem.getValueAsString();
        String secondaryPayerClassName = secondaryPayerClassNames.get(s1);

        return secondaryPayerClassName;
    }

    private void populateCounty(String countyIdVal) {
        Long countyId = null;
        for (Map.Entry<String, String> entry : ApplicationContextUtils.getCounty().entrySet()) {
            if ((entry.getValue()).equalsIgnoreCase(countyIdVal)) {
                countyId = Long.valueOf(entry.getKey());
                break;
            }
        }
        if (countyId != null) {
            countySelectItem.setValue(countyId);
        }
    }

    private void populateInsuranceSection(PatientEnrollmentDTO patientEnrollment) {

        // primary payer class
        PayerClassDTO primaryPayerClass = patientEnrollment.getPrimaryPayerClass();
        if (primaryPayerClass != null) {
            Long primaryPayerClassId = primaryPayerClass.getId();
            if (primaryPayerClassId != null) {
                primaryPayerClassSelectItem.setValue(primaryPayerClassId);
            }
        }

        // secondary payer class
        PayerClassDTO secondaryPayerClass = patientEnrollment.getSecondaryPayerClass();
        if (secondaryPayerClass != null) {
            Long secondaryPayerClassId = secondaryPayerClass.getId();
            if (secondaryPayerClassId != null) {
                secondaryPayerClassSelectItem.setValue(secondaryPayerClassId);
            }
        }

        // primary payer plan               
        PayerPlanDTO primaryPayerPlan = patientEnrollment.getPrimaryPayerPlan();

        if (primaryPayerPlan != null) {
            Long primaryPayerPlanId = primaryPayerPlan.getId();
            primaryPayerPlanComboBoxItem.setValue(Constants.DELIMITER_SELECT_ITEM + primaryPayerPlanId);
        }

        // secondary payer plan
        PayerPlanDTO secondaryPayerPlan = patientEnrollment.getSecondaryPayerPlan();
        if (secondaryPayerPlan != null) {
            Long secondaryPayerPlanId = secondaryPayerPlan.getId();
            secondaryPayerPlanComboBoxItem.setValue(Constants.DELIMITER_SELECT_ITEM + secondaryPayerPlanId);
        }

        // primary payer medicaid medicare id
        String primaryPayerMedicaidMedicareId = patientEnrollment.getPrimaryPayerMedicaidMedicareId();
        if (StringUtils.isNotBlank(primaryPayerMedicaidMedicareId)) {
            primaryMedicaidIdTextItem.setValue(primaryPayerMedicaidMedicareId);
        }

        // secondary payer medicaid medicare id
        String secondaryPayerMedicaidMedicareId = patientEnrollment.getSecondaryPayerMedicaidMedicareId();
        if (StringUtils.isNotBlank(secondaryPayerMedicaidMedicareId)) {
            secondaryMedicaidIdTextItem.setValue(secondaryPayerMedicaidMedicareId);
        }

        // primary effective date
        Date primaryEffectiveDate = patientEnrollment.getPrimaryPlanEffectiveDate();
        if (primaryEffectiveDate != null) {
            primaryEffectiveDateItem.setValue(primaryEffectiveDate);
        }

        // secondary effective date
        Date secondaryEffectiveDate = patientEnrollment.getSecondaryPlanEffectiveDate();
        if (secondaryEffectiveDate != null) {
            secondaryEffectiveDateItem.setValue(secondaryEffectiveDate);
        }

    }

    private void populateEmergencyContactSection(PatientEnrollmentDTO patientEnrollment) {

        PatientEmergencyContactDTO emergencyContact = patientEnrollment.getPatientEmergencyContact();

        if (emergencyContact != null) {
            fNameEmergencyTextItem.setValue(emergencyContact.getFirstName());
            lNameEmergencyTextItem.setValue(emergencyContact.getLastName());
            mNameEmergencyTextItem.setValue(emergencyContact.getMiddleName());
            emergencyEmailTextItem.setValue(emergencyContact.getEmailAddress());
            telephoneEmergencyTextItem.setValue(emergencyContact.getTelephone());

            Long patientRelationshipCode = emergencyContact.getPatientRelationshipId();

            if ((patientRelationshipCode != null) && (patientRelationshipCode.longValue() > 0)) {
                relationshipSelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + patientRelationshipCode);
            }
        }
    }

    private Date getMinDate() {
        DateTimeFormat dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
        Date theDate = dateFormatter.parse(MIN_DATE);

        return theDate;
    }

    private String getDateStr(Date theDate) {
        DateTimeFormat dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
        String theDateStr = dateFormatter.format(theDate);

        return theDateStr;
    }


   public PatientInfo getPatientInfo() {
        if(patientInfo == null) {
            patientInfo = populatePatientInfo();
        }
        return patientInfo;
    }

    private PatientInfo populatePatientInfo() throws NumberFormatException {
        patientInfo = new PatientInfo();

        PatientEnrollmentDTO patientEnrollment = person.getPatientEnrollment();

        // email
        patientEnrollment.setEmailAddress(emailTextItem.getValueAsString());


       // patientInfo.setConsentTime(consentTimeTextItem.getValueAsString());
        //patientInfo.setConsentAMPM(consentAMPM.getValueAsString());

        patientInfo.setPatientId(patientEnrollment.getPatientId());

            //Patient user Active /Inactive to save

        String existFlag = person.getPatientEnrollment().getPatientUserActive();

        patientInfo.setOriginalPatientUserActive(existFlag);

         patientInfo.setPatientEnrollment(patientEnrollment);

        return patientInfo;
    }

   private List<ListGridRecord> buildUsersMap(List<UserDTO> users) {

        List<ListGridRecord> reclist = new ArrayList<ListGridRecord>(users.size());

        for (UserDTO tempUser : users) {
            StringBuilder label = new StringBuilder();

            String status = tempUser.getStatus();
            boolean active = StringUtils.equalsIgnoreCase(status, "ACTIVE");
            String userId = tempUser.getUserId().toString();
            String key = Constants.DELIMITER_SELECT_ITEM + userId;
            label.append(tempUser.getLastName());
            label.append(", ");
            label.append(tempUser.getFirstName());

            String organization = tempUser.getOrganization();

            if (StringUtils.isNotBlank(organization)) {
                label.append(" - ");
                label.append(organization);

            }
            ListGridRecord record = new ListGridRecord();
            record.setAttribute("value", key);
            record.setEnabled(active);
            if(active)
            record.setAttribute("display", label.toString());
            else{
                record.setAttribute("display", label.toString());
                record.setCustomStyle("background-color-white");
            }
            reclist.add(record);

        }
        return reclist;
    }

    private List<ListGridRecord> buildConsentersMapByListGridRecord(List<ConsenterDTO> consenters) {

       // Window.alert("Patientform buildConsenters from:"+consenters.size());
        List<ListGridRecord> reclist = new ArrayList<ListGridRecord>(consenters.size());

        for (ConsenterDTO tempConsenter : consenters) {
            StringBuilder label = new StringBuilder();

            long code = tempConsenter.getCode();
            String description = tempConsenter.getCodeDescription();
            String consenterCode = String.valueOf(code);
            String key = Constants.DELIMITER_SELECT_ITEM + consenterCode;
            label.append(description);
            boolean active =true;


            ListGridRecord record = new ListGridRecord();
            record.setAttribute("value", key);
            record.setEnabled(active);
            if(active)
            record.setAttribute("display", label.toString());
            else{
                record.setAttribute("display", label.toString());
                record.setCustomStyle("background-color-white");
            }
            reclist.add(record);

        }
        return reclist;
    }

      private List<ListGridRecord> buildConsentersMapByListGridRecord(LinkedHashMap<String,String> consenters) {

        // Window.alert("Patientform buildConsenters from:"+consenters.size());
        List<ListGridRecord> reclist = new ArrayList<ListGridRecord>(consenters.size());

         for (String consenterCode : consenters.keySet()) {
            StringBuilder label = new StringBuilder();

           // long code = tempConsenter.getCode();
            String codeDescription = consenters.get(consenterCode);
          //  String consenterCode = String.valueOf(code);
            String key = Constants.DELIMITER_SELECT_ITEM + consenterCode;
            label.append(codeDescription);
            boolean active =true;


            ListGridRecord record = new ListGridRecord();
            record.setAttribute("value", key);

            if (minorFlag && StringUtils.contains(consenterCode,"4")){
                //minor patient  consenters can't pick code 4
               // Window.alert("Minor deactivate: " + label.toString());
                active = false;
            }
             if (!minorFlag && !(StringUtils.contains(consenterCode,"3")||
                     StringUtils.contains(consenterCode,"4"))){
                //adult patient consenter can only pick 3 or 4
               // Window.alert("Minor deactivate: " + label.toString());
                active = false;
            }
            record.setEnabled(active);
            if(active)
            record.setAttribute("display", label.toString());
            else{
                record.setAttribute("display", label.toString());
                record.setCustomStyle("background-color-white");
            }
            reclist.add(record);

        }
        return reclist;
    }

    private LinkedHashMap<String, String> buildCareteamsMap(LinkedHashMap<String, String> baseData) {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
//        data.put("", "");

        for (String tempKey : baseData.keySet()) {
            String tempValue = baseData.get(tempKey);

            tempKey = Constants.DELIMITER_SELECT_ITEM + tempKey;
            data.put(tempKey, tempValue);
        }

        return data;
    }


    private LinkedHashMap<String, String> buildPayerPlanNamesMap(LinkedHashMap<String, String> baseData) {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
        data.put("", "");
        for (String tempKey : baseData.keySet()) {
            String tempValue = baseData.get(tempKey);
            tempKey = Constants.DELIMITER_SELECT_ITEM + tempKey;
            data.put(tempKey, tempValue);
        }
        return data;

    }

    private LinkedHashMap<String, String> buildPatientRelationshipMap(LinkedHashMap<Long, String> baseData) {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

        for (Long tempKey : baseData.keySet()) {
            String tempValue = baseData.get(tempKey);

            String theKey = Constants.DELIMITER_SELECT_ITEM + tempKey;
            data.put(theKey, tempValue);
        }

        return data;
    }

    public boolean validateRequiredFieildsForDupPatients() {
        boolean firstNameValid = firstNameTextItem.validate();
        boolean lastNameValid = lastNameTextItem.validate();
        boolean dobValid = dobDateItem.validate();
        boolean genderVaild = genderSelectItem.validate();

        return (firstNameValid && lastNameValid && dobValid && genderVaild);
    }

  class SsnBlurHandler implements BlurHandler {

        public void onBlur(BlurEvent event) {
            validSsnEntered = ssnTextItem.validate();

            if (validSsnEntered) {
                realSsnValue = ssnTextItem.getValueAsString();
                removeSsnMaskAndValidators();

                if (StringUtils.isNotBlank(realSsnValue)) {
                    ssnTextItem.setValue(SsnUtils.getMaskedSsn(realSsnValue));
                }

                enteredValue = null;
            } else {
                enteredValue = ssnTextItem.getEnteredValue();

                if (StringUtils.isNotBlank(enteredValue)) {
                    // remove the dashes
                    enteredValue = enteredValue.replaceAll("\\-", "");
                }
            }
        }
    }

    class SsnFocusHandler implements FocusHandler {

        public void onFocus(FocusEvent event) {
            setSsnMaskAndValidators();

            if (validSsnEntered) {
                if (StringUtils.isNotBlank(realSsnValue)) {
                    ssnTextItem.setValue(realSsnValue);
                }
            } else {
                ssnTextItem.setValue(enteredValue);
            }

        }
    }

    class OtherPatientIdsClickHandler implements ClickHandler {

        public void onClick(ClickEvent ce) {
            PatientOtherIdsWindow window = new PatientOtherIdsWindow(person.getPatientEnrollment().getPatientId(), communityId);
            window.show();
        }
    }

    private void populateEnrollStatusChangeDate(Date enrollStatusChangeDate) {
        DateTimeFormat enrollStatusChangeDateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
        String enrollStatusChangeDateStr = enrollStatusChangeDateFormatter.format(enrollStatusChangeDate);
        enrollmentChangeDateStaticTextItem.setValue(enrollStatusChangeDateStr);
    }

    class ManagePatientProgramClickHandler implements ClickHandler {

        public void onClick(ClickEvent ce) {
            person = getPerson();
            ManagePatientProgramWindow window = new ManagePatientProgramWindow(person);
            window.show();
        }
    }

//    class dobBlurHandler implements BlurHandler {
//
//        @Override
//        public void onBlur(BlurEvent event) {
//            enableMinorFields(dobDateItem.getValueAsDate());
//        }
//    }
//

    public Date getDOB() {
        Date dob = dobDateItem.getValueAsDate();
        return dob;

    }
    
    @Override
    public void onUpdateUserContext(UpdateUserContextEvent updateUserContextEvent){
    }
  
    private boolean isIE11Browser(){
        return Window.Navigator.getUserAgent().toLowerCase().contains("trident") ;
     }
    
    private void setFormWidth(DynamicForm theForm){
        if(isIE11Browser()){
             theForm.addStyleName("width-update-patient-form");
        }else{
             theForm.setWidth(FORM_WIDTH);
        }
    }
}
