package com.gsihealth.dashboard.client.enrollment;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.gsihealth.dashboard.client.common.Cancelable;
import com.gsihealth.dashboard.client.enrollment.callbacks.GetOrganizationNamesCallback;
import com.gsihealth.dashboard.client.service.OrganizationService;
import com.gsihealth.dashboard.client.service.OrganizationServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.client.util.ValidationUtils;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.util.SsnUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.RowSpacerItem;
import com.smartgwt.client.widgets.form.fields.SectionItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.validator.DateRangeValidator;
import com.smartgwt.client.widgets.form.validator.LengthRangeValidator;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import java.util.Date;
import java.util.LinkedHashMap;
import com.gsihealth.dashboard.client.common.FormUtils;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import java.util.ArrayList;
import java.util.Collection;
import com.gsihealth.dashboard.entity.dto.ProgramNameDTO;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;

/**
 *
 * @author Chad Darby
 */
@Deprecated
public class PatientSearchForm extends DynamicForm implements Cancelable {

    private TextItem lastNameTextItem;
    private TextItem firstNameTextItem;
    private TextItem middleNameTextItem;
    private DateItem dateOfBirthDateItem;
    private SelectItem genderSelectItem;
    private TextItem address1TextItem;
    private TextItem address2TextItem;
    private TextItem cityTextItem;
    private TextItem zipCodeTextItem;
    private SelectItem stateSelectItem;
    private TextItem phoneTextItem;
    private SelectItem programLevelConsentStatusSelectItem;
    private SelectItem programNameSelectItem;
    private SelectItem enrollmentStatusSelectItem;
    private SelectItem minorSelectItem;
    private SelectItem organizationSelectItem;
    private OrganizationServiceAsync orgService = GWT.create(OrganizationService.class);
    private DateTimeFormat dateFormatter;
    private SectionItem organizationSectionItem;
    private ClickHandler searchClickHandler;
    private TextItem SSNItem;
    private TextItem medicaidItem;
    private SectionItem insuranceSectionItem;
    private String[] PLCS_NO_ENROLLMENT_STATUSES = {"Inactive"};
    private String[] PLCS_YES_ENROLLMENT_STATUSES = {"Pending","Enrolled", "Assigned"};
    private LengthRangeValidator ssnLengthValidator;
    private String plcsTitle;
//    private boolean enrolledMode;
    private boolean navigationChanged = false;
//    public static final String[] STATUS_CODES_FOR_PLCS_CANDIDATES = {"Pending","Enrolled", "Assigned"};
    public static final String[] MINOR_CODES_FOR_CANDIDATES = {"Yes", "No"};
    public static final String[] STATUS_CODES_FOR_PATIENTS = {"Pending", "Inactive","Enrolled", "Assigned"};
    private TextItem patientIDItem;
    private SelectItem programHealthHomeSelectItem;
    private String healthHomeLabel;
    private boolean requireChh;
    private CheckboxItem searchMorePatientsCheckBoxItem;

    public PatientSearchForm(ClickHandler theSearchClickHandler , CheckboxItem searchMorePatientsCheckBoxItem) {

        searchClickHandler = theSearchClickHandler;
        this.searchMorePatientsCheckBoxItem = searchMorePatientsCheckBoxItem;
        dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");

        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
        healthHomeLabel = props.getHealthHomeLabel();
        requireChh = props.isChildrensHealthHome();
        long communityId=ApplicationContextUtils.getLoginResult().getCommunityId();
        buildGui();

        loadOrganizationNames(communityId);

    }

    private void buildGui() {

        // getting PLCS title from prop file.
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        plcsTitle = loginResult.getPlcsTitle();

        KeyPressHandler searchKeyPressHandler = new SearchKeyPressHandler();

        setBrowserSpellCheck(false);

        RegExpValidator alphaOnlyRegExpValidator = ValidationUtils.getAlphaOnlyRegExpValidator();
        RegExpValidator alphaHGAARegExValidator = ValidationUtils.getAlphaHGAARegExValidator();
        RegExpValidator alphaNumberRegExValidator = ValidationUtils.getAlphaNumberRegExpValidator();

        //
        // Demographics
        //
        SectionItem demographicsSectionItem = buildSectionItem("Demographics");

        lastNameTextItem = new TextItem("lastName", "Last Name");
        lastNameTextItem.setRequired(false);
        ValidationUtils.setValidators(lastNameTextItem, alphaHGAARegExValidator, false);
        lastNameTextItem.addKeyPressHandler(searchKeyPressHandler);


        firstNameTextItem = new TextItem("firstName", "First Name");
        ValidationUtils.setValidators(firstNameTextItem, alphaHGAARegExValidator, true);
        firstNameTextItem.addKeyPressHandler(searchKeyPressHandler);

        middleNameTextItem = new TextItem("middleName", "Middle Name");
        ValidationUtils.setValidators(middleNameTextItem, alphaHGAARegExValidator, true);
        middleNameTextItem.addKeyPressHandler(searchKeyPressHandler);


        dateOfBirthDateItem = buildDateItem("dateOfBirth", "Date of Birth");

        genderSelectItem = new SelectItem("gender", "Gender");
        genderSelectItem.setDefaultValue("");
        genderSelectItem.setValueMap(ApplicationContextUtils.getGenderCodes());


        // Length validator for SSN
        ssnLengthValidator = new LengthRangeValidator();
        ssnLengthValidator.setMax(SsnUtils.SSN_LENGTH);
        ssnLengthValidator.setErrorMessage("Invalid length");

        SSNItem = new TextItem("SSN", "SSN");
        SSNItem.setMask("###-##-####");
        SSNItem.setMaskOverwriteMode(true);
        SSNItem.setValidators(ssnLengthValidator);


        SSNItem.addKeyPressHandler(searchKeyPressHandler);

        //patient id for search

        patientIDItem = new TextItem("PatientId", "Patient ID");
        patientIDItem.setMask("#########");
        patientIDItem.setMaskOverwriteMode(true);
        
        minorSelectItem = new SelectItem("minor", "Minor");
        this.setMinorValues(MINOR_CODES_FOR_CANDIDATES);
        minorSelectItem.setDefaultValue("");

        LengthRangeValidator patientIDValidator = new LengthRangeValidator();
        patientIDValidator.setMax(9);
        patientIDValidator.setMin(1);
        patientIDItem.setValidators(patientIDValidator);
        patientIDItem.addKeyPressHandler(searchKeyPressHandler);
        
        searchMorePatientsCheckBoxItem.setWrapTitle(false);
         searchMorePatientsCheckBoxItem.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                if (((Boolean) event.getValue())) {
                    firstNameTextItem.setTitle("* First Name");
                    lastNameTextItem.setTitle("* Last Name");
                    firstNameTextItem.setRequired(true);
                    lastNameTextItem.setRequired(true);
                    firstNameTextItem.redraw();
                    lastNameTextItem.redraw();
                } else {
                    firstNameTextItem.setTitle("First Name");
                    lastNameTextItem.setTitle("Last Name");
                    firstNameTextItem.setRequired(false);
                    lastNameTextItem.setRequired(false);
                    firstNameTextItem.redraw();
                    lastNameTextItem.redraw();
                    validateFirstNameAndLastName();
                }
            
            }
        });

        if(requireChh)
        {
        demographicsSectionItem.setItemIds(lastNameTextItem.getName(), firstNameTextItem.getName(), middleNameTextItem.getName(), dateOfBirthDateItem.getName(), genderSelectItem.getName(), SSNItem.getName(), patientIDItem.getName(),minorSelectItem.getName());
        }else {
        demographicsSectionItem.setItemIds(lastNameTextItem.getName(), firstNameTextItem.getName(), middleNameTextItem.getName(), dateOfBirthDateItem.getName(), genderSelectItem.getName(), SSNItem.getName(), patientIDItem.getName());

        }
        //
        // Address
        //
        RegExpValidator alphaNumberSpecialSymbolsRegExpValidator = ValidationUtils.getAlphaNumberSpecialSymbolsRegExpValidator();

        SectionItem addressSectionItem = buildSectionItem("Address");
        addressSectionItem.setSectionExpanded(false);
        address1TextItem = new TextItem("address1", "Address 1");
        ValidationUtils.setValidators(address1TextItem, alphaNumberSpecialSymbolsRegExpValidator, true);
        address1TextItem.addKeyPressHandler(searchKeyPressHandler);

        address2TextItem = new TextItem("address2", "Address 2");
        ValidationUtils.setValidators(address2TextItem, alphaNumberSpecialSymbolsRegExpValidator, true);
        address2TextItem.addKeyPressHandler(searchKeyPressHandler);


        cityTextItem = new TextItem("city", "City");
        ValidationUtils.setValidators(cityTextItem, alphaOnlyRegExpValidator, true);

        stateSelectItem = new SelectItem("state", "State");
        stateSelectItem.setDefaultValue("");
        stateSelectItem.setValueMap(ApplicationContextUtils.getStateCodes());

        zipCodeTextItem = buildZipCodeTextItem();
        zipCodeTextItem.addKeyPressHandler(searchKeyPressHandler);

        phoneTextItem = buildPhoneTextItem();
        phoneTextItem.addKeyPressHandler(searchKeyPressHandler);

        addressSectionItem.setItemIds(address1TextItem.getName(), address2TextItem.getName(), cityTextItem.getName(), stateSelectItem.getName(), zipCodeTextItem.getName(), phoneTextItem.getName());


        //
        // Enrollment
        //
        SectionItem enrollmentSectionItem = buildSectionItem("Enrollment");
        enrollmentSectionItem.setSectionExpanded(false);

        programLevelConsentStatusSelectItem = new SelectItem("programLevel", plcsTitle);
        programLevelConsentStatusSelectItem.setDefaultValue("");
        programLevelConsentStatusSelectItem.setValueMap(ApplicationContextUtils.getProgramLevelConsentStatusCodes());
        // adding handler
        programLevelConsentStatusSelectItem.addChangedHandler(new ProgramLevelConsentStatusChangedHandler());


        //Health Home search
          programHealthHomeSelectItem=new SelectItem("healthHome", healthHomeLabel);
          programHealthHomeSelectItem.setValueMap(ApplicationContextUtils.getHealthHomeCodes());
          programHealthHomeSelectItem.setDefaultValue("");
        programNameSelectItem = new SelectItem("programName", "Program Name");
        programNameSelectItem.setValueMap(ApplicationContextUtils.getProgramNameValue());
        programNameSelectItem.setDefaultValue("");

        enrollmentStatusSelectItem = new SelectItem("enrollmentStatus", "Enrollment Status");
        this.setEnrollmentStatusValues(STATUS_CODES_FOR_PATIENTS);
        enrollmentStatusSelectItem.setDefaultValue("");

        enrollmentSectionItem.setItemIds(programLevelConsentStatusSelectItem.getName(), getProgramHealthHomeSelectItem().getName(), programNameSelectItem.getName(), enrollmentStatusSelectItem.getName());

        //
        // Organization
        //
        organizationSectionItem = buildSectionItem("Organization");
        organizationSectionItem.setSectionExpanded(false);
        organizationSectionItem.setVisible(true);

        organizationSelectItem = new SelectItem();
        organizationSelectItem.setTitle("Organization");
        organizationSelectItem.setVisible(false);

        organizationSectionItem.setItemIds(organizationSelectItem.getName());


        // Insurance section
        insuranceSectionItem = buildSectionItem("Insurance");
        insuranceSectionItem.setSectionExpanded(false);

        medicaidItem = new TextItem("MedicaidItem", "Medicaid/Medicare/Payer ID");
        medicaidItem.addKeyPressHandler(searchKeyPressHandler);
        ValidationUtils.setValidators(medicaidItem, alphaNumberRegExValidator, false);

        insuranceSectionItem.setItemIds(medicaidItem.getName());


        RowSpacerItem rowSpacerItem = new RowSpacerItem();
        if (requireChh){
        this.setFields(demographicsSectionItem, lastNameTextItem, firstNameTextItem, middleNameTextItem, dateOfBirthDateItem, genderSelectItem, SSNItem, patientIDItem,minorSelectItem, rowSpacerItem,
                addressSectionItem, address1TextItem, address2TextItem, cityTextItem, stateSelectItem, zipCodeTextItem, phoneTextItem, rowSpacerItem,
                enrollmentSectionItem, programLevelConsentStatusSelectItem, getProgramHealthHomeSelectItem(), programNameSelectItem, enrollmentStatusSelectItem, rowSpacerItem,
                insuranceSectionItem, medicaidItem, rowSpacerItem,
                organizationSectionItem, organizationSelectItem, rowSpacerItem);
        }
        else {
        this.setFields(demographicsSectionItem, lastNameTextItem, firstNameTextItem, middleNameTextItem, dateOfBirthDateItem, genderSelectItem, SSNItem, patientIDItem, rowSpacerItem,
                addressSectionItem, address1TextItem, address2TextItem, cityTextItem, stateSelectItem, zipCodeTextItem, phoneTextItem, rowSpacerItem,
                enrollmentSectionItem, programLevelConsentStatusSelectItem, getProgramHealthHomeSelectItem(), programNameSelectItem, enrollmentStatusSelectItem, rowSpacerItem,
                insuranceSectionItem, medicaidItem, rowSpacerItem,
                organizationSectionItem, organizationSelectItem, rowSpacerItem);
        }
    }

    public SelectItem getProgramNameSelectItem() {
        return programNameSelectItem;
    }

    public void setProgramNameSelectItem(SelectItem programNameSelectItem) {
        this.programNameSelectItem = programNameSelectItem;
    }

    /**
     * @return the programHealthHomeSelectItem
     */
    public SelectItem getProgramHealthHomeSelectItem() {
        return programHealthHomeSelectItem;
    }

    /**
     * @param programHealthHomeSelectItem the programHealthHomeSelectItem to set
     */
    public void setProgramHealthHomeSelectItem(SelectItem programHealthHomeSelectItem) {
        this.programHealthHomeSelectItem = programHealthHomeSelectItem;
    }

    class HealthHomeProgramChangedHandler implements ChangedHandler {

        public void onChanged(ChangedEvent event) {
            String healthHomeWithDelimiter = (String) event.getValue();
            String healthHome = healthHomeWithDelimiter.split(Constants.DELIMITER_SELECT_ITEM)[1];
            LinkedHashMap<Long, ProgramNameDTO> programNameHealthHomeMap = ApplicationContextUtils.getProgramNameForHealthHomeValue();

            Collection<ProgramNameDTO> dto = programNameHealthHomeMap.values();
            ArrayList<ProgramNameDTO> programList = new ArrayList<ProgramNameDTO>(dto);

            String[] programName = FormUtils.convertProgramNameHealthHome(healthHome, programList);
            String[] programNameCodes = FormUtils.getCodesWithAddedBlankOption(programName);

            programNameSelectItem.setValue(FormUtils.EMPTY);
            programNameSelectItem.setValueMap(programNameCodes);


        }
    }

    private TextItem buildPhoneTextItem() {
        TextItem theTextItem = new TextItem("phone", "Telephone #");

        theTextItem.setValidateOnExit(false);
        theTextItem.setWidth(100);
        theTextItem.setMask("###-###-####");

        LengthRangeValidator workPhoneValidator = new LengthRangeValidator();
        workPhoneValidator.setMax(10);
        workPhoneValidator.setMin(10);
        workPhoneValidator.setErrorMessage("Invalid length");
        theTextItem.setValidators(workPhoneValidator);

        return theTextItem;
    }

    private TextItem buildZipCodeTextItem() {
        TextItem theTextItem = new TextItem("zipCode", "Zip Code");

        String zipCodeMask = "#####";
        theTextItem.setMask(zipCodeMask);
        LengthRangeValidator zipCodeValidator = new LengthRangeValidator();
        zipCodeValidator.setMax(5);
        zipCodeValidator.setMin(5);
        zipCodeValidator.setErrorMessage("Invalid length");
        theTextItem.setValidators(zipCodeValidator);

        return theTextItem;
    }

    private SectionItem buildSectionItem(String title) {
        SectionItem sectionItem = new SectionItem();
        sectionItem.setDefaultValue(title);  // the title of section
        sectionItem.setSectionExpanded(true);

        return sectionItem;
    }

    private DateItem buildDateItem(String id, String title) {
        DateItem tempDateItem = new DateItem(id);

        tempDateItem.setTitle(title);
        tempDateItem.setEnforceDate(true);
        tempDateItem.setDateFormatter(DateDisplayFormat.TOUSSHORTDATE);
        tempDateItem.setUseTextField(true);
        tempDateItem.setTextAlign(Alignment.CENTER);
        tempDateItem.setUseMask(true);

        tempDateItem.setWidth(100);
        tempDateItem.setStartDate(dateFormatter.parse("01/01/1900"));

        // add validation rules
        DateRangeValidator dateRangeValidator = new DateRangeValidator();
        Date today = new java.util.Date();
        dateRangeValidator.setMax(today);

        dateRangeValidator.setErrorMessage("Date must be today's date or prior.");
        tempDateItem.setValidators(dateRangeValidator);
        tempDateItem.setValidateOnChange(true);

        return tempDateItem;
    }

    protected String getDateOfBirthString(Date dateOfBirth) {
        String result = null;

        result = dateFormatter.format(dateOfBirth);

        return result;
    }

    public SearchCriteria getSearchCriteria() {
        SearchCriteria searchCriteria = new SearchCriteria();

        searchCriteria.setLastName(StringUtils.trim(lastNameTextItem.getValueAsString()));
        searchCriteria.setFirstName(StringUtils.trim(firstNameTextItem.getValueAsString()));
        searchCriteria.setMiddleName(StringUtils.trim(middleNameTextItem.getValueAsString()));

        Date dateOfBirth = dateOfBirthDateItem.getValueAsDate();
        if (dateOfBirth != null) {
            String dateOfBirthStr = getDateOfBirthString(dateOfBirth);
            searchCriteria.setDateOfBirthStr(dateOfBirthStr);
        }

        searchCriteria.setGender(genderSelectItem.getValueAsString());
        searchCriteria.setMinor(minorSelectItem.getValueAsString());
        searchCriteria.setSearchAllPatients(searchMorePatientsCheckBoxItem.getValueAsBoolean());
        
        searchCriteria.setAddress1(StringUtils.trim(address1TextItem.getValueAsString()));
        searchCriteria.setAddress2(StringUtils.trim(address2TextItem.getValueAsString()));
        searchCriteria.setCity(StringUtils.trim(cityTextItem.getValueAsString()));
        searchCriteria.setState(StringUtils.trim(stateSelectItem.getValueAsString()));
        searchCriteria.setZipCode(StringUtils.trim(zipCodeTextItem.getValueAsString()));
        searchCriteria.setPhone(StringUtils.trim(phoneTextItem.getValueAsString()));

        searchCriteria.setProgramLevelConsentStatus(programLevelConsentStatusSelectItem.getValueAsString());
        String programNameValue = getProgramNameSelectItem().getValueAsString();
        if (!StringUtils.isBlank(programNameValue)) {

            if (programNameValue.startsWith("_")) {
                String programName = programNameValue.split(Constants.DELIMITER_SELECT_ITEM)[1];                
            }
            searchCriteria.setProgramNameId(StringUtils.trim(programNameValue));
            }
        
        searchCriteria.setEnrollmentStatus(enrollmentStatusSelectItem.getValueAsString());

        String organizationIdStr = organizationSelectItem.getValueAsString();
        if (StringUtils.isNotBlank(organizationIdStr)) {
            organizationIdStr = organizationIdStr.split(Constants.DELIMITER_SELECT_ITEM)[1];
        }

        if (!StringUtils.isBlank(organizationIdStr)) {
            long organizationId = Long.parseLong(organizationIdStr);
            searchCriteria.setOrganizationId(organizationId);
        }

        searchCriteria.setSsn(StringUtils.trim(SSNItem.getValueAsString()));
        searchCriteria.setMediCaidCareId(StringUtils.trim(medicaidItem.getValueAsString()));
        searchCriteria.setPatientID(StringUtils.trim(patientIDItem.getValueAsString()));
       
        LinkedHashMap<Long, String> healthHomeMap = ApplicationContextUtils.getHealthHomeCodes();
        String healthHomeValue = getProgramHealthHomeSelectItem().getValueAsString();
        if (healthHomeValue != null && !StringUtils.isBlank(healthHomeValue)) {
            searchCriteria.setHealthHome(healthHomeMap.get(Long.parseLong(healthHomeValue)));
        }
        return searchCriteria;
    }

    @Override
    public boolean validate() {
        boolean valid = super.validate();
        return valid;
    }
    
    public boolean validateForExtraPatientSearch() {
        boolean valid = true;
        if(isSearchMorePatientsCheckBoxItemSelected()){
            return firstNameTextItem.getValueAsString() != null&& firstNameTextItem.getValueAsString().length()> 0 
                    && lastNameTextItem.getValueAsString()!= null &&lastNameTextItem.getValueAsString().length()> 0;
        }else{
           return valid;
        }     
    }

    public void setEnrollmentStatusValues(String[] valueMap) {
        enrollmentStatusSelectItem.setValueMap(valueMap);
    }
     public void setMinorValues(String[] valueMap) {
        minorSelectItem.setValueMap(valueMap);
    }

    public void setProgramLevelConsentStatusValues(String[] values) {
        programLevelConsentStatusSelectItem.setValueMap(values);
    }

    public void clearValueProgramLevelConsentStatus() {
        programLevelConsentStatusSelectItem.clearValue();
    }

    public void selectProgramLevelConsentStatus(String value) {
        programLevelConsentStatusSelectItem.setValue(value);
    }

    public TextItem getLastNameTextItem() {
        return lastNameTextItem;
    }

    public void clearEnrollmentStatus() {
        enrollmentStatusSelectItem.clearValue();
        enrollmentStatusSelectItem.setDisabled(false);
    }
      public void clearMinor() {
        minorSelectItem.clearValue();
        minorSelectItem.setDisabled(false);
    }

    /**
     * Loads the organization names
     */
    private void loadOrganizationNames(long communityId) {
        orgService.getOrganizationNamesForSearchForm(communityId,new GetOrganizationNamesCallback(organizationSelectItem));
    }

//    public void setVisibleOrganizationSection(boolean flag) {
//
//        if (flag) {
//            organizationSectionItem.show();
//            organizationSelectItem.show();
//        } else {
//            organizationSectionItem.hide();
//            organizationSelectItem.hide();
//        }
//    }

    class SearchKeyPressHandler implements KeyPressHandler {

        @Override
        public void onKeyPress(KeyPressEvent event) {
            if (event.getKeyName().equals(Constants.ENTER_KEY)) {
                searchClickHandler.onClick(null);
            }
        }
    }

    class ProgramLevelConsentStatusChangedHandler implements ChangedHandler {

        public void onChanged(ChangedEvent event) {
            String value = event.getValue().toString();

            if (StringUtils.equalsIgnoreCase(value, "No")) {
                enrollmentStatusSelectItem.setValueMap(PLCS_NO_ENROLLMENT_STATUSES);
                enrollmentStatusSelectItem.setValue("");
                enrollmentStatusSelectItem.setDisabled(false);
            } else if (StringUtils.equalsIgnoreCase(value, "Yes")) {
//                if (navigationChanged && getEnrolledMode()) {
                    enrollmentStatusSelectItem.setValueMap(PLCS_YES_ENROLLMENT_STATUSES);
                    enrollmentStatusSelectItem.setDisabled(false);
                    enrollmentStatusSelectItem.setValue("");

//                } else {
//                    enrollmentStatusSelectItem.setValueMap(PLCS_YES_ENROLLMENT_STATUSES);
//                    enrollmentStatusSelectItem.setValue("");
//                    enrollmentStatusSelectItem.setDisabled(false);
//                }
            } else if (StringUtils.equalsIgnoreCase(value, "Unknown") || (StringUtils.isBlank(value))) {
                enrollmentStatusSelectItem.setValue("Pending");
                enrollmentStatusSelectItem.setDisabled(true);

            }




        }
    }

//    public void setEnrolledMode(boolean theEnrolledMode) {
//        enrolledMode = theEnrolledMode;
//        navigationChanged = true;
//
//    }

//    public boolean getEnrolledMode() {
//
//        return enrolledMode;
//    }

    public void clearSearchValues() {
        lastNameTextItem.clearValue();
        firstNameTextItem.clearValue();
        middleNameTextItem.clearValue();
        dateOfBirthDateItem.clearValue();
        genderSelectItem.clearValue();
        address1TextItem.clearValue();
        address2TextItem.clearValue();
        cityTextItem.clearValue();
        zipCodeTextItem.clearValue();
        stateSelectItem.clearValue();
        phoneTextItem.clearValue();
        programLevelConsentStatusSelectItem.clearValue();
        programNameSelectItem.clearValue();
        programHealthHomeSelectItem.clearValue();
        enrollmentStatusSelectItem.clearValue();
        if (requireChh){
            minorSelectItem.clearValue();//FIXME too much distributed code
        }
        organizationSelectItem.clearValue();
        SSNItem.clearValue();
        medicaidItem.clearValue();
        insuranceSectionItem.clearValue();
        patientIDItem.clearValue();
        searchMorePatientsCheckBoxItem.clearValue();
        firstNameTextItem.setTitle("First Name");
        lastNameTextItem.setTitle("Last Name");
        firstNameTextItem.setRequired(false);
        lastNameTextItem.setRequired(false);
        firstNameTextItem.redraw();
        lastNameTextItem.redraw();
        validateFirstNameAndLastName();
    }
    
    public boolean isSearchMorePatientsCheckBoxItemSelected(){
        return searchMorePatientsCheckBoxItem.getValueAsBoolean();
    }
    
    public void validateFirstNameAndLastName(){
        firstNameTextItem.validate();
        lastNameTextItem.validate();
    }
}
