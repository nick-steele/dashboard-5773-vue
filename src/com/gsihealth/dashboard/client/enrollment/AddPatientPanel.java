package com.gsihealth.dashboard.client.enrollment;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.callbacks.BasicPortalAsyncCallback;
import com.gsihealth.dashboard.client.callbacks.CheckIfDuplicatePatientCallback;
import com.gsihealth.dashboard.client.common.PersonInfo;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.service.CareteamService;
import com.gsihealth.dashboard.client.service.CareteamServiceAsync;
import com.gsihealth.dashboard.client.service.EnrollmentService;
import com.gsihealth.dashboard.client.service.EnrollmentServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.PropertyUtils;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.patientmdminterface.entity.DuplicateCheckProcessResult;
import com.gsihealth.patientmdminterface.entity.DuplicateCheckProcessResult.ProcessDecision;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;

import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class AddPatientPanel extends BasePatientPanel {

    private PatientForm patientForm;
    private Button topSaveButton;
    private Button topCancelButton;
    private Button topCheckDuplicateButton;
    private Button bottomSaveButton;
    private Button bottomCancelButton;
    private Button bottomCheckDuplicateButton;
    private ProgressBarWindow progressBarWindow;
    private final EnrollmentServiceAsync enrollmentService = GWT.create(EnrollmentService.class);
    private CareteamServiceAsync careteamService = GWT.create(CareteamService.class);
    private boolean programNameRequired;
    private boolean healthHomeRequired;
    private boolean requireMedicaidID ;
    private boolean requireMedicareID ;
    private boolean childrensHealthHome=false; 
    private boolean minorFlag=false;
    private boolean canConsentEnrollMinors=false;
    
    //JIRA 2523
    private String daeCloseButtonText;
    private String daeDescLabelText;
    private String daeSelectButtonText;
    private String daeTitleLabelText;
    private String dseAddNowButtonText;
    private String dseDescLabelText;
    private String dseSelectButtonText;
    private String dseTitleLabelText;
    
    ClientApplicationProperties props;
    
    //Spira 7215
    private String mpiSearch;
    
    public AddPatientPanel() {
    	props = ApplicationContextUtils.getClientApplicationProperties();
        buildGui(null);
        setFormValidationRulesFromClientAppProperties();
        DashBoardApp.restartTimer();
    }

    public AddPatientPanel(PersonInfo thePerson) {
    	props = ApplicationContextUtils.getClientApplicationProperties();
        buildGui(thePerson);
        setFormValidationRulesFromClientAppProperties();
        DashBoardApp.restartTimer();
    }

    protected void setFormValidationRulesFromClientAppProperties() {
         programNameRequired = props.isProgramNameRequired();
         healthHomeRequired=props.isHealthHomeRequired();
         requireMedicaidID = props.isRequireMedicaidID();
         requireMedicareID = props.isRequireMedicareID();
         childrensHealthHome=props.isChildrensHealthHome();
         
        //JIRA 2523
        daeCloseButtonText = props.getDaeCloseButtonText();
        daeDescLabelText = props.getDaeDescLabelText();
        daeSelectButtonText = props.getDaeSelectButtonText();
        daeTitleLabelText = props.getDaeTitleLabelText();
        dseAddNowButtonText = props.getDseAddNowButtonText();
        dseDescLabelText = props.getDseDescLabelText();
        dseSelectButtonText = props.getDseSelectButtonText();
        dseTitleLabelText = props.getDseTitleLabelText();
        
        //Spira 7215
        mpiSearch = props.getMirthSearchVisible();
    }
    private void buildGui(PersonInfo personInfo) {

        setPadding(5);
        setMembersMargin(7);

        Label label = new Label("Add Patient");
        label.setWidth(300);
        label.setHeight(10);
        label.setStyleName("patientPanelTitleLabel");
        addMember(label);

        Label labelNote = new Label("<b>* indicates required field</b><br> <b>** indicates required for duplicate patient check</b>");
        labelNote.setWidth(300);
        labelNote.setHeight(5);

        addMember(labelNote);

        Layout topButtonBar = buildTopButtonBar();
        addMember(topButtonBar);

        patientForm = new PatientForm(this);
        if (personInfo != null) {
            patientForm.populatePersonInfo(personInfo);
        }
        addMember(patientForm);

        Layout bottomButtonBar = buildBottomButtonBar();
        addMember(bottomButtonBar);

        this.setOverflow(Overflow.AUTO);
    }

    private Layout buildTopButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        //buttonLayout.setPadding(10);
        //buttonLayout.setMargin(20);

        buttonLayout.setWidth(950);
        buttonLayout.setAlign(Alignment.CENTER);

        topCheckDuplicateButton = new Button(props.getDemographicCheckDuplicateButton());
        topSaveButton = new Button(props.getDemographicSaveButton());
        topCancelButton = new Button(props.getDemographicCancelButton());

        buttonLayout.addMember(topCheckDuplicateButton);
        buttonLayout.addMember(topSaveButton);
        buttonLayout.addMember(topCancelButton);


        topCheckDuplicateButton.addClickHandler(new CheckIfDuplicateClickHandler());
        topSaveButton.addClickHandler(new AddPatientClickHandler());
        topCancelButton.addClickHandler(new CancelClickHandler());

        return buttonLayout;
    }

    private Layout buildBottomButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        //buttonLayout.setPadding(10);
        //buttonLayout.setMargin(10);

        buttonLayout.setWidth(950);
        buttonLayout.setAlign(Alignment.CENTER);

        bottomCheckDuplicateButton = new Button(props.getDemographicCheckDuplicateButton());
        bottomSaveButton = new Button(props.getDemographicSaveButton());
        bottomCancelButton = new Button(props.getDemographicCancelButton());

        viewCareTeamButton = new Button("View Care Team");
        viewCareTeamButton.setVisible(false);

        modifyCareTeamButton = new Button("Modify Care Team");
        modifyCareTeamButton.setVisible(false);

        addCareTeamButton = new Button("Create Care Team");
        addCareTeamButton.setVisible(false);

       
        buttonLayout.addMember(viewCareTeamButton);
        buttonLayout.addMember(modifyCareTeamButton);
        buttonLayout.addMember(addCareTeamButton);
        buttonLayout.addMember(bottomCheckDuplicateButton);
        buttonLayout.addMember(bottomSaveButton);
        buttonLayout.addMember(bottomCancelButton);

        bottomCheckDuplicateButton.addClickHandler(new CheckIfDuplicateClickHandler());
        bottomSaveButton.addClickHandler(new AddPatientClickHandler());
        bottomCancelButton.addClickHandler(new CancelClickHandler());
        return buttonLayout;
    }
    
    private void setSaveCancelButtons(boolean disable){
        topSaveButton.setDisabled(disable);
        topCancelButton.setDisabled(disable);
        topCheckDuplicateButton.setDisabled(disable);
        bottomSaveButton.setDisabled(disable);
        bottomCancelButton.setDisabled(disable); 
        bottomCheckDuplicateButton.setDisabled(disable);
    }

    class AddPatientClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            if (patientForm.validate()) {
                PersonDTO person = patientForm.getPerson();
                canConsentEnrollMinors = ApplicationContextUtils.getLoginResult().isCanConsentEnrollMinors();
                minorFlag = PropertyUtils.isMinor(person.getDateOfBirth());
//                if (person.getPatientEnrollment().getPrimaryPayerClass()!= null && person.getPatientEnrollment().getSecondaryPayerClass()!=null){
                if ((person.getPatientEnrollment().getPrimaryPayerClass()!= null && requireMedicaidID == true  && patientForm.primaryPayerClassValues().equalsIgnoreCase("Medicaid")  && person.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId() == null) || (person.getPatientEnrollment().getSecondaryPayerClass()!=null && requireMedicaidID == true  && patientForm.secondaryPayerClassValues().equalsIgnoreCase("Medicaid") && person.getPatientEnrollment().getSecondaryPayerMedicaidMedicareId() == null)) {
//                     if (requireMedicaidID = true && person.getPatientEnrollment().getPrimaryPayerClass() != null||person.getPatientEnrollment().getSecondaryPayerClass() != null &&  person.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId() == null|| person.getPatientEnrollment().getSecondaryPayerMedicaidMedicareId() == null){       
                    SC.warn("Please enter Medicaid ID");
                    return;
                }

                if (( person.getPatientEnrollment().getPrimaryPayerClass()!= null && requireMedicareID == true && patientForm.primaryPayerClassValues().equalsIgnoreCase("Medicare") && person.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId() == null) || (person.getPatientEnrollment().getSecondaryPayerClass()!=null && requireMedicareID == true && patientForm.secondaryPayerClassValues().equalsIgnoreCase("Medicare") && person.getPatientEnrollment().getSecondaryPayerMedicaidMedicareId() == null)) {
//                     if (requireMedicaidID = true && person.getPatientEnrollment().getPrimaryPayerClass() != null||person.getPatientEnrollment().getSecondaryPayerClass() != null &&  person.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId() == null|| person.getPatientEnrollment().getSecondaryPayerMedicaidMedicareId() == null){       
                  
                    SC.warn("Please enter Medicare ID");
                    return;
                }

                if (( person.getPatientEnrollment().getPrimaryPayerClass()!= null && patientForm.primaryPayerClassValues().equalsIgnoreCase("Medicare") && person.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId() != null && person.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId().matches(Constants.MEDICAID_ID_ALPHA_NUMBER_FORMAT_REGEX)) || 
                        (person.getPatientEnrollment().getSecondaryPayerClass()!=null && patientForm.secondaryPayerClassValues().equalsIgnoreCase("Medicare") && person.getPatientEnrollment().getSecondaryPayerMedicaidMedicareId() != null && person.getPatientEnrollment().getSecondaryPayerMedicaidMedicareId().matches(Constants.MEDICAID_ID_ALPHA_NUMBER_FORMAT_REGEX))) {
                  
                     SC.warn("Medicare should not have format of AA11111A.");
                     return;
                  }
//                }
                if (person.getPatientEnrollment().getPrimaryPayerClass() == null && person.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId() != null) {
                    SC.warn("Payer class can not be blank if Medicaid/Medicare/Payer Id is entered");
                    return;
                }

                if (person.getPatientEnrollment().getSecondaryPayerClass() == null && person.getPatientEnrollment().getSecondaryPayerMedicaidMedicareId() != null) {
                    SC.warn("Payer class can not be blank if Medicaid/Medicare/Payer Id is entered");
                    return;
                }


                if (person.getPatientEnrollment().getProgramNameEffectiveDate() != null && person.getPatientEnrollment().getProgramName().equals("")) {
                    SC.warn("Select a Program Name before adding Program Effective Date!");
                    return;
                }

                if (person.getPatientEnrollment().getProgramNameEffectiveDate() == null && person.getPatientEnrollment().getProgramEndDate() != null) {
                    SC.warn("Select a Program Effective Date before adding Program End Date!");
                    return;
                }

                if (person.getPatientEnrollment().getProgramEndDate() != null && person.getPatientEnrollment().getProgramName().equals("")) {
                    SC.warn("Select a Program Name before adding Program End Date!");
                    return;
                }

                if (person.getPatientEnrollment().getProgramNameEffectiveDate() != null && person.getPatientEnrollment().getProgramEndDate() != null && person.getPatientEnrollment().getProgramNameEffectiveDate().after(person.getPatientEnrollment().getProgramEndDate())) {
                    SC.warn("Program Effective Date can not be greater than  Program End Date!");
                    return;
                }

                 if(childrensHealthHome && canConsentEnrollMinors && minorFlag && person.getPatientEnrollment().getInformationSharingStatus()!=null){
                     if(person.getPatientEnrollment().getMinorConsentDateTime()==null || person.getPatientEnrollment().getMinorConsentObtainedByUserId()==0){
                     SC.warn("Please complete the information sharing fields!");
                    return;
                }

                }
                 if(childrensHealthHome && minorFlag && !canConsentEnrollMinors && person.getPatientEnrollment().getProgramLevelConsentStatus()!=null && person.getPatientEnrollment().getProgramLevelConsentStatus().equalsIgnoreCase("Yes") && person.getPatientEnrollment().getStatus().equalsIgnoreCase("Enrolled")  ){  
                     SC.warn("Your role does not have permission to consent or enroll minor. Please contact your administrator!");
                    return;
                }
                 if(childrensHealthHome && minorFlag && !canConsentEnrollMinors && person.getPatientEnrollment().getProgramLevelConsentStatus()!=null && person.getPatientEnrollment().getProgramLevelConsentStatus().equalsIgnoreCase("Yes") && person.getPatientEnrollment().getStatus().equalsIgnoreCase("Assigned")  ){
                     SC.warn("Your role does not have permission to consent or enroll minor. Please contact your administrator!");
                    return;
                }
                setSaveCancelButtons(true);
                progressBarWindow = new ProgressBarWindow();
                progressBarWindow.show();

                //JIRA 2523
                long communityId= ApplicationContextUtils.getLoginResult().getCommunityId();
                boolean mdm = mpiSearch.equalsIgnoreCase(Constants.MDM);
                if(mdm) {
                    addPatient(person);
                }
                else {
                	enrollmentService.checkForDuplicates(person.getSimplePerson(), communityId,
                        new VerifyBeforeAddCallback(person));
               }
            }
        }
    }

    class CancelClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            patientForm.clearErrors();
            patientForm.clearValues();

        }
    }

    private void addPatient(PersonDTO person) {
        String confirmationMessage = "Save successful.";
        enrollmentService.addPatient(person,
                    new AddPatientCallback(confirmationMessage));
    }
    
    class VerifyBeforeAddCallback extends BasicPortalAsyncCallback {
        PersonDTO person;
        String careteamVal;
        public VerifyBeforeAddCallback(PersonDTO person) {
            this.person = person;
        }
        
        @Override
        public void onFailure(Throwable exc) {
            showOnFailure(exc);
        }

        @Override
        public void onSuccess(Object obj) {
            DuplicateCheckProcessResult result = (DuplicateCheckProcessResult) obj;
            if(result.getProcessDecision().equals(ProcessDecision.NONE_EXISTS)) {
                addPatient(person);
            }
            else if(result.getProcessDecision().equals(ProcessDecision.SIMILAR_EXISTS)) {
                SimilarExistsWindow similarExistsWindow = new SimilarExistsWindow(person, careteamVal, 
                        result.getPossibleDuplicates());
                similarExistsWindow.show();
                hideProgressBar();
                setSaveCancelButtons(false);
            }
            else if(result.getProcessDecision().equals(ProcessDecision.ALREADY_EXISTS)) {
                AlreadyExistsWindow alreadyExistsWindow = new AlreadyExistsWindow(result.getPossibleDuplicates());
                alreadyExistsWindow.show();
                hideProgressBar();
            }
        }
    }
    
    class AddPatientCallback extends BasicPortalAsyncCallback {

        public AddPatientCallback(String confirmationMessage) {
            super(AddPatientPanel.this.progressBarWindow,confirmationMessage);
        }

        @Override
        public void onSuccess(Object obj) {
            super.onSuccess(obj);
            PersonDTO person = (PersonDTO) obj;
            patientForm.clearValues();

            setSaveCancelButtons(false);
            EnrollmentPanel enrollmentPanel = ApplicationContextUtils.getEnrollmentPanel();
            enrollmentPanel.showViewPatientPanel(person);
        }

        @Override
        public void onFailure(Throwable exc) {
            showOnFailure(exc);
        }
    }
    
    class LoadPatientCallback extends BasicPortalAsyncCallback {

        public LoadPatientCallback() {
        }
        
        @Override
        public void onSuccess(Object obj) {
        	hideProgressBar();        	
            PersonDTO thePerson = (PersonDTO) obj;
            EnrollmentPanel enrollmentPanel = ApplicationContextUtils.getEnrollmentPanel();
            enrollmentPanel.showViewPatientPanel(thePerson);
        }

        @Override
        public void onFailure(Throwable exc) {
            showOnFailure(exc);
        }
    }
    
    class SimilarExistsWindow extends Window {

        private List<SimplePerson> possibleDuplicates;

        private Label label;
        private Label label2;
        private Button selectAndAccessButton;
        private Button addNowButton;
        private PossibleDuplicatePatientPanel pdpPanel;

        private PersonDTO person;
        private String careteamVal;
		
        public SimilarExistsWindow(PersonDTO person, String careteamVal, 
				List<SimplePerson> possibleDuplicates) {
            this.possibleDuplicates = possibleDuplicates;
            this.person = person;
            this.careteamVal = careteamVal;

            buildGui();
        }

        private void buildGui() {

            setWindowProps();

            label = new Label(dseTitleLabelText);
            label.setStyleName("patientPanelTitleLabel");
            label.setHeight(20);
            label.setMargin(5);
            addItem(label);
            
            label2 = new Label(dseDescLabelText);
            label2.setHeight(20);
            label2.setMargin(5);
            addItem(label2);

            pdpPanel = new PossibleDuplicatePatientPanel(possibleDuplicates);
            addItem(pdpPanel);
            
            selectAndAccessButton = new Button(dseSelectButtonText);
            selectAndAccessButton.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    SimplePerson selectedPerson = pdpPanel.getSelectedPerson();
                    if(selectedPerson != null) {
                    	showProgressBar();
                        long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
                        long currentUserOrgId = ApplicationContextUtils.getLoginResult().getOrganizationId();
                        enrollmentService.findPatient(selectedPerson, communityId, currentUserOrgId, 
                                new LoadPatientCallback());
                        SimilarExistsWindow.this.close();
                    }
                }        
            });

            addNowButton = new Button(dseAddNowButtonText);
            addNowButton.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent ce) {
                    SimilarExistsWindow.this.close();
                    showProgressBar();
                    addPatient(person);
                 }
            });
         
            SimilarExistsWindow.this.addCloseClickHandler(new CloseClickHandler() {
				@Override
				public void onCloseClick(CloseClickEvent event) {
					SimilarExistsWindow.this.close();
					AddPatientPanel.this.redraw();
				}
			});

            HLayout buttonLayout = new HLayout(10);
            buttonLayout.setHeight(30);
            buttonLayout.setAlign(Alignment.CENTER);
            buttonLayout.addMember(selectAndAccessButton);
            buttonLayout.addMember(addNowButton);
            addItem(buttonLayout);
        }

        private void setWindowProps() {
            setPadding(1);
            setWidth(600);
            setHeight(250);

            setTitle(dseTitleLabelText);
            setShowMinimizeButton(false);
            setIsModal(true);
            setShowModalMask(true);

            centerInPage();
        }
    }    
    
    class AlreadyExistsWindow extends Window {

        private List<SimplePerson> possibleDuplicates;

        private Label label;
        private Label label2;
        private Button selectAndAccessButton;
        private Button closeAndContactButton;
        private PossibleDuplicatePatientPanel pdpPanel;

        public AlreadyExistsWindow(List<SimplePerson> possibleDuplicates) {
            this.possibleDuplicates = possibleDuplicates;

            buildGui();
        }

        private void buildGui() {

            setWindowProps();

            label = new Label(daeTitleLabelText);
            label.setStyleName("patientPanelTitleLabel");
            label.setHeight(20);
            label.setMargin(5);
            addItem(label);
            
            label2 = new Label(daeDescLabelText);
            label2.setHeight(20);
            label2.setMargin(5);
            addItem(label2);

            pdpPanel = new PossibleDuplicatePatientPanel(possibleDuplicates);
            addItem(pdpPanel);
            
            selectAndAccessButton = new Button(daeSelectButtonText);
            selectAndAccessButton.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    SimplePerson selectedPerson = pdpPanel.getSelectedPerson();
                    if(selectedPerson != null) {
                    	showProgressBar();
                        long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
                        long currentUserOrgId = ApplicationContextUtils.getLoginResult().getOrganizationId();
                        enrollmentService.findPatient(selectedPerson, communityId, currentUserOrgId, 
                                new LoadPatientCallback());
                        AlreadyExistsWindow.this.close();
                    }
                }        
            });

            closeAndContactButton = new Button(daeCloseButtonText);
            closeAndContactButton.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent ce) {
                    doClose();
                }
            });
            
            AlreadyExistsWindow.this.addCloseClickHandler(new CloseClickHandler() {
				@Override
				public void onCloseClick(CloseClickEvent event) {
					doClose();
				}
			});
            
            HLayout buttonLayout = new HLayout(10);
            buttonLayout.setHeight(30);
            buttonLayout.setAlign(Alignment.CENTER);
            buttonLayout.addMember(selectAndAccessButton);
            buttonLayout.addMember(closeAndContactButton);
            addItem(buttonLayout);
        }

        private void setWindowProps() {
            setPadding(1);
            setWidth(600);
            setHeight(250);

            setTitle(daeTitleLabelText);
            setShowMinimizeButton(false);
            setIsModal(true);
            setShowModalMask(true);

            centerInPage();
        }
        
		private void doClose() {
			setSaveCancelButtons(false);
			AlreadyExistsWindow.this.close();
            AddPatientPanel.this.redraw();
		}        
    }
    
    private void hideProgressBar()
    {
        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }
    }
    
    private void showProgressBar()
    {
        if (progressBarWindow != null) {
            progressBarWindow.show();
        }    	
    }


    class CheckIfDuplicateClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            boolean valid = patientForm.validateRequiredFieildsForDupPatients();
            long commuintyId=ApplicationContextUtils.getLoginResult().getCommunityId();
            if (valid) {
                PersonDTO person = patientForm.getPerson();
                enrollmentService.checkIfDuplicatePatient(person,commuintyId, new CheckIfDuplicatePatientCallback());

            }
        }
    }
    
    private void showOnFailure(Throwable exc) {
        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }
        setSaveCancelButtons(false);
        //Spira 7307
        AddPatientPanel.this.redraw();
        SC.warn(exc.getMessage());
    }
}
