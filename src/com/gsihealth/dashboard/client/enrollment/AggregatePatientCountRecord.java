/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.enrollment;

import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 *
 * @author User
 */

public class AggregatePatientCountRecord  extends ListGridRecord {
    private int index;
    //Total Number of Candidates for Enrollment
    private String totalNumberofCandidatesforEnrollment;
    private String totalNumberofPatientsAssigned ;
    private String totalNumberEnrolledPatients;
    private String totalNumberPatientsCompletedCarePlan;
    private String totalNumberPatientsCompletedAssessment;
    

    public AggregatePatientCountRecord() {
    }

    public AggregatePatientCountRecord(String totalNumberCandidatesEnrollment,  String totalNumberofPatientsAssigned ,String totalNumberEnrolledPatients, String totalNumberPatientsCompletedCarePlan, String totalNumberPatientsCompletedAssessment) {
        this.totalNumberofCandidatesforEnrollment = totalNumberCandidatesEnrollment;
        this.totalNumberofPatientsAssigned = totalNumberofPatientsAssigned;
        this.totalNumberEnrolledPatients = totalNumberEnrolledPatients;
        this.totalNumberPatientsCompletedCarePlan = totalNumberPatientsCompletedCarePlan;
        this.totalNumberPatientsCompletedAssessment = totalNumberPatientsCompletedAssessment;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
    
    

    public String getTotalNumberCandidatesEnrollment() {
        return getAttribute("Candidates for Enrollment");
    }

    public void setTotalNumberCandidatesEnrollment(String totalNumberofCandidatesforEnrollment) {
        setAttribute("Candidates for Enrollment", totalNumberofCandidatesforEnrollment);
    }

    public String getTotalNumberEnrolledPatients() {
        return getAttribute("Patients Enrolled and No Care Team Assignment");
    }

    public void setTotalNumberEnrolledPatients(String totalNumberEnrolledPatients) {
        setAttribute("Patients Enrolled and No Care Team Assignment", totalNumberEnrolledPatients);
    }

    public String getTotalNumberPatientsCompletedAssessment() {
        return getAttribute("Patients with a Completed Assessment");
    }

    public void setTotalNumberPatientsCompletedAssessment(String totalNumberPatientsCompletedAssessment) {
        setAttribute("Patients with a Completed Assessment", totalNumberPatientsCompletedAssessment);
    }

    public String getTotalNumberPatientsCompletedCarePlan() {
        return getAttribute("Patients with a Completed Care Plan");
    }

    public void setTotalNumberPatientsCompletedCarePlan(String totalNumberPatientsCompletedCarePlan) {
        setAttribute("Patients with a Completed Care Plan", totalNumberPatientsCompletedCarePlan);
    }

    /**
     * @return the totalNumberofPatientsAssigned
     */
    public String getTotalNumberofPatientsAssigned() {
       return getAttribute("Patients Assigned to a Care Team");
    }

    /**
     * @param totalNumberofPatientsAssigned the totalNumberofPatientsAssigned to set
     */
    public void setTotalNumberofPatientsAssigned(String totalNumberofPatientsAssigned) {
        setAttribute("Patients Assigned to a Care Team", totalNumberofPatientsAssigned);
    }
    
}
