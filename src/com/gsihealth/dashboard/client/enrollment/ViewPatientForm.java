package com.gsihealth.dashboard.client.enrollment;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.common.util.SsnUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.PatientEmergencyContactDTO;
import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;
import com.gsihealth.dashboard.entity.dto.insurance.PayerClassDTO;
import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.layout.VLayout;

import java.util.Date;
import java.util.Map;

/**
 *
 * @author Chad Darby
 */
public class ViewPatientForm extends VLayout {

    public static final int FORM_WIDTH = 800;
    private StaticTextItem patientIDStaticTextItem;
    private StaticTextItem firstNameStaticTextItem;
    private StaticTextItem middleNameStaticTextItem;
    private StaticTextItem lastNameStaticTextItem;
    private StaticTextItem dobDateItem;
    private StaticTextItem genderStaticTextItem;
    private StaticTextItem telephoneStaticTextItem;
    private StaticTextItem stateStaticTextItem;
    private StaticTextItem countyStaticTextItem;
    private StaticTextItem address1StaticTextItem, emailStaticTextItem;
    private StaticTextItem address2StaticTextItem;
    private StaticTextItem cityStaticTextItem;
    private StaticTextItem zipCodeStaticTextItem;
    private StaticTextItem organizationMrnStaticTextItem;
    private StaticTextItem careteamStaticTextItem;
    private StaticTextItem ssnStaticTextItem;
    private StaticTextItem ethnicityStaticTextItem, raceStaticTextItem, languageStaticTextItem;
    private StaticTextItem fNameEmergencyStaticTextItem, mNameEmergencyStaticTextItem, lNameEmergencyStaticTextItem, telephoneEmergencyStaticTextItem,
            emergencyEmailStaticTextItem, emergencyRelationshipStaticTextItem;
    private DynamicForm emergencyContactForm, insuranceForm;
    private StaticTextItem primaryInsuranceStaticTextItem, primaryPayerClassStaticTextItem, primaryMedicaidIdStaticTextItem, primaryPayerPlanStaticTextItem, primaryEffectiveStaticTextItem, secondaryInsuranceStaticTextItem, secondaryPayerClassStaticTextItem,
            secondaryMedicaidIdStaticTextItem, secondaryPayerPlanStaticTextItem, secondaryEffectiveStaticTextItem;
    private StaticTextItem patientUserNameStaticTextItem;
    private boolean patientEngagementEnable;
    private StaticTextItem acuityScoreTextItem;
    private boolean countyEnabled;

    public ViewPatientForm() {
        setFormValidationRulesFromClientAppProperties();
        buildGui();
    }

    protected void buildGui() {

        setMembersMargin(10);

        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        countyEnabled = ApplicationContextUtils.getLoginResult().isCountyEnabled();
        // consentReasonsForRefusal = getDisenrollmentReasonsForRefusal();

        DynamicForm patientDemographicsForm = buildPatientDemographicsForm();
        addMember(patientDemographicsForm);

        DynamicForm addressForm = buildAddressForm();
        addMember(addressForm);

        // Emergency Contact Form
        emergencyContactForm = buildEmergencyContactForm();
        addMember(emergencyContactForm);

        insuranceForm = buildInsuranceForm();
        addMember(insuranceForm);

    }

    protected void setFormValidationRulesFromClientAppProperties() {
        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
        patientEngagementEnable = props.isPatientEngagementEnable();
    }

    private DynamicForm buildAddressForm() {
        DynamicForm theForm = new DynamicForm();
        theForm.setNumCols(6);
        theForm.setGroupTitle("Contact Information");
        theForm.setIsGroup(true);
        setFormWidth(theForm);

        address1StaticTextItem = new StaticTextItem("streetAddress1", "Address 1 ");

        address2StaticTextItem = new StaticTextItem("streetAddress2", "Address 2");

        //county
        if(countyEnabled) {
            countyStaticTextItem = new StaticTextItem("countyStaticTextItem", "County");
        }

        cityStaticTextItem = new StaticTextItem("newStaticTextItem_3", "City");

        stateStaticTextItem = new StaticTextItem("newSelectItem_8", "State");


        zipCodeStaticTextItem = new StaticTextItem("newStaticTextItem_12", "Zip Code");
        zipCodeStaticTextItem.setWidth(100);
        zipCodeStaticTextItem.setEndRow(true);

        telephoneStaticTextItem = new StaticTextItem("newStaticTextItem_1", "Telephone");
        telephoneStaticTextItem.setWidth(100);

        // email address
        emailStaticTextItem = new StaticTextItem("emailStaticTextItem", "Email Address");
        emailStaticTextItem.setEndRow(true);


            if(countyStaticTextItem != null) {
                theForm.setFields(address1StaticTextItem, cityStaticTextItem, telephoneStaticTextItem, address2StaticTextItem, stateStaticTextItem, emailStaticTextItem, countyStaticTextItem, zipCodeStaticTextItem);
            }else{
                theForm.setFields(address1StaticTextItem, cityStaticTextItem, telephoneStaticTextItem, address2StaticTextItem, stateStaticTextItem, emailStaticTextItem, new SpacerItem(), new SpacerItem(), zipCodeStaticTextItem);
            }

        return theForm;
    }

    private DynamicForm buildPatientDemographicsForm() {
        DynamicForm theForm = new DynamicForm();
        setFormWidth(theForm);
        theForm.setCellPadding(2);
        theForm.setNumCols(6);
        theForm.setIsGroup(true);
        theForm.setGroupTitle("Patient Demographics");

        patientIDStaticTextItem = new StaticTextItem("patientIDStaticTextItem", "Patient ID");
        patientIDStaticTextItem.setWrapTitle(false);
        patientIDStaticTextItem.setColSpan(3);


        //pe work      
        patientUserNameStaticTextItem = new StaticTextItem("patientUserName", "Patient Username");
        patientUserNameStaticTextItem.setWrapTitle(false);
        patientUserNameStaticTextItem.setEndRow(true);
        if (!patientEngagementEnable) {
            patientUserNameStaticTextItem.setVisible(false);
        }

        firstNameStaticTextItem = new StaticTextItem("firstNameStaticTextItem", "First Name");
        firstNameStaticTextItem.setWrapTitle(false);
        firstNameStaticTextItem.setStartRow(true);

        middleNameStaticTextItem = new StaticTextItem("middleNameStaticTextItem", "Middle Name");
        middleNameStaticTextItem.setWrapTitle(false);

        lastNameStaticTextItem = new StaticTextItem("lastNameStaticTextItem", "Last Name");
        lastNameStaticTextItem.setWrapTitle(false);

        organizationMrnStaticTextItem = new StaticTextItem("organizationMrn", "Patient MRN");
        organizationMrnStaticTextItem.setWrapTitle(false);


        ssnStaticTextItem = new StaticTextItem("ssnStaticTextItem", "SSN");
        ssnStaticTextItem.setWrapTitle(false);

        ethnicityStaticTextItem = new StaticTextItem("ethnicityStaticTextItem", "Ethnicity");
        ethnicityStaticTextItem.setEndRow(true);
        ethnicityStaticTextItem.setWrapTitle(false);
        ethnicityStaticTextItem.setWidth(90);

        // Race
        raceStaticTextItem = new StaticTextItem("raceStaticTextItem", "Race");
        raceStaticTextItem.setEndRow(true);
        raceStaticTextItem.setWrapTitle(false);

        // Preferred Language
        languageStaticTextItem = new StaticTextItem("languageStaticTextItem", "Preferred Language");
        languageStaticTextItem.setEndRow(true);
        languageStaticTextItem.setWrapTitle(false);



        dobDateItem = new StaticTextItem("dateOfBirthDateItem", "Date of Birth");
        dobDateItem.setWrapTitle(false);
        dobDateItem.setTextAlign(Alignment.LEFT);
        dobDateItem.setWidth(90);

        genderStaticTextItem = new StaticTextItem("genderSelectItem", "Gender");
        genderStaticTextItem.setWrapTitle(false);
        genderStaticTextItem.setWidth(90);

        acuityScoreTextItem = new StaticTextItem("acuityScoreTextItem", "Acuity Score");
        acuityScoreTextItem.setWrapTitle(false);
        acuityScoreTextItem.setColSpan(2);
        acuityScoreTextItem.setEndRow(true);

        SpacerItem spacerItem1 = new SpacerItem();
        spacerItem1.setName("spacer1");

        theForm.setFields(
                patientIDStaticTextItem, patientUserNameStaticTextItem,
                firstNameStaticTextItem, ssnStaticTextItem, ethnicityStaticTextItem,
                middleNameStaticTextItem, dobDateItem, raceStaticTextItem,
                lastNameStaticTextItem, genderStaticTextItem, languageStaticTextItem,
                organizationMrnStaticTextItem, acuityScoreTextItem);

        return theForm;
    }

    private DynamicForm buildEmergencyContactForm() {
        DynamicForm theForm = new DynamicForm();
        theForm.setWrapItemTitles(true);
        theForm.setGroupTitle("Emergency Contact");
        theForm.setIsGroup(true);
        theForm.setNumCols(6);
        setFormWidth(theForm);

        fNameEmergencyStaticTextItem = new StaticTextItem("fNameEmergencyStaticTextItem", "First Name");
        fNameEmergencyStaticTextItem.setWrapTitle(false);


        mNameEmergencyStaticTextItem = new StaticTextItem("mNameEmergencyStaticTextItem", "Middle Name");
        mNameEmergencyStaticTextItem.setWrapTitle(false);


        lNameEmergencyStaticTextItem = new StaticTextItem("lNameEmergencyStaticTextItem", "Last Name");
        lNameEmergencyStaticTextItem.setWrapTitle(false);
        lNameEmergencyStaticTextItem.setEndRow(true);


        telephoneEmergencyStaticTextItem = new StaticTextItem("telephoneEmergencyStaticTextItem", "Telephone");
        telephoneEmergencyStaticTextItem.setWrapTitle(false);

        // email address
        emergencyEmailStaticTextItem = new StaticTextItem("emergencyEmailStaticTextItem", "Email Address");
        //emergencyEmailStaticTextItem.setEndRow(true);
        emergencyEmailStaticTextItem.setWrapTitle(false);

        //relationship

        emergencyRelationshipStaticTextItem = new StaticTextItem("emergencyRelationshipStaticTextItem", "Relationship");
        emergencyRelationshipStaticTextItem.setEndRow(true);
        emergencyRelationshipStaticTextItem.setWrapTitle(false);

        theForm.setFields(fNameEmergencyStaticTextItem, mNameEmergencyStaticTextItem, lNameEmergencyStaticTextItem, telephoneEmergencyStaticTextItem, emergencyEmailStaticTextItem, emergencyRelationshipStaticTextItem);

        return theForm;
    }

    private DynamicForm buildCareTeamForm() {
        DynamicForm theForm = new DynamicForm();

        theForm.setWrapItemTitles(true);
        theForm.setGroupTitle("Care Team");
        theForm.setIsGroup(true);
        theForm.setNumCols(5);
        setFormWidth(theForm);

        careteamStaticTextItem = new StaticTextItem("careTeam", "Care Team");

        theForm.setFields(careteamStaticTextItem);

        theForm.setVisible(false);

        return theForm;
    }

    private DynamicForm buildInsuranceForm() {
        DynamicForm theForm = new DynamicForm();
        theForm.setWrapItemTitles(true);
        theForm.setGroupTitle("Insurance");
        theForm.setIsGroup(true);
        theForm.setNumCols(12);
        setFormWidth(theForm);

        primaryInsuranceStaticTextItem = new StaticTextItem("primaryInsuranceStaticTextItem", "Primary");
        primaryInsuranceStaticTextItem.setWrapTitle(false);
        primaryInsuranceStaticTextItem.setEndRow(true);
        primaryInsuranceStaticTextItem.setWidth(10);


        secondaryInsuranceStaticTextItem = new StaticTextItem("secondaryInsuranceStaticTextItem", "Secondary");
        secondaryInsuranceStaticTextItem.setWrapTitle(false);
        secondaryInsuranceStaticTextItem.setEndRow(true);
        secondaryInsuranceStaticTextItem.setWidth(10);

        primaryMedicaidIdStaticTextItem = new StaticTextItem("primaryMedicaidIdStaticTextItem", "&#09;&#09;Medicaid/Medicare/Payer ID");
        primaryMedicaidIdStaticTextItem.setEndRow(true);
        primaryMedicaidIdStaticTextItem.setWrapTitle(false);
        primaryMedicaidIdStaticTextItem.setAlign(Alignment.LEFT);

        secondaryMedicaidIdStaticTextItem = new StaticTextItem("secondaryMedicaidIdStaticTextItem", "&#09;&#09;Medicaid/Medicare/Payer ID");
        secondaryMedicaidIdStaticTextItem.setEndRow(true);

        primaryPayerClassStaticTextItem = new StaticTextItem("primaryPayerClassStaticTextItem", "Payer Class");
        primaryPayerClassStaticTextItem.setWrapTitle(false);


        primaryPayerPlanStaticTextItem = new StaticTextItem("primaryPayerPlanStaticTextItem", "Payer Plan");
        primaryPayerPlanStaticTextItem.setWrapTitle(false);
        primaryPayerPlanStaticTextItem.setWidth(100);

        secondaryPayerClassStaticTextItem = new StaticTextItem("secondaryPayerClassStaticTextItem", "Payer Class");
        secondaryPayerClassStaticTextItem.setWrapTitle(false);


        secondaryPayerPlanStaticTextItem = new StaticTextItem("secondaryPayerPlanStaticTextItem", "Payer Plan");
        secondaryPayerPlanStaticTextItem.setWrapTitle(false);
        secondaryPayerPlanStaticTextItem.setWidth(100);

        primaryEffectiveStaticTextItem = new StaticTextItem("primaryEffectiveStaticTextItem", "&#09;&#09;Effective Date");
        primaryEffectiveStaticTextItem.setEndRow(true);
        primaryEffectiveStaticTextItem.setWrapTitle(false);

        secondaryEffectiveStaticTextItem = new StaticTextItem("secondaryEffectiveStaticTextItem", "&#09;&#09;Effective Date");
        secondaryEffectiveStaticTextItem.setEndRow(true);
        secondaryEffectiveStaticTextItem.setWrapTitle(false);


        SpacerItem space = new SpacerItem();
        space.setColSpan(4);

        SpacerItem space2 = new SpacerItem();
        space2.setWidth(67);

        theForm.setFields(primaryInsuranceStaticTextItem, space2, primaryPayerClassStaticTextItem, space, primaryMedicaidIdStaticTextItem, space2, primaryPayerPlanStaticTextItem, space, primaryEffectiveStaticTextItem, secondaryInsuranceStaticTextItem, space2, secondaryPayerClassStaticTextItem, space, secondaryMedicaidIdStaticTextItem, space2, secondaryPayerPlanStaticTextItem, space, secondaryEffectiveStaticTextItem);


        return theForm;
    }

    public void populate(PersonDTO thePerson) {

        PatientEnrollmentDTO patientEnrollment = thePerson.getPatientEnrollment();

        patientIDStaticTextItem.setValue(StringUtils.defaultString(String.valueOf(patientEnrollment.getPatientId())));
        firstNameStaticTextItem.setValue(thePerson.getFirstName());
        middleNameStaticTextItem.setValue(thePerson.getMiddleName());
        lastNameStaticTextItem.setValue(thePerson.getLastName());

        //pe

        patientUserNameStaticTextItem.setValue(patientEnrollment.getPatientLoginId());

        // ssn
        String ssn = thePerson.getSsn();
        if (StringUtils.isNotBlank(ssn)) {
            String maskedSsn = SsnUtils.getMaskedSsn(ssn);
            ssnStaticTextItem.setValue(maskedSsn);
        }
        String dobAsString = thePerson.getDateOfBirthAsString();
        dobDateItem.setValue(dobAsString);
       //Spira 5438 : Sometimes Date objects cahnge there value from client to server so used string
        thePerson.setDateOfBirthAsString(dobAsString);
        // Following code brings the dob in the date format and set it on the 'thePerson'. This will allow the data which is modified to reset on the field
        DateTimeFormat dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
        Date dobAsFromDate = dateFormatter.parse(dobAsString);
        thePerson.setDateOfBirth(dobAsFromDate);

        // gender
        Map<String, String> genderCodes = ApplicationContextUtils.getGenderCodes();
        String genderDescription = genderCodes.get(thePerson.getGender());
        genderStaticTextItem.setValue(genderDescription);

        // ethnicity
        Map<String, String> ethnicCodes = ApplicationContextUtils.getEthnicCodes();
        String ethnicDescription = ethnicCodes.get(thePerson.getEthnic());
        ethnicityStaticTextItem.setValue(ethnicDescription);

        // Race
        Map<String, String> raceCodes = ApplicationContextUtils.getRaceCodes();
        String raceDescription = raceCodes.get(thePerson.getRace());
        raceStaticTextItem.setValue(raceDescription);

        // language
        Map<String, String> languageCodes = ApplicationContextUtils.getLanguageCodes();
        String languageDescription = languageCodes.get(thePerson.getLanguage());
        languageStaticTextItem.setValue(languageDescription);

        // address
        address1StaticTextItem.setValue(thePerson.getStreetAddress1());
        address2StaticTextItem.setValue(thePerson.getStreetAddress2());
        cityStaticTextItem.setValue(thePerson.getCity());
        stateStaticTextItem.setValue(thePerson.getState());
        if (countyStaticTextItem != null) {
            countyStaticTextItem.setValue(thePerson.getCounty());
        }
        zipCodeStaticTextItem.setValue(thePerson.getZipCode());
        emailStaticTextItem.setValue(patientEnrollment.getEmailAddress());
        telephoneStaticTextItem.setValue(thePerson.getTelephone());
        emailStaticTextItem.setValue(patientEnrollment.getEmailAddress());


        // mrn
        String patientMrn = patientEnrollment.getPatientMrn();
        if (patientMrn != null) {
            organizationMrnStaticTextItem.setValue(patientMrn);
        }

       String acuityScore = patientEnrollment.getAcuityScore();
       if(acuityScore!=null){
           acuityScoreTextItem.setValue(acuityScore);
       }

        // Insurance
        populateInsuranceSection(patientEnrollment);

        // Emergency contacts
        populateEmergencyContactSection(patientEnrollment);

    }

    private void populateInsuranceSection(PatientEnrollmentDTO patientEnrollment) {

        //  Primary payer class
        PayerClassDTO primaryPayerClass = patientEnrollment.getPrimaryPayerClass();
        if (primaryPayerClass != null) {
            String primaryPayerClassName = primaryPayerClass.getName();
            if (!StringUtils.isBlank(primaryPayerClassName)) {
                primaryPayerClassStaticTextItem.setValue(primaryPayerClassName);
            }
        }

        // secondary payer class
        PayerClassDTO secondaryPayerClass = patientEnrollment.getSecondaryPayerClass();
        if (secondaryPayerClass != null) {
            String secondaryPayerClassName = secondaryPayerClass.getName();
            if (!StringUtils.isBlank(secondaryPayerClassName)) {
                secondaryPayerClassStaticTextItem.setValue(secondaryPayerClassName);
            }
        }

        // primary payer plan
        PayerPlanDTO primaryPayerPlan = patientEnrollment.getPrimaryPayerPlan();
        if (primaryPayerPlan != null) {
            String primaryPayerPlanName = primaryPayerPlan.getName();
            if (!StringUtils.isBlank(primaryPayerPlanName)) {
                primaryPayerPlanStaticTextItem.setValue(primaryPayerPlanName);
            }
        }

        // secondary payer plan
        PayerPlanDTO secondaryPayerPlan = patientEnrollment.getSecondaryPayerPlan();
        if (secondaryPayerPlan != null) {
            String secondaryPayerPlanName = secondaryPayerPlan.getName();
            if (!StringUtils.isBlank(secondaryPayerPlanName)) {
                secondaryPayerPlanStaticTextItem.setValue(secondaryPayerPlanName);
            }
        }

        // primary payer medicaid medicare id
        String primaryPayerMedicaidMedicareId = patientEnrollment.getPrimaryPayerMedicaidMedicareId();
        if (StringUtils.isNotBlank(primaryPayerMedicaidMedicareId)) {
            primaryMedicaidIdStaticTextItem.setValue(primaryPayerMedicaidMedicareId);
        }

        // secondary payer medicaid medicare id
        String secondaryPayerMedicaidMedicareId = patientEnrollment.getSecondaryPayerMedicaidMedicareId();
        if (StringUtils.isNotBlank(secondaryPayerMedicaidMedicareId)) {
            secondaryMedicaidIdStaticTextItem.setValue(secondaryPayerMedicaidMedicareId);
        }

        // primary effective date
        Date primaryEffectiveDate = patientEnrollment.getPrimaryPlanEffectiveDate();
        // date formatter
        DateTimeFormat dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
        if (primaryEffectiveDate != null) {
            String primaryDateStr = dateFormatter.format(primaryEffectiveDate);
            primaryEffectiveStaticTextItem.setValue(primaryDateStr);
        }

        // secondary effective date
        Date secondaryEffectiveDate = patientEnrollment.getSecondaryPlanEffectiveDate();
        if (secondaryEffectiveDate != null) {
            String secondaryDateStr = dateFormatter.format(secondaryEffectiveDate);
            secondaryEffectiveStaticTextItem.setValue(secondaryDateStr);
        }

    }

    private void populateEmergencyContactSection(PatientEnrollmentDTO patientEnrollment) {

        PatientEmergencyContactDTO emergencyContact = patientEnrollment.getPatientEmergencyContact();

        if (emergencyContact != null) {
            fNameEmergencyStaticTextItem.setValue(emergencyContact.getFirstName());
            lNameEmergencyStaticTextItem.setValue(emergencyContact.getLastName());
            mNameEmergencyStaticTextItem.setValue(emergencyContact.getMiddleName());
            emergencyEmailStaticTextItem.setValue(emergencyContact.getEmailAddress());
            telephoneEmergencyStaticTextItem.setValue(emergencyContact.getTelephone());

            emergencyRelationshipStaticTextItem.setValue(emergencyContact.getPatientRelationshipDescription());
        }
    }


    private boolean isIE11Browser(){
        return Window.Navigator.getUserAgent().toLowerCase().contains("trident") ;
     }
    
    private void setFormWidth(DynamicForm theForm){
        if(isIE11Browser()){
             theForm.addStyleName("width-view-patient-form");
        }else{
             theForm.setWidth(FORM_WIDTH);
        }
    }
}
