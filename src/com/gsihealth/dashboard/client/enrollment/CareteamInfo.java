package com.gsihealth.dashboard.client.enrollment;

/**
 *
 * @author Chad Darby
 */
public class CareteamInfo implements java.io.Serializable {

    private long communityCareteamId;
    private String careteamName;

    public CareteamInfo() {
        
    }

    public String getCareteamName() {
        return careteamName;
    }

    public void setCareteamName(String careteamName) {
        this.careteamName = careteamName;
    }

    public long getCommunityCareteamId() {
        return communityCareteamId;
    }

    public void setCommunityCareteamId(long communityCareteamId) {
        this.communityCareteamId = communityCareteamId;
    }


}
