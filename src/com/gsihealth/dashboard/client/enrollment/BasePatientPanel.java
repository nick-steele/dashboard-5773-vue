package com.gsihealth.dashboard.client.enrollment;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 * @author Chad Darby
 */
public class BasePatientPanel extends VLayout {

    protected Button viewCareTeamButton;
    protected Button modifyCareTeamButton;
    protected Button addCareTeamButton;

    public BasePatientPanel() {

    }
    
    public void showCareteamButtons(boolean flag) {
        if (flag) {
            viewCareTeamButton.show();
            modifyCareTeamButton.show();
            addCareTeamButton.show();
        } else {
            viewCareTeamButton.hide();
            modifyCareTeamButton.hide();
            addCareTeamButton.hide();
        }
    }

    class HandlePatientCareteamCallback implements AsyncCallback {

        @Override
        public void onSuccess(Object t) {
        }

        @Override
        public void onFailure(Throwable thrwbl) {
            SC.warn("Error updating patient care team.");
        }        
    }
}
