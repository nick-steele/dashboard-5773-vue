package com.gsihealth.dashboard.client.enrollment;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.activitytracker.ActivityTrackerWindow;
import com.gsihealth.dashboard.client.common.BaseViewPatientPanel;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.redux.Redux;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;

/**
 *
 * @author Chad Darby
 */
public class ViewPatientPanel extends BaseViewPatientPanel {

    private ViewPatientForm viewPatientForm;
    private Button modifyButton;
    private Button openEnrollmentButton;
//    private String activityTrackerButtonText;
//    private boolean readOnly;
//    private final String SPACER_TO_TEXT = "&nbsp&nbsp&nbsp&nbsp&nbsp";
    
    public ViewPatientPanel() {
       super();
//        activityTrackerButtonText=props.getActivityTrackerButtonText();
        buildGui();
        DashBoardApp.restartTimer();
    }

    public ViewPatientPanel(PersonDTO thePerson) {
        this();
        populate(thePerson);
    }

    public void buildGui() {

        setPadding(5);
        setMembersMargin(5);

        Label label = new Label("View Patient");
        label.setWidth(300);
        label.setHeight(40);
        label.setStyleName("patientPanelTitleLabel");
        addMember(label);
        
        HLayout topLayout = new HLayout(10);
        // create MyPatientList GUI items ... hide them at first
        alreadyOnListLayout = buildAlreadyOnListLayout();
        alreadyOnListLayout.hide();

        addToMyListLayout = buildAddToMyListLayout();
        addToMyListLayout.hide();

        topLayout.addMember(alreadyOnListLayout);
        topLayout.addMember(addToMyListLayout);
        
//        Layout theLayout=buildAcivityTrackerButton(activityTrackerButtonText);
//        topLayout.addMember(theLayout);
        addMember(topLayout);
        viewPatientForm = new ViewPatientForm();

        addMember(viewPatientForm);

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(10);
        addMember(spacer);

        Layout buttonBar = buildButtonBar();
        addMember(buttonBar);
    }
    
// private  Layout buildAcivityTrackerButton(String buttonText){
//         HLayout theLayout = new HLayout(10);
//        Button theButton= new Button(SPACER_TO_TEXT + buttonText + SPACER_TO_TEXT);
//        theButton.setID("activityTrackerButtonViewPatientPanel");
//        theButton.setAutoFit(true);
//        theButton.addClickHandler(new activityTackerClickHanddler());
//        theLayout.addMember(theButton);
//        return theLayout;
//    }
    private Layout buildButtonBar() {
        HLayout buttonLayout = new HLayout(10);

        buttonLayout.setWidth(PatientForm.FORM_WIDTH);
        buttonLayout.setAlign(Alignment.CENTER);

        modifyButton = new Button(props.getDemographicModifyButton());
        modifyButton.setID("Modify");
        buttonLayout.addMember(modifyButton);

        modifyButton.addClickHandler(new ModifyPatientClickHandler());

        openEnrollmentButton = new Button(props.getDemographicEnrollmentButton());
        openEnrollmentButton.setID("Enrollment");
        buttonLayout.addMember(openEnrollmentButton);
        openEnrollmentButton.addClickHandler(new OpenEnrollmentClickHandler());

        return buttonLayout;
    }

    public void populate(PersonDTO thePerson) {
        person = thePerson;
        viewPatientForm.populate(thePerson);

        handleMyPatientListGuiItems();
    }

    class ModifyPatientClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            EnrollmentPanel enrollmentPanel = ApplicationContextUtils.getEnrollmentPanel();
            enrollmentPanel.showModifyPatientPanel(person);
        }
    }

    class OpenEnrollmentClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            Redux.jsEvent("local:appPatientEnrollment:openEnrollmentWizard", "{\"patientId\": " + person.getPatientEnrollment().getPatientId() +"}");
        }
    }
    
//     public class activityTackerClickHanddler implements ClickHandler{
//            @Override
//            public void onClick(ClickEvent event) {
//            readOnly=false;    
//            ActivityTrackerWindow window = new ActivityTrackerWindow(person,readOnly);
//            window.show();
//            }
//        }
}
