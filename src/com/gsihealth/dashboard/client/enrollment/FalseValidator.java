package com.gsihealth.dashboard.client.enrollment;

import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.validator.CustomValidator;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class FalseValidator extends CustomValidator {

    private static final String MEDICAID_PAYER_CLASS_CODE = "1";
    private String payerClassSelectItem;
    private TextItem medicaidIdTextItem;
    private final static Logger log = Logger.getLogger(FalseValidator.class.getName());

    public FalseValidator(String thePayerClassSelectItem, TextItem theMedicaidIdTextItem) {
        payerClassSelectItem = thePayerClassSelectItem;
        medicaidIdTextItem = new TextItem();
        medicaidIdTextItem = theMedicaidIdTextItem;


        setErrorMessage("Medicaid ID must have the format of AA11111A.");
    }

    @Override
    protected boolean condition(Object value) {
        return validateMedicaidId();
    }

    private boolean validateMedicaidId() {

        boolean result = false;

        if (StringUtils.equalsIgnoreCase(payerClassSelectItem, MEDICAID_PAYER_CLASS_CODE)) {

            // validate the field

            // if value is not blank, then validate it
            if (StringUtils.isNotBlank(medicaidIdTextItem.getValueAsString())) {
                String value = medicaidIdTextItem.getValueAsString();
                log.info(value);
                result = value.matches(Constants.MEDICAID_ID_ALPHA_NUMBER_FORMAT_REGEX);
            } else {
                result = true;
            }
        } else {
            result = true;
        }

        return result;
    }
}
