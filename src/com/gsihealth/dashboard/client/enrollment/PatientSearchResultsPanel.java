package com.gsihealth.dashboard.client.enrollment;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.common.PersonRecord;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.enrollment.MirthPatientSearchForm;
import com.gsihealth.dashboard.client.common.GridPager;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.client.service.EnrollmentService;
import com.gsihealth.dashboard.client.service.EnrollmentServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.DateFieldCellFormatter;
import com.gsihealth.dashboard.common.AccessLevelConstants;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.List;
import java.util.Date;

/**
 *
 * @author Chad Darby
 */
public class PatientSearchResultsPanel extends VLayout {

    private static final int DATE_COLUMN = 3;
    private ListGrid listGrid;
    private Button selectPatientButton;
    private List<PersonDTO> persons;
    private static String[] columnNames;
//    private static String[] columnNamesEnrolledPatients;
    private Label consentLabel;
    private GridPager pager;
    private String plcsTitle;
    private boolean candidatesEnrollmentDisableIntialLoad;
    private boolean enrolledPatientDisableIntialLoad;
    private static final String POWERUSERMSG = "<font color='black' class = 'search-font-size'>As a power user, system allows you to access ALL patients regardless of their consent preferences.</font>";
    private static final String CONSENTMSG = "<font color='red'>Search results consist of only patients who have granted consent to your organization to view their entire patient record.</font>";
    private static final String INITIALLOADINGMSG = "<font color='red'>Click ‘Search’ to retrieve patient list, Search Criteria are optional. </font>";
    private static final String POST_LOADING_INITIAL_MSG = "<font color='black' class = 'search-font-size'>" + ApplicationContextUtils.getClientApplicationProperties().getNormalPatientsSearchText() + "</font>";
    private static final String POST_LOADING_ADDITIONAL_SEARCH_MSG = "<font color='red' class = 'search-font-size'>" + ApplicationContextUtils.getClientApplicationProperties().getSearchForAdditionalPatientsText() + "</font>";
    private boolean patientEngagementEnable;
    private boolean requireChh;
    private PatientSearchForm patientSearchForm;
    private MirthPatientSearchForm mirthPatientSearchForm;
        
    ClientApplicationProperties props;
     
    public PatientSearchResultsPanel(PatientSearchForm patientSearchForm) {
        this.patientSearchForm = patientSearchForm;
        props = ApplicationContextUtils.getClientApplicationProperties();
        setFormValidationRulesFromClientAppProperties();
        buildGui();
    }  
    public PatientSearchResultsPanel(MirthPatientSearchForm mirthPatientSearchForm) {
        this.mirthPatientSearchForm = mirthPatientSearchForm;
        props = ApplicationContextUtils.getClientApplicationProperties();
        setFormValidationRulesFromClientAppProperties();
        buildGui();
    } 
       
        private void buildGui() {

        //getting PLCS title from prop file.
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        plcsTitle = loginResult.getPlcsTitle();
//        if (requireChh) {
//            columnNames = new String[]{"Patient ID", "Last Name", "First Name", "Middle Name",
//                "DOB", "Minor", "Address", "Organization", plcsTitle,
//                "Enrollment Status","Care Team"};
//        } else {
//            columnNames = new String[]{"Patient ID", "Last Name", "First Name", "Middle Name",
//                    "DOB", "Minor", "Address", "Organization", plcsTitle, "Enrollment Status", "Care Team"};
//        }
//        //vinay
        if (requireChh) {
            if (patientEngagementEnable) {
                columnNames = new String[]{"Patient ID", "Last Name", "First Name", "Middle Name",
                    "DOB", "Gender", "Minor", "Address", "Organization", "Patient Status", "Care Team", "Patient Messaging"};
            } else {
                columnNames = new String[]{"Patient ID", "Last Name", "First Name", "Middle Name",
                    "DOB", "Gender", "Minor", "Address", "Organization", "Patient Status", "Care Team"};
            }
        } else if (patientEngagementEnable) {
            columnNames = new String[]{"Patient ID", "Last Name", "First Name", "Middle Name",
                "DOB", "Gender", "Address", "Organization", "Patient Status", "Care Team", "Patient Messaging"};
        } else {
            columnNames = new String[]{"Patient ID", "Last Name", "First Name", "Middle Name",
                "DOB", "Gender", "Address", "Organization", "Patient Status", "Care Team"};
        }
//        } else {
//            if (patientEngagementEnable) {
//                columnNamesEnrolledPatients = new String[]{"Patient ID", "Last Name", "First Name", "Middle Name",
//                    "DOB", "Address", "Organization", plcsTitle, "Enrollment Status", "Care Team", "Patient Messaging"};
//            } else {
//                columnNamesEnrolledPatients = new String[]{"Patient ID", "Last Name", "First Name", "Middle Name",
//                    "DOB", "Address", "Organization", plcsTitle, "Enrollment Status", "Care Team"};
//            }
//        }
        listGrid = buildListGrid();

        setPadding(5);

        // header
        Layout headerBar = buildHeaderBar();
        addMember(headerBar);

        // list grid
        addMember(listGrid);

        // pager
        pager = new GridPager(0);
        addMember(pager);

        LayoutSpacer topSpacer = new LayoutSpacer();
        topSpacer.setHeight(20);
        addMember(topSpacer);

        // button bar
        Layout buttonBar = buildButtonBar();
        addMember(buttonBar);

        setWidth100();
        setHeight100();

        // register listeners
        selectPatientButton.addClickHandler(new ViewPatientClickHandler());

        listGrid.addRecordDoubleClickHandler(new PatientRecordClickHandler());
    }

    public void populateListGrid(ListGridRecord[] data, List<PersonDTO> thePersons, int totalCount) {

        // scroll to first row
        listGrid.scrollToRow(1);

        listGrid.setData(data);
        listGrid.markForRedraw();
        persons = thePersons;
        
        pager.setTotalCount(totalCount);
        ClientApplicationProperties clientApplicationProperties = ApplicationContextUtils.getClientApplicationProperties();
        String mpiSearch = clientApplicationProperties.getMirthSearchVisible();
        
        if (ApplicationContextUtils.getLoginResult().getAccessLevelId() != AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
            if(mpiSearch.equals("mdm")){
                SearchCriteria searchCriteria = this.patientSearchForm.getSearchCriteria();
                if (this.patientSearchForm.isSearchMorePatientsCheckBoxItemSelected() && searchCriteria!=null &&
                        searchCriteria.getFirstName()!=null && searchCriteria.getLastName()!=null) {
                    consentLabel.setContents(POST_LOADING_ADDITIONAL_SEARCH_MSG);
                } else {
                    consentLabel.setContents(POST_LOADING_INITIAL_MSG);
                }
            }
            else if(mpiSearch.equals("mirth") || mpiSearch.equals("demog")){
                SearchCriteria searchCriteria = this.mirthPatientSearchForm.getSearchCriteria();
                if (this.mirthPatientSearchForm.isSearchMorePatientsCheckBoxItemSelected() && searchCriteria!=null &&
                        searchCriteria.getFirstName()!=null && searchCriteria.getLastName()!=null) {
                    consentLabel.setContents(POST_LOADING_ADDITIONAL_SEARCH_MSG);
                } else {
                    consentLabel.setContents(POST_LOADING_INITIAL_MSG);
                }
            }
        }
    }

    /**
     * Helper method to build a ListGrid
     *
     * @return
     */
    private ListGrid buildListGrid() {

        ListGrid theListGrid = new ListGrid();

        theListGrid.setCanReorderFields(true);
        theListGrid.setCanReorderRecords(true);

        theListGrid.setWidth100();
        theListGrid.setHeight100();

        theListGrid.setAlternateRecordStyles(true);
        theListGrid.setShowAllRecords(false);
        theListGrid.setSelectionType(SelectionStyle.SINGLE);

        theListGrid.setCanEdit(false);

        // add fields
        ListGridField[] fields = buildListGridFields(columnNames);
        theListGrid.setFields(fields);

        ListGridField dateField = fields[DATE_COLUMN];
        setDateFieldCellFormatter(dateField);

        theListGrid.setEmptyMessage("...");
        theListGrid.setCanResizeFields(true);

        return theListGrid;
    }

    private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];

        for (int i = 0; i < columnNames.length; i++) {
            String tempColumnName = columnNames[i];
            ListGridField tempField = buildListGridField(tempColumnName);
            fields[i] = tempField;
        }

        return fields;
    }

    private ListGridField buildListGridField(String title) {
        ListGridField tempField = new ListGridField(title, title);

        tempField.setCanEdit(false);

        return tempField;
    }

    public ListGrid getListGrid() {
        return listGrid;
    }

    private Layout buildButtonBar() {
        HLayout buttonLayout = new HLayout(10);

        buttonLayout.setWidth100();
        buttonLayout.setAlign(Alignment.CENTER);

        selectPatientButton = new Button(props.getDemographicSelectButton());
        selectPatientButton.setID("Select");
        buttonLayout.addMember(selectPatientButton);

        return buttonLayout;
    }

    private Layout buildHeaderBar() {
        HLayout layout = new HLayout(10);

        layout.setWidth100();
        layout.setAlign(Alignment.CENTER);

        consentLabel = new Label();
        consentLabel.setHeight(30);
        consentLabel.setWidth100();
        consentLabel.setAlign(Alignment.CENTER);
        layout.addMember(consentLabel);

        return layout;
    }

    /**
     * Add cell formatter for date field
     *
     * @param dateField
     */
    private void setDateFieldCellFormatter(ListGridField dateField) {

        final DateTimeFormat dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
        dateField.setCellFormatter(new DateFieldCellFormatter(dateFormatter));
    }

    public void clearListGrid() {
        ListGridRecord[] records = new ListGridRecord[1];
        listGrid.setData(records);
    }

    public void setColumns(String[] columnNames) {
        clearListGrid();

        ListGridField[] fields = buildListGridFields(columnNames);
        listGrid.setFields(fields);

        listGrid.markForRedraw();
    }

//    public void setColumnsByCandidateOrEnrolled(boolean enrolledMode) {
//        if(enrolledMode) {
//            this.setColumnsForEnrolledPatients();
//        } else {
//            this.setColumnsForCandiates();
//        }
//    }
//    
//    public void setColumnsForCandiates() {
//        clearListGrid();
//
//        ListGridField[] fields = buildListGridFields(columnNamesCandidates);
//        listGrid.setFields(fields);
//
//        listGrid.markForRedraw();
//    }
//
//    public void setColumnsForEnrolledPatients() {
//        clearListGrid();
//
//        ListGridField[] fields = buildListGridFields(columnNamesEnrolledPatients);
//        listGrid.setFields(fields);
//
//        listGrid.markForRedraw();
//    }
    private final EnrollmentServiceAsync enrollmentService = GWT.create(EnrollmentService.class);

    protected void handleSelectedPatient() {
        PersonRecord theRecord = (PersonRecord) listGrid.getSelectedRecord();

        if (theRecord == null) {
            SC.warn("Nothing selected.");
            return;
        }

        int index = theRecord.getIndex();

        // get selected DTO
        final PersonDTO thePerson = persons.get(index);

        // now show the panel
        if (thePerson.isSelfAssertEligible()) {
            final ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
            if (ApplicationContextUtils.getClientApplicationProperties().isPatientSelfAssertedConsentAccess()) {
                SC.confirm("Warning", props.getPatientSelfAssertWarningText(), new BooleanCallback() {
                    @Override
                    public void execute(Boolean value) {
                        if (value.booleanValue()) {

                            final String consentLanguage = props.getConsentLanguage();
                            SC.ask("Consent Confirmation", consentLanguage, new BooleanCallback() {
                                public void execute(Boolean value) {
                                    if (value.booleanValue()) {
                                        thePerson.getPatientEnrollment().setSelfAssertedConsentDate(new Date());
                                        enrollmentService.updatePatientBySelfAssertion(thePerson, new AsyncCallback<PersonDTO>() {
                                            @Override
                                            public void onSuccess(PersonDTO updatedPersonDtoAfterSelfAssertion) {
                                                EnrollmentPanel enrollmentPanel = ApplicationContextUtils.getEnrollmentPanel();
                                                enrollmentPanel.showViewPatientPanel(updatedPersonDtoAfterSelfAssertion);
                                            }

                                            @Override
                                            public void onFailure(Throwable cause) {
                                                SC.say("Update failed");
                                            }
                                        });
                                    }
                                }
                            });

                        } else {
                            SC.say("Do nothing");
                        }
                    }

                });
            } else {
                SC.say("This patient has not yet granted access to your organization.  Please contact your administrator.");
            }
        } else {
            EnrollmentPanel enrollmentPanel = ApplicationContextUtils.getEnrollmentPanel();
            enrollmentPanel.showViewPatientPanel(thePerson);
        }
    }

    public void gotoPage(int pageNumber) {
        pager.gotoPage(pageNumber);
    }

    public int getCurrentPageNumber() {
        return pager.getCurrentPageNumber();
    }

    public void setPageEventHandler(PageEventHandler handler) {
        pager.setPageEventHandler(handler);
    }

    public void refreshPageLabels(int pageNum) {
        pager.refreshPageLabels(pageNum);
    }

    public void setTotalCount(int totalCount) {
        pager.setTotalCount(totalCount);
    }

    public int getPageSize() {
        return pager.getPageSize();
    }

    public void setEnrolledMode1(boolean theEnrolledMode, boolean searchMode) {
        long userAccessLevelId = ApplicationContextUtils.getLoginResult().getAccessLevelId();

        // add message to consent label on header
//        if (theEnrolledMode) {
//            if (searchMode) {
//
//                if (userAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
//                    consentLabel.setContents(POWERUSERMSG);
//                } else {
//                    consentLabel.setContents(CONSENTMSG);
//                }
//
//            } else {
//
//                if (candidatesEnrollmentDisableIntialLoad && enrolledPatientDisableIntialLoad) {
//                    consentLabel.setContents(INITIALLOADINGMSG);
//                }
//
//                if (candidatesEnrollmentDisableIntialLoad && !enrolledPatientDisableIntialLoad) {
//                    if (userAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
//                        consentLabel.setContents(POWERUSERMSG);
//                    } else {
//                        consentLabel.setContents(CONSENTMSG);
//                    }
//                }
//
//                if (!candidatesEnrollmentDisableIntialLoad && enrolledPatientDisableIntialLoad) {
//                    consentLabel.setContents(INITIALLOADINGMSG);
//                }
//
//                if (!candidatesEnrollmentDisableIntialLoad && !enrolledPatientDisableIntialLoad) {
//
//                    if (userAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
//                        consentLabel.setContents(POWERUSERMSG);
//                    } else {
//                        consentLabel.setContents(CONSENTMSG);
//                    }
//                }
//            }
//        } else {
        if (searchMode) {
            if (userAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
                consentLabel.setContents(POWERUSERMSG);
            } else {
                consentLabel.setContents("");
            }

        } else {

            if (candidatesEnrollmentDisableIntialLoad) {
                consentLabel.setContents(INITIALLOADINGMSG);
            }

//                if (candidatesEnrollmentDisableIntialLoad && !enrolledPatientDisableIntialLoad) {
//                    consentLabel.setContents(INITIALLOADINGMSG);
//                }
            if (!candidatesEnrollmentDisableIntialLoad) {
                if (userAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
                    consentLabel.setContents(POWERUSERMSG);
                } else {
                    consentLabel.setContents("");
                }
            }

//                if (!candidatesEnrollmentDisableIntialLoad && !enrolledPatientDisableIntialLoad) {
//
//                    if (userAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
//                        consentLabel.setContents(POWERUSERMSG);
//                    } else {
//                        consentLabel.setContents("");
//                    }
//                }
        }
//        }
    }

    class ViewPatientClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            handleSelectedPatient();
        }
    }

    /**
     * For each record clicked, populate form w/ user data
     */
    class PatientRecordClickHandler implements RecordDoubleClickHandler {

        @Override
        public void onRecordDoubleClick(RecordDoubleClickEvent event) {
            handleSelectedPatient();
        }
    }

    protected void setFormValidationRulesFromClientAppProperties() {

        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
        candidatesEnrollmentDisableIntialLoad = props.isCandidatesEnrollmentDisableIntialLoad();
//        enrolledPatientDisableIntialLoad = props.isEnrolledPatientDisableIntialLoad();
        patientEngagementEnable = props.isPatientEngagementEnable();
        requireChh = props.isChildrensHealthHome();

    }

    public void populateListGrid1(int totalCount) {

        // scroll to first row
        listGrid.scrollToRow(1);
        listGrid.markForRedraw();
        pager.setTotalCount(totalCount);
    }

}
