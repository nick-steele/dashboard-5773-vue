package com.gsihealth.dashboard.client.enrollment.callbacks;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.util.Constants;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import java.util.LinkedHashMap;

/**
 * Populate the organization drop down list w/ data from server
 *
 */
public class GetOrganizationNamesCallback implements AsyncCallback<LinkedHashMap<String, String>> {

    private SelectItem selectItem;

    public GetOrganizationNamesCallback(SelectItem theSelectItem) {
        selectItem = theSelectItem;
    }

    @Override
    public void onSuccess(LinkedHashMap<String, String> data) {

        LinkedHashMap<String, String> orgNamesMap = buildOrgNamesMap(data);

        selectItem.setValueMap(orgNamesMap);
    }

    @Override
    public void onFailure(Throwable thrwbl) {
        SC.warn("Error reading organizations.");
    }

    private LinkedHashMap<String, String> buildOrgNamesMap(LinkedHashMap<String, String> baseData) {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
        data.put("", "");

        for (String tempKey : baseData.keySet()) {
            String tempValue = baseData.get(tempKey);

            tempKey = Constants.DELIMITER_SELECT_ITEM + tempKey;
            data.put(tempKey, tempValue);
        }

        return data;
    }
}
