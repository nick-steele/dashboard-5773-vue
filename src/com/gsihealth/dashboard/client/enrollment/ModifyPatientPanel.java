package com.gsihealth.dashboard.client.enrollment;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.callbacks.CheckIfDuplicatePatientCallback;
import com.gsihealth.dashboard.client.common.BaseViewPatientPanel;
import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.service.*;
import com.gsihealth.dashboard.client.util.*;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;
import com.gsihealth.patientmdminterface.entity.DuplicateCheckProcessResult;
import com.gsihealth.patientmdminterface.entity.DuplicateCheckProcessResult.ProcessDecision;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;

import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class ModifyPatientPanel extends BaseViewPatientPanel {

    private PatientForm patientForm;
    private Button saveButton;
    private Button cancelButton;
    private final EnrollmentServiceAsync enrollmentService = GWT.create(EnrollmentService.class);
    private CareteamServiceAsync careteamService = GWT.create(CareteamService.class);
    private String oldEnrollmentStatus = "";
    private String oldProgramLevelConsentStatus;
    private String oldProgramName;
    private ViewPatientForm viewPatientForm;
    private Button checkDuplicateButton;
    private boolean requireMedicaidID ;
    private boolean requireMedicareID ;
    private boolean childrensHealthHome=false;
    private boolean minorFlag=false;
    private boolean canConsentEnrollMinors=false;   
    private String minorNote=null;
//    private Button activityTrackerButton;
//    private String activityTrackerButtonText;

    //JIRA 1024   
    private ManagePatientProgramServiceAsync managePatientProgramService = GWT.create(ManagePatientProgramService.class);    
    private boolean warnConsentStatusChangeFlag;
    private String warnConsentStatusChange;
    private String warnActivePrograms;
    private String managePatientPrograms;
    private String warnConsentStatusChangeDialogTitle;
    private String warnActiveProgramsDialogTitle;
    private String warnActiveProgramsInactiveButtonText;
//    private final String SPACER_TO_TEXT = "&nbsp&nbsp&nbsp&nbsp&nbsp";
    private int oldActiveProgramsCount;
    private boolean preventPatientInactivation;
    private String dseTitleLabelText;
    private String dseUpdateDescLabelText;
    private String dseUpdateSaveButtonText;    
    private String dseUpdateCloseButtonText;
    
    private PersonDTO unchangedPerson;
    
    ClientApplicationProperties props;

    public ModifyPatientPanel(PersonDTO thePerson) {
       
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        
        super.person = thePerson;
        PatientEnrollmentDTO patientEnrollment = thePerson.getPatientEnrollment();
        applicationContext.putAttribute(Constants.ENROLLMENT_STATUS, patientEnrollment.getStatus());
        
        props = ApplicationContextUtils.getClientApplicationProperties();

        //JIRA 1024

        oldAcuityScore=patientEnrollment.getAcuityScore();
        loadClientProperties(); 
        minorFlag=PropertyUtils.isMinor(thePerson.getDateOfBirth());

        unchangedPerson = thePerson;
        buildGui(thePerson);
        DashBoardApp.restartTimer();
    }

    public void buildGui(PersonDTO thePerson) {

        setPadding(5);
        setMembersMargin(5);

        Label label = new Label("Modify Patient");
        label.setWidth(300);
        label.setHeight(40);
        label.setStyleName("patientPanelTitleLabel");
        addMember(label);
        
        Layout headLayout=buildHeadLayout();
        addMember(headLayout);
        patientForm = new PatientForm(thePerson);

        Layout topButtonBar = buildTopButtonBar();
        addMember(topButtonBar);

        addMember(patientForm);

        Layout bottomButtonBar = buildBottomButtonBar();
        addMember(bottomButtonBar);
        handleMyPatientListGuiItems();
    }
    
    private Layout buildHeadLayout() {

        HLayout headLayout = new HLayout(20);
        headLayout.setWidth(950);
        headLayout.setAlign(Alignment.LEFT);

        Label labelNote = new Label("<b>* indicates required field</b><br> <b>** indicates required for duplicate patient check</b>");
        labelNote.setWidth(300);
        labelNote.setHeight(5);

        Label minorLabel = new Label("<font color='red'>" + minorNote + "</font>");
        minorLabel.setWidth(300);
        minorLabel.setHeight(5);
        headLayout.addMember(labelNote);
        if(childrensHealthHome && minorFlag){
        headLayout.addMember(minorLabel);
        }

        return headLayout;
    }

    private Layout buildTopButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        //buttonLayout.setPadding(10);
        //buttonLayout.setMargin(20);

        buttonLayout.setWidth(950);
        buttonLayout.setAlign(Alignment.CENTER);

        checkDuplicateButton = new Button(props.getDemographicCheckDuplicateButton());
//        checkDuplicateButton.setID("Check if Duplicate");
        saveButton = new Button(props.getDemographicSaveButton());
        saveButton.setID("Save");
        cancelButton = new Button(props.getDemographicCancelButton());
        cancelButton.setID("Cancel");
        
        addToListButton = buildAddToListButton();

           // ------------Activity Tracker is moved to Carebook ----------------
//        activityTrackerButton=new Button(SPACER_TO_TEXT + activityTrackerButtonText + SPACER_TO_TEXT);
//        activityTrackerButton.setID("activityTrackerButtonModifyPatientPanel");
//        activityTrackerButton.setAutoFit(true);

        buttonLayout.addMember(checkDuplicateButton);
        buttonLayout.addMember(saveButton);
        buttonLayout.addMember(cancelButton);
        buttonLayout.addMember(addToListButton);
//        buttonLayout.addMember(addToPatientButton);
//        buttonLayout.addMember(activityTrackerButton);

        checkDuplicateButton.addClickHandler(new CheckIfDuplicateClickHandler());
        saveButton.addClickHandler(new SavePatientClickHandler());
        cancelButton.addClickHandler(new CancelClickHandler());
//        activityTrackerButton.addClickHandler(new activityTackerClickHanddler());        
        
        return buttonLayout;
    }

    private Layout buildBottomButtonBar() {
        HLayout buttonLayout = new HLayout(10);

        buttonLayout.setWidth(950);
        buttonLayout.setAlign(Alignment.CENTER);

        checkDuplicateButton = new Button(props.getDemographicCheckDuplicateButton());
        saveButton = new Button(props.getDemographicSaveButton());
        saveButton.setID("SaveButton");
        cancelButton = new Button(props.getDemographicCancelButton());
        cancelButton.setID("CancelButton");


        buttonLayout.addMember(checkDuplicateButton);
        buttonLayout.addMember(saveButton);
        buttonLayout.addMember(cancelButton);

        checkDuplicateButton.addClickHandler(new CheckIfDuplicateClickHandler());
        saveButton.addClickHandler(new SavePatientClickHandler());
        cancelButton.addClickHandler(new CancelClickHandler());
        return buttonLayout;
    }

    private void updatePatient()
    {
        boolean checkForDuplicate = isDifferent(originalPerson, person, true);
        updatePatient(checkForDuplicate);
    }

    protected void updatePatient(boolean checkForDuplicate) {        
        progressBarWindow = new ProgressBarWindow();
        progressBarWindow.show();

        confirmationMessage = "Save successful.";
        
        PatientInfo patientInfo = patientForm.getPatientInfo();
        boolean checkEnableCareteam=patientInfo.isEnableCareteamPanel();

        boolean checkForDuplicateSsn = !StringUtils.equalsIgnoreCase(originalPerson.getSsn(), person.getSsn());

        boolean chkDuplicatePrimaryMedicaidcareId = !StringUtils.equalsIgnoreCase(originalPerson.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId(),
                person.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId());

        boolean chkDuplicateSecondaryMedicaidcareId = !StringUtils.equalsIgnoreCase(originalPerson.getPatientEnrollment().getSecondaryPayerMedicaidMedicareId(),
                person.getPatientEnrollment().getSecondaryPayerMedicaidMedicareId());

        long oldOrganizationId = originalPerson.getPatientEnrollment().getOrgId();

//        String oldProgramName = originalPerson.getPatientEnrollment().getProgramName();
        
        //patient user creation check
        
        
        boolean checkPatientUserUpdate=false;

        if(originalPerson.getOriginalPatientUserActive().equalsIgnoreCase(person.getPatientUserActive())){
            checkPatientUserUpdate=true;
        }
        System.out.println("checkPatientUserUpdate5:"+checkPatientUserUpdate);
               
        //   save patient demographics
        enrollmentService.updatePatient(person,checkEnableCareteam, checkForDuplicate, checkForDuplicateSsn,
                        chkDuplicatePrimaryMedicaidcareId, chkDuplicateSecondaryMedicaidcareId, null,
                                null, null, null, oldOrganizationId,checkPatientUserUpdate,oldAcuityScore,
                                new SavePatientCallback(progressBarWindow, confirmationMessage));
    }
    protected PersonDTO originalPerson;
    protected PersonDTO person;
    protected String careTeamIdNew;
    protected String careTeamIdOld;
    protected String oldAcuityScore;
    protected ProgressBarWindow progressBarWindow;
    protected String confirmationMessage;

    private boolean isDifferent(PersonDTO originalPerson, PersonDTO person, boolean isDobCheckInStringFormat) {

        boolean firstNameEquals = StringUtils.equalsIgnoreCase(originalPerson.getFirstName(), person.getFirstName());
        boolean lastNameEquals = StringUtils.equalsIgnoreCase(originalPerson.getLastName(), person.getLastName());
        boolean genderEquals = StringUtils.equalsIgnoreCase(originalPerson.getGender(), person.getGender());
        //Spira 5438 : Sometimes Date objects cahnge there value from client to server 
        //reason could be timezone difference or GWT RPC issue. this time GWT RPC call were changing the date object value.
        //So here we are using date as string in update patient case so, comparing strings in case of update.
        boolean dobSameDay;
        if(!isDobCheckInStringFormat){
            dobSameDay = DateUtils.isSameDay(originalPerson.getDateOfBirth(), person.getDateOfBirth());
        }
        else{
            dobSameDay = originalPerson.getDateOfBirthAsString().equalsIgnoreCase(person.getDateOfBirthAsString());
        }

        return !(firstNameEquals && lastNameEquals && genderEquals && dobSameDay);
    }
    
    class SavePatientClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            if (patientForm.validate()) {
                person = patientForm.getPerson();

//                if ((requireMedicaidID == true && requireMedicareID == true  && person.getPatientEnrollment().getPrimaryPayerClass() != null &&  person.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId() == null)||(requireMedicaidID == true && requireMedicareID == true  && person.getPatientEnrollment().getSecondaryPayerClass() != null &&  person.getPatientEnrollment().getSecondaryPayerMedicaidMedicareId() == null)){
//                   SC.warn("Please enter patient's ID"); 
//                    return;
//                }
                if ((person.getPatientEnrollment().getPrimaryPayerClass()!= null && requireMedicaidID == true  && patientForm.primaryPayerClassValues().equalsIgnoreCase("Medicaid")  && person.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId() == null) || (person.getPatientEnrollment().getSecondaryPayerClass()!=null && requireMedicaidID == true  && patientForm.secondaryPayerClassValues().equalsIgnoreCase("Medicaid") && person.getPatientEnrollment().getSecondaryPayerMedicaidMedicareId() == null)) {
//                     if (requireMedicaidID = true && person.getPatientEnrollment().getPrimaryPayerClass() != null||person.getPatientEnrollment().getSecondaryPayerClass() != null &&  person.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId() == null|| person.getPatientEnrollment().getSecondaryPayerMedicaidMedicareId() == null){       
                    SC.warn("Please enter Medicaid ID");
                    return;
                }
                
                if (( person.getPatientEnrollment().getPrimaryPayerClass()!= null && requireMedicareID == true && patientForm.primaryPayerClassValues().equalsIgnoreCase("Medicare") && person.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId() == null) || (person.getPatientEnrollment().getSecondaryPayerClass()!=null && requireMedicareID == true && patientForm.secondaryPayerClassValues().equalsIgnoreCase("Medicare") && person.getPatientEnrollment().getSecondaryPayerMedicaidMedicareId() == null)) {
//                     if (requireMedicaidID = true && person.getPatientEnrollment().getPrimaryPayerClass() != null||person.getPatientEnrollment().getSecondaryPayerClass() != null &&  person.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId() == null|| person.getPatientEnrollment().getSecondaryPayerMedicaidMedicareId() == null){       
                  
                    SC.warn("Please enter Medicare ID");
                    return;
                }

                if (( person.getPatientEnrollment().getPrimaryPayerClass()!= null && patientForm.primaryPayerClassValues().equalsIgnoreCase("Medicare") && person.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId() != null && person.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId().matches(Constants.MEDICAID_ID_ALPHA_NUMBER_FORMAT_REGEX)) || 
                        (person.getPatientEnrollment().getSecondaryPayerClass()!=null && patientForm.secondaryPayerClassValues().equalsIgnoreCase("Medicare") && person.getPatientEnrollment().getSecondaryPayerMedicaidMedicareId() != null && person.getPatientEnrollment().getSecondaryPayerMedicaidMedicareId().matches(Constants.MEDICAID_ID_ALPHA_NUMBER_FORMAT_REGEX))) {
                  
                     SC.warn("Medicare should not have format of AA11111A.");
                     return;
                }
                
                if (person.getPatientEnrollment().getPrimaryPayerClass() == null && person.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId() != null) {
                    SC.warn("Payer class can not be blank if Medicaid/Medicare/Payer Id is entered!");
                    return;
                }


                if (person.getPatientEnrollment().getSecondaryPayerClass() == null && person.getPatientEnrollment().getSecondaryPayerMedicaidMedicareId() != null) {
                    SC.warn("Payer class can not be blank if Medicaid/Medicare/Payer Id is entered!");
                    return;
                }
              originalPerson = patientForm.getOriginalPerson();

          updatePatient();
            }
        }
    }

   /*
     * public void savePatientInfo(PatientInfo patientInfo) { ProgressBarWindow
     * progressBar = new ProgressBarWindow(); progressBar.show();
     *
     * String confirmationMessage = "Save successful.";
     * careteamService.savePatientInfo(patientInfo, new
     * BasicPortalAsyncCallback(progressBar, confirmationMessage)); }
     */
    class CancelClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            EnrollmentPanel enrollmentPanel = ApplicationContextUtils.getEnrollmentPanel();
            enrollmentPanel.showViewPatientPanel(unchangedPerson);
        }
    }
    


	class SavePatientCallback implements AsyncCallback<Object> {

        ProgressBarWindow progressBarWindow;
        String confirmationMessage;

        public SavePatientCallback(ProgressBarWindow theProgressBarWindow, String theConfirmationMessage) {
            progressBarWindow = theProgressBarWindow;
            confirmationMessage = theConfirmationMessage;

         }

        @Override
        public void onSuccess(Object t) {

            cleanup();
            
            // update the "old" values
           PersonDTO person = patientForm.getPerson();
           EnrollmentPanel enrollmentPanel = ApplicationContextUtils.getEnrollmentPanel();
           enrollmentPanel.showViewPatientPanel(person);
        }

        @Override
        public void onFailure(Throwable exc) {

            //get exception
            //check for similar exists
            //if so, show window
            //otherwise, show error message
            PortalException pe = (PortalException) exc;
            if(pe.getDuplicateCheckProcessResult() != null) {
                if (progressBarWindow != null) {
                    progressBarWindow.hide();
                }                
                DuplicateCheckProcessResult result = pe.getDuplicateCheckProcessResult();
                if(result.getProcessDecision().equals(ProcessDecision.SIMILAR_EXISTS)) {
                    SimilarExistsDuringUpdateWindow similarExistsWindow = new SimilarExistsDuringUpdateWindow(person, 
                                    result.getPossibleDuplicates());
                    similarExistsWindow.show();
                }
            }
            else {
                cleanup();

                SC.warn(exc.getMessage());
            }
        }

        private void cleanup() {
            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            if (confirmationMessage != null) {
                SC.say(confirmationMessage);
                
            }
        }
    }

    class CheckIfDuplicateClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            PersonDTO person = patientForm.getPerson();
            originalPerson = patientForm.getOriginalPerson();
            boolean checkForDuplicate = isDifferent(originalPerson, person, false);


            boolean valid = patientForm.validateRequiredFieildsForDupPatients();
            long communityId=ApplicationContextUtils.getLoginResult().getCommunityId();
            if (valid) {
                enrollmentService.checkIfDuplicatePatientModify(person, checkForDuplicate, communityId ,new CheckIfDuplicatePatientCallback());
            }

        }
    }
    

    class SimilarExistsDuringUpdateWindow extends Window {

        private List<SimplePerson> possibleDuplicates;

        private Label label;
        private Label label2;
        private Button saveButton;
        private Button closeButton;
        private PossibleDuplicatePatientPanel pdpPanel;

        private PersonDTO person;
		
        public SimilarExistsDuringUpdateWindow(PersonDTO person,
				List<SimplePerson> possibleDuplicates) {
            this.possibleDuplicates = possibleDuplicates;
            this.person = person;

            buildGui();
        }

        private void buildGui() {

            setWindowProps();

            label = new Label(dseTitleLabelText);
            label.setStyleName("patientPanelTitleLabel");
            label.setHeight(20);
            label.setMargin(5);
            addItem(label);
            
            label2 = new Label(dseUpdateDescLabelText);
            label2.setHeight(20);
            label2.setMargin(5);
            addItem(label2);

            pdpPanel = new PossibleDuplicatePatientPanel(possibleDuplicates);
            addItem(pdpPanel);
            
            saveButton = new Button(dseUpdateSaveButtonText);
            saveButton.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    SimilarExistsDuringUpdateWindow.this.close();
                    updatePatient(false);
                }        
            });

            closeButton = new Button(dseUpdateCloseButtonText);
            closeButton.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent ce) {
                    SimilarExistsDuringUpdateWindow.this.close();
                }
            });
            
            HLayout buttonLayout = new HLayout(10);
            buttonLayout.setHeight(30);
            buttonLayout.setAlign(Alignment.CENTER);
            buttonLayout.addMember(saveButton);
            buttonLayout.addMember(closeButton);
            addItem(buttonLayout);
        }

        private void setWindowProps() {
            setPadding(1);
            setWidth(600);
            setHeight(250);

            setTitle(dseTitleLabelText);
            setShowMinimizeButton(false);
            setIsModal(true);
            setShowModalMask(true);

            centerInPage();
        }
    }    

    public void loadClientProperties(){
        
        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
        requireMedicaidID = props.isRequireMedicaidID();
        requireMedicareID = props.isRequireMedicareID();
        childrensHealthHome=props.isChildrensHealthHome();       
        minorNote=props.getMinorWarning();
//        activityTrackerButtonText=props.getActivityTrackerButtonText();
        
        //JIRA 1024
        warnConsentStatusChangeFlag = props.isWarnConsentStatusChangeFlag();
        warnConsentStatusChange = props.getWarnConsentStatusChange();
        warnActivePrograms = props.getWarnActivePrograms();
        warnConsentStatusChangeDialogTitle = props.getWarnConsentStatusChangeDialogTitle();
        warnActiveProgramsDialogTitle = props.getWarnActiveProgramsDialogTitle();
        warnActiveProgramsInactiveButtonText = props.getWarnActiveProgramsInactiveButtonText();
        
        managePatientPrograms=props.getPatientProgramButtonText();
        preventPatientInactivation=props.isPreventPatientInactivation();
        
        dseTitleLabelText = props.getDseTitleLabelText();
        dseUpdateDescLabelText = props.getDseUpdateDescLabelText();
        dseUpdateSaveButtonText = props.getDseUpdateSaveButtonText();
        dseUpdateCloseButtonText = props.getDseUpdateCloseButtonText();
    }
    
//    public class activityTackerClickHanddler implements ClickHandler{
//            @Override
//            public void onClick(ClickEvent event) {
//            person = patientForm.getPerson();
//            boolean readOnly=false;
//            ActivityTrackerWindow window = new ActivityTrackerWindow(person,readOnly);
//            window.show();
//            }
//        }
}
