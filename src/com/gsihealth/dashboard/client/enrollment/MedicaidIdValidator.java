package com.gsihealth.dashboard.client.enrollment;

import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.validator.CustomValidator;

public class MedicaidIdValidator extends CustomValidator {

    private static final String MEDICAID_PAYER_CLASS_CODE = "1";
    private SelectItem payerClassSelectItem;
    private TextItem medicaidIdTextItem;

    public MedicaidIdValidator(SelectItem thePayerClassSelectItem, TextItem theMedicaidIdTextItem) {
        payerClassSelectItem = thePayerClassSelectItem;
        medicaidIdTextItem = theMedicaidIdTextItem;

        setErrorMessage("Medicaid ID must have the format of AA11111A.");
    }

    @Override
    protected boolean condition(Object value) {
        return validateMedicaidId();
    }

    /**
     * Helper method for validating the Medicaid ID text item. Follows these business rules:
     * 
     * Medicaid IDs are meant to be alphanumeric of the form AA11111A (where A means any letter and 1 means any number).  
     * This is the format that would need to be put into the Patient Tracking Sheet.  
     * As well as having PTS throw out an ID when it does not conform to the format, we should do selective checking on the Medicaid/Medicare/Payer ID format:
     * ·         When Payer Class = ‘Medicaid’ then ID has to be of the form AA11111A else error/warning. (i.e. perform validation)
     * ·         When Payer Class <> ‘Medicaid’ then free text. (i.e. remove validation from the field, do not perform validation)
     * 
     * @return 
     */
    private boolean validateMedicaidId() {

        String payerClassCode = payerClassSelectItem.getValueAsString();

        boolean result = false;

        if (StringUtils.equalsIgnoreCase(payerClassCode, MEDICAID_PAYER_CLASS_CODE)) {

            // validate the field
            if (medicaidIdTextItem != null) {
                String value = medicaidIdTextItem.getValueAsString();

                // if value is not blank, then validate it
                if (StringUtils.isNotBlank(medicaidIdTextItem.getValueAsString())) {
                    result = value.matches(Constants.MEDICAID_ID_ALPHA_NUMBER_FORMAT_REGEX);
                } else {
                    result = true;
                }
            } else {
                result = true;
            }


        } else {
            result = true;

        }

        return result;
    }
}
