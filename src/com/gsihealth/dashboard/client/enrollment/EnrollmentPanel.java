package com.gsihealth.dashboard.client.enrollment;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.client.common.PersonInfo;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

/**
 *
 * @author Chad Darby
 */
public class EnrollmentPanel extends VLayout {

    private static int MAIN_CONTENT_POSITION = 1;

    public EnrollmentPanel() {
        buildGui();
        DashBoardApp.restartTimer();
    }

    private void buildGui() {

        ToolStrip toolStrip = createToolStrip();
        addMember(toolStrip);

        addMember(new PatientSearchPanel());

        setPadding(5);
    }

    protected ToolStrip createToolStrip() {
        ToolStripButton searchPatientButton = new ToolStripButton();
        searchPatientButton.setID("SearchPatient");
        searchPatientButton.setTitle("Search Patient");
        
       // ToolStripButton searchPatientButton = new ToolStripButton("Search Patient");
        
        ToolStripButton addPatientButton = new ToolStripButton("Add Patient");
        addPatientButton.setID("AddPatient");

        ToolStrip toolStrip = new ToolStrip();
        toolStrip.setWidth100();
        toolStrip.addButton(searchPatientButton);
        toolStrip.addSeparator();
        toolStrip.addButton(addPatientButton);

        searchPatientButton.addClickHandler(new SearchPatientClickHandler());
        addPatientButton.addClickHandler(new AddPatientClickHandler());

        return toolStrip;
    }

    public void showViewPatientPanel(PersonDTO thePerson) {
        removeMainContent();

        addMember(new ViewPatientPanel(thePerson));
    }

    public void showModifyPatientPanel(PersonDTO thePerson) {
        removeMainContent();

        addMember(new ModifyPatientPanel(thePerson));
    }

    public void showAddPatientPanel() {
        removeMainContent();

        addMember(new AddPatientPanel());
    }

    public void showAddPatientPanel(PersonInfo personInfo) {
        removeMainContent();

        addMember(new AddPatientPanel(personInfo));
    }

    public void showPatientSearchPanel() {
        removeMainContent();

        addMember(new PatientSearchPanel());
    }

    public void removeMainContent() {

        Canvas[] members = getMembers();

        if (members.length > 1) {
            Canvas mainContent = members[MAIN_CONTENT_POSITION];
            this.removeMember(mainContent);
        }
    }

    class SearchPatientClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            removeMainContent();
            showPatientSearchPanel();
        }
    }

    class AddPatientClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            removeMainContent();
            showAddPatientPanel();
        }
    }
}
