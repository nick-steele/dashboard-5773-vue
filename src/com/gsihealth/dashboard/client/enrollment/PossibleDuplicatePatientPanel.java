package com.gsihealth.dashboard.client.enrollment;

import com.gsihealth.dashboard.client.PatientSearchRecord;
import com.gsihealth.dashboard.client.common.PersonRecord;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.List;

/**
 *
 * @author rmannar
 */
public class PossibleDuplicatePatientPanel extends VLayout {

    private ListGrid listGrid;
    private List<SimplePerson> possibleDuplicates;

    public PossibleDuplicatePatientPanel(List<SimplePerson> possibleDuplicates) {
        buildGui();
        this.possibleDuplicates = possibleDuplicates;
        setData(possibleDuplicates);
    }

    private void buildGui() {

        setMargin(5);

        listGrid = buildListGrid();
        addMember(listGrid);
    }

    private ListGrid buildListGrid() {

        String[] columnNames = {PatientSearchRecord.FIRST_NAME, PatientSearchRecord.LAST_NAME, 
            PatientSearchRecord.GENDER, PatientSearchRecord.DOB};
        listGrid = new ListGrid();

        listGrid.setWidth100();
        listGrid.setHeight100();

        listGrid.setShowAllRecords(true);
        listGrid.setSelectionType(SelectionStyle.SINGLE);

        listGrid.setCanEdit(false);

        // add fields
        ListGridField[] fields = buildListGridFields(columnNames);
        listGrid.setFields(fields);

        listGrid.setEmptyMessage("...");
        listGrid.setCanResizeFields(true);

        return listGrid;
    }

    private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];

        for (int i = 0; i < columnNames.length; i++) {
            String colName = columnNames[i];
            ListGridField tempField = new ListGridField(colName, colName);
            tempField.setCanEdit(false);
            fields[i] = tempField;
        }
        return fields;
    }

    private void setData(List<SimplePerson> possibleDuplicates) {
        // scroll to first row
        listGrid.scrollToRow(1);

        listGrid.setData(buildListGridRecords(possibleDuplicates));
        listGrid.markForRedraw();
    }

    private ListGridRecord[] buildListGridRecords(List<SimplePerson> possibleDuplicates) {
        ListGridRecord[] records = new ListGridRecord[possibleDuplicates.size()];

        int index = 0;
        for (SimplePerson sp : possibleDuplicates) {
            records[index] = createListGridRecord(index, sp);
            index++;
        }

        return records;
    }

    private ListGridRecord createListGridRecord(int idx, SimplePerson sp) {
        PatientSearchRecord record = new PatientSearchRecord();
        record.setFirstName(sp.getFirstName());
        record.setLastName(sp.getLastName());
        record.setGender(sp.getGenderCode());
        record.setDOB(sp.getBirthDateTime());
        record.setIndex(idx);
        return record;
    }
    
    public SimplePerson getSelectedPerson()
    {
        PatientSearchRecord theRecord = (PatientSearchRecord) listGrid.getSelectedRecord();

        if (theRecord == null) {
            SC.warn("Nothing selected.");
            return null;
        }

        int index = theRecord.getIndex();
        return possibleDuplicates.get(index);
    }
}
