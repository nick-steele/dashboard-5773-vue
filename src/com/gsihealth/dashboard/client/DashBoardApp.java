package com.gsihealth.dashboard.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.dom.client.Style.Display;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.RootPanel;
import com.gsihealth.dashboard.client.admintools.AdminToolsPanel;
import com.gsihealth.dashboard.client.alert.AlertPanel;
import com.gsihealth.dashboard.client.callbacks.AlertPreferenceCallback;
import com.gsihealth.dashboard.client.callbacks.DocumentCenterCallback;
import com.gsihealth.dashboard.client.callbacks.GetReferenceDataCallbackAction;
import com.gsihealth.dashboard.client.careteam.CareTeamPanel;
import com.gsihealth.dashboard.client.common.DashboardTitles;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.enrollment.EnrollmentPanel;
import com.gsihealth.dashboard.client.eula.EulaWindow;
import com.gsihealth.dashboard.client.login.BottomPanel;
import com.gsihealth.dashboard.client.login.ChangePasswordWindow;
import com.gsihealth.dashboard.client.login.CommunityListWindow;
import com.gsihealth.dashboard.client.login.LoginPanel;
import com.gsihealth.dashboard.client.reports.*;
import com.gsihealth.dashboard.client.samhsa.SamhsaPanel;
import com.gsihealth.dashboard.client.service.*;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.ApplicationInfo;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.util.EncryptUtils;
import com.gsihealth.dashboard.entity.dto.CommunityDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.redux.Redux;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.JSOHelper;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static com.gsihealth.dashboard.client.util.Constants.*;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class DashBoardApp implements EntryPoint {

    private static LoginPanel loginPanel;
    private static BottomPanel bottomPanel = new BottomPanel();
    private static String userId;
    private static ReferenceDataServiceAsync referenceDataService = GWT.create(ReferenceDataService.class);
    private static ReportServiceAsync reportService = GWT.create(ReportService.class);
    private static SecurityServiceAsync securityService = GWT.create(SecurityService.class);
    private static Label userNameLabel = new Label("First Name LastName");
    private static Label accessLevelUserLevelOrgNameLabel = new Label("Access Level");
    private static Label userCommunityLabel = new Label("Community");
    private static HLayout subHeader;
    private static Timer timer;
    private static int sessionTimeoutInSeconds;
    private static long userIdLong;
    private static Long communityId;
    private static boolean devmode;
    private static DocumentResourceCenterServiceAsync docService = GWT.create(DocumentResourceCenterService.class);
    private static ProgressBarWindow progressBarWindow;
    private static Logger logger = Logger.getLogger(DashBoardApp.class.getName());

    //Attributes for Bottom Pabel
    private static boolean privacyPolicyVisible;
    private static String privacyPolicyFooterFileName;
    private static boolean loginAttributeForSamhsa;

    private ClientApplicationProperties props;
    
    //JIRA 727
    private static final AdminServiceAsync adminService = GWT.create(AdminService.class);
    private static boolean canShowCommunity;
    public static boolean multiTenantLoginPageFlag;
    private static List<CommunityDTO> communityDtoList;

    private static final MyPatientListServiceAsync patientListService = GWT.create(MyPatientListService.class);

    private static WindowManager windManager;
    private static String activePanelDiv = null;



    private static void showDashboard(LoginResult loginResult) {

        //Notify Redux login is successful

        Redux.log("successful login");
        Redux.jsEvent("local:login:success");
        Redux.loginTimer("login_success");
//        //Ready to show dashboard
//        Redux.jsEvent("local:login:ready");
        restartTimer();
        hideFooter();


        // now show main app
        //don't need to render dashboard by GWT
        //RootPanel dashboardApplicationContent = RootPanel.get("dashboardApplicationContent");
        HLayout hLayout = new HLayout();
        hLayout.setLayoutMargin(1);
        hLayout.setWidth(DEFAULT_LAYOUT_WIDTH);
        hLayout.setHeight(DEFAULT_LAYOUT_HEIGHT);

        WindowManager windowManager = new WindowManager();
        windManager = windowManager;
        hLayout.addMember(windowManager);


        //FIX: dashboardPanel is initialized in windowmanager
//        DashboardPanel dashboardPanel = new DashboardPanel();
//        dashboardPanel.setWidth(DEFAULT_LAYOUT_WIDTH);
//        dashboardPanel.setHeight(DEFAULT_LAYOUT_HEIGHT);

        //don't use dashboardPanel-> Angularjs material Home Panel
        //dashboardApplicationContent.add(dashboardPanel);


        // get community id
        communityId = loginResult.getCommunityId();
        //JIRA 727
        //community might have changed here - so, change the label
        if(devmode) {
            userCommunityLabel.setContents("Logged on community: " + loginResult.getCommunityId());
        }
        loadReferenceData(communityId);

        com.google.gwt.user.client.Window.addResizeHandler(new ResizeHandler() {
            @Override
            public void onResize(ResizeEvent resizeEvent) {
                resizeTab();
                resizeReportDialog();
            }
        });

    }

    public static void resizeTab(){

        RootPanel rp = RootPanel.get(activePanelDiv!=null?activePanelDiv:"appHome");
        int w = rp.getOffsetWidth();
        int h = rp.getOffsetHeight();
        for (Layout panel :
                activePanels.values()) {
            if(panel==null) continue;
            panel.setWidth(w);
            panel.setHeight(h-10);
        }
    }

    public static Map<String,Layout> activePanels = new HashMap<String,Layout>();

    public static void showEulaWindow(boolean showSamsha, String email, boolean changePassword) {
        Redux.loginTimer("show_eula");
        EulaWindow window = new EulaWindow(showSamsha, email,  changePassword);
        window.show();
    }

    public static void showSamhsaWindow() {
        if (loginAttributeForSamhsa == true) {
            Redux.loginTimer("show_samhsa");
            SamhsaPanel window = new SamhsaPanel();
            window.show();
        } else {
            showMainApplication();

        }
    }


    public static void showPanel(String divId, Layout layoutPanel, String attribute) {
        RootPanel rootPanel = RootPanel.get(divId);
        rootPanel.clear();
        layoutPanel.setHeight(rootPanel.getOffsetHeight()-10);
        layoutPanel.setWidth(rootPanel.getOffsetWidth());
        rootPanel.add(layoutPanel);
        activePanelDiv=divId;
        activePanels.put(attribute,layoutPanel);
        resizeTab();
        ApplicationContext context = ApplicationContext.getInstance();
        context.putAttribute(attribute, layoutPanel);
    }


    public static void showEnrollment(){
        EnrollmentPanel enrollment = new EnrollmentPanel();
        showPanel("appEnrollment",enrollment, DashboardTitles.ENROLLMENT);

    }

    public static void closeEnrollment(){
        RootPanel panel = RootPanel.get("appEnrollment");
        panel.clear();
        clearPanel(DashboardTitles.ENROLLMENT);
    }

    public static void showAdminPanel() {
        restartTimer();
        AdminToolsPanel adminToolsPanel = new AdminToolsPanel();
        showPanel("appAdmin", adminToolsPanel, DashboardTitles.ADMIN);

    }


    public static void closeAdminPanel(){
        RootPanel adminPanel = RootPanel.get("appAdmin");
        adminPanel.clear();
        clearPanel(DashboardTitles.ADMIN);
    }

    public static void showReportsPanel() {
        restartTimer();
        MainReportPanel mainReportPanel = new MainReportPanel();
        showPanel("appReports", mainReportPanel, DashboardTitles.REPORTS);
    }


    public static void closeReportsPanel(){
        RootPanel adminPanel = RootPanel.get("appReports");
        adminPanel.clear();
        clearPanel(DashboardTitles.REPORTS);
    }

    public static void showCareTeamPanel() {
        restartTimer();
        CareTeamPanel careTeamPanel = new CareTeamPanel();
        showPanel("appCareteams", careTeamPanel, DashboardTitles.CARE_TEAM_ASSIGNMENT);
    }

    public static void closeCareTeamPanel(){
        RootPanel adminPanel = RootPanel.get("appCareteams");
        adminPanel.clear();
        clearPanel(DashboardTitles.CARE_TEAM_ASSIGNMENT);
    }

    public static void showAlertPanel() {
        restartTimer();
        AlertPanel alerts = new AlertPanel();
        alerts.gotoPage(1);
        showPanel("appAlerts", alerts, DashboardTitles.ALERTS);
    }


    public static void closeAlertsPanel(){
        RootPanel adminPanel = RootPanel.get("appAlerts");
        adminPanel.clear();
        clearPanel(DashboardTitles.ALERTS);
    }
//    public static void showTaskPanel() {
//        restartTimer();
//        RootPanel taskListPanel = RootPanel.get("appTaskList");
//        TaskListPanel taskList = new TaskListPanel();
//        showPanel(taskListPanel, taskList, DashboardTitles.TASK_LIST);
//    }
//
//    public static void closeTaskPanel(){
//        RootPanel adminPanel = RootPanel.get("appTaskList");
//        adminPanel.clear();
//    }

    public static void closeCareBookPatientSearch(){
        RootPanel mainPanel = RootPanel.get("appCareBookSearch");
        mainPanel.clear();
    }

    public static void clearPanel(String target){

        ApplicationContext context = ApplicationContext.getInstance();
        Layout layout = (Layout) context.getAttribute(target);
        layout.removeMembers(layout.getMembers());
        context.putAttribute(target, layout);
    }

    public static void updatePanel(String target, String newElement, Layout newPanel){
        ApplicationContext context = ApplicationContext.getInstance();
        Layout layout = (Layout) context.getAttribute(target);
        newPanel.setWidth(DEFAULT_LAYOUT_WIDTH);
        newPanel.setHeight(DEFAULT_LAYOUT_HEIGHT);
        layout.addMember(newPanel);
        context.putAttribute(target, layout);
        context.putAttribute(newElement, newPanel);
    }

    public native String getTenantId() /*-{
        return ''+$wnd.QA.tenantId;
    }-*/;

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {

//        try {

            // Setup everything Redux needs to function in a single location (JSNI exports, etc)...
            Redux.startup();
            //Force initialization
            timer = new SessionTimer();

            //Setting communityId from tenant settings
            communityId = Long.parseLong(getTenantId());
            Redux.log("tenant id: "+ communityId);

            loginPanel = new LoginPanel();

            GetClientApplicationPropertiesAction getClientApplicationPropertiesAction = new GetClientApplicationPropertiesAction(referenceDataService, communityId);
            getClientApplicationPropertiesAction.attempt();

            // BREAK IT BY REMOVE DIV FROM JSP
            clearLoadingIndicator();

            // Calander year range for date item and invalid date range message to DateRange Item global change.
            JavaScriptObject startDateJS = JSOHelper.getJSLogicalDate(new Date(1900, 0, 1));
            JavaScriptObject endDateJS = JSOHelper.getJSLogicalDate(new Date(2020, 0, 1));
            setDefaultDateRange(startDateJS, endDateJS);
            setDefaultDateRangeItem();

            Redux.log("moduleLoad done");


//
//        } catch (Exception exc) {
//            Redux.log(exc.getMessage());
//            exc.printStackTrace();
//            Window.alert("An error occurred starting the application. Please reload the page.");
//        }

    }

    public static void checkToken(String claimToken){
        Redux.loginTimer("login_start");
        securityService.authenticate(claimToken, new TokenCheckCallback());
    }

    private native void setDefaultDateRange(JavaScriptObject startDateJS, JavaScriptObject endDateJS) /*-{
        $wnd.isc.DateItem.addProperties({
            startDate:startDateJS,
            endDate:endDateJS
        });
        }-*/;

    private native void setDefaultDateRangeItem() /*-{
        $wnd.isc.DateRangeItem.addProperties({
            invalidRangeErrorMessage: "To field value cannot be earlier than From field value.",
        });
        }-*/;
//
//    private void buildGui() {
//        showLoginPanel();
//        //  showBottomPanel();
//    }

    public static void showMainApplication() {


        LoginResult loginResult = ApplicationContextUtils.getLoginResult();

        if (loginResult.isMustChangePassword()) {
            String email = loginResult.getEmail();
            DashBoardApp.showChangePasswordWindow(email, false);
        } else if(multiTenantLoginPageFlag && canShowCommunity) {
            //don't forget to reset the flag!
            canShowCommunity = false;
            //show multi-tenant popup
            showCommunityPopupWindow(loginResult);
        } else {
            showDashboard(loginResult);
        }
    }

//    public void showLoginPanel() {
//        RootPanel loginPanelWrapper = RootPanel.get("gwtLoginPanelWrapper");
//        loginPanel = new LoginPanel();
//        loginPanel.addStyleName("gwt-login-panel");
////        loginPanel.setLeft(400);
////        loginPanel.setTop(200);
////        loginPanel.draw();
//        if(loginPanelWrapper!=null){
//            loginPanelWrapper.add(loginPanel);
//        }
//    }

    public static void showBottomPanel() {
        bottomPanel.init(privacyPolicyVisible, privacyPolicyFooterFileName);
        bottomPanel.draw();
    }

    protected static void hideFooter() {

        //show dashboard show entire login panel
        bottomPanel.showDashboardBottomPanelMembers();

        RootPanel footer = RootPanel.get("footer");

        if (footer != null) {
            footer.getElement().getStyle().setDisplay(Display.NONE);
        }
    }

    protected void clearLoadingIndicator() {
        RootPanel loadingWrapper = RootPanel.get("loadingWrapper");
        if (loadingWrapper != null) {
            RootPanel.getBodyElement().removeChild(loadingWrapper.getElement());
        }
    }

    private static void loadReferenceData(long communityId) {
        RetryActionCallback retryAction = new GetReferenceDataCallbackAction(communityId, referenceDataService);
        retryAction.attempt();
    }

    public static String getUserId() {
        return userId;
    }

    public static void setUserId(String theUserId) {
        userId = theUserId;
    }

    protected static void showChangePasswordWindow(String email, boolean showSamsha) {

        Redux.loginTimer("show_change_password");
        // show modal popup window
        ChangePasswordWindow changePasswordWindow = new ChangePasswordWindow(securityService, email, showSamsha);
        changePasswordWindow.show();
    }

    public static void setUserName(String firstName, String lastName) {
        userNameLabel.setContents("Welcome: " + firstName + " " + lastName);
    }

    //JIRA 727
    protected static void showCommunityPopupWindow(LoginResult loginResult) {
        // show modal popup window

        Redux.loginTimer("show_commpopup");
        CommunityListWindow communityListWindow = new CommunityListWindow(loginResult, communityDtoList);
        communityListWindow.show();
    }
    
    public static void completeAppPrerequisite(LoginResult loginResult) {
        //JIRA 727
        long userId = loginResult.getUserId();
        if(multiTenantLoginPageFlag) {
            adminService.getCommunitiesForUser(userId, new GetCommunitiesForUserCallback());
        }

 //       initializeCommunityTagMap(loginResult.getCommunityId());

        ApplicationContext context = ApplicationContext.getInstance();
        context.putAttribute(LOGIN_RESULT, loginResult);
// TODO: deprecated : 5.3
//        VLayout vLayout = new VLayout();
//        vLayout.setWidth("50%");
//        vLayout.setHeight(80);
//        vLayout.setAlign(Alignment.RIGHT);
//
//        DynamicForm navigationForm = new DynamicForm();
//        navigationForm.setHeight(10);
//        navigationForm.setNumCols(8);
//        navigationForm.setWidth(90);
//
//        LinkItem resCenterLinkItem = new LinkItem("resCenter");
//        resCenterLinkItem.setWidth(105);
//        resCenterLinkItem.setShowTitle(false);
//        resCenterLinkItem.setLinkTitle("Resource Center");
//        resCenterLinkItem.setName("ResourceCenter");
//        resCenterLinkItem.addClickHandler(new ResCenterClickHandler());
//
//        LinkItem prefsLinkItem = new LinkItem("prefs");
//        prefsLinkItem.setWidth(65);
//        prefsLinkItem.setShowTitle(false);
//        prefsLinkItem.setLinkTitle("Preferences");
//        prefsLinkItem.setName("Preferences");
//        prefsLinkItem.addClickHandler(new PrefsClickHandler());
//
//        LinkItem helpLinkItem = new LinkItem("help");
//        helpLinkItem.setWidth(30);
//        helpLinkItem.setShowTitle(false);
//        helpLinkItem.setLinkTitle("Help");
//        helpLinkItem.setName("Help");
//        helpLinkItem.addClickHandler(new HelpClickHandler());
//
//        LinkItem logoutLinkItem = new LinkItem("linkItem");
//        logoutLinkItem.setWidth(40);
//        logoutLinkItem.setShowTitle(false);
//        logoutLinkItem.setLinkTitle("Logout");
//        logoutLinkItem.setName("Logout");
//        logoutLinkItem.addClickHandler(new LogoutClickHandler());
//
//        SpacerItem spacerItem1 = new SpacerItem();
//        spacerItem1.setWidth(10);
//
//        //navigationForm.setFields(prefsLinkItem, spacerItem1, helpLinkItem, spacerItem1, logoutLinkItem);
//        navigationForm.setFields(resCenterLinkItem, spacerItem1, prefsLinkItem, spacerItem1, helpLinkItem, spacerItem1, logoutLinkItem);
//
//        subHeader = new HLayout();
//        subHeader.setHeight(10);
//        subHeader.setWidth100();
//        subHeader.setAlign(Alignment.RIGHT);
//        subHeader.addMember(navigationForm);
//
//        //prepare rest of the header
//        //prepare name
//        userNameLabel.setContents("Welcome: " + loginResult.getFirstName() + " " + loginResult.getLastName());
//        userNameLabel.setHeight(20);
//        userNameLabel.setAlign(Alignment.RIGHT);
//
//        //prepare AccessLevel
//        accessLevelUserLevelOrgNameLabel.setContents(loginResult.getAccessLevel() + " / " + loginResult.getUserLevel() + ", " + loginResult.getOrganizationName());
//        accessLevelUserLevelOrgNameLabel.setHeight(20);
//        accessLevelUserLevelOrgNameLabel.setAlign(Alignment.RIGHT);
//
//        if(devmode) {
//            userCommunityLabel.setContents("Logged on community: " + loginResult.getCommunityId());
//            userCommunityLabel.setHeight(20);
//            userCommunityLabel.setAlign(Alignment.RIGHT);
//            vLayout.addMember(userCommunityLabel);
//        }
//        vLayout.addMember(subHeader);
//        vLayout.addMember(userNameLabel);
//        vLayout.addMember(accessLevelUserLevelOrgNameLabel);
//
//        vLayout.setAlign(Alignment.RIGHT);
//
//        RootPanel.get("userHeaderSection").add(vLayout);
    }
    
    //JIRA 727
    static class GetCommunitiesForUserCallback implements AsyncCallback<List<CommunityDTO>> {

      @Override
      public void onSuccess(List<CommunityDTO> communityDtoList) {
          DashBoardApp.communityDtoList = communityDtoList;
          DashBoardApp.canShowCommunity = communityDtoList.size() > 1;

      }

      @Override
      public void onFailure(Throwable thrwbl) {
        DashBoardApp.canShowCommunity = false;
      }
    }

    private static void initializeCommunityTagMap(long communityId) {
        HashMap<Long, Map<String, Long>> communityTagMap = new HashMap<Long, Map<String, Long>>();
        ApplicationContext.getInstance().putAttribute(COMMUNITY_TAG_LIST_MAP, communityTagMap);
        Redux.log("initializing tag list for: "+communityId);
    //    patientListService.getListTags(communityId,new GetListTagsCallback(communityId,communityTagMap));
    }


 /*   static class GetListTagsCallback implements AsyncCallback<List<ListTagDTO>> {

        private long communityId;
        private HashMap<Long, Map<String, Long>> tagMap;

        GetListTagsCallback(long communityId,HashMap<Long, Map<String, Long>> tagMap){
            super();
            this.communityId = communityId;
            this.tagMap = tagMap;
        }

        @Override
        public void onFailure(Throwable throwable) {
            logger.severe("Failed to get list tags for :"+ communityId);
        }

        @Override
        public void onSuccess(List<ListTagDTO> listTagDTOs) {
             Map<String, Long> map = new HashMap<String, Long>();
            for (ListTagDTO listTagDTO:
                 listTagDTOs) {
                map.put(listTagDTO.getTagType(),listTagDTO.getTagId());
            }
            tagMap.put(communityId,map);
        }
    }*/

    static class ResCenterClickHandler implements com.smartgwt.client.widgets.form.fields.events.ClickHandler {

        @Override
        public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
            // Redux: Extracted this into a function so that Angular can call it...
            handleResCenterClick();
        }
    }

    static class PrefsClickHandler implements com.smartgwt.client.widgets.form.fields.events.ClickHandler {

        @Override
        public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
            // Redux: Extracted this into a function so that Angular can call it...
            handlePrefsClick();
        }
    }

    static class HelpClickHandler implements com.smartgwt.client.widgets.form.fields.events.ClickHandler {

        @Override
        public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
            handleHelpClick();
        }
    }

    static class LogoutClickHandler implements com.smartgwt.client.widgets.form.fields.events.ClickHandler {

        @Override
        public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
            handleLogoutClick();
        }
    }

    
    // Returns if we're in devmode or not...
    public static boolean reduxIsDevmode() {
        return devmode;
    }

    // Redux: This is callable by Angular...
    public static void handleResCenterClick() {
            progressBarWindow = new ProgressBarWindow();
            progressBarWindow.show();
            DocumentCenterCallback documentCenterCallback = new DocumentCenterCallback(progressBarWindow);
            LoginResult loginResult = ApplicationContextUtils.getLoginResult();
            userIdLong = loginResult.getUserId();
            long communityId = loginResult.getCommunityId();
            docService.getDocuments(communityId, documentCenterCallback);
    }
    
    // Redux: This is callable by Angular...
    public static void handlePrefsClick() {
            LoginResult loginResult = ApplicationContextUtils.getLoginResult();

            String email = loginResult.getEmail();
            Long communityId = loginResult.getCommunityId();
            Map<String, ApplicationInfo> applicationInfoMap = (Map<String, ApplicationInfo>) ApplicationContext.getInstance().getAttribute(APPLICATION_INFO_MAP);
            userIdLong = loginResult.getUserId();

            AlertPreferenceCallback alertPreferenceCallback = new AlertPreferenceCallback(email, communityId, applicationInfoMap, userIdLong);
            alertPreferenceCallback.attempt();
    }

    // Redux: This is callable by Angular...
    public static void handleHelpClick() {
            DashBoardApp.restartTimer();

            ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();

            String url = props.getHelpUrl();
            String name = "helpwindow";
            String features = "location=no,status=yes,scrollbars=yes,width=1200,height=800,resizable=yes";

            Redux.showAppWindow(url, name, features);
    }

    // Redux: This is callable by Angular...
    public static void handleLogoutClick() {
            SC.ask("Logout", "Are you sure you want to logout?", new LogoutClickCallback());
    }

    public static boolean getStatusEffectiveDateEditable(){
        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
        if(props == null) return false;
        return props.getMppStatusEffectiveDateEditable();
    }

    public static WindowManager getCurrentWindowManager() {
        return windManager;
    }

    static class LogoutClickCallback implements BooleanCallback {

        @Override
        public void execute(Boolean value) {

            if (value.booleanValue()) {
                doLogout();
            }
        }
    }

    //JIRA 1122
    public static void doLogout()
    {
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        //Log out of Dashboard
        securityService.logout(loginResult.getEmail(), loginResult.getToken(), communityId, new LogoutCallback());
    }
    
    //JIRA 1122
    static class LogoutCallback implements AsyncCallback<Void> {
        @Override
        public void onFailure(Throwable thrwbl) {
            logoutAndRedirect();
        }

        @Override
        public void onSuccess(Void t) {
            logoutAndRedirect();
        }

        private void logoutAndRedirect()
        {
            logOutPentahoServer();
            //Spira 6121
            Window.Location.replace(Window.Location.getHref());
        }
    }


    static class GetClientApplicationPropertiesAction extends RetryActionCallback<ClientApplicationProperties> {

        private Long communityId;
        private ReferenceDataServiceAsync referenceDataService;

        public GetClientApplicationPropertiesAction(ReferenceDataServiceAsync referenceDataService, Long communityId) {
            this.communityId = communityId;
            this.referenceDataService = referenceDataService;
        }

        @Override
        public void attempt() {

            Redux.log("attempting " + this.communityId);
            referenceDataService.getClientApplicationProperties(communityId, this);
        }
        

        @Override
        public void onCapture(ClientApplicationProperties props) {
            ApplicationContext context = ApplicationContext.getInstance();
            context.putAttribute(CLIENT_APPLICATION_PROPERTIES_KEY, props);
            communityId = props.getCommunityId();
            sessionTimeoutInSeconds = props.getSessionTimeoutInSeconds();
            devmode = props.isDevmode();
            boolean enableClientMfa=props.isEnableClientMFA();
            boolean allowSubsequentLogin=props.isAllowSubsequentLogin();
            boolean loginLogoVisible = props.isLoginLogoVisible();
            String loginLogoFileName = props.getLoginLogoFileName();
            int loginLogoWidth = props.getLoginLogoWidth();
            int loginLogoHeight = props.getLoginLogoHeight();
            String attest = props.getAttestLabel();
            boolean loginAttributes = props.isLoginAttributesEnable();
            
            loginAttributeForSamhsa = loginAttributes;
            privacyPolicyVisible = props.isPrivacyPolicyVisible();
            privacyPolicyFooterFileName = props.getPrivacyPolicyFooterFileName();
            
            //JIRA 727
            multiTenantLoginPageFlag = props.isMultiTenantLoginPageFlag();
            
            // Disabled for Redux 3/3/16
            //showBottomPanel();

            Redux.log("ClientApplicationPropertiesAction done");

            loginPanel.init(loginLogoVisible, loginLogoFileName, loginLogoWidth, loginLogoHeight, 
                    attest, loginAttributes,enableClientMfa,allowSubsequentLogin, communityId);

        }
    }


    private enum ReportType{
        adminUserList, patientTrackingSheet, patientConsented, patientNotConsented, patientTotals, patientProgramReports, patientAtRisk;
    }

    public static Layout activeReportDetailsPanel;

    public static void resizeReportDialog(){
        if(activeReportDetailsPanel == null) return;
        RootPanel rp = RootPanel.get("reportDetails");
        if(rp==null || !rp.isVisible()) return;
        activeReportDetailsPanel.setWidth(rp.getOffsetWidth()-17);
        activeReportDetailsPanel.setHeight(rp.getOffsetHeight()-17);
    }

    public static void showReportDetails(String reportId){

        ReportType type;
        try {
            type = ReportType.valueOf(reportId);
        } catch (IllegalArgumentException e) {
            //TODO: error handler here
            return;
        }

        Layout panel;
        String attribute;
        if(type == ReportType.patientTrackingSheet){

//           Copied from  MainReportPanel.PatientTrackingClickHandler

            LoginResult loginResult = ApplicationContextUtils.getLoginResult();
            long communityId=loginResult.getCommunityId();
            long userId = loginResult.getUserId();
            long accessLevelId = loginResult.getAccessLevelId();
//            long organizationId = loginResult.getOrganizationId();
//            String oid = loginResult.getOid();


            reportService.findPTSRecordsCount(communityId, userId, accessLevelId, new AsyncCallback<Long>() {
                @Override
                public void onFailure(Throwable thrwbl) {
                    SC.warn(thrwbl.getMessage());
                }

                @Override
                public void onSuccess(Long t) {

                    if(t>10000){
                        ReportsGenerationDurationWindow window= new ReportsGenerationDurationWindow();
                        window.show();

                    }
                    else{

                        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
                        long communityId=loginResult.getCommunityId();
                        long userId = loginResult.getUserId();
                        long accessLevelId = loginResult.getAccessLevelId();
                        long organizationId = loginResult.getOrganizationId();
                        String oid = loginResult.getOid();
                        StringBuilder url = new StringBuilder(ReportUtils.getCurrentPageUrl());

                        // add reference to export servlet
                        url.append("/reports/dohPatientTracking/patient_tracking_sheet_" + ReportUtils.getTimeStamp() + ".xlsx");

                        String encryptedArgs = null;

                        try {
                            // org, access, oid
                            StringBuilder data = new StringBuilder();
                            data.append(organizationId + ",");
                            data.append(accessLevelId + ",");
                            data.append(oid + ",");
                            data.append(userId + ",");
                            data.append(communityId);

                            encryptedArgs = EncryptUtils.encode(data.toString());
                        } catch (Exception exc) {
                        }

                        url.append("?p=" + encryptedArgs);

                        Window.open(url.toString(), "_blank", "");
                    }
                }
            });

            return;
        }

        switch(type){
            case adminUserList:
                panel = new ListUsersReportPanel();
                attribute = DashboardTitles.LIST_USERS_REPORT;
                break;
            case patientConsented:
                panel = new EnrolledPatientReportPanel();
                attribute = DashboardTitles.CONSENTED_PATIENTS_REPORT;
                break;
            case patientNotConsented:
                panel = new NotEnrolledPatientReportPanel();
                attribute = DashboardTitles.PATIENTS_NOT_CONSENTED_REPORT;
                break;
            case patientTotals:
                panel = new AggregatePatientCountReportPanel();
                attribute = DashboardTitles.PATIENT_COUNT_REPORT;
                break;
            case patientProgramReports:
                panel = new PatientProgramReportPanel();
                attribute = DashboardTitles.PATIENT_PROGRAM_REPORT;
                break;
            case patientAtRisk:
                panel = new PatientProgramReportPanel();
                attribute = "Patients At Risk";
                break;
            default:
                //TODO: error here
                return;
        }
        RootPanel rp = RootPanel.get("reportDetails");
        int w = rp.getOffsetWidth();
        int h = rp.getOffsetHeight();
        rp.clear();
        rp.add(panel);
        panel.setWidth(w-17);
        panel.setHeight(h-17);
        activeReportDetailsPanel = panel;

        ApplicationContext context = ApplicationContext.getInstance();
        context.putAttribute(attribute, panel);
    }

    public static void restartTimer() {
        if(timer==null)Redux.log("timer null??");
        else{
            timer.cancel();
            startTimer();
        }
    }

    public static void startTimer() {
        // start timer
        if(timer==null){
            Redux.log("start timer is null");
            timer = new SessionTimer();
        }
        timer.schedule(sessionTimeoutInSeconds * 1000);
        Redux.jsEvent("local:timer:sessionReset");
    }

    static class SessionTimer extends Timer {

        public void run() {

            Redux.jsEvent("local:session:expired");

            //Removed for 5.3
            // stop alerts timer
//            DashboardPanel dashboardPanel = (DashboardPanel) ApplicationContext.getInstance().getAttribute(DASHBOARD_PANEL_KEY);
//            dashboardPanel.stopAlertTimer();
//
//            // alert user of time out
//            int minutes = sessionTimeoutInSeconds / 60;
//            String message = "Your session has expired due to " + minutes + " minutes of inactivity. Please log in again.";
//
//            SC.warn(message, new SendToLogoutPageCallback());
        }
    }

    static class SendToLogoutPageCallback implements BooleanCallback {

        @Override
        public void execute(Boolean value) {
            //This is the timed automatic logout
            doLogout();
        }
    }

    static void logOutPentahoServer() {

        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        //Logout of Pentaho Server
        if (!StringUtils.isEmpty(loginResult.getSsoPentahoServer())) {
            Window.Location.assign(loginResult.getSsoPentahoServer()
                    + "/pentaho/Logout?userid="
                    + loginResult.getReportingRole()
                    + "&password="
                    + loginResult.getReportingRole() + "123");
        }
    }


    public static class TokenCheckCallback implements AsyncCallback<LoginResult> {


        @Override
        public void onFailure(Throwable thrwbl) {
            SC.warn("Error during login. : " + thrwbl.getMessage() + " : " + thrwbl.getCause());
            Redux.loginTimer("login_failure");
            Redux.jsEvent("local:login:failed");
        }

        @Override
        public void onSuccess(LoginResult loginResult) {
            // Service should return error code if user is not authenticated,
            // but it seems that's not the case with SecurityService.authenticate.
            // Checking isAuthenticated
            Redux.loginTimer("auth_success");
            ApplicationContext context = ApplicationContext.getInstance();
            context.putAttribute(Constants.LOGIN_RESULT, loginResult);
            if(loginResult.isAuthenticated()){
                Redux.log("token check success");
                loginPanel.checkLoginPopups(loginResult);
            }else{
                SC.say("Error", loginResult.getErrorMessage());
                Redux.loginTimer("login_failure");
                Redux.jsEvent("local:login:failed");
            }

        }
    }



}
