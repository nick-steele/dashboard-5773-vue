/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.activitytracker;

/**
 *
 * @author ssingh
 */
public interface ActivityTrackerConstants {
    
    public static final String PATIENT_ID = "Patient ID";
    public static final String DELETE = "Remove";
    public static final String EFFECTIVE_DATE = "Effective Date";
    public static final String CREATION_DATE="Creation Date";
}
