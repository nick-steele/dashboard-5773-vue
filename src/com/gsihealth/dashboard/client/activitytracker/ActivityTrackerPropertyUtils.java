/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.activitytracker;

import com.gsihealth.dashboard.entity.dto.PatientActivityTrackerDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import java.util.List;

/**
 *
 * @author ssingh
 */
public class ActivityTrackerPropertyUtils {
    
      
     public static ListGridRecord[] convert(List<PatientActivityTrackerDTO> theActivities) {
         ListGridRecord[] records = new ListGridRecord[theActivities.size()];
        
        int index = 0;
        for (PatientActivityTrackerDTO tempActivity : theActivities) {
            records[index] = convert(tempActivity);
            index++;
        }
        
        return records;
    }
    
       public static ListGridRecord convert(PatientActivityTrackerDTO activityTrackerDTO) {
        
        ListGridRecord record = new ActivityTrackerRecord().setListGridRecord(activityTrackerDTO);
        record.setAttribute("id", activityTrackerDTO.getId());
        return record;
    }
    
}
