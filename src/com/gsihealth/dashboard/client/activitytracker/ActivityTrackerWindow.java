
package com.gsihealth.dashboard.client.activitytracker;


import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.smartgwt.client.widgets.Window;

/**
 *
 * @author ssingh
 */
public class ActivityTrackerWindow extends Window{
    
    private PersonDTO personDTO;
    private String activityWindowTitleText;
    private boolean readOnly;
    
     public ActivityTrackerWindow(PersonDTO thePersonDTO, boolean readOnly) {
        personDTO = thePersonDTO;
        this.readOnly=readOnly;
        loadClientProperties();
        buildGui();
    }
    
     private void buildGui() {
         DashBoardApp.restartTimer();

        setWindowProps();
        
        ActivityTrackerPanel activityTrackerPanel = new ActivityTrackerPanel(personDTO,readOnly);
         // store in app context for later user
        ApplicationContext.getInstance().putAttribute(Constants.ACTIVITY_TRACKER_PANEL_KEY, activityTrackerPanel);
        addItem(activityTrackerPanel);
        
    }
   private void setWindowProps() {
      //  setPadding(10);
        setWidth(650);
        setHeight(350);
        
        String patientName = personDTO.getFirstName() + " " + personDTO.getLastName();
        setTitle(activityWindowTitleText+" : " + patientName);
        setShowMinimizeButton(false);
        setIsModal(true);
        setShowModalMask(true);
       
        centerInPage();
    } 
   
   public void loadClientProperties(){
       
        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
        activityWindowTitleText=props.getActivityTrackerWindowTitle();
   }
}
