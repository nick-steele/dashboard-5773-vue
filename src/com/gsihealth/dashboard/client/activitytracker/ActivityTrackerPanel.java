
package com.gsihealth.dashboard.client.activitytracker;


import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.callbacks.BasicPortalAsyncCallback;
import com.gsihealth.dashboard.client.common.FormUtils;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.service.PatientActivityTrackerService;
import com.gsihealth.dashboard.client.service.PatientActivityTrackerServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.ValidationUtils;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.PatientActivityTrackerDTO;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceDateField;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridEditEvent;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.types.RowEndEditAction;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.FilterEditorSubmitEvent;
import com.smartgwt.client.widgets.grid.events.FilterEditorSubmitHandler;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author ssingh
 */
public class ActivityTrackerPanel extends VLayout{
    
    private PersonDTO personDTO;
    private long communityId;
    private String activityNameText;
    private String activityValueText;
    private ListGrid listGrid;
    private ToolStripButton addButton;
    private ToolStripButton saveButton;
    private ToolStripButton cancelButton;   
    private static final int NAME_COLUMN =0;
    private static final int VALUE_COLUMN =1;
    private static final int EFFECTIVE_DATE_COLUMN =2;
    private static final int DELETE_COLUMN=3;
    private LoginResult loginResult;
    private boolean updateMode=false;
    private boolean readOnly;
    private PatientActivityTrackerServiceAsync activityTrackerService = GWT.create(PatientActivityTrackerService.class);
    
    public ActivityTrackerPanel(PersonDTO thePersonDTO,boolean readOnly){
        personDTO = thePersonDTO;
        this.readOnly=readOnly;
        setClientAppProperties();
        communityId= ApplicationContextUtils.getLoginResult().getCommunityId();
        loginResult = ApplicationContextUtils.getLoginResult();
        buildGui();
       
        loadPatientActivityData();
        DashBoardApp.restartTimer();
    }
    
     private void buildGui() {

        setMargin(5);
        
        ToolStrip toolStrip = createToolStrip();
        toolStrip.setVisible(!readOnly);
        addMember(toolStrip);
        
        LayoutSpacer topSpacer = new LayoutSpacer();
        topSpacer.setHeight(10);
        addMember(topSpacer);

        listGrid = buildListGrid();
        setPadding(5);
        addMember(listGrid);

        LayoutSpacer topSpacer1 = new LayoutSpacer();
        topSpacer1.setHeight(10);
        addMember(topSpacer1);

        
              
    }
    
      private ListGrid buildListGrid() {
         
           String [] columnNames = new String[]{activityNameText,activityValueText,ActivityTrackerConstants.EFFECTIVE_DATE,ActivityTrackerConstants.DELETE};
       
        listGrid = new ActivityTrackerListGrid(readOnly);
        listGrid.setSaveLocally(true);

        listGrid.setWidth100();
        listGrid.setHeight100();

        listGrid.setShowAllRecords(true);
        listGrid.setSelectionType(SelectionStyle.SINGLE);
         
        listGrid.setCanEdit(true);
        listGrid.setVirtualScrolling(false);          
        listGrid.setShowRecordComponents(true);
        listGrid.setShowRecordComponentsByCell(true);  

        // add fields
        ListGridField[] fields = buildListGridFields(columnNames);
        listGrid.setFields(fields);

        listGrid.setEmptyMessage("...");
        listGrid.setCanResizeFields(true);
        listGrid.setAutoSaveEdits(false);
        listGrid.setShowFilterEditor(true);
        listGrid.setFilterByCell(true);
        
        listGrid.setFilterOnKeypress(true);
        listGrid.setEditEvent(ListGridEditEvent.CLICK);
//        listGrid.setEditByCell(true);
        
        listGrid.setAutoFetchData(true);  
        
        return listGrid;
    }
     
       private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];

          for (int i = 0; i < columnNames.length; i++) {
              String tempColumnName = columnNames[i];              
              ListGridField tempField = buildListGridField(tempColumnName);
              fields[i] = tempField;

              if (i == NAME_COLUMN) {
                  fields[i].setCanEdit(true);
                  final SelectItem activityNameSelectItem = new SelectItem();
                  LinkedHashMap<String, String> activityNamesMap = FormUtils.buildMap(ApplicationContextUtils.getActivityNamesValue());
                  activityNameSelectItem.setValueMap(activityNamesMap);
                  fields[i].setValueMap(activityNamesMap);
                  fields[i].setAlign(Alignment.CENTER);
                  fields[i].setEditorType(activityNameSelectItem);
               
           } 
            else if (i == VALUE_COLUMN) {
                fields[i].setCanEdit(!readOnly);
                fields[i].setType(ListGridFieldType.TEXT);
                fields[i].setAlign(Alignment.CENTER);
                RegExpValidator alphaNumberRegExValidator = ValidationUtils.getTextRegExValidator();
                fields[i].setValidators(alphaNumberRegExValidator);
                fields[i].setValidateOnChange(true);
            } 
            else if (i ==EFFECTIVE_DATE_COLUMN) {
                fields[i].setCanEdit(!readOnly);
                fields[i].setType(ListGridFieldType.DATE);
                fields[i].setAlign(Alignment.CENTER);
            }
               
          }
          return fields;
       }
      
       private ListGridField buildListGridField(String title) {
           
        ListGridField tempField = new ListGridField(title, title);
        
        tempField.setCanEdit(false);

        return tempField;
    }
       protected ToolStrip createToolStrip() {   
         
        addButton = new ToolStripButton("Add New Activity");
        saveButton = new ToolStripButton("Save");
        cancelButton = new ToolStripButton("Cancel");
      
        ToolStrip toolStrip = new ToolStrip();
        toolStrip.setWidth100();
        toolStrip.setAlign(Alignment.LEFT);
        
        toolStrip.addButton(addButton);
        toolStrip.addSeparator();
        toolStrip.addButton(saveButton);
        toolStrip.addSeparator();
        toolStrip.addButton(cancelButton);
        toolStrip.addSeparator();
        
        addButton.addClickHandler(new addNewClickHandler());
        saveButton.addClickHandler(new SaveClickHandler());
        cancelButton.addClickHandler(new CancelClickHandler());
        
        return toolStrip;
    }
       
     public void populateListGrid(ListGridRecord[] pagedData) {

         // scroll to first row
        listGrid.scrollToRow(1);
      
        final DataSource dataSource = new DataSource();
//        dataSource.setID("dataSource");
        dataSource.setClientOnly(true);
        DataSourceTextField activityName = new DataSourceTextField(activityNameText, "Activity Name");
        activityName.setValueMap(FormUtils.buildMap(ApplicationContextUtils.getActivityNamesValue()));
        activityName.setCanEdit(false);
        DataSourceTextField activityValue = new DataSourceTextField(activityValueText, "Activity Value");
        RegExpValidator alphaNumberRegExValidator = ValidationUtils.getTextRegExValidator();
        activityValue.setValidators(alphaNumberRegExValidator);
        DataSourceDateField effectiveDate = new DataSourceDateField(ActivityTrackerConstants.EFFECTIVE_DATE, "Effective Date");
        DataSourceTextField remove = new DataSourceTextField(ActivityTrackerConstants.DELETE, "Remove");
        remove.setCanFilter(false);
        remove.setCanEdit(false);
        remove.setAttribute("width", "18%");
        remove.setAttribute("Align", "CENTER");
        dataSource.setFields(activityName, activityValue, effectiveDate,remove);
        dataSource.setCacheData(pagedData);
        listGrid.setModalEditing(true); 
        listGrid.addFilterEditorSubmitHandler(new FilterEditorSubmitHandler() {
			
			@Override
			public void onFilterEditorSubmit(FilterEditorSubmitEvent event) {
				if(listGrid.getAllEditRows() != null && listGrid.getAllEditRows().length > 0){
					event.cancel();
					SC.warn("Can not apply filter on unsaved data. First save data then apply filter.");
                                        listGrid.setShowFilterEditor(false);
                                        listGrid.setShowFilterEditor(true);
				}
				
			}
		});
        listGrid.setListEndEditAction(RowEndEditAction.NEXT);  
        listGrid.setDataSource(dataSource);
        listGrid.setData(pagedData);
        listGrid.markForRedraw();
    }
      
      public void clearListGrid() {
          
        ListGridRecord[] records = new ListGridRecord[0];
        
        listGrid.setData(records);
       
    }
     
     class CancelClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            loadPatientActivityData();
        }
    } 
     
    public void loadPatientActivityData() {
       
       LoadPatientActivityDataCallback loadPatientActivityDataCallback = new LoadPatientActivityDataCallback();
        loadPatientActivityDataCallback.attempt();
    }
     class LoadPatientActivityDataCallback extends RetryActionCallback<List<PatientActivityTrackerDTO>> {
       
        @Override
        public void attempt() {
           
            long patientId = personDTO.getPatientEnrollment().getPatientId();
            long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
            activityTrackerService.getPatientActivities(communityId, patientId,this);          
        }
        
        @Override
        public void onCapture(List<PatientActivityTrackerDTO> data) {
           
            if (!data.isEmpty()) {
            ListGridRecord[] records = ActivityTrackerPropertyUtils.convert(data);
            populateListGrid(records);

        } else {
            clearListGrid();
        
        }
    } }
     protected void setClientAppProperties() {

        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
        activityNameText=props.getActivityNameText();
        activityValueText=props.getActivityValueText();
    }
     
     class addNewClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
                        
               listGrid.getField(activityNameText).setCanEdit(true);  
               listGrid.startEditingNew();
                           
        }
    }
     
       class SaveClickHandler implements ClickHandler {

        PatientActivityTrackerDTO activityTrackerDTO = new PatientActivityTrackerDTO();

        @Override
        public void onClick(ClickEvent event) {
           boolean isValid = true;
           int [] rows = listGrid.getAllEditRows();

           for(int j :rows){
              isValid = isValid && listGrid.validateRow(j);
           }
           
           if(isValid){
            long patientId = personDTO.getPatientEnrollment().getPatientId();
            ListGridRecord[] records=new ListGridRecord[rows.length];
            for(int i=0;i<rows.length;i++){
                 records[i] = (ListGridRecord)listGrid.getEditedRecord(rows[i]);
             }
                        
            List<PatientActivityTrackerDTO> dtos = getPatientActivities(records, patientId, communityId);
                        
            ProgressBarWindow progressBarWindow = new ProgressBarWindow("Saving", "Saving...");

            String confirmationMessage = "Save successful.";
            if (!dtos.isEmpty() && validateListGridRecords(dtos)) {
            
                progressBarWindow.show();
                activityTrackerService.addUpdatePatientActivities(dtos, personDTO, new SaveAsyncCallback(progressBarWindow,confirmationMessage));
               }
           } else{
               SC.warn("Entry must contain string upto 40 characters.");
           }
        }}   
       
        class SaveAsyncCallback extends BasicPortalAsyncCallback {

        public SaveAsyncCallback(ProgressBarWindow theProgressBarWindow, String confirmationMessage) {
            super(theProgressBarWindow, confirmationMessage);
        }

        @Override
        public void onSuccess(Object t) {
            progressBarWindow.hide();
            SC.say(confirmationMessage);
            loadPatientActivityData();
        }
        @Override
        public void onFailure(Throwable exc) {
            progressBarWindow.hide();

          SC.warn(exc.getMessage());
        }
    }
       
        public boolean validateListGridRecords(List<PatientActivityTrackerDTO> dtos) {
        boolean validate = true;
        
        for (PatientActivityTrackerDTO patientActivityDTO : dtos) {
            
            long activityNameID = patientActivityDTO.getActivityNameId();
            String avtivityValue=patientActivityDTO.getActivityValue();
          

            if (activityNameID == 0) {
                validate = false;
                String msg = "Activity Name cannot be left blank!";
                SC.say(msg);
            } else if (avtivityValue==null) {
                validate = false;
                SC.warn("Activity Value cannot be left blank");
            }            
        }
        return validate;
    }
        public List<PatientActivityTrackerDTO> getPatientActivities(ListGridRecord[] records, long patientId, long communityId) {

        List<PatientActivityTrackerDTO> dtos = new ArrayList<PatientActivityTrackerDTO>();

        for (ListGridRecord tempRecord : records) {
                LoginResult loginResult = ApplicationContextUtils.getLoginResult();
                PatientActivityTrackerDTO tempDto = new ActivityTrackerRecord(tempRecord, patientId, communityId).getPatientActivitiesDto();
                tempDto.setUserId(loginResult.getUserId());
                                dtos.add(tempDto);
                if (tempDto.getActivityNameId() == 0
                        && tempDto.getActivityValue() == null
                        && tempDto.getEffectiveDate() == null) {
                    dtos.remove(tempDto);
                }
            }
       

        return dtos;
    }
}
