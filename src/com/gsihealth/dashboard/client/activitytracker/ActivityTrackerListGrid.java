/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.activitytracker;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.service.PatientActivityTrackerService;
import com.gsihealth.dashboard.client.service.PatientActivityTrackerServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.ImgButton;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;

/**
 *
 * @author ssingh
 */
public class ActivityTrackerListGrid extends ListGrid {
    
    private PatientActivityTrackerServiceAsync activityTrackerService = GWT.create(PatientActivityTrackerService.class);
    private boolean readOnly;
    
    public ActivityTrackerListGrid(boolean readOnly) {
        this.readOnly = readOnly;
    }
    
    @Override
    protected Canvas createRecordComponent(final ListGridRecord record, Integer colNum) {
        
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        long userId = loginResult.getUserId();
        long activiyTrackerId = 0;
        long communityId;
        
        String fieldName = this.getFieldName(colNum);
        
        if (fieldName.equals(ActivityTrackerConstants.DELETE)) {
            
            communityId = loginResult.getCommunityId();
            
            HLayout recordCanvas = new HLayout(2);
            
            recordCanvas.setHeight(18);
            recordCanvas.setAlign(Alignment.CENTER);
            
            ImgButton removeImage = createGridButton("mypatientlist/remove.png", "Remove");
            removeImage.setVisible(!readOnly);
            if (record != null) {
                removeImage.addClickHandler(new RemoveClickHandler(record, communityId));
            }
            
            recordCanvas.addMember(removeImage);
            
            return recordCanvas;
            
        } else {
            return null;
        }
    }
    
    protected ImgButton createGridButton(String imageSrc, String prompt) {
        ImgButton theImageButton = new ImgButton();
        
        theImageButton.setSrc(imageSrc);
        theImageButton.setPrompt(prompt);
        
        theImageButton.setShowDown(false);
        theImageButton.setShowRollOver(false);
        theImageButton.setAlign(Alignment.CENTER);
        theImageButton.setHeight(16);
        theImageButton.setWidth(16);
        
        return theImageButton;
    }
    
    class RemoveClickHandler implements ClickHandler {
        
        ListGridRecord myRecord;
        long activiyTrackerId;
        long communityId;
        
        RemoveClickHandler(ListGridRecord theRecord, long theCommunityId) {
            myRecord = theRecord;
            communityId = theCommunityId;
        }
        
        @Override
        public void onClick(ClickEvent clickEvent) {
            activiyTrackerId = myRecord.getAttributeAsLong("id");
            if (activiyTrackerId != 0) {
                String message = "Are you sure you want to remove patient activity ?"
                        + "\nPlease note that this will remove patient activity from the system.";
                
                SC.ask("Remove Patient Activity from List", message, new RemoveConfirmationCallback(myRecord, activiyTrackerId, communityId));
            } else {
                SC.say("This record is not present in DB. Click on cancel to remove it");
            }
        }
    }

    class RemoveConfirmationCallback implements BooleanCallback {
        
        ListGridRecord myRecord;
        long id;
        long communityId;
        
        RemoveConfirmationCallback(ListGridRecord theRecord, long activityTrackerId, long theCommunityId) {
            myRecord = theRecord;
            id = activityTrackerId;
            communityId = theCommunityId;
        }
        
        @Override
        public void execute(Boolean value) {
            
            boolean confirmed = value.booleanValue();
            
            if (confirmed) {
                LoginResult loginResult = ApplicationContextUtils.getLoginResult();
                long userId = loginResult.getUserId();

                // remove call
                activityTrackerService.delete(id, communityId, new RemovePatientCallback(myRecord));
                
            }
        }
    }
    
    class RemovePatientCallback implements AsyncCallback {
        
        ListGridRecord myRecord;
        
        RemovePatientCallback(ListGridRecord theRecord) {
            myRecord = theRecord;
        }
        
        @Override
        public void onSuccess(Object t) {
            refreshPatientActivityListGui();
        }
        
        @Override
        public void onFailure(Throwable thrwbl) {
            SC.warn("Error removing patient activity.");
        }
    }
    
    protected void refreshPatientActivityListGui() {
        // refresh gui
        ActivityTrackerPanel patientActivityListPanel = (ActivityTrackerPanel) ApplicationContext.getInstance().getAttribute(Constants.ACTIVITY_TRACKER_PANEL_KEY);
        patientActivityListPanel.loadPatientActivityData();
    }
}
