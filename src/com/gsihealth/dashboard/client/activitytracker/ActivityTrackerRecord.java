/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.activitytracker;

import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.entity.dto.PatientActivityTrackerDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import java.util.Date;

/**
 *
 * @author ssingh
 */
public class ActivityTrackerRecord extends ListGridRecord {
    
    PatientActivityTrackerDTO theActivityTrackerDTO;
    ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
    String activityName=props.getActivityNameText();
    String activityValue=props.getActivityValueText();    
    ListGridRecord listGridRecord;
    long patientId;
    long communityId;
     
    public ActivityTrackerRecord(ListGridRecord listGridRecord, long patientId, long communityId) {
        this.listGridRecord = listGridRecord;
        this.patientId = patientId;
        this.communityId = communityId;
    }

    public ActivityTrackerRecord() {
    }
    
    public ListGridRecord setListGridRecord(PatientActivityTrackerDTO patdto){
       
       listGridRecord = new ListGridRecord();
             
        theActivityTrackerDTO=patdto;
        
        String activityNameId =Constants.DELIMITER_SELECT_ITEM + patdto.getActivityNameId();   
        this.listGridRecord.setAttribute("id",patdto.getId());
        this.listGridRecord.setAttribute(activityName, activityNameId);
        this.listGridRecord.setAttribute(activityValue,patdto.getActivityValue());
        this.listGridRecord.setAttribute(ActivityTrackerConstants.EFFECTIVE_DATE,patdto.getEffectiveDate());
        this.listGridRecord.setAttribute("oldRecord", patdto.isOldRecord());
        this.listGridRecord.setAttribute(ActivityTrackerConstants.DELETE,"");
        this.listGridRecord.setAttribute(ActivityTrackerConstants.PATIENT_ID,patdto.getPatientId());
        this.listGridRecord.setAttribute(ActivityTrackerConstants.CREATION_DATE,patdto.getCreationDate());
        
        return listGridRecord;
    }
     public ListGridRecord getListGridRecord(){
         return this.listGridRecord;
     }
     public PatientActivityTrackerDTO getPatientActivitiesDto() {
        // read the attributes
        theActivityTrackerDTO = new PatientActivityTrackerDTO();
        String activityId = listGridRecord.getAttribute(activityName);
        Date effectDate = listGridRecord.getAttributeAsDate(ActivityTrackerConstants.EFFECTIVE_DATE);
        String actValue = listGridRecord.getAttribute(activityValue);
        Date creationDate=listGridRecord.getAttributeAsDate(ActivityTrackerConstants.CREATION_DATE);
     
        boolean isOld=listGridRecord.getAttributeAsBoolean("oldRecord");
     
        long activityNameId = getId(activityId);
        
        if(listGridRecord.getAttributeAsLong("id")!=null){
        theActivityTrackerDTO.setId(listGridRecord.getAttributeAsLong("id"));
        }
        theActivityTrackerDTO.setActivityNameId(activityNameId);
        theActivityTrackerDTO.setActivityValue(actValue);   
        theActivityTrackerDTO.setEffectiveDate(effectDate);
        theActivityTrackerDTO.setCommunityId(communityId);
        theActivityTrackerDTO.setPatientId(patientId);
      
     
        theActivityTrackerDTO.setOldRecord(isOld);
             
        if (isOld){
            theActivityTrackerDTO.setOperation("Update");
            theActivityTrackerDTO.setCreationDate(creationDate);
        }
        else{
            theActivityTrackerDTO.setOperation("Create");
        }
        
        return theActivityTrackerDTO;
    }
     
      public long getId(String str) {
       
        long Id = 0;

        if (StringUtils.isNotBlank(str)) {
            //for multiple health Home vaule in DB
            if (str.startsWith("_")) {

                Id = Long.parseLong(str.split(Constants.DELIMITER_SELECT_ITEM)[1]);
            } else {
                Id = Long.parseLong(str);

            }
        } else {
            Id = 0;
        }
        return Id;
    }
}
