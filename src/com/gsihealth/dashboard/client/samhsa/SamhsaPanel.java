/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.samhsa;

/**
 * @author rsharan
 */

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.service.SamhsaService;
import com.gsihealth.dashboard.client.service.SamhsaServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;

import java.util.Date;

/**
 *
 * @author rsharan
 */
public class SamhsaPanel extends Window {

    private Button agreeButton;
    private LoginResult loginResult;
    private Button declineButton;
    private CheckboxItem checkboxItem;
    private HTMLPane contentsHTMLPane;
    private final SamhsaServiceAsync samhsaService = GWT.create(SamhsaService.class);

    public SamhsaPanel() {
        buildGui();
        loadSamhsa();
    }

    private void buildGui() {
        setWindowProps();

        Layout contentLayout = buildContentLayout();
        addItem(contentLayout);

        DynamicForm form = new DynamicForm();
        checkboxItem = new CheckboxItem("agree", "I have read and understand the redisclosure warning.");
        checkboxItem.setRequired(true);
        checkboxItem.addChangedHandler(new AcceptChangedHandler());
        form.setPadding(10);

        form.setFields(checkboxItem);
        addItem(form);

        Canvas buttonBar = buildButtonBar();
        addItem(buttonBar);
    }

    private void setWindowProps() {
        setTitle("SAMHSA Redisclosure Warning");
        setPadding(10);
        setWidth(600);
        setHeight(450);
        setShowMinimizeButton(false);
        setShowCloseButton(false);
        setIsModal(true);
        setShowModalMask(true);
        centerInPage();
    }

    private Layout buildContentLayout() throws IllegalStateException {
        HLayout layout = new HLayout(10);

        layout.setPadding(10);
        layout.setWidth100();
        layout.setAlign(Alignment.CENTER);

        contentsHTMLPane = new HTMLPane();
        contentsHTMLPane.setShowEdges(true);
        contentsHTMLPane.setEdgeSize(1);
        contentsHTMLPane.setWidth100();
        contentsHTMLPane.setHeight(300);
        contentsHTMLPane.setPadding(5);
        contentsHTMLPane.setAlign(Alignment.CENTER);

        layout.addMember(contentsHTMLPane);

        return layout;
    }

    private void loadSamhsa() {
        long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
        samhsaService.getActiveSamhsaContent(communityId, new GetActiveSamhsaContentCallback());
    }

    private void setSamhsaContent(String data) {
        contentsHTMLPane.setContents(data);
    }


    class AcceptChangedHandler implements ChangedHandler {

        public void onChanged(ChangedEvent event) {
            boolean checked = (Boolean) event.getValue();

            agreeButton.setDisabled(!checked);
        }
    }

    private Layout buildButtonBar() {
        HLayout buttonLayout = new HLayout(10);

        buttonLayout.setPadding(10);
        buttonLayout.setWidth100();
        buttonLayout.setAlign(Alignment.CENTER);

        agreeButton = new Button("I Agree");
        agreeButton.setDisabled(true);
        buttonLayout.addMember(agreeButton);

        declineButton = new Button("I Decline");
        buttonLayout.addMember(declineButton);

        // register listeners
        agreeButton.addClickHandler(new AgreeClickHandler());
        declineButton.addClickHandler(new DeclineClickHandler());


        return buttonLayout;
    }

    private void updateLoginResults(boolean flag) {
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        loginResult.setSamhsa(flag);
        updateUserSamhsa(loginResult.getEmail(), flag);
    }

    private void updateUserSamhsa(String email, boolean flag) {
        long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
        samhsaService.updateUserSamhsaStatus(email, communityId, flag, new UpdateUserSamhsaAsyncCallback(flag));
    }

    class AgreeClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            updateLoginResults(true);
            SamhsaPanel.this.hide();
            DashBoardApp.showMainApplication();
        }
    }

    class DeclineClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            SC.confirm("Decline", "You are declining the SAMHSA Agreement. You will not be able to access the system.", new DeclineCallback());
        }
    }

    class DeclineCallback implements BooleanCallback {

        @Override
        public void execute(Boolean value) {
            if (value != null && value.booleanValue()) {
                updateLoginResults(false);
            }
        }
    }


    class GetActiveSamhsaContentCallback implements AsyncCallback<String> {

        @Override
        public void onSuccess(String data) {
            setSamhsaContent(data);
        }

        @Override
        public void onFailure(Throwable exc) {
            SC.warn("Error: " + exc.getMessage());
        }
    }


    class UpdateUserSamhsaAsyncCallback implements AsyncCallback {

        boolean accepted;

        UpdateUserSamhsaAsyncCallback(Boolean acceptedFlag) {
            accepted = acceptedFlag;
        }

        @Override
        public void onSuccess(Object data) {
            if (!accepted) {
                logoffUser();
            }
        }

        @Override
        public void onFailure(Throwable exc) {
            SC.warn("Error: " + exc.getMessage());
            logoffUser();
        }
    }


    private void logoffUser() {
        //Spira 6058
        DashBoardApp.doLogout();
    }
}
