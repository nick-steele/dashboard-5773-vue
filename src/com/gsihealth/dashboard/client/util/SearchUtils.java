package com.gsihealth.dashboard.client.util;

/**
 *
 * @author Chad Darby
 */
public class SearchUtils {

    public static String getMaxPatientSearchResultsWarningMessage(int maxPatientSearchResultsSize) {
        return "Your search criteria found more than " +  maxPatientSearchResultsSize + " patients. The application only returned the first " +  maxPatientSearchResultsSize + " patients due to demographic search limitation. Please limit your search criteria.";
    }
}
