package com.gsihealth.dashboard.client.util;

import com.gsihealth.dashboard.redux.Redux;

import java.util.HashMap;
import java.util.Map;

/**
 * Central location to stored client side data. Similar to a ServletContext.
 * 
 * @author Chad Darby
 */
public class ApplicationContext {

    private static Map<String, Object> attributes;
    private static ApplicationContext instance;

    private ApplicationContext() {
        attributes = new HashMap<String, Object>();
    }

    public static ApplicationContext getInstance() {

        if (instance == null) {
            Redux.log("Instantiating app context");
            instance = new ApplicationContext();
        }

        return instance;
    }

    /**
     * Removes the mapping for a key from this map if it is present (optional operation). 
     * 
     * @param key
     * @return
     */
    public Object removeAttribute(String key) {
        return attributes.remove(key);
    }

    /**
     * Associates the specified value with the specified key in this map (optional operation).
     * 
     * @param key
     * @param value
     * @return
     */
    public Object putAttribute(String key, Object value) {
        return attributes.put(key, value);
    }

    /**
     * Returns the value to which the specified key is mapped, or null if this map contains no mapping for the key.
     * 
     * @param key
     * @return
     */
    public Object getAttribute(String key) {
        return attributes.get(key);
    }

    /**
     * Returns true is the key is mapped
     */
    public boolean containsKey(String key) {
        return attributes.containsKey(key);
    }
}
