package com.gsihealth.dashboard.client.util;

import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.layout.Layout;

/**
 *
 * @author Chad Darby
 */
public class LayoutUtils {

    public static void removeAllMembers(Layout theCanvas) {
        Canvas[] members = theCanvas.getMembers();
        theCanvas.removeMembers(members);
    }

}
