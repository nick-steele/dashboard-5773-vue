package com.gsihealth.dashboard.client.util;

import com.gsihealth.dashboard.client.admintools.AdminToolsPanel;
import com.gsihealth.dashboard.client.careteam.CareTeamPanel;
import com.gsihealth.dashboard.client.common.DashboardTitles;
import com.gsihealth.dashboard.client.enrollment.EnrollmentPanel;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.ProgramNameDTO;
import com.gsihealth.dashboard.entity.dto.ConsenterDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Collection of utils to retrieve data from app context
 *
 * @author Chad Darby
 */
public class ApplicationContextUtils {

    /**
     * Return loginResult
     *
     * @return
     */
    public static LoginResult getLoginResult() {

        // get the user in context
        ApplicationContext context = ApplicationContext.getInstance();
        LoginResult loginResult = (LoginResult) context.getAttribute(Constants.LOGIN_RESULT);

        if (loginResult == null) {
            loginResult = new LoginResult();
            context.putAttribute(Constants.LOGIN_RESULT, loginResult);
        }

        return loginResult;
    }

    public static String getQueryString() {
        //get OpenAM SAML token to pass through URL
        ApplicationContext context = ApplicationContext.getInstance();
        String queryString = (String) context.getAttribute("QueryString");
        return queryString;
    }

    public static String[] getStateCodes() {

        ApplicationContext context = ApplicationContext.getInstance();
        String[] data = (String[]) context.getAttribute(Constants.POSTAL_STATE_CODES_KEY);

        return data;
    }

    public static LinkedHashMap<String, String> getGenderCodes() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) context.getAttribute(Constants.GENDER_CODES_KEY);

        return data;
    }

    public static LinkedHashMap<String, String> getRaceCodes() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) context.getAttribute(Constants.RACE_CODES_KEY);

        return data;
    }

    public static LinkedHashMap<String, String> getEthnicCodes() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) context.getAttribute(Constants.ETHNIC_CODES_KEY);

        return data;
    }

    public static LinkedHashMap<String, String> getLanguageCodes() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) context.getAttribute(Constants.LANGUAGE_CODES_KEY);

        return data;
    }

    public static String[] getProgramLevelConsentStatusCodes() {
        ApplicationContext context = ApplicationContext.getInstance();
        String[] data = (String[]) context.getAttribute(Constants.PROGRAM_LEVEL_CONSENT_STATUS_CODES_KEY);

        return data;
    }


    public static LinkedHashMap<Integer, String> getProgramLevelConsentStatusMapCodes() {
        ApplicationContext context = ApplicationContext.getInstance();
        return (LinkedHashMap<Integer, String>) context.getAttribute(Constants.PROGRAM_LEVEL_CONSENT_STATUS_MAP_CODES_KEY);
    }


    public static String[] getProgramNameCodes() {
        ApplicationContext context = ApplicationContext.getInstance();
        String[] data = (String[]) context.getAttribute(Constants.PROGRAM_NAME_CODES_KEY);

        return data;
    }

    public static String[] getUserCredentialCodes() {
        ApplicationContext context = ApplicationContext.getInstance();
        String[] data = (String[]) context.getAttribute(Constants.USER_CREDENTIAL_CODES_KEY);

        return data;
    }

    public static String[] getUserPrefixCodes() {
        ApplicationContext context = ApplicationContext.getInstance();
        String[] data = (String[]) context.getAttribute(Constants.USER_PREFIX_CODES_KEY);

        return data;
    }

    public static String getCarebookURL() {
        ApplicationContext context = ApplicationContext.getInstance();

        ClientApplicationProperties props = (ClientApplicationProperties) context.getAttribute(Constants.CLIENT_APPLICATION_PROPERTIES_KEY);

        String data = props.getCarebookUrl();

        return data;
    }

    public static String getMessageURL() {
        ApplicationContext context = ApplicationContext.getInstance();

        ClientApplicationProperties props = (ClientApplicationProperties) context.getAttribute(Constants.CLIENT_APPLICATION_PROPERTIES_KEY);

        String data = props.getMessageUrl();

        return data;
    }

    public static String getPopulationManagerURL() {
        ApplicationContext context = ApplicationContext.getInstance();

        ClientApplicationProperties props = (ClientApplicationProperties) context.getAttribute(Constants.CLIENT_APPLICATION_PROPERTIES_KEY);

        String data = props.getPopulationManagerUrl();

        return data;
    }

    public static String[] getEnrollmentStatusCodes() {
        ApplicationContext context = ApplicationContext.getInstance();
        String[] data = (String[]) context.getAttribute(Constants.ENROLLMENT_STATUS_CODES_KEY);

        return data;
    }

    public static EnrollmentPanel getEnrollmentPanel() {
        ApplicationContext context = ApplicationContext.getInstance();
        EnrollmentPanel data = (EnrollmentPanel) context.getAttribute(DashboardTitles.ENROLLMENT);

        return data;
    }

    public static CareTeamPanel getCareTeamPanel() {
        ApplicationContext context = ApplicationContext.getInstance();
        CareTeamPanel data = (CareTeamPanel) context.getAttribute(DashboardTitles.CARE_TEAM_ASSIGNMENT);

        return data;
    }

    public static AdminToolsPanel getAdminToolsPanel() {
        ApplicationContext context = ApplicationContext.getInstance();
        AdminToolsPanel data = (AdminToolsPanel) context.getAttribute(DashboardTitles.ADMIN);

        return data;
    }

    public static LinkedHashMap<String, String> getCareteamNames() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) context.getAttribute(Constants.CARE_TEAM_CODES_KEY);

        return data;
    }

    public static ClientApplicationProperties getClientApplicationProperties() {

        ApplicationContext context = ApplicationContext.getInstance();
        ClientApplicationProperties data = (ClientApplicationProperties) context.getAttribute(Constants.CLIENT_APPLICATION_PROPERTIES_KEY);

        return data;
    }

    public static String getSelectedItemProperties() {

        ApplicationContext context = ApplicationContext.getInstance();
        String data = (String) context.getAttribute(Constants.SELECT_ITEM);

        return data;
    }

    public static List<UserDTO> getProvidersForConsentObtainedBy() {

        ApplicationContext context = ApplicationContext.getInstance();
        List<UserDTO> data = (List<UserDTO>) context.getAttribute(Constants.PROVIDERS_FOR_CONSENT_OBTAINED_BY);

        return data;
    }
    
   /* public static List<ConsenterDTO> getConsenters() {

        ApplicationContext context = ApplicationContext.getInstance();
        List<ConsenterDTO> data = (List<ConsenterDTO>) context.getAttribute(Constants.CONSENTERS);

        return data;
    }*/
    
    public static LinkedHashMap<String,String> getConsenters() {

        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<String,String> data = (LinkedHashMap<String,String>) context.getAttribute(Constants.CONSENTERS);

        return data;
    }

//    public static long getSouthwestBrooklynOrgId() {
//        ApplicationContext context = ApplicationContext.getInstance();
//        long data = (Long) context.getAttribute(com.gsihealth.dashboard.common.Constants.SOUTHWEST_BROOKLYN_ORG_ID_KEY);
//
//        return data;
//    }

    public static LinkedHashMap<Long, String> getPatientRelationship() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<Long, String> data = (LinkedHashMap<Long, String>) context.getAttribute(Constants.PATIENT_RELATIONSHIP_CODES_KEY);

        return data;
    }

    public static String[] getDisenrollmentReasons() {
        ApplicationContext context = ApplicationContext.getInstance();
        String[] data = (String[]) context.getAttribute(Constants.ALL_DISENROLLMENT_CODES);

        return data;
    }

    public static String[] getDuplicateCareteamRoles() {
        ApplicationContext context = ApplicationContext.getInstance();
        String[] data = (String[]) context.getAttribute(Constants.DUPLICATE_CARETEAM_ROLES);

        return data;
    }

    public static String[] getRequiredCareteamRoles() {
        ApplicationContext context = ApplicationContext.getInstance();
        String[] data = (String[]) context.getAttribute(Constants.REQUIRED_CARETEAM_ROLES);

        return data;
    } 

    public static LinkedHashMap<String, String> getPayerClassNames() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) context.getAttribute(Constants.PAYER_CLASS);
    
        return data;
    }
    
    public static LinkedHashMap<String, String> getPayerPlanNames() {
    ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) context.getAttribute(Constants.PAYER_PLAN);

        return data;
}

    public static LinkedHashMap<String, String> getEULA() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) context.getAttribute(Constants.EULA);

        return data;
    }
         public static LinkedHashMap<String, String> getSAMHSA() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) context.getAttribute(Constants.SAMHSA);

        return data;
    }
    public static LinkedHashMap<String, String> getCounty() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<String, String> data = (LinkedHashMap<String, String>) context.getAttribute(Constants.COUNTY);

        return data;
    }
    public static String getUhcEnrollmentAppURL() {
        ApplicationContext context = ApplicationContext.getInstance();

        ClientApplicationProperties props = (ClientApplicationProperties) context.getAttribute(Constants.CLIENT_APPLICATION_PROPERTIES_KEY);

        String data = props.getUhcEnrollmentReportUrl();

        return data;
    }
    
     public static String getTaskListAppURL() {
        ApplicationContext context = ApplicationContext.getInstance();

        ClientApplicationProperties props = (ClientApplicationProperties) context.getAttribute(Constants.CLIENT_APPLICATION_PROPERTIES_KEY);

        String data = props.getTaskListAppURL();

        return data;
    }
     
       public static String getPatientListUtilityURL() {
        ApplicationContext context = ApplicationContext.getInstance();

        ClientApplicationProperties props = (ClientApplicationProperties) context.getAttribute(Constants.CLIENT_APPLICATION_PROPERTIES_KEY);

        String data = props.getPatientListUtilityURL();

        return data;
    }


    public static Map<Long, String> getAccessLevelMap() {
        ApplicationContext context = ApplicationContext.getInstance();
        Map<Long, String> data = (Map<Long, String>) context.getAttribute(Constants.ACCESS_LEVEL_MAP);

        return data;
    }

    public static LinkedHashMap<Long, String> getProgramHealthHomeValue() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<Long, String> data = (LinkedHashMap<Long, String>) context.getAttribute(Constants.PROGRAM_HEALTH_HOME_KEY);

        return data;
    }

    public static LinkedHashMap<Long, ProgramNameDTO> getProgramNameForHealthHomeValue() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<Long, ProgramNameDTO> data = (LinkedHashMap<Long, ProgramNameDTO>) context.getAttribute(Constants.PROGRAM_NAME_HEALTHHOME_KEY);

        return data;
    }

    public static LinkedHashMap<Long, String> getUserRoles() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<Long, String> data = (LinkedHashMap<Long, String>) context.getAttribute(Constants.USER_ROLE_KEY);

        return data;
    }
    
    //JIRA 1023
     public static LinkedHashMap<Long, String> getHealthHomeCodes() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<Long, String> data = (LinkedHashMap<Long, String>) context.getAttribute(Constants.PROGRAM_HEALTH_HOME_CODES_KEY);

        return data;
    }

    //JIRA 1023
     public static LinkedHashMap<Long, List<Long>> getProgramForHealthHomeCodes() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<Long, List<Long>> data = (LinkedHashMap<Long, List<Long>>) context.getAttribute(Constants.PROGRAM_FOR_HEALTH_HOME_CODES_KEY);

        return data;
    }
    
      public static LinkedHashMap<Long, String> getProgramNameValue() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<Long, String> data = (LinkedHashMap<Long, String>) context.getAttribute(Constants.PROGRAM_NAME_SELECT_KEY);

        return data;
    }
      
       public static LinkedHashMap<Long, String> getPatientStatusValue() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<Long, String>  data = (LinkedHashMap<Long, String>) context.getAttribute(Constants.PATIENT_STATUS_CODES_KEY);

        return data;
    }
       
       public static String[] getTerminationReasonCodes() {
        ApplicationContext context = ApplicationContext.getInstance();
        String[] data = (String[]) context.getAttribute(Constants.TERMINATION_REASON_CODES_KEY);

        return data;
    }
       
        public static LinkedHashMap<String, String[]> getReasonForPatientStatusValue() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<String, String[]>  data = (LinkedHashMap<String, String[]>) context.getAttribute(Constants.TERMINATION_REASON_FOR_STATUS_KEY);

        return data;
    }
        
       public static LinkedHashMap<Long, String> getHHRequiredForProgramValue() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<Long, String>  data = (LinkedHashMap<Long, String>) context.getAttribute(Constants.HH_REQUIRED_FOR_PROGRAM);

        return data;
    }
         
        public static LinkedHashMap<String, String[]> getHealthHomeForProgram() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<String, String[]>  data = (LinkedHashMap<String, String[]>) context.getAttribute(Constants.HEALTH_HOME_FOR_PROGRAM_KEY);

        return data;
    }
        
        public static String[] getProgramDeleteReasonCodes() {
        ApplicationContext context = ApplicationContext.getInstance();
        String[] data = (String[]) context.getAttribute(Constants.PROGRAM_DELETE_REASON_CODES_KEY);

        return data;
    }
        
        public static LinkedHashMap<Long, String> getPatientStatusEntities() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<Long, String> data = (LinkedHashMap<Long, String>) context.getAttribute(Constants.PATIENT_STATUS_KEY);

        return data;
    }        
      
        public static LinkedHashMap<Long, String> getActivityNamesValue() {
        ApplicationContext context = ApplicationContext.getInstance();
        LinkedHashMap<Long, String> data = (LinkedHashMap<Long, String>) context.getAttribute(Constants.ACIVITY_NAMES_KEY);

        return data;
    }
}
