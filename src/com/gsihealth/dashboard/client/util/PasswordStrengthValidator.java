package com.gsihealth.dashboard.client.util;

import com.smartgwt.client.widgets.form.validator.CustomValidator;

/**
 *
 * @author Chad Darby
 */
public class PasswordStrengthValidator extends CustomValidator {

    public PasswordStrengthValidator() {
        setErrorMessage("Weak password. Password must include at least one capital letter, one lowercase letter, one number, and one symbol. Password length must be a minimum of 8 characters/numbers/symbols.");
    }

    @Override
    protected boolean condition(Object value) {

        if (value != null) {
            String password = value.toString();
            return ValidationUtils.isValidPassword(password);
        }
        else {
            return false;
        }
    }

}
