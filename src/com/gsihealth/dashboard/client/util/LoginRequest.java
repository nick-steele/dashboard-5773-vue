package com.gsihealth.dashboard.client.util;

import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.user.client.Window;
import org.restlet.client.engine.util.Base64;


/**
 *
 * @author Edwin.Montgomery
 */
public class LoginRequest {

    public LoginRequest(RequestCallback callback) {
        //       String url = "http://www.httpwatch.com/httpgallery/authentication/authenticatedimage/default.aspx";
        String url = "http://127.0.0.1:8888/login/treat.jsp";
        //    String url = "http://184.106.144.43/httpgallery/authentication/authenticatedimage/default.aspx";
        Window.alert(url);
        RequestBuilder rb = new RequestBuilder(RequestBuilder.POST, url);
        rb.setHeader("Authorization", createBasicAuthToken());
        rb.setUser("httpwatch");
        rb.setPassword("password");

        System.out.println("Request Header Auth:" + rb.getHeader("Authorization"));
        System.out.println("Request Data:" + rb.getRequestData());
        System.out.println("Url:" + rb.getUrl());
        System.out.println("User::" + rb.getUser());
        System.out.println("Password:" + rb.getPassword());

        rb.setCallback(callback);

        try {
            rb.send();
        } catch (Exception e) {
            Window.alert(e.getMessage());
        }
    }

    private String createBasicAuthToken() {
        byte[] bytes = stringToBytes("httpwatchwrong" + ":" + "token12345678");
        String token = Base64.encode(bytes, false);
        System.out.println("token:" + token);
        //String token = Base64Utils.toBase64(bytes); //Gwt server
        //String token = Base64.encode(bytes).toString();  //core jersey
        return "Basic " + token;
    }

    protected byte[] stringToBytes(String msg) {
        int len = msg.length();
        byte[] bytes = new byte[len];
        for (int i = 0; i < len; i++) {
            bytes[i] = (byte) (msg.charAt(i) & 0xff);
        }
        return bytes;
    }
}
