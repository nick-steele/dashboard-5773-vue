package com.gsihealth.dashboard.client.util;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import java.util.Date;

/**
 *
 * @author Chad Darby
 */
public class DateFieldCellFormatter implements CellFormatter {

    private DateTimeFormat dateFormatter;

    /**
     * Create a format with default format: "MM/dd/yyyy"
     */
    public DateFieldCellFormatter() {
        dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
    }

    /**
     * Create a cell formatter based on given date time format
     *
     * @param theDateTimeFormat
     */
    public DateFieldCellFormatter(DateTimeFormat theDateTimeFormat) {
        dateFormatter = theDateTimeFormat;
    }

    @Override
    public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
        if (value != null) {
            try {
                Date dateValue = (Date) value;
                return dateFormatter.format(dateValue);
            } catch (Exception e) {
                return value.toString();
            }
        } else {
            return "";
        }
    }
}
