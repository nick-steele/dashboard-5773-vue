package com.gsihealth.dashboard.client.util;

import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.form.validator.Validator;

/**
 * Collection of form validation utilities
 *
 * @author Chad Darby
 */
public class ValidationUtils {

    private static int MIN_PASSWORD_LENGTH = 8;
    private static String SPECIAL_SYMBOLS = "`~!@#$%^&*()-_=+{}[]\\|:;'\",./<>?";

     /**
     * Validator for time (hh:mm)
     *
     * @return
     */
    public static RegExpValidator getTimeRegExpValidator() {
        RegExpValidator validator = new RegExpValidator();
        validator.setExpression(Constants.TIME_REGEX);
        validator.setErrorMessage("A valid time is required." );
        return validator;
    }

     /**
     * Validator for alph and numbers
     *
     * @return
     */
    public static RegExpValidator getAlphaNumberCareTeamRegExpValidator() {
        RegExpValidator validator = new RegExpValidator();
        validator.setExpression(Constants.ALPHA_NUMBER_REGEX_CARE_TEAM);
        validator.setErrorMessage("Entry must contain text or numbers only." );
        return validator;
    }

    /**
     * Validator for ALPHA_Hyphen_GraveAccent_Apostrophe_REGEX only
     *
     * @return
     */
    public static RegExpValidator getAlphaHGAARegExValidator() {
        RegExpValidator validator = new RegExpValidator();
        validator.setExpression(Constants.ALPHA_Hyphen_GraveAccent_Apostrophe_REGEX);
        validator.setErrorMessage("Entry must contain text or special symbols (- ' `)  only." );
        return validator;
    }

    public static RegExpValidator getAlphaNumberSpecialSymbolsRegExpValidator() {
        RegExpValidator validator = new RegExpValidator();
        validator.setExpression(Constants.ALPHA_NUMBER_SPECIAL_SYMBOLS_REGEX);
        validator.setErrorMessage("Entry must contain text, numbers or special symbols (- / # ,) only." );
        return validator;
    }

    public static RegExpValidator getAlphaNumberSpecialSymbolsIncludingAmpersandRegExpValidator() {
        RegExpValidator validator = new RegExpValidator();
        validator.setExpression(Constants.ALPHA_NUMBER_SPECIAL_SYMBOLS_INCLUDING_AMPERSAND_REGEX);
        validator.setErrorMessage("Entry must contain text, numbers or special symbols (- / # , &) only." );
        return validator;
    }
    
    /**
     * Validator for alph and numbers
     *
     * @return
     */
    public static RegExpValidator getAlphaNumberRegExpValidator() {
        RegExpValidator validator = new RegExpValidator();
        validator.setExpression(Constants.ALPHA_NUMBER_REGEX);
        validator.setErrorMessage("Entry must contain text or numbers only." );
        return validator;
    }
    
        /**
     * Validator for alph and numbers
     *
     * @return
     */
    public static RegExpValidator getMedicaidIdFormatRegExpValidator() {
        RegExpValidator validator = new RegExpValidator();
        validator.setExpression(Constants.MEDICAID_ID_ALPHA_NUMBER_FORMAT_REGEX);
        validator.setErrorMessage("Medicaid ID must have the format of AA11111A." );
        return validator;
    }

    /**
     * Validator for alpha only
     *
     * @return
     */
    public static RegExpValidator getAlphaOnlyRegExpValidator() {
        RegExpValidator validator = new RegExpValidator();
        validator.setExpression(Constants.ALPHA_ONLY_REGEX);
        validator.setErrorMessage("Entry must contain text only." );
        return validator;
    }

    /**
     * Validator for numbers and periods only
     *
     * @return
     */
    public static RegExpValidator getNumberPeriodsOnlyRegExpValidator() {
        RegExpValidator validator = new RegExpValidator();
        validator.setExpression(Constants.NUMBER_PERIODS_ONLY_REGEX);
        validator.setErrorMessage("Entry must contain numbers and periods only." );
        return validator;
    }

    /**
     * Validator for numbers only
     *
     * @return
     */
    public static RegExpValidator getNumbersOnlyRegExpValidator() {
        RegExpValidator validator = new RegExpValidator();
        validator.setExpression(Constants.NUMBERS_ONLY_REGEX);
        validator.setErrorMessage("Entry must contain numbers only." );
        return validator;
    }

    /**
     * Validator for portal user id
     *
     * @return
     */
    public static RegExpValidator getPortalUserIdRegExpValidator() {
        RegExpValidator validator = new RegExpValidator();
        validator.setExpression(Constants.EMAIL_VALIDATION_REGEX);
        validator.setErrorMessage("ID must be an email address.");
        return validator;
    }

     public static RegExpValidator getDecimalRegExValidator() {
        RegExpValidator validator = new RegExpValidator();
        validator.setExpression(Constants.ACUITY_SCORE_DECIMAL_NUMBER_FORMAT_REGEX);
        validator.setErrorMessage("Entry must contain values greater than 0 and upto 4 decimal places only." );
        return validator;
    }
     
       public static RegExpValidator getTextRegExValidator() {
        RegExpValidator validator = new RegExpValidator();
        validator.setExpression(Constants.ACTIVITY_VALUE_TEXT_FORMAT);
        validator.setErrorMessage("Entry must contain string upto 40 characters." );
        return validator;
    }
    /**
     * Helper method to set validators on a form item
     *
     * @param validator
     * @param item
     */
    public static void setValidators(FormItem item, Validator validator, boolean validateOnExitFlag) {
        item.setValidators(validator);
        item.setValidateOnExit(validateOnExitFlag);
    }


    public static boolean isValidPassword(String password) {

        // fail-fast for null/empty strings
        // don't need to test for this since the "requires" validation
        // will catch this first
        if (StringUtils.isEmpty(password)) {
            return true;
        }

        boolean hasUpper = hasUpper(password);
        boolean hasLower = hasLower(password);
        boolean hasDigit = hasDigit(password);
        boolean noWhitespace = hasNoWhitespace(password);
        boolean hasSpecialSymbol = hasSpecialSymbol(password);

        boolean minLength = password.length() >= MIN_PASSWORD_LENGTH;

        return hasUpper && hasLower && hasDigit && noWhitespace && hasSpecialSymbol && minLength;
    }

    private static boolean hasUpper(String password) {
        boolean result = false;

        for (int i=0; i < password.length(); i++) {
            Character c = password.charAt(i);

            if (Character.isUpperCase(c)) {
                result = true;
                break;
            }
        }

        return result;
    }

    private static boolean hasLower(String password) {
        boolean result = false;

        for (int i=0; i < password.length(); i++) {
            Character c = password.charAt(i);

            if (Character.isLowerCase(c)) {
                result = true;
                break;
            }
        }

        return result;
    }

    private static boolean hasDigit(String password) {
        boolean result = false;

        for (int i=0; i < password.length(); i++) {
            Character c = password.charAt(i);

            if (Character.isDigit(c)) {
                result = true;
                break;
            }
        }

        return result;
    }

    private static boolean hasNoWhitespace(String password) {
        boolean result = true;

        for (int i=0; i < password.length(); i++) {
            char c = password.charAt(i);

            if (c == ' ') {
                result = false;
                break;
            }
        }

        return result;
    }

    private static boolean hasSpecialSymbol(String password) {
        boolean result = false;

        for (int i=0; i < password.length(); i++) {
            Character c = password.charAt(i);

            if (SPECIAL_SYMBOLS.contains(Character.toString(c))) {
                result = true;
                break;
            }
        }

        return result;
    }

}
