package com.gsihealth.dashboard.client.util;

/**
 * Collection of String utilities
 *
 * @author Chad Darby
 */
public class StringUtils {

    /**
     * Returns either the passed in String, or if the String is null, an empty String ("").
     *
     * <pre>
     * StringUtils.defaultString(null)  = ""
     * StringUtils.defaultString("")    = ""
     * StringUtils.defaultString("bat") = "bat"
     * </pre>
     *
     * @param str the String to check, may be null
     * @return the passed in String, or the empty String if it was null
     */
    public static String defaultString(String str) {
        String result = null;

        if (str == null) {
            result = "";
        } else {
            result = str;
        }

        return result;
    }

    /**
     * Checks if a String is empty ("") or null.
     *
     * <pre>
     * StringUtils.isBlank(null)      = true
     * StringUtils.isBlank("")        = true
     * StringUtils.isBlank(" ")       = false
     * StringUtils.isBlank("bob")     = false
     * StringUtils.isBlank("  bob  ") = false
     * </pre>
     *
     * @param str
     * @return
     */
    public static boolean isEmpty(String str) {
        return ((str == null) || (str.length() == 0));
    }

    /**
     * Returns either the passed in String, or if the String is null, an empty String ("").
     *
     * <pre>
     * StringUtils.defaultIfEmpty(null, "foo")  = "foo"
     * StringUtils.defaultIfEmpty("", "foo")    = "foo"
     * StringUtils.defaultIfEmpty("bat", "foo") = "bat"
     * StringUtils.defaultIfEmpty("", null) = null
     * </pre>
     *
     * @param str the String to check, may be null
     * @return the passed in String, or the empty String if it was null
     */
    public static String defaultIfEmpty(String str, String defaultStr) {
        String result = null;

        if (isBlank(str)) {
            result = defaultStr;
        } else {
            result = str;
        }

        return result;
    }

    /**
     * Checks if a String is whitespace, empty ("") or null.
     *
     * <pre>
     * StringUtils.isBlank(null)      = true
     * StringUtils.isBlank("")        = true
     * StringUtils.isBlank(" ")       = true
     * StringUtils.isBlank("bob")     = false
     * StringUtils.isBlank("  bob  ") = false
     * </pre>
     *
     * @param str
     * @return
     */
    public static boolean isBlank(String str) {
        return ((str == null) || (str.trim().length() == 0));
    }

    /**
     * Checks if a String is NOT whitespace, empty ("") or null.
     *
     * <pre>
     * StringUtils.isNotBlank(null)      = false
     * StringUtils.isNotBlank("")        = false
     * StringUtils.isNotBlank(" ")       = false
     * StringUtils.isNotBlank("bob")     = true
     * StringUtils.isNotBlank("  bob  ") = true
     * </pre>
     *
     * @param str
     * @return
     */
    public static boolean isNotBlank(String str) {
        return !isBlank(str);
    }

    /**
     * <p>Compares two Strings, returning <code>true</code> if they are equal.</p>
     *
     * <p><code>null</code>s are handled without exceptions. Two <code>null</code>
     * references are considered to be equal. The comparison is case sensitive.</p>
     *
     * <pre>
     * StringUtils.equals(null, null) = true
     * StringUtils.equals(null, "abc") = false
     * StringUtils.equals("abc", null) = false
     * StringUtils.equals("abc", "abc") = true
     * StringUtils.equals("abc", "ABC") = false
     * </pre>
     *
     * @see java.lang.String#equals(Object)
     * @param str1 the first String, may be null
     * @param str2 the second String, may be null
     * @return <code>true</code> if the Strings are equal, case sensitive, or
     * both <code>null</code>
     */
    public static boolean equals(String s1, String s2) {
        return s1 == null ? s2 == null : s1.equals(s2);
    }

    public static boolean equalsIgnoreCase(String s1, String s2) {
        return s1 == null ? s2 == null : s1.equalsIgnoreCase(s2);
    }

    /**
     * GWT Client side URL encoding
     *
     * @param value - string to be encoded
     * @return UTF-8 encoded Query String
     *
     */
    public static String encode(String value) {
        if (value != null) {
            value = com.google.gwt.http.client.URL.encodeQueryString(value);
        } else {
            value = "";
        }
        return value;
    }
    /**
     * Checks to see if search string is contained in target string
     * 
     * @param targetStr - string to be searched
     * @param searchStr - search string
     */
    public static boolean contains(String targetStr, String searchStr) {
        
        if (isEmpty(targetStr)) {
            return false;
        }
        
        return (targetStr.indexOf(searchStr) != -1);        
    }
    
   /**
     * <p>Removes control characters (char &lt;= 32) from both
     * ends of this String, handling {@code null} by returning
     * {@code null}.</p>
     *
     * <p>The String is trimmed using {@link String#trim()}.
     * Trim removes start and end characters &lt;= 32.
     * To strip whitespace use {@link #strip(String)}.</p>
     *
     * <p>To trim your choice of characters, use the
     * {@link #strip(String, String)} methods.</p>
     *
     * <pre>
     * StringUtils.trim(null)          = null
     * StringUtils.trim("")            = ""
     * StringUtils.trim("     ")       = ""
     * StringUtils.trim("abc")         = "abc"
     * StringUtils.trim("    abc    ") = "abc"
     * </pre>
     *
     * @param str  the String to be trimmed, may be null
     * @return the trimmed string, {@code null} if null String input
     */
    public static String trim(final String str) {
        return str == null ? null : str.trim();
    }    
}
