package com.gsihealth.dashboard.client.util;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.gsihealth.dashboard.common.SimpleDate;
import java.util.Date;

/**
 *
 * @author Chad Darby
 */
public class SimpleDateUtils {

    private static final DateTimeFormat DATE_FORMATTER = DateTimeFormat.getFormat("MM/dd/yyyy");
    
    /**
     * Convert a simple date to regular date
     * @param theSimpleDate
     * @return 
     */
    public static Date getDate(SimpleDate theSimpleDate) {
        String dateStr = theSimpleDate.getMonth() + "/" + theSimpleDate.getDay() + "/" + theSimpleDate.getYear();
        
        return DATE_FORMATTER.parse(dateStr);
    }
    
    /**
     * Convert regular date to simple date
     * 
     * @param theDate
     * @return 
     */
    public static SimpleDate getSimpleDate(Date theDate) {
        
        int month = DateUtils.getMonth(theDate);
        int day = DateUtils.getDay(theDate);
        int year = DateUtils.getYear(theDate);
        
        return new SimpleDate(month, day, year);
    }
}
