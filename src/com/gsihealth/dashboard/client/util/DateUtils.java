package com.gsihealth.dashboard.client.util;

import com.google.gwt.i18n.client.DateTimeFormat;
import java.util.Date;

/**
 *
 * @author Chad Darby
 */
public class DateUtils {

    private static final DateTimeFormat DAY_FORMATTER = DateTimeFormat.getFormat("d");
    private static final DateTimeFormat MONTH_FORMATTER = DateTimeFormat.getFormat("M");
    private static final DateTimeFormat YEAR_FORMATTER = DateTimeFormat.getFormat("yyyy");
    private static final DateTimeFormat FULL_DATE_FORMATTER = DateTimeFormat.getFormat("MM/dd/yyyy HH:mm:ss");

    public static boolean isSameDay(Date date1, Date date2) {

        if (date1 == null || date2 == null) {
            throw new IllegalArgumentException("The date must not be null");
        }

        int month1 = getMonth(date1);
        int day1 = getDay(date1);
        int year1 = getYear(date1);

        int month2 = getMonth(date2);
        int day2 = getDay(date2);
        int year2 = getYear(date2);

        boolean result = (month1 == month2) && (day1 == day2) && (year1 == year2);

        return result;
    }

    public static int getDay(Date date) {
        return Integer.parseInt(DAY_FORMATTER.format(date));
    }

    public static int getMonth(Date date) {
        return Integer.parseInt(MONTH_FORMATTER.format(date));
    }

    public static int getYear(Date date) {
        return Integer.parseInt(YEAR_FORMATTER.format(date));
    }

    /**
     * Return current date with time set to 23:59:59
     * @return 
     */
    public static Date getDateAtEndOfToday() {
        
        Date today = new Date();
        Date theDate = null;
        
        int month = getMonth(today);
        int day = getDay(today);
        int year = getYear(today);
        
        String dateStr = month + "/" + day + "/" + year + " 23:59:59";
        
        theDate = FULL_DATE_FORMATTER.parse(dateStr);
        
        return theDate;
        
    }
}
