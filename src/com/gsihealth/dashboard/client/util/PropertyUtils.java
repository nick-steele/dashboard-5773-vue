package com.gsihealth.dashboard.client.util;

import com.gsihealth.dashboard.common.util.SsnUtils;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.gsihealth.dashboard.client.admintools.organization.OrganizationRecord;
import com.gsihealth.dashboard.client.admintools.payerplan.PayerPlanRecord;
import com.gsihealth.dashboard.client.admintools.user.UserRecord;
import com.gsihealth.dashboard.client.careteam.CareTeamRecord;
import com.gsihealth.dashboard.client.common.PersonRecord;
import com.gsihealth.dashboard.client.enrollment.AggregatePatientCountRecord;
import com.gsihealth.dashboard.client.preferences.AlertPreferenceSearchData;
import com.gsihealth.dashboard.client.reports.EnrolledPatientRecord;
import com.gsihealth.dashboard.client.reports.NotEnrolledPatientRecord;
import com.gsihealth.dashboard.client.reports.PatientProgramRecord;
import com.gsihealth.dashboard.client.reports.UserRecordReport;
import com.gsihealth.dashboard.client.resourcecenter.DocumentRecord;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.*;
import com.gsihealth.dashboard.entity.dto.insurance.PayerClassDTO;
import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Chad Darby
 */
public class PropertyUtils {

    private static DateTimeFormat dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");

    private static String getDateOfBirth(UserDTO theUserDTO) {

        String dateOfBirthStr;

        Date dob = theUserDTO.getDateOfBirth();

        if (dob != null) {
            dateOfBirthStr = dateFormatter.format(dob);
        } else {
            dateOfBirthStr = "";
        }

        return dateOfBirthStr;
    }

    private static String getDate(Date date) {

        String dateOfBirthStr;

        if (date != null) {
            dateOfBirthStr = dateFormatter.format(date);
        } else {
            dateOfBirthStr = "";
        }

        return dateOfBirthStr;
    }

    public static UserRecord convert(int index, UserDTO theUserDTO) {
        UserRecord record = null;

        String firstName = theUserDTO.getFirstName();
        String middleName = theUserDTO.getMiddleName();
        String lastName = theUserDTO.getLastName();
        String organization = theUserDTO.getOrganization();
        String city = theUserDTO.getCity();
        String state = theUserDTO.getState();
        String zipCode = theUserDTO.getZipCode();
        String accessLevel = theUserDTO.getAccessLevel();
        String userRole = theUserDTO.getSecurityRole();
        String status = theUserDTO.getStatus();

        String completeAddress = getCompleteAddress(theUserDTO);

        String dateOfBirth = getDateOfBirth(theUserDTO);

        record = new UserRecord(index, firstName, middleName, lastName, completeAddress, organization, city, state, zipCode, accessLevel, userRole, status, dateOfBirth);

        return record;
    }

    public static PayerPlanRecord convert(int index, PayerPlanDTO thePayerPlanDTO) {

        PayerPlanRecord record = new PayerPlanRecord(thePayerPlanDTO);

        return record;
    }

    public static UserRecordReport convertToUserRecord(int index, UserDTO theUserDTO) {
        UserRecordReport record = null;
        String email = theUserDTO.getEmail();
        String firstName = theUserDTO.getFirstName();
        String middleName = theUserDTO.getMiddleName();
        String lastName = theUserDTO.getLastName();
        String streetAddress1 = theUserDTO.getStreetAddress1();
        String streetAddress2 = theUserDTO.getStreetAddress2();
        String org = theUserDTO.getOrganization();
        String city = theUserDTO.getCity();
        String state = theUserDTO.getState();
        String zipCode = theUserDTO.getZipCode();
        String accessLevel = theUserDTO.getAccessLevel().toString();
        String userRole = theUserDTO.getSecurityRole().toString();
        String lastAccessDate = getDate(theUserDTO.getLastSuccessfulLoginDate());

        record = new UserRecordReport(index, email, firstName, middleName, lastName, streetAddress1, streetAddress2, org, city, state, zipCode, accessLevel, userRole, lastAccessDate);

        return record;
    }

    /**
     * Convert to list grid record
     *
     * @param index
     * @param aggregatePatientCountDTO
     * @return
     */
    public static AggregatePatientCountRecord convert(int index, AggregatePatientCountDTO aggregatePatientCountDTO) {
        AggregatePatientCountRecord record = null;

        record = new AggregatePatientCountRecord();
        record.setIndex(index);
        record.setTotalNumberCandidatesEnrollment(aggregatePatientCountDTO.getTotalNumberCandidatesEnrollment());
        record.setTotalNumberofPatientsAssigned(aggregatePatientCountDTO.getTotalNumberAssignedPatients());
        record.setTotalNumberEnrolledPatients(aggregatePatientCountDTO.getTotalNumberEnrolledPatients());
        record.setTotalNumberPatientsCompletedAssessment(aggregatePatientCountDTO.getTotalNumberPatientsCompletedAssessment());
        record.setTotalNumberPatientsCompletedCarePlan(aggregatePatientCountDTO.getTotalNumberPatientsCompletedCarePlan());

        return record;
    }

    public static OrganizationRecord convert(int index, OrganizationDTO theOrgDTO) {
        OrganizationRecord record = null;

        String organizationName = theOrgDTO.getOrganizationName();
        String oid = theOrgDTO.getOID();
        String facilityType = theOrgDTO.getFacilityTypeDesc();
        String physicalStreetAddress1 = theOrgDTO.getPhysicalStreetAddress1();
        String workPhone = theOrgDTO.getWorkPhone();
        record = new OrganizationRecord(index, organizationName, oid, facilityType, physicalStreetAddress1, workPhone);

        return record;
    }

    /**
     * Convert to list grid record
     *
     * @param index
     * @param thePerson
     * @return
     */
    public static EnrolledPatientRecord convertToEnrolledPatientRecord(int index, PersonDTO thePerson) {
        EnrolledPatientRecord record = null;

        record = new EnrolledPatientRecord();
        record.setIndex(index);

        PatientEnrollmentDTO patientEnrollment = thePerson.getPatientEnrollment();

        record.setPatientID(patientEnrollment.getPatientId());
        record.setLastName(thePerson.getLastName());
        record.setFirstName(thePerson.getFirstName());
        record.setMiddleName(thePerson.getMiddleName());
        record.setAddress1(thePerson.getStreetAddress1());
        record.setAddress2(thePerson.getStreetAddress2());
        record.setCity(thePerson.getCity());
        record.setState(thePerson.getState());
        record.setZipCode(thePerson.getZipCode());
        record.setOrg(patientEnrollment.getOrganizationName());

        record.setSource(thePerson.getSource());
        record.setEnrollmentStatus(patientEnrollment.getStatus());
        if(patientEnrollment.getStatus().equals("Assigned")){
              record.setCareTeam(thePerson.getCareteam());
        }
      	
        record.setPatientUserActive(patientEnrollment.getPatientUserActive());

        // handle ssn
        String theSsn = thePerson.getSsn();
        if (StringUtils.isNotBlank(theSsn)) {
            record.setSsn(SsnUtils.getMaskedSsn(theSsn));
        }

        PayerPlanDTO payerPlan = patientEnrollment.getPrimaryPayerPlan();
        if (payerPlan != null) {
            record.setPrimaryPayerPlan(payerPlan.getName());
        }

        PayerClassDTO payerClass = patientEnrollment.getPrimaryPayerClass();
        if (payerClass != null) {
            record.setPrimaryPayerClass(payerClass.getName());
        }

//        record.setProgramName(patientEnrollment.getProgramName());
//
//        // program effective date
//        Date programDate = patientEnrollment.getProgramNameEffectiveDate();
//        if (programDate != null) {
//            record.setProgramEffectiveDate(getStringProgramDate(programDate));
//        }
//
//        Date programEndDate = patientEnrollment.getProgramEndDate();
//        if (programEndDate != null) {
//            record.setProgramEndDate(getStringProgramDate(programEndDate));
//        }

        record.setMedicaidMedicarePayerId(patientEnrollment.getPrimaryPayerMedicaidMedicareId());
        
//        if(patientEnrollment.getProgramHealthHome()!=null){
//         record.setHealthHome(patientEnrollment.getProgramHealthHome().getHealthHomeName());
//        }
        record.setAcuityScore(patientEnrollment.getAcuityScore());

        return record;
    }

    /**
     * Convert to list grid record
     *
     * @param index
     * @param thePerson
     * @return
     */
    public static NotEnrolledPatientRecord convertToNotEnrolledPatientRecord(int index, PersonDTO thePerson) {
        NotEnrolledPatientRecord record = null;

        record = new NotEnrolledPatientRecord();
        record.setIndex(index);
        record.setPatientID(thePerson.getPatientEnrollment().getPatientId());
        record.setLastName(thePerson.getLastName());
        record.setFirstName(thePerson.getFirstName());
        record.setMiddleName(thePerson.getMiddleName());
        record.setAddress1(thePerson.getStreetAddress1());
        record.setAddress2(thePerson.getStreetAddress2());
        record.setCity(thePerson.getCity());
        record.setState(thePerson.getState());
        record.setZipCode(thePerson.getZipCode());

        PatientEnrollmentDTO patientEnrollment = thePerson.getPatientEnrollment();
        record.setOrg(patientEnrollment.getOrganizationName());
        record.setProgramLevelConsentStatus(patientEnrollment.getProgramLevelConsentStatus());
        record.setEnrollmentStatus(patientEnrollment.getStatus());

        record.setSource(thePerson.getSource());

        // handle ssn
        String theSsn = thePerson.getSsn();
        if (StringUtils.isNotBlank(theSsn)) {
            record.setSsn(SsnUtils.getMaskedSsn(theSsn));
        }

        PayerPlanDTO payerPlan = patientEnrollment.getPrimaryPayerPlan();
        if (payerPlan != null) {
            record.setPrimaryPayerPlan(payerPlan.getName());
        }

        PayerClassDTO payerClass = patientEnrollment.getPrimaryPayerClass();
        if (payerClass != null) {
            record.setPrimaryPayerClass(payerClass.getName());
        }

        record.setMedicaidMedicarePayerId(patientEnrollment.getPrimaryPayerMedicaidMedicareId());
        
        
//       if(patientEnrollment.getProgramHealthHome()!=null){
//         record.setHealthHome(patientEnrollment.getProgramHealthHome().getHealthHomeName());
//        }
//
//       Date programDate = patientEnrollment.getProgramNameEffectiveDate();
//        if (programDate != null) {
//            record.setProgramEffectiveDate(getStringProgramDate(programDate));
//        }
//
//        Date programEndDate = patientEnrollment.getProgramEndDate();
//        if (programEndDate != null) {
//            record.setProgramEndDate(getStringProgramDate(programEndDate));
//        }
       
        record.setAcuityScore(patientEnrollment.getAcuityScore());
        
        return record;
    }

    /**
     * Convert to list grid record
     *
     * @param index
     * @param thePerson
     * @return
     */
    public static PersonRecord convert(int index, PersonDTO thePerson) {
        PersonRecord record = null;
        
        record = new PersonRecord();
        
        if(!thePerson.isNotFoundInMirth()){
            record.setPatientID(thePerson.getPatientEnrollment().getPatientId());
        } else {
            record.setPatientID(Long.valueOf(thePerson.getLocalId()));
            record.setEnabled(Boolean.FALSE);
        }
        record.setIndex(index);
        record.setPersonDTO(thePerson);

        record.setLastName(thePerson.getLastName());
        record.setFirstName(thePerson.getFirstName());
        record.setMiddleName(thePerson.getMiddleName());

        String completeAddress = getCompleteAddress(thePerson);
        
        //Spira 5438 : Sometimes Date objects cahnge there value from client to server 
        //reason could be timezone difference or GWT RPC issue. this time GWT RPC call were changing the date object value.
        //So receive value of dob in String from server to receive as it is.Set to record to display on patient grid.
        String dob = thePerson.getDateOfBirthAsString();
        record.setDateOfBirthAsString(dob);

        record.setGender(thePerson.getGender());

        String careteam = thePerson.getCareteam();
        if(careteam != null) {
            record.setCareteam(careteam.trim().isEmpty() ? "Yes" : careteam);
        }

        PatientEnrollmentDTO patientEnrollment = thePerson.getPatientEnrollment();
         //vinay

        record.setPatientUserActive(patientEnrollment.getPatientUserActive());

        if (patientEnrollment != null) {
//            record.setProgramName(patientEnrollment.getProgramName());
            //record.setOrganizationName(patientEnrollment.getOrganizationName());
//            record.setGender(thePerson.getGender());
            record.setMinor(thePerson.getMinor());
//            record.setMinor(completeAddress);
//            ProgramHealthHomeDTO phhd = patientEnrollment.getProgramHealthHome();
//            if (phhd != null) {
//                record.setHealthHome(phhd.getHealthHomeName());
//            }
        }

        if (ApplicationContextUtils.getClientApplicationProperties().isObfuscateSearchResultsForConsent()) {
            if (thePerson.isSelfAssertEligible()) {
                record.setAddress("<img src=" + "images/mypatientlist/remove.png " + "align='middle'" + ">");
                record.setOrganizationName("<img src=" + "images/mypatientlist/remove.png" + ">");
                record.setEnrollmentStatus("<img src=" + "images/mypatientlist/remove.png" + ">");
            } else {
                record.setAddress(completeAddress);
                record.setOrganizationName(patientEnrollment.getOrganizationName());
                record.setEnrollmentStatus(patientEnrollment.getStatus());
                record.setPatientStatus(thePerson.getPatientStatus());
            }
        } else {
            if (thePerson.isSelfAssertEligible()) {
                record.setOrganizationName("<img src=" + "images/mypatientlist/remove.png" + ">");
            } else {
                record.setOrganizationName(patientEnrollment.getOrganizationName());
            }
            record.setAddress(completeAddress);
            record.setEnrollmentStatus(patientEnrollment.getStatus());
            record.setPatientStatus(thePerson.getPatientStatus());
        }
        return record;
    }

    protected static String getCompleteAddress(PersonDTO thePerson) {
        StringBuilder completeAddress = new StringBuilder();

        String streetAddress1 = thePerson.getStreetAddress1();
        append(completeAddress, streetAddress1);

        String streetAddress2 = thePerson.getStreetAddress2();
        append(completeAddress, streetAddress2);

        String city = thePerson.getCity();
        append(completeAddress, city);

        String state = thePerson.getState();
        append(completeAddress, state);

        String zipCode = thePerson.getZipCode();
        append(completeAddress, zipCode);

        return completeAddress.toString();
    }

    protected static String getCompleteAddress(UserDTO theUser) {
        StringBuilder completeAddress = new StringBuilder();

        String streetAddress1 = theUser.getStreetAddress1();
        append(completeAddress, streetAddress1);

        String streetAddress2 = theUser.getStreetAddress2();
        append(completeAddress, streetAddress2);

        String city = theUser.getCity();
        append(completeAddress, city);

        String state = theUser.getState();
        append(completeAddress, state);

        String zipCode = theUser.getZipCode();
        append(completeAddress, zipCode);

        return completeAddress.toString();
    }

    /**
     * Appends data to completeAddress. If completeAddress is not empty, then
     * prepend comma
     *
     * @param completeAddress
     * @param data
     */
    protected static void append(StringBuilder completeAddress, String data) {
        if (!StringUtils.isBlank(data)) {
            if (!StringUtils.isBlank(completeAddress.toString())) {
                completeAddress.append(", ");
            }
            completeAddress.append(data);
        }
    }

    public static ListGridRecord convertToCareTeamRecord(int index, UserDTO tempUser) {
        CareTeamRecord record = new CareTeamRecord(tempUser);

        record.setIndex(index);
        record.setUserId(tempUser.getUserId());
        record.setFirstName(tempUser.getFirstName());
        record.setLastName(tempUser.getLastName());
        record.setUserLevel(tempUser.getSecurityRole());
        record.setUserLevelId(Long.parseLong(tempUser.getSecurityRoleId()));
        record.setCredential(tempUser.getCredentials());
        record.setPrefix(tempUser.getPrefix());
        record.setOrganization(tempUser.getOrganization());
        record.setStatus(tempUser.getStatus());

        return record;
    }

    public static UserDTO convert(CareTeamRecord tempCareteamRecord) {
        UserDTO user = new UserDTO();

        user.setUserId(tempCareteamRecord.getUserId());
        user.setFirstName(tempCareteamRecord.getFirstName());
        user.setLastName(tempCareteamRecord.getLastName());
        user.setSecurityRole(tempCareteamRecord.getUserLevel());
        user.setSecurityRoleId(Long.toString(tempCareteamRecord.getUserLevelId()));
        user.setPrefix(tempCareteamRecord.getPrefix());
        user.setCredentials(tempCareteamRecord.getCredential());
        user.setOrganization(tempCareteamRecord.getOrganization());
        user.setStatus(tempCareteamRecord.getStatus());

        return user;
    }

    public static PersonRecord convertDashBoard(int index, PersonDTO thePerson) {

        PersonRecord record = null;
        
        record = new PersonRecord();
        record.setIndex(index);
        record.setPersonDTO(thePerson);
        record.setPatientID(thePerson.getPatientEnrollment().getPatientId());
        record.setLastName(thePerson.getLastName());
        record.setFirstName(thePerson.getFirstName());
        record.setMiddleName(thePerson.getMiddleName());
        record.setGender(thePerson.getGender());
        record.setDateOfBirth(thePerson.getDateOfBirth());
        record.setMinor(thePerson.getMinor());
        if (ApplicationContextUtils.getClientApplicationProperties().isObfuscateSearchResultsForConsent()) {
            if (thePerson.isSelfAssertEligible()) {
                 record.setOrgId("<img src=" + "images/mypatientlist/remove.png" + ">");
                 record.setEnrollmentStatus("<img src=" + "images/mypatientlist/remove.png" + ">");
            }else{
                record.setOrgId(thePerson.getPatientEnrollment().getOrganizationName());
                record.setEnrollmentStatus(thePerson.getPatientEnrollment().getStatus());
            }
           
        }else{
            if (thePerson.isSelfAssertEligible()) {
                record.setOrgId("<img src=" + "images/mypatientlist/remove.png" + ">");
            }else{
                record.setOrgId(thePerson.getPatientEnrollment().getOrganizationName());
            }
            record.setEnrollmentStatus(thePerson.getPatientEnrollment().getStatus());
        }
        

        return record;
    }

    public static DocumentRecord convertDocumentData(int index, DocumentResourceCenterDTO theDoc) {


        DocumentRecord record = null;
        record = new DocumentRecord();

        record.setIndex(index);
        record.setDocumentTitle(theDoc.getDocumentTitle());
        record.setDescription(theDoc.getDescription());
        record.setUploadedDate(theDoc.getUploadDate());
        record.setDocFilePath(theDoc.getPathLinkToFile());

        return record;
    }

    public static AlertPreferenceSearchData convertAlertData(int index, AlertPreferenceDTO alert) {

        AlertPreferenceSearchData alertPrefSearchData = new AlertPreferenceSearchData();

        alertPrefSearchData.setIndex(index);
        alertPrefSearchData.setAlertIcon(alert.getAlertIcon());
        alertPrefSearchData.setAlertName(alert.getAlertName());
        alertPrefSearchData.setApplicationID(alert.getApplicationID());
        alertPrefSearchData.setCommunityID(alert.getCommunityID());

        return alertPrefSearchData;
    }

    private static String getStringProgramDate(Date programDate) {
        DateTimeFormat programDateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
        String programDateStr = programDateFormatter.format(programDate);
        return programDateStr;
    }
    
     public static PatientProgramRecord convertToPatientProgramRecord(int index, PersonDTO thePatientDto,PatientProgramDTO thePatientProgDto,boolean healthHomeRequired, String healthHomeLabel, String programEndReasonLabel) {
         PatientProgramRecord record = null;
         String dob;
         record = new PatientProgramRecord();
         record.setIndex(index);
         if(thePatientDto.getDateOfBirth()!=null){
             dob=getStringProgramDate(thePatientDto.getDateOfBirth());
         }
         else{
             dob="";
         }
        
        String gender=thePatientDto.getGender();
        if(gender!=null && gender.equalsIgnoreCase("M")){
            gender="Male";
        }
        else if(gender!=null && gender.equalsIgnoreCase("F")){
             gender="Female";
        }
         String patientInfo =  thePatientDto.getLastName() + "," + thePatientDto.getFirstName() + "  " + gender + "  DOB " + dob + "  PatientId " + thePatientDto.getPatientEnrollment().getPatientId();

         record.setPatientInfo(patientInfo);
         thePatientProgDto.getProgramId();

         record.setLastName(thePatientDto.getLastName());
         record.setFirstName(thePatientDto.getFirstName());
         record.setProgram(thePatientProgDto.getProgram()!=null?thePatientProgDto.getProgram().getValue():"");
//         record.setProgram(thePatientProgDto.getProgram().getValue());
//         ProgramNameDTO pnd = thePatientProgDto.getProgramNameDTO();
//         if (pnd != null) {
//             record.setProgram(pnd.getValue());
//         }
         if(healthHomeRequired){
         record.setAttribute(healthHomeLabel,thePatientProgDto.getHealthHome());
         }
         record.setStatus(thePatientProgDto.getStatus()!=null?thePatientProgDto.getStatus().getValue():"");
    
         Date programEffectiveDate = thePatientProgDto.getProgramEffectiveDate();
         if (programEffectiveDate != null) {
             record.setEffectiveDate(getStringProgramDate(programEffectiveDate));
         }
        Date programEndDate = thePatientProgDto.getProgramEndDate();
        if (programEndDate != null) {
            record.setEndDate(getStringProgramDate(programEndDate));
        }
        Date statusEffectDate = thePatientProgDto.getStatusEffectiveDate();
        if (statusEffectDate != null) {
            record.setStatusEffective(getStringProgramDate(statusEffectDate));
        }
        record.setAttribute(programEndReasonLabel,thePatientProgDto.getTerminationReason());

        return record;
    }

    public static boolean isMinor(Date dateOfBirth) {
        Date dob = dateOfBirth;

        double age = getAge(dob);
        boolean isMinor = false;
        int minorAge = 0;
        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
        minorAge = props.getMinorAge();


        if (dob != null && age < minorAge) {
            isMinor = true;
        } else {
            isMinor = false;
        }

        return isMinor;
    }

    public static double getAge(Date dateOfBirth) {

        Date dob = dateOfBirth;
        double age;
        long ageInMillis = new Date().getTime() - dob.getTime();

        age = ageInMillis / (365.25 * 24 * 60 * 60 * 1000);
        return age;

    }
}
