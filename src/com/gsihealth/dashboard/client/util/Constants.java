package com.gsihealth.dashboard.client.util;

/**
 *
 * @author Chad Darby
 */
public interface Constants {

    public static final String PASSWORD_PLACEHOLDER = "xxxxxxxx";
    //Validations RE
    public static final String ALPHA_NUMBER_SPECIAL_SYMBOLS_REGEX = "^[A-Za-z0-9,#,\\-][^<>!\\\\\"`~&\\[\\]:';@~%_=\\^\\|\\*\\$\\%\\+\\(\\)\\?\\{\\}]*$";
    public static final String ALPHA_NUMBER_SPECIAL_SYMBOLS_INCLUDING_AMPERSAND_REGEX = "^[A-Za-z0-9,&,#,\\-][^<>!\\\\\"`~\\[\\]:';@~%_=\\^\\|\\*\\$\\%\\+\\(\\)\\?\\{\\}]*$";
    public static final String EMAIL_VALIDATION_REGEX = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    public static final String ALPHA_NUMBER_REGEX = "^[A-Za-z0-9][^<>!,\\\\\"`~&\\[\\]:'/;#@~%_=\\^\\|\\*\\$\\%\\+\\-\\(\\)\\?\\{\\}]*$";
    public static final String ALPHA_ONLY_REGEX = "^[A-Za-z][^0-9<>!,\\\\\"`~&\\[\\]:'/;#@~%_=\\^\\|\\*\\$\\%\\+\\-\\(\\)\\?\\{\\}]*$";
    public static final String ALPHA_NUMBER_REGEX_CARE_TEAM = "^[A-Za-z0-9][^<>!,\\\\\"`~&\\[\\]:';@~%_=\\^\\*\\$\\%\\+\\(\\)\\?\\{\\}]*$";
    public static final String NUMBER_PERIODS_ONLY_REGEX = "^[0-9][^A-Za-z<>!,\\\\\"`~&\\[\\]:'/;#@~%_=\\^\\|\\*\\$\\%\\+\\-\\(\\)\\?\\{\\}\\s]*$";
    public static String NUMBERS_ONLY_REGEX = "^[0-9][^A-Za-z<>!,\\\\\"`~&\\[\\]:'/;#@~%_=\\^\\|\\*\\$\\%\\+\\-\\(\\)\\?\\{\\}\\s\\.]*$";
    public static final String NHIN_DIRECT_ENDPOINT_REGEX = "^[A-Za-z0-9][^<>!,\\\\\"`~&\\[\\]';#~%=\\^\\|\\*\\$\\%\\+\\-\\(\\)\\?\\{\\}]*$";
    public static String SERVER_HOST_NAME_REGEX = "^[A-Za-z0-9][^<>!,\\\\\"`~&\\[\\]:'/;#@~%=\\^\\|\\*\\$\\%\\+\\(\\)\\?\\{\\}]*$";
    public static final String ALPHA_Hyphen_GraveAccent_Apostrophe_REGEX = "^[A-Za-z][^0-9<>!,\\\\\"~&\\[\\]:/;#@~%_=\\^\\|\\*\\$\\%\\+\\(\\)\\?\\{\\}]*$";
    public static final String TIME_REGEX = "^(([0]?[1-9]|1[0-2])(:)([0-5][0-9]))$";
    public static final String MEDICAID_ID_ALPHA_NUMBER_FORMAT_REGEX = "^[A-Za-z]{2}[0-9]{5}[A-Za-z]{1}$";
    public static final String ACUITY_SCORE_DECIMAL_NUMBER_FORMAT_REGEX = "^\\s*\\d*(?:\\.\\d{1,4})?\\s*$";
    public static final String ACTIVITY_VALUE_TEXT_FORMAT="^.{1,40}$";
    //Application context keys
    public static final String LOGIN_RESULT = "LOGIN_RESULT";
    public static final String FACILITY_TYPES_KEY = "FACILITY_TYPES_KEY";
    public static final String EHR_VENDORS_KEY = "EHR_VENDORS_KEY";
    public static final String POSTAL_STATE_CODES_KEY = "POSTAL_STATE_CODES_KEY";
    public static final String ADMINISTRATOR_ROLES_KEY = "ADMINISTRATOR_ROLES_KEY";
    public static final String AUTHORIZED_USER_ROLES_KEY = "AUTHORIZED_USER_ROLES_KEY";
    public static final String CURRENT_ORGANIZATION_KEY = "CURRENT_ORGANIZATION_KEY";
    public static final String GENDER_CODES_KEY = "GENDER_CODES_KEY";
    public static final String PROGRAM_LEVEL_CONSENT_STATUS_CODES_KEY = "PROGRAM_LEVEL_CONSENT_STATUS_CODES_KEY";
    String PROGRAM_LEVEL_CONSENT_STATUS_MAP_CODES_KEY = "PROGRAM_LEVEL_CONSENT_STATUS_MAP_CODES_KEY";
    public static final String PROGRAM_NAME_CODES_KEY = "CONSENT_LEVEL_CODES_KEY";
    public static final String ENROLLMENT_STATUS_CODES_KEY = "ENROLLMENT_STATUS_CODES_KEY";
    public static final String CARE_TEAM_CODES_KEY = "CARE_TEAM_CODES_KEY";
    public static final String USER_CREDENTIAL_CODES_KEY = "USER_CREDENTIAL_CODES_KEY";
    public static final String USER_PREFIX_CODES_KEY = "USER_PREFIX_CODES_KEY";
    public static final String USER_STATUS_ACTIVE = "ACTIVE";
    public static final String USER_STATUS_INACTIVE = "INACTIVE";
    public static final String PATIENT_SEARCH_FORM_KEY = "PATIENT_SEARCH_FORM_KEY";
    public static final String VIEW_PATIENT_FORM_KEY = "VIEW_PATIENT_FORM_KEY";
    public static final String SSO_TREAT_URL_KEY = "SSO_TREAT_URL_KEY";
    public static final String SSO_LOGOUT_URL_KEY = "SSO_LOGOUT_URL_KEY";
    public static final String CLIENT_APPLICATION_PROPERTIES_KEY = "CLIENT_APPLICATION_PROPERTIES_KEY";
    public static final String ALL = "All";
    public static final String CONSENT_CONFIRMATION_MESSAGE = "Yosu are requesting access to protected health information of a patient.  <br><br>By clicking <b>Yes</b> below, you affirm that you have obtained the written consent of the patient.";
//    public static final int REPORTS_LIST_GRID_WIDTH = 1250;
    public static final String REPORTS_LIST_GRID_WIDTH = "98%";
    public static final String REPORTS_LIST_GRID_HEIGHT = "90%";
    public static final String SELECT_ITEM = "";
    public static final String ENROLLMENT_STATUS = "ENROLLMENT_STATUS";
    public static final long EMPTY_USER = 0;
    public static final long NO_FORM_ID = 0;
    public static final String MESSAGE_URL = "MESSAGE_URL";
    public static final String APPLICATION_INFO_MAP = "APPLICATION_INFO_MAP";
    public static final String COMMUNITY_TAG_LIST_MAP = "COMMUNITY_TAG_LIST_MAP";
    // error messages
    public static final String SEARCH_DID_NOT_RETURN_ANY_RESULTS = "Search did not return any results. Please modify your search criteria.";
    public static final String ENTER_KEY = "Enter";
    public static final String PROVIDERS_FOR_CONSENT_OBTAINED_BY = "PROVIDERS_FOR_CONSENT_OBTAINED_BY";
    public static final String CONSENTERS = "CONSENTERS";
    public static final String RACE_CODES_KEY = "RACE_CODES_KEY";
    public static final String ETHNIC_CODES_KEY = "ETHNIC_CODES_KEY";
    public static final String LANGUAGE_CODES_KEY = "LANGUAGE_CODES_KEY";
    public static final String PAYER_PLAN_SEARCH_RESULTS_PANEL_KEY = "PAYER_PLAN_SEARCH_RESULTS_PANEL_KEY";
    public static final String ENROLLMENT_REASONS_FOR_INACTIVATION_KEY = "ENROLLMENT_REASONS_FOR_INACTIVATION_KEY";
    public static final String DASHBOARD_PANEL_KEY = "DASHBOARD_PANEL_KEY";
    public static final String ALERT_BUTTON_KEY = "ALERT_BUTTON_KEY";
    public static final String ALL_DISENROLLMENT_CODES = "ALL_DISENROLLMENT_CODES";
    public static final String DISENROLLMENT_CODES_FOR_REFUSAL = "DISENROLLMENT_CODES_FOR_REFUSAL";
    public static final String DISENROLLMENT_CODES_FOR_INACTIVATION = "DISENROLLMENT_CODES_FOR_INACTIVATION";
    public static final String PATIENT_RELATIONSHIP_CODES_KEY = "PATIENT_RELATIONSHIP_CODES_KEY";
    public static final String DELIMITER_SELECT_ITEM = "_";
    public static final String DUPLICATE_CARETEAM_ROLES = "DUPLICATE_CARETEAM_ROLES";
    public static final String REQUIRED_CARETEAM_ROLES = "REQUIRED_CARETEAM_ROLES";
    public static final String PAYER_CLASS = "PAYER_CLASS";
    public static final String PAYER_PLAN = "PAYER_PLAN";
    public static final String EULA = "EULA";
    public static final String SAMHSA = "SAMHSA";
    public static final String COUNTY = "COUNTY";
    public static final String ACCESS_LEVEL_MAP = "ACCESS_LEVEL_MAP";
    public static final String PROGRAM_HEALTH_HOME_KEY = "PROGRAM_HEALTH_HOME_KEY";
    public static final String PROGRAM_NAME_HEALTHHOME_KEY = "PROGRAM_NAME_HEALTHHOME_KEY";
    public static final String USER_ROLE_KEY = "USER_ROLE_KEY";
    public static final String ADD_PATIENT_TOCARE_TEAM_CONF="Add Patient to Care Team Confirmation";
    public static final String REMOVE_PATIENT_FROM_CARE_TEAM_CONF="Removing Patient from Care Team Confirmation";
    public static final String DIRECT_SECURED_MESSAGING="DIRECT Secured Messaging";
    
    public static final String PATINET_ACTIVE="Active";
    public static final String PATINET_INACTIVE="Inactive";
    public static final String PATINET_NONE="None";
    
    public static final String SERVICE_SUBSCRIPTION = "Service Subscription";
    public static final String USER_VALID_EMAIL_ADDRESS = "User ID must be a valid email address.";
    public static final String PROPOSED_WARNING_TEXT = "Proposed Redisclosure Warning Text";
//    public static final String IN_PROVIDER_NAME_CODES_KEY = "IN_PROVIDER_NAME_CODES_KEY"; 
//    public static final String OUT_PROVIDER_NAME_CODES_KEY="OUT_PROVIDER_NAME_CODES_KEY";
    public static final String PROGRAM_NAME_SELECT_KEY = "PROGRAM_NAME_SELECT_KEY";
    public static final String PATIENT_STATUS_CODES_KEY = "PATIENT_STATUS_CODES_KEY";
    public static final String PROGRAM_HEALTH_HOME_CODES_KEY = "PROGRAM_HEALTH_HOME_CODES_KEY";
    public static final String TERMINATION_REASON_CODES_KEY = "TERMINATION_REASON_CODES_KEY";
    public static final String TERMINATION_REASON_FOR_STATUS_KEY = "TERMINATION_REASON_FOR_STATUS_KEY";
    public static final String HH_REQUIRED_FOR_PROGRAM = "HH_REQUIRED_FOR_PROGRAM";
    public static final String HEALTH_HOME_FOR_PROGRAM_KEY="HEALTH_HOME_FOR_PROGRAM_KEY";
    public static final String PROGRAM_DELETE_REASON_CODES_KEY="PROGRAM_DELETE_REASON_CODES_KEY";
    public static final String PATIENT_STATUS_KEY="PATIENT_STATUS_KEY";
    public static final String MY_PATIENT_LIST_PANEL_KEY = "MY_PATIENT_LIST_PANEL_KEY";
    public static String REFERENCE_DATA_KEY = "REFERENCE_DATA_KEY";    
    public static String ACIVITY_NAMES_KEY = "ACIVITY_NAMES_KEY";
     public static final String ACTIVITY_TRACKER_PANEL_KEY = "ACTIVITY_TRACKER_PANEL_KEY";
    //JIRA 1023
    public static String PROGRAM_FOR_HEALTH_HOME_CODES_KEY = "PROGRAM_FOR_HEALTH_HOME_CODES_KEY";
    String DEFAULT_LAYOUT_WIDTH = "98%";
    String DEFAULT_LAYOUT_HEIGHT = "75%";

    public static String TAGTYPE_USER_ADDED = "USER";
    public static String USER_ADDED_LISTTAG = "USER_ADDED_LISTTAG";
    String CAREPLAN_HOMEPAGE = "homePage";
    
    //Spira 7215
    public static final String MDM = "mdm";


    int PROGRAM_CONSENT_YES = 1;
    int PROGRAM_CONSENT_NO = 0;
    int PROGRAM_CONSENT_NOT_REQUIRED = -1;
    String PROGRAM_CONSENT_YES_TEXT = "Yes";
    String PROGRAM_CONSENT_NO_TEXT = "No";
    String PROGRAM_CONSENT_NOT_REQUIRED_TEXT = "No Consent Required";

    String IN_PROVIDER="IN_PROVIDERS";
    String OUT_PROVIDER="OUT_PROVIDERS";

    String BLANK_CARETEAM_ID = "-1";
}
