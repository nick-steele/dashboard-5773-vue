package com.gsihealth.dashboard.client.eula;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.login.LoginPanel;
import com.gsihealth.dashboard.client.service.EulaService;
import com.gsihealth.dashboard.client.service.EulaServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;

/**
 *
 * @author Chad Darby
 */
public class EulaWindow extends Window {

    private Button agreeButton;
    private Button declineButton;
    private Button printButton;
    private CheckboxItem checkboxItem;
    private HTMLPane contentsHTMLPane;
    private final EulaServiceAsync eulaService = GWT.create(EulaService.class);
    private boolean showSamsha;
    private boolean changePassword;
    private String email;


    public EulaWindow(boolean showSamsha, String email,  boolean changePassword) {
        this.showSamsha = showSamsha;
        this.email = email;
        this.changePassword = changePassword;
        buildGui();
        loadEula();
    }
    
    private void buildGui() {
        setWindowProps();

        Layout contentLayout = buildContentLayout();
        addItem(contentLayout);

        DynamicForm form = new DynamicForm();
        checkboxItem = new CheckboxItem("agree", "I have read and agree to the terms and conditions.");
        checkboxItem.setRequired(true);
        checkboxItem.addChangedHandler(new AcceptChangedHandler());
        form.setPadding(10);

        form.setFields(checkboxItem);
        addItem(form);

        Canvas buttonBar = buildButtonBar();
        addItem(buttonBar);
    }

    private Layout buildContentLayout() throws IllegalStateException {
        HLayout layout = new HLayout(10);

        layout.setPadding(10);
        layout.setWidth100();
        layout.setAlign(Alignment.CENTER);

        contentsHTMLPane = new HTMLPane();
        contentsHTMLPane.setShowEdges(true);
        contentsHTMLPane.setEdgeSize(1);
        contentsHTMLPane.setWidth100();
        contentsHTMLPane.setHeight(400);
        contentsHTMLPane.setPadding(5);
        contentsHTMLPane.setAlign(Alignment.CENTER);

        layout.addMember(contentsHTMLPane);

        return layout;
    }

    private void loadEula() {
        long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
        eulaService.getActiveEulaContent(communityId, new GetActiveEulaContentCallback());
    }

    private void setEulaContent(String data) {
        contentsHTMLPane.setContents(data);
    }

    private void setWindowProps() {
        setTitle("End User License Agreement");
        setPadding(10);
        setShowCloseButton(false);
        setWidth(800);
        setHeight(550);
        setShowMinimizeButton(false);
        setIsModal(true);
        setShowModalMask(true);
        centerInPage();
    }

    private Layout buildButtonBar() {
        HLayout buttonLayout = new HLayout(10);

        buttonLayout.setPadding(10);
        buttonLayout.setWidth100();
        buttonLayout.setAlign(Alignment.CENTER);

        agreeButton = new Button("I Agree");
        agreeButton.setDisabled(true);
        buttonLayout.addMember(agreeButton);

        declineButton = new Button("I Decline");
        buttonLayout.addMember(declineButton);

        printButton = new Button("Print");
        buttonLayout.addMember(printButton);

        // register listeners
        agreeButton.addClickHandler(new AgreeClickHandler());
        declineButton.addClickHandler(new DeclineClickHandler());
        printButton.addClickHandler(new PrintClickHandler());

        return buttonLayout;
    }

    private void updateUserEula(String email, boolean flag) {
        long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
        eulaService.updateUserEulaStatus(email, communityId, flag, new UpdateUserEulaAsyncCallback(flag));
    }

    class AcceptChangedHandler implements ChangedHandler {

        public void onChanged(ChangedEvent event) {
            boolean checked = (Boolean) event.getValue();

            agreeButton.setDisabled(!checked);
        }
    }

    class AgreeClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {

            LoginResult loginResult = ApplicationContextUtils.getLoginResult();
            loginResult.setEula(true);

            String email = loginResult.getEmail();
            updateUserEula(email, true);

            EulaWindow.this.hide();

            if (changePassword) {
                new LoginPanel().showChangePasswordWindow(email, showSamsha);
            }
            else{
                if (showSamsha){
                    DashBoardApp.showSamhsaWindow();

                }
                else {
                    DashBoardApp.showMainApplication();
                }
            }
        }
    }

    class DeclineClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            SC.confirm("Decline", "You are declining the End User License Agreement. You will not be able to access the system.", new DeclineCallback());
        }
    }

    class DeclineCallback implements BooleanCallback {

        @Override
        public void execute(Boolean value) {

            if (value != null && value.booleanValue()) {
                LoginResult loginResult = ApplicationContextUtils.getLoginResult();
                String email = loginResult.getEmail();

                updateUserEula(email, false);
            }
        }
    }

    class PrintClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {

            Object[] components = new Object[1];
            components[0] = contentsHTMLPane;
            Canvas.printComponents(components);
        }
    }

    class GetActiveEulaContentCallback implements AsyncCallback<String> {

        @Override
        public void onSuccess(String data) {
            setEulaContent(data);
        }

        @Override
        public void onFailure(Throwable exc) {
            SC.warn("Error: " + exc.getMessage());
        }
    }

    class UpdateUserEulaAsyncCallback implements AsyncCallback {

        boolean accepted;

        UpdateUserEulaAsyncCallback(Boolean acceptedFlag) {
            accepted = acceptedFlag;
        }

        @Override
        public void onSuccess(Object data) {
            if (!accepted) {
                logoffUser();
            }
        }

        @Override
        public void onFailure(Throwable exc) {
            SC.warn("Error: " + exc.getMessage());
            logoffUser();
        }
    }

    private void logoffUser() {
        //Spira 6163
        DashBoardApp.doLogout();
    }
}
