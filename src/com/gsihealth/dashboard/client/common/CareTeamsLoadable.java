package com.gsihealth.dashboard.client.common;

/**
 * Interface for loading care team names
 * 
 * @author Chad Darby
 */
public interface CareTeamsLoadable {

    public abstract void loadCareTeamNames();    
}
