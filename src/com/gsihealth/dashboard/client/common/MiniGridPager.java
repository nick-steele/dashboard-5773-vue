package com.gsihealth.dashboard.client.common;

import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

/**
 * Component for displaying a pager of larger results. Displays the following options
 * - "Showing 1 to 10 of 99 records"
 * - "Page 1 of 3"
 * - Buttons for First, Previous, Next and Last
 * - "Go to page"
 * - "Records per page"
 * 
 * <pre>
 * Usage example:
 *    MiniGridPager pager = new MiniGridPager();
 *    
 *    // add it beneath your ListGrid
 *    addMember(pager);
 * 
 *    // to make it useful, you need to set the total count of items from
 *    // your database
 *    pager.setTotalCount(theCountFromDB);
 * 
 *    // when the user clicks previous, next button on pager, you need to provide
 *    // a handler to perform the appropriate action
 *    page.addPageEventHandler(new AlertPageEventHandler());  // defined below
 * 
 *    // you also need to provide the implementation of your handler
 *     class AlertPageEventHandler implements PageEventHandler {
 *
 *       public void handlePage(int pageNum, int pageSize) {
 *           alertService.getUserAlerts(userId, pageNum, pageSize, getPageAlertsCallback);
 *       }
 *   }
 * </pre>
 * 
 * That's it. Now when the user selects the prev/next buttons to page thru the results
 * then the grid pager will load it accordingly.
 * 
 * See the examples for Alerts and Admin App (Users, Organizations)
 * 
 * @author Chad Darby
 */
public class MiniGridPager extends ToolStrip {
    
    private static final int DEFAULT_PAGE_SIZE = 10;
    private static final String[] PAGE_SIZE_VALUES = {"10", "25", "50", "100", "200"};
    
    private int pageSize;
    private int currentPageNumber;
    private int totalCount;
    private Label totalLabel;
    private Label pagesInfoLabel;
    private ToolStripButton nextButton;
    private ToolStripButton previousButton;
    private PageEventHandler handler;
    private DynamicForm form;
    private SelectItem pageSizeSelectItem;

    public MiniGridPager() {
        this(1, DEFAULT_PAGE_SIZE);
    }
    
    public MiniGridPager(int theTotalCount) {
        this(theTotalCount, DEFAULT_PAGE_SIZE);
    }
    
    public MiniGridPager(int theTotalCount, int thePageSize) {
        pageSize = thePageSize;
        totalCount = theTotalCount;
        currentPageNumber = 1;
        
        buildGui();
        
        gotoPage(1);
    }
    
    /**
     * Build GUI
     * 
     * @throws IllegalStateException 
     */
    protected void buildGui() throws IllegalStateException {
        pagesInfoLabel = new Label();
        
        pagesInfoLabel.setWrap(false);
        pagesInfoLabel.setWidth(100);
        pagesInfoLabel.setAlign(Alignment.RIGHT);
                
        previousButton = new ToolStripButton();
        previousButton.setIcon("[SKIN]/actions/back.png");
        previousButton.addClickHandler(new ClickHandler() {
            
            public void onClick(ClickEvent event) {
                gotoPage(currentPageNumber - 1);
            }
        });
        
        nextButton = new ToolStripButton();
        nextButton.setIcon("[SKIN]/actions/forward.png");
        nextButton.addClickHandler(new ClickHandler() {
            
            public void onClick(ClickEvent event) {
                gotoPage(currentPageNumber + 1);
            }
        });
                
        totalLabel = new Label();
        totalLabel.setWrap(false);
        totalLabel.setWidth(100);
        
        setHeight(25);
        setOverflow(Overflow.VISIBLE);
        
        form = new DynamicForm();
        form.setNumCols(4);
        
        pageSizeSelectItem = new SelectItem("pagesize", "Page size");
        pageSizeSelectItem.setWrapTitle(false);
        pageSizeSelectItem.setWidth(60);
        pageSizeSelectItem.setValueMap(PAGE_SIZE_VALUES);
        pageSizeSelectItem.setValue(Integer.toString(pageSize));
        pageSizeSelectItem.addChangedHandler(new PageSizeChangedHandler());
        
        form.setFields(pageSizeSelectItem);
        
        addSpacer(2);
        addMember(totalLabel);
        
        // addFill();
        
        addMember(pagesInfoLabel);
        addSpacer(4);
        addMember(previousButton);
        addSpacer(2);
        
        addMember(nextButton);
        
        addSpacer(4);
        addMember(form);
        
        addSpacer(2);
    }
    
    /**
     * Goto the given page number, and load data for this page
     * 
     * @param thePageNum 
     */
    public void gotoPage(int thePageNum) {
        handleNavigationButtons(thePageNum);
        
        updatePage(currentPageNumber);
    }

    public void handleNavigationButtons(int thePageNum) {
        if (thePageNum < 1) {
            currentPageNumber = 1;
        } else if (thePageNum > getTotalPages()) {
            currentPageNumber = getTotalPages();
        } else {
            currentPageNumber = thePageNum;
        }
        
        if (currentPageNumber == 1) {
            previousButton.hide();
        } else {
            previousButton.show();
        }
        
        if (currentPageNumber == getTotalPages()) {
            nextButton.hide();
        } else {
            nextButton.show();
        }
    }
    
    /**
     * Refresh the page labels for "Showing xx" and "Page xx"
     * 
     * @param pageNum 
     */
    public void refreshPageLabels(int pageNum) {
        int startRecordNumber = Math.max(1, ((pageNum - 1) * pageSize) + 1);
        int endRecordNumber = Math.min(startRecordNumber + pageSize - 1, totalCount);
        
        if (totalCount > 0) {
            totalLabel.setContents("Showing "
                    + startRecordNumber
                    + " to "
                    + endRecordNumber
                    + " of "
                    + totalCount
                    + " records.");
            
        } else {
            totalLabel.setContents("No records found.");
        }
        
        pagesInfoLabel.setContents("Page " + pageNum + " of " + getTotalPages());
    }
    
    /**
     * Clear the page labels
     */
    public void clearPageLabels() {
        totalLabel.setContents("");
        pagesInfoLabel.setContents("");
    }
    
    /**
     * Compute the total number of pages based on total count and page size
     * @return 
     */
    private int getTotalPages() {
        int pages = (int) Math.ceil(((float) totalCount) / ((float) pageSize));

        // never return zero pages
        if (pages == 0) {
            pages = 1;
        }
        
        return pages;
    }

    /**
     * Update page data 
     * 
     * @param pageNum 
     */
    public void updatePage(int pageNum) {
        
        refreshPageLabels(pageNum);
        
        if (handler != null && totalCount > 0) {
            handler.handlePage(pageNum, pageSize);
        }
    }
    
    public int getCurrentPageNumber() {
        return currentPageNumber;
    }
    
    public void setCurrentPageNumber(int num) {
        currentPageNumber = num;
    }
    
    public int getTotalCount() {
        return totalCount;
    }
    
    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
    
    public void setPageEventHandler(PageEventHandler theHandler) {
        handler = theHandler;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void showLoading() {
        clearPageLabels();
        totalLabel.setContents("Loading...");
    }
    
    /**
     * Handles the "Page size" request
     */
    class PageSizeChangedHandler implements ChangedHandler {

        @Override
        public void onChanged(ChangedEvent event) {
            String value = event.getValue().toString();    
            pageSize = Integer.parseInt(value);
            
            
            // now goto page 1...always.
            gotoPage(1);
        }
    }   
}
