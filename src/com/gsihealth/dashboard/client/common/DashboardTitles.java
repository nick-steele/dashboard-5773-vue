package com.gsihealth.dashboard.client.common;

/**
 *
 * @author Chad Darby
 */
public interface DashboardTitles {

    public static final String HOME = "Home";
    public static final String ALERTS = "Alerts";
    public static final String CARE_TEAM_ASSIGNMENT = "Care Teams";
    public static final String ENROLLMENT = "Enrollment";
    public static final String REFERRALS = "Referrals";
    public static final String MESSAGES = "Messages";
    public static final String TREAT = "Care Plan";
    public static final String SEARCH = "Search";
    public static final String ADMIN = "Admin";
    public static final String REPORTS = "Reports";
    public static final String SYSTEM_ADMINISTRATOR_REPORTS = "Administrative Reports";
    public static final String PATIENT_LEVEL_REPORT = "Patient Level Reports";
    public static final String PATIENTS_NOT_CONSENTED_REPORT = "Patients Not Consented";
    public static final String CONSENTED_PATIENTS_REPORT = "Consented Patients";
    public static final String PATIENT_COUNT_REPORT = "Total Patient Count";
    public static final String LIST_USERS_REPORT = "User List";
    public static final String TASK_LIST = "Task Manager";
    
    public static final String POPULATION_MANAGER = "Population Manager";
    public static final String REFERRAL_MANAGER = "Referral Manager";
    public static final String PATIENT_ENGAGEMENT = "Patient Engagement";
    public static final String DOH_PATIENT_TRACKING_SHEET ="Patient Tracking Sheet";
    public static final String PATIENT_PROGRAM_REPORT="Patient Program Report";
    public static final String DEMOGRAPHICS = "Demographics";
}
