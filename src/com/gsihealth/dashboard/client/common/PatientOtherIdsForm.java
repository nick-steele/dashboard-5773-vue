package com.gsihealth.dashboard.client.common;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.service.PatientOtherIdsService;
import com.gsihealth.dashboard.client.service.PatientOtherIdsServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.common.dto.PatientOtherIdsDTO;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.*;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import java.util.LinkedHashMap;

/**
 *
 * @author Beth Boose
 */
public class PatientOtherIdsForm extends VLayout implements Cancelable {

    private PatientOtherIdsServiceAsync patientOtherIdsService = GWT.create(PatientOtherIdsService.class);
    
    private DynamicForm dynamicForm;
    private PatientOtherIdsDTO patientOtherIdsDTO;
    PatientOtherIdsPanel patientOtherIdsPanel;
    long patientId;

    private TextItem patientOtherIdItem;
    private TextItem orgOid;
    private TextItem orgName;
    private ComboBoxItem orgOidSelectItem;
    private ButtonItem addNewOrgButton;

    private String addOrUpdate = "Add";
    private boolean updating = false;

    private Button addButton;
    private Button saveButton;
    private Button cancelButton;
    
    PatientOtherIdsForm(PatientOtherIdsPanel patientOtherIdsPanel, PatientOtherIdsDTO patientOtherIdsDTO, long patientId) {
        this.patientOtherIdsDTO = patientOtherIdsDTO;
        this.patientOtherIdsPanel = patientOtherIdsPanel;
        this.updating = (patientOtherIdsDTO == null) ? false : true;
        this.patientId = patientId;
        buildGui();
        this.loadOrgOids();
    }

    private void buildGui() {
        this.setPadding(5);
        this.setMembersMargin(5);
        this.setAutoHeight();
        this.setAutoWidth();

        // patient id     
        patientOtherIdItem = new TextItem("patientOtherIdItem", "Other ID");
        patientOtherIdItem.setRequired(true);
        patientOtherIdItem.setRequiredMessage("Other ID is required");
        patientOtherIdItem.addChangedHandler(new SomethingChangedHandler());
        patientOtherIdItem.setStartRow(true);
        patientOtherIdItem.setEndRow(false);

        // add new org button
        addNewOrgButton = new ButtonItem("addNewOrgButton", "Org not listed");
        addNewOrgButton.addClickHandler(new EnableTextBoxesHandler());
        addNewOrgButton.setStartRow(false);
        addNewOrgButton.setEndRow(true);
        
         // dropdown
        orgOidSelectItem = new ComboBoxItem("orgOidSelectItem", "Auto populate Org data");
        orgOidSelectItem.setAddUnknownValues(false);
        orgOidSelectItem.setRequired(false);
        orgOidSelectItem.addChangedHandler(new OrgDropdownChangedHandler());
        orgOidSelectItem.setEndRow(false);
        
        // org name
        orgName = new TextItem("orgName", "Org Name");
        orgName.setRequired(true);
        orgName.setRequiredMessage("Org name is required");
        orgName.addChangedHandler(new SomethingChangedHandler());
        orgName.setStartRow(false);
        orgName.setEndRow(false);
        
        SpacerItem spacerItem1 = new SpacerItem();
        spacerItem1.setName("spacer1");
        spacerItem1.setEndRow(false);

        // oid
        orgOid = new TextItem("orgOid", "Org ID");
        orgOid.setRequired(true);
        orgOid.setRequiredMessage("Org ID is required");
        orgOid.addChangedHandler(new SomethingChangedHandler());
        orgOid.setStartRow(false);
        orgOid.setEndRow(true);
        
        toggleTextBoxesReadOnly(true);


        if (this.updating) {
            this.populateForm(patientOtherIdsDTO);
            this.addOrUpdate = "Edit";
        }

        this.dynamicForm = new DynamicForm();
        this.dynamicForm.setGroupTitle(addOrUpdate + " Patient Other ID");
        this.dynamicForm.setNumCols(6);
        this.dynamicForm.setFields(patientOtherIdItem, spacerItem1, addNewOrgButton, orgOidSelectItem, orgName, orgOid);
        this.addMember(dynamicForm);

        Layout buttonBar = this.buildButtonBar();
        addMember(buttonBar);
    }

    private Layout buildButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        buttonLayout.setPadding(5);
        buttonLayout.setAlign(Alignment.LEFT);
        
        if (updating) {
            this.saveButton = new Button("Save");
            buttonLayout.addMember(this.saveButton);
            this.saveButton.addClickHandler(new UpdateButtonHandler());
            this.saveButton.disable();
        } else {
            this.addButton = new Button("Add");
            buttonLayout.addMember(this.addButton);
            this.addButton.addClickHandler(new AddButtonHandler());
            this.addButton.disable();
        }
        
        this.cancelButton = new Button("Cancel");
        this.cancelButton.addClickHandler(new CancelButtonHandler());
        buttonLayout.addMember(this.cancelButton);

        return buttonLayout;
    }
    
    /**
     * enable textboxes for manual entry, disable when u use the dropdown
     * @param enable 
     */
    private void toggleTextBoxesReadOnly(boolean enable) {
        orgOid.setDisabled(enable);
        orgName.setDisabled(enable);
    }

    public PatientOtherIdsDTO getPatientOtherIdsDTO() {
        if (!updating) {
            patientOtherIdsDTO = new PatientOtherIdsDTO();
        }
        OrganizationDTO organizationDTO = new OrganizationDTO();
        organizationDTO.setOID(this.orgOid.getValueAsString());
        patientOtherIdsDTO.setPatientOtherOrgOid(this.orgOid.getValueAsString());
        patientOtherIdsDTO.setPatientId(this.patientId);
        patientOtherIdsDTO.setPatientOtherId(patientOtherIdItem.getValueAsString());
        patientOtherIdsDTO.setPatientOtherOrgName(this.orgName.getValueAsString());
        patientOtherIdsDTO.setCommunityId(this.patientOtherIdsPanel.getCommunityId());
        return patientOtherIdsDTO;
    }

    public void populateForm(PatientOtherIdsDTO patientOtherIdsDTO) {
        orgName.setValue(patientOtherIdsDTO.getPatientOtherOrgName());
        orgOid.setValue(patientOtherIdsDTO.getPatientOtherOrgOid());
        patientOtherIdItem.setValue(patientOtherIdsDTO.getPatientOtherId());
    }

    public void clearErrors(boolean flag) {
        this.dynamicForm.clearErrors(flag);
    }

    public void clearValues() {
        this.dynamicForm.clearValues();
    }

    private void enableAddOrUpdateButton() {
        if (updating) {
            saveButton.enable();
        } else {
            addButton.enable();
        }
    }

    private void loadOrgOids() {
        LoadOrganizationCallback loadOrganizationCallback = new LoadOrganizationCallback();
        loadOrganizationCallback.attempt();
    }
    
    class LoadOrganizationCallback extends RetryActionCallback<LinkedHashMap<String, String>> {

        @Override
        public void attempt() {
           long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
            patientOtherIdsService.getOrganizations(communityId,this);
        }
        @Override
        public void onCapture(LinkedHashMap<String, String> data) {
            orgOidSelectItem.setValueMap(data);
        }
    }

    /**
     * call this for adding new ids
     */
    class AddButtonHandler implements ClickHandler {

        PatientOtherIdsDTO patientOtherIdsDTO = new PatientOtherIdsDTO();

        @Override
        public void onClick(ClickEvent event) {
            patientOtherIdsDTO = getPatientOtherIdsDTO();
            if (dynamicForm.validate()) {
                ProgressBarWindow progressBarWindow = new ProgressBarWindow();
                progressBarWindow.show();
                String confirmationMessage = "new patient other ID successfully added";
                patientOtherIdsPanel.getPatientOtherIdsService().addPatientOtherId(patientOtherIdsDTO, new AddUpdateCallback(progressBarWindow, confirmationMessage));
                clearValues();
                patientOtherIdsPanel.showIdList(patientId);
            }
        }
    }

    /**
     * call this for updating new ids
     */
    class UpdateButtonHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            patientOtherIdsDTO = getPatientOtherIdsDTO();
            if (dynamicForm.validate()) {
                ProgressBarWindow progressBarWindow = new ProgressBarWindow();
                progressBarWindow.show();
                String confirmationMessage = "patient other ID successfully updated";
                patientOtherIdsPanel.getPatientOtherIdsService()
                        .updatePatientOtherId(patientOtherIdsDTO, true, new AddUpdateCallback(progressBarWindow, confirmationMessage));

            }
        }
    }

    class EnableTextBoxesHandler implements com.smartgwt.client.widgets.form.fields.events.ClickHandler {

        @Override
        public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
            toggleTextBoxesReadOnly(false);
        }
    }

    class CancelButtonHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            patientOtherIdsPanel.showIdList(patientId);
        }
    }

    class AddUpdateCallback implements AsyncCallback {

        ProgressBarWindow progressBarWindow;
        String message;

        AddUpdateCallback(ProgressBarWindow theProgressBarWindow, String theMessage) {
            progressBarWindow = theProgressBarWindow;
            message = theMessage;
        }

        public void onSuccess(Object t) {
            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }
            clearValues();
            patientOtherIdsPanel.showIdList(patientId);
            SC.say(message);
        }

        public void onFailure(Throwable thrwbl) {
            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            SC.warn(thrwbl.getMessage());
        }
    }

    /**
     * lazy? maybe, the fields don't need their own handlers right now, so just
     * us this to enable the add/update button
     */
    class SomethingChangedHandler implements ChangedHandler {

        public void onChanged(ChangedEvent event) {
            enableAddOrUpdateButton();
        }
    }

    /**
     * when a user selects a name from the org dropdown, populate the org oid
     * box and org name boxes TODO, if a user enters a new name, they'll have to
     * manually populate the name box
     */
    class OrgDropdownChangedHandler implements ChangedHandler {

        public void onChanged(ChangedEvent event) {
            enableAddOrUpdateButton();
            toggleTextBoxesReadOnly(true);
            orgOid.setValue(event.getValue());
            orgName.setValue(((ComboBoxItem) event.getItem()).getDisplayValue());
        }

    }

}
