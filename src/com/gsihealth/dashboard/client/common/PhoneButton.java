package com.gsihealth.dashboard.client.common;

import com.google.gwt.event.shared.HandlerRegistration;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VStack;

public class PhoneButton extends VStack {

    private Button button;
    private Label label;

    public static final int DEFAULT_BUTTON_SIZE = 80;


    public PhoneButton(String text) {
        this(DEFAULT_BUTTON_SIZE, text);
    }

    /**
     * @param size defines the size of image and width of the panel
     * @param text Text which comes below the button
     */
    public PhoneButton(int size, String text) {
        this(size, text, "phone-button");
    }

    public PhoneButton(int size, String text, String cssStyleName) {

        super();

        // force super panel to fit children
        setWidth(size);

        // for providing spacing around the widget
        setMargin(12); 

        button = new Button("");
        button.setHeight(size);
        button.setWidth(size);

        // for removing focus style
        button.setShowFocused(false); 

        label = new Label(text);
        label.setWidth(size);
        label.setAutoHeight();

        // to center align the text
        label.setAlign(Alignment.CENTER); 

        setStyleName(cssStyleName);

        addMember(button);
        addMember(label);
    }

    /**
     * Sets a new style for the button
     * three styles have to be implemented in css, "style", "styleDown", "styleOver"
     * to be able to see the images, other wise blank image will come.
     * Default image can not be supported since multiple styles are not supported by
     * {@link SmartGWT}
     * @param style
     */
    public void setIconStyle(String style) {
        button.setBaseStyle(style);
    }

    /**
     * @return the button
     */
    public Button getButton() {
        return button;
    }

    /**
     * @return the label
     */
    public Label getLabel() {
        return label;
    }

    @Override
    public HandlerRegistration addClickHandler(ClickHandler handler) {
        return button.addClickHandler(handler);
    }
    
}
