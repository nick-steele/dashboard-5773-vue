package com.gsihealth.dashboard.client.common;

/**
 *
 * @author Chad Darby
 */
public interface PageEventHandler {

    public void handlePage(int pageNum, int pageSize);        
}
