package com.gsihealth.dashboard.client.common;

import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.VLayout;

public class ButtonPanel extends VLayout {
    public static final int WIDTH = 90;

    private Img img;
    private Label label;

    public ButtonPanel(Img theImg, String labelText) {
        img = theImg;

        setPadding(10);
        setWidth(WIDTH);
        
        label = new Label("<b>" + labelText + "</b>");
        label.setHeight(10);
        label.setWidth(WIDTH);
        addMember(img);
        addMember(label);
    }

}
