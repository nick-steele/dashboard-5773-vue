package com.gsihealth.dashboard.client.common;

/**
 * Interface for components that can be canceled.
 * 
 * @author Satyendra
 */
public interface Cancelable {

        public void clearErrors(boolean flag);
        public void clearValues();

}
