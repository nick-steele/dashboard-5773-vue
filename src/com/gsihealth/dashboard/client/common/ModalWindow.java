package com.gsihealth.dashboard.client.common;

import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 * @author Chad Darby
 */
public class ModalWindow extends Window {

    public ModalWindow() {
        buildGui();
    }

    private void buildGui() {

        VLayout layout = new VLayout();
        layout.setMembersMargin(10);
        setWidth(450);
        setHeight(200);
        setTitle("Warning");
        setShowMinimizeButton(false);
        setScrollbarSize(0);
        setShowCloseButton(true);
        setShowMaximizeButton(false);

        setIsModal(true);
        setShowModalMask(true);
        centerInPage();
        Label label = new Label("Search did not return any results. Please modify  your search criteria.");
        addItem(label);
        Button okButton = new Button("OK");
        okButton.setWidth(75);
        okButton.setHeight(25);
        okButton.addClickHandler(new OKButtonClickHandler());

        HLayout buttonBar = new HLayout(10);
        buttonBar.setWidth100();
        buttonBar.setAlign(Alignment.CENTER);
        buttonBar.addMember(okButton);

        layout.addMember(buttonBar);

        addItem(layout);

        this.addCloseClickHandler(new WinCloseClickHandler());
    }

    class WinCloseClickHandler implements CloseClickHandler {

        @Override
        public void onCloseClick(CloseClickEvent event) {
            hide();
        }
    }

    class OKButtonClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            hide();
        }
    }
}
