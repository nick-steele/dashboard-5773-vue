package com.gsihealth.dashboard.client.common;

import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Chad Darby
 */
public class PatientInfo implements Serializable {

    private long communityCareteamId;
    private long patientId;
    private String careteamName;
    
    private String consentTime;
    private String consentAMPM;
    
    private PatientEnrollmentDTO patientEnrollment;
    
    private boolean careteamChanged;
    private boolean enrollmentStatusChanged;
    private String oldEnrollmentStatus;

    private String oldProgramLevelConsentStatus;
    private long oldOrganizationId;
    //pe
    private String patientUserActive;
    private String originalPatientUserActive;
    private UserDTO  userDTO;
    private boolean savePatientUserFlag;
    private boolean enableCareteamPanel;
    private String minorConsentTime;
    private String minorConsentAMPM;
    private boolean careTeamEnd;
    
    public PatientInfo() {

    }

    public long getCommunityCareteamId() {
        return communityCareteamId;
    }

    public void setCommunityCareteamId(long communityCareteamId) {
        this.communityCareteamId = communityCareteamId;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public String getCareteamName() {
        return careteamName;
    }

    public void setCareteamName(String careteamName) {
        this.careteamName = careteamName;
    }

    public PatientEnrollmentDTO getPatientEnrollment() {
        return patientEnrollment;
    }

    public void setPatientEnrollment(PatientEnrollmentDTO thePatientEnrollment) {
        patientEnrollment = thePatientEnrollment;
    }

    public String getConsentAMPM() {
        return consentAMPM;
    }

    public String getConsentTime() {
        return consentTime;
    }

    public void setConsentTime(String data) {
        consentTime = data;
    }

    public void setConsentAMPM(String data) {
        consentAMPM = data;
    }

    public boolean isCareteamChanged() {
        return careteamChanged;
    }

    public void setCareteamChanged(boolean careteamChanged) {
        this.careteamChanged = careteamChanged;
    }

    public boolean isEnrollmentStatusChanged() {
        return enrollmentStatusChanged;
    }

    public void setEnrollmentStatusChanged(boolean enrollmentStatusChanged) {
        this.enrollmentStatusChanged = enrollmentStatusChanged;
    }

    public String getOldEnrollmentStatus() {
        return oldEnrollmentStatus;
    }    
    
    public void setOldEnrollmentStatus(String theOldEnrollmentStatus) {
        oldEnrollmentStatus = theOldEnrollmentStatus;
    }

    public String getOldProgramLevelConsentStatus() {
        return oldProgramLevelConsentStatus;
    }

    public void setOldProgramLevelConsentStatus(String oldConsent) {
        this.oldProgramLevelConsentStatus = oldConsent;
    }

    public long getOldOrganizationId() {
        return oldOrganizationId;
    }

    public void setOldOrganizationId(long oldOrganizationId) {
        this.oldOrganizationId = oldOrganizationId;
    }
    
     public String getPatientUserActive() {
        return patientUserActive;
    }

    public void setPatientUserActive(String patientUserActive) {
        this.patientUserActive = patientUserActive;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public String getOriginalPatientUserActive() {
        return originalPatientUserActive;
    }

    public void setOriginalPatientUserActive(String originalPatientUserActive) {
        this.originalPatientUserActive = originalPatientUserActive;
    }

    public boolean isSavePatientUserFlag() {
        return savePatientUserFlag;
    }

    public void setSavePatientUserFlag(boolean savePatientUserFlag) {
        this.savePatientUserFlag = savePatientUserFlag;
    }

    public boolean isEnableCareteamPanel() {
        return enableCareteamPanel;
    }

    public void setEnableCareteamPanel(boolean enableCareteamPanel) {
        this.enableCareteamPanel = enableCareteamPanel;
    }

    /**
     * @return the minorConsentTime
     */
    public String getMinorConsentTime() {
        return minorConsentTime;
    }

    /**
     * @param minorConsentTime the minorConsentTime to set
     */
    public void setMinorConsentTime(String minorConsentTime) {
        this.minorConsentTime = minorConsentTime;
    }

    /**
     * @return the minorConsentAMPM
     */
    public String getMinorConsentAMPM() {
        return minorConsentAMPM;
    }

    /**
     * @param minorConsentAMPM the minorConsentAMPM to set
     */
    public void setMinorConsentAMPM(String minorConsentAMPM) {
        this.minorConsentAMPM = minorConsentAMPM;
    }

    /**
     * @return the careTeamEnd
     */
    public boolean isCareTeamEnd() {
        return careTeamEnd;
    }

    /**
     * @param careTeamEnd the careTeamEnd to set
     */
    public void setCareTeamEnd(boolean careTeamEnd) {
        this.careTeamEnd = careTeamEnd;
    }
    
    
}
