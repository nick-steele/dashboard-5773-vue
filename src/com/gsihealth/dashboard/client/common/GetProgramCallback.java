package com.gsihealth.dashboard.client.common;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.managepatientprogram.ManagePatientProgramUtils;
import com.gsihealth.dashboard.entity.dto.ProgramDTO;
import com.smartgwt.client.widgets.form.fields.SelectItem;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by thnguyen on 9/25/2017.
 */
public class GetProgramCallback implements AsyncCallback<List<ProgramDTO>> {

    private SelectItem selectItem;
    protected LinkedHashMap<Long, String> valueMap;

    public GetProgramCallback(SelectItem selectItem){
        this. selectItem =selectItem;
    }
    @Override
    public void onFailure(Throwable throwable) {
        //TODO
    }
    @Override
    public void onSuccess(List<ProgramDTO> programDTOS) {
        LinkedHashMap<Long, String> valueMap = ManagePatientProgramUtils.convertToMap(programDTOS);
        this.valueMap = valueMap;
        selectItem.setValueMap(valueMap);
    }
}
