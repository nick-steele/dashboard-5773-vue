package com.gsihealth.dashboard.client.common;

import java.util.List;

/**
 * Created by thnguyen on 7/23/2017.
 */
public class PersonInfo implements java.io.Serializable{
    private String address1;
    private String city;
    private String communityId;
    private String communityName;
    private boolean consented;
    private String dob;
    private String firstName;
    private String middleName;
    private String lastName;
    private String gender;
    private boolean minor;
    private String organizationName;
    private List<Object> otherCommunities;
    private long patientId;
    private String state;
    private String status;
    private String county;
    private String zipcode;
    private long organizationId;
    private String medicaidId;
    private String medicareId;
    private String ssn; 
    

    public PersonInfo() {};

    public PersonInfo(String address1, String city, String communityId, String communityName, boolean consented, String dob, String firstName, String middleName, String lastName, String gender, boolean minor, String organizationName, List<Object> otherCommunities, long patientId, String state, String county,String status, String zipcode, long organizationId, String medicaidId, String medicareId, String ssn) {
        this.address1 = address1;
        this.city = city;
        this.communityId = communityId;
        this.communityName = communityName;
        this.consented = consented;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.gender = gender;
        this.minor = minor;
        this.organizationName = organizationName;
        this.otherCommunities = otherCommunities;
        this.patientId = patientId;
        this.state = state;
        this.county = county;
        this.status = status;
        this.zipcode = zipcode;
        this.organizationId = organizationId;
        this.medicaidId = medicaidId;
        this.medicareId = medicareId;
        this.ssn = ssn;

        if (dob.indexOf("T") > 0) {
            dob = dob.substring(0, dob.indexOf("T"));
        }
        this.dob = dob;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCommunityId() {
        return communityId;
    }

    public void setCommunityId(String communityId) {
        this.communityId = communityId;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public boolean isConsented() {
        return consented;
    }

    public void setConsented(boolean consented) {
        this.consented = consented;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        if (dob.indexOf("T") > 0) {
            dob = dob.substring(0, dob.indexOf("T"));
        }
        this.dob = dob;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean isMinor() {
        return minor;
    }

    public void setMinor(boolean minor) {
        this.minor = minor;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public List<Object> getOtherCommunities() {
        return otherCommunities;
    }

    public void setOtherCommunities(List<Object> otherCommunities) {
        this.otherCommunities = otherCommunities;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(long organizationId) {
        this.organizationId = organizationId;
    }

	public String getMedicaidId() {
		return medicaidId;
	}

	public void setMedicaidId(String medicaidId) {
		this.medicaidId = medicaidId;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getMedicareId() {
		return medicareId;
	}

	public void setMedicareId(String medicareId) {
		this.medicareId = medicareId;
	}
}
