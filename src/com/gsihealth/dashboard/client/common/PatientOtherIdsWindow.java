/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gsihealth.dashboard.client.common;

import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.smartgwt.client.widgets.Window;
import java.util.logging.Logger;

/**
 *
 * @author beth.boose
 */
public class PatientOtherIdsWindow extends Window {
    private Logger logger = Logger.getLogger(getClass().getName());
    private boolean showUtilityButtons;
    private long patientId;
    private long communityId;

    public PatientOtherIdsWindow(long localId, long communityId) {
        this.patientId = localId;
        this.communityId = communityId;
        this.buildGui();
    }
    
     private void buildGui() {
        setWindowProps(); 
        showUtilityButtons = this.showPatientOtherIdsCrud();
        PatientOtherIdsPanel patientOtherIdsPanel = new PatientOtherIdsPanel(this.patientId, this.communityId, showUtilityButtons);
        this.addItem(patientOtherIdsPanel);
    }
    
    private void setWindowProps() {
        setPadding(10);
        setWidth(600);
        setHeight(200);
        
        setTitle("External IDs for Patient: " + patientId);
        setShowMinimizeButton(false);
        setIsModal(true);
        setShowModalMask(true);
        
        centerInPage();
    }
    
        private boolean showPatientOtherIdsCrud() {

        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();

        return props.getShowPatientOtherIdsCrud();
    }
    
    
    
}
