package com.gsihealth.dashboard.client.common;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;

/**
 * Base class for callbacks
 * 
 * @author User
 */
public class PortalAsyncCallback <T> implements AsyncCallback<T> {

    protected ProgressBarWindow progressBarWindow;
    
    public PortalAsyncCallback() {

    }

    public PortalAsyncCallback(ProgressBarWindow theProgressBarWindow) {
        progressBarWindow = theProgressBarWindow;
    }

    @Override
    public void onFailure(Throwable exc) {

        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

        SC.warn("An error occurred. " + exc.getMessage());
    }

    // Provide a default implementation. Code that wants to handle exceptions can do so by overriding this method
    // Lazy developers can just ignore it, and the app-wide error handling framework will do the needful
    public void handleFailure(Throwable t) throws Throwable {
        throw t;
    }

    @Override
    public void onSuccess(T t) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
