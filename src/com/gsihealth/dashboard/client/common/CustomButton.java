package com.gsihealth.dashboard.client.common;

import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.DomEvent.Type;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;

public class CustomButton extends FlowPanel {

    private SimplePanel button;
    private Label alertLabel;
    private boolean alertVisible;
    
    public CustomButton() {
        super();
        addStyleName("customButton");

        button = new SimplePanel();

        button.addStyleName("iphone-ui-common");

        button.addStyleName("iphone-ui");

        alertLabel = new Label();
        alertLabel.addStyleName("alertLabel");
        alertVisible = false;

        registerListeners();

        add(button);

    }

    protected void registerListeners() {
        button.addDomHandler(new MouseOverHandler() {

            @Override
            public void onMouseOver(MouseOverEvent event) {
                button.addStyleDependentName("over");
            }
        }, MouseOverEvent.getType());

        button.addDomHandler(new MouseOutHandler() {

            @Override
            public void onMouseOut(MouseOutEvent event) {
                button.removeStyleDependentName("over");
            }
        }, MouseOutEvent.getType());

        button.addDomHandler(new MouseDownHandler() {

            @Override
            public void onMouseDown(MouseDownEvent event) {
                button.addStyleDependentName("down");
            }
        }, MouseDownEvent.getType());

        button.addDomHandler(new MouseUpHandler() {

            @Override
            public void onMouseUp(MouseUpEvent event) {
                button.removeStyleDependentName("down");
            }
        }, MouseUpEvent.getType());
    }

    public void setIconStyle(String style) {
        button.setStyleName(style);
        button.addStyleName("iphone-ui-common");

    }

    public void setAlertVisible(boolean theAlertFlag) {

        if (theAlertFlag) {
            remove(alertLabel);
            add(alertLabel);
        }
        else {
            remove(alertLabel);
        }

        alertVisible = theAlertFlag;

    }

    public void setAlertText(int i) {
        alertLabel.setText(Integer.toString(i));
    }

    public void addClickHandler(ClickHandler clickHandler,
            Type<ClickHandler> type) {
        button.addDomHandler(clickHandler, type);
    }
}
