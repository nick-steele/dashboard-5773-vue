package com.gsihealth.dashboard.client.common;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.common.ClientApplicationProperties;

/**
 *
 * @author Chad Darby
 */
public abstract class RetryActionCallback<T> implements AsyncCallback<T> {
    
    protected int maxRetries = 5;
    protected int interval = 1000;
    private int count = 0;
    
    private Logger logger = Logger.getLogger(getClass().getName());

    public RetryActionCallback() {
        
        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
        
        if (props != null) {
            maxRetries = props.getRpcRetryMaxAttempts();
            interval = props.getRpcRetryIntervalInMillis();
        }
    }
    
    /**
     * The implementation code for calling your remote service
     */
    public abstract void attempt();
    
    /**
     * This is code you would normally place in onSuccess(). This is where you get
     * the results of the remote call.
     * 
     * @param value 
     */
    public abstract void onCapture(T value);
    
    /**
     * If all is well, call the oncapture method. If an exception call onFailure
     * @param value 
     */
    @Override
    public void onSuccess(T value) {
        try {
            onCapture(value);
        } catch (Exception error) {
            onFailure(error);
        }
    }
    
    /**
     * This method is called when the remote call fails. Attempt to perform the operation
     * again for MAX_RETRIES. At the end, alert the user of failure
     * 
     * @param error 
     */
    @Override
    public void onFailure(Throwable error) {
        
        count++;
        
        // attempt to run again after a brief delay
        if (count < maxRetries) {
            Timer timer = new Timer() {
                public void run() {
                    attempt();                    
                }
            };
            
            timer.schedule(interval * count);
        } else {
            // log the error for posterity
            System.out.println("RETRY FAILURE: tried " + count + " times on a callback, we give up because: " + error);
            logger.log(Level.SEVERE, "RETRY FAILURE: tried " + count + " times on a callback, we give up because: ", error);
            Window.alert("A network error occurred. Please try again.");
        }
    }
}