/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gsihealth.dashboard.client.common;

import com.gsihealth.dashboard.common.dto.PatientOtherIdsDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 *
 * @author beth.boose
 */
public class PatientOtherIdsRecord extends ListGridRecord {
    private PatientOtherIdsDTO patientOtherIdsDTO;

    public PatientOtherIdsRecord(PatientOtherIdsDTO patientOtherIdsDTO) {
        this.patientOtherIdsDTO = patientOtherIdsDTO;
        setAttribute(PatientOtherIdsListPanel.ORG_OID, patientOtherIdsDTO.getPatientOtherOrgOid());
        setAttribute(PatientOtherIdsListPanel.ORG_NAME, patientOtherIdsDTO.getPatientOtherOrgName());
        setAttribute(PatientOtherIdsListPanel.PATIENT_OTHER_ID, patientOtherIdsDTO.getPatientOtherId());
        setAttribute(PatientOtherIdsListPanel.PATIENT_OTHER_ID_ID, patientOtherIdsDTO.getPatientOtherIdsId());
        setAttribute(PatientOtherIdsListPanel.PATIENT_CREATED_DATE, patientOtherIdsDTO.getCreationDateTime());
    }

    public PatientOtherIdsDTO getPatientOtherIdsDTO() {
        return patientOtherIdsDTO;
    }
    
    
    
    
    
    
    
}
