/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.common;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.service.PatientOtherIdsService;
import com.gsihealth.dashboard.client.service.PatientOtherIdsServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.dto.PatientOtherIdsDTO;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;
import java.util.logging.Logger;

/**
 * add crud buttons for patientOtherIds popup
 *
 * @author beth.boose
 */
public class PatientOtherIdsPanel extends VLayout {

    private Logger logger = Logger.getLogger(getClass().getName());

    long patientId;
    private ToolStripButton listIds;
    private ToolStripButton addIds;
    private ToolStripButton editId;
    private ToolStripButton deleteId;
    private boolean showUtilityButtons;
    private long communityId;

    private PatientOtherIdsListPanel patientOtherIdsListPanel;
    private PatientOtherIdsServiceAsync patientOtherIdsService = GWT.create(PatientOtherIdsService.class);

    PatientOtherIdsPanel(long patientId, long communityId, boolean showUtilityButtons) {
        this.patientId = patientId;
        this.communityId = communityId;
        this.showUtilityButtons = showUtilityButtons;  
        if (showUtilityButtons) {
            this.addMember(this.createToolStrip());
        }
        this.showIdList(patientId);
    }

    protected ToolStrip createToolStrip() {
        listIds = new ToolStripButton("List IDs");
        addIds = new ToolStripButton("Add ID");
        editId = new ToolStripButton("Update ID");
        deleteId = new ToolStripButton("Delete ID");

        ToolStrip toolStrip = new ToolStrip();
        toolStrip.setWidth100();
        toolStrip.addButton(listIds);
        toolStrip.addSeparator();
        toolStrip.addButton(addIds);
        toolStrip.addSeparator();
        toolStrip.addButton(editId);
        toolStrip.addSeparator();
        toolStrip.addButton(deleteId);
        toolStrip.addSeparator();

        listIds.addClickHandler(new ListIdsClickHandler());
        addIds.addClickHandler(new AddIdsClickHandler());
        editId.addClickHandler(new EditIdClickHandler());
        deleteId.addClickHandler(new DeleteIdClickHandler());

        return toolStrip;
    }

    public void showIdList(long patientId) {
        
        if (this.showUtilityButtons) {
            editId.disable();
            deleteId.disable();
        }
        this.patientOtherIdsListPanel = new PatientOtherIdsListPanel(this, patientId, this.showUtilityButtons);
        this.removeMainContent();
        this.addMember(patientOtherIdsListPanel);
    }

    public PatientOtherIdsServiceAsync getPatientOtherIdsService() {
        return patientOtherIdsService;
    }

    /**
     * TODO - what does this do?
     */
    private void removeMainContent() {

        Canvas[] members = getMembers();

        if (members.length > 1) {
            Canvas mainContent = members[1];
            this.removeMember(mainContent);
        }
    }

    public void flipUpdateIdButton(boolean flag) {
        if (flag) {
            this.editId.enable();
        } else {
            this.editId.hide();
        }
    }

    public void flipDeleteIdButton(boolean flag) {
        if (flag) {
            this.deleteId.enable();
        } else {
            this.deleteId.hide();
        }
    }

    /**
     * show the form to add new ids or edit current ones
     */
    public void showPatientOtherIdsForm(boolean newPatientId) {
        if (this.showUtilityButtons) {
            editId.disable();
            deleteId.disable();
        }

        removeMainContent();
        PatientOtherIdsDTO patientOtherIdsDTO = null;
        if(!newPatientId) { // don't load a currently selected patient to the form if you're adding
           patientOtherIdsDTO = this.patientOtherIdsListPanel.getSelectedPatientOtherId(); 
        } 
        addMember(new PatientOtherIdsForm(this, patientOtherIdsDTO, this.patientId));
    }

    long getCommunityId() {
        return this.communityId;
    }
    


    /**
     * go back to the patient ids list
     */
    class ListIdsClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            showIdList(patientId);
        }
    }

    /**
     * show the form to add a new patient id
     */
    class AddIdsClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            showPatientOtherIdsForm(true);
        }
    }

    /**
     * show the form to edit an existing patient id
     */
    class EditIdClickHandler implements ClickHandler {
// FIXME patientOtherIdsDTO is redundant
        public void onClick(ClickEvent event) {
            PatientOtherIdsDTO patientOtherIdsDTO = patientOtherIdsListPanel.getSelectedPatientOtherIds();
            if (patientOtherIdsDTO == null) {
                SC.warn("Please select a patient and try again.");
                return;
            }
            showPatientOtherIdsForm(false);
        }
    }

    /**
     * delete currently selected patient id entry
     */
    class DeleteIdClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            PatientOtherIdsDTO patientOtherIdsDTO = patientOtherIdsListPanel.getSelectedPatientOtherIds();
            if (patientOtherIdsDTO == null) {
                SC.warn("Please select a patient and try again.");
                return;
            }

            String message = "Are you sure you want to delete this id?";
            SC.confirm("Delete patient other ID", message, new DeleteConfirmationCallback(patientOtherIdsDTO));
        }
    }

    class DeleteConfirmationCallback implements BooleanCallback {

        PatientOtherIdsDTO delPatientOtherIdsDTO;

        DeleteConfirmationCallback(PatientOtherIdsDTO deletePatientOtherIdsDTO) {
            delPatientOtherIdsDTO = deletePatientOtherIdsDTO;
        }

        @Override
        public void execute(Boolean value) {

            // if the user said yes, then remove patientOtherIdsDTO
            if (value.booleanValue()) {
                patientOtherIdsService.deletePatientOtherId(delPatientOtherIdsDTO, new DeletePatientOtherIdsCallback());
            }
        }
    }
    
        class DeletePatientOtherIdsCallback implements AsyncCallback {

        @Override
        public void onSuccess(Object t) {
            patientOtherIdsListPanel.gotoPage(1);

        }

        @Override
        public void onFailure(Throwable thrwbl) {
            SC.warn("Error deleting patient other ID.");
        }
    }

}
