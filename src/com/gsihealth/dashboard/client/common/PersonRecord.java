package com.gsihealth.dashboard.client.common;

import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import java.util.Date;

/**
 *
 * @author Chad Darby
 */
public class PersonRecord extends ListGridRecord {


    private int index;

    private static final String PATIENT_ID = "Patient ID";
    private static final String FIRST_NAME = "First Name";
    private static final String LAST_NAME = "Last Name";
    private static final String MIDDLE_NAME = "Middle Name";
    private static final String DATE_OF_BIRTH = "DOB";
    private static final String ADDRESS = "Address";
    private static final String ORGANIZATION = "Organization";
    private static final String PROGRAM_NAME = "Program Name";
    private static final String ENROLLMENT_STATUS = "Enrollment Status";
    private static final String PATIENT_STATUS = "Patient Status";
    private static final String CARETEAM = "Care Team";
    private static final String HEALTH_HOME = "Health Home";
    private static String PATIENT_MESSAGING="Patient Messaging";
    private static String GENDER ="Gender";
    private PersonDTO personDTO;
    public static String CM_ORG = "CM Org";
    private static String MINOR = "Minor";
    
    public PersonRecord() {

    }
    
    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * @return Patient ID
     */
    public long getPatientID() {
        return Long.valueOf(getAttribute(PATIENT_ID));
    }
    
    /**
     * @param patientID the patientID to set
     */
    public void setPatientID(long patientID) {
        setAttribute(PATIENT_ID, patientID);
    }
    
    /**
     * @param patientID the patientID to set
     */
    public void setPatientIDAsString(String patientID) {
        setAttribute(PATIENT_ID, patientID);
    }
    
    /**
     * @return the firstName
     */
    public String getFirstName() {
        return getAttribute(FIRST_NAME);
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        setAttribute(FIRST_NAME, firstName);
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return getAttribute(LAST_NAME);
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        setAttribute(LAST_NAME, lastName);
    }

    /**
     * @return the lastName
     */
    public String getMiddleName() {
        return getAttribute(MIDDLE_NAME);
    }

    /**
     * @param lastName the lastName to set
     */
    public void setMiddleName(String lastName) {
        setAttribute(MIDDLE_NAME, lastName);
    }

    /**
     * @return the dateOfBirth
     */
    public Date getDateOfBirth() {
        return getAttributeAsDate(DATE_OF_BIRTH);
    }

    /**
     * @param dateOfBirth the dateOfBirth to set
     */
    public void setDateOfBirth(Date dateOfBirth) {
        this.setAttribute(DATE_OF_BIRTH, dateOfBirth);
    }
    
     /**
     * @return the dateOfBirth
     */
    //Spira 5438 : Sometimes Date objects cahnge there value from client to server so used string
    public String getDateOfBirthAsString() {
        return getAttributeAsString(DATE_OF_BIRTH);
    }

    /**
     * @param dateOfBirth the dateOfBirth to set
     */
    //Spira 5438 : Sometimes Date objects cahnge there value from client to server so used string to display record in Grid
    public void setDateOfBirthAsString(String dateOfBirth) {
        this.setAttribute(DATE_OF_BIRTH, dateOfBirth);
    }

    public String getAddress() {
        return getAttribute(ADDRESS);
    }

    public void setAddress(String data) {
        setAttribute(ADDRESS, data);
    }


    public String getProgramName() {
        return StringUtils.defaultString(getAttribute(PROGRAM_NAME));
    }

    public void setProgramName(String consentLevel) {
        setAttribute(PROGRAM_NAME, consentLevel);
    }

    public String getEnrollmentStatus() {
        return StringUtils.defaultString(getAttribute(ENROLLMENT_STATUS));
    }

    public void setEnrollmentStatus(String enrollmentStatus) {
        setAttribute(ENROLLMENT_STATUS, enrollmentStatus);
    }


    public String getPatientStatus() {
        return StringUtils.defaultString(getAttribute(PATIENT_STATUS));
    }

    public void setPatientStatus(String patientStatus) {
        setAttribute(PATIENT_STATUS, patientStatus);
    }



    public String getCareteam() {
        return StringUtils.defaultString(getAttribute(CARETEAM));
    }

    public void setCareteam(String careteam) {
        setAttribute(CARETEAM, careteam);
    }
    
    public String getHealthHome() {
        return StringUtils.defaultString(getAttribute(HEALTH_HOME));
    }

    public void setHealthHome(String healthHome) {
        setAttribute(HEALTH_HOME, healthHome);
    }

    /**
     * @return the org
     */
    public String getOrganizationName() {
        return getAttribute(ORGANIZATION);
    }

    /**
     * @param org the org to set
     */
    public void setOrganizationName(String org) {
        setAttribute(ORGANIZATION, org);
    }

    public PersonDTO getPersonDTO() {
        return personDTO;
    }

    public void setPersonDTO(PersonDTO personDTO) {
        this.personDTO = personDTO;
    }
    
     /**
     * @return the GENDER
     */
    public String getGender() {
        return getAttribute(GENDER);
    }

    /**
     * @param gender the GENDER to set
     */
    public void setGender(String gender) {
        setAttribute(GENDER, gender);
    }
    
    /**
     * @return the patient is Active or not
     */
    public String getPatientUserActive() {
        return getAttribute(PATIENT_MESSAGING);
    }

    /**
     * @param patientMessaging set patient to Active or not
     */
    public void setPatientUserActive(String patientMessaging) {
        setAttribute(PATIENT_MESSAGING, patientMessaging);
    }

     public String getOrgId() {
        return getAttribute(CM_ORG);
    }

    public void setOrgId(String orgId) {
        setAttribute(CM_ORG, orgId);
    }
    
   public String getMinor() {
        return getAttribute(MINOR);
    }

   
    public void setMinor(String minor) {
        setAttribute(MINOR, minor);
    }


}
