package com.gsihealth.dashboard.client.common;

import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.entity.dto.ProgramNameDTO;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class FormUtils {

    public static final String EMPTY = "";
    public static final String REQUIRED_TITLE_PREFIX = "* ";

    /**
     * Add an empty string to beginning
     *
     * @return
     */
    public static String[] getCodesWithAddedBlankOption(String[] baseCodes) {

        String[] updatedCodes = new String[baseCodes.length + 1];

        updatedCodes[0] = EMPTY;

        System.arraycopy(baseCodes, 0, updatedCodes, 1, baseCodes.length);

        return updatedCodes;
    }
    /**
     * method for populate the HealthHome Drop down values
     *
     * @param baseData
     * @return
     */
    public static LinkedHashMap<String, String> buildProgramHealthHomeMap(LinkedHashMap<Long, String> baseData) {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

        for (Long tempKey : baseData.keySet()) {
            String tempValue = baseData.get(tempKey);

                 
            String theKey = Constants.DELIMITER_SELECT_ITEM + tempKey;
            data.put(theKey, tempValue);
        }

        return data;
    }
    
     public static LinkedHashMap<String, String> buildProgramNameMap(LinkedHashMap<Long, String> baseData) {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

        //data.put(FormUtils.EMPTY, FormUtils.EMPTY);
        for (Long tempKey : baseData.keySet()) {
            String tempValue = baseData.get(tempKey);

            //String theKey = Constants.DELIMITER_SELECT_ITEM + tempKey;
            data.put(tempValue, tempValue);
        }

        return data;
    }
     
     //JIRA 1023
     public static LinkedHashMap<String, String> buildProgramNameWithCodeMap(LinkedHashMap<Long, String> baseData) {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

        //data.put(FormUtils.EMPTY, FormUtils.EMPTY);
        for (Long tempKey : baseData.keySet()) {
            String tempValue = baseData.get(tempKey);

            String theKey = Constants.DELIMITER_SELECT_ITEM + tempKey;
            data.put(theKey, tempValue);
        }

        return data;
    }
     
     public static LinkedHashMap<String, String> buildHealthHomeMap(LinkedHashMap<Long, String> baseData) {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

        for (Long tempKey : baseData.keySet()) {
            String tempValue = baseData.get(tempKey);
            String theKey = Constants.DELIMITER_SELECT_ITEM + tempKey;
            data.put(theKey, tempValue);
        }

        return data;
    }
     
    public static LinkedHashMap<String, String> buildProgramCodeForHealthHomeMap(List<Long> baseData, LinkedHashMap<Long, String> programNameMap)
    {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
        for (Long programId : baseData) {
            String val = Constants.DELIMITER_SELECT_ITEM + programId;
            data.put(val, programNameMap.get(programId));
        }

        return data;
    }
     
    /**
     * method for filter the program name dropdown value based upon HealthHome
     * values
     *
     * @param healthHome
     * @param list
     * @return
     */
    public static String[] convertProgramNameHealthHome(String healthHome, List<ProgramNameDTO> list) {
        List<ProgramNameDTO> programNameDTOs = getProgramNameHealthHomeSize(healthHome, list);

        String[] data = new String[programNameDTOs.size()];

        for (int i = 0; i < programNameDTOs.size(); i++) {
            if (programNameDTOs.get(i).getProgramHealth().toString().equalsIgnoreCase(healthHome)) {
                data[i] = programNameDTOs.get(i).getValue();
            }
        }

        return data;
    }

    private static List<ProgramNameDTO> getProgramNameHealthHomeSize(String healthHome, List<ProgramNameDTO> list) {

        List<ProgramNameDTO> pnList = new ArrayList<ProgramNameDTO>();
        for (ProgramNameDTO nameDTO : list) {

            if (nameDTO.getProgramHealth().toString().equalsIgnoreCase(healthHome)) {
                pnList.add(nameDTO);
            }
        }
        return pnList;
    }
    
     public static LinkedHashMap<String, String> buildPatientStatusMap(LinkedHashMap<Long, String> baseData) {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

        for (Long tempKey : baseData.keySet()) {
            String tempValue = baseData.get(tempKey);

                 
            String theKey = Constants.DELIMITER_SELECT_ITEM + tempKey;
            data.put(theKey, tempValue);
        }

        return data;
    }
    
       public static LinkedHashMap<String, String> buildMap(LinkedHashMap<Long, String> baseData) {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

        for (Long tempKey : baseData.keySet()) {
            String tempValue = baseData.get(tempKey);

                 
            String theKey = Constants.DELIMITER_SELECT_ITEM + tempKey;
            data.put(theKey, tempValue);
        }

        return data;
    }
}
