/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.common;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.service.PatientOtherIdsService;
import com.gsihealth.dashboard.client.service.PatientOtherIdsServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PatientOtherIdsDTO;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author beth.boose
 */
public class PatientOtherIdsListPanel extends VLayout {
    private Logger logger = Logger.getLogger(getClass().getName());

    public static final String ORG_OID = "Org ID";
    public static final String ORG_NAME = "Org Name";
    public static final String PATIENT_OTHER_ID = "Patient ID";
    public static String PATIENT_OTHER_ID_ID = "Patient other id id";
    public static String PATIENT_CREATED_DATE = "Patient created date";
    String[] columnNames = {ORG_OID, ORG_NAME, PATIENT_OTHER_ID};

    private long patientId;
    private ListGrid listGrid;
    private GridPager pager;
    private ProgressBarWindow progressBarWindow;
    private GetPageProvidersCallback getPageProvidersCallback;
    private PatientOtherIdsServiceAsync patientOtherIdsService = GWT.create(PatientOtherIdsService.class);
    private PatientOtherIdsPanel parentPanel;
    private boolean showUtilityButtons;

    public PatientOtherIdsListPanel(PatientOtherIdsPanel parentPanel, long patientId, boolean showUtilityButtons) {
        this.parentPanel = parentPanel;
        this.patientId = patientId;
        this.showUtilityButtons = showUtilityButtons;
        this.buildGui();
        progressBarWindow = new ProgressBarWindow("Loading", "Loading...");
        this.getPageProvidersCallback = new GetPageProvidersCallback();
        gotoPage(1);
    }

    private void buildGui() {
        this.listGrid = this.buildListGrid();
        
        if (this.showUtilityButtons) {
            listGrid.addRecordDoubleClickHandler(new OtherIdDoubleClickHandler());
            listGrid.addRecordClickHandler(new OtherIdClickHandler());
        }
        addMember(listGrid);
        pager = new GridPager(0);
        pager.setPageEventHandler(new ProviderPageEventHandler());
        addMember(pager);
    }

    private ListGrid buildListGrid() {
        this.listGrid = new ListGrid();
        this.listGrid.setCanReorderFields(true);
        this.listGrid.setCanReorderRecords(true);

        this.listGrid.setWidth100();
        this.listGrid.setHeight100();

        this.listGrid.setAlternateRecordStyles(true);
        this.listGrid.setShowAllRecords(false);
        this.listGrid.setSelectionType(SelectionStyle.SINGLE);
        this.listGrid.setLeaveScrollbarGap(false);
        this.listGrid.setCanEdit(false);

        ListGridField[] fields = buildListGridFields(columnNames);
        this.listGrid.setFields(fields);
        this.listGrid.setCanResizeFields(true);

        return this.listGrid;
    }

    private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];

        for (int i = 0; i < columnNames.length; i++) {
            String tempColumnName = columnNames[i];
            ListGridField tempField = buildListGridField(tempColumnName);
            fields[i] = tempField;
        }

        return fields;
    }

    private ListGridField buildListGridField(String title) {
        ListGridField tempField = new ListGridField(title, title);

        tempField.setCanEdit(false);

        return tempField;
    }

    class ProviderPageEventHandler implements PageEventHandler {

        public void handlePage(int pageNum, int pageSize) {
            long communityId=ApplicationContextUtils.getLoginResult().getCommunityId();
            progressBarWindow.show();
            patientOtherIdsService.getPatientOtherIds(patientId, pageNum, pageSize,communityId, getPageProvidersCallback);
        }
    }

    class GetPageProvidersCallback implements AsyncCallback<SearchResults<PatientOtherIdsDTO>> {

        public void onSuccess(SearchResults<PatientOtherIdsDTO> searchResults) {
            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            List<PatientOtherIdsDTO> patientOtherIdsDTO = searchResults.getData();
            int totalCount = searchResults.getTotalCount();

            populate(patientOtherIdsDTO, totalCount);
        }

        public void onFailure(Throwable thrwbl) {
            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }
            SC.say("Error loading IDs. Please try again.");
        }
    }

    public void gotoPage(final int pageNumber) {
        
        long communityId=ApplicationContextUtils.getLoginResult().getCommunityId();
        patientOtherIdsService.getTotalCountOfOtherIds(patientId,communityId, new AsyncCallback<Integer>() {

            public void onSuccess(Integer total) {

                if (total > 0) {
                    pager.setTotalCount(total);
                    pager.refreshPageLabels(pageNumber);
                    pager.gotoPage(pageNumber);
                } else {
                    clearListGrid();
                }
            }

            public void onFailure(Throwable thrwbl) {
            }
        });
    }

    protected void populate(List<PatientOtherIdsDTO> patientOtherIdsDTO, int totalCount) {
        if (!patientOtherIdsDTO.isEmpty()) {
            ListGridRecord[] records = this.convertDTOsToRecords(patientOtherIdsDTO);
            populateListGrid(records, totalCount);

            int currentPageNumber = pager.getCurrentPageNumber();
            pager.refreshPageLabels(currentPageNumber);

            pager.handleNavigationButtons(currentPageNumber);
        } else {
            clearListGrid();
        }
    }

    public void populateListGrid(ListGridRecord[] pagedData, int totalCount) {
        // scroll to first row
        listGrid.scrollToRow(1);

        listGrid.setData(pagedData);
        listGrid.markForRedraw();
        pager.setTotalCount(totalCount);
    }

    public PatientOtherIdsDTO getSelectedPatientOtherIds() {
        PatientOtherIdsDTO patientOtherIdsDTO = null;
        PatientOtherIdsRecord record = (PatientOtherIdsRecord) listGrid.getSelectedRecord();
        if (record != null) {
            patientOtherIdsDTO = record.getPatientOtherIdsDTO();
        }
        return patientOtherIdsDTO;
    }

    private PatientOtherIdsRecord[] convertDTOsToRecords(List<PatientOtherIdsDTO> patientOtherIdsDTO) {
        PatientOtherIdsRecord[] records = new PatientOtherIdsRecord[patientOtherIdsDTO.size()];
        int i = 0;
        for (PatientOtherIdsDTO each : patientOtherIdsDTO) {
            records[i] = new PatientOtherIdsRecord(each);
            i++;
        }

        return records;
    }

    public void clearListGrid() {
        ListGridRecord[] records = new ListGridRecord[1];
        listGrid.setData(records);

        pager.clearPageLabels();
    }

    public PatientOtherIdsDTO getSelectedPatientOtherId() {
        PatientOtherIdsDTO patientOtherId = null;

        PatientOtherIdsRecord record = (PatientOtherIdsRecord) listGrid.getSelectedRecord();

        if (record != null) {
            patientOtherId = record.getPatientOtherIdsDTO();
        }

        return patientOtherId;
    }
    
    public PatientOtherIdsServiceAsync getPatientOtherIdsService() {
        return this.patientOtherIdsService;
    }

    /**
     * click twice on an id and directly open the update box
     */
    class OtherIdDoubleClickHandler implements RecordDoubleClickHandler {

        @Override
        public void onRecordDoubleClick(RecordDoubleClickEvent event) {
            parentPanel.showPatientOtherIdsForm(false);
        }
    }

    /**
     * click once on an id and enable the update button
     */
    class OtherIdClickHandler implements RecordClickHandler {

        public void onRecordClick(RecordClickEvent event) {
            parentPanel.flipUpdateIdButton(true);
            parentPanel.flipDeleteIdButton(true);
        }

    }
}
