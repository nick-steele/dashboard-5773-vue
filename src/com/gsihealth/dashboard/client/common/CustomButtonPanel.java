package com.gsihealth.dashboard.client.common;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;

public class CustomButtonPanel extends FlowPanel {

    private CustomButton button;
    private Label label;
    private boolean enable;

    /**
     * @param size
     *            defines the size of image and width of the panel
     * @param text
     *            Text which comes below the button
     * @param customButtonPanelListener
     *            {@link ClickHandler} for the button
     */
    public CustomButtonPanel(final String text) {
        super();
        addStyleName("customButtonPanel");
        button = new CustomButton();
        String textNoSpace = text.replaceAll(" ", "");
        button.setTitle(textNoSpace);

        label = new Label(text);
        label.addStyleName("button-label");

        addStyleName("iphone-button");

        setEnable(true);
        add(button);
        add(label);

    }

    /**
     * Sets a new style for the button three styles have to be implemented in
     * css, "style", "styleDown", "styleOver" to be able to see the images,
     * other wise blank image will come. Default image can not be supported
     * since multiple styles are not supported by {@link SmartGWT}
     *
     * @param style
     */
    public void setIconStyle(String style) {
        button.setIconStyle(style);
    }

    public void setEnable(boolean enable){
    	this.enable = enable;
    }
    /**
     * @return the label
     */
    public Label getLabel() {
        return label;
    }

    public void setAlertVisible(boolean alertVisible) {
        button.setAlertVisible(alertVisible);
    }

    public void setAlertText(int i) {
        button.setAlertText(i);
    }

    public void addClickHandler(final com.smartgwt.client.widgets.events.ClickHandler smartGwtClickHandler) {
        if(this.enable == true){
	    	button.addClickHandler(new com.google.gwt.event.dom.client.ClickHandler() {
	            @Override
	            public void onClick(ClickEvent event) {
	                smartGwtClickHandler.onClick(null);
	            }
	        }, ClickEvent.getType());
        }
    }
}
