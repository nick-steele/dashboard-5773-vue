package com.gsihealth.dashboard.client.common;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.careteam.ViewPatientForm;
import com.gsihealth.dashboard.client.enrollment.BasePatientPanel;
import com.gsihealth.dashboard.client.service.MyPatientListService;
import com.gsihealth.dashboard.client.service.MyPatientListServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.ListTagDTO;
import com.gsihealth.dashboard.entity.dto.MyPatientInfoDTO;
import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;
import com.gsihealth.dashboard.redux.Redux;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;

import java.util.Map;

import static com.gsihealth.dashboard.client.util.Constants.USER_ADDED_LISTTAG;
import static com.gsihealth.dashboard.client.util.Constants.TAGTYPE_USER_ADDED;

import com.gsihealth.dashboard.entity.dto.PatientListLabelDTO;


/**
 *
 * @author Chad.Darby
 */
public class BaseViewPatientPanel extends BasePatientPanel {


    public static final String DEFAULT_EXIST_PATIENT_LBL = "Patient is already on your Patient Panel";
    protected Layout addToMyListLayout = null;
    protected Layout alreadyOnListLayout = null;
    protected Label alreadyOnListLabel;
    protected MyPatientListServiceAsync myPatientListService = GWT.create(MyPatientListService.class);
    protected PersonDTO person;
    protected Button activityTrackerButton;
    protected ViewPatientForm viewPatientForm;
    private Map<Long, Map<String, Long>> communityTagMaps;
    
    protected Button addToListButton = null;

    protected ClientApplicationProperties props;

    public BaseViewPatientPanel() {
    	props = ApplicationContextUtils.getClientApplicationProperties();
    }

    /**
     * there are 3 cases for my patient list labels: 
     * show add button (you can add this patient)
     * show already on list label (you already have this patient)
     * show neither (you don't have access to this patient)
     * work it out here
     */
    protected void handleMyPatientListGuiItems() {
        // show the MyPatient GUI items based on the person, if they don't have access, don't do anything
        IsPatientAlreadyOnListCallback isPatientAlreadyOnListCallback = new IsPatientAlreadyOnListCallback();
        isPatientAlreadyOnListCallback.attempt();
    }

//    protected void refreshMyPatientList() {
//        // refresh gui
//        MyPatientListPanel myPatientListPanel = (MyPatientListPanel) ApplicationContext.getInstance().getAttribute(Constants.MY_PATIENT_LIST_PANEL_KEY);
//        myPatientListPanel.loadMyPatientList();
//    }

    protected Layout buildAlreadyOnListLayout() {
        HLayout theLayout = new HLayout(10);

        theLayout.setWidth(460);
        theLayout.setAlign(Alignment.RIGHT);

        long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
        myPatientListService.getConfigurationValue(communityId,
                DEFAULT_EXIST_PATIENT_LBL,
                "homepage.message.bundle",
                "popup.tooltip.already_added",
                new GetExistPatientLayoutCallback());
        alreadyOnListLabel = new Label(DEFAULT_EXIST_PATIENT_LBL);
        alreadyOnListLabel.setWidth(300);
        alreadyOnListLabel.setHeight(40);
        alreadyOnListLabel.setStyleName("myPatientAlreadyOnListLabel");
        theLayout.addMember(alreadyOnListLabel);
        return theLayout;
    }

    protected Layout buildAddToMyListLayout() {
        HLayout theLayout = new HLayout(10);

        theLayout.setWidth(460);
        theLayout.setAlign(Alignment.RIGHT);

        Button addToMyListButton = new Button(props.getDemographicAddToPatientButton());
        addToMyListButton.setWidth(150);
        //addToMyListButton.setID("addToMyListBaseViewPatientPanel");
        addToMyListButton.addClickHandler(new AddToMyListClickHandler());

        theLayout.addMember(addToMyListButton);

//        activityTrackerButton= buildAcivityTrackerButton(activityTrackerButtonText);
//        theLayout.addMember(activityTrackerButton);
        return theLayout;
    }
    
    protected Button buildAddToListButton() {
    	Button button = new Button(props.getDemographicAddToPatientButton());
    	button.setWidth(150);
    	button.addClickHandler(new AddToMyListClickHandler());
    	button.hide();
        return button;
    }
    

    class AddToMyListClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {

            PatientEnrollmentDTO patientEnrollment = person.getPatientEnrollment();

            long userId = ApplicationContextUtils.getLoginResult().getUserId();
            long patientId = patientEnrollment.getPatientId();
            long communityId = patientEnrollment.getCommunityId();

            MyPatientInfoDTO myPatientInfoDTO = new MyPatientInfoDTO(userId, patientId, communityId);
//            myPatientListService.getListTags(communityId);
            addToMyList(myPatientInfoDTO, communityId);
        }
    }

    private void addToMyList(MyPatientInfoDTO myPatientInfoDTO, long communityId) {
        ListTagDTO userAddedListTag = (ListTagDTO) ApplicationContext.getInstance().getAttribute(USER_ADDED_LISTTAG);
        if(userAddedListTag == null){        	
            myPatientListService.getTagByType(communityId, TAGTYPE_USER_ADDED, new GetUserTagCallback(myPatientInfoDTO));
        }else{
            myPatientInfoDTO.getTags().add(userAddedListTag);
            myPatientListService.add(myPatientInfoDTO, new AddToMyListCallback());
        }

    }

    class GetUserTagCallback implements AsyncCallback<ListTagDTO> {
        private MyPatientInfoDTO myPatientInfoDTO;

        public GetUserTagCallback(MyPatientInfoDTO myPatientInfoDTO){
            this.myPatientInfoDTO = myPatientInfoDTO;
        }

        @Override
        public void onFailure(Throwable throwable) {
            SC.warn("Error adding patient to your patient panel.");
        }

        @Override
        public void onSuccess(ListTagDTO listTagDTO) {
            ApplicationContext.getInstance().putAttribute(USER_ADDED_LISTTAG, listTagDTO);
            myPatientInfoDTO.getTags().add(listTagDTO);
            myPatientListService.add(myPatientInfoDTO, new AddToMyListCallback());

        }
    }

   class AddToMyListCallback implements AsyncCallback {

        @Override
        public void onSuccess(Object t) {
            Redux.jsEvent("local:myPatientList:refresh");
            // hide button
            if(addToMyListLayout != null) {
            	addToMyListLayout.hide();
            }
            
            if (addToListButton != null) {
            	addToListButton.hide();
            }

            // show label
            if(alreadyOnListLayout != null) {
	            alreadyOnListLabel.setContents("Patient added to your patient panel.");
	            alreadyOnListLayout.show();
            }

            SC.say("Patient added to your patient panel.");
        }

        @Override
        public void onFailure(Throwable thrwbl) {
            SC.warn("Error adding patient to your patient panel.");
        }
    }

    class IsPatientAlreadyOnListCallback extends RetryActionCallback<PatientListLabelDTO> {

        @Override
        public void attempt() {
            long userId = ApplicationContextUtils.getLoginResult().getUserId();

            PatientEnrollmentDTO patientEnrollment = person.getPatientEnrollment();
            long patientId = patientEnrollment.getPatientId();
            long communityId = patientEnrollment.getCommunityId();
            myPatientListService.myPatientListSelectionDisplayOptions(userId, patientId, communityId, this);
        }

        @Override
        public void onCapture(PatientListLabelDTO labelSelectSettings) {
        	if (alreadyOnListLayout != null) {
        		alreadyOnListLayout.setVisible(labelSelectSettings.getShowLabel());
        	}
        	if (addToMyListLayout != null) {
        		addToMyListLayout.setVisible(labelSelectSettings.getShowButtom());
        	}
        	if (addToListButton != null) {
            	addToListButton.setVisible(labelSelectSettings.getShowButtom());
            }
        }
    }

    @Deprecated
    class IsListFullCallback implements AsyncCallback<Boolean> {

        @Override
        public void onSuccess(Boolean listFull) {

            if (listFull) {
                addToMyListLayout.hide();
                alreadyOnListLayout.show();

                alreadyOnListLabel.setContents("Your patient list is full.");
            } else {
                // show the button "Add to MyList"
                addToMyListLayout.show();
                alreadyOnListLayout.hide();
            }
        }

        @Override
        public void onFailure(Throwable thr) {
            SC.warn("Failure checking your patient list.");
        }
    }

    class GetExistPatientLayoutCallback implements AsyncCallback<String> {
        @Override
        public void onSuccess(String configValue) {
            alreadyOnListLabel.setContents(configValue);
        }

        @Override
        public void onFailure(Throwable thr) {
            SC.warn("Failure getting label for exist patient.");
        }
    }


}
