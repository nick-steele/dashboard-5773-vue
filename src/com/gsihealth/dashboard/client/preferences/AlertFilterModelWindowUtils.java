/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.preferences;

import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.entity.dto.AlertFilterPreferenceDTO;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author beth.boose
 *
 */
public class AlertFilterModelWindowUtils {

    public static final String SEVERITY = "Severity";
    public static final String ALERT_TYPE = "Alert Type";
    public static final String CHECK_VALUE = "checkValue";
    public static final String COMMUNITY_ID = "communityID";
    public static final String APPLICATION_ID = "applicationID";
    public static final String APPLICATION_ALERT_ID = "applicationALertID";
    public static final String USER_ID = "userID";
    public static final String STATUS = "status";
    public static final String IS_ROLE_BASED = "isRoleBased";
    public static final String SEVERITY_COLUMN_SIZE = "25%";
    public static final String NO_FILTERING = "noFiltering";

    public static AlertFilterListGridRecord convertDTOtoRecord(int index, AlertFilterPreferenceDTO theAlertType) {

        AlertFilterListGridRecord alertFilterListGridRecord = new AlertFilterListGridRecord();
        setAlertFilterAttributes(alertFilterListGridRecord, theAlertType, index);

        return alertFilterListGridRecord;
    }

    /**
     *
     * @param alertFilterPrefDTO
     * @return a list of dto's converted to records
     */
    public static AlertFilterListGridRecord[] convertDTOListToRecordList(List<AlertFilterPreferenceDTO> alertFilterPrefDTO) {

        AlertFilterListGridRecord[] records = new AlertFilterListGridRecord[alertFilterPrefDTO.size()];
        for (int index = 0; index < alertFilterPrefDTO.size(); index++) {
            AlertFilterPreferenceDTO tempAlertFilter = alertFilterPrefDTO.get(index);

            records[index] = convertDTOtoRecord(index, tempAlertFilter);
        }
        return records;
    }

    /**
     * TODO - null check?
     *
     * @param alertFilterListGridRecords
     * @return list of alertFilterListGridRecords
     */
    public static List<AlertFilterPreferenceDTO> convertRecordsListToDTOlist(AlertFilterListGridRecord[] alertFilterListGridRecords) {
        List<AlertFilterPreferenceDTO> alertFilterRefDataDTOList = new ArrayList<AlertFilterPreferenceDTO>();
        for (AlertFilterListGridRecord each : alertFilterListGridRecords) {
            alertFilterRefDataDTOList.add(convertRecordToDTO(each));
        }
        return alertFilterRefDataDTOList;
    }

    /**
     * TODO yes I'm going to hell for this, pull out all selected records and
     * set checkValue to true, remove them from listGrid, then set the remaining
     * records to false
     *
     * @param listGrid
     * @return
     */
    public static List<AlertFilterPreferenceDTO> convertListGridToDTOlist(ListGrid listGrid) {
        ListGridRecord[] selected = listGrid.getSelectedRecords();
        setCheckValueAttribute(selected, true);
        listGrid.removeSelectedData();
        ListGridRecord[] unselected = listGrid.getRecords();
        setCheckValueAttribute(unselected, false);

        // doh, can't use apache commons clientside for gwt testing, do it the old fashioned way FIXME configure gwt for apache
        ListGridRecord[] combinedListGridRecords = concat(selected, unselected);
        AlertFilterListGridRecord[] recombinedArr = cast(combinedListGridRecords);
        return convertRecordsListToDTOlist(recombinedArr);
    }

    /**
     * TODO - null check?
     *
     * @param alertFilterListGridRecord
     * @return alertFilterListGridRecord
     */
    public static AlertFilterPreferenceDTO convertRecordToDTO(AlertFilterListGridRecord alertFilterListGridRecord) {

        AlertFilterPreferenceDTO alertFilterPreferenceDTO = new AlertFilterPreferenceDTO();
        alertFilterPreferenceDTO.setCommunityID(alertFilterListGridRecord.getCommunityID());
        alertFilterPreferenceDTO.setApplicationAlertID(alertFilterListGridRecord.getApplicationALertID());
        alertFilterPreferenceDTO.setUserID(alertFilterListGridRecord.getUserID());
        alertFilterPreferenceDTO.setAlertName(alertFilterListGridRecord.getAlertTypeName());
        alertFilterPreferenceDTO.setNoFiltering(alertFilterListGridRecord.getNoFiltering());
        
        alertFilterPreferenceDTO.setCheckValue(alertFilterListGridRecord.getCheckVaue());

        return alertFilterPreferenceDTO;
    }

    /**
     * FIXME clean this up - after fixing backing code - enabled doesn't belong
     * here
     *
     * @param alertFilterReferenceData
     * @param alertFilterPrefDTO
     * @param index
     */
    private static void setAlertFilterAttributes(AlertFilterListGridRecord alertFilterListGridRecord, AlertFilterPreferenceDTO alertFilterPrefDTO, int index) {
        alertFilterListGridRecord.setIndex(index);
        alertFilterListGridRecord.setSeverity(alertFilterPrefDTO.getAlertSeverity());
        alertFilterListGridRecord.setAlertTypeName(alertFilterPrefDTO.getAlertName());
        alertFilterListGridRecord.setApplicationALertID(alertFilterPrefDTO.getApplicationAlertID());
        alertFilterListGridRecord.setApplicationID(alertFilterPrefDTO.getApplicationID());
        alertFilterListGridRecord.setUserID(alertFilterPrefDTO.getUserID());
        alertFilterListGridRecord.setCommunityID(alertFilterPrefDTO.getCommunityID());
        alertFilterListGridRecord.setNoFiltering(alertFilterPrefDTO.getNoFiltering());
        alertFilterListGridRecord.setEnabled(!alertFilterPrefDTO.getNoFiltering());
        alertFilterListGridRecord.setCheckVaue(alertFilterPrefDTO.isCheckValue());
        if(alertFilterPrefDTO.getNoFiltering()){
           alertFilterListGridRecord.setCheckVaue(true); 
        }
    }

    /**
     * TODO yeah its ugly, pull out all the selected records
     *
     * @param listGrid
     * @return
     */
    private static void setCheckValueAttribute(ListGridRecord[] listGridRecords, boolean setValue) {
        for (ListGridRecord each : listGridRecords) {
            each.setAttribute(CHECK_VALUE, setValue);
        }

    }

    private static AlertFilterListGridRecord[] cast(ListGridRecord[] listGridRecords) {
        AlertFilterListGridRecord[] alertFilterListGridRecords = new AlertFilterListGridRecord[listGridRecords.length];
        for (int i = 0; i < listGridRecords.length; i++) {
            alertFilterListGridRecords[i] = (AlertFilterListGridRecord) listGridRecords[i];
        }
        return alertFilterListGridRecords;
    }

    public static ListGridRecord[] concat(ListGridRecord[] first, ListGridRecord[] second) {
        if (first == null) {
            return second;
        }
        if (second == null) {
            return first;
        }
        int len = first.length + second.length;
        ListGridRecord[] result = new ListGridRecord[len];
        System.arraycopy(first, 0, result, 0, first.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }
}
