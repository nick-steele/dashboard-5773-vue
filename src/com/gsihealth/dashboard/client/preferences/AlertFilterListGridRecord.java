/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.preferences;

import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 * TODO clean this up for next release
 * FIXME - fix equals/hashcode, maybe add the alertfilterid?
 * @author Vinay
 */
public class AlertFilterListGridRecord extends ListGridRecord{
    private int index;
    
    public AlertFilterListGridRecord(){
        
    } 
   
    public int getIndex() {
        return index;
    }

   
    public void setIndex(int index) {
        this.index = index;
    }

    public  String getSeverity() {
        return getAttribute(AlertFilterModelWindowUtils.SEVERITY);
    }

    public  void setSeverity(String severity) {
        setAttribute(AlertFilterModelWindowUtils.SEVERITY, severity);
    }
    
    public  String getAlertTypeName() {
       return getAttribute(AlertFilterModelWindowUtils.ALERT_TYPE);
    }

    public  void setAlertTypeName(String alertType) {
         setAttribute(AlertFilterModelWindowUtils.ALERT_TYPE, alertType);
    }
    
    public boolean getCheckVaue() {
       return getAttributeAsBoolean(AlertFilterModelWindowUtils.CHECK_VALUE);
    }

    public  void setCheckVaue(boolean checkValue) {
       setAttribute(AlertFilterModelWindowUtils.CHECK_VALUE, checkValue);
    }

    public Integer getCommunityID() {
        return getAttributeAsInt(AlertFilterModelWindowUtils.COMMUNITY_ID);
    }

    /**
     * @param communityID the communityID to set
     */
    public void setCommunityID(Integer communityID) {
        setAttribute(AlertFilterModelWindowUtils.COMMUNITY_ID, communityID);
    }

    /**
     * @return the applicationID
     */
    public Long getApplicationID() {
        return getAttributeAsLong(AlertFilterModelWindowUtils.APPLICATION_ID);
    }

    /**
     * @param applicationID the applicationID to set
     */
    public void setApplicationID(Long applicationID) {
        setAttribute(AlertFilterModelWindowUtils.APPLICATION_ID, applicationID);
    }

    /**
     * @return the userID
     */
    public Long getUserID() {
        return getAttributeAsLong(AlertFilterModelWindowUtils.USER_ID);
    }

    /**
     * @param userID the userID to set
     */
    public void setUserID(Long userID) {
        setAttribute(AlertFilterModelWindowUtils.USER_ID, userID);
    }

    /**
     * @return the applicationALertID
     */
    public Long getApplicationALertID() {
        return getAttributeAsLong(AlertFilterModelWindowUtils.APPLICATION_ALERT_ID);
    }

    /**
     * @param applicationALertID the applicationALertID to set
     */
    public void setApplicationALertID(Long applicationALertID) {
        setAttribute(AlertFilterModelWindowUtils.APPLICATION_ALERT_ID, applicationALertID);
    }
   
    public String getStatus() {
        return getAttribute(AlertFilterModelWindowUtils.STATUS);
    }
    public void setStatus(String status) {
       setAttribute(AlertFilterModelWindowUtils.STATUS, status); 
    }
    
    public boolean getIsRoleBased() {
       return getAttributeAsBoolean(AlertFilterModelWindowUtils.IS_ROLE_BASED); 
    }
    
    public void setIsRoleBased(boolean isRoleBased) {
       setAttribute(AlertFilterModelWindowUtils.IS_ROLE_BASED, isRoleBased); 
    }
    
    public boolean getNoFiltering() {
       return getAttributeAsBoolean(AlertFilterModelWindowUtils.NO_FILTERING); 
    }
    
    public void setNoFiltering(boolean noFiltering) {
       setAttribute(AlertFilterModelWindowUtils.NO_FILTERING, noFiltering); 
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("alertTypeName: ");
        sb.append(this.getAlertTypeName());
        sb.append("\napp id: ");
        sb.append(this.getApplicationID());
        sb.append("\nalert id: ");
        sb.append(this.getApplicationALertID());
        sb.append("\ncheckValue ");
        sb.append(this.getCheckVaue());
        sb.append("\ncommunity id: ");
        sb.append(this.getCommunityID());
        sb.append("\nisRoleBased:  ");
        sb.append(this.getIsRoleBased());
        sb.append("\nseverity ");
        sb.append(this.getSeverity());
        sb.append("\nstatus ");
        sb.append(this.getStatus());
        sb.append("\nuserid: ");
        sb.append(this.getUserID());
        sb.append("\nnoFiltering: ");
        sb.append(this.getNoFiltering());
        
        return sb.toString();
    }
    
    
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + (this.getCommunityID() != null ? this.getCommunityID().hashCode() : 0);
        hash = 61 * hash + (this.getUserID() != null ? this.getUserID().hashCode() : 0);
        hash = 61 * hash + (this.getApplicationALertID() != null ? this.getApplicationALertID().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        
        if (!(obj instanceof AlertFilterListGridRecord)) {
            return false;
        }
        
        final AlertFilterListGridRecord other = (AlertFilterListGridRecord) obj;
        if((this.getCommunityID() == null) ? (other.getCommunityID() != null) : !this.getCommunityID().equals(other.getCommunityID())) {
            return false;
        }
        if((this.getUserID() == null) ? (other.getUserID() != null) : !this.getUserID().equals(other.getUserID())) {
            return false;
        }
        if((this.getApplicationALertID() == null) ? (other.getApplicationALertID() != null) : !this.getApplicationALertID().equals(other.getApplicationALertID())) {
            return false;
        }
        return true;
    }
    
    

    
}
