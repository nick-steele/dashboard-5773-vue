/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.preferences;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.callbacks.BasicPortalAsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.service.SubcriptionManagerAlertService;
import com.gsihealth.dashboard.client.service.SubcriptionManagerAlertServiceAsync;
import com.gsihealth.dashboard.entity.dto.AlertFilterPreferenceDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Autofit;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.grid.events.SelectionUpdatedEvent;
import com.smartgwt.client.widgets.grid.events.SelectionUpdatedHandler;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

/**
 *
 * @author Vinay
 */
public class AlertFilterModelWindow extends Window {

    private Logger logger = Logger.getLogger(getClass().getName());

    private ListGrid listGrid;
    private String alert;
    private Long userIdLong;
    private ProgressBarWindow progressBarWindow;

    private Set<AlertFilterListGridRecord> editedRecords;

    private static SubcriptionManagerAlertServiceAsync alertService = GWT.create(SubcriptionManagerAlertService.class);
    private Button saveButton;

    public AlertFilterModelWindow(String alert) {
        this.alert = alert;
        buildGui();
    }

    public AlertFilterModelWindow(String alert, long userId) {
        this.alert = alert;
        this.userIdLong = userId;
        buildGui();
    }

    private void buildGui() {
        this.setWindowProps();
        // build and add the list
        this.listGrid = this.buildListGrid();
        this.addItem(this.listGrid);

        // create a separator between the list and save/cancel buttons    
        LayoutSpacer topSpacer = new LayoutSpacer();
        topSpacer.setHeight(20);
        this.addItem(topSpacer);
        // build and add save and cancel buttons    
        Layout buttonBar = buildButtonBar();
        this.addItem(buttonBar);

        this.progressBarWindow = new ProgressBarWindow("loading", "Loading...");
    }

    private void setWindowProps() {
        this.setPadding(5);
        this.setMargin(10);
        this.setWidth(600);
        this.setHeight(420);
        this.setTitle(this.alert);
        this.setShowMinimizeButton(false);
        this.setShowMinimizeButton(false);
        this.setShowCloseButton(true);
        this.setShowMaximizeButton(false);
        this.setIsModal(true);
        this.setShowModalMask(true); // TODO needs to be set before this?
        this.centerInPage();
    }

    private ListGrid buildListGrid() {
        ListGrid newListGrid = new ListGrid();
        newListGrid.setHeight(300);
        newListGrid.setAutoFitData(Autofit.HORIZONTAL);
        newListGrid.setLeaveScrollbarGap(false);
        newListGrid.setShowAllRecords(false);
        newListGrid.setCanEdit(true);
        newListGrid.setSelectionType(SelectionStyle.SIMPLE);
        newListGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
        newListGrid.setSelectionProperty(AlertFilterModelWindowUtils.CHECK_VALUE);   // set the checkbox on loading to this attribute

        // add fields
        ListGridField[] fields = this.buildListGridFields();
        newListGrid.setFields(fields);
        newListGrid.getField(AlertFilterModelWindowUtils.SEVERITY).setWidth(AlertFilterModelWindowUtils.SEVERITY_COLUMN_SIZE);

        // add a handler to collect any changed records
        newListGrid.addSelectionUpdatedHandler(new AlertFilterSelectionUpdatedHandler());
        newListGrid.addSelectionChangedHandler(new AlertFilterSelectionChangedHandler());
        newListGrid.setEmptyMessage("No alerts available"); // TODO do we need this?
        newListGrid.setCanResizeFields(true); // TODO do we need this?

        return newListGrid;
    }

    /**
     * @return array of fields with name, title and editing set
     */
    private ListGridField[] buildListGridFields() {
        String[] columnNames = {AlertFilterModelWindowUtils.SEVERITY, AlertFilterModelWindowUtils.ALERT_TYPE};
        int numberOfColumns = columnNames.length;
        ListGridField[] fields = new ListGridField[numberOfColumns];
        for (int i = 0; i < numberOfColumns; i++) {
            ListGridField field = new ListGridField(columnNames[i], columnNames[i]);
            field.setCanEdit(false);
            field.setWidth("100%");
            fields[i] = field;
        }

        return fields;
    }

    /**
     * @return Save and Cancel button panel - save disabled
     */
    private HLayout buildButtonBar() {
        HLayout buttonBar = new HLayout(10);

        saveButton = new Button("Save");
        saveButton.setDisabled(true);
        Button cancelButton = new Button("Cancel");

        cancelButton.addClickHandler(new CancelButtonClickHandler());
        saveButton.addClickHandler(new SaveClickHandler());

        buttonBar.setWidth100();
        buttonBar.setAlign(Alignment.CENTER);
        buttonBar.addMember(saveButton);
        buttonBar.addMember(cancelButton);

        return buttonBar;
    }

    public void populateListGrid(ListGridRecord[] records) {
        listGrid.scrollToRow(1); //TODO do we need this?
        listGrid.setData(records);
        listGrid.markForRedraw();
    }

    /**
     * TODO this fires twice - using updatedHandler has no access to edited
     * records - save them all for now Collect all changed records for updating
     * and enable the save button
     */
    class AlertFilterSelectionChangedHandler implements SelectionChangedHandler {

        public void onSelectionChanged(SelectionEvent event) {
            if (editedRecords == null) {
                editedRecords = new HashSet<AlertFilterListGridRecord>();
            }
            saveButton.setDisabled(false);
//            AlertFilterListGridRecord aflgr = (AlertFilterListGridRecord)event.getRecord();
//            aflgr.setAttribute(AlertFilterModelWindowUtils.CHECK_VALUE, event.getState());
//            System.out.println(aflgr.getAlertTypeName() + " set to " + event.getState());
//            editedRecords.add(aflgr); 

        }
    }

    /** 
     * Collect all changed records for updating and enable the save button TODO
     */
    class AlertFilterSelectionUpdatedHandler implements SelectionUpdatedHandler {

        public void onSelectionUpdated(SelectionUpdatedEvent event) {
//            if (editedRecords == null) {
//                editedRecords = new HashSet<AlertFilterListGridRecord>();
//            }
           // saveButton.setDisabled(false);

        }
    }

    class CancelButtonClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            hide();
            destroy();
        }
    }

    /**
     * save any edited records
     */
    class SaveClickHandler implements ClickHandler {

        public void onClick(ClickEvent clickEvent) {

            ProgressBarWindow progressBar = new ProgressBarWindow();
            progressBar.show();
            String confirmationMessage = "Save successful.";
            List<AlertFilterPreferenceDTO> alertFilterRefDataDTOList = AlertFilterModelWindowUtils.convertListGridToDTOlist(listGrid);

            alertService.updateUserAlertFilter(alertFilterRefDataDTOList, new BasicPortalAsyncCallback(progressBar, confirmationMessage));

            hide();
            destroy();

        }
    }
}
