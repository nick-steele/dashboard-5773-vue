package com.gsihealth.dashboard.client.preferences;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.admintools.user.UpdateUserContextEvent;
import com.gsihealth.dashboard.client.callbacks.AlertFilterPreferenceCallback;
import com.gsihealth.dashboard.client.callbacks.BasicPortalAsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.eventbus.EventBusUtils;
import com.gsihealth.dashboard.client.service.AdminService;
import com.gsihealth.dashboard.client.service.AdminServiceAsync;
import com.gsihealth.dashboard.client.service.SubcriptionManagerAlertService;
import com.gsihealth.dashboard.client.service.SubcriptionManagerAlertServiceAsync;
import com.gsihealth.dashboard.client.util.*;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.entity.dto.AlertPreferenceDTO;
import com.gsihealth.dashboard.entity.dto.ApplicationDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.gsihealth.dashboard.redux.Redux;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.ListGridFieldType;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.CellFormatter;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Chad Darby
 */
public class PreferencesWindow extends Window {

    private PreferencesForm preferencesForm;
    private AdminServiceAsync adminService = GWT.create(AdminService.class);
    private Label label;
    private ListGrid alertListGrid;
    private static final int ICON_COLUMN = 0;
    //private static final int ALERTDESC_COLUMN = 1;
    private static SubcriptionManagerAlertServiceAsync alertService = GWT.create(SubcriptionManagerAlertService.class);
    private static final String PREFERENCE = "Preferences";
    private static final String USER_PREFERENCE = "User Preferences";
    private static final String ALERT_PREFERENCE = "Alert Preferences";
    private static final String SELECT_ALERT_PREFERENCE = "<b>Select Alert Category</b>";
    private static long userIdLong;
    private static Long communityId;
    private boolean enableClientMfa;   
 

    public PreferencesWindow(String theEmail) {
        communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
        setVariablesFromClientApplicationProperties();
        buildGui();
        populateWithUser(theEmail);

    }

    private void buildGui() {

        boolean alertTabEnable = false;

        final TabSet topTabSet = new TabSet();
        topTabSet.setWidth(940);
        topTabSet.setHeight(590);

        final Tab userPreferencesTab = new Tab(USER_PREFERENCE);

        VLayout topUserlayout = new VLayout();

        VLayout userlayout = new VLayout();
        preferencesForm = new PreferencesForm();
        userlayout.addMember(preferencesForm);

        VLayout buttonlayout = new VLayout();
        Canvas buttonBar = buildButtonBar();
        buttonlayout.addMember(buttonBar);

        topUserlayout.addMember(userlayout);
        topUserlayout.addMember(buttonlayout);
        userPreferencesTab.setPane(topUserlayout);

        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        List<ApplicationDTO> userApps = loginResult.getUserApps();

        alertTabEnable = getAlertTabEnable(userApps, alertTabEnable);

        Tab alertTab = new Tab(ALERT_PREFERENCE);
        VLayout topAlertlayout = new VLayout();

        VLayout labelLayout = new VLayout();
        label = new Label();

        label.setContents(SELECT_ALERT_PREFERENCE);
        label.setHeight(50);
        label.setLeft(40);
        label.setAlign(Alignment.CENTER);
        labelLayout.setHeight(40);
        labelLayout.setLeft(40);
        labelLayout.setAlign(Alignment.CENTER);
        labelLayout.addMember(label);

        topAlertlayout.addMember(labelLayout);

        VLayout alertlayout = new VLayout();
        Canvas alertCanvas = buildListGridLayout();
        DynamicForm alertForm = new DynamicForm();

        alertForm.addChild(alertCanvas);
        alertlayout.addMember(alertForm);

        topAlertlayout.addMember(alertlayout);
        //alert tab
        alertTab.setPane(topAlertlayout);

        //Top tab
        topTabSet.addTab(userPreferencesTab);

        if (alertTabEnable) {
            topTabSet.addTab(alertTab);
        }


        //Top layout
        VLayout layout = new VLayout();
        layout.setMembersMargin(10);
        setWidth(955);
        setHeight(630);
        setTitle(PREFERENCE);
        setShowMinimizeButton(false);
        setShowCloseButton(true);
        setShowMaximizeButton(false);

        setIsModal(true);
        setShowModalMask(true);
        centerInPage();
        layout.addMember(topTabSet);

        addItem(layout);
    }

    private Canvas buildListGridLayout() {
        HStack listGridLayout = new HStack();
        listGridLayout.setPadding(5);

        alertListGrid = buildListGrid();

        VLayout alertLayout = createListGridLayout(alertListGrid);
        listGridLayout.addMember(alertLayout);

        return listGridLayout;
    }

    private ListGrid buildListGrid() {

        String[] columnNames = {AlertPreferenceSearchData.ALERT_ICON};
        ListGrid theListGrid = new ListGrid();

        theListGrid.setWidth(900);
        theListGrid.setHeight(450);
        theListGrid.setCellHeight(60);

        ListGridField[] fields = buildListGridFields(columnNames);
        theListGrid.setFields(fields);

        ListGridField iconField = fields[ICON_COLUMN];
        iconField.setShowTitle(false);
        iconField.setWidth(890);




        iconField.setAlign(Alignment.LEFT);
        iconField.setCellFormatter(new ImageTextCellFormatter());
        iconField.setType(ListGridFieldType.ICON);


//        ListGridField alertDescField = fields[ALERTDESC_COLUMN];
//        alertDescField.setWidth(790);
//        alertDescField.setAlign(Alignment.LEFT);
//        alertDescField.setShowTitle(false);

        iconField.addRecordClickHandler(new AlertFilterSelectionClickHandler());
        return theListGrid;
    }

    class ImageTextCellFormatter implements CellFormatter {

        public String format(Object value, ListGridRecord record, int rowNum, int colNum) {
            AlertPreferenceSearchData alertRecord = (AlertPreferenceSearchData) record;
            return "<img src='images/32x32/" + alertRecord.getAlertIcon()
                    + "' style='padding:5px;vertical-align:middle;' />"
                    + "<span style='margin-left:10px;'><b>" + alertRecord.getAlertName() + "</b></span>";
        }
    }

    private VLayout createListGridLayout(ListGrid theListGrid) {
        VLayout theLayout = new VLayout();

        theListGrid.setWidth(900);
        theLayout.addMember(theListGrid);

        return theLayout;
    }

    private ListGridField[] buildListGridFields(String[] columnNames) {

        ListGridField[] fields = new ListGridField[columnNames.length];

        for (int i = 0; i < columnNames.length; i++) {
            String tempColumnName = columnNames[i];
            ListGridField tempField = buildListGridField(tempColumnName);

            fields[i] = tempField;
        }

        return fields;
    }

    private ListGridField buildListGridField(String title) {
        ListGridField tempField = new ListGridField(title, title);

        tempField.setCanEdit(false);



        return tempField;
    }

    public void populate(List<AlertPreferenceDTO> users) {

        ListGridRecord[] docRecords = convert(users);

        alertListGrid.setData(docRecords);
        alertListGrid.markForRedraw();
    }

    private ListGridRecord[] convert(List<AlertPreferenceDTO> docs) {

        ListGridRecord[] records = new ListGridRecord[docs.size()];

        // convert dtos to records
        for (int index = 0; index < docs.size(); index++) {
            AlertPreferenceDTO tempDoc = docs.get(index);
            records[index] = PropertyUtils.convertAlertData(index, tempDoc);
        }

        return records;
    }

    class AlertFilterSelectionClickHandler implements com.smartgwt.client.widgets.grid.events.RecordClickHandler {

        public void onRecordClick(RecordClickEvent event) {

            LoginResult loginResult = ApplicationContextUtils.getLoginResult();
            userIdLong = loginResult.getUserId();

            AlertPreferenceSearchData alertRecord = (AlertPreferenceSearchData) event.getRecord();
            Long communityID = alertRecord.getCommunityID();
            AlertPreferenceDTO apdto = new AlertPreferenceDTO();
            apdto.setApplicationID(alertRecord.getApplicationID());
            apdto.setCommunityID(communityID);
            apdto.setStatus(alertRecord.getStatus());
            apdto.setUserLevelId(loginResult.getUserLevelId());
            apdto.setAccessLevelID(loginResult.getAccessLevelId());

            AlertFilterPreferenceCallback alertFilterSelectionCallback = new AlertFilterPreferenceCallback(alertRecord.getAlertName(), userIdLong); //FIXME I changed this
            alertService.getAlertFilterSelections(userIdLong, apdto, communityId, alertFilterSelectionCallback);
        }
    }

    private HLayout buildButtonBar() {
        Button saveButton = new Button("Save");
        saveButton.setID("Save");
        saveButton.addClickHandler(new SaveButtonClickHandler());

        Button cancelButton = new Button("Cancel");
        cancelButton.setID("Cancel");
        cancelButton.addClickHandler(new CancelButtonClickHandler());

        HLayout buttonBar = new HLayout(10);
        buttonBar.setWidth100();
        buttonBar.setAlign(Alignment.CENTER);
        buttonBar.addMember(saveButton);
        buttonBar.addMember(cancelButton);

        return buttonBar;
    }

    private void populateWithUser(String theEmail) {
        adminService.getUser(theEmail, communityId, new GetUserCallback());
    }

    class GetUserCallback implements AsyncCallback<UserDTO> {

        public void onFailure(Throwable thrwbl) {
            SC.warn("Error loading user preferences.");
        }

        public void onSuccess(UserDTO theUserDTO) {
            preferencesForm.populate(theUserDTO);
            // load user apps
            loadUserApps(theUserDTO);
        }
    }

    class SaveButtonClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {

            if (preferencesForm.validate()) {

                UserDTO theUserDTO = preferencesForm.getUserDTO();

                theUserDTO.setOrganization(theUserDTO.getOrgid());

                boolean blankPassword = StringUtils.isBlank(theUserDTO.getPassword());
                boolean blankConfirmPassword = StringUtils.isBlank(theUserDTO.getConfirmPassword());

                boolean passwordNotSet = blankPassword && blankConfirmPassword;
                theUserDTO.setPasswordNotSet(passwordNotSet);
              
                ProgressBarWindow progressBarWindow = new ProgressBarWindow();
                progressBarWindow.show();
               
                if(enableClientMfa && (theUserDTO.getMobileNo()==null)){
                    progressBarWindow.hide();
                     SC.warn("Mobile phone number required");
                }

                else{

                SaveCallback saveCallback = new SaveCallback(theUserDTO, progressBarWindow);
                saveCallback.attempt();
            }}
        }
    }

    class SaveCallback extends RetryActionCallback<String> {

        private UserDTO userDTO;
        private ProgressBarWindow progressBarWindow;

        public SaveCallback(UserDTO theUserDTO, ProgressBarWindow theProgressBarWindow) {
            userDTO = theUserDTO;
            progressBarWindow = theProgressBarWindow;
        }

        @Override
        public void attempt() {
            long communityId=ApplicationContextUtils.getLoginResult().getCommunityId();               
            List<String> userApps = preferencesForm.getSelectedApps();
            adminService.updateUserPreferences(userDTO, userApps, communityId,this);
        }

        @Override
        public void onCapture(String changePwdToken) {

            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            if(null != changePwdToken) {
                Redux.jsEvent("local:login:syncTokenFromGWT", changePwdToken);
            }
            
            SC.say("Save successful.");

            UserDTO theUserDTO = preferencesForm.getUserDTO();

            String firstName = theUserDTO.getFirstName();
            String lastName = theUserDTO.getLastName();

            // update UI header
            DashBoardApp.setUserName(firstName, lastName);

            // update login result
            LoginResult loginResult = ApplicationContextUtils.getLoginResult();
            loginResult.setFirstName(firstName);
            loginResult.setLastName(lastName);

            //After updation of users update user in Application contexts for client and server.
            adminService.updateUserContexts(ApplicationContextUtils.getLoginResult().getCommunityId() , new AsyncCallback<List<UserDTO>>(){

                @Override
                public void onSuccess(List<UserDTO> result){
                    ApplicationContext context = ApplicationContext.getInstance();
                    context.putAttribute(Constants.PROVIDERS_FOR_CONSENT_OBTAINED_BY, result);
                    // fire event
                    EventBus eventBus = EventBusUtils.getEventBus();
                    eventBus.fireEvent(new UpdateUserContextEvent());
                }

                @Override
                public void onFailure(Throwable th){

                }
            });
        }
    }

    private void loadUserApps(UserDTO theUserDTO) {
        GetUserAppsCallback getUserAppsCallback = new GetUserAppsCallback(theUserDTO);
        getUserAppsCallback.attempt();
    }

    class GetUserAppsCallback extends RetryActionCallback<List<String>> {

        private UserDTO userDTO;

        public GetUserAppsCallback(UserDTO theUserDTO) {
            userDTO = theUserDTO;
        }

        @Override
        public void attempt() {
            adminService.getUserApps_Legacy(userDTO, this);
        }

        @Override
        public void onCapture(List<String> userApps) {
            preferencesForm.populateUserApps(userApps);
        }
    }

    class CancelButtonClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            hide();
            destroy();
        }
    }

    static class OpenLightBoxClickHandler implements com.smartgwt.client.widgets.form.fields.events.ClickHandler {

        @Override
        public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
            LoginResult loginResult = ApplicationContextUtils.getLoginResult();

            String email = loginResult.getEmail();
            // Long userId=loginResult.getUserId();
            AlertFilterModelWindow prefsWindow = new AlertFilterModelWindow(email);
            prefsWindow.show();
        }
    }

    //check application id for Alert tab Enable/Disable
    private boolean getAlertTabEnable(List<ApplicationDTO> userApps, boolean alertTabEnable) {

        ListIterator<ApplicationDTO> litr = userApps.listIterator();
        while (litr.hasNext()) {
            ApplicationDTO tO = (ApplicationDTO) litr.next();

            if (tO.getApplicationId() == 9) {
                alertTabEnable = true;
            }
        }
        return alertTabEnable;
    }
    
     public void setVariablesFromClientApplicationProperties(){
        ClientApplicationProperties props=ApplicationContextUtils.getClientApplicationProperties();
        enableClientMfa=props.isEnableClientMFA();
              
    }
}
