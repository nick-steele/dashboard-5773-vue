package com.gsihealth.dashboard.client.preferences;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.gsihealth.dashboard.client.admintools.user.SupervisorForm;
import com.gsihealth.dashboard.client.common.FormUtils;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.PasswordStrengthValidator;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.client.util.ValidationUtils;
import com.gsihealth.dashboard.common.AppKeyConstants;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.PasswordItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.smartgwt.client.widgets.form.validator.DateRangeValidator;
import com.smartgwt.client.widgets.form.validator.LengthRangeValidator;
import com.smartgwt.client.widgets.form.validator.MatchesFieldValidator;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.form.validator.Validator;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class PreferencesForm extends HLayout {

    private static final int FIELD_WIDTH = 320;
    private DynamicForm contactForm;
    private DynamicForm securityForm;
    private TextItem firstNameTextItem;
    private TextItem middleNameTextItem;
    private TextItem lastNameTextItem;
    private SelectItem genderSelectItem;
    private TextItem workPhoneTextItem;
    private SelectItem stateSelectItem;
    private TextItem cityTextItem;
    private TextItem zipCodeTextItem;
    private DateItem dateOfBirthDateItem;
    private TextItem streetAddress1TextItem;
    private TextItem streetAddress2TextItem;
    private SelectItem userPrefixSelectItem;
    private SelectItem userCredentialSelectItem;
    private TextItem emailTextItem;
    private PasswordItem passwordItem;
    private PasswordItem confirmPasswordItem;
    private UserDTO userDTO;
    private CheckboxItem intraCommunityCheckBox;
    private DynamicForm serviceSubscriptionForm;
    private DateTimeFormat dateFormatter;
    private TextItem mobileTextItem;
    private boolean  enableClientMfa;
    private TextItem workPhoneExtTextItem,countryCodeTextItem;
    private SupervisorForm supervisorForm;

    public PreferencesForm() {
        dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
        ClientApplicationProperties props=ApplicationContextUtils.getClientApplicationProperties();
        enableClientMfa=props.isEnableClientMFA();
        buildGui();

        loadGender();
        loadUserCredentials();
        loadUserPrefixes();
    }

    private void buildGui() {
        
        setPadding(10);
        VLayout layout = new VLayout();
        if(enableClientMfa){
        Label labelNote = new Label("<font color='red'>This site uses 2-Factor Authentication requires a user to have a mobile phone that is used for registration.</font>");
        labelNote.setWidth(300);
        labelNote.setHeight(30);
        layout.addMember(labelNote);
         }
        // add the main form
        contactForm = buildContactForm();
        contactForm.setPadding(5);
        layout.addMember(contactForm);

        // add spacer
        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setWidth(50);
        layout.addMember(spacer);
        addMember(layout);
        
        VLayout vLayout = new VLayout();
        if(enableClientMfa){
          LayoutSpacer spacer1 = new LayoutSpacer();
          spacer1.setHeight(30);
          vLayout.addMember(spacer1);
         }
        securityForm = buildSecurityForm();
        vLayout.addMember(securityForm);

        LayoutSpacer vlayoutSpacer = new LayoutSpacer();
        vlayoutSpacer.setHeight(15);
        vLayout.addMember(vlayoutSpacer);

        serviceSubscriptionForm = buildServiceSubscriptionForm();

        if (intraHHCommunityFieldVisible()) {
            vLayout.addMember(serviceSubscriptionForm);
        }

        vLayout.addMember(vlayoutSpacer);
        supervisorForm = new SupervisorForm(430, 328);
        vLayout.addMember(supervisorForm);

        addMember(vLayout);
    }


    /**
     * Build the contact form
     *
     * @return
     */
    private DynamicForm buildContactForm() {

        RegExpValidator alphaNumberSpecialSymbolsRegExpValidator = ValidationUtils.getAlphaNumberSpecialSymbolsRegExpValidator();
        RegExpValidator alphaOnlyRegExpValidator = ValidationUtils.getAlphaOnlyRegExpValidator();
        RegExpValidator numbetRegExValidator=ValidationUtils.getNumbersOnlyRegExpValidator();

        DynamicForm tempForm = new DynamicForm();
        tempForm.setIsGroup(true);
        tempForm.setWidth(200);
        tempForm.setWrapItemTitles(false);
        tempForm.setCellPadding(5);
        tempForm.setNumCols(4);
        tempForm.setGroupTitle("User Information");

        userPrefixSelectItem = buildSelectItem("Prefix", false);
        userPrefixSelectItem.setColSpan(4);

        firstNameTextItem = buildTextItem("First Name", true);
        ValidationUtils.setValidators(firstNameTextItem, alphaOnlyRegExpValidator, true);
        firstNameTextItem.setColSpan(4);

        middleNameTextItem = buildTextItem("Middle Name", false);
        ValidationUtils.setValidators(middleNameTextItem, alphaOnlyRegExpValidator, false);
        middleNameTextItem.setColSpan(4);

        lastNameTextItem = buildTextItem("Last Name", true);
        ValidationUtils.setValidators(lastNameTextItem, alphaOnlyRegExpValidator, true);
        lastNameTextItem.setColSpan(4);

        genderSelectItem = buildSelectItem("Gender", true);
        genderSelectItem.setValidateOnExit(false);
        genderSelectItem.setColSpan(4);

        dateOfBirthDateItem = buildDateItem("Date of Birth", true);
        dateOfBirthDateItem.setValidateOnExit(false);
        dateOfBirthDateItem.setColSpan(4);

        // add date validation rules
        DateRangeValidator dateRangeValidator = new DateRangeValidator();
        Date today = new java.util.Date();
        dateRangeValidator.setMax(today);

        dateRangeValidator.setErrorMessage("Date must be today's date or prior.");
        dateOfBirthDateItem.setValidators(dateRangeValidator);
        dateOfBirthDateItem.setValidateOnChange(true);
        
        Date before = new java.util.Date(-59,00,01);
        dateRangeValidator.setMin(before);
        dateRangeValidator.setErrorMessage("Date must be between 31st Dec 1840 and todays date");

        streetAddress1TextItem = buildTextItem("Address 1", true);
        ValidationUtils.setValidators(streetAddress1TextItem, alphaNumberSpecialSymbolsRegExpValidator, true);
        streetAddress1TextItem.setColSpan(4);

        streetAddress2TextItem = buildTextItem("Address 2", false);
        ValidationUtils.setValidators(streetAddress2TextItem, alphaNumberSpecialSymbolsRegExpValidator, true);
        streetAddress2TextItem.setColSpan(4);

        cityTextItem = buildTextItem("City", true);
        ValidationUtils.setValidators(cityTextItem, alphaOnlyRegExpValidator, true);
        cityTextItem.setColSpan(4);

        stateSelectItem = buildSelectItem("State", true);

        stateSelectItem.setValueMap(ApplicationContextUtils.getStateCodes());
        stateSelectItem.setValidateOnExit(false);
        stateSelectItem.setDefaultValue("NY");
        stateSelectItem.setColSpan(4);

        zipCodeTextItem = buildTextItem("Zip Code", true);
        zipCodeTextItem.setValidateOnExit(true);
        zipCodeTextItem.setWidth(100);
        String zipCodeMask = "#####";
        zipCodeTextItem.setMask(zipCodeMask);
        LengthRangeValidator zipCodeValidator = new LengthRangeValidator();
        zipCodeValidator.setMax(5);
        zipCodeValidator.setMin(5);
        zipCodeValidator.setErrorMessage("Invalid length");
        zipCodeTextItem.setValidators(zipCodeValidator);
        zipCodeTextItem.setEndRow(true);

        workPhoneTextItem = buildTextItem("Telephone", false);
        workPhoneTextItem.setValidateOnExit(true);
        workPhoneTextItem.setWidth(100);
        workPhoneTextItem.setMask("###-###-####");
        LengthRangeValidator workPhoneValidator = new LengthRangeValidator();
        workPhoneValidator.setMax(10);
        workPhoneValidator.setMin(10);
        workPhoneValidator.setErrorMessage("Invalid length");
        workPhoneTextItem.setValidators(workPhoneValidator);
        
        workPhoneExtTextItem = buildTextItem("Ext", false);
        workPhoneExtTextItem.setValidateOnExit(true);
        workPhoneExtTextItem.setWidth(100);
        workPhoneExtTextItem.setRequired(false);
        workPhoneExtTextItem.setEndRow(true);
        LengthRangeValidator workPhoneExtValidator = new LengthRangeValidator();
        workPhoneExtTextItem.setMask("######");
        workPhoneExtValidator.setMax(6);
        workPhoneExtValidator.setMin(1);
        workPhoneExtValidator.setErrorMessage("Invalid length");
        workPhoneExtTextItem.setValidators(workPhoneExtValidator);
        
        mobileTextItem = buildTextItem("Mobile No.", false);
        mobileTextItem.setValidateOnExit(true);
        mobileTextItem.setWidth(100);
        mobileTextItem.setMask("###-###-####");
        LengthRangeValidator mobilePhoneValidator = new LengthRangeValidator();
        workPhoneValidator.setMax(10);
        workPhoneValidator.setMin(10);
        workPhoneValidator.setErrorMessage("Invalid length");
        mobileTextItem.setValidators(workPhoneValidator);
        
        countryCodeTextItem = buildTextItem("Country Code", false);
        ValidationUtils.setValidators(countryCodeTextItem, numbetRegExValidator, true);
        countryCodeTextItem.setValidateOnExit(true);
        countryCodeTextItem.setWidth(60);
        countryCodeTextItem.setEndRow(false);

        userCredentialSelectItem = buildSelectItem("Credential", true);
        userCredentialSelectItem.setColSpan(4);

        tempForm.setFields(userPrefixSelectItem, firstNameTextItem, middleNameTextItem, lastNameTextItem, userCredentialSelectItem, genderSelectItem, dateOfBirthDateItem, streetAddress1TextItem, streetAddress2TextItem, cityTextItem, stateSelectItem, zipCodeTextItem, workPhoneTextItem,workPhoneExtTextItem,countryCodeTextItem,mobileTextItem);

        return tempForm;
    }

    /**
     * Build the security form
     *
     * @return
     */
    private DynamicForm buildSecurityForm() {
        DynamicForm tempForm = new DynamicForm();

        tempForm.setIsGroup(true);
        tempForm.setGroupTitle("Security");
        tempForm.setWidth(200);
        tempForm.setHeight(100);
        tempForm.setWrapItemTitles(false);
        tempForm.setCellPadding(5);
        tempForm.setNumCols(2);

        emailTextItem = buildTextItem("User ID/Email", true);
        emailTextItem.setDisabled(true);

        LengthRangeValidator emailValidator = new LengthRangeValidator();
        emailValidator.setMax(50);
        emailValidator.setErrorMessage("Invalid length");
        emailTextItem.setValidators(emailValidator);

        passwordItem = buildPasswordItem("Password", false);
        passwordItem.setName("dashboard_password");
        Validator passwordStrengthValidator = new PasswordStrengthValidator();
        passwordItem.setValidateOnExit(true);
        passwordItem.setValidators(passwordStrengthValidator);
        passwordItem.addChangeHandler(new PasswordChangeHandler());

        confirmPasswordItem = buildPasswordItem("Confirm Password", false);
        MatchesFieldValidator matchValidator = new MatchesFieldValidator();
        matchValidator.setOtherField("dashboard_password"); // refs back to orig password field
        matchValidator.setErrorMessage("Passwords do not match.");
        confirmPasswordItem.setValidators(matchValidator);
        confirmPasswordItem.setValidateOnExit(true);

        tempForm.setFields(emailTextItem, passwordItem, confirmPasswordItem);

        return tempForm;
    }

    /**
     * Build the Service Subscription form
     *
     * @return
     */
    private DynamicForm buildServiceSubscriptionForm() {
        DynamicForm tempForm = new DynamicForm();

        tempForm.setIsGroup(true);
        tempForm.setGroupTitle("Service Subscription ");
        tempForm.setWidth(350);
        tempForm.setHeight(50);
        tempForm.setWrapItemTitles(false);
        tempForm.setCellPadding(5);
        tempForm.setNumCols(2);
        tempForm.setRequiredTitlePrefix(FormUtils.REQUIRED_TITLE_PREFIX);

        intraCommunityCheckBox = buildCheckBox(Constants.DIRECT_SECURED_MESSAGING, false);
        intraCommunityCheckBox.setColSpan(5);
        tempForm.setFields(intraCommunityCheckBox);

        return tempForm;
    }

    public void populate(UserDTO theUserDTO) {

        userDTO = theUserDTO;

        // demographic fields
        firstNameTextItem.setValue(theUserDTO.getFirstName());
        middleNameTextItem.setValue(theUserDTO.getMiddleName());
        lastNameTextItem.setValue(theUserDTO.getLastName());
        genderSelectItem.setValue(theUserDTO.getGender());
        dateOfBirthDateItem.setValue(theUserDTO.getDateOfBirth());
        streetAddress1TextItem.setValue(theUserDTO.getStreetAddress1());
        streetAddress2TextItem.setValue(theUserDTO.getStreetAddress2());
        cityTextItem.setValue(theUserDTO.getCity());
        stateSelectItem.setValue(theUserDTO.getState());
        zipCodeTextItem.setValue(theUserDTO.getZipCode());
        workPhoneTextItem.setValue(theUserDTO.getWorkPhone());
        workPhoneExtTextItem.setValue(theUserDTO.getWorkPhoneExt());

        populateMobileNo(theUserDTO);
        emailTextItem.setValue(theUserDTO.getEmail());
        passwordItem.setValue("");
        confirmPasswordItem.setValue("");

        userPrefixSelectItem.setValue(theUserDTO.getPrefix());
        userCredentialSelectItem.setValue(theUserDTO.getCredentials());
        supervisorForm.populateSupervisorSelectItem(theUserDTO.getOrgid(), theUserDTO.getSupervisorId(), theUserDTO.getUserId());
    }

    private void populateMobileNo(UserDTO userDto) {
        String number = userDto.getMobileNo();
        String countryCode=null;
        if (number != null && number.length()!=10) {
            String mobileNo = number.substring(number.length() - 10, number.length());
            countryCode = number.replace(mobileNo, "");
            mobileTextItem.setValue(mobileNo);
            countryCodeTextItem.setValue(countryCode);
        } else {
            String code=null;
            mobileTextItem.setValue(number);
            countryCodeTextItem.setValue(code);
        }

    }
    /**
     * Read the form data
     *
     * @return the user
     */
    public UserDTO getUserDTO() {

        // read from form fields
        String firstName = StringUtils.trim(firstNameTextItem.getValueAsString());
        String middleName = StringUtils.trim(middleNameTextItem.getValueAsString());
        String lastName = StringUtils.trim(lastNameTextItem.getValueAsString());

        String gender = genderSelectItem.getValueAsString();
        Date dateOfBirth = dateOfBirthDateItem.getValueAsDate();

        String streetAddress1 = StringUtils.trim(streetAddress1TextItem.getValueAsString());
        String streetAddress2 = StringUtils.trim(streetAddress2TextItem.getValueAsString());
        String city = StringUtils.trim(cityTextItem.getValueAsString());
        String state = stateSelectItem.getValueAsString();

        String zipCode = StringUtils.trim(zipCodeTextItem.getValueAsString());
        String workPhone = StringUtils.trim(workPhoneTextItem.getValueAsString());
        String workPhoneExt=StringUtils.trim(workPhoneExtTextItem.getValueAsString());

        String email = StringUtils.trim(emailTextItem.getValueAsString());
        String password = passwordItem.getValueAsString();
        String confirmPassword = confirmPasswordItem.getValueAsString();

        String credentials = userCredentialSelectItem.getValueAsString();
        String prefix = userPrefixSelectItem.getValueAsString();
        String mobileNo=mobileTextItem.getValueAsString();
        String countryCode=countryCodeTextItem.getValueAsString();

        userDTO.setFirstName(firstName);
        userDTO.setMiddleName(middleName);
        userDTO.setLastName(lastName);

        userDTO.setGender(gender);
        userDTO.setDateOfBirth(dateOfBirth);

        userDTO.setStreetAddress1(streetAddress1);
        userDTO.setStreetAddress2(streetAddress2);
        userDTO.setCity(city);
        userDTO.setState(state);
        userDTO.setZipCode(zipCode);
        userDTO.setWorkPhone(workPhone);
        userDTO.setWorkPhoneExt(workPhoneExt);
        userDTO.setMobileNo(mobileNo);
        userDTO.setCountryCode(countryCode);

        userDTO.setEmail(email);
        userDTO.setPassword(password);
        userDTO.setConfirmPassword(confirmPassword);

        userDTO.setCredentials(credentials);
        userDTO.setPrefix(prefix);

        userDTO.setSupervisorId(supervisorForm.getSelectedSupervisor());
        return userDTO;
    }

    /**
     * Helper method to validate the form
     */
    public boolean validate() {
        boolean flag = contactForm.validate() && securityForm.validate();
        return flag;
    }

    private void loadGender() {
        LinkedHashMap<String, String> genderMap = ApplicationContextUtils.getGenderCodes();
        genderSelectItem.setValueMap(genderMap);
    }

    private void loadUserCredentials() {
        String[] codes = ApplicationContextUtils.getUserCredentialCodes();
        userCredentialSelectItem.setValueMap(codes);
    }

    private void loadUserPrefixes() {
        String[] codes = ApplicationContextUtils.getUserPrefixCodes();
        userPrefixSelectItem.setValueMap(codes);
    }

    private TextItem buildTextItem(String title, boolean required) {
        TextItem item = new TextItem();

        item.setTitle(title);
        item.setRequired(required);
        item.setWidth(FIELD_WIDTH);

        return item;
    }

    private SelectItem buildSelectItem(String title, boolean required) {
        SelectItem tempSelectItem = new SelectItem();

        tempSelectItem.setTitle(title);
        tempSelectItem.setRequired(required);
        tempSelectItem.setWidth(FIELD_WIDTH);
        if(null != title && !title.equals("State")){
            tempSelectItem.setAnimatePickList(true);
            tempSelectItem.setAnimationTime(600);
        }
        tempSelectItem.setColSpan(4);

        return tempSelectItem;
    }

    private DateItem buildDateItem(String title, boolean required) {
        DateItem dateItem = new DateItem();

        dateItem.setTitle(title);
        dateItem.setRequired(required);

        dateItem.setTextAlign(Alignment.LEFT);
        dateItem.setUseMask(true);
        dateItem.setEnforceDate(true);
        dateItem.setDisplayFormat(DateDisplayFormat.TOUSSHORTDATE);
        dateItem.setWidth(120);

        dateItem.setStartDate(dateFormatter.parse("01/01/1900"));

        return dateItem;
    }

    private CheckboxItem buildCheckBox(String title, boolean requiredFlag) {
        CheckboxItem tempCheckBox = new CheckboxItem();
        tempCheckBox.setRequired(requiredFlag);
        tempCheckBox.setTitle(title);
        tempCheckBox.setWidth(415);
        return tempCheckBox;
    }

    private PasswordItem buildPasswordItem(String title, boolean requiredFlag) {
        PasswordItem thePasswordItem = new PasswordItem();
        thePasswordItem.setTitle(title);
        thePasswordItem.setWidth(FIELD_WIDTH);
        thePasswordItem.setRequired(requiredFlag);
        return thePasswordItem;
    }

    class PasswordChangeHandler implements ChangeHandler {

        public void onChange(ChangeEvent event) {
            confirmPasswordItem.clearValue();
        }
    }

    public List<String> getSelectedApps() {
        List<String> selectedApps = new ArrayList<String>();

        boolean selected = intraCommunityCheckBox.getValueAsBoolean();

        if (selected) {
            selectedApps.add(AppKeyConstants.MESSAGING_APP_KEY);
        }

        return selectedApps;
    }

    public void populateUserApps(List<String> userApps) {

        if (userApps != null && !userApps.isEmpty()) {
            boolean messageAppChecked = userApps.contains(AppKeyConstants.MESSAGING_APP_KEY);

            intraCommunityCheckBox.setValue(messageAppChecked);

            // disable the messaging app if it has already been selected
            intraCommunityCheckBox.setDisabled(messageAppChecked);
        }
    }

    private boolean intraHHCommunityFieldVisible() {

        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();

        return props.isIntraHHComunityFieldVisible();
    }

}
