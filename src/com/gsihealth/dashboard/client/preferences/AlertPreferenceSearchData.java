/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.preferences;

import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 *
 * @author Vinay
 */
public class AlertPreferenceSearchData extends ListGridRecord {

    private int index;
    public static String ALERT_ICON = "Icon";
    public static String ALERT_NAME = "Alert Name";
    private Long  applicationID;
    private Long  alertTypeID;
    private Long applicationAlertID;
    private boolean isRoleBase;
    private String status;
    private Long communityID;

    public AlertPreferenceSearchData() {
    }

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

    
    public String getAlertIcon() {
        return getAttribute(ALERT_ICON);
    }

    public void setAlertIcon(String docName) {
        setAttribute(ALERT_ICON, docName);
    }

     public String getAlertName() {
        return getAttribute(ALERT_NAME);
    }

    public void setAlertName(String name) {
        setAttribute(ALERT_NAME, name);
    }
    


    /**
     * @return the applicationID
     */
    public Long getApplicationID() {
        return applicationID;
    }

    /**
     * @param applicationID the applicationID to set
     */
    public void setApplicationID(Long applicationID) {
        this.applicationID = applicationID;
    }

    /**
     * @return the alertTypeID
     */
    public Long getAlertTypeID() {
        return alertTypeID;
    }

    /**
     * @param alertTypeID the alertTypeID to set
     */
    public void setAlertTypeID(Long alertTypeID) {
        this.alertTypeID = alertTypeID;
    }

    /**
     * @return the applicationAlertID
     */
    public Long getApplicationAlertID() {
        return applicationAlertID;
    }

    /**
     * @param applicationAlertID the applicationAlertID to set
     */
    public void setApplicationAlertID(Long applicationAlertID) {
        this.applicationAlertID = applicationAlertID;
    }

    /**
     * @return the isRoleBase
     */
    public boolean getIsRoleBase() {
        return isRoleBase;
    }

    /**
     * @param isRoleBase the isRoleBase to set
     */
    public void setIsRoleBase(boolean isRoleBase) {
        this.isRoleBase = isRoleBase;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the communityID
     */
    public Long getCommunityID() {
        return communityID;
    }

    /**
     * @param communityID the communityID to set
     */
    public void setCommunityID(Long communityID) {
        this.communityID = communityID;
    }
}
