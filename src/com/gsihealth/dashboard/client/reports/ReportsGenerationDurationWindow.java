/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.reports;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.smartgwt.client.widgets.Window;

/**
 * This class will be used to open a Window on Patient tracking sheet click. It
 * will provide a date range to get sheet with specified range of time.
 *
 * @author nkumar
 */
public class ReportsGenerationDurationWindow extends Window {

    private ReportsGenerationDurationPanel reportsGenerationDurationPanel;

    public ReportsGenerationDurationWindow() {
        buildGui();
    }

    /**
     * This method will design internal architecture of window.
     */
    private void buildGui() {
        DashBoardApp.restartTimer();
        setWindowProps();
        reportsGenerationDurationPanel = new ReportsGenerationDurationPanel();
        addItem(reportsGenerationDurationPanel);
    }

    /**
     * This method is setting properties of pop up window.
     */
    private void setWindowProps() {
        setPadding(8);
        setWidth(680);
        setHeight(125);
        //TODO : This title name will come from Dynamic properties.
        setTitle(ApplicationContextUtils.getClientApplicationProperties().getReportGenerationDurationWindowTitle());
        setShowMinimizeButton(false);
        setIsModal(true);
        setShowModalMask(true);
        centerInPage();
    }
}
