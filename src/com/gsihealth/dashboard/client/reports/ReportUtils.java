package com.gsihealth.dashboard.client.reports;

import com.google.gwt.i18n.client.DateTimeFormat;
import java.util.Date;

/**
 *
 * @author Chad Darby
 */
public class ReportUtils {

    private static DateTimeFormat dateFormatter = DateTimeFormat.getFormat("MMddyyyy_HHmmss");
    
    /**
     * Get the current page URL
     * 
     * @return 
     */
    public static String getCurrentPageUrl() {
        // build full URL to this page
        String protocol = com.google.gwt.user.client.Window.Location.getProtocol();
        String hostAndPort = com.google.gwt.user.client.Window.Location.getHost();
        String currentPage = com.google.gwt.user.client.Window.Location.getPath();

        // handle for devmode
        String path = "";
        if (currentPage.startsWith("/dashboard")) {
            path = "/dashboard";
        }

        String url = protocol + "//" + hostAndPort + path;

        return url;
    }
    
    public static String getTimeStamp() {
        return dateFormatter.format(new Date());
    }
    
}
