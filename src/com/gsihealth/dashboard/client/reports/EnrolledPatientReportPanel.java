package com.gsihealth.dashboard.client.reports;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.common.DashboardTitles;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.reports.callbacks.GetEnrolledPatientCallback;
import com.gsihealth.dashboard.client.service.ReportService;
import com.gsihealth.dashboard.client.service.ReportServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.common.util.EncryptUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 * 
 * 
 * @author Satyendra Singh
 */
public class EnrolledPatientReportPanel extends VLayout {

    private EnrolledPatientReportListPanel enrolledPatientLevelReportListPanel;
    private final ReportServiceAsync reportService = GWT.create(ReportService.class);
    
    private GetEnrolledPatientCallback getEnrolledPatientCallback;    
    private ProgressBarWindow progressBarWindow;

    private long userId;
    private long accessLevelId;
    private long communityId;
    
    public EnrolledPatientReportPanel() {

        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        communityId=loginResult.getCommunityId();
        userId = loginResult.getUserId();
        accessLevelId = loginResult.getAccessLevelId();
        
        buildGui(); 
        DashBoardApp.restartTimer();
    }

    private void buildGui() {
        Label label = new Label("<h3>" + DashboardTitles.CONSENTED_PATIENTS_REPORT + "</h3>");
        label.setWidth(400);
        label.setHeight(40);
        addMember(label);       
        
        Img excelImg = new Img("excel-logo.jpg", 30, 30);
        excelImg.setAltText("Download");
        Layout excelImgLayout = new HLayout();
        excelImgLayout.setWidth(Constants.REPORTS_LIST_GRID_WIDTH);
        excelImgLayout.setHeight(30);
        excelImgLayout.setAlign(Alignment.LEFT);
        excelImgLayout.addMember(excelImg);
        addMember(excelImgLayout);

        enrolledPatientLevelReportListPanel = new EnrolledPatientReportListPanel();
        enrolledPatientLevelReportListPanel.setPageEventHandler(new UserListPageEventHandler());
        excelImg.addClickHandler(new ExcelDownloaderClickHandler());

        addMember(enrolledPatientLevelReportListPanel);

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(20);
        addMember(spacer);
        this.setOverflow(Overflow.AUTO);

        progressBarWindow = new ProgressBarWindow();
        getEnrolledPatientCallback = new GetEnrolledPatientCallback(enrolledPatientLevelReportListPanel, progressBarWindow);
        
        // Get Patient from the server        
        gotoPage(1);
    }

    public void gotoPage(final int pageNumber) {

        progressBarWindow.setVisible(true);
        
        GetTotalCountCallback totalCountCallback = new GetTotalCountCallback(pageNumber);
        totalCountCallback.attempt();
    }
    
    class ExcelDownloaderClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            StringBuilder url = new StringBuilder(ReportUtils.getCurrentPageUrl());
            
            url.append("/reports/enrolledPatients/consented_patients_report_" + ReportUtils.getTimeStamp() + ".csv");
            
            String encryptedArgs = null;

            try {
                // org, access, oid
                StringBuffer data = new StringBuffer();
                data.append(userId + ",");
                data.append(accessLevelId + ",");
                
                encryptedArgs = EncryptUtils.encode(data.toString());
            } catch (Exception exc) {
            }

            url.append("?p=" + encryptedArgs);
            
            
            Window.open(url.toString(), "_blank", null);            
        }
    }

    class UserListPageEventHandler implements PageEventHandler {

        public void handlePage(int pageNum, int pageSize) {

            progressBarWindow.show();
            reportService.getEnrolledPatients(communityId,userId, accessLevelId, pageNum, pageSize, getEnrolledPatientCallback);
        }
    }

    private class GetTotalCountCallback extends RetryActionCallback<Integer> {

        private final int pageNumber;

        public GetTotalCountCallback(int pageNumber) {
            this.pageNumber = pageNumber;
        }

        @Override
        public void attempt() {
            reportService.getTotalCountForEnrolledPatients(communityId,userId, accessLevelId, this);
        }
        
        @Override
        public void onCapture(Integer total) {                
            enrolledPatientLevelReportListPanel.setTotalCount(total);
            enrolledPatientLevelReportListPanel.gotoPage(pageNumber);
        }
    }
    
}
