package com.gsihealth.dashboard.client.reports;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.common.GridPager;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.VLayout;

import static com.gsihealth.dashboard.client.util.Constants.REPORTS_LIST_GRID_HEIGHT;
import static com.gsihealth.dashboard.client.util.Constants.REPORTS_LIST_GRID_WIDTH;

/**
 *
 * @author Satyendra Singh
 */
public class NotEnrolledPatientReportListPanel extends VLayout {

    private ListGrid reportListGrid;    
    private String[] columnNames;
    private GridPager pager;
    private String plcsTitle;
    
    public NotEnrolledPatientReportListPanel() {
        buildGui();
        DashBoardApp.restartTimer();
    }

    private void buildGui() {
         //getting PLCS title from prop file.
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        plcsTitle = loginResult.getPlcsTitle();
        
        columnNames = new String[]{"Patient ID","First Name", "Middle Name", "Last Name", "SSN", "Address 1" ,"Address 2" , "City", "State", "ZipCode", "Organization", "Source","Acuity Score", plcsTitle, "Enrollment Status", "Primary Payer Class", "Primary Payer Plan","Medicaid/Medicare/Payer ID"};

        setHeight(REPORTS_LIST_GRID_HEIGHT);
        reportListGrid = buildReportListGrid();

        addMember(reportListGrid);
        
        pager = new GridPager(0);
        pager.setWidth(REPORTS_LIST_GRID_WIDTH);
        pager.showLoading();
        addMember(pager);                
    }

    public void populateListGrid(ListGridRecord[] data, int totalCount) {
        reportListGrid.setData(data);
        reportListGrid.markForRedraw();
        pager.setTotalCount(totalCount);
    }

    /** Helper method to build a ListGrid
     *
     * @return
     */
    private ListGrid buildReportListGrid() {
                     
        ListGrid theListGrid = new ListGrid();

        theListGrid.setCanReorderFields(true);
        theListGrid.setCanReorderRecords(true);

        theListGrid.setWidth(REPORTS_LIST_GRID_WIDTH);
        theListGrid.setHeight(REPORTS_LIST_GRID_HEIGHT);

        theListGrid.setAlternateRecordStyles(true);
        theListGrid.setShowAllRecords(false);
        theListGrid.setSelectionType(SelectionStyle.SINGLE);
        
        // add fields
        ListGridField[] fields = buildListGridFields(columnNames);
        theListGrid.setFields(fields);
        
        theListGrid.setEmptyMessage("...");
        theListGrid.setCanResizeFields(true);

        return theListGrid;
    }

    private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];

        for (int i=0; i < columnNames.length; i++) {
            String tempColumnName = columnNames[i];
            ListGridField tempField = buildListGridField(tempColumnName);
            fields[i] = tempField;
        }
        
        return fields;
    }
    
    private ListGridField buildListGridField(String title) {
        ListGridField tempField = new ListGridField(title, title);

        tempField.setCanEdit(false);
        
        return tempField;
    }

    public ListGrid getListGrid() {
        return reportListGrid;
    }

    public void gotoPage(int pageNumber) {
        pager.gotoPage(pageNumber);
    }

    public int getCurrentPageNumber() {
        return pager.getCurrentPageNumber();
    }

    public void setPageEventHandler(PageEventHandler handler) {
        pager.setPageEventHandler(handler);
    }

    public void refreshPageLabels(int pageNum) {
        pager.refreshPageLabels(pageNum);
    }

    public void setTotalCount(int totalCount) {
        pager.setTotalCount(totalCount);
    }

    public int getPageSize() {
        return pager.getPageSize();
    }
    
}