/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.reports;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.datepicker.client.CalendarUtil;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.DateUtils;
import com.gsihealth.dashboard.common.util.EncryptUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.ButtonItem;
import com.smartgwt.client.widgets.form.fields.DateItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.SpacerItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangeEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangeHandler;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.validator.CustomValidator;
import com.smartgwt.client.widgets.form.validator.DateRangeValidator;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.Date;
import java.util.LinkedHashMap;


/**
 * This class is a Panel class which will arrange internal components of Patient
 * tracking sheet range window.
 *
 * @author nkumar
 */
public class ReportsGenerationDurationPanel extends VLayout {

    private SelectItem rangeSelectItem;
    private DateItem fromDateItem;
    private DateItem toDateItem;
    private DateTimeFormat dateFormatter;
    private ButtonItem goButtonItem;
    private String reportFromDate = "";
    private String reportToDate = "";

    public ReportsGenerationDurationPanel() {
        buildGui();
        DashBoardApp.restartTimer();
    }

    /**
     * This method will arrange UI components and will add into panel.
     */
    private void buildGui() {
        //Dynamic form to create space at top of the window
        DynamicForm dynamicFormForSpace = new DynamicForm();
        dynamicFormForSpace.setNumCols(10);
        SpacerItem spacerItem = new SpacerItem();
        spacerItem.setStartRow(false);
        spacerItem.setWidth(2);
        dynamicFormForSpace.setFields(spacerItem);
        addMember(dynamicFormForSpace);
        //Dynamic form which contains major components of date range to generate patient tracking sheet.
        DynamicForm dynamicForm = createDynamicForm();
        addMember(dynamicForm);
        //Dynamic form which contains static message relade to date range on UI date selectors.        
        DynamicForm dynamicFormForMessage = new DynamicForm();
        dynamicFormForMessage.setNumCols(10);
        SpacerItem spacerItemForMessage = new SpacerItem();
        spacerItemForMessage.setStartRow(false);
        spacerItemForMessage.setWidth(346);
        StaticTextItem textItem = new StaticTextItem("selectPeriodMessage", "You can select any 90-Day period.");
        textItem.setWrapTitle(false);
        dynamicFormForMessage.setTitleSuffix("");
        dynamicFormForMessage.setFields(spacerItemForMessage, textItem);
        addMember(dynamicFormForMessage);
    }

    //This method will create major components of date range to generate patient tracking sheet.
    private DynamicForm createDynamicForm() {
        dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");

        DynamicForm theForm = new DynamicForm();
        theForm.setNumCols(10);
        rangeSelectItem = new SelectItem("rangeSelectItem", "Time Period");
        rangeSelectItem.setValueMap(getValueMapOfRangeDropDown());
        rangeSelectItem.setWrapTitle(false);
        rangeSelectItem.setTabIndex(1);
        rangeSelectItem.setRequired(true);
        rangeSelectItem.setAlign(Alignment.CENTER);
        //Range dropdown values changed handler added to dropdown here.
        rangeSelectItem.addChangeHandler(new ChangeHandler() {
            @Override
          public void onChange(ChangeEvent event) {
             String value = event.getValue().toString();
                reportToDate = formatDateWithoutAddUpYears(new Date());

                if (value.equalsIgnoreCase("Past 30 Days")) {
                     Date cal = new Date();
                     CalendarUtil.addDaysToDate(cal, -29);
                    fromDateItem.setValue(cal);
                    toDateItem.setValue(new Date());
                    reportFromDate = formatDate(cal);
                    disableDateItems();
                    validateDateItems();
                } else if (value.equalsIgnoreCase("Past 60 Days")) {
                     Date cal = new Date();
                     CalendarUtil.addDaysToDate(cal, -59);
                    fromDateItem.setValue(cal);
                    toDateItem.setValue(new Date());
                    reportFromDate = formatDate(cal);
                    disableDateItems();
                    validateDateItems();
                } else if (value.equalsIgnoreCase("Past 90 Days")) {
                    Date cal = new Date();
                     CalendarUtil.addDaysToDate(cal, -89);
                    fromDateItem.setValue(cal);
                    toDateItem.setValue(new Date());
                    reportFromDate = formatDate(cal);
                    disableDateItems();
                    validateDateItems();
                } else if (value.equalsIgnoreCase("Choose Custom Time Period")) {
                    fromDateItem.clearValue();
                    toDateItem.clearValue();
                    fromDateItem.enable();
                    toDateItem.enable();
                } else {
                    String quarterNumber = value.trim().charAt(1) + "";
                    String quarterYear = value.substring(2, value.indexOf("(")).trim();
                    Date calanderToStartQuarterDate = new Date();
                    Date calanderToEndQuarterDate = new Date();

                    if (quarterNumber.equalsIgnoreCase("1")) {
                        calanderToStartQuarterDate = new Date(Integer.parseInt(quarterYear)- 1900, 0, 1, 0, 0);
                        calanderToEndQuarterDate= new Date(Integer.parseInt(quarterYear) - 1900, 2, 31, 0, 0);
                    } else if (quarterNumber.equalsIgnoreCase("2")) {
                        calanderToStartQuarterDate= new Date(Integer.parseInt(quarterYear)- 1900, 3, 1, 0, 0);
                        calanderToEndQuarterDate= new Date(Integer.parseInt(quarterYear) - 1900, 5, 30, 0, 0);
                    } else if (quarterNumber.equalsIgnoreCase("3")) {
                        calanderToStartQuarterDate= new Date(Integer.parseInt(quarterYear) - 1900, 6, 1, 0, 0);
                        calanderToEndQuarterDate= new Date(Integer.parseInt(quarterYear )- 1900, 8, 30, 0, 0);
                    } else if (quarterNumber.equalsIgnoreCase("4")) {
                        calanderToStartQuarterDate= new Date(Integer.parseInt(quarterYear )- 1900, 9, 1, 0, 0);
                        calanderToEndQuarterDate= new Date(Integer.parseInt(quarterYear )- 1900, 11, 31, 0, 0);
                    }
                    fromDateItem.setValue(calanderToStartQuarterDate);
                    toDateItem.setValue(calanderToEndQuarterDate);
                    reportFromDate = formatDate(calanderToStartQuarterDate);
                    reportToDate = formatDate(calanderToEndQuarterDate);
                    disableDateItems();
                    validateDateItems();
                }
         }
        });

        fromDateItem = new DateItem("fromDateItem", " From ");
        fromDateItem.setTabIndex(2);
        fromDateItem.setEndRow(false);
        fromDateItem.setWrapTitle(false);
        fromDateItem.setTextAlign(Alignment.LEFT);
        fromDateItem.setUseMask(true);
        fromDateItem.setEnforceDate(true);
        fromDateItem.setDisplayFormat(DateDisplayFormat.TOUSSHORTDATE);
        fromDateItem.setStartDate(dateFormatter.parse("01/01/1900"));
        fromDateItem.setRequired(true);
        fromDateItem.setAlign(Alignment.CENTER);
        fromDateItem.setValidateOnChange(true);

        toDateItem = new DateItem("toDateItem", " To ");
        toDateItem.setTabIndex(3);
        toDateItem.setEndRow(false);
        toDateItem.setWrapTitle(false);
        toDateItem.setTextAlign(Alignment.LEFT);
        toDateItem.setUseMask(true);
        toDateItem.setEnforceDate(true);
        toDateItem.setDisplayFormat(DateDisplayFormat.TOUSSHORTDATE);
        toDateItem.setStartDate(dateFormatter.parse("01/01/1900"));
        toDateItem.setRequired(true);
        toDateItem.setAlign(Alignment.CENTER);
        toDateItem.setValidateOnChange(true);
        disableDateItems();

        // add date validation rules
        DateRangeValidator dateRangeValidator = new DateRangeValidator();
        dateRangeValidator.setErrorMessage("To date can't be in future.");
        Date today = DateUtils.getDateAtEndOfToday();
        dateRangeValidator.setMax(today);
        
        
        fromDateItem.setValidators(dateRangeValidator, new CustomValidator() {
            @Override
            protected boolean condition(Object value) {
                Date customFromDate = fromDateItem.getValueAsDate();
                Date customToDate = toDateItem.getValueAsDate();
                 if (customFromDate != null && customToDate != null) {
                    customFromDate.setYear(customFromDate.getYear() - 1900);
                    customToDate.setYear(customToDate.getYear() - 1900);
                 }
                        if (customFromDate != null && customToDate != null) {
                            Long diffInDays = (customToDate.getTime() - customFromDate.getTime()) / (24 * 60 * 60 * 1000);
                          if(diffInDays < 0 ){
                              setErrorMessage("From date should be less than To date.");
                              return false;
                          }else if(diffInDays > 92){
                             setErrorMessage("Difference of dates is more than 90 days.");
                              return false;
                        }else{
                              reportFromDate = formatDate(customFromDate);
                              reportToDate = formatDate(customToDate);
                              return true;
                          }
                     }
               setErrorMessage("");
               return false;                        
            }
            
        });
        
        
        
        toDateItem.setValidators(dateRangeValidator );
        
        SpacerItem spacerItem = new SpacerItem();
        spacerItem.setEndRow(false);
        spacerItem.setWidth(2);

        goButtonItem = new ButtonItem("goButtonItem", "Go");
        goButtonItem.setShowTitle(false);
        goButtonItem.setWidth(30);
        goButtonItem.setTabIndex(4);
        //Click event of Go button is added here.
        goButtonItem.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if (isValidateAllItems()) {
                        downloadPTSheet(reportFromDate,reportToDate);                 
                }
            }
        });
        goButtonItem.setStartRow(false);
        goButtonItem.setAlign(Alignment.CENTER);
        theForm.setFields(rangeSelectItem, fromDateItem, toDateItem, spacerItem, goButtonItem);
        return theForm;
    }
private void  downloadPTSheet(String reportFromDate,String reportToDate){
     LoginResult loginResult = ApplicationContextUtils.getLoginResult();
            long communityId=loginResult.getCommunityId();
            long userId = loginResult.getUserId();
            long accessLevelId = loginResult.getAccessLevelId();
            long organizationId = loginResult.getOrganizationId();
            String oid = loginResult.getOid();
                         StringBuilder url = new StringBuilder(ReportUtils.getCurrentPageUrl());

            // add reference to export servlet            
            url.append("/reports/dohPatientTracking/patient_tracking_sheet_" + ReportUtils.getTimeStamp() + ".xlsx");

            String encryptedArgs = null;

            try {
                // org, access, oid
                StringBuilder data = new StringBuilder();
                data.append(organizationId + ",");
                data.append(accessLevelId + ",");
                data.append(oid + ",");
                data.append(userId + ",");
                data.append(communityId);

                encryptedArgs = EncryptUtils.encode(data.toString());
            } catch (Exception exc) {
            }

            url.append("?p=" + encryptedArgs+"&&fromDate="+reportFromDate+"&&toDate="+reportToDate);

            Window.open(url.toString(), "_blank", null);
}
    /**
     * This method will disable both date picker on Patient tracking sheet range
     * select UI.
     */
    public void disableDateItems() {
        fromDateItem.disable();
        toDateItem.disable();
    }
    
    public void validateDateItems() {
        fromDateItem.validate();
        toDateItem.validate();
    }

    /**
     * This method is creating value map of range period drop down. In which
     * last eight completed quarters are being calculated dynamically.
     *
     * @return
     */
    private LinkedHashMap<String, String> getValueMapOfRangeDropDown() {
        Date currentDate = new Date();
        LinkedHashMap<String, String> mapOfRangeDropDown = new LinkedHashMap<String, String>();
        mapOfRangeDropDown.put("Past 30 Days", "Past 30 Days");
        mapOfRangeDropDown.put("Past 60 Days", "Past 60 Days");
        mapOfRangeDropDown.put("Past 90 Days", "Past 90 Days");
        int currentYear = DateUtils.getYear(currentDate);
        int currentMonth = DateUtils.getMonth(currentDate);
        if (currentMonth >= 1 && currentMonth <= 3) {
            commonQuartersForLastYear(mapOfRangeDropDown, currentYear);
            commonQuartersOfSecondLastYear(mapOfRangeDropDown, currentYear);
            mapOfRangeDropDown.put("Q1 " + (currentYear - 2) + getQuartersMap().get(1), "Q1 " + (currentYear - 2) + getQuartersMap().get(1));
        } else if (currentMonth >= 4 && currentMonth <= 6) {
            mapOfRangeDropDown.put("Q1 " + currentYear + getQuartersMap().get(1), "Q1 " + currentYear + getQuartersMap().get(1));
            commonQuartersForLastYear(mapOfRangeDropDown, currentYear);
            commonQuartersOfSecondLastYear(mapOfRangeDropDown, currentYear);
        } else if (currentMonth >= 7 && currentMonth <= 9) {
            mapOfRangeDropDown.put("Q2 " + currentYear + getQuartersMap().get(2), "Q2 " + currentYear + getQuartersMap().get(2));
            mapOfRangeDropDown.put("Q1 " + currentYear + getQuartersMap().get(1), "Q1 " + currentYear + getQuartersMap().get(1));
            commonQuartersForLastYear(mapOfRangeDropDown, currentYear);
            mapOfRangeDropDown.put("Q4 " + (currentYear - 2) + getQuartersMap().get(4), "Q4 " + (currentYear - 2) + getQuartersMap().get(4));
            mapOfRangeDropDown.put("Q3 " + (currentYear - 2) + getQuartersMap().get(3), "Q3 " + (currentYear - 2) + getQuartersMap().get(3));
        } else if (currentMonth >= 10 && currentMonth <= 12) {
            commonQuartersOfCurrentYear(mapOfRangeDropDown, currentYear);
            commonQuartersForLastYear(mapOfRangeDropDown, currentYear);
            mapOfRangeDropDown.put("Q4 " + (currentYear - 2) + getQuartersMap().get(4), "Q4 " + (currentYear - 2) + getQuartersMap().get(4));
        }

        mapOfRangeDropDown.put("Choose Custom Time Period", "Choose Custom Time Period");
        return mapOfRangeDropDown;
    }

    /**
     * This method will add second last year's common quarters to drop down.
     *
     * @param mapOfRangeDropDown
     * @param currentYear
     */
    private void commonQuartersOfSecondLastYear(LinkedHashMap<String, String> mapOfRangeDropDown, int currentYear) {
        mapOfRangeDropDown.put("Q4 " + (currentYear - 2) + getQuartersMap().get(4), "Q4 " + (currentYear - 2) + getQuartersMap().get(4));
        mapOfRangeDropDown.put("Q3 " + (currentYear - 2) + getQuartersMap().get(3), "Q3 " + (currentYear - 2) + getQuartersMap().get(3));
        mapOfRangeDropDown.put("Q2 " + (currentYear - 2) + getQuartersMap().get(2), "Q2 " + (currentYear - 2) + getQuartersMap().get(2));
    }

    /**
     * This method will add current year's common quarters to drop down.
     *
     * @param mapOfRangeDropDown
     * @param currentYear
     */
    private void commonQuartersOfCurrentYear(LinkedHashMap<String, String> mapOfRangeDropDown, int currentYear) {
        mapOfRangeDropDown.put("Q3 " + currentYear + getQuartersMap().get(3), "Q3 " + currentYear + getQuartersMap().get(3));
        mapOfRangeDropDown.put("Q2 " + currentYear + getQuartersMap().get(2), "Q2 " + currentYear + getQuartersMap().get(2));
        mapOfRangeDropDown.put("Q1 " + currentYear + getQuartersMap().get(1), "Q1 " + currentYear + getQuartersMap().get(1));
    }

    /**
     * This method will add last year's and current year's common quarters to
     * drop down.
     *
     * @param mapOfRangeDropDown
     * @param currentYear
     */
    private void commonQuartersForLastYear(LinkedHashMap<String, String> mapOfRangeDropDown, int currentYear) {
        mapOfRangeDropDown.put("Q4 " + (currentYear - 1) + getQuartersMap().get(4), "Q4 " + (currentYear - 1) + getQuartersMap().get(4));
        commonQuartersOfCurrentYear(mapOfRangeDropDown, (currentYear - 1));
    }

    /**
     * This method is being used to resolve duplicate code on the basis of
     * quarters.
     *
     * @return
     */
    private LinkedHashMap<Integer, String> getQuartersMap() {
        LinkedHashMap<Integer, String> quartersMap = new LinkedHashMap<Integer, String>();
        quartersMap.put(1, " (Jan - Mar)");
        quartersMap.put(2, " (Apr - Jun)");
        quartersMap.put(3, " (Jul - Sep)");
        quartersMap.put(4, " (Oct - Dec)");
        return quartersMap;
    }

    public String getReportFromDate() {
        return reportFromDate;
    }

    public String getReportToDate() {
        return reportToDate;
    }

    /**
     * This method is will perform validations on date range components for PTS
     * generation.
     *
     * @return
     */
    private boolean isValidateAllItems() {
        boolean rangeSelectItemValidate = rangeSelectItem.validate();
        boolean fromDateItemValidate = fromDateItem.validate();
        boolean toDateItemValidate = toDateItem.validate();
        return rangeSelectItemValidate && fromDateItemValidate && toDateItemValidate;
    }
    
   private String formatDateWithoutAddUpYears(Date date){
     DateTimeFormat dateFormatter = DateTimeFormat.getFormat("yyyy-MM-dd");
     return dateFormatter.format(date);
    }
   
   
    private String formatDate(Date date){
     DateTimeFormat dateFormatter = DateTimeFormat.getFormat("yyyy-MM-dd");
     date.setYear(date.getYear() + 1900);
     return dateFormatter.format(date);
    }
}
