package com.gsihealth.dashboard.client.reports;

import com.google.gwt.core.client.GWT;
import com.google.gwt.http.client.URL;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.WindowManager;
import com.gsihealth.dashboard.client.common.ButtonPanel;
import com.gsihealth.dashboard.client.common.DashboardTitles;
import com.gsihealth.dashboard.client.service.ReportService;
import com.gsihealth.dashboard.client.service.ReportServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.AccessLevelConstants;
import com.gsihealth.dashboard.common.util.EncryptUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 * @author Satyendra Singh
 *
 */
public class MainReportPanel extends VLayout {

    /**
     *
     */
      private final ReportServiceAsync reportService = GWT.create(ReportService.class);
    public MainReportPanel() {
        buildGUI();
        DashBoardApp.restartTimer();
    }

    public void buildGUI() {

        Canvas reportButtonsCanvas = buildReportButtons();

        setHeight100();

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(20);

        VLayout layout = new VLayout();
        layout.setMembers(reportButtonsCanvas, spacer);
        addMember(layout);
        setPadding(5);
    }

    private Canvas buildReportButtons() {
        VLayout vLayout = new VLayout();
        HLayout layout = new HLayout();

        vLayout.setWidth(900);
        vLayout.setBorder("1px solid lightgray");

        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        long accessLevelId = loginResult.getAccessLevelId();

        boolean superUser = accessLevelId == AccessLevelConstants.SUPER_USER_ACCESS_LEVEL_ID;
        boolean localAdmin = accessLevelId == AccessLevelConstants.LOCAL_ADMIN_ACCESS_LEVEL_ID;
        
        // report handle for power user
        boolean powerUser = accessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID;

        if (localAdmin) {
            Img userLevelReportsImg = new Img("64x64/reports.jpg", 64, 64);
            ButtonPanel userButtonPanel = new ButtonPanel(userLevelReportsImg, DashboardTitles.SYSTEM_ADMINISTRATOR_REPORTS);
            layout.addMember(userButtonPanel);
            userLevelReportsImg.addClickHandler(new UserReportClickHandler());

            Img patientTrackingReportsImg = new Img("64x64/reports.jpg", 64, 64);
            ButtonPanel patientTrackingButtonPanel = new ButtonPanel(patientTrackingReportsImg, DashboardTitles.DOH_PATIENT_TRACKING_SHEET);
            layout.addMember(patientTrackingButtonPanel);
            patientTrackingReportsImg.addClickHandler(new PatientTrackingClickHandler());
        }

        if (superUser || powerUser) {
            Img userLevelReportsImg = new Img("64x64/reports.jpg", 64, 64);
            ButtonPanel userButtonPanel = new ButtonPanel(userLevelReportsImg, DashboardTitles.SYSTEM_ADMINISTRATOR_REPORTS);
            layout.addMember(userButtonPanel);
            userLevelReportsImg.addClickHandler(new UserReportClickHandler());

            Img patientTrackingReportsImg = new Img("64x64/reports.jpg", 64, 64);
            ButtonPanel patientTrackingButtonPanel = new ButtonPanel(patientTrackingReportsImg, DashboardTitles.DOH_PATIENT_TRACKING_SHEET);
            layout.addMember(patientTrackingButtonPanel);
            patientTrackingReportsImg.addClickHandler(new PatientTrackingClickHandler());
        }

        Img patientLevelReportsImg = new Img("64x64/reports.jpg", 64, 64);
        ButtonPanel patientButtonPanel = new ButtonPanel(patientLevelReportsImg, DashboardTitles.PATIENT_LEVEL_REPORT);
        layout.addMember(patientButtonPanel);

        Img patientProgramsReportsImg = new Img("64x64/reports.jpg", 64, 64);
        ButtonPanel programButtonPanel = new ButtonPanel(patientProgramsReportsImg, DashboardTitles.PATIENT_PROGRAM_REPORT);
        layout.addMember(programButtonPanel);
        // setup handlers
        patientLevelReportsImg.addClickHandler(new PatientReportClickHandler());

        patientProgramsReportsImg.addClickHandler(new PatientProgramReportClickHandler());
         
        Label title = new Label("Reports");
        title.setHeight(20);
        title.setStyleName("dashboardTitleLabel");
        vLayout.addMember(title);
        vLayout.addMember(layout);

        Canvas canvas = new Canvas();
        canvas.setAutoHeight();
        canvas.setAutoWidth();
        canvas.addChild(vLayout);
        return canvas;
    }

    class UserReportClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
//            WindowManager.addWindow(DashboardTitles.SYSTEM_ADMINISTRATOR_REPORTS, new UserLevelReportsMainPanel());
            DashBoardApp.clearPanel(DashboardTitles.REPORTS);
            DashBoardApp.updatePanel(DashboardTitles.REPORTS, DashboardTitles.SYSTEM_ADMINISTRATOR_REPORTS, new UserLevelReportsMainPanel());
        }
    }

    class PatientReportClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
//            WindowManager.addWindow(DashboardTitles.PATIENT_LEVEL_REPORT, new PatientReportsMainPanel());
            DashBoardApp.clearPanel(DashboardTitles.REPORTS);
            DashBoardApp.updatePanel(DashboardTitles.REPORTS, DashboardTitles.PATIENT_LEVEL_REPORT, new PatientReportsMainPanel());
        }
    }
    class PatientProgramReportClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
//            WindowManager.addWindow(DashboardTitles.PATIENT_PROGRAM_REPORT, new PatientProgramReportPanel());
            DashBoardApp.clearPanel(DashboardTitles.REPORTS);
            DashBoardApp.updatePanel(DashboardTitles.REPORTS, DashboardTitles.PATIENT_PROGRAM_REPORT, new PatientProgramReportPanel());
        }
    }

    class PatientTrackingClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            LoginResult loginResult = ApplicationContextUtils.getLoginResult();
            long communityId=loginResult.getCommunityId();
            long userId = loginResult.getUserId();
            long accessLevelId = loginResult.getAccessLevelId();
            long organizationId = loginResult.getOrganizationId();
            String oid = loginResult.getOid();

          
            reportService.findPTSRecordsCount(communityId, userId, accessLevelId, new AsyncCallback<Long>() {
                @Override
                public void onFailure(Throwable thrwbl) {
                     SC.warn(thrwbl.getMessage());
                }

                @Override
                public void onSuccess(Long t) {
                   
                    if(t>10000){
                        ReportsGenerationDurationWindow window=new ReportsGenerationDurationWindow();
                        window.show();
                                
                    }
                    else{
                        
                          LoginResult loginResult = ApplicationContextUtils.getLoginResult();
            long communityId=loginResult.getCommunityId();
            long userId = loginResult.getUserId();
            long accessLevelId = loginResult.getAccessLevelId();
            long organizationId = loginResult.getOrganizationId();
            String oid = loginResult.getOid();
                         StringBuilder url = new StringBuilder(ReportUtils.getCurrentPageUrl());

            // add reference to export servlet            
            url.append("/reports/dohPatientTracking/patient_tracking_sheet_" + ReportUtils.getTimeStamp() + ".xlsx");

            String encryptedArgs = null;

            try {
                // org, access, oid
                StringBuilder data = new StringBuilder();
                data.append(organizationId + ",");
                data.append(accessLevelId + ",");
                data.append(oid + ",");
                data.append(userId + ",");
                data.append(communityId);

                encryptedArgs = EncryptUtils.encode(data.toString());
            } catch (Exception exc) {
            }

            url.append("?p=" + encryptedArgs);

            Window.open(url.toString(), "_blank", null);
                    }
                }
            });
            
           
        }
    }

    protected String encode(String data) {
        String encodedData = null;

        try {
            encodedData = URL.encode(data);
        } catch (Exception exc) {
            // do nothing
            encodedData = data;
        }

        return encodedData;
    }
}
