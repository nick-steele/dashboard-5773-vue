package com.gsihealth.dashboard.client.reports;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.util.Constants;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 * @author Satyendra Singh
 */
public class AggregatePatientCountListPanel extends VLayout {

    private ListGrid patientCountReportListGrid;
    
    public AggregatePatientCountListPanel() {
        buildGui();
        DashBoardApp.restartTimer();
    }

    private void buildGui() {

//        setHeight(Constants.REPORTS_LIST_GRID_HEIGHT);
        patientCountReportListGrid = buildReportListGrid();

        addMember(patientCountReportListGrid);
    }

    public void populateListGrid(ListGridRecord[] data) {
        patientCountReportListGrid.setData(data);
    }

    /** Helper method to build a ListGrid
     *
     * @return
     */
    private ListGrid buildReportListGrid() {

        String[] columnNames = {"Candidates for Enrollment", "Patients Assigned to a Care Team", "Patients Enrolled and No Care Team Assignment", "Patients with a Completed Care Plan", "Patients with a Completed Assessment"};
        
        ListGrid theListGrid = new ListGrid();

        theListGrid.setCanReorderFields(true);
        theListGrid.setCanReorderRecords(true);

        theListGrid.setWidth(Constants.REPORTS_LIST_GRID_WIDTH);
        theListGrid.setHeight(Constants.REPORTS_LIST_GRID_HEIGHT);

        theListGrid.setAlternateRecordStyles(true);
        theListGrid.setShowAllRecords(true);
        theListGrid.setSelectionType(SelectionStyle.SINGLE);
        
        // add fields
        ListGridField[] fields = buildListGridFields(columnNames);
        theListGrid.setFields(fields);
        
        theListGrid.setEmptyMessage("...");
        theListGrid.setCanResizeFields(true);

        return theListGrid;
    }

    private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];

        for (int i=0; i < columnNames.length; i++) {
            String tempColumnName = columnNames[i];
            ListGridField tempField = buildListGridField(tempColumnName);
            fields[i] = tempField;
        }
        
        return fields;
    }
    
    private ListGridField buildListGridField(String title) {
        ListGridField tempField = new ListGridField(title, title);

        tempField.setCanEdit(false);
        
        return tempField;
    }

    public ListGrid getListGrid() {
        return patientCountReportListGrid;
    }

}
