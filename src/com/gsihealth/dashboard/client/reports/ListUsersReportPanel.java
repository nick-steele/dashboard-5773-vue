package com.gsihealth.dashboard.client.reports;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.common.DashboardTitles;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.service.ReportService;
import com.gsihealth.dashboard.client.service.ReportServiceAsync;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.PropertyUtils;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class ListUsersReportPanel extends VLayout {

    private UserLevelReportListPanel userLevelReportListPanel;
    private final ReportServiceAsync reportService = GWT.create(ReportService.class);
    private ProgressBarWindow progressBarWindow;

    public ListUsersReportPanel() {

        buildGui();
        DashBoardApp.restartTimer();
    }

    private void buildGui() {
        Label label = new Label("<h3>" + DashboardTitles.LIST_USERS_REPORT + "</h3>");
        label.setWidth(200);
        label.setHeight(40);
        addMember(label);

        Img excelImg = new Img("excel-logo.jpg", 30, 30);
        Layout excelImgLayout = new HLayout();
        excelImgLayout.setWidth(Constants.REPORTS_LIST_GRID_WIDTH);
        excelImgLayout.setHeight(30);
        excelImgLayout.setAlign(Alignment.LEFT);
        excelImgLayout.addMember(excelImg);
        addMember(excelImgLayout);

        userLevelReportListPanel = new UserLevelReportListPanel();
        userLevelReportListPanel.setPageEventHandler(new UserListPageEventHandler());
        excelImg.addClickHandler(new ExcelDownloaderClickHandler());

        addMember(userLevelReportListPanel);

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(20);
        addMember(spacer);
        this.setOverflow(Overflow.AUTO);

        progressBarWindow = new ProgressBarWindow("Loading", "Loading...");

        // Get Users from the server
        gotoPage(1);
    }

    class ExcelDownloaderClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            String url = ReportUtils.getCurrentPageUrl();

            // add reference to export servlet            
            url += "/reports/userReport/user_list_report_" + ReportUtils.getTimeStamp() + ".csv";

            Window.open(url, "_blank", null);
        }
    }

    public void gotoPage(final int pageNumber) {

        progressBarWindow.setVisible(true);

        GetTotalCountCallback getTotalCountCallback = new GetTotalCountCallback(pageNumber);
        getTotalCountCallback.attempt();
        
    }

    class UserListPageEventHandler implements PageEventHandler {

        public void handlePage(int pageNum, int pageSize) {

            progressBarWindow.show();
            
            GetReportUsersCallback getReportUsersCallback = new GetReportUsersCallback(pageNum, pageSize);
            getReportUsersCallback.attempt();
                    

        }
    }

    class GetReportUsersCallback extends RetryActionCallback<SearchResults<UserDTO>> {

        private int pageNum;
        private int pageSize;
        
        GetReportUsersCallback(int pageNum, int pageSize) {
            this.pageNum = pageNum;
            this.pageSize = pageSize;
        }

        @Override
        public void attempt() {
            reportService.getReportUsers(pageNum, pageSize, this);
        }
        
        /**
         * Populate the list grid
         */
        @Override
        public void onCapture(SearchResults<UserDTO> searchResults) {

            List<UserDTO> userDTOs = searchResults.getData();
            ListGridRecord[] data = new ListGridRecord[userDTOs.size()];

            // populate the list grid
            for (int index = 0; index < userDTOs.size(); index++) {
                UserDTO theUserDTO = userDTOs.get(index);
                data[index] = PropertyUtils.convertToUserRecord(index, theUserDTO);
            }

            int totalCount = searchResults.getTotalCount();
            userLevelReportListPanel.populateListGrid(data, totalCount);

            ListGrid listGrid = userLevelReportListPanel.getListGrid();
            listGrid.markForRedraw();

            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }
        }

        /**
         *
         * @param exc
         */
        @Override
        public void onFailure(Throwable exc) {

            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            super.onFailure(exc);
        }
    }

    class GetTotalCountCallback extends RetryActionCallback<Integer> {

        private final int pageNumber;

        public GetTotalCountCallback(int pageNumber) {
            this.pageNumber = pageNumber;
        }

        @Override
        public void attempt() {
            reportService.getTotalCountForReportUsers(this);
        }
        
        @Override
        public void onCapture(Integer total) {
            userLevelReportListPanel.setTotalCount(total);
            userLevelReportListPanel.gotoPage(pageNumber);
        }

        @Override
        public void onFailure(Throwable thrwbl) {
            progressBarWindow.setVisible(false);
            super.onFailure(thrwbl);
        }
    }
}
