
package com.gsihealth.dashboard.client.reports;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.common.DashboardTitles;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.util.Constants;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.reports.callbacks.GetPatientProgramsCallback;
import com.gsihealth.dashboard.client.service.CareteamService;
import com.gsihealth.dashboard.client.service.CareteamServiceAsync;
import com.gsihealth.dashboard.client.service.ReportService;
import com.gsihealth.dashboard.client.service.ReportServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.client.util.ValidationUtils;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.util.EncryptUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.EditorExitEvent;
import com.smartgwt.client.widgets.form.fields.events.EditorExitHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyPressHandler;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;

/**
 *
 * @author ssingh
 */
public class PatientProgramReportPanel  extends VLayout {
  
    private PatientProgramReportListPanel patientProgramReportListPanel;
    private ProgressBarWindow progressBarWindow;
    private final ReportServiceAsync reportService = GWT.create(ReportService.class);
    private long userId;
    private long accessLevelId; 
    private long currentUserOrgId;
    private long communityId;
    private GetPatientProgramsCallback getPatientProgramsCallback; 
    private DynamicForm patientSearchForm;
    private SearchCriteria searchCriteria = null;
    private TextItem lastNameTextItem;
    private TextItem firstNameTextItem;
    private static String FIRST_NAME_PLACE_HOLDER = "-First Name-";
    private static final String LAST_NAME_PLACE_HOLDER = "*-Last Name-";
    private final CareteamServiceAsync careteamService = GWT.create(CareteamService.class);
    private int currentPageNumber;
    private boolean initialLoad = true;
    private String firstName = "";
    private String lastName = "";

    public PatientProgramReportPanel() {
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        userId = loginResult.getUserId();
        accessLevelId = loginResult.getAccessLevelId();
        currentUserOrgId = loginResult.getOrganizationId();
        communityId = loginResult.getCommunityId();
      
        buildGui();
        DashBoardApp.restartTimer();
    }
 
     private void buildGui() {
        Label label = new Label("<h3>" + DashboardTitles.PATIENT_PROGRAM_REPORT + "</h3>");
        label.setWidth(400);
        label.setHeight(40);
        addMember(label);

        Img excelImg = new Img("excel-logo.jpg", 30, 30);
        excelImg.setAltText("Download");
        Layout excelImgLayout = new HLayout();
        excelImgLayout.setWidth(Constants.REPORTS_LIST_GRID_WIDTH);
        excelImgLayout.setHeight(30);
        excelImgLayout.setAlign(Alignment.LEFT);
        excelImgLayout.addMember(excelImg);
        addMember(excelImgLayout);
        
        final DynamicForm form = new DynamicForm();
        form.setLayoutAlign(Alignment.LEFT);
        form.setVisible(true);
        form.setHeight(20);

        HLayout hLayout = new HLayout();
         hLayout.setVisible(true);
        hLayout.setHeight(15);
        hLayout.setLayoutTopMargin(0);
        hLayout.setLayoutBottomMargin(0);
        hLayout.setAlign(Alignment.LEFT);
        addMember(hLayout);

        addMember(form);
        patientSearch(form, hLayout);

        patientProgramReportListPanel = new PatientProgramReportListPanel();
        patientProgramReportListPanel.setPageEventHandler(new PatientProgramListPageEventHandler());
        excelImg.addClickHandler(new ExcelDownloaderClickHandler());

        addMember(patientProgramReportListPanel);

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(20);
        addMember(spacer);
        this.setOverflow(Overflow.AUTO);

        progressBarWindow = new ProgressBarWindow();
        getPatientProgramsCallback = new GetPatientProgramsCallback(patientProgramReportListPanel, progressBarWindow);
//        
        // Get Patient from the server        
        gotoPage(1);
  
    }
  public void gotoPage(final int pageNumber) {

        progressBarWindow.setVisible(true);
        
        GetTotalCountCallback totalCountCallback = new GetTotalCountCallback(pageNumber);
        totalCountCallback.attempt();
    }
  
  class PatientProgramListPageEventHandler implements PageEventHandler {

        public void handlePage(int pageNum, int pageSize) {
            if(initialLoad==true){
                searchCriteria=null;
            } 
            long communityId=ApplicationContextUtils.getLoginResult().getCommunityId();
            progressBarWindow.show();
            reportService.getPatientPrograms(userId, accessLevelId,currentUserOrgId, communityId,searchCriteria, pageNum, pageSize, getPatientProgramsCallback);
        }
    }
  
   private class GetTotalCountCallback extends RetryActionCallback<Integer> {

        private final int pageNumber;

        public GetTotalCountCallback(int pageNumber) {
            this.pageNumber = pageNumber;
        }

        @Override
        public void attempt() {
          if(initialLoad==true){
                searchCriteria=null;
            } 
          long communityId=ApplicationContextUtils.getLoginResult().getCommunityId();
          reportService.getTotalCountOfPrograms(userId, accessLevelId,currentUserOrgId,communityId,searchCriteria, this);
        }
        
        @Override
        public void onCapture(Integer total) {                
            patientProgramReportListPanel.setTotalCount(total);
            patientProgramReportListPanel.gotoPage(pageNumber);
        }
    }
   
    class ExcelDownloaderClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            StringBuilder url = new StringBuilder(ReportUtils.getCurrentPageUrl());
            getSearchCriteria();
            url.append("/reports/patientProgramReportServlet/patient_program_sheet_" + ReportUtils.getTimeStamp() + ".xlsx");

            String encryptedArgs = null;

            try {
                // org, access, oid
                StringBuffer data = new StringBuffer();
                data.append(currentUserOrgId + ",");
                data.append(userId + ",");
                data.append(accessLevelId + ",");
                data.append(lastName + ",");
                data.append(firstName + ",");
                encryptedArgs = EncryptUtils.encode(data.toString());
            } catch (Exception exc) {
            }

            url.append("?p=" + encryptedArgs);
            
            
            Window.open(url.toString(), "_blank", null);            
        }
    }
    
      private void patientSearch(final DynamicForm theForm, final HLayout hLayout) {

        patientSearchForm = theForm;
        searchCriteria = new SearchCriteria();
        progressBarWindow = new ProgressBarWindow("Loading", "Loading...");
        getPatientProgramsCallback = new GetPatientProgramsCallback(patientProgramReportListPanel, progressBarWindow);

        /*
         * Adding validation to allow text or special symbols (- ' `)  only.
         */
          RegExpValidator alphaHGAARegExValidator = ValidationUtils.getAlphaHGAARegExValidator();


        patientSearchForm.setNumCols(2);
        patientSearchForm.setColWidths("100", "100");

//        Label patientSearchLabel = new Label("<h4 style='margin:0px; padding:0px'>Search</h4>");
//        patientSearchLabel.setWrap(false);
//        patientSearchLabel.setStyleName("font-size:20px");
//
//        hLayout.addMember(patientSearchLabel);

          PickerIcon clearPicker = new PickerIcon(PickerIcon.CLEAR, new FormItemClickHandler() {

            public void onFormItemClick(FormItemIconClickEvent event) {
                patientSearchForm.clearValues();
                initialLoad=true;
                firstName=null;
                lastName=null;
                PatientProgramListPageEventHandler pageEventHandler=new PatientProgramListPageEventHandler();
                pageEventHandler.handlePage(0,200);
                gotoPage(1);
            }
        });

        final SearchClickHandler searchClickHandler = new SearchClickHandler();
        PickerIcon searchPickerIcon = new PickerIcon(PickerIcon.SEARCH);
        searchPickerIcon.addFormItemClickHandler(searchClickHandler);


        firstNameTextItem = new TextItem();
        firstNameTextItem.setDefaultValue(FIRST_NAME_PLACE_HOLDER);
        firstNameTextItem.setShowTitle(false);
        firstNameTextItem.setAlign(Alignment.LEFT);
        firstNameTextItem.setIcons(searchPickerIcon, clearPicker);
        firstNameTextItem.setValidateOnExit(false);

        /* 
         * Adding click handler to remove predefined values
         */
        firstNameTextItem.addClickHandler(new FirstClickHandler());

        firstNameTextItem.addKeyPressHandler(new KeyHandler(searchClickHandler));


        lastNameTextItem = new TextItem();
        lastNameTextItem.setDefaultValue(LAST_NAME_PLACE_HOLDER);
        lastNameTextItem.setShowTitle(false);
        lastNameTextItem.setRequired(true);
        lastNameTextItem.setAlign(Alignment.LEFT);
//      
        lastNameTextItem.setValidateOnExit(false);


        /* 
         * Adding click handler to remove predefined values
         */
        lastNameTextItem.addClickHandler(new FirstClickHandler());
        lastNameTextItem.addKeyPressHandler(new KeyHandler(searchClickHandler));
//
//        /*
//         * Adding EditorExitHandler for setting default values when user skips textItem without entering any value.
//         */
        lastNameTextItem.addEditorExitHandler(new TextEditorExitHandler());
        firstNameTextItem.addEditorExitHandler(new TextEditorExitHandler());

        /*
         * Adding validation to firstNameTextItem and lastNameTextItem
         */
        ValidationUtils.setValidators(firstNameTextItem, alphaHGAARegExValidator, false);
        ValidationUtils.setValidators(lastNameTextItem, alphaHGAARegExValidator, false);


        patientSearchForm.setFields(lastNameTextItem,firstNameTextItem);
      }
       
      class FirstClickHandler implements com.smartgwt.client.widgets.form.fields.events.ClickHandler {

        public void onClick(com.smartgwt.client.widgets.form.fields.events.ClickEvent event) {
            boolean eraseFirstNameText = StringUtils.equalsIgnoreCase(firstNameTextItem.getValueAsString(), FIRST_NAME_PLACE_HOLDER);
            boolean eraseLastNameText = StringUtils.equalsIgnoreCase(lastNameTextItem.getValueAsString(), LAST_NAME_PLACE_HOLDER);

            if (event.getSource().equals(lastNameTextItem)) {
                if (eraseLastNameText) {
                    lastNameTextItem.setValue("");
                }
            } else if (event.getSource().equals(firstNameTextItem)) {

                if (eraseFirstNameText) {
                    firstNameTextItem.setValue("");
                }
            }
        }
    }
       
      class KeyHandler implements KeyPressHandler {

        SearchClickHandler clickHandler = null;

        public KeyHandler(SearchClickHandler handler) {
            clickHandler = handler;
        }

        public void onKeyPress(KeyPressEvent event) {
            if (event.getKeyName().equals("Enter")) {
                clickHandler.onFormItemClick(null);
            }

        }
    }
      
   class TextEditorExitHandler implements EditorExitHandler {

        public void onEditorExit(EditorExitEvent event) {
            if (event.getSource().equals(firstNameTextItem)) {
                if (StringUtils.isBlank(firstNameTextItem.getValueAsString())) {
                    firstNameTextItem.setValue(FIRST_NAME_PLACE_HOLDER);
                    patientSearchForm.clearErrors(true);
                }
            } else if (event.getSource().equals(lastNameTextItem)) {
                if (StringUtils.isBlank(lastNameTextItem.getValueAsString())) {
                    lastNameTextItem.setValue(LAST_NAME_PLACE_HOLDER);
                    patientSearchForm.clearErrors(true);
                    if (!lastNameTextItem.validate()) {
                        patientSearchForm.clearErrors(true);
                    }
                }
            }
        }
    } 
   
   class SearchClickHandler implements FormItemClickHandler {

        public void onFormItemClick(FormItemIconClickEvent event) {
            initialLoad=false;
            searchCriteria=new SearchCriteria();
            firstName = firstNameTextItem.getValueAsString();
             lastName = lastNameTextItem.getValueAsString();

            if (StringUtils.isNotBlank(lastName) && StringUtils.equals(lastName, LAST_NAME_PLACE_HOLDER)) {
                lastNameTextItem.setValue("");
                lastName=null;
            }

            if (StringUtils.isNotBlank(firstName) && StringUtils.equals(firstName, FIRST_NAME_PLACE_HOLDER)) {
                firstNameTextItem.setValue("");
                firstName =null;
            }

            if (patientSearchForm.validate()) {
//                searchCriteria.setFirstName(null);
//                searchCriteria.setLastName(null);
                if (StringUtils.isNotBlank(lastName)) {
                    searchCriteria.setLastName(lastName);
                    if (StringUtils.isNotBlank(firstName)) {
                        searchCriteria.setFirstName(firstName);
                    }
                 
                    patientSearch(1, communityId, userId, searchCriteria);
                } else {
                    SC.warn("You must enter Last Name for Search");
                    lastNameTextItem.focusInItem();
                }
            }
            

        }
    }

   public void patientSearch(int pageNumber, long communityId, long userId, final SearchCriteria searchCriteria) {

        currentPageNumber = pageNumber;
        progressBarWindow.show();
        
        reportService.findPatientWithPrograms(communityId, userId, searchCriteria, getPatientProgramsCallback);
        gotoPage(1);
   }
 
   public void getSearchCriteria(){
              
       firstName=firstNameTextItem.getValueAsString();
       lastName= lastNameTextItem.getValueAsString();
       if(firstName.isEmpty()){
           firstName=FIRST_NAME_PLACE_HOLDER;
           
       }
    
   }
   
}
