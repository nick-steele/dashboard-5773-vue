package com.gsihealth.dashboard.client.reports;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.common.GridPager;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.client.util.Constants;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.VLayout;

import static com.gsihealth.dashboard.client.util.Constants.DEFAULT_LAYOUT_HEIGHT;
import static com.gsihealth.dashboard.client.util.Constants.REPORTS_LIST_GRID_HEIGHT;
import static com.gsihealth.dashboard.client.util.Constants.REPORTS_LIST_GRID_WIDTH;

/**
 *
 * @author Satyendra Singh
 */
public class UserLevelReportListPanel extends VLayout {

    private ListGrid reportListGrid;
    private GridPager pager;
    
    public UserLevelReportListPanel() {
        buildGui();
        DashBoardApp.restartTimer();
    }

    private void buildGui() {

        setHeight(DEFAULT_LAYOUT_HEIGHT);
        reportListGrid = buildReportListGrid();
        
        addMember(reportListGrid);

        pager = new GridPager(0);
        pager.setWidth(REPORTS_LIST_GRID_WIDTH);
        pager.showLoading();
        addMember(pager);        
    }

    public void populateListGrid(ListGridRecord[] data, int totalCount) {
        reportListGrid.setData(data);
        reportListGrid.markForRedraw();
        pager.setTotalCount(totalCount);
    }
    
    /** Helper method to build a ListGrid
     *
     * @return
     */
    private ListGrid buildReportListGrid() {

        String[] columnNames = {"User ID/Email","First Name", "Middle Name", "Last Name", "Address 1" ,"Address 2" , "City", "State", "ZipCode", "Organization", "Access Level", "User Role", "Last Access Date"};
        
        ListGrid theListGrid = new ListGrid();

        theListGrid.setCanReorderFields(true);
        theListGrid.setCanReorderRecords(true);

        theListGrid.setWidth(REPORTS_LIST_GRID_WIDTH);
        theListGrid.setHeight(REPORTS_LIST_GRID_HEIGHT);

        theListGrid.setAlternateRecordStyles(true);
        theListGrid.setShowAllRecords(false);
        theListGrid.setSelectionType(SelectionStyle.SINGLE);
        
        // add fields
        ListGridField[] fields = buildListGridFields(columnNames);
        theListGrid.setFields(fields);
        
        theListGrid.setEmptyMessage("...");
        theListGrid.setCanResizeFields(true);

        return theListGrid;
    }

    private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];

        for (int i=0; i < columnNames.length; i++) {
            String tempColumnName = columnNames[i];
            ListGridField tempField = buildListGridField(tempColumnName);
            fields[i] = tempField;
        }
        
        return fields;
    }
    
    private ListGridField buildListGridField(String title) {
        ListGridField tempField = new ListGridField(title, title);

        tempField.setCanEdit(false);
        
        return tempField;
    }

    public ListGrid getListGrid() {
        return reportListGrid;
    }

    public void gotoPage(int pageNumber) {
        pager.gotoPage(pageNumber);
    }

    public int getCurrentPageNumber() {
        return pager.getCurrentPageNumber();
    }

    public void setPageEventHandler(PageEventHandler handler) {
        pager.setPageEventHandler(handler);
    }

    public void refreshPageLabels(int pageNum) {
        pager.refreshPageLabels(pageNum);
    }

    public void setTotalCount(int totalCount) {
        pager.setTotalCount(totalCount);
    }

    public int getPageSize() {
        return pager.getPageSize();
    }        
    
}
