
package com.gsihealth.dashboard.client.reports;

import com.smartgwt.client.widgets.grid.ListGridRecord;
import java.util.Date;

/**
 *
 * @author ssingh
 */
public class PatientProgramRecord extends ListGridRecord{
    
    private int index;
    
    

    /**
     * @return the index
     */
    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

    public String getPatientInfo() {
        return getAttribute("PatientInfo");
    }

    public void setPatientInfo(String patientInfo) {
        setAttribute("PatientInfo", patientInfo);
    }
    
     public String getLastName() {
        return getAttribute("Last Name");
    }

    public void setLastName(String lastName) {
        setAttribute("Last Name", lastName);
    }
    
     public String getFirstName() {
        return getAttribute("First Name");
    }

    public void setFirstName(String firstName) {
        setAttribute("First Name", firstName);
    }
    /**
     * @return the programId
     */
    public String getProgram() {
        return getAttribute("Program Name");
    }

    public void setProgram(String programName) {
        setAttribute("Program Name", programName);
    }
    /**
     * @return the healthHome
     */
    public String getHealthHome() {
       return getAttribute("Health Home");
    }

    /**
     * @param healthHome the healthHome to set
     */
    public void setHealthHome(String healthHome) {
        setAttribute("Health Home" , healthHome);
    }

    /**
     * @return the effectiveDate
     */
    public String getEffectiveDate() {
        return getAttribute("Program Effective");
    }

    /**
     * @param effectiveDate the effectiveDate to set
     */
    public void setEffectiveDate(String effectiveDate) {
       setAttribute("Program Effective" , effectiveDate);
    }

    /**
     * @return the endDate
     */
    public String getEndDate() {
        return getAttribute("Program End");
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(String endDate) {
       setAttribute("Program End" , endDate);
    }

    /**
     * @return the statusEffective
     */
    public String getStatusEffective() {
        return getAttribute("Status Effective");
    }

    /**
     * @param statusEffective the statusEffective to set
     */
    public void setStatusEffective(String statusEffective) {
      setAttribute("Status Effective", statusEffective);
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return getAttribute("Patient Status");
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
         setAttribute("Patient Status", status);
    }

    /**
     * @return the endReason
     */
    public String getEndReason() {
        return getAttribute("Program End Reason");
    }

    /**
     * @param endReason the endReason to set
     */
    public void setEndReason(String endReason) {
        setAttribute("Program End Reason", endReason);
    }
    
    
}
