package com.gsihealth.dashboard.client.reports;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.common.DashboardTitles;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.reports.callbacks.GetReportPatientCountCallback;
import com.gsihealth.dashboard.client.service.ReportService;
import com.gsihealth.dashboard.client.service.ReportServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 * 
 * 
 * @author Satyendra Singh
 */
public class AggregatePatientCountReportPanel extends VLayout {

    private AggregatePatientCountListPanel aggregatePatientCountListPanel;
    private final ReportServiceAsync reportService = GWT.create(ReportService.class);
    private GetReportPatientCountCallback getReportPatientCountCallback;
    private long communityId;
    
    public AggregatePatientCountReportPanel() {

         LoginResult loginResult = ApplicationContextUtils.getLoginResult();
         communityId=loginResult.getCommunityId();
        buildGui();
        DashBoardApp.restartTimer();
    }

    private void buildGui() {
        Label label = new Label("<h3>" + DashboardTitles.PATIENT_COUNT_REPORT + "</h3>");
        label.setWidth(250);
        label.setHeight(40);
        addMember(label);       
        
        Img excelImg = new Img("excel-logo.jpg", 30, 30);
        excelImg.setAltText("Download");
        Layout excelImgLayout = new HLayout();
        excelImgLayout.setWidth(Constants.REPORTS_LIST_GRID_WIDTH);
        excelImgLayout.setHeight(30);
        excelImgLayout.setAlign(Alignment.LEFT);
        excelImgLayout.addMember(excelImg);
        addMember(excelImgLayout);

        aggregatePatientCountListPanel = new AggregatePatientCountListPanel();
        excelImg.addClickHandler(new ExcelDownloaderClickHandler());

        addMember(aggregatePatientCountListPanel);

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(20);
        addMember(spacer);
        this.setOverflow(Overflow.AUTO);

        // Get Users from the server        
        loadListWithPatientCount();

    }

    /**
     * Load list grid with new data
     */
    private void loadListWithPatientCount() {

        ProgressBarWindow progressBarWindow = new ProgressBarWindow();
        progressBarWindow.show();
        getReportPatientCountCallback = new GetReportPatientCountCallback(aggregatePatientCountListPanel, progressBarWindow);

        reportService.getAggregatePatientCount(communityId,getReportPatientCountCallback);
    }
    
    class ExcelDownloaderClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
          
            StringBuilder url = new StringBuilder(ReportUtils.getCurrentPageUrl());
            
            url.append("/reports/aggregatePatientCount/total_patient_count_report_" + ReportUtils.getTimeStamp() + ".csv");
            
            Window.open(url.toString(), "_blank", null);            
         }
    }
  
}
