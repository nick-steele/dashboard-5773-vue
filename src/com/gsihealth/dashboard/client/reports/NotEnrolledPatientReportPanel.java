package com.gsihealth.dashboard.client.reports;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.Window;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.common.DashboardTitles;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.reports.callbacks.GetNotEnrolledPatientCallback;
import com.gsihealth.dashboard.client.service.ReportService;
import com.gsihealth.dashboard.client.service.ReportServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.common.util.EncryptUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 *
 * @author Satyendra Singh
 */
public class NotEnrolledPatientReportPanel extends VLayout {

    private NotEnrolledPatientReportListPanel notEnrolledPatientLevelReportListPanel;
    private final ReportServiceAsync reportService = GWT.create(ReportService.class);
    private GetNotEnrolledPatientCallback getNotEnrolledPatientCallback;
    private ProgressBarWindow progressBarWindow;
    private long currentUserOrgId;
    private long accessLevelId;
    private int currentPageNumber;
    private long commmunityid;

    public NotEnrolledPatientReportPanel() {

        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        currentUserOrgId = loginResult.getOrganizationId();
        commmunityid=loginResult.getCommunityId();
        // handle for power user
        accessLevelId = loginResult.getAccessLevelId();

        buildGui();
        DashBoardApp.restartTimer();
    }

    private void buildGui() {
        Label label = new Label("<h3>" + DashboardTitles.PATIENTS_NOT_CONSENTED_REPORT + "</h3>");
        label.setWidth(Constants.REPORTS_LIST_GRID_WIDTH);
        label.setHeight(40);
        addMember(label);

        Img excelImg = new Img("excel-logo.jpg", 30, 30);
        excelImg.setAltText("Download");

        Layout excelImgLayout = new HLayout();
        excelImgLayout.setWidth(Constants.REPORTS_LIST_GRID_WIDTH);
        excelImgLayout.setHeight(30);
        excelImgLayout.setAlign(Alignment.LEFT);
        excelImgLayout.addMember(excelImg);

        addMember(excelImgLayout);

        notEnrolledPatientLevelReportListPanel = new NotEnrolledPatientReportListPanel();
        notEnrolledPatientLevelReportListPanel.setPageEventHandler(new UserListPageEventHandler());
        excelImg.addClickHandler(new ExcelDownloaderClickHandler());

        addMember(notEnrolledPatientLevelReportListPanel);

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(20);
        addMember(spacer);
        this.setOverflow(Overflow.AUTO);

        progressBarWindow = new ProgressBarWindow();
        getNotEnrolledPatientCallback = new GetNotEnrolledPatientCallback(notEnrolledPatientLevelReportListPanel, progressBarWindow);

        // Get Patient from the server        
        gotoPage(1);
    }

    public void gotoPage(final int pageNumber) {
        currentPageNumber = pageNumber;
        progressBarWindow.setVisible(true);

        TotalCountCallback totalCountCallback = new TotalCountCallback();
        totalCountCallback.attempt();
    }

    class TotalCountCallback extends RetryActionCallback<Integer> {

        @Override
        public void attempt() {
            
            // handle for power user
                reportService.getTotalCountForCandidatePatientsNotEnrolled(commmunityid,this);

        }

        @Override
        public void onCapture(Integer total) {
            notEnrolledPatientLevelReportListPanel.setTotalCount(total);
            notEnrolledPatientLevelReportListPanel.gotoPage(currentPageNumber);
        }

        @Override
        public void onFailure(Throwable exc) {
            progressBarWindow.setVisible(false);
            super.onFailure(exc);
        }
    }

    class ExcelDownloaderClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            LoginResult loginResult = ApplicationContextUtils.getLoginResult();
            long currentUserOrgId = loginResult.getOrganizationId();
            long accessLevelId = loginResult.getAccessLevelId();

            StringBuilder url = new StringBuilder(ReportUtils.getCurrentPageUrl());

            url.append("/reports/notEnrolledPatients/patients_not_consented_report_" + ReportUtils.getTimeStamp() + ".csv");

            String encryptedArgs = null;

            try {
                // org, access, oid
                StringBuffer data = new StringBuffer();
                data.append(currentUserOrgId + ",");
                data.append(accessLevelId + ",");

                encryptedArgs = EncryptUtils.encode(data.toString());
            } catch (Exception exc) {
            }

            url.append("?p=" + encryptedArgs);


            Window.open(url.toString(), "_blank", null);
        }
    }

    class UserListPageEventHandler implements PageEventHandler {

        public void handlePage(int pageNum, int pageSize) {

            progressBarWindow.show();

                reportService.getCandidatePatientsNotEnrolled(commmunityid,pageNum, pageSize, getNotEnrolledPatientCallback);

        }
    }
}
