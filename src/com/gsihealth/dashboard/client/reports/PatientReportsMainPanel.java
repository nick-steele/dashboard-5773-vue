package com.gsihealth.dashboard.client.reports;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.WindowManager;
import com.gsihealth.dashboard.client.common.ButtonPanel;
import com.gsihealth.dashboard.client.common.DashboardTitles;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;

import static com.gsihealth.dashboard.client.DashBoardApp.clearPanel;
import static com.gsihealth.dashboard.client.DashBoardApp.updatePanel;
import static com.gsihealth.dashboard.client.common.DashboardTitles.REPORTS;

/**
 * @author Satyendra Singh
 *
 */
public class PatientReportsMainPanel extends VLayout {

    /**
     *
     */
    public PatientReportsMainPanel() {
        buildGUI();
        DashBoardApp.restartTimer();
    }

    public void buildGUI() {
        Canvas reportsPanel = buildReportsPanel();

        setHeight100();

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(20);

        VLayout layout = new VLayout();
        layout.setMembers(reportsPanel, spacer);
        addMember(layout);
    }

    private Canvas buildReportsPanel() {
        VLayout vLayout = new VLayout();
        HLayout layout = new HLayout();

        vLayout.setWidth(900);
        vLayout.setBorder("1px solid lightgray");

        Img notEnrolledPatientReportsImg = new Img("64x64/reports.jpg", 64, 64);
        ButtonPanel notEnrolledButtonPanel = new ButtonPanel(notEnrolledPatientReportsImg, DashboardTitles.PATIENTS_NOT_CONSENTED_REPORT);
        layout.addMember(notEnrolledButtonPanel);

        Img enrolledPatientReportsImg = new Img("64x64/reports.jpg", 64, 64);
        ButtonPanel enrolledButtonPanel = new ButtonPanel(enrolledPatientReportsImg, DashboardTitles.CONSENTED_PATIENTS_REPORT);
        layout.addMember(enrolledButtonPanel);

        Img patientCountReportsImg = new Img("64x64/reports.jpg", 64, 64);
        ButtonPanel patientCountButtonPanel = new ButtonPanel(patientCountReportsImg, DashboardTitles.PATIENT_COUNT_REPORT);
        layout.addMember(patientCountButtonPanel);

        // setup handlers
        notEnrolledPatientReportsImg.addClickHandler(new NotEnrolledPatientsClickHandler());
        enrolledPatientReportsImg.addClickHandler(new EnrolledPatientsClickHandler());
        patientCountReportsImg.addClickHandler(new PatientCountClickHandler());

        Label title = new Label("Patient Reports");
        title.setHeight(20);
        title.setStyleName("dashboardTitleLabel");
        vLayout.addMember(title);
        vLayout.addMember(layout);

        Canvas canvas = new Canvas();
        canvas.setAutoHeight();
        canvas.setAutoWidth();
        canvas.addChild(vLayout);
        return canvas;
    }

    class NotEnrolledPatientsClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
//            WindowManager.addWindow(DashboardTitles.PATIENTS_NOT_CONSENTED_REPORT, new NotEnrolledPatientReportPanel());
            clearPanel(REPORTS);
            updatePanel(REPORTS, DashboardTitles.PATIENTS_NOT_CONSENTED_REPORT, new NotEnrolledPatientReportPanel());
        }
    }

    class EnrolledPatientsClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
//            WindowManager.addWindow(DashboardTitles.CONSENTED_PATIENTS_REPORT, new EnrolledPatientReportPanel());
            clearPanel(REPORTS);
            updatePanel(REPORTS, DashboardTitles.CONSENTED_PATIENTS_REPORT, new EnrolledPatientReportPanel());
        }
    }

    class PatientCountClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
//            WindowManager.addWindow(DashboardTitles.PATIENT_COUNT_REPORT, new AggregatePatientCountReportPanel());
            clearPanel(REPORTS);
            updatePanel(REPORTS, DashboardTitles.PATIENT_COUNT_REPORT, new AggregatePatientCountReportPanel());

        }
    }
}
