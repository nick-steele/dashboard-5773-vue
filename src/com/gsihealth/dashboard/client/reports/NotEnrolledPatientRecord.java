package com.gsihealth.dashboard.client.reports;

import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 *
 * @author Satyendra
 */
public class NotEnrolledPatientRecord extends ListGridRecord {

    private int index;
    private String plcsTitle;

    public NotEnrolledPatientRecord() {
         //getting PLCS title from prop file.
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        plcsTitle = loginResult.getPlcsTitle();
    }

    public int getIndex() {
        return index;
    }

    /**
     * @return Patient ID
     */
    public long getPatientID() {
        return Long.valueOf(getAttribute("Patient ID"));
    }

    /**
     * @param patientID the patientID to set
     */
    public void setPatientID(long patientID) {
        setAttribute("Patient ID", patientID);
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return getAttribute("First Name");
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        setAttribute("First Name", firstName);
    }

    /**
     * @return the middleName
     */
    public String getMiddleName() {
        return getAttribute("Middle Name");
    }

    /**
     * @param lastName the MiddleName to set
     */
    public void setMiddleName(String middleName) {
        setAttribute("Middle Name", middleName);
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return getAttribute("Last Name");
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        setAttribute("Last Name", lastName);
    }

    /**
     * @return the address1
     */
    public String getAddress1() {
        return getAttribute("Address 1");
    }

    /**
     * @param lastName the address1 to set
     */
    public void setAddress1(String address1) {
        setAttribute("Address 1", address1);
    }

    /**
     * @return the address1
     */
    public String getAddress2() {
        return getAttribute("Address 2");
    }

    /**
     * @param lastName the address1 to set
     */
    public void setAddress2(String address2) {
        setAttribute("Address 2", address2);
    }

    /**
     * @return the org
     */
    public String getOrg() {
        return getAttribute("Organization");
    }

    /**
     * @param org the org to set
     */
    public void setOrg(String org) {
        setAttribute("Organization", org);
    }

    /**
     * @return the city
     */
    public String getCity() {
        return getAttribute("City");
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        setAttribute("City", city);
    }

    /**
     * @return the state
     */
    public String getState() {
        return getAttribute("State");
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        setAttribute("State", state);

    }

    /**
     * @return the zipcode
     */
    public String getZipCode() {
        return getAttribute("ZipCode");
    }

    /**
     * @param state the ZipCode to set
     */
    public void setZipCode(String zipCode) {
        setAttribute("ZipCode", zipCode);

    }

    /**
     * @return the Source
     */
    public String getSource() {
        return getAttribute("Source");
    }

    /**
     * @param Source
     */
    public void setSource(String source) {
        setAttribute("Source", source);

    }

    /**
     * @return the ProgramLevelConsentStatus
     */
    public String getProgramLevelConsentStatus() {
        return getAttribute("Program Level Consent Status");
    }

    /**
     * @param ProgramLevelConsentStatus 
     */
    public void setProgramLevelConsentStatus(String programLevelConsentStatus) {
        setAttribute(plcsTitle, programLevelConsentStatus);

    }

    /**
     * @return the CareTeam
     */
    public String getCareTeam() {
        return getAttribute("Care Team");
    }

    /**
     * @param CareTeam 
     */
    public void setCareTeam(String careTeam) {
        setAttribute("Care Team", careTeam);

    }

    /**
     * @return the EnrollmentStatus
     */
    public String getEnrollmentStatus() {
        return getAttribute("Enrollment Status");
    }

    /**
     * @param EnrollmentStatus 
     */
    public void setEnrollmentStatus(String enrollmentStatus) {
        setAttribute("Enrollment Status", enrollmentStatus);

    }

    public String getSsn() {
        return getAttribute("SSN");
    }

    public void setSsn(String theSsn) {
        setAttribute("SSN", theSsn);
    }

    public void setPrimaryPayerClass(String primaryPayerClass) {
        setAttribute("Primary Payer Class", primaryPayerClass);
    }

    public String getPrimaryPayerClass() {
        return getAttribute("Primary Payer Class");
    }

    public void setPrimaryPayerPlan(String data) {
        setAttribute("Primary Payer Plan", data);
    }

    public String getPrimaryPayerPlan() {
        return getAttribute("Primary Payer Plan");
    }

    public void setMedicaidMedicarePayerId(String data) {
        setAttribute("Medicaid/Medicare/Payer ID", data);
    }

    public String getMedicaidMedicarePayerId() {
        return getAttribute("Medicaid/Medicare/Payer ID");
    }
  
    public String getHealthHome() {
        return getAttribute("Health Home");
    }

    public void setHealthHome(String healthName) {
        setAttribute("Health Home", healthName);
    }
    
     public void setProgramEffectiveDate(String data) {
        setAttribute("Program Effective Date", data);
    }
    
    public String getProgramEffectiveDate() {
        return getAttribute("Program Effective Date");
    }
    
     public void setProgramEndDate(String data) {
        setAttribute("Program End Date", data);
    }
    
    public String getProgramEndDate() {
        return getAttribute("Program End Date");
    }
    
    public void setAcuityScore(String acuityScore) {
        setAttribute("Acuity Score", acuityScore);
    }
    
    public String getAcuityScore() {
        return getAttribute("Acuity Score");
    }
}
