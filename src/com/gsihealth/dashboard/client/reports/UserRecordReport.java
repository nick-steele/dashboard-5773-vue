package com.gsihealth.dashboard.client.reports;

import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 *
 * @author Chad Darby
 */
public class UserRecordReport extends ListGridRecord {

    private int index;

    public UserRecordReport(int index, String email, String firstName, String middleName, String lastName, String Address1, String Address2, String org, String city, String state, String zipCode, String accessLevel, String userRole, String lastAccessDate) {

        setIndex(index);
        setEmail(email);
        setFirstName(firstName);
        setMiddleName(middleName);
        setLastName(lastName);
        setAddress1(Address1);
        setAddress2(Address2);

        setOrg(org);
        setCity(city);
        setState(state);
        setZipCode(zipCode);
        setAccessLevel(accessLevel);
        setUserRole(userRole);
        setLastAccessDate(lastAccessDate);
    }

    public int getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return getAttribute("First Name");
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        setAttribute("First Name", firstName);
    }

    /**
     * @return the middleName
     */
    public String getMiddleName() {
        return getAttribute("Middle Name");
    }

    /**
     * @param lastName the MiddleName to set
     */
    public void setMiddleName(String middleName) {
        setAttribute("Middle Name", middleName);
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return getAttribute("Last Name");
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        setAttribute("Last Name", lastName);
    }

    /**
     * @return the address1
     */
    public String getAddress1() {
        return getAttribute("Address 1");
    }

    /**
     * @param lastName the address1 to set
     */
    public void setAddress1(String Address1) {
        setAttribute("Address 1", Address1);
    }

    /**
     * @return the address2
     */
    public String getAddress2() {
        return getAttribute("Address 2");
    }

    /**
     * @param lastName the address2 to set
     */
    public void setAddress2(String Address2) {
        setAttribute("Address 2", Address2);
    }

    /**
     * @return the org
     */
    public String getOrg() {
        return getAttribute("Organization");
    }

    /**
     * @param org the org to set
     */
    public void setOrg(String org) {
        setAttribute("Organization", org);
    }

    /**
     * @return the dept
     */
    public String getDept() {
        return getAttribute("Department");
    }

    /**
     * @param dept the dept to set
     */
    public void setDept(String dept) {
        setAttribute("Department", dept);
    }

    /**
     * @return the city
     */
    public String getCity() {
        return getAttribute("City");
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        setAttribute("City", city);
    }

    /**
     * @return the state
     */
    public String getState() {
        return getAttribute("State");
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        setAttribute("State", state);

    }

    /**
     * @return the zipcode
     */
    public String getZipCode() {
        return getAttribute("ZipCode");
    }

    /**
     * @param state the ZipCode to set
     */
    public void setZipCode(String zipCode) {
        setAttribute("ZipCode", zipCode);

    }

    /**
     * @return the accessLevel
     */
    public String getAccessLevel() {
        return getAttribute("Access Level");
    }

    /**
     * @param accessLevel the accessLevel to set
     */
    public void setAccessLevel(String accessLevel) {
        setAttribute("Access Level", accessLevel);

    }

    /**
     * @return the UserRole
     */
    public String getUserRole() {
        return getAttribute("User Role");
    }

    /**
     * @param accessLevel the UserRole to set
     */
    public void setUserRole(String userLevel) {
        setAttribute("User Role", userLevel);

    }

    public String getLastAccessDate() {
        return getAttribute("Last Access Date");
    }

    public void setLastAccessDate(String lastAccessDate) {
        setAttribute("Last Access Date", lastAccessDate);

    }

    public String getEmail() {
        return getAttribute("User ID/Email");
    }

    public void setEmail(String email) {
        setAttribute("User ID/Email", email);
    }
}
