package com.gsihealth.dashboard.client.reports.callbacks;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.reports.EnrolledPatientRecord;
import com.gsihealth.dashboard.client.reports.EnrolledPatientReportListPanel;
import com.gsihealth.dashboard.client.util.PropertyUtils;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGrid;
import java.util.List;
/**
 * Receives a list of patients and populates a list grid
 *
 * @author Satyendra Singh
 */
public class GetEnrolledPatientCallback implements AsyncCallback<SearchResults<PersonDTO>> {

    private ProgressBarWindow progressBarWindow;
    private EnrolledPatientReportListPanel enrolledPatientReportListPanel;

    public GetEnrolledPatientCallback(EnrolledPatientReportListPanel thePatientsLevelReportListPanel) {
        this(thePatientsLevelReportListPanel, null);
    }

    public GetEnrolledPatientCallback(EnrolledPatientReportListPanel thePatientsLevelReportListPanel, ProgressBarWindow theProgressBarWindow) {
        enrolledPatientReportListPanel = thePatientsLevelReportListPanel;
        progressBarWindow = theProgressBarWindow;
    }

    /**
     * Populate the list grid
     */
    public void onSuccess(SearchResults<PersonDTO> searchResults) {

        List<PersonDTO> personDTOs = searchResults.getData();
        
        EnrolledPatientRecord[] data = new EnrolledPatientRecord[personDTOs.size()];

        // populate the list grid
        for (int index = 0; index < personDTOs.size(); index++) {
            PersonDTO thePersonDTO = personDTOs.get(index);
            data[index] = PropertyUtils.convertToEnrolledPatientRecord(index, thePersonDTO); 
        }
        
        int totalCount = searchResults.getTotalCount();
        enrolledPatientReportListPanel.populateListGrid(data, totalCount);
        		
        ListGrid listGrid = enrolledPatientReportListPanel.getListGrid();
        listGrid.markForRedraw();

        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

    }

    /**
     *
     * @param exc
     */
    public void onFailure(Throwable exc) {

        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

        SC.warn("Error retrieving patients.");
    }

}
