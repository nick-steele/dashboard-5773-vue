package com.gsihealth.dashboard.client.reports.callbacks;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.reports.NotEnrolledPatientRecord;
import com.gsihealth.dashboard.client.reports.NotEnrolledPatientReportListPanel;
import com.gsihealth.dashboard.client.util.PropertyUtils;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGrid;
import java.util.List;

/**
 * Receives a list of patients and populates a list grid
 *
 * @author Satyendra Singh
 */
public class GetNotEnrolledPatientCallback implements AsyncCallback<SearchResults<PersonDTO>> {

    private ProgressBarWindow progressBarWindow;
    private NotEnrolledPatientReportListPanel notEnrolledPatientReportListPanel;

    public GetNotEnrolledPatientCallback(NotEnrolledPatientReportListPanel thePatientsLevelReportListPanel) {
        this(thePatientsLevelReportListPanel, null);
    }

    public GetNotEnrolledPatientCallback(NotEnrolledPatientReportListPanel thePatientsLevelReportListPanel, ProgressBarWindow theProgressBarWindow) {
        notEnrolledPatientReportListPanel = thePatientsLevelReportListPanel;
        progressBarWindow = theProgressBarWindow;
    }

    /**
     * Populate the list grid
     */
    public void onSuccess(SearchResults<PersonDTO> searchResults) {

        List<PersonDTO> personDTOs = searchResults.getData();
        NotEnrolledPatientRecord[] data = new NotEnrolledPatientRecord[personDTOs.size()];

        // populate the list grid
        
        for (int index = 0; index < personDTOs.size(); index++) {
            PersonDTO thePersonDTO = personDTOs.get(index);
            data[index] = PropertyUtils.convertToNotEnrolledPatientRecord(index, thePersonDTO);
        }

        int totalCount = searchResults.getTotalCount();
        notEnrolledPatientReportListPanel.populateListGrid(data, totalCount);		

        ListGrid listGrid = notEnrolledPatientReportListPanel.getListGrid();
        listGrid.markForRedraw();

        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

    }

    /**
     *
     * @param exc
     */
    public void onFailure(Throwable exc) {

        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

        SC.warn("Error retrieving patients.");
    }

}
