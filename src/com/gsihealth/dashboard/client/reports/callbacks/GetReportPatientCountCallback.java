package com.gsihealth.dashboard.client.reports.callbacks;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.enrollment.AggregatePatientCountRecord;
import com.gsihealth.dashboard.client.reports.AggregatePatientCountListPanel;
import com.gsihealth.dashboard.client.util.PropertyUtils;
import com.gsihealth.dashboard.entity.dto.AggregatePatientCountDTO;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
/**
 * Receives a list of patients and populates a list grid
 *
 * @author Satyendra Singh
 */
public class GetReportPatientCountCallback implements AsyncCallback<AggregatePatientCountDTO> {

    private AggregatePatientCountDTO aggregatePatientCountDTO;
    private ProgressBarWindow progressBarWindow;
    private AggregatePatientCountListPanel aggregatePatientCountListPanel;

    public GetReportPatientCountCallback(AggregatePatientCountListPanel aggregatePatientCountListPanel) {
        this(aggregatePatientCountListPanel, null);
    }

    public GetReportPatientCountCallback(AggregatePatientCountListPanel aggregatePatientCountListPanel, ProgressBarWindow theProgressBarWindow) {
        this.aggregatePatientCountListPanel = aggregatePatientCountListPanel;
        progressBarWindow = theProgressBarWindow;
    }

    /**
     * Populate the list grid
     */
    public void onSuccess(AggregatePatientCountDTO theAggregatePatientCountDTO) {

        aggregatePatientCountDTO = theAggregatePatientCountDTO;
        AggregatePatientCountRecord[] data = new AggregatePatientCountRecord[1];

        // populate the list grid
        
        for (int index = 0; index < 1; index++) {            
            data[index] = PropertyUtils.convert(index, theAggregatePatientCountDTO);
        }
//        data[0] = PropertyUtils.convert(0, aggregatePatientCountDTO);
        aggregatePatientCountListPanel.populateListGrid(data);

        ListGrid listGrid = aggregatePatientCountListPanel.getListGrid();
        listGrid.markForRedraw();

        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

    }

    /**
     *
     * @param exc
     */
    public void onFailure(Throwable exc) {

        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

        SC.warn("Error retrieving users.");
    }

    public AggregatePatientCountDTO getAggregatePatientCountDTOs() {
        return aggregatePatientCountDTO;
    }

}
