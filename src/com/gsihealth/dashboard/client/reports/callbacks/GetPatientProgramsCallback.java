
package com.gsihealth.dashboard.client.reports.callbacks;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.reports.PatientProgramRecord;
import com.gsihealth.dashboard.client.reports.PatientProgramReportListPanel;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.PropertyUtils;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.PatientProgramDTO;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGrid;
import java.util.List;


/**
 *
 * @author ssingh
 */
public class GetPatientProgramsCallback implements AsyncCallback<SearchResults<PersonDTO>>{

    private ProgressBarWindow progressBarWindow;
    private PatientProgramReportListPanel patientProgramReportListPanel;
    private boolean healthHomeRequired;
    private String healthHomeLabel;
    private String programEndReasonLabel;

    public GetPatientProgramsCallback(PatientProgramReportListPanel thePatientProgramReportListPanel) {
        this(thePatientProgramReportListPanel, null);
        this.getProperties();
    }

    public GetPatientProgramsCallback(PatientProgramReportListPanel thePatientProgramReportListPanel, ProgressBarWindow theProgressBarWindow) {
        patientProgramReportListPanel = thePatientProgramReportListPanel;
        progressBarWindow = theProgressBarWindow;
        this.getProperties();
    }

    public void onFailure(Throwable thrwbl) {
        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

        SC.warn("Error retrieving patient programs");
    }

    public void onSuccess(SearchResults<PersonDTO> searchResults) {

        int newIndex = 0;
        int size = 0;

        List<PersonDTO> person = searchResults.getData();
        for (PersonDTO personDTO : person) {

            List<PatientProgramDTO> thePatientProgDto = personDTO.getPatientEnrollment()!=null?personDTO.getPatientEnrollment().getPatientProgramName():null;
            if(thePatientProgDto!=null){
            size = size + thePatientProgDto.size();
        }}
        System.out.println("The Size >>>>>>>>>>"+size);
        PatientProgramRecord[] data = new PatientProgramRecord[size];
        // populate the list grid
        for (int index = 0; index < person.size(); index++) {
            PersonDTO personDto = person.get(index);

            List<PatientProgramDTO> thePatientProgDto = personDto.getPatientEnrollment()!=null?personDto.getPatientEnrollment().getPatientProgramName():null;
//            if(!thePatientProgDto.isEmpty()){
             if(thePatientProgDto!=null){
            for (PatientProgramDTO patientProgramDTO : thePatientProgDto) {
                data[newIndex] = PropertyUtils.convertToPatientProgramRecord(newIndex, personDto, patientProgramDTO,healthHomeRequired,healthHomeLabel,programEndReasonLabel);
                newIndex++;
            }
        }}
//         for (int index = 0; index < person.size(); index++) {
//            PersonDTO thePersonDTO = person.get(index);
//            data[index] = PropertyUtils.convertToPatientProgramRecord(newIndex, thePersonDTO, patientProgramDTO);
//        }
        

        int totalCount = searchResults.getTotalCount();
        patientProgramReportListPanel.populateListGrid(data, totalCount);
        ListGrid listGrid = patientProgramReportListPanel.getListGrid();
        if (data.length == 0) {

            listGrid.setEmptyMessage("No record found.....");

        }
        listGrid.markForRedraw();


        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

    }
    private void getProperties(){     
          
    ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
      healthHomeRequired= props.isHealthHomeRequired();
      healthHomeLabel=props.getHealthHomeLabel();
      programEndReasonLabel=props.getProgramEndReasonLabel();
    }
}
