package com.gsihealth.dashboard.client.reports;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.WindowManager;
import com.gsihealth.dashboard.client.common.ButtonPanel;
import com.gsihealth.dashboard.client.common.DashboardTitles;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 * 
 * 
 * @author Satyendra Singh
 */
public class UserLevelReportsMainPanel extends VLayout {

    /**
     *
     */
    public UserLevelReportsMainPanel() {
        buildGUI();
        DashBoardApp.restartTimer();
    }

    public void buildGUI() {
        Canvas reportsPanel = buildReportsPanel();

        setHeight100();

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(20);

        VLayout layout = new VLayout();
        layout.setMembers(reportsPanel, spacer);
        addMember(layout);
    }

    private Canvas buildReportsPanel() {
        VLayout vLayout = new VLayout();
        HLayout layout = new HLayout();

        vLayout.setWidth(900);
        vLayout.setBorder("1px solid lightgray");

        Img userLevelReportsImg = new Img("64x64/reports.jpg", 64, 64);
        ButtonPanel userButtonPanel = new ButtonPanel(userLevelReportsImg, DashboardTitles.LIST_USERS_REPORT);
        layout.addMember(userButtonPanel);
        
        // setup handlers
        userLevelReportsImg.addClickHandler(new UserLevelReportsClickHandler());

        Label title = new Label("Administrative Reports");
        title.setHeight(20);
        title.setStyleName("dashboardTitleLabel");
        vLayout.addMember(title);
        vLayout.addMember(layout);

        Canvas canvas = new Canvas();
        canvas.setAutoHeight();
        canvas.setAutoWidth();
        canvas.addChild(vLayout);
        return canvas;
    }

    class UserLevelReportsClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
//            WindowManager.addWindow(DashboardTitles.LIST_USERS_REPORT, new ListUsersReportPanel());
            DashBoardApp.clearPanel(DashboardTitles.REPORTS);
            DashBoardApp.updatePanel(DashboardTitles.REPORTS, DashboardTitles.LIST_USERS_REPORT, new ListUsersReportPanel());
        }
    }

}
