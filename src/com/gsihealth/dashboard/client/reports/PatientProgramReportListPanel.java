package com.gsihealth.dashboard.client.reports;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.common.GridPager;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.client.service.ReportService;
import com.gsihealth.dashboard.client.service.ReportServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.FetchMode;
import com.smartgwt.client.types.GroupStartOpen;
import com.smartgwt.client.types.ListGridEditEvent;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 * @author ssingh
 */
public class PatientProgramReportListPanel extends VLayout{
    
    private GridPager pager;
    private String[] columnNames;
    private ListGrid reportListGrid;
    private long userId;
    private long accessLevelId;
    private final ReportServiceAsync reportService = GWT.create(ReportService.class);
    private String healthHomeLabel;
    private boolean healthHomeRequired;
    private String programEndReasonLabel;

    public PatientProgramReportListPanel() {

        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        userId = loginResult.getUserId();
        accessLevelId = loginResult.getAccessLevelId();
        setFormValidationRulesFromClientAppProperties();
        buildGui();
        DashBoardApp.restartTimer();
    }

    private void buildGui() {
        
        if (healthHomeRequired) {
            columnNames = new String[]{"PatientInfo", "Last Name", "First Name", healthHomeLabel, "Program Name", /*"Program Effective", "Program End",*/ "Patient Status", "Status Effective", programEndReasonLabel};
        } else {
            columnNames = new String[]{"PatientInfo", "Last Name", "First Name", "Program Name", /*"Program Effective", "Program End",*/ "Patient Status", "Status Effective", programEndReasonLabel};
        }
        setHeight(Constants.REPORTS_LIST_GRID_HEIGHT);
        reportListGrid = buildPatientProgramReportListGrid();

        addMember(reportListGrid);

        pager = new GridPager(0, 200);
        pager.setWidth(Constants.REPORTS_LIST_GRID_WIDTH);
        pager.showLoading();
        pager.setPageSize(200);
        addMember(pager);
    }
 
  private ListGrid buildPatientProgramReportListGrid() {

        ListGrid theListGrid = new ListGrid();
      
      theListGrid.setCanReorderFields(true);
      theListGrid.setCanReorderRecords(true);
      
      theListGrid.setWidth(Constants.REPORTS_LIST_GRID_WIDTH);
      theListGrid.setHeight(Constants.REPORTS_LIST_GRID_HEIGHT);
      
      theListGrid.setAlternateRecordStyles(true);
      theListGrid.setShowAllRecords(false);
      theListGrid.setSelectionType(SelectionStyle.SINGLE);
      theListGrid.setCanEdit(false);
      
      theListGrid.setEditEvent(ListGridEditEvent.CLICK);
      theListGrid.setEditByCell(true);
      theListGrid.setGroupStartOpen(GroupStartOpen.ALL);      
      theListGrid.setGroupByField("PatientInfo");

      // add fields
      ListGridField[] fields = buildListGridFields(columnNames);
      theListGrid.setFields(fields);
      
      theListGrid.setEmptyMessage("....");
      theListGrid.setCanResizeFields(true);
      theListGrid.getField("PatientInfo").setHidden(true);
       
        return theListGrid;
    }
  
   private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];

        for (int i = 0; i < columnNames.length; i++) {
            String tempColumnName = columnNames[i];
            ListGridField tempField = buildListGridField(tempColumnName);
            fields[i] = tempField;

        }
        return fields;
    }
    private ListGridField buildListGridField(String title) {
        ListGridField tempField = new ListGridField(title, title);
        tempField.setCanSort(false);
        tempField.setCanGroupBy(false);

        tempField.setCanEdit(false);
        return tempField;
    }
         
    public ListGrid getListGrid() {
        return reportListGrid;
    }
     public void gotoPage(int pageNumber) {
        pager.gotoPage(pageNumber);
    }

    public int getCurrentPageNumber() {
        return pager.getCurrentPageNumber();
    }

    public void setPageEventHandler(PageEventHandler handler) {
        pager.setPageEventHandler(handler);
    }

    public void refreshPageLabels(int pageNum) {
        pager.refreshPageLabels(pageNum);
    }

    public void setTotalCount(int totalCount) {
        pager.setTotalCount(totalCount);
    }

    public int getPageSize() {
        return pager.getPageSize();
    }

     public void populateListGrid(ListGridRecord[] data, int totalCount) {
      
//        DataSourceTextField patientInfoField = new DataSourceTextField("PatientInfo", "PatientInfo");
//        patientInfoField.setHidden(true);
//        DataSourceTextField lastNameField = new DataSourceTextField("Last Name", "Last Name");
//        DataSourceTextField firstNameField = new DataSourceTextField("First Name", "First Name");
//        DataSourceTextField programNameField = new DataSourceTextField("Program Name", "Program Name");
//        DataSourceTextField programEffectiveField = new DataSourceTextField("Program Effective", "Program Effective");
//        DataSourceTextField programEndField = new DataSourceTextField("Program End", "Program End");
//        DataSourceTextField patientStatusField = new DataSourceTextField("Patient Status", "Patient Status");
//        DataSourceTextField statusEffectiveField = new DataSourceTextField("Status Effective", "Status Effective");
//        DataSourceTextField programEndReasonField = new DataSourceTextField("Program End Reason", "Program End Reason");
//        DataSourceTextField healthHomeField = new DataSourceTextField("Health Home", "Health Home");
//
//
//
//        DataSource datasource = new DataSource();
//        datasource.setFields(patientInfoField,lastNameField,firstNameField, programNameField, programEffectiveField, programEndField, patientStatusField, statusEffectiveField, programEndReasonField, healthHomeField);
//        datasource.setClientOnly(true);
//        datasource.setTestData(data);
//
//        reportListGrid.setDataSource(datasource);
        reportListGrid.setData(data);
        reportListGrid.groupBy("PatientInfo");

        reportListGrid.markForRedraw();
        pager.setTotalCount(totalCount);
    }
 
     protected void setFormValidationRulesFromClientAppProperties() {

        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
        healthHomeRequired = props.isHealthHomeRequired();
        healthHomeLabel=props.getHealthHomeLabel();
        programEndReasonLabel=props.getProgramEndReasonLabel();
        
    }
}
