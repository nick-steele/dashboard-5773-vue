/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.callbacks;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.resourcecenter.ResourceCenterWindow;
import com.gsihealth.dashboard.entity.dto.DocumentResourceCenterDTO;
import com.smartgwt.client.util.SC;
import java.util.List;

/**
 *
 * @author User
 */
public class DocumentCenterCallback implements AsyncCallback<List<DocumentResourceCenterDTO>> {

    private ProgressBarWindow progressBarWindow;

    public DocumentCenterCallback(ProgressBarWindow theProgressBarWindow) {
        progressBarWindow = theProgressBarWindow;
    }

    public void onSuccess(List<DocumentResourceCenterDTO> docs) {
        ResourceCenterWindow window = new ResourceCenterWindow();
        window.populate(docs);
        window.show();


        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

    }

    public void onFailure(Throwable thrwbl) {
        SC.warn("Error retrieving documents.");

    }
}
