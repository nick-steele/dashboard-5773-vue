/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.callbacks;

import com.smartgwt.client.util.SC;

/**
 *
 * @author Satyendra Singh
 */
public class CheckIfDuplicatePatientCallback extends BasicPortalAsyncCallback {

    @Override
    public void onSuccess(Object obj) {

        super.onSuccess(obj);

    }

    @Override
    public void onFailure(Throwable exc) {

        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

        SC.warn(exc.getMessage());
    }
}
