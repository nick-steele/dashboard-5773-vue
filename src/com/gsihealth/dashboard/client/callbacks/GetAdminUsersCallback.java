/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.callbacks;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.admintools.AdminSearchResultsPanel;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.util.PropertyUtils;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import java.util.List;

/**
 * Receives a list of users and populates a list grid
 * 
 * @author Chad Darby
 */
public class GetAdminUsersCallback implements AsyncCallback<SearchResults<UserDTO>> {

    private List<UserDTO> userDTOs;
    private ProgressBarWindow progressBarWindow;
    private AdminSearchResultsPanel adminSearchResultsPanel;

    public GetAdminUsersCallback(AdminSearchResultsPanel theUsersListPanel) {
        this(theUsersListPanel, null);
    }

    public GetAdminUsersCallback(AdminSearchResultsPanel theadminSearchResultsPanel, ProgressBarWindow theProgressBarWindow) {
        adminSearchResultsPanel = theadminSearchResultsPanel;
        progressBarWindow = theProgressBarWindow;
        progressBarWindow.show();
    }

    /**
     * Populate the list grid
     */
    public void onSuccess(SearchResults<UserDTO> searchResults) {
       
       
        
        userDTOs = searchResults.getData();
              
        ListGridRecord[] data = new ListGridRecord[userDTOs.size()];

        // populate the list grid
        for (int index = 0; index < userDTOs.size(); index++) {
            UserDTO theUserDTO = userDTOs.get(index);
            data[index] = PropertyUtils.convert(index, theUserDTO);
        }

        int totalCount = searchResults.getTotalCount();
     
        adminSearchResultsPanel.populateListGrid(data, totalCount);

        ListGrid listGrid = adminSearchResultsPanel.getListGrid();
        listGrid.markForRedraw();

        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }
    }

    /**
     *
     * @param exc
     */
    public void onFailure(Throwable exc) {
       // System.out.println("onFailure ::");
        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

        SC.warn("No Record Found.");
    }

    public List<UserDTO> getUserDTOs() {
        return userDTOs;
    }
}
