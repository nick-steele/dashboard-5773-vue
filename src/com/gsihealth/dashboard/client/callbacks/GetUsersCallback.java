package com.gsihealth.dashboard.client.callbacks;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.util.PropertyUtils;
import com.gsihealth.dashboard.client.admintools.user.UserListPanel;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import java.util.List;

/**
 * Receives a list of users and populates a list grid
 * 
 * @author Chad Darby
 */
public class GetUsersCallback implements AsyncCallback<SearchResults<UserDTO>> {

    private UserListPanel usersListPanel;
    private List<UserDTO> userDTOs;
    private ProgressBarWindow progressBarWindow;

    public GetUsersCallback(UserListPanel theUsersListPanel) {
        this(theUsersListPanel, null);
    }

    public GetUsersCallback(UserListPanel theUsersListPanel, ProgressBarWindow theProgressBarWindow) {
        usersListPanel = theUsersListPanel;
        progressBarWindow = theProgressBarWindow;
    }

    /**
     * Populate the list grid
     */
    public void onSuccess(SearchResults<UserDTO> searchResults) {

        
        userDTOs = searchResults.getData();
        int totalCount = 0;
        if (searchResults != null) {
            totalCount = searchResults.getTotalCount();
        }

        

        if (!userDTOs.isEmpty()) {
           
            ListGridRecord[] data = new ListGridRecord[userDTOs.size()];

            // populate the list grid
            for (int index = 0; index < userDTOs.size(); index++) {

                UserDTO theUserDTO = userDTOs.get(index);
                // System.out.println("userDTO: " + theUserDTO.toString());
                data[index] = PropertyUtils.convert(index, theUserDTO);
            }

            usersListPanel.populateListGrid(data, userDTOs, totalCount);

            ListGrid listGrid = usersListPanel.getListGrid();
            listGrid.markForRedraw();
        } else {
           
            usersListPanel.clearListGrid();

            SC.warn("Search did not return any results. Please modify your search criteria.");

        }
        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

    }

    /**
     *
     * @param exc
     */
    public void onFailure(Throwable exc) {
        System.out.println("onFailure::");
        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

        SC.warn("Error retrieving administrators.");
    }

    public List<UserDTO> getUserDTOs() {
        return userDTOs;
    }
}
