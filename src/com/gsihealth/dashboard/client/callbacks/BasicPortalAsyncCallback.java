package com.gsihealth.dashboard.client.callbacks;

import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.smartgwt.client.util.SC;

/**
 * Basic call back that hides the progress bar and shows the confirmation message
 * 
 * @author Chad Darby
 */
public class BasicPortalAsyncCallback<T> extends PortalAsyncCallback<T> {

    protected String confirmationMessage;

    public BasicPortalAsyncCallback() {

    }
    
    public BasicPortalAsyncCallback(String theConfirmationMessage) {
        this(null, theConfirmationMessage);
    }

    public BasicPortalAsyncCallback(ProgressBarWindow theProgressBarWindow, String theConfirmationMessage) {
        super(theProgressBarWindow);
        confirmationMessage = theConfirmationMessage;
    }

    @Override
    public void onSuccess(Object t) {

        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

        SC.say(confirmationMessage);
    }
}
