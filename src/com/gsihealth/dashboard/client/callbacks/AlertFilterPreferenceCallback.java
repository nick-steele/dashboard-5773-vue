/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.callbacks;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.preferences.AlertFilterModelWindow;
import com.gsihealth.dashboard.client.preferences.AlertFilterModelWindowUtils;
import com.gsihealth.dashboard.entity.dto.AlertFilterPreferenceDTO;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import java.util.List;

/**
 *
 * @author Vinay 
 */
public class AlertFilterPreferenceCallback implements AsyncCallback<List<AlertFilterPreferenceDTO>> {
    private String alertName;
    private long userId;

    public AlertFilterPreferenceCallback(String alertName, long userId) {
         this.alertName = alertName;
         this.userId = userId;
    }
         
    @Override
        public void onSuccess(List<AlertFilterPreferenceDTO> alerts) {
            AlertFilterModelWindow alertFilterModelWindow = new AlertFilterModelWindow(this.alertName, this.userId);
            ListGridRecord[] records = AlertFilterModelWindowUtils.convertDTOListToRecordList(alerts);
            alertFilterModelWindow.populateListGrid(records);
            alertFilterModelWindow.show();
        }

        @Override
        public void onFailure(Throwable thrwbl) {
            SC.warn("Error retrieving alert.");

        }
}
