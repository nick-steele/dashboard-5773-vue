package com.gsihealth.dashboard.client.callbacks;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.admintools.organization.OrganizationListPanel;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.util.PropertyUtils;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import java.util.List;

/**
 * Receives a list of organizations and populates a list grid
 * 
 * @author Satyendra Singh
 */
public class GetOrganizationsCallback implements AsyncCallback<SearchResults<OrganizationDTO>> {

    private OrganizationListPanel organizationsListPanel;
    private List<OrganizationDTO> orgDTOs;
    private ProgressBarWindow progressBarWindow;

    public GetOrganizationsCallback(OrganizationListPanel theOrganizationsListPanel) {
        this(theOrganizationsListPanel, null);
    }

    public GetOrganizationsCallback(OrganizationListPanel theOrganizationsListPanel, ProgressBarWindow theProgressBarWindow) {
        organizationsListPanel = theOrganizationsListPanel;
        progressBarWindow = theProgressBarWindow;
    }

    /**
     * Populate the list grid
     */
    public void onSuccess(SearchResults<OrganizationDTO> searchResults) {

        int totalCount = 0;
        orgDTOs = searchResults.getData();

        if (searchResults != null) {
            totalCount = searchResults.getTotalCount();
        }

        if (!orgDTOs.isEmpty()) {
            ListGridRecord[] data = new ListGridRecord[orgDTOs.size()];

            // populate the list grid
            for (int index = 0; index < orgDTOs.size(); index++) {
                OrganizationDTO theOrgDTO = orgDTOs.get(index);
                data[index] = PropertyUtils.convert(index, theOrgDTO);
            }

            //organizationsListPanel.populateListGrid(data,totalCount);
            organizationsListPanel.populateListGrid(data, orgDTOs, totalCount);



            ListGrid listGrid = organizationsListPanel.getListGrid();
            listGrid.markForRedraw();

        } else {
            organizationsListPanel.clearListGrid();

            SC.warn("Search did not return any results. Please modify your search criteria.");
        }
        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

    }

    /**
     *
     * @param exc
     */
    public void onFailure(Throwable exc) {

        if (progressBarWindow != null) {
            progressBarWindow.hide();
        }

        SC.warn("Error retrieving organizations.");
    }

    public List<OrganizationDTO> getOrgDTOs() {
        return orgDTOs;
    }
}
