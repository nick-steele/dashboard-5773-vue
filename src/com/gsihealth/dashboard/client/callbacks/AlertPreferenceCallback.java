package com.gsihealth.dashboard.client.callbacks;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.preferences.PreferencesWindow;
import com.gsihealth.dashboard.client.service.SubcriptionManagerAlertService;
import com.gsihealth.dashboard.client.service.SubcriptionManagerAlertServiceAsync;
import com.gsihealth.dashboard.common.ApplicationInfo;
import com.gsihealth.dashboard.entity.dto.AlertPreferenceDTO;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Chad.Darby
 */
public class AlertPreferenceCallback extends RetryActionCallback<List<AlertPreferenceDTO>> {

    private static SubcriptionManagerAlertServiceAsync alertService = GWT.create(SubcriptionManagerAlertService.class);
    private String email;
    private long communityId;
    private Map<String, ApplicationInfo> applicationInfoMap;
    private long userId;

    public AlertPreferenceCallback(String email, long communityId, Map<String, ApplicationInfo> applicationInfoMap, long userId) {
        this.email = email;
        this.applicationInfoMap = applicationInfoMap;
        this.communityId = communityId;
        this.userId = userId;
    }

    @Override
    public void attempt() {
        alertService.getAlerts(communityId, applicationInfoMap, userId, this);
    }

    @Override
    public void onCapture(List<AlertPreferenceDTO> docs) {
        PreferencesWindow window = new PreferencesWindow(email);
        window.populate(docs);
        window.show();
    }
}
