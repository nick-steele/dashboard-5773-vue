package com.gsihealth.dashboard.client.callbacks;

import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.service.ReferenceDataServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.common.ReferenceData;

/**
 *
 * @author Chad Darby
 */
public class GetReferenceDataCallbackAction extends RetryActionCallback<ReferenceData> {

    private static ReferenceDataServiceAsync referenceDataService;
    private long communityId;
    
    public GetReferenceDataCallbackAction(Long theCommunityId, ReferenceDataServiceAsync theReferenceDataService) {
        communityId = theCommunityId;
        referenceDataService = theReferenceDataService;
    }
    
    @Override
    public void attempt() {
        referenceDataService.getReferenceData(communityId, this);
    }

    @Override
    public void onCapture(ReferenceData data) {
        ApplicationContext context = ApplicationContext.getInstance();

        context.putAttribute(Constants.ENROLLMENT_STATUS_CODES_KEY, data.getEnrollmentStatuses());
        
        context.putAttribute(Constants.PROGRAM_NAME_CODES_KEY, data.getProgramNames());
        context.putAttribute(Constants.PROGRAM_LEVEL_CONSENT_STATUS_CODES_KEY, data.getProgramLevels());
        context.putAttribute(Constants.PROGRAM_LEVEL_CONSENT_STATUS_MAP_CODES_KEY, data.getProgramLevelMap());


        context.putAttribute(Constants.USER_CREDENTIAL_CODES_KEY, data.getUserCredentials());
        context.putAttribute(Constants.USER_PREFIX_CODES_KEY, data.getUserPrefixes());
        
        context.putAttribute(Constants.POSTAL_STATE_CODES_KEY, data.getPostalStateCodes());
        context.putAttribute(Constants.GENDER_CODES_KEY, data.getGenderCodes());

        context.putAttribute(Constants.RACE_CODES_KEY, data.getRaceCodes());
        context.putAttribute(Constants.ETHNIC_CODES_KEY, data.getEthnicCodes());
        context.putAttribute(Constants.LANGUAGE_CODES_KEY, data.getLanguageCodes());
        
//        context.putAttribute(com.gsihealth.dashboard.common.Constants.SOUTHWEST_BROOKLYN_ORG_ID_KEY, data.getSouthwestBrooklynOrgId());
    
        context.putAttribute(Constants.APPLICATION_INFO_MAP, data.getApplicationInfoMap());
        
        // reasons for refusal
        context.putAttribute(Constants.ENROLLMENT_REASONS_FOR_INACTIVATION_KEY, data.getEnrollmentReasonsForInactivation());
        
        context.putAttribute(Constants.PATIENT_RELATIONSHIP_CODES_KEY, data.getPatientRelationship());   
        // Disenrollment codes
        context.putAttribute(Constants.ALL_DISENROLLMENT_CODES, data.getAllDisenrollmentReasons());
        context.putAttribute(Constants.DISENROLLMENT_CODES_FOR_REFUSAL, data.getDisenrollmentReasonsForRefusal());
        context.putAttribute(Constants.DISENROLLMENT_CODES_FOR_INACTIVATION, data.getDisenrollmentReasonsForInactivation());
        // Duplicate Careteam Roles
        context.putAttribute(Constants.DUPLICATE_CARETEAM_ROLES, data.getDuplicateCareteamRoles());
        context.putAttribute(Constants.REQUIRED_CARETEAM_ROLES, data.getRequiredCareteamRoles());
        
        // payer class
        context.putAttribute(Constants.PAYER_CLASS, data.getPayerClass());
        
         // payer plan
        context.putAttribute(Constants.PAYER_PLAN, data.getPayerPlan());
        
        // EULA
        context.putAttribute(Constants.EULA, data.getEula());

        // access levels
        context.putAttribute(Constants.ACCESS_LEVEL_MAP, data.getAccessLevelMap());
        
        //PROGRAM_HEALTH_HOME_KEY
        context.putAttribute(Constants.PROGRAM_HEALTH_HOME_KEY, data.getProgramHealthHome());
        
       // PROGRAM_NAME_HEALTHHOME_KEY
        context.putAttribute(Constants.PROGRAM_NAME_HEALTHHOME_KEY, data.getProgramNameHealthHome());
        
        //Roles 
        context.putAttribute(Constants.USER_ROLE_KEY, data.getUserRole());
        
//        // IN_PROVIDER_NAME_CODES_KEY
//        context.putAttribute(Constants.IN_PROVIDER_NAME_CODES_KEY, data.getInProviderName());
//        
//         // OUT_PROVIDER_NAME_CODES_KEY
//        context.putAttribute(Constants.OUT_PROVIDER_NAME_CODES_KEY, data.getOutProviderName());

        context.putAttribute(Constants.PROGRAM_NAME_SELECT_KEY, data.getProgramNameMap());
        
        //PATIENT_STATUS_CODES_KEY
        context.putAttribute(Constants.PATIENT_STATUS_CODES_KEY, data.getPatientStatus());
        
        context.putAttribute(Constants.PROGRAM_HEALTH_HOME_CODES_KEY, data.getHealthHomeCodes());
        
        context.putAttribute(Constants.TERMINATION_REASON_CODES_KEY, data.getTerminationReasonCodes());
        
        context.putAttribute(Constants.TERMINATION_REASON_FOR_STATUS_KEY, data.getReasonForPatientStatus());
        
        context.putAttribute(Constants.HH_REQUIRED_FOR_PROGRAM, data.getHhRequiredForProgram());
        
        context.putAttribute(Constants.HEALTH_HOME_FOR_PROGRAM_KEY, data.getHealthHomeForProgram());
        
        context.putAttribute(Constants.PROGRAM_DELETE_REASON_CODES_KEY, data.getDeleteReasons()); 
        
        context.putAttribute(Constants.PATIENT_STATUS_KEY, data.getStatusWithReason());
        
        context.putAttribute(Constants.SAMHSA, data.getSamhsa());
                
        context.putAttribute(Constants.ACIVITY_NAMES_KEY, data.getActivityNamesMap());
        
        //JIRA 1023
        context.putAttribute(Constants.PROGRAM_FOR_HEALTH_HOME_CODES_KEY, data.getProgramForHealthHomeCodes());

        context.putAttribute(Constants.COUNTY, data.getCounty());
    }
}
