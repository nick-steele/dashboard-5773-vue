package com.gsihealth.dashboard.client.callbacks;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;

/**
 *
 * @author Chad Darby
 */
public class GetOrganizationNameCallback implements AsyncCallback<String> {

    private StaticTextItem orgNameStaticTextItem;
    
    public GetOrganizationNameCallback(StaticTextItem theStaticTextItem) {
        orgNameStaticTextItem = theStaticTextItem;
    }

    public void onSuccess(String data) {
        orgNameStaticTextItem.setValue(data);
    }

    public void onFailure(Throwable exc) {
        SC.warn("Error reading organization name.");
    }


}
