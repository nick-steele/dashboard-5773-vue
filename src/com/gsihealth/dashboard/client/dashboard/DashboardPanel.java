package com.gsihealth.dashboard.client.dashboard;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.RootPanel;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.WindowManager;
import com.gsihealth.dashboard.client.TaskList.TaskListPanel;
import com.gsihealth.dashboard.client.careplan.AppCareplanPanel;
import com.gsihealth.dashboard.client.admintools.AdminToolsPanel;
import com.gsihealth.dashboard.client.alert.AlertEvent;
import com.gsihealth.dashboard.client.alert.AlertEventHandler;
import com.gsihealth.dashboard.client.alert.AlertPanel;
import com.gsihealth.dashboard.client.careteam.CareTeamPanel;
import com.gsihealth.dashboard.client.common.CustomButtonPanel;
import com.gsihealth.dashboard.client.common.DashboardTitles;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.gsihealth.dashboard.client.enrollment.EnrollmentPanel;
import com.gsihealth.dashboard.client.eventbus.EventBusUtils;
//import com.gsihealth.dashboard.client.mypatientlist.MyPatientListPanel;
import com.gsihealth.dashboard.client.patientdrilldown.PopupCareBookClickCallback;
import com.gsihealth.dashboard.client.patientdrilldown.PopupEnrollmentAddCallback;
import com.gsihealth.dashboard.client.patientdrilldown.PopupEnrollmentEditCallback;
import com.gsihealth.dashboard.client.patientsearch.PatientSearchCallback;
import com.gsihealth.dashboard.client.reports.MainReportPanel;
import com.gsihealth.dashboard.client.service.*;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.AccessLevelConstants;
import com.gsihealth.dashboard.common.ClientApplicationProperties; // Redux
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.common.util.AppConfigConstants;
import com.gsihealth.dashboard.entity.dto.ApplicationDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.UserAlertDTO;
import com.gsihealth.dashboard.redux.Redux;
import com.smartgwt.client.types.AnimationEffect;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.types.Visibility;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;

import static com.gsihealth.dashboard.common.Constants.DESTINATION_HTTP_PARAM_NAME;
import static com.gsihealth.dashboard.common.Constants.PATIENT_ID_HTTP_PARAM_NAME;

/**
 * @author Vishal (Off.)
 *
 */
public class DashboardPanel extends VLayout {

    private CustomButtonPanel alertButton;
    private CustomButtonPanel messagesButton;
    private CustomButtonPanel tasksButton;
    private Timer alertTimer;
    private Timer messagesTimer;
    private Timer tasksTimer;
    private final AlertServiceAsync alertService = GWT.create(AlertService.class);
    private final MessageCountServiceAsync messagesService = GWT.create(MessageCountService.class);
    private final TaskCountServiceAsync tasksService = GWT.create(TaskCountService.class);
    private final static EnrollmentServiceAsync enrollmentService = GWT.create(EnrollmentService.class);
    private long userId;
    private static String emailAddress;
    private HandlerRegistration handlerRegistration;
    private HandlerRegistration popupHandlerRegistration;
    private boolean receivedFirstAlerts;
    private Timer alertPopupTimer;
    private Label alertPopupLabel;
    private Timer messagesPopupTimer;
    private Label messagesPopupLabel;
    private Timer tasksPopupTimer;
    private Label tasksPopupLabel;
    private List<UserAlertDTO> oldAlerts;
    private int oldAlertsTotalCount;
    private int oldMessagesTotalCount;
    private int oldTasksTotalCount;
    private int currentMessagesTotalCount;
    private static DateTimeFormat dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy h:mm a");
    private int pollingIntervalInSeconds;
    private boolean pollingEnabled;
    //

    @Deprecated
    private Map<Long, ClickHandler> appClickHandlerMap;
    @Deprecated
    private Map<Long, String> appIconMap;
    private boolean receivedFirstMessage;
    private boolean receivedFirstTask;

    // Redux
    private ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
    private int userInterfaceType = props.getUserInterfaceType();

    /**
     *
     */
    public DashboardPanel() {

        Redux.log("DashboardPanel constructor was called");
        // TODO: Deprecated
//        setupAppConfigReferenceData();

        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        userId = loginResult.getUserId();
        emailAddress = loginResult.getEmail();

        pollingEnabled = loginResult.isPoolingEnabled();
        pollingIntervalInSeconds = loginResult.getPollingIntervalInSeconds();

        // Redux
        prepareGUI();

//        alertPopupTimer = new ShowAlertNoteTimer();
//        messagesPopupTimer = new ShowMessagesNoteTimer();
//        tasksPopupTimer = new ShowTasksNoteTimer();
//        loadMessagesCount();
        loadUserAlerts();
//        loadTasksCount();
    }

    native void consoleLog(String message) /*-{
      console.log( message );
    }-*/;

    /*redu
     * Redux
     * Refactored existing code to bundle if blocks
     */
    public void prepareGUI() {

        // If Redux is our UI, build it...
        if (Redux.useRedux()) {
            Redux.log("Building Redux");
            buildGUIRedux();
            // Otherwise, build GWT only...
        } else {
            Redux.log("Building Classic");
            setupAppConfigReferenceData();
            buildGUI();
        }
    }

    // This builds the existing GUI so it plays nice with Redux...
    public void buildGUIRedux() {
        // Creates JSNI export functions (So Angular can call GWT)...
        declareNonStaticJSNI(this);
        FlowPanel panel = new FlowPanel();

        setHeight100();
        setWidth100();

        panel.getElement().setId("appHome");
        panel.getElement().setAttribute("dynamic", "app.html.home");
        addMember(panel);

        panel.setWidth("100%");
        panel.setHeight("100%");

        setBorder("1px solid red");

        // TODO: testing hidden but funtional old mypatientlist.. 2/12/16
        // TODO: Deprecated
//        MyPatientListPanel myPatientListPanel;
//        myPatientListPanel = new MyPatientListPanel();
//
//        // store in app context for later user
//        ApplicationContext.getInstance().putAttribute(Constants.MY_PATIENT_LIST_PANEL_KEY, myPatientListPanel);
        // Get a list of user apps...
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        List<ApplicationDTO> userApps = loginResult.getUserApps();

        // Loop through them and do something special for apps with notifications
        for (ApplicationDTO tempApp : userApps) {
            if (isAlertApp(tempApp)) {
                subscribeToBus();
                alertTimer = new AlertTimer();
                startAlertTimer();
            }
//            else if (isMessagesApp(tempApp)) {
//                messagesTimer = new MessageTimer();
//                startMessageTimer();
//            } else if (isTaskManagerApp(tempApp)) {
//                tasksTimer = new TasksTimer();
//                startTasksTimer();
//            }
        }
    }

    // This builds the existing GUI...
    @Deprecated
    public void buildGUI() {

        Canvas clinicalWorkflowDashboard = buildClinicalWorkflowApps();

        setHeight100();
        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setWidth(50);

        HLayout layout = new HLayout();

        // TODO: Deprecated
//        MyPatientListPanel myPatientListPanel;
//        myPatientListPanel = new MyPatientListPanel();
//        layout.setMembers(clinicalWorkflowDashboard, spacer, myPatientListPanel);
        addMember(layout);

        // store in app context for later user
//        ApplicationContext.getInstance().putAttribute(Constants.MY_PATIENT_LIST_PANEL_KEY, myPatientListPanel);
    }

    @Deprecated
    private Canvas buildClinicalWorkflowApps() {
        VLayout vLayout = new VLayout();
        HLayout layout = new HLayout();

        vLayout.setWidth(600);
        vLayout.setHeight(175);

        vLayout.setBorder("1px solid lightgray");

        LoginResult loginResult = ApplicationContextUtils.getLoginResult();

        List<ApplicationDTO> userApps = loginResult.getUserApps();

        for (ApplicationDTO tempApp : userApps) {

            long tempAppId = tempApp.getApplicationId();

            if (isAlertApp(tempApp)) {
                // alert app
                showAlertIcon(layout);
            } else if (isMessagesApp(tempApp)) {
                showMessagesIcon(layout);
            } else if (isTaskManagerApp(tempApp)) {
                showTasksIcon(layout);
            } else {
                CustomButtonPanel tempButton = new CustomButtonPanel(tempApp.getScreenName());

                // @TODO: hack for sales-demo: Task Manager app
                if (tempAppId == AppConfigConstants.PATIENT_SUMMARY_APP_ID) {
                    String screenName = tempApp.getScreenName();
                    String url = tempApp.getExternalApplicationUrl();
                    appClickHandlerMap.put(AppConfigConstants.PATIENT_SUMMARY_APP_ID, new PatientSummaryClickHandler(screenName, url));
                }

                String iconStyle = this.appIconMap.get(tempAppId);
                tempButton.setIconStyle(iconStyle);

                ClickHandler handler = this.appClickHandlerMap.get(tempAppId);
                tempButton.addClickHandler(handler);

                layout.addMember(tempButton);
            }
        }

        Label title = new Label("Health Home Dashboard Apps");
        title.setHeight(20);
        title.setStyleName("dashboardTitleLabel");
        vLayout.addMember(title);
        vLayout.addMember(layout);

        Canvas canvas = new Canvas();
        canvas.setAutoHeight();
        canvas.setAutoWidth();
        canvas.addChild(vLayout);

        // The "toaster" popup for the bottom right corner
        buildAlertToasterPopupLabel();
        buildMessageToasterPopupLabel();

        return canvas;
    }

    protected boolean isAlertApp(ApplicationDTO tempApp) {
        return tempApp.getApplicationId() == AppConfigConstants.ALERTS_APP_ID;
    }

    protected void showAlertIcon(HLayout layout) {
        // show alert icon
        alertButton = new CustomButtonPanel(DashboardTitles.ALERTS);
        alertButton.setIconStyle("alerts-icon");
        layout.addMember(alertButton);

        alertButton.addClickHandler(new AlertClickHandler());

        // store in app context
        ApplicationContext context = ApplicationContext.getInstance();
        context.putAttribute(Constants.ALERT_BUTTON_KEY, alertButton);

        // setup alerts bus and alertings
        subscribeToBus();
        alertTimer = new AlertTimer();
        startAlertTimer();
    }

    /**
     * The "toaster" popup for the bottom right corner
     *
     * @throws IllegalStateException
     */
    protected void buildAlertToasterPopupLabel() throws IllegalStateException {
        alertPopupLabel = new Label();
        alertPopupLabel.setShowEdges(true);
        alertPopupLabel.setEdgeSize(5);

        alertPopupLabel.setPadding(10);
        alertPopupLabel.setWidth(150);
        alertPopupLabel.setHeight(75);
        alertPopupLabel.setBackgroundColor("#");
        alertPopupLabel.setVisibility(Visibility.HIDDEN);
        alertPopupLabel.setValign(VerticalAlignment.CENTER);
        alertPopupLabel.setAnimateTime(1500); // milliseconds
    }

    protected boolean isMessagesApp(ApplicationDTO tempApp) {
        return tempApp.getApplicationId() == AppConfigConstants.MESSAGES_APP_ID;
    }

    protected void showMessagesIcon(HLayout layout) {
        // show alert icon
        messagesButton = new CustomButtonPanel(DashboardTitles.MESSAGES);
        messagesButton.setIconStyle("message-icon");
        layout.addMember(messagesButton);

        messagesButton.addClickHandler(new MessageClickHandler());

        // store in app context
        // ApplicationContext context = ApplicationContext.getInstance();
        // context.putAttribute(Constants.ALERT_BUTTON_KEY, alertButton);
        // setup alerts bus and alertings
        // subscribeToBus();
//        messagesTimer = new MessageTimer();
//        startMessageTimer();
    }

    /**
     * The "toaster" popup for the bottom right corner
     *
     * @throws IllegalStateException
     */
    protected void buildMessageToasterPopupLabel() throws IllegalStateException {
        messagesPopupLabel = new Label();
        messagesPopupLabel.setShowEdges(true);
        messagesPopupLabel.setEdgeSize(5);

        messagesPopupLabel.setPadding(10);
        messagesPopupLabel.setWidth(150);
        messagesPopupLabel.setHeight(75);
        messagesPopupLabel.setBackgroundColor("#");
        messagesPopupLabel.setVisibility(Visibility.HIDDEN);
        messagesPopupLabel.setValign(VerticalAlignment.CENTER);
        messagesPopupLabel.setAnimateTime(1500); // milliseconds
    }

    protected boolean isTaskManagerApp(ApplicationDTO tempApp) {
        return tempApp.getApplicationId() == AppConfigConstants.TASK_LIST_APP_ID;
    }

    protected void showTasksIcon(HLayout layout) {

        tasksButton = new CustomButtonPanel(DashboardTitles.TASK_LIST);
        tasksButton.setIconStyle("patient-summary-icon");
        layout.addMember(tasksButton);

        tasksButton.addClickHandler(new TaskListClickHandler());

//        tasksTimer = new TasksTimer();
//        startTasksTimer();
    }

    /**
     * Set up the app configuration details
     *
     */
    @Deprecated
    private void setupAppConfigReferenceData() {

        // key: app id, value: click handler implementation
        appClickHandlerMap = new HashMap<Long, ClickHandler>();

        appClickHandlerMap.put(AppConfigConstants.ENROLLMENT_APP_ID, new EnrollmentClickHandler());
        appClickHandlerMap.put(AppConfigConstants.CARETEAM_APP_ID, new CareTeamClickHandler());
        appClickHandlerMap.put(AppConfigConstants.CAREPLAN_APP_ID, new CarePlanClickHandler());
        appClickHandlerMap.put(AppConfigConstants.ADMIN_APP_ID, new AdminClickHandler());
        appClickHandlerMap.put(AppConfigConstants.REPORTS_APP_ID, new ReportsClickHandler());
        appClickHandlerMap.put(AppConfigConstants.MESSAGES_APP_ID, new MessageClickHandler());
        appClickHandlerMap.put(AppConfigConstants.PATIENT_ENGAGEMENT_APP_ID, new AvadoClickHandler());
        appClickHandlerMap.put(AppConfigConstants.CAREBOOK_APP_ID, new CarebookClickHandler());
        appClickHandlerMap.put(AppConfigConstants.POPULATION_MANAGER_APP_ID, new PopulationManagerClickHandler());
        appClickHandlerMap.put(AppConfigConstants.UHC_ENROLLMENT_REPORT_APP_ID, new UhcEnrollmentReportClickHandler());
        appClickHandlerMap.put(AppConfigConstants.PATIENT_LIST_UTILITY_APP_ID, new PatientListClickHandler());
        appClickHandlerMap.put(AppConfigConstants.TASK_LIST_APP_ID, new TaskListClickHandler());

        // key: app id,  value: icon info
        appIconMap = new HashMap<Long, String>();

        appIconMap.put(AppConfigConstants.ENROLLMENT_APP_ID, "enrollments-icon");
        appIconMap.put(AppConfigConstants.CARETEAM_APP_ID, "careteam-icon");
        appIconMap.put(AppConfigConstants.CAREPLAN_APP_ID, "treat-icon");
        appIconMap.put(AppConfigConstants.ADMIN_APP_ID, "admin-icon");
        appIconMap.put(AppConfigConstants.REPORTS_APP_ID, "reports-icon");
        appIconMap.put(AppConfigConstants.MESSAGES_APP_ID, "message-icon");
        appIconMap.put(AppConfigConstants.PATIENT_ENGAGEMENT_APP_ID, "patient-engagement-icon");
        appIconMap.put(AppConfigConstants.CAREBOOK_APP_ID, "care-book-icon");
        appIconMap.put(AppConfigConstants.POPULATION_MANAGER_APP_ID, "population-manager-icon");
        appIconMap.put(AppConfigConstants.UHC_ENROLLMENT_REPORT_APP_ID, "uhc-enrollment-report-icon");
        appIconMap.put(AppConfigConstants.PATIENT_LIST_UTILITY_APP_ID, "population-manager-icon");
        appIconMap.put(AppConfigConstants.TASK_LIST_APP_ID, "patient-summary-icon");

        // for demo only
        appIconMap.put(AppConfigConstants.PATIENT_SUMMARY_APP_ID, "patient-summary-icon");
    }

    class CareTeamClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            handleCareTeamClick();
        }
    }

    class EnrollmentClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            handleEnrollmentClick();
        }
    }

    class AdminClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            handleAdminClick();
        }
    }

    class ReportsClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            handleReportsClick();
        }
    }

    class CarePlanClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            showTreatWindow(Constants.CAREPLAN_HOMEPAGE, "", false);
        }
    }

    class AvadoClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            showAvadoWindow();
        }
    }

    class AlertClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            handleAlertsClick();
        }
    }

    // Redux - these functions are availble to angular...
    public static void handleAdminClick() {
        WindowManager.addWindow(DashboardTitles.ADMIN, new AdminToolsPanel());
    }

    public static void handleEnrollmentClick() {
        WindowManager.updateWindow(DashboardTitles.ENROLLMENT, new EnrollmentPanel());
    }

    public static void handleCareTeamClick() {
        WindowManager.addWindow(DashboardTitles.CARE_TEAM_ASSIGNMENT, new CareTeamPanel());
    }

    public static void handleReportsClick() {
        WindowManager.addWindow(DashboardTitles.REPORTS, new MainReportPanel());
    }

    public static void handleTaskListClick() {
        Redux.note("handleTaskListClick() was called");
        WindowManager.addWindow(DashboardTitles.TASK_LIST, new TaskListPanel());
    }

    public static void handleAppCareplanClick() {
        Redux.note("handleAppCareplanClick() was called");
        WindowManager.addWindow(DashboardTitles.TASK_LIST, new AppCareplanPanel());
    }

    public static void handleAlertsClick() {
        AlertPanel alertPanel = new AlertPanel();
        WindowManager.addWindow(DashboardTitles.ALERTS, alertPanel);
        alertPanel.gotoPage(1);
    }

    public static void globalSearch(String name) {
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();

        String email = loginResult.getEmail();
        Long communityId = loginResult.getCommunityId();

        if (StringUtils.isBlank(name)) {
            SC.warn("You must enter Name for Search");
        } else {
            SearchCriteria searchCriteria = new SearchCriteria();
            String nameParts[] = name.split("\\s+");

            searchCriteria.setFirstName(nameParts[0]);
            if (nameParts.length > 1 && StringUtils.isNotBlank(nameParts[1])) {
                searchCriteria.setLastName(nameParts[1]);
            }
            searchCriteria.setIsCarebookPatientSearch(true);
            searchCriteria.setSearchAllPatients(true);
            DashBoardApp.getCurrentWindowManager().findDashboardPatientSearch(1, communityId, loginResult.getUserId(), searchCriteria);
        }

    }

    public static void handleSelectedPatientInSearch(String patientId) {
        final LoginResult loginResult = ApplicationContextUtils.getLoginResult();

        final String selectedPatientId = patientId;
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setPatientID(selectedPatientId);
        searchCriteria.setIsCarebookPatientSearch(true);
        searchCriteria.setSearchAllPatients(true);
        Redux.log("handleSelectedPatientInSearch start:" + selectedPatientId + " " + loginResult.getCommunityId());

        enrollmentService.findPatients(loginResult.getCommunityId(), searchCriteria, false, loginResult.getOrganizationId(), -1, -1, loginResult.getUserId(), new PatientSearchCallback() {
            @ Override
            public void onSuccess(SearchResults<PersonDTO> users) {

                if((users.getData() == null) ||(users.getData() != null && users.getData().size() == 0 )){

                    Redux.jsEvent("local:appCareBookSearch:handleCarebookSearchError", selectedPatientId);

                }
                else {
                    PersonDTO thePerson = users.getData().get(0);

                    enrollmentService.updatePatientBySelfAssertion(thePerson, new AsyncCallback<PersonDTO>() {
                        @Override
                        public void onSuccess(PersonDTO updatedPersonDtoAfterSelfAssertion) {

                            Redux.jsEvent("local:appCareBookSearch:toCareBookByPatient", selectedPatientId);
                        }

                        @Override
                        public void onFailure(Throwable cause) {

                            //Redux.jsEvent("local:appCareBookSearch:initIframe", selectedPatientId);
                            Redux.jsEvent("local:appCareBookSearch:handleCarebookSearchError", selectedPatientId);

                        }
                    });
                }
            }

            @Override
            public void onFailure(Throwable thrwbl) {

                Redux.jsEvent("local:appCareBookSearch:handleCarebookSearchError", selectedPatientId);
            }
        });
        Redux.log("handleSelectedPatientInSearch stop:");
    }

    public static void searchPatientsByFirstnLastName(String firstName, String lastName) {
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();

        String email = loginResult.getEmail();
        Long communityId = loginResult.getCommunityId();

        SearchCriteria criteria = new SearchCriteria();

        if (StringUtils.isNotBlank(firstName)) {
            criteria.setFirstName(firstName);
        }

        if (StringUtils.isNotBlank(lastName)) {
            criteria.setLastName(lastName);
        }

        criteria.setIsCarebookPatientSearch(true);
        criteria.setSearchAllPatients(true);
        DashBoardApp.getCurrentWindowManager().findDashboardPatientSearch(1, communityId, loginResult.getUserId(), criteria);
    }


    public static void appCarebookPatientOpen(String patientId) {
        handlePopupEvent(patientId, new PopupCareBookClickCallback());
    }

    public static void appEnrollmentEditOpen(String patientId) {
        handlePopupEvent(patientId, new PopupEnrollmentEditCallback());
    }

    private static void handlePopupEvent(String patientId, PortalAsyncCallback<SearchResults<PersonDTO>> callback) {
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();

        Long communityId = loginResult.getCommunityId();
        Long userOrgId = loginResult.getOrganizationId();
        Long userId = loginResult.getUserId();
        // For power user user organization Id will be used as null as per
        // existing code in Dashboard.
        long userAccessLevelId = loginResult.getAccessLevelId();
        if (userAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
            userOrgId = null;
        }

        if (StringUtils.isBlank(patientId)) {
            SC.warn("patientId is invalid");
        } else {
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setPatientID(patientId);
            searchCriteria.setIsCarebookPatientSearch(true);
            searchCriteria.setSearchAllPatients(true);
            enrollmentService.findPatients(communityId, searchCriteria, false, userOrgId, -1, -1, userId, callback);

        }
    }


    public static void appAddPatientOpen(String data) {
        enrollmentService.parseStringToPerson(data, new PopupEnrollmentAddCallback());
    }

    // Redux 
    public static int angularGWT() {
        return 100;
    }

    class CarebookClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            showCarebookAppWindow();
        }
    }

    class PopulationManagerClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            showPopulationManagerWindow();
        }
    }

    class UhcEnrollmentReportClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            // showUhcEnrollmentReportAppWindow();
            showPopulationManagerWindow();
        }
    }

    class PatientListClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            showPatientListAppWindow();
        }
    }

    class TaskListClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            //showTaskListAppWindow();
            handleTaskListClick();
        }
    }

    class MessageClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            showMessageAppWindow();
        }
    }

    class PatientSummaryClickHandler implements ClickHandler {

        String label;
        String appUrl;

        PatientSummaryClickHandler(String theLabel, String theAppUrl) {
            label = theLabel;
            appUrl = theAppUrl;
        }

        public void onClick(ClickEvent event) {

            String features = "location=no,status=yes,scrollbars=yes,width=1200,height=800,resizable=yes";
            String name = "Demo";

            com.google.gwt.user.client.Window.open(appUrl, name, features);
        }
    }

    public int getAngularMessages() {
        return currentMessagesTotalCount;
    }

    //Method which declares non-static method in javascript
    public native void declareNonStaticJSNI(DashboardPanel dashboard) /*-{
        var dashboard = dashboard;
        $wnd.getAngularMessages = function() {
            dashboard.@com.gsihealth.dashboard.client.dashboard.DashboardPanel::getAngularMessages(*);
        }
    }-*/;

    public static void showTreatWindow(String destination, String patientId, boolean reset) throws IllegalStateException {
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();

        String treatUrl = loginResult.getTreatUrl();
        final String treatName = loginResult.getTreatName();
        final String treatFeatures = loginResult.getTreatFeatures();
        final boolean treatRestfulSSOEnabled = loginResult.isTreatRestfulSSOEnabled();
        System.out.println("Treat SSO Enabled Boolean: " + String.valueOf(treatRestfulSSOEnabled));

        if (treatRestfulSSOEnabled) {
            System.out.println("Treat SSO Enabled");
            //change url settings to use  POST to send tokenid to Messages App
            String protocol = com.google.gwt.user.client.Window.Location.getProtocol();
            String hostAndPort = com.google.gwt.user.client.Window.Location.getHost();

            String currentPage = com.google.gwt.user.client.Window.Location.getPath();

            String path = "";
            if (currentPage.startsWith("/dashboard")) {
                path = "/dashboard";
            }

            path += "/login/post_for_treat.jsp";
            String queryString = com.google.gwt.user.client.Window.Location.getQueryString();
            if (!StringUtils.isEmpty(queryString)) {
                queryString = queryString + "&";
            } else {
                queryString = "?";
            }
            queryString = queryString + DESTINATION_HTTP_PARAM_NAME + "=" + destination + "&"
                    + PATIENT_ID_HTTP_PARAM_NAME + "=" + patientId;
            treatUrl = protocol + "//" + hostAndPort + path + queryString;
            System.out.println("Treat URL -" + treatUrl);
        }
        if (reset) {
            Redux.openAppWindow(treatUrl, treatName, treatFeatures);
        } else {
            Redux.showAppWindow(treatUrl, treatName, treatFeatures);
        }

    }

    public static void showAvadoWindow() throws IllegalStateException {
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();

        String avadoUrl = loginResult.getAvadoUrl();
        String avadoName = loginResult.getAvadoName();
        String avadoFeatures = loginResult.getAvadoFeatures();
        String avadoTokenId = loginResult.getToken();
        String avadoUsePost = loginResult.getAvadoUsePost();
        boolean AvadoUsePostFlag = Boolean.parseBoolean(avadoUsePost);

        avadoUrl += "?tokenid=" + avadoTokenId;

        if (AvadoUsePostFlag) {
            //change url settings to use  POST to send tokenid to Avado instead of GET
            String protocol = com.google.gwt.user.client.Window.Location.getProtocol();
            String hostAndPort = com.google.gwt.user.client.Window.Location.getHost();

            String currentPage = com.google.gwt.user.client.Window.Location.getPath();

            String path = "";
            if (currentPage.startsWith("/dashboard")) {
                path = "/dashboard";
            }

            path += "/login/post_for_avado.jsp";
            String queryString = com.google.gwt.user.client.Window.Location.getQueryString();
            avadoUrl = protocol + "//" + hostAndPort + path + queryString;
        }

        com.google.gwt.user.client.Window.open(avadoUrl, avadoName, avadoFeatures);

    }

    public static void showCarebookAppWindow() throws IllegalStateException {

        String carebookURL = ApplicationContextUtils.getCarebookURL();
        String features = "location=no,status=yes,scrollbars=yes,width=1200,height=800,resizable=yes";
        String name = "Care_Book_App";

        Redux.showAppWindow(carebookURL, name, features);
    }

    public static void showUhcEnrollmentReportAppWindow() throws IllegalStateException {

        String carebookURL = ApplicationContextUtils.getUhcEnrollmentAppURL();
        String features = "location=no,status=yes,scrollbars=yes,width=1200,height=800,resizable=yes";
        String name = "UHC_Enrollment_Report";

        Redux.showAppWindow(carebookURL, name, features);
    }

    public static void showPatientListAppWindow() throws IllegalStateException {
        String carebookURL = ApplicationContextUtils.getPatientListUtilityURL();
        String features = "location=no,status=yes,scrollbars=yes,width=1200,height=800,resizable=yes";
        String name = "Patient_List_Utility";

        com.google.gwt.user.client.Window.open(carebookURL, name, features);
    }

    public static void showTaskListAppWindow() throws IllegalStateException {
        String taskListUrl = ApplicationContextUtils.getTaskListAppURL() + emailAddress;
        String features = "location=no,status=yes,scrollbars=yes,width=1200,height=800,resizable=yes";
        String name = "Task_List";

        com.google.gwt.user.client.Window.open(taskListUrl, name, features);
    }

    /*public static void showPopulationManagerWindow() throws IllegalStateException {
        String theUrl = ApplicationContextUtils.getPopulationManagerURL();
        String features = "toolbar=yes,location=yes,status=yes,scrollbars=yes,width=1200,height=800,resizable=yes";
        String name = "Population_Manager";
            
         com.google.gwt.user.client.Window.open(theUrl, name, features);
    }*/
    public static void setPentahoCalled(boolean called){
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        loginResult.setPentahoCalled(called);
    }

    public static void showPopulationManagerWindow() throws IllegalStateException {
        
            LoginResult loginResult = ApplicationContextUtils.getLoginResult();
            String special = loginResult.getSpecial();
            //special = URL.encode(special);
            //special = special.replace("#","%23");
            special = StringUtils.encode(special);
            String user = emailAddress;
            String theUrl = null;
            String features = "toolbar=yes,location=no,status=yes,scrollbars=yes,width=1200,height=800,resizable=yes";
            String name = "Population_Manager";
            if (loginResult.isPentahoCalled()) {
                // theUrl = loginResult.getSsoPentahoServer() +"/pentaho/Home";
                theUrl = loginResult.getPentahoLandingPage();
            } else {
                String server = loginResult.getSsoPentahoServer();
                String[] tokens = server.split(":\\/\\/");
                String protocol = tokens[0];
                String pserver = tokens[1];
                theUrl = ApplicationContextUtils.getPopulationManagerURL();

                theUrl += "?j_username=" + user + "&j_password=" + special + "&protocol=" + protocol + "&server=" + pserver;
                loginResult.setPentahoCalled(true);
            }
          //  SC.warn(theUrl);
            Redux.showAppWindow(theUrl, name, features);
        
    }

    public static void showMessageAppWindow() throws IllegalStateException {

        //change url settings to use  POST to send tokenid to Messages App
        String protocol = com.google.gwt.user.client.Window.Location.getProtocol();
        String hostAndPort = com.google.gwt.user.client.Window.Location.getHost();

        String currentPage = com.google.gwt.user.client.Window.Location.getPath();

        String path = "";
        if (currentPage.startsWith("/dashboard")) {
            path = "/dashboard";
        }

        path += "/login/post_for_messages.jsp";
        String queryString = com.google.gwt.user.client.Window.Location.getQueryString();
        String messageURL = protocol + "//" + hostAndPort + path + queryString;

        Redux.showAppWindow(messageURL, "Message_App", "toolbar=yes,status=yes,scrollbars=yes,width=1200,height=800,resizable=yes");
    }

    class AlertTimer extends Timer {

        public void run() {
            loadUserAlerts();
        }
    }

    public void startAlertTimer() {
        // start timer
        if (pollingEnabled) {
            alertTimer.scheduleRepeating(pollingIntervalInSeconds * 1000);
        }
    }

    public void stopAlertTimer() {
        // stop timer
        if (alertTimer != null) {
            alertTimer.cancel();
        }
    }

    @Deprecated
    class MessageTimer extends Timer {

        public void run() {
            loadMessagesCount();
        }
    }

    @Deprecated
    public void startMessageTimer() {
        // start timer
        if (pollingEnabled) {
            messagesTimer.scheduleRepeating(pollingIntervalInSeconds * 1000);
        }
    }

    @Deprecated
    public void stopMessageTimer() {
        // stop timer
        if (messagesTimer != null) {
            messagesTimer.cancel();
        }
    }

    @Deprecated
    class TasksTimer extends Timer {

        @Override
        public void run() {
            loadTasksCount();
        }
    }

    @Deprecated
    public void startTasksTimer() {
        consoleLog("tasks timer is starting...");
        if (pollingEnabled) {
            tasksTimer.scheduleRepeating(pollingIntervalInSeconds * 1000);
            consoleLog("tasks timer started.");
        }
    }

    @Deprecated
    public void stopTasksTimer() {
        if (tasksTimer != null) {
            tasksTimer.cancel();
        }
    }

    private void loadUserAlerts() {

        alertService.getUserAlertsPartial(userId, new GetAlertsCallback());
    }

    class GetAlertsCallback implements AsyncCallback<SearchResults<UserAlertDTO>> {

        public void onSuccess(SearchResults<UserAlertDTO> searchResults) {
            // fire event
            EventBus eventBus = EventBusUtils.getEventBus();
            eventBus.fireEvent(new AlertEvent(searchResults));
        }

        public void onFailure(Throwable thrwbl) {
            // do nothing...timer will run again
        }
    }

    @Deprecated
    private void loadMessagesCount() {

        messagesService.getUnreadMailCount(new GetMessagesCountCallback());
    }

    @Deprecated
    class GetMessagesCountCallback implements AsyncCallback<Integer> {

        public void onSuccess(Integer count) {
            if (count != null && count == -1) {
                stopMessageTimer();
                count = 0;

            }
            updateMessagesButton(count);

        }

        public void onFailure(Throwable thrwbl) {
            // do nothing...timer will run again
        }
    }

    @Deprecated
    private void loadTasksCount() {
        tasksService.getOverdueTaskCount(new GetTasksCountCallback());
    }

    @Deprecated
    class GetTasksCountCallback implements AsyncCallback<Integer> {

        @Override
        public void onSuccess(Integer count) {
            consoleLog("Overdue tasks were retrieved: count was " + count);

            if (count != null && count == -1) {
                stopTasksTimer();
                count = 0;
            }
            updateTasksButton(count);
        }

        @Override
        public void onFailure(Throwable error) {
            consoleLog("Overdue tasks could not be retrieved because of " + error.getClass() + ": " + error.getMessage());
            updateTasksButton(0);
        }
    }

    /**
     * Subscribe as a handler for the event bus. Analogous to signing up as a
     * ClickHandler
     */
    @Deprecated
    private void subscribeToBus() {
//        EventBus eventBus = EventBusUtils.getEventBus();
//
//        AlertEventHandler handler = new AlertButtonEventHandler();
//        handlerRegistration = eventBus.addHandler(AlertEvent.TYPE, handler);

//        AlertPopupEventHandler popupEventHandler = new AlertPopupEventHandler();
//        popupHandlerRegistration = eventBus.addHandler(AlertEvent.TYPE, popupEventHandler);
    }

    @Override
    public void destroy() {
        super.destroy();

        handlerRegistration.removeHandler();
        popupHandlerRegistration.removeHandler();

        stopAlertTimer();
        stopMessageTimer();
    }

    @Deprecated
    class AlertButtonEventHandler implements AlertEventHandler {

        @Override
        public void onAlert(AlertEvent alertEvent) {
//            Redux.jsEvent("local:gwtEvent:alertCount");
//
//            int totalCount = alertEvent.getTotalCount();
//            updateAlertsButton(totalCount);
        }
    }

    protected void updateAlertsButton(int totalCount) {
        boolean visible = totalCount > 0;
        alertButton.setAlertVisible(visible);
        // Redux - Disabled due to cleaner implementation: @TODO: delete this line when done research - 2/11/16 Nick Steele
        // angularUpdateAlerts(totalCount);

        if (visible) {
            alertButton.setAlertText(totalCount);
        }
    }

    // Redux JSNI javascript call...
    public static native void angularUpdateAlerts(int totalCount) /*-{
        $wnd.GWTAngularUpdateAlerts(totalCount);
    }-*/;

    @Deprecated
    protected void updateMessagesButton(int totalCount) {
        //TODO: deprecated 5.3
//        boolean visible = totalCount > 0;
//        messagesButton.setAlertVisible(visible);

        //5.3_removed
//        if (visible) {
//            messagesButton.setAlertText(totalCount);
//        }
        if (receivedFirstMessage) {

            if (hasNewMails(totalCount)) {
//                Date theDate = new Date();
//                showMessageLabel(theDate);
            }
        } else {
            receivedFirstMessage = true;
        }

    }

    @Deprecated
    protected void updateTasksButton(int totalCount) {
        //TODO: deprecated 5.3
//        boolean visible = totalCount > 0;
//        tasksButton.setAlertVisible(visible);
//
//        if (visible) {
//            tasksButton.setAlertText(totalCount);
//        }
        if (receivedFirstTask) {
            if (hasNewTasks(totalCount)) {
//                Date theDate = new Date();
//                showTaskLabel(theDate);
            }
        } else {
            receivedFirstTask = true;
        }

        oldTasksTotalCount = totalCount;
    }

    @Deprecated
    class AlertPopupEventHandler implements AlertEventHandler {

        //TODO: deprecated 5.3
        @Override
        public void onAlert(AlertEvent alertEvent) {

            if (receivedFirstAlerts) {

                if (hasNewAlerts(alertEvent)) {
//                    Date theDate = new Date();
//                    showAlertLabel(theDate);
//                    Redux.jsEvent("local:notification:appAlerts");
                }
            } else {
                receivedFirstAlerts = true;
            }

            oldAlertsTotalCount = alertEvent.getTotalCount();
        }

        protected boolean hasNewAlerts(AlertEvent alertEvent) {
            int totalCount = alertEvent.getTotalCount();

            return (totalCount > oldAlertsTotalCount);
        }
    }

    @Deprecated
    protected boolean hasNewMails(int count) {
        int totalCount = count;

        return (totalCount > oldMessagesTotalCount);
    }

    @Deprecated
    protected boolean hasNewTasks(int count) {
        int totalCount = count;
        return (totalCount > oldTasksTotalCount);
    }

    @Deprecated
    private void showAlertLabel(Date theDate) {

        String time = dateFormatter.format(theDate);
        alertPopupLabel.setContents("<b><u>" + time + "</u><br>New alerts received.</b>");

        int browserInnerWidth = com.google.gwt.user.client.Window.getClientWidth();
        int browserInnerHeight = com.google.gwt.user.client.Window.getClientHeight();

        alertPopupLabel.setLeft(browserInnerWidth - alertPopupLabel.getWidth());
        alertPopupLabel.setTop(browserInnerHeight - alertPopupLabel.getHeight());

        alertPopupLabel.animateShow(AnimationEffect.SLIDE);

        alertPopupTimer.schedule(6000);
    }

    @Deprecated
    private void showMessageLabel(Date theDate) {

        String time = dateFormatter.format(theDate);
        messagesPopupLabel.setContents("<b><u>" + time + "</u><br>New messages received.</b>");

        int browserInnerWidth = com.google.gwt.user.client.Window.getClientWidth();
        int browserInnerHeight = com.google.gwt.user.client.Window.getClientHeight();

        messagesPopupLabel.setLeft(browserInnerWidth - messagesPopupLabel.getWidth());
        messagesPopupLabel.setTop(browserInnerHeight - messagesPopupLabel.getHeight());

        messagesPopupLabel.animateShow(AnimationEffect.SLIDE);

        messagesPopupTimer.schedule(6000);
    }

    @Deprecated
    private void showTaskLabel(Date theDate) {

        String time = dateFormatter.format(theDate);
        tasksPopupLabel.setContents("<b><u>" + time + "</u><br>New tasks received.</b>");

        int browserInnerWidth = com.google.gwt.user.client.Window.getClientWidth();
        int browserInnerHeight = com.google.gwt.user.client.Window.getClientHeight();

        tasksPopupLabel.setLeft(browserInnerWidth - tasksPopupLabel.getWidth());
        tasksPopupLabel.setTop(browserInnerHeight - tasksPopupLabel.getHeight());

        tasksPopupLabel.animateShow(AnimationEffect.SLIDE);

        tasksPopupTimer.schedule(6000);
    }

    class ShowAlertNoteTimer extends Timer {

        public void run() {
            alertPopupLabel.animateHide(AnimationEffect.SLIDE);
        }
    }

    class ShowMessagesNoteTimer extends Timer {

        public void run() {
            messagesPopupLabel.animateHide(AnimationEffect.SLIDE);
        }
    }

    class ShowTasksNoteTimer extends Timer {

        @Override
        public void run() {
            tasksPopupLabel.animateHide(AnimationEffect.SLIDE);
        }
    }
}
