package com.gsihealth.dashboard.client.careteam;

import com.smartgwt.client.widgets.form.fields.SelectItem;

/**
 *
 * @author Chad Darby
 */
public class AddCareTeamWindow extends ManageCareTeamWindow {

    public AddCareTeamWindow() {

        setTitle("Create Care Team");
        saveButton.hide();

        saveAsNewButton.setTitle("Save");
        
        isFromAddCareTeamWindow = true;
        
        setCareteamSelectItem(null);
        
        loadAvailableUsersData();
    }

    public AddCareTeamWindow(SelectItem theCareteamSelectItem) {

        setTitle("Create Care Team");
        saveButton.hide();

        saveAsNewButton.setTitle("Save");
        
        isFromAddCareTeamWindow = true;
        
        setCareteamSelectItem(theCareteamSelectItem);
        
        loadAvailableUsersData();
    }
}
