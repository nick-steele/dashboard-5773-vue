package com.gsihealth.dashboard.client.careteam;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.entity.dto.CareTeamDTO;
import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.FormErrorOrientation;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.StaticTextItem;
import com.smartgwt.client.widgets.form.validator.CustomValidator;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class CareTeamInfoWindow extends Window {

    private DynamicForm form;
    private static final String OTHER_PROVIDER = "Other";
    private static final String PREFIX = "*";
    private HashMap<Long, String> roleMap;
    private static final String PATIENT = "Patient";

     public CareTeamInfoWindow(String careteamName, CareTeamDTO careTeamUser) {
        buildGui(careteamName, careTeamUser, null);
    }

    public CareTeamInfoWindow(String careteamName,  CareTeamDTO careTeamUser, List<NonHealthHomeProviderDTO> providers) {
        buildGui(careteamName, careTeamUser, providers);
        DashBoardApp.restartTimer();
    }

    private void buildGui(String careteamName, CareTeamDTO careTeamUser , List<NonHealthHomeProviderDTO> providers) {
        roleMap = ApplicationContextUtils.getUserRoles();
        setPadding(10);

        setWidth(450);
        setHeight(300);
        setTitle("");
        setShowMinimizeButton(false);
        setIsModal(true);
        setShowModalMask(true);
        centerInPage();

        DynamicForm careteamTitle = buildCareteamTitle(careteamName);
        addItem(careteamTitle);

        
        
        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(5);
        addItem(spacer);

        form = buildCareteamForm(careTeamUser, providers);
        addItem(form);
        
        addItem(buildLegend());

        Canvas buttonBar = buildButtonBar();
        
        addItem(buttonBar);
    }

    public void validate() {
        form.validate();
    }

    private DynamicForm buildCareteamForm(CareTeamDTO careTeamUser, List<NonHealthHomeProviderDTO> providers) {
        DynamicForm form = new DynamicForm();
        form.setHeight100();
        form.setWidth100();
        form.setPadding(5);
        form.setErrorOrientation(FormErrorOrientation.RIGHT);

       List<UserDTO>users =careTeamUser.getUserDTOList();
        int usersSize = users.size();
        FormItem[] formItems;

        int providersSize = 0;
        if (providers != null) {
            providersSize = providers.size();

        }
        if (careTeamUser.getPersonDTO()!=null){
        
        // adding index to adjust other providers and patient to the form.
           formItems = new FormItem[usersSize + providersSize+1];
        }
        else{
            // adding index to adjust other providers to the form.
           formItems = new FormItem[usersSize + providersSize];
        }
        

        int i = 0;

        for (UserDTO tempUser : users) {
            String userLevel = tempUser.getSecurityRole();

            String name = getCompleteNamePrefixAndCredentials(tempUser);

            String info = name;

            String org = tempUser.getOrganization();
            if (!StringUtils.isBlank(org)) {
                info += " - " + org;
            }

            formItems[i] = buildTextItem(userLevel, info);

            // to do:  check the value of tempUser.hasConsent()
            //         if value is false, then add red icon as shown
            //         in requirements mockup
            if (!tempUser.hasPermitConsent()) {
                formItems[i].setValidators(new ConsentValidator(tempUser));
                formItems[i].setShowErrorIcon(true);
            }

            i++;
        }

        // adding other provider info to the form.
        if (providers != null) {
            for (NonHealthHomeProviderDTO tempProvider : providers) {
                String name = getProviderCompleteName(tempProvider);
                String role = tempProvider.getRole();
                Long roleId=0L;
                
                if(role!= null) {
                 roleId = Long.parseLong(tempProvider.getRole());
                }
                
                String roleName;
                if (roleId > 0) {
                    roleName= roleMap.get(roleId);
                } else {
                    roleName = OTHER_PROVIDER;
                }
                
                
                String completeRole = PREFIX + roleName;

                formItems[i] = buildTextItem(completeRole, name);

                if ("DENY".equalsIgnoreCase(tempProvider.getConsentStatus())) {
                    formItems[i].setValidators(new ConsentValidatorForProvider(tempProvider));
                    formItems[i].setShowErrorIcon(true);
                }

                i++;
            }
        }
       
        // adding patient info to the form.
        if (careTeamUser.getPersonDTO() != null) {

            String name = careTeamUser.getPersonDTO().getFirstName() + " " + careTeamUser.getPersonDTO().getLastName();
            String role = PATIENT;

            formItems[i] = buildTextItem(role, name);
            i++;
        }
 
        form.setFields(formItems);

        return form;
    }

    private String getCompleteNamePrefixAndCredentials(UserDTO tempUser) {
        String prefix = StringUtils.defaultString(tempUser.getPrefix());
        if (!StringUtils.isBlank(prefix)) {
            prefix += " ";
        }

        String credentials = StringUtils.defaultString(tempUser.getCredentials());
        if (!StringUtils.isBlank(credentials)) {
            credentials = ", " + credentials;
        }

        String name = prefix + tempUser.getFirstName() + " " + tempUser.getLastName() + credentials;

        return name;
    }

    private String getProviderCompleteName(NonHealthHomeProviderDTO providerDTO) {
        String title = StringUtils.defaultString(providerDTO.getTitle());

        if (!StringUtils.isBlank(title)) {
            title += " ";
        }

        String credentials = StringUtils.defaultString(providerDTO.getCredentials());

        if (!StringUtils.isBlank(credentials)) {
            credentials = ", " + credentials;
        }

        String name = title + providerDTO.getFirstName() + " " + providerDTO.getLastName() + credentials;

        return name;
    }

    private StaticTextItem buildTextItem(String title, String value) {
        StaticTextItem textItem = new StaticTextItem();
        textItem.setTitle(title);
        textItem.setWrapTitle(false);
        textItem.setValue(value);

        return textItem;
    }

    private DynamicForm buildCareteamTitle(String careteamName) {

        VLayout layout = new VLayout();
        DynamicForm form = new DynamicForm();
        //layout.setPadding(10);
        layout.setWidth100();
        layout.setAlign(Alignment.CENTER);
        layout.setHeight(5);

        StaticTextItem careteamTitle = new StaticTextItem("careteamTitle","");
        careteamTitle.setAlign(Alignment.CENTER);
        
        //careteamTitle.setWidth100();
        careteamTitle.setValue("<b>" + careteamName + "</b><br>");
        careteamTitle.setEndRow(true);
        
     
        
        //layout.addMember(careteamTitle);
        //layout.addMember(legend);
        
        form.setFields(careteamTitle);

        return form;

    }
    
    private Layout buildLegend() {

        VLayout layout = new VLayout();
       
        //layout.setPadding(10);
        layout.setWidth100();
        layout.setAlign(Alignment.CENTER);
        layout.setHeight(5);
       
        Label legend = new Label();
        legend.setAlign(Alignment.LEFT);
        legend.setContents("<b>   * = Out Of Network Provider</b><br>");
        
        layout.addMember(legend);
        
        

        return layout;

    }

    private Layout buildButtonBar() {
        HLayout buttonLayout = new HLayout(10);

        buttonLayout.setPadding(10);
        buttonLayout.setWidth100();
        buttonLayout.setAlign(Alignment.CENTER);

        Button okButton = new Button("OK");
        buttonLayout.addMember(okButton);

        okButton.addClickHandler(new ClickHandler() {

            public void onClick(ClickEvent event) {
                CareTeamInfoWindow.this.hide();
                CareTeamInfoWindow.this.destroy();
            }
        });

        return buttonLayout;
    }

    class ConsentValidator extends CustomValidator {

        UserDTO userDTO;

        ConsentValidator(UserDTO theUserDTO) {
            userDTO = theUserDTO;

            setErrorMessage("This care team member does not have consent for selected patient");
        }

        @Override
        protected boolean condition(Object value) {
            return userDTO.hasPermitConsent();
        }
    }

    class ConsentValidatorForProvider extends CustomValidator {

        NonHealthHomeProviderDTO providerDTO;

        ConsentValidatorForProvider(NonHealthHomeProviderDTO theProviderDTO) {
            providerDTO = theProviderDTO;

            setErrorMessage("This care team member does not have consent for selected patient");
        }

        @Override
        protected boolean condition(Object value) {
            return "PERMIT".equalsIgnoreCase(providerDTO.getConsentStatus());
        }
    }
}
