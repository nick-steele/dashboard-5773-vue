package com.gsihealth.dashboard.client.careteam;

import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 *
 * @author Chad Darby
 */
public class CareteamUtils {

    /**
     * Searches for careteam id in the map of care team names stored in ApplicationContext
     *
     * Returns care team id if found. If not found throws IllegalArgumentException
     * 
     * @param careteamName
     * @return
     * @throws IllegalArgumentException
     */
    public static long getCareteamId(String careteamName) throws IllegalArgumentException {
        long careteamId = 0;

        LinkedHashMap<String, String> data = ApplicationContextUtils.getCareteamNames();

        Set<Map.Entry<String, String>> entrySet = data.entrySet();

        boolean found = false;
        for (Map.Entry<String, String> tempEntry : entrySet) {

            String value = tempEntry.getValue();

            if (value.equals(careteamName)) {
                String key = tempEntry.getKey();
                careteamId = Long.parseLong(key);
                found = true;
                break;
            }
        }

        if (found) {
            return careteamId;
        }

        if (!found) {
            throw new IllegalArgumentException("Did not find careteam name in the map.  careteamName=" + careteamName);
        }

        return careteamId;
    }

    public static boolean validateSelectedUserLevels(String[] requiredRolesForCareteam, String[] duplicateRolesForCareteam, List<UserDTO> selectedUsers) {
        boolean result = false;
        
        Set<String> userLevelsSet = getMissingUserLevels(requiredRolesForCareteam, selectedUsers);

        Set<String> duplicateUserLevelsSet = getDuplicateUserLevels(duplicateRolesForCareteam, selectedUsers);
        
        result = userLevelsSet.isEmpty() && duplicateUserLevelsSet.isEmpty();

        return result;
    }

    public static Set<String> getMissingUserLevels(String[] reqdRoles, List<UserDTO> selectedUsers) {
        // put levels in a set
        Set<String> userLevelsSet = new HashSet<String>();
        Collections.addAll(userLevelsSet, reqdRoles);
        
        // put user roles in a set
        Set<String> selectedUserLevelsSet = getSelectedUserLevelsSet(selectedUsers);

        // get difference
        userLevelsSet.removeAll(selectedUserLevelsSet);

        return new TreeSet(userLevelsSet);
    }
    
    private static Set<String> getSelectedUserLevelsSet(List<UserDTO> selectedUsers) {
        Set<String> set = new HashSet<String>();
        for (UserDTO temp : selectedUsers) {
            set.add(temp.getSecurityRole());
        }
        return set;
    }

    public static Set<String> getDuplicateUserLevels(String[] theDuplicateRolesForCareteam, List<UserDTO> selectedUsers) {
        Set<String> duplicates = new HashSet<String>();
        Set<String> base = new HashSet<String>();
        List<String> duplicateRolesForCareteam = Arrays.asList(theDuplicateRolesForCareteam);

        for (UserDTO temp : selectedUsers) {
            String userLevel = temp.getSecurityRole();
            String role = temp.getSecurityRole();
            if (duplicateRolesForCareteam.contains(userLevel)) {
                continue;
            }
            
            if (base.contains(userLevel)) {
                duplicates.add(role);
            }
            else {
                base.add(userLevel);
            }
        }
        
        return duplicates;
    }
    
    public static boolean hasInactiveUsers(List<UserDTO> users) {
        boolean result = false;

        for (UserDTO tempUser : users) {
            String status = tempUser.getStatus();
            boolean inactive = Constants.USER_STATUS_INACTIVE.equals(status);

            if (inactive) {
                result = true;
                break;
            }
        }

        return result;
    }

    public static Set<String> getInactiveUsers(List<UserDTO> users) {
        Set<String> inactiveUsers = new HashSet<String>();

        for (UserDTO tempUser : users) {
            String status = tempUser.getStatus();
            if (Constants.USER_STATUS_INACTIVE.equals(status)) {
                String fullName = tempUser.getFirstName() + " " + tempUser.getLastName();
                inactiveUsers.add(fullName);
            }
        }

        return inactiveUsers;
    }

}
