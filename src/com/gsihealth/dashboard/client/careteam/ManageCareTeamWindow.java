package com.gsihealth.dashboard.client.careteam;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.callbacks.BasicPortalAsyncCallback;
import com.gsihealth.dashboard.client.common.CareTeamsLoadable;
import com.gsihealth.dashboard.client.common.MiniGridPager;
import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.enrollment.PatientForm;
import com.gsihealth.dashboard.client.service.CareteamService;
import com.gsihealth.dashboard.client.service.CareteamServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.PropertyUtils;
import com.gsihealth.dashboard.client.util.ValidationUtils;
import com.gsihealth.dashboard.common.ManageCareTeamFormData;
import com.gsihealth.dashboard.entity.dto.CommunityCareteamDTO;
import com.gsihealth.dashboard.entity.dto.RoleDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.fields.DataSourceTextField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DragDataAction;
import com.smartgwt.client.types.FetchMode;
import com.smartgwt.client.types.ListGridEditEvent;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.HStack;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.layout.VStack;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

/**
 *
 * @author Chad Darby
 */
public class ManageCareTeamWindow extends Window {

    private final static Logger logger = Logger.getLogger("ManageCareTeamWindow");

    private static final String CARETEAMNAME_FIELD = "careteamName";
    private TextItem careTeamNameTextItem;
    private SelectItem userLevelSelectItem;
    private ListGrid availableUsersListGrid;
    private ListGrid selectedUsersListGrid;
    private CareteamServiceAsync careteamService = GWT.create(CareteamService.class);
    private long communityCareteamId;
    private DynamicForm careteamNameForm;
    protected Button saveButton;
    protected Button saveAsNewButton;
    private Button cancelButton;
    private SelectItem careteamSelectItem;
    private String[] requiredRolesForCareteam;
    private String[] duplicateRolesForCareteam;
//    private MiniGridPager pager;
    private ProgressBarWindow progressBarWindow;
    private int currentPageNumber;
    private long communityId;
    protected boolean isFromAddCareTeamWindow = false;

    public ManageCareTeamWindow() {
        this(0, "");
    }

    public ManageCareTeamWindow(long theCommunityCareteamId, String careteamName) {
        communityCareteamId = theCommunityCareteamId;
        communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
        System.out.println("careteam window commId: " + communityId);
        buildGui(careteamName);

        loadFormData();
    }

    public ManageCareTeamWindow(long theCommunityCareteamId, String careteamName, SelectItem theCareteamSelectItem) {
        communityCareteamId = theCommunityCareteamId;
        careteamSelectItem = theCareteamSelectItem;
        communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
        System.out.println("careteam window commId: " + communityId);
        buildGui(careteamName);

        loadFormData();
    }

    private void buildGui(String careteamName) {
        DashBoardApp.restartTimer();

        setWindowProps();
        
        Canvas heading = buildHeading(careteamName);
        addItem(heading);

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(5);
        addItem(spacer);

        DynamicForm userLevelForm = buildUserLevelForm();
        addItem(userLevelForm);

        Canvas listGridLayout = buildListGridLayout();
        addItem(listGridLayout);

        Canvas buttonBar = buildButtonBar();
        addItem(buttonBar);

        progressBarWindow = new ProgressBarWindow();
        loadAvailableUsersData();
//        pager.setPageEventHandler(new UserListPageEventHandler());
    }

    private Canvas buildListGridLayout() {

        HStack listGridLayout = new HStack();
        listGridLayout.setPadding(5);

        availableUsersListGrid = buildListGrid();

//        pager = new MiniGridPager(0,200);
        VLayout availableLayout = createListGridLayout("Select Providers", availableUsersListGrid);
        listGridLayout.addMember(availableLayout);

        Canvas arrowCanvas = buildArrowCanvas();
        listGridLayout.addMember(arrowCanvas);

        selectedUsersListGrid = buildSelectedUserListGrid();
        VLayout selectedLayout = createListGridLayout("Selected Providers", selectedUsersListGrid);
        listGridLayout.addMember(selectedLayout);

        return listGridLayout;
    }

    private VLayout createListGridLayout(String title, ListGrid theListGrid, MiniGridPager theGridPager) {
        VLayout theLayout = new VLayout();

        Label titleLabel = new Label("<b>" + title + "</b>");
        titleLabel.setHeight(20);
        titleLabel.setWidth(150);
        theLayout.addMember(titleLabel);

        theListGrid.setWidth(495);
        theLayout.addMember(theListGrid);

        theGridPager.setWidth(495);
        theLayout.addMember(theGridPager);

        return theLayout;
    }

    private VLayout createListGridLayout(String title, ListGrid theListGrid) {
        VLayout theLayout = new VLayout();

        Label titleLabel = new Label("<b>" + title + "</b>");
        titleLabel.setHeight(20);
        titleLabel.setWidth(150);
        theLayout.addMember(titleLabel);
        theLayout.addMember(theListGrid);

        return theLayout;
    }

    private void setWindowProps() {
        setPadding(10);
        setWidth(1080);
        setHeight(450);
        setTitle("Modify Care Team");
        setShowMinimizeButton(false);
        setIsModal(true);
        setShowModalMask(true);
        centerInPage();
    }

    private Layout buildHeading(String careteamName) {
        HLayout layout = new HLayout(10);

        layout.setPadding(10);
        layout.setWidth100();
        layout.setAlign(Alignment.CENTER);
        layout.setHeight(40);
        DynamicForm careTeamNameForm = buildCareTeamNameForm(careteamName);

        layout.addMember(careTeamNameForm);

        return layout;
    }

    private Layout buildButtonBar() {
        HLayout buttonLayout = new HLayout(10);

        buttonLayout.setPadding(10);
        buttonLayout.setWidth100();
        buttonLayout.setAlign(Alignment.CENTER);

        saveButton = new Button("Save");
        buttonLayout.addMember(saveButton);
       
        saveAsNewButton = new Button("Save As New");
        buttonLayout.addMember(saveAsNewButton);

        cancelButton = new Button("Cancel");
        buttonLayout.addMember(cancelButton);

        // register listeners
        saveButton.addClickHandler(new SaveClickHandler());
        saveAsNewButton.addClickHandler(new SaveAsNewClickHandler());
        cancelButton.addClickHandler(new CancelClickHandler());

        return buttonLayout;
    }

    private DynamicForm buildCareTeamNameForm(String careteamName) {
        careteamNameForm = new DynamicForm();

        careTeamNameTextItem = new TextItem(CARETEAMNAME_FIELD);
        careTeamNameTextItem.setTitle("Care Team Name");
        careTeamNameTextItem.setValue(careteamName);
        careTeamNameTextItem.setWrapTitle(false);
        careTeamNameTextItem.setRequired(true);

        RegExpValidator alphaNumberRegExpValidator = ValidationUtils.getAlphaNumberCareTeamRegExpValidator();
        ValidationUtils.setValidators(careTeamNameTextItem, alphaNumberRegExpValidator, true);

        careteamNameForm.setPadding(5);

        careteamNameForm.setFields(careTeamNameTextItem);

        return careteamNameForm;
    }

    private DynamicForm buildUserLevelForm() {
        DynamicForm form = new DynamicForm();

        userLevelSelectItem = new SelectItem();
        userLevelSelectItem.setShowTitle(false);
        userLevelSelectItem.setWidth(175);

        form.setPadding(5);

        form.setFields(userLevelSelectItem);
        userLevelSelectItem.setDefaultValue("All");
        userLevelSelectItem.addChangedHandler(new UserLevelChangedHandler());

        return form;
    }

    /**
     * Create a canvas to hold the left/right arrows
     *
     * @return
     * @throws IllegalStateException
     */
    private VStack buildArrowCanvas() {
        VStack vStack = new VStack(10);
        vStack.setWidth(62);
        vStack.setHeight(74);
        vStack.setLayoutAlign(Alignment.CENTER);
        Img rightImg = new Img("arrow_right.png", 32, 32);
        rightImg.setLayoutAlign(Alignment.CENTER);

        rightImg.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                selectedUsersListGrid.transferSelectedData(availableUsersListGrid);
                loadAvailableUsersData();
            }
        });

        vStack.addMember(rightImg);

        Img leftImg = new Img("arrow_left.png", 32, 32);
        leftImg.setLayoutAlign(Alignment.CENTER);
        leftImg.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                availableUsersListGrid.transferSelectedData(selectedUsersListGrid);
                loadAvailableUsersData();
            }
        });
        vStack.addMember(leftImg);

        return vStack;
    }

    /**
     * Helper method to build a ListGrid
     *
     * @return
     */
    private ListGrid buildListGrid() {

        String[] columnNames = {CareTeamRecord.PREFIX, CareTeamRecord.FIRST_NAME, CareTeamRecord.LAST_NAME, CareTeamRecord.CREDENTIAL, CareTeamRecord.USER_LEVEL, CareTeamRecord.ORGANIZATION};

        ListGrid theListGrid = new ListGrid();

        theListGrid.setCanReorderFields(true);
        theListGrid.setCanReorderRecords(true);

        theListGrid.setWidth(495);
        theListGrid.setHeight(200);

        theListGrid.setAlternateRecordStyles(true);
        theListGrid.setShowAllRecords(false);

        theListGrid.setDataFetchMode(FetchMode.LOCAL);
        theListGrid.setCanEdit(false);
        theListGrid.setAutoFetchData(true);
        theListGrid.setShowFilterEditor(true);
        theListGrid.setFilterByCell(true);
        theListGrid.setFilterOnKeypress(true);
        theListGrid.setEditEvent(ListGridEditEvent.CLICK);
        theListGrid.setEditByCell(true);
        theListGrid.setCanDragRecordsOut(true);
        theListGrid.setDragDataAction(DragDataAction.COPY);

        ListGridField[] fields = buildListGridFields(columnNames);

        theListGrid.setFields(fields);
        theListGrid.setEmptyMessage("...");

        theListGrid.setCanResizeFields(true);
        theListGrid.setSaveLocally(true);
        return theListGrid;
    }

    private ListGrid buildSelectedUserListGrid() {

        String[] columnNames = {CareTeamRecord.PREFIX, CareTeamRecord.FIRST_NAME, CareTeamRecord.LAST_NAME, CareTeamRecord.CREDENTIAL, CareTeamRecord.USER_LEVEL, CareTeamRecord.ORGANIZATION};

        ListGrid theListGrid = new ListGrid();

        theListGrid.setCanReorderFields(true);
        theListGrid.setCanReorderRecords(true);

        theListGrid.setWidth(495);
        theListGrid.setHeight(200);

        theListGrid.setAlternateRecordStyles(true);
        theListGrid.setShowAllRecords(false);

        theListGrid.setCanEdit(false);
        theListGrid.setCanAcceptDroppedRecords(true);

        ListGridField[] fields = buildListGridFields(columnNames);

        theListGrid.setFields(fields);
        theListGrid.setEmptyMessage("...");

        theListGrid.setCanResizeFields(true);
        theListGrid.setSaveLocally(true);

        return theListGrid;
    }

    private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];

        for (int i = 0; i < columnNames.length; i++) {
            String tempColumnName = columnNames[i];
            ListGridField tempField = buildListGridField(tempColumnName);
            fields[i] = tempField;
        }

        return fields;
    }

    private ListGridField buildListGridField(String title) {
        ListGridField tempField = new ListGridField(title, title);

        tempField.setCanEdit(false);

        return tempField;
    }

    private void populateAvailableUsers(List<UserDTO> users) {
        ListGridRecord[] records = convert(users);

        availableUsersListGrid.scrollToRow(1);

        final DataSource dataSource = new DataSource();
        dataSource.setID("dataSource");
        dataSource.setClientOnly(true);
        DataSourceTextField prefix = new DataSourceTextField(CareTeamRecord.PREFIX, "Prefix");
        prefix.setCanFilter(false);
        DataSourceTextField firstName = new DataSourceTextField(CareTeamRecord.FIRST_NAME, "First Name");
        DataSourceTextField lastName = new DataSourceTextField(CareTeamRecord.LAST_NAME, "Last Name");

        DataSourceTextField credential = new DataSourceTextField(CareTeamRecord.CREDENTIAL, "Credential");
        credential.setCanFilter(false);
        DataSourceTextField userLevel = new DataSourceTextField(CareTeamRecord.USER_LEVEL, "User Level");
        userLevel.setCanFilter(false);
        DataSourceTextField organization = new DataSourceTextField(CareTeamRecord.ORGANIZATION, "Organization");
        organization.setCanFilter(false);
        dataSource.setFields(prefix, firstName, lastName, credential, credential, userLevel, organization);
        dataSource.setCacheData(records);

        availableUsersListGrid.setDataSource(dataSource);
        availableUsersListGrid.setData(records);
        availableUsersListGrid.markForRedraw();

//        pager.setTotalCount(totalCount);
    }

    public void populateSelectedUsers(List<UserDTO> users) {
        ListGridRecord[] records = convert(users);
        selectedUsersListGrid.setData(records);
        selectedUsersListGrid.markForRedraw();
    }

    private ListGridRecord[] convert(List<UserDTO> users) {
        ListGridRecord[] records = new ListGridRecord[users.size()];

        // convert dtos to records
        for (int index = 0; index < users.size(); index++) {
            UserDTO tempUser = users.get(index);
            records[index] = PropertyUtils.convertToCareTeamRecord(index, tempUser);
        }

        return records;
    }

    private List<UserDTO> convert(ListGridRecord[] records) {
        List<UserDTO> users = new ArrayList<UserDTO>();

        for (ListGridRecord tempRecord : records) {

            if (tempRecord instanceof CareTeamRecord) {
                CareTeamRecord tempCareteamRecord = (CareTeamRecord) tempRecord;

                UserDTO tempUser = PropertyUtils.convert(tempCareteamRecord);

                users.add(tempUser);

            } else {
                UserDTO tempUser = (UserDTO) tempRecord.getAttributeAsObject("UserDto");
                users.add(tempUser);
            }
        }

        return users;
    }

    private void loadFormData() {
        careteamService.getManageCareTeamFormData(communityId, new GetFormDataCalback());
    }

    class CancelClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            ManageCareTeamWindow.this.hide();
            ManageCareTeamWindow.this.destroy();
        }
    }

    class GetAvailableUsersCallback extends RetryActionCallback<List<UserDTO>> {

        private String role;
        private List<Long> userIdsToExclude;

        private GetAvailableUsersCallback(String role, List<Long> userIdsToExclude) {
            this.role = role;
            this.userIdsToExclude = userIdsToExclude;
        }

        @Override
        public void attempt() {
            careteamService.getAvailableUsers(role, userIdsToExclude, this);
        }

        @Override
        public void onCapture(List<UserDTO> searchResults) {
            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }
            List<UserDTO> users = searchResults;

            if (!users.isEmpty()) {
                populateAvailableUsers(users);
            } else {
                SC.warn("Available users not found.");
            }
        }

        @Override
        public void onFailure(Throwable exc) {
            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            super.onFailure(exc);
        }
    }

    class GetFormDataCalback extends RetryActionCallback<ManageCareTeamFormData> {

        @Override
        public void attempt() {
            careteamService.getManageCareTeamFormData(communityId, this);
        }

        @Override
        public void onCapture(ManageCareTeamFormData theManageCareTeamFormData) {

            List<String> theRequiredRoles = theManageCareTeamFormData.getRequiredRolesForCareTeam();
            requiredRolesForCareteam = theRequiredRoles.toArray(new String[0]);

            List<String> theDuplicateRoles = theManageCareTeamFormData.getDuplicateRoles();
            duplicateRolesForCareteam = theDuplicateRoles.toArray(new String[0]);

            List<RoleDTO> rolesFromDatabase = theManageCareTeamFormData.getUserRoles();

            //FIXME move to a separate method
            LinkedHashMap<String, String> valueMap = new LinkedHashMap<String, String>();
            for (RoleDTO each : rolesFromDatabase) {
                String roleName = each.getRoleName();
                String roleId = each.getPrimaryRoleId().toString();
                valueMap.put(roleId, roleName);
            }
            valueMap.put("All", "All");
            // populate drop-down list
            userLevelSelectItem.setValueMap(valueMap);
            userLevelSelectItem.setDefaultValue("All");
        }
    }

    class UserLevelChangedHandler implements ChangedHandler {

        @Override
        public void onChanged(ChangedEvent event) {
            loadAvailableUsersData();
        }
    }

    private List<UserDTO> getSelectedUsers() {
        ListGridRecord[] records = selectedUsersListGrid.getRecords();
        System.out.println("Records selected" + records.length);
        List<UserDTO> users = convert(records);

        return users;
    }

    private List<Long> getSelectedUserIds() {
        ListGridRecord[] records = selectedUsersListGrid.getRecords();

        List<UserDTO> users = convert(records);

        List<Long> userIds = new ArrayList<Long>();

        for (UserDTO tempDTO : users) {
            Long tempUserId = tempDTO.getUserId();
            userIds.add(tempUserId);
        }

        return userIds;
    }

    abstract class BaseSaveClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            List<UserDTO> selectedUsers = getSelectedUsers();

            boolean validCareteamNameForm = careteamNameForm.validate();
            boolean validSelectedUserLevels = CareteamUtils.
                    validateSelectedUserLevels(requiredRolesForCareteam, duplicateRolesForCareteam, selectedUsers);

            boolean hasInactiveUsers = CareteamUtils.hasInactiveUsers(selectedUsers);

            if (validCareteamNameForm && validSelectedUserLevels && !hasInactiveUsers) {
                String careteamName = careTeamNameTextItem.getValueAsString();

                ProgressBarWindow progressBarWindow = new ProgressBarWindow();
                progressBarWindow.show();

                String confirmationMessage = "Save successful.";

                doSave(progressBarWindow, confirmationMessage, careteamName, selectedUsers);
            } else {
                if (!validCareteamNameForm) {

                    Map errors = careteamNameForm.getErrors();
                    String errorMessage = (String) errors.get(CARETEAMNAME_FIELD);

                    SC.warn("Care team name error: " + errorMessage);
                    return;
                }

                if (!validSelectedUserLevels) {
                    Set<String> missingLevels = CareteamUtils.getMissingUserLevels(requiredRolesForCareteam, selectedUsers);

                    if (!missingLevels.isEmpty()) {
                        StringBuilder list = buildErrorList(missingLevels);
                        SC.warn("<b>Missing Members</b><br><br>The following care team members need to be added before the care team can be saved:<br> " + list);
                        return;
                    }

                    Set<String> duplicateLevels = CareteamUtils.getDuplicateUserLevels(duplicateRolesForCareteam, selectedUsers);

                    if (!duplicateLevels.isEmpty()) {
                        StringBuilder list = buildErrorList(duplicateLevels);
                        SC.warn("<b>Duplicate Members</b><br><br>Duplicate members are not allowed. The following care team members need to be removed before the care team can be saved:<br> " + list);
                        return;
                    }
                }

                if (hasInactiveUsers) {
                    Set<String> inactiveUsers = CareteamUtils.getInactiveUsers(selectedUsers);
                    StringBuilder list = buildErrorList(inactiveUsers);
                    SC.warn("<b>Inactive Members</b><br><br>Inactive members are not allowed. The following care team members are inactive:<br> " + list);
                    return;
                }

            }
        }

        private StringBuilder buildErrorList(Set<String> data) {
            StringBuilder list = new StringBuilder();

            list.append("<ul>");
            for (String temp : data) {
                list.append("<li>" + temp + "</li>");
            }
            list.append("</ul>");

            return list;
        }

        /**
         * Performs the save operation.
         *
         * @param progressBarWindow
         * @param confirmationMessage
         * @param careteamName
         * @param selectedUsers
         */
        protected abstract void doSave(ProgressBarWindow progressBarWindow, String confirmationMessage, String careteamName, List<UserDTO> selectedUsers);
    }

    class SaveClickHandler extends BaseSaveClickHandler {

        @Override
        protected void doSave(ProgressBarWindow progressBarWindow, String confirmationMessage, String careteamName, List<UserDTO> selectedUsers) {
            // call hasOtherPatientsAssignedToCareTeam
            AsyncCallback otherPatientsAssignedCallback = new OtherPatientsAssignedCallback(progressBarWindow, confirmationMessage, careteamName, selectedUsers);
            careteamService.hasPatientsAssignedToCareteam(communityCareteamId, otherPatientsAssignedCallback);
        }
    }

    class SaveAsNewClickHandler extends BaseSaveClickHandler {

        @Override
        protected void doSave(ProgressBarWindow progressBarWindow, String confirmationMessage,
                String careteamName, List<UserDTO> selectedUsers) {
            logger.info("saving careteam " + careteamName + " to community " + communityId);
            if(!isFromAddCareTeamWindow){
                confirmationMessage = "The patient will be assigned to the renamed Care Team" ;
            }
            SaveCareteamCallback callback = new SaveCareteamCallback(progressBarWindow, confirmationMessage);
            careteamService.saveNewCareteam(careteamName, communityId, selectedUsers, callback);
        }
    }

    class OtherPatientsAssignedCallback implements AsyncCallback<Boolean> {

        ProgressBarWindow progressBarWindow;
        String confirmationMessage;
        String careteamName;
        List<UserDTO> selectedUsers;

        OtherPatientsAssignedCallback(ProgressBarWindow progressBarWindow, String confirmationMessage,
                String careteamName, List<UserDTO> selectedUsers) {
            this.progressBarWindow = progressBarWindow;
            this.confirmationMessage = confirmationMessage;
            this.careteamName = careteamName;
            this.selectedUsers = selectedUsers;
        }

        @Override
        public void onSuccess(Boolean hasOtherPatientsAssigned) {
            if (hasOtherPatientsAssigned) {
                progressBarWindow.hide();

                // prompt user
                String message = "The Care Team has patients assigned to it. By saving the modified Care Team it will affect those patients as well.<br><br>"
                        + "<b>If you want to make the change for all patients assigned to this Care Team, click ‘Save’.<br><br>"+
                           "If you only want to change the Care Team for this patient, rename the Care Team and click ‘Save as New'</b>";

                SC.ask("Save Care Team", message, new SaveConfirmationCallback());
            } else {
                progressBarWindow.show();
                SaveCareteamCallback callback = new SaveCareteamCallback(progressBarWindow, confirmationMessage);
                careteamService.saveCareteam(communityCareteamId, careteamName, selectedUsers, communityId, callback);
            }
        }

        @Override
        public void onFailure(Throwable thrwbl) {

            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            SC.warn("Error checking for assigned patients to care team.");
        }

        class SaveConfirmationCallback implements BooleanCallback {

            @Override
            public void execute(Boolean value) {
                if (value.booleanValue()) {
                    progressBarWindow.show();
                    SaveCareteamCallback callback = new SaveCareteamCallback(progressBarWindow, confirmationMessage);
                    careteamService.saveCareteam(communityCareteamId, careteamName, selectedUsers, communityId, callback);
                }
            }
        }
    }

    class SaveCareteamCallback extends BasicPortalAsyncCallback {

        SaveCareteamCallback(ProgressBarWindow theProgressBarWindow, String theConfirmationMessage) {
            super(theProgressBarWindow, theConfirmationMessage);
        }

        @Override
        public void onSuccess(Object t) {
            super.onSuccess(t);

            refreshCareteamSelectItems();
        }

        private void refreshCareteamSelectItems() {
            ApplicationContext context = ApplicationContext.getInstance();
            
            CareTeamsLoadable careTeamsLoadable = (CareTeamsLoadable) context.getAttribute(Constants.VIEW_PATIENT_FORM_KEY);
            if (careTeamsLoadable != null) {
                careTeamsLoadable.loadCareTeamNames();
                String theCareteamName = careTeamNameTextItem.getValueAsString();
                careteamService.getCareteam(theCareteamName, communityId, new GetCareteamNameCallback());
            } 
            PatientSearchForm patientSearchForm = (PatientSearchForm) context.getAttribute(Constants.PATIENT_SEARCH_FORM_KEY);
            if (patientSearchForm != null) {
                patientSearchForm.loadCareTeamNames();
            }
        }
    }

    public SelectItem getCareteamSelectItem() {
        return careteamSelectItem;
    }

    public void setCareteamSelectItem(SelectItem careteamSelectItem) {
        this.careteamSelectItem = careteamSelectItem;
    }

    class GetCareteamNameCallback implements AsyncCallback<CommunityCareteamDTO> {

        public void onSuccess(CommunityCareteamDTO careteamDTO) {
            if (careteamSelectItem != null) {
                final String valCommunityCareteamId = Constants.DELIMITER_SELECT_ITEM + careteamDTO.getCommunityCareteamId();
                ApplicationContext context = ApplicationContext.getInstance();
                Object objViewPatientForm = context.getAttribute(Constants.VIEW_PATIENT_FORM_KEY);
                if(objViewPatientForm instanceof com.gsihealth.dashboard.client.careteam.ViewPatientForm) {
                    ViewPatientForm viewPatientForm = (ViewPatientForm) objViewPatientForm;
                    viewPatientForm.loadCareTeamNamesAndSetvalue(valCommunityCareteamId);
                }
                else {
                    careteamSelectItem.setValue(valCommunityCareteamId);
                }
            }
        }

        public void onFailure(Throwable thrwbl) {
            SC.warn("Error retrieving care team name.");
        }
    }

    public void loadAvailableUsersData() {
        progressBarWindow.setVisible(true);
        progressBarWindow.show();
        String role = userLevelSelectItem.getValueAsString();
        List<Long> userIdsToExclude = getSelectedUserIds();
        GetAvailableUsersCallback getAvailableUsersCallback = new GetAvailableUsersCallback(role, userIdsToExclude);
        getAvailableUsersCallback.attempt();
    }
}
