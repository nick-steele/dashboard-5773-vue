package com.gsihealth.dashboard.client.careteam;

import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.widgets.grid.ListGridRecord;

/**
 *
 * @author Chad Darby
 */
public class CareTeamRecord extends ListGridRecord {

    private int index;
    private long userId;
    private long userLevelId;
    private String status;
    
    public static final String PREFIX = "Prefix";
    public static final String PATIENT_ID = "Patient ID";
    public static final String LAST_NAME = "Last Name";
    public static final String FIRST_NAME = "First Name";
    public static final String USER_LEVEL = "User Level";
    public static final String CREDENTIAL = "Credential";
    public static final String ORGANIZATION = "Organization";
    public  UserDTO userDto;
            
    public CareTeamRecord( UserDTO userDto) {
      setAttribute("UserDto", userDto);
       
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }


    public String getFirstName() {
        return getAttribute(FIRST_NAME);
    }

    public void setFirstName(String firstName) {
        setAttribute(FIRST_NAME, firstName);
    }

    public String getLastName() {
        return getAttribute(LAST_NAME);
    }

    public void setLastName(String lastName) {
        setAttribute(LAST_NAME, lastName);
    }

    public String getUserLevel() {
        return getAttribute(USER_LEVEL);
    }

    public void setUserLevel(String userLevel) {
        setAttribute(USER_LEVEL, userLevel);
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getUserLevelId() {
        return userLevelId;
    }

    public void setUserLevelId(long userLevelId) {
        this.userLevelId = userLevelId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getCredential() {
        return getAttribute(CREDENTIAL);
    }

    public void setCredential(String cred) {
        setAttribute(CREDENTIAL, cred);
    }
    
    public String getPrefix() {
        return getAttribute(PREFIX);
    }

    public void setPrefix(String prefix) {
        setAttribute(PREFIX, prefix);
    }

    public String getOrganization() {
        return getAttribute(ORGANIZATION);
    }

    public void setOrganization(String org) {
        setAttribute(ORGANIZATION, org);
    }

    /**
     * @return the userDto
     */
    public UserDTO getUserDto() {
       
       userDto.setPrefix(getPrefix());
       userDto.setUserId(getUserId());
       userDto.setFirstName(getFirstName());
       userDto.setLastName(getLastName());
       userDto.setCredentials(getCredential());
       userDto.setAccessLevel(getUserLevel());
       userDto.setOrganization(getOrganization());
       
        return userDto;
    }

    /**
     * @param userDto the userDto to set
     */
    public void setUserDto(UserDTO userDto) {
        this.userDto = userDto;
    }
}
