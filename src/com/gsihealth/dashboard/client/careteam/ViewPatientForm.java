package com.gsihealth.dashboard.client.careteam;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.Window;
import com.gsihealth.dashboard.client.admintools.user.UpdateUserContextEvent;
import com.gsihealth.dashboard.client.admintools.user.UpdateUserContextEventHandler;
import com.gsihealth.dashboard.client.callbacks.GetOrganizationNameCallback;
import com.gsihealth.dashboard.client.careteam.callbacks.GetCareteamNamesCallback;
import com.gsihealth.dashboard.client.common.FormUtils;
import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.client.common.PatientOtherIdsWindow;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.consent.healthhomeorganization.OrganizationPatientConsentWindow;
import com.gsihealth.dashboard.client.consent.nonhealthhomeprovider.NonHealthHomeProviderWindow;
import com.gsihealth.dashboard.client.eventbus.EventBusUtils;
import com.gsihealth.dashboard.client.managepatientprogram.ManagePatientProgramWindow;
import com.gsihealth.dashboard.client.service.CareteamService;
import com.gsihealth.dashboard.client.service.CareteamServiceAsync;
import com.gsihealth.dashboard.client.service.OrganizationService;
import com.gsihealth.dashboard.client.service.OrganizationServiceAsync;
import com.gsihealth.dashboard.client.util.*;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.common.util.SsnUtils;
import com.gsihealth.dashboard.entity.dto.*;
import com.gsihealth.dashboard.entity.dto.insurance.PayerClassDTO;
import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.*;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.ClickEvent;
import com.smartgwt.client.widgets.form.fields.events.ClickHandler;
import com.smartgwt.client.widgets.form.validator.DateRangeValidator;
import com.smartgwt.client.widgets.form.validator.LengthRangeValidator;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import java.util.*;
import java.util.logging.Logger;

/**
 *
 * @author Chad Darby
 */
public class ViewPatientForm extends BasePatientForm implements UpdateUserContextEventHandler{

    private final static Logger logger = Logger.getLogger("ViewPatientForm");
    public static final int FORM_WIDTH = 900;
    private static final String CONSENT_AMPM_NAME = "consent_ampm";
    private static final String CONSENT_DATE_ITEM_NAME = "consent_date_item";
    private static final String CONSENT_TIME_ITEM_NAME = "consent_time_item";
    private static final String CONSENT_VERSION_NAME = "consent_version";
    private static final String CONSENT_OBTAINED_BY = "consent_obtained_by";
    private static final String CONSENTER = "consenter";
    private static final String ADD_ASSIGN_OUT_OF_NETWORK_PROVIDERS = "Add/Assign Out of Network Providers";
    private String manageHealthHomeProviderConsent;
    private DynamicForm patientDemographicsForm;
    private DynamicForm addressForm;
    private DynamicForm organizationForm;
    private DynamicForm enrollmentLevelForm;
    private DynamicForm careTeamForm;
    private StaticTextItem patientIDStaticTextItem;
    private StaticTextItem firstNameStaticTextItem;
    private StaticTextItem middleNameStaticTextItem;
    private StaticTextItem lastNameStaticTextItem;
    private StaticTextItem dobDateItem;
    private StaticTextItem genderStaticTextItem;
    private StaticTextItem ssnStaticTextItem;
    private StaticTextItem telephoneStaticTextItem;
    private StaticTextItem stateStaticTextItem;
    private StaticTextItem address1StaticTextItem;
    private StaticTextItem address2StaticTextItem;
    private StaticTextItem cityStaticTextItem;
    private StaticTextItem zipCodeStaticTextItem;
    private SelectItem programLevelSelectItem,informationSharingSelectItem;
    private SelectItem programNameSelectItem;
    private DateItem consentDateItem, programEffectiveDateItem, programEndDateItem,dateItem;
//    private TextItem /*consentTimeTextItem,*/timeTextItem;
    private SelectItem consentObtainedBySelectItem,obtainedBySelectItem;
    private SelectItem consenterSelectItem;
    private SelectItem enrollmentStatusSelectItem;
    private StaticTextItem organizationStaticTextItem;
    private StaticTextItem organizationMrnStaticTextItem;
    private SelectItem careTeamSelectItem;
    private CareteamServiceAsync careteamService = GWT.create(CareteamService.class);
    private OrganizationServiceAsync orgService = GWT.create(OrganizationService.class);
    private PersonDTO person;
    private CareTeamViewPatientPanel viewPatientPanel;
    private String[] CONSENT_STATUSES = {"Yes", "No"};
    private String[] PLCS_NO_ENROLLMENT_STATUSES = {"Inactive"};
    private String[] PLCS_YES_ENROLLMENT_STATUSES = {"Pending", "Inactive", "Enrolled", "Assigned"};
    private SelectItem reasonforInactivationSelectItem;
//    private SelectItem /*consentAMPM,*/consentAMPMItem;
    private static long CARETEAM_NOT_SET = -1;
    private long communityCareteamId = CARETEAM_NOT_SET;
    private LinkedHashMap<String, String> usersMap;
    private String oldProgramLevelConsentStatus;
    private LinkItem manageNonHealthHomeLinkItem;
    private SpacerItem spacerItem;
    private SpacerItem spacerItem1;
    private ButtonItem manageHealthHomeButtonItem;
    private StaticTextItem ethnicityStaticTextItem, raceStaticTextItem, languageStaticTextItem;
    private StaticTextItem emailStaticTextItem;
    private long oldOrganizationId;
    private DynamicForm emergencyContactForm, insuranceForm;
    private StaticTextItem fNameEmergencyStaticTextItem, mNameEmergencyStaticTextItem, lNameEmergencyStaticTextItem, telephoneEmergencyStaticTextItem, emergencyEmailStaticTextItem, emergencyRelationshipStaticTextItem;
    private StaticTextItem primaryInsuranceStaticTextItem, secondaryInsuranceStaticTextItem, primaryMedicaidIdStaticTextItem, secondaryMedicaidIdStaticTextItem;
    private StaticTextItem primaryPayerClassStaticTextItem, primaryPayerPlanStaticTextItem, secondaryPayerClassStaticTextItem, secondaryPayerPlanStaticTextItem;
    private StaticTextItem primaryEffectiveStaticTextItem, secondaryEffectiveStaticTextItem, enrollmentChangeDateStaticTextItem;
    private String[] consentReasonsForRefusal;
    private String[] enrollmentReasonsForInactivation;
    private String disenrollmentCodeTitle;
    private String plcsTitle;
    private StaticTextItem relationshipTextItem;
    private DateTimeFormat dateFormatter;
    private LinkItem patientOtherIdsLinkItem;
    private String healthHomeLabel;
    private SelectItem healthHomeLabelSelectItem;
    private boolean programNameRequired;
    private String[] programNameCodes;
    private boolean healthHomeRequired;
    private boolean programNameBln = false;
    private long healthHomeId;
    private Date consentDateTime;
    private boolean consentNone;
    private long consentingUser;
     
    private CheckboxItem addPatientSelectItem;
    private String patientToCareTeamLabel;
    private boolean patientEngagementEnable;
    private StaticTextItem patientUserNameStaticTextItem;
    private boolean uncheckedFlag = false;
    private boolean enableCareteamPanel = false;
    private boolean showManageHealthHomeLink = false;
    private ButtonItem managePatientProgramButtonItem;
    private String managePatientPrograms;
    private PatientInfo patientInfo;
    private TextItem acuityScoreTextItem;
    private Long communityId;
    private  Date consentDate2 =new java.util.Date() ;
    private static final String[] INFORMATION_SHARING_MINOR_CODES = {"Yes","N/A"};
    private boolean childrensHealthHome=false;
    private boolean minorFlag=false;
    private boolean canConsentEnrollMinors=false;
    private boolean disableMinorDirectMessg=false;
    private HandlerRegistration handlerRegistration;
    private DataSource userDataSource;
    private DataSource consenterDataSource;

    public ViewPatientForm(CareTeamViewPatientPanel theViewPatientPanel, PersonDTO thePerson) {
        viewPatientPanel = theViewPatientPanel;
        person = thePerson;
        communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
        canConsentEnrollMinors=ApplicationContextUtils.getLoginResult().isCanConsentEnrollMinors();
        dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");

        //consentReasonsForRefusal = getConsentReasonsForRefusal();
        enrollmentReasonsForInactivation = getDisenrollmentReasons();
        setFormValidationRulesFromClientAppProperties();
        healthHomeId = getHealthHomeId(thePerson);
        minorFlag = PropertyUtils.isMinor(thePerson.getDateOfBirth());
        buildGui();

        loadCareTeamNames(); 
       
        populate();
        
        subscribeToBus();
    }
    
    /**
     * Subscribe as a handler for the event bus. Analogous to signing up as a
     * ClickHandler
     */
    private void subscribeToBus() {
        EventBus eventBus = EventBusUtils.getEventBus();

        handlerRegistration = eventBus.addHandler(UpdateUserContextEvent.TYPE, this);
    }

    @Override
    public void destroy() {
        super.destroy();

        // remove our handler
        if (handlerRegistration != null) {
            handlerRegistration.removeHandler();
        }
    }

    private long getHealthHomeId(PersonDTO thePerson) {
        long healthHomeId = 0;
        ProgramHealthHomeDTO programHealthHomeDTO = thePerson.getPatientEnrollment().getProgramHealthHome();
        if (programHealthHomeDTO != null) {
            healthHomeId = programHealthHomeDTO.getId();
        }
        return healthHomeId;
    }

    @Override
    public void loadCareTeamNames() {
        GetCareteamNamesCallback getCareteamNamesCallback = 
                new GetCareteamNamesCallback(careTeamSelectItem, communityId);
        getCareteamNamesCallback.attempt();
    }
    
    public void loadCareTeamNamesAndSetvalue(String newCareTeamValue)
    {
        GetCareteamNamesCallback getCareteamNamesCallback = 
                new GetCareteamNamesCallback(careTeamSelectItem, communityId, newCareTeamValue);
        getCareteamNamesCallback.attempt();
        
        //if care team has been changed, set the flag
        if(newCareTeamValue != null) {
            newCareTeamValue = newCareTeamValue.split(Constants.DELIMITER_SELECT_ITEM)[1];
            long theNewcommunityCareteamId = Long.parseLong(newCareTeamValue);

            if(communityCareteamId != theNewcommunityCareteamId) {
                viewPatientPanel.setLastCareTeamWasBlankSelected(true);
            }
        }  
                
    }

    protected void buildGui() {

        setMembersMargin(10);

        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        disenrollmentCodeTitle = loginResult.getDisenrollmentCodeTitle();
        plcsTitle = loginResult.getPlcsTitle();

        patientDemographicsForm = buildPatientDemographicsForm();
        addMember(patientDemographicsForm);

        addressForm = buildAddressForm();
        addMember(addressForm);

        emergencyContactForm = buildEmergencyContactForm();
        addMember(emergencyContactForm);

        organizationForm = buildOrganizationForm();
        addMember(organizationForm);

        enrollmentLevelForm = buildEnrollmentForm();
        addMember(enrollmentLevelForm);

        careTeamForm = buildCareTeamForm();
        addMember(careTeamForm);

        insuranceForm = buildInsuranceForm();
        addMember(insuranceForm);
    }

    private DynamicForm buildEnrollmentForm() {
        DynamicForm theForm = new DynamicForm();
        theForm.setGroupTitle("Enrollment");
        theForm.setIsGroup(true);
        theForm.setNumCols(11);
        setFormWidth(theForm);
        theForm.setWrapItemTitles(true);
        theForm.setRequiredTitlePrefix(FormUtils.REQUIRED_TITLE_PREFIX);
        theForm.setBrowserSpellCheck(false);

         RegExpValidator decimalRegExValidator = ValidationUtils.getDecimalRegExValidator();
        
      if(childrensHealthHome){ 
        programLevelSelectItem = new SelectItem("program_consent_status", plcsTitle);
        programLevelSelectItem.setTabIndex(1);
        programLevelSelectItem.setValueMap(getProgramLevelConsentStatusCodes());
        programLevelSelectItem.setRequired(false);
        programLevelSelectItem.setWrapTitle(false);
        programLevelSelectItem.addChangedHandler(new ProgramLevelConsentStatusChangedHandler());
        programLevelSelectItem.setWidth(100);
        programLevelSelectItem.setColSpan(3);
        
        informationSharingSelectItem = new SelectItem("information_sharing_minor", "Information Sharing (Minor)");
        informationSharingSelectItem.setTabIndex(7);
        informationSharingSelectItem.setValueMap(INFORMATION_SHARING_MINOR_CODES);
        informationSharingSelectItem.setDefaultValue("Select");
        informationSharingSelectItem.setRequired(false);
        informationSharingSelectItem.setWrapTitle(false);
        informationSharingSelectItem.setWidth(100);
        informationSharingSelectItem.setColSpan(2);
        informationSharingSelectItem.setEndRow(false);
        informationSharingSelectItem.setDisabled(true);
        informationSharingSelectItem.addChangedHandler(new InformationStatusChangedHandler());
        
        acuityScoreTextItem = new TextItem("acuityScoreTextItem", "Acuity Score");
        acuityScoreTextItem.setTabIndex(7);
        acuityScoreTextItem.setWrapTitle(false);
        acuityScoreTextItem.setColSpan(1);
        ValidationUtils.setValidators(acuityScoreTextItem, decimalRegExValidator, true);

        // Manage Patient Program
        managePatientProgramButtonItem = new ButtonItem("ManagePatientPrograms",managePatientPrograms);
        managePatientProgramButtonItem.setVisible(true);
        managePatientProgramButtonItem.setShowTitle(false);
        managePatientProgramButtonItem.setColSpan(1);
        managePatientProgramButtonItem.setWidth(150);
        managePatientProgramButtonItem.setEndRow(true);
        managePatientProgramButtonItem.addClickHandler(new ManagePatientProgramClickHandler());
        managePatientProgramButtonItem.setStartRow(false);


        consentDateItem = new DateItem(CONSENT_DATE_ITEM_NAME, "Consent Date");
        consentDateItem.setTabIndex(2);
        consentDateItem.setRequired(false);
        consentDateItem.setWrapTitle(false);
//        consentDateItem.setEndRow(true);
        consentDateItem.setColSpan(3);
        consentDateItem.setTextAlign(Alignment.LEFT);
        consentDateItem.setUseMask(true);
        consentDateItem.setEnforceDate(true);
        consentDateItem.setWidth(100);
        consentDateItem.setRequired(true);
        consentDateItem.setStartDate(dateFormatter.parse("01/01/1900"));

        // add date validation rules
        DateRangeValidator dateRangeValidator = new DateRangeValidator();
        Date today = DateUtils.getDateAtEndOfToday();
        dateRangeValidator.setMax(today);

        dateRangeValidator.setErrorMessage("Date must be today's date or prior.");
        consentDateItem.setValidators(dateRangeValidator);
        consentDateItem.setValidateOnChange(true);
        
        dateItem = new DateItem("Date_item", "Date");
        dateItem.setTabIndex(8);
        dateItem.setRequired(false);
        dateItem.setWrapTitle(false);
        dateItem.setColSpan(3);

        dateItem.setTextAlign(Alignment.LEFT);
        dateItem.setUseMask(true);
        dateItem.setEnforceDate(true);
        dateItem.setWidth(100);
        dateItem.setDisabled(true);
//        dateItem.setRequired(true);
        dateItem.setInvalidDateStringMessage("A valid consent date is required.");
        dateItem.setStartDate(dateFormatter.parse("01/01/1900"));
        // add date validation rules
       // add date validation rules
        DateRangeValidator dateRangeValidation = new DateRangeValidator();
        Date todaysDate = DateUtils.getDateAtEndOfToday();
        dateRangeValidator.setMax(today);

        dateRangeValidator.setErrorMessage("Date must be today's date or prior.");
        dateItem.setValidators(dateRangeValidation);
        dateItem.setValidateOnChange(true);

//        consentObtainedBySelectItem = new SelectItem(CONSENT_OBTAINED_BY, "Consent Obtained By");
//        consentObtainedBySelectItem.setEndRow(false);
//        consentObtainedBySelectItem.setAllowEmptyValue(true);
//        consentObtainedBySelectItem.setWrapTitle(false);
//        consentObtainedBySelectItem.setColSpan(3);
//        consentObtainedBySelectItem.setWidth(100);
//        consentObtainedBySelectItem.setStartRow(true);

          populateConsentObtainedBySelectItem(3);
        
        obtainedBySelectItem = new SelectItem("obtained_by", "Obtained By");
        obtainedBySelectItem.setTabIndex(11);
        obtainedBySelectItem.setEndRow(false);
        obtainedBySelectItem.setAllowEmptyValue(true);
        obtainedBySelectItem.setWrapTitle(false);
        obtainedBySelectItem.setColSpan(3);
        obtainedBySelectItem.setWidth(100);
        obtainedBySelectItem.setDisabled(true);
       
//        consentTimeTextItem = new TextItem(CONSENT_TIME_ITEM_NAME, "Consent Time");
//        consentTimeTextItem.setWidth(45);
//        consentTimeTextItem.setMask("##:##");
//        consentTimeTextItem.setValidateOnExit(true);
//        consentTimeTextItem.setMaskSaveLiterals(true);
        LengthRangeValidator consentTimeLengthValidator = new LengthRangeValidator();
        consentTimeLengthValidator.setMin(5);
        consentTimeLengthValidator.setMax(5);
        consentTimeLengthValidator.setErrorMessage("Invalid length");
//        consentTimeTextItem.setValidators(consentTimeLengthValidator, ValidationUtils.getTimeRegExpValidator());
        
//        timeTextItem = new TextItem("time", "Time");
//        timeTextItem.setWidth(45);
//        timeTextItem.setMask("##:##");
//        timeTextItem.setValidateOnExit(true);
//        timeTextItem.setMaskSaveLiterals(true);
//        timeTextItem.setDisabled(true);
        LengthRangeValidator TimeLengthValidator = new LengthRangeValidator();
        TimeLengthValidator.setMin(5);
        TimeLengthValidator.setMax(5);
        TimeLengthValidator.setErrorMessage("A valid consent time is required.");
//        timeTextItem.setValidators(consentTimeLengthValidator, ValidationUtils.getTimeRegExpValidator());

        

//        consentAMPM = new SelectItem(CONSENT_AMPM_NAME);
//        consentAMPM.setShowTitle(false);
//        consentAMPM.setWidth(70);
////        consentAMPM.setEndRow(true);
//        consentAMPM.setValueMap(new String[]{"AM", "PM"});
//        consentAMPM.setAllowEmptyValue(true);
//        consentAMPM.setRequired(true);
//        consentAMPM.setValidateOnExit(true);
//        consentAMPM.setColSpan(2);
        
//        consentAMPMItem = new SelectItem("am_pm_Name");
//        consentAMPMItem.setTabIndex(10);
//        consentAMPMItem.setShowTitle(false);
//        consentAMPMItem.setEndRow(true);
//        consentAMPMItem.setWidth(70);
//        consentAMPMItem.setValueMap(new String[]{"AM", "PM"});
//        consentAMPMItem.setAllowEmptyValue(true);
//        consentAMPMItem.setRequired(true);
//        consentAMPMItem.setValidateOnExit(true);
//        consentAMPMItem.setColSpan(5);
//        consentAMPMItem.setDisabled(true);

        populateConsenterSelectItem(3);
        
        enrollmentStatusSelectItem = new SelectItem("newComboBoxItem_2", "Enrollment Status");
        enrollmentStatusSelectItem.setTabIndex(6);
        enrollmentStatusSelectItem.setValueMap(PLCS_NO_ENROLLMENT_STATUSES);
        enrollmentStatusSelectItem.setRequired(false);
        enrollmentStatusSelectItem.setDefaultValue("Pending");
        enrollmentStatusSelectItem.addChangedHandler(new EnrollmentStatusChangedHandler());
        enrollmentStatusSelectItem.setColSpan(2);
        enrollmentStatusSelectItem.setWidth(100);
        enrollmentStatusSelectItem.setStartRow(true);

        enrollmentChangeDateStaticTextItem = new StaticTextItem("newComboBoxItem_6", "Dated");
        enrollmentChangeDateStaticTextItem.setColSpan(4);

        reasonforInactivationSelectItem = new SelectItem("newComboBoxItem_4", disenrollmentCodeTitle);
        reasonforInactivationSelectItem.setValueMap(enrollmentReasonsForInactivation);
        reasonforInactivationSelectItem.setRequired(false);
        reasonforInactivationSelectItem.setWrapTitle(false);
        reasonforInactivationSelectItem.setValue(FormUtils.EMPTY);
        reasonforInactivationSelectItem.setVisible(false);
        reasonforInactivationSelectItem.setRequiredMessage("A valid reason for inactivation is required.");
        reasonforInactivationSelectItem.setEndRow(true);
        reasonforInactivationSelectItem.setColSpan(1);

        // Manage Consent for Health Home Providers
        manageHealthHomeButtonItem = new ButtonItem("ManageConsentforHealthHomeProviders",manageHealthHomeProviderConsent);
        manageHealthHomeButtonItem.setVisible(true);
        manageHealthHomeButtonItem.setShowTitle(false);
        manageHealthHomeButtonItem.setColSpan(1);
        manageHealthHomeButtonItem.setWidth(150);
        manageHealthHomeButtonItem.setEndRow(true);
        manageHealthHomeButtonItem.addClickHandler(new ManageHealthHomeClickHandler());
        manageHealthHomeButtonItem.setStartRow(false);
//        manageHealthHomeLinkItem.setDisabled(!showManageHealthHomeLink);

        SpacerItem spacerItem = new SpacerItem();
        spacerItem.setEndRow(false);
        spacerItem.setWidth(2);
           if (isIE11Browser()) {
              SpacerItem spacerItem2 = new SpacerItem();
              spacerItem2.setWidth(2);
              spacerItem2.setEndRow(true);
              spacerItem2.setStartRow(false);
              theForm.setFields(programLevelSelectItem, informationSharingSelectItem, spacerItem, acuityScoreTextItem, spacerItem2, consentDateItem, dateItem, spacerItem, managePatientProgramButtonItem, /*consentTimeTextItem, consentAMPM,timeTextItem,consentAMPMItem, */ consentObtainedBySelectItem, obtainedBySelectItem,spacerItem, manageHealthHomeButtonItem,consenterSelectItem, enrollmentStatusSelectItem, enrollmentChangeDateStaticTextItem, reasonforInactivationSelectItem);
          } else {
               theForm.setFields(programLevelSelectItem, informationSharingSelectItem, spacerItem, acuityScoreTextItem, consentDateItem, dateItem, spacerItem, managePatientProgramButtonItem, /*consentTimeTextItem, consentAMPM,timeTextItem,consentAMPMItem, */ consentObtainedBySelectItem, obtainedBySelectItem , spacerItem, manageHealthHomeButtonItem, consenterSelectItem, enrollmentStatusSelectItem, enrollmentChangeDateStaticTextItem, reasonforInactivationSelectItem);
          }
      
        }
        else{            
         buildFormFields();
            
        SpacerItem spacerItem = new SpacerItem();
        spacerItem.setEndRow(false);
        spacerItem.setWidth(5);
     
        if (isIE11Browser()) {
              SpacerItem spacerItem2 = new SpacerItem();              
              spacerItem2.setWidth(2);
              spacerItem2.setEndRow(true);
              spacerItem2.setStartRow(false);
              theForm.setFields(programLevelSelectItem,spacerItem,acuityScoreTextItem,spacerItem2, consentDateItem,spacerItem, managePatientProgramButtonItem,/*consentTimeTextItem, consentAMPM,*/ consentObtainedBySelectItem, spacerItem,manageHealthHomeButtonItem , consenterSelectItem,enrollmentStatusSelectItem, enrollmentChangeDateStaticTextItem,spacerItem,reasonforInactivationSelectItem);
          } else {
            theForm.setFields(programLevelSelectItem,spacerItem,acuityScoreTextItem, consentDateItem,spacerItem, managePatientProgramButtonItem,/*consentTimeTextItem, consentAMPM,*/ consentObtainedBySelectItem,spacerItem,manageHealthHomeButtonItem ,consenterSelectItem,enrollmentStatusSelectItem, enrollmentChangeDateStaticTextItem,spacerItem,reasonforInactivationSelectItem );
        }

        }
      
        return theForm;
    }

    private void buildFormFields(){
         RegExpValidator decimalRegExValidator = ValidationUtils.getDecimalRegExValidator();
        
        programLevelSelectItem = new SelectItem("program_consent_status", plcsTitle);
        programLevelSelectItem.setTabIndex(1);
        programLevelSelectItem.setValueMap(getProgramLevelConsentStatusCodes());
        programLevelSelectItem.setRequired(false);
        programLevelSelectItem.setWrapTitle(false);
        programLevelSelectItem.addChangedHandler(new ProgramLevelConsentStatusChangedHandler());
        programLevelSelectItem.setWidth(100);
        programLevelSelectItem.setColSpan(6);
        
        acuityScoreTextItem = new TextItem("acuityScoreTextItem", "Acuity Score");
        acuityScoreTextItem.setTabIndex(7);
        acuityScoreTextItem.setWrapTitle(false);
        acuityScoreTextItem.setColSpan(1);
        acuityScoreTextItem.setEndRow(true);
        ValidationUtils.setValidators(acuityScoreTextItem, decimalRegExValidator, true);

        // Manage Patient Program
        managePatientProgramButtonItem = new ButtonItem("ManagePatientPrograms",managePatientPrograms);
        managePatientProgramButtonItem.setVisible(true);
        managePatientProgramButtonItem.setShowTitle(false);
        managePatientProgramButtonItem.setColSpan(1);
        managePatientProgramButtonItem.setWidth(150);
        managePatientProgramButtonItem.setEndRow(true);
        managePatientProgramButtonItem.addClickHandler(new ManagePatientProgramClickHandler());
        managePatientProgramButtonItem.setStartRow(false);

        consentDateItem = new DateItem(CONSENT_DATE_ITEM_NAME, "Consent Date");
        consentDateItem.setTabIndex(2);
        consentDateItem.setRequired(false);
        consentDateItem.setWrapTitle(false);
//        consentDateItem.setEndRow(true);
        consentDateItem.setColSpan(7);
        consentDateItem.setTextAlign(Alignment.LEFT);
        consentDateItem.setUseMask(true);
        consentDateItem.setEnforceDate(true);
        consentDateItem.setWidth(100);
        consentDateItem.setRequired(true);
        consentDateItem.setStartDate(dateFormatter.parse("01/01/1900"));

        // add date validation rules
        DateRangeValidator dateRangeValidator = new DateRangeValidator();
        Date today = DateUtils.getDateAtEndOfToday();
        dateRangeValidator.setMax(today);

        dateRangeValidator.setErrorMessage("Date must be today's date or prior.");
        consentDateItem.setValidators(dateRangeValidator);
        consentDateItem.setValidateOnChange(true);

//        consentObtainedBySelectItem = new SelectItem(CONSENT_OBTAINED_BY, "Consent Obtained By");
//        consentObtainedBySelectItem.setEndRow(false);
//        consentObtainedBySelectItem.setAllowEmptyValue(true);
//        consentObtainedBySelectItem.setWrapTitle(false);
//        consentObtainedBySelectItem.setColSpan(7);
//        consentObtainedBySelectItem.setWidth(100);
//        consentObtainedBySelectItem.setStartRow(true);

          populateConsentObtainedBySelectItem(7);

//        consentTimeTextItem = new TextItem(CONSENT_TIME_ITEM_NAME, "Consent Time");
//        consentTimeTextItem.setWidth(45);
//        consentTimeTextItem.setMask("##:##");
//        consentTimeTextItem.setValidateOnExit(true);
//        consentTimeTextItem.setMaskSaveLiterals(true);

        LengthRangeValidator consentTimeLengthValidator = new LengthRangeValidator();
        consentTimeLengthValidator.setMin(5);
        consentTimeLengthValidator.setMax(5);
        consentTimeLengthValidator.setErrorMessage("Invalid length");
 //       consentTimeTextItem.setValidators(consentTimeLengthValidator, ValidationUtils.getTimeRegExpValidator());

//        consentAMPM = new SelectItem(CONSENT_AMPM_NAME);
//        consentAMPM.setShowTitle(false);
//        consentAMPM.setWidth(70);
//        consentAMPM.setEndRow(true);
//        consentAMPM.setValueMap(new String[]{"AM", "PM"});
//        consentAMPM.setAllowEmptyValue(true);
//        consentAMPM.setRequired(true);
//        consentAMPM.setValidateOnExit(true);
//        consentAMPM.setColSpan(6);

        populateConsenterSelectItem(3);

        enrollmentStatusSelectItem = new SelectItem("newComboBoxItem_2", "Enrollment Status");
        enrollmentStatusSelectItem.setTabIndex(6);
        enrollmentStatusSelectItem.setValueMap(PLCS_NO_ENROLLMENT_STATUSES);
        enrollmentStatusSelectItem.setRequired(false);
        enrollmentStatusSelectItem.setDefaultValue("Pending");
        enrollmentStatusSelectItem.addChangedHandler(new EnrollmentStatusChangedHandler());
        enrollmentStatusSelectItem.setColSpan(2);
        enrollmentStatusSelectItem.setWidth(100);
        enrollmentStatusSelectItem.setStartRow(true);

        enrollmentChangeDateStaticTextItem = new StaticTextItem("newComboBoxItem_6", "Dated");
        enrollmentChangeDateStaticTextItem.setColSpan(3);

        reasonforInactivationSelectItem = new SelectItem("newComboBoxItem_4", disenrollmentCodeTitle);
        reasonforInactivationSelectItem.setValueMap(enrollmentReasonsForInactivation);
        reasonforInactivationSelectItem.setRequired(false);
        reasonforInactivationSelectItem.setWrapTitle(false);
        reasonforInactivationSelectItem.setValue(FormUtils.EMPTY);
        reasonforInactivationSelectItem.setVisible(false);
        reasonforInactivationSelectItem.setRequiredMessage("A valid reason for inactivation is required.");
        reasonforInactivationSelectItem.setEndRow(true);
        reasonforInactivationSelectItem.setColSpan(1);

        // Manage Consent for Health Home Providers
        manageHealthHomeButtonItem = new ButtonItem("ManageConsentforHealthHomeProviders",manageHealthHomeProviderConsent);
        manageHealthHomeButtonItem.setVisible(true);
        manageHealthHomeButtonItem.setShowTitle(false);
        manageHealthHomeButtonItem.setColSpan(1);
        manageHealthHomeButtonItem.setWidth(150);
        manageHealthHomeButtonItem.setEndRow(true);
        manageHealthHomeButtonItem.addClickHandler(new ManageHealthHomeClickHandler());
        manageHealthHomeButtonItem.setStartRow(false);
//        manageHealthHomeLinkItem.setDisabled(!showManageHealthHomeLink);

        StaticTextItem administrativeUseLabel2 = new StaticTextItem("", "");
        administrativeUseLabel2.setValue("");
        administrativeUseLabel2.setWrapTitle(false);
        administrativeUseLabel2.setEndRow(true);
        //administrativeUseLabel.setWidth(150);
        administrativeUseLabel2.setAlign(Alignment.RIGHT);
          
    }
//    private String[] getProgramNameCodesValues(long healthHomeId) {
//
//        String healthHomeWithDelimiter = Constants.DELIMITER_SELECT_ITEM + healthHomeId;
//        String healthHome = healthHomeWithDelimiter.split(Constants.DELIMITER_SELECT_ITEM)[1];
//        LinkedHashMap<Long, ProgramNameDTO> programNameHealthHomeMap = ApplicationContextUtils.getProgramNameForHealthHomeValue();
//        Collection<ProgramNameDTO> dto = programNameHealthHomeMap.values();
//        ArrayList<ProgramNameDTO> programList = new ArrayList<ProgramNameDTO>(dto);
//
//        String[] programName = FormUtils.convertProgramNameHealthHome(healthHome, programList);
//
//        String[] programNameCodes = FormUtils.getCodesWithAddedBlankOption(programName);
//
//        return programNameCodes;
//    }

    private DynamicForm buildOrganizationForm() {
        DynamicForm theForm = new DynamicForm();

        theForm.setWrapItemTitles(true);
        theForm.setGroupTitle("Organization");
        theForm.setIsGroup(true);
        theForm.setNumCols(6);
        setFormWidth(theForm);

        organizationStaticTextItem = new StaticTextItem("organizationComboBoxItem", "Care Management Organization");
        organizationStaticTextItem.setWrapTitle(false);

        organizationMrnStaticTextItem = new StaticTextItem("organizationMrn", "Patient MRN");
        organizationMrnStaticTextItem.setWrapTitle(false);
        theForm.setFields(organizationStaticTextItem, new SpacerItem(), new SpacerItem(), organizationMrnStaticTextItem);

        return theForm;
    }

    private DynamicForm buildAddressForm() {
        DynamicForm theForm = new DynamicForm();
        theForm.setNumCols(6);
        theForm.setGroupTitle("Contact Information");
        theForm.setIsGroup(true);
        setFormWidth(theForm);

        address1StaticTextItem = new StaticTextItem("streetAddress1", "Address 1 ");

        address2StaticTextItem = new StaticTextItem("streetAddress2", "Address 2");

        cityStaticTextItem = new StaticTextItem("newStaticTextItem_3", "City");

        stateStaticTextItem = new StaticTextItem("newSelectItem_8", "State");

        zipCodeStaticTextItem = new StaticTextItem("newStaticTextItem_12", "Zip Code");
        zipCodeStaticTextItem.setWidth(100);
        zipCodeStaticTextItem.setEndRow(true);

        telephoneStaticTextItem = new StaticTextItem("newStaticTextItem_1", "Telephone");
        telephoneStaticTextItem.setWidth(100);

        // email address
        emailStaticTextItem = new StaticTextItem("emailStaticTextItem", "Email Address");
        emailStaticTextItem.setEndRow(true);

        theForm.setFields(address1StaticTextItem, cityStaticTextItem, telephoneStaticTextItem, address2StaticTextItem, stateStaticTextItem, emailStaticTextItem, new SpacerItem(), new SpacerItem(), zipCodeStaticTextItem);

        return theForm;
    }

    private DynamicForm buildPatientDemographicsForm() {
        DynamicForm theForm = new DynamicForm();
        setFormWidth(theForm);
        theForm.setCellPadding(2);
        theForm.setNumCols(6);
        theForm.setIsGroup(true);
        theForm.setGroupTitle("Patient Demographics");

        patientIDStaticTextItem = new StaticTextItem("patientIDStaticTextItem", "Patient ID");
        patientIDStaticTextItem.setWrapTitle(false);
        patientIDStaticTextItem.setEndRow(false);

        firstNameStaticTextItem = new StaticTextItem("firstNameStaticTextItem", "First Name");
        firstNameStaticTextItem.setWrapTitle(false);
        firstNameStaticTextItem.setStartRow(true);

        middleNameStaticTextItem = new StaticTextItem("middleNameStaticTextItem", "Middle Name");
        middleNameStaticTextItem.setWrapTitle(false);

        lastNameStaticTextItem = new StaticTextItem("lastNameStaticTextItem", "Last Name");
        lastNameStaticTextItem.setWrapTitle(false);

        patientOtherIdsLinkItem = new LinkItem();
        patientOtherIdsLinkItem.setValue("Patient other IDs");
        patientOtherIdsLinkItem.setVisible(true);
        patientOtherIdsLinkItem.setShowTitle(false);
        patientOtherIdsLinkItem.setColSpan(2);
        // patientOtherIdsLinkItem.setEndRow(true);
        patientOtherIdsLinkItem.addClickHandler(new OtherPatientIdsClickHandler());

        //Patient consentingUser Name
        patientUserNameStaticTextItem = new StaticTextItem("patientUserName", "Patient Username");
        patientUserNameStaticTextItem.setWrapTitle(false);
        patientUserNameStaticTextItem.setEndRow(true);
        if (!patientEngagementEnable) {
            patientUserNameStaticTextItem.setVisible(false);
        }

        ssnStaticTextItem = new StaticTextItem("ssnStaticTextItem", "SSN");
        ssnStaticTextItem.setWrapTitle(false);

        ethnicityStaticTextItem = new StaticTextItem("ethnicityStaticTextItem", "Ethnicity");
        ethnicityStaticTextItem.setEndRow(true);
        ethnicityStaticTextItem.setWrapTitle(false);
        ethnicityStaticTextItem.setWidth(90);

        // Race
        raceStaticTextItem = new StaticTextItem("raceStaticTextItem", "Race");
        raceStaticTextItem.setEndRow(true);
        raceStaticTextItem.setWrapTitle(false);

        // Preferred Language
        languageStaticTextItem = new StaticTextItem("languageStaticTextItem", "Preferred Language");
        languageStaticTextItem.setEndRow(true);
        languageStaticTextItem.setWrapTitle(false);

        dobDateItem = new StaticTextItem("dateOfBirthDateItem", "Date of Birth");
        dobDateItem.setWrapTitle(false);
        dobDateItem.setTextAlign(Alignment.LEFT);
        dobDateItem.setWidth(90);

        genderStaticTextItem = new StaticTextItem("genderSelectItem", "Gender");
        genderStaticTextItem.setWrapTitle(false);
        genderStaticTextItem.setWidth(90);

        SpacerItem spacerItem1 = new SpacerItem();
        spacerItem1.setName("spacer1");

        theForm.setFields(patientIDStaticTextItem, patientOtherIdsLinkItem, patientUserNameStaticTextItem,
                firstNameStaticTextItem, ssnStaticTextItem, ethnicityStaticTextItem,
                middleNameStaticTextItem, dobDateItem, raceStaticTextItem,
                lastNameStaticTextItem, genderStaticTextItem, languageStaticTextItem);

        return theForm;
    }

    private DynamicForm buildEmergencyContactForm() {
        DynamicForm theForm = new DynamicForm();
        theForm.setWrapItemTitles(true);
        theForm.setGroupTitle("Emergency Contact");
        theForm.setIsGroup(true);
        theForm.setNumCols(6);
        setFormWidth(theForm);

        fNameEmergencyStaticTextItem = new StaticTextItem("fNameEmergencyStaticTextItem", "First Name");
        fNameEmergencyStaticTextItem.setWrapTitle(false);

        mNameEmergencyStaticTextItem = new StaticTextItem("mNameEmergencyStaticTextItem", "Middle Name");
        mNameEmergencyStaticTextItem.setWrapTitle(false);

        lNameEmergencyStaticTextItem = new StaticTextItem("lNameEmergencyStaticTextItem", "Last Name");
        lNameEmergencyStaticTextItem.setWrapTitle(false);
        lNameEmergencyStaticTextItem.setEndRow(true);

        telephoneEmergencyStaticTextItem = new StaticTextItem("telephoneEmergencyStaticTextItem", "Telephone");
        telephoneEmergencyStaticTextItem.setWrapTitle(false);

        // email address
        emergencyEmailStaticTextItem = new StaticTextItem("emergencyEmailStaticTextItem", "Email Address");
        emergencyEmailStaticTextItem.setWrapTitle(false);

        relationshipTextItem = new StaticTextItem("relationSelectItem", "Relationship");
        relationshipTextItem.setWrapTitle(false);
        relationshipTextItem.setTabIndex(8);
        relationshipTextItem.setEndRow(true);

        theForm.setFields(fNameEmergencyStaticTextItem, mNameEmergencyStaticTextItem, lNameEmergencyStaticTextItem, telephoneEmergencyStaticTextItem, emergencyEmailStaticTextItem, relationshipTextItem);

        return theForm;
    }

    private DynamicForm buildInsuranceForm() {
        DynamicForm theForm = new DynamicForm();
        theForm.setWrapItemTitles(true);
        theForm.setGroupTitle("Insurance");
        theForm.setIsGroup(true);
        theForm.setNumCols(10);
        setFormWidth(theForm);

        primaryInsuranceStaticTextItem = new StaticTextItem("primaryInsuranceStaticTextItem", "Primary");
        primaryInsuranceStaticTextItem.setWrapTitle(false);
        primaryInsuranceStaticTextItem.setEndRow(true);

        secondaryInsuranceStaticTextItem = new StaticTextItem("secondaryInsuranceStaticTextItem", "Secondary");
        secondaryInsuranceStaticTextItem.setWrapTitle(false);
        secondaryInsuranceStaticTextItem.setEndRow(true);

        primaryMedicaidIdStaticTextItem = new StaticTextItem("primaryMedicaidIdStaticTextItem", "&#09;&#09;Medicaid/Medicare/Payer ID");
        primaryMedicaidIdStaticTextItem.setEndRow(true);
        primaryMedicaidIdStaticTextItem.setWrapTitle(false);
        primaryMedicaidIdStaticTextItem.setAlign(Alignment.LEFT);

        secondaryMedicaidIdStaticTextItem = new StaticTextItem("secondaryMedicaidIdStaticTextItem", "&#09;&#09;Medicaid/Medicare/Payer ID");
        secondaryMedicaidIdStaticTextItem.setEndRow(true);

        primaryPayerClassStaticTextItem = new StaticTextItem("primaryPayerClassStaticTextItem", "Payer Class");
        primaryPayerClassStaticTextItem.setWrapTitle(false);

        primaryPayerPlanStaticTextItem = new StaticTextItem("primaryPayerPlanStaticTextItem", "Payer Plan");
        primaryPayerPlanStaticTextItem.setWrapTitle(false);
        primaryPayerPlanStaticTextItem.setWidth(100);

        secondaryPayerClassStaticTextItem = new StaticTextItem("secondaryPayerClassStaticTextItem", "Payer Class");
        secondaryPayerClassStaticTextItem.setWrapTitle(false);

        secondaryPayerPlanStaticTextItem = new StaticTextItem("secondaryPayerPlanStaticTextItem", "Payer Plan");
        secondaryPayerPlanStaticTextItem.setWrapTitle(false);
        secondaryPayerPlanStaticTextItem.setWidth(100);

        primaryEffectiveStaticTextItem = new StaticTextItem("primaryEffectiveStaticTextItem", "&#09;&#09;Effective Date");
        primaryEffectiveStaticTextItem.setEndRow(true);
        primaryEffectiveStaticTextItem.setWrapTitle(false);

        secondaryEffectiveStaticTextItem = new StaticTextItem("secondaryEffectiveStaticTextItem", "&#09;&#09;Effective Date");
        secondaryEffectiveStaticTextItem.setEndRow(true);
        secondaryEffectiveStaticTextItem.setWrapTitle(false);

        SpacerItem space = new SpacerItem();
        space.setColSpan(4);

        theForm.setFields(primaryInsuranceStaticTextItem, new SpacerItem(), primaryPayerClassStaticTextItem, space, primaryMedicaidIdStaticTextItem, new SpacerItem(), primaryPayerPlanStaticTextItem, space, primaryEffectiveStaticTextItem, secondaryInsuranceStaticTextItem, new SpacerItem(), secondaryPayerClassStaticTextItem, space, secondaryMedicaidIdStaticTextItem, new SpacerItem(), secondaryPayerPlanStaticTextItem, space, secondaryEffectiveStaticTextItem);

        return theForm;
    }

    public boolean validate() {

        boolean validEnrollmentForm = enrollmentLevelForm.validate();
        boolean validDobAndConsentDate = validateDobAndConsentDate();

        boolean isValid = validEnrollmentForm && validDobAndConsentDate;
        return isValid;
    }

    private boolean validateDobAndConsentDate() {

        Date dob = person.getDateOfBirth();

        Date consentDate = consentDateItem.getValueAsDate();

        boolean result = false;

        if (dob == null || consentDate == null) {
            result = true;
        }

        if ((dob != null) && (consentDate != null)) {
            result = dob.before(consentDate);
        } else {
            result = true;
        }

        if (!result) {
            enrollmentLevelForm.setFieldErrors(CONSENT_DATE_ITEM_NAME, "A valid consent date is required.", true);
        }

        return result;
    }

    private void populateConsentObtainedBy(PatientEnrollmentDTO patientEnrollment) {
        final long consentObtainedByUserId = patientEnrollment.getConsentObtainedByUserId();
        consentingUser = consentObtainedByUserId;
        if (consentObtainedByUserId > Constants.EMPTY_USER) {

            // work around for sorting by keys. prepend numeric key with a string
            // to preserved sorted order
            // http://code.google.com/p/smartgwt/issues/detail?id=569
            consentObtainedBySelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + consentObtainedByUserId);
        }
    }
     protected void populateConsentObtainedBySelectItem(int colSpan) {
        //4th row
        consentObtainedBySelectItem = buildUserSelectItem(CONSENT_OBTAINED_BY, true);
        consentObtainedBySelectItem.setTabIndex(5);
        consentObtainedBySelectItem.setEndRow(false);
        consentObtainedBySelectItem.setAllowEmptyValue(true);
        consentObtainedBySelectItem.setWrapTitle(false);
        consentObtainedBySelectItem.setColSpan(colSpan);
        consentObtainedBySelectItem.setWidth(100);
        consentObtainedBySelectItem.setStartRow(true);
    }
    
    private SelectItem buildUserSelectItem(String title, boolean required) {
         createUserDataSource();
        consentObtainedBySelectItem = new SelectItem(title, "Consent Obtained By");
//        consentObtainedBySelectItem.setOptionDataSource(userDataSource);
        consentObtainedBySelectItem.setValueField("value");
        consentObtainedBySelectItem.setDisplayField("display");
        return consentObtainedBySelectItem;
    }
    
    private void createUserDataSource() {
        userDataSource = new DataSource();
        userDataSource.setClientOnly(true);
        userDataSource.setFields(
                new DataSourceField("value", FieldType.TEXT),
                new DataSourceField("display", FieldType.TEXT));
    }
    
     protected void populateConsenterSelectItem(int colSpan) {
        //5th row
        consenterSelectItem = buildConsenterSelectItem(CONSENTER, true);
        consenterSelectItem.setTabIndex(5);
        consenterSelectItem.setEndRow(false);
        consenterSelectItem.setAllowEmptyValue(true);
        consenterSelectItem.setWrapTitle(false);
        consenterSelectItem.setColSpan(colSpan);
        consenterSelectItem.setWidth(100);
        consenterSelectItem.setStartRow(true);
        consenterSelectItem.setRequired(true);
    }

     private SelectItem buildConsenterSelectItem(String title, boolean required) {
        createConsenterDataSource();
        consenterSelectItem = new SelectItem(title, "Consenter");
//        consenterSelectItem.setOptionDataSource(consenterDataSource);
        consenterSelectItem.setValueField("value");
        consenterSelectItem.setDisplayField("display");
        return consenterSelectItem;
    }

       private void createConsenterDataSource() {
        consenterDataSource = new DataSource();
        consenterDataSource.setClientOnly(true);
        consenterDataSource.setFields(
                new DataSourceField("value", FieldType.TEXT),
                new DataSourceField("display", FieldType.TEXT));
    }
   

    private void loadProvidersForConsentObtainedBy() {
        boolean cachedLocally = isCachedProvidersForConsentObtainedBy();

        if (cachedLocally) {
            List<UserDTO> users = ApplicationContextUtils.getProvidersForConsentObtainedBy();

            List<ListGridRecord> list = buildUsersMapByListGridRecord(users);
           userDataSource.setCacheData(list.toArray(new ListGridRecord[list.size()]));
           consentObtainedBySelectItem.setOptionDataSource(userDataSource);

            if (person != null) {
                // use value of the patient
                populateConsentObtainedBy(person.getPatientEnrollment());
            } else {
                // default to current user
                LoginResult loginResult = ApplicationContextUtils.getLoginResult();
                long theUserId = loginResult.getUserId();
                consentObtainedBySelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + theUserId);
            }

        } else {
            // get from the server
            careteamService.getAvailableUsers(new GetAvailableUsersCallback());
        }
    }

    private boolean isCachedProvidersForConsentObtainedBy() {
        ApplicationContext context = ApplicationContext.getInstance();

        boolean cached = context.containsKey(Constants.PROVIDERS_FOR_CONSENT_OBTAINED_BY);

        return cached;
    }
    
    private void loadConsenters() {
         //Window.alert("CareTeam/ViewPatientform loadConsenters");
        boolean cachedLocally = isCachedConsenters();

        if (cachedLocally) {
           //   Window.alert("CareTeam/ViewPatientform cachedlocally");
            LinkedHashMap<String,String> consenters = ApplicationContextUtils.getConsenters();

            List<ListGridRecord> list = buildConsentersMapByListGridRecord(consenters);
           consenterDataSource.setCacheData(list.toArray(new ListGridRecord[list.size()]));
           consenterSelectItem.setOptionDataSource(consenterDataSource);

            if (person != null) {
                // use value of the patient
                populateConsenter(person.getPatientEnrollment());
            } else {
                // default to current user - whats the default consenter??
                //LoginResult loginResult = ApplicationContextUtils.getLoginResult();
                //long theUserId = loginResult.getUserId();
                consenterSelectItem.setValue("");
            }

        } else {
            // get from the server
            // Window.alert("CareTeam/ViewPatientform NOT cachedlocally communityId: "+communityId);
            careteamService.getConsenterTable(communityId,new GetConsentersCallback(communityId));
        }
    }

     private boolean isCachedConsenters() {
        ApplicationContext context = ApplicationContext.getInstance();

        boolean cached = context.containsKey(Constants.CONSENTERS);

        return cached;
    }
    private void populateProvidersForConsentObtainedBy() {
        if (usersMap == null) {
            loadProvidersForConsentObtainedBy();
            loadConsenters();
        } else {
            consentObtainedBySelectItem.setValueMap(usersMap);
           // consenterSelectItem.setValueMap(usersMap);
        }
    }
 
    private void populate() {
       
        patientIDStaticTextItem.setValue(StringUtils.defaultString(String.valueOf(person.getPatientEnrollment().getPatientId())));
        firstNameStaticTextItem.setValue(person.getFirstName());
        middleNameStaticTextItem.setValue(person.getMiddleName());
        lastNameStaticTextItem.setValue(person.getLastName());

        //pe work
        patientUserNameStaticTextItem.setValue(person.getPatientEnrollment().getPatientLoginId());

        // ssn
        String ssn = person.getSsn();
        if (StringUtils.isNotBlank(ssn)) {
            String maskedSsn = SsnUtils.getMaskedSsn(ssn);
            ssnStaticTextItem.setValue(maskedSsn);
        }

        Date dateOfBirth = person.getDateOfBirth();
        if (dateOfBirth != null) {
            DateTimeFormat dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
            String data = dateFormatter.format(dateOfBirth);
            dobDateItem.setValue(data);
        }

        Map<String, String> genderCodes = ApplicationContextUtils.getGenderCodes();
        String genderDescription = genderCodes.get(person.getGender());
        genderStaticTextItem.setValue(genderDescription);

        // ethnicity
        Map<String, String> ethnicCodes = ApplicationContextUtils.getEthnicCodes();
        String ethnicDescription = ethnicCodes.get(person.getEthnic());
        ethnicityStaticTextItem.setValue(ethnicDescription);

        // Race
        Map<String, String> raceCodes = ApplicationContextUtils.getRaceCodes();
        String raceDescription = raceCodes.get(person.getRace());
        raceStaticTextItem.setValue(raceDescription);

        // language
        Map<String, String> languageCodes = ApplicationContextUtils.getLanguageCodes();
        String languageDescription = languageCodes.get(person.getLanguage());
        languageStaticTextItem.setValue(languageDescription);

        address1StaticTextItem.setValue(person.getStreetAddress1());
        address2StaticTextItem.setValue(person.getStreetAddress2());

        cityStaticTextItem.setValue(person.getCity());
        stateStaticTextItem.setValue(person.getState());
        zipCodeStaticTextItem.setValue(person.getZipCode());

        telephoneStaticTextItem.setValue(person.getTelephone());

        PatientEnrollmentDTO patientEnrollment = person.getPatientEnrollment();

        String status = patientEnrollment.getStatus();
        if (!StringUtils.isBlank(status)) {
            enrollmentStatusSelectItem.setValue(status);
        }

//        String programName = patientEnrollment.getProgramName();
//        if (!StringUtils.isBlank(programName)) {
//            programNameSelectItem.setValue(programName);
//        } else {
//            programNameSelectItem.setDefaultToFirstOption(true);
//        }
//
//        Date programNameEffectiveDate = patientEnrollment.getProgramNameEffectiveDate();
//        if (programNameEffectiveDate != null) {
//            programEffectiveDateItem.setValue(programNameEffectiveDate);
//        }
//
//        Date programNameEndDate = patientEnrollment.getProgramEndDate();
//        if (programNameEndDate != null) {
//            programEndDateItem.setValue(programNameEndDate);
//        }
        // email
        emailStaticTextItem.setValue(patientEnrollment.getEmailAddress());

        // organization
        populateOrganizationName(patientEnrollment.getOrgId());

        // mrn
        String patientMrn = patientEnrollment.getPatientMrn();
        if (patientMrn != null) {
            organizationMrnStaticTextItem.setValue(patientMrn);
        }

        // enrollment
        String programLevelConsentStatus = patientEnrollment.getProgramLevelConsentStatus();
        oldProgramLevelConsentStatus = programLevelConsentStatus;

        oldOrganizationId = patientEnrollment.getOrgId();
        String enrollmentStatus = patientEnrollment.getStatus();

        if (programLevelConsentStatus != null) {
            programLevelSelectItem.setValue(programLevelConsentStatus);

            if (StringUtils.equalsIgnoreCase(programLevelConsentStatus, "Yes")) {
                if (StringUtils.equalsIgnoreCase(enrollmentStatus, "Enrolled") || StringUtils.equalsIgnoreCase(enrollmentStatus, "Assigned")) {
                    programLevelSelectItem.setValueMap(CONSENT_STATUSES);
                }
            }
            if (programLevelConsentStatus.equals("Yes")) {
                enrollmentStatusSelectItem.setValueMap(PLCS_YES_ENROLLMENT_STATUSES);
                
                Date consentDate = patientEnrollment.getConsentDateTime();
                populateConsentDate(consentDate);

//                populateConsentTime(consentDate);
                consentDateTime = consentDate;
                         
                loadProvidersForConsentObtainedBy();
                loadConsenters();

                checkToShowReasonForInactivation();

            } else if (programLevelConsentStatus.equalsIgnoreCase("No")) {
                enrollmentStatusSelectItem.setValueMap(PLCS_NO_ENROLLMENT_STATUSES);
                enrollmentStatusSelectItem.setValue(PLCS_NO_ENROLLMENT_STATUSES[0]);
                enrollmentStatusSelectItem.setDisabled(false);
                Date consentDate = patientEnrollment.getConsentDateTime();
                populateConsentDate(consentDate);
                         
//                populateConsentTime(consentDate);
                consentDateTime = consentDate;               
                loadProvidersForConsentObtainedBy();
                loadConsenters();
            } else {
                enrollmentStatusSelectItem.setValueMap(PLCS_NO_ENROLLMENT_STATUSES);
                enrollmentStatusSelectItem.setValue("Pending");
                enrollmentStatusSelectItem.setDisabled(true);
            }
        } else {
            enrollmentStatusSelectItem.setValueMap(PLCS_NO_ENROLLMENT_STATUSES);
            enrollmentStatusSelectItem.setValue("Pending");
            enrollmentStatusSelectItem.setDisabled(true);
        }

       if(patientEnrollment.getAcuityScore()!=null){
        acuityScoreTextItem.setValue(patientEnrollment.getAcuityScore());
       }
        //set enrollment status change effective date
        Date enrollStatusChangeDate = person.getEnrollmentChangeDate();
        if (enrollStatusChangeDate != null) {
            populateEnrollStatusChangeDate(enrollStatusChangeDate);
        }
       if(childrensHealthHome && minorFlag ){
         String infoSharingStatus=patientEnrollment.getInformationSharingStatus();
        if(infoSharingStatus!=null && !StringUtils.isBlank(infoSharingStatus)){  
        
          informationSharingSelectItem.setValue(infoSharingStatus);
           Date minorConsentDate = patientEnrollment.getMinorConsentDateTime();
                 populateMinorConsentDate(minorConsentDate);
//                 populateMinorConsentTime(minorConsentDate);
                 loadMinorConsentObtainedBy();
                
        }
       }
//        // set health home label
//        ProgramHealthHomeDTO programHealthHome = patientEnrollment.getProgramHealthHome();
//        if (programHealthHome != null) {
//
//            Long healthHomeNameId = programHealthHome.getId();
//            if ((healthHomeNameId != null) && (healthHomeNameId.longValue() > 0)) {
//                healthHomeLabelSelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + healthHomeNameId);
//            }
//        }
        // pe PatientUserActive from data base 
        //System.out.println("PatientUserActive for DB for care Team:" + patientEnrollment.getPatientUserActive());
         if ((childrensHealthHome && !minorFlag) || (childrensHealthHome && minorFlag && !disableMinorDirectMessg) || !childrensHealthHome) {
            String patientMsg = patientEnrollment.getPatientUserActive();
            if (patientMsg != null) {
                if (patientMsg.equalsIgnoreCase(Constants.PATINET_ACTIVE)) {
                    addPatientSelectItem.setValue(true);
                    uncheckedFlag = true;
                } else if (patientMsg.equalsIgnoreCase(Constants.PATINET_INACTIVE) || patientMsg.equalsIgnoreCase(Constants.PATINET_NONE)) {
                    addPatientSelectItem.setValue(false);
                }
            }
        }          
          else if(childrensHealthHome && minorFlag && disableMinorDirectMessg){
            addPatientSelectItem.setVisible(false);
        }
                 
        enableMinorFields(person.getDateOfBirth());
        selectCareteam();

        checkToShowCareteamFields();
        // checkToShowReasonForRefusal();

        checkToDisableConsentFields(programLevelConsentStatus);

        // Insurance
        populateInsuranceSection(patientEnrollment);

        // Emergency contacts
        populateEmergencyContactSection(patientEnrollment);

    }

    private void populateInsuranceSection(PatientEnrollmentDTO patientEnrollment) {

        //  Primary payer class
        PayerClassDTO primaryPayerClass = patientEnrollment.getPrimaryPayerClass();
        if (primaryPayerClass != null) {
            String primaryPayerClassName = primaryPayerClass.getName();
            if (!StringUtils.isBlank(primaryPayerClassName)) {
                primaryPayerClassStaticTextItem.setValue(primaryPayerClassName);
            }
        }

        // secondary payer class
        PayerClassDTO secondaryPayerClass = patientEnrollment.getSecondaryPayerClass();
        if (secondaryPayerClass != null) {
            String secondaryPayerClassName = secondaryPayerClass.getName();
            if (!StringUtils.isBlank(secondaryPayerClassName)) {
                secondaryPayerClassStaticTextItem.setValue(secondaryPayerClassName);
            }
        }

        // primary payer plan
        PayerPlanDTO primaryPayerPlan = patientEnrollment.getPrimaryPayerPlan();
        if (primaryPayerPlan != null) {
            String primaryPayerPlanName = primaryPayerPlan.getName();
            if (!StringUtils.isBlank(primaryPayerPlanName)) {
                primaryPayerPlanStaticTextItem.setValue(primaryPayerPlanName);
            }
        }

        // secondary payer plan
        PayerPlanDTO secondaryPayerPlan = patientEnrollment.getSecondaryPayerPlan();
        if (secondaryPayerPlan != null) {
            String secondaryPayerPlanName = secondaryPayerPlan.getName();
            if (!StringUtils.isBlank(secondaryPayerPlanName)) {
                secondaryPayerPlanStaticTextItem.setValue(secondaryPayerPlanName);
            }
        }

        // primary payer medicaid medicare id
        String primaryPayerMedicaidMedicareId = patientEnrollment.getPrimaryPayerMedicaidMedicareId();
        if (StringUtils.isNotBlank(primaryPayerMedicaidMedicareId)) {
            primaryMedicaidIdStaticTextItem.setValue(primaryPayerMedicaidMedicareId);
        }

        // secondary payer medicaid medicare id
        String secondaryPayerMedicaidMedicareId = patientEnrollment.getSecondaryPayerMedicaidMedicareId();
        if (StringUtils.isNotBlank(secondaryPayerMedicaidMedicareId)) {
            secondaryMedicaidIdStaticTextItem.setValue(secondaryPayerMedicaidMedicareId);
        }

        // primary effective date
        Date primaryEffectiveDate = patientEnrollment.getPrimaryPlanEffectiveDate();
        // date formatter
        DateTimeFormat dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
        if (primaryEffectiveDate != null) {
            String primaryDateStr = dateFormatter.format(primaryEffectiveDate);
            primaryEffectiveStaticTextItem.setValue(primaryDateStr);
        }

        // secondary effective date
        Date secondaryEffectiveDate = patientEnrollment.getSecondaryPlanEffectiveDate();
        if (secondaryEffectiveDate != null) {
            String secondaryDateStr = dateFormatter.format(secondaryEffectiveDate);
            secondaryEffectiveStaticTextItem.setValue(secondaryDateStr);
        }

    }

    private void populateEmergencyContactSection(PatientEnrollmentDTO patientEnrollment) {

        PatientEmergencyContactDTO emergencyContact = patientEnrollment.getPatientEmergencyContact();

        if (emergencyContact != null) {
            fNameEmergencyStaticTextItem.setValue(emergencyContact.getFirstName());
            lNameEmergencyStaticTextItem.setValue(emergencyContact.getLastName());
            mNameEmergencyStaticTextItem.setValue(emergencyContact.getMiddleName());
            emergencyEmailStaticTextItem.setValue(emergencyContact.getEmailAddress());
            telephoneEmergencyStaticTextItem.setValue(emergencyContact.getTelephone());
            relationshipTextItem.setValue(emergencyContact.getPatientRelationshipDescription());
        }
    }

    private void populateConsentDate(Date consentDate) {
        consentDateItem.setValue(consentDate);
    }
    
//     private void populateConsentDate2(Date consentDate2) {
//         consentDateItem.setValue(consentDate2);
//    }

    private void populateConsentObtainedByUser(long User) {
        consentObtainedBySelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + User);
    }
    
     private void populateConsenter(PatientEnrollmentDTO patientEnrollment) {

        long consenterCode = patientEnrollment.getConsenter();
       // Window.alert("CareTeam/ViewPatientForm:" + consenterCode);
        consenterSelectItem.setValue((consenterCode == 0) ? "" : Constants.DELIMITER_SELECT_ITEM + consenterCode);
       
    }

//    private void populateConsentTime(Date consentDate) {
//        if (consentDate != null) {
//            DateTimeFormat dateFormatter = DateTimeFormat.getFormat("hh:mm");
//            String data = dateFormatter.format(consentDate);
//            consentTimeTextItem.setValue(data);
//
//            DateTimeFormat ampmFormatter = DateTimeFormat.getFormat("a");
//            String ampm = ampmFormatter.format(consentDate);
//
//            consentAMPM.setValue(ampm);
//        }
//    }
    
//     private void populateConsentTime2(Date consentDate2) {
//
//          if (consentDate2 != null) {
//            DateTimeFormat dateFormatter = DateTimeFormat.getFormat("hh:mm");
//            String data2 = dateFormatter.format(consentDate2);
//            consentTimeTextItem.setValue(data2);
//
//            DateTimeFormat ampmFormatter = DateTimeFormat.getFormat("a");
//            String ampm2 = ampmFormatter.format(consentDate2);
//
//            consentAMPM.setValue(ampm2);
//    }
//     }
     
     private void populateMinorConsentDate(Date consentDate) {
        dateItem.setValue(consentDate);
    }
//     private void populateMinorConsentTime(Date consentDate) {
//        if (consentDate != null) {
//            DateTimeFormat dateFormatter = DateTimeFormat.getFormat("hh:mm");
//            String data = dateFormatter.format(consentDate);
//            timeTextItem.setValue(data);
//
//            DateTimeFormat ampmFormatter = DateTimeFormat.getFormat("a");
//            String ampm = ampmFormatter.format(consentDate);
//
//            consentAMPMItem.setValue(ampm);
//        }
//    }

    private DynamicForm buildCareTeamForm() {
        DynamicForm theForm = new DynamicForm();

        theForm.setWrapItemTitles(true);
        theForm.setGroupTitle("Care Team");
        theForm.setIsGroup(true);
        theForm.setNumCols(8);
        setFormWidth(theForm);
        theForm.setRequiredTitlePrefix(FormUtils.REQUIRED_TITLE_PREFIX);
        theForm.setTitleWidth(160);

        careTeamSelectItem = new SelectItem("careteam", FormUtils.REQUIRED_TITLE_PREFIX + "Care Team");
        careTeamSelectItem.setRequired(false);
        careTeamSelectItem.setColSpan(4);
        careTeamSelectItem.setTabIndex(1);
        careTeamSelectItem.setWidth(130);

        SpacerItem spacerItem = new SpacerItem();
        spacerItem.setWidth(75);

        // Manage Consent for Non Health Home Providers
        manageNonHealthHomeLinkItem = new LinkItem();
        manageNonHealthHomeLinkItem.setValue(ADD_ASSIGN_OUT_OF_NETWORK_PROVIDERS);
        manageNonHealthHomeLinkItem.setVisible(true);
        manageNonHealthHomeLinkItem.setShowTitle(false);
        manageNonHealthHomeLinkItem.setColSpan(2);
        manageNonHealthHomeLinkItem.setWidth(260);
        manageNonHealthHomeLinkItem.setTabIndex(2);
        manageNonHealthHomeLinkItem.addClickHandler(new ManageNonHealthHomeClickHandler());
        manageNonHealthHomeLinkItem.setEndRow(true);

        // Add patient to care team 
        SpacerItem spacerItem1 = new SpacerItem();
        addPatientSelectItem = new CheckboxItem("addPatientToCareTeam", patientToCareTeamLabel);
        addPatientSelectItem.setWrapTitle(false);
        addPatientSelectItem.setWidth(130);
        addPatientSelectItem.setEndRow(true);
        addPatientSelectItem.setTabIndex(3);
        addPatientSelectItem.setColSpan(2);
        addPatientSelectItem.setVisible(patientEngagementEnable );
        addPatientSelectItem.addChangedHandler(new AddPatientToCareTeamChangeHandler());
        spacerItem1.setColSpan(2);
//        SpacerItem spacerItem2 = new SpacerItem();
//        spacerItem2.setWidth(10);

        theForm.setFields(careTeamSelectItem, spacerItem, manageNonHealthHomeLinkItem, spacerItem1, addPatientSelectItem);

        return theForm;
    }

    class AddPatientToCareTeamChangeHandler implements ChangedHandler {

        public void onChanged(ChangedEvent event) {

            String careTeamValue = careTeamSelectItem.getValueAsString();
            if (careTeamValue != null) {

                if (addPatientSelectItem.getValueAsBoolean()) {
                    String addPatientToCareTeamMsg = ApplicationContextUtils.getClientApplicationProperties().getAddPatientToCareTeamMsg();
                    SC.confirm(Constants.ADD_PATIENT_TOCARE_TEAM_CONF, addPatientToCareTeamMsg, new PatientToCareTeamConfirmationCallback());
                } else if (!addPatientSelectItem.getValueAsBoolean() && uncheckedFlag) {

                    String removePatientFromCareTeamMsg = ApplicationContextUtils.getClientApplicationProperties().getRemovePatientFromCareTeamMsg();
                    SC.confirm(Constants.REMOVE_PATIENT_FROM_CARE_TEAM_CONF, removePatientFromCareTeamMsg, new RemovePatientFromCareTeamConfirmationCallback());

                } else {
                    addPatientSelectItem.setValue(false);
                }

            } else {
                SC.warn("Please select a Care Team.");
                addPatientSelectItem.setValue(false);
            }
        }
    }

    class RemovePatientFromCareTeamConfirmationCallback implements BooleanCallback {

        @Override
        public void execute(Boolean value) {
            addPatientSelectItem.setValue(true);

            if (value.booleanValue()) {

                addPatientSelectItem.setValue(false);

            } else if (!value.booleanValue()) {
                addPatientSelectItem.setValue(true);

            }
        }
    }

    class PatientToCareTeamConfirmationCallback implements BooleanCallback {

        @Override
        public void execute(Boolean value) {
            addPatientSelectItem.setValue(false);

            if (value.booleanValue()) {

                if (person.getPatientEnrollment().getEmailAddress() != null) {

                    addPatientSelectItem.setValue(true);
                } else {
                    SC.warn("Patient doesn't have a valid Email Address.");
                    addPatientSelectItem.setValue(false);
                }
            } else if (!value.booleanValue()) {
                addPatientSelectItem.setValue(false);

            }
        }
    }

    public void selectCareteam() {
        PatientEnrollmentDTO patientEnrollment = person.getPatientEnrollment();
        long patientId = patientEnrollment.getPatientId();
        if (person.getPatientEnrollment().getStatus().equalsIgnoreCase("Assigned")) {
            careteamService.getCareteamForPatient(patientId, new GetCareteamForPatientCallback(patientId));
        }
        else{
             careTeamSelectItem.setValue("");
        }
    }

    public long getPatientOrganizationId() {
        return person.getPatientEnrollment().getOrgId();
    }

    public PersonDTO getPerson() {
        return person;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }

    /**
     * @return
     */
    public PatientInfo getPatientInfo() {
        if(patientInfo == null) {
            patientInfo = populatePatientInfo();
        }
        return patientInfo;
    }

    private PatientInfo populatePatientInfo() {
        patientInfo = new PatientInfo();

        patientInfo.setEnableCareteamPanel(enableCareteamPanel);
        patientInfo.setOldProgramLevelConsentStatus(oldProgramLevelConsentStatus);
        patientInfo.setOldOrganizationId(oldOrganizationId);

        boolean validEnrollment = enrollmentLevelForm.validate();

        if (validEnrollment) {
          

            PatientEnrollmentDTO patientEnrollment = person.getPatientEnrollment();
            updatePatientInfoCareTeamDetails(patientEnrollment);
            String programLevelConsentStatus = programLevelSelectItem.getValueAsString();
            patientEnrollment.setProgramLevelConsentStatus(programLevelConsentStatus);
            String theNewEnrollmentStatus = enrollmentStatusSelectItem.getValueAsString();

            if (StringUtils.equals(theNewEnrollmentStatus, "Inactive")) {
                patientEnrollment.setReasonForInactivation(reasonforInactivationSelectItem.getValueAsString());
            } else {
                patientEnrollment.setReasonForInactivation(null);
            }

            // check enrollment status change
            String oldEnrollmentStatus = patientEnrollment.getStatus();
            boolean enrollmentStatusChanged = !StringUtils.equals(oldEnrollmentStatus, theNewEnrollmentStatus);
            patientInfo.setEnrollmentStatusChanged(enrollmentStatusChanged);
            patientInfo.setOldEnrollmentStatus(oldEnrollmentStatus);

            patientEnrollment.setStatus(theNewEnrollmentStatus);

            patientEnrollment.setConsentDateTime(consentDateItem.getValueAsDate());

            // consent obtained by
            String consentObtainedByStr = consentObtainedBySelectItem.getValueAsString();
            if (StringUtils.isNotBlank(consentObtainedByStr)) {
                //
                // work around for sorting by keys. prepend numeric key with a string
                // to preserved sorted order
                // http://code.google.com/p/smartgwt/issues/detail?id=569
                //
                // retrieve user id
                String userIdStr = consentObtainedByStr.split(Constants.DELIMITER_SELECT_ITEM)[1];
                patientEnrollment.setConsentObtainedByUserId(Long.parseLong(userIdStr));
            } else {
                patientEnrollment.setConsentObtainedByUserId(Constants.EMPTY_USER);
            }
             // consenter
            String consenterStr = consenterSelectItem.getValueAsString();
            if (StringUtils.isNotBlank(consenterStr)) {               
                String consenterCode = consenterStr.split(Constants.DELIMITER_SELECT_ITEM)[1];
                patientEnrollment.setConsenter(Long.parseLong(consenterCode));
            } else {
                patientEnrollment.setConsenter(0L);
            }
            
            patientEnrollment.setAcuityScore(acuityScoreTextItem.getValueAsString());

//            patientInfo.setConsentTime(consentTimeTextItem.getValueAsString());
//            patientInfo.setConsentAMPM(consentAMPM.getValueAsString());


// minor patient consent work (CHH)           
            if (childrensHealthHome && minorFlag) {
                String informationSharingStatus = informationSharingSelectItem.getValueAsString();
                if (informationSharingStatus.equalsIgnoreCase("select") || informationSharingStatus.isEmpty()) {

                    patientEnrollment.setInformationSharingStatus(null);
                } else {
                    patientEnrollment.setInformationSharingStatus(informationSharingStatus);
                }
                patientEnrollment.setMinorConsentDateTime(dateItem.getValueAsDate());
//                patientInfo.setMinorConsentTime(timeTextItem.getValueAsString());
//                patientInfo.setMinorConsentAMPM(consentAMPMItem.getValueAsString());

                String minorConsentObtainedByStr = obtainedBySelectItem.getValueAsString();
                if (StringUtils.isNotBlank(minorConsentObtainedByStr)) {
                    // retrieve user id
                    String userIdStrng = minorConsentObtainedByStr.split(Constants.DELIMITER_SELECT_ITEM)[1];
                    patientEnrollment.setMinorConsentObtainedByUserId(Long.parseLong(userIdStrng));
                } else {
                patientEnrollment.setMinorConsentObtainedByUserId(Constants.EMPTY_USER);
            }
            }
            //pe work for save care team
            String existPatient = patientEnrollment.getPatientUserActive();
            patientInfo.setOriginalPatientUserActive(existPatient);
            System.out.println("existPatient:" + existPatient);

            boolean patientActive = addPatientSelectItem.getValueAsBoolean();
            System.out.println("patientActive:" + patientActive);

            if (patientActive) {
                patientInfo.setPatientUserActive(Constants.PATINET_ACTIVE);
            } else {
                if (existPatient != null && existPatient.equalsIgnoreCase(Constants.PATINET_ACTIVE)) {
                    patientInfo.setPatientUserActive(Constants.PATINET_INACTIVE);
                } else if (existPatient != null && existPatient.equalsIgnoreCase(Constants.PATINET_INACTIVE)) {
                    patientInfo.setPatientUserActive(Constants.PATINET_INACTIVE);
                } else {
                    patientInfo.setPatientUserActive(Constants.PATINET_NONE);
                }
            }

            UserDTO userDTO = new UserDTO();

            userDTO.setEuid(person.getEuid());

            userDTO.setUserId(person.getPatientEnrollment().getPatientId());

            userDTO.setFirstName(person.getFirstName());
            userDTO.setLastName(person.getLastName());
            userDTO.setMiddleName(person.getMiddleName());

            Date dateOfBirth = person.getDateOfBirth();
            userDTO.setDateOfBirth(dateOfBirth);

            userDTO.setEmail(person.getPatientEnrollment().getEmailAddress());
            Map<String, String> genderCodes = ApplicationContextUtils.getGenderCodes();
            String genderDescription = genderCodes.get(person.getGender());
            userDTO.setGender(genderDescription);

            userDTO.setStreetAddress1(person.getStreetAddress1());
            userDTO.setStreetAddress2(person.getStreetAddress2());
            userDTO.setCity(person.getCity());
            userDTO.setState(person.getState());
            userDTO.setZipCode(person.getZipCode());

            userDTO.setWorkPhone(person.getTelephone());
            userDTO.setWorkPhoneExt(person.getTelephoneExtension());
            userDTO.setIsDeleted('N');
            userDTO.setFailedAccessAttempts(3);
            userDTO.setCreationDate(new Date());

            userDTO.setEulaAccepted('N');
            userDTO.setEulaDate(new Date());
            userDTO.setEulaId(200);

            userDTO.setCredentials("");
            userDTO.setPrefix("MR");
            userDTO.setNpi("NO");
            userDTO.setEnabled('Y');

            patientInfo.setUserDTO(userDTO);
            //end Patient user account PE work

            patientInfo.setPatientId(patientEnrollment.getPatientId());

            patientInfo.setPatientEnrollment(patientEnrollment);
        } else {
            patientInfo = null;
        }

        return patientInfo;
    }

    public void updatePatientInfoCareTeamDetails( PatientEnrollmentDTO patientEnrollment) throws NumberFormatException {
       
        String oldEnrollStatus= patientEnrollment.getStatus();
        String newEnrollStatus= enrollmentStatusSelectItem.getValueAsString();
      
        
        if(oldEnrollStatus.equalsIgnoreCase("Assigned") && !newEnrollStatus.equalsIgnoreCase("Assigned") ){
            patientInfo.setCareTeamEnd(true);
            
        }
//        else if ((oldEnrollStatus.equalsIgnoreCase("Enrolled") && newEnrollStatus.equalsIgnoreCase("Assigned"))) {
//             patientInfo.setCareTeamEnd(true);
//            
//        } 
        checkAndSetCareTeamChanged();
    }
    
    public void checkAndSetCareTeamChanged(){
        boolean careTeamChanged=setCareTeamInfoToPatientInfo();
        patientInfo.setCareteamChanged(careTeamChanged);
    }
    
    private boolean setCareTeamInfoToPatientInfo(){
        
        String careTeamIdStr = careTeamSelectItem.getValueAsString();           
            System.out.println("enrollment is valid what about care team string? right now its: " + careTeamIdStr);
       boolean careTeamChanged=false;
            if (StringUtils.isNotBlank(careTeamIdStr)) {
            // convert from delimited format
            careTeamIdStr = careTeamIdStr.split(Constants.DELIMITER_SELECT_ITEM)[1];
            System.out.println("careteamIdStr is there set it to: " + careTeamIdStr);
            long theNewcommunityCareteamId = Long.parseLong(careTeamIdStr);
            patientInfo.setCommunityCareteamId(theNewcommunityCareteamId);
            
            LinkedHashMap<String, String> careteamNames = ApplicationContextUtils.getCareteamNames();
            String careteamName = careteamNames.get(Long.toString(theNewcommunityCareteamId));
            patientInfo.setCareteamName(careteamName);
           
            // check if care team has changed
            careTeamChanged = communityCareteamId != theNewcommunityCareteamId;
            System.out.println("did the care team name change?: " + careTeamChanged);
         }  
            return careTeamChanged;
    }

    public SelectItem getEnrollmentStatusSelectItem() {
        return enrollmentStatusSelectItem;
    }

    private void checkToShowReasonForInactivation() {
        String enrollmentStatus = enrollmentStatusSelectItem.getValueAsString();
        boolean inActiveEnrollmentStatus = StringUtils.equals(enrollmentStatus, "Inactive");
        if (inActiveEnrollmentStatus) {
            reasonforInactivationSelectItem.show();
        } else {
            reasonforInactivationSelectItem.hide();
        }
        reasonforInactivationSelectItem.setRequired(inActiveEnrollmentStatus);
    }

    class GetCareteamForPatientCallback extends RetryActionCallback<CommunityCareteamDTO> {

        long patientId;

        GetCareteamForPatientCallback(long thePatientId) {
            patientId = thePatientId;
        }

        @Override
        public void attempt() {
            careteamService.getCareteamForPatient(patientId, this);
        }

        @Override
        public void onCapture(CommunityCareteamDTO communityCareteam) {
            if (communityCareteam != null) {
                communityCareteamId = communityCareteam.getCommunityCareteamId();
                careTeamSelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + communityCareteamId);
                
            }
        }
    }

    public SelectItem getCareTeamSelectItem() {
        return careTeamSelectItem;
    }

    private void populateOrganizationName(long orgId) {
        orgService.getOrganizationName(orgId, new GetOrganizationNameCallback(organizationStaticTextItem));
    }

    private void checkToShowCareteamFields() {
        String programConsentLevelStatus = programLevelSelectItem.getValueAsString();
        String enrollmentStatus = enrollmentStatusSelectItem.getValueAsString();

        enableCareteamPanel = false;
        boolean programConsentLevelStatusYes = StringUtils.equals(programConsentLevelStatus, "Yes");
        boolean validEnrollmentStatus = StringUtils.equals(enrollmentStatus, "Assigned");

        boolean show = programConsentLevelStatusYes && validEnrollmentStatus;

        viewPatientPanel.showCareteamButtons(show);

        if (show) {
            enableCareteamPanel = true;
            careTeamForm.show();
        } else {
            careTeamForm.hide();
        }
    }

    private String[] getProgramNameCodes() {
        String[] baseCodes = ApplicationContextUtils.getProgramNameCodes();

        String[] updatedCodes = FormUtils.getCodesWithAddedBlankOption(baseCodes);

        return updatedCodes;
    }

    private String[] getProgramLevelConsentStatusCodes() {
        String[] baseCodes = ApplicationContextUtils.getProgramLevelConsentStatusCodes();

        String[] updatedCodes = FormUtils.getCodesWithAddedBlankOption(baseCodes);

        return updatedCodes;
    }

    private void checkToDisableConsentFields(String value) {
        boolean disableConsentFields = !(StringUtils.equalsIgnoreCase(value, "Yes"));

        checkToDisableConsentFields(disableConsentFields,value);
    }

    /**
     * TODO for the love of all that's holy!!!!! why all the duplication between here and enrollment???
     * @param disableConsentFields
     * @param plcs 
     */
    private void checkToDisableConsentFields(boolean disableConsentFields, String plcs) {
        consentDateItem.setDisabled(disableConsentFields);
        consentObtainedBySelectItem.setDisabled(disableConsentFields);
        consenterSelectItem.setDisabled(disableConsentFields);

        consentDateItem.setRequired(!disableConsentFields);
        consentObtainedBySelectItem.setRequired(!disableConsentFields);
        consenterSelectItem.setRequired(!disableConsentFields);

        if (disableConsentFields) {
            manageNonHealthHomeLinkItem.setValue("");
            if(StringUtils.isBlank(plcs)){
                clearConsentDateTimeFields();
            }

        } else {
            manageNonHealthHomeLinkItem.setValue(ADD_ASSIGN_OUT_OF_NETWORK_PROVIDERS); // TODO what does this have to do with consent fields?
        }

        System.out.println("showManageHealthHomeLink" + showManageHealthHomeLink);
        manageHealthHomeButtonItem.setVisible(showManageHealthHomeLink || plcs.equalsIgnoreCase("Yes"));
    }

    private void clearConsentDateTimeFields() {
        consentDateItem.clearValue();
        consentObtainedBySelectItem.clearValue();
        consenterSelectItem.clearValue();

        enrollmentLevelForm.clearFieldErrors(CONSENT_DATE_ITEM_NAME, true);
        enrollmentLevelForm.clearFieldErrors(CONSENT_VERSION_NAME, true);
        enrollmentLevelForm.clearFieldErrors(CONSENT_TIME_ITEM_NAME, true);
        enrollmentLevelForm.clearFieldErrors(CONSENT_AMPM_NAME, true);
        enrollmentLevelForm.clearFieldErrors(CONSENT_OBTAINED_BY, true);
        enrollmentLevelForm.clearFieldErrors(CONSENTER, true);
    }

    private long getLoggedOnUser() {
        return ApplicationContextUtils.getLoginResult().getUserId();
    }

    class ProgramLevelConsentStatusChangedHandler implements ChangedHandler {

//        @Override
        public void onChanged(ChangedEvent event) {
            String value = event.getValue().toString();
//            User = getLoggedOnUser();
            if (StringUtils.equalsIgnoreCase(value, "Yes")) {
                ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
                String consentLanguage = props.getConsentLanguage();
                if (consentNone) {
                    populateConsentDate(consentDateTime);
//                    populateConsentTime(consentDateTime);
                }
                SC.ask("Consent Confirmation", consentLanguage, new ConsentConfirmationCallback());
            } else if (StringUtils.equalsIgnoreCase(value, "No")) {

                enrollmentStatusSelectItem.setValueMap(PLCS_NO_ENROLLMENT_STATUSES);
                enrollmentStatusSelectItem.setValue(PLCS_NO_ENROLLMENT_STATUSES[0]);
                enrollmentStatusSelectItem.setDisabled(false);

                // clear the selected care team
                careTeamSelectItem.setValue("");
               
                checkToDisableConsentFields(value);
                consenterSelectItem.setRequired(false);
                if (consentNone) {
                    populateConsentDate(consentDateTime);
//                    populateConsentTime(consentDateTime);
//                    populateConsentObtainedByUser(consentingUser);
                     populateProvidersForConsentObtainedBy(); 
                }
                else{  
                   populateProvidersForConsentObtainedBy(); 
//                    populateConsentObtainedByUser(consentingUser);
                }
 
            } else {
                enrollmentStatusSelectItem.setValueMap(PLCS_NO_ENROLLMENT_STATUSES);
                enrollmentStatusSelectItem.setValue("Pending");
                enrollmentStatusSelectItem.setDisabled(true);
//                populateConsentObtainedByUser(consentingUser);
                populateProvidersForConsentObtainedBy();
                consentNone = true;
                // clear the selected care team
                careTeamSelectItem.setValue("");
//                populateConsentObtainedByUser(consentingUser);
                populateProvidersForConsentObtainedBy();
                checkToDisableConsentFields(value);
            }

            checkToShowCareteamFields();
            checkToShowReasonForInactivation();
        }
    }

    class ConsentConfirmationCallback implements BooleanCallback {

        @Override
        public void execute(Boolean value) {
            // settings when dialog is closed with cross button.
            programLevelSelectItem.setValue("No");
            enrollmentStatusSelectItem.setValueMap(PLCS_NO_ENROLLMENT_STATUSES);
            enrollmentStatusSelectItem.setValue(PLCS_NO_ENROLLMENT_STATUSES[0]);
            enrollmentStatusSelectItem.setDisabled(false);
            consentObtainedBySelectItem.setDisabled(false);
            consenterSelectItem.setDisabled(false);
            
//            consentAMPM.setDisabled(false);
            consentDateItem.setDisabled(false);
//            consentTimeTextItem.setDisabled(false);
            consentObtainedBySelectItem.setDisabled(false);
             consenterSelectItem.setDisabled(false);
//            populateConsentObtainedByUser(consentingUser);
          populateProvidersForConsentObtainedBy();

            // settings when user selects Yes or No button
            if (value.booleanValue()) {
                programLevelSelectItem.setValue("Yes");
                enrollmentStatusSelectItem.setValueMap(PLCS_YES_ENROLLMENT_STATUSES);
                enrollmentStatusSelectItem.setValue(PLCS_YES_ENROLLMENT_STATUSES[0]);
                enrollmentStatusSelectItem.setDisabled(false);

            } else if (!value.booleanValue()) {
                programLevelSelectItem.setValue("No");
                enrollmentStatusSelectItem.setValueMap(PLCS_NO_ENROLLMENT_STATUSES);
                enrollmentStatusSelectItem.setValue(PLCS_NO_ENROLLMENT_STATUSES[0]);
                enrollmentStatusSelectItem.setDisabled(false);
                // clear the selected care team
                careTeamSelectItem.setValue("");
            }

//            populateConsentObtainedByUser(consentingUser);
            populateProvidersForConsentObtainedBy();

            checkToDisableConsentFields(programLevelSelectItem.getValueAsString());

            checkToShowCareteamFields();
            checkToShowReasonForInactivation();
        }
    }
    
     class InformationStatusChangedHandler implements ChangedHandler {

        public void onChanged(ChangedEvent event) {
            String value = event.getValue().toString();
          
            if (StringUtils.equals(value, "Yes") || StringUtils.equals(value, "N/A")) {
               populateMinorConsent();
               dateItem.setValue(consentDate2);
//               populateMinorConsentTime(consentDate2);
            }
        }
    }
    
    class EnrollmentStatusChangedHandler implements ChangedHandler {

        public void onChanged(ChangedEvent event) {
            consentingUser = getLoggedOnUser();
            String enrollmentStatus = enrollmentStatusSelectItem.getValueAsString();

            boolean notAssignedStatus = !StringUtils.equals(enrollmentStatus, "Assigned");

            if (notAssignedStatus) {
                // clear the selected care team
                careTeamSelectItem.setValue("");
            }

            if (StringUtils.equals(enrollmentStatus, "Inactive")) {
                programLevelSelectItem.setValue("No");
                enrollmentStatusSelectItem.setValueMap(PLCS_NO_ENROLLMENT_STATUSES);
                checkToDisableConsentFields(programLevelSelectItem.getValueAsString());
            }

           
//            populateConsentObtainedByUser(consentingUser);
            populateProvidersForConsentObtainedBy();
            checkToShowCareteamFields();
            checkToShowReasonForInactivation();
            if(childrensHealthHome && minorFlag && disableMinorDirectMessg ){
                addPatientSelectItem.setVisible(false);
            }
            //checkToShowReasonForRefusal();
        }
    }

    class GetAvailableUsersCallback extends RetryActionCallback<List<UserDTO>> {

        @Override
        public void attempt() {
            careteamService.getAvailableUsers(this);
        }

        @Override
        public void onCapture(List<UserDTO> users) {
            if (!users.isEmpty()) {
                List<ListGridRecord> list = buildUsersMapByListGridRecord(users);
                userDataSource.setCacheData(list.toArray(new ListGridRecord[list.size()]));
                consentObtainedBySelectItem.setOptionDataSource(userDataSource);

                populateConsentObtainedBy(person.getPatientEnrollment());

                // update the cache
                ApplicationContext context = ApplicationContext.getInstance();
                context.putAttribute(Constants.PROVIDERS_FOR_CONSENT_OBTAINED_BY, users);

            } else {
                SC.warn("Providers not found.");
            }
        }
    }
    
    class GetConsentersCallback extends RetryActionCallback<LinkedHashMap<String, String>> {

        long communityId;

        GetConsentersCallback(long theCommunityId ) {
            communityId = theCommunityId;
        }
        
        @Override
        public void attempt() {
            careteamService.getConsenterTable(communityId, this);
        }

        @Override
        public void onCapture(LinkedHashMap<String, String> consenters) {
            if (!consenters.isEmpty()) {
                List<ListGridRecord> list = buildConsentersMapByListGridRecord(consenters);
                consenterDataSource.setCacheData(list.toArray(new ListGridRecord[list.size()]));
                consenterSelectItem.setOptionDataSource(consenterDataSource);

                populateConsenter(person.getPatientEnrollment());

                // update the cache
                ApplicationContext context = ApplicationContext.getInstance();
                context.putAttribute(Constants.CONSENTERS, consenters);

            } else {
                SC.warn("Consenter Table  not found.");
            }
        }
    }

    protected LinkedHashMap<String, String> buildUsersMap(List<UserDTO> users) {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

        for (UserDTO tempUser : users) {
            StringBuilder label = new StringBuilder();
            String userId = tempUser.getUserId().toString();
            label.append(tempUser.getLastName());
            label.append(", ");
            label.append(tempUser.getFirstName());

            String organization = tempUser.getOrganization();

            if (StringUtils.isNotBlank(organization)) {
                label.append(" - ");
                label.append(organization);
            }

            data.put(Constants.DELIMITER_SELECT_ITEM + userId, label.toString());
        }

        return data;
    }
    
    private List<ListGridRecord> buildUsersMapByListGridRecord(List<UserDTO> users) {

        List<ListGridRecord> reclist = new ArrayList<ListGridRecord>(users.size());

        for (UserDTO tempUser : users) {
            StringBuilder label = new StringBuilder();

            String status = tempUser.getStatus();
            boolean active = StringUtils.equalsIgnoreCase(status, "ACTIVE");
            String userId = tempUser.getUserId().toString();
            String key = Constants.DELIMITER_SELECT_ITEM + userId;
            label.append(tempUser.getLastName());
            label.append(", ");
            label.append(tempUser.getFirstName());

            String organization = tempUser.getOrganization();

            if (StringUtils.isNotBlank(organization)) {
                label.append(" - ");
                label.append(organization);

            }
            ListGridRecord record = new ListGridRecord();
            record.setAttribute("value", key);
            record.setEnabled(active);
            if(active)
            record.setAttribute("display", label.toString());
            else{
                record.setAttribute("display", label.toString());  
                record.setCustomStyle("background-color-white");
            }
            reclist.add(record);

        }
        return reclist;
    }
     private List<ListGridRecord> buildConsentersMapByListGridRecord(List<ConsenterDTO> consenters) {

        List<ListGridRecord> reclist = new ArrayList<ListGridRecord>(consenters.size());

        for (ConsenterDTO tempConsenter : consenters) {
            StringBuilder label = new StringBuilder();

            long code = tempConsenter.getCode();
            String description = tempConsenter.getCodeDescription();
            String consenterCode = String.valueOf(code);
            String key = Constants.DELIMITER_SELECT_ITEM + consenterCode;
            label.append(description);
            boolean active =true;

            
            ListGridRecord record = new ListGridRecord();
            record.setAttribute("value", key);
            record.setEnabled(active);
            if(active)
            record.setAttribute("display", label.toString());
            else{
                record.setAttribute("display", label.toString());  
                record.setCustomStyle("background-color-white");
            }
            reclist.add(record);

        }
        return reclist;
    }

     private List<ListGridRecord> buildConsentersMapByListGridRecord(LinkedHashMap<String,String> consenters) {

        //Window.alert("Viewform buildConsenters hashmap: "+consenters.size());
        List<ListGridRecord> reclist = new ArrayList<ListGridRecord>(consenters.size());
    
         for (String consenterCode : consenters.keySet()) {
             StringBuilder label = new StringBuilder();

             // long code = tempConsenter.getCode();
             String codeDescription = consenters.get(consenterCode);
             //  String consenterCode = String.valueOf(code);
             String key = Constants.DELIMITER_SELECT_ITEM + consenterCode;
             label.append(codeDescription);
             boolean active = true;
                   
            if (minorFlag && StringUtils.contains(consenterCode,"4")){
                //minor patient  consenters can't pick code 4
               // Window.alert("Minor deactivate: " + label.toString());
                active = false;
            }
             if (!minorFlag && !(StringUtils.contains(consenterCode,"3")||
                     StringUtils.contains(consenterCode,"4"))){
                //adult patient consenter can only pick 3 or 4
               // Window.alert("Minor deactivate: " + label.toString());
                active = false;
            }

             ListGridRecord record = new ListGridRecord();
             record.setAttribute("value", key);
             record.setEnabled(active);
             if (active) {
                 record.setAttribute("display", label.toString());
             } else {
                 record.setAttribute("display", label.toString());
                 record.setCustomStyle("background-color-white");
             }
             reclist.add(record);

        }
        return reclist;
    }
    class OtherPatientIdsClickHandler implements ClickHandler {

        public void onClick(ClickEvent ce) {
            long patientId = person.getPatientEnrollment().getPatientId();
            long communityId = person.getPatientEnrollment().getCommunityId();
            PatientOtherIdsWindow window = new PatientOtherIdsWindow(patientId, communityId);
            window.show();
        }
    }

    class ManageNonHealthHomeClickHandler implements ClickHandler {

        public void onClick(ClickEvent ce) {
            NonHealthHomeProviderWindow window = new NonHealthHomeProviderWindow(person);
            window.show();
        }
    }

    class ManageHealthHomeClickHandler implements ClickHandler {

        public void onClick(ClickEvent ce) {
            OrganizationPatientConsentWindow window = new OrganizationPatientConsentWindow(person);
            window.show();
        }
    }

    private String[] getDisenrollmentReasons() {
        return (String[]) ApplicationContext.getInstance().getAttribute(Constants.ALL_DISENROLLMENT_CODES);
    }

    protected void setFormValidationRulesFromClientAppProperties() {

        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
        healthHomeLabel = props.getHealthHomeLabel();
        programNameRequired = props.isProgramNameRequired();
        healthHomeRequired = props.isHealthHomeRequired();
        patientToCareTeamLabel = props.getPatientToCareTeamLabel();
        patientEngagementEnable = props.isPatientEngagementEnable();
        showManageHealthHomeLink = props.isManageConsent();
        childrensHealthHome=props.isChildrensHealthHome();
        disableMinorDirectMessg=props.isDisableMinorDirectMessaging();
        manageHealthHomeProviderConsent=props.getHealthHomeProviderButtonText();
        managePatientPrograms=props.getPatientProgramButtonText();
    }

    class HealthHomeProgramChangedHandler implements ChangedHandler {

        public void onChanged(ChangedEvent event) {
            String healthHomeWithDelimiter = (String) event.getValue();

            String healthHome = healthHomeWithDelimiter.split(Constants.DELIMITER_SELECT_ITEM)[1];

            LinkedHashMap<Long, ProgramNameDTO> programNameHealthHomeMap = ApplicationContextUtils.getProgramNameForHealthHomeValue();

            Collection<ProgramNameDTO> dto = programNameHealthHomeMap.values();
            ArrayList<ProgramNameDTO> programList = new ArrayList<ProgramNameDTO>(dto);
            programNameSelectItem.setValue(FormUtils.EMPTY);
            if (programNameBln) {
                String[] programName = FormUtils.convertProgramNameHealthHome(healthHome, programList);
                programNameCodes = FormUtils.getCodesWithAddedBlankOption(programName);

                programNameSelectItem.setValueMap(programNameCodes);
            } else {
                String[] programName = FormUtils.convertProgramNameHealthHome(healthHome, programList);
                programNameCodes = FormUtils.getCodesWithAddedBlankOption(programName);
                programNameSelectItem.setValueMap(programNameCodes);
            }
        }
    }

    private void populateEnrollStatusChangeDate(Date enrollStatusChangeDate) {
        DateTimeFormat enrollStatusChangeDateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
        String enrollStatusChangeDateStr = enrollStatusChangeDateFormatter.format(enrollStatusChangeDate);
        enrollmentChangeDateStaticTextItem.setValue(enrollStatusChangeDateStr);
    }

    class ManagePatientProgramClickHandler implements ClickHandler {

        public void onClick(ClickEvent ce) {
            person = getPerson();
            ManagePatientProgramWindow window = new ManagePatientProgramWindow(person);
            window.show();
        }
    }
    
     public void enableMinorFields(Date dob) {

        minorFlag = PropertyUtils.isMinor(dob);
       if(childrensHealthHome && minorFlag && canConsentEnrollMinors){
            informationSharingSelectItem.setDisabled(false);
            dateItem.setDisabled(false);
//            timeTextItem.setDisabled(false);
//            consentAMPMItem.setDisabled(false);
            obtainedBySelectItem.setDisabled(false);
            consenterSelectItem.setDisabled(false);

        }
      
    }

     private void loadMinorConsentObtainedBy() {
        boolean cachedLocally = isCachedProvidersForConsentObtainedBy();
        List<UserDTO> userDTOList = new ArrayList<UserDTO>();
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        boolean userCanConsentEnrollMinor = loginResult.isCanConsentEnrollMinors();
        
        if (cachedLocally) {
            List<UserDTO> users = ApplicationContextUtils.getProvidersForConsentObtainedBy();
            for (UserDTO user : users) {
                if (user.isCanConsentEnrollMinors()) {
                    userDTOList.add(user);
                }

            }
            LinkedHashMap<String, String> userMap = buildUsersMap(userDTOList);
            obtainedBySelectItem.setValueMap(userMap);

            if (person != null) {
                // use value of the patient
                populateMinorConsentObtainedBy(person.getPatientEnrollment(),loginResult);
            } else if(userCanConsentEnrollMinor){
                // default to current user who can consent and enroll 
                long userId=loginResult.getUserId();                
                obtainedBySelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + userId);
            }
            else{
                  obtainedBySelectItem.setValue("");
            }

        }
      else {
            // get from the server
            careteamService.getAvailableUsers(new GetUsersCallback());
        }
    }
    
    private void populateMinorConsentObtainedBy(PatientEnrollmentDTO patientEnrollment,LoginResult loginResult) {
        final long consentObtainedByUserId = patientEnrollment.getMinorConsentObtainedByUserId();
        long theUserId=0;

        if (consentObtainedByUserId > Constants.EMPTY_USER) {
            theUserId = consentObtainedByUserId;
            obtainedBySelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + theUserId);
        } else if(loginResult.isCanConsentEnrollMinors()){
                // default to current user who can consent and enroll 
                 theUserId=loginResult.getUserId();                
                 obtainedBySelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + theUserId);
            }
        else{
             obtainedBySelectItem.setValue("");
        }
      
    }
    
     class GetUsersCallback extends RetryActionCallback<List<UserDTO>> {

        @Override
        public void attempt() {
            careteamService.getAvailableUsers(this);
        }

        @Override
        public void onCapture(List<UserDTO> users) {
             List<UserDTO> userDTOList = new ArrayList<UserDTO>();
             LoginResult loginResult = ApplicationContextUtils.getLoginResult();
             boolean userCanConsentEnrollMinor = loginResult.isCanConsentEnrollMinors();
            if (!users.isEmpty()) {
                 for (UserDTO user : users) {
                if (user.isCanConsentEnrollMinors()) {
                    userDTOList.add(user);
                }

            }
            LinkedHashMap<String, String> userMap = buildUsersMap(userDTOList);
            obtainedBySelectItem.setValueMap(userMap);

            if (person != null) {
                // use value of the patient
                populateMinorConsentObtainedBy(person.getPatientEnrollment(),loginResult);
            } else if(userCanConsentEnrollMinor){
                // default to current user who can consent and enroll minors
                long userId=loginResult.getUserId();                
                obtainedBySelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + userId);
            }
            else{
                  obtainedBySelectItem.setValue("");
            }

                // update the cache
                ApplicationContext context = ApplicationContext.getInstance();
                context.putAttribute(Constants.PROVIDERS_FOR_CONSENT_OBTAINED_BY, users);

            } else {
                SC.warn("Users not found.");
            }
        }
    }
    
     private void populateMinorConsent(){
         List<UserDTO> userDTOList = new ArrayList<UserDTO>();
         LoginResult loginResult = ApplicationContextUtils.getLoginResult();
         List<UserDTO> users = ApplicationContextUtils.getProvidersForConsentObtainedBy();
        for (UserDTO user : users) {
            if (user.isCanConsentEnrollMinors()) {
                userDTOList.add(user);
            }
        }
         LinkedHashMap<String, String> userMap = buildUsersMap(userDTOList);
         obtainedBySelectItem.setValueMap(userMap);

         if (loginResult.isCanConsentEnrollMinors()) {
            // default to current user who can consent and enroll 
           long userId = loginResult.getUserId();
            obtainedBySelectItem.setValue(Constants.DELIMITER_SELECT_ITEM + userId);
        }
    }
     
     @Override
    public void onUpdateUserContext(UpdateUserContextEvent updateUserContextEvent){
        
            loadProvidersForConsentObtainedBy();
            consentObtainedBySelectItem.fetchData();
            System.out.println("update user context");
            loadConsenters();
            consenterSelectItem.fetchData();
    }
    
    private boolean isIE11Browser(){
        return Window.Navigator.getUserAgent().toLowerCase().contains("trident") ;
     }
    
    private void setFormWidth(DynamicForm theForm){
        if(isIE11Browser()){
             theForm.addStyleName("width-view-patient-form-care-plan");
        }else{
             theForm.setWidth(FORM_WIDTH);
        }
    }
}
