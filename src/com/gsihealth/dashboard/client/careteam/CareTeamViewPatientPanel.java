package com.gsihealth.dashboard.client.careteam;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.callbacks.BasicPortalAsyncCallback;
import com.gsihealth.dashboard.client.careteam.callbacks.ModifyCareteamAsyncCallback;
import com.gsihealth.dashboard.client.careteam.callbacks.ViewCareteamAsyncCallback;
import com.gsihealth.dashboard.client.common.BaseViewPatientPanel;
import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.managepatientprogram.ManagePatientProgramWindow;
import com.gsihealth.dashboard.client.service.CareteamService;
import com.gsihealth.dashboard.client.service.CareteamServiceAsync;
import com.gsihealth.dashboard.client.service.ManagePatientProgramService;
import com.gsihealth.dashboard.client.service.ManagePatientProgramServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.PropertyUtils;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Dialog;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ButtonClickEvent;
import com.smartgwt.client.widgets.events.ButtonClickHandler;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class CareTeamViewPatientPanel extends BaseViewPatientPanel {

    private ViewPatientForm viewPatientForm;
    private Button viewCareTeamButton;
    private Button modifyCareTeamButton;
    private Button addCareTeamButton;
    private Button saveButton;
    private Button cancelButton;
    private CareteamServiceAsync careteamService = GWT.create(CareteamService.class);
    private boolean childrensHealthHome=false;
    private boolean minorFlag=false;
    private boolean canConsentEnrollMinors=false;
    private String activityTrackerButtonText;
    private boolean readOnly;
    
    //JIRA 1024
    private String oldProgramLevelConsentStatus;
    private String oldEnrollmentStatus;
    private int oldActiveProgramsCount;    
    private ManagePatientProgramServiceAsync managePatientProgramService = GWT.create(ManagePatientProgramService.class);
    private boolean warnConsentStatusChangeFlag;
    private String warnConsentStatusChange;
    private String warnActivePrograms;
    private String managePatientPrograms;
    private String warnConsentStatusChangeDialogTitle;
    private String warnActiveProgramsDialogTitle;
    private String warnActiveProgramsInactiveButtonText;
    private final String SPACER_TO_TEXT = "&nbsp&nbsp&nbsp&nbsp&nbsp";
    private boolean preventPatientInactivation;
    
    public CareTeamViewPatientPanel(PersonDTO thePerson) {
        person=thePerson;
        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
        childrensHealthHome=props.isChildrensHealthHome();
        activityTrackerButtonText=props.getActivityTrackerButtonText();
        canConsentEnrollMinors=ApplicationContextUtils.getLoginResult().isCanConsentEnrollMinors();
        minorFlag= PropertyUtils.isMinor(thePerson.getDateOfBirth());
        
        //JIRA 1024
        PatientEnrollmentDTO patientEnrollment = thePerson.getPatientEnrollment();
        oldEnrollmentStatus = patientEnrollment.getStatus();
        oldProgramLevelConsentStatus = patientEnrollment.getProgramLevelConsentStatus();
        refreshOldActiveProgramsCount(patientEnrollment.getPatientId(), patientEnrollment.getCommunityId());
        
        warnConsentStatusChangeFlag = props.isWarnConsentStatusChangeFlag();
        warnConsentStatusChange = props.getWarnConsentStatusChange();
        warnActivePrograms = props.getWarnActivePrograms();
        warnConsentStatusChangeDialogTitle = props.getWarnConsentStatusChangeDialogTitle();
        warnActiveProgramsDialogTitle = props.getWarnActiveProgramsDialogTitle();
        warnActiveProgramsInactiveButtonText = props.getWarnActiveProgramsInactiveButtonText();
        
        managePatientPrograms=props.getPatientProgramButtonText();
        preventPatientInactivation=props.isPreventPatientInactivation();
        
        buildGui(thePerson);
        DashBoardApp.restartTimer();
    }

    public void buildGui(PersonDTO thePerson) {

        setPadding(5);
        setMembersMargin(5);

        Layout buttonBar = buildButtonBar();

        Label label = new Label("<h3>Patient Information</h3>");
        label.setWidth(300);
        label.setHeight(40);
        addMember(label);

        Label labelNote = new Label("<b>* indicates required field</b>");
        labelNote.setWidth(300);
        labelNote.setHeight(30);
        addMember(labelNote);

         HLayout topLayout = new HLayout(10);
        // create MyPatientList GUI items ... hide them at first
        alreadyOnListLayout = buildAlreadyOnListLayout();
        alreadyOnListLayout.hide();

        addToMyListLayout = buildAddToMyListLayout();
        addToMyListLayout.hide();

        topLayout.addMember(alreadyOnListLayout);
        topLayout.addMember(addToMyListLayout);
       // ------------Activity Tracker is moved to Carebook ----------------
//        Layout theLayout=buildAcivityTrackerButton(activityTrackerButtonText);
//        topLayout.addMember(theLayout);
        addMember(topLayout);

        viewPatientForm = new ViewPatientForm(this, thePerson);

        addMember(viewPatientForm);
        this.setOverflow(Overflow.AUTO);

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(10);
        addMember(spacer);

        addMember(buttonBar);

        handleMyPatientListGuiItems();
    }

//    private  Layout buildAcivityTrackerButton(String buttonText){
//         HLayout theLayout = new HLayout(10);
//        Button theButton= new Button(SPACER_TO_TEXT + buttonText + SPACER_TO_TEXT );
//        theButton.setID("activityTrackerButtonCareTeamViewPatientPanel");
//        theButton.setAutoFit(true);
//        theButton.addClickHandler(new activityTackerClickHanddler());
//        theLayout.addMember(theButton);
//        return theLayout;
//    }
    private Layout buildButtonBar() {
        HLayout buttonLayout = new HLayout(10);

        buttonLayout.setWidth(ViewPatientForm.FORM_WIDTH);
        buttonLayout.setAlign(Alignment.CENTER);

        viewCareTeamButton = new Button("View Care Team");
        buttonLayout.addMember(viewCareTeamButton);

        modifyCareTeamButton = new Button("Modify Care Team");
        buttonLayout.addMember(modifyCareTeamButton);

        addCareTeamButton = new Button("Create Care Team");
        buttonLayout.addMember(addCareTeamButton);

        saveButton = new Button("Save");
        buttonLayout.addMember(saveButton);

        cancelButton = new Button("Cancel");
        buttonLayout.addMember(cancelButton);

        // register listeners
        viewCareTeamButton.addClickHandler(new ViewCareTeamClickHandler());
        modifyCareTeamButton.addClickHandler(new ModifyCareTeamClickHandler());
        addCareTeamButton.addClickHandler(new AddCareTeamClickHandler());
        saveButton.addClickHandler(new SavePatientInfoClickHandler());
        cancelButton.addClickHandler(new CancelClickHandler());

        return buttonLayout;
    }

    public void showCareteamButtons(boolean flag) {
        if (flag) {
            viewCareTeamButton.show();
            modifyCareTeamButton.show();
            addCareTeamButton.show();
        } else {
            viewCareTeamButton.hide();
            modifyCareTeamButton.hide();
            addCareTeamButton.hide();
        }
    }

    class ViewCareTeamClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
            PatientInfo patientInfo = viewPatientForm.getPatientInfo();
            viewPatientForm.updatePatientInfoCareTeamDetails(viewPatientForm.getPerson().getPatientEnrollment());
            if (patientInfo != null) {
                String careteamName = patientInfo.getCareteamName();
                if (!StringUtils.isBlank(careteamName)) {

                    long patientId = patientInfo.getPatientId();
                    long patientOrgId = viewPatientForm.getPatientOrganizationId();

                    careteamService.getUsersForCareteam(patientInfo.getCommunityCareteamId(), patientId, patientOrgId, new ViewCareteamAsyncCallback(careteamName, patientId, communityId));
                } else {
                    SC.warn("You must select a care team.");
                }
            }
        }
    }

    private boolean lastCareTeamWasBlankSelected ;

    public boolean isLastCareTeamWasBlankSelected() {
        return lastCareTeamWasBlankSelected;
    }

    public void setLastCareTeamWasBlankSelected(boolean lastCareTeamWasBlankSelected) {
        this.lastCareTeamWasBlankSelected = lastCareTeamWasBlankSelected;
    }
    class SavePatientInfoClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            if (viewPatientForm.validate()) {
                 person = viewPatientForm.getPerson();
                String careteamVal = viewPatientForm.getCareTeamSelectItem().getValueAsString();
                String enrollmentStatus = viewPatientForm.getEnrollmentStatusSelectItem().getValueAsString();
                PatientInfo patientInfo = viewPatientForm.getPatientInfo();
                PatientEnrollmentDTO patEnrollment = patientInfo.getPatientEnrollment();

                String oldEnrollmentStatus=patientInfo.getOldEnrollmentStatus();
                String newEnrollmentStatus=person.getPatientEnrollment().getStatus();
                boolean enrollmentStatusIsAssigned = enrollmentStatus.equals("Assigned");

                if (enrollmentStatusIsAssigned && StringUtils.isBlank(careteamVal)) {
                    SC.warn("A care team must be selected before the record can be saved. Please select a care team or change the enrollment status.");
                    setLastCareTeamWasBlankSelected(true);
                    return;
                }                
               if(childrensHealthHome && canConsentEnrollMinors && minorFlag && patEnrollment.getInformationSharingStatus()!=null){
                     if(patEnrollment.getMinorConsentDateTime()==null){
                     SC.warn("Please complete the information sharing fields!");
                    return;
                }
              
                }
                System.out.println("oldEnrollmentStatus"+oldEnrollmentStatus+" newEnrollmentStatus"+newEnrollmentStatus);
                if(childrensHealthHome && minorFlag && !canConsentEnrollMinors && person.getPatientEnrollment().getProgramLevelConsentStatus()!=null && person.getPatientEnrollment().getProgramLevelConsentStatus().equalsIgnoreCase("Yes") && !newEnrollmentStatus.equalsIgnoreCase(oldEnrollmentStatus) && newEnrollmentStatus.equalsIgnoreCase("Enrolled")  ){
                    SC.warn("Your role does not have permission to consent or enroll minor. Please contact your administrator!");
                    return;
                }
                if(childrensHealthHome && minorFlag && !canConsentEnrollMinors && person.getPatientEnrollment().getProgramLevelConsentStatus()!=null && person.getPatientEnrollment().getProgramLevelConsentStatus().equalsIgnoreCase("Yes") && !newEnrollmentStatus.equalsIgnoreCase(oldEnrollmentStatus)&& newEnrollmentStatus.equalsIgnoreCase("Assigned" ) ){
                    SC.warn("Your role does not have permission to consent or enroll minor. Please contact your administrator!");
                    return;
                }
                /**
                 * To Fix 5412 and 7138
                 */
                if(careteamVal != null && StringUtils.isNotBlank(careteamVal) && isLastCareTeamWasBlankSelected()){                  
                    // convert from delimited format
                    careteamVal = careteamVal.split(Constants.DELIMITER_SELECT_ITEM)[1];
                    patientInfo.setCommunityCareteamId(Long.parseLong(careteamVal));
                    LinkedHashMap<String, String> careteamNames = ApplicationContextUtils.getCareteamNames();
                    String careteamName = careteamNames.get(careteamVal);
                    patientInfo.setCareteamName(careteamName);
                    patientInfo.setCareteamChanged(true);
                    setLastCareTeamWasBlankSelected(false);                    
                }
                if (viewPatientForm.validate()) {

                    if (!enrollmentStatusIsAssigned) {
                        doMppCheckAndSavePatientInfo(patientInfo);                        
                    } else {
                        checkConsentForCareteamUsers(patientInfo);
                    }
                }
            }
        }

        private void checkConsentForCareteamUsers(PatientInfo patientInfo) {

            long communityCareteamId = patientInfo.getCommunityCareteamId();
            long patientId = patientInfo.getPatientId();
            long patientOrgId = patientInfo.getPatientEnrollment().getOrgId();

            careteamService.getUsersForCareteamWithoutPermitConsent(communityCareteamId, patientId, patientOrgId, new GetUsersForCareteamWithoutPermitConsent(patientInfo));
        }
    }

    private void promptUser(PatientInfo patientInfo, List<UserDTO> users) {

        String careteamName = patientInfo.getCareteamName();

        StringBuilder message = new StringBuilder();
        message.append("<font color='red'>The assigned care team members:");

        // create a bulleted list of members
        String bulletList = createBulletListOfCareteamMembers(users);
        message.append(bulletList);

        message.append("DO NOT have consent to access the patient's information.</font>");
        message.append("<br><br>");
        message.append("Please click <b>'YES'</b> if you would still like to assign the patient to the team or click <b>'NO'</b> to change the care team selection.<br><br>");
        message.append("<font color='red'><b>Note:</b> This does not change the consent for those Care Team members.</font>");

        SC.ask("Consent Advisory for Care Team: " + careteamName, message.toString(), new PromptConsentForAllCareteamMembersCallback(patientInfo));
    }

    private String createBulletListOfCareteamMembers(List<UserDTO> users) {
        StringBuilder list = new StringBuilder();

        list.append("<ul>");
        for (UserDTO tempUser : users) {

            String name = getCompleteNamePrefixAndCredentials(tempUser);

            StringBuilder info = new StringBuilder(name);

            String org = tempUser.getOrganization();
            if (!StringUtils.isBlank(org)) {
                info.append(" - " + org);
            }

            list.append("<li>" + info + "</li>");
        }
        list.append("</ul>");

        return list.toString();
    }

    private String getCompleteNamePrefixAndCredentials(UserDTO tempUser) {
        String prefix = StringUtils.defaultString(tempUser.getPrefix());
        if (!StringUtils.isBlank(prefix)) {
            prefix += " ";


        }

        String credentials = StringUtils.defaultString(tempUser.getCredentials());
        if (!StringUtils.isBlank(credentials)) {
            credentials = ", " + credentials;
        }

        String name = prefix + tempUser.getFirstName() + " " + tempUser.getLastName() + credentials;

        return name;
    }

    class GetUsersForCareteamWithoutPermitConsent implements AsyncCallback<List<UserDTO>> {

        PatientInfo patientInfo;

        GetUsersForCareteamWithoutPermitConsent(PatientInfo thePatientInfo) {
            patientInfo = thePatientInfo;
        }

        @Override
        public void onSuccess(List<UserDTO> users) {

            // if user list is not empty, then prompt user
            if (!users.isEmpty()) {
                promptUser(patientInfo, users);
            } else {
                doMppCheckAndSavePatientInfo(patientInfo);
            }
        }

        @Override
        public void onFailure(Throwable thrwbl) {
            SC.warn("Error reading care team members. Please try again.");
        }
    }

    class PromptConsentForAllCareteamMembersCallback implements BooleanCallback {

        PatientInfo patientInfo;

        PromptConsentForAllCareteamMembersCallback(PatientInfo thePatientInfo) {
            patientInfo = thePatientInfo;
        }

        @Override
        public void execute(Boolean value) {
            if (value.booleanValue()) {
                doMppCheckAndSavePatientInfo(patientInfo);
            }
        }
    }

    //JIRA 1024
    //Ranga - introduced new method to do the mpp check 
    //(made sure existing calls are routed here by first renaming the method)
    private void doMppCheckAndSavePatientInfo(final PatientInfo patientInfo) {
        //JIRA 1024
        PatientEnrollmentDTO patEnrollment = patientInfo.getPatientEnrollment();        
        boolean hasEnrollmentStatusChanged = !patEnrollment.getStatus().equalsIgnoreCase(oldEnrollmentStatus);
        boolean hasPecChanged = !patEnrollment.getProgramLevelConsentStatus().equalsIgnoreCase(oldProgramLevelConsentStatus);
        
        final boolean isEnrolInactive = patEnrollment.getStatus().equalsIgnoreCase("Inactive");
        final boolean isPecNo = patEnrollment.getProgramLevelConsentStatus().equalsIgnoreCase("No");
        final long patientId = patEnrollment.getPatientId();
        final long communityId = patEnrollment.getCommunityId();
        
        //do patient program check if flag is true in config
        if((hasEnrollmentStatusChanged || hasPecChanged)
            && warnConsentStatusChangeFlag) {
            managePatientProgramService.getTotalCountOfActivePrograms(
                  patientId, communityId, new AsyncCallback<Integer>() {

                @Override
                public void onSuccess(Integer totalActiveCount) {
                    if(!isEnrolInactive && !isPecNo && totalActiveCount == oldActiveProgramsCount) {
                        final Button mppBtn = new Button(managePatientPrograms);
                        mppBtn.setID("mppDialog");
                        final Button cntBtn = new Button("Continue");
                        final Button cancelBtn = new Button("Cancel");
                        final Dialog dialog = new Dialog();
                        dialog.setIsModal(true);
                        dialog.setTitle(warnConsentStatusChangeDialogTitle);
                        dialog.setMessage(warnConsentStatusChange);
                        dialog.setButtons(mppBtn, cntBtn, cancelBtn);
                        dialog.addButtonClickHandler(new ButtonClickHandler() {
                            @Override
                            public void onButtonClick(ButtonClickEvent event) {
                                dialog.hide();
                                Button source = event.getButton();
                                if(source == cancelBtn) {
                                    //do nothing
                                }
                                else if(source == mppBtn) {
                                    //show the Manage Patient Programs window
                                    ManagePatientProgramWindow window = new ManagePatientProgramWindow(person);
                                    window.show();
                                }
                                else if(source == cntBtn) {
                                    savePatientInfo(patientInfo);
                                }
                            }
                        });
                        dialog.draw();
                    }
                    else if(isEnrolInactive && isPecNo && totalActiveCount > 0) {
                        final Button inactBtn = new Button(warnActiveProgramsInactiveButtonText);
                        final Button cancelBtn = new Button("Cancel");
                        final Dialog dialog = new Dialog();
                        dialog.setIsModal(true);
                        dialog.setTitle(warnActiveProgramsDialogTitle);
                        //java.text.MessageFormat is not supported in GWT
                        
                        // JIRA - 2491 Satyendra
                        // Prevent users for inactivating the patients if in active programs via config                               
                        if(preventPatientInactivation){
                               inactBtn.hide();
                        }
                        dialog.setMessage(warnActivePrograms.replace("{0}", String.valueOf(totalActiveCount)));
                        dialog.setButtons(inactBtn, cancelBtn);
                        dialog.addButtonClickHandler(new ButtonClickHandler() {
                            @Override
                            public void onButtonClick(ButtonClickEvent event) {
                                dialog.hide();
                                Button source = event.getButton();
                                if(source == cancelBtn) {
                                    //do nothing
                                }
                                else if(source == inactBtn) {
                                    savePatientInfo(patientInfo);
                                }
                            }
                        });
                        dialog.draw();
                    }
                    else {
                        savePatientInfo(patientInfo);
                    }
                }

                @Override                        
                public void onFailure(Throwable exc) {
                    SC.warn(exc.getMessage());                            
                }
            });
        }
        else {
            savePatientInfo(patientInfo);
        }
    }
    
    private void savePatientInfo(PatientInfo patientInfo) {
        boolean checkEnableCareteam = patientInfo.isEnableCareteamPanel();
        ProgressBarWindow progressBar = new ProgressBarWindow();
        progressBar.show();
        
        String confirmationMessage = "Save successful.";
        long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();


        boolean checkPatientUserActive = StringUtils.equalsIgnoreCase(patientInfo.getOriginalPatientUserActive(),
                patientInfo.getPatientUserActive());

        careteamService.savePatientInfo(person, patientInfo, checkEnableCareteam, checkPatientUserActive, communityId, new SavePatientInfoCallback(progressBar, confirmationMessage));
    }
    
    //JIRA 1024
    private void refreshOldActiveProgramsCount(long patientId, long communityId) {
        managePatientProgramService.getTotalCountOfActivePrograms(patientId, communityId,
            new AsyncCallback<Integer>() {

                @Override
                public void onSuccess(Integer totalActiveCount) {
                    oldActiveProgramsCount = totalActiveCount;
                }

                @Override                        
                public void onFailure(Throwable exc) {
                    SC.warn(exc.getMessage());                            
                }
        });
    }    

    class SavePatientInfoCallback extends BasicPortalAsyncCallback {

        public SavePatientInfoCallback(ProgressBarWindow theProgressBarWindow, String theConfirmationMessage) {
            super(theProgressBarWindow, theConfirmationMessage);
        }

        @Override
        public void onSuccess(Object t) {
            super.onSuccess(t); 
        CareTeamPanel careTeamPanel = ApplicationContextUtils.getCareTeamPanel();
        careTeamPanel.showViewPatientPanel(person);
            //JIRA 1024
            PatientEnrollmentDTO patientEnrollment = person.getPatientEnrollment();
            oldEnrollmentStatus = patientEnrollment.getStatus();
            oldProgramLevelConsentStatus = patientEnrollment.getProgramLevelConsentStatus();
            refreshOldActiveProgramsCount(patientEnrollment.getPatientId(), patientEnrollment.getCommunityId());
                    
//            refreshMyPatientList();
        }
    }
    
    class ModifyCareTeamClickHandler implements ClickHandler {

        @Override
        public void onClick(ClickEvent event) {
            PatientInfo patientInfo = viewPatientForm.getPatientInfo();
            viewPatientForm.updatePatientInfoCareTeamDetails(viewPatientForm.getPerson().getPatientEnrollment());
            System.out.println("in modifyCareTeamClickHandler ");
            String careteamName = patientInfo.getCareteamName();

            if (!StringUtils.isBlank(careteamName)) {
                ApplicationContext context = ApplicationContext.getInstance();
                context.putAttribute(Constants.VIEW_PATIENT_FORM_KEY, viewPatientForm);

                long communityCareteamId = patientInfo.getCommunityCareteamId();
                SelectItem careTeamSelectItem = viewPatientForm.getCareTeamSelectItem();

                careteamService.getUsersForCareteam(patientInfo.getCommunityCareteamId(), new ModifyCareteamAsyncCallback(communityCareteamId, careteamName, careTeamSelectItem));
            } else {
                SC.warn("You must select a care team.");
            }
        }
    }

    class AddCareTeamClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {

            ApplicationContext context = ApplicationContext.getInstance();
            context.putAttribute(Constants.VIEW_PATIENT_FORM_KEY, viewPatientForm);

            SelectItem careTeamSelectItem = viewPatientForm.getCareTeamSelectItem();
            AddCareTeamWindow window = new AddCareTeamWindow(careTeamSelectItem);
            window.show();
        }
    }

    class CancelClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            // now show the panel
            CareTeamPanel careTeamPanel = ApplicationContextUtils.getCareTeamPanel();
            careTeamPanel.showPatientSearchPanel();
        }
    }

// public class activityTackerClickHanddler implements ClickHandler{
//            @Override
//            public void onClick(ClickEvent event) {
//            readOnly=false;    
//            ActivityTrackerWindow window = new ActivityTrackerWindow(person,readOnly);
//            window.show();
//            }
//        }
}
