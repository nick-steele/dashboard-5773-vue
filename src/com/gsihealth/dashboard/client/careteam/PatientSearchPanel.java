package com.gsihealth.dashboard.client.careteam;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.common.FormUtils;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.service.CareteamService;
import com.gsihealth.dashboard.client.service.CareteamServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.client.util.PropertyUtils;
import com.gsihealth.dashboard.client.util.SearchUtils;
import com.gsihealth.dashboard.common.AccessLevelConstants;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchPatientResults;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class PatientSearchPanel extends VLayout {

    private PatientSearchForm patientSearchForm;
    private PatientSearchResultsPanel patientSearchResultsPanel;
    private Button searchButton;
    private Button clearButton;
    private MirthPatientSearchForm mirthPatientSearchForm;
    private MirthSearchClickHandler mirthSearchClickHandler;
    private final CareteamServiceAsync careteamService = GWT.create(CareteamService.class);
    private SearchClickHandler searchClickHandler;
    private PatientSearchCallback patientSearchCallback;
    private ProgressBarWindow progressBarWindow;
    private boolean searchMode;
    private int currentPageNumber;
    private long userId;
    private long communityId;
    private boolean enrolledPatientDisableIntialLoad;
    private int maxPatientSearchResultsSize;
    private boolean checkIntialFlg;
    private long userOrganizationId;

     
    public PatientSearchPanel() {
        searchMode = false;

        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        userId = loginResult.getUserId();
        communityId = loginResult.getCommunityId();
        userOrganizationId = loginResult.getOrganizationId();
        
        setStuffRequiringClientApplicationProperties();
        
        buildGui();
        DashBoardApp.restartTimer();
    }

    private void buildGui() {
        checkIntialFlg = false;
        searchClickHandler = new SearchClickHandler();

        ClientApplicationProperties clientApplicationProperties = ApplicationContextUtils.getClientApplicationProperties();
        String mpiSearch = clientApplicationProperties.getMirthSearchVisible();
        
        HLayout hLayout = new HLayout();
        hLayout.setWidth100();
        hLayout.setHeight100();

        VLayout sideSearchLayout = buildSideSearchLayout();
        hLayout.addMember(sideSearchLayout);

        patientSearchResultsPanel = new PatientSearchResultsPanel();
        patientSearchResultsPanel.setPageEventHandler(new ResultsPageEventHandler());
        patientSearchResultsPanel.setDisableIntialLoadMsg(enrolledPatientDisableIntialLoad, false);
        hLayout.addMember(patientSearchResultsPanel);
        this.setOverflow(Overflow.AUTO);
        addMember(hLayout);

        progressBarWindow = new ProgressBarWindow();
        patientSearchCallback = new PatientSearchCallback(progressBarWindow);

        // Get Patients from the server     
        if (!enrolledPatientDisableIntialLoad) {
            gotoPage(1);
        } else {
            setIntialLoadQueueSize(true);
        }
    }

    public void setIntialLoadQueueSize(boolean enrolledPatientBlnFlag) {

        TotalCountsQueueSizeCallback totalCountsQueueSizeCallback = new TotalCountsQueueSizeCallback();
        totalCountsQueueSizeCallback.attempt();
    }

    private VLayout buildSideSearchLayout() {
        VLayout sideSearchLayout = new VLayout();
        
        ClientApplicationProperties clientApplicationProperties = ApplicationContextUtils.getClientApplicationProperties();
        String mpiSearch = clientApplicationProperties.getMirthSearchVisible();
        
        sideSearchLayout.setHeight(200);
        sideSearchLayout.setWidth(250);
        sideSearchLayout.setShowResizeBar(true);

        LayoutSpacer spacer = new LayoutSpacer();
        spacer.setHeight(10);
        sideSearchLayout.addMember(spacer);
        
        if (mpiSearch.equals("mdm")) {

        searchClickHandler = new SearchClickHandler();
        patientSearchForm = new PatientSearchForm(searchClickHandler);

        Layout searchButtonBar = buildSearchButtonBar();
        sideSearchLayout.addMember(searchButtonBar);

        LayoutSpacer lowerSpacer = new LayoutSpacer();
        lowerSpacer.setHeight(10);
        sideSearchLayout.addMember(lowerSpacer);

        sideSearchLayout.addMember(patientSearchForm);

        Layout searchBottomButtonBar = buildSearchButtonBar();
        sideSearchLayout.addMember(searchBottomButtonBar);
        }
        else if(mpiSearch.equals("mirth") || mpiSearch.equals("demog")) {

            mirthSearchClickHandler = new MirthSearchClickHandler();
            mirthPatientSearchForm = new MirthPatientSearchForm(mirthSearchClickHandler);

            Layout mirthSearchButtonBar = buildMirthSearchButtonBar();
            sideSearchLayout.addMember(mirthSearchButtonBar);

            LayoutSpacer lowerSpacer = new LayoutSpacer();
            lowerSpacer.setHeight(10);
            sideSearchLayout.addMember(lowerSpacer);

            sideSearchLayout.addMember(mirthPatientSearchForm);

            Layout mirthSearchBottomButtonBar = buildMirthSearchButtonBar();
            sideSearchLayout.addMember(mirthSearchBottomButtonBar);
        }
        return sideSearchLayout;
    }

    /**
     * Helper method to build button bar
     *
     * @return
     */
    private Layout buildSearchButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        buttonLayout.setWidth100();
        buttonLayout.setAlign(Alignment.CENTER);

        searchButton = new Button("Search");
        clearButton = new Button("Clear");
        buttonLayout.addMember(searchButton);
        buttonLayout.addMember(clearButton);

        searchButton.addClickHandler(searchClickHandler);
        clearButton.addClickHandler(new ClearClickHandler());

        return buttonLayout;
    }
    
    //mirth
    private Layout buildMirthSearchButtonBar() {
        HLayout buttonLayout = new HLayout(10);
        buttonLayout.setWidth100();
        buttonLayout.setAlign(Alignment.CENTER);

        searchButton = new Button("Search");
        clearButton = new Button("Clear");
        buttonLayout.addMember(searchButton);
        buttonLayout.addMember(clearButton);

        searchButton.addClickHandler(mirthSearchClickHandler);
        clearButton.addClickHandler(new MirthClearClickHandler());

        return buttonLayout;
    }
     

    public void gotoPage(final int pageNumber) {

        progressBarWindow.setVisible(true);
        currentPageNumber = pageNumber;

        TotalCountCallback totalCountCallback = new TotalCountCallback();
        totalCountCallback.attempt();
    }

    class TotalCountCallback extends RetryActionCallback<SearchPatientResults> {

        @Override
        public void attempt() {
            

            Long userOrg = userOrganizationId;
            Long userAccessLevelId = ApplicationContextUtils.getLoginResult().getAccessLevelId();

            if (userAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
                userOrg = null;
            }
            careteamService.countPatients(communityId, null, true, userOrg, userId, this);
        }

        @Override
        public void onCapture(SearchPatientResults total) {
            patientSearchResultsPanel.setTotalCount(total.getPatientCount());
            patientSearchResultsPanel.gotoPage(currentPageNumber);

            if (total.getPatientCount() == 0) {
                progressBarWindow.setVisible(false);
                patientSearchResultsPanel.clearListGrid();

                if (searchMode) {
                    if (!checkIntialFlg) {
                        SC.warn("No patient records found.");
                    } else {
                        SC.warn(Constants.SEARCH_DID_NOT_RETURN_ANY_RESULTS);
                    }
                } else {
                    if (!checkIntialFlg) {
                        SC.warn("No patient records found.");
                    }
                }
            } else {
                if (total.getPatientCount() > 0) {
                    checkIntialFlg = true;
                }
            }
        }

        @Override
        public void onFailure(Throwable exc) {
            progressBarWindow.setVisible(false);
            super.onFailure(exc);
        }
    }

    public void gotoPage(final int pageNumber, final SearchCriteria searchCriteria) {
        currentPageNumber = pageNumber;
        progressBarWindow.setVisible(true);

        SearchTotalCountCallback searchTotalCountCallback = new SearchTotalCountCallback(searchCriteria);
        searchTotalCountCallback.attempt();
    }

    class SearchClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            searchMode = true;
            DashBoardApp.restartTimer();

            if (patientSearchForm.validate()) {
                patientSearchResultsPanel.setDisableIntialLoadMsg(enrolledPatientDisableIntialLoad, true);
                SearchCriteria searchCriteria = patientSearchForm.getSearchCriteria();
                gotoPage(1, searchCriteria);
            }
        }
    }
    class MirthSearchClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            searchMode = true;
            DashBoardApp.restartTimer();

            if (mirthPatientSearchForm.validate()) {
                patientSearchResultsPanel.setDisableIntialLoadMsg(enrolledPatientDisableIntialLoad, true);
                SearchCriteria searchCriteria = mirthPatientSearchForm.getSearchCriteria();
                gotoPage(1, searchCriteria);
            }
        } 
    }

    class ClearClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            System.out.println("somebody clicked me");
            searchMode = false;
            System.out.println("getting ready to clear psf");
            patientSearchForm.clearSearchValues();
            patientSearchForm.getProgramNameSelectItem().setValueMap(FormUtils.buildProgramNameWithCodeMap(ApplicationContextUtils.getProgramNameValue()));
                       
            DashBoardApp.restartTimer();
        }
    }
    class MirthClearClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            System.out.println("somebody clicked me");
            searchMode = false;
            System.out.println("getting ready to clear psf");
            mirthPatientSearchForm.clearSearchValues();
                       
            DashBoardApp.restartTimer();
        }
    }

    class PatientSearchCallback extends PortalAsyncCallback<SearchResults<PersonDTO>> {

        PatientSearchCallback(ProgressBarWindow theProgressBarWindow) {
            super(theProgressBarWindow);

        }

        @Override
        public void onSuccess(SearchResults<PersonDTO> searchResults) {

            if (progressBarWindow != null) {
                progressBarWindow.hide();
            }

            if (!searchMode) {
                if (searchResults.getTotalCount() > 0) {
                    checkIntialFlg = true;
                } else {
                    SC.warn("No patient records found.");
                }
            }

            List<PersonDTO> patients = searchResults.getData();
            int totalCount = searchResults.getTotalCount();

            if (!patients.isEmpty()) {
                // populate GUI
                ListGridRecord[] data = new ListGridRecord[patients.size()];

                // populate the list grid
                for (int index = 0; index < patients.size(); index++) {
                    PersonDTO tempPerson = patients.get(index);
                    data[index] = PropertyUtils.convert(index, tempPerson);
                }

                patientSearchResultsPanel.populateListGrid(data, patients, totalCount);
            } else {
                patientSearchResultsPanel.clearListGrid();

            }

            // if search results exceed max, only show warning message on first page
            if (searchMode) {

                int thePageNumber = patientSearchResultsPanel.getCurrentPageNumber();

                boolean isFirstPage = thePageNumber == 1;
                boolean mdmMaxExceeded = searchResults.isMdmMaxExceeded();

                if (mdmMaxExceeded && isFirstPage) {
                    String message = SearchUtils.getMaxPatientSearchResultsWarningMessage(maxPatientSearchResultsSize);
                    SC.warn(message);
                }
            }
        }
    }

    class ResultsPageEventHandler implements PageEventHandler {

        public void handlePage(int pageNum, int pageSize) {
            Long userAccessLevelId = ApplicationContextUtils.getLoginResult().getAccessLevelId();
            progressBarWindow.show();
            SearchCriteria searchCriteria = null;
            ClientApplicationProperties clientApplicationProperties = ApplicationContextUtils.getClientApplicationProperties();
            String mpiSearch = clientApplicationProperties.getMirthSearchVisible();
            
            if (mpiSearch.equals("mdm")){
                     searchCriteria = patientSearchForm.getSearchCriteria();
            }
            else if(mpiSearch.equals("mirth") || mpiSearch.equals("demog")){
                    searchCriteria= mirthPatientSearchForm.getSearchCriteria();
            }
            
            Long userOrg = userOrganizationId;
            
            if (userAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
                userOrg = null;
            }
            
            careteamService.findPatients(communityId, searchCriteria, true, userOrg, 
                    pageNum, pageSize, userId, patientSearchCallback);
        }
    }

    class SearchTotalCountCallback extends RetryActionCallback<SearchPatientResults> {

        private SearchCriteria searchCriteria;
        private long userAccessLevelId;

        SearchTotalCountCallback(SearchCriteria theSearchCriteria) {
            searchCriteria = theSearchCriteria;
            userAccessLevelId = ApplicationContextUtils.getLoginResult().getAccessLevelId();
        }

        @Override
        public void attempt() {
            Long userOrg = userOrganizationId;
            if (userAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
                userOrg = null;
            }
            careteamService.countPatients(communityId, searchCriteria, true, userOrg, userId, this);
        }

        @Override
        public void onCapture(SearchPatientResults total) {
            patientSearchResultsPanel.setTotalCount(total.getPatientCount());
            patientSearchResultsPanel.gotoPage(currentPageNumber);

            if (total.getPatientCount() == 0) {
                progressBarWindow.setVisible(false);
                patientSearchResultsPanel.clearListGrid();

                if (searchMode) {
                    if (!checkIntialFlg) {
                        SC.warn("No patient records found.");
                    } else {
                        SC.warn(Constants.SEARCH_DID_NOT_RETURN_ANY_RESULTS);
                    }
                }
            }
        }
        
        @Override
        public void onFailure(Throwable exc) {
            progressBarWindow.setVisible(false);
            super.onFailure(exc);
        }
    }

    protected final void setStuffRequiringClientApplicationProperties(){
       ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
        enrolledPatientDisableIntialLoad = props.isEnrolledPatientDisableIntialLoad();
        maxPatientSearchResultsSize = props.getMaxPatientSearchResultsSize();
    }

    class TotalCountsQueueSizeCallback extends RetryActionCallback<SearchPatientResults> {

        @Override
        public void attempt() {
            System.out.println("inside patientSearchPanel.TotalCountsQueueSizeCallback.attempt - counting patients");
            Long userOrg = userOrganizationId;
            Long userAccessLevelId = ApplicationContextUtils.getLoginResult().getAccessLevelId();
            if (userAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
                userOrg = null;
            }
            careteamService.countPatients(communityId, null, true, userOrg, userId, this);
        }

        @Override
        public void onCapture(SearchPatientResults total) {
            patientSearchResultsPanel.setTotalCount(0);
            patientSearchResultsPanel.setListGrid(0);
            patientSearchResultsPanel.clearListGrid();
            patientSearchResultsPanel.gotoPage(currentPageNumber);

            if (total.getPatientCount() > 0) {
                checkIntialFlg = true;
            } else {
                SC.warn("No patient records found.");
            }
        }

        @Override
        public void onFailure(Throwable exc) {
            progressBarWindow.setVisible(false);
            super.onFailure(exc);
        }
    }
    
    
}
