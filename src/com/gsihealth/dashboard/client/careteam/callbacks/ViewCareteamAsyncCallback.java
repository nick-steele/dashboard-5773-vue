package com.gsihealth.dashboard.client.careteam.callbacks;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.careteam.CareTeamInfoWindow;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.gsihealth.dashboard.client.service.NonHealthHomeProviderService;
import com.gsihealth.dashboard.client.service.NonHealthHomeProviderServiceAsync;
import com.gsihealth.dashboard.entity.dto.CareTeamDTO;
import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.util.SC;
import java.util.List;

public class ViewCareteamAsyncCallback implements AsyncCallback<CareTeamDTO> {

    private String careteamName;
    private long patientId=-1;
    private long communityId;
    private NonHealthHomeProviderServiceAsync nonHealthHomeProviderService = GWT.create(NonHealthHomeProviderService.class);


    public ViewCareteamAsyncCallback(String theCareteamName, long thePatientId, long theCommunityId) {
        careteamName = theCareteamName;
        patientId = thePatientId;
        communityId = theCommunityId;

    }
    
    public ViewCareteamAsyncCallback(String theCareTeamName) {
        careteamName = theCareTeamName;

    }

    @Override
    public void onSuccess(CareTeamDTO careTeamUser) {
        if(patientId==-1) {
            if (careTeamUser!=null) {
                CareTeamInfoWindow window = new CareTeamInfoWindow(careteamName, careTeamUser);
                window.show();
                window.validate();
            } else {
                SC.warn("Care team members have not been assigned.");
            }
        } else {

        nonHealthHomeProviderService.getProviderList(patientId, communityId, new ViewCareteamForwardAsyncCallback(careteamName, patientId, careTeamUser));
        }

    }

    @Override
    public void onFailure(Throwable thrwbl) {
        SC.warn("Error retrieving care team members.");
    }

    private class ViewCareteamForwardAsyncCallback extends PortalAsyncCallback<List<NonHealthHomeProviderDTO>> {

        String careTeamName;
        long patientId;
       CareTeamDTO careTeamDTOs;

        private ViewCareteamForwardAsyncCallback(String theCareTeamName, long thePatientId, CareTeamDTO careTeamUser) {
            careTeamName = theCareTeamName;
            patientId = thePatientId;
            careTeamDTOs = careTeamUser;
        }

        @Override
        public void onSuccess(List<NonHealthHomeProviderDTO> providerDTOs) {
            
            if (careTeamDTOs!=null) {
                CareTeamInfoWindow window = new CareTeamInfoWindow(careteamName, careTeamDTOs, providerDTOs);
                window.show();
                window.validate();
            } else {
                SC.warn("Care team members have not been assigned.");
            }
        }
    }
}
