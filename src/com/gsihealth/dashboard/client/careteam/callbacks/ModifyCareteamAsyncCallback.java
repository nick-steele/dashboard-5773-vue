package com.gsihealth.dashboard.client.careteam.callbacks;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.careteam.ManageCareTeamWindow;
import com.gsihealth.dashboard.entity.dto.CareTeamDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class ModifyCareteamAsyncCallback implements AsyncCallback<CareTeamDTO> {

    private long communityCareteamId;
    private String careteamName;
    private SelectItem careTeamSelectItem;
     private ManageCareTeamWindow manageCareTeamWindow;
     
    public ModifyCareteamAsyncCallback(long theCommunityCareteamId, String theCareteamName) {
        this(theCommunityCareteamId, theCareteamName, null);
    }

    public ModifyCareteamAsyncCallback(long theCommunityCareteamId, String theCareteamName, SelectItem theCareTeamSelectItem) {
        communityCareteamId = theCommunityCareteamId;
        careteamName = theCareteamName;
        careTeamSelectItem = theCareTeamSelectItem;
    }

    @Override
    public void onSuccess(CareTeamDTO careTeamUsers) {
        List<UserDTO> users=careTeamUsers.getUserDTOList();
        ManageCareTeamWindow window = new ManageCareTeamWindow(communityCareteamId, careteamName, careTeamSelectItem);
        window.populateSelectedUsers(users);
//        window.gotoPage(1);
        window.show();
        window.loadAvailableUsersData();
    }

    @Override
    public void onFailure(Throwable thrwbl) {
        SC.warn("Error retrieving care team members.");
    }
}