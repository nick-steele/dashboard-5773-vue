package com.gsihealth.dashboard.client.careteam.callbacks;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.common.RetryActionCallback;
import com.gsihealth.dashboard.client.service.CareteamService;
import com.gsihealth.dashboard.client.service.CareteamServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.Constants;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import java.util.LinkedHashMap;

/**
 * Populate the care teams drop down list w/ data from server
 */
public class GetCareteamNamesCallback extends RetryActionCallback<LinkedHashMap<String, String>> {

    private CareteamServiceAsync careteamService = GWT.create(CareteamService.class);
    private SelectItem selectItem;
    private Long communityId;
    private String newValue;

    public GetCareteamNamesCallback(SelectItem selectItem, Long communityId, String newValue) {
        this.selectItem = selectItem;
        this.communityId = communityId;
        this.newValue = newValue;
    }

    public GetCareteamNamesCallback(SelectItem selectItem, Long communityId) {
        this.selectItem = selectItem;
        this.communityId = communityId;
    }

    public GetCareteamNamesCallback(SelectItem theSelectItem) {
        selectItem = theSelectItem;
    }

    @Override
    public void attempt() {
        careteamService.getCareteamNames(communityId, this);        
    }
    
    @Override
    public void onCapture(LinkedHashMap<String, String> baseData) {

        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
        data.put("", "");
        data.put(Constants.DELIMITER_SELECT_ITEM + Constants.BLANK_CARETEAM_ID, "'No name'");
        
        for (String tempKey : baseData.keySet()) {
            String tempValue = baseData.get(tempKey);
        
            tempKey = Constants.DELIMITER_SELECT_ITEM + tempKey;
            data.put(tempKey, tempValue);
        }
                
        selectItem.setValueMap(data);
        if(newValue != null) {
            selectItem.setValue(newValue);
        }
        
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        applicationContext.putAttribute(Constants.CARE_TEAM_CODES_KEY, baseData);
    }

}
