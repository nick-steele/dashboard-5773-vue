package com.gsihealth.dashboard.client.careteam;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.common.GridPager;
import com.gsihealth.dashboard.client.common.PageEventHandler;
import com.gsihealth.dashboard.client.common.PersonRecord;
import com.gsihealth.dashboard.client.service.CareteamService;
import com.gsihealth.dashboard.client.service.CareteamServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.DateFieldCellFormatter;
import com.gsihealth.dashboard.common.AccessLevelConstants;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;
import com.gsihealth.dashboard.redux.Redux;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.LayoutSpacer;
import com.smartgwt.client.widgets.layout.VLayout;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class PatientSearchResultsPanel extends VLayout {

    private static final int DATE_COLUMN = 3;
    private ListGrid listGrid;
    private Button manageCareTeamButton;
    private List<PersonDTO> persons;
    private GridPager pager;
    private final CareteamServiceAsync careteamService = GWT.create(CareteamService.class);
    private static final String POWERUSERMSG = "<font color='red'>As a power user, system allows you to access ALL patients regardless of their consent preferences.</font>";
    private static final String CONSENTMSG = "<font color='red'>Search results consist of only patients who have granted consent to your organization to view their entire patient record.</font>";
    private static final String INITIALLOADINGMSG = "<font color='red'>Enter Search Criteria and Click 'Search' to retrieve the patient list. </font>";
    private Label consentLabel;
    private boolean patientEngagementEnable;
    private static String[] columnNames;
    private static String[] columnNamesMinor;
    private boolean requireChh;
    
    public PatientSearchResultsPanel() {
        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
        patientEngagementEnable = props.isPatientEngagementEnable();
        requireChh = props.isChildrensHealthHome();
        buildGui();
    }

    private void buildGui() {

        listGrid = buildListGrid();

        setPadding(5);

        // header
        Layout headerBar = buildHeaderBar();
        addMember(headerBar);

        // grid
        addMember(listGrid);

        // pager
        pager = new GridPager(0);
        addMember(pager);

        LayoutSpacer topSpacer = new LayoutSpacer();
        topSpacer.setHeight(20);
        addMember(topSpacer);

        // buttons bar
        Layout buttonBar = buildButtonBar();
        addMember(buttonBar);

        setWidth100();
        this.setOverflow(Overflow.AUTO);

        listGrid.addRecordDoubleClickHandler(new PatientRecordDoubleClickHandler());
    }

    public void populateListGrid(ListGridRecord[] data, List<PersonDTO> thePersons, int totalCount) {

        // scroll to first row
        listGrid.scrollToRow(1);

        listGrid.setData(data);
        listGrid.markForRedraw();
        persons = thePersons;

        pager.setTotalCount(totalCount);
    }

    /** Helper method to build a ListGrid
     *
     * @return
     */
    private ListGrid buildListGrid() {
        if (requireChh) {
            if (patientEngagementEnable) {
                columnNames = new String[]{"Patient ID", "Last Name", "First Name", "Middle Name", "DOB", "Minor", "Address", "Organization", "Patient Status", "Care Team", "Patient Messaging"};
            } else {
                columnNames = new String[]{"Patient ID", "Last Name", "First Name", "Middle Name",
                    "DOB", "Minor", "Address", "Organization",
                    "Patient Status", "Care Team"};
            }
        } else {
            if (patientEngagementEnable) {
                columnNames = new String[]{"Patient ID", "Last Name", "First Name", "Middle Name", "DOB", "Address", "Organization", "Patient Status", "Care Team", "Patient Messaging"};
            } else {
                columnNames = new String[]{"Patient ID", "Last Name", "First Name", "Middle Name",
                    "DOB", "Address", "Organization",
                    "Patient Status", "Care Team"};
            }
        }
        ListGrid theListGrid = new ListGrid();

        theListGrid.setCanReorderFields(true);
        theListGrid.setCanReorderRecords(true);

        theListGrid.setWidth100();
        theListGrid.setHeight100();

        theListGrid.setAlternateRecordStyles(true);
        theListGrid.setShowAllRecords(false);
        theListGrid.setSelectionType(SelectionStyle.SINGLE);

        theListGrid.setCanEdit(false);

        // add fields
        ListGridField[] fields = buildListGridFields(columnNames);
        theListGrid.setFields(fields);

        ListGridField dateField = fields[DATE_COLUMN];
        setDateFieldCellFormatter(dateField);

        theListGrid.setEmptyMessage("...");
        theListGrid.setCanResizeFields(true);

        return theListGrid;
    }

    private ListGridField[] buildListGridFields(String[] columnNames) {
        ListGridField[] fields = new ListGridField[columnNames.length];

        for (int i = 0; i < columnNames.length; i++) {
            String tempColumnName = columnNames[i];
            ListGridField tempField = buildListGridField(tempColumnName);
            fields[i] = tempField;
        }

        return fields;
    }

    private ListGridField buildListGridField(String title) {
        ListGridField tempField = new ListGridField(title, title);

        tempField.setCanEdit(false);

        return tempField;
    }

    public ListGrid getListGrid() {
        return listGrid;
    }

    private Layout buildButtonBar() {
        HLayout buttonLayout = new HLayout(7);

        buttonLayout.setWidth100();
        buttonLayout.setAlign(Alignment.CENTER);

        manageCareTeamButton = new Button("Manage Care Team");
        manageCareTeamButton.setWidth(120);
        manageCareTeamButton.setID("ManageCareTeam");

        buttonLayout.addMember(manageCareTeamButton);

        manageCareTeamButton.addClickHandler(new ManageCareTeamClickHandler());
        return buttonLayout;
    }

    private Layout buildHeaderBar() {
        //long accessLevelId = ApplicationContextUtils.getLoginResult().getAccessLevelId();

        HLayout layout = new HLayout(10);

        layout.setWidth100();
        layout.setAlign(Alignment.CENTER);

        consentLabel = new Label();
        consentLabel.setHeight(30);
        consentLabel.setWidth100();
        consentLabel.setAlign(Alignment.CENTER);

        layout.addMember(consentLabel);

        return layout;
    }

    /**
     * Add cell formatter for date field
     *
     * @param dateField
     */
    private void setDateFieldCellFormatter(ListGridField dateField) {

        final DateTimeFormat dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
        dateField.setCellFormatter(new DateFieldCellFormatter(dateFormatter));
    }

    public void clearListGrid() {
        ListGridRecord[] records = new ListGridRecord[1];
        listGrid.setData(records);
    }

    protected void handleSelectedPatient() {
        PersonRecord theRecord = (PersonRecord) listGrid.getSelectedRecord();

        if (theRecord == null) {
            SC.warn("Nothing selected.");
            return;
        }

        int index = theRecord.getIndex();

        // get selected DTO
        final PersonDTO thePerson = persons.get(index);

        // now show the panel
        if (thePerson.isSelfAssertEligible()) {
            final ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
            if (ApplicationContextUtils.getClientApplicationProperties().isPatientSelfAssertedConsentAccess()) {
                SC.confirm("Warning", props.getPatientSelfAssertWarningText(), new BooleanCallback() {
                    @Override
                    public void execute(Boolean value) {
                        if (value.booleanValue()) {

                            final String consentLanguage = props.getConsentLanguage();
                            SC.ask("Consent Confirmation", consentLanguage, new BooleanCallback() {
                                public void execute(Boolean value) {
                                    if (value.booleanValue()) {
                                        thePerson.getPatientEnrollment().setSelfAssertedConsentDate(new Date());
                                        careteamService.updatePatientBySelfAssertion(thePerson, new AsyncCallback<PersonDTO>() {
                                            @Override
                                            public void onSuccess(PersonDTO updatedPersonDtoAfterSelfAssertion) {
                                                // don't do anything, just update
                                            }

                                            @Override
                                            public void onFailure(Throwable cause) {
                                                // don't do anything - if update fails, still give access, just won't be permanent
                                            }
                                        });
                                        long patientId = thePerson.getPatientEnrollment().getPatientId();
                                        Redux.jsEvent("local:appPatientEnrollment:openCareTeamWizard", "{\"patientId\": " + patientId + "}");
                                    }
                                }
                            });

                        } else {
                            SC.say("Do nothing");
                        }
                    }

                });
            } else {
                SC.say("This patient has not yet granted access to your organization.  Please contact your administrator.");
            }
        }  else {

            long patientId = thePerson.getPatientEnrollment().getPatientId();
            Redux.jsEvent("local:appPatientEnrollment:openCareTeamWizard", "{\"patientId\": " + patientId + "}");
        }
    }

    public void gotoPage(int pageNumber) {
        pager.gotoPage(pageNumber);
    }

    public int getCurrentPageNumber() {
        return pager.getCurrentPageNumber();
    }

    public void setPageEventHandler(PageEventHandler handler) {
        pager.setPageEventHandler(handler);
    }

    public void refreshPageLabels(int pageNum) {
        pager.refreshPageLabels(pageNum);
    }

    public void setTotalCount(int totalCount) {
        pager.setTotalCount(totalCount);
    }

    public int getPageSize() {
        return pager.getPageSize();
    }

    class PatientRecordDoubleClickHandler implements RecordDoubleClickHandler {

        @Override
        public void onRecordDoubleClick(RecordDoubleClickEvent event) {
            handleSelectedPatient();
        }
    }

    class ManageCareTeamClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            PersonRecord theRecord = (PersonRecord) listGrid.getSelectedRecord();
            long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();

            if (theRecord == null) {
                SC.warn("Nothing selected.");
                return;
            }

//            String careteamName = theRecord.getCareteam();

            /*if (StringUtils.isBlank(careteamName)) {
                SC.warn("Patient does not have a care team.");
                return;
            }*/


            PatientEnrollmentDTO patientEnrollment = theRecord.getPersonDTO().getPatientEnrollment();

            long patientId = patientEnrollment.getPatientId();
            Redux.jsEvent("local:appPatientEnrollment:openCareTeamWizard", "{\"patientId\": " + patientId +"}");

            //careteamService.getUsersForCareteam(careteamId, patientId, patientOrgId, new ViewCareteamAsyncCallback(careteamName, patientId, communityId));

        }
    }

    public void setDisableIntialLoadMsg(boolean disableIntialLoadFlag, boolean searchModeFlag) {
        long userAccessLevelId = ApplicationContextUtils.getLoginResult().getAccessLevelId();
        
       
        // add message to consent label on header
        if (disableIntialLoadFlag && !searchModeFlag) {
             consentLabel.setContents(INITIALLOADINGMSG);
        }

        if (disableIntialLoadFlag && searchModeFlag) {
          
            if (userAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
                consentLabel.setContents(POWERUSERMSG);
            } else {
                consentLabel.setContents(CONSENTMSG);
            }
        }

        if (!disableIntialLoadFlag && !searchModeFlag) {
           
            if (userAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
                consentLabel.setContents(POWERUSERMSG);
            } else {
                consentLabel.setContents(CONSENTMSG);
            }
        }
    }
    
     public void setListGrid(int totalCount) {

        // scroll to first row
        listGrid.scrollToRow(1);
 
        listGrid.markForRedraw();
        pager.setTotalCount(totalCount);
    }
        
}
