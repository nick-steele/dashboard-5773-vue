/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.client.careteam;
import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.gsihealth.dashboard.client.careteam.callbacks.GetCareteamNamesCallback;
import com.gsihealth.dashboard.client.common.Cancelable;
import com.gsihealth.dashboard.client.common.FormUtils;
import com.gsihealth.dashboard.client.common.GetProgramCallback;
import com.gsihealth.dashboard.client.enrollment.callbacks.GetOrganizationNamesCallback;
import com.gsihealth.dashboard.client.managepatientprogram.ManagePatientProgramUtils;
import com.gsihealth.dashboard.client.service.*;
import com.gsihealth.dashboard.client.util.*;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.util.SsnUtils;
import com.gsihealth.dashboard.entity.dto.*;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.DateDisplayFormat;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.*;
import com.smartgwt.client.widgets.form.fields.events.*;
import com.smartgwt.client.widgets.form.validator.DateRangeValidator;
import com.smartgwt.client.widgets.form.validator.LengthRangeValidator;
import com.smartgwt.client.widgets.form.validator.RegExpValidator;

import java.util.*;
 
/**
 *
 * @author rsundar
 */
public class MirthPatientSearchForm extends DynamicForm implements Cancelable {
    private TextItem lastNameTextItem;
    private TextItem firstNameTextItem;
    private TextItem middleNameTextItem;
    private DateItem dateOfBirthDateItem;
    private SelectItem genderSelectItem;
    private TextItem address1TextItem;
    private TextItem address2TextItem;
    private TextItem cityTextItem;
    private TextItem zipCodeTextItem;
    private SelectItem stateSelectItem;
    private TextItem phoneTextItem;
    private SelectItem programLevelConsentStatusSelectItem;
    private SelectItem programNameSelectItem;
    private SelectItem enrollmentStatusSelectItem;
    private SelectItem organizationSelectItem;
    private DateTimeFormat dateFormatter;
    private SelectItem careTeamSelectItem;
    private OrganizationServiceAsync orgService = GWT.create(OrganizationService.class);
    private CareteamServiceAsync careteamService = GWT.create(CareteamService.class);
    private ProgramServiceAsync programService = GWT.create(ProgramService.class);

    private String[] enrollmentStatusCodes = {"Enrolled", "Assigned"};
    private ClickHandler searchClickHandler;
    private TextItem SSNItem;
    private TextItem medicaidItem;
    private SectionItem insuranceSectionItem;
    private LengthRangeValidator ssnLengthValidator;
    private String plcsTitle;
    private TextItem patientIDItem;
    private SelectItem programHealthHomeSelectItem;
    private String healthHomeLabel;
    private SelectItem patientUserActiveSelectItem;
    private String[] patientMessagingStatus = {"", "Active", "Inactive", "None"};
    private boolean patientEngagementEnable;
    private ComboBoxItem providerTypeComboBoxItem;
    private ComboBoxItem providerNamesComboBoxItem;

    private LinkedHashMap<String, String> providerTypes = null;

    private final String IN_PROVIDERS = Constants.IN_PROVIDER;
    private final String OUT_PROVIDERS = Constants.OUT_PROVIDER;

    private NonHealthHomeProviderServiceAsync providerService = GWT.create(NonHealthHomeProviderService.class);
    private AdminServiceAsync adminService = GWT.create(AdminService.class);
    private LinkedHashMap<String, String> providerMap;
    private LinkedHashMap<String, String> UserMap;
    private Long communityId;
    private SelectItem minorSelectItem;
    public static final String[] MINOR_CODES_FOR_CANDIDATES = {"Yes", "No"};
    private boolean requireChh;
    private boolean programSectionInitialized;
    private boolean requireParentProgram;

    private LinkedHashMap<Long,String> parentProgramFullMap;
    private LinkedHashMap<Long,String> subProgramFullMap;
    private LinkedHashMap<Long,String> statusFullMap;
    private LinkedHashMap<Long,String> systemStatusMap;
    private LinkedHashMap<Integer,String> consentFullMap;
    private LinkedHashMap<Integer,String> consentNotRequiredMap;
    private LinkedHashMap<Integer, String> programLevelConsentStatusMapCodes;


    public MirthPatientSearchForm(ClickHandler theSearchClickHandler) {

        searchClickHandler = theSearchClickHandler;
        communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
        dateFormatter = DateTimeFormat.getFormat("MM/dd/yyyy");
        ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
        programLevelConsentStatusMapCodes = ApplicationContextUtils.getProgramLevelConsentStatusMapCodes();
        patientEngagementEnable = props.isPatientEngagementEnable();
        healthHomeLabel = props.getHealthHomeLabel();
        requireChh = props.isChildrensHealthHome();
        requireParentProgram = props.isHealthHomeRequired();
        long communityId=ApplicationContextUtils.getLoginResult().getCommunityId();

        providerTypes = props.getProviderTypes();

        buildGui();

        loadOrganizationNames(communityId);
        System.out.println("IN aptient Search form");
        loadCareTeamNames();
//        loadInProviderNames();
//        loadOutProviderNames();

        ApplicationContext context = ApplicationContext.getInstance();
        context.putAttribute(Constants.PATIENT_SEARCH_FORM_KEY, this);

    }

    private void buildGui() {

        // getting PLCS title from prop file.
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        plcsTitle = loginResult.getPlcsTitle();

        KeyPressHandler searchKeyPressHandler = new SearchKeyPressHandler();

        setBrowserSpellCheck(false);

        RegExpValidator alphaOnlyRegExpValidator = ValidationUtils.getAlphaOnlyRegExpValidator();
        RegExpValidator alphaHGAARegExValidator = ValidationUtils.getAlphaHGAARegExValidator();
        RegExpValidator alphaNumberRegExValidator = ValidationUtils.getAlphaNumberRegExpValidator();
        this.setRequiredTitlePrefix(FormUtils.REQUIRED_TITLE_PREFIX);
        //
        // Demographics
        //
        SectionItem demographicsSectionItem = buildSectionItem("Demographics");

        lastNameTextItem = new TextItem("lastName", "Last Name");
        lastNameTextItem.setRequired(false);
        ValidationUtils.setValidators(lastNameTextItem, alphaHGAARegExValidator, false);
        lastNameTextItem.addKeyPressHandler(searchKeyPressHandler);

        firstNameTextItem = new TextItem("firstName", "First Name");
        ValidationUtils.setValidators(firstNameTextItem, alphaHGAARegExValidator, true);
        firstNameTextItem.addKeyPressHandler(searchKeyPressHandler);

        middleNameTextItem = new TextItem("middleName", "Middle Name");
        ValidationUtils.setValidators(middleNameTextItem, alphaHGAARegExValidator, true);
        middleNameTextItem.addKeyPressHandler(searchKeyPressHandler);

        dateOfBirthDateItem = buildDateItem("dateOfBirth", "Date of Birth");
        genderSelectItem = new SelectItem("gender", "Gender");
        genderSelectItem.setValueMap(ApplicationContextUtils.getGenderCodes());

        // Length validator for SSN
        ssnLengthValidator = new LengthRangeValidator();
        ssnLengthValidator.setMax(SsnUtils.SSN_LENGTH);
        ssnLengthValidator.setErrorMessage("Invalid length");

        SSNItem = new TextItem("SSN", "SSN");
        SSNItem.setMask("###-##-####");
        SSNItem.setMaskOverwriteMode(true);
        SSNItem.setValidators(ssnLengthValidator);
        SSNItem.addKeyPressHandler(searchKeyPressHandler);

        //patient id for search
        patientIDItem = new TextItem("PatientId", "Patient ID");
        patientIDItem.setMask("#########");
        patientIDItem.setMaskOverwriteMode(true);
        
        minorSelectItem = new SelectItem("minor", "Minor");
        this.setMinorValues(MINOR_CODES_FOR_CANDIDATES);
        minorSelectItem.setDefaultValue("");

        LengthRangeValidator patientIDValidator = new LengthRangeValidator();
        patientIDValidator.setMax(9);
        patientIDValidator.setMin(1);
        patientIDItem.setValidators(patientIDValidator);
        patientIDItem.addKeyPressHandler(searchKeyPressHandler);
        if (requireChh){
        demographicsSectionItem.setItemIds(lastNameTextItem.getName(), firstNameTextItem.getName(), middleNameTextItem.getName(), dateOfBirthDateItem.getName(), genderSelectItem.getName(), SSNItem.getName(), patientIDItem.getName(),minorSelectItem.getName());
        } else{
         demographicsSectionItem.setItemIds(lastNameTextItem.getName(), firstNameTextItem.getName(), middleNameTextItem.getName(), dateOfBirthDateItem.getName(), genderSelectItem.getName(), SSNItem.getName(), patientIDItem.getName());

        }
        //
        // Address
        //
        RegExpValidator alphaNumberSpecialSymbolsRegExpValidator = ValidationUtils.getAlphaNumberSpecialSymbolsRegExpValidator();

        SectionItem addressSectionItem = buildSectionItem("Address");
        addressSectionItem.setSectionExpanded(false);
        address1TextItem = new TextItem("address1", "Address 1");
        ValidationUtils.setValidators(address1TextItem, alphaNumberSpecialSymbolsRegExpValidator, true);
        address1TextItem.addKeyPressHandler(searchKeyPressHandler);

        address2TextItem = new TextItem("address2", "Address 2");
        ValidationUtils.setValidators(address2TextItem, alphaNumberSpecialSymbolsRegExpValidator, true);
        address2TextItem.addKeyPressHandler(searchKeyPressHandler);

        cityTextItem = new TextItem("city", "City");
        ValidationUtils.setValidators(cityTextItem, alphaOnlyRegExpValidator, true);

        stateSelectItem = new SelectItem("state", "State");
        stateSelectItem.setValueMap(ApplicationContextUtils.getStateCodes());

        zipCodeTextItem = buildZipCodeTextItem();
        zipCodeTextItem.addKeyPressHandler(searchKeyPressHandler);

        phoneTextItem = buildPhoneTextItem();
        phoneTextItem.addKeyPressHandler(searchKeyPressHandler);

        addressSectionItem.setItemIds(address1TextItem.getName(), address2TextItem.getName(), cityTextItem.getName(), stateSelectItem.getName(), zipCodeTextItem.getName(), phoneTextItem.getName());

        //
        // Care team
        //
        SectionItem careTeamSectionItem = buildSectionItem("Care Team");
        careTeamSectionItem.setSectionExpanded(false);

        careTeamSelectItem = new SelectItem("careTeam", "Care Team");
//        if (patientEngagementEnable) {
        patientUserActiveSelectItem = new SelectItem("patientMessaging", "Patient Messaging");
        patientUserActiveSelectItem.setWrapTitle(true);
        patientUserActiveSelectItem.setValueMap(patientMessagingStatus);

         // provider type
        providerTypeComboBoxItem = new ComboBoxItem("providerType", "Provider Type");
        providerTypeComboBoxItem.setAddUnknownValues(false);
        providerTypeComboBoxItem.setAllowEmptyValue(true);
        providerTypeComboBoxItem.setWrapTitle(true);
        providerTypeComboBoxItem.setWidth(150);
        providerTypeComboBoxItem.setValueMap(providerTypes);
        providerTypeComboBoxItem.addChangedHandler(new providerTypeChangeHandler());

        providerTypeComboBoxItem.addKeyPressHandler(searchKeyPressHandler);

          //In/Out Network providers
        providerNamesComboBoxItem = new ComboBoxItem("providerName", "Provider Name");
        providerNamesComboBoxItem.setAddUnknownValues(false);
        providerNamesComboBoxItem.setAllowEmptyValue(true);
        providerNamesComboBoxItem.setWrapTitle(true);
        providerNamesComboBoxItem.setWidth(150);
        providerNamesComboBoxItem.setDisabled(true);

        providerNamesComboBoxItem.addKeyPressHandler(searchKeyPressHandler);

//        careTeamSectionItem.setItemIds(careTeamSelectItem.getName(), outOfNetworkProviderComboBoxItem.getName());
        careTeamSectionItem.setItemIds(careTeamSelectItem.getName(), providerTypeComboBoxItem.getName(), providerNamesComboBoxItem.getName(), patientUserActiveSelectItem.getName());

        //
        // Enrollment
        // Change to Program
        //
        final long communityId = ApplicationContextUtils.getLoginResult().getCommunityId();
        final SectionItem enrollmentSectionItem = buildSectionItem("Programs");
        enrollmentSectionItem.setSectionExpanded(false);
        enrollmentSectionItem.addClickHandler(new com.smartgwt.client.widgets.form.fields.events.ClickHandler() {

            @Override
            public void onClick(ClickEvent clickEvent) {
                if(programSectionInitialized) return;
                programSectionInitialized = true;
                if(requireParentProgram) {
                    programService.getParentPrograms(communityId, new GetParentProgramCallback(true));
                    systemStatusMap = new LinkedHashMap<Long, String>();
                    systemStatusMap.put(0L, "Active");
                    systemStatusMap.put(1L, "Inactive");
                }
                programService.getSubordinatePrograms(communityId, new GetSubordinateProgramCallback(true));
                programService.getProgramStatuses(
                        communityId,
                        new GetProgramStatusCallback(enrollmentStatusSelectItem, true)
                );
            }
        });


        if(requireParentProgram){
            //Health Home search
            programHealthHomeSelectItem=new SelectItem("healthHome", healthHomeLabel);
            programHealthHomeSelectItem.setAllowEmptyValue(true);
            programHealthHomeSelectItem.addChangedHandler(new ChangedHandler() {
                @Override
                public void onChanged(ChangedEvent changedEvent) {

                    if(changedEvent.getValue() == null){
                        programNameSelectItem.setValueMap(subProgramFullMap);
                        programNameSelectItem.clearValue();
                        enrollmentStatusSelectItem.setValueMap(statusFullMap);
                        enrollmentStatusSelectItem.clearValue();
                        programLevelConsentStatusSelectItem.setValueMap(consentFullMap);
                        programLevelConsentStatusSelectItem.clearValue();
                        return;
                    }
                    String parentProgramId = changedEvent.getValue().toString();
                    programService.filterProgramOptionsByParent(
                            communityId,
                            Long.parseLong(parentProgramId),
                            new GetSearchFilterCallback(true)
                    );
                }
            });
        }

        programNameSelectItem = new SelectItem("programName", "Program Name");
        programNameSelectItem.setAllowEmptyValue(true);
        programNameSelectItem.addChangedHandler(new ChangedHandler() {
            @Override
            public void onChanged(ChangedEvent changedEvent) {

                if(changedEvent.getValue()!=null){
                    String subordinateProgramId = changedEvent.getValue().toString();
                    programService.filterProgramOptionsByProgram(
                            communityId,
                            Long.parseLong(subordinateProgramId),
                            new GetSearchFilterCallback(false)
                    );
                }else if(requireParentProgram && !programHealthHomeSelectItem.getValueAsString().isEmpty()){
                    String parentProgramId = programHealthHomeSelectItem.getValueAsString();
                    programService.filterProgramOptionsByParent(
                            communityId,
                            Long.parseLong(parentProgramId),
                            new GetSearchFilterCallback(true)
                    );
                }else{
                    enrollmentStatusSelectItem.setValueMap(statusFullMap);
                    enrollmentStatusSelectItem.clearValue();
                    programLevelConsentStatusSelectItem.setValueMap(consentFullMap);
                    programLevelConsentStatusSelectItem.clearValue();
                }
            }
        });

        programLevelConsentStatusSelectItem = new SelectItem("programLevel", plcsTitle);
        programLevelConsentStatusSelectItem.setAllowEmptyValue(true);

        setupConsentMaps();
        programLevelConsentStatusSelectItem.setValueMap(consentFullMap);
        programLevelConsentStatusSelectItem.setAllowEmptyValue(true);


        enrollmentStatusSelectItem = new SelectItem("patientStatus", "Patient Status");

        List<String> sectionItemIDs = new ArrayList<String>();
        sectionItemIDs.add(programLevelConsentStatusSelectItem.getName());
        if(requireParentProgram) sectionItemIDs.add(programHealthHomeSelectItem.getName());
        sectionItemIDs.add(programNameSelectItem.getName());
        sectionItemIDs.add(enrollmentStatusSelectItem.getName());

        enrollmentSectionItem.setItemIds(
                sectionItemIDs.toArray(new String[]{})
        );

        //
        // Organization
        //
        SectionItem organizationSectionItem = buildSectionItem("Organization");
        organizationSectionItem.setSectionExpanded(false);

        organizationSelectItem = new SelectItem();
        organizationSelectItem.setTitle("Organization");

        organizationSectionItem.setItemIds(organizationSelectItem.getName());

        // Insurance section
        insuranceSectionItem = buildSectionItem("Insurance");
        insuranceSectionItem.setSectionExpanded(false);

        medicaidItem = new TextItem("MedicaidItem", "Medicaid/Medicare/Payer ID");
        medicaidItem.addKeyPressHandler(searchKeyPressHandler);
        ValidationUtils.setValidators(medicaidItem, alphaNumberRegExValidator, false);

        insuranceSectionItem.setItemIds(medicaidItem.getName());
        RowSpacerItem rowSpacerItem = new RowSpacerItem();

        List<FormItem> demographicSection = new ArrayList<FormItem>();
        demographicSection.addAll(
                Arrays.asList(
                        demographicsSectionItem,
                        lastNameTextItem,
                        firstNameTextItem,
                        middleNameTextItem,
                        dateOfBirthDateItem,
                        genderSelectItem,
                        SSNItem,
                        patientIDItem
                )
        );
        if(requireChh) demographicSection.add(minorSelectItem);

        FormItem[] addressSection = new FormItem[]{
                addressSectionItem,
                address1TextItem,
                address2TextItem,
                cityTextItem, stateSelectItem,
                zipCodeTextItem,
                phoneTextItem
        };

        List<FormItem> careteamSection = new ArrayList<FormItem>();
        careteamSection.addAll(
                Arrays.asList(
                        careTeamSectionItem,
                        careTeamSelectItem,
                        providerTypeComboBoxItem,
                        providerNamesComboBoxItem
                )
        );
        if(patientEngagementEnable) {
            careteamSection.add(patientUserActiveSelectItem);
        }

        List<FormItem> programSection = new ArrayList<FormItem>();
        programSection.add(enrollmentSectionItem);
        if(requireParentProgram) programSection.add(programHealthHomeSelectItem);
        programSection.addAll(
                Arrays.asList(
                        programNameSelectItem,
                        programLevelConsentStatusSelectItem,
                        enrollmentStatusSelectItem
                )
        );

        FormItem[] orgSection = new FormItem[]{ organizationSectionItem, organizationSelectItem};
        FormItem[] insuranceSection = new FormItem[]{insuranceSectionItem, medicaidItem};

        //concat sections with row spacer in between
        List<FormItem> items = new ArrayList<FormItem>();
        for(FormItem[] section: Arrays.asList(
                demographicSection.toArray(new FormItem[demographicSection.size()]),
                addressSection,
                careteamSection.toArray(new FormItem[careteamSection.size()]),
                programSection.toArray(new FormItem[programSection.size()]),
                insuranceSection,
                orgSection)){
            items.addAll(Arrays.asList(section));
            items.add(rowSpacerItem);
        }

        this.setFields(items.toArray(new FormItem[items.size()]));
    }

    private TextItem buildPhoneTextItem() {
        TextItem theTextItem = new TextItem("phone", "Telephone #");

        theTextItem.setValidateOnExit(false);
        theTextItem.setWidth(100);
        theTextItem.setMask("###-###-####");

        LengthRangeValidator workPhoneValidator = new LengthRangeValidator();
        workPhoneValidator.setMax(10);
        workPhoneValidator.setMin(10);
        workPhoneValidator.setErrorMessage("Invalid lengthfparentProgramId");
        theTextItem.setValidators(workPhoneValidator);

        return theTextItem;
    }

    private TextItem buildZipCodeTextItem() {
        TextItem theTextItem = new TextItem("zipCode", "Zip Code");

        String zipCodeMask = "#####";
        theTextItem.setMask(zipCodeMask);
        LengthRangeValidator zipCodeValidator = new LengthRangeValidator();
        zipCodeValidator.setMax(5);
        zipCodeValidator.setMin(5);
        zipCodeValidator.setErrorMessage("Invalid length");
        theTextItem.setValidators(zipCodeValidator);

        return theTextItem;
    }

    private SectionItem buildSectionItem(String title) {
        SectionItem sectionItem = new SectionItem();
        sectionItem.setDefaultValue(title);  // the title of section
        sectionItem.setSectionExpanded(true);

        return sectionItem;
    }

    private DateItem buildDateItem(String id, String title) {
        DateItem tempDateItem = new DateItem(id);

        tempDateItem.setTitle(title);
        tempDateItem.setEnforceDate(true);
        tempDateItem.setDateFormatter(DateDisplayFormat.TOUSSHORTDATE);
        tempDateItem.setUseTextField(true);
        tempDateItem.setTextAlign(Alignment.CENTER);
        tempDateItem.setUseMask(true);
        tempDateItem.setStartDate(dateFormatter.parse("01/01/1900"));

        tempDateItem.setWidth(100);

        // add validation rules
        DateRangeValidator dateRangeValidator = new DateRangeValidator();
        Date today = new java.util.Date();
        dateRangeValidator.setMax(today);

        dateRangeValidator.setErrorMessage("Date must be today's date or prior.");
        tempDateItem.setValidators(dateRangeValidator);
        tempDateItem.setValidateOnChange(true);

        return tempDateItem;
    }

    protected String getDateOfBirthString(Date dateOfBirth) {
        String result = dateFormatter.format(dateOfBirth);

        return result;
    }

    public SearchCriteria getSearchCriteria() {
        SearchCriteria searchCriteria = new SearchCriteria();

        searchCriteria.setLastName(StringUtils.trim(lastNameTextItem.getValueAsString()));
        searchCriteria.setFirstName(StringUtils.trim(firstNameTextItem.getValueAsString()));
        searchCriteria.setMiddleName(StringUtils.trim(middleNameTextItem.getValueAsString()));

        Date dateOfBirth = dateOfBirthDateItem.getValueAsDate();
        if (dateOfBirth != null) {
            String dateOfBirthStr = getDateOfBirthString(dateOfBirth);
            searchCriteria.setDateOfBirthStr(dateOfBirthStr);
        }

        searchCriteria.setGender(genderSelectItem.getValueAsString());
        if (requireChh){
        searchCriteria.setMinor(minorSelectItem.getValueAsString());
        }

        searchCriteria.setAddress1(StringUtils.trim(address1TextItem.getValueAsString()));
        searchCriteria.setAddress2(StringUtils.trim(address2TextItem.getValueAsString()));
        searchCriteria.setCity(StringUtils.trim(cityTextItem.getValueAsString()));
        searchCriteria.setState(StringUtils.trim(stateSelectItem.getValueAsString()));
        searchCriteria.setZipCode(StringUtils.trim(zipCodeTextItem.getValueAsString()));
        searchCriteria.setPhone(StringUtils.trim(phoneTextItem.getValueAsString()));

        String careTeamIdStr = careTeamSelectItem.getValueAsString();

        if (StringUtils.isNotBlank(careTeamIdStr)) {
            careTeamIdStr = careTeamIdStr.split(Constants.DELIMITER_SELECT_ITEM)[1];
            searchCriteria.setCareteamId(careTeamIdStr);
        }

        // In/Out of Network Providers
        String providerTypeStr = providerTypeComboBoxItem.getValueAsString();
        if (StringUtils.isNotBlank(providerTypeStr)) {
            searchCriteria.setProviderType(providerTypeStr);

        }

        String providerStr = providerNamesComboBoxItem.getValueAsString();
        if (StringUtils.isNotBlank(providerStr)) {
            // convert from delimited format
            String str = providerStr;
            providerStr = providerStr.split(Constants.DELIMITER_SELECT_ITEM)[1];
            searchCriteria.setNetworkProviderId(providerStr);

        }

        // get organization info
        String organizationIdStr = organizationSelectItem.getValueAsString();

        if (StringUtils.isNotBlank(organizationIdStr)) {
            organizationIdStr = organizationIdStr.split(Constants.DELIMITER_SELECT_ITEM)[1];
        }

        if (!StringUtils.isBlank(organizationIdStr)) {
            long organizationId = Long.parseLong(organizationIdStr);
            searchCriteria.setOrganizationId(organizationId);

        }

        searchCriteria.setSsn(StringUtils.trim(SSNItem.getValueAsString()));
        searchCriteria.setMediCaidCareId(StringUtils.trim(medicaidItem.getValueAsString()));

        // patientIDItem
        searchCriteria.setPatientID(patientIDItem.getValueAsString());

        // patient acive/inactive
        String isPatientActive = patientUserActiveSelectItem.getValueAsString();

        if (!StringUtils.isBlank(isPatientActive)) {
            searchCriteria.setActivePatient(isPatientActive);
        }

        //program
        if(requireParentProgram){
            String parentProgramId = programHealthHomeSelectItem.getValueAsString();
            if(StringUtils.isNotBlank(parentProgramId))
                searchCriteria.setHealthHome(parentProgramId);
        }

        String subordinateProgramId = programNameSelectItem.getValueAsString();
        if (!StringUtils.isBlank(subordinateProgramId))
            searchCriteria.setProgramNameId(subordinateProgramId);

        String consentValue = programLevelConsentStatusSelectItem.getValueAsString();
        if (!StringUtils.isBlank(consentValue))
            searchCriteria.setProgramLevelConsentStatus(consentValue);


        String programStatusId = enrollmentStatusSelectItem.getValueAsString();
        if (!StringUtils.isBlank(programStatusId))
            searchCriteria.setEnrollmentStatus(programStatusId);


        return searchCriteria;
    }

    @Override
    public boolean validate() {
        boolean valid = super.validate();

//        boolean atLeastOneSearchCriteriaIsEntered = checkForData();
//
//
//        if (!atLeastOneSearchCriteriaIsEntered) {
//            SC.warn("You must enter at least one search criteria");
//        }
        return valid;
    }

    /**
     * Check if a form has data
     *
     * @return
     */
    private boolean checkForData() {

        boolean hasData = false;

        FormItem[] formItems = getFields();
        for (FormItem tempFormItem : formItems) {

            // skip section items
            if (tempFormItem instanceof SectionItem) {
                continue;
            }

            Object value = tempFormItem.getValue();
            if (value != null) {
                hasData = true;
                break;
            }
        }

        return hasData;
    }
    
    public void setMinorValues(String[] valueMap) {
        minorSelectItem.setValueMap(valueMap);
        }
    
     public void clearMinor() {
        minorSelectItem.clearValue();
        minorSelectItem.setDisabled(false);
    }

    public TextItem getLastNameTextItem() {
        return lastNameTextItem;
    }

    public SelectItem getProgramNameSelectItem() {
        return programNameSelectItem;
    }


    /**
     * Loads the organization names
     */
    private void loadOrganizationNames(long communityId) {
        orgService.getOrganizationNamesForSearchForm(communityId, new GetOrganizationNamesCallback(organizationSelectItem));
    }

    public void loadCareTeamNames() {
        GetCareteamNamesCallback getCareteamNamesCallback = 
                new GetCareteamNamesCallback(careTeamSelectItem, communityId);
        getCareteamNamesCallback.attempt();
    }

    public void loadOutProviderNames() {
        providerService.getProvidersByCommunityId(ApplicationContextUtils.getLoginResult().getCommunityId(), new ProvidersCallback());

    }

    public void loadInProviderNames() {
        adminService.getUsers(communityId, new UsersCallback());

    }

    class SearchKeyPressHandler implements KeyPressHandler {

        @Override
        public void onKeyPress(KeyPressEvent event) {
            if (event.getKeyName().equals(Constants.ENTER_KEY)) {
                searchClickHandler.onClick(null);
            }
        }
    }

    public void clearSearchValues() {

        lastNameTextItem.clearValue();
        firstNameTextItem.clearValue();
        middleNameTextItem.clearValue();
        dateOfBirthDateItem.clearValue();
        genderSelectItem.clearValue();
        address1TextItem.clearValue();
        address2TextItem.clearValue();
        cityTextItem.clearValue();
        zipCodeTextItem.clearValue();
        stateSelectItem.clearValue();
        phoneTextItem.clearValue();
        careTeamSelectItem.clearValue();
        organizationSelectItem.clearValue();
        SSNItem.clearValue();
        medicaidItem.clearValue();
        insuranceSectionItem.clearValue();
        patientIDItem.clearValue();
        providerTypeComboBoxItem.clearValue();
        providerNamesComboBoxItem.clearValue();
        providerNamesComboBoxItem.setDisabled(true);
        providerNamesComboBoxItem.setRequired(false);
        if (requireChh){
        minorSelectItem.clearValue();
        }

        programNameSelectItem.clearValue();
        programNameSelectItem.setValueMap(subProgramFullMap);
        if(requireParentProgram){
            programHealthHomeSelectItem.clearValue();
            programHealthHomeSelectItem.setValueMap(parentProgramFullMap);
        }
        enrollmentStatusSelectItem.clearValue();
        enrollmentStatusSelectItem.setValueMap(statusFullMap);
        programLevelConsentStatusSelectItem.clearValue();
        programLevelConsentStatusSelectItem.setValueMap(consentFullMap);

        if (patientEngagementEnable) {
            patientUserActiveSelectItem.clearValue();
        }

    }

    private class ProvidersCallback implements AsyncCallback<List<NonHealthHomeProviderDTO>> {

        public void onFailure(Throwable thrwbl) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        public void onSuccess(List<NonHealthHomeProviderDTO> t) {

            providerMap = new LinkedHashMap<String, String>();
             
                providerMap = buildUsersMap(t);

            providerNamesComboBoxItem.setValueMap(providerMap);
            providerNamesComboBoxItem.clearValue();
             
        }
    }

    protected LinkedHashMap<String, String> buildUsersMap(List<NonHealthHomeProviderDTO> users) {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

        for (NonHealthHomeProviderDTO tempUser : users) {
            String userId = String.valueOf(tempUser.getProviderId());
            String label = tempUser.getLastName() + " " + tempUser.getFirstName();
            data.put(Constants.DELIMITER_SELECT_ITEM + userId, label.toString());
        }

        return data;
    }

    private class UsersCallback implements AsyncCallback<List<UserDTO>> {

        public void onFailure(Throwable thrwbl) {
            //throw new UnsupportedOperationException("Not supported yet.");
        }

        public void onSuccess(List<UserDTO> t) {

            UserMap = new LinkedHashMap<String, String>();

            for (UserDTO dTO : t) {
                if(dTO.getStatus().equals("ACTIVE")){
                    String completeName = dTO.getLastName() + "," + dTO.getFirstName();
                    String userId = Constants.DELIMITER_SELECT_ITEM + dTO.getUserId().toString();
                    UserMap.put(userId, completeName);
                }
            }

            providerNamesComboBoxItem.setValueMap(UserMap);
            providerNamesComboBoxItem.clearValue();
        }
    }

    class providerTypeChangeHandler implements ChangedHandler {

        public void onChanged(ChangedEvent event) {
            String value = event.getValue().toString();
            if (StringUtils.isNotBlank(value)) {
                providerNamesComboBoxItem.setDisabled(false);
                providerNamesComboBoxItem.setRequired(true);
                if (StringUtils.equalsIgnoreCase(value, IN_PROVIDERS)) {
                    if(UserMap!=null){
                        providerNamesComboBoxItem.setValueMap(UserMap);
                        providerNamesComboBoxItem.clearValue();
                    }else{
                        providerNamesComboBoxItem.setValueMap("");
                        providerNamesComboBoxItem.clearValue();
                        loadInProviderNames();
                    }
                } else if (StringUtils.equalsIgnoreCase(value, OUT_PROVIDERS)) {
                    if(providerMap!=null){
                        providerNamesComboBoxItem.setValueMap(providerMap);
                        providerNamesComboBoxItem.clearValue();
                    }else{
                        providerNamesComboBoxItem.setValueMap("");
                        providerNamesComboBoxItem.clearValue();
                        loadOutProviderNames();
                    }
                }
            } else {
                providerNamesComboBoxItem.setDisabled(true);
                providerNamesComboBoxItem.setRequired(false);
                providerNamesComboBoxItem.clearValue();
            }
        }
    }

    class ProgramLevelConsentStatusChangedHandler implements ChangedHandler {

        public void onChanged(ChangedEvent event) {
        }
    }

    private void setupConsentMaps() {
        consentFullMap = new LinkedHashMap<Integer, String>();
        for(Map.Entry<Integer,String> e: programLevelConsentStatusMapCodes.entrySet()){
            consentFullMap.put(e.getKey(),e.getValue());
        }
        consentFullMap.put(Constants.PROGRAM_CONSENT_NOT_REQUIRED, Constants.PROGRAM_CONSENT_NOT_REQUIRED_TEXT);

        consentNotRequiredMap= new LinkedHashMap<Integer, String>();
        consentNotRequiredMap.put(Constants.PROGRAM_CONSENT_NOT_REQUIRED, Constants.PROGRAM_CONSENT_NOT_REQUIRED_TEXT);
    }

    private class GetSubordinateProgramCallback extends GetProgramCallback {

        boolean doCache = false;

        public GetSubordinateProgramCallback(){
            super(programNameSelectItem);
        }

        public GetSubordinateProgramCallback(boolean doCache){
            this();
            this.doCache = doCache;
        }

        @Override
        public void onFailure(Throwable throwable) {
            super.onFailure(throwable);
        }
        @Override
        public void onSuccess(List<ProgramDTO> programDTOS) {
            super.onSuccess(programDTOS);
            if(doCache) subProgramFullMap = this.valueMap;
        }
    }



    private class GetParentProgramCallback extends GetProgramCallback {

        boolean doCache = false;

        public GetParentProgramCallback(){
            super(programHealthHomeSelectItem);
        }

        public GetParentProgramCallback(boolean doCache){
            this();
            this.doCache = doCache;
        }

        @Override
        public void onFailure(Throwable throwable) {
            super.onFailure(throwable);
        }
        @Override
        public void onSuccess(List<ProgramDTO> programDTOS) {
            super.onSuccess(programDTOS);
            if(doCache) parentProgramFullMap = this.valueMap;
        }
    }

    private class GetSearchFilterCallback implements AsyncCallback<ProgramSearchOptionsDTO>{

        private boolean byParent;

        GetSearchFilterCallback(boolean byParent){
            this.byParent = byParent;
        }

        @Override
        public void onFailure(Throwable throwable) {
            //TODO
        }

        @Override
        public void onSuccess(ProgramSearchOptionsDTO programSearchOptionsDTO) {

            //subordinate
            if(byParent){
                programNameSelectItem.setValueMap("");
                LinkedHashMap<Long, String> subMap = ManagePatientProgramUtils.convertToMap(programSearchOptionsDTO.getSubordinatePrograms());
                if(subMap!=null)programNameSelectItem.setValueMap(subMap);
                programNameSelectItem.clearValue();
            }

            //consent
            programLevelConsentStatusSelectItem.setValueMap("");
            programLevelConsentStatusSelectItem.clearValue();

            Boolean consentRequired = programSearchOptionsDTO.isConsentRequired();
            if(consentRequired != null && consentRequired){
                programLevelConsentStatusSelectItem.setValueMap(programLevelConsentStatusMapCodes);
            }else if (consentRequired != null && !consentRequired){
                programLevelConsentStatusSelectItem.setValueMap(consentNotRequiredMap);
            }

            //status
            enrollmentStatusSelectItem.setValueMap("");
            enrollmentStatusSelectItem.clearValue();

            if(programSearchOptionsDTO.isSimpleSubordinate()){
                enrollmentStatusSelectItem.setValueMap(systemStatusMap);
            }else if(programSearchOptionsDTO.getStatuses()!=null){
                enrollmentStatusSelectItem.setValueMap(ManagePatientProgramUtils.convertStatusToMap(programSearchOptionsDTO.getStatuses()));
            }
        }
    }

    private class GetProgramStatusCallback implements AsyncCallback<List<ProgramStatusDTO>> {

        private SelectItem selectItem;
        private boolean doCache;

        public GetProgramStatusCallback(SelectItem selectItem){
            this.selectItem =selectItem;
        }
        public GetProgramStatusCallback(SelectItem selectItem, boolean doCache){
            this(selectItem);
            this.doCache = doCache;
        }

        @Override
        public void onFailure(Throwable throwable) {
            //todo
        }

        @Override
        public void onSuccess(List<ProgramStatusDTO> programStatusDTOS) {
            LinkedHashMap<Long, String> valueMap = ManagePatientProgramUtils.convertStatusToMap(programStatusDTOS);
            if(valueMap!=null) selectItem.setValueMap(valueMap);
            if(doCache)statusFullMap = valueMap;
        }
    }

}
