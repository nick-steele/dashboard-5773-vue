package com.gsihealth.dashboard.client.careteam;

import com.gsihealth.dashboard.client.common.CareTeamsLoadable;
import com.smartgwt.client.widgets.layout.VLayout;

/**
 *
 * @author Chad Darby
 */
public abstract class BasePatientForm extends VLayout implements CareTeamsLoadable {

    public BasePatientForm() {
        
    }
    
}
