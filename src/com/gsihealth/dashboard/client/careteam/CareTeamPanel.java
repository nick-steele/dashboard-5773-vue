package com.gsihealth.dashboard.client.careteam;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.smartgwt.client.types.Overflow;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

/**
 *
 * @author Chad Darby
 */
public class CareTeamPanel extends VLayout {

    private static int MAIN_CONTENT_POSITION = 1;

    public CareTeamPanel() {
        buildGui();
        DashBoardApp.restartTimer();
    }

    private void buildGui() {

        ToolStrip toolStrip = createToolStrip();
        addMember(toolStrip);

        addMember(new PatientSearchPanel());
        this.setOverflow(Overflow.AUTO);

        setPadding(5);
    }

    protected ToolStrip createToolStrip() {
        ToolStripButton searchPatientButton = new ToolStripButton("Search Patient");

        ToolStrip toolStrip = new ToolStrip();
        toolStrip.setWidth100();
        toolStrip.addButton(searchPatientButton);

        searchPatientButton.addClickHandler(new SearchPatientClickHandler());

        return toolStrip;
    }

    public void showViewPatientPanel(PersonDTO thePerson) {
        removeMainContent();

        addMember(new CareTeamViewPatientPanel(thePerson));
    }

    public void showPatientSearchPanel() {
        removeMainContent();

        addMember(new PatientSearchPanel());
    }

    private void removeMainContent() {

        Canvas[] members = getMembers();

        if (members.length > 1) {
            Canvas mainContent = members[MAIN_CONTENT_POSITION];
            this.removeMember(mainContent);
        }
    }

    class SearchPatientClickHandler implements ClickHandler {

        public void onClick(ClickEvent event) {
            removeMainContent();
            showPatientSearchPanel();
        }
    }
}
