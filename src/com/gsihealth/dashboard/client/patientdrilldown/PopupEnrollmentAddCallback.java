package com.gsihealth.dashboard.client.patientdrilldown;

import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.common.DashboardTitles;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.gsihealth.dashboard.client.enrollment.EnrollmentPanel;
import com.gsihealth.dashboard.client.common.PersonInfo;
import com.gsihealth.dashboard.redux.Redux;

/**
 * Created by thnguyen on 7/21/2017.
 */

public class PopupEnrollmentAddCallback extends PortalAsyncCallback<PersonInfo> {
    @Override
    public void onSuccess(PersonInfo patientInfo) {
        EnrollmentPanel enrollment = new EnrollmentPanel();

        enrollment.showAddPatientPanel(patientInfo);
        DashBoardApp.showPanel("appEnrollment", enrollment, DashboardTitles.ENROLLMENT);
        Redux.jsEvent("local:openAppFromGWT:appEnrollment");
    }
}
