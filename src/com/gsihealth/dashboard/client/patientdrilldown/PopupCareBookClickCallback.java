package com.gsihealth.dashboard.client.patientdrilldown;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Frame;
import com.google.gwt.user.client.ui.RootPanel;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.gsihealth.dashboard.client.service.EnrollmentService;
import com.gsihealth.dashboard.client.service.EnrollmentServiceAsync;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.redux.Redux;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;

import java.util.Date;

/**
 * Created by lanhnguyen on 13/09/2016.
 */
public class PopupCareBookClickCallback extends PortalAsyncCallback<SearchResults<PersonDTO>> {

    private final EnrollmentServiceAsync enrollmentService = GWT.create(EnrollmentService.class);

    @Override
    public void onSuccess(SearchResults<PersonDTO> personDTOSearchResults) {
        if (personDTOSearchResults != null && !personDTOSearchResults.getData().isEmpty()){
            PersonDTO thePerson = personDTOSearchResults.getData().get(0);
            openCarebook(thePerson);
        } else {
            SC.warn("You do not have access to this patient");
        }
    }

    private void openCarebook(final PersonDTO thePerson) {
        final LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        final String tokenId = loginResult.getToken();

        String careBookUrl = ApplicationContextUtils.getCarebookURL();

        careBookUrl += "?patientId=" + thePerson.getPatientEnrollment().getPatientId() +
                "&userId=" + loginResult.getEmail() + "&communityId=" + loginResult.getCommunityId() +
                "&tokenId=" + tokenId;

        final String features = "location=no,status=yes,scrollbars=yes,width=1200,height=800,resizable=yes";

        /*
         * Internet Explorer does not like spaces in window name when calling window.open(), therefore removed spaces from name.
         */
        final String name = "CareBook";
        // now show the panel
        if (thePerson.isSelfAssertEligible()) {
            final ClientApplicationProperties props = ApplicationContextUtils.getClientApplicationProperties();
            if (ApplicationContextUtils.getClientApplicationProperties().isPatientSelfAssertedConsentAccess()) {
                SC.confirm("Warning", props.getPatientSelfAssertWarningText(), new BooleanCallback() {
                    @Override
                    public void execute(Boolean value) {
                        if (value.booleanValue()) {

                            final String consentLanguage = props.getConsentLanguage();
                            SC.ask("Consent Confirmation", consentLanguage, new BooleanCallback() {
                                public void execute(Boolean value) {
                                    if (value.booleanValue()) {
                                        thePerson.getPatientEnrollment().setSelfAssertedConsentDate(new Date());
                                        enrollmentService.updatePatientBySelfAssertion(thePerson, new AsyncCallback<PersonDTO>() {
                                            @Override
                                            public void onSuccess(PersonDTO updatedPersonDtoAfterSelfAssertion) {

                                                String careBookUrlUpdatedUser = ApplicationContextUtils.getCarebookURL();
                                                careBookUrlUpdatedUser += "?patientId=" +
                                                        thePerson.getPatientEnrollment().getPatientId() +
                                                        "&userId=" + loginResult.getEmail() +
                                                        "&communityId=" + loginResult.getCommunityId() +
                                                        "&tokenId=" + tokenId;
                                                // RootPanel mainPanel = RootPanel.get("appCareBookSearch");
                                                // mainPanel.clear();
                                                // Frame frame = new Frame(careBookUrlUpdatedUser);
                                                // mainPanel.add(frame);
                                                // Redux.jsEvent("local:openApp:appCareBookSearch");
                                                Redux.jsEvent("local:appCareBookSearch:toCareBookByUrl", careBookUrlUpdatedUser);
                                            }

                                            @Override
                                            public void onFailure(Throwable cause) {
                                                SC.say("Updation failed");
                                            }
                                        });
                                    }
                                }
                            });

                        } else {
                            SC.say("Do nothing");
                        }
                    }

                });
            } else {
                SC.say("This patient has not yet granted access to your organization.  Please contact your administrator.");
            }
        } else {
            //com.google.gwt.user.client.Window.open(careBookUrl, name, features);

            // RootPanel mainPanel = RootPanel.get("appCareBookSearch");
            // mainPanel.clear();
            // Frame frame = new Frame(careBookUrl);
            // mainPanel.add(frame);
            // Redux.jsEvent("local:openApp:appCareBookSearch");

            Redux.jsEvent("local:appCareBookSearch:toCareBookByUrl", careBookUrl);
        }
    }
}
