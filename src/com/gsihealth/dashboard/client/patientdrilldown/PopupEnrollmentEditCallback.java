package com.gsihealth.dashboard.client.patientdrilldown;

import com.google.gwt.user.client.ui.RootPanel;
import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.common.DashboardTitles;
import com.gsihealth.dashboard.client.common.PortalAsyncCallback;
import com.gsihealth.dashboard.client.enrollment.EnrollmentPanel;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.redux.Redux;
import com.smartgwt.client.util.SC;

/**
 * @author  lanhnguyen on 13/09/2016.
 */
public class PopupEnrollmentEditCallback extends PortalAsyncCallback<SearchResults<PersonDTO>> {
    @Override
    public void onSuccess(SearchResults<PersonDTO> personDTOSearchResults) {
        if (personDTOSearchResults != null && !personDTOSearchResults.getData().isEmpty()){
            PersonDTO thePerson = personDTOSearchResults.getData().get(0);
            EnrollmentPanel enrollment = new EnrollmentPanel();
            enrollment.showModifyPatientPanel(thePerson);
            DashBoardApp.showPanel("appEnrollment", enrollment, DashboardTitles.ENROLLMENT);
            Redux.jsEvent("local:openAppFromGWT:appEnrollment");
        } else {
            SC.warn("You do not have access to this patient");
        }

    }
}
