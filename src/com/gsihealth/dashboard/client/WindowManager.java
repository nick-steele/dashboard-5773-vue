package com.gsihealth.dashboard.client;

import com.google.gwt.core.client.GWT;
import com.gsihealth.dashboard.client.common.DashboardTitles;
import com.gsihealth.dashboard.client.common.ProgressBarWindow;
import com.gsihealth.dashboard.client.dashboard.DashboardPanel;
import com.gsihealth.dashboard.client.patientsearch.PatientSearchCallback;
import com.gsihealth.dashboard.client.service.*;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.client.util.Constants;
import com.gsihealth.dashboard.common.AccessLevelConstants;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.events.CloseClickHandler;
import com.smartgwt.client.widgets.tab.events.TabCloseClickEvent;

/**
 * Collection of tabs for the application. Manages adding and removing tabs.
 *
 * @author Vishal (Off.)
 *
 */
public class WindowManager extends VLayout {

    private static TabSet tabSet;
    private final CareteamServiceAsync careteamService = GWT.create(CareteamService.class);
    private final EnrollmentServiceAsync enrollmentService= GWT.create(EnrollmentService.class);
    private ProgressBarWindow progressBarWindow;
    private long userId;
    private Long userOrgId;
    private long communityId;
    private PatientSearchCallback patientSearchCallback;
    private final SecurityServiceAsync securityService = GWT.create(SecurityService.class);
    private int currentPageNumber;
    private SearchCriteria searchCriteria = null;
    private long userAccessLevelId;

    public WindowManager() {
        buildGUI();
    }

    public void buildGUI() {

        tabSet = new TabSet();
        tabSet.setWidth100();
//        Tab dashboardTab = new Tab(DashboardTitles.HOME);
        DashboardPanel dashboardPanel = new DashboardPanel();

        // TODO: Deprecated
//        dashboardTab.setPane(dashboardPanel);
//        tabSet.addTab(dashboardTab);

        /*
         * store DashboardPanel
         */
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        applicationContext.putAttribute(Constants.DASHBOARD_PANEL_KEY, dashboardPanel);

        // TODO: Deprecated
//        tabSet.addCloseClickHandler(new TabCloseClickHandler());
        addMember(tabSet);

        /*
         * Allowing all users (Super User also) to access Carebook
         */
        boolean patientSearchEnabled = ApplicationContextUtils.getClientApplicationProperties().isPatientSearchEnabled();

        if (patientSearchEnabled) {
            initSearchConfig();

            tabSet.setVisible(true);
        } else {
            tabSet.setVisible(true);
        }

    }

    private void initSearchConfig() {
        searchCriteria = new SearchCriteria();
        progressBarWindow = new ProgressBarWindow("Loading", "Loading...");
        patientSearchCallback = new PatientSearchCallback(searchCriteria, progressBarWindow , WindowManager.this);

        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        userId = loginResult.getUserId();
        userOrgId=loginResult.getOrganizationId();
        // For power user user organization Id will be used as null as per
        // existing code in Dashboard.
        userAccessLevelId = loginResult.getAccessLevelId();
        if (userAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
            userOrgId = null;
        }
        communityId = loginResult.getCommunityId();
    }



    /**
     *
     * @param pageNumber
     * @param searchCriteria
     */
    public void findDashboardPatientSearch(int pageNumber, long communityId, long userId, final SearchCriteria searchCriteria) {

        currentPageNumber = pageNumber;
        progressBarWindow.show();
        // FIXME this uses a completely diff search method that makes the baby jesus cry, make it use the existing one
//        careteamService.findDashboardPatient(communityId, userId, searchCriteria, patientSearchCallback);
        
    //    JIRA - 1193 --Satyendra---
   //    Now it will use the existing search functionality for dashboard patient search.   
        enrollmentService.findPatients(communityId, searchCriteria, false, userOrgId, -1, -1, userId, patientSearchCallback);
        
    }

    public static void addWindow(String tabName, Canvas panel) {
        Tab currentTab = null;

        if (tabExists(tabSet, tabName)) {
            currentTab = getTab(tabSet, tabName);
        } else {
            currentTab = new Tab(tabName);
            currentTab.setPane(panel);

            currentTab.setCanClose(true);
            tabSet.addTab(currentTab);

            ApplicationContext context = ApplicationContext.getInstance();
            context.putAttribute(tabName, panel);
        }

        tabSet.selectTab(currentTab);
    }

    public static void updateWindow(String tabName, Canvas panel) {
        Tab currentTab = null;

        if (tabExists(tabSet, tabName)) {
            
            // just update the contents of the tab
            currentTab = getTab(tabSet, tabName);
            
            currentTab.setPane(panel);            

            ApplicationContext context = ApplicationContext.getInstance();
            context.putAttribute(tabName, panel);
        } else {
            // create a new tab
            currentTab = new Tab(tabName);
            currentTab.setPane(panel);

            currentTab.setCanClose(true);
            tabSet.addTab(currentTab);

            ApplicationContext context = ApplicationContext.getInstance();
            context.putAttribute(tabName, panel);
        }

        tabSet.selectTab(currentTab);
    }
    
     public static void closeTab(String tabName) {
        Tab currentTab = null;

        if (tabExists(tabSet, tabName)) {
            currentTab = getTab(tabSet, tabName);         
            tabSet.removeTab(currentTab);
           
        }
     }
    /**
     * Checks to see if the tabName exists. Returns true if found, else false
     *
     * @param theTabSet
     * @param tabName
     * @return
     */
    private static boolean tabExists(TabSet theTabSet, String tabName) {
        Tab[] tabs = theTabSet.getTabs();

        boolean exists = false;

        for (Tab tempTab : tabs) {
            if (tempTab.getTitle().equals(tabName)) {
                exists = true;
                break;
            }
        }

        return exists;
    }

    /**
     * Returns reference to tabName
     *
     * @param theTabSet
     * @param tabName
     * @return
     */
    private static Tab getTab(TabSet theTabSet, String tabName) {
        Tab[] tabs = theTabSet.getTabs();

        Tab targetTab = null;

        for (Tab tempTab : tabs) {
            if (tempTab.getTitle().equals(tabName)) {
                targetTab = tempTab;
                break;
            }
        }

        return targetTab;
    }

    class TabCloseClickHandler implements CloseClickHandler {

        public void onCloseClick(TabCloseClickEvent event) {
            Tab theTab = event.getTab();
            String tabTitle = theTab.getTitle();

            if (tabTitle.equals(DashboardTitles.CARE_TEAM_ASSIGNMENT)) {
                ApplicationContext context = ApplicationContext.getInstance();

                context.removeAttribute(Constants.VIEW_PATIENT_FORM_KEY);
                context.removeAttribute(Constants.PATIENT_SEARCH_FORM_KEY);
            }
        }
    }
    
    public void reloadScreenWithSearchCriteria(){
        findDashboardPatientSearch(1, communityId, userId, searchCriteria);
    }

}
