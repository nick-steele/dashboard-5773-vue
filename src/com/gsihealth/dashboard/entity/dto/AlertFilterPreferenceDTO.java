/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.entity.dto;

/**
 *
 * @author User
 */
public class AlertFilterPreferenceDTO  implements java.io.Serializable{
 
    private Long alertId;
    private String alertSeverity;
    private String alertName;
    private boolean checkValue;
    private String status;
    private Long applicationID;
    private Long userID;
    private Integer showAlert;
    private Long applicationAlertID;
    private Integer communityID;
    private boolean isRoleBased;
    private boolean noFiltering;

    /**
     * @return the alertId
     */
    public Long getAlertId() {
        return alertId;
    }

    /**
     * @param alertId the alertId to set
     */
    public void setAlertId(Long alertId) {
        this.alertId = alertId;
    }

    /**
     * @return the alertSeverity
     */
    public String getAlertSeverity() {
        return alertSeverity;
    }

    /**
     * @param alertSeverity the alertSeverity to set
     */
    public void setAlertSeverity(String alertSeverity) {
        this.alertSeverity = alertSeverity;
    }

    /**
     * @return the alertType
     */
    public String getAlertName() {
        return alertName;
    }

    /**
     * @param alertType the alertType to set
     */
    public void setAlertName(String alertType) {
        this.alertName = alertType;
    }

    /**
     * @return the checkValue
     */
    public boolean isCheckValue() {
        return checkValue;
    }

    /**
     * @param checkValue the checkValue to set
     */
    public void setCheckValue(boolean checkValue) {
        this.checkValue = checkValue;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the applicationID
     */
    public Long getApplicationID() {
        return applicationID;
    }

    /**
     * @param applicationID the applicationID to set
     */
    public void setApplicationID(Long applicationID) {
        this.applicationID = applicationID;
    }

    

    /**
     * @return the userID
     */
    public Long getUserID() {
        return userID;
    }

    /**
     * @param userID the userID to set
     */
    public void setUserID(Long userID) {
        this.userID = userID;
    }

    /**
     * @return the showAlert
     */
    public Integer getShowAlert() {
        return showAlert;
    }

    /**
     * @param showAlert the showAlert to set
     */
    public void setShowAlert(Integer showAlert) {
        this.showAlert = showAlert;
    }

    /**
     * @return the applicationAlertID
     */
    public Long getApplicationAlertID() {
        return applicationAlertID;
    }

    /**
     * @param applicationAlertID the applicationAlertID to set
     */
    public void setApplicationAlertID(Long applicationAlertID) {
        this.applicationAlertID = applicationAlertID;
    }

    /**
     * @return the communityID
     */
    public Integer getCommunityID() {
        return communityID;
    }

    /**
     * @param communityID the communityID to set
     */
    public void setCommunityID(Integer communityID) {
        this.communityID = communityID;
    }

    /**
     * @return the isRoleBased
     */
    public boolean getIsRoleBased() {
        return isRoleBased;
    }

    /**
     * @param isRoleBased the isRoleBased to set
     */
    public void setIsRoleBased(boolean isRoleBased) {
        this.isRoleBased = isRoleBased;
    }
    public boolean getNoFiltering() {
        return noFiltering;
    }

    public void setNoFiltering(boolean noFiltering) {
        this.noFiltering = noFiltering;
    }

     @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nalertName: ");
        sb.append(this.alertName);
        sb.append("\napp id: ");
        sb.append(this.applicationID);
        sb.append("\napp alert id: ");
        sb.append(this.applicationAlertID);
        sb.append("\ncheckValue ");
        sb.append(this.showAlert);
        sb.append("\nnofiltering:  ");
        sb.append(this.noFiltering);
        sb.append("\ncommunity id: ");
        sb.append(this.getCommunityID());
        sb.append("\nisRoleBased:  ");
        sb.append(this.getIsRoleBased());
        sb.append("\nseverity ");
        sb.append(this.alertSeverity);
        sb.append("\nstatus ");
        sb.append(this.getStatus());
        sb.append("\nuserid: ");
        sb.append(this.userID);
        
        return sb.toString();
    }
}
