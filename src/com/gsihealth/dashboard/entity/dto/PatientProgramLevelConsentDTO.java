package com.gsihealth.dashboard.entity.dto;

import java.io.Serializable;

/**
 *
 * @author Chad Darby
 */
public class PatientProgramLevelConsentDTO implements Serializable {

    private long communityOrgId;
    private long patientId;
    private String status;

    public PatientProgramLevelConsentDTO() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getCommunityOrgId() {
        return communityOrgId;
    }

    public void setCommunityOrgId(long communityOrgId) {
        this.communityOrgId = communityOrgId;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    
}
