/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.entity.dto;

/**
 *
 * @author User
 */
public class AlertPreferenceDTO implements java.io.Serializable{
    
    private Long applicationAlertID;
    private String alertIcon;
    private String alertName;
    private String alertDesc;
    private Long applicationID;
    private Long  alertTypeID;
    private String status;
    private boolean isRoleBase;
    private Long communityID;
    
    private long userLevelId;
    private long roleTypeId;
    private long accessLevelID;
    
   
    /**
     * @return the applicationAlertID
     */
    public Long getApplicationAlertID() {
        return applicationAlertID;
    }

    /**
     * @param applicationAlertID the applicationAlertID to set
     */
    public void setApplicationAlertID(Long applicationAlertID) {
        this.applicationAlertID = applicationAlertID;
    }
    

    /**
     * @return the alertIcon
     */
    public String getAlertIcon() {
        return alertIcon;
    }

    /**
     * @param alertIcon the alertIcon to set
     */
    public void setAlertIcon(String alertIcon) {
        this.alertIcon = alertIcon;
    }

    /**
     * @return the alertDesc
     */
    public String getAlertDesc() {
        return alertDesc;
    }

    /**
     * @param alertDesc the alertDesc to set
     */
    public void setAlertDesc(String alertDesc) {
        this.alertDesc = alertDesc;
    }

    /**
     * @return the applicationID
     */
    public Long getApplicationID() {
        return applicationID;
    }

    /**
     * @param applicationID the applicationID to set
     */
    public void setApplicationID(Long applicationID) {
        this.applicationID = applicationID;
    }

    /**
     * @return the alertTypeID
     */
    public Long getAlertTypeID() {
        return alertTypeID;
    }

    /**
     * @param alertTypeID the alertTypeID to set
     */
    public void setAlertTypeID(Long alertTypeID) {
        this.alertTypeID = alertTypeID;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    

    /**
     * @return the alertName
     */
    public String getAlertName() {
        return alertName;
    }

    /**
     * @param alertName the alertName to set
     */
    public void setAlertName(String alertName) {
        this.alertName = alertName;
    }

    /**
     * @return the isRoleBase
     */
    public boolean getIsRoleBase() {
        return isRoleBase;
    }

    /**
     * @param isRoleBase the isRoleBase to set
     */
    public void setIsRoleBase(boolean isRoleBase) {
        this.isRoleBase = isRoleBase;
    }

    /**
     * @return the communityID
     */
    public Long getCommunityID() {
        return communityID;
    }

    /**
     * @param communityID the communityID to set
     */
    public void setCommunityID(Long communityID) {
        this.communityID = communityID;
    }

    /**
     * @return the userLevelId
     */
    public long getUserLevelId() {
        return userLevelId;
    }

    /**
     * @param userLevelId the userLevelId to set
     */
    public void setUserLevelId(long userLevelId) {
        this.userLevelId = userLevelId;
    }

    /**
     * @return the roleTypeId
     */
    public long getRoleTypeId() {
        return roleTypeId;
    }

    /**
     * @param roleTypeId the roleTypeId to set
     */
    public void setRoleTypeId(long roleTypeId) {
        this.roleTypeId = roleTypeId;
    }

    
    /**
     * @return the accessLevelID
     */
    public long getAccessLevelID() {
        return accessLevelID;
    }

    /**
     * @param accessLevelID the accessLevelID to set
     */
    public void setAccessLevelID(long accessLevelID) {
        this.accessLevelID = accessLevelID;
    }
   
}
