
package com.gsihealth.dashboard.entity.dto;

/**
 *
 * @author ssingh
 */
public class PatientStatusDTO implements java.io.Serializable{
   
    private Long id;
    private String value;
    private String terminationReason;
    
     public PatientStatusDTO() {
         
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the terminationReason
     */
    public String getTerminationReason() {
        return terminationReason;
    }

    /**
     * @param terminationReason the terminationReason to set
     */
    public void setTerminationReason(String terminationReason) {
        this.terminationReason = terminationReason;
    }
     
}
