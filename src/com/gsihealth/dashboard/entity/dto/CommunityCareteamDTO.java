package com.gsihealth.dashboard.entity.dto;

import java.io.Serializable;

/**
 *
 * @author Chad Darby
 */
public class CommunityCareteamDTO implements Serializable {

    private Long communityCareteamId;

    private String name;

    public CommunityCareteamDTO() {

    }

    public Long getCommunityCareteamId() {
        return communityCareteamId;
    }

    public void setCommunityCareteamId(Long communityCareteamId) {
        this.communityCareteamId = communityCareteamId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    
}
