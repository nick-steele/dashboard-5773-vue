/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.entity.dto;

import java.io.Serializable;

/**
 *
 * @author Beth.Boose
 */
public class RoleDTO  implements Serializable {
    private Long roleId;
    private String roleName;
    private Long primaryRoleId;
    private long communityId;

    public RoleDTO() {
    }

    
    
    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Long getPrimaryRoleId() {
        return primaryRoleId;
    }

    public void setPrimaryRoleId(Long primaryRoleId) {
        this.primaryRoleId = primaryRoleId;
    }

    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }
    
    
    
}
