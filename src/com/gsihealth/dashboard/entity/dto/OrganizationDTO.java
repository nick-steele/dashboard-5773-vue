package com.gsihealth.dashboard.entity.dto;

import java.util.Date;

/**
 *
 * @author Satyendra Singh
 */
public class OrganizationDTO implements java.io.Serializable {

    private Long organizationId;
    private String OID;
    private String status;
    private String facilityId;
    private String organizationName;
    private String facilityTypeDesc;
    private String physicalStreetAddress1;
    private String physicalStreetAddress2;
    private String physicalCity;
    private String physicalState;
    private String physicalZipCode;
    private String workPhone;
    private Date effectiveDate;
    private Date endDate;
    private Long communityId;

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    
    public String getOID() {
        return OID;
    }

    public void setOID(String OID) {
        this.OID = OID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getPhysicalStreetAddress1() {
        return physicalStreetAddress1;
    }

    public void setPhysicalStreetAddress1(String physicalStreetAddress1) {
        this.physicalStreetAddress1 = physicalStreetAddress1;
    }

    public String getPhysicalStreetAddress2() {
        return physicalStreetAddress2;
    }

    public void setPhysicalStreetAddress2(String physicalStreetAddress2) {
        this.physicalStreetAddress2 = physicalStreetAddress2;
    }

    public String getPhysicalCity() {
        return physicalCity;
    }

    public void setPhysicalCity(String physicalCity) {
        this.physicalCity = physicalCity;
    }

    public String getPhysicalState() {
        return physicalState;
    }

    public void setPhysicalState(String physicalState) {
        this.physicalState = physicalState;
    }

    public String getPhysicalZipCode() {
        return physicalZipCode;
    }

    public void setPhysicalZipCode(String physicalZipCode) {
        this.physicalZipCode = physicalZipCode;
    }
         
    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }
    
    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date EffectiveDate) {
        this.effectiveDate = EffectiveDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date EndDate) {
        this.endDate = EndDate;
    }
            
    public String getFacilityTypeDesc() {
        return facilityTypeDesc;
    }

     public void setFacilityTypeDesc(String facilityTypeDesc) {
        this.facilityTypeDesc = facilityTypeDesc;
    }     
    
    public String getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(String facilityId) {
        this.facilityId = facilityId;
    }        

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    @Override
    public String toString() {
        return "OrganizationDTO{" + "organizationId=" + organizationId + ", OID=" + OID 
                + ", status=" + status + ", facilityId=" + facilityId 
                + ", organizationName=" + organizationName 
                + ", facilityType=" + facilityTypeDesc + ", physicalStreetAddress1=" 
                + physicalStreetAddress1 + ", physicalStreetAddress2=" + physicalStreetAddress2 
                + ", physicalCity=" + physicalCity + ", physicalState=" + physicalState 
                + ", physicalZipCode=" + physicalZipCode + ", workPhone=" + workPhone 
                + ", effectiveDate=" + effectiveDate + ", endDate=" + endDate 
                + ", communityId=" + communityId + '}';
    }

    
    
 }
    

    
    