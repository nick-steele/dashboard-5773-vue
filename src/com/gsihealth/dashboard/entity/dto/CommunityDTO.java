package com.gsihealth.dashboard.entity.dto;

/**
 *
 * @author Satyendra Singh
 */
public class CommunityDTO implements java.io.Serializable {

    private Long communityId;
    private String communityName;
    
    public CommunityDTO(){
        
    }
    
    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }
    
 }
    

    
    