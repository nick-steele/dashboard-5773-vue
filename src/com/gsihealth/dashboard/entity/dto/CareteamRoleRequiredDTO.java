/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.entity.dto;

import java.io.Serializable;

/**
 *
 * @author Satyendra Singh
 */
public class CareteamRoleRequiredDTO implements Serializable{
    private Long idcareteamRoleRequired;
    private Long communityId;
    private String role;
   

    /**
     * @return the communityId
     */
    public Long getCommunityId() {
        return communityId;
    }

    /**
     * @param communityId the communityId to set
     */
    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    /**
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return the idcareteamRoleRequired
     */
    public Long getIdcareteamRoleRequired() {
        return idcareteamRoleRequired;
    }

    /**
     * @param idcareteamRoleRequired the idcareteamRoleRequired to set
     */
    public void setIdcareteamRoleRequired(Long idcareteamRoleRequired) {
        this.idcareteamRoleRequired = idcareteamRoleRequired;
    }
    
    
    
}
