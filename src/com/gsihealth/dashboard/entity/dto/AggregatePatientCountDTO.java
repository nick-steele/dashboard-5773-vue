/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.entity.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 *
 * @author User
 */
public class AggregatePatientCountDTO implements IsSerializable {

    private String totalNumberCandidatesEnrollment;
    private String totalNumberAssignedPatients;
    private String totalNumberEnrolledPatients;
    private String totalNumberPatientsCompletedCarePlan;
    private String totalNumberPatientsCompletedAssessment;

    public AggregatePatientCountDTO() {
    }

    public AggregatePatientCountDTO(String totalNumberCandidatesEnrollment, String totalNumberAssignedPatients, String totalNumberEnrolledPatients, String totalNumberPatientsCompletedCarePlan, String totalNumberPatientsCompletedAssessment) {
        this.totalNumberCandidatesEnrollment = totalNumberCandidatesEnrollment;
        this.totalNumberAssignedPatients = totalNumberAssignedPatients;
        this.totalNumberEnrolledPatients = totalNumberEnrolledPatients;
        this.totalNumberPatientsCompletedCarePlan = totalNumberPatientsCompletedCarePlan;
        this.totalNumberPatientsCompletedAssessment = totalNumberPatientsCompletedAssessment;
    }

    public String getTotalNumberCandidatesEnrollment() {
        return totalNumberCandidatesEnrollment;
    }

    public void setTotalNumberCandidatesEnrollment(String totalNumberCandidatesEnrollment) {
        this.totalNumberCandidatesEnrollment = totalNumberCandidatesEnrollment;
    }

    public String getTotalNumberEnrolledPatients() {
        return totalNumberEnrolledPatients;
    }

    public void setTotalNumberEnrolledPatients(String totalNumberEnrolledPatients) {
        this.totalNumberEnrolledPatients = totalNumberEnrolledPatients;
    }

    public String getTotalNumberPatientsCompletedAssessment() {
        return totalNumberPatientsCompletedAssessment;
    }

    public void setTotalNumberPatientsCompletedAssessment(String totalNumberPatientsCompletedAssessment) {
        this.totalNumberPatientsCompletedAssessment = totalNumberPatientsCompletedAssessment;
    }

    public String getTotalNumberPatientsCompletedCarePlan() {
        return totalNumberPatientsCompletedCarePlan;
    }

    public void setTotalNumberPatientsCompletedCarePlan(String totalNumberPatientsCompletedCarePlan) {
        this.totalNumberPatientsCompletedCarePlan = totalNumberPatientsCompletedCarePlan;
    }

    public String getTotalNumberAssignedPatients() {
        return totalNumberAssignedPatients;
    }

    public void setTotalNumberAssignedPatients(String totalNumberAssignedPatients) {
        this.totalNumberAssignedPatients = totalNumberAssignedPatients;
    }
}
