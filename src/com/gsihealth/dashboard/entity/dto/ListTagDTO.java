/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.entity.dto;

import java.math.BigInteger;

/**
 *
 * @author vlewis
 */
public class ListTagDTO implements java.io.Serializable{
    private String tagName = null;
    private String tagType = null;
    private Long tagId = null;
    private String tagAbbr = null;

    /**
     * @return the tagName
     */
    public String getTagName() {
        return tagName;
    }

    /**
     * @param tagName the tagName to set
     */
    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    /**
     * @return the tagId
     */
    public Long getTagId() {
        return tagId;
    }

    /**
     * @param tagId the tagId to set
     */
    public void setTagId(Long tagId) {
        this.tagId = tagId;
    }

    /**
     * @return the tagType
     */
    public String getTagType() {
        return tagType;
    }

    /**
     * @param tagType the tagType to set
     */
    public void setTagType(String tagType) {
        this.tagType = tagType;
    }


    /**
     * @return the tagAbbr
     */
    public String getTagAbbr() {
        return tagAbbr;
    }

    /**
     * @param tagAbbr the tagAbbr to set
     */
    public void setTagAbbr(String tagAbbr) {
        this.tagAbbr = tagAbbr;
    }


}
