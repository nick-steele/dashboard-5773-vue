package com.gsihealth.dashboard.entity.dto;

/**
 *
 * @author User
 */
@Deprecated
public class ProgramHealthHomeDTO implements java.io.Serializable {

    private Long id;
    private Long communityId;
    private String healthHomeName;
    private String healthHomeMmisId;

    public ProgramHealthHomeDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the healtHomeName
     */
    public String getHealthHomeName() {
        return healthHomeName;
    }

    /**
     * @param healthHomeName the healtHomeName to set
     */
    public void setHealthHomeName(String healthHomeName) {
        this.healthHomeName = healthHomeName;
    }

    /**
     * @return the healthHomeMmisId
     */
    public String getHealthHomeMmisId() {
        return healthHomeMmisId;
    }

    /**
     * @param healthHomeMmisId the healthHomeMmisId to set
     */
    public void setHealthHomeMmisId(String healthHomeMmisId) {
        this.healthHomeMmisId = healthHomeMmisId;
    }
    
    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }
}
