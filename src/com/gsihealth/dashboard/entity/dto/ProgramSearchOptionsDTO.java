package com.gsihealth.dashboard.entity.dto;

import java.io.Serializable;
import java.util.List;

public class ProgramSearchOptionsDTO implements Serializable {
    List<ProgramDTO> subordinatePrograms;
    List<ProgramStatusDTO> statuses;
    Boolean consentRequired;
    boolean isSimpleSubordinate;

    public ProgramSearchOptionsDTO() {
    }

    public List<ProgramDTO> getSubordinatePrograms() {
        return subordinatePrograms;
    }

    public void setSubordinatePrograms(List<ProgramDTO> subordinatePrograms) {
        this.subordinatePrograms = subordinatePrograms;
    }

    public List<ProgramStatusDTO> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<ProgramStatusDTO> statuses) {
        this.statuses = statuses;
    }

    public Boolean isConsentRequired() {
        return consentRequired;
    }

    public void setConsentRequired(Boolean consentRequired) {
        this.consentRequired = consentRequired;
    }


    public boolean isSimpleSubordinate() {
        return isSimpleSubordinate;
    }

    public void setSimpleSubordinate(boolean simpleSubordinate) {
        isSimpleSubordinate = simpleSubordinate;
    }
}
