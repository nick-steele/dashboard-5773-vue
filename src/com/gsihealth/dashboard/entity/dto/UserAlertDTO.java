package com.gsihealth.dashboard.entity.dto;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

/**
 * Data transfer object
 * 
 * @author Chad Darby
 */
public class UserAlertDTO implements Serializable {
 
    private Long userAlertId;
    
    private BigInteger userId;    
    
    private Boolean isVisible;
    private Date creationDateTime;
    private Date lastUpdateDateTime;
    private Boolean processed;
    private AlertMessageDTO alertMessageId;
    private long communityId;
    private String applicationName;

    
    public UserAlertDTO() {
        
    }

    public UserAlertDTO(long theAlertId) {
        userAlertId = theAlertId;
    }
    
    public AlertMessageDTO getAlertMessageId() {
        return alertMessageId;
    }

    public void setAlertMessageId(AlertMessageDTO alertMessageId) {
        this.alertMessageId = alertMessageId;
    }

    public Date getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(Date creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    public Boolean getIsVisible() {
        return isVisible;
    }

    public void setIsVisible(Boolean isVisible) {
        this.isVisible = isVisible;
    }

    public Date getLastUpdateDateTime() {
        return lastUpdateDateTime;
    }

    public void setLastUpdateDateTime(Date lastUpdateDateTime) {
        this.lastUpdateDateTime = lastUpdateDateTime;
    }

    public Boolean getProcessed() {
        return processed;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }

    public Long getUserAlertId() {
        return userAlertId;
    }

    public void setUserAlertId(Long userAlertId) {
        this.userAlertId = userAlertId;
    }

    public BigInteger getUserId() {
        return userId;
    }

    public void setUserId(BigInteger userId) {
        this.userId = userId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }
     public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserAlertDTO other = (UserAlertDTO) obj;
        if (this.userAlertId != other.userAlertId && (this.userAlertId == null || !this.userAlertId.equals(other.userAlertId))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + (this.userAlertId != null ? this.userAlertId.hashCode() : 0);
        return hash;
    }
       
    
}
