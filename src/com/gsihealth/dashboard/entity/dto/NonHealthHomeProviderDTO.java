package com.gsihealth.dashboard.entity.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Chad Darby
 */
public class NonHealthHomeProviderDTO implements Serializable {
 
    private long providerId;
    private long patientId;

    private String title;
    private String firstName;
    private String middleInitial;
    private String lastName;
    private String credentials;
    private String streetAddress1;
    private String streetAddress2;
    private String city;
    private String state;
    private String zipCode;
    private String telephoneNumber;
    private String email;
    private String consentStatus;
    private Date consentDateTime;
    private long consentObtainedByUserId;
    private String consentObtainedByName;
    private long communityId;
    private String assignedStatus;
    
  //new field  
    private String specialty;
    private String primaryPracitionerFacility;
    private String directAddress;
    private String role;
    private String NPI;
    
    public NonHealthHomeProviderDTO() {
        
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getConsentDateTime() {
        return consentDateTime;
    }

    public void setConsentDateTime(Date consentDateTime) {
        this.consentDateTime = consentDateTime;
    }

    public long getConsentObtainedByUserId() {
        return consentObtainedByUserId;
    }

    public void setConsentObtainedByUserId(long consentObtainedByUserId) {
        this.consentObtainedByUserId = consentObtainedByUserId;
    }

    public String getConsentStatus() {
        return consentStatus;
    }

    public void setConsentStatus(String consentStatus) {
        this.consentStatus = consentStatus;
    }

    public String getCredentials() {
        return credentials;
    }

    public void setCredentials(String credentials) {
        this.credentials = credentials;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleInitial() {
        return middleInitial;
    }

    public void setMiddleInitial(String middleInitial) {
        this.middleInitial = middleInitial;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public long getProviderId() {
        return providerId;
    }

    public void setProviderId(long providerId) {
        this.providerId = providerId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStreetAddress1() {
        return streetAddress1;
    }

    public void setStreetAddress1(String streetAddress1) {
        this.streetAddress1 = streetAddress1;
    }

    public String getStreetAddress2() {
        return streetAddress2;
    }

    public void setStreetAddress2(String streetAddress2) {
        this.streetAddress2 = streetAddress2;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getConsentObtainedByName() {
        return consentObtainedByName;
    }

    public void setConsentObtainedByName(String consentObtainedByName) {
        this.consentObtainedByName = consentObtainedByName;
    }

    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }

    public String getAssignedStatus() {
        return assignedStatus;
    }

    public void setAssignedStatus(String assignedStatus) {
        this.assignedStatus = assignedStatus;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getPrimaryPracitionerFacility() {
        return primaryPracitionerFacility;
    }

    public void setPrimaryPracitionerFacility(String primaryPracitionerFacility) {
        this.primaryPracitionerFacility = primaryPracitionerFacility;
    }

    public String getDirectAddress() {
        return directAddress;
    }

    public void setDirectAddress(String directAddress) {
        this.directAddress = directAddress;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
        
    public String getNPI() {
        return NPI;
    }

    public void setNPI(String NPI) {
        this.NPI = NPI;
    }
}
