/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.entity.dto;

import java.io.Serializable;

/**
 *
 * @author Satyendra Singh
 */
public class DisenrollmentReasonsDTO implements Serializable{
    private String reason;
    private long id;
    private long communityId;

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the communityId
     */
    public long getCommunityId() {
        return communityId;
    }

    /**
     * @param communityId the communityId to set
     */
    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }
    
    
}
