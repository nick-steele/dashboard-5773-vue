package com.gsihealth.dashboard.entity.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Chad Darby
 */
public class UserDTO implements Serializable {

    private Long userId;
    private String orgid;
    private Long communityId;
    private String organization;
    private String portalUserId;
    private String euid;
    private Date effectiveDate;
    private Date endDate;
    private Date creationDate;
    private Character isDeleted;
    private Date lastModified;
    private String lastComment;
    private Character loggedIn;
    private Integer failedAccessAttempts;
    private Character accountLockedOut;
    private Character mustChangePassword;
    private Date lastPasswordChangeDate;
    private Date lastSuccessfulLoginDate;
    private Date lastLoginFailureDate;
    private Date lastUpdateDate;
    private String firstName;
    private String lastName;
    private String middleName;
    private String gender;
    private String status;
    private Date dateOfBirth;
    private String streetAddress1;
    private String streetAddress2;
    private String city;
    private String state;
    private String zipCode;
    private String workPhone;
    private String workPhoneExt;
    private String email;
    private String password;
    private String confirmPassword;
    private String securityRoleId;
    private String securityRole;
    private String accessLevel;
    private String accessLevelId;
    private Boolean isPasswordNotSet;

    private Character enabled;
    private String credentials;
    private String prefix;
    private Character eulaAccepted;
    private Date eulaDate;
    private Integer eulaId;
    private Character samhsaAccepted;
    private Date samhsaDate;
    private Integer samhsaId;

    private String npi;

    private boolean userDeactivated;

    private boolean permitConsent;
    private Character canManagePowerUser;
    private String reportingRole;
    private String oid;
    private int authyId;
    private String mobileNo;
    private String disableUserMfa;
    private boolean canConsentEnrollMinors;
    private String countryCode;

    private Long supervisorId;

    private boolean inActiveInOtherCommunities;
    private boolean found;
    private boolean isUserExistInCommunity;
    private boolean isUserActiveInCommunity;
    private boolean isUserActiveAtleastInOneOther;
    private boolean isUserExsistInOneOtherCommunity;

    public UserDTO() {
        isPasswordNotSet = false;
        permitConsent = true;
        inActiveInOtherCommunities = true;
    }

    public UserDTO(long theUserId) {
        this(theUserId, null);
    }

    public UserDTO(long theUserId, String theSecurityRole) {
        this(theUserId, theSecurityRole, null);
    }

    public UserDTO(long theUserId, String theSecurityRole, String theSecurityRoleId) {
        userId = theUserId;
        securityRole = theSecurityRole;
        securityRoleId = theSecurityRoleId;
        permitConsent = true;
        inActiveInOtherCommunities = true;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public Boolean isPasswordNotSet() {
        return isPasswordNotSet;
    }

    public void setPasswordNotSet(Boolean isPasswordNotSet) {
        this.isPasswordNotSet = isPasswordNotSet;
    }

    public Boolean isPasswordSet() {
        return !isPasswordNotSet;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSecurityRoleId() {
        return securityRoleId;
    }

    public void setSecurityRoleId(String securityRoleId) {
        this.securityRoleId = securityRoleId;
    }

    public String getSecurityRole() {
        return securityRole;
    }

    public void setSecurityRole(String securityRole) {
        this.securityRole = securityRole;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStreetAddress1() {
        return streetAddress1;
    }

    public void setStreetAddress1(String streetAddress1) {
        this.streetAddress1 = streetAddress1;
    }

    public String getStreetAddress2() {
        return streetAddress2;
    }

    public void setStreetAddress2(String streetAddress2) {
        this.streetAddress2 = streetAddress2;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Character getAccountLockedOut() {
        return accountLockedOut;
    }

    public void setAccountLockedOut(Character accountLockedOut) {
        this.accountLockedOut = accountLockedOut;
    }


    public Date getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getEuid() {
        return euid;
    }

    public void setEuid(String euid) {
        this.euid = euid;
    }

    public Integer getFailedAccessAttempts() {
        return failedAccessAttempts;
    }

    public void setFailedAccessAttempts(Integer failedAccessAttempts) {
        this.failedAccessAttempts = failedAccessAttempts;
    }

    public Character getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Character isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getLastComment() {
        return lastComment;
    }

    public void setLastComment(String lastComment) {
        this.lastComment = lastComment;
    }

    public Date getLastLoginFailureDate() {
        return lastLoginFailureDate;
    }

    public void setLastLoginFailureDate(Date lastLoginFailureDate) {
        this.lastLoginFailureDate = lastLoginFailureDate;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public Date getLastPasswordChangeDate() {
        return lastPasswordChangeDate;
    }

    public void setLastPasswordChangeDate(Date lastPasswordChangeDate) {
        this.lastPasswordChangeDate = lastPasswordChangeDate;
    }

    public Date getLastSuccessfulLoginDate() {
        return lastSuccessfulLoginDate;
    }

    public void setLastSuccessfulLoginDate(Date lastSuccessfulLoginDate) {
        this.lastSuccessfulLoginDate = lastSuccessfulLoginDate;
    }

    public Character getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(Character loggedIn) {
        this.loggedIn = loggedIn;
    }

    public Character getMustChangePassword() {
        return mustChangePassword;
    }

    public void setMustChangePassword(Character mustChangePassword) {
        this.mustChangePassword = mustChangePassword;
    }

    public String getPortalUserId() {
        return portalUserId;
    }

    public void setPortalUserId(String portalUserId) {
        this.portalUserId = portalUserId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getWorkPhoneExt() {
        return workPhoneExt;
    }

    public void setWorkPhoneExt(String workPhoneExt) {
        this.workPhoneExt = workPhoneExt;
    }

    public String getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(String accessLevel) {
        this.accessLevel = accessLevel;
    }

    public String getAccessLevelId() {
        return accessLevelId;
    }

    public void setAccessLevelId(String accessLevelId) {
        this.accessLevelId = accessLevelId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the orgid
     */
    public String getOrgid() {
        return orgid;
    }

    /**
     * @param orgid the orgid to set
     */
    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    /**
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * @param middleName the middleName to set
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the dateOfBirth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth the dateOfBirth to set
     */
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Character getEnabled() {
        return enabled;
    }

    public void setEnabled(Character enabled) {
        this.enabled = enabled;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getCredentials() {
        return credentials;
    }

    public void setCredentials(String credentials) {
        this.credentials = credentials;
    }

    public Character getEulaAccepted() {
        return eulaAccepted;
    }

    public void setEulaAccepted(Character eulaAccepted) {
        this.eulaAccepted = eulaAccepted;
    }

    public Date getEulaDate() {
        return eulaDate;
    }

    public void setEulaDate(Date eulaDate) {
        this.eulaDate = eulaDate;
    }

    public Integer getEulaId() {
        return eulaId;
    }

    public void setEulaId(Integer eulaId) {
        this.eulaId = eulaId;
    }

    public Character getSamhsaAccepted() {
        return samhsaAccepted;
    }

    public void setSamhsaAccepted(Character samhsaAccepted) {
        this.samhsaAccepted = samhsaAccepted;
    }

    public Date getSamhsaDate() {
        return samhsaDate;
    }

    public void setSamhsaDate(Date samhsaDate) {
        this.samhsaDate = samhsaDate;
    }

    public Integer getSamhsaId() {
        return samhsaId;
    }

    public void setSamhsaId(Integer samhsaId) {
        this.samhsaId = samhsaId;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public boolean isEulaAccepted() {

        boolean result = eulaAccepted != null && eulaAccepted.equals('Y');

        return result;
    }

    public boolean isSamhsaAccepted() {

        boolean result = samhsaAccepted != null && samhsaAccepted.equals('Y');

        return result;
    }

    public String getNpi() {
        return npi;
    }

    public void setNpi(String npi) {
        this.npi = npi;
    }

    public void setUserDeactivated(boolean theUserDeactivated) {
        userDeactivated = theUserDeactivated;
    }

    public boolean isUserDeactivated() {
        return userDeactivated;
    }

    public boolean hasPermitConsent() {
        return permitConsent;
    }

    public void setPermitConsent(boolean permitConsent) {
        this.permitConsent = permitConsent;
    }

    /**
     * @return the canManagePowerUser
     */
    public Character getCanManagePowerUser() {
        return canManagePowerUser;
    }

    /**
     * @param canManagePowerUser the canManagePowerUser to set
     */
    public void setCanManagePowerUser(Character canManagePowerUser) {
        this.canManagePowerUser = canManagePowerUser;
    }

    public String getReportingRole() {
        return reportingRole;
    }

    public void setReportingRole(String reportingRole) {
        this.reportingRole = reportingRole;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    /**
     * @return the authyId
     */
    public int getAuthyId() {
        return authyId;
    }

    /**
     * @param authyId the authyId to set
     */
    public void setAuthyId(int authyId) {
        this.authyId = authyId;
    }

    /**
     * @return the mobileNo
     */
    public String getMobileNo() {
        return mobileNo;
    }

    /**
     * @param mobileNo the mobileNo to set
     */
    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    /**
     * @return the disableUserMfa
     */
    public String getDisableUserMfa() {
        return disableUserMfa;
    }

    /**
     * @param disableUserMfa the disableUserMfa to set
     */
    public void setDisableUserMfa(String disableUserMfa) {
        this.disableUserMfa = disableUserMfa;
    }

    /**
     * @return the canConsentEnrollMinors
     */
    public boolean isCanConsentEnrollMinors() {
        return canConsentEnrollMinors;
    }

    /**
     * @param canConsentEnrollMinors the canConsentEnrollMinors to set
     */
    public void setCanConsentEnrollMinors(boolean canConsentEnrollMinors) {
        this.canConsentEnrollMinors = canConsentEnrollMinors;
    }

    @Override
    public String toString() {
        return "UserDTO{" + "userId=" + userId + ", orgid=" + orgid + ", communityId=" + communityId + ", organization=" + organization + ", firstName=" + firstName + ", lastName=" + lastName + ", middleName=" + middleName + ", gender=" + gender + ", status=" + status + ", dateOfBirth=" + dateOfBirth + ", city=" + city + ", state=" + state + ", zipCode=" + zipCode + ", workPhone=" + workPhone + ", email=" + email + ", securityRoleId=" + securityRoleId + ", securityRole=" + securityRole + ", accessLevel=" + accessLevel + ", accessLevelId=" + accessLevelId + '}';
    }

    /**
     * @return the countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param countryCode the countryCode to set
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Long getSupervisorId() {
        return supervisorId;
    }

    public void setSupervisorId(Long supervisorId) {
        this.supervisorId = supervisorId;
    }

    public boolean isInActiveInOtherCommunities() {
        return inActiveInOtherCommunities;
    }

    public void setInActiveInOtherCommunities(boolean inActiveInOtherCommunities) {
        this.inActiveInOtherCommunities = inActiveInOtherCommunities;
    }

    public boolean isFound() {
        return found;
    }

    public void setFound(boolean found) {
        this.found = found;
    }

    public boolean isUserExistInCommunity() {return isUserExistInCommunity; }

    public void setUserExistInCommunity(boolean userExistInCommunity) {
        isUserExistInCommunity = userExistInCommunity; }

    public boolean isUserActiveInCommunity() {return isUserActiveInCommunity; }

    public void setUserActiveInCommunity(boolean userActiveInCommunity) {isUserActiveInCommunity = userActiveInCommunity; }

    public boolean isUserActiveAtleastInOneOther() {return isUserActiveAtleastInOneOther;    }

    public void setUserActiveAtleastInOneOther(boolean userActiveAtleastInOneOther) {isUserActiveAtleastInOneOther = userActiveAtleastInOneOther;  }

    public boolean isUserExsistInOneOtherCommunity() {return isUserExsistInOneOtherCommunity;}

    public void setUserExsistInOneOtherCommunity(boolean userExsistInOneOtherCommunity) { isUserExsistInOneOtherCommunity = userExsistInOneOtherCommunity;  }
}
