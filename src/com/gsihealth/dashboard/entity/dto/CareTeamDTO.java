/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.entity.dto;

import com.gsihealth.dashboard.common.dto.PersonDTO;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author User
 */
public class CareTeamDTO implements Serializable {

    private List<UserDTO> userDTOList;
    private PersonDTO personDTO;

    public List<UserDTO> getUserDTOList() {
        return userDTOList;
    }

    public void setUserDTOList(List<UserDTO> userDTOList) {
        this.userDTOList = userDTOList;
    }

    public PersonDTO getPersonDTO() {
        return personDTO;
    }

    public void setPersonDTO(PersonDTO personDTO) {
        this.personDTO = personDTO;
    }
}
