package com.gsihealth.dashboard.entity.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Chad Darby
 */
public class OrganizationPatientConsentDTO implements Serializable {

    private long communityId;
    private long patientId;
    private long organizationId;
    private String consentStatus;
    private Date startDate;
    private Date endDate;

    // org details
    private String organizationName;
    private String city;
    private String state;
    private boolean autoConsent; 

    public OrganizationPatientConsentDTO() {
    }
    
    
    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }

    public String getConsentStatus() {
        return consentStatus;
    }

    public void setConsentStatus(String consentStatus) {
        this.consentStatus = consentStatus;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(long organizationId) {
        this.organizationId = organizationId;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isAutoConsent() {
        return autoConsent;
    }

    public void setAutoConsent(boolean autoConsent) {
        this.autoConsent = autoConsent;
    }
}
