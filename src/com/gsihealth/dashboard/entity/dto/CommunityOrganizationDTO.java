package com.gsihealth.dashboard.entity.dto;

import com.google.gwt.user.client.rpc.IsSerializable;
/**
 *
 * @author Satyendra Singh
 */
public class CommunityOrganizationDTO implements java.io.Serializable {

    private Long communityOrganizationId;
    private Long communityId;
    private Long organizationId;
    private String facilityTypeId;

    public CommunityOrganizationDTO(){
        
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public Long getCommunityOrganizationId() {
        return communityOrganizationId;
    }

    public void setCommunityOrganizationId(Long communityOrganizationId) {
        this.communityOrganizationId = communityOrganizationId;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }
        public String getFacilityTypeId() {
        return facilityTypeId;
    }

    public void setFacilityTypeId(String facilityTypeId) {
        this.facilityTypeId = facilityTypeId;
    }
 }
    

    