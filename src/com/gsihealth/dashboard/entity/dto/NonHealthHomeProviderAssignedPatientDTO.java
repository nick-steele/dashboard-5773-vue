/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.entity.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Satyendra Singh
 */
public class NonHealthHomeProviderAssignedPatientDTO implements Serializable{
    private long providerId;
    private long patientId;
    private long communityId;
    private long consentObtainedByUserId;
    private String consentStatus;
    private Date consentDateTime;
    private String roleId;
    private String roleName;
    
    public NonHealthHomeProviderAssignedPatientDTO() {
        
    }

    public long getProviderId() {
        return providerId;
    }

    public void setProviderId(long providerId) {
        this.providerId = providerId;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }

    public long getConsentObtainedByUserId() {
        return consentObtainedByUserId;
    }

    public void setConsentObtainedByUserId(long consentObtainedByUserId) {
        this.consentObtainedByUserId = consentObtainedByUserId;
    }

    public String getConsentStatus() {
        return consentStatus;
    }

    public void setConsentStatus(String consentStatus) {
        this.consentStatus = consentStatus;
    }

    public Date getConsentDateTime() {
        return consentDateTime;
    }

    public void setConsentDateTime(Date consentDateTime) {
        this.consentDateTime = consentDateTime;
    }

    /**
     * @return the roleId
     */
    public String getRoleId() {
        return roleId;
    }

    /**
     * @param roleId the roleId to set
     */
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    /**
     * @return the roleName
     */
    public String getRoleName() {
        return roleName;
    }

    /**
     * @param roleName the roleName to set
     */
    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
        
}
