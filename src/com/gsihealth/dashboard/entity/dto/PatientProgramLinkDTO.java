package com.gsihealth.dashboard.entity.dto;

import java.io.Serializable;

public class PatientProgramLinkDTO implements Serializable{

    public PatientProgramLinkDTO() {
    }

    private long id;
    private ProgramDTO program;
    private ProgramStatusDTO programStatus;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ProgramDTO getProgram() {
        return program;
    }

    public void setProgram(ProgramDTO program) {
        this.program = program;
    }

    public ProgramStatusDTO getProgramStatus() {
        return programStatus;
    }

    public void setProgramStatus(ProgramStatusDTO programStatus) {
        this.programStatus = programStatus;
    }
}
