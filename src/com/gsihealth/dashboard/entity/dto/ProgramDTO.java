package com.gsihealth.dashboard.entity.dto;

import java.io.Serializable;

public class ProgramDTO implements Serializable {

    private long id;
    private String name;

    private ProgramDTO() {}

    public ProgramDTO(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
