/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.entity.dto;

import java.io.Serializable;

/**
 *
 * @author User
 */
public class PatientCommunityEmergencyContactPK implements Serializable {
    
    private long id;
   private long communityId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }
}
