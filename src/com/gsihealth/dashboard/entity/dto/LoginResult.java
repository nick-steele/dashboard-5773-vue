package com.gsihealth.dashboard.entity.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author Chad Darby
 */
public class LoginResult implements java.io.Serializable {

    private String firstName;
    private String lastName;
    private String email;
    private String special;
    private String errorMessage;
    private boolean authenticated;
    private long userAccessTypeId;
    private long roleTypeId;
    private boolean mustChangePassword;
    private boolean userAlreadyLoggedIn;
    private String accessLevel;
    private long accessLevelId;
    private long userId;
    private String organizationName;
    private long organizationId;
    private String oid;
    private String token;
    private String userLevel;
    private long userLevelId;
    private boolean eula;
    private boolean samhsa;
    private String loginUrl;
    private String logoutUrl;
    private String treatUrl;
    private String treatName;
    private String treatFeatures;
    private boolean treatRestfulSSOEnabled;
    private String avadoUrl;
    private String avadoName;
    private String avadoFeatures;
    private String avadoUsePost;
    private String avadoUserList;
    private String sessionUrl;
    private String sessionName;
    private String sessionFeatures;
    private String sessionGoto;
    private String ssoPentahoServer;
    private List<String> userAppKeys;
    private List<ApplicationDTO> userApps;
    private List<ApplicationDTO> allApps;
    private int pollingIntervalInSeconds;
    private boolean poolingEnabled;
    private String carebookSummaryURL;
    private String disenrollmentCodeTitle;
    private String plcsTitle;
    private Character canManagePowerUser;
    private long communityId;
    private boolean clickToOpenEnabled;
    private String reportingRole;
    private Date lastLoginDate;
    private String openAMinstance;
    private String communityOid;
    private int authyId;
    private String disableUserMfa;
    private boolean canConsentEnrollMinors;
    private String countryCode;
    private Date lastMfaVerificationDate;
    private int userInterfaceType;          // For Redux
    private String directAddress;
    private boolean pentahoCalled = false;
    private String settings;
    private String pentahoLandingPage;
    private String communityName;
    private boolean canViewOrgPP = false;
    private boolean countyEnabled ;
    private boolean countyRequired;

    /**
     * Default constructor
     */
    public LoginResult() {
        userAppKeys = new ArrayList<String>();
    }

    /**
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage the errorMessage to set
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * @return the authenticated
     */
    public boolean isAuthenticated() {
        return authenticated;
    }

    /**
     * @param authenticated the authenticated to set
     */
    public void setAuthenticated(boolean authenticated) {
        this.authenticated = authenticated;
    }

    public boolean isPentahoCalled() {
        return pentahoCalled;
    }

    /**
     * @param authenticated the authenticated to set
     */
    public void setPentahoCalled(boolean pentahoCalled) {
        this.pentahoCalled = pentahoCalled;
    }

    public void setUserAccessTypeId(Long theId) {
        userAccessTypeId = theId;
    }

    // For Redux Jira DASHBOARD-964 - Optional user interface type (Redux or old view)
    public int getUserInterfaceType() {
        return userInterfaceType;
    }

    public void setUserInterfaceType(int userInterfaceType) {
        this.userInterfaceType = userInterfaceType;
    }

    /**
     * @return the roleId
     */
    public long getUserAccessTypeId() {
        return userAccessTypeId;
    }

    /**
     * @return the mustChangePassword
     */
    public boolean isMustChangePassword() {
        return mustChangePassword;
    }

    /**
     * @param mustChangePassword the mustChangePassword to set
     */
    public void setMustChangePassword(boolean mustChangePassword) {
        this.mustChangePassword = mustChangePassword;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getSpecial() {
        return special;
    }

    public void setSpecial(String special) {
        this.special = special;
    }

    public String getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

    public long getRoleTypeId() {
        return roleTypeId;
    }

    public void setRoleTypeId(long roleTypeId) {
        this.roleTypeId = roleTypeId;
    }

    public boolean isUserAlreadyLoggedIn() {
        return userAlreadyLoggedIn;
    }

    public void setUserAlreadyLoggedIn(boolean theFlag) {
        userAlreadyLoggedIn = theFlag;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the accessLevel
     */
    public String getAccessLevel() {
        return accessLevel;
    }

    /**
     * @param accessLevel the accessLevel to set
     */
    public void setAccessLevel(String accessLevel) {
        this.accessLevel = accessLevel;
    }

    /**
     * @return the organization
     */
    public String getOrganizationName() {
        return organizationName;
    }

    /**
     * @param organization the organization to set
     */
    public void setOrganizationName(String organization) {
        this.organizationName = organization;
    }

    public String getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(String userLevel) {
        this.userLevel = userLevel;
    }

    public String getEmail() {
        return email;
    }

    public long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(long organizationId) {
        this.organizationId = organizationId;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public long getAccessLevelId() {
        return accessLevelId;
    }

    public void setAccessLevelId(long accessLevelId) {
        this.accessLevelId = accessLevelId;
    }

    public boolean hasEula() {
        return eula;
    }

    public void setEula(boolean eula) {
        this.eula = eula;
    }

    public boolean hasSamhsa() {
        return samhsa;
    }

    public void setSamhsa(boolean samhsa) {
        this.samhsa = samhsa;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getLogoutUrl() {
        return logoutUrl;
    }

    public void setLogoutUrl(String logoutUrl) {
        this.logoutUrl = logoutUrl;
    }

    public String getTreatUrl() {
        return treatUrl;
    }

    public void setTreatUrl(String treatUrl) {
        this.treatUrl = treatUrl;
    }

    public String getTreatFeatures() {
        return treatFeatures;
    }

    public void setTreatFeatures(String treatFeatures) {
        this.treatFeatures = treatFeatures;
    }

    public String getTreatName() {
        return treatName;
    }

    public void setTreatName(String treatName) {
        this.treatName = treatName;
    }

    public String getAvadoFeatures() {
        return avadoFeatures;
    }

    public void setAvadoFeatures(String avadoFeatures) {
        this.avadoFeatures = avadoFeatures;
    }

    public String getAvadoName() {
        return avadoName;
    }

    public void setAvadoName(String avadoName) {
        this.avadoName = avadoName;
    }

    public String getAvadoUrl() {
        return avadoUrl;
    }

    public void setAvadoUrl(String avadoUrl) {
        this.avadoUrl = avadoUrl;
    }

    public String getAvadoUsePost() {
        return avadoUsePost;
    }

    public void setAvadoUsePost(String avadoUsePost) {
        this.avadoUsePost = avadoUsePost;
    }

    public String getAvadoUserList() {
        return avadoUserList;
    }

    public void setAvadoUserList(String avadoUserList) {
        this.avadoUserList = avadoUserList;
    }

// Hidden Session window popup
    public String getSessionFeatures() {
        return sessionFeatures;
    }

    public void setSessionFeatures(String sessionFeatures) {
        this.sessionFeatures = sessionFeatures;
    }

    public String getSessionGoto() {
        return sessionGoto;
    }

    public void setSessionGoto(String sessionGoto) {
        this.sessionGoto = sessionGoto;
    }

    public String getSessionName() {
        return sessionName;
    }

    public void setSessionName(String sessionName) {
        this.sessionName = sessionName;
    }

    public String getSessionUrl() {
        return sessionUrl;
    }

    public void setSessionUrl(String sessionUrl) {
        this.sessionUrl = sessionUrl;
    }

    public List<String> getUserAppKeys() {
        return userAppKeys;
    }

    public void setUserAppKeys(List<String> userAppKeys) {
        this.userAppKeys = userAppKeys;
    }

    public void addAppKey(String appKey) {
        userAppKeys.add(appKey);
    }

    public List<ApplicationDTO> getUserApps() {
        return userApps;
    }

    public void setUserApps(List<ApplicationDTO> userApps) {
        this.userApps = userApps;
    }

    public List<ApplicationDTO> getAllApps() {
        return allApps;
    }

    public void setAllApps(List<ApplicationDTO> allApps) {
        this.allApps = allApps;
    }

    public long getUserLevelId() {
        return userLevelId;
    }

    public void setUserLevelId(long userLevelId) {
        this.userLevelId = userLevelId;
    }

    public int getPollingIntervalInSeconds() {
        return pollingIntervalInSeconds;
    }

    public void setPollingIntervalInSeconds(int pollingIntervalInSeconds) {
        this.pollingIntervalInSeconds = pollingIntervalInSeconds;
    }

    public boolean isPoolingEnabled() {
        return poolingEnabled;
    }

    public void setPoolingEnabled(boolean poolingEnabled) {
        this.poolingEnabled = poolingEnabled;
    }

    public boolean isTreatRestfulSSOEnabled() {
        return treatRestfulSSOEnabled;
    }

    public void setTreatRestfulSSOEnabled(boolean treatRestfulSSOEnabled) {
        this.treatRestfulSSOEnabled = treatRestfulSSOEnabled;
    }

    /**
     * @return the carebookSummaryURL
     */
    public String getCarebookSummaryURL() {
        return carebookSummaryURL;
    }

    /**
     * @param carebookSummaryURL the carebookSummaryURL to set
     */
    public void setCarebookSummaryURL(String carebookSummaryURL) {
        this.carebookSummaryURL = carebookSummaryURL;
    }

    /**
     * @return the disenrollmentCodeTitle
     */
    public String getDisenrollmentCodeTitle() {
        return disenrollmentCodeTitle;
    }

    /**
     * @param disenrollmentCodeTitle the disenrollmentCodeTitle to set
     */
    public void setDisenrollmentCodeTitle(String disenrollmentCodeTitle) {
        this.disenrollmentCodeTitle = disenrollmentCodeTitle;
    }

    /**
     * @return the plcsTitle
     */
    public String getPlcsTitle() {
        return plcsTitle;
    }

    /**
     * @param plcsTitle the plcsTitle to set
     */
    public void setPlcsTitle(String plcsTitle) {
        this.plcsTitle = plcsTitle;
    }

    /**
     * @return the canManagePowerUser
     */
    public Character getCanManagePowerUser() {
        return canManagePowerUser;
    }

    /**
     * @param canManagePowerUser the canManagePowerUser to set
     */
    public void setCanManagePowerUser(Character canManagePowerUser) {
        this.canManagePowerUser = canManagePowerUser;
    }

    public String getSsoPentahoServer() {
        return ssoPentahoServer;
    }

    public String getPentahoLandingPage() {
        return pentahoLandingPage;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }

    public long getCommunityId() {
        return communityId;
    }

    public void setSsoPentahoServer(String ssoPentahoServer) {
        this.ssoPentahoServer = ssoPentahoServer;
    }

    public void setPentahoLandingPage(String pentahoLandingPage) {
        this.pentahoLandingPage = pentahoLandingPage;
    }

    public Boolean getClickToOpenEnabled() {
        return clickToOpenEnabled;
    }

    public void setClickToOpenEnabled(Boolean clickToOpenEnabled) {
        this.clickToOpenEnabled = clickToOpenEnabled;
    }

    public String getReportingRole() {
        return reportingRole;
    }

    public void setReportingRole(String reportingRole) {
        this.reportingRole = reportingRole;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public String getOpenAMinstance() {
        return openAMinstance;
    }

    public void setOpenAMinstance(String openAMinstance) {
        this.openAMinstance = openAMinstance;
    }

    public String getCommunityOid() {
        return communityOid;
    }

    public void setCommunityOid(String communityOid) {
        this.communityOid = communityOid;
    }

    /**
     * @return the authyId
     */
    public int getAuthyId() {
        return authyId;
    }

    /**
     * @param authyId the authyId to set
     */
    public void setAuthyId(int authyId) {
        this.authyId = authyId;
    }

    /**
     * @return the disableUserMfa
     */
    public String getDisableUserMfa() {
        return disableUserMfa;
    }

    /**
     * @param disableUserMfa the disableUserMfa to set
     */
    public void setDisableUserMfa(String disableUserMfa) {
        this.disableUserMfa = disableUserMfa;
    }

    public boolean isCanConsentEnrollMinors() {
        return canConsentEnrollMinors;
    }

    public void setCanConsentEnrollMinors(boolean canConsentEnrollMinors) {
        this.canConsentEnrollMinors = canConsentEnrollMinors;
    }

    /**
     * @return the countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * @param countryCode the countryCode to set
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    /**
     * @return the lastMfaVerificationDate
     */
    public Date getLastMfaVerificationDate() {
        return lastMfaVerificationDate;
    }

    /**
     * @param lastMfaVerificationDate the lastMfaVerificationDate to set
     */
    public void setLastMfaVerificationDate(Date lastMfaVerificationDate) {
        this.lastMfaVerificationDate = lastMfaVerificationDate;
    }

    public String getDirectAddress() {
        return directAddress;
    }

    public void setDirectAddress(String directAddress) {
        this.directAddress = directAddress;
    }

    public String getSettings() {
        return settings;
    }

    public void setSettings(String settings) {
        this.settings = settings;
    }

	public String getCommunityName() {
		return communityName;
	}

	public void setCommunityName(String communityName) {
		this.communityName = communityName;
	}

    public boolean isCanViewOrgPP() {
        return canViewOrgPP;
    }

    public void setCanViewOrgPP(boolean canViewOrgPP) {
        this.canViewOrgPP = canViewOrgPP;
    }

    public boolean isCountyEnabled() {
        return countyEnabled;
    }

    public void setCountyEnabled(boolean countyEnabled) {
        this.countyEnabled = countyEnabled;
    }

    public boolean isCountyRequired() {
        return countyRequired;
    }

    public void setCountyRequired(boolean countyRequired) {
        this.countyRequired = countyRequired;
    }
}
