package com.gsihealth.dashboard.entity.dto;

import java.io.Serializable;

/**
 *
 * @author Chad Darby
 */
public class PatientEmergencyContactDTO implements Serializable {
    
    private String firstName;
    private String middleName;
    private String lastName;
    private String telephone;
    private String emailAddress;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zipCode;
    private Long id;
    private String patientRelationshipDescription;
    private Long patientRelationshipId;

    public PatientEmergencyContactDTO() {
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }    

    /**
     * @return the patientRelationship
     */
    public String getPatientRelationshipDescription() {
        return patientRelationshipDescription;
    }

    /**
     * @param patientRelationship the patientRelationship to set
     */
    public void setPatientRelationshipDescription(String patientRelationship) {
        this.patientRelationshipDescription = patientRelationship;
    }

    public Long getPatientRelationshipId() {
        return patientRelationshipId;
    }

    public void setPatientRelationshipId(Long thePatientRelationshipId) {
        this.patientRelationshipId = thePatientRelationshipId;
    }
}
