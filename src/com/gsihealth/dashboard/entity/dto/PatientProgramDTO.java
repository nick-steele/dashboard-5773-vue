
package com.gsihealth.dashboard.entity.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author ssingh
 */
public class PatientProgramDTO implements Serializable {

    private long patientProgramNameId;
    private long patientId;
    private long programId;
    private long communityId;
    private String healthHome;
    private Date programEffectiveDate;
    private Date programEndDate;
    private long patientStatus;
    private String terminationReason;
    private boolean  oldRecord;
    private String operation;
    private Date statusEffectiveDate;
    private boolean delete;
    private ProgramNameDTO program;
    private PatientStatusDTO status;
    
    
     public PatientProgramDTO() {
    }

    /**
     * @return the patientId
     */
    public long getPatientId() {
        return patientId;
    }

    /**
     * @param patientId the patientId to set
     */
    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    /**
     * @return the programId
     */
    public long getProgramId() {
        return programId;
    }

    /**
     * @param programId the programId to set
     */
    public void setProgramId(long programId) {
        this.programId = programId;
    }

    /**
     * @return the communityId
     */
    public long getCommunityId() {
        return communityId;
    }

    /**
     * @param communityId the communityId to set
     */
    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }

    /**
     * @return the healthHome
     */
    public String getHealthHome() {
        return healthHome;
    }

    /**
     * @param healthHome the healthHome to set
     */
    public void setHealthHome(String healthHome) {
        this.healthHome = healthHome;
    }

    /**
     * @return the programEffectiveDate
     */
    public Date getProgramEffectiveDate() {
        return programEffectiveDate;
    }

    /**
     * @param programEffectiveDate the programEffectiveDate to set
     */
    public void setProgramEffectiveDate(Date programEffectiveDate) {
        this.programEffectiveDate = programEffectiveDate;
    }

    /**
     * @return the programEndDate
     */
    public Date getProgramEndDate() {
        return programEndDate;
    }

    /**
     * @param programEndDate the programEndDate to set
     */
    public void setProgramEndDate(Date programEndDate) {
        this.programEndDate = programEndDate;
    }

    /**
     * @return the patientStatus
     */
    public long getPatientStatus() {
        return patientStatus;
    }

    /**
     * @param patientStatus the patientStatus to set
     */
    public void setPatientStatus(long patientStatus) {
        this.patientStatus = patientStatus;
    }

    /**
     * @return the terminationReason
     */
    public String getTerminationReason() {
        return terminationReason;
    }

    /**
     * @param terminationReason the terminationReason to set
     */
    public void setTerminationReason(String terminationReason) {
        this.terminationReason = terminationReason;
    }

    /**
     * @return the oldRecord
     */
    public boolean isOldRecord() {
        return oldRecord;
    }

    /**
     * @param oldRecord the oldRecord to set
     */
    public void setOldRecord(boolean oldRecord) {
        this.oldRecord = oldRecord;
    }

    /**
     * @return the operation
     */
    public String getOperation() {
        return operation;
    }

    /**
     * @param operation the operation to set
     */
    public void setOperation(String operation) {
        this.operation = operation;
    }

    /**
     * @return the statusEffectiveDate
     */
    public Date getStatusEffectiveDate() {
        return statusEffectiveDate;
    }

    /**
     * @param statusEffectiveDate the statusEffectiveDate to set
     */
    public void setStatusEffectiveDate(Date statusEffectiveDate) {
        this.statusEffectiveDate = statusEffectiveDate;
    }

    /**
     * @return the delete
     */
    public boolean isDelete() {
        return delete;
    }

    /**
     * @param delete the delete to set
     */
    public void setDelete(boolean delete) {
        this.delete = delete;
    }

   
    /**
     * @return the program
     */
    public ProgramNameDTO getProgram() {
        return program;
    }

    /**
     * @param program the program to set
     */
    public void setProgram(ProgramNameDTO program) {
        this.program = program;
    }

    /**
     * @return the status
     */
    public PatientStatusDTO getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(PatientStatusDTO status) {
        this.status = status;
    }

    /**
     * @return the patientProgramNameId
     */
    public long getPatientProgramNameId() {
        return patientProgramNameId;
    }

    /**
     * @param patientProgramNameId the patientProgramNameId to set
     */
    public void setPatientProgramNameId(long patientProgramNameId) {
        this.patientProgramNameId = patientProgramNameId;
    }

    
    
}
