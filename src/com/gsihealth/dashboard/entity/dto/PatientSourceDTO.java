package com.gsihealth.dashboard.entity.dto;

/**
 *
 * @author Chad.Darby
 */
public class PatientSourceDTO implements java.io.Serializable {

    private Long patientSourceId;
    private String organization;
    private String organizationType;
    private String referenceSource;
    private String referenceType;
    private long communityId;

    public PatientSourceDTO() {
    }

    public PatientSourceDTO(Long patientSourceId, String organization, String organizationType, String referenceSource, String referenceType, long communityId) {
        this.patientSourceId = patientSourceId;
        this.organization = organization;
        this.organizationType = organizationType;
        this.referenceSource = referenceSource;
        this.referenceType = referenceType;
        this.communityId = communityId;
    }

    public Long getPatientSourceId() {
        return patientSourceId;
    }

    public void setPatientSourceId(Long patientSourceId) {
        this.patientSourceId = patientSourceId;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(String organizationType) {
        this.organizationType = organizationType;
    }

    public String getReferenceSource() {
        return referenceSource;
    }

    public void setReferenceSource(String referenceSource) {
        this.referenceSource = referenceSource;
    }

    public String getReferenceType() {
        return referenceType;
    }

    public void setReferenceType(String referenceType) {
        this.referenceType = referenceType;
    }

    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }    
}
