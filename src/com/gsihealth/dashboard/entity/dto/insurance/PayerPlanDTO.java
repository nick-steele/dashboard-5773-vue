package com.gsihealth.dashboard.entity.dto.insurance;

/**
 *
 * @author Chad Darby
 */
public class PayerPlanDTO implements java.io.Serializable {
    private Long communityId;
    private Long id;
    private String name;
    private String mmisId;

    public PayerPlanDTO() {
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMmisId() {
        return mmisId;
    }

    public void setMmisId(String mmisId) {
        this.mmisId = mmisId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
}
