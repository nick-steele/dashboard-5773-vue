/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.entity.dto;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author ssingh
 */
public class PatientActivityTrackerDTO implements Serializable{
    
    private long id;
    private long patientId;
    private long userId;
    private long activityNameId;
    private String activityValue;
    private Date creationDate;
    private Date lastUpdatedDate;
    private long communityId;
    private Date effectiveDate;
    private boolean oldRecord;
     private String operation;
    
    public PatientActivityTrackerDTO() {
        
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the patientId
     */
    public long getPatientId() {
        return patientId;
    }

    /**
     * @param patientId the patientId to set
     */
    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    /**
     * @return the userId
     */
    public long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * @return the activityNameId
     */
    public long getActivityNameId() {
        return activityNameId;
    }

    /**
     * @param activityNameId the activityNameId to set
     */
    public void setActivityNameId(long activityNameId) {
        this.activityNameId = activityNameId;
    }

    /**
     * @return the activityValue
     */
    public String getActivityValue() {
        return activityValue;
    }

    /**
     * @param activityValue the activityValue to set
     */
    public void setActivityValue(String activityValue) {
        this.activityValue = activityValue;
    }

    /**
     * @return the creationDate
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * @param creationDate the creationDate to set
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * @return the lastUpdatedDate
     */
    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    /**
     * @param lastUpdatedDate the lastUpdatedDate to set
     */
    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    /**
     * @return the communityId
     */
    public long getCommunityId() {
        return communityId;
    }

    /**
     * @param communityId the communityId to set
     */
    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }

    /**
     * @return the effectiveDate
     */
    public Date getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * @param effectiveDate the effectiveDate to set
     */
    public void setEffectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    /**
     * @return the oldRecord
     */
    public boolean isOldRecord() {
        return oldRecord;
    }

    /**
     * @param oldRecord the oldRecord to set
     */
    public void setOldRecord(boolean oldRecord) {
        this.oldRecord = oldRecord;
    }

    /**
     * @return the operation
     */
    public String getOperation() {
        return operation;
    }

    /**
     * @param operation the operation to set
     */
    public void setOperation(String operation) {
        this.operation = operation;
    }
    
    
}
