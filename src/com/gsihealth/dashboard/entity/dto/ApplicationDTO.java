package com.gsihealth.dashboard.entity.dto;

/**
 *
 * @author Chad Darby
 */
public class ApplicationDTO implements java.io.Serializable {
    
    //private Long applicationId;
    private String applicationName;
    private String notificationEndpoint;
    private String oid;
    private String screenName;
    private String externalApplicationUrl;
    private Boolean ssoEnabled;
    private Integer displayOrder;
    private Boolean showInPopupWindow;
    private Boolean nativeApp;
    private Boolean clickToOpenEnabled;
    private long communityId;
    private long applicationId;

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public String getExternalApplicationUrl() {
        return externalApplicationUrl;
    }

    public void setExternalApplicationUrl(String externalApplicationUrl) {
        this.externalApplicationUrl = externalApplicationUrl;
    }

    public Boolean getNativeApp() {
        return nativeApp;
    }

    public void setNativeApp(Boolean nativeApp) {
        this.nativeApp = nativeApp;
    }

    public String getNotificationEndpoint() {
        return notificationEndpoint;
    }

    public void setNotificationEndpoint(String notificationEndpoint) {
        this.notificationEndpoint = notificationEndpoint;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public Boolean getShowInPopupWindow() {
        return showInPopupWindow;
    }

    public void setShowInPopupWindow(Boolean showInPopupWindow) {
        this.showInPopupWindow = showInPopupWindow;
    }

    public Boolean getSsoEnabled() {
        return ssoEnabled;
    }

    public void setSsoEnabled(Boolean ssoEnabled) {
        this.ssoEnabled = ssoEnabled;
    }

    public Boolean getClickToOpenEnabled() {
        return clickToOpenEnabled;
    }

    public void setClickToOpenEnabled(Boolean clickToOpenEnabled) {
        this.clickToOpenEnabled = clickToOpenEnabled;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }

    public long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(long applicationId) {
        this.applicationId = applicationId;
    }

    
    
}
