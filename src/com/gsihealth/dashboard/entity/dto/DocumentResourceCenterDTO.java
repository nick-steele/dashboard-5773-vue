/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.entity.dto;

import java.util.Date;

/**
 *
 * @author User
 */
public class DocumentResourceCenterDTO implements java.io.Serializable {

    private Long documentId;
    private String documentTitle;
    private String description;
    private String extendedMIMEType;
    private String pathLinkToFile;
    private Date uploadDate;

    /**
     * @return the documentId
     */
    public Long getDocumentId() {
        return documentId;
    }

    /**
     * @param documentId the documentId to set
     */
    public void setDocumentId(Long documentId) {
        this.documentId = documentId;
    }

    /**
     * @return the documentTitle
     */
    public String getDocumentTitle() {
        return documentTitle;
    }

    /**
     * @param documentTitle the documentTitle to set
     */
    public void setDocumentTitle(String documentTitle) {
        this.documentTitle = documentTitle;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the extendedMIMEType
     */
    public String getExtendedMIMEType() {
        return extendedMIMEType;
    }

    /**
     * @param extendedMIMEType the extendedMIMEType to set
     */
    public void setExtendedMIMEType(String extendedMIMEType) {
        this.extendedMIMEType = extendedMIMEType;
    }

    /**
     * @return the pathLinkToFile
     */
    public String getPathLinkToFile() {
        return pathLinkToFile;
    }

    /**
     * @param pathLinkToFile the pathLinkToFile to set
     */
    public void setPathLinkToFile(String pathLinkToFile) {
        this.pathLinkToFile = pathLinkToFile;
    }

    /**
     * @return the uploadDate
     */
    public Date getUploadDate() {
        return uploadDate;
    }

    /**
     * @param uploadDate the uploadDate to set
     */
    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }
}
