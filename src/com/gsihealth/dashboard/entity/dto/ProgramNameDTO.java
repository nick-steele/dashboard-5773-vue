/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.entity.dto;

/**
 *
 * @author User
 */
@Deprecated
public class ProgramNameDTO implements java.io.Serializable {
    
    private Long id;
    private String value;
    private Long programHealth;

    public ProgramNameDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getProgramHealth() {
        return programHealth;
    }

    public void setProgramHealth(Long programHealth) {
        this.programHealth = programHealth;
    }

   
    
}
