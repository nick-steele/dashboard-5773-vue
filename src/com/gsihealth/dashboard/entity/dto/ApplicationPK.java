
package com.gsihealth.dashboard.entity.dto;

import java.io.Serializable;

/**
 *
 * @author User
 */
public class ApplicationPK implements Serializable{
    
    private long applicationId;
    private long communityId;

    public long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(long applicationId) {
        this.applicationId = applicationId;
    }

    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }
}
