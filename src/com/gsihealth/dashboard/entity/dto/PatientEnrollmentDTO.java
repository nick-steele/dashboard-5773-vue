package com.gsihealth.dashboard.entity.dto;

import com.gsihealth.dashboard.entity.dto.insurance.PayerClassDTO;
import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class PatientEnrollmentDTO implements Serializable {

    private long communityId;
    private long patientId;
    private String patientEuid;
    private String oid;
    private Date startDate;
    private String status;
    private String programName;
    private String programLevelConsentStatus;
    private long orgId;
    private String reasonForInactivation;
    private String reasonForRefusal;
    private String patientMrn;
    private String organizationName;
    private Date consentDateTime;
    private long consentFormId;
    private long consentObtainedByUserId;
    private CommunityDTO communityDTO;
    private PayerClassDTO secondaryPayerClass;
    private PayerClassDTO primaryPayerClass;
    private PayerPlanDTO secondaryPayerPlan;
    private PayerPlanDTO primaryPayerPlan;
    private String primaryPayerMedicaidMedicareId;
    private Date primaryPlanEffectiveDate;
    private String secondaryPayerMedicaidMedicareId;
    private Date secondaryPlanEffectiveDate;
    private String emailAddress;
    private Date programNameEffectiveDate;
    private Date programEndDate;
    private Date createdDate;
    private Long disenrollmentReasonCodeId;

    private PatientEmergencyContactDTO patientEmergencyContact;
    private String healthHome;
    private ProgramHealthHomeDTO programHealthHome;
    private String patientUserActive;
    private String directAddress ;
    private long roleID;
    private String patientLoginId;
    
    private long patientSourceId;
    private List<PatientProgramDTO> patientProgramName;
    private String acuityScore;  
    private String informationSharingStatus;
    private long minorConsentObtainedByUserId;
    private Date minorConsentDateTime;
    private List<PatientActivityTrackerDTO> patientActivities;
    private Date selfAssertedConsentDate;
    private long consenter;

    private List<PatientProgramLinkDTO> patientProgramLinks;
    
    public PatientEnrollmentDTO() {
    }

    public String getPatientEuid() {
        return patientEuid;
    }

    public void setPatientEuid(String patientEuid) {
        this.patientEuid = patientEuid;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String theProgramName) {
        this.programName = theProgramName;
    }

    public CommunityDTO getCommunityDTO() {
        return communityDTO;
    }

    public void setCommunityDTO(CommunityDTO communityDTO) {
        this.communityDTO = communityDTO;
    }

    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    /**
     * @return the oid
     */
    public String getOid() {
        return oid;
    }

    /**
     * @param oid the oid to set
     */
    public void setOid(String oid) {
        this.oid = oid;
    }

    public long getOrgId() {
        return orgId;
    }

    public void setOrgId(long orgId) {
        this.orgId = orgId;
    }

    public String getPatientMrn() {
        return patientMrn;
    }

    public void setPatientMrn(String patientMrn) {
        this.patientMrn = patientMrn;
    }

    public String getProgramLevelConsentStatus() {
        return programLevelConsentStatus;
    }

    public void setProgramLevelConsentStatus(String programLevelConsentStatus) {
        this.programLevelConsentStatus = programLevelConsentStatus;
    }

    public String getReasonForInactivation() {
        return reasonForInactivation;
    }

    public void setReasonForInactivation(String reasonForInactivation) {
        this.reasonForInactivation = reasonForInactivation;
    }

    public String getOrganizationName() {
        return organizationName;
    }

    public void setOrganizationName(String organizationName) {
        this.organizationName = organizationName;
    }

    public Date getConsentDateTime() {
        return consentDateTime;
    }

    public void setConsentDateTime(Date consentStartDateTime) {
        this.consentDateTime = consentStartDateTime;
    }

    public long getConsentFormId() {
        return consentFormId;
    }

    public void setConsentFormId(long consentVersionId) {
        this.consentFormId = consentVersionId;
    }

    public long getConsentObtainedByUserId() {
        return consentObtainedByUserId;
    }

    public void setConsentObtainedByUserId(long consentObtainedByUserId) {
        this.consentObtainedByUserId = consentObtainedByUserId;
    }

    public String getReasonForRefusal() {
        return reasonForRefusal;
    }

    public void setReasonForRefusal(String reasonForRefusal) {
        this.reasonForRefusal = reasonForRefusal;
    }

    public PayerClassDTO getPrimaryPayerClass() {
        return primaryPayerClass;
    }

    public void setPrimaryPayerClass(PayerClassDTO primaryPayerClass) {
        this.primaryPayerClass = primaryPayerClass;
    }

    public String getPrimaryPayerMedicaidMedicareId() {
        return primaryPayerMedicaidMedicareId;
    }

    public void setPrimaryPayerMedicaidMedicareId(String primaryPayerMedicaidMedicareId) {
        this.primaryPayerMedicaidMedicareId = primaryPayerMedicaidMedicareId;
    }

    public PayerPlanDTO getPrimaryPayerPlan() {
        return primaryPayerPlan;
    }

    public void setPrimaryPayerPlan(PayerPlanDTO primaryPayerPlan) {
        this.primaryPayerPlan = primaryPayerPlan;
    }

    public Date getPrimaryPlanEffectiveDate() {
        return primaryPlanEffectiveDate;
    }

    public void setPrimaryPlanEffectiveDate(Date primaryPlanEffectiveDate) {
        this.primaryPlanEffectiveDate = primaryPlanEffectiveDate;
    }

    public PayerClassDTO getSecondaryPayerClass() {
        return secondaryPayerClass;
    }

    public void setSecondaryPayerClass(PayerClassDTO secondaryPayerClass) {
        this.secondaryPayerClass = secondaryPayerClass;
    }

    public String getSecondaryPayerMedicaidMedicareId() {
        return secondaryPayerMedicaidMedicareId;
    }

    public void setSecondaryPayerMedicaidMedicareId(String secondaryPayerMedicaidMedicareId) {
        this.secondaryPayerMedicaidMedicareId = secondaryPayerMedicaidMedicareId;
    }

    public PayerPlanDTO getSecondaryPayerPlan() {
        return secondaryPayerPlan;
    }

    public void setSecondaryPayerPlan(PayerPlanDTO secondaryPayerPlan) {
        this.secondaryPayerPlan = secondaryPayerPlan;
    }

    public Date getSecondaryPlanEffectiveDate() {
        return secondaryPlanEffectiveDate;
    }

    public void setSecondaryPlanEffectiveDate(Date secondaryPlanEffectiveDate) {
        this.secondaryPlanEffectiveDate = secondaryPlanEffectiveDate;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public PatientEmergencyContactDTO getPatientEmergencyContact() {
        return patientEmergencyContact;
    }

    public void setPatientEmergencyContact(PatientEmergencyContactDTO patientEmergencyContact) {
        this.patientEmergencyContact = patientEmergencyContact;
    }

    public Date getProgramNameEffectiveDate() {
        return programNameEffectiveDate;
    }

    public void setProgramNameEffectiveDate(Date programNameEffectiveDate) {
        this.programNameEffectiveDate = programNameEffectiveDate;
    }

    public Date getProgramEndDate() {
        return programEndDate;
    }

    public void setProgramEndDate(Date programEndDate) {
        this.programEndDate = programEndDate;
    }

    public Long getDisenrollmentReasonCodeId() {
        return disenrollmentReasonCodeId;
    }

    public void setDisenrollmentReasonCodeId(Long disenrollmentReasonCodeId) {
        this.disenrollmentReasonCodeId = disenrollmentReasonCodeId;
    }

    /**
     * @return the programHealthHome
     */
    public ProgramHealthHomeDTO getProgramHealthHome() {
        return programHealthHome;
    }

    /**
     * @param programHealthHome the programHealthHome to set
     */
    public void setProgramHealthHome(ProgramHealthHomeDTO programHealthHome) {
        this.programHealthHome = programHealthHome;
    }

    /**
     * @return the healthHome
     */
    public String getHealthHome() {
        return healthHome;
    }

    /**
     * @param healthHome the healthHome to set
     */
    public void setHealthHome(String healthHome) {
        this.healthHome = healthHome;
    }
    
    public String getPatientUserActive() {
        return patientUserActive;
    }

    public void setPatientUserActive(String patientUserActive) {
        this.patientUserActive = patientUserActive;
    }

    public String getDirectAddress() {
        return directAddress;
    }

    public void setDirectAddress(String directAddress) {
        this.directAddress = directAddress;
    }

    public long getRoleID() {
        return roleID;
    }

    public void setRoleID(long roleID) {
        this.roleID = roleID;
    }

    public String getPatientLoginId() {
        return patientLoginId;
    }

    public void setPatientLoginId(String patientLoginId) {
        this.patientLoginId = patientLoginId;
    }

    /**
     * @return the createdDate
     */
    public Date getCreatedDate() {
        return createdDate;
    }

    /**
     * @param createdDate the createdDate to set
     */
    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public long getPatientSourceId() {
        return patientSourceId;
    }

    public void setPatientSourceId(long patientSourceId) {
        this.patientSourceId = patientSourceId;
    }
   /**
     * @return the patientProgramName
     */
    public List<PatientProgramDTO> getPatientProgramName() {
        return patientProgramName;
    }

    /**
     * @param patientProgramName the patientProgramName to set
     */
    public void setPatientProgramName(List<PatientProgramDTO> patientProgramName) {
        this.patientProgramName = patientProgramName;
    }

    /**
     * @return the acuityScore
     */
    public String getAcuityScore() {
        return acuityScore;
    }

    /**
     * @param acuityScore the acuityScore to set
     */
    public void setAcuityScore(String acuityScore) {
        this.acuityScore = acuityScore;
    }

    /**
     * @return the informationSharingConsentStatus
     */
    public String getInformationSharingStatus() {
        return informationSharingStatus;
    }

    /**
     * @param informationSharingConsentStatus the informationSharingConsentStatus to set
     */
    public void setInformationSharingStatus(String informationSharingStatus) {
        this.informationSharingStatus = informationSharingStatus;
    }

    /**
     * @return the minorConsentObtainedByUserId
     */
    public long getMinorConsentObtainedByUserId() {
        return minorConsentObtainedByUserId;
    }

    /**
     * @param minorConsentObtainedByUserId the minorConsentObtainedByUserId to set
     */
    public void setMinorConsentObtainedByUserId(long minorConsentObtainedByUserId) {
        this.minorConsentObtainedByUserId = minorConsentObtainedByUserId;
    }

    /**
     * @return the minorConsentDateTime
     */
    public Date getMinorConsentDateTime() {
        return minorConsentDateTime;
    }

    /**
     * @param minorConsentDateTime the minorConsentDateTime to set
     */
    public void setMinorConsentDateTime(Date minorConsentDateTime) {
        this.minorConsentDateTime = minorConsentDateTime;
    }

    /**
     * @return the patientActivities
     */
    public List<PatientActivityTrackerDTO> getPatientActivities() {
        return patientActivities;
    }

    /**
     * @param patientActivities the patientActivities to set
     */
    public void setPatientActivities(List<PatientActivityTrackerDTO> patientActivities) {
        this.patientActivities = patientActivities;
    }

    /**
     * @return the selfAssertedConsentDate
     */
    public Date getSelfAssertedConsentDate() {
        return selfAssertedConsentDate;
    }

    /**
     * @param selfAssertedConsentDate the selfAssertedConsentDate to set
     */
    public void setSelfAssertedConsentDate(Date selfAssertedConsentDate) {
        this.selfAssertedConsentDate = selfAssertedConsentDate;
    }

    public long getConsenter() {
        return consenter;
    }

    public void setConsenter(long consenter) {
        this.consenter = consenter;
    }

    public List<PatientProgramLinkDTO> getPatientProgramLinks() {
        return patientProgramLinks;
    }

    public void setPatientProgramLinks(List<PatientProgramLinkDTO> patientProgramLinks) {
        this.patientProgramLinks = patientProgramLinks;
    }
}

