/*
 * my sleight of hand pojo to get past chaining callbacks
 */
package com.gsihealth.dashboard.entity.dto;

/**
 *
 * @author Beth.Boose
 */
public class PatientListLabelDTO implements java.io.Serializable {
    boolean showLabel;
    boolean showButtom;
    boolean hasConsent;

    public PatientListLabelDTO() {
    }

    public PatientListLabelDTO(boolean showLabel, boolean showButtom, boolean hasConsent) {
        this.showLabel = showLabel;
        this.showButtom = showButtom;
        this.hasConsent = hasConsent;
    }

    public PatientListLabelDTO(boolean showLabel, boolean showButtom) {
        this.showLabel = showLabel;
        this.showButtom = showButtom;
    }

    public boolean getShowLabel() {
        return showLabel;
    }

    public void setShowLabel(boolean showLabel) {
        this.showLabel = showLabel;
    }

    public boolean getShowButtom() {
        return showButtom;
    }

    public void setShowButtom(boolean showButtom) {
        this.showButtom = showButtom;
    }

    public boolean isHasConsent() {
        return hasConsent;
    }

    public void setHasConsent(boolean hasConsent) {
        this.hasConsent = hasConsent;
    }
    
    
    
}
