package com.gsihealth.dashboard.entity.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Chad.Darby
 */
public class MyPatientInfoDTO implements java.io.Serializable {

    private long userId;
    private long patientId;
    private long communityId;    
    private Character flagged;            

    private String firstName;
    
    private String lastName;
    
    private String cmOrganizationName;
    
    private String enrollmentStatus;
    
    private String euid;
    
    private Date dob;
    
    private String gender;
    
    private List<ListTagDTO> tags = new ArrayList();
    
    public MyPatientInfoDTO() {
        
    }

    public MyPatientInfoDTO(long userId, long patientId, long communityId, Character flagged) {
        this.userId = userId;
        this.patientId = patientId;
        this.communityId = communityId;
        this.flagged = flagged;
    }
    
    public MyPatientInfoDTO(long userId, long patientId, long communityId) {
        this(userId, patientId, communityId, 'N');
    }

    public MyPatientInfoDTO(long userId, long patientId, long communityId, Character flagged, String cmOrganizationName, String enrollmentStatus, String euid) {
        this(userId, patientId, communityId, flagged, null, null, cmOrganizationName, enrollmentStatus, euid);
    }
    
    public MyPatientInfoDTO(long userId, long patientId, long communityId, Character flagged, String firstName, String lastName, String cmOrganizationName, String enrollmentStatus, String euid) {
        this.userId = userId;
        this.patientId = patientId;
        this.communityId = communityId;
        this.flagged = flagged;
        this.firstName = firstName;
        this.lastName = lastName;
        this.cmOrganizationName = cmOrganizationName;
        this.enrollmentStatus = enrollmentStatus;
        this.euid = euid;
    }
     
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }

    public Character getFlagged() {
        return flagged;
    }

    public void setFlagged(Character flagged) {
        this.flagged = flagged;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCmOrganizationName() {
        return cmOrganizationName;
    }

    public void setCmOrganizationName(String cmOrganizationName) {
        this.cmOrganizationName = cmOrganizationName;
    }

    public String getEnrollmentStatus() {
        return enrollmentStatus;
    }

    public void setEnrollmentStatus(String enrollmentStatus) {
        this.enrollmentStatus = enrollmentStatus;
    }

    public String getEuid() {
        return euid;
    }

    public void setEuid(String euid) {
        this.euid = euid;
    }

    /**
     * @return the tags
     */
    public List<ListTagDTO> getTags() {
        return tags;
    }

    /**
     * @return the dob
     */
    public Date getDob() {
        return dob;
    }

    /**
     * @param dob the dob to set
     */
    public void setDob(Date dob) {
        this.dob = dob;
    }

    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

  
    
    
}
