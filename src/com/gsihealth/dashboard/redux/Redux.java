package com.gsihealth.dashboard.redux;


import com.gsihealth.dashboard.client.DashBoardApp;
import com.gsihealth.dashboard.client.login.LoginPanel;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;


/**
 * @author Nick Steele
 *         Created 2016/2/25 for 5.1 s3
 *         Contains common Redux methods to connect the new Angular client app with GWT
 */

public class Redux {

    /**
     * GWT will compile this JSNI function to emit events in the new Angular client.
     * In the new client, there is a single JS object called GWT and a built-in event function that receives these events.
     *
     * @param eventName the three part global event name (with colon delimiters) i.e. local:core:restart
     **/

    public static native void setGwtReady() /*-{
        $wnd.GWT.ready = true;
    }-*/;

    public static native void jsEvent(String eventName) /*-{
        $wnd.GWT.event(eventName);
    }-*/;

    public static native void showAppWindow(String url, String name, String features) /*-{
        $wnd.GWT.showAppWindow(url, name, features);
    }-*/;

    public static native void showAppTab(String url, String name, String features) /*-{
        $wnd.GWT.showAppTab(url, name, features);
    }-*/;

    public static native void openAppWindow(String url, String name, String features) /*-{
        $wnd.GWT.openAppWindow(url, name, features);
    }-*/;

    public static native void jsEvent(String eventName, Object obj) /*-{
        $wnd.GWT.event(eventName, obj);
    }-*/;

    public static native void loginTimer(String msg) /*-{
        $wnd.GWT.loginTimer(msg);
    }-*/;

    public static native void log(String msg) /*-{
        $wnd.GWT.log(msg);
    }-*/;

    public static native void note(String msg) /*-{
        $wnd.GWT.note(msg);
    }-*/;


    //    FIXME: Must deprecate GWT.login
    public static native void declareLoginPanelMethod(LoginPanel loginPanel) /*-{
        var _loginPanel = loginPanel;
        $wnd.GWT.login = function(username,password) {
            _loginPanel.@com.gsihealth.dashboard.client.login.LoginPanel::doLogin(Ljava/lang/String;Ljava/lang/String;)(username, password);        };
        $wnd.GWT.forgotPassword = function(username) {
            _loginPanel.@com.gsihealth.dashboard.client.login.LoginPanel::doForgotPassword(Ljava/lang/String;)(username);
        };
        $wnd.GWT.runForgotPassword = function(username) {
            _loginPanel.@com.gsihealth.dashboard.client.login.LoginPanel::runForgotPassword(Ljava/lang/String;)(username);
        };
        $wnd.GWT.checkToken = function (token) {
            @com.gsihealth.dashboard.client.DashBoardApp::checkToken(Ljava/lang/String;)(token);
        };

    }-*/;


    /**
     * Called by the application entry point ( Currently DashBoardApp::onModuleLoad() )
     */
    public static void startup() {
        exports();
    }

    // Callable from server side, and from non-static JS definition from exports() below
    public static int interfaceType() {
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        int interfaceType = loginResult.getUserInterfaceType();
        return interfaceType;
    }

    // Because the interfaceType is a bitmask, break it down for readability...
    public static boolean useRedux() {
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        int interfaceType = loginResult.getUserInterfaceType();
        return (interfaceType & 1) == 1;        // Return bit 1
    }

    // Because the interfaceType is a bitmask, break it down for readability...
    public static boolean userSelectable() {
        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
        int interfaceType = loginResult.getUserInterfaceType();
        return (interfaceType & 512) == 512;    // Return bit 9
    }


    /**
     * Single place to handle all JSNI exports for GWT
     */
    public static native void exports() /*-{
        
        // Context launchers...


            
        $wnd.GWT.activity           = @com.gsihealth.dashboard.client.DashBoardApp::restartTimer(*);
        $wnd.GWT.ResCenter          = @com.gsihealth.dashboard.client.DashBoardApp::handleResCenterClick(*);
        $wnd.GWT.Logout             = @com.gsihealth.dashboard.client.DashBoardApp::handleLogoutClick(*);
        $wnd.GWT.Prefs              = @com.gsihealth.dashboard.client.DashBoardApp::handlePrefsClick(*);
        $wnd.GWT.Help               = @com.gsihealth.dashboard.client.DashBoardApp::handleHelpClick(*);
        $wnd.GWT.isDevmode          = @com.gsihealth.dashboard.client.DashBoardApp::reduxIsDevmode(*);       
            
        // Application launchers...
            
        $wnd.GWT.appAdmin           = @com.gsihealth.dashboard.client.DashBoardApp::showAdminPanel(*);
        $wnd.GWT.appEnrollment      = @com.gsihealth.dashboard.client.DashBoardApp::showEnrollment(*);
        $wnd.GWT.appCareteams       = @com.gsihealth.dashboard.client.DashBoardApp::showCareTeamPanel(*);
        $wnd.GWT.appReports         = @com.gsihealth.dashboard.client.DashBoardApp::showReportsPanel(*);
        //$wnd.GWT.appCareplan        = @com.gsihealth.dashboard.client.dashboard.DashboardPanel::showTreatWindow(*);
        $wnd.GWT.appMessages        = @com.gsihealth.dashboard.client.dashboard.DashboardPanel::showMessageAppWindow(*);
        $wnd.GWT.appAlerts          = @com.gsihealth.dashboard.client.DashBoardApp::showAlertPanel(*);
        $wnd.GWT.appPopManager      = @com.gsihealth.dashboard.client.dashboard.DashboardPanel::showPopulationManagerWindow(*);
        $wnd.GWT.appCarebook        = @com.gsihealth.dashboard.client.dashboard.DashboardPanel::showCarebookAppWindow(*);
        $wnd.GWT.appPatientList     = @com.gsihealth.dashboard.client.dashboard.DashboardPanel::showPatientListAppWindow(*);
        $wnd.GWT.appUHCEnrollment   = @com.gsihealth.dashboard.client.dashboard.DashboardPanel::showPopulationManagerWindow(*);
        $wnd.GWT.appEngagement      = @com.gsihealth.dashboard.client.dashboard.DashboardPanel::showAvadoWindow(*);

        $wnd.GWT.appAdminClose           = @com.gsihealth.dashboard.client.DashBoardApp::closeAdminPanel(*);
        $wnd.GWT.appEnrollmentClose      = @com.gsihealth.dashboard.client.DashBoardApp::closeEnrollment(*);
        $wnd.GWT.appCareteamsClose       = @com.gsihealth.dashboard.client.DashBoardApp::closeCareTeamPanel(*);
        $wnd.GWT.appReportsClose         = @com.gsihealth.dashboard.client.DashBoardApp::closeReportsPanel(*);
        $wnd.GWT.appAlertsClose          = @com.gsihealth.dashboard.client.DashBoardApp::closeAlertsPanel(*);

        $wnd.GWT.appCareBookSearchClose = @com.gsihealth.dashboard.client.DashBoardApp::closeCareBookPatientSearch(*);

        $wnd.GWT.globalSearch        = function(name) {
            console.log('Test name:' + name);
            @com.gsihealth.dashboard.client.dashboard.DashboardPanel::globalSearch(Ljava/lang/String;)(name);
        };

        $wnd.GWT.handleSelectedPatientInSearch        = function(data) {
            console.log('AHHHHHHHHHHHHHHH Test Data PatientId:' + data);
            @com.gsihealth.dashboard.client.dashboard.DashboardPanel::handleSelectedPatientInSearch(Ljava/lang/String;)(data);
        };

        $wnd.GWT.searchPatientsByFirstnLastName        = function(firstName, lastName) {
            @com.gsihealth.dashboard.client.dashboard.DashboardPanel::searchPatientsByFirstnLastName(Ljava/lang/String;Ljava/lang/String;)(firstName, lastName);
        };

        $wnd.GWT.openCarebook        = function(patientId) {
            console.log('Open careBook with patientId:' + patientId);
            @com.gsihealth.dashboard.client.dashboard.DashboardPanel::appCarebookPatientOpen(Ljava/lang/String;)(patientId);
        };

        $wnd.GWT.openEnrollmentEdit        = function(patientId) {
            console.log('Open careBook with patientId:' + patientId);
            @com.gsihealth.dashboard.client.dashboard.DashboardPanel::appEnrollmentEditOpen(Ljava/lang/String;)(patientId);
        };

        $wnd.GWT.openEnrollmentAdd        = function(data) {
            console.log('Open enrollment add: ' + data);
            @com.gsihealth.dashboard.client.dashboard.DashboardPanel::appAddPatientOpen(Ljava/lang/String;)(data);
        };

        $wnd.GWT.openCarePlanDirect      = function(destination, patientId) {
            console.log('Open Careplan with patientId:' + patientId);
            @com.gsihealth.dashboard.client.dashboard.DashboardPanel::showTreatWindow(Ljava/lang/String;Ljava/lang/String;Z)(destination, patientId, true);
        };
        $wnd.GWT.appCareplan = function () {
            @com.gsihealth.dashboard.client.dashboard.DashboardPanel::showTreatWindow(Ljava/lang/String;Ljava/lang/String;Z)("homePage", "", false);
        };

        $wnd.GWT.showReportDetails = function (reportId) {
            @com.gsihealth.dashboard.client.DashBoardApp::showReportDetails(Ljava/lang/String;)(reportId);
        };

        $wnd.GWT.setPentahoCalled = function (called) {
            @com.gsihealth.dashboard.client.dashboard.DashboardPanel::setPentahoCalled(Z)(called);
        };

        $wnd.GWT.getStatusEffectiveDateEditable = @com.gsihealth.dashboard.client.DashBoardApp::getStatusEffectiveDateEditable();

        $wnd.GWT.resizeReportDetails = @com.gsihealth.dashboard.client.DashBoardApp::resizeReportDialog(*);


        $wnd.GWT.resizeTab          = @com.gsihealth.dashboard.client.DashBoardApp::resizeTab(*);

        $wnd.GWT.interfaceType      = @com.gsihealth.dashboard.redux.Redux::interfaceType(*);

        // Misc. launchers...
            
        $wnd.GWT.test               = @com.gsihealth.dashboard.client.dashboard.DashboardPanel::angularGWT(*);


    }-*/;
}
