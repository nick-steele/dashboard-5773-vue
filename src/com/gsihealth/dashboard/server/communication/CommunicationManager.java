package com.gsihealth.dashboard.server.communication;

import com.sun.jersey.api.client.ClientResponse.Status;
import java.net.URI;
import java.util.Map;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.representation.Form;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Main CommunicationManager to handle SSL communication for GET, POST HTTP
 * methods.
 *
 * @author Vishal
 *
 */
public final class CommunicationManager {

    private Logger logger = Logger.getLogger(getClass().getName());

    private static HashMap<URI, WebResource> resources = new HashMap();

    public CommunicationManager() {
    }

    // SSL
    /**
     * Used for requesting a GET call on REST API URI server.
     *
     * @param uri
     * @return
     */
    public ClientResponse callGET(URI uri) throws Exception {
        return callGET(uri, "text/plain");
    }
    
    // SSL
    /**
     * Used for requesting a GET call on REST API URI server.
     *
     * @param uri
     * @return
     */
    public ClientResponse callGET(URI uri, String contentType) throws Exception {

        // for SSL
        Client client = Client.create();

        WebResource webResource = client.resource(uri);

        ClientResponse response = webResource.accept(contentType).get(ClientResponse.class);

        Status clientResponseStatus = response.getClientResponseStatus();

        if (clientResponseStatus != Status.OK) {
            throw new Exception("Error: " + clientResponseStatus + ",  " + clientResponseStatus.getReasonPhrase());
        }

        return response;
    }

    /**
     * Used for requesting a POST call on REST API URI server.
     *
     * @param uri
     * @param putValues
     * @return
     */
    public ClientResponse callPOST(URI uri, Map<String, String> putValues) throws Exception {

        Client client = Client.create();
        WebResource webResource = client.resource(uri);

        ClientResponse clientResponse = null;
        if (putValues != null) {
            Form formData = new Form();
            for (String key : putValues.keySet()) {
                String value = putValues.get(key);
                formData.add(key, value);
            }
            clientResponse = webResource.type("application/x-www-form-urlencoded").post(ClientResponse.class, formData);
        } else {
            clientResponse = webResource.type("application/x-www-form-urlencoded").post(ClientResponse.class);
        }

        Status clientResponseStatus = clientResponse.getClientResponseStatus();

        if (clientResponseStatus != Status.OK) {
            throw new Exception("Error: " + clientResponseStatus + ",  " + clientResponseStatus.getReasonPhrase());
        }

        return clientResponse;
    }

    /**
     * Used for requesting a PUT call on REST API URI server.
     *
     * @param uri
     * @param putValues
     * @return
     */
    public ClientResponse callPUT(URI uri, Map<String, String> putValues) throws Exception {

        Client client = Client.create();
        WebResource webResource = client.resource(uri);

        ClientResponse clientResponse = null;
        if (putValues != null) {
            Form formData = new Form();
            for (String key : putValues.keySet()) {
                String value = putValues.get(key);
                formData.add(key, value);
            }
            clientResponse = webResource.type("application/x-www-form-urlencoded").put(ClientResponse.class, formData);
        } else {
            clientResponse = webResource.type("application/x-www-form-urlencoded").put(ClientResponse.class);
        }

        Status clientResponseStatus = clientResponse.getClientResponseStatus();

        if (clientResponseStatus != Status.OK) {
            throw new Exception("Error: " + clientResponseStatus + ",  " + clientResponseStatus.getReasonPhrase());
        }

        return clientResponse;
    }

    /**
     * Used for requesting a PUT call on REST API URI server.
     *
     * @param uri
     * @param putValues
     * @return
     */
    public ClientResponse callPUTJson(URI uri, String putJson) throws Exception {

        Client client = Client.create();
        WebResource webResource = client.resource(uri);

        ClientResponse clientResponse = webResource.type("application/json").put(ClientResponse.class, putJson);

        Status clientResponseStatus = clientResponse.getClientResponseStatus();

        if (clientResponseStatus != Status.OK) {
            throw new Exception("Error: " + clientResponseStatus + ",  " + clientResponseStatus.getReasonPhrase());
        }

        return clientResponse;
    }
    
    public String callValidatePOST(URL uri) throws Exception {

        OutputStreamWriter wr = null;
        BufferedReader rd = null;
        HttpURLConnection conn = null;
        try {

            conn = (HttpURLConnection) uri.openConnection();
            conn.setReadTimeout(3000);
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            wr = new OutputStreamWriter(conn.getOutputStream());
           // wr.write(data);
            wr.flush();

            // Get the response
            rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            String line;
            StringBuilder value = new StringBuilder("");
            while ((line = rd.readLine()) != null) {

                value.append(line);

            }

            return value.toString();
        } finally {
            if (rd != null) {
                rd.close();
            }

            if (wr != null) {
                wr.close();
            }
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    public ClientResponse callLoginPOST(URI uri, Map<String, String> putValues) throws Exception {

        // Client client = Client.create();
        // WebResource webResource = client.resource(uri);
        WebResource login = null;
        if (resources.containsKey(uri)) {
            login = resources.get(uri);
        } else {
            Client client = Client.create();
            login = client.resource(uri);
            resources.put(uri, login);
        }

        ClientResponse clientResponse = null;
        if (putValues != null) {
            Form formData = new Form();
            for (String key : putValues.keySet()) {
                String value = putValues.get(key);
                formData.add(key, value);
            }

            clientResponse = login.type("application/x-www-form-urlencoded").post(ClientResponse.class, formData);

        } else {
            clientResponse = login.type("application/x-www-form-urlencoded").post(ClientResponse.class);
        }

        Status clientResponseStatus = clientResponse.getClientResponseStatus();

        if (clientResponseStatus != Status.OK) {
            throw new Exception("Error: " + clientResponseStatus + ",  " + clientResponseStatus.getReasonPhrase());
        }

        return clientResponse;
    }

    public ClientResponse callLogoutPOST(URI uri, Map<String, String> putValues) throws Exception {

        WebResource logout = null;
        if (resources.containsKey(uri)) {
            logout = resources.get(uri);
        } else {
            Client client = Client.create();
            logout = client.resource(uri);
            resources.put(uri, logout);
        }

        ClientResponse clientResponse = null;
        if (putValues != null) {
            Form formData = new Form();
            for (String key : putValues.keySet()) {
                String value = putValues.get(key);
                formData.add(key, value);
            }
            clientResponse = logout.type("application/x-www-form-urlencoded").post(ClientResponse.class, formData);
        } else {
            clientResponse = logout.type("application/x-www-form-urlencoded").post(ClientResponse.class);
        }

        Status clientResponseStatus = clientResponse.getClientResponseStatus();

        if (clientResponseStatus != Status.OK) {
            throw new Exception("Error: " + clientResponseStatus + ",  " + clientResponseStatus.getReasonPhrase());
        }

        return clientResponse;
    }
}
