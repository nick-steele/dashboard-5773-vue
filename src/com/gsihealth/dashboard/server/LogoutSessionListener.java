package com.gsihealth.dashboard.server;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 *
 * @author Chad Darby
 */
public class LogoutSessionListener implements HttpSessionListener {

    public void sessionCreated(HttpSessionEvent sessionEvent) {

    }

    public void sessionDestroyed(HttpSessionEvent sessionEvent) {

    }

}
