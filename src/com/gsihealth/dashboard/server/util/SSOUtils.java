package com.gsihealth.dashboard.server.util;

import com.gsihealth.entity.OrganizationCwd;
import com.gsihealth.entity.User;
import com.gsihealth.entity.UserCommunityOrganization;
import java.net.URLEncoder;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author edwin
 */
public class SSOUtils {

    private Logger logger = Logger.getLogger(getClass().getName());

    public SSOUtils() {
    }

    public StringBuilder buildSsoUser(User user, String userPassword, String directAddress, UserCommunityOrganization userCommunityOrganization, OrganizationCwd organization, String patientGroup, boolean isExistingUser) throws Exception {

        StringBuilder ssoUser = new StringBuilder();

        ssoUser.append("&identity_name=" + encode(user.getEmail()));
        ssoUser.append("&identity_attribute_names=cn&identity_attribute_values_cn=" + encode(user.getFirstName()) + "%20" + encode(user.getLastName()));
        ssoUser.append("&identity_attribute_names=givenname&identity_attribute_values_givenname=" + encode(user.getFirstName()));
        ssoUser.append("&identity_attribute_names=sn&identity_attribute_values_sn=" + encode(user.getLastName()));
        //JIRA 1853
        //password change will be handled by REST call to UserManager
        //for add thought, we will still need this
        if (!userPassword.equals("") && !isExistingUser) {
            ssoUser.append(getSsoStringForPassword(userPassword));
        }

        ssoUser.append("&identity_realm=%2F&identity_type=user");

        //custom Attributes
        String middleName = getDefaultString(user.getMiddleName());
        ssoUser.append("&identity_attribute_names=middleInitial&identity_attribute_values_middleInitial=" + encode(middleName));

        String telephoneNumber = getDefaultString(user.getTelephoneNumber());
        ssoUser.append("&identity_attribute_names=telephoneNumber&identity_attribute_values_telephoneNumber=" + encode(telephoneNumber));

        String phoneExt = getDefaultString(user.getPhoneExt());
        ssoUser.append("&identity_attribute_names=telephoneExtension&identity_attribute_values_telephoneExtension=" + encode(phoneExt));

        ssoUser.append("&identity_attribute_names=signingCredentials&identity_attribute_values_signingCredentials=" + encode(userCommunityOrganization.getCredentials()));

        ssoUser.append("&identity_attribute_names=customGroups&identity_attribute_values_customGroups=" + encode(String.valueOf(userCommunityOrganization.getAccessLevel().getAccessLevelPK().getAccessLevelId())));
        //Last Update Date      
        ssoUser.append("&identity_attribute_names=lastUpdated&identity_attribute_values_lastUpdated=" + encode(user.getLastUpdateDate().toString()));

        //Organization name , and Organization OID
        String oid = getDefaultString(organization.getOid());
        logger.info("add user: oid=" + organization.getOid() + " orgName=" + organization.getOrganizationCwdName()
                + " orgID=" + organization.getOrgId());
        ssoUser.append("&identity_attribute_names=orgName&identity_attribute_values_orgName=" + encode(organization.getOrganizationCwdName()));
        ssoUser.append("&identity_attribute_names=organizationId&identity_attribute_values_organizationId=" + encode(oid));

        //Community OrganizationCwd - right now it is MMC = 1
        if (userCommunityOrganization != null) {
            String communityName = getDefaultString(userCommunityOrganization.getCommunity().getCommunityName());
            String communityId = getDefaultString(userCommunityOrganization.getCommunity().getCommunityId().toString());
            logger.info("add user: community id=" + communityId + " community Name=" + communityName);
            ssoUser.append("&identity_attribute_names=community&identity_attribute_values_community=" + encode(communityId));
        }
        // Add support for Direct Address
        directAddress = getDefaultString(directAddress);
        logger.info("add user: directAddress=" + directAddress);
        ssoUser.append("&identity_attribute_names=directAddress&identity_attribute_values_directAddress=" + encode(directAddress));
        ssoUser.append("&identity_attribute_names=mail&identity_attribute_values_mail=" + encode(directAddress)); // Transitional:  unused mail attribute for directAddress

        //Add Health Home Role 
        String healthhomerole = getDefaultString(Long.toString(userCommunityOrganization.getRole().getRoleId()));
        logger.info("add user: healthhomerole=" + healthhomerole);
        ssoUser.append("&identity_attribute_names=healthhomerole&identity_attribute_values_healthhomerole=" + encode(healthhomerole));

        //Add Access Level
        String accesslevel = encode(String.valueOf(userCommunityOrganization.getAccessLevel().getAccessLevelPK().getAccessLevelId()));
        logger.info("add user: accesslevel=" + accesslevel);
        ssoUser.append("&identity_attribute_names=accesslevel&identity_attribute_values_accesslevel=" + encode(accesslevel));

        //Add OpenAM group attribute for "petient" groups" if set in dashboard.properties       
        if (StringUtils.isNotBlank(patientGroup)) {
            logger.info("add user: identity_groups=" + patientGroup);
            ssoUser.append("&identity_groups=" + patientGroup);
        }

        return ssoUser;
    }

    protected String encode(String value) throws Exception {
        if (value != null) {
            value = URLEncoder.encode(value, "UTF-8");
        } else {
            value = "";
        }
        return value;
    }

    private String getDefaultString(String data) {
        return StringUtils.defaultString(data);
    }

    public StringBuilder buildSsoPatientUser(User user, String userPassword, String directAddress, UserCommunityOrganization userCommunityOrganization, OrganizationCwd organization, String patientGroup, String patientEngaRole, String patientUser) throws Exception {

        StringBuilder ssoUser = new StringBuilder();

        ssoUser.append("&identity_name=" + encode(patientUser));
        ssoUser.append("&identity_attribute_names=cn&identity_attribute_values_cn=" + encode(user.getFirstName()) + "%20" + encode(user.getLastName()));
        ssoUser.append("&identity_attribute_names=givenname&identity_attribute_values_givenname=" + encode(user.getFirstName()));
        ssoUser.append("&identity_attribute_names=sn&identity_attribute_values_sn=" + encode(user.getLastName()));
        if (!userPassword.equals("")) {
            ssoUser.append("&identity_attribute_names=userpassword&identity_attribute_values_userpassword=" + encode(userPassword));
        }
        ssoUser.append("&identity_realm=%2F&identity_type=user");

        //custom Attributes
        String middleName = getDefaultString(user.getMiddleName());
        ssoUser.append("&identity_attribute_names=middleInitial&identity_attribute_values_middleInitial=" + encode(middleName));

        String telephoneNumber = getDefaultString(user.getTelephoneNumber());
        ssoUser.append("&identity_attribute_names=telephoneNumber&identity_attribute_values_telephoneNumber=" + encode(telephoneNumber));

        String phoneExt = getDefaultString(user.getPhoneExt());
        ssoUser.append("&identity_attribute_names=telephoneExtension&identity_attribute_values_telephoneExtension=" + encode(phoneExt));

        ssoUser.append("&identity_attribute_names=signingCredentials&identity_attribute_values_signingCredentials=" + encode(userCommunityOrganization.getCredentials()));
        ssoUser.append("&identity_attribute_names=customGroups&identity_attribute_values_customGroups=" + encode(String.valueOf(userCommunityOrganization.getAccessLevel().getAccessLevelPK().getAccessLevelId())));
        //Last Update Date      
        ssoUser.append("&identity_attribute_names=lastUpdated&identity_attribute_values_lastUpdated=" + encode(user.getLastUpdateDate().toString()));

        //Organization name , and Organization OID
        String oid = getDefaultString(organization.getOid());
        logger.info("add Patient user: oid=" + organization.getOid() + " orgName=" + organization.getOrganizationCwdName()
                + " orgID=" + organization.getOrgId());
        ssoUser.append("&identity_attribute_names=orgName&identity_attribute_values_orgName=" + encode(organization.getOrganizationCwdName()));
        ssoUser.append("&identity_attribute_names=organizationId&identity_attribute_values_organizationId=" + encode(oid));

        //Community Organization - right now it is MMC = 1
        if (userCommunityOrganization != null) {
            String communityName = getDefaultString(userCommunityOrganization.getCommunity().getCommunityName());
            String communityId = getDefaultString(userCommunityOrganization.getCommunity().getCommunityId().toString());
            logger.info("add Patient user: community id=" + communityId + " community Name=" + communityName);
            ssoUser.append("&identity_attribute_names=community&identity_attribute_values_community=" + encode(communityId));
        }
        // Add support for Direct Address
        directAddress = getDefaultString(directAddress);
        logger.info("add Patient user: directAddress=" + directAddress);
        ssoUser.append("&identity_attribute_names=directAddress&identity_attribute_values_directAddress=" + encode(directAddress));
        ssoUser.append("&identity_attribute_names=mail&identity_attribute_values_mail=" + encode(directAddress)); // Transitional:  unused mail attribute for directAddress

        //Add Health Home Role 
        String healthhomerole = getDefaultString(patientEngaRole.toString());
        logger.info("add Patient user: healthhomerole=" + healthhomerole);
        ssoUser.append("&identity_attribute_names=healthhomerole&identity_attribute_values_healthhomerole=" + encode(healthhomerole));

        //Add Access Level
        String accesslevel = encode(String.valueOf(userCommunityOrganization.getAccessLevel().getAccessLevelPK().getAccessLevelId()));
        logger.info("add Patient user: accesslevel=" + accesslevel);
        ssoUser.append("&identity_attribute_names=accesslevel&identity_attribute_values_accesslevel=" + encode(accesslevel));

        //Add OpenAM group attribute for "petient" groups" if set in dashboard.properties       
        if (StringUtils.isNotBlank(patientGroup)) {
            logger.info("add Patient user: identity_groups=" + patientGroup);
            ssoUser.append("&identity_groups=" + patientGroup);
        }

        return ssoUser;
    }
    
    public String getSsoStringForPassword(String pwd) throws Exception
    {
        return "&identity_attribute_names=userpassword&identity_attribute_values_userpassword=" + encode(pwd);
    }
}
