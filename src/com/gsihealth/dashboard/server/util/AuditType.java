package com.gsihealth.dashboard.server.util;

/**
 *
 * @author Chad Darby
 */
public interface AuditType {

    public static final String ADDED_PATIENT = "Added Patient";
    public static final String UPDATED_PATIENT = "Updated Patient";
    public static final String UPDATE_PATIENT_VIA_SELF_ASSERT="Update Patient via Self Assert";

    public static final String ADDED_USER = "Added User";
    public static final String UPDATED_USER = "Updated User";

    public static final String USER_LOGGED_IN = "User Logged In";
    public static final String USER_LOGGED_OUT = "User Logged Out";

    public static final String ADDED_ORGANIZATION = "Added Organization";
    public static final String UPDATED_ORGANIZATION = "Updated Organization";

    public static final String ACCEPTED_EULA = "Accepted EULA";
    public static final String DECLINED_EULA = "Declined EULA";
    
    public static final String ACCEPTED_SAMHSA = "Accepted SAMHSA";
    public static final String DECLINED_SAMHSA = "Declined SAMHSA";

    public static final String OUTBOUND_PIX_FEED = "Sent PIX Feed";
    public static final String OUTBOUND_XACML_CONSENT = "Sent XACML Feed";
    
    public static final String ALERT_APP_ACCESSED = "Alert App Accessed";

    String ADDED_SUPERVISOR = "Added Supervisor";
    String UPDATED_SUPERVISOR = "Updated Supervisor";
    String REMOVED_SUPERVISOR = "Removed Supervisor";
}
