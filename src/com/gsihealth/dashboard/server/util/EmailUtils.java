package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import java.util.Properties;
import java.util.logging.Logger;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Utility class for sending email
 * 
 * @author Chad Darby
 */
public class EmailUtils {

    private Logger logger = Logger.getLogger(getClass().getName());
    
    private Authenticator authenticator;
  
    private Properties mailProperties;
    
    public EmailUtils(long communityId) {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        
        String mailServerUserName = configurationDAO.getProperty(communityId, "mail.coordinator.mailserver.username");
        String mailServerPassword = configurationDAO.getProperty(communityId, "mail.coordinator.mailserver.password");
        
        authenticator = new SMTPAuthenticator(mailServerUserName, mailServerPassword);
        
        String theMailHost = configurationDAO.getProperty(communityId, "mail.smtp.host");
        String theMailPort = configurationDAO.getProperty(communityId, "mail.smtp.port");
        String theMailAuth = configurationDAO.getProperty(communityId, "mail.smtp.auth");

        mailProperties = new Properties();
        mailProperties.setProperty("mail.smtp.host", theMailHost);
        mailProperties.setProperty("mail.smtp.port", theMailPort);
        mailProperties.setProperty("mail.smtp.auth", theMailAuth);        
    }

    /**
     *  Sends a simple text e-mail message.
     *
     *  @param smtpHost the SMTP mail server
     *  @param fromEmail address of the sender (ie maxwell@grandpuba.com)
     *  @param toEmail e-mail address of the recipient (ie eric@eazye.com)
     *  @param subject the subject
     *  @param messageText the message text
     *
     *  @throws javax.mail.MessagingException errors sending message
     */
    public void sendMessage(
            String fromEmail,
            String toEmail,
            String subject,
            String messageText) throws javax.mail.MessagingException {

        String[] toEmailList = {toEmail};

        sendMessage(fromEmail, toEmailList, subject, messageText);
    }

    /**
     *  Sends a simple text e-mail message to a list of recipients
     *
     *  @param smtpHost the SMTP mail server
     *  @param fromEmail address of the sender (ie maxwell@grandpuba.com)
     *  @param toEmailList e-mail addresses of the recipients
     *  @param subject the subject
     *  @param messageText the message text
     *
     *  @throws javax.mail.MessagingException errors sending message
     */
    public void sendMessage(
            String fromEmail,
            String[] toEmailList,
            String subject,
            String messageText)
            throws javax.mail.MessagingException {

        // start a session with given properties
        Session mailSession = Session.getDefaultInstance(mailProperties, authenticator);
        InternetAddress fromAddress = new InternetAddress(fromEmail);

        // construct a message
        MimeMessage myMessage = new MimeMessage(mailSession);
        myMessage.setFrom(fromAddress);

        for (int i = 0; i < toEmailList.length; i++) {
            myMessage.addRecipient(
                    javax.mail.Message.RecipientType.TO,
                    new InternetAddress(toEmailList[i]));
        }

        myMessage.setSentDate(new java.util.Date());
        myMessage.setSubject(subject);
        myMessage.setText(messageText);

        // now send the message!
        logger.info("Sending email to: " + getAllRecipients(myMessage));
        Transport.send(myMessage);
        logger.info("Sent email successfully at " + new java.util.Date());
    }

    private String getAllRecipients(Message message) throws javax.mail.MessagingException {
        StringBuilder result = new StringBuilder();

        Address[] addresses = message.getAllRecipients();
        for (int i = 0; i < addresses.length; i++) {
            InternetAddress temp = (InternetAddress) addresses[i];
            result.append(temp.getAddress() + "  ");
        }

        return result.toString();
    }

    private class SMTPAuthenticator extends javax.mail.Authenticator {

        private String userName;
        private String password;
        
        SMTPAuthenticator(String theUserName, String thePassword) {
            userName = theUserName;
            password = thePassword;
        }
        
        @Override
        protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(userName, password);
        }        
    }
}
