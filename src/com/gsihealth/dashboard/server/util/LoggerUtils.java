package com.gsihealth.dashboard.server.util;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chad.Darby
 */
public class LoggerUtils {

    /**
     * Obtain the current level of the Logger instance. When the level is not
     * set explicitly the level will be inherited from the parent logger.
     *
     * @param logger
     * @return
     */
    public static Level getLevel(Logger logger) {
        Level level = logger.getLevel();

        if (level == null && logger.getParent() != null) {
            level = logger.getParent().getLevel();
        }

        return level;
    }

    /**
     * Return true if debug level (CONFIG, FINE, FINER, FINEST)
     * 
     * @param logger
     * @return 
     */
    public static boolean isDebug(Logger logger) {

        boolean result = false;

        Level loggerLevel = LoggerUtils.getLevel(logger);

        result = (loggerLevel != null 
                && (loggerLevel.equals(Level.CONFIG) || 
                loggerLevel.equals(Level.FINE) || 
                loggerLevel.equals(Level.FINER) || 
                loggerLevel.equals(Level.FINEST)));

        return result;
    }
}
