package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.entity.Audit;
import com.gsihealth.entity.PatientCommunityEnrollment;
import com.gsihealth.entity.User;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.WebConstants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Chad Darby
 */
public class AuditUtils {

    public static Audit convertUser(LoginResult loginResult, User user) {
        Audit audit = new Audit();

        audit.setUserAccessLevel(loginResult.getAccessLevel());
        audit.setUserEmail(loginResult.getEmail());
        audit.setUserFirstName(loginResult.getFirstName());
        audit.setUserLastName(loginResult.getLastName());
        audit.setUserId(loginResult.getUserId());
        audit.setUserOrganizationName(loginResult.getOrganizationName());
        audit.setUserRole(loginResult.getUserLevel());

        audit.setTargetUserFirstName(user.getFirstName());
        audit.setTargetUserLastName(user.getLastName());

        return audit;
    }

    public static Audit convertPerson(LoginResult loginResult, Person person) {
        Audit audit = new Audit();

        audit.setUserAccessLevel(loginResult.getAccessLevel());
        audit.setUserEmail(loginResult.getEmail());
        audit.setUserFirstName(loginResult.getFirstName());
        audit.setUserLastName(loginResult.getLastName());
        audit.setUserId(loginResult.getUserId());
        audit.setUserOrganizationName(loginResult.getOrganizationName());
        audit.setUserRole(loginResult.getUserLevel());

        audit.setPatientFirstName(person.getFirstName());
        audit.setPatientLastName(person.getLastName());
        audit.setPatientEuid(person.getEuid());

        PatientCommunityEnrollment patientEnrollment = person.getPatientCommunityEnrollment();
        if (patientEnrollment != null) {
            audit.setPatientOrganizationName(patientEnrollment.getOrganizationName());
        }
        
        return audit;
    }

    public static LoginResult getLoginResult(HttpServletRequest request) {
    	if(request != null) {
	        HttpSession session = request.getSession();
	        LoginResult loginResult = (LoginResult) session.getAttribute(WebConstants.LOGIN_RESULT_KEY);
	        
	        return loginResult;
    	} else {
    		return null;
    	}
    }


}
