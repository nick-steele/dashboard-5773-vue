/**
 * 
 */
package com.gsihealth.dashboard.server.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Vishal (Off.)
 * 
 */
public enum DateUtilFactory {

		DD_MM_YYYY {
			@Override
			public String convertDate(Date date, String delimiter) {
				dateFormat = new SimpleDateFormat("dd"+delimiter+"MM"+delimiter+"yyyy");
				return new StringBuilder(dateFormat.format(date)).toString();
			}
		},
		MM_DD_YYYY {
			@Override
			public String convertDate(Date date, String delimiter) {
				dateFormat = new SimpleDateFormat("MMddyyyy");				
				return new StringBuilder(dateFormat.format(date)).toString();
			}
		},
		DD_MM_YY {
			@Override
			public String convertDate(Date date, String delimiter) {
				// TODO Auto-generated method stub
				return null;
			}
		},
		MM_DD_YY {
			@Override
			public String convertDate(Date date, String delimiter) {
				// TODO Auto-generated method stub
				return null;
			}
		};

		private static SimpleDateFormat dateFormat;
		public abstract String convertDate(Date date, String delimiter);	

}
