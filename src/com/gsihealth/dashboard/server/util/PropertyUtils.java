package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.entity.AccessLevel;
import com.gsihealth.entity.Community;
import com.gsihealth.entity.OrganizationCwd;
import com.gsihealth.entity.Role;
import com.gsihealth.entity.User;
import com.gsihealth.entity.UserCommunityOrganization;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.gsihealth.dashboard.server.dao.UserCommunityOrganizationDAO;
import com.gsihealth.patientmdminterface.util.PatientUtils;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;

/**
 * Conversion utilities for patient search
 *
 * @author Chad Darby
 */
public class PropertyUtils {

    private static DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
    private static final Logger logger = Logger.getLogger(PropertyUtils.class.getName());


   
    /**
     * FIXME IS THIS NEEDED?
     * @param person
     * @return 
     */
    public static SearchCriteria convertForDuplicateSearch(SimplePerson person) throws ParseException {
        SearchCriteria searchCriteria = new SearchCriteria();

        searchCriteria.setFirstName(person.getFirstName());
        searchCriteria.setLastName(person.getLastName());
        searchCriteria.setGender(person.getGenderCode());

        Date dob = com.gsihealth.patientmdminterface.util.PatientUtils.convertStrToDate(person.getBirthDateTime());
        if (dob != null) {
            String dobStr = formatter.format(dob);
            searchCriteria.setDateOfBirthStr(dobStr);
        }
        logger.severe("DO WE NEED THIS??");
        return searchCriteria;
    }

    /**
     * Convert search criteria to person
     *
     * @param searchCriteria
     * @return
     * @throws Exception
     */
    public static SimplePerson convert(SearchCriteria searchCriteria) throws Exception {

        SimplePerson person = new SimplePerson();

        String firstName = StringUtils.trimToEmpty(searchCriteria.getFirstName());
        if (StringUtils.isNotBlank(firstName)) {
            person.setFirstName(firstName);
        }

        String middleName = StringUtils.trimToEmpty(searchCriteria.getMiddleName());
        if (StringUtils.isNotBlank(middleName)) {
            person.setMiddleName(middleName);
        }

        String lastName = StringUtils.trimToEmpty(searchCriteria.getLastName());
        if (StringUtils.isNotBlank(lastName)) {
            person.setLastName(lastName);
        }

        String dateOfBirthStr = StringUtils.trimToEmpty(searchCriteria.getDateOfBirthStr());
        person.setBirthDateTime(dateOfBirthStr);
        if(StringUtils.isNotBlank(dateOfBirthStr)) {
            person.setDateOfBirth(PatientUtils.convertStrToDate(dateOfBirthStr));
        } 
        
        String gender = searchCriteria.getGender();
        if (StringUtils.isNotBlank(gender)) {
            person.setGenderCode(gender);
        }

        String address1 = StringUtils.trimToEmpty(searchCriteria.getAddress1());
        if (StringUtils.isNotBlank(address1)) {
            person.setStreetAddress1(address1);
        }

        String address2 = StringUtils.trimToEmpty(searchCriteria.getAddress2());
        if (StringUtils.isNotBlank(address2)) {
            person.setStreetAddress2(address2);
        }

        String city = StringUtils.trimToEmpty(searchCriteria.getCity());
        if (StringUtils.isNotBlank(city)) {
            person.setCity(city);
        }

        String state = searchCriteria.getState();
        if (StringUtils.isNotBlank(state)) {
            person.setState(state);
        }

        String zipCode = StringUtils.trimToEmpty(searchCriteria.getZipCode());
        if (StringUtils.isNotBlank(zipCode)) {
            person.setZipCode(zipCode);
        }

        String phone = StringUtils.trimToEmpty(searchCriteria.getPhone());
        if (StringUtils.isNotBlank(phone)) {
            person.setPhoneHome(phone);
        }

        String ssn = StringUtils.trimToEmpty(searchCriteria.getSsn());
        if (StringUtils.isNotBlank(ssn)) {
            person.setSSN(ssn);
        }
        
        String minor = searchCriteria.getMinor();
        if (StringUtils.isNotBlank(minor)) {
            person.setMinor(minor);
        }

        return person;
    }

    
    /**
     * FIXME - wasteful
     * convert a bunch of users map them to communities - duplicate users will exist
     * @param users
     * @param app
     * @return a map of users by community
     * @throws Exception 
     */
     public static Map<Long, List<UserDTO>> convertToDTOMap(List<User> users, UserCommunityOrganizationDAO userCommunityOrganizationDAO) throws Exception {

        Map<Long, List<UserDTO>> userDTOMap = new HashMap<Long, List<UserDTO>>();
        List<UserDTO> userDTOList = convertToDTOs(users, userCommunityOrganizationDAO);
        for(UserDTO each : userDTOList){
            
            Long communityId = each.getCommunityId();
            if(userDTOMap.get(communityId) == null){
               userDTOMap.put(communityId, new ArrayList<UserDTO>()); 
            }
            userDTOMap.get(communityId).add(each);
        }

        return userDTOMap;
    }
     
     /** FIXME
      * convert users to dto - ignores community
      * @param users
      * @param userCommunityOrganizationDAO
      * @return
      * @throws Exception 
      */
     public static List<UserDTO> convertToDTOs(List<User> users, UserCommunityOrganizationDAO userCommunityOrganizationDAO) throws Exception {
        List<UserDTO> userDTOList = new ArrayList<UserDTO>();
        for (User user : users) {
            for(UserCommunityOrganization uco : user.getUserCommunityOrganization()){
                try {
                    UserDTO userDTO = populateUserDTO(user, uco);
                    userDTOList.add(userDTO);
                } catch (NullPointerException npe) {
                    logger.info("user number " + user.getUserId() + " can't be converted, discarding");
                }
            }
        }
        return userDTOList;
    }
    
     /**
      * convert users to dtos, community matters
      * @param users
      * @param userCommunityOrganizationDAO
      * @param communityId
      * @return
      * @throws Exception 
      */
    public static List<UserDTO> convertToDTOs(List<User> users, UserCommunityOrganizationDAO userCommunityOrganizationDAO, 
            Long communityId) throws Exception {

        List<UserDTO> userDTOList = new ArrayList<UserDTO>();

        for (User user : users) {
            try {
                UserDTO userDTO = populateUserDTO(user, communityId);
                userDTOList.add(userDTO);
            } catch (NullPointerException npe) {
                logger.info("user number " + user.getUserId() + " can't be converted, discarding");
            }
        }

        return userDTOList;

    }

    /**
     * add the user demographic data to a userDTO object
     * @param user
     * @return 
     */
    public static UserDTO populateUserDTODemographicData(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setUserId(user.getUserId());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setMiddleName(user.getMiddleName());
        userDTO.setLastName(user.getLastName());
        userDTO.setDateOfBirth(user.getDateOfBirth());
        userDTO.setStreetAddress1(user.getStreetAddress1());
        userDTO.setStreetAddress2(user.getStreetAddress2());
        userDTO.setWorkPhone(user.getTelephoneNumber());
        userDTO.setMobileNo(user.getMobileNumber());
        userDTO.setCity(user.getCity());
        userDTO.setState(user.getState());
        userDTO.setZipCode(user.getZipCode());
        userDTO.setEmail(user.getEmail());
        userDTO.setWorkPhoneExt(user.getPhoneExt());
        userDTO.setIsDeleted(user.getDeleted());
        userDTO.setMustChangePassword(user.getMustChangePassword());
        userDTO.setAccountLockedOut(user.getAccountLockedOut());
        userDTO.setFailedAccessAttempts(user.getFailedAccessAttempts());
        userDTO.setCreationDate(user.getCreationDate());
        userDTO.setEulaAccepted(user.getEulaAccepted());
        userDTO.setEulaDate(user.getEulaDate());
        userDTO.setEulaId(user.getEulaId());
        userDTO.setPrefix(user.getPrefix());
        userDTO.setLastUpdateDate(user.getLastUpdateDate());
        userDTO.setLastSuccessfulLoginDate(user.getLastSuccessfulLoginDate());
        userDTO.setLastPasswordChangeDate(user.getLastPasswordChangeDate());
        userDTO.setNpi(user.getNpi());
        userDTO.setEnabled(user.getEnabled());
        userDTO.setReportingRole(user.getReportingRole());
        userDTO.setDisableUserMfa(user.getDisableUserMfa());
        return userDTO;
    }
    
    /**
     * populate a uco object
     * @param user
     * @param uco
     * @param community
     * @param organization
     * @param accessLevel
     * @param startDate
     * @return 
     */
    public static UserCommunityOrganization populateUserUcoData(User user, UserCommunityOrganization uco, 
            Community community, OrganizationCwd organization, AccessLevel accessLevel, Role role, Date startDate,
            String directAddress, String status, String gender, String credentials, User supervisor) {
        uco.setAccessLevel(accessLevel);
        uco.setAccessLevelId(accessLevel.getAccessLevelPK().getAccessLevelId());
        uco.setRole(role);
        uco.setUser(user);
        uco.setCommunity(community);
        uco.setOrganizationCwd(organization);
        uco.setStartDate(startDate);
        uco.setDirectAddress(directAddress);
        uco.setStatus(status);
        uco.setGender(gender);
        uco.setCredentials(credentials);
        uco.setSupervisor(supervisor);
        return uco;
    }
    
    /**
     * convert a user to user dto
     * @param user
     * @param communityId
     * @return 
     */
    public static UserDTO populateUserDTO(User user, Long communityId) {
        UserDTO userDTO = populateUserDTODemographicData(user);
        UserCommunityOrganization uco = user.getUserCommunityOrganization(communityId);
        populateUserDTOUcoData(uco, userDTO);
        return userDTO;
    }
    
    public static UserDTO populateUserDTO(User user, UserCommunityOrganization uco) {
        UserDTO userDTO = populateUserDTODemographicData(user);
        populateUserDTOUcoData(uco, userDTO);
        return userDTO;
    }

    /**
     * add the uco data to a userDTO
     * @param uco
     * @param userDTO 
     */
    private static void populateUserDTOUcoData(UserCommunityOrganization uco, UserDTO userDTO) throws NullPointerException{
        Community community = uco.getCommunity();
        userDTO.setCommunityId(community.getCommunityId());
        OrganizationCwd organization = uco.getOrganizationCwd();
        userDTO.setOrgid(Long.toString(organization.getOrgId()));
        userDTO.setOrganization(organization.getOrganizationCwdName());
        userDTO.setOid(organization.getOid());
        userDTO.setStatus(uco.getStatus());
        userDTO.setGender(uco.getGender());
        userDTO.setCredentials(uco.getCredentials());
        userDTO.setSamhsaDate(uco.getSamhsaDate());

        AccessLevel accessLevel = uco.getAccessLevel();
        if (accessLevel != null) {
            userDTO.setAccessLevel(accessLevel.getAccessLevelDesc());
            userDTO.setAccessLevelId(Long.toString(accessLevel.getAccessLevelPK().getAccessLevelId()));
        }
        
        Role role = uco.getRole();
        if (role != null) {
            userDTO.setSecurityRole(role.getRoleName());
            userDTO.setSecurityRoleId(Long.toString(role.getPrimaryRoleId()));
            userDTO.setCanConsentEnrollMinors(role.isCanConsentEnrollMinors());
        }

        User supervisor = uco.getSupervisor();
        if (supervisor != null) {
            userDTO.setSupervisorId(supervisor.getUserId());
        }
    }

    public static SearchCriteria convertForDuplicateSearchModify(SimplePerson person) throws Exception {
        SearchCriteria searchCriteria = new SearchCriteria();

        searchCriteria.setFirstName(person.getFirstName());
        searchCriteria.setLastName(person.getLastName());
        searchCriteria.setGender(person.getGenderCode());

        Date dob = PatientUtils.convertStrToDate(person.getBirthDateTime());
        if (dob != null) {
            String dobStr = formatter.format(dob);
            searchCriteria.setDateOfBirthStr(dobStr);
        }

        return searchCriteria;
    }
     public static boolean isMinor(Date dateOfBirth, int minorAge) {
        Date dob = dateOfBirth;
        boolean isMinor = false;
        if(dob == null){
            return isMinor;
        }
        double age = getAge(dob);
        

        if (age < minorAge) {
            isMinor = true;
        } else {
            isMinor = false;
        }

        return isMinor;
    }

    public static double getAge(Date dateOfBirth) {

        Date dob = dateOfBirth;
        double age;
        long ageInMillis = new Date().getTime() - dob.getTime();

        age = ageInMillis / (365.25 * 24 * 60 * 60 * 1000);
        return age;

    }
}
