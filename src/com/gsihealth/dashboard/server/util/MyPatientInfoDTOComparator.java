package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.entity.dto.MyPatientInfoDTO;
import java.util.Comparator;

/**
 *
 * @author Chad.Darby
 */
public class MyPatientInfoDTOComparator implements Comparator<MyPatientInfoDTO>{

    @Override
    public int compare(MyPatientInfoDTO person1, MyPatientInfoDTO person2) {
        if(person1.getLastName()!=null &&
            person1.getFirstName()!=null &&
                person2.getLastName()!=null &&
                person2.getFirstName()!=null){
        String lastName1 = person1.getLastName().toUpperCase();
        String firstName1 = person1.getFirstName().toUpperCase();

        String lastName2 = person2.getLastName().toUpperCase();
        String firstName2 = person2.getFirstName().toUpperCase();

        if (!lastName1.equals(lastName2)) {
            return lastName1.compareTo(lastName2);
        }
        else {
            return firstName1.compareTo(firstName2);
        }
        }else{
            return 0;
        }
    }
    
}
