package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.common.dto.PersonDTO;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class PagingUtils {

    
    public static List<PersonDTO> getSublist(List<PersonDTO> theList, int pageNumber, int pageSize) {

        List<PersonDTO> sub = new ArrayList<PersonDTO>();

        int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);
        int endRecordNumber = startRecordNumber + pageSize;

        if (endRecordNumber > theList.size()) {
            endRecordNumber = theList.size();
        }

        for (int i = startRecordNumber; i < endRecordNumber; i++) {
            sub.add(theList.get(i));
        }

        return sub;
    }
    
}
