package com.gsihealth.dashboard.server.util;

import com.gsihealth.entity.FacilityType;
import com.gsihealth.entity.OrganizationCwd;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class OrgUtils {

    public static List<OrganizationDTO> convertToDTOs(List<OrganizationCwd> orgs,long communityId) {

        List<OrganizationDTO> orgDTOList = new ArrayList<OrganizationDTO>();
        
        for (OrganizationCwd org : orgs) {
            OrganizationDTO orgDTO = convertOrgToDTO(org,communityId);
            orgDTOList.add(orgDTO);
        }

        return orgDTOList;
    }
   //FIXME replace these with dozer 
    public static OrganizationDTO convertOrgToDTO(OrganizationCwd org,long communityId) {
       int facId = 0;
       OrganizationDTO orgDTO = null;
       orgDTO = new OrganizationDTO();
            
            orgDTO.setOrganizationId(org.getOrgId());
            orgDTO.setOID(org.getOid());
            if(org.getCommunityOrganization(communityId)!=null){
            orgDTO.setStatus(org.getCommunityOrganization(communityId).getStatus());
            facId = org.getCommunityOrganization(communityId).getFacilityTypeId();
            orgDTO.setFacilityId(String.valueOf(facId));
            }
            orgDTO.setOrganizationName(org.getOrganizationCwdName());
            orgDTO.setPhysicalStreetAddress1(org.getPhysicalStreetAddress1());
            orgDTO.setPhysicalStreetAddress2(org.getPhysicalStreetAddress2());
            orgDTO.setPhysicalCity(org.getPhysicalCity());
            orgDTO.setPhysicalState(org.getPhysicalAddressState());
            orgDTO.setPhysicalZipCode(org.getPhysicalZipCode());
            orgDTO.setWorkPhone(org.getTelephoneNumber());
            orgDTO.setFacilityTypeDesc(org.getFacilityTypeDesc());            
            return orgDTO;
    }
    
}
