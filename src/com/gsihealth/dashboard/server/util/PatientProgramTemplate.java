
package com.gsihealth.dashboard.server.util;

import com.gsihealth.entity.PatientProgramSheet;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author ssingh
 */
public class PatientProgramTemplate {
    
    private String[] topColumns;
    private XSSFSheet firstSheet;
    private XSSFWorkbook workbook;
    private static final Logger logger = Logger.getLogger(ClientTemplate.class.getName());
    
       
    private static final int LAST_NAME_COLUMN = 0;
    private static final int FIRST_NAME_COLUMN = 1;
    private static final int GENDER_COLUMN = 2;
    private static final int DOB_COLUMN = 3;
    private static final int ID_COLUMN = 4;
//    private static final int EUID_COLUMN = 5;
    private static final int PROGRAM_COLUMN = 6;
    //private static final int EFFECTIVE_COLUMN=7;
    //private static final int END_COLUMN=8;
    private static final int STATUS_COLUMN=7;
    private static final int STATUS_EFFECTIVE_COLUMN=8;
    private static final int PROGRAM_END_REASON_COLUMN=9;
    private static final int HEALTH_HOME_COLUMN=5;
    private boolean healthHomeRequired;
    private String healthHomeLabel;
    private String endReasonLabel;
    
     public PatientProgramTemplate(){
         
     }
    
    public PatientProgramTemplate(boolean isHealthHomeRequired, String theHealthHomeLabel,String theEndReasonLabel) {
        this.endReasonLabel=theEndReasonLabel;
        this.healthHomeLabel=theHealthHomeLabel;
        this.healthHomeRequired=isHealthHomeRequired;
        topColumns = new String[]{"Last Name","First Name","Gender","DOB","Patient ID",healthHomeLabel,"Program Name",/*"Program Effective","Program End",*/"Patient Status","Status Effective",endReasonLabel};
        createHeaderColumns();
    }  
    
     public static void main(String[] args) {
        PatientProgramTemplate template = new PatientProgramTemplate();
        //template.createTopColums();
        template.createHeaderColumns();

        /*
         * stream where the workbook content will be written to.
         */

         FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(new File("xyzPatientProgramTemplate.xlsx"));
            template.workbook.write(fos);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error: " + e.getMessage(), e);
        } finally {
            if (fos != null) {
                try {
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    logger.log(Level.SEVERE, "Error: " + e.getMessage(), e);
                }
            }
        }
    }
    private void createHeaderColumns() {
    
          try {
            workbook = new XSSFWorkbook();

             /*
             * Create one sheet in the excel document and name it Sheet
             */
            firstSheet = workbook.createSheet("Sheet1");
        /*
         * Bold font
         */
        
        Font font = workbook.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        font.setColor(new XSSFColor(new java.awt.Color(39, 51, 89)).getIndexed());

        /*
         * Green color style
         */
        XSSFCellStyle greenColorStyle = workbook.createCellStyle();
        greenColorStyle.setFillForegroundColor(new XSSFColor(Color.decode("#C2D69A")));//new XSSFColor(new java.awt.Color(194, 214, 154)));
        greenColorStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        greenColorStyle.setAlignment(CellStyle.ALIGN_CENTER);
        greenColorStyle.setVerticalAlignment((short) 1);
        greenColorStyle.setWrapText(true);
        greenColorStyle.setBorderBottom(BorderStyle.THIN);
        greenColorStyle.setBorderTop(BorderStyle.THIN);
        greenColorStyle.setBorderRight(BorderStyle.THIN);
        greenColorStyle.setBorderLeft(BorderStyle.THIN);
        greenColorStyle.setFont(font);
        
        
        int size = topColumns.length;
        XSSFCell[] noCell = new XSSFCell[size];
        XSSFRow row1 = firstSheet.createRow(1);
        row1.setHeightInPoints(43);
        for (int i = 0; i < size; i++) {
            firstSheet.setDefaultColumnWidth((short) 15);
             noCell[i] = row1.createCell(i);
                noCell[i].setCellValue(new XSSFRichTextString(topColumns[i]));
                noCell[i].setCellStyle(greenColorStyle);
        }
        
        }
          catch (Exception e) {
            logger.log(Level.SEVERE, "Error building spreadsheet", e);
        }
}
      
    public void populate(List<PatientProgramSheet> persons) {
        int size = persons.size();
              
         /*
//         * setup date cell style for DOB
//         */
        XSSFDataFormat dataFormat = workbook.createDataFormat();
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(dataFormat.getFormat("MM/dd/yyyy"));
        
         /*
         * hiding Health home column
         */
        if (!healthHomeRequired) {
            firstSheet.setColumnHidden(HEALTH_HOME_COLUMN, true);
        }
        
        /*
         * populate patient data
         */
        for (int row = 0; row < size; row++) {
            
            PatientProgramSheet tempPerson = persons.get(row);
            //  List<PatientProgramDTO> programs=tempPerson.getPatientEnrollment().getPatientProgramName();
              
            XSSFRow tempRow = firstSheet.createRow(3 + row);
            
            for (int col = 0; col < 10; col++) {
                
                  XSSFCell tempCell = tempRow.createCell(col);
                 
                  if (col == LAST_NAME_COLUMN) {
                    tempCell.setCellValue(tempPerson.getLastName());
                } else if (col == FIRST_NAME_COLUMN) {
                    tempCell.setCellValue(tempPerson.getFirstName());
                } else if (col == GENDER_COLUMN) {
                    tempCell.setCellValue(getGender(tempPerson.getGender()));
                } else if (col == DOB_COLUMN) {
                      Date dob = tempPerson.getDob();
                    setDate(tempCell, dateCellStyle, dob);
                }
                else if (col == ID_COLUMN) {
                     tempCell.setCellValue(tempPerson.getPatientID());
                 }

                  else if (col == PROGRAM_COLUMN) {
                     tempCell.setCellValue(tempPerson.getProgramName() != null ? tempPerson.getProgramName() : "");
//                    tempCell.setCellValue(tempDTO.getProgram().getValue());
                 } /*else if (col == EFFECTIVE_COLUMN) {
                     Date effectiveDate = tempPerson.getProgramEffective();
                     setDate(tempCell, dateCellStyle, effectiveDate);
                 } else if (col == END_COLUMN) {
                     Date endDate = tempPerson.getProgramEnd();
                     setDate(tempCell, dateCellStyle, endDate);
                 }*/
                 if (col == STATUS_COLUMN) {
                     tempCell.setCellValue(tempPerson.getPatientStatus() != null ? tempPerson.getPatientStatus() : "");
                 } else if (col == STATUS_EFFECTIVE_COLUMN) {
                     Date statusEffectiveDate = tempPerson.getStatusEffective();
                     setDate(tempCell, dateCellStyle, statusEffectiveDate);
                 } else if (col == PROGRAM_END_REASON_COLUMN) {
                     tempCell.setCellValue(tempPerson.getProgramEndReason());
                 } else if (col == HEALTH_HOME_COLUMN) {
                     tempCell.setCellValue(tempPerson.getHealthHome());
                 }

//                else if (col == EUID_COLUMN) {
//                    tempCell.setCellValue(tempPerson.getPatientEnrollment().getPatientEuid());
//                }
             }
//               if(!programs.isEmpty())    {
//             for(int j=0;j<programs.size();j++){
//
//                 PatientProgramDTO tempDTO=programs.get(j);
//                  XSSFRow progRow=tempRow;
//                  if (j==0){
//                      for (int column = 5; column <= 11; column++) {
//
//                  XSSFCell tempCell = progRow.createCell(column);
//
//                  if (column == PROGRAM_COLUMN) {
//                       tempCell.setCellValue(tempDTO.getProgram()!=null?tempDTO.getProgram().getValue():"");
////                    tempCell.setCellValue(tempDTO.getProgram().getValue());
//                } else if (column == EFFECTIVE_COLUMN) {
//                    Date effectiveDate = tempDTO.getProgramEffectiveDate();
//                    setDate(tempCell, dateCellStyle, effectiveDate);
//                } else if (column == END_COLUMN) {
//                    Date endDate = tempDTO.getProgramEndDate();
//                    setDate(tempCell, dateCellStyle, endDate);
//                } if (column == STATUS_COLUMN) {
//                     tempCell.setCellValue(tempDTO.getStatus()!=null?tempDTO.getStatus().getValue():"");
//                } else if (column == STATUS_EFFECTIVE_COLUMN) {
//                    Date statusEffectiveDate = tempDTO.getStatusEffectiveDate();
//                    setDate(tempCell, dateCellStyle, statusEffectiveDate);
//                }else if (column == PROGRAM_END_REASON_COLUMN) {
//                    tempCell.setCellValue(tempDTO.getTerminationReason());
//                }else if (column == HEALTH_HOME_COLUMN) {
//                    tempCell.setCellValue(tempDTO.getHealthHome());
//                }
//                  }}
//                else{
//                       progRow = firstSheet.createRow(3 + row);
//
//                 for (int column = 0; column <= 11; column++) {
//
//                  XSSFCell tempCell = progRow.createCell(column);
//
//                   if (column == LAST_NAME_COLUMN) {
//                    tempCell.setCellValue(tempPerson.getLastName());
//                } else if (column == FIRST_NAME_COLUMN) {
//                    tempCell.setCellValue(tempPerson.getFirstName());
//                } else if (column == GENDER_COLUMN) {
//                    tempCell.setCellValue(getGender(tempPerson.getGender()));
//                } else if (column == DOB_COLUMN) {
//                    Date dob = tempPerson.getDateOfBirth();
//                    setDate(tempCell, dateCellStyle, dob);
//                }else if (column == ID_COLUMN) {
//                    tempCell.setCellValue(Long.toString(tempPerson.getPatientEnrollment().getPatientId()));
//                }
//                  if (column == PROGRAM_COLUMN) {
//                    tempCell.setCellValue(tempDTO.getProgram()!=null?tempDTO.getProgram().getValue():"");
////                    tempCell.setCellValue(tempDTO.getProgram().getValue());
//                } else if (column == EFFECTIVE_COLUMN) {
//                    Date effectiveDate = tempDTO.getProgramEffectiveDate();
//                    setDate(tempCell, dateCellStyle, effectiveDate);
//                } else if (column == END_COLUMN) {
//                    Date endDate = tempDTO.getProgramEndDate();
//                    setDate(tempCell, dateCellStyle, endDate);
//                } if (column == STATUS_COLUMN) {
//                    tempCell.setCellValue(tempDTO.getStatus()!=null?tempDTO.getStatus().getValue():"");
////                    tempCell.setCellValue(tempDTO.getStatus().getValue());
//                } else if (column == STATUS_EFFECTIVE_COLUMN) {
//                    Date statusEffectiveDate = tempDTO.getStatusEffectiveDate();
//                    setDate(tempCell, dateCellStyle, statusEffectiveDate);
//                }else if (column == PROGRAM_END_REASON_COLUMN) {
//                    tempCell.setCellValue(tempDTO.getTerminationReason());
//                }else if (column == HEALTH_HOME_COLUMN) {
//                    tempCell.setCellValue(tempDTO.getHealthHome());
//                }
//
//             }}
//                 row++;
//             }
//
//               }
//               else{
//                   row ++;
//               }
        }
      }
         public String getGender( String gender){
              if(gender.equalsIgnoreCase("M")){
               gender="Male";
           }
           else if(gender.equalsIgnoreCase("F")){
               gender="Female";
           }
              return gender;
         }        
       protected void setDate(XSSFCell tempCell, CellStyle dateCellStyle, Date theDate) {
        // add special date cell style
        tempCell.setCellStyle(dateCellStyle);

        if (theDate != null) {
            tempCell.setCellValue(theDate);
        }
    }
         
      public XSSFWorkbook getWorkbook() {
        return workbook;
    }

    public void setWorkbook(XSSFWorkbook workbook) {
        this.workbook = workbook;
    }

}
