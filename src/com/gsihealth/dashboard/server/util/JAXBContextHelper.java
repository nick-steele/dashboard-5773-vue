package com.gsihealth.dashboard.server.util;

import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import oasis.names.tc.xacml._2_0.policy.schema.os.ObjectFactory;

/**
 *
 * @author Chad Darby
 */
public class JAXBContextHelper {

    private static JAXBContext jaxbContext;
    private static Logger logger = Logger.getLogger(JAXBContextHelper.class.getName());

    protected JAXBContextHelper() {
    }

    public static JAXBContext getContext() throws Exception {
        if (jaxbContext == null) {
            jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
        }
        
    logger.info("Object Factory class: " + ObjectFactory.class.toString());
    logger.info("Object Facory path: "+ getClassLocation(ObjectFactory.class).toString());
        

        return jaxbContext;
    }
    
    public static URL getClassLocation(final Class cls) { 
        final ProtectionDomain pd = cls.getProtectionDomain();
        final CodeSource cs = pd.getCodeSource();
        return cs.getLocation();
    }
}
