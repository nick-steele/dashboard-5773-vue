package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.Constants;
import com.gsihealth.entity.PatientCommunityEnrollment;
import java.util.logging.Logger;

/**
 *
 * @author Chad Darby
 */
public class ConsentUtils {
    
    private static Logger logger = Logger.getLogger(ConsentUtils.class.getName());

    /**
     * Returns true for YES, else false
     * 
     * @param programLevelConsentStatus
     * @return 
     */
    public static boolean computeConsentForBHIX(String programLevelConsentStatus) {
        return StringUtils.equalsIgnoreCase(programLevelConsentStatus, Constants.YES);
    }
    
    /**
     * Checks to see if we need to send consent for BHIX
     * 
     * @param oldProgramLevelConsentStatus
     * @param newProgramLevelConsentStatus
     * @return 
     */
    public static boolean hasConsentChangedForBHIX(String oldProgramLevelConsentStatus, String newProgramLevelConsentStatus) {
        boolean notEquals = !StringUtils.equals(newProgramLevelConsentStatus, oldProgramLevelConsentStatus);

        boolean nullToNo = StringUtils.isBlank(oldProgramLevelConsentStatus) && StringUtils.equals(newProgramLevelConsentStatus, Constants.NO);
        boolean nullToUnknown = StringUtils.isBlank(oldProgramLevelConsentStatus) && StringUtils.equals(newProgramLevelConsentStatus, Constants.UNKNOWN);

        boolean noToNull = StringUtils.equals(oldProgramLevelConsentStatus, Constants.NO) && StringUtils.isBlank(newProgramLevelConsentStatus);
        boolean noToUnknown = StringUtils.equals(oldProgramLevelConsentStatus, Constants.NO) && StringUtils.equals(newProgramLevelConsentStatus, Constants.UNKNOWN);

        boolean unknownToNull = StringUtils.equals(oldProgramLevelConsentStatus, Constants.UNKNOWN) && StringUtils.isBlank(newProgramLevelConsentStatus);
        boolean unknownToNo = StringUtils.equals(oldProgramLevelConsentStatus, Constants.UNKNOWN) && StringUtils.equals(newProgramLevelConsentStatus, Constants.NO);
        
        boolean consentHasChanged = notEquals &&
                !(nullToNo || nullToUnknown || noToNull || noToUnknown || unknownToNull || unknownToNo);
        
        logger.info("consentHasChanged=" + consentHasChanged);
        
        return consentHasChanged;
    }
}
