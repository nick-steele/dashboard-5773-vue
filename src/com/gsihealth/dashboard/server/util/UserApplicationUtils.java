package com.gsihealth.dashboard.server.util;

import com.gsihealth.entity.Application;
import com.gsihealth.dashboard.entity.dto.ApplicationDTO;
import java.util.ArrayList;
import java.util.List;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

/**
 *
 * @author Chad Darby
 */
public class UserApplicationUtils {
    
    /**
     * Convert entity objects to DTOs
     * 
     * @param apps
     * @return 
     */
    public static List<ApplicationDTO> convert(List<Application> apps) {
        List<ApplicationDTO> appDtos = new ArrayList<ApplicationDTO>();

        // setup
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        for (Application tempApp : apps) {
            
            // convert
            ApplicationDTO tempDto = new ApplicationDTO();
            tempDto.setApplicationId(tempApp.getApplicationPK().getApplicationId());
            tempDto.setApplicationName(tempApp.getApplicationName());
            tempDto.setClickToOpenEnabled(tempApp.getIsClickToOpenEnabled());
            tempDto.setCommunityId(tempApp.getApplicationPK().getCommunityId());
            tempDto.setDisplayOrder(tempApp.getDisplayOrder());
            tempDto.setExternalApplicationUrl(tempApp.getExternalApplicationUrl());
            tempDto.setNativeApp(tempApp.getNativeApp());
            tempDto.setNotificationEndpoint(tempApp.getNotificationEndpoint());
            tempDto.setOid(tempApp.getOid());
            tempDto.setScreenName(tempApp.getScreenName());
            tempDto.setShowInPopupWindow(tempApp.getShowInPopupWindow());
            tempDto.setSsoEnabled(tempApp.getIsSsoEnabled());
            
            appDtos.add(tempDto);
        }

        return appDtos;
    }
    
}
