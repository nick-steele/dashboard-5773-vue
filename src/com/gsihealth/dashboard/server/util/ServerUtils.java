/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.util;

/**
 *
 * @author Chad Darby
 */
public class ServerUtils {
    
    public static void dumpSystemProperties() {
     
        String trustStore = System.getProperty("javax.net.ssl.trustStore");
        String trustStorePassword= System.getProperty("javax.net.ssl.trustStorePassword");
        String keyStore = System.getProperty("javax.net.ssl.keyStore");
        String keyStorePassword = System.getProperty("javax.net.ssl.keyStorePassword");
        String allowUnsafeRenegotiation = System.getProperty("sun.security.ssl.allowUnsafeRenegotiation");
        
        System.out.println();
        System.out.println("trustStore=" + trustStore);
        System.out.println("trustStorePassword=" + trustStorePassword);
        System.out.println("keyStore=" + keyStore);
        System.out.println("keyStorePassword=" + keyStorePassword);
        System.out.println("allowUnsafeRenegotiation=" + allowUnsafeRenegotiation);
        System.out.println();
    }
}
