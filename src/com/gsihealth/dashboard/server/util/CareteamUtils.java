package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.server.WebConstants;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Chad Darby
 */
public class CareteamUtils {

    /**
     * Compute the enrollment status flag for sending to BHIX
     * 
     * @param enrollmentStatusOld
     * @param newEnrollmentStatus
     * @param patientInfo
     * @return 
     */
    public static boolean computeSendEnrollmentStatusFlagForBHIX(String enrollmentStatusOld, String newEnrollmentStatus, PatientInfo patientInfo) {
        
        boolean changeRules = ((StringUtils.equals(enrollmentStatusOld, WebConstants.PENDING) && !StringUtils.equals(newEnrollmentStatus, WebConstants.INACTIVE))
                || (StringUtils.equals(enrollmentStatusOld, WebConstants.INACTIVE) && !StringUtils.equals(newEnrollmentStatus, WebConstants.PENDING))
                || (StringUtils.equals(enrollmentStatusOld, WebConstants.ENROLLED) && !StringUtils.equals(newEnrollmentStatus, WebConstants.ASSIGNED))
                || (StringUtils.equals(enrollmentStatusOld, WebConstants.ASSIGNED) && !StringUtils.equals(newEnrollmentStatus, WebConstants.ENROLLED)));

        boolean sendEnrollmentStatusFlag = patientInfo.isEnrollmentStatusChanged() && changeRules;
        
        return sendEnrollmentStatusFlag;
    }

    /**
     * Compute the care team flag for sending to BHIX
     * 
     * @param patientInfo
     * @param newEnrollmentStatus
     * @return 
     */
    public static boolean computeSendCareteamFlagForBHIX(PatientInfo patientInfo, String newEnrollmentStatus) {
        boolean result = patientInfo.isCareteamChanged() || (patientInfo.isEnrollmentStatusChanged() && StringUtils.equals(newEnrollmentStatus, WebConstants.ASSIGNED)); 
        
        return result;
    }
    
}
