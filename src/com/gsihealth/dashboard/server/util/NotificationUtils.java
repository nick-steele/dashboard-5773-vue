/**
 *
 */
package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.AlertConstants;
import com.gsihealth.dashboard.common.ConsentConstants;
import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;

import java.util.*;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.gsihealth.entity.*;
import org.gsihealth.wsn.EnrollmentStatusNotificationMessage;
import org.gsihealth.wsn.GSIHealthNotificationMessage;
import org.gsihealth.wsn.NotificationMessage;
import org.gsihealth.wsn.NotificationMetadata;
import org.gsihealth.wsn.NotificationResponseType;
import org.gsihealth.wsn.Patient;
import org.gsihealth.wsn.Organization;
import org.gsihealth.wsn.PatientCreationNotificationMessage;
import org.gsihealth.wsn.PatientDemogUpdateNotificationMessage;
import org.gsihealth.wsn.PatientIdentity;
import org.gsihealth.wsn.ResponseStatusType;
import org.oasis_open.docs.wsn.b_2.Notify2;

import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import com.gsihealth.dashboard.entity.dto.PatientProgramDTO;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.CommunityDAO;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.dashboard.server.dao.OrganizationDAO;
import com.gsihealth.dashboard.server.dao.PatientProgramDAO;
import com.gsihealth.dashboard.server.dao.ReferenceDataDAO;
import com.gsihealth.dashboard.server.jms.MessageBrokerContext;
import com.gsihealth.notification.NotificationSender;

import java.io.StringWriter;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;

import oasis.names.tc.xacml._2_0.policy.schema.os.AttributeValueType;
import oasis.names.tc.xacml._2_0.policy.schema.os.ResourceType;
import oasis.names.tc.xacml._2_0.policy.schema.os.SubjectType;
import oasis.names.tc.xacml._2_0.policy.schema.os.ActionMatchType;
import oasis.names.tc.xacml._2_0.policy.schema.os.ActionType;
import oasis.names.tc.xacml._2_0.policy.schema.os.ActionsType;
import oasis.names.tc.xacml._2_0.policy.schema.os.AttributeDesignatorType;
import oasis.names.tc.xacml._2_0.policy.schema.os.EffectType;
import oasis.names.tc.xacml._2_0.policy.schema.os.PolicyType;
import oasis.names.tc.xacml._2_0.policy.schema.os.ResourceMatchType;
import oasis.names.tc.xacml._2_0.policy.schema.os.ResourcesType;
import oasis.names.tc.xacml._2_0.policy.schema.os.RuleType;
import oasis.names.tc.xacml._2_0.policy.schema.os.SubjectAttributeDesignatorType;
import oasis.names.tc.xacml._2_0.policy.schema.os.SubjectMatchType;
import oasis.names.tc.xacml._2_0.policy.schema.os.SubjectsType;
import oasis.names.tc.xacml._2_0.policy.schema.os.TargetType;
import org.apache.commons.lang.time.StopWatch;
import org.hl7.v3.PRPAMT201302UV02PatientId;


import org.gsihealth.wsn.AlertMessage;
import org.gsihealth.wsn.AlertNotificationMessage;
import org.gsihealth.wsn.CareTeamNotificationMessage;
import org.gsihealth.wsn.CoordinatorProgram;
import org.gsihealth.wsn.CoordinatorPrograms;
import org.gsihealth.wsn.OrganizationCreationNotificationMessage;
import org.gsihealth.wsn.OrganizationUpdateNotificationMessage;
import org.gsihealth.wsn.PatientConsentNotificationMessage;

/**
 * @author Vishal (Off.)
 *
 */
public class NotificationUtils {

    private static Logger logger = Logger.getLogger(NotificationUtils.class.getName());
    private static String EMPTY_STRING = "";
    private static CommunityDAO communityDAO;
    private static PatientProgramDAO patientProgramDAO;
    private static ReferenceDataDAO referenceDataDAO;
   
    public static String sendNotification(long communityId, GSIHealthNotificationMessage gsiHealthNotificationMessage) throws Exception {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        String endPoint = configurationDAO.getProperty(communityId, "gsihealth.notification.manager.endpoint.url");

        String status = sendNotification(communityId, gsiHealthNotificationMessage, endPoint);

        return status;
    }

    /**
     * Use to send notification to a Web Service endpoint.
     *
     * @param message - The marshaled message String from the JAX-B
     * @throws Exception
     */
    private static String sendNotification(long communityId, GSIHealthNotificationMessage gsiHealthNotificationMessage, String endPoint) throws Exception {

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        String uuid = UUID.randomUUID().toString();

        try {
            logger.info("Sending message JMS Queue");

            MessageBrokerContext messageBrokerContext = MessageBrokerContext.getInstance();

            ConnectionFactory connectionFactory = messageBrokerContext.getConnectionFactory();
            Queue queue = messageBrokerContext.getQueue();

            if (connectionFactory == null) {
                logger.warning(">>> Not sending message. Notification Connection Factory is null.");
                return "FAIL. Notification Connection Factory is null.";
            }

            String communityIdString = String.valueOf(communityId);
            NotificationSender notificationSender = new NotificationSender(communityIdString, endPoint, uuid, connectionFactory, queue); 
        
            stopWatch.stop();

            logger.info("[profiling], NotificationUtils: sendNotification - create notification sender,  " + (stopWatch.getTime() / 1000.0) + ", secs");

            // log the message

            if (LoggerUtils.isDebug(logger)) {

                stopWatch.reset();
                stopWatch.start();

                String message = notificationSender.marshalMessage(gsiHealthNotificationMessage);

                dump(endPoint, message);

                stopWatch.stop();

                logger.info("[profiling], NotificationUtils: sendNotification - log message,  " + (stopWatch.getTime() / 1000.0) + ", secs");
            }

            stopWatch.reset();
            stopWatch.start();

            // send the message
            Notify2 notify = notificationSender.formNotify(gsiHealthNotificationMessage);
            NotificationResponseType notificationResponseType = notificationSender.sendNotify(notify);

            stopWatch.stop();

            logger.info("[profiling], NotificationUtils: sendNotification - notificationSender.sendNotify,  " + (stopWatch.getTime() / 1000.0) + ", secs");

            // return the status
            ResponseStatusType status = notificationResponseType.getStatus();

            logger.info("Status: " + status);

            return status.toString();

        } catch (Exception ex) {
            throw new Exception("Failed to send Message to Endpoint Service: " + endPoint, ex);
        }
    }

    public static AlertNotificationMessage buildAlertNotificationMessage(String alertLogicalId, String alertMessageBody, String theOid, String patientIdentifier, String careteamName) {
        AlertNotificationMessage alertNotificationMessage = new AlertNotificationMessage();

        alertNotificationMessage.setAlertId(alertLogicalId);

        AlertMessage alertMessage = new AlertMessage();

        alertMessage.setAlertMessageBody(alertMessageBody);

        Patient patient = buildPatient(theOid, patientIdentifier, theOid);
        alertMessage.setPatient(patient);

        alertNotificationMessage.setAlertMessage(alertMessage);

        return alertNotificationMessage;
    }

    public static PatientConsentNotificationMessage buildPatientConsentNotificationMessage(String localId, String localOid,
            HashMap<String, String> consent, String docId) {
        logger.fine("localId: " + localId + " localOid: " + localOid);

        PatientConsentNotificationMessage message = new PatientConsentNotificationMessage();

        // build the policy
        // use the XACML code for creating the policy type
        PolicyType policy = new PolicyType();

        policy.setPolicyId(docId);
        policy.setRuleCombiningAlgId("urn:oasis:names:tc:xacml:1.0:rule-combining-algorithm:first-applicable");

        TargetType polTarget = new TargetType();
        policy.setTarget(polTarget);
        addPatientResource(polTarget, localOid, localId);

        Iterator<String> iorgs = consent.keySet().iterator();
        while (iorgs.hasNext()) {
            String consentOid = iorgs.next();
            String type = consent.get(consentOid);
            addRuleOrg(policy, consentOid, type);
        }

        QName qname = new QName("urn:oasis:names:tc:xacml:2.0:policy:schema:os", "Policy");
        String doc = marshal(qname, policy);
        logger.info(doc);

        // build the message
        message.setConsentXACMLPolicy(policy);

        return message;
    }

    private static void addPatientResource(TargetType polTarget, String orgOid, String id) {
        ResourcesType resources = new ResourcesType();

        ResourceType resource = new ResourceType();
        polTarget.setResources(resources);
        List<ResourceType> Resources = resources.getResource();
        Resources.add(resource);
        List<ResourceMatchType> ems = resource.getResourceMatch();
        ResourceMatchType emt = new ResourceMatchType();
        ems.add(emt);
        emt.setMatchId("urn:oasis:names:tc:xacml:1.0:function:anyURI-equal");
        AttributeValueType av2 = new AttributeValueType();
        av2.setDataType("urn:hl7-org:v3#II");
        emt.setAttributeValue(av2);
        List econtent = av2.getContent();
        QName pq = new QName("urn:hl7-org:v3", "PatientId");
        PRPAMT201302UV02PatientId pid = new PRPAMT201302UV02PatientId();
        logger.info("Patient Id class: " +PRPAMT201302UV02PatientId.class.toString());
        logger.info("PatientId path: "+ getClassLocation(PRPAMT201302UV02PatientId.class).toString());
        
        pid.setExtension(id);
        pid.setRoot(orgOid);
        JAXBElement jpid = new JAXBElement(pq, PRPAMT201302UV02PatientId.class, pid);
        econtent.add(jpid);
        AttributeDesignatorType adt = new AttributeDesignatorType();
        adt.setAttributeId("http://www.hhs.gov/healthit/nhin#subject-id");
        adt.setDataType("urn:hl7-org:v3#II");
        emt.setResourceAttributeDesignator(adt);

    }
    public static URL getClassLocation(final Class cls) { 
        final ProtectionDomain pd = cls.getProtectionDomain();
        final CodeSource cs = pd.getCodeSource();
        return cs.getLocation();
    }
    
     

    private static void addTransactions(TargetType polTarget) {

        ActionsType acts = new ActionsType();

        ActionType action = new ActionType();
        polTarget.setActions(acts);
        List<ActionType> actions = acts.getAction();
        actions.add(action);
        List<ActionMatchType> ams = action.getActionMatch();
        ActionMatchType amt = new ActionMatchType();
        ams.add(amt);
        amt.setMatchId("urn:oasis:names:tc:xacml:1.0:function:anyURI-equal");
        AttributeValueType av = new AttributeValueType();
        av.setDataType("http://www.w3.org/2001/XMLSchema#anyURI");
        amt.setAttributeValue(av);
        List content = av.getContent();
        content.add("urn:ihe:iti:2007:DocumentRetrieve");
    }

    private static void addRuleOrg(PolicyType policy, String orgOid, String type) {
        orgOid = "urn:oid:" + orgOid;
        RuleType rule1 = new RuleType();

        List rules = policy.getCombinerParametersOrRuleCombinerParametersOrVariableDefinition();
        rules.add(rule1);
        rule1.setRuleId(orgOid);
        if (type.equals(ConsentConstants.CONSENT_PERMIT)) {
            rule1.setEffect(EffectType.PERMIT);
        }
        if (type.equals(ConsentConstants.CONSENT_DENY)) {
            rule1.setEffect(EffectType.DENY);
        }
        rule1.setDescription("allow access to normal documents to physician and administrators (with clinical viewing privs) within an organization");

        TargetType r1Target1 = new TargetType();

        rule1.setTarget(r1Target1);
        SubjectsType subs = new SubjectsType();
        r1Target1.setSubjects(subs);
        addEqualSubject(subs, "urn:oasis:names:tc:xspa:1.0:subject:organization-id", orgOid);

    }

    private static void addEqualSubject(SubjectsType subs, String id, String value) {
        String type = "http://www.w3.org/2001/XMLSchema#anyURI";
        SubjectType sub = new SubjectType();
        List<SubjectType> subjects = subs.getSubject();
        subjects.add(sub);
        List<SubjectMatchType> sms = sub.getSubjectMatch();
        SubjectMatchType smt = new SubjectMatchType();
        sms.add(smt);
        smt.setMatchId("urn:oasis:names:tc:xacml:1.0:function:anyURI-equal");
        AttributeValueType av = new AttributeValueType();
        av.setDataType(type);
        smt.setAttributeValue(av);
        List content = av.getContent();
        content.add(value);
        SubjectAttributeDesignatorType sadt = new SubjectAttributeDesignatorType();
        smt.setSubjectAttributeDesignator(sadt);
        sadt.setAttributeId(id);
        sadt.setDataType(type);
    }

    public static String marshal(QName altName, Object jaxb) {

        String ret = null;
        try {
            javax.xml.bind.JAXBContext jaxbContext = JAXBContextHelper.getContext();

            Marshaller marshaller = jaxbContext.createMarshaller();

            StringWriter sw = new StringWriter();
            marshaller.marshal(new JAXBElement(altName, jaxb.getClass(), jaxb), sw);
            StringBuffer sb = sw.getBuffer();
            ret = sb.toString();

        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Error marshalling", ex);
        }
        return ret;
    }

    protected static CareTeamNotificationMessage buildCareTeamNotificationMessage(List<PersonDTO> personDTOList, OrganizationDAO organizationDAO, String from, long communityId) {
        CareTeamNotificationMessage careTeamNotificationMessage = new CareTeamNotificationMessage();
        List<Patient> patientList = careTeamNotificationMessage.getPatient();
        for (PersonDTO tempPersonDTO : personDTOList) {
            String patientOrgOID = getOrganizationOID(organizationDAO, tempPersonDTO, communityId);

            PatientEnrollmentDTO patientEnrollment = tempPersonDTO.getPatientEnrollment();
            String patientId = Long.toString(patientEnrollment.getPatientId());

            Patient tempPatient = buildPatient(patientOrgOID, patientId, from);

            patientList.add(tempPatient);
        }
        return careTeamNotificationMessage;
    }

   

    protected static XMLGregorianCalendar getCurrentTimeStamp() {
        GregorianCalendar currentTime = new GregorianCalendar();
        currentTime.setTimeInMillis(System.currentTimeMillis());
        XMLGregorianCalendar currentTimeStamp = null;
        try {
            currentTimeStamp = DatatypeFactory.newInstance().newXMLGregorianCalendar(currentTime);
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        return currentTimeStamp;
    }

    /**
     * strip some characters and format the message
     *
     * @param endPoint
     * @param message
     */
    protected static void dump(String endPoint, String message) {
        logger.fine("Sending to endpoint: " + endPoint + "\n");

        String sanitizedMessage = MessageUtils.sanitize(message);

        logger.fine(XmlUtils.prettyFormat(sanitizedMessage));
    }

    protected static String dump(String message) {

        String sanitizedMessage = MessageUtils.sanitize(message);

        String pretty = XmlUtils.prettyFormat(sanitizedMessage);

        logger.fine(pretty);

        return pretty;
    }

    /**
     * Use to send notification to a end point Web Service for BHIX.
     *
     * @param message - The marshaled message String from the JAX-B
     * @throws Exception
     */
    public static String sendNotificationBHIX(long communityId, GSIHealthNotificationMessage gsiHealthNotificationMessage) throws Exception {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        try {
            String status = sendNotification(communityId, gsiHealthNotificationMessage);

            return status;
        } catch (Exception exc) {
            throw new Exception("Failed to send Message to Endpoint Service", exc);
        }
    }

    /**
     *
     *
     * @param personDTO -
     * @param reasonCode -
     * @param From -
     * @param to -
     * @return -
     *
     */
    public static synchronized GSIHealthNotificationMessage constructMessage(long communityId,
            String orgId,
            String patientIdentifier,
            PersonDTO personDTO,
            NotificationReasonCode reasonCode,
            String from,
            String to) {

        GSIHealthNotificationMessage gsiHealthNotificationMessage = new GSIHealthNotificationMessage();

        NotificationMetadata notificationMetadata = buildNotificationMetadata(reasonCode, from, EMPTY_STRING, communityId);

        gsiHealthNotificationMessage.setNotificationMetadata(notificationMetadata);

        NotificationMessage notificationMessage = reasonCode.getNotificationMessage(orgId, patientIdentifier, from, personDTO);
        gsiHealthNotificationMessage.setNotificationMessage(notificationMessage);

        return gsiHealthNotificationMessage;
    }

    public static synchronized GSIHealthNotificationMessage constructMessage(long communityId,
            String orgId,
            long patientIdentifier,
            PersonDTO personDTO,
            NotificationReasonCode reasonCode,
            String from,
            String to) {

        String patientId = Long.toString(patientIdentifier);

        return constructMessage(communityId,
                orgId,
                patientId,
                personDTO,
                reasonCode,
                from,
                to);
    }

    /**
     * @param reasonCode
     * @param from
     * @param to
     * @return
     */
    private static NotificationMetadata buildNotificationMetadata(NotificationReasonCode reasonCode, String from, String to, long communityId) {

        String reasonCodeStr = reasonCode.toString();
        String fromApplication = AlertConstants.GSIHEALTH_DASHBOARD_APP;

        NotificationMetadata notificationMetadata = buildNotificationMetaData(reasonCodeStr, fromApplication, from, to, communityId);

        return notificationMetadata;
    }

    protected static NotificationMetadata buildNotificationMetaData(String reasonCodeStr, String fromApplication, String from, String to, long communityId) {
        XMLGregorianCalendar currentTimeStamp = getCurrentTimeStamp();

        NotificationMetadata notificationMetadata = new NotificationMetadata();

        notificationMetadata.setFrom(from);
        notificationMetadata.setTo(to);
        notificationMetadata.setReasonCode(reasonCodeStr);
        notificationMetadata.setDate(currentTimeStamp);

        notificationMetadata.setFromApplication(fromApplication);

        notificationMetadata.setCommunityId(getCommunityIdOID(communityId));
        
         return notificationMetadata;
    }
    
     /**
     * @param reasonCode
     * @param from
     * @param to
     * @return
     */
    private static NotificationMetadata buildOrganizationNotificationMetadata(OrganizationNotificationReasonCode reasonCode, String from, String to, String  communityIdOid) {

        String reasonCodeStr = reasonCode.toString();
        String fromApplication = AlertConstants.GSIHEALTH_DASHBOARD_APP;

        NotificationMetadata notificationMetadata = buildOrgNotificationMetaData(reasonCodeStr, fromApplication, from, to, communityIdOid);

        return notificationMetadata;
    }

     
     protected static NotificationMetadata buildOrgNotificationMetaData(String reasonCodeStr, String fromApplication, String from, String to, String  communityIdOid) {
        XMLGregorianCalendar currentTimeStamp = getCurrentTimeStamp();

        NotificationMetadata notificationMetadata = new NotificationMetadata();

        notificationMetadata.setFrom(from);
        notificationMetadata.setTo(to);
        notificationMetadata.setReasonCode(reasonCodeStr);
        notificationMetadata.setDate(currentTimeStamp);

        notificationMetadata.setFromApplication(fromApplication);
//should really put local code here to get communityIdOid as is the only place it is needed really..
      //  maybe save then revert back to subversion 
        notificationMetadata.setCommunityId(communityIdOid);

        return notificationMetadata;
    }
    /**
     * @param personDTO
     * @return
     */
    public static Patient buildPatient(String orgId, String patientIdentifier, String fromOid) {

        PatientIdentity patientIdentity = new PatientIdentity();

        patientIdentity.setPatientId(patientIdentifier);

        patientIdentity.setOrganizationID(fromOid);

        Patient patient = new Patient();
        patient.setPatientIdentity(patientIdentity);

        //specially asked to add by Parag on demand.
        patient.setCareManagementOID(orgId);
         
        return patient;
    }
    
     /**
     * @param personDTO
     * @return
     * 
     * new for DASHBOARD-2157 Send Program Info to Outbound Entities
     * for Patient Creation and Patient Demographic  Update MEssages
     */
    public static Patient buildPatient(String orgId, String patientIdentifier, String fromOid,PersonDTO personDTO) {

        PatientIdentity patientIdentity = new PatientIdentity();

        patientIdentity.setPatientId(patientIdentifier);

        patientIdentity.setOrganizationID(fromOid);

        Patient patient = new Patient();
        patient.setPatientIdentity(patientIdentity);

        //specially asked to add by Parag on demand.
        patient.setCareManagementOID(orgId);
        
        //DASHBOARD-2157, 2722, 2870 Send Program Info to Outbound Entities 
        patient.setEmergencyContactFirstName(personDTO.getPatientEnrollment().getPatientEmergencyContact()
                                                .getFirstName());
        patient.setEmergencyContactLastName(personDTO.getPatientEnrollment().getPatientEmergencyContact()
                                                .getLastName());
        patient.setEmergencyContactTelephone(personDTO.getPatientEnrollment().getPatientEmergencyContact()
                                               .getTelephone());
        Long patientRelationshipCode = personDTO.getPatientEnrollment().getPatientEmergencyContact().getPatientRelationshipId();
        
        //patient.setCoordinatorEnrollmentStatus(personDTO.getPatientEnrollment().getStatus());
        
        //Get Coordinator Programs
        CoordinatorPrograms coordinatorPrograms = new CoordinatorPrograms();

        try {
            //Get Emergency Contact RelationShip
            logger.info("Emergency Contact Relationship Code: " + patientRelationshipCode);
            if (patientRelationshipCode != null && patientRelationshipCode > -1) {
                List<PatientRelationshipCode> relations = getReferenceDataDAO().findPatientRelationshipEntities(personDTO.getPatientEnrollment().getCommunityId());
                
                if (relations != null) {
                    logger.info("successfully read back  relationship codes from DB: relations size: " + relations.size());
                    for (PatientRelationshipCode r : relations) {
                        if (r.getPatientRelationshipCodePK().getId() == patientRelationshipCode) {
                            logger.info("patient emerg relationship:" + r.getValue());
                            patient.setEmergencyContactRelationship(r.getValue());
                        }
                    }
                }
                logger.info("EmergencyContact Relationship: " + patient.getEmergencyContactRelationship());
            }
            List<PatientProgramLink> patientProgramLinks = patientProgramDAO.findPatientProgramLinks(personDTO.getPatientEnrollment().getPatientId(), personDTO.getPatientEnrollment().getCommunityId());
            if (patientProgramLinks != null && patientProgramLinks.size() > 0) {
                for (PatientProgramLink patientProgramLink : patientProgramLinks) {
                    CoordinatorProgram coordinatorProgram = new CoordinatorProgram();
                    coordinatorProgram.setCoordinatorProgramName(patientProgramLink.getProgram().getName());
                    coordinatorProgram.setCoordinatorProgramStatus(patientProgramLink.getProgramStatus().getName());
                    coordinatorPrograms.getCoordinatorProgram().add(coordinatorProgram);
                }
                patient.setCoordinatorPrograms(coordinatorPrograms);
            }

        } catch (Exception ex) {
            String message = "Build Patient failed: id:" +patientIdentifier + " "+ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
          
        }

        return patient;
    }
    /**
     * Convert entity objects to DTOs
     *
     * @param patientProgramNames
     * @return
     */
    public static List<PatientProgramDTO> convert(List<PatientProgramName> patientProgramNames, long patientId) {

        List<PatientProgramDTO> patientProgDTOs = new ArrayList<PatientProgramDTO>();

        for (PatientProgramName tempPatientProgramName : patientProgramNames) {
            PatientProgramDTO tempDTO = convertTODTO(tempPatientProgramName, patientId);
            patientProgDTOs.add(tempDTO);
        }

        return patientProgDTOs;
    }

    public static PatientProgramDTO convertTODTO(PatientProgramName patientProgramName, long patientId) {
        PatientProgramDTO patientProgramDTO = new PatientProgramDTO();
        patientProgramDTO.setPatientProgramNameId(patientProgramName.getPatientProgramNameId());
        patientProgramDTO.setCommunityId(patientProgramName.getCommunityId());
        patientProgramDTO.setPatientId(patientId);
        patientProgramDTO.setHealthHome(patientProgramName.getHealthHome());
        patientProgramDTO.setPatientStatus(patientProgramName.getPatientStatus());
        patientProgramDTO.setProgramEffectiveDate(patientProgramName.getProgramEffectiveDate());
        patientProgramDTO.setProgramEndDate(patientProgramName.getProgramEndDate());
        patientProgramDTO.setProgramId(patientProgramName.getProgramId());
        patientProgramDTO.setTerminationReason(patientProgramName.getTerminationReason());
        patientProgramDTO.setStatusEffectiveDate(patientProgramName.getStatusEffectiveDate());
        patientProgramDTO.setOldRecord(true);
        return patientProgramDTO;
    }

     public static String dump(Object object) {
        java.lang.reflect.Field[] fields = object.getClass().getDeclaredFields();
        StringBuilder sb = new StringBuilder();
        sb.append(object.getClass().getSimpleName()).append('{');

        boolean firstRound = true;

        for (java.lang.reflect.Field field : fields) {
            if (!firstRound) {
                sb.append(", ");
            }
            firstRound = false;
            field.setAccessible(true);
            try {
                final Object fieldObj = field.get(object);
                final String value;
                if (null == fieldObj) {
                    value = "null";
                } else {
                    value = fieldObj.toString();
                }
                sb.append(field.getName()).append('=').append('\'')
                        .append(value).append('\'');
            } catch (IllegalAccessException ignore) {
                //this should never happen
            }

        }

        sb.append('}');
        return sb.toString();
    }
    
    /**
     * @param organizationDTO
     * @return
     */
    public static org.gsihealth.wsn.Organization buildOrganization(OrganizationDTO organizationDTO, String fromOid) {

         logger.fine("-inside build organization:"
                        +" org name:" + organizationDTO.getOrganizationName()
                        + " from oid:" + fromOid
                        + " from organizationDTO Oid: " + organizationDTO.getOID());
        //this Organization is from org.gsihealth.wsn
        org.gsihealth.wsn.Organization organization = new Organization();
        organization.setOrganizationID(organizationDTO.getOID());
        organization.setOrgDesc("");
        organization.setOrganizationName(organizationDTO.getOrganizationName()); 
     
        organization.setStatus(organizationDTO.getStatus());
              
        return organization;
    }

    public static GSIHealthNotificationMessage constructCareteamAssignmentMessage(OrganizationDAO organizationDAO, List<PersonDTO> personDTOList, String from, String to, long communityId) {

        GSIHealthNotificationMessage gsiHealthNotificationMessage = new GSIHealthNotificationMessage();

        NotificationMetadata notificationMetadata = buildNotificationMetadata(NotificationReasonCode.PATIENT_CARE_TEAM_ASSIGNMENT, from, to, communityId);

        gsiHealthNotificationMessage.setNotificationMetadata(notificationMetadata);

        CareTeamNotificationMessage careTeamNotificationMessage = buildCareTeamNotificationMessage(personDTOList, organizationDAO, from, communityId);

        NotificationMessage notificationMessage = new NotificationMessage();
        notificationMessage.setCareTeamNotificationMessage(careTeamNotificationMessage);

        gsiHealthNotificationMessage.setNotificationMessage(notificationMessage);

        return gsiHealthNotificationMessage;
    }

    public static GSIHealthNotificationMessage constructPatientConsentNotificationMessage(String localId, String localOid,
            HashMap<String, String> consent, String docId, String from, String to, long communityId) throws Exception {

        GSIHealthNotificationMessage gsiHealthNotificationMessage = new GSIHealthNotificationMessage();
        NotificationMetadata notificationMetadata = buildNotificationMetadata(NotificationReasonCode.PATIENT_CONSENT, from, to, communityId);
        gsiHealthNotificationMessage.setNotificationMetadata(notificationMetadata);

        PatientConsentNotificationMessage patientConsentNotificationMessage = buildPatientConsentNotificationMessage(localId, localOid, consent, docId);
        logger.fine("from: " + from + " localId: " + localId + " localOid: " + localOid + " to: " + to + " communityId: " + communityId);

        NotificationMessage notificationMessage = new NotificationMessage();
        notificationMessage.setPatientConsentNotificationMessage(patientConsentNotificationMessage);

        gsiHealthNotificationMessage.setNotificationMessage(notificationMessage);

        return gsiHealthNotificationMessage;
    }
    
    public static GSIHealthNotificationMessage constructOrganizationCreatedNotificationMessage(String communityIdOid,
            String orgId,
            OrganizationDTO organizationDTO,
            String from,
            String to) throws Exception {

        GSIHealthNotificationMessage gsiHealthNotificationMessage = new GSIHealthNotificationMessage();
        NotificationMetadata notificationMetadata = buildOrganizationNotificationMetadata(OrganizationNotificationReasonCode.ORGANIZATION_CREATION, from, to, communityIdOid);
        gsiHealthNotificationMessage.setNotificationMetadata(notificationMetadata);
        
        //could push this reasoncode back up to the calling routine
        OrganizationNotificationReasonCode organizationNotificationReasonCode = OrganizationNotificationReasonCode.ORGANIZATION_CREATION;

       
        logger.info("inside constructOrgMsg - from: " +  " to: " + to + " communityId: " + communityIdOid
        +" org name:"+  organizationDTO.getOrganizationName());

        NotificationMessage notificationMessage = organizationNotificationReasonCode.getOrganizationNotificationMessage(from, organizationDTO);  
      
       // notificationMessage.setOrganizationCreationNotificationMessage(organizationCreationNotificationMessage);
        //TODO is this the correct Organization Message
        gsiHealthNotificationMessage.setNotificationMessage(notificationMessage);

        return gsiHealthNotificationMessage;
    }
    
    public static GSIHealthNotificationMessage constructOrganizationUpdatedNotificationMessage(String communityIdOid,          
            OrganizationDTO organizationDTO,
            String from,
            String to) throws Exception {

        GSIHealthNotificationMessage gsiHealthNotificationMessage = new GSIHealthNotificationMessage();
        NotificationMetadata notificationMetadata = buildOrganizationNotificationMetadata(OrganizationNotificationReasonCode.ORGANIZATION_UPDATE, from, to, communityIdOid);
        gsiHealthNotificationMessage.setNotificationMetadata(notificationMetadata);
        
        //could push this reasoncode back up to the calling routine
        OrganizationNotificationReasonCode organizationNotificationReasonCode = OrganizationNotificationReasonCode.ORGANIZATION_UPDATE;

       logger.info("inside constructOrgUpdateMsg - from: " +  " to: " + to + " communityId: " + communityIdOid
        +" org name:"+  organizationDTO.getOrganizationName());
       

        NotificationMessage notificationMessage = organizationNotificationReasonCode.getOrganizationNotificationMessage( from, organizationDTO);  
      
       // notificationMessage.setOrganizationCreationNotificationMessage(organizationCreationNotificationMessage);
        //TODO is this the correct Organization Message
        gsiHealthNotificationMessage.setNotificationMessage(notificationMessage);

        return gsiHealthNotificationMessage;
    }
    
    
//    example
//  public static GSIHealthNotificationMessage constructMessage(long communityId, <-- model for Patient Creation
//            String orgId,
//            String patientIdentifier,
//            PersonDTO personDTO,
//            NotificationReasonCode reasonCode,
//            String from,
//            String to) {
//
//        GSIHealthNotificationMessage gsiHealthNotificationMessage = new GSIHealthNotificationMessage();
//
//        NotificationMetadata notificationMetadata = buildNotificationMetadata(reasonCode, from, EMPTY_STRING, communityId);
//
//        gsiHealthNotificationMessage.setNotificationMetadata(notificationMetadata);
//
//        NotificationMessage notificationMessage = reasonCode.getNotificationMessage(orgId, patientIdentifier, from, personDTO);
//        gsiHealthNotificationMessage.setNotificationMessage(notificationMessage);
//
//        return gsiHealthNotificationMessage;
//    }

    /**
     * public static PatientConsentNotificationMessage
     * buildPatientConsentNotificationMessage(String localId, String localOid,
     * HashMap<String, String> consent, String docId)
     *
     * @param reasonCodeStr
     * @param fromApplication
     * @param from
     * @param to
     * @return
     */
    protected static String getOrganizationOID(OrganizationDAO organizationDAO, PersonDTO personDTO, long communityId) {

        PatientEnrollmentDTO patientEnrollment = personDTO.getPatientEnrollment();
        long theOrgId = patientEnrollment.getOrgId();

        com.gsihealth.entity.OrganizationCwd organization = organizationDAO.findOrganization(theOrgId, communityId);

        String oid = organization.getOid();

        return oid;
    }
    
     protected static String getCommunityIdOID(long communityId) {

       
        com.gsihealth.entity.Community community = getCommunityDAO().getCommunityById(communityId);
  
        String oid = community.getCommunityOid();
        logger.info("CommunityID and Oid for metadata: " +  communityId + " oid:" + oid);

        return oid;
    }

    public static GSIHealthNotificationMessage constructPatientAlertNotificationMessage(long communityId, String alertLogicalId, String fromApplication, String alertMessageBody, PersonDTO personDTO, String fromOid, String patientId, String careteamName) {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        String gsiOid = configurationDAO.getProperty(communityId, "gsihealth.notification.send.fromOID");

        GSIHealthNotificationMessage gsiHealthNotificationMessage = new GSIHealthNotificationMessage();

        NotificationMetadata notificationMetadata = buildNotificationMetaData(AlertConstants.ALERT_NOTIFICATION, fromApplication, fromOid, gsiOid, communityId);

        gsiHealthNotificationMessage.setNotificationMetadata(notificationMetadata);

        AlertNotificationMessage alertNotificationMessage = buildAlertNotificationMessage(alertLogicalId, alertMessageBody, fromOid, patientId, careteamName);
        if(careteamName != null){
            alertNotificationMessage.setCareTeam(careteamName);
        }
        NotificationMessage notificationMessage = new NotificationMessage();
        notificationMessage.setAlertNotificationMessage(alertNotificationMessage);

        gsiHealthNotificationMessage.setNotificationMessage(notificationMessage);

        return gsiHealthNotificationMessage;
    }

    /**
     *
     * @author Vishal (Off.)
     *
     */
    public enum NotificationReasonCode {

        PATIENT_CREATION {
            @Override
            public NotificationMessage getNotificationMessage(String orgId, String patientIdentifier, String fromOid, PersonDTO personDTO) {

                notificationMessage = new NotificationMessage();

                PatientCreationNotificationMessage patientCreationNotificationMessage = new PatientCreationNotificationMessage();
                patientCreationNotificationMessage.getPatient().add(buildPatient(orgId, patientIdentifier, fromOid, personDTO));

                notificationMessage.setPatientCreationNotificationMessage(patientCreationNotificationMessage);

                return notificationMessage;
            }
        },
        PATIENT_DEMOG_UPDATE {
            @Override
            public NotificationMessage getNotificationMessage(String orgId, String patientIdentifier, String fromOid, PersonDTO personDTO) {

                notificationMessage = new NotificationMessage();

                PatientDemogUpdateNotificationMessage patientDemogUpdateNotificationMessage = new PatientDemogUpdateNotificationMessage();
                patientDemogUpdateNotificationMessage.getPatient().add(buildPatient(orgId, patientIdentifier, fromOid, personDTO));

                notificationMessage.setPatientDemogUpdateNotificationMessage(patientDemogUpdateNotificationMessage);
                return notificationMessage;
            }
        },
        PATIENT_CONSENT {
            @Override
            public NotificationMessage getNotificationMessage(String orgId, String patientIdentifier, String fromOid, PersonDTO personDTO) {

                notificationMessage = new NotificationMessage();
                HashMap<String, String> consent = new HashMap<String, String>();
                consent.put(orgId, ConsentConstants.CONSENT_PERMIT);

                //What is the consent docId
                String docId = "1234";

                PatientConsentNotificationMessage patientConsentNotificationMessage = buildPatientConsentNotificationMessage(patientIdentifier, orgId, consent, docId);

                notificationMessage.setPatientConsentNotificationMessage(patientConsentNotificationMessage);
                return notificationMessage;
            }
        },
        PATIENT_ENROLLMENT_STATUS_CHANGE {
            @Override
            public NotificationMessage getNotificationMessage(String orgId, String patientIdentifier, String fromOid, PersonDTO personDTO) {

                notificationMessage = new NotificationMessage();

                EnrollmentStatusNotificationMessage enrollmentStatusNotificationMessage = new EnrollmentStatusNotificationMessage();

                List<Patient> patientList = enrollmentStatusNotificationMessage.getPatient();
                Patient thePatient = buildPatient(orgId, patientIdentifier, fromOid);
                patientList.add(thePatient);

                PatientEnrollmentDTO patientEnrollment = personDTO.getPatientEnrollment();
                String enrollmentStatus = patientEnrollment.getStatus();

                String theStatus = null;
                if (StringUtils.equals(enrollmentStatus, WebConstants.ENROLLED) || StringUtils.equals(enrollmentStatus, WebConstants.ASSIGNED)) {
                    theStatus = "ENROLLED";
                } else if (StringUtils.equals(enrollmentStatus, WebConstants.PENDING) || StringUtils.equals(enrollmentStatus, WebConstants.INACTIVE)) {
                    theStatus = "INACTIVE";

                    if (StringUtils.equals(enrollmentStatus, WebConstants.INACTIVE)) {
                        String reasonForInactivation = patientEnrollment.getReasonForInactivation();
                        enrollmentStatusNotificationMessage.setEnrollmentStatusChangeEventDesc(reasonForInactivation);
                    }
                }

                enrollmentStatusNotificationMessage.setEnrollmentStatus(theStatus);

                notificationMessage.setEnrollmentStatusNotificationMessage(enrollmentStatusNotificationMessage);

                return notificationMessage;
            }
        },
        PATIENT_CARE_TEAM_ASSIGNMENT {
            @Override
            public NotificationMessage getNotificationMessage(String orgId, String patientIdentifier, String fromOid, PersonDTO personDTO) {

                notificationMessage = new NotificationMessage();

                CareTeamNotificationMessage careTeamNotificationMessage = new CareTeamNotificationMessage();

                careTeamNotificationMessage.getPatient().add(buildPatient(orgId, patientIdentifier, fromOid));

                notificationMessage.setCareTeamNotificationMessage(careTeamNotificationMessage);

                return notificationMessage;
            }
        };
        /**
         *
         */
        private static NotificationMessage notificationMessage = null;

        /**
         * @param personDTO
         * @return
         */
        public abstract NotificationMessage getNotificationMessage(String orgId, String patientIdentifier, String fromOid, PersonDTO personDTO);
    }
      
    
//    /**
//         * @param organizationDTO
//         * @return
//         */
//        public abstract NotificationMessage getNotificationMessage(String orgId,  String fromOid, OrganizationDTO organizationDTO);
//    }

    public enum OrganizationNotificationReasonCode {
        
         ORGANIZATION_CREATION {
            @Override
            public NotificationMessage getOrganizationNotificationMessage( String fromOid, OrganizationDTO organizationDTO) {

                notificationMessage = new NotificationMessage();
                OrganizationCreationNotificationMessage organizationCreationNotificationMessage = new OrganizationCreationNotificationMessage();
                
                 logger.fine("-check getOrgMessage orgDTO:" 
                        +" org name:" + organizationDTO.getOrganizationName()
                        + " from oid:" + fromOid
                     + " orgDTO Oid:" + organizationDTO.getOID());
                
                organizationCreationNotificationMessage.getOrganization().add(buildOrganization(organizationDTO,fromOid));
                notificationMessage.setOrganizationCreationNotificationMessage(organizationCreationNotificationMessage);
                return notificationMessage;
            }
         },
         ORGANIZATION_UPDATE {
            @Override
            public NotificationMessage getOrganizationNotificationMessage(String fromOid, OrganizationDTO organizationDTO) {

                notificationMessage = new NotificationMessage();
                OrganizationUpdateNotificationMessage organizationUpdateNotificationMessage = new OrganizationUpdateNotificationMessage();
                
                logger.fine("-check getOrgMessage Update orgDTO:" 
                        +" org name:" + organizationDTO.getOrganizationName()
                        + " from oid:" + fromOid
                     + " orgDTO Oid:" + organizationDTO.getOID());
                     
                organizationUpdateNotificationMessage.getOrganization().add(buildOrganization( organizationDTO,fromOid));
                notificationMessage.setOrganizationUpdateNotificationMessage(organizationUpdateNotificationMessage);
                return notificationMessage;
            }
        }; //, for more enums
         
         private static NotificationMessage notificationMessage = null;

        /**
         * @param organizationDTO
         * @return
         */
        public abstract NotificationMessage getOrganizationNotificationMessage(String fromOid, OrganizationDTO organizationDTO);
    }
        
    
    /**
     * Use to send notification to a end point Web Service.
     *
     * @param message - The marshaled message String from the JAX-B
     * @throws Exception
     */
    public static String sendAlert(long communityId, GSIHealthNotificationMessage gsiHealthNotificationMessage) throws Exception {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        String endPoint = configurationDAO.getProperty(communityId, "gsihealth.notification.manager.endpoint.url");
        logger.fine("sending an alert for community " + communityId + " for: " +
                gsiHealthNotificationMessage.getNotificationMetadata().getReasonCode());
        return sendNotification(communityId, gsiHealthNotificationMessage, endPoint);
    }

    /**
     * Use to send notification to a end point Web Service.
     *
     * @param message - The marshaled message String from the JAX-B
     * @throws Exception
     */
    public static String sendConsentMessage(long communityId, GSIHealthNotificationMessage gsiHealthNotificationMessage) throws Exception {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        String endPoint = configurationDAO.getProperty(communityId, "gsihealth.notification.manager.endpoint.url");

        String sendNotificationResult = sendNotification(communityId, gsiHealthNotificationMessage, endPoint);

        return sendNotificationResult;
    }

    /**
     * Use to send notification to a end point Web Service.
     *
     * @param message - The marshaled message String from the JAX-B
     * @throws Exception
     */
    public static String sendOrganizationMessage(long communityId, GSIHealthNotificationMessage gsiHealthNotificationMessage) throws Exception {

        List<org.gsihealth.wsn.Organization> orgs = null;
        
        if (gsiHealthNotificationMessage.getNotificationMessage()
                        .getOrganizationCreationNotificationMessage() != null) {
             orgs = gsiHealthNotificationMessage.getNotificationMessage()
                        .getOrganizationCreationNotificationMessage().getOrganization();
        } else if (gsiHealthNotificationMessage.getNotificationMessage()
                        .getOrganizationUpdateNotificationMessage() != null) {
             orgs = gsiHealthNotificationMessage.getNotificationMessage()
                        .getOrganizationUpdateNotificationMessage().getOrganization();
        }
              
       for (org.gsihealth.wsn.Organization temp: orgs){
           logger.info( "sendorg notification name: " + temp.getOrganizationName() 
                   + " : Oid: " + temp.getOrganizationID());
       }
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        String endPoint = configurationDAO.getProperty(communityId, "gsihealth.notification.manager.endpoint.url");

        String sendNotificationResult = sendNotification(communityId, gsiHealthNotificationMessage, endPoint);

        return sendNotificationResult;
    }
    
    public static GSIHealthNotificationMessage constructUserAlertNotificationMessage(long communityId, String alertLogicalId, String alertMessageBody, String userOid) {
        return constructUserAlertNotificationMessage(communityId, alertLogicalId, alertMessageBody, userOid, userOid);
    }

    public static GSIHealthNotificationMessage constructUserAlertNotificationMessage(long communityId, String alertLogicalId, String alertMessageBody, String fromOid, String toOid) {

        GSIHealthNotificationMessage gsiHealthNotificationMessage = new GSIHealthNotificationMessage();

        NotificationMetadata notificationMetadata = buildNotificationMetaData(AlertConstants.ALERT_NOTIFICATION, AlertConstants.GSI_ADMIN_APP, fromOid, toOid, communityId);
        gsiHealthNotificationMessage.setNotificationMetadata(notificationMetadata);

        AlertNotificationMessage alertNotificationMessage = buildAlertNotificationMessage(alertLogicalId, alertMessageBody);

        NotificationMessage notificationMessage = new NotificationMessage();
        notificationMessage.setAlertNotificationMessage(alertNotificationMessage);

        gsiHealthNotificationMessage.setNotificationMessage(notificationMessage);

        return gsiHealthNotificationMessage;
    }

    public static GSIHealthNotificationMessage constructCareteamAlertNotificationMessage(long communityId, String alertLogicalId, String alertMessageBody, String fromOid, String toOid, String careteamName) {

        GSIHealthNotificationMessage gsiHealthNotificationMessage = new GSIHealthNotificationMessage();

        NotificationMetadata notificationMetadata = buildNotificationMetaData(AlertConstants.ALERT_NOTIFICATION, AlertConstants.GSI_CARETEAM_APP, fromOid, toOid, communityId);
        gsiHealthNotificationMessage.setNotificationMetadata(notificationMetadata);

        AlertNotificationMessage alertNotificationMessage = buildAlertNotificationMessage(alertLogicalId, alertMessageBody);
        if(careteamName != null){
            alertNotificationMessage.setCareTeam(careteamName);
        }
        NotificationMessage notificationMessage = new NotificationMessage();
        notificationMessage.setAlertNotificationMessage(alertNotificationMessage);

        gsiHealthNotificationMessage.setNotificationMessage(notificationMessage);

        return gsiHealthNotificationMessage;
    }

    private static AlertNotificationMessage buildAlertNotificationMessage(String alertLogicalId, String alertMessageBody) {
        AlertNotificationMessage alertNotificationMessage = new AlertNotificationMessage();

        alertNotificationMessage.setAlertId(alertLogicalId);

        AlertMessage alertMessage = new AlertMessage();

        alertMessage.setAlertMessageBody(alertMessageBody);

        alertNotificationMessage.setAlertMessage(alertMessage);

        return alertNotificationMessage;
    }

    private static AlertNotificationMessage buildAlertNotificationMessage(String theOid, String alertLogicalId, String alertMessageBody, PersonDTO thePersonDTO, String careteamName) {
        String patientIdentifier = Long.toString(thePersonDTO.getPatientEnrollment().getPatientId());

        return buildAlertNotificationMessage(alertLogicalId, alertMessageBody, theOid, patientIdentifier, careteamName);
    }

    public static GSIHealthNotificationMessage constructPatientAlertNotificationMessage(long communityId, String alertLogicalId, String fromApplication, String alertMessageBody, PersonDTO thePersonDTO, String careteamName) {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        String gsiOid = configurationDAO.getProperty(communityId, "gsihealth.notification.send.fromOID");

        GSIHealthNotificationMessage gsiHealthNotificationMessage = new GSIHealthNotificationMessage();

        NotificationMetadata notificationMetadata = buildNotificationMetaData(AlertConstants.ALERT_NOTIFICATION, fromApplication, gsiOid, gsiOid, communityId);
        gsiHealthNotificationMessage.setNotificationMetadata(notificationMetadata);

        AlertNotificationMessage alertNotificationMessage = buildAlertNotificationMessage(gsiOid, alertLogicalId, alertMessageBody, thePersonDTO, careteamName);
        if(careteamName != null){
            alertNotificationMessage.setCareTeam(careteamName);
        }
        NotificationMessage notificationMessage = new NotificationMessage();
        notificationMessage.setAlertNotificationMessage(alertNotificationMessage);

        gsiHealthNotificationMessage.setNotificationMessage(notificationMessage);

        return gsiHealthNotificationMessage;
    }

    public static CommunityDAO getCommunityDAO() {
        return communityDAO;
    }

    public static void setCommunityDAO(CommunityDAO communityDAO) {
        NotificationUtils.communityDAO = communityDAO;
    }

    public static PatientProgramDAO getPatientProgramDAO() {
        return patientProgramDAO;
    }

    public static void setPatientProgramDAO(PatientProgramDAO patientProgramDAO) {
        NotificationUtils.patientProgramDAO = patientProgramDAO;
    }

    public static ReferenceDataDAO getReferenceDataDAO() {
        return referenceDataDAO;
    }

    public static void setReferenceDataDAO(ReferenceDataDAO referenceDataDAO) {
        NotificationUtils.referenceDataDAO = referenceDataDAO;
    }
    
}
