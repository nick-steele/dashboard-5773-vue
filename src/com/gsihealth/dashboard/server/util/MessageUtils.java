package com.gsihealth.dashboard.server.util;

/**
 *
 * @author Chad Darby
 */
public class MessageUtils {
    
    public static String sanitize(String message) {
        String result = "";

        // RegEx to filter log messages
        if (message.indexOf("for") != -1) {
            //keep sentence before "for" and delete up to Patient ID
            result = message.replaceAll(" for.*,", " for");
        } else {
            //delete up to "Patient ID"
            result = message.replaceAll("<ns3:AlertMessageBody>.*Patient ID", "<ns3:AlertMessageBody>Patient ID"); //almost <ns3:AlertMessageBody>.*, ", "<ns3:AlertMessageBody>");
        }

        return result;
    }
}
