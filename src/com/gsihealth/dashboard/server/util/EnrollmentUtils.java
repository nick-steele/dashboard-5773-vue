package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.common.Person;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;

import java.util.logging.Logger;

/**
 *
 * @author Chad Darby
 */
public class EnrollmentUtils {

    private static Logger logger = Logger.getLogger(EnrollmentUtils.class.getName());

    /**
     * Business logic to determine if we need to send an enrollment status change to BHIX
     * 
     * @param oldEnrollmentStatus
     * @param newEnrollmentStatus
     * @return 
     */
    public static boolean computeSendEnrollmentStatusFlagForBHIX(String oldEnrollmentStatus, String newEnrollmentStatus) {

        boolean sendEnrollmentStatusFlag = !StringUtils.equals(oldEnrollmentStatus, newEnrollmentStatus) && ((StringUtils.equals(oldEnrollmentStatus, WebConstants.PENDING) && !StringUtils.equals(newEnrollmentStatus, WebConstants.INACTIVE))
                || (StringUtils.equals(oldEnrollmentStatus, WebConstants.INACTIVE) && !StringUtils.equals(newEnrollmentStatus, WebConstants.PENDING))
                || (StringUtils.equals(oldEnrollmentStatus, WebConstants.ENROLLED) && !StringUtils.equals(newEnrollmentStatus, WebConstants.ASSIGNED))
                || (StringUtils.equals(oldEnrollmentStatus, WebConstants.ASSIGNED) && !StringUtils.equals(newEnrollmentStatus, WebConstants.ENROLLED))
                || (StringUtils.isBlank(oldEnrollmentStatus) && StringUtils.equals(newEnrollmentStatus, WebConstants.ENROLLED))
                || (StringUtils.isBlank(oldEnrollmentStatus) && StringUtils.equals(newEnrollmentStatus, WebConstants.ASSIGNED)));

        logger.info("sendEnrollmentStatusFlag=" + sendEnrollmentStatusFlag);

        return sendEnrollmentStatusFlag;
    }

    /**
     * Returns true if search criteria has demographic data
     * 
     * @param searchCriteria
     * @return 
     */
    public static boolean hasDemographicData(SearchCriteria searchCriteria) {
        boolean result = false;

        result = StringUtils.isNotBlank(searchCriteria.getFirstName())
                || StringUtils.isNotBlank(searchCriteria.getLastName())
                || StringUtils.isNotBlank(searchCriteria.getMiddleName())
                || StringUtils.isNotBlank(searchCriteria.getDateOfBirthStr())
                || StringUtils.isNotBlank(searchCriteria.getGender())
                || StringUtils.isNotBlank(searchCriteria.getAddress1())
                || StringUtils.isNotBlank(searchCriteria.getAddress2())
                || StringUtils.isNotBlank(searchCriteria.getCity())
                || StringUtils.isNotBlank(searchCriteria.getState())
                || StringUtils.isNotBlank(searchCriteria.getZipCode())
                || StringUtils.isNotBlank(searchCriteria.getPhone())
                || StringUtils.isNotBlank(searchCriteria.getSsn())
                || StringUtils.isNotBlank(searchCriteria.getMinor());
        logger.info("????????????????????????????????????????????????has demo:" + result);
        
        return result;
    }
    
    public static boolean hasDemographicDataChanged(Person personOne, Person personTwo) {
        
        if (personOne == null || personTwo == null) {
            throw new IllegalArgumentException("Can not compare null patients");
        }
        
        boolean equals = new EqualsBuilder()
                .append(personOne.getFirstName(), personTwo.getFirstName())
                .append(personOne.getLastName(), personTwo.getLastName())
                .append(personOne.getMiddleName(), personTwo.getMiddleName())
                .append(personOne.getGender(), personTwo.getGender())
                .append(personOne.getStreetAddress1(), personTwo.getStreetAddress1())
                .append(personOne.getStreetAddress2(), personTwo.getStreetAddress2())
                .append(personOne.getCity(), personTwo.getCity())
                .append(personOne.getState(), personTwo.getState())
                .append(personOne.getZipCode(), personTwo.getZipCode())
                .append(personOne.getCounty(), personTwo.getCounty())
                .append(personOne.getTelephone(), personTwo.getTelephone())
                .append(personOne.getSsn(), personTwo.getSsn())
                .isEquals();
        boolean datesEqual = DateUtils.equalsDatePortionOnly(
                personOne.getDateOfBirth(), personTwo.getDateOfBirth());
        boolean allEqual = datesEqual && equals;
        return !allEqual;        
    }
    
}
