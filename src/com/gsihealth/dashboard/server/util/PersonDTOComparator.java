package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.common.dto.PersonDTO;
import java.util.Comparator;

/**
 * Compare two PersonDTO objects
 * 
 * @author Chad Darby
 */
public class PersonDTOComparator implements Comparator<PersonDTO> {

    @Override
    public int compare(PersonDTO person1, PersonDTO person2) {
        String lastName1 = person1.getLastName().toUpperCase();
        String firstName1 = person1.getFirstName().toUpperCase();

        String lastName2 = person2.getLastName().toUpperCase();
        String firstName2 = person2.getFirstName().toUpperCase();

        if (!lastName1.equals(lastName2)) {
            return lastName1.compareTo(lastName2);
        }
        else {
            return firstName1.compareTo(firstName2);
        }
    }

}
