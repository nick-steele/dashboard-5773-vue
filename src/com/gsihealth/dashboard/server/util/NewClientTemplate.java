/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.dto.PersonDTO;

import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;
import com.gsihealth.dashboard.entity.dto.ProgramHealthHomeDTO;
import com.gsihealth.dashboard.entity.dto.insurance.PayerClassDTO;
import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.reports.PatientTrackingReportData;
import com.gsihealth.entity.PatientTrackingSheet;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.gsihealth.entity.Program;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;

/**
 *
 * @author Satyendra Singh
 */
public class NewClientTemplate {

    private String[] topData = {"Organization Name:", "Date Of Submission:", "MMIS ID#"};
    private String[] topColumns;
    private XSSFSheet firstSheet;
    private XSSFWorkbook workbook;
    private static final Logger logger = Logger.getLogger(ClientTemplate.class.getName());

    private static final int PATIENT_ID_COLUMN = 0;
//    private static final int PATIENT_EUID_COLUMN = 1;
    private static final int MEDICAID_ID_COLUMN = 1;
    private static final int DOB_COLUMN = 2;
    private static final int GENDER_COLUMN = 3;
    private static final int MINOR_COLUMN = 4;
    private static final int CREATED_DATE_COLUMN = 5;
    private static final int END_DATE_COLUMN = 6;
    private static final int CARE_MANAGEMENT_AGENCY_COLUMN = 7;
    private static final int CARE_MANAGEMENT_MMIS_ID_COLUMN = 8;
    private static final int ENROLLMENT_STATUS_COLUMN = 10;
    private static final int ENROLLMENT_STATUS_EFFECTIVE_DATE_COLUMN = 11;
    private static final int ENROLL_CONSENT_DATE_COLUMN = 12;
    private static final int PRIMARY_PAYER_PLAN_COLUMN = 13;
    private static final int SECONDARY_PAYER_PLAN_COLUMN = 14;
    private static final int DISENROLLMENT_CODE_COLUMN = 15;
    private static final int DISENROLLMENT_CODE_ID_COLUMN = 16;
    private static final int NYSID_COLUMN = 17;
    private static final int ACUITY_SCORE_COLUMN = 18;
    private static final int FIRST_NAME_COLUMN = 19;
    private static final int LAST_NAME_COLUMN = 20;
    private static final int STREET_ADDRESS1_COLUMN = 21;
    private static final int STREET_ADDRESS2_COLUMN = 22;
    private static final int CITY_COLUMN = 23;
    private static final int STATE_COLUMN = 24;
    private static final int ZIP_CODE_COLUMN = 25;
    private static final int PRIMARY_PHONE_COLUMN = 26;
    private static final int ALTERNATE_PHONE_COLUMN = 27;
    private static final int DATE_OF_EARLIEST_CONTACT_COLUMN = 28;
    private static final int DATE_OF_MOST_RECENT_CONTACT_COLUMN = 29;
    private static final int MAIL_COUNT_COLUMN = 30;
    private static final int PHONE_COUNT_COLUMN = 31;
    private static final int PERSON_COUNT_COLUMN = 32;

//    private static final int PROGRAM_NAME_COLUMN = 18;
//    private static final int HEALTH_HOME_COLUMN = 7;
//    private static final int HEALTH_HOME_MMIS_ID_COLUMN=7;
    private boolean hideNYSIDColumn;
    private String disenrollmentCodeTitle;
    private String primaryCMALabel;
    private boolean CHHFlag;

    public NewClientTemplate() {
    }

    public NewClientTemplate(boolean flag) {
        hideNYSIDColumn = flag;
    }

    public NewClientTemplate(boolean flag, boolean CHH, String theDisenrollmentCodeTitle, String primaryCMALabel) {
        hideNYSIDColumn = flag;
        CHHFlag = CHH;
        this.disenrollmentCodeTitle = theDisenrollmentCodeTitle;
        this.primaryCMALabel = primaryCMALabel;
        topColumns = new String[]{"Dashboard MRN", "CIN", "Date Of Birth", "Gender", "Minor", "Patient Create Date", "Most Recent Inactive Date", this.primaryCMALabel, this.primaryCMALabel.trim() + " MMIS ID", "Patient Source", "Patient Status", "Enrollment Status Effective Date", "Consent Date","Primary Payer Plan","Secondary Payer Plan", "Disenrollment Reason", disenrollmentCodeTitle, "NYSID", "Acuity Score", "First Name", "Last Name", "Member's Address", "Address Line 2", "City", "State", "Zip", "Primary Phone Number", "Alternate Phone Number", "Date Of Earliest Contact", "Date Of Most Recent Contact", "Count Mail", "Count Phone", "Count Person"};
    }

    public static void main(String[] args) {
        NewClientTemplate template = new NewClientTemplate(true);
        //template.createTopColums();
        template.createHeaderColumns();

        /*
         * stream where the workbook content will be written to.
         */
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(new File("xyzPatientTrackingTemplate.xlsx"));
            template.workbook.write(fos);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error: " + e.getMessage(), e);
        } finally {
            if (fos != null) {
                try {
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    logger.log(Level.SEVERE, "Error: " + e.getMessage(), e);
                }
            }
        }
    }

//    public void createTopColums() {
//        /*
//         * Creating an instance of XSSFWorkbook.
//         */
//        try {
//            workbook = new XSSFWorkbook();
//
//            /*
//             * Bold font
//             */
//            Font font = workbook.createFont();
//            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
//
//            /*
//             * Create one sheet in the excel document and name it Sheet
//             */
//            firstSheet = workbook.createSheet("Sheet1");
//
//            /*
//             * Top columns
//             */
//            XSSFRow row1 = firstSheet.createRow(0);
//            XSSFCell cell2 = row1.createCell(2);
//            XSSFCell cell3 = row1.createCell(3);
//            XSSFCell cell4 = row1.createCell(4);
//
//            /*
//             * merging rows
//             */
//            firstSheet.addMergedRegion(new CellRangeAddress(0, 0, 2, 4));
//
//            /*
//             * Yellow background
//             */
//            CellStyle yellowColorStyle = workbook.createCellStyle();
//            yellowColorStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
//            yellowColorStyle.setFont(font);
//
//            yellowColorStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
//            cell2.setCellStyle(yellowColorStyle);
//            cell2.setCellValue(new XSSFRichTextString(""));
//            cell3.setCellStyle(yellowColorStyle);
//            cell3.setCellValue(new XSSFRichTextString(""));
//            cell4.setCellStyle(yellowColorStyle);
//            cell4.setCellValue(new XSSFRichTextString(""));
//
//            XSSFRow[] row = new XSSFRow[4];
//            /*
//             * Grey background
//             */
//            XSSFCellStyle greyColorStyle = workbook.createCellStyle();
//            greyColorStyle.setFillForegroundColor(new XSSFColor(Color.decode("#C5D9F1")));//new XSSFColor(new java.awt.Color(194, 214, 154)));
//            greyColorStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
//            greyColorStyle.setAlignment(CellStyle.ALIGN_CENTER);
//            greyColorStyle.setWrapText(true);
//            greyColorStyle.setBorderBottom(BorderStyle.THIN);
//            greyColorStyle.setBorderTop(BorderStyle.THIN);
//            greyColorStyle.setBorderRight(BorderStyle.THIN);
//            greyColorStyle.setBorderLeft(BorderStyle.THIN);
//            greyColorStyle.setVerticalAlignment((short) 1);
//
//            for (int i = 1; i <= 4; i++) {
//                row[i - 1] = firstSheet.createRow(i);
//
//                for (int j = 2; j <= 4; j++) {
//                    if (i == 1 && j == 2) {
//                        XSSFCell cellA = row[i - 1].createCell(j);
//                        cellA.setCellStyle(greyColorStyle);
//                        cellA.setCellValue(new XSSFRichTextString(topData[i - 1]));
//                    } else if (i == 1 && j == 3) {
//                        XSSFCell cellB = row[i - 1].createCell(j);
//                        cellB.setCellStyle(greyColorStyle);
//
//                    } else if (i == 1 && j == 4) {
//                        XSSFCell cellB = row[i - 1].createCell(j);
//                        cellB.setCellStyle(greyColorStyle);
//
//                    } else if (i == 2 && j == 2) {
//                        XSSFCell cellB = row[i - 1].createCell(j);
//                        cellB.setCellStyle(greyColorStyle);
//                        cellB.setCellValue(new XSSFRichTextString(topData[i - 1]));
//                    } else if (i == 2 && j == 3) {
//                        XSSFCell cellD = row[i - 1].createCell(j);
//                        cellD.setCellStyle(greyColorStyle);
//
//                    } else if (i == 2 && j == 4) {
//                        XSSFCell cellD = row[i - 1].createCell(j);
//                        cellD.setCellStyle(greyColorStyle);
//
//                    } else if (i == 3 && j == 2) {
//                        XSSFCell cellC = row[i - 1].createCell(j);
//                        cellC.setCellStyle(greyColorStyle);
//                        cellC.setCellValue(new XSSFRichTextString(topData[i - 1]));
//                    } else if (i == 3 && j == 3) {
//                        XSSFCell cellC = row[i - 1].createCell(j);
//                        cellC.setCellStyle(greyColorStyle);
//
//                    } else if (i == 3 && j == 4) {
//                        XSSFCell cellC = row[i - 1].createCell(j);
//                        cellC.setCellStyle(greyColorStyle);
//                        XSSFCell cellD = row[i - 1].createCell(35);
//                        XSSFCell cellE = row[i - 1].createCell(36);
//                        XSSFCell cellF = row[i - 1].createCell(37);
//                        XSSFCell cellG = row[i - 1].createCell(38);
//                        cellD.setCellStyle(greyColorStyle);
//                        cellE.setCellStyle(greyColorStyle);
//                        cellF.setCellStyle(greyColorStyle);
//                        cellG.setCellStyle(greyColorStyle);
//                        cellD.setCellStyle(greyColorStyle);
//                        cellD.setCellValue(new XSSFRichTextString("Only complete for patients during the outreach period"));
//                        firstSheet.addMergedRegion(new CellRangeAddress(3, 3, 35, 38));
//                    }
//                }
//            }
//
//            firstSheet.addMergedRegion(new CellRangeAddress(1, 1, 3, 4));
//            firstSheet.addMergedRegion(new CellRangeAddress(2, 2, 3, 4));
//            firstSheet.addMergedRegion(new CellRangeAddress(3, 3, 3, 4));
//
//        } catch (Exception e) {
//            logger.log(Level.SEVERE, "Error building spreadsheet", e);
//        }
//    }
    public void createHeaderColumns() {

        try {
            workbook = new XSSFWorkbook();

            /*
             * Create one sheet in the excel document and name it Sheet
             */
            firstSheet = workbook.createSheet("Sheet1");
            /*
             * Bold font
             */

            Font font = workbook.createFont();
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            font.setColor(new XSSFColor(new java.awt.Color(39, 51, 89)).getIndexed());

            /*
             * Green color style
             */
            XSSFCellStyle greenColorStyle = workbook.createCellStyle();
            greenColorStyle.setFillForegroundColor(new XSSFColor(Color.decode("#C2D69A")));//new XSSFColor(new java.awt.Color(194, 214, 154)));
            greenColorStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            greenColorStyle.setAlignment(CellStyle.ALIGN_CENTER);
            greenColorStyle.setVerticalAlignment((short) 1);
            greenColorStyle.setWrapText(true);
            greenColorStyle.setBorderBottom(BorderStyle.THIN);
            greenColorStyle.setBorderTop(BorderStyle.THIN);
            greenColorStyle.setBorderRight(BorderStyle.THIN);
            greenColorStyle.setBorderLeft(BorderStyle.THIN);
            greenColorStyle.setFont(font);

            XSSFCellStyle colorsStyle = workbook.createCellStyle();
            colorsStyle.setFillForegroundColor(new XSSFColor(Color.decode("#B8CCE4")));//new XSSFColor(new java.awt.Color(194, 214, 154)));
            colorsStyle.setAlignment(CellStyle.ALIGN_CENTER);
            colorsStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            colorsStyle.setBorderBottom(BorderStyle.THIN);
            colorsStyle.setBorderTop(BorderStyle.THIN);
            colorsStyle.setBorderRight(BorderStyle.THIN);
            colorsStyle.setBorderLeft(BorderStyle.THIN);
            colorsStyle.setVerticalAlignment((short) 1);

            int size = topColumns.length;
            XSSFCell[] noCell = new XSSFCell[size];
            XSSFRow row1 = firstSheet.createRow(1);
            //if (i == 3 && j == 4) {
            XSSFCell cellC = row1.createCell(3);

            XSSFCell cellD = row1.createCell(30);
            XSSFCell cellE = row1.createCell(31);
            XSSFCell cellF = row1.createCell(32);

            cellD.setCellStyle(colorsStyle);
            cellE.setCellStyle(colorsStyle);
            cellF.setCellStyle(colorsStyle);

            cellD.setCellValue(new XSSFRichTextString("Only complete for patients during the outreach period"));
            firstSheet.addMergedRegion(new CellRangeAddress(1, 1, 30, 32));

            XSSFRow row2 = firstSheet.createRow(2);

            row2.setHeightInPoints(43);
            for (int i = 0; i < size; i++) {
                firstSheet.setDefaultColumnWidth((short) 15);

                if (i == 30 || i == 31 || i == 32) {

                    noCell[i] = row2.createCell(i);
                    noCell[i].setCellValue(new XSSFRichTextString(topColumns[i]));
                    noCell[i].setCellStyle(colorsStyle);

//                noCell[i] = row1.createCell(i);
//                noCell[i].setCellStyle(colorsStyle);
//                noCell[i].setCellValue(new XSSFRichTextString("Only complete for patients during the outreach period"));
//                firstSheet.addMergedRegion(new CellRangeAddress(1, 1, 30, 32));
                } else {
                    noCell[i] = row2.createCell(i);
                    noCell[i].setCellValue(new XSSFRichTextString(topColumns[i]));
                    noCell[i].setCellStyle(greenColorStyle);

                }
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error building spreadsheet", e);
        }
    }

    public void populate(Program program, List<PatientTrackingSheet> persons, String organizationName, String mmisId) {
        int size = persons.size();
        
        /*
         * populate organization name cell value
         */
//        XSSFRow orgRow = firstSheet.getRow(1);
//        XSSFCell organizationNameCell = orgRow.getCell(3);
//        organizationNameCell.setCellValue(organizationName);

        /*
         * hiding NYSID column
         */
        if (hideNYSIDColumn) {
            firstSheet.setColumnHidden(NYSID_COLUMN, hideNYSIDColumn);
        }
        if (!CHHFlag) {
            boolean hideCHHFlag = true;
            firstSheet.setColumnHidden(MINOR_COLUMN, hideCHHFlag);
        }
        /*
         * populate MMIS ID cell value
         */
//        XSSFRow mmisRow = firstSheet.getRow(3);
//        XSSFCell mmisIdCell = mmisRow.getCell(3);
//        mmisIdCell.setCellValue(mmisId);
//
//        /*
//         * setup date cell style for DOB
//         */
        XSSFDataFormat dataFormat = workbook.createDataFormat();
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(dataFormat.getFormat("MM/dd/yyyy"));
      

        PatientTrackingSheet tempPerson = null;
        XSSFRow tempRow = null;
        XSSFCell tempCell = null;

        //populate program information
        if(program != null) {
            Font font = workbook.createFont();
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            font.setColor(new XSSFColor(new java.awt.Color(39, 51, 89)).getIndexed());

            XSSFCellStyle greenColorStyle = workbook.createCellStyle();
            greenColorStyle.setFillForegroundColor(new XSSFColor(Color.decode("#C2D69A")));//new XSSFColor(new java.awt.Color(194, 214, 154)));
            greenColorStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            greenColorStyle.setAlignment(CellStyle.ALIGN_LEFT);
            greenColorStyle.setVerticalAlignment((short) 1);
            greenColorStyle.setWrapText(true);
//            greenColorStyle.setBorderBottom(BorderStyle.THIN);
//            greenColorStyle.setBorderTop(BorderStyle.THIN);
//            greenColorStyle.setBorderRight(BorderStyle.THIN);
//            greenColorStyle.setBorderLeft(BorderStyle.THIN);
            greenColorStyle.setFont(font);

            tempRow = firstSheet.createRow(0);
            tempCell = tempRow.createCell(0);
            tempCell.setCellStyle(greenColorStyle);
            tempCell.setCellValue("The Patient Tracking Sheet Report of " + program.getName());
            firstSheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 32));
        }

         /*
         * populate patient data
         */
        for (int row = 0; row < size; row++) {

            tempPerson = persons.get(row);
            tempRow = firstSheet.createRow(3 + row);
            //boolean inactivePatient = StringUtils.equalsIgnoreCase(tempPerson.getEnrollmentStatus(), WebConstants.INACTIVE);
            /*
             * get care plan report data for this patient
             */
           // PatientEnrollmentDTO patientEnrollment = tempPerson.getPatientEnrollment();
            //long patientId = patientEnrollment.getPatientId();

            /*
             * get report data
             */
          //  PatientTrackingReportData patientTrackingReportData = patientTrackingReportDataMap.get(patientId);
            //   boolean hasPatientTrackingReportData = patientTrackingReportData != null;

            /*
             * creating cells
             */
            tempCell = null;
            for (int col = 0; col <= 32; col++) {
                tempCell = tempRow.createCell(col);

                
                switch (col) {
                    case FIRST_NAME_COLUMN: {
                        tempCell.setCellValue(tempPerson.getFirstName());
                        break;
                    }
                    case LAST_NAME_COLUMN: {
                        tempCell.setCellValue(tempPerson.getLastName());
                        break;
                    }

                    case MEDICAID_ID_COLUMN: {
                        tempCell.setCellValue(tempPerson.getCin());
                        break;
// setMedicaidId(patientEnrollment, tempCell);
                    }
                    case DOB_COLUMN: {
                        setDate(tempCell, dateCellStyle, tempPerson.getDob());
                        break;
                    }
                    case GENDER_COLUMN: {
                        tempCell.setCellValue(tempPerson.getGender());
                        break;
                    }
                    case STREET_ADDRESS1_COLUMN: {
                        tempCell.setCellValue(tempPerson.getAddressLine1());
                        break;
                    }
                    case STREET_ADDRESS2_COLUMN: {
                        tempCell.setCellValue(tempPerson.getAddressLine2());
                        break;
                    }
                    case CITY_COLUMN: {
                        tempCell.setCellValue(tempPerson.getCity());
                        break;
                    }
                    case STATE_COLUMN: {
                        tempCell.setCellValue(tempPerson.getState());
                        break;
                    }
                    case ZIP_CODE_COLUMN: {
                        tempCell.setCellValue(tempPerson.getZipCode());
                        break;
                    }
                    case PRIMARY_PHONE_COLUMN: {
                        tempCell.setCellValue(tempPerson.getPrimePhone());
                        break;
                    }
                    case ALTERNATE_PHONE_COLUMN: {
                        tempCell.setCellValue(tempPerson.getAlternatePhone());
                        break;
                    }
                    case ENROLL_CONSENT_DATE_COLUMN: {

                        setDate(tempCell, dateCellStyle, tempPerson.getConsentDate());
                        break;
                    }
                     case PRIMARY_PAYER_PLAN_COLUMN: {

                        tempCell.setCellValue(tempPerson.getPrimaryPayerPlan());
                        break;
                    }
                      case SECONDARY_PAYER_PLAN_COLUMN: {

                        tempCell.setCellValue(tempPerson.getSecondaryPayerPlan());
                        break;
                    }
                    case DATE_OF_EARLIEST_CONTACT_COLUMN: /*&& hasPatientTrackingReportData*/ {

                        setDate(tempCell, dateCellStyle, tempPerson.getDateOfEarliestContact());
                        break;
                    }
                    case DATE_OF_MOST_RECENT_CONTACT_COLUMN /*&& hasPatientTrackingReportData*/: {

                        setDate(tempCell, dateCellStyle, tempPerson.getDateOfMostRecentContact());
                        break;
                    }
                    case MAIL_COUNT_COLUMN:{
                    countCellFormat(tempPerson.getCountMail(), tempCell);
                        break;
                    }
                    case PHONE_COUNT_COLUMN: {
                        countCellFormat(tempPerson.getCountPhone(), tempCell);                        
                        break;
                    }
                    case PERSON_COUNT_COLUMN: {
                        countCellFormat(tempPerson.getCountPerson(), tempCell);
                        break;
                    }
                    case ACUITY_SCORE_COLUMN: {
                        tempCell.setCellValue(tempPerson.getAcuityScore());
                        break;
                    }
//                else if(col==HEALTH_HOME_COLUMN){
//                    if(patientEnrollment.getProgramHealthHome()!=null){
//                        String healthHome= getHealthHome(tempPerson);
//                        tempCell.setCellValue(healthHome);
//                    }
//                 } 
//                else if(col==HEALTH_HOME_MMIS_ID_COLUMN){
//                    if(patientEnrollment.getProgramHealthHome()!=null){
//                        String healthHomeMmis= getHealthHomeMmisId(tempPerson);
//                        tempCell.setCellValue(healthHomeMmis);
//                    }
//                 }
                    case CARE_MANAGEMENT_AGENCY_COLUMN: {
                        tempCell.setCellValue(tempPerson.getCareManagementAgency());
                        break;
                    }
                    case CARE_MANAGEMENT_MMIS_ID_COLUMN /*&& hasPatientTrackingReportData*/: {
                        tempCell.setCellValue(tempPerson.getCareManagementAgencyMMISId());
                        break;
                    }

                    case CREATED_DATE_COLUMN: {

                        setDate(tempCell, dateCellStyle, tempPerson.getPatientCreateDate());
                        break;
                    }
                    case END_DATE_COLUMN: {

                        setDate(tempCell, dateCellStyle, tempPerson.getMostRecentInactiveDate());
                        break;
                    }
                    case ENROLLMENT_STATUS_COLUMN: {
                        tempCell.setCellValue(tempPerson.getEnrollmentStatus());
                        break;
                    }
                    case ENROLLMENT_STATUS_EFFECTIVE_DATE_COLUMN: {

                        setDate(tempCell, dateCellStyle, tempPerson.getEnrollmentStatusEffectiveDate());
                        break;
                    }
                    case DISENROLLMENT_CODE_COLUMN: {
                        if (tempPerson.getDisenrollmentReasondise() != null) {
                            tempCell.setCellValue(tempPerson.getDisenrollmentReasondise());

                        }
                        break;
                    }
                    case DISENROLLMENT_CODE_ID_COLUMN: {
                        if (tempPerson.getDisrenrollmentReasonCode() != null) {
                            tempCell.setCellValue(tempPerson.getDisrenrollmentReasonCode());
                        }
                        break;
                    }
                    case MINOR_COLUMN: {
                        tempCell.setCellValue(tempPerson.getMinor());
                        break;
                    }
//                else if (col == PROGRAM_NAME_COLUMN) {
//                    tempCell.setCellValue(patientEnrollment.getProgramName());
//                }
                    case PATIENT_ID_COLUMN: {
                        tempCell.setCellValue(tempPerson.getDashboardMRN());
                        break;
                    }
//                else if (col == PATIENT_EUID_COLUMN) {
//                    tempCell.setCellValue(patientEnrollment.getPatientEuid());
//                }
                }
            }
        }
    }

    public void countCellFormat(String countInString, XSSFCell tempCell) throws NumberFormatException {
        if(countInString != null && !countInString.isEmpty() && !countInString.equalsIgnoreCase("0.0"))
            tempCell.setCellValue(((Double)Double.parseDouble(countInString)).intValue());
        else if(countInString != null && !countInString.isEmpty() && countInString.equalsIgnoreCase("0.0"))
            tempCell.setCellValue(0);
        else
            tempCell.setCellValue(countInString);
    }

    protected void setBillingProviderMmisId(PatientEnrollmentDTO patientEnrollment, XSSFCell tempCell) {
        PayerPlanDTO primaryPayerPlan = patientEnrollment.getPrimaryPayerPlan();

        if (primaryPayerPlan != null) {
            String primaryPayerPlanMmisId = primaryPayerPlan.getMmisId();
            tempCell.setCellValue(primaryPayerPlanMmisId);
        }
    }

    protected void setPersonCount(boolean hasPatientTrackingReportData, XSSFCell tempCell, PatientTrackingReportData patientTrackingReportData) {
        if (hasPatientTrackingReportData) {
            tempCell.setCellValue(patientTrackingReportData.getPersonCount());
        } else {
            tempCell.setCellValue(0);
        }
    }

    protected void setPhoneCount(boolean hasPatientTrackingReportData, XSSFCell tempCell, PatientTrackingReportData patientTrackingReportData) {
        if (hasPatientTrackingReportData) {
            tempCell.setCellValue(patientTrackingReportData.getPhoneCount());
        } else {
            tempCell.setCellValue(0);
        }
    }

    protected void setMailCount(boolean hasPatientTrackingReportData, XSSFCell tempCell, PatientTrackingReportData patientTrackingReportData) {
        if (hasPatientTrackingReportData) {
            tempCell.setCellValue(patientTrackingReportData.getMailCount());
        } else {
            tempCell.setCellValue(0);
        }
    }

    protected void setDate(XSSFCell tempCell, CellStyle dateCellStyle, Date theDate) {
        // add special date cell style
        tempCell.setCellStyle(dateCellStyle);

        if (theDate != null) {
            tempCell.setCellValue(theDate);
        }
    }

    protected static Map<Long, PatientTrackingReportData> buildPatientTrackingReportDataMap() {
        Map<Long, PatientTrackingReportData> theMap = new HashMap<Long, PatientTrackingReportData>();

        PatientTrackingReportData data = null;

        // person 1
        data = new PatientTrackingReportData();
        data.setEarliestContactDate(getDate(11, 13, 2007));
        data.setMailCount(2);
        data.setPhoneCount(4);
        data.setPersonCount(6);
        data.setCareManagementMmisId("987654321");
        theMap.put(1L, data);

        // person 3
        data = new PatientTrackingReportData();
        data.setLatestContactDate(getDate(5, 12, 2008));
        theMap.put(3L, data);

        // person 4
        data = new PatientTrackingReportData();
        data.setInitialAssessmentDate(getDate(7, 4, 2009));
        theMap.put(4L, data);

        return theMap;
    }

    protected static Date getDate(int month, int day, int year) {
        Date theDate = null;

        try {
            String dateStr = month + "/" + day + "/" + year;
            theDate = new SimpleDateFormat("MM/dd/yyyy").parse(dateStr);
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error: " + exc.getMessage(), exc);
        }

        return theDate;
    }

    public XSSFWorkbook getWorkbook() {
        return workbook;
    }

    public void setWorkbook(XSSFWorkbook workbook) {
        this.workbook = workbook;
    }

    private void setMedicaidId(PatientEnrollmentDTO patientEnrollment, XSSFCell tempCell) {
        PayerClassDTO primaryPayerClass = patientEnrollment.getPrimaryPayerClass();
        PayerClassDTO secondaryPayerClass = patientEnrollment.getSecondaryPayerClass();
        //boolean isPrimaryClassFound = false;

        /*
         * Including Secondary Insurance section as well
         */
        if ((primaryPayerClass != null) && (primaryPayerClass.getId() != null) && (primaryPayerClass.getId() == 1)) {

            tempCell.setCellValue(patientEnrollment.getPrimaryPayerMedicaidMedicareId());

        } else if ((secondaryPayerClass != null) && (secondaryPayerClass.getId() != null) && (secondaryPayerClass.getId() == 1)) {

            tempCell.setCellValue(patientEnrollment.getSecondaryPayerMedicaidMedicareId());

        }

    }

    public static String getHealthHome(PersonDTO personDTO) {

        String result = "";
        ProgramHealthHomeDTO programHealthHome = personDTO.getPatientEnrollment().getProgramHealthHome();
        if (programHealthHome != null) {
            result = programHealthHome.getHealthHomeName();
        }
        return result;
    }

    public static String getHealthHomeMmisId(PersonDTO personDTO) {

        String result = "";
        ProgramHealthHomeDTO programHealthHome = personDTO.getPatientEnrollment().getProgramHealthHome();
        if (programHealthHome != null) {
            result = programHealthHome.getHealthHomeMmisId();
        }
        return result;
    }
}
