package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.client.util.StringUtils;
import com.smartgwt.client.widgets.Canvas;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.MutableDateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * Collection of date utilities
 * 
 * @author Chad Darby
 */
public class DateUtils {
    private static final Logger logger = Logger.getLogger(DateUtils.class.getName());
    /**
     * Returns the number of days between date1 and date2.
     *
     * Value is the absolute number of days. Does not care if date1
     * is before/after date2.
     * 
     * @param date1
     * @param pastDate
     * @return
     */
    public static int daysBetween(Date date1, Date date2) {
        Days daysBetween = Days.daysBetween(new DateTime(date1), new DateTime(date2));

        int days = Math.abs(daysBetween.getDays());

        return days;
    }
    
    /**
     * Combines the date and time
     * 
     * @param date
     * @param time in format hh:mm
     * @return 
     */
    public static Date combineDateTime(Date date, String time, String ampm) {
        Date resultDate = null;
        
        DateTime dt = new DateTime(date);
        
        MutableDateTime mdt = dt.toMutableDateTime();
        
        String[] timeData = time.split(":");
        
        int hourOfDay = Integer.parseInt(timeData[0]);

        if (StringUtils.equalsIgnoreCase(ampm, "PM") && hourOfDay < 12) {
            hourOfDay += 12;
        }
        
        mdt.setHourOfDay(hourOfDay);
        mdt.setMinuteOfHour(Integer.parseInt(timeData[1]));

        resultDate = mdt.toDate();
        
        return resultDate;
    }
    
    /**
     * Returns a noon version of the date. This method only changes the time field to "12:00:00".
     * 
     * @param date
     * @return 
     */
    public static Date getDateNoon(Date date) {
        
        DateTime dt = new DateTime(date);

        MutableDateTime mdt = dt.toMutableDateTime();

        mdt.setHourOfDay(12);
        mdt.setMinuteOfHour(0);
        mdt.setSecondOfMinute(0);
        mdt.setMillisOfSecond(0);

        Date resultDate = mdt.toDate();
        
        return resultDate;     
    }
    
    /**
     * no null check for dob
     * @param dob
     * @param calcToDate - null for today's date
     * @return age based on difference between dob and calcToDate
     */
    public static int getAgeInYears(Date dob, Date calcToDate){
        LocalDate bornOn = new LocalDate(dob);
        LocalDate toDate = calcToDate == null ? new LocalDate(calcToDate) : new LocalDate();
        return DateUtils.getPeriodBetweenTwoDates(bornOn, toDate).getYears();
    }
    
    public static Period getPeriodBetweenTwoDates(LocalDate from, LocalDate to){
        Period period = new Period(from, to);
        return period;
    }
    
    /**
     * yes this is oddly specific - input the interval between now and 
     * future age.
     * give me a birthday for someone who will be 18 in 5 days
     * @param interval - time(number of days/weeks, etc converted to milliseconds 
     * @param ageToSearchFor
     * @return birthday as a string that is now + interval in format of MM/dd/yyyy
     * @throws ParseException 
     */
    public static String calculateBirthdayToSearchFor(long interval, int ageToSearchFor) throws ParseException {
        LocalDate todayPlusInterval = new LocalDate((System.currentTimeMillis() + interval));
        // subtract minor.age to get coming birthday
        long birthdayDate = todayPlusInterval.minusYears(ageToSearchFor).toDateMidnight().getMillis(); // subtract minor.age
        SimpleDateFormat dtf = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzz yyyy");
        String birthdayToSearchFor = new Date(birthdayDate).toString();
        Date formatDate = dtf.parse(birthdayToSearchFor);
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("MM/dd/yyyy");
        DateTime jodaTime = new DateTime(formatDate);
        birthdayToSearchFor = dtfOut.print(jodaTime);
        return birthdayToSearchFor;
    }
    
    public static boolean equalsDatePortionOnly(Date first, Date second) {
        return compareDatePortionOnly(first, second) == 0;
    }
    
    public static int compareDatePortionOnly(Date first, Date second) {
        if(first == null || second == null) {
          logger.warning("at least one of the dates you're comparing is null,"
                  + " failing this comparision between " + first + " and "
                  + second);
          return -1;
        }
        DateTime jfirst = new DateTime(first);
        DateTime jsecond = new DateTime(second);
        return DateTimeComparator.getDateOnlyInstance().compare(jfirst, jsecond);
        
    }
}
