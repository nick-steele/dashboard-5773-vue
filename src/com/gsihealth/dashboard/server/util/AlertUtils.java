package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.common.AlertConstants;
import com.gsihealth.dashboard.common.Constants;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.entity.CommunityCareteam;
import com.gsihealth.dashboard.server.dao.CommunityCareteamDAO;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;
import org.gsihealth.wsn.GSIHealthNotificationMessage;

/**
 *
 * @author Chad Darby
 */
public class AlertUtils {

    private static Logger logger = Logger.getLogger(AlertUtils.class.getName());

    public static void sendPatientCreatedAlert(long communityId, PersonDTO personDTO) {
        GSIHealthNotificationMessage constructedMessage = null;
        String notificationStatus = null;
        String alertMessageBody = null;

        StopWatch totalStopWatch = new StopWatch();
        totalStopWatch.start();
        
        try {            
            StopWatch tempStopWatch = new StopWatch();
            tempStopWatch.start();
            
            // get alert logical id
            String alertLogicalId = AlertConstants.PATIENT_CREATED_ALERT_ID;

            // alert message body
            alertMessageBody = personDTO.getFirstName() + " " + personDTO.getLastName() 
                    + ", Patient ID: " + personDTO.getPatientEnrollment().getPatientId() 
                    + " has been added to GSIHealthCoordinator.";

            // construct the message
            constructedMessage = NotificationUtils.constructPatientAlertNotificationMessage(
                    communityId, alertLogicalId, AlertConstants.GSI_CARE_COORDINATION_APP, alertMessageBody, personDTO,null);

            tempStopWatch.stop();
            logger.info("[profiling], AlertUtils: sendPatientCreatedAlert - constructed message,  " 
                    + (tempStopWatch.getTime() / 1000.0) + ", secs");

            tempStopWatch.reset();
            tempStopWatch.start();
            
            // send the message
            notificationStatus = NotificationUtils.sendAlert(communityId, constructedMessage);
            
            tempStopWatch.stop();
            logger.info("[profiling], AlertUtils: sendPatientCreatedAlert -> Called NotificationUtils.sendAlert,  " 
                    + (tempStopWatch.getTime() / 1000.0) + ", secs");
            
        } catch (Throwable exc) {
            final String message = "Error Sending message to WebService for 'NotificationManager' ";

            logger.logp(Level.SEVERE, "AlertUtils", "SendPatientCreatedAlertThread.", message, exc);
            notificationStatus = "FAILURE";
        } finally {
            // audit the message
            //  addNotificationAudit(userDTO, notificationStatus, constructedMessage);
        }
        
        totalStopWatch.stop();
        logger.info("[profiling], AlertUtils: TOTAL - sendPatientCreatedAlert, " 
                + (totalStopWatch.getTime() / 1000.0) + ", secs");
        
    }

    public static void sendPatientCreatedAlertFromTreat(
            long communityId, PersonDTO personDTO, String fromOid, String treatPatientId) {
        GSIHealthNotificationMessage constructedMessage = null;
        String notificationStatus = null;
        String alertMessageBody = null;

        try {
            // get alert logical id
            String alertLogicalId = AlertConstants.PATIENT_CREATED_ALERT_ID;

            // alert message body
            alertMessageBody = personDTO.getFirstName() + " " + personDTO.getLastName() 
                    + ", Patient ID: " + personDTO.getPatientEnrollment().getPatientId() 
                    + " has been added to GSIHealthCoordinator." + Constants.TREAT_PATIENT_ALERT_TAG;

            // construct the message
            constructedMessage = NotificationUtils.constructPatientAlertNotificationMessage(
                    communityId, alertLogicalId, AlertConstants.GSI_CARE_COORDINATION_APP, 
                    alertMessageBody, personDTO, fromOid, treatPatientId, null);

            // send the message
            notificationStatus = NotificationUtils.sendAlert(communityId, constructedMessage);
        } catch (Throwable exc) {
            final String message = "Error Sending message to WebService for 'NotificationManager' ";

            logger.logp(Level.SEVERE, "AlertUtils", "SendPatientCreatedAlertThread.", message, exc);
            notificationStatus = "FAILURE";
        } finally {
            // audit the message
            //  addNotificationAudit(userDTO, notificationStatus, constructedMessage);
        }
    }
 
    /**
     * 
     * @param communityId
     * @param personDTO
     * @param minorAge - age of majority
     * @param numberOfDays - how many days before birthday for alert text
     * @param fromOid
     * @return 
     */
    public static String sendPatientAgeOfMajorityAlert(long communityId, PersonDTO personDTO,
        int minorAge, int numberOfDays, String fromOid) {
        GSIHealthNotificationMessage constructedMessage = null;
        String notificationStatus = null;

        try {
            // get alert logical id
            String alertLogicalId = AlertConstants.PATIENT_AGE_OF_MAJORITY;

            // alert message body
            String alertMessageBody = null;
            if (numberOfDays == 0) {
                alertMessageBody = "Minor patient " + personDTO.getFirstName()
                        + " " + personDTO.getLastName() + ", Patient ID: " + personDTO.getPatientEnrollment().getPatientId()
                        + " is " + minorAge + " today";
            } else {
                alertMessageBody = "Minor patient " + personDTO.getFirstName()
                        + " " + personDTO.getLastName() + ", Patient ID: " + personDTO.getPatientEnrollment().getPatientId()
                        + " will turn " + minorAge + " within " + numberOfDays + " days";
            }
            logger.info("sending alert for patientId " + personDTO.getPatientEnrollment().getPatientId() + " days: " + numberOfDays);
            // construct the message
            constructedMessage = NotificationUtils.constructPatientAlertNotificationMessage(
                    communityId, alertLogicalId, AlertConstants.GSI_CARETEAM_APP, alertMessageBody, personDTO, null);

            // send the message
            notificationStatus = NotificationUtils.sendAlert(communityId, constructedMessage);
        } catch (Throwable exc) {
            final String message = "Error Sending message to WebService for 'NotificationManager' ";
            logger.logp(Level.SEVERE, "AlertUtils", "sendPatientAgeOfMajorityAlert.", message, exc);
            notificationStatus = "FAILURE";
        } finally {
            // audit the message
            // addNotificationAudit(userDTO, notificationStatus, constructedMessage);
        }
        return notificationStatus;

    }
        
    public static void sendPatientDemographicsChangedAlert(long communityId, PersonDTO personDTO) {
        GSIHealthNotificationMessage constructedMessage = null;
        String notificationStatus = null;

        try {
            // get alert logical id
            String alertLogicalId = AlertConstants.PATIENT_DEMOGRAPHICS_CHANGED_ALERT_ID;

            // alert message body
            String alertMessageBody = "The patient demographic information has been updated for " + personDTO.getFirstName() + " " + personDTO.getLastName() + ", Patient ID: " + personDTO.getPatientEnrollment().getPatientId();

            // construct the message
            constructedMessage = NotificationUtils.constructPatientAlertNotificationMessage(communityId, alertLogicalId, AlertConstants.GSI_CARE_COORDINATION_APP, alertMessageBody, personDTO,null);

            // send the message
            notificationStatus = NotificationUtils.sendAlert(communityId, constructedMessage);
        } catch (Throwable exc) {
            final String message = "Error Sending message to WebService for 'NotificationManager' ";

            logger.logp(Level.SEVERE, "AlertUtils", "sendPatientDemographicsChangedAlert.", message, exc);

            notificationStatus = "FAILURE";
        } finally {
            // audit the message
            // addNotificationAudit(userDTO, notificationStatus, constructedMessage);
        }

    }

    public static void sendPatientDemographicsChangedAlertFromTreat(long communityId, PersonDTO personDTO, String fromOid, String patientId) {

        GSIHealthNotificationMessage constructedMessage = null;
        String notificationStatus = null;

        try {

            // get alert logical id
            String alertLogicalId = AlertConstants.PATIENT_DEMOGRAPHICS_CHANGED_ALERT_ID;

            // alert message body
            String alertMessageBody = "The patient demographic information has been updated for " + personDTO.getFirstName() + " " + personDTO.getLastName() + ", Patient ID: " + personDTO.getPatientEnrollment().getPatientId() + Constants.TREAT_PATIENT_ALERT_TAG;

            // construct the message
            constructedMessage = NotificationUtils.constructPatientAlertNotificationMessage(communityId, alertLogicalId, AlertConstants.GSI_CARE_COORDINATION_APP, alertMessageBody, personDTO, fromOid, patientId,null);

            // send the message
            notificationStatus = NotificationUtils.sendAlert(communityId, constructedMessage);
        } catch (Throwable exc) {
            final String message = "Error Sending message to WebService for 'NotificationManager' ";

            logger.logp(Level.SEVERE, "AlertUtils", "sendPatientDemographicsChangedAlert.", message, exc);

            notificationStatus = "FAILURE";
        } finally {
            // audit the message
            // addNotificationAudit(userDTO, notificationStatus, constructedMessage);
        }
    }
    
    public static void sendPatientEnrolledStatusChangedAlert(long communityId, PersonDTO personDTO, String newEnrollStatus) {
        GSIHealthNotificationMessage constructedMessage = null;
        String notificationStatus = null;

        try {
            // get alert logical id
            String alertLogicalId = AlertConstants.ENROLLMENT_STATUS_CHANGE_ALERT_ID;

            // alert message body
            String alertMessageBody = "The enrollment status for Patient: " + personDTO.getFirstName() + " " + personDTO.getLastName() + ", Patient ID: " + personDTO.getPatientEnrollment().getPatientId() + " has been updated to " + newEnrollStatus;

            // construct the message
            constructedMessage = NotificationUtils.constructPatientAlertNotificationMessage(communityId, alertLogicalId, AlertConstants.GSI_ENROLLMENT_APP, alertMessageBody, personDTO, null);

            // send the message
            notificationStatus = NotificationUtils.sendAlert(communityId, constructedMessage);
        } catch (Throwable exc) {
            final String message = "Error Sending message to WebService for 'NotificationManager' ";
            
            logger.logp(Level.SEVERE, "AlertUtils", "sendPatientEnrolledStatusChangedAlert.", message, exc);
            notificationStatus = "FAILURE";
        } finally {
            // audit the message
            //  addNotificationAudit(userDTO, notificationStatus, constructedMessage);
        }
    }

    public static void sendPatientCareteamRemovedAlert(long communityId, PersonDTO personDTO, String communityCareteamName) {

        GSIHealthNotificationMessage constructedMessage = null;
        String notificationStatus = null;

        try {
            // get alert logical id
            String alertLogicalId = AlertConstants.PATIENT_CARETEAM_REMOVED_ALERT_ID;

            // alert message body
            String alertMessageBody = personDTO.getFirstName() + " " + personDTO.getLastName() + ", Patient ID: " + personDTO.getPatientEnrollment().getPatientId() + " has been removed from " + communityCareteamName;

            // construct the message
            constructedMessage = NotificationUtils.constructPatientAlertNotificationMessage(communityId, alertLogicalId, AlertConstants.GSI_CARETEAM_APP, alertMessageBody, personDTO, communityCareteamName);

            // send the message
            notificationStatus = NotificationUtils.sendAlert(communityId, constructedMessage);
        } catch (Throwable exc) {
            final String message = "Error Sending message to WebService for 'NotificationManager' ";

            logger.logp(Level.SEVERE, "AlertUtils", "sendPatientCareteamRemovedAlert.", message, exc);
            notificationStatus = "FAILURE";
        } finally {
            // audit the message
            // addNotificationAudit(userDTO, notificationStatus, constructedMessage);
        }
    }

    public static void sendPatientCareteamAssignedAlert(long communityId, CommunityCareteamDAO careteamDAO, PersonDTO personDTO, long communityCareteamId) {
        // get the care team name
        CommunityCareteam communityCareteam = careteamDAO.findCommunityCareteam(communityCareteamId);
        String careteamName = communityCareteam.getName();
        sendPatientCareteamAssignedAlert(communityId, careteamName, personDTO, communityCareteamId);
    }
    public static void sendPatientCareteamAssignedAlert(long communityId, String careteamName, PersonDTO personDTO, long communityCareteamId) {

        GSIHealthNotificationMessage constructedMessage = null;
        String notificationStatus = null;

        StopWatch totalStopWatch = new StopWatch();
        totalStopWatch.start();
        
        try {
            StopWatch tempStopWatch = new StopWatch();
            tempStopWatch.start();



            // get alert logical id
            String alertLogicalId = AlertConstants.PATIENT_ASSIGNED_TO_CARETEAM_ALERT_ID;

            // alert message body
            String alertMessageBody = personDTO.getFirstName() + " " + personDTO.getLastName() + ", Patient ID: " + personDTO.getPatientEnrollment().getPatientId() + " has been assigned to " + careteamName;

            // construct the message
            constructedMessage = NotificationUtils.constructPatientAlertNotificationMessage(communityId, alertLogicalId, AlertConstants.GSI_CARETEAM_APP, alertMessageBody, personDTO, careteamName);

            tempStopWatch.stop();
            logger.info("[profiling], AlertUtils: sendPatientCareteamAssignedAlert - constructed message,  " + (tempStopWatch.getTime() / 1000.0) + ", secs");
            
            tempStopWatch.reset();
            tempStopWatch.start();
            // send the message
            notificationStatus = NotificationUtils.sendAlert(communityId, constructedMessage);

            tempStopWatch.stop();
            logger.info("[profiling], sendPatientCareteamAssignedAlert -> Called NotificationUtils.sendAlert,  " + (tempStopWatch.getTime() / 1000.0) + ", secs");
        
        } catch (Throwable exc) {
            final String message = "Error Sending message to WebService for 'NotificationManager' ";
            logger.logp(Level.SEVERE, "AlertUtils", "sendPatientCareteamAssignedAlert.", message, exc);
            notificationStatus = "FAILURE";
        } finally {
            // audit the message
            // addNotificationAudit(userDTO, notificationStatus, constructedMessage);
        }
        
        totalStopWatch.stop();
        logger.info("[profiling], AlertUtils: TOTAL - sendPatientCareteamAssignedAlert, " + (totalStopWatch.getTime() / 1000.0) + ", secs");
        
    }
    
    public static void sendPatientConsentChangedAlert(long communityId, PersonDTO personDTO, String newProgramLevelConsentStatus) {

        GSIHealthNotificationMessage constructedMessage = null;
        String notificationStatus = null;

        StopWatch totalStopWatch = new StopWatch();
        totalStopWatch.start();
        
        try {
            
            StopWatch tempStopWatch = new StopWatch();
            tempStopWatch.start();
            
            // alert message body
            String alertMessageBody = personDTO.getFirstName() + " " + personDTO.getLastName()
                    + ", Patient ID: " + personDTO.getPatientEnrollment().getPatientId() + " has ";

            boolean hasConsent = StringUtils.equalsIgnoreCase(newProgramLevelConsentStatus, Constants.YES);

            // get alert logical id and final piece of message
            String alertLogicalId = null;

            if (hasConsent) {
                alertLogicalId = AlertConstants.CONSENT_STATUS_CHANGED_TO_YES_ALERT_ID;
                alertMessageBody += "provided consent";
            } else {
                alertLogicalId = AlertConstants.CONSENT_STATUS_CHANGED_TO_NO_ALERT_ID;
                alertMessageBody += "revoked consent";
            }

            // construct the message
            constructedMessage = NotificationUtils.constructPatientAlertNotificationMessage(communityId, alertLogicalId, AlertConstants.GSI_ENROLLMENT_APP, alertMessageBody, personDTO,null);

            tempStopWatch.stop();
            logger.info("[profiling], AlertUtils: sendPatientConsentChangedAlert - constructed message,  " + (tempStopWatch.getTime() / 1000.0) + ", secs");
            
            tempStopWatch.reset();
            tempStopWatch.start();
            
            // send the message
            notificationStatus = NotificationUtils.sendAlert(communityId, constructedMessage);

            tempStopWatch.stop();
            logger.info("[profiling], sendPatientConsentChangedAlert -> Called NotificationUtils.sendAlert,  " + (tempStopWatch.getTime() / 1000.0) + ", secs");

        } catch (Throwable exc) {
            final String message = "Error Sending message to WebService for 'NotificationManager' ";
            logger.logp(Level.SEVERE, "AlertUtils", "sendPatientConsentChangedAlert.", message, exc);
            notificationStatus = "FAILURE";
        } finally {
            // audit the message
            // addNotificationAudit(userDTO, notificationStatus, constructedMessage);
        }
        
        totalStopWatch.stop();
        logger.info("[profiling], AlertUtils: TOTAL - sendPatientConsentChangedAlert, " + (totalStopWatch.getTime() / 1000.0) + ", secs");
        
    }  
}
