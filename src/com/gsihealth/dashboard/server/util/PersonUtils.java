package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.server.common.Person;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

/**
 *
 * @author Chad Darby
 */
public class PersonUtils {

    public static boolean equalsIgnoreCase(Person sourcePerson, Person targetPerson) {

        boolean firstNameEquals = StringUtils.equalsIgnoreCase(sourcePerson.getFirstName(), targetPerson.getFirstName());
        boolean lastNameEquals = StringUtils.equalsIgnoreCase(sourcePerson.getLastName(), targetPerson.getLastName());
        boolean genderEquals = StringUtils.equalsIgnoreCase(sourcePerson.getGender(), targetPerson.getGender());
        boolean dobSameDay = DateUtils.isSameDay(sourcePerson.getDateOfBirth(), targetPerson.getDateOfBirth());

        return (firstNameEquals && lastNameEquals && genderEquals && dobSameDay);
    }

}
