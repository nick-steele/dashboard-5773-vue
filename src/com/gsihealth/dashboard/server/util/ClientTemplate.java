package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;
import com.gsihealth.dashboard.entity.dto.insurance.PayerClassDTO;
import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import com.gsihealth.entity.insurance.PayerClass;
import com.gsihealth.dashboard.server.reports.PatientTrackingReportData;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;

import org.apache.poi.xssf.usermodel.*;

/**
 *
 * @author Satyendra Singh
 */
public class ClientTemplate {

    private static final int FIRST_NAME_COLUMN = 1;
    private static final int LAST_NAME_COLUMN = 2;
    private static final int MEDICAID_ID_COLUMN = 4;
    private static final int DOB_COLUMN = 5;
    private static final int GENDER_COLUMN = 6;
    private static final int CARE_MANAGEMENT_MMIS_ID_COLUMN = 12;
    private static final int BILLING_PROVIDER_MMIS_ID_COLUMN = 18;
    private static final int DATE_OF_EARLIEST_CONTACT_COLUMN = 22;
    private static final int DATE_OF_LATEST_CONTACT_COLUMN = 23;
    private static final int DATE_OF_INITIAL_ASSESSMENT_COLUMN = 35;
    private static final int STREET_ADDRESS1_COLUMN = 25;
    private static final int STREET_ADDRESS2_COLUMN = 26;
    private static final int CITY_COLUMN = 27;
    private static final int STATE_COLUMN = 28;
    private static final int ZIP_CODE_COLUMN = 29;
    private static final int PRIMARY_PHONE_COLUMN = 31;
    private static final int ENROLL_CONSENT_DATE_COLUMN = 36;
    private static final int MAIL_COUNT_COLUMN = 38;
    private static final int PHONE_COUNT_COLUMN = 39;
    private static final int PERSON_COUNT_COLUMN = 40;
    private static final int PROGRAM_NAME_COLUMN= 48;
    private static final int END_DATE_COLUMN=8;
    private static final String MEDICAID_PAYER_CLASS = "Medicaid";
    private String[] topData = {"Organization Name:", "Date Of Submission:", "MMIS ID#"};
    private String[] topColumns = {"Tracking #", "First Name", "Last Name", "Record Type", "CIN", "Member DOB", "Member Gender", "Begin Date", "End Date", "Outreach or Enrollment", "Managed Care Plan MMIS ID", "Health Home MMIS ID #", "Care Management Agency MMIS ID #", "TCM/ MATS/ COBRA/ CIDP Indicator", "Existing or new rate for TCM/ MATS/ CIDP?", "Date Assigned to HH", "Referral Code", "Segment End Date Disenrollment Reason Code", "Billing Provider MMIS ID #", "Managed Care Assignment Date", "Referral Date", "Transfer Provider MMIS ID", "Date of Earliest Contact", "Date of most recent contact ", "Updated Address?", "Member's Address", "Address Line 2", "City", "State", "Zip", "Updated Phone Number?", "Primary Phone Number", "Alternate Phone Number", "Date of Initial Contact", "Able to Contact?", "Date of initial assessment", "Enroll Consent Date", "Refused HH", "Count Mail", "Count Phone", "Count Person", "Closure Date", "Reason Codes", "Suggested Health Home", "Reason Closure", "Other Notes", "Submission Date", "Organization Name", "Program Name"};
    private XSSFWorkbook workbook;
    private XSSFSheet firstSheet;
    
    private static final Logger logger = Logger.getLogger(ClientTemplate.class.getName());

    public ClientTemplate() {
    }

    public static void main(String[] args) {

        ClientTemplate clientTemplate = new ClientTemplate();

        clientTemplate.createTopColums();
        clientTemplate.createHeaderColumns();

        // create dummy date for person list
        List<PersonDTO> personDTOs = clientTemplate.createDummyListDTO();


        // populate data into sheet.
        Map<Long, PatientTrackingReportData> carePlanReportDataMap = buildPatientTrackingReportDataMap();
        clientTemplate.populate(personDTOs, "The Acme Org", "1234567", carePlanReportDataMap);

        // stream where the workbook content will be written to.

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(new File("PatientTrackingTemplate.xlsx"));
            clientTemplate.workbook.write(fos);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error: " + e.getMessage(), e);
        } finally {
            if (fos != null) {
                try {
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    logger.log(Level.SEVERE, "Error: " + e.getMessage(), e);
                }
            }
        }
    }

    public void createTopColums() {
        // Creating an instance of XSSFWorkbook.
        try {
            workbook = new XSSFWorkbook();

            // Bold font
            Font font = workbook.createFont();
            font.setBoldweight(Font.BOLDWEIGHT_BOLD);

            // Create one sheet in the excel document and name it Sheet
            firstSheet = workbook.createSheet("Sheet1");

            // Top columns
            XSSFRow row1 = firstSheet.createRow(0);
            XSSFCell cell2 = row1.createCell(2);
            XSSFCell cell3 = row1.createCell(3);
            XSSFCell cell4 = row1.createCell(4);

            // merging rows
            firstSheet.addMergedRegion(new CellRangeAddress(0, 0, 2, 4));

            // Yellow background
            CellStyle yellowColorStyle = workbook.createCellStyle();
            yellowColorStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
            yellowColorStyle.setFont(font);

            yellowColorStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            cell2.setCellStyle(yellowColorStyle);
            cell2.setCellValue(new XSSFRichTextString(""));
            cell3.setCellStyle(yellowColorStyle);
            cell3.setCellValue(new XSSFRichTextString(""));
            cell4.setCellStyle(yellowColorStyle);
            cell4.setCellValue(new XSSFRichTextString(""));

            XSSFRow[] row = new XSSFRow[4];
            // Grey background
            XSSFCellStyle greyColorStyle = workbook.createCellStyle();
            greyColorStyle.setFillForegroundColor(new XSSFColor(Color.decode("#C5D9F1")));//new XSSFColor(new java.awt.Color(194, 214, 154)));
            greyColorStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
            greyColorStyle.setAlignment(CellStyle.ALIGN_CENTER);
            greyColorStyle.setWrapText(true);
            greyColorStyle.setBorderBottom(BorderStyle.THIN);
            greyColorStyle.setBorderTop(BorderStyle.THIN);
            greyColorStyle.setBorderRight(BorderStyle.THIN);
            greyColorStyle.setBorderLeft(BorderStyle.THIN);
            greyColorStyle.setVerticalAlignment((short) 1);

            for (int i = 1; i <= 4; i++) {
                row[i - 1] = firstSheet.createRow(i);

                for (int j = 2; j <= 4; j++) {
                    if (i == 1 && j == 2) {
                        XSSFCell cellA = row[i - 1].createCell(j);
                        cellA.setCellStyle(greyColorStyle);
                        cellA.setCellValue(new XSSFRichTextString(topData[i - 1]));
                    } else if (i == 1 && j == 3) {
                        XSSFCell cellB = row[i - 1].createCell(j);
                        cellB.setCellStyle(greyColorStyle);

                    } else if (i == 1 && j == 4) {
                        XSSFCell cellB = row[i - 1].createCell(j);
                        cellB.setCellStyle(greyColorStyle);

                    } else if (i == 2 && j == 2) {
                        XSSFCell cellB = row[i - 1].createCell(j);
                        cellB.setCellStyle(greyColorStyle);
                        cellB.setCellValue(new XSSFRichTextString(topData[i - 1]));
                    } else if (i == 2 && j == 3) {
                        XSSFCell cellD = row[i - 1].createCell(j);
                        cellD.setCellStyle(greyColorStyle);

                    } else if (i == 2 && j == 4) {
                        XSSFCell cellD = row[i - 1].createCell(j);
                        cellD.setCellStyle(greyColorStyle);

                    } else if (i == 3 && j == 2) {
                        XSSFCell cellC = row[i - 1].createCell(j);
                        cellC.setCellStyle(greyColorStyle);
                        cellC.setCellValue(new XSSFRichTextString(topData[i - 1]));
                    } else if (i == 3 && j == 3) {
                        XSSFCell cellC = row[i - 1].createCell(j);
                        cellC.setCellStyle(greyColorStyle);

                    } else if (i == 3 && j == 4) {
                        XSSFCell cellC = row[i - 1].createCell(j);
                        cellC.setCellStyle(greyColorStyle);
                        XSSFCell cellD = row[i - 1].createCell(37);
                        cellD.setCellStyle(greyColorStyle);
                        cellD.setCellValue(new XSSFRichTextString("Only complete for patients during the outreach period"));
                        firstSheet.addMergedRegion(new CellRangeAddress(3, 3, 37, 40));
                    }
                }
            }

            firstSheet.addMergedRegion(new CellRangeAddress(1, 1, 3, 4));
            firstSheet.addMergedRegion(new CellRangeAddress(2, 2, 3, 4));
            firstSheet.addMergedRegion(new CellRangeAddress(3, 3, 3, 4));

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error building spreadsheet", e);
        }
    }

    public void createHeaderColumns() {
        // Bold font
        Font font = workbook.createFont();
        font.setBoldweight(Font.BOLDWEIGHT_BOLD);
        font.setColor(new XSSFColor(new java.awt.Color(39, 51, 89)).getIndexed());

        // Green color style
        XSSFCellStyle greenColorStyle = workbook.createCellStyle();
        greenColorStyle.setFillForegroundColor(new XSSFColor(Color.decode("#C2D69A")));//new XSSFColor(new java.awt.Color(194, 214, 154)));
        greenColorStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
        greenColorStyle.setAlignment(CellStyle.ALIGN_CENTER);
        greenColorStyle.setVerticalAlignment((short) 1);
        greenColorStyle.setWrapText(true);
        greenColorStyle.setBorderBottom(BorderStyle.THIN);
        greenColorStyle.setBorderTop(BorderStyle.THIN);
        greenColorStyle.setBorderRight(BorderStyle.THIN);
        greenColorStyle.setBorderLeft(BorderStyle.THIN);
        greenColorStyle.setFont(font);

        int size = topColumns.length;
        XSSFCell[] noCell = new XSSFCell[size];
        XSSFRow row4 = firstSheet.createRow(4);
        XSSFRow row5 = firstSheet.createRow(5);

        row5.setHeightInPoints(43);
        for (int i = 0; i < size; i++) {
            firstSheet.setDefaultColumnWidth((short) 15);

            if (i == 37 || i == 38 || i == 39 | i == 40) {
                XSSFCellStyle colorsStyle = workbook.createCellStyle();
                colorsStyle.setFillForegroundColor(new XSSFColor(Color.decode("#B8CCE4")));//new XSSFColor(new java.awt.Color(194, 214, 154)));
                colorsStyle.setAlignment(CellStyle.ALIGN_CENTER);
                colorsStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
                colorsStyle.setBorderBottom(BorderStyle.THIN);
                colorsStyle.setBorderTop(BorderStyle.THIN);
                colorsStyle.setBorderRight(BorderStyle.THIN);
                colorsStyle.setBorderLeft(BorderStyle.THIN);
                colorsStyle.setVerticalAlignment((short) 1);

                noCell[i] = row5.createCell(i);
                noCell[i].setCellValue(new XSSFRichTextString(topColumns[i]));
                noCell[i].setCellStyle(colorsStyle);

                XSSFCell cella = row4.createCell(i);
                cella.setCellStyle(colorsStyle);
            } else {
                noCell[i] = row5.createCell(i);
                noCell[i].setCellValue(new XSSFRichTextString(topColumns[i]));
                noCell[i].setCellStyle(greenColorStyle);

            }
        }
    }

    public void populate(List<PersonDTO> persons, String organizationName, String mmisId, Map<Long, PatientTrackingReportData> patientTrackingReportDataMap) {
        int size = persons.size();

        // populate organization name cell value
        XSSFRow orgRow = firstSheet.getRow(1);
        XSSFCell organizationNameCell = orgRow.getCell(3);
        organizationNameCell.setCellValue(organizationName);

        // populate MMIS ID cell value
        XSSFRow mmisRow = firstSheet.getRow(3);
        XSSFCell mmisIdCell = mmisRow.getCell(3);
        mmisIdCell.setCellValue(mmisId);

        // setup date cell style for DOB
        XSSFDataFormat dataFormat = workbook.createDataFormat();
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(dataFormat.getFormat("MM/dd/yyyy"));

        // populate patient data
        for (int row = 0; row < size; row++) {

            PersonDTO tempPerson = persons.get(row);
            XSSFRow tempRow = firstSheet.createRow(6 + row);

            // get care plan report data for this patient
            PatientEnrollmentDTO patientEnrollment = tempPerson.getPatientEnrollment();
            long patientId = patientEnrollment.getPatientId();

            // get report data
            PatientTrackingReportData patientTrackingReportData = patientTrackingReportDataMap.get(patientId);

            boolean hasPatientTrackingReportData = patientTrackingReportData != null;
            
            // creating cells
            for (int col = 0; col <= 48; col++) {
                XSSFCell tempCell = tempRow.createCell(col);

                if (col == FIRST_NAME_COLUMN) {
                    tempCell.setCellValue(tempPerson.getFirstName());
                } else if (col == LAST_NAME_COLUMN) {
                    tempCell.setCellValue(tempPerson.getLastName());
                } else if (col == MEDICAID_ID_COLUMN) {
                    setMedicaidId(patientEnrollment, tempCell);
                } else if (col == DOB_COLUMN) {
                    setDate(tempCell, dateCellStyle, tempPerson.getDateOfBirth());
                } else if (col == GENDER_COLUMN) {
                    tempCell.setCellValue(tempPerson.getGender());
                } else if (col == STREET_ADDRESS1_COLUMN) {
                    tempCell.setCellValue(tempPerson.getStreetAddress1());
                } else if (col == STREET_ADDRESS2_COLUMN) {
                    tempCell.setCellValue(tempPerson.getStreetAddress2());
                } else if (col == CITY_COLUMN) {
                    tempCell.setCellValue(tempPerson.getCity());
                } else if (col == STATE_COLUMN) {
                    tempCell.setCellValue(tempPerson.getState());
                } else if (col == ZIP_CODE_COLUMN) {
                    tempCell.setCellValue(tempPerson.getZipCode());
                } else if (col == PRIMARY_PHONE_COLUMN) {
                    tempCell.setCellValue(tempPerson.getTelephone());
                } else if (col == ENROLL_CONSENT_DATE_COLUMN) {
                    Date consentDate = patientEnrollment.getConsentDateTime();
                    setDate(tempCell, dateCellStyle, consentDate);
                } else if (col == BILLING_PROVIDER_MMIS_ID_COLUMN) {
                    setBillingProviderMmisId(patientEnrollment, tempCell);
                } else if (col == DATE_OF_EARLIEST_CONTACT_COLUMN && hasPatientTrackingReportData) {
                    Date theDate = patientTrackingReportData.getEarliestContactDate();
                    setDate(tempCell, dateCellStyle, theDate);
                } else if (col == DATE_OF_LATEST_CONTACT_COLUMN && hasPatientTrackingReportData) {
                    Date theDate = patientTrackingReportData.getLatestContactDate();
                    setDate(tempCell, dateCellStyle, theDate);
                } else if (col == DATE_OF_INITIAL_ASSESSMENT_COLUMN && hasPatientTrackingReportData) {
                    Date theDate = patientTrackingReportData.getInitialAssessmentDate();
                    setDate(tempCell, dateCellStyle, theDate);
                } else if (col == MAIL_COUNT_COLUMN) {
                    setMailCount(hasPatientTrackingReportData, tempCell, patientTrackingReportData);
                } else if (col == PHONE_COUNT_COLUMN) {
                    setPhoneCount(hasPatientTrackingReportData, tempCell, patientTrackingReportData);
                } else if (col == PERSON_COUNT_COLUMN) {
                    setPersonCount(hasPatientTrackingReportData, tempCell, patientTrackingReportData);
                } else if (col == CARE_MANAGEMENT_MMIS_ID_COLUMN && hasPatientTrackingReportData) {
                    tempCell.setCellValue(patientTrackingReportData.getCareManagementMmisId());
                } else if (col== PROGRAM_NAME_COLUMN) {
                    tempCell.setCellValue(patientEnrollment.getProgramName()) ;
                }
                 else if (col== END_DATE_COLUMN) {
                            Date endDate =tempPerson.getEndDate() ;
                            setDate(tempCell, dateCellStyle, endDate);
                }
                
            }
        }
    }

    protected void setBillingProviderMmisId(PatientEnrollmentDTO patientEnrollment, XSSFCell tempCell) {
        PayerPlanDTO primaryPayerPlan = patientEnrollment.getPrimaryPayerPlan();

        if (primaryPayerPlan != null) {
            String primaryPayerPlanMmisId = primaryPayerPlan.getMmisId();
            tempCell.setCellValue(primaryPayerPlanMmisId);
        }
    }

    protected void setPersonCount(boolean hasPatientTrackingReportData, XSSFCell tempCell, PatientTrackingReportData patientTrackingReportData) {
        if (hasPatientTrackingReportData) {
            tempCell.setCellValue(patientTrackingReportData.getPersonCount());
        } else {
            tempCell.setCellValue(0);
        }
    }

    protected void setPhoneCount(boolean hasPatientTrackingReportData, XSSFCell tempCell, PatientTrackingReportData patientTrackingReportData) {
        if (hasPatientTrackingReportData) {
            tempCell.setCellValue(patientTrackingReportData.getPhoneCount());
        } else {
            tempCell.setCellValue(0);
        }
    }

    protected void setMailCount(boolean hasPatientTrackingReportData, XSSFCell tempCell, PatientTrackingReportData patientTrackingReportData) {
        if (hasPatientTrackingReportData) {
            tempCell.setCellValue(patientTrackingReportData.getMailCount());
        } else {
            tempCell.setCellValue(0);
        }
    }

    protected void setDate(XSSFCell tempCell, CellStyle dateCellStyle, Date theDate) {
        // add special date cell style
        tempCell.setCellStyle(dateCellStyle);

        if (theDate != null) {
            tempCell.setCellValue(theDate);
        }
    }

    protected static Map<Long, PatientTrackingReportData> buildPatientTrackingReportDataMap() {
        Map<Long, PatientTrackingReportData> theMap = new HashMap<Long, PatientTrackingReportData>();

        PatientTrackingReportData data = null;

        // person 1
        data = new PatientTrackingReportData();
        data.setEarliestContactDate(getDate(11, 13, 2007));
        data.setMailCount(2);
        data.setPhoneCount(4);
        data.setPersonCount(6);
        data.setCareManagementMmisId("987654321");
        theMap.put(1L, data);

        // person 3
        data = new PatientTrackingReportData();
        data.setLatestContactDate(getDate(5, 12, 2008));
        theMap.put(3L, data);

        // person 4
        data = new PatientTrackingReportData();
        data.setInitialAssessmentDate(getDate(7, 4, 2009));
        theMap.put(4L, data);

        return theMap;
    }

    public List<PersonDTO> createDummyListDTO() {

        List<PersonDTO> thePersonDTOs = new ArrayList<PersonDTO>();
        // person 1

        PersonDTO person1 = new PersonDTO();
        person1.setFirstName("John");
        person1.setLastName("Doe");
        person1.setGender("M");
        person1.setDateOfBirth(new Date());
        person1.setStreetAddress1("1942 North Bay Road");
        person1.setStreetAddress2("Street 34");
        person1.setCity("Miami");
        person1.setState("FL");
        person1.getPatientEnrollment().setPatientEuid("00001234567");
        person1.getPatientEnrollment().setConsentDateTime(new Date());
        person1.getPatientEnrollment().setPatientId(1);
        person1.getPatientEnrollment().setOrgId(16);

        thePersonDTOs.add(person1);

        // person 2
        PersonDTO person2 = new PersonDTO();
        person2.setFirstName("Peter");
        person2.setLastName("Parker ");
        person2.setGender("M");
        person2.setStreetAddress1("1942 South Bay Road");
        person2.setStreetAddress2("Street 34");
        person2.setCity("XYZ");
        person2.setState("PA");
        person2.getPatientEnrollment().setPatientId(2);
        thePersonDTOs.add(person2);

        // person 3
        PersonDTO person3 = new PersonDTO();
        person3.setFirstName("Sonia");
        person3.setLastName("Green");
        person3.setGender("F");
        person3.setStreetAddress1("Norman Avenue");
        person3.setStreetAddress2("Street 77");
        person3.setCity("XYZ");
        person3.setState("PA");
        person3.getPatientEnrollment().setPatientId(3);
        thePersonDTOs.add(person3);

        // person 4
        PersonDTO person4 = new PersonDTO();
        person4.setFirstName("Pamela");
        person4.setLastName("Chow");
        person4.setGender("F");
        person4.setDateOfBirth(new Date());
        person4.setDateOfBirth(person4.getDateOfBirth());
        person4.setStreetAddress1("Jackman street");
        person4.setStreetAddress2("Open P.O 122");
        person4.setCity("XYZ");
        person4.setState("PA");
        person4.getPatientEnrollment().setPatientId(4);
        thePersonDTOs.add(person4);

        // person 5
        PersonDTO person5 = new PersonDTO();
        person5.setFirstName("Graham");
        person5.setLastName("Ben");
        person5.setGender("M");
        person5.setStreetAddress1("Kate Street");
        person5.setStreetAddress2("Street 44");
        person5.setCity("XYZ");
        person5.setState("PA");
        person5.getPatientEnrollment().setPatientId(5);
        thePersonDTOs.add(person5);

        return thePersonDTOs;
    }

    public XSSFSheet getFirstSheet() {
        return firstSheet;
    }

    public void setFirstSheet(XSSFSheet firstSheet) {
        this.firstSheet = firstSheet;
    }

    public String[] getTopColumns() {
        return topColumns;
    }

    public void setTopColumns(String[] topColumns) {
        this.topColumns = topColumns;
    }

    public String[] getTopData() {
        return topData;
    }

    public void setTopData(String[] topData) {
        this.topData = topData;
    }

    public XSSFWorkbook getWorkbook() {
        return workbook;
    }

    public void setWorkbook(XSSFWorkbook workbook) {
        this.workbook = workbook;
    }

    protected static Date getDate(int month, int day, int year) {
        Date theDate = null;

        try {
            String dateStr = month + "/" + day + "/" + year;
            theDate = new SimpleDateFormat("MM/dd/yyyy").parse(dateStr);
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error: " + exc.getMessage(), exc);
        }

        return theDate;
    }

    private void setMedicaidId(PatientEnrollmentDTO patientEnrollment, XSSFCell tempCell) {
        PayerClassDTO primaryPayerClass = patientEnrollment.getPrimaryPayerClass();

        if (primaryPayerClass != null && StringUtils.equalsIgnoreCase(primaryPayerClass.getName(), MEDICAID_PAYER_CLASS)) {
            tempCell.setCellValue(patientEnrollment.getPrimaryPayerMedicaidMedicareId());
        }
    }
}
