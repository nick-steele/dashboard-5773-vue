package com.gsihealth.dashboard.server.util;

import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

/**
 *
 * @author Chad Darby
 */
public class XmlUtils {

    private static final int DEFAULT_INDENT = 2;
    
    /**
     * Pretty print XML with default indent
     * 
     * @param input
     * @return 
     */
    public static String prettyFormat(String input) {
        return prettyFormat(input, DEFAULT_INDENT);
    }
    
    /**
     * Pretty print XML
     * 
     * @param input
     * @param indent
     * @return 
     */
    public static String prettyFormat(String input, int indent) {
        try {
            Source xmlInput = new StreamSource(new StringReader(input));
            StringWriter stringWriter = new StringWriter();
            StreamResult xmlOutput = new StreamResult(stringWriter);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-number", "4");
            
            transformer.transform(xmlInput, xmlOutput);
            
            return xmlOutput.getWriter().toString();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e); // simple exception handling, please review it
        }
    }
    
    public static void main(String[] args) {
        String stuff = "<a><b><c>Just more stuff</c></b></a>";
        
        System.out.println(prettyFormat(stuff));
    }
}
