package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import java.net.URI;
import java.net.URL;
import java.util.Map;

/**
 *
 * @author Vishal (Off.)
 * 
 * sso.server.name=gsi-idev-app4.gsihealth.local
 * sso.server.port=8080
 * sso.server.app=openam_s952
 * sso.server.update.uri=/identity/update
 * sso.server.create.uri=/identity/create
 * sso.server.delete.uri=/identity/delete
 * sso.server.authenticate.uri=/identity/authenticate
 * sso.server.authorize.uri=/identity/authorize   
 * 
 */
public class URLGenerator {

    private String serverName;
    private String appName;
    private int port;
    private String updateURL;
    private String createURL;
    private String deleteURL;
    private String authenticateURL;
    private String authorizeURL;
    private String validateURL;
    private boolean isSSLEnabled;
    //JIRA 1122
    private String logoutURL;
    
    private String avadoURL;

    private long communityId;
    private String realm;
    
    public URLGenerator(long theCommunityId) {
        
        communityId = theCommunityId;
    
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        
        serverName = configurationDAO.getProperty(communityId, "sso.server.name");
        appName = configurationDAO.getProperty(communityId, "sso.server.app");
        port = Integer.parseInt(configurationDAO.getProperty(communityId, "sso.server.port"));
        updateURL = configurationDAO.getProperty(communityId, "sso.server.update.uri");
        createURL = configurationDAO.getProperty(communityId, "sso.server.create.uri");
        deleteURL = configurationDAO.getProperty(communityId, "sso.server.delete.uri");
        authenticateURL = configurationDAO.getProperty(communityId, "sso.server.authenticate.uri");
        authorizeURL = configurationDAO.getProperty(communityId, "sso.server.authorize.uri");
        validateURL = configurationDAO.getProperty(communityId, "sso.server.validate.uri");
        isSSLEnabled = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "sso.server.ssl"));
        //JIRA 1122
        logoutURL = configurationDAO.getProperty(communityId, "sso.server.logout.uri");
        
        avadoURL = configurationDAO.getProperty(communityId, "sso.avado.url");
        realm = configurationDAO.getProperty(communityId, "sso.realm");
    }

    public  String getAppName() {
        return appName;
    }
     public  String getRealm() {
        return realm;
    }

    public  URI getAuthenticateURI() throws Exception {
        URI authenticateURI = null;

        authenticateURI = new URI("http"
                + ((isSSLEnabled == true) ? "s:" : ":")
                + "//" + ((serverName != null) ? serverName : "")
                + ":" + port
                + "/" + ((appName != null) ? appName : "")
                + authenticateURL.toString() + "?");

        return authenticateURI;
    }
     public  URL getValidateURI(String token) throws Exception {
        URL validateURI = null;

        validateURI = new URL("http"
                + ((isSSLEnabled == true) ? "s:" : ":")
                + "//" + ((serverName != null) ? serverName : "")
                + ":" + port
                + "/" + ((appName != null) ? appName : "")
                + validateURL + "?tokenid="+token);
      
        return validateURI;
    }

    public URI getAuthorizeURI() {
        URI authorizeURI = null;

        authorizeURI = URI.create("http"
                + ((isSSLEnabled == true) ? "s" : ":")
                + "//" + ((serverName != null) ? serverName : "")
                + ":" + port
                + "/" + ((appName != null) ? appName : "")
                + authorizeURL.toString() + "?");

        return authorizeURI;
    }
    
     public URI getAvadoURI() throws Exception {
        URI avadoURI = null;

        avadoURI = new URI(avadoURL);

        return avadoURI;
    }

    public  URI getCreateURI(String tokenId, String ssoUser) throws Exception {
        URI createURI = null;

        String uri = getUriBase();
        if(realm!=null){
          ssoUser = ssoUser.replaceAll("identity_realm=%2F", "identity_realm="+realm);
  
        }
        String temp = createURL + "?admin=" + tokenId + ssoUser;
        uri += temp;

        createURI = new URI(uri);

        return createURI;
    }

    protected String getUriBase() {

        String uri = "http"
                + ((isSSLEnabled == true) ? "s" : ":")
                + "//" + ((serverName != null) ? serverName : "")
                + ":" + port
                + "/" + ((appName != null) ? appName : "");
        return uri;
    }

    public URI getDeleteURI(String tokenId, String ssoUser) throws Exception {
        URI deleteURI = null;

        String uri = getUriBase();
        if(realm!=null){
          ssoUser = ssoUser.replaceAll("identity_realm=%2F", "identity_realm="+realm);
  
        }

        String temp = deleteURL + "?admin=" + tokenId + ssoUser;
        uri += temp;

        deleteURI = new URI(uri);

        return deleteURI;
    }

    public int getPort() {
        return port;
    }

    public String getServerName() {
        return serverName;
    }

    protected  String buildQuery(Map<String, String> putValues) {

        String query = "";
        for (String key : putValues.keySet()) {
            String value = putValues.get(key);
            query = query + "&" + key + "=" + value;
        }

        return query;
    }

    public URI getUpdateURI(String tokenId, String ssoUser) throws Exception {

        URI updateURI = null;

        String uri = getUriBase();

        if(realm!=null){
          ssoUser = ssoUser.replaceAll("identity_realm=%2F", "identity_realm="+realm);
  
        }
        String temp = updateURL + "?admin=" + tokenId + ssoUser;
        uri += temp;

        updateURI = new URI(uri);

        return updateURI;
    }

    //JIRA 1122
    public  URI getLogoutURI() throws Exception {
        URI logoutURI = null;

        String uri = getUriBase();
        uri += logoutURL;
        logoutURI = new URI(uri);

        return logoutURI;
    }
    
    @Override
    public String toString() {
        return "URLGenerator{" + serverName + appName + port +  updateURL +  createURL + deleteURL + authenticateURL + authorizeURL 
                + isSSLEnabled + avadoURL + communityId + '}';
    }
    
    
}
