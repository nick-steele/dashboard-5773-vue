package com.gsihealth.dashboard.server.util;

import com.gsihealth.entity.NotificationCareteamRole;
import com.gsihealth.entity.User;
import com.gsihealth.entity.UserCommunityOrganization;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.commons.lang.StringUtils;
import org.gsihealth.wsn.Address;
import org.gsihealth.wsn.CareProvider;
import org.gsihealth.wsn.Organization;

/**
 * Collection of routines for building the notification properties
 * 
 * @author Chad Darby
 */
public class NotificationPropertyUtils {

    private static Logger logger = Logger.getLogger(NotificationPropertyUtils.class.getName());

    /**
     * Build a list of care providers based on the care team users
     * 
     * @param users
     * @return 
     */
    public static List<CareProvider> buildCareProviders(List<User> users, Map<String, String> notificationCareteamRoles, Long communityId) {
        List<CareProvider> careProviders = new ArrayList<CareProvider>();

        logger.info("\n\n>>>>>>>>>>>>>>>>>>>>>>>>>   notificationCareteamRoles: " + notificationCareteamRoles);
        for (User tempUser : users) {
            CareProvider tempCareProvider = new CareProvider();
            UserCommunityOrganization uco = tempUser.getUserCommunityOrganization(communityId);
            org.gsihealth.wsn.Person wsPerson = getPerson(tempUser, communityId, uco);
            tempCareProvider.setPerson(wsPerson);
            
            tempCareProvider.setNPI(tempUser.getNpi());

            tempCareProvider.setProviderId(Long.toString(tempUser.getUserId()));

            String role = uco.getRole().getRoleName();
            
                      
            String webserverNotificationRole = notificationCareteamRoles.get(role);
            tempCareProvider.setProgramRole(webserverNotificationRole);
                      
            careProviders.add(tempCareProvider);
        }

        System.out.println("\n");
        
        return careProviders;
    }

    private static String getGenderCode(String gender) {
        if (StringUtils.isNotBlank(gender)
                && (gender.startsWith("M") || gender.startsWith("F"))) {
            gender = Character.toString(gender.charAt(0));
        } else {
            gender = "UN";
        }

        return gender;
    }

    /**
     * Get the organization for a given user
     * 
     * @param tempUser
     * @return 
     */
    private static Organization getOrganization(User tempUser, Long communityId) {
        org.gsihealth.wsn.Organization organization = new org.gsihealth.wsn.Organization();

        UserCommunityOrganization userCommunityOrganization = tempUser.getUserCommunityOrganization(communityId);
        com.gsihealth.entity.OrganizationCwd theOrg = userCommunityOrganization.getOrganizationCwd();

        String organizationName = theOrg.getOrganizationCwdName();
        organization.setOrgDesc(StringUtils.upperCase(organizationName));

        String OID = theOrg.getOid();
        organization.setOrganizationID(OID);

        return organization;
    }

    /**
     * Get the person object for a given user
     * 
     * @param tempUser
     * @return 
     */
    private static org.gsihealth.wsn.Person getPerson(User tempUser, Long communityId, UserCommunityOrganization uco) {
        org.gsihealth.wsn.Person wsPerson = new org.gsihealth.wsn.Person();

        // name, email, gender
        wsPerson.setFirstName(tempUser.getFirstName());
        wsPerson.setLastName(tempUser.getLastName());
        wsPerson.setCredentials(uco.getCredentials());
        wsPerson.setEmail(tempUser.getEmail());

        String genderCode = getGenderCode(uco.getGender());

        wsPerson.setGender(genderCode);

        // birth date
        setBirthDate(tempUser, wsPerson);

        // address
        Address address = getAddress(tempUser);
        wsPerson.setAddress(address);

        // telephone
        wsPerson.setPrimaryContactNumber(tempUser.getTelephoneNumber());
        wsPerson.setSecondaryContactNumber("");

        // organization
        org.gsihealth.wsn.Organization organization = getOrganization(tempUser, communityId);
        wsPerson.setOrganization(organization);

        return wsPerson;
    }

    /**
     * Get the address for a given user
     * 
     * @param tempUser
     * @return 
     */
    private static Address getAddress(User tempUser) {
        org.gsihealth.wsn.Address address = new org.gsihealth.wsn.Address();

        address.setAddressLine(tempUser.getStreetAddress1());
        address.setCity(tempUser.getCity());
        address.setState(tempUser.getState());
        address.setZipCode(tempUser.getZipCode());

        return address;
    }

    /**
     * Set the birthdate from the user to the person
     * 
     * @param tempUser
     * @param wsPerson 
     */
    private static void setBirthDate(User tempUser, org.gsihealth.wsn.Person wsPerson) {
        try {
            XMLGregorianCalendar birthDate = getBirthDate(tempUser.getDateOfBirth());
            wsPerson.setBirthDate(birthDate);
        } catch (Exception exc) {
            // ignore setting the birth date
            logger.warning(String.format("Could not set the birthdate for %s %s: %s", tempUser.getFirstName(), tempUser.getLastName(), exc.getMessage()));
        }
    }

    /**
     * Get an XML birth date from given birth date
     * 
     * @param dateOfBirth
     * @return
     * @throws Exception 
     */
    protected static XMLGregorianCalendar getBirthDate(Date dateOfBirth) throws Exception {

        if (dateOfBirth == null) {
            dateOfBirth = new Date();
        }

        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(dateOfBirth);

        XMLGregorianCalendar calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);

        return calendar;
    }

    public static Map<String, String> buildNoticationCareteamRoles(List<NotificationCareteamRole> roles) {
        Map<String, String> notificationCareteamRoles = new HashMap<String, String>();

        for (NotificationCareteamRole tempRole : roles) {

            String key = tempRole.getCareteamRole();
            String value = tempRole.getNotificationWebserviceRole();

            notificationCareteamRoles.put(key, value);
        }

        return notificationCareteamRoles;
    }
}
