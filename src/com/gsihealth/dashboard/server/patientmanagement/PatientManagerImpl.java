package com.gsihealth.dashboard.server.patientmanagement;

import com.gsihealth.patientmdminterface.PatientInterfaceFactory;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.server.util.PropertyUtils;
import com.gsihealth.patientmdminterface.PatientInterface;
import com.gsihealth.patientmdminterface.entity.DuplicateCheckProcessResult;
import com.gsihealth.patientmdminterface.entity.SimplePerson;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.persistence.EntityManager;

/**
 *
 * @author Chad Darby
 */
public class PatientManagerImpl implements PatientManager {

    private Logger logger = Logger.getLogger(getClass().getName());
    private PatientInterface patientInterface;
    private Long communityId;
    private String gsiOid;
    private String whoAmI;
	
    private EntityManager em;
    
    public PatientManagerImpl(Map<String, String> mpiMap) throws Exception {
        this.gsiOid = mpiMap.get(PatientInterface.GSI_OID);
        String commid = mpiMap.get(PatientInterface.COMMUNITY_ID);
        whoAmI = mpiMap.get(PatientInterface.MPI);
        this.communityId = Long.parseLong(commid);
        patientInterface = PatientInterfaceFactory.getMPInterface(mpiMap);
    }

    /**
     * converts searchCriteria into simplePerson and searches mdm
     *
     * @param searchCriteria
     * @return
     */
    @Override
    public List<SimplePerson> findPatient(SearchCriteria searchCriteria) throws Exception {
        SimplePerson personSearchCriteria = PropertyUtils.convert(searchCriteria);
        List<SimplePerson> results = this.findPatient(personSearchCriteria);
        return results;
    }

    /** FIXME makes all searches fuzzy, change this in the new mirth ui
     * @param sp
     * @return
     * @throws Exception
     */
    @Override
    public List<SimplePerson> findPatient(SimplePerson sp) throws Exception {
        sp.setIsFuzzy(true);
        List<SimplePerson> results = patientInterface.findPatient(sp);
        return results;
    }
    
    @Override
    public String getSystemCode(String euid) throws Exception {
        return patientInterface.getSystemCode(euid);
    }

    //JIRA 2523
    @Override
    public DuplicateCheckProcessResult checkForDuplicates(SimplePerson person) throws Exception {
        return patientInterface.checkForDuplicates(person);
    }

    /**
     * Adds a patient to the MDM. If patient already exists, then updates the
     * patient
     *
     * @param person
     * @return
     * @throws Exception
     */
    @Override
    public String addPatient(SimplePerson person) throws Exception {
        String euid = patientInterface.addPatient(person);
        return euid;
    }

    /**
     * Add a patient with the given OID
     *
     * @param person
     * @param oid
     * @return
     * @throws Exception
     */
    @Override
    public String addPatient(SimplePerson person, String oid) throws Exception {
        return patientInterface.addPatient(person, oid);
    }

    /**
     * TODO - is this any different than find patient?
     *
     * @param euid
     * @return
     * @throws Exception
     */
    @Override
    public SimplePerson getPatient(String euid) throws Exception {
        SimplePerson person = patientInterface.getPatient(euid);
        return person;
    }

    @Override
    public SimplePerson getPatient(String oid, long patientId) throws Exception {
        SimplePerson person = patientInterface.getPatientFromLidOrg(oid, Long.toString(patientId));
        
        return person;
    }
    
    @Override
    public Long getCommunityId() {
        return communityId;
    }
    
    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    /**
     *
     * @param theSsn
     * @return true if this ssn belongs to an existing patient
     * @throws Exception
     */
    @Override
    public boolean isDuplicateSsn(String theSsn) throws Exception {
        boolean duplicate = patientInterface.isDuplicateSsn(theSsn);
        return duplicate;
    }

    /**
     *
     * @param thePerson
     * @return thei euid of the duplicate patient
     * @throws Exception
     */
    @Override
    public String getDuplicatePatientEuid(SimplePerson thePerson) throws Exception {
        SimplePerson result = patientInterface.getDuplicatePatient(thePerson);
        if(null == result)
            return null;
        return result.getPatientEuid();
    }
    
    @Override
    public SimplePerson findPatient(long patientId) throws Exception {
        String pIdStr = String.valueOf(patientId);
        return patientInterface.getPatientFromLidOrg(gsiOid,pIdStr);
    }

    /**
     * Find patients based on search criteria
     *
     * @param searchCriteria
     * @return
     */
    @Override
    public List<SimplePerson> findDashboardPatient(SearchCriteria searchCriteria) throws Exception {
        List<SimplePerson> results;
        SimplePerson personSearchCriteria = PropertyUtils.convert(searchCriteria);
        results = patientInterface.findPatient(personSearchCriteria);
        return results;
    }
    
    @Override
    public void setProperties(Map propertiesMap) {
        patientInterface.setProperties(propertiesMap);
    }
    
    @Override
    public void setProperty(String key, String value) {
        patientInterface.setProperty(key, value);
    }

    @Override
    public List<SimplePerson> getPatientsDirectQuery(EntityManager em, List<String> patients, String oid) {
        
        return patientInterface.getPatientsDirectQuery(em, patients, oid);
        
    }

    @Override
    public Map<String, SimplePerson> getMappedPatientsDirectQuery(EntityManager em, List<String> patients, String oid) {
        return patientInterface.getMappedPatientsDirectQuery(em, patients, oid);
        
    }

	@Override
	public void setEntityManager(EntityManager em) throws Exception {
		this.em = em;
        patientInterface.setEntityManager(em);
		logger.warning("em in patient manager: " + em.isOpen());
	}
        
    public String getWhoAmI() {
        return whoAmI;
    }

}
