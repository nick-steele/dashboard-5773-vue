package com.gsihealth.dashboard.server.patientmanagement;

import com.gsihealth.dashboard.server.util.PropertyUtils;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.patientmdminterface.entity.DuplicateCheckProcessResult;
import com.gsihealth.patientmdminterface.util.PatientUtils;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Chad Darby
 */
public class PatientManagerMock implements PatientManager {

    private List<SimplePerson> data;
    
    public List<SimplePerson> findPatient(SearchCriteria searchCriteria) throws Exception {
        return data;
    }

    @Override
    public DuplicateCheckProcessResult checkForDuplicates(SimplePerson person) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String addPatient(Person person) throws Exception {
        return "test-euid";
    }

    public SimplePerson getPatient(String euid) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean isDuplicate(SimplePerson thePerson) throws Exception {

        boolean result = false;
        SearchCriteria searchCriteria = PropertyUtils.convertForDuplicateSearch(thePerson);
        List<SimplePerson> searchResults = findPatient(searchCriteria);

        for (SimplePerson tempPerson : searchResults) {

            if (PatientUtils.personEqualsIgnoreCase(thePerson, tempPerson)) {
                result = true;
                break;
            }
        }

        return result;
    }

    public boolean isDuplicateSsn(String theSsn) throws Exception {
        boolean duplicate = false;

        // short-circuit if no SSN provided
        if (StringUtils.isBlank(theSsn)) {
            return false;
        }
        
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setSsn(theSsn);

        List<SimplePerson> searchResults = findPatient(searchCriteria);

        for (SimplePerson tempPerson : searchResults) {

            duplicate = StringUtils.equals(theSsn, tempPerson.getSSN());

            if (duplicate) {
                break;
            }
        }

        return duplicate;
    }
    
    public String getDuplicatePatientEuid(SimplePerson thePerson) throws Exception {

        String result = "";
        SearchCriteria searchCriteria = PropertyUtils.convertForDuplicateSearch(thePerson);
        List<SimplePerson> searchResults = findPatient(searchCriteria);

        for (SimplePerson tempPerson : searchResults) {

            if (PatientUtils.personEqualsIgnoreCase(thePerson, tempPerson)) {
                result = tempPerson.getPatientEuid();
                break;
            }
        }

        return result;
    }
    
    public List<SimplePerson> getData() {
        return data;
    }

    public void setData(List<SimplePerson> data) {
        this.data = data;
    }

    public String addPatient(SimplePerson person, String oid) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<SimplePerson> findDashboardPatient(SearchCriteria searchCriteria) throws Exception {
        return data;
    }

    @Override
    public SimplePerson getPatient(String oid, long patientId) throws Exception {
         throw new UnsupportedOperationException("Not supported yet.");
    }
    @Override
    public String getSystemCode(String euid) throws Exception {
        throw new UnsupportedOperationException("Implement me stupid");
    }

    @Override
    public String addPatient(SimplePerson person) throws Exception {
        throw new UnsupportedOperationException("com.gsihealth.dashboard.server.patientmanagement.PatientManagerMock.addPatient - Implement me stupid");
    }

    @Override
    public List<SimplePerson> findPatient(SimplePerson sp) throws Exception {
        throw new UnsupportedOperationException("com.gsihealth.dashboard.server.patientmanagement.PatientManagerMock.findPatient - Implement me stupid");
    }

    @Override
    public void setProperties(Map propertiesMap) {
        throw new UnsupportedOperationException("com.gsihealth.dashboard.server.patientmanagement.PatientManagerMock.setProperties - Implement me stupid");
    }

    @Override
    public void setProperty(String key, String value) {
        throw new UnsupportedOperationException("com.gsihealth.dashboard.server.patientmanagement.PatientManagerMock.setProperty - Implement me stupid");
    }

    @Override
    public SimplePerson findPatient(long patientId) throws Exception {
        throw new UnsupportedOperationException("com.gsihealth.dashboard.server.patientmanagement.PatientManagerMock.findPatient - Implement me stupid");
    }
     @Override
    public List<SimplePerson> getPatientsDirectQuery(EntityManager em, List<String> patients, String oid) {
        return null;
    }

    @Override
    public Map<String, SimplePerson> getMappedPatientsDirectQuery(EntityManager em, List<String> patients, String oid) {
        return null;
    }

    @Override
    public Long getCommunityId() {
        throw new UnsupportedOperationException("com.gsihealth.dashboard.server.patientmanagement.PatientManagerMock.getCommunityId - Implement me stupid");
    }

	@Override
	public void setEntityManager(EntityManager em) throws Exception {
		// TODO Auto-generated method stub
		
	}

    @Override
    public String getWhoAmI() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
