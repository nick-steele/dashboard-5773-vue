package com.gsihealth.dashboard.server.patientmanagement;

import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.patientmdminterface.entity.DuplicateCheckProcessResult;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;

/**
 *
 * @author Chad Darby
 */
public interface PatientManager {

    public List<SimplePerson> findPatient(SearchCriteria searchCriteria) throws Exception;
    
    public List<SimplePerson> findPatient(SimplePerson sp) throws Exception;
    
    public SimplePerson findPatient(long patientId) throws Exception;

    //JIRA 2523
    public abstract DuplicateCheckProcessResult checkForDuplicates(SimplePerson person) throws Exception;
    
    public String addPatient(SimplePerson person) throws Exception;

    public String addPatient(SimplePerson person, String oid) throws Exception;
    
    public SimplePerson getPatient(String euid) throws Exception;
    
    public boolean isDuplicateSsn(String theSsn) throws Exception;
    
    public List<SimplePerson> findDashboardPatient(SearchCriteria searchCriteria) throws Exception;
    
    public String getDuplicatePatientEuid(SimplePerson thePerson) throws Exception;
    public String getSystemCode(String euid) throws Exception;
    
    public void setProperties(Map propertiesMap);
    
    public void setProperty(String key, String value);
    
    public Long getCommunityId();
    
    public SimplePerson getPatient(String oid, long patientId) throws Exception ;
    // new stuff
    
//    /** TODO - won't be euid, we'll have to figure that out
//     * @param euid
//     * @return true if found delete is successful or patient not found
//     * @throws Exception 
//     */
//    public boolean deletePatient(String euid) throws Exception;
//    
//    /**
//     * TODO - won't be euid, we'll have to figure that out
//     * @param euid
//     * @return patients with listed euids
//     * @throws Exception 
//     */
//    public List<Person> getPatients(List<String> euid) throws Exception;
//   
    public List<SimplePerson> getPatientsDirectQuery(EntityManager em, List<String> patients, String oid);
    public Map<String, SimplePerson> getMappedPatientsDirectQuery(EntityManager em, List<String> patients, String oid);

	void setEntityManager(EntityManager em) throws Exception;
    
    public String getWhoAmI();
}
