package com.gsihealth.dashboard.server.patientmanagement;

/**
 *
 * @author edwin
 */

import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.server.util.PropertyUtils;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;

/**
 * Hello world!
 *
 */
public class App {

    private static final String defaultOid = "2.16.840.1.113883.3.1358";
    private static final String DEV_PATIENT_MDM_ENDPOINT = "http://10.110.210.60:52501/PatientMDMEJBService/PatientMDMEJB?wsdl";
    private static final String QA_PATIENT_MDM_ENDPOINT = "http://10.110.210.85:52501/PatientMDMEJBService/PatientMDMEJB?wsdl";
    private static final String STAGING_PATIENT_MDM_ENDPOINT = "http://10.110.210.94:52501/PatientMDMEJBService/PatientMDMEJB?wsdl";
    // private static final String PRODUCTION_PATIENT_MDM_ENDPOINT = "http://10.128.65.114:52501/PatientMDMEJBService/PatientMDMEJB?wsdl";
    private Map<String, String> environments;
    private Map<String, String> mpiMap = new HashMap<String,String>();
    private Logger logger = Logger.getLogger(getClass().getName());

    public App() {
        environments = new HashMap<String, String>();

        environments.put(DEV_PATIENT_MDM_ENDPOINT, "DEV");
        environments.put(QA_PATIENT_MDM_ENDPOINT, "QA");
        environments.put(STAGING_PATIENT_MDM_ENDPOINT, "STAGING");
        // environments.put(PRODUCTION_PATIENT_MDM_ENDPOINT, "PRODUCTION");
    }

    private String getEnvironmentName(String key) {
        return environments.get(key);
    }

//    public void testGetEUID() {
//        try {
//            PatientManagerImpl service = new PatientManagerImpl(DEV_PATIENT_MDM_ENDPOINT, defaultOid);
//            String systemCode = "2.16.840.1.113883.3.72.5.9.1";
//            String localid = "RS-491";
//
//            String result = service.getEUID(systemCode, localid);
//            System.out.println("Result = " + result);
//        } catch (Exception exc) {
//            exc.printStackTrace();
//        }
//    }

    public void testFindPatient() {
        try {
            
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setLastName("Dimas");

            List<SimplePerson> results = service.findPatient(searchCriteria);

            for (SimplePerson temp : results) {
                System.out.println(ReflectionToStringBuilder.toString(temp));
            }

            System.out.println("Size = " + results.size());
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testLoadPatient() {
        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            String systemCode = defaultOid;
            String localid = "197677599";

            SimplePerson person = new SimplePerson();
            person.setFirstName("Mary");
            person.setLastName("Donahue");
            person.setLocalId(localid);
            person.setLocalOrg(systemCode);

            person.setBirthDateTime("07/12/1963"); //DOB needs to be set to avoid null pointer

            person.setGenderCode("M");
            person.setStreetAddress1("500 Bellfort Blvd");
            person.setStreetAddress2("apt 12");
            person.setCity("Houston");
            person.setState("TX");
            person.setZipCode("77033");
            person.setPhoneHome("7137430006");
            person.setTelephoneExtension("989");

            String results = service.addPatient(person);

            System.out.println("Finished adding patient. EUID = " + results);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testLoadPatient_WithPhoneNumber() {
        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            String systemCode = "1.2.3.4";
            String localid = "197677888";

            SimplePerson person = new SimplePerson();
            person.setFirstName("Larry");
            person.setLastName("Apple");
            person.setLocalId(localid);
            person.setLocalOrg(systemCode);

            person.setBirthDateTime("07/12/1960"); //DOB needs to be set to avoid null pointer

            person.setGenderCode("F");
            person.setStreetAddress1("500 Bellfort Blvd");
            person.setStreetAddress2("apt 12");
            person.setCity("Houston");
            person.setState("TX");
            person.setZipCode("77033");
            person.setPhoneHome("2222222222");
            person.setTelephoneExtension("888");

            String results = service.addPatient(person);

            System.out.println("Finished adding patient. EUID = " + results);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    // 0000027040
    public void testUpdatePatient_ClearPhoneNumber() {
        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            String euid = "0000027349";

            SimplePerson person = service.getPatient(euid);

            System.out.println("BEFORE: " + ReflectionToStringBuilder.toString(person));
            String before = person.getPhoneHome();
            System.out.println("BEFORE: " + before);

            /*
            System.out.println("setting phone number to blank and updating in MDM");
            person.setTelephone("5555555555");
            person.setTelephoneExtension("4444");
            String localid = "197677033";
            person.setLocalId(localid);
            String results = service.addPatient(person);
            System.out.println("update complete");
            
            Person personAfter = service.getPatient(euid);
            
            System.out.println("\n\nAFTER: " + ReflectionToStringBuilder.toString(personAfter));            
            String after = personAfter.getTelephone();
            System.out.println("AFTER: " + after);
            
            System.out.println("UPDATED = " + !StringUtils.equals(before, after));
             */

            // System.out.println("Finished updating patient: " + results);

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testUpdatePatient_ClearAddress2() {
        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            String euid = "0000027348";

            SimplePerson person = service.getPatient(euid);

            System.out.println("BEFORE: " + ReflectionToStringBuilder.toString(person));
            String before = person.getStreetAddress2();
            System.out.println("BEFORE: " + before);

            System.out.println("setting address 2to blank and updating in MDM");
            person.setStreetAddress2("-");
            String localid = "197677033";
            person.setLocalId(localid);
            String results = service.addPatient(person);
            System.out.println("update complete");

            SimplePerson personAfter = service.getPatient(euid);

            System.out.println("\n\nAFTER: " + ReflectionToStringBuilder.toString(personAfter));
            String after = personAfter.getStreetAddress2();
            System.out.println("AFTER: " + after);

            System.out.println("UPDATED = " + !StringUtils.equals(before, after));

            System.out.println("Finished updating patient: " + results);

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testUpdatePatient() {
        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            String systemCode = "2.16.840.1.113883.3.72.5.9.1";
            String localid = "RS-491129";

            SimplePerson person = new SimplePerson();
            person.setFirstName("Peter");
            person.setLastName("Scott");
            person.setLocalId(localid);
            person.setLocalOrg(systemCode);


         
            person.setBirthDateTime("07/07/1907"); //DOB needs to be set to avoid null pointer

            person.setGenderCode("M");
            person.setStreetAddress1("500 main avenue");
            person.setStreetAddress2("apt 555");
            person.setCity("pittsburgh");
            person.setState("PA");
            person.setZipCode("15213");

            String results = service.addPatient(person);

            System.out.println("Finished updating patient: " + results);

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testSetPatientRace() {

        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);
            String euid = "0000027382";

            SimplePerson person = service.getPatient(euid);

            String raceCode = "2028-9";
            person.setRace(raceCode);
            // person.setRace(null);
            person.setLocalId("8");

            int x = 0;

            service.addPatient(person);

            person = service.getPatient(euid);

            System.out.println(ReflectionToStringBuilder.toString(person));
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testSetPatientEthnic() {

        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);
            String euid = "0000027382";

            SimplePerson person = service.getPatient(euid);

            String ethnicCode = "H";
            // person.setEthnic(ethnicCode);
            // person.setRace(null);
            
            person.setLanguage("NL");
            
            person.setLocalId("8");

            service.addPatient(person);

            person = service.getPatient(euid);

            System.out.println(ReflectionToStringBuilder.toString(person));

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testGetPatient() {

        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);
            String euid = "0000027382";

            SimplePerson person = service.getPatient(euid);

            System.out.println(ReflectionToStringBuilder.toString(person));

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testSearch_LastName(String endpoint, String lastName, String telephone) {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("\n\n" + methodName + ":  " + getEnvironmentName(endpoint));

        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setLastName(lastName);
            searchCriteria.setPhone(telephone);

            List<SimplePerson> persons = service.findPatient(searchCriteria);

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testSearch_LastName(String endpoint, String lastName) {

        testSearch_LastName(endpoint, lastName, null);
    }

    public void testSearch_LastName(String endpoint) {
        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("\n\n" + methodName + ":  " + getEnvironmentName(endpoint));

        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setLastName("robinson");
            List<SimplePerson> persons = service.findPatient(searchCriteria);

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testSearch_Gender(String endpoint) {

        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("\n\n" + methodName + ":  " + getEnvironmentName(endpoint));

        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setGender("M");
            List<SimplePerson> persons = service.findPatient(searchCriteria);

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testSearch_FirstName(String endpoint) {

        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("\n\n" + methodName + ":  " + getEnvironmentName(endpoint));

        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setFirstName("kevin");
            List<SimplePerson> persons = service.findPatient(searchCriteria);

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testSearch_MiddleName(String endpoint) {

        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("\n\n" + methodName + ":  " + getEnvironmentName(endpoint));

        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setMiddleName("ray");
            List<SimplePerson> persons = service.findPatient(searchCriteria);

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testSearch_DOB(String endpoint) {

        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("\n\n" + methodName + ":  " + getEnvironmentName(endpoint));

        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setDateOfBirthStr("01/02/1962");
            List<SimplePerson> persons = service.findPatient(searchCriteria);

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testSearch_StreetAddress1(String endpoint) {

        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("\n\n" + methodName + ":  " + getEnvironmentName(endpoint));

        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setAddress1("500 main st.");
            List<SimplePerson> persons = service.findPatient(searchCriteria);

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testSearch_StreetAddress2(String endpoint) {


        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("\n\n" + methodName + ":  " + getEnvironmentName(endpoint));


        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            System.out.println("Hello World in test method");

            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setAddress2("apt 713");
            List<SimplePerson> persons = service.findPatient(searchCriteria);

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testSearch_City(String endpoint) {

        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("\n\n" + methodName + ":  " + getEnvironmentName(endpoint));

        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setCity("philadelphia");
            List<SimplePerson> persons = service.findPatient(searchCriteria);

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testSearch_State(String endpoint) {

        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("\n\n" + methodName + ":  " + getEnvironmentName(endpoint));

        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setState("PA");
            List<SimplePerson> persons = service.findPatient(searchCriteria);

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testSearch_ZipCode(String endpoint) {

        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("\n\n" + methodName + ":  " + getEnvironmentName(endpoint));

        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setZipCode("19103");
            List<SimplePerson> persons = service.findPatient(searchCriteria);

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testSearch_Telephone(String endpoint) {

        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("\n\n" + methodName + ":  " + getEnvironmentName(endpoint));

        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setPhone("2155551212");
            List<SimplePerson> persons = service.findPatient(searchCriteria);

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public void testSearch_All(String endpoint) {

        String methodName = new Object() {
        }.getClass().getEnclosingMethod().getName();
        logger.info("\n\n" + methodName + ":  " + getEnvironmentName(endpoint));

        try {
            PatientManagerImpl service = new PatientManagerImpl(mpiMap);

            SearchCriteria searchCriteria = new SearchCriteria();
            List<SimplePerson> persons = service.findPatient(searchCriteria);

        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    public static void main(String[] args) {
        App app = new App();
        System.out.println("Thurs morning");

        // app.testGetEUID();

        // app.testLoadPatient();

        // app.testFindPatient();
        // app.testUpdatePatient_ClearPhoneNumber();

        // app.testUpdatePatient_ClearAddress2();

        // app.testUpdatePatient();

        // app.testGetPatient();

        /*
        app.testSearch_LastName(DEV_PATIENT_MDM_ENDPOINT);
        app.testSearch_LastName(QA_PATIENT_MDM_ENDPOINT);
         */
        // app.testSearch_LastName(STAGING_PATIENT_MDM_ENDPOINT, "thursday", "1312313213");

        /*
        app.testSearch_FirstName(DEV_PATIENT_MDM_ENDPOINT);
        app.testSearch_FirstName(QA_PATIENT_MDM_ENDPOINT);
        app.testSearch_FirstName(STAGING_PATIENT_MDM_ENDPOINT);
        
        app.testSearch_MiddleName(DEV_PATIENT_MDM_ENDPOINT);
        app.testSearch_MiddleName(QA_PATIENT_MDM_ENDPOINT);
        app.testSearch_MiddleName(STAGING_PATIENT_MDM_ENDPOINT);
         */

        // app.testSearch_Gender(DEV_PATIENT_MDM_ENDPOINT);
        // app.testSearch_Gender(QA_PATIENT_MDM_ENDPOINT);
        // app.testSearch_Gender(STAGING_PATIENT_MDM_ENDPOINT);

        /*
        app.testSearch_DOB(DEV_PATIENT_MDM_ENDPOINT);
        app.testSearch_DOB(QA_PATIENT_MDM_ENDPOINT);
        app.testSearch_DOB(STAGING_PATIENT_MDM_ENDPOINT);
        
        app.testSearch_StreetAddress1(DEV_PATIENT_MDM_ENDPOINT);
        app.testSearch_StreetAddress1(QA_PATIENT_MDM_ENDPOINT);
        app.testSearch_StreetAddress1(STAGING_PATIENT_MDM_ENDPOINT);
        
        app.testSearch_StreetAddress2(DEV_PATIENT_MDM_ENDPOINT);
        app.testSearch_StreetAddress2(QA_PATIENT_MDM_ENDPOINT);
        app.testSearch_StreetAddress2(STAGING_PATIENT_MDM_ENDPOINT);
        
        app.testSearch_City(DEV_PATIENT_MDM_ENDPOINT);
        app.testSearch_City(QA_PATIENT_MDM_ENDPOINT);
        app.testSearch_City(STAGING_PATIENT_MDM_ENDPOINT);
        
        app.testSearch_State(DEV_PATIENT_MDM_ENDPOINT);
        app.testSearch_State(QA_PATIENT_MDM_ENDPOINT);
        app.testSearch_State(STAGING_PATIENT_MDM_ENDPOINT);
        
        app.testSearch_ZipCode(DEV_PATIENT_MDM_ENDPOINT);
        app.testSearch_ZipCode(QA_PATIENT_MDM_ENDPOINT);
        app.testSearch_ZipCode(STAGING_PATIENT_MDM_ENDPOINT);
        
        app.testSearch_Telephone(DEV_PATIENT_MDM_ENDPOINT);
        app.testSearch_Telephone(QA_PATIENT_MDM_ENDPOINT);
        app.testSearch_Telephone(STAGING_PATIENT_MDM_ENDPOINT);
         */

        // app.testSetPatientRace();
        app.testSetPatientEthnic();

    }
}
