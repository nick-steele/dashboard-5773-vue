package com.gsihealth.dashboard.server.patientmanagement;

/**
 *
 * @author Chad Darby
 */
public class MdmConstants {

    public static String HOME = "HOME";

    public static final String CLEAR_FIELD_INDICATOR = "NULL_VALUE";

    public static final String PHONE_CLEAR_FIELD_INDICATOR = "0000000000";

    public static final String PHONE_EXT_CLEAR_FIELD_INDICATOR = "000000";

    public static final String SSN_CLEAR_FIELD_INDICATOR = "000000000";

    public static final String RACE_CLEAR_FIELD_INDICATOR = "0000-0";

    public static final String ETHNIC_CLEAR_FIELD_INDICATOR = "NULL";

    public static final String LANGUAGE_CLEAR_FIELD_INDICATOR = "NULL";
    
}
