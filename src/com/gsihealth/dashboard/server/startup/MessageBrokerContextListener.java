package com.gsihealth.dashboard.server.startup;

import com.gsihealth.dashboard.server.jms.MessageBrokerContext;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Reads the connection factory and queue. Stores the objects in the message broker context
 * 
 * @author Chad.Darby
 */
@WebListener
public class MessageBrokerContextListener implements ServletContextListener {

    @Resource(mappedName = "jms/NotificationConnectionFactory")
    private ConnectionFactory connectionFactory;
    
    @Resource(mappedName = "jms/NotificationQueue")
    private Queue notificationQueue;
    
    @Resource(mappedName = "jms/NotificationConnectionFactory")
    private ConnectionFactory xdsConnectionFactory;
    
    @Resource(mappedName = "jms/NotificationQueue")
    private Queue xdsQueue;
    
    private Logger logger = Logger.getLogger(getClass().getName());
    
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        logger.info("JmsMessageBrokerSupportContextListener: starting initialization");

        try {
            logger.info("ConnectionFactory: " + connectionFactory);
            logger.info("notificationQueue: " + notificationQueue);

            // store conn factory and queue in context            
            MessageBrokerContext messageBrokerContext = MessageBrokerContext.getInstance();
            
            messageBrokerContext.setConnectionFactory(connectionFactory);
            messageBrokerContext.setQueue(notificationQueue);
            
            messageBrokerContext.setXdsConnectionFactory(xdsConnectionFactory);
            messageBrokerContext.setXdsQueue(xdsQueue);
            
            logger.info("JmsMessageBrokerSupportContextListener: initialized succesfully");
        }
        catch (Exception exc) {
            logger.logp(Level.SEVERE, getClass().getName(), "contextInitialized", "Error setting up JMS components", exc);            
        }
                
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        
    }
    
}
