package com.gsihealth.dashboard.server.startup;

import com.gsihealth.dashboard.server.jms.CoordinatorBrokerContext;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedScheduledExecutorService;
import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Reads the connection factory and queue. Stores the objects in the coordinator broker context
 * 
 * @author Chad.Darby
 */
@WebListener
public class CoordinatorBrokerContextListener implements ServletContextListener {

    @Resource(mappedName = "jms/CoordinatorConnectionFactory")
    private ConnectionFactory connectionFactory;
    
    @Resource(mappedName = "jms/Enrollment_PatientUpdate_Queue")
    private Queue enrollmentPatientUpdateQueue;

    @Resource(mappedName = "jms/CareTeam_PatientUpdate_Queue")
    private Queue careTeamPatientUpdateQueue;
    
    @Resource(mappedName = "concurrent/__defaultManagedScheduledExecutorService")
    private ManagedScheduledExecutorService mses;
    
    private Logger logger = Logger.getLogger(getClass().getName());
    
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        logger.info("CoordinatorBrokerContextListener: starting initialization");

        try {
            logger.info("ConnectionFactory: " + connectionFactory);
            logger.info("enrollmentPatientUpdateQueue: " + enrollmentPatientUpdateQueue);
            logger.info("careTeamPatientUpdateQueue: " + careTeamPatientUpdateQueue);
            logger.info("mses: " + mses);

            // store conn factory and queue in context            
            CoordinatorBrokerContext coordinatorBrokerContext = CoordinatorBrokerContext.getInstance();
            
            coordinatorBrokerContext.setConnectionFactory(connectionFactory);
            coordinatorBrokerContext.setEnrollmentPatientUpdateQueue(enrollmentPatientUpdateQueue);
            coordinatorBrokerContext.setCareTeamPatientUpdateQueue(careTeamPatientUpdateQueue);
            
            logger.info("CoordinatorBrokerContextListener: initialized succesfully");
        }
        catch (Exception exc) {
            logger.logp(Level.SEVERE, getClass().getName(), "contextInitialized", "Error setting up JMS components", exc);            
        }
                
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        
    } 
}
