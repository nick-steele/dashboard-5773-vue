package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.common.*;
import com.gsihealth.dashboard.common.Constants;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.*;
import com.gsihealth.dashboard.entity.dto.insurance.PayerClassDTO;
import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import com.gsihealth.dashboard.server.CommunityContexts;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.common.*;
import com.gsihealth.dashboard.server.dao.*;
import com.gsihealth.dashboard.server.hisp.HispClient;
import com.gsihealth.dashboard.server.hisp.HispClientUtils;
import com.gsihealth.dashboard.server.jms.MessageBrokerContext;
import com.gsihealth.dashboard.server.partner.PixClient;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import com.gsihealth.dashboard.server.service.integration.PatientUserNotification;
import com.gsihealth.dashboard.server.service.integration.UserService;
import com.gsihealth.dashboard.server.tasks.MinorMessageProcessing;
import com.gsihealth.dashboard.server.util.*;
import com.gsihealth.entity.*;
import com.gsihealth.patientmdminterface.entity.DuplicateCheckProcessResult;
import com.gsihealth.patientmdminterface.entity.DuplicateCheckProcessResult.ProcessDecision;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.dozer.MappingException;
import org.gsihealth.wsn.CareProvider;
import org.gsihealth.wsn.CareTeamAssociation;
import org.gsihealth.wsn.GSIHealthNotificationMessage;

import javax.servlet.ServletContext;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chad Darby
 */
public class EnrollmentServiceHelper {

    private Logger logger = Logger.getLogger(getClass().getName());
    private AuditDAO auditDAO;
    private CommunityCareteamDAO careteamDAO;
    private NotificationAuditDAO notificationAuditDAO;
    private OrganizationPatientConsentDAO organizationPatientConsentDAO;
    private OrganizationDAO organizationDAO;
    private UserPatientConsentDAO userPatientConsentDAO;
    private UserDAO userDAO;
    private PatientEnrollmentHistoryDAO patientEnrollmentHistoryDAO;
    private PixClient pixClient;
    private final String bhixConsentWsdl = "/META-INF/wsdl/XDS.b_DocumentRepository.wsdl";
    private DisenrollmentReasonDAO disenrollmentReasonDAO;
    private Map<Long, String> orgOidsCache = new ConcurrentHashMap<>();
    private ProgramNameHistoryDAO programNameHistoryDAO;
    private AccessLevelDAO accessLevelDAO;
    private SSOUserDAO sSOUserDAO;
    private UserService userService;
    private UserCommunityOrganizationDAO userCommunityOrganizationDAO;
    private PatientUserNotification patientUserNotification;
    private SecurityServiceHelper securityServiceHelper;
    private CareManagementOrganizationHistoryDAO careManagementOrganizationHistoryDAO;
    private Map<Long, CommunityContexts> communityContexts;
    private AcuityScoreHistoryDAO acuityScoreHistoryDAO;
    private ConsentHistoryDAO consentHistoryDAO;
    private CommunityOrganizationDAO communityOrganizationDAO;
    private ConfigurationDAO configurationDAO;
    // use these only for messageTool FIXME - FIND A BETTER WAY TO DO THIS
    private PatientManager patientManager;
    private PatientEnrollmentDAO patientEnrollmentDAO;
    private String fromOid;
    private String toOid;

    //JIRA 629 - remove properties
    public EnrollmentServiceHelper(PatientManager patientManager,
            NotificationAuditDAO notificationAuditDAO, OrganizationPatientConsentDAO organizationPatientConsentDAO,
            PatientEnrollmentDAO patientEnrollmentDAO, OrganizationDAO organizationDAO) {

        this.patientManager = patientManager;
        this.notificationAuditDAO = notificationAuditDAO;
        this.organizationPatientConsentDAO = organizationPatientConsentDAO;
        this.patientEnrollmentDAO = patientEnrollmentDAO;
        this.organizationDAO = organizationDAO;
    }

    //JIRA 629 - remove properties
    public EnrollmentServiceHelper(Map<Long, CommunityContexts> communityContexts,
            AuditDAO auditDAO, CommunityCareteamDAO careteamDAO, NotificationAuditDAO notificationAuditDAO, OrganizationPatientConsentDAO organizationPatientConsentDAO,
            OrganizationDAO organizationDAO, UserPatientConsentDAO userPatientConsentDAO,
            UserDAO userDAO, PatientUserNotification patientUserNotification, PixClient pixClient) {
        this.auditDAO = auditDAO;
        this.careteamDAO = careteamDAO;
        this.communityContexts = communityContexts;
        this.notificationAuditDAO = notificationAuditDAO;
        this.organizationDAO = organizationDAO;
        this.organizationPatientConsentDAO = organizationPatientConsentDAO;
        this.patientUserNotification = patientUserNotification;
        this.pixClient = pixClient;
        this.userDAO = userDAO;
        this.userPatientConsentDAO = userPatientConsentDAO;
        postConfig();
    }

    //JIRA 629 - remove properties
    public EnrollmentServiceHelper(Map<Long, CommunityContexts> communityContexts,
            AuditDAO auditDAO, CommunityCareteamDAO careteamDAO, NotificationAuditDAO notificationAuditDAO, OrganizationPatientConsentDAO organizationPatientConsentDAO,
            OrganizationDAO organizationDAO, UserPatientConsentDAO userPatientConsentDAO,
            UserDAO userDAO, PatientEnrollmentHistoryDAO patientEnrollmentHistoryDAO, DisenrollmentReasonDAO disenrollmentReasonDAO, ProgramNameHistoryDAO programNameHistoryDAO,
            AccessLevelDAO accessLevelDAO, SSOUserDAO sSOUserDAO, UserService userService,
            UserCommunityOrganizationDAO userCommunityOrganizationDAO,
            PixClient pixClient, PatientUserNotification patientUserNotification,
            CareManagementOrganizationHistoryDAO careManagementOrganizationHistoryDAO, AcuityScoreHistoryDAO acuityScoreHistoryDAO,
            CommunityOrganizationDAO communityOrganizationDAO, ConfigurationDAO configurationDAO,ConsentHistoryDAO consentHistoryDAO) {

        this.communityContexts = communityContexts;
        this.auditDAO = auditDAO;
        this.careteamDAO = careteamDAO;
        this.notificationAuditDAO = notificationAuditDAO;
        this.organizationPatientConsentDAO = organizationPatientConsentDAO;
        this.organizationDAO = organizationDAO;
        this.userPatientConsentDAO = userPatientConsentDAO;
        this.userDAO = userDAO;
        this.patientEnrollmentHistoryDAO = patientEnrollmentHistoryDAO;
        this.disenrollmentReasonDAO = disenrollmentReasonDAO;
        this.programNameHistoryDAO = programNameHistoryDAO;
        this.accessLevelDAO = accessLevelDAO;
        this.sSOUserDAO = sSOUserDAO;
        this.userService = userService;
        this.userCommunityOrganizationDAO = userCommunityOrganizationDAO;
        this.pixClient = pixClient;
        this.patientUserNotification = patientUserNotification;
        this.careManagementOrganizationHistoryDAO = careManagementOrganizationHistoryDAO;
        this.acuityScoreHistoryDAO = acuityScoreHistoryDAO;
        this.communityOrganizationDAO = communityOrganizationDAO;
        this.configurationDAO = configurationDAO;
        this.consentHistoryDAO = consentHistoryDAO;
        postConfig();
    }

    public EnrollmentServiceHelper() {
    }

    EnrollmentServiceHelper(Map<Long, CommunityContexts> communityContexts, AuditDAO auditDAO, CommunityCareteamDAO careteamDAO, 
            NotificationAuditDAO notificationAuditDAO, OrganizationPatientConsentDAO organizationPatientConsentDAO, OrganizationDAO organizationDAO, 
            UserPatientConsentDAO userPatientConsentDAO, UserDAO userDAO, PatientUserNotification patientUserNotification, 
            PixClient pixClient, CommunityOrganizationDAO communityOrganizationDAO) {
        this.auditDAO = auditDAO;
        this.careteamDAO = careteamDAO;
        this.communityContexts = communityContexts;
        this.notificationAuditDAO = notificationAuditDAO;
        this.organizationDAO = organizationDAO;
        this.organizationPatientConsentDAO = organizationPatientConsentDAO;
        this.patientUserNotification = patientUserNotification;
        this.pixClient = pixClient;
        this.userDAO = userDAO;
        this.userPatientConsentDAO = userPatientConsentDAO;
        this.communityOrganizationDAO = communityOrganizationDAO;
        postConfig();
    }

    public void postConfig() {

        securityServiceHelper = new SecurityServiceHelper();
    }

    public void updatePatientHelper(
            LoginResult loginResult,
            PersonDTO personDTO,
            String oldEnrollmentStatus,
            boolean checkForDuplicate,boolean checkForDuplicateSsn,
            boolean primaryMedicaidcareIdBln,
            boolean secondaryMedicaidcareIdBln,
            boolean checkEnableCareteam,
            PatientInfo patientInfo,
            long oldOrganizationId,
            Person updatedPerson,
            String oldAcuityScore
    ) throws PortalException, DuplicateException, DuplicatePatientException, DuplicatePatientMedicaidMedicareIDException, NoSoupForYouException, Exception {

        checkAuthorization(loginResult, personDTO);
        System.out.println("Patient Info At starting>>>"+patientInfo);

        logger.info("personDTO additional orgs: " + personDTO.getAdditionalConsentedOrganizations());
        Long superDuperUserCommunityIdToSet = null;
        long communityId = this.getCommunityIdToSaveWith(loginResult.getCommunityId(), superDuperUserCommunityIdToSet, false);
        
        Community community = new Community(communityId);

        // JIRA - 618
        //TODO Clean this up as well, fresh PCE is retrieved at multiple places
        PatientCommunityEnrollment patientData = this.getPatientEnrollmentDAO(communityId).findPatientEnrollment(personDTO.getPatientEnrollment().getPatientId());
        // PLCS now gets operated by program. Always keep original value
        updatedPerson.getPatientCommunityEnrollment().setProgramLevelConsentStatus(patientData.getProgramLevelConsentStatus());
        
        PatientEnrollmentDTO patientEnrollmentDTO = personDTO.getPatientEnrollment();
        logger.fine("update  patientId = " + personDTO.getPatientEnrollment().getPatientId() + "in community: " + communityId);
        PayerPlanDTO primaryPayerPlanDTO = patientEnrollmentDTO.getPrimaryPayerPlan();
        String newEnrollmentStatus = personDTO.getPatientEnrollment().getStatus();
        logger.info("old enrollment status: " + oldEnrollmentStatus + " new enr status: " + newEnrollmentStatus
                + " for patient " + personDTO.getPatientEnrollment().getPatientId());


        //Spira 8758 - Populate created date from PCE
        updatedPerson.getSimplePerson().setCreatedDate(patientData.getCreatedDate());

        Long patientRelationshipId = personDTO.getPatientEnrollment().getPatientEmergencyContact().getPatientRelationshipId();

        logger.info("4733: helpe: patientRelationshipId=" + patientRelationshipId);

        if (patientRelationshipId != null && patientRelationshipId > 0) {
            PatientEmergencyContact patientEmergencyContact = updatedPerson.getPatientCommunityEnrollment().getPatientEmergencyContact();
            patientEmergencyContact.setRelationship(patientRelationshipId);
        }

        String email = updatedPerson.getPatientCommunityEnrollment().getEmailAddress();
        String patEuid = updatedPerson.getPatientCommunityEnrollment().getPatientEuid();
        PatientCommunityEnrollment patient = this.getPatientEnrollmentDAO(communityId).findPatientEnrollment(patEuid);
        String sameEmail = patient.getEmailAddress();

        if ((email != null) && (!email.equals(sameEmail))) {

            PatientCommunityEnrollment duplicateEnrollment = this.getPatientEnrollmentDAO(communityId).isDuplicateEmail(email);

            if (duplicateEnrollment != null) {
                Long orgId = duplicateEnrollment.getOrgId();
                String patientOrg = organizationDAO.findOrganization(orgId, communityId).getOrganizationCwdName();
                String status = duplicateEnrollment.getStatus();
                String dupEmailMsg = Constants.DUPLICATEE_EMAILMAIL_MSG + patientOrg + ".";
                throw new DuplicateException(dupEmailMsg);
            }
        }

        if(checkForDuplicate) {
            checkDuplicate(communityId, updatedPerson, "updating");
        }

        //Update payer
        //primary Payer Plan
        if (personDTO.getPatientEnrollment().getPrimaryPayerPlan() != null) {

            primaryPayerPlanDTO = personDTO.getPatientEnrollment().getPrimaryPayerPlan();
            if (primaryPayerPlanDTO != null) {
                updatedPerson.setPrimaryPayerPlanId(primaryPayerPlanDTO.getId());
                updatedPerson.getPatientCommunityEnrollment().setPrimaryPayerPlanId(primaryPayerPlanDTO.getId());
            }
        }

        //primary payer class
        if (personDTO.getPatientEnrollment().getPrimaryPayerClass() != null) {
            PayerClassDTO primaryPayerClassDTO = personDTO.getPatientEnrollment().getPrimaryPayerClass();

            if (primaryPayerClassDTO != null) {
                updatedPerson.setPrimaryPayerClassId(primaryPayerClassDTO.getId());
                updatedPerson.getPatientCommunityEnrollment().setPrimaryPayerClassId(primaryPayerClassDTO.getId());
            }
        }

        //secondary Payer Plan
        if (personDTO.getPatientEnrollment().getSecondaryPayerPlan() != null) {

            PayerPlanDTO secondaryPayerPlanDTO = personDTO.getPatientEnrollment().getSecondaryPayerPlan();

            if (secondaryPayerPlanDTO != null) {
                updatedPerson.setSecondaryPayerPlanId(secondaryPayerPlanDTO.getId());
                updatedPerson.getPatientCommunityEnrollment().setSecondaryPayerPlanId(secondaryPayerPlanDTO.getId());
            }
        }

        //secondary Payer class
        if (personDTO.getPatientEnrollment().getSecondaryPayerClass() != null) {

            PayerClassDTO secondaryPayerClassDTO = personDTO.getPatientEnrollment().getSecondaryPayerClass();
            if (secondaryPayerClassDTO != null) {
                updatedPerson.setSecondaryPayerClassId(secondaryPayerClassDTO.getId());
                updatedPerson.getPatientCommunityEnrollment().setSecondaryPayerClassId(secondaryPayerClassDTO.getId());
            }
        }

        //Spira 6940 - remove the if condition and make the dup check happen all the time
        //if (checkPatientUserUpdate) {
        String checkSSNMedicaidCareMsg = this.checkDuplicateSSNMedicaidMediCareIDUpdate(updatedPerson, checkForDuplicateSsn, primaryMedicaidcareIdBln, secondaryMedicaidcareIdBln, communityId);
        if (checkSSNMedicaidCareMsg != null) {
            throw new DuplicatePatientMedicaidMedicareIDException(checkSSNMedicaidCareMsg);
        }

        PatientCommunityEnrollment patientEnrollment = updatedPerson.getPatientCommunityEnrollment();

        String newProgramLevelConsentStatus = patientEnrollment.getProgramLevelConsentStatus();
        boolean saveConsentDate = StringUtils.equalsIgnoreCase(newProgramLevelConsentStatus, Constants.YES) || StringUtils.equalsIgnoreCase(newProgramLevelConsentStatus, Constants.NO);

        if (saveConsentDate) {
            Date consentDate = personDTO.getPatientEnrollment().getConsentDateTime();
            patientEnrollment.setConsentDateTime(consentDate);
            patientEnrollment.setConsentObtainedByUserId(personDTO.getPatientEnrollment().getConsentObtainedByUserId());
        }
// save minor consent data
        // Fix me as consentDate contains date and time combined, only format needs to be changed.
        if (personDTO.getPatientEnrollment().getMinorConsentDateTime() != null) {

            Date consentDate = personDTO.getPatientEnrollment().getMinorConsentDateTime();
            patientEnrollment.setMinorConsentDateTime(consentDate);
        }

        //Program Health Home
        if (personDTO.getPatientEnrollment().getProgramHealthHome() != null) {
            ProgramHealthHomeDTO programHealthHomeDTO = personDTO.getPatientEnrollment().getProgramHealthHome();
            if (programHealthHomeDTO != null) {
                updatedPerson.setProgramHealthHomeId(programHealthHomeDTO.getId());
            }
        }


        //patient user account creation here
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        boolean patientEngagementEnable = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "patient.engagement.enable", "true"));
        boolean msgAppUrlConfFlag = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "patientuser.webservice.client.url.conf.flag", "true"));
        logger.info("patientEngagementEnable:" + patientEngagementEnable + ":msgAppUrlConfFlag:" + msgAppUrlConfFlag + ":Euid:" + personDTO.getPatientEnrollment().getPatientEuid());;

        // TODO why get a fresh enrollment? well for now, did someone update org somewhere else?  get it
        // FIXME: We can reuse this entity retrieved earlier in this method. This call seems to be redundant:
        // FIXME: PatientCommunityEnrollment patient = this.getPatientEnrollmentDAO(communityId).findPatientEnrollment(patEuid);
        PatientCommunityEnrollment enrollment = careteamDAO.findPatientEnrollment(personDTO.getPatientEnrollment().getPatientEuid());
        long organizationId = enrollment.getOrgId();
        OrganizationCwd organization = organizationDAO.findOrganization(organizationId, communityId);
        CommunityOrganization communityOrganization = new CommunityOrganization(community, organization);
        updatedPerson.getPatientCommunityEnrollment().setOrganization(organization);
        updatedPerson.getPatientCommunityEnrollment().setOrgId(organizationId);
        String patientUserActiveStatus = enrollment.getPatientUserActive();

        logger.info(">>>>>>>>>>>>>>>back-end status:" + patientUserActiveStatus + ":UI VALUE:" + personDTO.getPatientUserActive());
        logger.info(">>:new PatientEnrollment().getStatus::" + newEnrollmentStatus);

        boolean isActiveProcessFlag = false;
        if (patientUserActiveStatus.equalsIgnoreCase(personDTO.getPatientUserActive())) {
            isActiveProcessFlag = true;
        }

        setPatientUserIsActive(patientUserActiveStatus, personDTO);
        logger.info(">>>>>>>>>>FINAL --isActiveProcessFlag:" + isActiveProcessFlag + ":PatientUserActive:" + personDTO.getPatientUserActive());

        PatientEngagementRole engagementRole = careteamDAO.findPatientEngagementRole(communityId);
        String patientEngaRole = String.valueOf(engagementRole.getPatientEngagementRolePK().getRoleId());
        System.out.println("engagementRole.getPatientEngagementRole" + engagementRole.getRoleName());

        // access level
        String accessLevelValue = configurationDAO.getProperty(communityId, "patientuser.accesslevel");
        Long patientAccessLevelID = Long.parseLong(accessLevelValue);

        String userPassword = PasswordUtils.generatePassword();

        String patientUserMessageAppUrl = configurationDAO.getProperty(communityId, "patientuser.webservice.client.url");
        String userToken = loginResult.getToken();

        User user = this.preparePatientUser(personDTO);
        UserCommunityOrganization userCommunityOrganization = new UserCommunityOrganization();
        AccessLevel accessLevel = accessLevelDAO.findAccessLevel(patientAccessLevelID,communityId);
        Long accessLevelId = accessLevel.getAccessLevelPK().getAccessLevelId();
        String gender = personDTO.getGender();
        String credentials = "";
        userCommunityOrganization = PropertyUtils.populateUserUcoData(user, userCommunityOrganization, community, organization, accessLevel,
                null, new Date(System.currentTimeMillis()), null, patientUserActiveStatus, gender, credentials, null);

        user.addUserCommunityOrganization(userCommunityOrganization);

        logger.info("CHECK patientEngagementEnable::" + patientEngagementEnable + ":isActiveProcessFlag:" + isActiveProcessFlag + ":Email:" + personDTO.getPatientEnrollment().getEmailAddress() + ":PatientUserActive:" + personDTO.getPatientUserActive() + "User Id :" + user.getUserId());

        // CA: It seems checkEnableCareteam is always false after GWT careteam panel removal, thus we never reach this code
        if (checkEnableCareteam && !isActiveProcessFlag) {

            List<PatientCommunityEnrollment> patientEnrollments = careteamDAO.findPatientEnrollmentList(personDTO.getPatientEnrollment().getEmailAddress());

            if (patientEnrollments.size() > 1) {
                String duplicateEmailMsg = this.prepareDuplicateEmailAddressMsg(patientEnrollments, communityId);
                throw new PatientEngDuplicateEmailAddressException(duplicateEmailMsg);
            } else {
                //restrict the scope
                String tokenId = this.getToken(communityId);

            patientInfo.setSavePatientUserFlag(true);

            long communityCareteamId = patientInfo.getCommunityCareteamId();
            long patientId = enrollment.getPatientId();
            PatientUserNotification.PatientStatus patientUserNotificationStatus = null;

            if (enrollment.getPatientLoginId() != null) {
                logger.info("WITH PE activate/Deactivate Patient User particular for ::"
                        + personDTO.getPatientUserActive() + ":enrollment.getDirectAddress():"
                        + enrollment.getDirectAddress());

                user.getUserCommunityOrganization(communityId).setDirectAddress(enrollment.getDirectAddress());


                if (personDTO.getPatientUserActive().equalsIgnoreCase("Active")) {
                    logger.info("WITH PE Inactive to Active case enrollment ------");

                    //JIRA 629 - remove properties
                    userService.updatePatientUser(sSOUserDAO, userCommunityOrganizationDAO, userDAO, user, userPassword,
                            communityOrganization, tokenId, organization, patientEngaRole, enrollment.getDirectAddress(), enrollment.getPatientLoginId());
                    logger.info("WITH PE Update OpenAM account completed ------");
                    patientUserNotificationStatus = PatientUserNotification.PatientStatus.Active;

                    enrollment.setPatientUserActive(personDTO.getPatientUserActive());
                    enrollment.setLastPasswordChangeDate(new Date());
                    enrollment.setStatus(patientInfo.getPatientEnrollment().getStatus());
                    careteamDAO.associatedPatientCommunityCareteam(communityCareteamId, patientId, personDTO.getPatientUserActive());
                    careteamDAO.activateDeactivatePatientUserAccount(enrollment);

                    //send mail to patient
                    //JIRA 629 - remove properties
                    securityServiceHelper.sendPasswordEmailToPatient(personDTO.getPatientEnrollment().getEmailAddress(), enrollment.getPatientLoginId(), userPassword, communityId);
                } else {
                    logger.info("WITH PE   Active  to Inactive case enrollment ------");
                    patientUserNotificationStatus = PatientUserNotification.PatientStatus.Inactive;
                    enrollment.setPatientUserActive(personDTO.getPatientUserActive());
                    careteamDAO.associatedPatientCommunityCareteam(communityCareteamId, patientId, personDTO.getPatientUserActive());
                    careteamDAO.activateDeactivatePatientUserAccount(enrollment);
                }

            } else {
                logger.info("WITH PE create new Patient User Account if not exist--::");
                String userPatientName = EnrollmentServiceUtils.getNewPatientUserName(personDTO);
                logger.info("WITH PE userPatientName--::" + userPatientName);
                String patientDirectAddress = this.createDirectAddress(communityId, userPatientName);
                user.getUserCommunityOrganization(communityId).setDirectAddress(patientDirectAddress);
                //james3 account
                this.createJames3User(communityId, userPatientName);
                logger.info("WITH PE createJames3User created successfully ...........................");
                try {
                    //openAM account
                    //JIRA 629 - remove properties
                    userService.createPatientUser(sSOUserDAO, userCommunityOrganizationDAO, userDAO, organization, user, userPassword, communityOrganization, tokenId, patientEngaRole, patientDirectAddress, userPatientName);
                    logger.info(" WITH PE -Enrollment---- create Patient User in openAM if not exist---- ");
                    patientUserNotificationStatus = PatientUserNotification.PatientStatus.Active;
                    careteamDAO.associatedPatientCommunityCareteam(communityCareteamId, patientId, personDTO.getPatientUserActive());

                    //data for update Patient Enrollment
                    updatedPerson.getPatientCommunityEnrollment().setDirectAddress(patientDirectAddress);
                    updatedPerson.getPatientCommunityEnrollment().setRoleID(engagementRole.getPatientEngagementRolePK().getRoleId());
                    updatedPerson.getPatientCommunityEnrollment().setPatientUserActive(personDTO.getPatientUserActive());
                    updatedPerson.setPatientLastPasswordChanged(new Date());
                    updatedPerson.getPatientCommunityEnrollment().setPatientLoginId(userPatientName);
                    this.getPatientEnrollmentDAO(communityId).updatePatient(updatedPerson);

                    //send mail to patient
                    //JIRA 629 - remove properties
                    securityServiceHelper.sendPasswordEmailToPatient(personDTO.getPatientEnrollment().getEmailAddress(), userPatientName, userPassword, communityId);

                } catch (Exception ex) {
                    String message = "Error adding Patient: " + ex.getMessage();
                    logger.log(Level.SEVERE, message, ex);
                    throw new PortalException("Error adding Patient");
                }
            }

                if (msgAppUrlConfFlag) {
                    patientUserNotification.messageAppNotification(user, userToken, patientUserMessageAppUrl,
                            patientUserNotificationStatus, organization, engagementRole, communityId, accessLevelId,
                            gender);
                }
                //make sure the admin token created earlier is logged out
                sSOUserDAO.logout(communityId, tokenId);
            }
        } else {
            logger.info("<<<<<<<<<<<<<<<WITHOUT PE UPDATE PATIENT ENGAGEMENT>>>>>>>>>>>>>>>>>>>>>>>>>:");
            String existStatus = enrollment.getStatus();
            System.out.println("existStatus = " + existStatus);
            String existDirectAddress = enrollment.getDirectAddress();
            String isPatientActive = enrollment.getPatientUserActive();
            System.out.println("no PE update to PE - pe status is = " + newEnrollmentStatus);
            logger.info(
                    "WITHOUT PE existStatus:" + existStatus +
                    ":existDirectAddress:" + existDirectAddress +
                    ":isPatientActive:" + isPatientActive +
                    ":currentStatus:" + newEnrollmentStatus
            );

            // CA: Enrollment status edit is removed from GWT UI, so we are unlikely to reach this code at least from UI
            if (existStatus != null
                    && existStatus.equalsIgnoreCase("Assigned")
                    && existDirectAddress != null && isPatientActive.equalsIgnoreCase("Active")
                    && !newEnrollmentStatus.equalsIgnoreCase("Assigned"))
            {
                logger.info("<<WITHOUT PE Enrollment status change to not Assigned for PE:>>>>>>>");
                enrollment.setPatientUserActive("Inactive");
                enrollment.setStatus(newEnrollmentStatus);
                enrollment.setReasonForInactivation(personDTO.getPatientEnrollment().getReasonForInactivation());
                if (msgAppUrlConfFlag) {
                    user.getUserCommunityOrganization(communityId).setDirectAddress(enrollment.getDirectAddress());
                    patientUserNotification.messageAppNotification(user, userToken, patientUserMessageAppUrl,
                            PatientUserNotification.PatientStatus.Inactive, organization, engagementRole,
                            communityId, accessLevelId, gender);
                }
                careteamDAO.activateDeactivatePatientUserAccount(enrollment);

            } else {
                logger.info("<<WITHOUT PE with out changed pe activity>>>>>>>>>>>>>>>>>>>:");
                updatedPerson.getPatientCommunityEnrollment().setPatientUserActive(patientUserActiveStatus);
                this.getPatientEnrollmentDAO(communityId).updatePatient(updatedPerson);
            }
        }

        Long patientId = this.getPatientEnrollmentDAO(communityId).findPatientId(updatedPerson.getEuid());

        logger.info("Check to update acuity score");

        if (((oldAcuityScore == null) && ((personDTO.getPatientEnrollment().getAcuityScore() != null)))||(oldAcuityScore!=null && personDTO.getPatientEnrollment().getAcuityScore() == null )) {
            addToAcuityScoreHistory(patientEnrollment, patientId, loginResult, "modify");
        } else if ((oldAcuityScore != null) && !(personDTO.getPatientEnrollment().getAcuityScore().equalsIgnoreCase(oldAcuityScore))) {
            addToAcuityScoreHistory(patientEnrollment, patientId, loginResult, "modify");
        }


        String comment = "Updated patient successfully: " + updatedPerson.getFirstName() + " " + updatedPerson.getLastName();
        audit(updatedPerson, comment, AuditType.UPDATED_PATIENT, loginResult, Constants.YES);
    }


    private void checkDuplicate(long communityId, Person updatedPerson, String action) throws DuplicatePatientException, Exception {
        // duplicate patient FIXME - same as add, make a method
        PatientCommunityEnrollment dupPatientEnrollment;
        //String dupPatientEUID = this.getPatientManager(communityId).getDuplicatePatientEuid(updatedPerson.getSimplePerson());
        DuplicateCheckProcessResult dupCheckResult = this.getPatientManager(communityId).checkForDuplicates(updatedPerson.getSimplePerson());

        if(dupCheckResult.getProcessDecision().equals(ProcessDecision.ALREADY_EXISTS)) {
            List<SimplePerson> list = dupCheckResult.getPossibleDuplicates();
            if(list != null && !list.isEmpty()) {
                String dupPatientEUID = list.get(0).getPatientEuid();

                logger.log(Level.INFO, "update patient dupPatientEUID: {0}", dupPatientEUID);

                if (dupPatientEUID != null) {
                    dupPatientEnrollment = this.getPatientEnrollmentDAO(communityId).findPatientEnrollment(dupPatientEUID);
                    logger.log(Level.INFO, "Duplicate patientenrollment {0}", dupPatientEnrollment);
                    Long orgId = dupPatientEnrollment.getOrgId();
                    logger.log(Level.INFO, "Duplicate org id {0}", orgId);
                    String duplicatePatientOrg = organizationDAO.findOrganization(orgId, communityId).getOrganizationCwdName();
                    String dupPatientStatus = dupPatientEnrollment.getStatus();
                    logger.log(Level.INFO, "Duplicate patient org: {0}", duplicatePatientOrg);

                    String message;
                    if (duplicatePatientOrg != null) {
                        message = "Error " + action + " patient. A patient with very similar details already exists in the system. That patient is assigned to: "
                                + duplicatePatientOrg + ".";
                    } else {
                        message = "Error " + action + " patient. A patient with very similar details already exists in the system as " + dupPatientEUID;
                    }

                    throw new DuplicatePatientException(message);
                }
            }
        }
        else if(dupCheckResult.getProcessDecision().equals(ProcessDecision.SIMILAR_EXISTS)) {
            //show the similar window
            throw new DuplicatePatientException("Similar patient exists", dupCheckResult);
        }
    }

    // JIRA - 618 Satyendra
    private void saveOrganizationPatientConsentData(PersonDTO personDTO, String oldPlcs, Person updatedPerson, long organizationId, LoginResult loginResult, long communityId) throws Exception {
        
        String newPlcs = updatedPerson.getPatientCommunityEnrollment().getProgramLevelConsentStatus();
        
        if (!newPlcs.equalsIgnoreCase("Yes") && oldPlcs.equalsIgnoreCase("Yes")) {
            logger.info("\n PLCS is no, so the consent should be revoked from other consented orgs! \n");
            logger.info(" Remove OPC entries for this patient, since consent is revoked !!!!!! ");
            List<OrganizationPatientConsentDTO> orgs = new ArrayList<OrganizationPatientConsentDTO>();
            PatientConsentUtils.saveOrganizationPatientConsentList(organizationPatientConsentDAO, userPatientConsentDAO, orgs, updatedPerson.getPatientCommunityEnrollment());
            // Spira- 5629
            // handle mypatientlist synch
            ServletContext application = EnrollmentServiceImpl.getContext();
            EnrollmentPatientUpdater enrollmentPatientUpdater = (EnrollmentPatientUpdater) application.getAttribute(WebConstants.ENROLLMENT_SERVICE_KEY);
            enrollmentPatientUpdater.synchMyPatientList(personDTO.getPatientEnrollment().getPatientId(), communityId);

            // send patient consent in the background
            // sendPatientConsentToTREAT(personDTO.getPatientEnrollment().getPatientId(), communityId, personDTO.getAdditionalConsentedOrganizations());

        } else {
            logger.info("\n\n personDTO Add Consented Orgs");
            addAllConsentedOrgs(personDTO,updatedPerson.getPatientCommunityEnrollment(),loginResult,communityId);
        }
    }
    
    /**
     * added to manage self assertions
     * @param personDTO
     * @param organizationId
     * @param patientEnrollment
     * @param communityId
     * @throws Exception 
     */
    public void addAndUpdateOrganizationPatientConsent (String additionalConsentedOrgs,long organizationId,
                                              PatientCommunityEnrollment patientEnrollment,long communityId) throws Exception {
        logger.info("personDTO Additional Consented Org Ids=" + additionalConsentedOrgs + " for patient " + patientEnrollment.getPatientId());
        PatientConsentUtils.saveOrganizationPatientConsentList(organizationPatientConsentDAO, userPatientConsentDAO, organizationId,
                    additionalConsentedOrgs, communityOrganizationDAO, patientEnrollment);
        
    }
    
    /**
     * @deprecated 
     * @param personDTO
     * @param organizationId
     * @param patientEnrollment
     * @param communityId
     * @throws Exception 
     */
    public void addOrganizationPatientConsent(PersonDTO personDTO,long organizationId,
                                              PatientCommunityEnrollment patientEnrollment,long communityId) throws Exception {

        //Insert Batch Patient Consent here - DASHBOARD-719
        logger.info("\n\nAdd Bstch - Care Management OrganizationId=" + organizationId + "\n\n");

        if (personDTO.getAdditionalConsentedOrganizations() != null) {
            logger.info("\n\n personDTO Additional Consented Org Ids=" + personDTO.getAdditionalConsentedOrganizations() + "\n\n");
            PatientConsentUtils.saveOrganizationPatientConsentList(organizationPatientConsentDAO, userPatientConsentDAO, organizationId,
                    personDTO.getAdditionalConsentedOrganizations(), communityOrganizationDAO, patientEnrollment);
            // logger.info("\n Sending  to Treat..");
            // send patient consent in the background
            // sendPatientConsentToTREAT(personDTO.getPatientEnrollment().getPatientId(), communityId, personDTO.getAdditionalConsentedOrganizations());
        } else {
            //Spira - 5750 and 5748
            String plcs=patientEnrollment.getProgramLevelConsentStatus();
            boolean disableForBHH = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "manage.consent.withut.enrollment"));

            if (plcs!=null && !plcs.equalsIgnoreCase("Yes") && !disableForBHH) {

                logger.info("\n PLCS is no, only CMO should have the consent \n");
                List<OrganizationPatientConsentDTO> orgs = new ArrayList<OrganizationPatientConsentDTO>();
                PatientConsentUtils.saveOrganizationPatientConsentList(organizationPatientConsentDAO, userPatientConsentDAO, orgs, patientEnrollment);
                // logger.info("\n Sending  to Treat..");
                // send patient consent in the background
                // sendPatientConsentToTREAT(personDTO.getPatientEnrollment().getPatientId(), communityId, personDTO.getAdditionalConsentedOrganizations());

            } else {
                logger.info("\n\n personDTO Add consent to CMO and auto-consented Orgs \n\n");
                PatientConsentUtils.saveOrganizationPatientConsentList(organizationPatientConsentDAO, userPatientConsentDAO, organizationId,
                        personDTO.getAdditionalConsentedOrganizations(), communityOrganizationDAO, patientEnrollment);
                // logger.info("\n Sending  to Treat..");
                // send patient consent in the background
                // sendPatientConsentToTREAT(personDTO.getPatientEnrollment().getPatientId(), communityId, personDTO.getAdditionalConsentedOrganizations());
            }
        }

    }

    public void addAllConsentedOrgs(PersonDTO personDTO,PatientCommunityEnrollment patientenrollment,LoginResult loginResult, long communityId)throws Exception{
        logger.info("In addAllConsentedOrgs");
        // list of existing consented orgs from OPC
        List<OrganizationPatientConsent> consentedList = organizationPatientConsentDAO.findOrganizationPatientConsentEntities(personDTO.getPatientEnrollment().getPatientId(), communityId);
        System.out.println("In addAllConsentedOrgs-- consentedList :"+consentedList.size());
        List<Long> autoConsentOrgIds = communityOrganizationDAO.getAutoConsentOrganizationId(personDTO.getPatientEnrollment().getCommunityId(), 'Y');
        System.out.println("In addAllConsentedOrgs-- autoConsentOrgIds :"+autoConsentOrgIds.size());
        if (autoConsentOrgIds != null) {
            for (long tempOrgId : autoConsentOrgIds) {
                if ("yes".equalsIgnoreCase(personDTO.getPatientEnrollment().getProgramLevelConsentStatus())) {
                    OrganizationPatientConsentDTO organizationPatientConsentDto = PatientConsentUtils.getOrganizationPatientConsent(patientenrollment, tempOrgId);
                    consentedList.add(PatientConsentUtils.convertToEntity(organizationPatientConsentDto));
                }
            }
        }

         //add consent to current user org

        //Spira - 5886 if not a power user
        //current user org is only required in case of self-assertion.
        long currentUserOrgId= loginResult.getOrganizationId();

        if(personDTO.isSelfAssertEligible()){
            OrganizationPatientConsentDTO organizationPatientConsentDto = PatientConsentUtils.getOrganizationPatientConsent(patientenrollment, currentUserOrgId);
            consentedList.add(PatientConsentUtils.convertToEntity(organizationPatientConsentDto));
        }
        PatientConsentUtils.addOrganizationPatientConsentList(organizationPatientConsentDAO, userPatientConsentDAO, consentedList,patientenrollment);
        // logger.info("\n Sending  to Treat..");
        // send patient consent in the background
        // sendPatientConsentToTREAT(personDTO.getPatientEnrollment().getPatientId(), communityId, personDTO.getAdditionalConsentedOrganizations());

    }

    public void updateBatchOrganizationPatientConsent(PersonDTO personDTO, Person updatedPerson,
            long organizationId,long communityId) throws Exception {

        // DASHBOARD-719 Batch Patient Consent
        // Get the current organizations? - No, this should be a fresh list needs to be fresh list
        // for Patient Loader Batch Organization Consent  updates and AutoConsent to Remain in effect
        // and only run with Patient Loader


        logger.info("\n\nUpdate Batch - Care Management organizationId=" + organizationId + "\n\n");
        logger.info("\n\n Update Helper personDTO Additional Consented Org Ids=" + personDTO.getAdditionalConsentedOrganizations() + "\n\n");
        if (personDTO.getAdditionalConsentedOrganizations()!=null) {
            PatientConsentUtils.saveOrganizationPatientConsentList(organizationPatientConsentDAO, userPatientConsentDAO, organizationId,
                    personDTO.getAdditionalConsentedOrganizations(), communityOrganizationDAO, updatedPerson.getPatientCommunityEnrollment());
            // logger.info("\n Sending to Treat..");
            // send patient consent in the background
            // sendPatientConsentToTREAT(personDTO.getPatientEnrollment().getPatientId(), communityId, personDTO.getAdditionalConsentedOrganizations());
        }
    }

    //JIRA 2523
    public DuplicateCheckProcessResult checkForDuplicates(SimplePerson person, Long communityId) throws Exception
    {
        return this.getPatientEnrollmentDAO(communityId).checkForDuplicates(person);
    }

    public PatientLoadResult addPatientHelper(PersonDTO personDTO, long communityCareteamId, LoginResult loginResult) throws Exception, MappingException {
        boolean runInBackgroundNotificationsAndAlerts = true;
        PatientLoadResult patientInfo = addPatientHelper(personDTO, communityCareteamId, loginResult, runInBackgroundNotificationsAndAlerts);

        return patientInfo;
    }

    public PatientLoadResult addPatientHelper(PersonDTO personDTO, long communityCareteamId, LoginResult loginResult, boolean runInBackgroundNotificationsAndAlerts) throws DuplicatePatientException, Exception, MappingException {

        PatientResult patientResult = null;

        logger.info("addPatientHelper: loginResult.getCommunityId() = " + loginResult.getCommunityId());
        logger.info("addPatientHelper: personDTO.getPatientEnrollment().getCommunityId() = " + personDTO.getPatientEnrollment().getCommunityId());

        checkAuthorization(loginResult, personDTO);

        StopWatch tempStopWatch = new StopWatch();
        tempStopWatch.start();

        Long superDuperUserCommunityIdToSet = null;
        long communityId = this.getCommunityIdToSaveWith(loginResult.getCommunityId(), superDuperUserCommunityIdToSet, false);
        long organizationId = 0;
        if ( personDTO.getPatientEnrollment().getOrgId() != 0){
            organizationId = personDTO.getPatientEnrollment().getOrgId();
        }
        else {
            organizationId = loginResult.getOrganizationId();
            personDTO.getPatientEnrollment().setOrgId(organizationId);
        }

        OrganizationCwd organization = organizationDAO.findOrganization(organizationId, communityId);
        Community community = new Community(communityId);

        CommunityOrganization communityOrganization = new CommunityOrganization(community, organization);

        logger.info("addPatientHelper: communityId/orgId = " + communityId + "/" + organizationId);

        tempStopWatch.stop();
        logger.info("[profiling], addPatientHelper: get patient organization, " + (tempStopWatch.getTime() / 1000.0) + ", secs");

        tempStopWatch.reset();
        tempStopWatch.start();

        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        Person thePerson = mapper.map(personDTO, Person.class);

        tempStopWatch.stop();
        logger.info("[profiling], addPatientHelper: dozer bean mapping: PersonDTO to Person, " + (tempStopWatch.getTime() / 1000.0) + ", secs");

        tempStopWatch.reset();
        tempStopWatch.start();

        String email = thePerson.getPatientCommunityEnrollment().getEmailAddress();
        if (email != null) {

            PatientCommunityEnrollment duplicateEnrollment = this.getPatientEnrollmentDAO(communityId).isDuplicateEmail(email);

            if (duplicateEnrollment != null) {
                Long orgId = duplicateEnrollment.getOrgId();
                String patientOrg = organizationDAO.findOrganization(orgId, communityId).getOrganizationCwdName();
                String status = duplicateEnrollment.getStatus();
                String dupEmailMsg = Constants.DUPLICATEE_EMAILMAIL_MSG + patientOrg +  ".";
                throw new DuplicateException(dupEmailMsg);
            }
        }

        PatientEmergencyContactDTO patientEmergencyContactDTO = personDTO.getPatientEnrollment().getPatientEmergencyContact();

        if ((patientEmergencyContactDTO) != null && (patientEmergencyContactDTO.getPatientRelationshipId() != null)) {
            Long patientRelationshipId = patientEmergencyContactDTO.getPatientRelationshipId();
            PatientEmergencyContact patientEmergencyContact = thePerson.getPatientCommunityEnrollment().getPatientEmergencyContact();
            patientEmergencyContact.setRelationship(patientRelationshipId);
        } else {
            // give them a blank emergency contact
            thePerson.getPatientCommunityEnrollment().setPatientEmergencyContact(new PatientEmergencyContact());
        }


        //primary Payer Plan
        if (personDTO.getPatientEnrollment().getPrimaryPayerPlan() != null) {

            PayerPlanDTO primaryPayerPlanDTO = personDTO.getPatientEnrollment().getPrimaryPayerPlan();
            if (primaryPayerPlanDTO != null) {

                thePerson.setPrimaryPayerPlanId(primaryPayerPlanDTO.getId());
                thePerson.getPatientCommunityEnrollment().setPrimaryPayerPlanId(thePerson.getPrimaryPayerPlanId());
            }
        }

        //primary payer class
        if (personDTO.getPatientEnrollment().getPrimaryPayerClass() != null) {
            PayerClassDTO primaryPayerClassDTO = personDTO.getPatientEnrollment().getPrimaryPayerClass();

            if (primaryPayerClassDTO != null) {
                thePerson.setPrimaryPayerClassId(primaryPayerClassDTO.getId());
                thePerson.getPatientCommunityEnrollment().setPrimaryPayerClassId(thePerson.getPrimaryPayerClassId());
            }
        }

        //secondary Payer Plan
        if (personDTO.getPatientEnrollment().getSecondaryPayerPlan() != null) {

            PayerPlanDTO secondaryPayerPlanDTO = personDTO.getPatientEnrollment().getSecondaryPayerPlan();

            if (secondaryPayerPlanDTO != null) {
                thePerson.setSecondaryPayerPlanId(secondaryPayerPlanDTO.getId());
                thePerson.getPatientCommunityEnrollment().setSecondaryPayerPlanId(thePerson.getSecondaryPayerPlanId());
            }
        }

        //secondary Payer class
        if (personDTO.getPatientEnrollment().getSecondaryPayerClass() != null) {

            PayerClassDTO secondaryPayerClassDTO = personDTO.getPatientEnrollment().getSecondaryPayerClass();
            if (secondaryPayerClassDTO != null) {
                thePerson.setSecondaryPayerClassId(secondaryPayerClassDTO.getId());
                thePerson.getPatientCommunityEnrollment().setSecondaryPayerClassId(thePerson.getSecondaryPayerClassId());
            }
        }

        //Program health home

        if (personDTO.getPatientEnrollment().getProgramHealthHome() != null) {
            ProgramHealthHomeDTO programHealthHomeDTO = personDTO.getPatientEnrollment().getProgramHealthHome();
            if (programHealthHomeDTO != null) {
                thePerson.setProgramHealthHomeId(programHealthHomeDTO.getId());
            }
        }

        /*
         * for add, we need not rethrow for SIMILAR_EXISTS, as it is already handled via the pre-add checkForDuplicate process
         * However, its important to let this check happen again for ALREADY_EXISTS - would be more like an optimistic lock exception
         */
        try {
        	checkDuplicate(communityId, thePerson, "adding");
        }
        catch(DuplicatePatientException dpe) {
        	DuplicateCheckProcessResult dcpr = dpe.getDuplicateCheckProcessResult();
        	if(dcpr == null || (dcpr != null && !dcpr.getProcessDecision().equals(ProcessDecision.SIMILAR_EXISTS))) {
        		throw dpe;
        	}
        }
        //SSN Medicaid & Care Id duplicate check
        String checkSSNMedicaidCareMsg = checkDuplicateSSNMedicaidMediCareIDAdd(thePerson, communityId);

        if (checkSSNMedicaidCareMsg != null) {
            throw new DuplicatePatientMedicaidMedicareIDException(checkSSNMedicaidCareMsg);
        }

        PatientCommunityEnrollment patientEnrollment = thePerson.getPatientCommunityEnrollment();
        patientEnrollment.setOrganization(organization);
        patientEnrollment.setOrganizationName(organization.getOrganizationCwdName());
        patientEnrollment.setOrgId(organizationId);
        String programLevelConsentStatus = patientEnrollment.getProgramLevelConsentStatus();

        if (StringUtils.equalsIgnoreCase(programLevelConsentStatus, Constants.YES) || StringUtils.equalsIgnoreCase(programLevelConsentStatus, Constants.NO)) {
            Date consentDate = personDTO.getPatientEnrollment().getConsentDateTime();
            patientEnrollment.setConsentDateTime(consentDate);
            patientEnrollment.setConsentObtainedByUserId(personDTO.getPatientEnrollment().getConsentObtainedByUserId());
        }

        //JIRA 2979
        //Save Consenter
        patientEnrollment.setConsenter(personDTO.getPatientEnrollment().getConsenter());

        // save minor consent data

        if (personDTO.getPatientEnrollment().getMinorConsentDateTime() != null) {

            Date consentDate = personDTO.getPatientEnrollment().getMinorConsentDateTime();
            patientEnrollment.setMinorConsentDateTime(consentDate);
        }
        tempStopWatch.stop();
        logger.info("[profiling], addPatientHelper: prep patient object, " + (tempStopWatch.getTime() / 1000.0) + ", secs");

        tempStopWatch.reset();
        tempStopWatch.start();

        // read properties FIXME - field already exists why use this?
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        boolean patientEngagementEnable = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "patient.engagement.enable", "true"));
        boolean msgAppUrlConfFlag = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "patientuser.webservice.client.url.conf.flag", "true"));
        logger.info("patientEngagementEnable:" + patientEngagementEnable + ":msgAppUrlConfFlag:" + msgAppUrlConfFlag + ":personDTO.getPatientUserActive():" + personDTO.getPatientUserActive());;

        //patient user account creation
        String patientUserActive = personDTO.getPatientUserActive();

        if (patientEngagementEnable && "Active".equalsIgnoreCase(patientUserActive)) {
            // add the patient with Patient User Creation

            List<PatientCommunityEnrollment> patientEnrollments = careteamDAO.findPatientEnrollmentList(thePerson.getPatientCommunityEnrollment().getEmailAddress());
            if (patientEnrollments.size() > 1) {
                String duplicateEmailMsg = prepareDuplicateEmailAddressMsg(patientEnrollments, communityId);
                throw new PatientEngDuplicateEmailAddressException(duplicateEmailMsg);
            } else {
                logger.info("WITH PE Create Patient User Account ----------for add new patient----");

                String userPatientName = EnrollmentServiceUtils.getNewPatientUserName(personDTO);

                String patientDirectAddress = createDirectAddress(communityId, userPatientName);
                logger.info("WITH PE  userPatientName:::" + userPatientName + "--patientDirectAddress ......" + patientDirectAddress);
                thePerson.getPatientCommunityEnrollment().setDirectAddress(patientDirectAddress);

                //james3 account
                createJames3User(communityId, userPatientName);
                logger.info("WITH PE createJames3User created successfully ...........................");

                //role
                PatientEngagementRole engagementRole = careteamDAO.findPatientEngagementRole(communityId);
                thePerson.getPatientCommunityEnrollment().setRoleID(engagementRole.getPatientEngagementRolePK().getRoleId());
                thePerson.getPatientCommunityEnrollment().setPatientUserActive(patientUserActive);
                thePerson.getPatientCommunityEnrollment().setPatientLoginId(userPatientName);

                try {

                    User user = preparePatientUser(personDTO);
                    user.setEmail(personDTO.getPatientEnrollment().getEmailAddress());

                    //role
                    String patientEngaRole = String.valueOf(engagementRole.getPatientEngagementRolePK().getRoleId());

                    UserCommunityOrganization userCommunityOrganization = new UserCommunityOrganization();
                    String accessLevelValue = configurationDAO.getProperty(communityId, "patientuser.accesslevel");
                    Long patientAccessLevelID = Long.parseLong(accessLevelValue);
                    AccessLevel accessLevel = accessLevelDAO.findAccessLevel(patientAccessLevelID,communityId);
                    Long accessLevelId = accessLevel.getAccessLevelPK().getAccessLevelId();
                    String gender = personDTO.getGender();
                    String credentials = "";
                    userCommunityOrganization = PropertyUtils.populateUserUcoData(user, userCommunityOrganization, community, organization,
                            accessLevel, null, new Date(System.currentTimeMillis()),
                            null, null, gender, credentials, null);
                    userCommunityOrganization.setDirectAddress(patientDirectAddress);
                    user.addUserCommunityOrganization(userCommunityOrganization);

                    String userPassword = PasswordUtils.generatePassword();

                    logger.info("WITH PE :patientEngaRole:" + patientEngaRole + ":userPassword:" + userPassword + ":accessLevelValue:" + accessLevelValue);

                    //Get fresh token
                    String tokenId = getToken(communityId);
                    userService.createPatientUser(sSOUserDAO, userCommunityOrganizationDAO, userDAO, organization, user, userPassword, communityOrganization, tokenId, patientEngaRole, patientDirectAddress, userPatientName);
                    logger.info(" WITH PE---- create Patient User Account in openAM---- ");
                    //make sure the admin token created earlier is logged out
                    sSOUserDAO.logout(communityId, tokenId);

                    //url
                    String patientUserMessageAppUrl = configurationDAO.getProperty(communityId, "patientuser.webservice.client.url");

                    //token
                    String userToken = loginResult.getToken();
                    if (msgAppUrlConfFlag) {
                        patientUserNotification.messageAppNotification(user, userToken, patientUserMessageAppUrl,
                                PatientUserNotification.PatientStatus.Active, organization, engagementRole,
                                communityId, accessLevelId, gender);
                    }
                    thePerson.setPatientLastPasswordChanged(new Date());

                    // add patient to the database
                    patientResult = this.getPatientEnrollmentDAO(communityId).addPatient(thePerson, communityId);

                    logger.info(" WITH PE---<<<<<<<<- addPatient>>>>>>>>>>>---- ");

                    long patientID = patientResult.getPatientId();
                    PatientCommunityEnrollment enrollment = this.getPatientEnrollmentDAO(communityId).findPatientEnrollment(patientID);
                    logger.info(" WITH PE---communityCareteamId:---- " + communityCareteamId);

                    //send mail to patient
                    //JIRA 629 - remove properties
                    securityServiceHelper.sendPasswordEmailToPatient(personDTO.getPatientEnrollment().getEmailAddress(), userPatientName, userPassword, communityId);
                    logger.info(" WITH PE -associatedPatientCommunityCareteam- with Patient User Account. ");

                } catch (Exception ex) {
                    String message = "Error adding Patient: " + ex.getMessage();
                    logger.log(Level.SEVERE, message, ex);
                    throw new PortalException("Error adding Patient.");
                }
            }
        } else {

            thePerson.getPatientCommunityEnrollment().setPatientUserActive("None");

            // add patient to the database
            patientResult = this.getPatientEnrollmentDAO(communityId).addPatient(thePerson, communityId);
        }

        tempStopWatch.stop();
        logger.info("[profiling], addPatientHelper: save patient time, " + (tempStopWatch.getTime() / 1000.0) + ", secs");

        long patientId = patientResult.getPatientId();
        String patientEuid = patientResult.getPatientEuid();

        personDTO.getPatientEnrollment().setPatientId(patientId);
        personDTO.setEuid(patientEuid);

        thePerson.setEuid(patientEuid);

        //
        //
        tempStopWatch.reset();
        tempStopWatch.start();

        tempStopWatch.stop();
        logger.info("[profiling], addPatientHelper: save care team time, " + (tempStopWatch.getTime() / 1000.0) + ", secs");

        // audit
        //
        //
        tempStopWatch.reset();
        tempStopWatch.start();

        String comment = "Added patient successfully: " + thePerson.getFirstName() + " " + thePerson.getLastName();
        audit(thePerson, comment, AuditType.ADDED_PATIENT, loginResult, Constants.YES);

        tempStopWatch.stop();
        logger.info("[profiling], addPatientHelper: audit time, " + (tempStopWatch.getTime() / 1000.0) + ", secs");

        // add to patientEnrollmentHistory
        //
        //
        tempStopWatch.reset();
        tempStopWatch.start();

        addToEnrollmentHistory(patientEnrollment, patientId, loginResult, "add");

        tempStopWatch.stop();
        logger.info("[profiling], addPatientHelper: add enrollment history time, " + (tempStopWatch.getTime() / 1000.0) + ", secs");

        tempStopWatch.reset();
        tempStopWatch.start();
        // Add Organization Patient Consent for Patient Loader or AutoConsent options
        addOrganizationPatientConsent(personDTO, organizationId, patientEnrollment,communityId);


        tempStopWatch.stop();
        logger.info("[profiling], addPatientHelper: save organization patient consent titme, " + (tempStopWatch.getTime() / 1000.0) + ", secs");
        addToCareManagementOrganizationhistory(patientEnrollment, patientId, loginResult, "add", null);
        logger.fine("addPatientHelper: addpatient Acuity Score info to acuity score history");
        addToAcuityScoreHistory(patientEnrollment, patientId, loginResult, "add");

        // check for running notifications and alerts in background
        runAddPatientProcess(loginResult, patientEnrollment, patientId,
                communityCareteamId, personDTO, programLevelConsentStatus, thePerson, configurationDAO);

        logger.info("Added patient successfully");

        // set up  the return value
        PatientLoadResult patientInfo = new PatientLoadResult(patientId, communityId);

        return patientInfo;
    }

    public void checkAuthorization(LoginResult loginResult, PersonDTO personDTO) throws NoSoupForYouException {
        boolean superDuperUser = false;
        if (!CanYouSeeIt.thisUserCanSeeThisPatient(loginResult, personDTO, superDuperUser)) {
            throw new NoSoupForYouException("this user has no business editing this patient!!!");
        }
    }


    public void addPatientCareteam(long communityCareteamId, long patientId) throws PortalException {

        try {
            PatientCommunityCareteam patientCommunityCareteam = new PatientCommunityCareteam(communityCareteamId, patientId);

            boolean patientCommunityCareTeamBln = careteamDAO.isSameCareTeamAssigned(communityCareteamId, patientId);
            if (!patientCommunityCareTeamBln) {

                careteamDAO.create(patientCommunityCareteam);
            }


        } catch (Exception exc) {
            final String message = "Error adding patient care team.";
            logger.logp(Level.SEVERE, getClass().getName(), "addPatientCareteam", message, exc);

            throw new PortalException(message);
        }
    }

    public void sendPatientCareteamAssignedAlert(long communityId, PersonDTO personDTO, long communityCareteamId) {
        AlertUtils.sendPatientCareteamAssignedAlert(communityId, careteamDAO, personDTO, communityCareteamId);
    }

    public void sendPatientConsentChangedAlert(long communityId, PersonDTO personDTO, String newProgramLevelConsentStatus) {
        AlertUtils.sendPatientConsentChangedAlert(communityId, personDTO, newProgramLevelConsentStatus);
    }

    protected final List<User> getCareteamMembers(String careteamIdStr) {

        long careteamId = Long.parseLong(careteamIdStr);
        CommunityCareteam communityCareteam = careteamDAO.findCommunityCareteam(careteamId);
        List<UserCommunityCareteam> userCommunityCareteamList = communityCareteam.getUserCommunityCareteamList();

        List<User> users = getUsers(userCommunityCareteamList);

        return users;
    }

    protected final List<User> getUsers(List<UserCommunityCareteam> userCommunityCareteamList) {
        List<User> users = new ArrayList<User>();

        for (UserCommunityCareteam tempUserCommunityCareteam : userCommunityCareteamList) {
            long id = tempUserCommunityCareteam.getUserCommunityCareteamPK().getUserId();

            User tempUser = userDAO.findUser(id);
            users.add(tempUser);
        }

        return users;
    }

    protected final void sendCareteamNotification(String orgId, PersonDTO personDTO, String careteamId, long communityId, String communityOid) {

        GSIHealthNotificationMessage careteamMessage = null;
        String careteamNotificationStatus = null;

        logger.info("Begin: sendCareteamNotificationBHIX");

        Map<String, String> notificationCareteamRoles = getNotificationCareteamRoles(communityId);

        try {
            PatientEnrollmentDTO patientEnrollment = personDTO.getPatientEnrollment();
            String patientId = Long.toString(patientEnrollment.getPatientId());

            // read properties
            ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
            String bhixSendToOid = configurationDAO.getProperty(communityId, "gsihealth.notificationbhix.send.toOID");

            careteamMessage = NotificationUtils.constructMessage(communityId, orgId, patientId, personDTO, NotificationUtils.NotificationReasonCode.PATIENT_CARE_TEAM_ASSIGNMENT, communityOid, bhixSendToOid);

            //  1. get the care team members for this care team
            List<User> users = getCareteamMembers(careteamId);

            //  2. build a list of providers based on care team members
            List<CareProvider> careProviders = NotificationPropertyUtils.buildCareProviders(users, notificationCareteamRoles, communityId);

            //  3. update the notification message with this care team info
            CareTeamAssociation careTeamAssociation = new CareTeamAssociation();
            careTeamAssociation.getCareProvider().addAll(careProviders);
            careteamMessage.getNotificationMessage().getCareTeamNotificationMessage().setCareTeamAssociation(careTeamAssociation);

            careteamNotificationStatus = NotificationUtils.sendNotificationBHIX(communityId, careteamMessage);
        } catch (Throwable exc) {
            final String message = "Error Sending message to WebService for 'Careteam Notification for BHIX' ";
            logger.log(Level.SEVERE, message, exc);
            exc.printStackTrace();
            careteamNotificationStatus = "FAILURE Careteam Notification for BHIX";
        } finally {
            addBHIXNotificationAudit(personDTO, careteamNotificationStatus, careteamMessage, communityId, communityOid);
        }

        logger.info("End: sendCareteamNotificationBHIX");

    }

    public void runAddPatientProcess(LoginResult loginResult, PatientCommunityEnrollment patientEnrollment,
                                     long patientId, long communityCareteamId, PersonDTO personDTO,
                                     String programLevelConsentStatus, Person thePerson, ConfigurationDAO configurationDAO) throws Exception, PortalException {

        try {

            long communityId = loginResult.getCommunityId();

            String careTeamIdNew = null;
            if (communityCareteamId >= 0) {
                // add community care team id
                addPatientCareteam(communityCareteamId, patientId);
                careTeamIdNew = Long.toString(communityCareteamId);

                // send alert for patient assigned to care team
                sendPatientCareteamAssignedAlert(communityId, personDTO, communityCareteamId);
            }

            // send alert for has consent
            boolean hasConsent = StringUtils.equalsIgnoreCase(programLevelConsentStatus, Constants.YES);
            if (hasConsent) {
                sendPatientConsentChangedAlert(communityId, personDTO, programLevelConsentStatus);
            }

            // send alert for minor?
            boolean minorAlertsEnabled = Boolean.parseBoolean(configurationDAO
                    .getProperty(communityId, ConfigurationConstants.MINOR_AGE_TASK_ENABLED, "false"));
            if (minorAlertsEnabled) {
                logger.info("minor alerts enabled, checking patient " + personDTO.getPatientEnrollment().getPatientId()
                        + " with dob of " + personDTO.getDateOfBirth());
                this.sendMinorMessageAlert(communityId, personDTO, configurationDAO);
            } else {
                logger.info("minor alerts not enabled or consent DNE 'yes'");
            }

            // send alert for new patient added
            AlertUtils.sendPatientCreatedAlert(communityId, personDTO);

            // now send notifications
            sendAddNotifications(loginResult, thePerson, personDTO, careTeamIdNew, programLevelConsentStatus);
        } catch (Throwable throwable) {
            logger.logp(Level.SEVERE, getClass().getName(), "runAddPatientProcess", "Error processing", throwable);
        }
    }

    public void sendAddNotifications(LoginResult loginResult, Person thePerson, PersonDTO personDTO, String careTeamIdNew, String programLevelConsentStatus) {
        try {

            StopWatch tempStopWatch = new StopWatch();

            //
            //
            tempStopWatch.start();

            // send pix message for PIX ADD: BHIX Point-to-Point
            sendPixClientMessage(thePerson, loginResult, WebConstants.PIX_ADD);

            tempStopWatch.stop();
            logger.info("[profiling], sendAddNotifications: send pix message for PIX ADD: BHIX Point-to-Point (synchronous) time, " + (tempStopWatch.getTime() / 1000.0) + ", secs");
            String communityOid = loginResult.getCommunityOid();

            // send add notification to Notification Manager
            sendAddNotificationsToNotificationManager(personDTO, loginResult.getCommunityId());

            // send notification to Notification Manager
            String enrollmentStatusOld = null;
            String careTeamIdOld = null;
            processCareteamAndEnrollmentNotificationsToNotificationManager(enrollmentStatusOld, personDTO, careTeamIdNew, careTeamIdOld, loginResult.getCommunityId(), communityOid);

            //
            //
            tempStopWatch.reset();
            tempStopWatch.start();

            // process consent for BHIX: : Point-to-Point
            String oldProgramLevelConsentStatus = "OLD";

            tempStopWatch.stop();
            logger.info("[profiling], sendAddNotifications: process consent for BHIX: : Point-to-Point (synchronous) time, " + (tempStopWatch.getTime() / 1000.0) + ", secs");

        } catch (Exception exc) {
            logger.logp(Level.WARNING, getClass().getName(), "addPatient", "Error sending notifications.", exc);
        }
    }


    public HashMap<String, String> buildConsent(HashMap<String, String> consent, List<OrganizationPatientConsent> organizationPatientConsent, long communityId) {

        for (OrganizationPatientConsent tempOrgPatConsent : organizationPatientConsent) {

            OrganizationPatientConsentPK organizationPatientConsentPK = tempOrgPatConsent.getOrganizationPatientConsentPK();
            long organizationId = organizationPatientConsentPK.getOrganizationId();

            String oid = getOid(organizationId, communityId);
            String consentStatus = tempOrgPatConsent.getConsentStatus();
            consent.put(oid, consentStatus);
        }

        return consent;
    }

    public void processCareteamAndEnrollmentNotificationsToNotificationManager(String oldEnrollmentStatus,
                                                                               PersonDTO personDTO, String careTeamIdNew, String careTeamIdOld, long communityId, String communityOid) {
        long patientId = personDTO.getPatientEnrollment().getPatientId();
        boolean sendCareTeamFlag;

        String newEnrollmentStatus = personDTO.getPatientEnrollment().getStatus();

        logger.info("process careteam/enrollment notifications for patient " + patientId +
                " oldEnrollmentStatus = " + oldEnrollmentStatus +
                "newEnrollmentStatus = " + newEnrollmentStatus + "\n" +
                " old care team: " + careTeamIdOld + " new care team: " + careTeamIdNew);

        boolean sendEnrollmentStatusFlag = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus,
                newEnrollmentStatus);
        sendCareTeamFlag = StringUtils.equals(newEnrollmentStatus, WebConstants.ASSIGNED) ;

        if (sendEnrollmentStatusFlag || sendCareTeamFlag) {
            logger.info("sending Notifications for enrollment status: " +  sendEnrollmentStatusFlag
                    + " and/or careteam: " + sendCareTeamFlag + " for patient " + patientId);
            sendCareteamAndEnrollmentNotificationsToNotificationManager(personDTO,
                    careTeamIdNew, sendEnrollmentStatusFlag, sendCareTeamFlag, communityId, communityOid);
        }
    }

    protected String getOrganizationOID(PersonDTO personDTO, long communityId) {

        PatientEnrollmentDTO patientEnrollment = personDTO.getPatientEnrollment();
        long theOrgId = patientEnrollment.getOrgId();
        OrganizationCwd organization = organizationDAO.findOrganization(theOrgId, communityId);

        String oid = organization.getOid();

        return oid;
    }

    protected String getOid(PersonDTO personDTO, long communityId) {
        String oid;
        PatientEnrollmentDTO patientEnrollment = personDTO.getPatientEnrollment();
        long organizationId = patientEnrollment.getOrgId();
        oid = organizationDAO.findOrganization(organizationId, communityId).getOid();
        return oid;
    }

    protected long getPatientId(PersonDTO personDTO) {
        PatientEnrollmentDTO patientEnrollment = personDTO.getPatientEnrollment();
        long patientId = patientEnrollment.getPatientId();
        return patientId;
    }


    protected final void addBHIXNotificationAudit(PersonDTO personDTO, String notificationStatus, GSIHealthNotificationMessage constructedMessage, long communityId, String communityOid) {

        // read properties
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        String bhixSendToOid = configurationDAO.getProperty(communityId, "gsihealth.notificationbhix.send.toOID");

        NotificationAudit notificationAudit = new NotificationAudit();
        notificationAudit.setCommunityId(communityId);
        notificationAudit.setCreatedOn(new java.util.Date());
        notificationAudit.setFromOrgid(communityOid);
        notificationAudit.setToOrgid(bhixSendToOid);
        notificationAudit.setEnrolledStatus(personDTO.getPatientEnrollment().getStatus());
        notificationAudit.setMessageStatus(notificationStatus);
        notificationAudit.setMessage(personDTO.getPatientEnrollment().getPatientId() + ":   " + personDTO.getLastName());
        notificationAudit.setReasonCode(constructedMessage.getNotificationMetadata().getReasonCode());
        notificationAudit.setPatientId(personDTO.getPatientEnrollment().getPatientId());
        //add Notification in NOTIFICATION_AUDIT Table
        notificationAuditDAO.create(notificationAudit);
    }

    public final void sendCareteamAndEnrollmentNotificationsToNotificationManager(PersonDTO personDTO, String careteamId, boolean sendEnrollmentStatusFlag, boolean sendCareTeamFlag, long communityId, String communityOid) {
        String oid = null;

        logger.info("Begin: sendCareteamAndEnrollmentNotificationsToNotificationManager:  careteamId=" + careteamId + ",  sendEnrollmentStatusFlag=" + sendEnrollmentStatusFlag + ",  sendCareTeamFlag=" + sendCareTeamFlag);
        logger.info("StringUtils.isNotBlank(careteamId): " + StringUtils.isNotBlank(careteamId));

        try {
            oid = getOrganizationOID(personDTO, communityId);

            if (StringUtils.isNotBlank(careteamId) && sendCareTeamFlag) {
                sendCareteamNotification(oid, personDTO, careteamId, communityId, communityOid);
            }

        } catch (Throwable ex) {
            final String message = "Error Sending message to WebService for 'PatientCreate for BHIX' & 'EnrollmentStatusChange for BHIX' ";
            logger.logp(Level.SEVERE, getClass().getName(), "sendUpdateNotificationToBHIX", message, ex);
        }

        logger.info("End: sendCareteamAndEnrollmentNotificationsToNotificationManager");
    }

    public void sendAddNotificationsToNotificationManager(PersonDTO personDTO, long communityId) {
        String oid = null;
        long patientId = 0L;

        logger.info("\n\nBEGIN: sendAddNotificationsToNotificationManager\n\n");

        StopWatch stopWatch = new StopWatch();

        try {
            oid = getOid(personDTO, communityId);
            patientId = getPatientId(personDTO);
            //String orgId = Long.toString(personDTO.getPatientCommunityEnrollment().getOrgId()); //FIXME this should be oid?

            // Send notification for PatientCreate
            GSIHealthNotificationMessage constructedMessage = null;
            String notificationStatus = "";

            // read properties
            ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
            String gsiOid = configurationDAO.getProperty(communityId, "gsihealth.notification.send.fromOID");
            String treatOid = configurationDAO.getProperty(communityId, "gsihealth.notification.send.toOID");

            String patientEuid = personDTO.getEuid();

            // send PATIENT CREATION message
            stopWatch.start();

            try {
                constructedMessage = NotificationUtils.constructMessage(communityId,
                        oid,
                        patientId,
                        personDTO,
                        NotificationUtils.NotificationReasonCode.PATIENT_CREATION,
                        gsiOid,
                        treatOid);
                notificationStatus = NotificationUtils.sendNotification(communityId, constructedMessage);
            } catch (Throwable ex) {
                final String message = "Error Sending message to WebService for 'PatientCreate' ";
                logger.logp(Level.SEVERE, getClass().getName(), "addPatient", message, ex);
                notificationStatus = "FAILURE";
            } finally {
                addNotificationAudit(personDTO, notificationStatus, constructedMessage, communityId);
            }

            stopWatch.stop();
            logger.info("Time for PATIENT_CREATE: " + stopWatch.getTime() / 1000.0 + " secs");


            // send patient consent
            stopWatch.reset();
            stopWatch.start();


            stopWatch.stop();
            logger.info("Time for PATIENT CONSENT: " + stopWatch.getTime() / 1000.0 + " secs");

        } catch (Throwable ex) {
            final String message = "Error Sending message to WebService for 'PatientCreate' & 'EnrollmentStatusChange' ";
            logger.logp(Level.SEVERE, getClass().getName(), "sendAddNotification", message, ex);
        } finally {
            logger.info("END: sendAddNotificationsToNotificationManager");
        }
    }

    /**
     * construct, format and send patient consent
     *
     * @param patientId - patient's local id
     * @param localOid - patient's enrollment oid
     * @param patientEuid
     * @param personDTO
     */
    protected void sendPatientConsentNotificationToNotificationManager(long patientId, String localOid, String patientEuid, PersonDTO personDTO, long communityId) {

        logger.info("Begin: sendPatientConsentNotificationToNotificationManager communityId :" + communityId);

        // Send PATIENT CONSENT
        GSIHealthNotificationMessage constructedMessage = null;
        String docId = UUID.randomUUID().toString();


        List<OrganizationPatientConsent> organizationPatientConsent = organizationPatientConsentDAO.findOrganizationPatientConsentEntities(patientId, communityId);
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        boolean streamlineConsent = Boolean.parseBoolean(
                configurationDAO.getProperty(communityId, ConsentConstants.CONSENT_NOTIFICATION_STREAMLINE, "true"));

        //get consent from organizationPatientConsent table if we're not streamlining consent message
        if (!streamlineConsent) {
            addDenyOrganizations(organizationPatientConsent, communityId);
        }

        HashMap<String, String> consent = new HashMap<String, String>();

        //add consent statuses from OrganizationCwd Patient Consent Table
        consent = buildConsent(consent, organizationPatientConsent, communityId);
        logger.fine("patientId: " + patientId + " localOid: " + localOid + " patientEuid: " + patientEuid);

        String notificationStatus = null;


        // build, send and audit the message -
        try {
            String fromOid = configurationDAO.getProperty(communityId, "gsihealth.notification.send.fromOID");
            String toOid = configurationDAO.getProperty(communityId, "gsihealth.notification.send.toOID");

            constructedMessage = NotificationUtils.constructPatientConsentNotificationMessage(
                    Long.toString(patientId),
                    localOid,
                    consent,
                    docId, fromOid, toOid,
                    communityId);

            logger.info("Sending consent message");
            notificationStatus = NotificationUtils.sendConsentMessage(communityId, constructedMessage);
            logger.info("Send patient consent complete. status=" + notificationStatus);

        } catch (Throwable exc) {
            final String message = "Error Sending message to WebService for 'PatientConsent' ";
            logger.logp(Level.SEVERE, getClass().getName(), "sendUpdateNotification", message, exc);
            notificationStatus = "FAILURE";
        } finally {
            addNotificationAudit(personDTO, notificationStatus, constructedMessage, communityId);
        }

        logger.info("End: sendPatientConsentNotificationToNotificationManager");
    }
    private Map<Long, Map<String, String>> notificationCareteamRolesMap = new HashMap<Long, Map<String, String>>();

    /**
     * Get the notification care team roles
     *
     * @param communityId
     * @return
     */
    private Map<String, String> getNotificationCareteamRoles(long communityId) {

        Map<String, String> notificationCareteamRoles = null;

        // check in our local cache, if not there then get from db
        if (notificationCareteamRolesMap.containsKey(communityId)) {
            notificationCareteamRoles = notificationCareteamRolesMap.get(communityId);
        } else {
            // get from db
            List<NotificationCareteamRole> roles = careteamDAO.getNotificationCareteamRoles(communityId);
            notificationCareteamRoles = NotificationPropertyUtils.buildNoticationCareteamRoles(roles);

            // update the cache
            notificationCareteamRolesMap.put(Long.valueOf(communityId), notificationCareteamRoles);
        }

        return notificationCareteamRoles;
    }

    protected final void sendCareteamNotificationBHIX(List<PersonDTO> patients, String careteamId, long communityId, String communityOid) {

        GSIHealthNotificationMessage careteamMessage = null;
        String careteamNotificationStatus = null;

        logger.info("Begin: sendCareteamNotificationBHIX");

        Map<String, String> notificationCareteamRoles = getNotificationCareteamRoles(communityId);

        try {
            // read properties
            ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
            String bhixSendToOid = configurationDAO.getProperty(communityId, "gsihealth.notificationbhix.send.toOID");

            // 0. Get all the patients assigned to the care team
            careteamMessage = NotificationUtils.constructCareteamAssignmentMessage(organizationDAO, patients, communityOid, bhixSendToOid, communityId);

            // 1. get the care team members for this care team
            List<User> users = getCareteamMembers(careteamId);

            // 2. build a list of providers based on care team members
            List<CareProvider> careProviders = NotificationPropertyUtils.buildCareProviders(users, notificationCareteamRoles, communityId);

            // 3. update the notification message with this care team info
            CareTeamAssociation careTeamAssociation = new CareTeamAssociation();
            careTeamAssociation.getCareProvider().addAll(careProviders);
            careteamMessage.getNotificationMessage().getCareTeamNotificationMessage().setCareTeamAssociation(careTeamAssociation);

            careteamNotificationStatus = NotificationUtils.sendNotificationBHIX(communityId, careteamMessage);
        } catch (Throwable exc) {
            final String message = "Error Sending message to WebService for 'Careteam Notification for BHIX' ";
            logger.log(Level.SEVERE, message, exc);
            exc.printStackTrace();
            careteamNotificationStatus = "FAILURE Careteam Notification for BHIX";
        } finally {
            addBulkBHIXNotificationAudit(patients, careteamNotificationStatus, careteamMessage, communityId, communityOid);
        }

        logger.info("End: sendCareteamNotificationBHIX");

    }

    /**
     * Get a list of orgs that are not in the current list Then add them to the
     * list as DENY
     *
     * @param
     */
    private void addDenyOrganizations(List<OrganizationPatientConsent> organizationPatientConsentList, long communityId) {
        Set<Long> permitOrgIds = new HashSet<Long>();

        if (organizationPatientConsentList == null || organizationPatientConsentList.isEmpty()) {
            return;
        }

        // get a list of all org ids for PERMIT
        for (OrganizationPatientConsent temp : organizationPatientConsentList) {
            if (temp.getConsentStatus().equals(ConsentConstants.CONSENT_PERMIT)) {
                permitOrgIds.add(temp.getOrganizationPatientConsentPK().getOrganizationId());
            }
        }

        OrganizationPatientConsent firstElement = organizationPatientConsentList.get(0);
        long patientId = firstElement.getOrganizationPatientConsentPK().getPatientId();

        // now get list of all org ids
        List<OrganizationCwd> orgs = organizationDAO.findOrganizationEntities(communityId);

        for (OrganizationCwd tempOrg : orgs) {

            // if the temp org id is not listed as PERMIT, then add entry for DENY
            if (!permitOrgIds.contains(tempOrg.getOrgId())) {

                // create an entry for DENY in organizationPatientConsentList
                OrganizationPatientConsent tempDenyOrg = new OrganizationPatientConsent();
                tempDenyOrg.setConsentStatus(ConsentConstants.CONSENT_DENY);

                OrganizationPatientConsentPK tempPK = new OrganizationPatientConsentPK();
                tempPK.setOrganizationId(tempOrg.getOrgId());
                tempPK.setPatientId(patientId);
                tempPK.setCommunityId(communityId);
                tempDenyOrg.setOrganizationPatientConsentPK(tempPK);

                organizationPatientConsentList.add(tempDenyOrg);
            }
        }
    }

    /**
     * TODO - what does this do? personDTOList isn't used
     *
     * @param personDTOList
     * @param notificationStatus
     * @param constructedMessage
     * @param communityId
     */
    protected final void addBulkBHIXNotificationAudit(List<PersonDTO> personDTOList, String notificationStatus, GSIHealthNotificationMessage constructedMessage, long communityId, String communityOid) {

        // read properties
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        String bhixSendToOid = configurationDAO.getProperty("default.community.id", communityId,
                "gsihealth.notificationbhix.send.toOID");

        NotificationAudit notificationAudit = new NotificationAudit();
        notificationAudit.setCommunityId(communityId);
        notificationAudit.setCreatedOn(new java.util.Date());
        notificationAudit.setFromOrgid(communityOid);
        notificationAudit.setToOrgid(bhixSendToOid);
        notificationAudit.setEnrolledStatus("");
        notificationAudit.setMessageStatus(notificationStatus);
        notificationAudit.setMessage("");
        notificationAudit.setReasonCode(constructedMessage.getNotificationMetadata().getReasonCode());

        //add Notification in NOTIFICATION_AUDIT Table
        notificationAuditDAO.create(notificationAudit);
    }

    protected void sendPixClientMessage(Person person, LoginResult theLoginResult, String transactionType) throws Exception {

        logger.info("Begin: sendPixClientMessage");

        long communityId = theLoginResult.getCommunityId();
        UUID uuid = UUID.randomUUID();
        String messageId = uuid.toString();

        MessageBrokerContext brokerContext = MessageBrokerContext.getInstance();


        String communityOid = theLoginResult.getCommunityOid();

        // read properties
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        String endPoint = configurationDAO.getProperty(communityId, "gsihealth.notification.manager.endpoint.url");
        String bhixSendToOid = configurationDAO.getProperty(communityId, "gsihealth.notificationbhix.send.toOID");

        try {
            logger.fine("\n\nSending PIX Client");
            logger.fine("person euid: " + person.getEuid());
            logger.fine("person lastname: " + person.getLastName());
            logger.fine("person localId: " + person.getLocalId());
            logger.fine("person localOrg: " + person.getLocalOrg());
            logger.fine("communityId: " + communityId);
            pixClient.send(person, transactionType, communityId, communityOid, messageId, endPoint, brokerContext.getXdsConnectionFactory(), brokerContext.getXdsQueue());
            logger.info("PIX Client send complete.\n\n");

            String comment = "Sent PIX feed successfully: " + person.getFirstName() + " " + person.getLastName();
            audit(person, comment, AuditType.OUTBOUND_PIX_FEED, theLoginResult, Constants.YES, bhixSendToOid);

        } catch (BHIXDevModeException exc) {
            String message = person.getLastName() + ": Message not sent because of configuration: dashboard.outbound.communication=false or the configuration is missing.";
            logger.warning(message);
            audit(person, message, AuditType.OUTBOUND_PIX_FEED, theLoginResult, Constants.NO, bhixSendToOid);
        } catch (Exception exc) {
            String comment = "PIX feed failed: " + person.getFirstName() + " " + person.getLastName();
            logger.log(Level.WARNING, "PIX feed failed.", exc);
            audit(person, comment, AuditType.OUTBOUND_PIX_FEED, theLoginResult, Constants.NO, bhixSendToOid);
        }

        logger.info("End: sendPixClientMessage");
    }

    protected final void addNotificationAudit(PersonDTO personDTO, String notificationStatus, GSIHealthNotificationMessage constructedMessage, long communityId) {
        logger.info("addNotificationAudit communityId: " + communityId);

        // read properties
        String notificationFromOid = this.getFromOid(communityId);
        String notificationToOid = this.getToOid(communityId);

        NotificationAudit notificationAudit = new NotificationAudit();
        notificationAudit.setCommunityId(communityId);

        notificationAudit.setCreatedOn(new java.util.Date());
        notificationAudit.setFromOrgid(notificationFromOid);
        notificationAudit.setToOrgid(notificationToOid);
        notificationAudit.setEnrolledStatus(personDTO.getPatientEnrollment().getStatus());
        notificationAudit.setMessageStatus(notificationStatus);
        notificationAudit.setMessage("Sending to Notification Manager: " + personDTO.getPatientEnrollment().getPatientId() + ":   " + personDTO.getLastName());

        if (constructedMessage != null) {
            notificationAudit.setReasonCode(constructedMessage.getNotificationMetadata().getReasonCode());
        }

        notificationAudit.setPatientId(personDTO.getPatientEnrollment().getPatientId());
        //add Notification in NOTIFICATION_AUDIT Table
        notificationAuditDAO.create(notificationAudit);
    }

    protected void audit(Person person, String comment, String auditType, LoginResult loginResult, String actionSuccess) {
        audit(person, comment, auditType, loginResult, actionSuccess, null);
    }

    protected void audit(Person person, String comment, String auditType, LoginResult loginResult, String actionSuccess, String sendToOid) {

        long communityId = loginResult.getCommunityId();
        logger.info("EnrollmentServiceHelper Audit CommunityId *###" + communityId);
        try {
            // audit
            Audit audit = AuditUtils.convertPerson(loginResult, person);


            audit.setActionComments(comment);
            audit.setActionType(auditType);
            audit.setActionSuccess(actionSuccess);
            audit.setSentToOid(sendToOid);

            audit.setCommunityId(communityId);
            auditDAO.addAudit(audit);
        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error auditing for " + comment, exc);
        }
    }

    public void sendPatientConsentToTREAT(long patientId, long communityId, String additionalConsentedOrganizations) throws Exception {//change method params

        PatientCommunityEnrollment patientEnrollment = this.getPatientEnrollmentDAO(communityId).findPatientEnrollment(patientId);

        String orgId = Long.toString(patientEnrollment.getOrgId());
        String patientEuid = patientEnrollment.getPatientEuid();
        Person person = new Person(this.getPatientManager(communityId).getPatient(patientEuid));
        person.setPatientCommunityEnrollment(patientEnrollment);

        // convert person to personDTO
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        PersonDTO personDTO = mapper.map(person, PersonDTO.class);
        logger.fine(("I called sentPatientConsentToTreat - sendPatientConsentToTREAT orgId is: ") + orgId);
        sendPatientConsentNotificationToNotificationManager(patientId, orgId, patientEuid, personDTO, communityId);

        /* Not needed because all consented Orgs already sent above
         //JIRA-719 Batch Organization Consent - need to send notifications to TREAT
        // for each additional Consented Organization
        //String additionalConsentedOrganizations = personDTO.getAdditionalConsentedOrganizations();
        if (additionalConsentedOrganizations != null) {
            //test values "3,5"; // Beta Medical,Delta Hospital";
            String[] batchConsentOrgIds = additionalConsentedOrganizations.split(",");
            for (int i = 0; i < batchConsentOrgIds.length; i++) {
                sendPatientConsentNotificationToNotificationManager(patientId, batchConsentOrgIds[i], patientEuid, personDTO, communityId);
                logger.fine((" sendPatientConsentToTREAT additional consented orgId: ") + batchConsentOrgIds[i]);
            }

        } */
    }

    /**
     * I created this for spira 4084 it's just an overload of the method I was
     * too lazy to get rid of
     *
     * @param patientId
     * @param person
     * @throws Exception
     */
    public void sendPatientConsentToTREAT(long patientId, Person person, long communityId) throws Exception {

        PatientCommunityEnrollment patientEnrollment = this.getPatientEnrollmentDAO(communityId).findPatientEnrollment(patientId);

        String orgId = Long.toString(patientEnrollment.getOrgId());
        String patientEuid = patientEnrollment.getPatientEuid();
        //Person person = patientManager.getPatient(patientEuid);
        person.setPatientCommunityEnrollment(patientEnrollment);

        // convert person to personDTO
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        PersonDTO personDTO = mapper.map(person, PersonDTO.class);
        logger.fine(("I called sentPatientConsentToTreat - sendPatientConsentToTREAT orgId is: ") + orgId);
        sendPatientConsentNotificationToNotificationManager(patientId, orgId, patientEuid, personDTO, communityId);
    }

    /**
     * Get the oid from memory cache or db
     *
     * @param organizationId
     * @return
     */
    protected String getOid(long organizationId, long communityId) {
        String oid;
        if (orgOidsCache.containsKey(organizationId)) {
            // get it from cache
            oid = orgOidsCache.get(organizationId);
        } else {
            // get it from db
            oid = organizationDAO.findOrganization(organizationId, communityId).getOid();
            orgOidsCache.put(organizationId, oid);
        }

        return oid;
    }

    private void sendPatientDemogUpdateNotificationToNotificationManager(GSIHealthNotificationMessage constructedMessage, String orgId, long patientId, PersonDTO personDTO, long communityId) {

        logger.info("Begin: sendPatientDemogUpdateNotificationToNotificationManager");

        String notificationStatus = null;

        // Send PATIENT DEMOG UPDATE
        try {
            // read properties
            String from = this.getFromOid(communityId);
            String to = this.getToOid(communityId);
            logger.info("from oid: " + from);
            constructedMessage = NotificationUtils.constructMessage(communityId, orgId, patientId, personDTO, NotificationUtils.NotificationReasonCode.PATIENT_DEMOG_UPDATE, from, to);
            notificationStatus = NotificationUtils.sendNotification(communityId, constructedMessage);
        } catch (Throwable exc) {
            final String message = "Error Sending message to WebService for 'patient demog update notification' ";
            logger.logp(Level.SEVERE, getClass().getName(), "patient demog update", message, exc);
            notificationStatus = "FAILURE";
        } finally {
            addNotificationAudit(personDTO, notificationStatus, constructedMessage, communityId);
        }

        logger.info("End: sendPatientDemogUpdateNotificationToNotificationManager");
    }

    public void sendUpdateNotificationToNotificationManager(PersonDTO personDTO, long communityId) {

        logger.info("Begin: sendUpdateNotificationToNotificationManager");

        // Send notification for PatientDemogUpdate
        String orgId = null;
        long patientId = 0L;

        try {
            patientId = getPatientId(personDTO);
            PatientEnrollmentDTO patientEnrollment = personDTO.getPatientEnrollment();
            orgId = organizationDAO.findOrganization(new Long(patientEnrollment.getOrgId()), communityId).getOid();
            GSIHealthNotificationMessage constructedMessage = null;
            String notificationStatus = "";

            String patientEuid = patientEnrollment.getPatientEuid();

            // send PATIENT_DEMO_UPDATE
            sendPatientDemogUpdateNotificationToNotificationManager(constructedMessage, orgId, patientId, personDTO, communityId);

        } catch (Throwable ex) {
            final String message = "Error Sending message to WebService for 'PatientCreate' & 'EnrollmentStatusChange' ";
            logger.logp(Level.SEVERE, getClass().getName(), "sendUpdateNotification", message, ex);
        }

        logger.info("End: sendUpdateNotificationToNotificationManager");

    }

    public void sendPatientProgramInfoToNotificationManager(PersonDTO personDTO, long communityId) {

        logger.info("Begin: sendPatientProgramInfoToNotificationManager");

        // Send notification for PatientDemogUpdate
        String orgId = null;
        long patientId = 0L;

        try {
            patientId = getPatientId(personDTO);
            PatientEnrollmentDTO patientEnrollment = personDTO.getPatientEnrollment();
            orgId = organizationDAO.findOrganization(new Long(patientEnrollment.getOrgId()), communityId).getOid();
            GSIHealthNotificationMessage constructedMessage = null;

            // send PATIENT_DEMO_UPDATE
            sendPatientDemogUpdateNotificationToNotificationManager(constructedMessage, orgId, patientId, personDTO, communityId);


        } catch (Throwable ex) {
            final String message = "Error Sending message to WebService for 'PatientCreate' & 'EnrollmentStatusChange' ";
            logger.logp(Level.SEVERE, getClass().getName(), "sendUpdateNotification", message, ex);
        }

        logger.info("End: sendPatientProgramInfoToNotificationManager");

    }

    private void addToEnrollmentHistory(PatientCommunityEnrollment enrollment, long patientId, LoginResult loginResult, String action) {


        logger.info("Adding patient's enrollment status info to patient_enrollment_history.CommunityId==" + loginResult.getCommunityId());

        PatientEnrollmentHistory enrollmentHistory = new PatientEnrollmentHistory();

//        PatientEnrollmentHistoryPK patientEnrollmentHistoryPK=new PatientEnrollmentHistoryPK();
//        patientEnrollmentHistoryPK.setCommunityId(loginResult.getCommunityId());
//        enrollmentHistory.setPatientEnrollmentHistoryPK(patientEnrollmentHistoryPK);
        enrollmentHistory.setCommunityId(loginResult.getCommunityId());
        enrollmentHistory.setPatientId(patientId);
        enrollmentHistory.setEnrollmentChangeDate(new Date());
        enrollmentHistory.setEnrollmentStatus(enrollment.getStatus());
        enrollmentHistory.setUserId(loginResult.getUserId());
        enrollmentHistory.setPatientOrgId(enrollment.getOrgId());

        enrollmentHistory.setAction(action);
        enrollmentHistory.setInformationSharingMinor(enrollment.getInformationSharingStatus());
        enrollmentHistory.setMinorConsentObtainedByUserId(enrollment.getMinorConsentObtainedByUserId());

        String reason = enrollment.getReasonForInactivation();
        if (reason != null) {
            enrollmentHistory.setReasonForInactivationId((disenrollmentReasonDAO.getDisenrollmentReason(loginResult.getCommunityId(), reason).getDisenrollmentReasonsPK().getId()));
        }

        patientEnrollmentHistoryDAO.addToEnrollmentHistory(enrollmentHistory);
    }

    private void addToCareManagementOrganizationhistory(PatientCommunityEnrollment enrollment, long patientId,
                                                        LoginResult loginResult, String action, Long oldOrganizationId) {


        logger.info("Adding organization info to care_management_organization_history.CommunityId==" + loginResult.getCommunityId());

        CareManagementOrganizationHistory careManagementOrganizationHistory = new CareManagementOrganizationHistory();

        careManagementOrganizationHistory.setCommunityId(loginResult.getCommunityId());
        careManagementOrganizationHistory.setPatientId(patientId);
        careManagementOrganizationHistory.setOrganizationChangeDate(new Date());

        careManagementOrganizationHistory.setUserId(loginResult.getUserId());
        careManagementOrganizationHistory.setOrgId(enrollment.getOrgId());
        careManagementOrganizationHistory.setPreviousOrganizationId(oldOrganizationId);
        careManagementOrganizationHistory.setAction(action);
        careManagementOrganizationHistory.setType(Constants.CMOH_TYPE_PRIMARY);

        careManagementOrganizationHistoryDAO.addToCareManagementOrganizationhistory(careManagementOrganizationHistory);
    }

    private void addToAcuityScoreHistory(PatientCommunityEnrollment enrollment, long patientId, LoginResult loginResult, String action) {


        logger.info("Adding acuity score info to acuity_score_history.CommunityId==" + loginResult.getCommunityId());

        AcuityScoreHistory acuityScoreHistory = new AcuityScoreHistory();

        acuityScoreHistory.setCommunityId(loginResult.getCommunityId());
        acuityScoreHistory.setPatientId(patientId);
        acuityScoreHistory.setAcuityScoreChangeDate(new Date());

        acuityScoreHistory.setUserId(loginResult.getUserId());
        acuityScoreHistory.setAcuityScore(enrollment.getAcuityScore());

        acuityScoreHistory.setAction(action);

        acuityScoreHistoryDAO.addToAcuityScoreHistory(acuityScoreHistory);
    }

    private void addToConsentHistory(PatientCommunityEnrollment enrollment, long patientId, LoginResult loginResult) {

        logger.info("Adding Consenter info to consenter_history.CommunityId==" + loginResult.getCommunityId());

        ConsentHistory consentHistory = new ConsentHistory();

        consentHistory.setPatientId(patientId);
        consentHistory.setCommunityId(loginResult.getCommunityId());
        consentHistory.setOrgId(enrollment.getOrgId());

        consentHistory.setConsentStatus(enrollment.getProgramLevelConsentStatus());
        consentHistory.setConsentDateTime(enrollment.getConsentDateTime());
        consentHistory.setConsentObtainedByUserId(enrollment.getConsentObtainedByUserId());
        consentHistory.setConsenter(enrollment.getConsenter());
        consentHistory.setLastModifiedDate(new Date()); //Not needed if DB field has  NULL ON UPDATE CURRENT_TIMESTAMP

        consentHistoryDAO.addToConsentHistory(consentHistory);

    }
//    private void addToProgramNameHistory(PatientEnrollment enrollment, long patientId, LoginResult loginResult) {
//        logger.info("Adding patient's program name info to program_name_history.CommunityId="+loginResult.getCommunityId());
//
//        ProgramNameHistory programNameHistory = new ProgramNameHistory();
//        programNameHistory.setCommunityId(loginResult.getCommunityId());
//
//        programNameHistory.setPatientId(patientId);
//        programNameHistory.setCurrentProgramName(enrollment.getProgramName());
//
//        // set program effective date
//        if (enrollment.getProgramNameEffectiveDate() != null) {
//            programNameHistory.setProgramEffectiveDate(enrollment.getProgramNameEffectiveDate());
//        }
//
//        // set program end date
//        if (enrollment.getProgramEndDate() != null) {
//            programNameHistory.setProgramEndDate(enrollment.getProgramEndDate());
//        }
//        programNameHistory.setUserId(loginResult.getUserId());
//        programNameHistory.setAction("Added Patient");
//
//        // adding to DAO
//        programNameHistoryDAO.addToProgramNameHistory(programNameHistory);
//
//    }

    /**
     *
     * @param thePerson
     * @return string
     * @throws Exception
     */
    public String checkDuplicateSSNMedicaidMediCareIDAdd(Person thePerson, long communityId) throws Exception {

        String checkduplicateMsg = null;

        logger.info(":checkDuplicateSSNMedicaidMediCareIDAdd :SSN:" + thePerson.getSsn() + ":PrimaryPayerMedicaidMedicareId:"
                + thePerson.getPatientCommunityEnrollment().getPrimaryPayerMedicaidMedicareId()
                + ":SecondaryPayerMedicaidMedicareId:" + thePerson.getPatientCommunityEnrollment().getSecondaryPayerMedicaidMedicareId());

        boolean ssnCheckFlg = false;
        boolean priMedicaidCheckFlg = false;
        boolean secMedicaidCheckFlg = false;
        String medicaidCareMsg = null;
        long priMedicaidCareID = 0;
        long secMedicaidCareID = 0;

        String ssnMsg = null;
        PatientCommunityEnrollment duplicateEnrollment;

        List<PatientCommunityEnrollment> primaryEnrollmentList = null;
        List<PatientCommunityEnrollment> secEnrollmentList = null;

        if (!StringUtils.isBlank(thePerson.getSsn())) {
            String duplicatePatientEUID = null;
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setSsn(thePerson.getSsn());
            List<SimplePerson> personsList = this.getPatientManager(communityId).findPatient(searchCriteria);

            if (!personsList.isEmpty()) {
                ssnCheckFlg = true;
                duplicatePatientEUID = personsList.get(0).getPatientEuid();
                ssnMsg = getSSNMessage(duplicatePatientEUID, communityId);
            }
        }
        if (thePerson.getPatientCommunityEnrollment().getPrimaryPayerMedicaidMedicareId() != null) {

            String medicaidId = thePerson.getPatientCommunityEnrollment().getPrimaryPayerMedicaidMedicareId();
            priMedicaidCareID = thePerson.getPatientCommunityEnrollment().getPrimaryPayerClassId();

            primaryEnrollmentList = this.getPatientEnrollmentDAO(communityId).getPrimaryPatientMedicaidMedicare(medicaidId, priMedicaidCareID, communityId);

            if (!primaryEnrollmentList.isEmpty()) {
                if (priMedicaidCareID <= 2) {
                    priMedicaidCheckFlg = true;
                }
            }
        }
        if (thePerson.getPatientCommunityEnrollment().getSecondaryPayerMedicaidMedicareId() != null) {

            String medicaidcareId = thePerson.getPatientCommunityEnrollment().getSecondaryPayerMedicaidMedicareId();
            secMedicaidCareID = thePerson.getPatientCommunityEnrollment().getSecondaryPayerClassId();

            secEnrollmentList = this.getPatientEnrollmentDAO(communityId).getSecondaryPatientMedicaidMedicare(medicaidcareId, secMedicaidCareID, communityId);

            if (!secEnrollmentList.isEmpty()) {
                if (secMedicaidCareID <= 2) {
                    secMedicaidCheckFlg = true;
                }
            }
        }

        logger.info("ssnCheckFlg:" + ssnCheckFlg + ":priMedicaidCheckFlg:" + priMedicaidCheckFlg + ":secMedicaidCheckFlg:" + secMedicaidCheckFlg);

        if (ssnCheckFlg && !priMedicaidCheckFlg && !secMedicaidCheckFlg) {
            medicaidCareMsg = ssnMsg;
        }

        if (ssnCheckFlg && priMedicaidCheckFlg && !secMedicaidCheckFlg) {

            duplicateEnrollment = primaryEnrollmentList.get(0);
            if (duplicateEnrollment != null) {
                PatientMedicaidCare pmc = getPatientMedicaidCare(duplicateEnrollment, communityId);
                if (pmc != null) {
                    String medicidCareMsg = getDuplicateSSNMedicaidCare(thePerson.getSsn(), priMedicaidCareID);
                    medicaidCareMsg = getMedicaidMediareMessage(primaryEnrollmentList, medicidCareMsg, pmc.getOrgName(), pmc.getStatus());
                }
            }
        }

        if (ssnCheckFlg && !priMedicaidCheckFlg && secMedicaidCheckFlg) {

            duplicateEnrollment = secEnrollmentList.get(0);
            if (duplicateEnrollment != null) {
                PatientMedicaidCare pmc = getPatientMedicaidCare(duplicateEnrollment, communityId);
                if (pmc != null) {
                    String medicidCareMsg = getDuplicateSSNMedicaidCare(thePerson.getSsn(), secMedicaidCareID);
                    medicaidCareMsg = getMedicaidMediareMessage(secEnrollmentList, medicidCareMsg, pmc.getOrgName(), pmc.getStatus());
                }
            }
        }

        if (!ssnCheckFlg && priMedicaidCheckFlg && !secMedicaidCheckFlg) {

            duplicateEnrollment = primaryEnrollmentList.get(0);
            if (duplicateEnrollment != null) {
                PatientMedicaidCare pmc = getPatientMedicaidCare(duplicateEnrollment, communityId);
                if (pmc != null) {
                    String medicidCareMsg = getDuplicateMedicaidMedicareIDMsg(priMedicaidCareID);
                    medicaidCareMsg = getMedicaidMediareMessage(primaryEnrollmentList, medicidCareMsg, pmc.getOrgName(), pmc.getStatus());
                }
            }
        }
        if (!ssnCheckFlg && !priMedicaidCheckFlg && secMedicaidCheckFlg) {

            duplicateEnrollment = secEnrollmentList.get(0);
            PatientMedicaidCare pmc = getPatientMedicaidCare(duplicateEnrollment, communityId);
            if (pmc != null) {
                String medicidCareMsg = getDuplicateMedicaidMedicareIDMsg(secMedicaidCareID);
                medicaidCareMsg = getMedicaidMediareMessage(secEnrollmentList, medicidCareMsg, pmc.getOrgName(), pmc.getStatus());
            }
        }

        if (!ssnCheckFlg && priMedicaidCheckFlg && secMedicaidCheckFlg) {

            duplicateEnrollment = primaryEnrollmentList.get(0);
            if (duplicateEnrollment != null) {
                PatientMedicaidCare pmc = getPatientMedicaidCare(duplicateEnrollment, communityId);
                if (pmc != null) {
                    String medicidCareMsg = getDuplicateMedicaidMedicareMsg(priMedicaidCareID, secMedicaidCareID);
                    medicaidCareMsg = getMedicaidMediareMessage(primaryEnrollmentList, medicidCareMsg, pmc.getOrgName(), pmc.getStatus());
                }
            }
        }
        if (ssnCheckFlg && priMedicaidCheckFlg && secMedicaidCheckFlg) {
            duplicateEnrollment = primaryEnrollmentList.get(0);
            if (duplicateEnrollment != null) {
                PatientMedicaidCare pmc = getPatientMedicaidCare(duplicateEnrollment, communityId);
                if (pmc != null) {
                    String medicidCareMsg = getDuplicateSSNMedicaidMedicareMsg(thePerson.getSsn(), priMedicaidCareID, secMedicaidCareID);
                    medicaidCareMsg = getMedicaidMediareMessage(primaryEnrollmentList, medicidCareMsg, pmc.getOrgName(), pmc.getStatus());
                }
            }
        }
        checkduplicateMsg = medicaidCareMsg;
        return checkduplicateMsg;
    }

    private String getMedicaidCareValue(long medicaidCareID) {
        String medicaidCareValue = null;
        if (medicaidCareID == 1) {
            medicaidCareValue = Constants.MEDICAIDID;
        }
        if (medicaidCareID == 2) {
            medicaidCareValue = Constants.MEDICAREID;
        }
        return medicaidCareValue;
    }

    /**
     *
     * @param thePerson
     * @return string
     * @throws Exception
     */
    public String checkDuplicateSSNMedicaidMediCareIDUpdate(Person thePerson, boolean checkSsnBln, boolean checkPrimMedicaidcareBln, boolean checkSecMedicaidcareBln, long communityId) throws Exception {

        String checkduplicateMsg = null;

        logger.info(":Update :SSN:" + thePerson.getSsn() + ":PrimaryPayerMedicaidMedicareId:"
                + thePerson.getPatientCommunityEnrollment().getPrimaryPayerMedicaidMedicareId()
                + ":SecondaryPayerMedicaidMedicareId:" + thePerson.getPatientCommunityEnrollment().getSecondaryPayerMedicaidMedicareId());

        logger.info(":Update :checkSsnBln:" + checkSsnBln + ":checkPrimMedicaidcareBln:"
                + checkPrimMedicaidcareBln + ":checkSecMedicaidcareBln:" + checkSecMedicaidcareBln);


        boolean ssnCheckFlg = false;
        boolean priMedicaidCheckFlg = false;
        boolean secMedicaidCheckFlg = false;
        String medicaidCareMsg = null;
        long priMedicaidCareID = 0;
        long secMedicaidCareID = 0;

        String ssnMsg = null;
        PatientCommunityEnrollment duplicateEnrollment;

        List<PatientCommunityEnrollment> primaryEnrollmentList = null;
        List<PatientCommunityEnrollment> secEnrollmentList = null;

        long primaryPayerClassID = 0;
        long secondaryPayerClassID = 0;

        logger.info("UI PrimaryPayerClassId:" + thePerson.getPatientCommunityEnrollment().getPrimaryPayerClassId() + ":SecondaryPayerClassId():" + thePerson.getPatientCommunityEnrollment().getSecondaryPayerClassId());
        if (thePerson.getPatientCommunityEnrollment().getPrimaryPayerClassId() != 0) {
            primaryPayerClassID = thePerson.getPatientCommunityEnrollment().getPrimaryPayerClassId();
        }

        if (thePerson.getPatientCommunityEnrollment().getSecondaryPayerClassId() != 0) {
            secondaryPayerClassID = thePerson.getPatientCommunityEnrollment().getSecondaryPayerClassId();
        }

        PatientCommunityEnrollment enrollment = this.getPatientEnrollmentDAO(communityId).findPatientEnrollment(thePerson.getEuid());
        logger.info("DB Values PrimaryPayerClassId:" + enrollment.getPrimaryPayerClassId() + ":SecondaryPayerClassId:" + enrollment.getSecondaryPayerClassId());

        long primPayerClassId = 0;

        if (enrollment.getPrimaryPayerClassId() != 0) {
            primPayerClassId = enrollment.getPrimaryPayerClassId();
        }

        long secPayerClassId = 0;
        if (enrollment.getSecondaryPayerClassId() != 0) {
            secPayerClassId = enrollment.getSecondaryPayerClassId();
        }

        logger.info("primaryPayerClassID:" + primaryPayerClassID + "primPayerClassId:" + primPayerClassId
                + ":secondaryPayerClassID:" + secondaryPayerClassID + ":secPayerClassId:" + secPayerClassId);

        boolean primPayerClassBln = false;

        if (primaryPayerClassID != primPayerClassId) {
            primPayerClassBln = true;
        }

        boolean secPayerClassBln = false;

        if (secondaryPayerClassID != secPayerClassId) {
            secPayerClassBln = true;
        }



        logger.info("<<< primPayerClassBln:" + primPayerClassBln + ":secPayerClassBln:" + secPayerClassBln);

        if (checkSsnBln) {
            if (!StringUtils.isBlank(thePerson.getSsn())) {
                String duplicatePatientEUID = null;
                SearchCriteria searchCriteria = new SearchCriteria();
                searchCriteria.setSsn(thePerson.getSsn());
                List<SimplePerson> personsList = this.getPatientManager(communityId).findPatient(searchCriteria);

                if (!personsList.isEmpty()) {
                    ssnCheckFlg = true;
                    duplicatePatientEUID = personsList.get(0).getPatientEuid();
                    ssnMsg = getSSNMessage(duplicatePatientEUID, communityId);
                }
            }
        }

        if (checkPrimMedicaidcareBln || primPayerClassBln) {
            logger.info("<<< checkPrimMedicaidcareBln || primPayerClassBln>>>>>>>>>>>>");
            logger.info("4499: checkPrimMedicaidcareBln=" + checkPrimMedicaidcareBln + ", primPayerClassBln=" + primPayerClassBln);

            logger.info("4499: thePerson.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId()=" + thePerson.getPatientCommunityEnrollment().getPrimaryPayerMedicaidMedicareId());

            if (thePerson.getPatientCommunityEnrollment().getPrimaryPayerMedicaidMedicareId() != null) {

                String medicaidId = thePerson.getPatientCommunityEnrollment().getPrimaryPayerMedicaidMedicareId();
                priMedicaidCareID = thePerson.getPatientCommunityEnrollment().getPrimaryPayerClassId();

                primaryEnrollmentList = this.getPatientEnrollmentDAO(communityId).getPrimaryPatientMedicaidMedicare(medicaidId, priMedicaidCareID, communityId);

                if (!primaryEnrollmentList.isEmpty()) {
                    if (priMedicaidCareID <= 2) {
                        priMedicaidCheckFlg = true;
                    }
                }
            }
        }

        logger.info("4499: priMedicaidCheckFlg=" + priMedicaidCheckFlg);


        if (checkSecMedicaidcareBln || secPayerClassBln) {
            logger.info("<<< checkSecMedicaidcareBln || secPayerClassBln>>>>>>>>>>>>");
            if (thePerson.getPatientCommunityEnrollment().getSecondaryPayerMedicaidMedicareId() != null) {

                String medicaidcareId = thePerson.getPatientCommunityEnrollment().getSecondaryPayerMedicaidMedicareId();
                secMedicaidCareID = thePerson.getPatientCommunityEnrollment().getSecondaryPayerClassId();

                secEnrollmentList = this.getPatientEnrollmentDAO(communityId).getSecondaryPatientMedicaidMedicare(medicaidcareId, secMedicaidCareID, communityId);

                if (!secEnrollmentList.isEmpty()) {
                    if (secMedicaidCareID <= 2) {
                        secMedicaidCheckFlg = true;
                    }
                }
            }
        }

        logger.info("4499: Update ssnCheckFlg = " + ssnCheckFlg + ",  priMedicaidCheckFlg=" + priMedicaidCheckFlg + ",  secMedicaidCheckFlg=" + secMedicaidCheckFlg);

        if (ssnCheckFlg && !priMedicaidCheckFlg && !secMedicaidCheckFlg) {
            medicaidCareMsg = ssnMsg;
        }

        if (ssnCheckFlg && priMedicaidCheckFlg && !secMedicaidCheckFlg) {

            duplicateEnrollment = primaryEnrollmentList.get(0);
            if (duplicateEnrollment != null) {
                PatientMedicaidCare pmc = getPatientMedicaidCare(duplicateEnrollment, communityId);
                if (pmc != null) {
                    String medicidCareMsg = getDuplicateSSNMedicaidCare(thePerson.getSsn(), priMedicaidCareID);
                    medicaidCareMsg = getMedicaidMediareMessage(primaryEnrollmentList, medicidCareMsg, pmc.getOrgName(), pmc.getStatus());
                }
            }
        }

        if (ssnCheckFlg && !priMedicaidCheckFlg && secMedicaidCheckFlg) {

            duplicateEnrollment = secEnrollmentList.get(0);
            if (duplicateEnrollment != null) {
                PatientMedicaidCare pmc = getPatientMedicaidCare(duplicateEnrollment, communityId);
                if (pmc != null) {
                    String medicidCareMsg = getDuplicateSSNMedicaidCare(thePerson.getSsn(), secMedicaidCareID);
                    medicaidCareMsg = getMedicaidMediareMessage(secEnrollmentList, medicidCareMsg, pmc.getOrgName(), pmc.getStatus());
                }
            }
        }

        if (!ssnCheckFlg && priMedicaidCheckFlg && !secMedicaidCheckFlg) {

            duplicateEnrollment = primaryEnrollmentList.get(0);
            if (duplicateEnrollment != null) {
                PatientMedicaidCare pmc = getPatientMedicaidCare(duplicateEnrollment, communityId);
                if (pmc != null) {
                    String medicidCareMsg = getDuplicateMedicaidMedicareIDMsg(priMedicaidCareID);
                    medicaidCareMsg = getMedicaidMediareMessage(primaryEnrollmentList, medicidCareMsg, pmc.getOrgName(), pmc.getStatus());
                }
            }
        }
        if (!ssnCheckFlg && !priMedicaidCheckFlg && secMedicaidCheckFlg) {

            duplicateEnrollment = secEnrollmentList.get(0);
            PatientMedicaidCare pmc = getPatientMedicaidCare(duplicateEnrollment, communityId);
            if (pmc != null) {
                String medicidCareMsg = getDuplicateMedicaidMedicareIDMsg(secMedicaidCareID);
                medicaidCareMsg = getMedicaidMediareMessage(secEnrollmentList, medicidCareMsg, pmc.getOrgName(), pmc.getStatus());
            }
        }

        if (!ssnCheckFlg && priMedicaidCheckFlg && secMedicaidCheckFlg) {

            duplicateEnrollment = primaryEnrollmentList.get(0);
            if (duplicateEnrollment != null) {
                PatientMedicaidCare pmc = getPatientMedicaidCare(duplicateEnrollment, communityId);
                if (pmc != null) {
                    String medicidCareMsg = getDuplicateMedicaidMedicareMsg(priMedicaidCareID, secMedicaidCareID);
                    medicaidCareMsg = getMedicaidMediareMessage(primaryEnrollmentList, medicidCareMsg, pmc.getOrgName(), pmc.getStatus());
                }
            }
        }

        if (ssnCheckFlg && priMedicaidCheckFlg && secMedicaidCheckFlg) {
            duplicateEnrollment = primaryEnrollmentList.get(0);
            if (duplicateEnrollment != null) {
                PatientMedicaidCare pmc = getPatientMedicaidCare(duplicateEnrollment, communityId);
                if (pmc != null) {
                    String medicidCareMsg = getDuplicateSSNMedicaidMedicareMsg(thePerson.getSsn(), priMedicaidCareID, secMedicaidCareID);
                    medicaidCareMsg = getMedicaidMediareMessage(primaryEnrollmentList, medicidCareMsg, pmc.getOrgName(), pmc.getStatus());
                }
            }
        }
        checkduplicateMsg = medicaidCareMsg;
        return checkduplicateMsg;
    }

    /**
     * @param ssn
     * @param medicaidCareID
     * @return
     */
    private String getDuplicateSSNMedicaidCare(String ssn, long medicaidCareID) {
        logger.info("getDuplicateSSNMedicaidCare ssn:" + ssn + ":medicaidCareID:" + medicaidCareID);
        String tempValue = null;
        if (StringUtils.isBlank(ssn) && medicaidCareID != 0) {
            tempValue = getMedicaidCareValue(medicaidCareID);
        }
        if (!StringUtils.isBlank(ssn) && medicaidCareID != 0) {
            tempValue = Constants.SSN + " and  " + getMedicaidCareValue(medicaidCareID);
        }
        return tempValue;
    }

    private String getDuplicateMedicaidMedicareIDMsg(long medicaidCareID) {
        logger.info("DuplicateSSNMedicaidMedicare 1:" + ":medicaidCareID:" + medicaidCareID);
        String tempValue = null;
        if (medicaidCareID != 0) {
            tempValue = getMedicaidCareValue(medicaidCareID);
        }
        return tempValue;
    }

    private String getDuplicateMedicaidMedicareMsg(long priMedicaidCareID, long secMedicaidCareID) {
        logger.info("getDuplicateSSNMedicaidMedicareMsg:" + ":priMedicaidCareID:" + priMedicaidCareID + ":secMedicaidCareID:" + secMedicaidCareID);
        String tempValue = null;
        if (priMedicaidCareID != 0 && secMedicaidCareID != 0) {
            tempValue = getMedicaidCareValue(priMedicaidCareID) + " and " + getMedicaidCareValue(secMedicaidCareID);
        }
        return tempValue;
    }

    private String getDuplicateSSNMedicaidMedicareMsg(String ssn, long priMedicaidCareID, long secMedicaidCareID) {
        logger.info("getDuplicateSSNMedicaidMedicareMsg:ssn:" + ssn + ":priMedicaidCareID:" + priMedicaidCareID + ":secMedicaidCareID:" + secMedicaidCareID);
        String tempValue = null;

        if (!StringUtils.isBlank(ssn) && priMedicaidCareID != 0 && secMedicaidCareID != 0) {
            tempValue = Constants.SSN + " , " + getMedicaidCareValue(priMedicaidCareID) + " and " + getMedicaidCareValue(secMedicaidCareID);
        }
        return tempValue;
    }

    private String getMedicaidMediareMessage(List<PatientCommunityEnrollment> enrollmentList, String ssnMedicaidCare, String duplicatePatientOrg, String status) {

        StringBuilder finalMesaage = new StringBuilder();
        if (enrollmentList.size() > Constants.TYPE_ONE) {
            finalMesaage.append(Constants.MSG);
            finalMesaage.append(ssnMedicaidCare);
            finalMesaage.append(Constants.OTHERPATIENTMSG);
            finalMesaage.append(Constants.ADMINMSG);
        } else {
            finalMesaage.append(Constants.MSG);
            finalMesaage.append(ssnMedicaidCare);
            finalMesaage.append(Constants.ONEPATIENTMSG);
            finalMesaage.append(Constants.ASSIGNMSG);
            finalMesaage.append(duplicatePatientOrg);
            finalMesaage.append(".");
        }

        return finalMesaage.toString();
    }

    private String getSSNMessage(String duplicatePatientEUID, long communityId) {
        PatientCommunityEnrollment duplicateEnrollment;
        String ssnMsg = null;
        if (duplicatePatientEUID != null) {
            duplicateEnrollment = this.getPatientEnrollmentDAO(communityId).findPatientEnrollment(duplicatePatientEUID);

            if (duplicateEnrollment != null) {
                Long orgId = duplicateEnrollment.getOrgId();
                String patientOrg = organizationDAO.findOrganization(orgId, communityId).getOrganizationCwdName();
                String status = duplicateEnrollment.getStatus();
                ssnMsg = Constants.SSN_MSG + patientOrg +  ".";

            }
        }
        return ssnMsg;
    }

    private PatientMedicaidCare getPatientMedicaidCare(PatientCommunityEnrollment duplicateEnrollment, long communityId) {

        PatientMedicaidCare pmc = null;

        if (duplicateEnrollment != null) {
            pmc = new PatientMedicaidCare();
            Long orgId = duplicateEnrollment.getOrgId();
            String patientOrg = organizationDAO.findOrganization(orgId, communityId).getOrganizationCwdName();
            pmc.setOrgName(patientOrg);
            pmc.setStatus(duplicateEnrollment.getStatus());
        }
        return pmc;
    }

    //pe
    public String prepareDuplicateEmailAddressMsg(List<PatientCommunityEnrollment> enrollmentList, long communityId) {

        PatientCommunityEnrollment duplicateEnrollment = enrollmentList.get(0);

        Long orgId = duplicateEnrollment.getOrgId();
        String patientOrg = organizationDAO.findOrganization(orgId, communityId).getOrganizationCwdName();
        String status = duplicateEnrollment.getStatus();

        StringBuilder sb = new StringBuilder(Constants.DUPLICATEE_EMAILMAIL_MSG);
        sb.append(patientOrg);
        sb.append(" .");

        return sb.toString();

    }

    public String createDirectAddress(long communityId, String userPatientName) throws Exception {

        // read properties
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        String directDomain = configurationDAO.getProperty(communityId, "hisp.james3.direct.domain");
        String patientOrgName = configurationDAO.getProperty(communityId, "patientuser.org.name");

        String directAddress = HispClientUtils.generatePatientDirectAddress(userPatientName, directDomain, patientOrgName);
        logger.info("directAddress  ------------:" + directAddress);

        //create unique direct address
        List<PatientCommunityEnrollment> patientEnrollments = careteamDAO.findPatientEnrollmentWithDirectAddress(directAddress);
        int num = patientEnrollments.size();
        logger.info("createDirectAddress num :" + num);
        if (num >= 1) {
            directAddress = HispClientUtils.generatePatientUniqueDirectAddress(userPatientName, directDomain, patientOrgName, num);
        }
        logger.info("created directAddress  ------------:" + directAddress);
        return directAddress;
    }

    public void createJames3User(long communityId, String userPatientName) throws Exception {

        // read properties
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        String directDomain = configurationDAO.getProperty(communityId, "hisp.james3.direct.domain");

        String james3DirectAddress = createDirectAddress(communityId, userPatientName);
        String password = configurationDAO.getProperty(communityId, "hisp.james3.direct.defaultuserpassword", "password");
        this.getHispClient(communityId).createAccountForJames3(james3DirectAddress, password, directDomain);

    }

    public User preparePatientUser(PersonDTO personDTO) throws Exception {
        logger.info(" preparePatientUser PatientId :" + personDTO.getPatientEnrollment().getPatientId());
        User user = new User();
        user.setUserId(personDTO.getPatientEnrollment().getPatientId());

        user.setFirstName(personDTO.getFirstName());
        user.setMiddleName(personDTO.getMiddleName());
        user.setLastName(personDTO.getLastName());
        user.setDateOfBirth(personDTO.getDateOfBirth());
        user.setStreetAddress1(personDTO.getStreetAddress1());
        user.setStreetAddress2(personDTO.getStreetAddress2());
        user.setCity(personDTO.getCity());
        user.setState(personDTO.getState());
        user.setZipCode(personDTO.getZipCode());
        user.setTelephoneNumber(personDTO.getTelephone());
        user.setPhoneExt(personDTO.getTelephoneExtension());
        user.setEmail(personDTO.getPatientEnrollment().getEmailAddress());
        user.setFailedAccessAttempts(3);
        user.setCreationDate(new Date());
        user.setLastUpdateDate(new Date());
        user.setEulaAccepted('N');
        user.setEulaDate(new Date());
        user.setEulaId(200);
        user.setPrefix("MR");
        user.setNpi("NO");
        user.setEnabled('Y');
        user.setMustChangePassword('Y');
        user.setDeleted('N');
        user.setReportingRole("Patient");
        user.setCanManagePowerUser(WebConstants.NO);

        return user;
    }

    public String getToken(long communityId) throws Exception {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        String adminUserName = configurationDAO.getProperty(communityId, "sso.server.admin.username");
        String adminPassword = configurationDAO.getProperty(communityId, "sso.server.admin.password");

        //get fresh tokenId for update
        //vpl TODO fix this
        String tokenId = sSOUserDAO.authenticate(communityId, adminUserName, adminPassword);
        tokenId = StringUtils.trimToEmpty(tokenId);
        logger.info("token result from OpenAM:-" + tokenId + "-");

        return tokenId;
    }

    private void setPatientUserIsActive(String status, PersonDTO personDTO) {
        logger.info(">>>setPatientUserIsActive--personDTO---------------------");

        String patientUserActive = personDTO.getPatientUserActive();

        // don't process null data
        if (status == null || patientUserActive == null) {
            return;
        }

        if (status.equalsIgnoreCase("None") && patientUserActive.equalsIgnoreCase("None")) {
            personDTO.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_NONE);
        } else if (status.equalsIgnoreCase("None") && patientUserActive.equalsIgnoreCase("Active")) {
            personDTO.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_ACTIVE);
        } else if (status.equalsIgnoreCase("Active") && patientUserActive.equalsIgnoreCase("Inactive")) {
            personDTO.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_INACTIVE);
        } else if (status.equalsIgnoreCase("Active") && patientUserActive.equalsIgnoreCase("Active")) {
            personDTO.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_ACTIVE);
        } else if (status.equalsIgnoreCase("Inactive") && patientUserActive.equalsIgnoreCase("Active")) {
            personDTO.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_ACTIVE);
        } else if (status.equalsIgnoreCase("Inactive") && patientUserActive.equalsIgnoreCase("Inactive")) {
            personDTO.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_INACTIVE);
        } else if (status.equalsIgnoreCase("Active") && patientUserActive.equalsIgnoreCase("None")) {
            personDTO.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_INACTIVE);
        }
    }

    /**
     * one day we'll add an omnipotent user who can add patients to any
     * community they want, until then, dummy it up
     *
     * @param communityId
     * @param superDuperUserCommunityIdToSet
     * @param superDuperUser
     * @return
     */
    private long getCommunityIdToSaveWith(long communityId, Long superDuperUserCommunityIdToSet, boolean superDuperUser) {
        return communityId;
    }

    protected PatientManager getPatientManager(Long communityId) {
        return communityContexts.get(communityId).getPatientManager();
    }

    protected PatientEnrollmentDAO getPatientEnrollmentDAO(Long communityId) {
        return communityContexts.get(communityId).getPatientEnrollmentDAO();
    }

    protected HispClient getHispClient(Long communityId) {
        return communityContexts.get(communityId).getHispClient();
    }

    /**
     * when a new patient is added, figure out if they need a minor message sent
     * and send it
     *
     * @param communityId
     * @param personDTO
     * @param configurationDAO
     */
    private void sendMinorMessageAlert(Long communityId, PersonDTO personDTO, ConfigurationDAO configurationDAO) {
        long patientId = personDTO.getPatientEnrollment().getPatientId();
        try {
            String minorAgeSchedule = configurationDAO
                    .getProperty(communityId, ConfigurationConstants.MINOR_AGE_MSG_TASK_SCHEDULE);
            int minorAge = Integer.parseInt(
                    configurationDAO.getProperty(communityId, ConfigurationConstants.MINOR_AGE, "-1"));

            MinorMessageProcessing mmp = new MinorMessageProcessing(communityId, "new minor patient", AlertConstants.GSI_CARETEAM_APP);
            mmp.setMinorAge(minorAge);
            Date dob = personDTO.getDateOfBirth();
            if (mmp.isPatientAMinor(dob, minorAge)) {
                logger.fine("patient with dob " + dob + " is a minor, lets send some messages");
                mmp.parseMinorAgeScheduleIntoIntervals(minorAgeSchedule, ConfigurationConstants.MINOR_AGE_REGEX);
                int numberOfDays = mmp.findMinimumNumberOfDaysThisPatientMatches(dob);
                if (numberOfDays > 0) {
                    logger.info("patient " + patientId + " requires a msg sent for " + numberOfDays);
                    String from = this.getFromOid(communityId);
                    AlertUtils.sendPatientAgeOfMajorityAlert(communityId, personDTO, minorAge, numberOfDays, from);
                }
            }
        } catch (Exception ex) {
            logger.warning("minor msg processing for " + patientId + "failed" + ex);
        }
    }

    /**
     * if fromOid hasn't been set, get it from the db
     *
     * @param communityId
     * @return fromOid or fresh one from db
     */
    private String getFromOid(Long communityId) {
        if (StringUtils.isBlank(this.fromOid)) {
            ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
            this.fromOid = configurationDAO
                    .getProperty(communityId, ConfigurationConstants.GSIHEALTH_NOTIFICATION_SEND_FROMOID);
        }
        return this.fromOid;
    }

    /**
     * if toOid hasn't been set, get it from the db
     *
     * @param communityId
     * @return toOid or fresh one from db
     */
    private String getToOid(Long communityId) {
        if (StringUtils.isBlank(this.toOid)) {
            ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
            this.toOid = configurationDAO
                    .getProperty(communityId, ConfigurationConstants.GSIHEALTH_NOTIFICATION_SEND_TOOID);
        }
        return this.toOid;
    }
}
