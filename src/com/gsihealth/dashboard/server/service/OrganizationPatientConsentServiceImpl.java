/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.service.OrganizationPatientConsentService;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.entity.OrganizationCwd;
import com.gsihealth.entity.OrganizationPatientConsent;
import com.gsihealth.entity.PatientCommunityEnrollment;
import com.gsihealth.entity.Community;
import com.gsihealth.entity.CommunityOrganization;
import com.gsihealth.dashboard.entity.dto.OrganizationPatientConsentDTO;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.AuditDAO;
import com.gsihealth.dashboard.server.dao.CommunityCareteamDAO;
import com.gsihealth.dashboard.server.dao.CommunityOrganizationDAO;
import com.gsihealth.dashboard.server.dao.NotificationAuditDAO;
import com.gsihealth.dashboard.server.dao.OrganizationDAO;
import com.gsihealth.dashboard.server.dao.OrganizationPatientConsentDAO;
import com.gsihealth.dashboard.server.dao.PatientEnrollmentDAO;
import com.gsihealth.dashboard.server.dao.UserDAO;
import com.gsihealth.dashboard.server.dao.UserPatientConsentDAO;
import com.gsihealth.dashboard.server.partner.PixClient;
import com.gsihealth.dashboard.server.partner.PixClientImpl;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import com.gsihealth.dashboard.server.service.integration.PatientUserNotification;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

/**
 *
 * @author Chad Darby
 */
public class OrganizationPatientConsentServiceImpl extends BasePortalServiceServlet implements OrganizationPatientConsentService {
    private Logger logger = Logger.getLogger(getClass().getName());
    
    private AuditDAO auditDAO;
    private EnrollmentServiceHelper enrollmentServiceHelper;
    private OrganizationPatientConsentDAO organizationPatientConsentDAO;
    private PatientUserNotification patientUserNotification;
    private PixClient pixClient;
    private UserPatientConsentDAO userPatientConsentDAO;
    protected CommunityCareteamDAO careteamDAO;
    protected NotificationAuditDAO notificationAuditDAO;
    protected CommunityOrganizationDAO communityOrganizationDAO;

    /**
     * Initializes the service with DAOs
     */
    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext application = getServletContext();

        auditDAO = (AuditDAO) application.getAttribute(WebConstants.AUDIT_DAO_KEY);

        careteamDAO = (CommunityCareteamDAO) application.getAttribute(WebConstants.COMMUNITY_CARE_TEAM_DAO_KEY);

        notificationAuditDAO = (NotificationAuditDAO) application.getAttribute(WebConstants.NOTIFICATION_AUDIT_DAO_KEY);

        organizationDAO = (OrganizationDAO) application.getAttribute(WebConstants.ORGANIZATION_DAO_KEY);
        organizationPatientConsentDAO = (OrganizationPatientConsentDAO) application.getAttribute(WebConstants.ORGANIZATION_PATIENT_CONSENT_DAO_KEY);
        userPatientConsentDAO = (UserPatientConsentDAO) application.getAttribute(WebConstants.USER_PATIENT_CONSENT_DAO_KEY);
        communityOrganizationDAO=(CommunityOrganizationDAO)application.getAttribute(WebConstants.COMMUNITY_ORGANIZATION_DAO_KEY);

        //JIRA 629 - remove properties
        pixClient = new PixClientImpl();

        userDAO = (UserDAO) application.getAttribute(WebConstants.USER_DAO_KEY);

        patientUserNotification = (PatientUserNotification) application.getAttribute(WebConstants.PATIENT_USER_NOTIFICATION);

        //JIRA 629 - remove properties
        enrollmentServiceHelper = new EnrollmentServiceHelper(communityContexts,
                auditDAO, careteamDAO, notificationAuditDAO, organizationPatientConsentDAO, organizationDAO,
                userPatientConsentDAO, userDAO,
                patientUserNotification,
                pixClient);

    }

    @Override
    public SearchResults<OrganizationPatientConsentDTO> getOrganizationPatientConsentList(long patientId, int pageNumber, int pageSize,long communityId) throws PortalException {

        try {
            int totalCount = organizationPatientConsentDAO.getOrganizationPatientConsentCount(patientId,communityId);

            List<OrganizationPatientConsent> orgs = organizationPatientConsentDAO.findOrganizationPatientConsentEntities(patientId,communityId, pageNumber, pageSize);

            // convert entity objects to dtos
            List<OrganizationPatientConsentDTO> orgDTOs = convert(orgs);

            // load organization details for the dtos
            loadOrganizationDetails(orgDTOs,communityId);

            // build results
            SearchResults<OrganizationPatientConsentDTO> searchResults = new SearchResults<OrganizationPatientConsentDTO>(totalCount, orgDTOs);

            return searchResults;

        } catch (Exception exc) {
            final String message = "Error getting organization consent.";
            logger.logp(Level.SEVERE, getClass().getName(), "getOrganizationPatientConsentList", message, exc);

            throw new PortalException(message);
        }
    }

    /**
     * Convert entity objects to DTOs
     *
     * @param providers
     * @return
     */
    protected List<OrganizationPatientConsentDTO> convert(List<OrganizationPatientConsent> providers) {

        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        List<OrganizationPatientConsentDTO> orgDTOs = new ArrayList<OrganizationPatientConsentDTO>();

        for (OrganizationPatientConsent tempProvider : providers) {
            OrganizationPatientConsentDTO tempDTO = mapper.map(tempProvider, OrganizationPatientConsentDTO.class);
            orgDTOs.add(tempDTO);
        }

        return orgDTOs;
    }

    private void loadOrganizationDetails(List<OrganizationPatientConsentDTO> orgDTOs,long communityId) {

        for (OrganizationPatientConsentDTO tempDTO : orgDTOs) {

            long orgId = tempDTO.getOrganizationId();

            OrganizationCwd tempOrg = organizationDAO.findOrganization(orgId,communityId);

            tempDTO.setOrganizationName(tempOrg.getOrganizationCwdName());
            tempDTO.setCity(tempOrg.getPhysicalCity());
            tempDTO.setState(tempOrg.getPhysicalAddressState());
        }
    }

    public List<OrganizationPatientConsentDTO> getOrganizationPatientConsentList(long patientId,long communityId) throws PortalException {
        try {
            List<OrganizationPatientConsent> orgConsentList = organizationPatientConsentDAO.findOrganizationPatientConsentEntities(patientId,communityId);

            List<OrganizationCwd> theOrganizations = organizationDAO.findOrganizationEntities(communityId);


            // convert entity objects to dtos
            List<OrganizationPatientConsentDTO> patientOrgDtos = convert(orgConsentList);

            Map<Long, OrganizationPatientConsentDTO> dtoMap = buildMap(patientOrgDtos);

            // buiild full list of DTOs based on orgs
            List<OrganizationPatientConsentDTO> fullOrgDtos = 
                    buildFullList(patientId, theOrganizations, dtoMap, communityId);

            return fullOrgDtos;

        } catch (Exception exc) {
            final String message = "Error getting organization consent.";
            logger.logp(Level.SEVERE, getClass().getName(), "getOrganizationPatientConsentList", message, exc);

            throw new PortalException(message);
        }
    }

    private Map<Long, OrganizationPatientConsentDTO> buildMap(List<OrganizationPatientConsentDTO> orgDTOs) {
        Map<Long, OrganizationPatientConsentDTO> dtoMap = new HashMap<Long, OrganizationPatientConsentDTO>();

        for (OrganizationPatientConsentDTO dto : orgDTOs) {
            dtoMap.put(dto.getOrganizationId(), dto);
        }

        return dtoMap;
    }

    private List<OrganizationPatientConsentDTO> buildFullList(long patientId, 
            List<OrganizationCwd> theOrganizations, Map<Long, OrganizationPatientConsentDTO> dtoMap, Long communityId) {
        List<OrganizationPatientConsentDTO> dtos = new ArrayList<OrganizationPatientConsentDTO>();
//FIXME WTF? why go over this list again????!!!!
        for (OrganizationCwd tempOrg : theOrganizations) {
            long orgId = tempOrg.getOrgId();
            OrganizationPatientConsentDTO tempOrganizationPatientConsentDto = new OrganizationPatientConsentDTO();
            tempOrganizationPatientConsentDto.setOrganizationId(orgId);
            tempOrganizationPatientConsentDto.setOrganizationName(tempOrg.getOrganizationCwdName());
            tempOrganizationPatientConsentDto.setPatientId(patientId);
            tempOrganizationPatientConsentDto.setCity(tempOrg.getPhysicalCity());
            tempOrganizationPatientConsentDto.setState(tempOrg.getPhysicalAddressState());
            tempOrganizationPatientConsentDto.setCommunityId(communityId);
          //  JIRA - 958 to pick the auto consented orgs 
            Community community = new Community(communityId);
            CommunityOrganization commOrg= communityOrganizationDAO.findCommunityOrganization(community, tempOrg);
            tempOrganizationPatientConsentDto.setAutoConsent(commOrg.isAutoConsent());
            
            if (dtoMap.containsKey(orgId)) {
                OrganizationPatientConsentDTO theDto = dtoMap.get(orgId);
                tempOrganizationPatientConsentDto.setConsentStatus(theDto.getConsentStatus());
            }

            dtos.add(tempOrganizationPatientConsentDto);
        }

        return dtos;
    }

    @Override
    public void saveOrganizationPatientConsentList(long patientId, List<OrganizationPatientConsentDTO> dtos,long communityId) throws PortalException {

        try {

            PatientCommunityEnrollment patientEnrollment = this.getPatientEnrollmentDAO(communityId).findPatientEnrollment(patientId);
            PatientConsentUtils.saveOrganizationPatientConsentList(organizationPatientConsentDAO, userPatientConsentDAO, dtos, patientEnrollment);

            // handle mypatientlist synch
            ServletContext application = getServletContext();
            EnrollmentPatientUpdater enrollmentPatientUpdater = (EnrollmentPatientUpdater) application.getAttribute(WebConstants.ENROLLMENT_SERVICE_KEY);
            enrollmentPatientUpdater.synchMyPatientList(patientId, communityId);
                        
            // send patient consent in the background
            enrollmentServiceHelper.sendPatientConsentToTREAT(patientId,communityId, null);

        } catch (Exception exc) {
            final String message = "Error saving organization consent. patient id=" + patientId;
            logger.logp(Level.SEVERE, getClass().getName(), "saveOrganizationPatientConsentList", message, exc);

            throw new PortalException(message);
        }
    }

}
