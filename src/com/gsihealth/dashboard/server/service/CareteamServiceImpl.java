package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.client.service.CareteamService;
import com.gsihealth.dashboard.common.*;
import com.gsihealth.dashboard.common.Constants;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.common.dto.enrollment.PatientFormReferenceData;
import com.gsihealth.dashboard.entity.dto.*;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.common.PatientEngDuplicateEmailAddressException;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.dao.*;
import com.gsihealth.dashboard.server.dao.util.PayerPlanDAOUtils;
import com.gsihealth.dashboard.server.hisp.HispClientUtils;
import com.gsihealth.dashboard.server.jms.CoordinatorBrokerContext;
import com.gsihealth.dashboard.server.jms.JmsConstants;
import com.gsihealth.dashboard.server.jms.JmsUtils;
import com.gsihealth.dashboard.server.partner.PixClient;
import com.gsihealth.dashboard.server.partner.PixClientImpl;
import com.gsihealth.dashboard.server.service.integration.PatientUserNotification;
import com.gsihealth.dashboard.server.service.integration.UserService;
import com.gsihealth.dashboard.server.util.*;
import com.gsihealth.entity.*;
import com.gsihealth.entity.insurance.PayerClass;
import com.gsihealth.entity.insurance.PayerPlan;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.dozer.MappingException;
import org.gsihealth.wsn.GSIHealthNotificationMessage;

import javax.jms.*;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chad Darby
 */
public class CareteamServiceImpl extends BasePatientService implements CareteamService, CareTeamPatientUpdater {

    private Logger logger = Logger.getLogger(getClass().getName());
    private DisenrollmentReasonDAO disenrollmentReasonDAO;
    private EnrollmentServiceHelper enrollmentServiceHelper;
    private List<String> carebookPatientSearchStatuses;
    private List<String> statuses;
    private PayerPlanDAO payerPlanDAO;
    private ConsenterDAO consenterDAO;
    private PixClient pixClient;
    private ProgramNameHistoryDAO programNameHistoryDAO;
    private SecurityServiceHelper securityServiceHelper;
    private AcuityScoreHistoryDAO acuityScoreHistoryDAO;
    private static final String MINIOR_CONSTANT = "Minor";
    private PatientEnrollmentDAO patientEnrollmentDAO;

    /**
     * Initializes the service with DAOs
     */
    @Override
    public void init() throws ServletException {
        logger.info("loading careteamServiceImpl");
        super.init();

        ServletContext application = getServletContext();
        payerPlanDAO = (PayerPlanDAO) application.getAttribute(WebConstants.PAYER_PLAN_DAO_KEY);
        disenrollmentReasonDAO = (DisenrollmentReasonDAO) application.getAttribute(WebConstants.DISENROLLMENT_DAO_DEY);

        programNameHistoryDAO = (ProgramNameHistoryDAO) application.getAttribute(WebConstants.PROGRAM_NAME_HISTORY_DAO_KEY);
        consenterDAO = (ConsenterDAO) application.getAttribute(WebConstants.CONSENTER_DAO_KEY);

        setSSOUserDAO((SSOUserDAO) application.getAttribute(WebConstants.SSO_USER_DAO_KEY));

        setRoleDAO((RoleDAO) application.getAttribute(WebConstants.ROLE_DAO_KEY));
        setCommunityDAO((CommunityDAO) application.getAttribute(WebConstants.COMMUNITY_DAO_KEY));

        setUserCommunityOrganizationDAO((UserCommunityOrganizationDAO) application.getAttribute(WebConstants.USER_COMMUNITY_ORGANIZATION_DAO_KEY));

        accessLevelDAO = (AccessLevelDAO) application.getAttribute(WebConstants.ACCESS_LEVEL_DAO_KEY);

        sSOUserDAO = (SSOUserDAO) application.getAttribute(WebConstants.SSO_USER_DAO_KEY);
        userService = (UserService) application.getAttribute(WebConstants.USER_SERVICE_KEY);
        userCommunityOrganizationDAO = (UserCommunityOrganizationDAO) application.getAttribute(WebConstants.USER_COMMUNITY_ORGANIZATION_DAO_KEY);
        patientUserNotification = (PatientUserNotification) application.getAttribute(WebConstants.PATIENT_USER_NOTIFICATION);
        securityServiceHelper = new SecurityServiceHelper();
        personDTOComparator = new PersonDTOComparator();
        communityOrganizationDAO =(CommunityOrganizationDAO)application.getAttribute(WebConstants.COMMUNITY_ORGANIZATION_DAO_KEY);
        statuses = new ArrayList<String>();
        statuses.add("Enrolled");
        statuses.add("Assigned");

        carebookPatientSearchStatuses = new ArrayList<String>();
        carebookPatientSearchStatuses.add("Enrolled");
        carebookPatientSearchStatuses.add("Assigned");
        carebookPatientSearchStatuses.add("Pending");
        carebookPatientSearchStatuses.add("Inactive");

        //JIRA 629 - remove properties
        pixClient = new PixClientImpl();

        //JIRA 629 - remove properties
        enrollmentServiceHelper = new EnrollmentServiceHelper(communityContexts,
                auditDAO, careteamDAO, notificationAuditDAO, organizationPatientConsentDAO, organizationDAO,
                userPatientConsentDAO, userDAO, patientUserNotification, pixClient, communityOrganizationDAO);
        patientEnrollmentDAO = (PatientEnrollmentDAO) application.getAttribute(
                WebConstants.PATIENT_ENROLLMENT_DAO_KEY);
        application.setAttribute(WebConstants.CARETEAM_SERVICE_KEY, this);
        // config the broker context
        CoordinatorBrokerContext coordinatorBrokerContext = CoordinatorBrokerContext.getInstance();
        coordinatorBrokerContext.setCareTeamPatientUpdater(this);
        logger.info("finished loading careteamServiceImpl");
    }

    protected List<PersonDTO> filterEnrolledPatientsWithDemographics(long communityId, long userId,
            SearchCriteria searchCriteria) throws MappingException, NumberFormatException, Exception {

        logger.info(" filterEnrolledPatientsWithDemographics CareteamServiceImpl not poweruser  :");

        List<PersonDTO> searchResults = new ArrayList<>();
        int minorAge = Integer.parseInt(configurationDAO.getProperty(communityId, "minor.age", "18"));

        // setup
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        List<SimplePerson> results = getPatientManager(communityId).findDashboardPatient(searchCriteria);

        // get the patient enrollment
        for (SimplePerson sp : results) {
            try {
                Person tempPerson = this.processEnrolledPatientWDemog(sp, searchCriteria, communityId, userId, false);
                if (tempPerson != null) {
                    PersonDTO personDto = mapper.map(tempPerson, PersonDTO.class);
                    setPatientRelationship(communityId, tempPerson, personDto);
                    setProgramHealthHome(tempPerson, personDto);
                    setMinor(tempPerson, personDto, minorAge);
                    searchResults.add(personDto);
                }

            } catch (Exception exc) {
                logger.warning("could not process patient, not including in search results" + exc);
            }
        }

        return searchResults;
    }

    /**
     *
     * @param sp a simplePerson
     * @param searchCriteria
     * @param communityId
     * @param userId
     * @param isAPowerUser true if this is a power user
     * @return person populated with pce and sp
     */
    private Person processEnrolledPatientWDemog(SimplePerson sp, SearchCriteria searchCriteria, long communityId,
            long userId, boolean isAPowerUser) throws Exception {
        String euid = sp.getPatientEuid();
        Person person = new Person(sp);

        PatientCommunityEnrollment pce = getPatientEnrollmentDAO(communityId)
                .findPatientEnrollment(euid, communityId);
        // get the patient id
        long patientId;
        if (pce == null) {
            // skip this patient since patient enrollment is null
            logger.fine("SKIP:  patient enrollment is null. euid=" + euid);
            return null;
        }
        logger.fine("Care Team PRIMARY MedicaidMedicareId:" + pce.getPrimaryPayerMedicaidMedicareId()
                + ":SECONDARY  MedicaidMedicareId:" + pce.getSecondaryPayerMedicaidMedicareId());
        patientId = pce.getPatientId();

        // patient id
        logger.fine("Patient id: " + patientId);

        String searchPatientId = searchCriteria.getPatientID();
        if (StringUtils.isNotBlank(searchPatientId) && StringUtils.isNumeric(searchPatientId)) {
            if (patientId != Integer.parseInt(searchPatientId)) {
                logger.info("SKIP:  patient ids do not match. patientId=" + patientId + ", searchPatientId=" + searchPatientId);
                return null;
            }
        }
        // not a poweruser, doesn't have consent
        if (!isAPowerUser) {
            boolean hasConsent = userPatientConsentDAO.hasConsent(userId, patientId, communityId);
            if (!hasConsent) {
                logger.fine("SKIP:  user does not have consent. euid=" + euid + ", "
                        + "userId=" + userId + ", patientId=" + patientId);
                return null;
            }
        }

        // do not include if invalid status
        String tempStatus = pce.getStatus();
        if (!searchCriteria.isCarebookPatientSearch() && isInvalidStatus(statuses, tempStatus)) {
            logger.fine("SKIP:  invalid status. euid=" + euid + ", invalid status=" + tempStatus);
            return null;
        }

        // check if patient id is in patient_community_careteam
        boolean patientHasACareteam = careteamDAO.doesPatientCommunityCareteamExist(patientId);

        String careteamIdSearchCriteria = searchCriteria.getCareteamId();

        // if so, then get care team id
        // - get care team name
        if (patientHasACareteam) {
            CommunityCareteam communityCareteam = careteamDAO.findCommunityCareteamForPatient(patientId);
            long careteamId = communityCareteam.getCommunityCareteamId();
            String careteamName = communityCareteam.getName();
            person.setCareteam(careteamName);

            // filter on careteam
            if (StringUtils.isNotBlank(careteamIdSearchCriteria)) {
                long searchCareteamId = Long.parseLong(careteamIdSearchCriteria);
                if (searchCareteamId != careteamId) {
                    logger.fine("SKIP:  care team does not match. euid=" + euid + ",  careteamId=" + careteamId);
                    return null;
                }
            }
        }
        // skip if patient doesn't have a care team and we searching by a careteam
        if (!patientHasACareteam && StringUtils.isNotBlank(careteamIdSearchCriteria)) {
            logger.fine("SKIP:  patient doesn't have a care team and we're searching by careteam. euid=" + euid + ", .");
            return null;
        }

        String progamLevelConsentStatus = searchCriteria.getProgramLevelConsentStatus();
        if (StringUtils.isNotBlank(progamLevelConsentStatus)) {
            if (!progamLevelConsentStatus.equals(pce.getProgramLevelConsentStatus())) {
                logger.fine("SKIP:  program level does not match. euid=" + euid + ",  "
                        + "tempPatientEnrollment.getProgramLevelConsentStatus()=" + pce.getProgramLevelConsentStatus());
                return null;
            }
        }

        String enrollmentStatus = searchCriteria.getEnrollmentStatus();
        if (StringUtils.isNotBlank(enrollmentStatus)) {
            if (!enrollmentStatus.equals(pce.getStatus())) {
                logger.fine("SKIP:  enrollment status does not match. euid=" + euid + ",  "
                        + "tempPatientEnrollment.getStatus()=" + pce.getStatus());
                return null;
            }
        }

        long orgId = searchCriteria.getOrganizationId();
        if (orgId >= 0) {
            if (orgId != pce.getOrgId()) {
                logger.fine("SKIP:  org id does not match. euid=" + euid + ",  "
                        + "tempPatientEnrollment.getOrgId()=" + pce.getOrgId());
                return null;
            }
        }

        // get organization name
        long patientOrgId = pce.getOrgId();
        String orgName = organizationDAO.getOrganizationName(patientOrgId);
        pce.setOrganizationName(orgName);
        person.setPatientCommunityEnrollment(pce);
        return person;
    }

    @Override
    public void setMinor(Person person, PersonDTO personDto, int minorAge) {
        if (isMinor(person.getDateOfBirth(), minorAge)) {
            personDto.setMinor(MINIOR_CONSTANT);
        } else {
            personDto.setMinor("");
        }
    }

    public static boolean isMinor(Date dateOfBirth, int minorAge) {
        Date dob = dateOfBirth;
        boolean isMinor = false;
        if(dob == null){
            return isMinor;
        }

        double age = getAge(dob);

        if (age < minorAge) {
            isMinor = true;
        } else {
            isMinor = false;
        }

        return isMinor;
    }

    public static double getAge(Date dateOfBirth) {

        Date dob = dateOfBirth;
        double age;
        long ageInMillis = new Date().getTime() - dob.getTime();

        age = ageInMillis / (365.25 * 24 * 60 * 60 * 1000);
        return age;

    }

    protected List<PersonDTO> filterEnrolledPatientsWithDemographics(long communityId, SearchCriteria searchCriteria) throws MappingException, NumberFormatException, Exception {

        logger.info(" power user filterEnrolledPatientsWithDemographics CareteamServiceImpl  :" + communityId);

        List<PersonDTO> searchResults = new ArrayList<>();
        int minorAge = Integer.parseInt(configurationDAO.getProperty(communityId, "minor.age", "18"));

        // setup
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        List<SimplePerson> results = this.getPatientManager(communityId).findDashboardPatient(searchCriteria);

        for (SimplePerson sp : results) {
            try {
                String euid = sp.getPatientEuid();
                logger.info(" euid :" + euid + ":LASTNAME:" + sp.getLastName());
                Person tempPerson = this.processEnrolledPatientWDemog(sp, searchCriteria, communityId, -1, true);
                PersonDTO personDto = mapper.map(tempPerson, PersonDTO.class);
                
                setPatientRelationship(communityId, tempPerson, personDto);
                setProgramHealthHome(tempPerson, personDto);
                setMinor(tempPerson, personDto, minorAge);
                searchResults.add(personDto);
            } catch (Exception exc) {
                logger.warning("could not process patient, not including in search results" + exc);
            }
        }

        return searchResults;
    }

    protected List<PersonDTO> getPatientsForCareteam(long communityCareteamId, Long communityId) throws Exception {
        // get a list of patients associated with careteam
        List<String> euids = careteamDAO.getPatientEuidsAssignedToCareteam(communityCareteamId);
        List<Person> patients = getPatients(euids, communityId);
        List<PersonDTO> personDTOlist = convert(patients, communityId);
        return personDTOlist;
    }

    protected PersonDTO getPersonDTO(String euid, Long communityId) throws Exception {
        Person thePerson = getPerson(euid, communityId);
        PersonDTO personDTO = convert(thePerson, communityId);
        return personDTO;
    }


    protected void saveCareteamBackgroundProcess(final long communityCareteamId, final String careteamName, long communityId) {

        // process bulk BHIX update notification
        boolean hasPatientsAssignedToCareteam = careteamDAO.hasPatientsAssignedToCareteam(communityCareteamId);

        if (hasPatientsAssignedToCareteam) {
            processBulkCareteamNotificationToBHIX(communityCareteamId, communityId);
        }

        // alert notification for careteam changed.
        sendCareteamChangedAlert(communityId, careteamName, hasPatientsAssignedToCareteam);
    }

    protected void saveNewCareteamBackgroundProcess(long communityId, final String careteamName) {
        // alert notification for careteam created.
        sendCareteamCreatedAlert(communityId, careteamName);
    }

    protected void savePatientInfo(Person person, PersonDTO personDTO, LoginResult loginResult, PatientCommunityEnrollment patientEnrollment,
            long communityCareteamId, PatientInfo patientInfo, String programLevelConsentStatus,
            String oldCareteamName) throws Exception {

        long communityId = loginResult.getCommunityId();

        //
        // send notification to BHIX
        //
        String euid = patientEnrollment.getPatientEuid();

        String careteamIdStr = null;
        if (communityCareteamId > 0) {
            careteamIdStr = Long.toString(communityCareteamId);
        }

        String oldEnrollmentStatus = patientInfo.getOldEnrollmentStatus();
        String newEnrollmentStatus = patientEnrollment.getStatus();

        boolean sendCareTeamFlag = CareteamUtils.computeSendCareteamFlagForBHIX(patientInfo, newEnrollmentStatus);
        logger.info("\n\n\nSend Care Team Flag for bhix = " + sendCareTeamFlag);

        boolean sendEnrollmentStatusFlag = CareteamUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus, patientInfo);
        logger.info("Send Enrollment status Flag for bhix = " + sendEnrollmentStatusFlag + "\n\n\n");

        // handle BHIX
        person.setPatientCommunityEnrollment(patientEnrollment);
        long patientId = patientEnrollment.getPatientId();
        person.setLocalId(Long.toString(patientId));
        sendNotificatonToBHIX(person, patientEnrollment, euid, careteamIdStr, sendEnrollmentStatusFlag,
                sendCareTeamFlag, loginResult.getCommunityId(), loginResult);

        // handle consent for TREAT
        enrollmentServiceHelper.sendPatientConsentToTREAT(patientId, person, loginResult.getCommunityId());

        //PersonDTO personDTO = getPersonDTO(euid);
        personDTO.setCareteam(oldCareteamName);

        // send patient alert
        if (communityCareteamId > 0) {
            // send alert for patient assigned to care team
            enrollmentServiceHelper.sendPatientCareteamAssignedAlert(communityId, personDTO, communityCareteamId);
        }

        // send alert for patient care team removed
        if (oldEnrollmentStatus.equalsIgnoreCase("ASSIGNED") && !oldEnrollmentStatus.equalsIgnoreCase(newEnrollmentStatus)) {
            AlertUtils.sendPatientCareteamRemovedAlert(communityId, personDTO, oldCareteamName);
        }

        // send alert for consent status changed
        String oldProgramLevelConsentStatus = patientInfo.getOldProgramLevelConsentStatus();
        boolean hasConsentChanged = ConsentUtils.hasConsentChangedForBHIX(oldProgramLevelConsentStatus, programLevelConsentStatus);
        if (hasConsentChanged) {
            enrollmentServiceHelper.sendPatientConsentChangedAlert(communityId, personDTO, programLevelConsentStatus);
        }

        // send alert for patient enrollment status changed.
        boolean hasEnrollmentStatusChanged = !StringUtils.equalsIgnoreCase(oldEnrollmentStatus, newEnrollmentStatus);
        logger.info("old enrollment status is: " + oldEnrollmentStatus + " new enr status is: " + newEnrollmentStatus
                + " for patient: " + patientId);
        logger.info("send alert for enrollment status change?: " + hasEnrollmentStatusChanged);
        if (hasEnrollmentStatusChanged) {
            logger.info("sending alert for enrollment status change for patient " + patientId);
            AlertUtils.sendPatientEnrolledStatusChangedAlert(communityId, personDTO, newEnrollmentStatus);
        }
    }

    private boolean isInvalidStatus(List<String> statuses, String tempStatus) {
        return !statuses.contains(tempStatus);
    }

    @Override
    public LinkedHashMap<String, String> getCareteamNames(Long communityId) throws PortalException {

        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
        try {
            List<CommunityCareteam> careteams = careteamDAO.findCommunityCareteamEntities(communityId);

            for (CommunityCareteam temp : careteams) {
                String id = Long.toString(temp.getCommunityCareteamId());
                String name = temp.getName();
                data.put(id, name);
            }
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error loading care teams.", exc);
            throw new PortalException(exc.getMessage());
        }

        return data;
    }

    @Override
    public CommunityCareteamDTO getCareteamForPatient(long patientId) throws PortalException {

        CommunityCareteamDTO communityCareteamDTO = null;

        try {
            boolean exists = careteamDAO.doesPatientCommunityCareteamExist(patientId);

            // if so, then get care team id
            // - get care team name
            if (exists) {
                CommunityCareteam communityCareteam = careteamDAO.findCommunityCareteamForPatient(patientId);

                Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
                communityCareteamDTO = mapper.map(communityCareteam, CommunityCareteamDTO.class);
            }

            return communityCareteamDTO;

        } catch (Exception exc) {
            final String message = "Error retrieving community care team for patient. patientId: " + patientId;
            logger.logp(Level.SEVERE, getClass().getName(), "getCareteamForPatient", message, exc);

            throw new PortalException("Error retrieving community care team for patient");
        }
    }

    @Override
    public void savePatientInfo(PersonDTO personDTO, PatientInfo patientInfo, boolean checkEnableCareteam, boolean checkPatientUserActive, long communityId) throws PortalException {
        logger.info(":savePatientInfo :communityId:" + communityId);
        try {
            LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());

            Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

            PatientEnrollmentDTO patientEnrollmentDTO = patientInfo.getPatientEnrollment();

            PatientCommunityEnrollment patientEnrollment = mapper.map(patientEnrollmentDTO, PatientCommunityEnrollment.class);
            //Spira 7258
            //relationship does not get copied
            if(patientEnrollmentDTO.getPatientEmergencyContact() != null) {
            	patientEnrollment.getPatientEmergencyContact().setRelationship(patientEnrollmentDTO.getPatientEmergencyContact().getPatientRelationshipId());
            }
            
            Person person = mapper.map(personDTO, Person.class);

            long patientId = patientInfo.getPatientId();

            String oldCareteamName = null;
            CommunityCareteam communityCareteamForPatient = careteamDAO.findCommunityCareteamForPatient(patientId);

            if (communityCareteamForPatient != null) {
                oldCareteamName = communityCareteamForPatient.getName();
            }

            long communityCareteamId = patientInfo.getCommunityCareteamId();
            String careteamName = patientInfo.getCareteamName();

            logger.info("\n\n\ncommunityCareteamId=" + communityCareteamId + ", careteamName=" + careteamName);
            System.out.println("CareTeam Changed "+patientInfo.isCareteamChanged());
             System.out.println("CareTeam Changed "+patientInfo.isCareTeamEnd());
            if (patientInfo.isCareteamChanged()) {
                careteamDAO.update(communityCareteamId, patientId);
            } else if(patientInfo.isCareTeamEnd()){
                careteamDAO.updateCateTeamEndDate(patientId);
            }
           
                // remove old care team assignment
//                careteamDAO.remove(patientId);

                //remove provider assigned to patient 
//                List<NonHealthHomeProviderAssignedPatient> nhhpap = nonHealthHomeProviderDAO.getProviderAssignedToPatient(patientId, communityId);
//                for (NonHealthHomeProviderAssignedPatient nonHealthHomeProviderAssignedPatient : nhhpap) {
//
//                    nonHealthHomeProviderDAO.unAssignProvider(nonHealthHomeProviderAssignedPatient);
//
//                }
          

            //Program health home
            if (patientInfo.getPatientEnrollment().getProgramHealthHome() != null) {
                ProgramHealthHomeDTO programHealthHomeDTO = patientInfo.getPatientEnrollment().getProgramHealthHome();
                if (programHealthHomeDTO != null) {
                    patientEnrollment.setProgramHealthHomeId(programHealthHomeDTO.getId());
                }
            }

            String programLevelConsentStatus = patientEnrollmentDTO.getProgramLevelConsentStatus();

            if (StringUtils.equalsIgnoreCase(programLevelConsentStatus, Constants.YES) || StringUtils.equalsIgnoreCase(programLevelConsentStatus, Constants.NO)) {
//                Date combinedDate = combineDate(patientInfo);
                Date consentDate = patientEnrollmentDTO.getConsentDateTime();
                patientEnrollment.setConsentDateTime(consentDate);
            }

            // checking if the enrollment status was changed.
            PatientCommunityEnrollment oldPatientEnrollment = this.getPatientEnrollmentDAO(communityId).findPatientEnrollment(patientId);
            boolean hasEnrolmentStatusChanged = !StringUtils.equalsIgnoreCase(oldPatientEnrollment.getStatus(), patientInfo.getPatientEnrollment().getStatus());

            // adding to patient_enrollment_history if enrollment status is changed.
            if (hasEnrolmentStatusChanged) {
                addToEnrollmentHistory(patientEnrollment, patientId, loginResult);
            }

            ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

            //Patient Engagement work
            boolean patientEngagementEnable = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "patient.engagement.enable", "true"));
            boolean msgAppUrlConfFlag = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "patientuser.webservice.client.url.conf.flag", "true"));
            logger.info("patientEngagementEnable:" + patientEngagementEnable + ":msgAppUrlConfFlag:" + msgAppUrlConfFlag + ":Euid:" + patientInfo.getUserDTO().getEuid());

            PatientCommunityEnrollment enrollment = careteamDAO.findPatientEnrollment(patientInfo.getUserDTO().getEuid());
            String status = enrollment.getPatientUserActive();

            logger.info(">>>>>>>>>>>>>>>back-end status:" + status + ":UI VALUE:" + patientInfo.getPatientUserActive());

            boolean isActiveProcessFlag = false;
            if (status.equalsIgnoreCase(patientInfo.getPatientUserActive())) {
                isActiveProcessFlag = true;
            }

            //prepare activate flag for db          
            setPatientUserIsActive(status, patientInfo);
            logger.info(">>>>>>>>>>FINAL  " + patientInfo.getPatientUserActive());

            //openAM and mgsApp Notification data 
            PatientEngagementRole engagementRole = careteamDAO.findPatientEngagementRole(communityId);
            String patientEngaRole = String.valueOf(engagementRole.getPatientEngagementRolePK().getRoleId());

            //access level
            String accessLevelValue = configurationDAO.getProperty(communityId, "patientuser.accesslevel");
            Long patientAccessLevelID = Long.parseLong(accessLevelValue);

            String userPassword = PasswordUtils.generatePassword();

            OrganizationCwd organization = getOrganization(communityId);
            CommunityOrganization communityOrganization = getCommunityOrganization(communityId);
            AccessLevel accessLevel = accessLevelDAO.findAccessLevel(patientAccessLevelID,communityId);
            Long accessLevelId = accessLevel.getAccessLevelPK().getAccessLevelId();
            User user = preparePatientUser(patientInfo);
            UserCommunityOrganization userCommunityOrganization = new UserCommunityOrganization();
            String gender = patientInfo.getUserDTO().getGender();
            String credentials = patientInfo.getUserDTO().getCredentials();

            userCommunityOrganization = PropertyUtils.populateUserUcoData(user, userCommunityOrganization,
                    communityOrganization.getCommunity(), organization, accessLevel,
                    null, new Date(System.currentTimeMillis()), null, status, gender, credentials, null);

            user.addUserCommunityOrganization(userCommunityOrganization);

            //url
            String patientUserMessageAppUrl = configurationDAO.getProperty(communityId, "patientuser.webservice.client.url");

            String userToken = loginResult.getToken();

            logger.info("CHECK patientEngagementEnable::" + patientEngagementEnable + ":isActiveProcessFlag:" + isActiveProcessFlag + ":Euid:" + patientInfo.getUserDTO().getEuid() + ":Email:" + patientInfo.getUserDTO().getEmail() + ":PatientUserActive:" + patientInfo.getPatientUserActive());
            if (checkEnableCareteam && !isActiveProcessFlag) {

                List<PatientCommunityEnrollment> patientEnrollments = careteamDAO.findPatientEnrollmentList(patientInfo.getUserDTO().getEmail());

                if (patientEnrollments.size() > 1) {
                    String duplicateEmailMsg = enrollmentServiceHelper.prepareDuplicateEmailAddressMsg(patientEnrollments, communityId);
                    throw new PatientEngDuplicateEmailAddressException(duplicateEmailMsg);
                } else {
                    //restrict the scope
                    //token
                    String tokenId = getToken(communityId);
                    logger.info("WITH PE enrollment.getPatientUserName()----------->>>>>" + enrollment.getPatientLoginId());
                    if (enrollment.getPatientLoginId() != null) {
                        logger.info("WITH PE activate Deactivate activity for Patient User Account::" + patientInfo.getPatientUserActive());

                        user.getUserCommunityOrganization(communityId).setDirectAddress(enrollment.getDirectAddress());
                        long careTeamPatientId = enrollment.getPatientId();

                        if (patientInfo.getPatientUserActive().equalsIgnoreCase("Active")) {
                            logger.info("WITH PE Inactive to Active process care team.............................");
                            //JIRA 629 - remove properties
                            userService.updatePatientUser(sSOUserDAO, userCommunityOrganizationDAO, userDAO, user, userPassword,
                                    communityOrganization, tokenId, organization, patientEngaRole, enrollment.getDirectAddress(), enrollment.getPatientLoginId());

                            if (msgAppUrlConfFlag) {
                                patientUserNotification.messageAppNotification(user, userToken, patientUserMessageAppUrl,
                                        PatientUserNotification.PatientStatus.Active, organization, engagementRole,
                                        communityId, accessLevelId, gender);
                            }
                            enrollment.setPatientUserActive(patientInfo.getPatientUserActive());
                            enrollment.setLastPasswordChangeDate(new Date());
                            enrollment.setStatus(patientInfo.getPatientEnrollment().getStatus());
                            careteamDAO.associatedPatientCommunityCareteam(communityCareteamId, careTeamPatientId, patientInfo.getPatientUserActive());
                            careteamDAO.activateDeactivatePatientUserAccount(enrollment);
                            //send mail to patient 
                            //JIRA 629 - remove properties
                            securityServiceHelper.sendPasswordEmailToPatient(patientInfo.getUserDTO().getEmail(), enrollment.getPatientLoginId(), userPassword, communityId);

                        } else {
                            logger.info("WITH PE Active to  Inactive process care team...........................");
                            if (msgAppUrlConfFlag) {
                                patientUserNotification.messageAppNotification(user, userToken, patientUserMessageAppUrl,
                                        PatientUserNotification.PatientStatus.Inactive, organization, engagementRole,
                                        communityId, accessLevelId, gender);
                            }

                            enrollment.setPatientUserActive(patientInfo.getPatientUserActive());
                            careteamDAO.associatedPatientCommunityCareteam(communityCareteamId, careTeamPatientId, patientInfo.getPatientUserActive());
                            careteamDAO.activateDeactivatePatientUserAccount(enrollment);
                        }

                    } else {
                        logger.info("WITH PE  <<<<<<<<<<<<<CREATE PATIENT USER ACCOUNT>>>>>>>>>>>>>>.....");

                        try {

                            String patientUserName = EnrollmentServiceUtils.getPatientUserName(patientInfo);
                            logger.info(":Care Team patientUserName:" + patientUserName);
                            //james3 account
                            createJames3User(communityId, patientUserName);

                            //direct address
                            String directAddress = createDirectAddress(communityId, patientUserName);
                            user.getUserCommunityOrganization(communityId).setDirectAddress(directAddress);
                            logger.info(":patientEngaRole:" + patientEngaRole + ":userPassword:" + userPassword + ":accessLevelValue:" + accessLevelValue);

                            //JIRA 629 - remove properties
                            userService.createPatientUser(sSOUserDAO, userCommunityOrganizationDAO, userDAO, organization, user, userPassword, communityOrganization, tokenId, patientEngaRole, directAddress, patientUserName);

                            if (msgAppUrlConfFlag) {
                                patientUserNotification.messageAppNotification(user, userToken, patientUserMessageAppUrl,
                                        PatientUserNotification.PatientStatus.Active, organization, engagementRole,
                                        communityId, accessLevelId, gender);
                            }
                            //enrollment patient  data for update patient account
                            patientEnrollment.setPatientLoginId(patientUserName);
                            patientEnrollment.setRoleID(engagementRole.getPatientEngagementRolePK().getRoleId());
                            patientEnrollment.setDirectAddress(directAddress);
                            patientEnrollment.setPatientUserActive(patientInfo.getPatientUserActive());
                            long careTeamPatientId = enrollment.getPatientId();

                            careteamDAO.associatedPatientCommunityCareteam(communityCareteamId, careTeamPatientId, patientInfo.getPatientUserActive());

                            patientEnrollment.setLastPasswordChangeDate(new Date());
                            this.getPatientEnrollmentDAO(communityId).updatePatientEnrollment(patientEnrollment);

                            //send mail to patient 
                            //JIRA 629 - remove properties
                            securityServiceHelper.sendPasswordEmailToPatient(patientInfo.getUserDTO().getEmail(), patientUserName, userPassword, communityId);

                            logger.info(" -----successfully update(create direct address) Patient User Account with ----------- ");

                        } catch (Exception ex) {
                            String message = "Error adding Patient: " + ex.getMessage();
                            logger.log(Level.SEVERE, message, ex);
                            throw new PortalException("Error adding Patient");
                        }
                    }
                    //make sure the admin token created earlier is logged out
                    sSOUserDAO.logout(communityId, tokenId);
                }
            } else {
                logger.info("<<<<<<<<<<<<<<<WITHOUT PE UPDATE PATIENT ENGAGEMENT>>>>>>>>>>>>>>>>>>>>>>>>>:");

                String existEnrollStatus = enrollment.getStatus();
                String existDirectAddress = enrollment.getDirectAddress();
                String isPatientActive = enrollment.getPatientUserActive();
                String currentEnrollStatus = patientInfo.getPatientEnrollment().getStatus();
                String progLevelConsentStatus = patientInfo.getPatientEnrollment().getProgramLevelConsentStatus();

                logger.info("WITHOUT PE existEnrollStatus:" + existEnrollStatus + ":existDirectAddress:" + existDirectAddress + ":isPatientActive:" + isPatientActive + ":currentStatus:" + currentEnrollStatus);

                if (existEnrollStatus.equalsIgnoreCase("Assigned") && existDirectAddress != null && isPatientActive.equalsIgnoreCase("Active") && !currentEnrollStatus.equalsIgnoreCase("Assigned")) {
                    logger.info("<<WITHOUT PE Enrollment status change to not Assigned for PE:>>>>>>>");
                    enrollment.setPatientUserActive("Inactive");
                    enrollment.setStatus(currentEnrollStatus);

                    if (msgAppUrlConfFlag) {
                        user.getUserCommunityOrganization(communityId).setDirectAddress(enrollment.getDirectAddress());
                        patientUserNotification.messageAppNotification(user, userToken, patientUserMessageAppUrl,
                                PatientUserNotification.PatientStatus.Inactive, organization, engagementRole,
                                communityId, accessLevelId, gender);
                    }

                    careteamDAO.activateDeactivatePatientUserAccount(enrollment);

                } else {
                    logger.info("<<WITHOUT PE with out changed pe activity>>>>>>>>>>>>>>>>>>>:");
                    patientEnrollment.setPatientUserActive(status);
                    patientEnrollment.setLastPasswordChangeDate(enrollment.getLastPasswordChangeDate());
                    this.getPatientEnrollmentDAO(communityId).updatePatientEnrollment(patientEnrollment);
                }
            }
            
            // JIRA - 618 Manage consented organization visibility across dashboard/Treat
           // We have two cases : BHH and UHC
          // This is for BHH case, that consent should be revoked from other orgs if plcs is 'No'.
          // For UHC, no change 
           boolean disableForBHH = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "manage.consent.withut.enrollment"));
           String plcs= patientEnrollment.getProgramLevelConsentStatus();
           if (!plcs.equalsIgnoreCase("Yes") && !disableForBHH) {
            logger.info("\n PLCS is no, so the consent should be revoked from other consented orgs! \n");
            logger.info(" Remove OPC entries for this patient, since consent is revoked !!!!!! ");
            List<OrganizationPatientConsentDTO> orgs = new ArrayList<OrganizationPatientConsentDTO>();
            PatientConsentUtils.saveOrganizationPatientConsentList(organizationPatientConsentDAO, userPatientConsentDAO, orgs, patientEnrollment);
             
            // handle mypatientlist synch
            ServletContext application = EnrollmentServiceImpl.getContext();
            EnrollmentPatientUpdater enrollmentPatientUpdater = (EnrollmentPatientUpdater) application.getAttribute(WebConstants.ENROLLMENT_SERVICE_KEY);
            enrollmentPatientUpdater.synchMyPatientList(personDTO.getPatientEnrollment().getPatientId(), communityId);

            // send patient consent in the background
            enrollmentServiceHelper.sendPatientConsentToTREAT(personDTO.getPatientEnrollment().getPatientId(), communityId, personDTO.getAdditionalConsentedOrganizations());

        } 
            //End Patient Engagement work

            runPatientUpdateProcessAsync(person, personDTO, patientInfo, organization, patientId, programLevelConsentStatus, loginResult, patientEnrollment, communityCareteamId, oldCareteamName);

        } catch (Exception exc) {
            final String message = "Error saving patient info. patientId: " + patientInfo.getPatientId();
            logger.logp(Level.SEVERE, getClass().getName(), "savePatientInfo", message, exc);

            throw new PortalException("Error saving patient info.");
        }
    }

    @Override
    public CareTeamDTO getUsersForCareteam(long communityCareteamId) throws PortalException {
        try {
            CareTeamDTO careTeamDTO = new CareTeamDTO();
            List<User> users = careteamDAO.getUsersForCareteam(communityCareteamId);
            LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
            Long communityId = loginResult.getCommunityId();
            List<UserDTO> userDTOs = PropertyUtils.convertToDTOs(users, userCommunityOrganizationDAO, communityId);

            careTeamDTO.setUserDTOList(userDTOs);

            return careTeamDTO;

        } catch (Exception exc) {
            final String message = "Error retrieving care team users. communityCareteamId: " + communityCareteamId;
            logger.logp(Level.SEVERE, getClass().getName(), "getUsersForCareteam", message, exc);

            throw new PortalException("Error retrieving care team users.");
        }

    }

    @Override
    public List<UserDTO> getUsersForCareteamWithoutPermitConsent(long communityCareteamId, long patientId, long patientOrgId) throws PortalException {

        try {
//            List<UserDTO> userDTOs = getUsersForCareteam(communityCareteamId, patientId, patientOrgId);
            CareTeamDTO careTeamDTOs = getUsersForCareteam(communityCareteamId, patientId, patientOrgId);
            List<UserDTO> userDTOs = careTeamDTOs.getUserDTOList();
            // filter those users w/o permit consent
            List<UserDTO> usersWithoutConsent = getUsersWithoutPermitConsent(userDTOs, patientId);

            return usersWithoutConsent;

        } catch (Exception exc) {
            final String message = "Error retrieving care team users w/o consent. communityCareteamId: " + communityCareteamId;
            logger.logp(Level.SEVERE, getClass().getName(), "getUsersForCareteamWithoutPermitConsent", message, exc);

            throw new PortalException("Error retrieving care team users.");
        }

    }

    @Override
    public CareTeamDTO getUsersForCareteam(long communityCareteamId, long patientId, long patientOrgId) throws PortalException {

        CareTeamDTO careTeamDTO = new CareTeamDTO();
        try {

            List<User> users = careteamDAO.getUsersForCareteam(communityCareteamId);
            LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
            Long communityId = loginResult.getCommunityId();
            List<UserDTO> userDTOs = PropertyUtils.convertToDTOs(users, userCommunityOrganizationDAO, communityId);

            PatientCommunityCareteam assciatedPatient = careteamDAO.findAssociatedPatientFlag(communityCareteamId, patientId);

            if (assciatedPatient != null) {

                if (assciatedPatient.getIsAssociate() != null) {

                    if (assciatedPatient.getIsAssociate().charValue() == 'Y') {
                        PatientCommunityEnrollment patient = this.getPatientEnrollmentDAO(communityId).findPatientEnrollment(patientId);
                        String patientEuid = patient.getPatientEuid();
                        PersonDTO patientData = getAssociatedPatient(patientEuid, communityId);
                        PersonDTO dTO = new PersonDTO();
                        dTO.setFirstName(patientData.getFirstName());
                        dTO.setLastName(patientData.getLastName());
                        careTeamDTO.setPersonDTO(dTO);
                    }
                }

            }
            // set the permit consent flag
            setPermitConsent(userDTOs, patientId, communityId, patientOrgId);

            careTeamDTO.setUserDTOList(userDTOs);

            return careTeamDTO;

        } catch (Exception exc) {
            final String message = "Error retrieving care team users. communityCareteamId: " + communityCareteamId;
            logger.logp(Level.SEVERE, getClass().getName(), "getUsersForCareteam", message, exc);

            throw new PortalException("Error retrieving care team users.");
        }

    }

    protected PersonDTO getAssociatedPatient(String euid, Long communityId) {

        PersonDTO personDTO = null;
        try {
            Person thePerson = new Person(this.getPatientManager(communityId).getPatient(euid));
            Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
            personDTO = mapper.map(thePerson, PersonDTO.class);

        } catch (Exception exc) {
            // skip this patient
            String message = "Could not find patient ID in MDM: " + euid;
            logger.log(Level.SEVERE, message, exc);

        }
        return personDTO;
    }

    @Override
    public List<UserDTO> getAvailableUsers() throws PortalException {

        List<UserDTO> userDTOs = getAvailableUsersFromCache();

        return userDTOs;
    }
    
    @Override
    public LinkedHashMap<String, String> getConsenterTable(long communityId) throws PortalException {

       // LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
       // Long communityId = loginResult.getCommunityId();
        LinkedHashMap<String, String> consenterDTOs = getConsenters(communityId);

        return consenterDTOs;
    }

    @Override
    public List<UserDTO> getAvailableUsers(String role, List<Long> userIdsToExclude) throws PortalException {
        try {

            LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
            Long communityId = loginResult.getCommunityId();
            List<User> users = careteamDAO.getAvailableUsers(role, userIdsToExclude, communityId);
            List<UserDTO> userDTOs = PropertyUtils.convertToDTOs(users, userCommunityOrganizationDAO, communityId);
            return userDTOs;
        } catch (Exception exc) {
            final String message = "Error retrieving available users.";
            logger.logp(Level.SEVERE, getClass().getName(), "getAvailableUsers", message, exc);

            throw new PortalException(message);
        }
    }

    @Override
    public void saveCareteam(long communityCareteamId, String careteamName, List<UserDTO> users, long communityId) throws PortalException {

        try {
            //JIRA 1178 - Spira 5201    
            boolean exists = careteamDAO.doesCareteamNameExistForOtherTeam(communityCareteamId, careteamName, communityId);

            if (!exists) {
                // save
                CommunityCareteam careteam = new CommunityCareteam();
                careteam.setCommunityCareteamId(communityCareteamId);
                careteam.setCommunityId(communityId);
                careteam.setName(careteamName);
                logger.info("careteamServiceImpl - saving Careteam " + careteamName + " in community: " + communityId);

                careteamDAO.edit(careteam, users, communityId);

                saveCareteamBackgroundProcess(communityCareteamId, careteamName, communityId);

            } else {
                throw new IllegalArgumentException("A care team with this name already exists. Please enter a new value within the Care Team Name field.");
            }
        } catch (IllegalArgumentException exc) {
            throw new PortalException(exc.getMessage());
        } catch (Exception exc) {
            final String message = "Error saving care team: " + careteamName;
            logger.logp(Level.SEVERE, getClass().getName(), "saveNewCareteam", message, exc);

            throw new PortalException(message);
        }
    }

    @Override
    public void saveNewCareteam(String careteamName, long communityId, List<UserDTO> users) throws PortalException {
        try {
            //JIRA 1178 - Spira 5201
            boolean exists = careteamDAO.doesCareteamNameExistForOtherTeam(careteamName, communityId);

            if (!exists) {
                // save
                CommunityCareteam careteam = new CommunityCareteam();
                careteam.setCommunityId(communityId);
                careteam.setName(careteamName);

                careteamDAO.create(careteam, users, communityId);

                saveNewCareteamBackgroundProcess(communityId, careteamName);
            } else {
                throw new IllegalArgumentException("A care team with this name already exists. Please enter a new value within the Care Team Name field.");
            }
        } catch (IllegalArgumentException exc) {
            throw new PortalException(exc.getMessage());
        } catch (Exception exc) {
            final String message = "Error saving care team: " + careteamName;
            logger.logp(Level.SEVERE, getClass().getName(), "saveNewCareteam", message, exc);

            throw new PortalException(message);
        }
    }

    //JIRA 1178 - Spira 5201
    @Override
    public CommunityCareteamDTO getCareteam(String careteamName, long communityId) throws PortalException {
        CommunityCareteamDTO communityCareteamDTO = null;

        try {
            CommunityCareteam communityCareteam = careteamDAO.findCommunityCareteam(careteamName, communityId);

            Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
            communityCareteamDTO = mapper.map(communityCareteam, CommunityCareteamDTO.class);
            logger.logp(Level.FINE,getClass().getName(),"CareTEamDTOObjectValue:",communityCareteamDTO.getCommunityCareteamId().toString());
            return communityCareteamDTO;

        } catch (Exception exc) {
            final String message = "Error retrieving community care team: " + careteamName;
            logger.logp(Level.SEVERE, getClass().getName(), "getCareteam", message, exc);

            throw new PortalException(message);
        }
    }

    private List<PersonDTO> findGenericCandidatesEnrolledPatients(long communityId, Map<String, PatientCommunityEnrollment> patientEnrollments) throws Exception {

        List<PersonDTO> searchResults = new ArrayList<PersonDTO>();
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        int minorAge = Integer.parseInt(configurationDAO.getProperty(communityId, "minor.age", "18"));

        // get the patient enrollment
        Set<Map.Entry<String, PatientCommunityEnrollment>> entries = patientEnrollments.entrySet();

        for (Map.Entry<String, PatientCommunityEnrollment> tempEntry : entries) {
            String euid = tempEntry.getKey();
            PatientCommunityEnrollment tempPatientEnrollment = tempEntry.getValue();

            Person tempPerson = null;

            try {
                tempPerson = new Person(this.getPatientManager(communityId).getPatient(euid));
            } catch (Exception exc) {
                // skip this patient
                String message = "Could not find patient ID in MDM: " + euid;
                logger.log(Level.SEVERE, message, exc);
                continue;
            }

            tempPerson.setPatientCommunityEnrollment(tempPatientEnrollment);

            // check if patient id is in patient_community_careteam
            long patientId = tempPatientEnrollment.getPatientId();

            boolean exists = careteamDAO.doesPatientCommunityCareteamExist(patientId);

            // if so, then get care team id
            // - get care team name
            if (exists) {
                CommunityCareteam communityCareteam = careteamDAO.findCommunityCareteamForPatient(patientId);
                String careteamName = communityCareteam.getName();
                tempPerson.setCareteam(careteamName);
            }

            // get organization name
            long orgId = tempPatientEnrollment.getOrgId();
            String orgName = organizationDAO.getOrganizationName(orgId);
            tempPatientEnrollment.setOrganizationName(orgName);

            PersonDTO personDto = mapper.map(tempPerson, PersonDTO.class);

            setPatientRelationship(communityId, tempPerson, personDto);
            //Program health home
            setProgramHealthHome(tempPerson, personDto);
            setMinor(tempPerson, personDto, minorAge);

            searchResults.add(personDto);
        }

        Collections.sort(searchResults, personDTOComparator);

        return searchResults;
    }

    protected void setProgramHealthHome(Person tempPerson, PersonDTO personDto) {

        if (tempPerson.getPatientCommunityEnrollment().getProgramHealthHomeId() != 0) {

            ProgramHealthHomeDTO programHealthHomeDTO = new ProgramHealthHomeDTO();
            long communityId = personDto.getPatientEnrollment().getCommunityId();
            long id = tempPerson.getPatientCommunityEnrollment().getProgramHealthHomeId();
            ProgramHealthHome healthHome = this.getPatientEnrollmentDAO(communityId).getProgramHealthHome(communityId, id);
            programHealthHomeDTO.setHealthHomeName(healthHome.getHealthHomeValue());
            programHealthHomeDTO.setId(healthHome.getProgramHealthHomePK().getId());
            personDto.getPatientEnrollment().setProgramHealthHome(programHealthHomeDTO);

        }
    }

    @Override
    public boolean hasPatientsAssignedToCareteam(long communityCareteamId) throws PortalException {
        try {
            return careteamDAO.hasPatientsAssignedToCareteam(communityCareteamId);

        } catch (Exception exc) {
            final String message = "Error checking patients assigned to care team.";
            logger.logp(Level.SEVERE, getClass().getName(), "hasPatientsAssignedToCareteam", message, exc);

            throw new PortalException(message);
        }
    }

    @Override
    public boolean hasOtherPatientsAssignedToCareteam(long communityCareteamId, long patientId) throws PortalException {
        try {
            return careteamDAO.hasOtherPatientsAssignedToCareteam(communityCareteamId, patientId);
        } catch (Exception exc) {
            final String message = "Error checking patients assigned to care team.";
            logger.logp(Level.SEVERE, getClass().getName(), "hasPatientsAssignedToCareteam", message, exc);

            throw new PortalException(message);
        }
    }

    @Override
    public ManageCareTeamFormData getManageCareTeamFormData(long communityId) throws PortalException {

        try {
            List<Role> roles = careteamDAO.getUserRoles(communityId);
            Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
            List<RoleDTO> roleDTO = new ArrayList<RoleDTO>(roles.size());
            for (Role role : roles) {
                roleDTO.add(mapper.map(role, RoleDTO.class));
            }

            List<String> requiredRoles = careteamDAO.getRequiredRolesForCareteam(communityId);
            List<String> duplicateRoles = careteamDAO.getDuplicateRolesForCareteam(communityId);

            ManageCareTeamFormData manageCareTeamFormData = new ManageCareTeamFormData();
            manageCareTeamFormData.setRequiredRolesForCareTeam(requiredRoles);
            manageCareTeamFormData.setUserRoles(roleDTO);
            manageCareTeamFormData.setDuplicateRoles(duplicateRoles);

            return manageCareTeamFormData;
        } catch (Exception exc) {
            final String message = "Error getting required roles for care team.";
            logger.logp(Level.SEVERE, getClass().getName(), "getRequiredRolesForCareteam", message, exc);

            throw new PortalException(message);
        }
    }

    private Date combineDate(PatientInfo patientInfo) {

        PatientEnrollmentDTO patientEnrollmentDTO = patientInfo.getPatientEnrollment();

        Date consentDate = patientEnrollmentDTO.getConsentDateTime();
        String time = patientInfo.getConsentTime();
        String ampm = patientInfo.getConsentAMPM();

        Date resultDate = DateUtils.combineDateTime(consentDate, time, ampm);

        return resultDate;
    }

    /**
     * FIXME clean up all these random loginResults - get it once
     *
     * @param person
     * @param patientEnrollment
     * @param euid
     * @param careteamId
     * @param sendEnrollmentStatusFlag
     * @param sendCareTeamFlag
     * @param communityId
     * @param loginResult
     * @throws Exception
     */
    private void sendNotificatonToBHIX(Person person, PatientCommunityEnrollment patientEnrollment,
            String euid, String careteamId, boolean sendEnrollmentStatusFlag,
            boolean sendCareTeamFlag, long communityId, LoginResult loginResult) throws Exception {

        // convert person to personDTO
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        PersonDTO personDTO = mapper.map(person, PersonDTO.class);
        // call inherited method for sending notification
        enrollmentServiceHelper.sendCareteamAndEnrollmentNotificationsToNotificationManager(personDTO, careteamId, sendEnrollmentStatusFlag, sendCareTeamFlag, communityId, loginResult.getCommunityOid());
    }

    protected Person getPerson(String euid, Long communityId) throws Exception {

        Person thePerson = new Person(this.getPatientManager(communityId).getPatient(euid));
        PatientCommunityEnrollment tempPatientEnrollment = this.getPatientEnrollmentDAO(communityId).findPatientEnrollment(euid);
        thePerson.setPatientCommunityEnrollment(tempPatientEnrollment);

        long localId = tempPatientEnrollment.getPatientId();
        thePerson.setLocalId(Long.toString(localId));

        return thePerson;
    }

    protected void processBulkCareteamNotificationToBHIX(long communityCareteamId, long communityId) {

        try {
            // get the patients assigned to this care team
            List<PersonDTO> personDTOlist = getPatientsForCareteam(communityCareteamId, communityId);

            String careteamId = Long.toString(communityCareteamId);
            LoginResult loginResult = getLoginResult();
            // send careteam notification message to BHIX
            enrollmentServiceHelper.sendCareteamNotificationBHIX(personDTOlist, careteamId, communityId, loginResult.getCommunityOid());

        } catch (Exception exc) {
            logger.log(Level.WARNING, "Unable to process bulk care team notification for care team id: " + communityCareteamId, exc);
        }
    }

    /**
     * Get patients for the given EUIDs
     *
     * @param euids
     * @return
     * @throws Exception
     */
    protected List<Person> getPatients(List<String> euids, Long communityId) throws Exception {
        List<Person> patients = new ArrayList<Person>();

        for (String tempEuid : euids) {
            Person tempPatient = new Person(this.getPatientManager(communityId).getPatient(tempEuid));
            patients.add(tempPatient);
        }

        return patients;
    }

    /**
     * Convert a person object to DTO
     *
     * @param tempPerson
     * @return
     */
    protected PersonDTO convert(Person tempPerson, Long communityId) {

        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        String euid = tempPerson.getEuid();
        PatientCommunityEnrollment tempPatientEnrollment = this.getPatientEnrollmentDAO(communityId).findPatientEnrollment(euid);
        logger.info("Convert Person to Person DTO :"+ tempPatientEnrollment.getConsenter() );
        tempPerson.setPatientCommunityEnrollment(tempPatientEnrollment);

        // check if patient id is in patient_community_careteam
        long patientId = tempPatientEnrollment.getPatientId();
        boolean exists = careteamDAO.doesPatientCommunityCareteamExist(patientId);

        // if so, then get care team id
        // - get care team name
        if (exists) {
            CommunityCareteam communityCareteam = careteamDAO.findCommunityCareteamForPatient(patientId);
            String careteamName = communityCareteam.getName();
            tempPerson.setCareteam(careteamName);
        }

        // get organization name
        long orgId = tempPatientEnrollment.getOrgId();
        String orgName = organizationDAO.getOrganizationName(orgId);
        tempPatientEnrollment.setOrganizationName(orgName);

        PersonDTO personDTO = mapper.map(tempPerson, PersonDTO.class);

        return personDTO;
    }

    /**
     * Convert a list of person objects to DTOs
     *
     * @param patients
     * @return
     */
    private List<PersonDTO> convert(List<Person> patients, Long communityId) {

        List<PersonDTO> dtos = new ArrayList<PersonDTO>();

        for (Person tempPatient : patients) {
            PersonDTO personDTO = convert(tempPatient, communityId);
            dtos.add(personDTO);
        }

        return dtos;
    }

    private List<PersonDTO> filterEnrolledPatientsWithoutDemographics(long communityId, long userId, SearchCriteria searchCriteria) throws Exception {

        Map<String, PatientCommunityEnrollment> patientEnrollmentsMap = this.getPatientEnrollmentDAO(communityId).filterEnrolledPatientsWithoutDemographics(userId, searchCriteria, statuses);

        List<PersonDTO> patients = findGenericCandidatesEnrolledPatients(communityId, patientEnrollmentsMap);

        return patients;
    }

    private List<PersonDTO> filterEnrolledPatientsWithoutDemographics(long communityId, SearchCriteria searchCriteria) throws Exception {

        Map<String, PatientCommunityEnrollment> patientEnrollmentsMap = this.getPatientEnrollmentDAO(communityId).filterEnrolledPatientsWithoutDemographics(searchCriteria, statuses);

        List<PersonDTO> patients = findGenericCandidatesEnrolledPatients(communityId, patientEnrollmentsMap);

        return patients;
    }

    @Override
    public int getTotalCountForAvailableUsers(String role, List<Long> userIdsToExclude) throws PortalException {

        try {
            LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
            Long communityId = loginResult.getCommunityId();
            int count = careteamDAO.getTotalCountForAvailableUsers(role, userIdsToExclude, communityId);

            return count;
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error getting total count.", exc);
            throw new PortalException(exc.getMessage());
        }
    }

    public PatientFormReferenceData getPatientFormReferenceData(long communityId) throws PortalException {
        try {
            PatientFormReferenceData patientFormReferenceData = new PatientFormReferenceData();

            patientFormReferenceData.setCareteamNames(getCareteamNames(communityId));
            // 1st, lets get communityOrgs
            List<CommunityOrganization> commOrgList = organizationDAO.findOrganizationsWithAllRelationships(communityId);
            // parse out org, set facility and get org names
            List<OrganizationDTO> orgDTOs = new ArrayList<>();
            LinkedHashMap<String, String> orgNames = new LinkedHashMap<>();
            
            this.allMannerOfShenanigansToPopulateOrgData(commOrgList, orgDTOs, orgNames, communityId);
            
            patientFormReferenceData.setOrganizationDTOs(orgDTOs);
            patientFormReferenceData.setOrganizationNames(orgNames);
            patientFormReferenceData.setConsenters(getConsenters(communityId));
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            patientFormReferenceData.setUserDTOs(getUserDTOs(communityId));
            stopWatch.stop();
            logger.info("it took " + stopWatch.getTime() / 1000.0 + " getPatientFormReferenceData/1391");
            patientFormReferenceData.setPayerPlanNames(getPayerPlans());
            //patientFormReferenceData.setPayerClassNames(getPayerClassNames());

            return patientFormReferenceData;
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error getting patient form reference data.", exc);
            throw new PortalException(exc.getMessage());
        }
    }
    
    private void allMannerOfShenanigansToPopulateOrgData(List<CommunityOrganization> coList, List<OrganizationDTO> orgDTOs, LinkedHashMap<String, String> orgNames, Long communityId) {
        // loop thru coList, populate the dto and names map at the same time
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        for (CommunityOrganization co : coList) {
            OrganizationDTO orgDTO = mapper.map(co, OrganizationDTO.class);
            logger.fine("converted " + orgDTO.getOrganizationName() + "to dto");
            orgDTOs.add(orgDTO);
        }
        stopWatch.stop();
        logger.fine("time to load " + coList.size() + " orgs for shenanigans: " + stopWatch.getTime() / 1000.0);
        
    }

    private void setPermitConsent(List<UserDTO> userDTOs, long patientId, long communityId, long patientOrgId) {

        for (UserDTO tempUser : userDTOs) {
            long userOrganizationId = Long.parseLong(tempUser.getOrgid());

            boolean hasConsent;

            // auto have consent if user is in primary care org
            if (userOrganizationId == patientOrgId) {
                hasConsent = true;
            } else {
                hasConsent = organizationPatientConsentDAO.hasConsent(patientId, communityId, userOrganizationId);
            }

            tempUser.setPermitConsent(hasConsent);
        }

    }

    private List<UserDTO> getUsersWithoutPermitConsent(List<UserDTO> userDTOs, long patientId) {

        List<UserDTO> usersWithoutConsent = new ArrayList<UserDTO>();

        for (UserDTO tempUser : userDTOs) {

            if (!tempUser.hasPermitConsent()) {
                usersWithoutConsent.add(tempUser);
            }
        }

        return usersWithoutConsent;
    }

    protected void sendCareteamCreatedAlert(long communityId, String careTeamName) {
        GSIHealthNotificationMessage constructedMessage = null;
        String notificationStatus = null;

        try {
            ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

            String gsiOid = configurationDAO.getProperty(communityId, "gsihealth.notification.send.fromOID");

            String alertLogicalId = AlertConstants.NEW_CARE_TEAM_CREATED_ALERT_ID;
            String alertMessageBody = "New Care Team " + careTeamName + " has been created with no patients assigned to it.";

            LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
            String senderOid = loginResult.getOid();

            //construct the message
            constructedMessage = NotificationUtils.constructCareteamAlertNotificationMessage(communityId, alertLogicalId, alertMessageBody, senderOid, gsiOid, careTeamName);

            // send the message
            notificationStatus = NotificationUtils.sendAlert(communityId, constructedMessage);

        } catch (Throwable exc) {
            final String message = constructedMessage.toString();
            logger.logp(Level.SEVERE, getClass().getName(), "addOrganization", message, exc);
            notificationStatus = "FAILURE";

        } finally {
            // audit the message
            //addNotificationAudit(organizationDTO, notificationStatus, constructedMessage);
        }

    }

    protected void sendCareteamChangedAlert(long communityId, String careTeamName, boolean hasPatientsAssignedToCareteam) {
        GSIHealthNotificationMessage constructedMessage = null;
        String notificationStatus = null;

        try {

            String alertLogicalId = AlertConstants.CARE_TEAM_CHANGED_ALERT_ID;
            StringBuilder alertMessageBody = new StringBuilder("Care Team " + careTeamName + " has been modified. Care Team " + careTeamName + " has ");

            if (!hasPatientsAssignedToCareteam) {
                alertMessageBody.append("no ");
            }

            alertMessageBody.append("patients assigned.");

            ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
            String gsiOid = configurationDAO.getProperty(communityId, "gsihealth.notification.send.fromOID");

            LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
            String senderOid = loginResult.getOid();

            //construct the message
            constructedMessage = NotificationUtils.constructCareteamAlertNotificationMessage(communityId, alertLogicalId, alertMessageBody.toString(), senderOid, gsiOid, careTeamName);

            // send the message
            notificationStatus = NotificationUtils.sendAlert(communityId, constructedMessage);

        } catch (Throwable exc) {
            final String message = constructedMessage.toString();
            logger.logp(Level.SEVERE, getClass().getName(), "addOrganization>", message, exc);
            notificationStatus = "FAILURE";

        } finally {
            // audit the message
            //addNotificationAudit(organizationDTO, notificationStatus, constructedMessage);
        }

    }

    protected LinkedHashMap<String, String> getOrganizationNames(long communityId) throws PortalException {
        LinkedHashMap<String, String> data = null;

        try {
            List<OrganizationCwd> orgs = organizationDAO.findOrganizationEntities2(communityId);

            data = new LinkedHashMap<String, String>();

            for (OrganizationCwd temp : orgs) {
                String oid = Long.toString(temp.getOrgId());
                String orgName = temp.getOrganizationCwdName();
                data.put(oid, orgName);
            }
        } catch (Exception exc) {
            String message = "Error loading organization names";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }

        return data;
    }
    
    private LinkedHashMap<String, String> getPayerPlans() throws PortalException {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

        try {
            List<PayerPlan> payerPlans = payerPlanDAO.findPayerPlanEntities();

            for (PayerPlan temp : payerPlans) {
                String id = Long.toString(temp.getPayerPlanPK().getId());
                String name = temp.getName();
                data.put(id, name);
            }

            // move "Other" to the end;
            PayerPlanDAOUtils.movePayerPlanToEnd(data, com.gsihealth.dashboard.server.dao.Constants.OTHER_PAYER_PLAN_NAME);
        
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error loading payer plans.", exc);
            throw new PortalException(exc.getMessage());
        }

        return data;
    }

    private LinkedHashMap<String, String> getPayerClassNames() throws PortalException {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

        try {
            List<PayerClass> payerClassList = payerPlanDAO.findPayerClassEntities();

            for (PayerClass temp : payerClassList) {
                String id = Long.toString(temp.getPayerClassPK().getId());
                String name = temp.getName();
                data.put(id, name);
            }
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error loading payer classes.", exc);
            throw new PortalException(exc.getMessage());
        }

        return data;
    }

    private LinkedHashMap<String, String> getConsenters(long communityId) throws PortalException {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

        try {
            List<Consenter> consenters = consenterDAO.getConsenters(communityId);

            for (Consenter temp : consenters) {
                String code = Long.toString(temp.getConsenterPK().getCode());
                String description = temp.getCodeDescription();
                logger.info("consenters code,description:" +code + ", "+description);
                data.put(code, description);
            }

        

        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error loading consenters.", exc);
            throw new PortalException(exc.getMessage());
        }
        return data;
    }
    
    private List<UserDTO> getAvailableUsersFromCache() {
        ServletContext application = getServletContext();
        LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
        Long communityId = loginResult.getCommunityId();

        Map<Long, List<UserDTO>> dtoMap = (Map<Long, List<UserDTO>>) application.getAttribute(WebConstants.AVAILABLE_USERS_KEY);
        List<UserDTO> users = dtoMap.get(communityId);

        return users;
    }

    public int findTotalNoOfUser(long communityId, SearchCriteria searchCriteria, long userId) throws PortalException {

        int totalCount = 0;

        try {
            LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
            Long userAccessLevelId = loginResult.getAccessLevelId();

            List<PersonDTO> patients = new ArrayList<PersonDTO>();

            boolean hasDemographics = EnrollmentUtils.hasDemographicData(searchCriteria);

            if (hasDemographics) {

                if (userAccessLevelId == WebConstants.POWER_USER_ACCESS_LEVEL_ID) {

                    patients = filterEnrolledPatientsWithDemographics(communityId, searchCriteria);
                } else {

                    patients = filterEnrolledPatientsWithDemographics(communityId, userId, searchCriteria);
                }

            } else {

                if (userAccessLevelId == WebConstants.POWER_USER_ACCESS_LEVEL_ID) {

                    patients = filterEnrolledPatientsWithoutDemographics(communityId, searchCriteria);
                } else {
                    patients = filterEnrolledPatientsWithoutDemographics(communityId, userId, searchCriteria);
                }
            }

            totalCount = patients.size();
        } catch (Exception exc) {
            final String message = "Error get total count for patients: ";
            logger.logp(Level.SEVERE, getClass().getName(), "getTotalCountForCandidates", message, exc);
        } finally {
            return totalCount;
        }
    }

    @Override
    public PersonDTO updatePatientBySelfAssertion(PersonDTO personDTO) throws PortalException {
        LoginResult loginResult = getLoginResult();
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        Person updatedPerson = mapper.map(personDTO, Person.class);
        PatientCommunityEnrollment pce = updatedPerson.getPatientCommunityEnrollment();
        Long userOrgId = this.getLoginResult().getOrganizationId();
        logger.info("user orgId: " + userOrgId + " for patient  " + updatedPerson.getPatientId());
        try{
            this.enrollmentServiceHelper.addAndUpdateOrganizationPatientConsent(null, userOrgId, pce, pce.getCommunityId());
        } catch (Exception e) {
           logger.logp(Level.SEVERE, getClass().getName(), "updatePatientBySelfAssertion", "updatePatientBySelfAssertion", e);
            throw new PortalException("Could not set self assertion for this patient"); 
        }

        String comment = "Update Patient via Self Assert: " + personDTO.getFirstName() + " " + personDTO.getLastName();
        audit(updatedPerson, comment, AuditType.UPDATE_PATIENT_VIA_SELF_ASSERT, loginResult, Constants.YES);

        return personDTO;

    }
    
    private void audit(Person person, String comment, String auditType, LoginResult loginResult, String actionSuccess) {
        audit(person, comment, auditType, loginResult, actionSuccess, null);
    }

    private void addToEnrollmentHistory(PatientCommunityEnrollment enrollment, long patientId, LoginResult loginResult) {

        logger.info("Adding patient's enrollment status info for care team to patient_enrollment_history....." + loginResult.getCommunityId());

        long communityId = loginResult.getCommunityId();
        PatientEnrollmentHistory enrollmentHistory = new PatientEnrollmentHistory();

        enrollmentHistory.setCommunityId(communityId);
        enrollmentHistory.setPatientId(patientId);
        enrollmentHistory.setEnrollmentChangeDate(new Date());
        enrollmentHistory.setEnrollmentStatus(enrollment.getStatus());
        enrollmentHistory.setUserId(loginResult.getUserId());
        enrollmentHistory.setPatientOrgId(enrollment.getOrgId());

        enrollmentHistory.setAction("modify");
        enrollmentHistory.setInformationSharingMinor(enrollment.getInformationSharingStatus());
        enrollmentHistory.setMinorConsentObtainedByUserId(enrollment.getMinorConsentObtainedByUserId());

        String reason = enrollment.getReasonForInactivation();
        if (reason != null) {
            enrollmentHistory.setReasonForInactivationId((disenrollmentReasonDAO.getDisenrollmentReason(loginResult.getCommunityId(), reason).getDisenrollmentReasonsPK().getId()));
        }

        patientEnrollmentHistoryDAO.addToEnrollmentHistory(enrollmentHistory);
    }

    private void addToAcuityScoreHistory(PatientCommunityEnrollment enrollment, long patientId, LoginResult loginResult) {

        logger.info("Adding organization info to care_management_organization_history.CommunityId==" + loginResult.getCommunityId());

        AcuityScoreHistory acuityScoreHistory = new AcuityScoreHistory();

        acuityScoreHistory.setCommunityId(loginResult.getCommunityId());
        acuityScoreHistory.setPatientId(patientId);
        acuityScoreHistory.setAcuityScoreChangeDate(new Date());

        acuityScoreHistory.setUserId(loginResult.getUserId());
        acuityScoreHistory.setAcuityScore(enrollment.getAcuityScore());

        acuityScoreHistory.setAction("modify");

        acuityScoreHistoryDAO.addToAcuityScoreHistory(acuityScoreHistory);
    }

    private void addToProgramNameHistory(PatientCommunityEnrollment enrollment, long patientId, LoginResult loginResult) {
        logger.info("Adding patient's program name info to program_name_history...");

        long communityId = loginResult.getCommunityId();

        ProgramNameHistory programNameHistory = new ProgramNameHistory();
        programNameHistory.setCommunityId(communityId);

        programNameHistory.setPatientId(patientId);
//        programNameHistory.setCurrentProgramName(enrollment.getProgramName());
        // set program effective date
        if (enrollment.getProgramNameEffectiveDate() != null) {
            programNameHistory.setProgramEffectiveDate(enrollment.getProgramNameEffectiveDate());
        }

        // set program end date
        if (enrollment.getProgramEndDate() != null) {
            programNameHistory.setProgramEndDate(enrollment.getProgramEndDate());
        }
        programNameHistory.setUserId(loginResult.getUserId());
        programNameHistory.setAction("Updated Patient");

        // adding to DAO
        programNameHistoryDAO.addToProgramNameHistory(programNameHistory);

    }

    /**
     * ok, lets stop the madness, One Ring to rule them all, One Ring to find
     * them, One Ring to bring them all and in the darkness bind them this
     * method returns a paginated list of patients AND sets the total patient
     * count It can be used for enrolled or non-enrolled patients - set the
     * search param power/non-power users - if currentUserOrgId is null, don't
     * set that search param, if searchCriteria is null, don't use it.
     *
     * @param communityId
     * @param searchCriteria
     * @param enrolledMode
     * @param currentUserOrgId
     * @param pageNumber
     * @param pageSize
     * @param userId
     * @return paginated list of patients meeting the contained params
     */
    @Override
    public SearchResults<PersonDTO> findPatients(
            long communityId,
            SearchCriteria searchCriteria,
            boolean enrolledMode,
            Long currentUserOrgId,
            int pageNumber,
            int pageSize,
            long userId) throws PortalException {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        int maxPatientSearchResultsSize = Integer.parseInt(configurationDAO.getProperty(communityId, "max.patient.search.results.size", "1000"));
        boolean isManageConsentWithoutEnrollment = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "manage.consent.withut.enrollment", "false"));
        boolean isAllOrgSelfAssert = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "all.orgs.self.assert", "false"));

        SearchResults<PersonDTO> searchResults = this.findGenericPatients(communityId, searchCriteria,
                enrolledMode, currentUserOrgId, pageNumber, pageSize, userId, isManageConsentWithoutEnrollment, isAllOrgSelfAssert, maxPatientSearchResultsSize);

        return searchResults;
    }

    /**
     * TODO - too many db searches, any way to get rid of some of them?
     *
     * @param communityId
     * @param searchCriteria
     * @param enrolledMode
     * @param currentUserOrgId
     * @param userId
     * @return total number of patients that meet searchCriteria
     */
    @Override
    public SearchPatientResults countPatients(long communityId, SearchCriteria searchCriteria, boolean enrolledMode,
            Long currentUserOrgId,long userId) throws PortalException {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        int maxPatientSearchResultsSize = Integer.parseInt(configurationDAO.getProperty(communityId, "max.patient.search.results.size", "1000"));
        boolean isManageConsentWithoutEnrollment = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "manage.consent.withut.enrollment", "false"));
        boolean isAllOrgSelfAssert = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "all.orgs.self.assert", "false"));

        SearchPatientResults patientCount = this.countGenericPatients(communityId,
                searchCriteria, enrolledMode, currentUserOrgId, userId, isManageConsentWithoutEnrollment, isAllOrgSelfAssert, maxPatientSearchResultsSize);
        return patientCount;
    }

    //pe
    private User preparePatientUser(PatientInfo patientInfo) throws Exception {

        UserDTO userDTO = patientInfo.getUserDTO();

        User user = new User();
        user.setUserId(userDTO.getUserId());

        user.setFirstName(userDTO.getFirstName());
        user.setMiddleName(userDTO.getMiddleName());
        user.setLastName(userDTO.getLastName());
        user.setDateOfBirth(userDTO.getDateOfBirth());
        user.setStreetAddress1(userDTO.getStreetAddress1());
        user.setStreetAddress2(userDTO.getStreetAddress2());
        user.setCity(userDTO.getCity());
        user.setState(userDTO.getState());
        user.setZipCode(userDTO.getZipCode());
        user.setTelephoneNumber(userDTO.getWorkPhone());
        user.setPhoneExt(userDTO.getWorkPhoneExt());
        user.setEmail(userDTO.getEmail());
        user.setFailedAccessAttempts(userDTO.getFailedAccessAttempts());
        user.setCreationDate(userDTO.getCreationDate());
        user.setLastUpdateDate(new Date());
        user.setEulaAccepted(userDTO.getEulaAccepted());
        user.setEulaDate(userDTO.getEulaDate());
        user.setEulaId(userDTO.getEulaId());

        user.setPrefix(userDTO.getPrefix());
        user.setNpi(userDTO.getNpi());
        user.setEnabled(userDTO.getEnabled());
        user.setMustChangePassword('Y');
        user.setDeleted('N');
        user.setReportingRole("Patient");
        user.setCanManagePowerUser(WebConstants.NO);
        return user;
    }

    public void createJames3User(long communityId, String patientUserName) throws Exception {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        String directDomain = configurationDAO.getProperty(communityId, "hisp.james3.direct.domain");

        String james3DirectAddress = createDirectAddress(communityId, patientUserName);
        String password = configurationDAO.getProperty(communityId, "hisp.james3.direct.defaultuserpassword", "password");
        getHispClient(communityId).createAccountForJames3(james3DirectAddress, password, directDomain);

    }

    public String getToken(long communityId) throws Exception {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        String adminUserName = configurationDAO.getProperty(communityId, "sso.server.admin.username");
        String adminPassword = configurationDAO.getProperty(communityId, "sso.server.admin.password");

        //get fresh tokenId for update
          //vpl TODO fix this
        String tokenId = sSOUserDAO.authenticate(communityId, adminUserName, adminPassword);
        tokenId = StringUtils.trimToEmpty(tokenId);
        logger.info("token result from OpenAM:-" + tokenId + "-");

        return tokenId;
    }

    public String createDirectAddress(long communityId, String patientUserName) throws Exception {
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        String directDomain = configurationDAO.getProperty(communityId, "hisp.james3.direct.domain");
        String patientOrgName = configurationDAO.getProperty(communityId, "patientuser.org.name");
        String directAddress = HispClientUtils.generatePatientDirectAddress(patientUserName, directDomain, patientOrgName);
        logger.info("created careteam directAddress  ------------:" + directAddress);
        //create unique direct address
        List<PatientCommunityEnrollment> patientEnrollments = careteamDAO.findPatientEnrollmentWithDirectAddress(directAddress);
        int num = patientEnrollments.size();
        logger.info("createDirectAddress num :" + num);
        if (num >= 1) {
            directAddress = HispClientUtils.generatePatientUniqueDirectAddress(patientUserName, directDomain, patientOrgName, num);
        }
        logger.info("created care directAddress  ------------:" + directAddress);
        return directAddress;
    }

    /**
     * FIXME - how will this work with multitenant?
     *
     * @param communityId
     * @return
     */
    public OrganizationCwd getOrganization(long communityId) {
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        String orgId = configurationDAO.getProperty(communityId, "patientuser.org.id");
        Long patientOrgId = Long.parseLong(orgId);
        String orgName = configurationDAO.getProperty(communityId, "patientuser.org.name");
        String orgOid = configurationDAO.getProperty(communityId, "patientuser.oid");

        logger.info("orgId :" + orgId + ":orgName:" + orgName + ":orgOid:" + orgOid);
        //org id and name 
        OrganizationCwd organization = new OrganizationCwd();
        organization.setOrgId(patientOrgId);
        organization.setOrganizationCwdName(orgName);
        organization.setOid(orgOid);

        return organization;
    }

    /**
     * @param communityId
     * @return
     */
    public CommunityOrganization getCommunityOrganization(long communityId) {
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        Community patientCommunity = new Community();
        String patientCommunityIdStr = configurationDAO.getProperty(communityId, "patientuser.community.id");
        Long patientCommunityId = Long.parseLong(patientCommunityIdStr);
        patientCommunity.setCommunityId(patientCommunityId);

        String communityName = configurationDAO.getProperty(communityId, "patientuser.community.name");
        patientCommunity.setCommunityName(communityName);
        logger.info("communityId :" + patientCommunityId + ":communityName:" + communityName);

        CommunityOrganization communityOrganization = new CommunityOrganization();
        communityOrganization.setCommunity(patientCommunity);

        return communityOrganization;
    }

    protected List<OrganizationCwd> getOrganizations(long communityId) {
        return organizationDAO.findOrganizationEntities(communityId);
    }

    @Override
    public List<OrganizationDTO> getOrganizationDTOs(long communityId) throws PortalException {
        List<OrganizationDTO> orgDTOs = null;
        try {
            List<OrganizationCwd> organizations = this.getOrganizations(communityId);
            orgDTOs = new ArrayList<>(organizations.size());
            logger.fine("mapping orgs to orgDtos for community " + communityId);
            for (OrganizationCwd each : organizations) {
                OrganizationDTO orgDTO = OrgUtils.convertOrgToDTO(each,communityId);
                logger.fine("orgDTO for patientFormReferenceData, " + orgDTO.toString());
                orgDTOs.add(orgDTO);
            }
        } catch (Exception exc) {
            String message = "Error loading organizations.";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }

        return orgDTOs;
    }

    private void setPatientUserIsActive(String status, PatientInfo patientInfo) {

        if (status.equalsIgnoreCase("None") && patientInfo.getPatientUserActive().equalsIgnoreCase("None")) {
            patientInfo.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_NONE);
        } else if (status.equalsIgnoreCase("None") && patientInfo.getPatientUserActive().equalsIgnoreCase("Active")) {
            patientInfo.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_ACTIVE);
        } else if (status.equalsIgnoreCase("Active") && patientInfo.getPatientUserActive().equalsIgnoreCase("Inactive")) {
            patientInfo.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_INACTIVE);
        } else if (status.equalsIgnoreCase("Active") && patientInfo.getPatientUserActive().equalsIgnoreCase("Active")) {
            patientInfo.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_ACTIVE);
        } else if (status.equalsIgnoreCase("Inactive") && patientInfo.getPatientUserActive().equalsIgnoreCase("Active")) {
            patientInfo.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_ACTIVE);
        } else if (status.equalsIgnoreCase("Inactive") && patientInfo.getPatientUserActive().equalsIgnoreCase("Inactive")) {
            patientInfo.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_INACTIVE);
        } else if (status.equalsIgnoreCase("Active") && patientInfo.getPatientUserActive().equalsIgnoreCase("None")) {
            patientInfo.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_INACTIVE);
        }
    }

    public void runPatientUpdateProcessAsync(Person person, PersonDTO personDTO, PatientInfo patientInfo, OrganizationCwd organization, long patientId, String programLevelConsentStatus, LoginResult loginResult, PatientCommunityEnrollment patientEnrollment, long communityCareteamId, String oldCareteamName) throws Exception {

        // send to JMS queue
        logger.info("Begin: runPatientUpdateProcessAsync - Sending message to Coordinator CareTeam JMS Queue");

        CoordinatorBrokerContext coordinatorBrokerContext = CoordinatorBrokerContext.getInstance();

        ConnectionFactory connectionFactory = coordinatorBrokerContext.getConnectionFactory();
        javax.jms.Queue queue = coordinatorBrokerContext.getCareTeamPatientUpdateQueue();

        if (connectionFactory == null) {
            logger.warning(">>> Not sending message. Connection Factory is null.");
            return;
        }

        Connection connection = null;
        Session session = null;
        MessageProducer producer = null;

        try {

            connection = connectionFactory.createConnection();
            session = connection.createSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
            producer = session.createProducer(queue);

            javax.jms.ObjectMessage objectMessage = session.createObjectMessage();
            populateObjectMessage(person, personDTO, objectMessage, patientInfo, organization, patientId, programLevelConsentStatus, loginResult, patientEnrollment, communityCareteamId, oldCareteamName);

            // send message
            logger.info("Sending message to " + queue.getQueueName());
            producer.send(objectMessage);
            logger.info("Finished sending message to " + queue.getQueueName());
        } catch (Exception exc) {
            final String message = "Error sending patient update message to Coordinator CareTeam Patient Update queue";

            logger.logp(Level.SEVERE, getClass().getName(), "updatePatient", message, exc);
        } finally {
            JmsUtils.close(producer, session, connection);
        }

        logger.info("End: runPatientUpdateProcessAsync - Sending message to Coordinator CareTeam JMS Queue");
    }

    @Override
    public void runPatientUpdateProcess(PatientInfo patientInfo, Person person, PersonDTO personDTO, OrganizationCwd organization,
            long patientId, String programLevelConsentStatus,
            LoginResult loginResult, PatientCommunityEnrollment patientEnrollment,
            long communityCareteamId, String oldCareteamName) throws Exception {

        long communityId = loginResult.getCommunityId();

        // synch
        long patientOrgId = -1;
        String patientEnrollStatus
                = patientInfo.getPatientEnrollment().getStatus();  // yeah its a duplicate - burn me

        // get patient org id
        if (organization != null) {
            patientOrgId = organization.getOrgId();
        }

        // synch organization patient consent
        synchOrganizationPatientConsent(this.getPatientEnrollmentDAO(communityId), patientId, communityId, programLevelConsentStatus,
                patientOrgId, patientEnrollStatus, this.getCommunityOrgId(communityId));

        // perform dynamic consent updates on myPatientList
        synchMyPatientList(patientId, communityId);

        // save patient info
        savePatientInfo(person, personDTO, loginResult, patientEnrollment, communityCareteamId, patientInfo, programLevelConsentStatus, oldCareteamName);
    }

    private void populateObjectMessage(Person person, PersonDTO personDTO, ObjectMessage objectMessage, PatientInfo patientInfo, OrganizationCwd organization, long patientId, String programLevelConsentStatus, LoginResult loginResult, PatientCommunityEnrollment patientEnrollment, long communityCareteamId, String oldCareteamName) throws Exception {
        logger.info("populating object message");
        HashMap<String, Object> map = new HashMap<String, Object>();

        map.put(JmsConstants.PATIENT_INFO_KEY, patientInfo);
        map.put(JmsConstants.ORGANIZATION_KEY, organization);
        map.put(JmsConstants.PATIENT_ID_KEY, patientId);
        map.put(JmsConstants.PERSON_KEY, person);
        map.put(JmsConstants.PERSON_DTO_KEY, personDTO);

        map.put(JmsConstants.PROGRAM_LEVEL_CONSENT_STATUS_KEY, programLevelConsentStatus);
        map.put(JmsConstants.LOGIN_RESULT_KEY, loginResult);

        map.put(JmsConstants.PATIENT_ENROLLMENT_KEY, patientEnrollment);
        map.put(JmsConstants.COMMUNITY_CARE_TEAM_ID_KEY, communityCareteamId);
        map.put(JmsConstants.OLD_CARE_TEAM_NAME_KEY, oldCareteamName);

        objectMessage.setObject(map);
    }

    private void synchMyPatientList(long patientId, long communityId) {

        try {
            // handle mypatientlist synch
            ServletContext application = getServletContext();
            EnrollmentPatientUpdater enrollmentPatientUpdater = (EnrollmentPatientUpdater) application.getAttribute(WebConstants.ENROLLMENT_SERVICE_KEY);
            enrollmentPatientUpdater.synchMyPatientList(patientId, communityId);
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "CareTeamService: error synchMyPatientList", exc);
        }
    }

    public List<UserDTO> getUserDTOs(long communityId) throws PortalException {
        List<UserDTO> userDTOs = null;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try { // we already have users in the app context, get it
            ServletContext application = getServletContext();
            Map<Long, List<UserDTO>> usermap = (Map<Long, List<UserDTO>>) application.getAttribute(WebConstants.AVAILABLE_USERS_KEY);
            userDTOs = usermap.get(communityId);
        } catch (Exception exc) {
            String message = "Error loading users.";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }
        stopWatch.stop();
        logger.info("it took " + stopWatch.getTime() / 1000.0 + " getUsers/2070");

        return userDTOs;
    }

    protected List<User> getUsers(long communityId) {

        return userDAO.findUsers(communityId);

    }

    @Override
    public String performPatientEngagement(Long communityId,
            Long patientId, boolean directMessagngRequired) throws Exception {

        boolean isAccountAlreadyCreated = false;
       

        
       
        PatientCommunityEnrollment enrollment = getPatientEnrollmentDAO(
                communityId).
                findPatientEnrollment(patientId, communityId);

        if (!directMessagngRequired) {
            enrollment.setPatientUserActive("Inactive");
            getPatientEnrollmentDAO(communityId).
                    updatePatientEnrollment(enrollment);
        } else if (directMessagngRequired) {

            PatientEngagementRole engagementRole = careteamDAO.
                    findPatientEngagementRole(communityId);
            String patientEngaRole = String.valueOf(engagementRole.
                    getPatientEngagementRolePK().
                    getRoleId());

            // access level
            String accessLevelValue = configurationDAO.getProperty(communityId,
                    "patientuser.accesslevel");
            Long patientAccessLevelID = Long.parseLong(accessLevelValue);
            AccessLevel accessLevel = accessLevelDAO.findAccessLevel(
                    patientAccessLevelID, communityId);
            Long accessLevelId = accessLevel.getAccessLevelPK().
                    getAccessLevelId();

            String userPassword = PasswordUtils.generatePassword();

            SimplePerson simplePerson = this.getPatientManager(communityId).
                    findPatient(patientId);
            String tokenId = this.getToken(communityId);

            OrganizationCwd organization = organizationDAO.findOrganization(
                    enrollment.getOrgId(), communityId);
            Community community = new Community(communityId);

            CommunityOrganization communityOrganization = new CommunityOrganization(
                    community, organization);
            User user = this.preparePatientUser(simplePerson, enrollment);
            UserCommunityOrganization userCommunityOrganization = new UserCommunityOrganization();
            String gender = simplePerson.getGenderCode();
            //String credentials = enrollment.getCredentials();

            userCommunityOrganization = PropertyUtils.populateUserUcoData(user,
                    userCommunityOrganization,
                    communityOrganization.getCommunity(), organization,
                    accessLevel,
                    null, new Date(System.currentTimeMillis()), null, "Active",
                    gender, null, null);

            user.addUserCommunityOrganization(userCommunityOrganization);

            isAccountAlreadyCreated = checkAccountAlreadyCreated(enrollment);
            if (isAccountAlreadyCreated) {
                enrollment.setPatientUserActive("Active");
                enrollment.setLastPasswordChangeDate(new Date());
                getPatientEnrollmentDAO(communityId).
                        updatePatientEnrollment(enrollment);
                user.getUserCommunityOrganization(communityId).
                        setDirectAddress(enrollment.getDirectAddress());
                userService.updatePatientUser(sSOUserDAO,
                        userCommunityOrganizationDAO, userDAO, user,
                        userPassword,
                        communityOrganization, tokenId, organization,
                        patientEngaRole, enrollment.getDirectAddress(),
                        enrollment.getPatientLoginId());
                securityServiceHelper.sendPasswordEmailToPatient(
                        enrollment.
                        getEmailAddress(), enrollment.
                        getPatientLoginId(), userPassword, communityId);
                sSOUserDAO.logout(communityId, tokenId);
            } else {
                String userPatientName = EnrollmentServiceUtils.
                        getNewPatientUserName(simplePerson);

                String patientDirectAddress = this.createDirectAddress(
                        communityId, userPatientName);

                //james3 account
                this.createJames3User(communityId, userPatientName);
                user.getUserCommunityOrganization(communityId).
                        setDirectAddress(patientDirectAddress);

                userService.createPatientUser(sSOUserDAO,
                        userCommunityOrganizationDAO, userDAO,
                        organization, user, userPassword,
                        communityOrganization, tokenId, patientEngaRole,
                        patientDirectAddress, userPatientName);

                enrollment.
                        setDirectAddress(patientDirectAddress);
                enrollment.
                        setRoleID(engagementRole.
                                getPatientEngagementRolePK().
                                getRoleId());

                enrollment.
                        setPatientUserActive("Active"
                        );
                enrollment.setLastPasswordChangeDate(new Date());
                enrollment.
                        setPatientLoginId(userPatientName);
                this.getPatientEnrollmentDAO(communityId).
                        updatePatientEnrollment(enrollment);

                //send mail to patient
                //JIRA 629 - remove properties
                securityServiceHelper.sendPasswordEmailToPatient(
                        enrollment.
                        getEmailAddress(), userPatientName, userPassword,
                        communityId);

                //make sure the admin token created earlier is logged out
                sSOUserDAO.logout(communityId, tokenId);

            }
        }
        return "Success";
    }

    private User preparePatientUser(SimplePerson sp,
            PatientCommunityEnrollment pce) throws Exception {

        User user = new User();
        user.setUserId(pce.
                getPatientId());

        user.setFirstName(sp.getFirstName());
        user.setMiddleName(sp.getMiddleName());
        user.setLastName(sp.getLastName());
        user.setDateOfBirth(sp.getDateOfBirth());
        user.setStreetAddress1(sp.getStreetAddress1());
        user.setStreetAddress2(sp.getStreetAddress2());
        user.setCity(sp.getCity());
        user.setState(sp.getState());
        user.setZipCode(sp.getZipCode());
        user.setTelephoneNumber(sp.getCellPhone());
        user.setPhoneExt(sp.getTelephoneExtension());
        user.setEmail(pce.
                getEmailAddress());
        user.setFailedAccessAttempts(3);
        user.setCreationDate(new Date());
        user.setLastUpdateDate(new Date());
        user.setEulaAccepted('N');
        user.setEulaDate(new Date());
        user.setEulaId(200);
        user.setPrefix("MR");
        user.setNpi("NO");
        user.setEnabled('Y');
        user.setMustChangePassword('Y');
        user.setDeleted('N');
        user.setReportingRole("Patient");
        user.setCanManagePowerUser(WebConstants.NO);

        return user;
    }
    private boolean checkAccountAlreadyCreated(PatientCommunityEnrollment pce) {
        if (pce != null && pce.getDirectAddress() != null && pce.
                getPatientLoginId() != null) {
            return true;

        }
        return false;
    }
}
