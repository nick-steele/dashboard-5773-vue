package com.gsihealth.dashboard.server.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsihealth.dashboard.client.service.MyPatientListService;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.ReferenceData;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.ListTagDTO;
import com.gsihealth.dashboard.entity.dto.MyPatientInfoDTO;
import com.gsihealth.dashboard.entity.dto.PatientEmergencyContactDTO;
import com.gsihealth.dashboard.entity.dto.PatientListLabelDTO;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.dao.CommunityDAO;
import com.gsihealth.dashboard.server.dao.MyPatientListDAO;
import com.gsihealth.dashboard.server.util.MyPatientInfoDTOComparator;
import com.gsihealth.entity.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chad.Darby
 */
public class MyPatientListServiceImpl extends BasePatientService implements MyPatientListService {

    private Logger logger = Logger.getLogger(getClass().getName());
    private MyPatientListDAO myPatientListDAO;
    private MyPatientInfoDTOComparator myPatientInfoDTOComparator = new MyPatientInfoDTOComparator();
    private CommunityDAO communityDAO;

    /**
     * Initializes the service with DAOs
     */
    @Override
    public void init() throws ServletException {

        ServletContext application = getServletContext();
        myPatientListDAO = (MyPatientListDAO) application.getAttribute(WebConstants.MY_PATIENT_LIST_DAO_KEY);
        communityDAO = (CommunityDAO) application.getAttribute(WebConstants.COMMUNITY_DAO_KEY);
        // store a reference to THIS service in application context
        application.setAttribute(WebConstants.MY_PATIENT_LIST_SERVICE_KEY, this);

        super.init();
    }

    @Override
    public void add(MyPatientInfoDTO myPatientInfoDTO) throws PortalException {
        logger.info("Begin: MyPatientListServiceImpl.add");

        try {
            // convert dto to entity
            Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
            MyPatientInfo myPatientInfo = mapper.map(myPatientInfoDTO, MyPatientInfo.class);
            List<ListTagDTO> tags = myPatientInfoDTO.getTags();
            Collection<UserPatientListTags> dbtags = new ArrayList();
            myPatientInfo.setUserPatientListTagsCollection(dbtags);
            for (ListTagDTO tag : tags) {
                UserPatientListTags dbtag = new UserPatientListTags();
                dbtag.setMyPatientList(myPatientInfo);
                ListTags lt = new ListTags(tag.getTagId());
                lt.setTagType(tag.getTagType());
                lt.setTagName(tag.getTagName());
                dbtag.setMyPatientList(myPatientInfo);
                dbtag.setTagId(lt);
                dbtag.setStatus("ENABLED");
                dbtags.add(dbtag);
            }

            // add to db
            myPatientListDAO.add(myPatientInfo);

        } catch (Exception exc) {
            final String message = "Error adding patient to MyList. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "getPatient", message, exc);
            throw new PortalException("Error adding patient to MyList.");
        } finally {
            logger.info("End: MyPatientListServiceImpl.add");
        }
    }

    @Override
    public void update(MyPatientInfoDTO myPatientInfoDTO) throws PortalException {
        try {
            Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
            MyPatientInfo myPatientInfo = mapper.map(myPatientInfoDTO, MyPatientInfo.class);
            List<ListTagDTO> tags = myPatientInfoDTO.getTags();
            Collection<UserPatientListTags> dbtags = new ArrayList();
            myPatientInfo.setUserPatientListTagsCollection(dbtags);
            for (ListTagDTO tag : tags) {
                UserPatientListTags dbtag = new UserPatientListTags();
                ListTags lt = new ListTags(tag.getTagId());
                lt.setTagType(tag.getTagType());
                lt.setTagName(tag.getTagName());
                dbtag.setMyPatientList(myPatientInfo);
                dbtag.setTagId(lt);
                dbtag.setStatus("ENABLED");
                dbtags.add(dbtag);
            }
            // add to db
            myPatientListDAO.update(myPatientInfo);
        } catch (Exception exc) {
            final String message = "Error adding patient to MyList. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "getPatient", message, exc);
            throw new PortalException("Error adding patient to MyList.");
        } finally {
            logger.info("End: MyPatientListServiceImpl.update");
        }
    }

    @Override
    public void updateTags(long userId, long communityId, String type, String id)throws PortalException {

        myPatientListDAO.updateTags(userId, communityId, type , id);

    }

    @Override
    public void delete(MyPatientInfoDTO myPatientInfoDTO) throws PortalException {
        logger.info("Begin: MyPatientListServiceImpl.delete");

        try {
            long userId = myPatientInfoDTO.getUserId();
            long patientId = myPatientInfoDTO.getPatientId();
            long communityId = myPatientInfoDTO.getCommunityId();

            MyPatientInfoPK pk = new MyPatientInfoPK(userId, patientId, communityId);

            // remove from db
            myPatientListDAO.delete(pk);

        } catch (Exception exc) {
            final String message = "Error removing patient from MyList. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "delete", message, exc);
            throw new PortalException("Error removing patient from MyList.");
        } finally {
            logger.info("End: MyPatientListServiceImpl.delete");
        }

    }

    @Override
    public List<MyPatientInfoDTO> getMyPatientList(long userId, long communityId) throws PortalException {

        logger.info("Begin: MyPatientListServiceImpl.getMyPatientList V2");

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        try {
            List<MyPatientInfoDTO> dtoResults = myPatientListDAO.findMyPatientInfoList(userId, communityId);
            Community community = communityDAO.getCommunityById(communityId);
            String oid = community.getCommunityOid();
            for (MyPatientInfoDTO temp : dtoResults) {
                // String euid = temp.getEuid();

                // get patient from mdm
                Person person = new Person(this.getPatientManager(communityId).getPatient(oid, temp.getPatientId()));
                temp.setFirstName(person.getFirstName());
                temp.setLastName(person.getLastName());
                temp.setDob(person.getDateOfBirth());
                temp.setGender(person.getGender());
            }

            // sort the results
            Collections.sort(dtoResults, myPatientInfoDTOComparator);

            stopWatch.stop();

            logger.info("Time to get MyPatientList V2: " + stopWatch.getTime() / 1000.0 + " secs");

            return dtoResults;

        } catch (Exception exc) {
            final String message = "Error getting patients from MyList. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "getMyPatientList", message, exc);
            throw new PortalException("Error getting patients from MyList.");
        } finally {
            logger.info("End: MyPatientListServiceImpl.getMyPatientList");
        }

    }

    @Override
    public String getMyPatientListCount(long userId, long communityId, String level, String tags, List patients) throws PortalException {

        logger.info("Begin: MyPatientListServiceImpl.getMyPatientList V2");

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        try {

            String count = myPatientListDAO.findMyPatientInfoListCount(userId, communityId, level, tags, patients);

            return count;

        } catch (Exception exc) {
            final String message = "Error getting patients from MyList. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "getMyPatientList", message, exc);
            throw new PortalException("Error getting patients from MyList.");
        } finally {
            logger.info("End: MyPatientListServiceImpl.getMyPatientList");
        }

    }

    @Override
    public List<MyPatientInfoDTO> getMyPatientListScroll(long userId, long communityId, long start, long stop, String level, String tags, List patients) throws PortalException {

        logger.info("Begin: MyPatientListServiceImpl.getMyPatientList V2");

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        try {
            List<MyPatientInfoDTO> dtoResults = myPatientListDAO.findMyPatientInfoList(userId, communityId, start, stop, level, tags, patients);
          /*  Community community = communityDAO.getCommunityById(communityId);
            String oid = community.getCommunityOid();



           // for (MyPatientInfoDTO temp : dtoResults) {

                Person person = new Person(this.getPatientManager(communityId).getPatient(oid, temp.getPatientId()));

                temp.setFirstName(person.getFirstName());
                temp.setLastName(person.getLastName());
                temp.setDob(person.getDateOfBirth());
                temp.setGender(person.getGender());
            }


            // sort the results
            //Collections.sort(dtoResults, myPatientInfoDTOComparator);
            stopWatch.stop();

            logger.info("Time to get MyPatientList V2: " + stopWatch.getTime() / 1000.0 + " secs");
            */
            return dtoResults;

        } catch (Exception exc) {
            final String message = "Error getting patients from MyList. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "getMyPatientList", message, exc);
            throw new PortalException("Error getting patients from MyList.");
        } finally {
            logger.info("End: MyPatientListServiceImpl.getMyPatientList");
        }

    }

    public List<ListTagDTO> getListTags(long communityId) throws Exception {
        List<ListTags> results = myPatientListDAO.getListTags(communityId);
        ArrayList<ListTagDTO> dtoResults = new ArrayList();
        for (ListTags temp : results) {

            ListTagDTO dto = new ListTagDTO();
            dto.setTagId(temp.getTagId());
            dto.setTagName(temp.getTagName());
            dto.setTagType(temp.getTagType());
            dto.setTagAbbr(temp.getTagAbbr());
            dtoResults.add(dto);
        }

        return dtoResults;
    }

    @Override
    public PersonDTO getPatient(long patientId, long communityId) throws PortalException {

        try {
            // get euid
            PatientCommunityEnrollment patientEnrollment = this.getPatientEnrollmentDAO(communityId).findPatientEnrollment(patientId);
            String euid = patientEnrollment.getPatientEuid();

            // get patient from mdm
            Person person = new Person(this.getPatientManager(communityId).getPatient(euid));

            person.setPatientCommunityEnrollment(patientEnrollment);

            // convert person to personDTO
            Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
            PersonDTO personDTO = mapper.map(person, PersonDTO.class);
            //Spira 5438 : After conversion from Person to personDto setting date in string format.
            //String will not be changed after GWT RPC call. It will reach as it is on client.
            //Then it can be used on Patient Search Grid, Patient View and patient update.
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            String dobAsString = df.format(person.getDateOfBirth());
            personDTO.setDateOfBirthAsString(dobAsString);

            // Enrollment status changed effective date: SPIRA 4653
            Date enrollStatusChangeDate = patientEnrollmentHistoryDAO.getEnrollmentStatusChangeDate(patientId);
            personDTO.setEnrollmentChangeDate(enrollStatusChangeDate);

            // set patient relationship: SPIRA 4816
            setPatientRelationship(communityId, person, personDTO);

            return personDTO;
        } catch (Exception exc) {
            final String message = "Error retrieving patient. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "getPatient", message, exc);
            throw new PortalException("Error retrieving patient.");
        }
    }

    @Override
    public boolean isAlreadyOnList(long userid, long patientId, long communityId) throws PortalException {

        try {

            return myPatientListDAO.isAlreadyOnList(userid, patientId, communityId);

        } catch (Exception exc) {
            final String message = "Error checking if patient is already on MyList. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "isAlreadyOnList", message, exc);
            throw new PortalException("Error checking if patient is already on MyList.");
        }
    }
    
    /**
     * @param userid
     * @param patientId
     * @param communityId
     * @return dto with settings for patientList label/button
     * @throws PortalException 
     */
    @Override
    public PatientListLabelDTO myPatientListSelectionDisplayOptions(long userid, long patientId, long communityId) throws PortalException {

        try {
          PatientListLabelDTO patientListLabelDTO = new PatientListLabelDTO(false, false);
          boolean hasConsent = this.hasConsent(userid, patientId, communityId);
          
          if(hasConsent) { // only do the work if they have consent
            boolean onList = myPatientListDAO.isAlreadyOnList(userid, patientId, communityId);
            patientListLabelDTO.setShowButtom(!onList);  // not on list, show add button
            patientListLabelDTO.setShowLabel(onList);  //already on the list show label
          }
         
            return patientListLabelDTO;

        } catch (Exception exc) {
            final String message = "Error checking if patient is already on MyList. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "myPatientListSelectionDisplayOptions", message, exc);
            throw new PortalException("Error checking if patient is already on MyList.");
        }
    }

    @Override
    public boolean isListFull(long userid, long communityId) throws PortalException {
        try {

            return myPatientListDAO.isListFull(userid, communityId);

        } catch (Exception exc) {
            final String message = "Error checking if MyList is full. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "isListFull", message, exc);
            throw new PortalException("Error checking if MyList is full.");
        }
    }

    @Override
    public boolean hasConsent(long userid, long patientId, long communityId) throws PortalException {
        try {

            return myPatientListDAO.hasConsent(userid, patientId, communityId);

        } catch (Exception exc) { //TODO what does this specifically have to do with patientList?
            final String message = "Error checking if patient is already on MyList. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "isAlreadyOnList", message, exc);
            throw new PortalException("Error checking if patient is already on MyList.");
        }
    }

    @Override
    public String getConfigurationValue(long communityId, String defaultValue, String key, String subKey) {
        try {
            String configValue = configurationDAO.getProperty(communityId, key);//"homepage.message.bundle");
            ObjectMapper mapper = new ObjectMapper();

            // read JSON from a file
            if(configValue != null && subKey != null && !subKey.trim().isEmpty()) {
                Map<String, String> map = mapper.readValue(
                        configValue,
                        Map.class);
                configValue = map.get(subKey);
            }
            if(configValue == null || configValue.trim().isEmpty()) {
                return defaultValue;
            }
            return configValue;

        }
        catch (Exception e) {
            return defaultValue;
        }
    }

    protected void setPatientRelationship(long communityId, Person tempPerson, PersonDTO personDto) {
        //set relationship
        PatientEmergencyContact patientEmergencyContact = tempPerson.getPatientCommunityEnrollment().getPatientEmergencyContact();

        if (patientEmergencyContact != null) {

            if (patientEmergencyContact.getRelationship() != null) {

                PatientRelationshipCode patientRelationshipCode = findPatientRelationship(communityId, patientEmergencyContact.getRelationship());

                if ((patientRelationshipCode != null) && (StringUtils.isNotBlank(patientRelationshipCode.getValue()))) {

                    PatientEmergencyContactDTO patientEmergencyContactDto = personDto.getPatientEnrollment().getPatientEmergencyContact();
                    patientEmergencyContactDto.setPatientRelationshipDescription(patientRelationshipCode.getValue());
                    patientEmergencyContactDto.setPatientRelationshipId(patientRelationshipCode.getPatientRelationshipCodePK().getId());
                }
            }
        }
    }

    protected PatientRelationshipCode findPatientRelationship(long communityId, Long relationshipId) {

        Map<Long, String> patientRelationshipMap = getPatientRelationshipMap(communityId);

        PatientRelationshipCode patientRelationshipCode = new PatientRelationshipCode();
        PatientRelationshipCodePK patientRelationshipCodePK = new PatientRelationshipCodePK();
        patientRelationshipCodePK.setId(relationshipId);

        patientRelationshipCode.setPatientRelationshipCodePK(patientRelationshipCodePK);
        patientRelationshipCode.setValue(patientRelationshipMap.get(relationshipId));

        return patientRelationshipCode;
    }

    private Map<Long, String> getPatientRelationshipMap(Long communityId) {

        ServletContext application = getServletContext();

        // get reference data map of all community ids
        Map<Long, ReferenceData> referenceDataMap = ((Map<Long, ReferenceData>) application.getAttribute(WebConstants.REFERENCE_DATA_KEY));

        // get the reference data for this user's community id
        ReferenceData referenceData = (ReferenceData) referenceDataMap.get(communityId);

        return referenceData.getPatientRelationship();
    }

    public ListTagDTO getTagByType(long communityId, String type) throws Exception {
        ListTags tag = myPatientListDAO.getTagByType(communityId, type);

        if (tag == null)
            return null;

        ListTagDTO dto = new ListTagDTO();
        dto.setTagId(tag.getTagId());
        dto.setTagName(tag.getTagName());
        dto.setTagType(tag.getTagType());
        dto.setTagAbbr(tag.getTagAbbr());

        return dto;
    }
}
