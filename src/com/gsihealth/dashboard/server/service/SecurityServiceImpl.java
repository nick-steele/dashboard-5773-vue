package com.gsihealth.dashboard.server.service;

import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.gsihealth.dashboard.common.Constants;
import com.gsihealth.dashboard.common.PortalException;
import java.util.logging.Logger;

import com.gsihealth.dashboard.client.service.SecurityService;
import com.gsihealth.dashboard.common.util.AppConfigConstants;
import com.gsihealth.dashboard.server.util.*;
import com.gsihealth.entity.*;
import com.gsihealth.dashboard.entity.dto.ApplicationDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.ApplicationDAO;
import com.gsihealth.dashboard.server.dao.AuditDAO;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.dashboard.server.dao.SSOUserDAO;
import com.gsihealth.dashboard.server.dao.UserCommunityOrganizationDAO;
import com.gsihealth.dashboard.server.dao.UserDAOImpl;

import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.persistence.NoResultException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import static javax.ws.rs.core.HttpHeaders.USER_AGENT;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;
import com.gsihealth.jwt.JWT;
import com.gsihealth.jwt.PojoUser;

/**
 * @author Vishal (Off.)
 *
 */
@RemoteServiceRelativePath("SecurityService")
public class SecurityServiceImpl extends BasePortalServiceServlet implements SecurityService {

    private static final int MAX_DAYS_SINCE_LAST_LOGIN = 90;
    private static final int MAX_DAYS_SINCE_LAST_PASSSWORD_CHANGE = 90;
    private static final int MAX_FAILED_ACCESS_ATTEMPTS = 5;
    private Logger logger = Logger.getLogger(getClass().getName());
    private SecurityServiceHelper securityServiceHelper;
    private AuditDAO auditDAO;
    private String realm = null;
    private JWT jwt;
    /**
     * Initializes the service with DAOs
     */
    @Override
    public void init() throws ServletException {
        logger.info("loading securityServiceImpl");
        ServletContext application = getServletContext();
        super.init();
        // Retrieve the DAOs. they were created in the DataContextListener class
        // and stored in the servlet context
        userDAO = (UserDAOImpl) application.getAttribute(WebConstants.USER_DAO_KEY);
        setUserDAO(userDAO);

        setSSOUserDAO((SSOUserDAO) application.getAttribute(WebConstants.SSO_USER_DAO_KEY));

        setApplicationDAO((ApplicationDAO) application.getAttribute(WebConstants.APPLICATION_DAO_KEY));

        securityServiceHelper = new SecurityServiceHelper(userDAO, sSOUserDAO);

        auditDAO = (AuditDAO) application.getAttribute(WebConstants.AUDIT_DAO_KEY);
        setUserCommunityOrganizationDAO((UserCommunityOrganizationDAO) application
                .getAttribute(WebConstants.USER_COMMUNITY_ORGANIZATION_DAO_KEY));
        jwt = new JWT();
        logger.info("securityServiceImpl loaded");
    }

    @Override
    public LoginResult authenticate(String claimToken) {

        logger.info("starting authentication with token");

        String key = "alcatraz";    //FIXME!!
        StringBuilder ssoTokenBf = new StringBuilder();
        
        PojoUser pojoUser = null;
        try {
            pojoUser = jwt.parseJWT(claimToken, key, ssoTokenBf);
        }catch(Exception e){
            logger.log(Level.WARNING, "Could not parse JWT token: "+ claimToken, e);
            LoginResult loginResult = new LoginResult();
            loginResult.setAuthenticated(false);
            loginResult.setErrorMessage("Error occurred during authentication.");
            return loginResult;
        }

        return authenticate(
                pojoUser.getUser(),
                pojoUser.getPassword(),
                pojoUser.getCommunityId(),
                ssoTokenBf.toString()
        );

    }
    @Override
    public LoginResult authenticate(String email, String password, Long communityId, String ssoToken) {




        logger.info("starting authenticate():  email=" + email);

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        timestamp("starting authenticate", email, stopWatch);

        // ######## START 11 queries & ~200ms...
        String errorMessage = null;

        LoginResult loginResult = new LoginResult();

        try {
            // Get user
            User user = null;
            UserCommunityOrganization uco = null;
            OrganizationCwd organization = null;
            CommunityOrganization communityOrganization = null;

            try {

                ServletContext application = getServletContext();

                // convert email address to lowercase
                email = email.toLowerCase();

                // 1.Find User by portal id.
                logger.info("debug-x userDAO.findUserByEmail");
                user = userDAO.findUserByEmail(email, communityId);
                logger.info("debug-x user.getUserCommunityOrganization");
                uco = user.getUserCommunityOrganization(communityId);

                organization = uco.getOrganizationCwd();

                logger.info("debug-x organization.getCommunityOrganization");
                communityOrganization = organization.getCommunityOrganization(communityId);
                loginResult.setEmail(user.getEmail());
                loginResult.setSpecial(password);
                loginResult.setDirectAddress(uco.getDirectAddress());
                logger.info("debug-x user.getAuthyId");
                int authId = (user.getAuthyId() == null) ? 0 : user.getAuthyId().intValue();
                loginResult.setAuthyId(authId);
                loginResult.setDisableUserMfa(user.getDisableUserMfa());
                loginResult.setLastLoginDate(user.getLastSuccessfulLoginDate());

                loginResult.setUserLevel(uco.getRole().getRoleName());

                loginResult.setUserLevelId(uco.getRole().getPrimaryRoleId());

                loginResult.setAccessLevel(uco.getAccessLevel().getAccessLevelDesc());

                loginResult.setAccessLevelId(uco.getAccessLevel().getAccessLevelPK().getAccessLevelId());
                loginResult.setUserId(user.getUserId());
                loginResult.setCanManagePowerUser(user.getCanManagePowerUser());
                loginResult.setReportingRole(user.getReportingRole());

                logger.info("debug-x done-x");
                /*
				
				This query...
				
				SELECT COMMUNITY_ID, COMMUNITY_NAME, COMMUNITY_OID FROM connect.community WHERE (COMMUNITY_ID = ?)
				
				...is performed for each community Id.
				
                 */

                // ######## END 11 queries & ~200ms ^^ 
                logger.info("REPORTING_ROLE=" + user.getReportingRole() + "--");

                String organizationName = organization.getOrganizationCwdName();
                loginResult.setOrganizationName(organizationName);

                logger.info("debug-x mark-1");
                long organizationId = organization.getOrgId();
                loginResult.setOrganizationId(organizationId);

                logger.info("debug-x mark-2");
                String oid = organization.getOid();
                loginResult.setOid(oid);

                logger.info("==== Performance set loginResult for " + email + " communityId to " + communityId);
                loginResult.setCommunityId(communityId);

                // Community OID
                String communityOid = uco.getCommunity().getCommunityOid();
                logger.info("set loginResult for " + email + " community OID to " + communityOid);
                loginResult.setCommunityOid(communityOid);
                loginResult.setCommunityName(uco.getCommunity().getCommunityName());
                logger.info("debug-x mark-3");

                boolean hasEula = getEula(user);
                loginResult.setEula(hasEula);
                timestamp("1/4 way through authenticate", email, stopWatch);
                timestamp("before getSamsha", email, stopWatch);
                boolean hasSamhsa = getSamhsa(user, communityId);
                logger.info("debug-x mark-4");
                loginResult.setSamhsa(hasSamhsa);
                timestamp("after getSamsha", email, stopWatch);
                logger.info("debug-x mark-5");

                // DASHBOARD - 716 MFA
                String countryCode = getCountryCode(user);
                loginResult.setCountryCode(countryCode);
                logger.info("debug-x mark-6");

                loginResult.setLastMfaVerificationDate(user.getLastMfaVerificationDate());
                logger.info("debug-x mark-7");

                // DASHBOARD-780 Children's Health Home
                loginResult.setCanConsentEnrollMinors(uco.getRole().isCanConsentEnrollMinors());
                logger.info("debug-x mark-8");

                loginResult.setCanViewOrgPP(uco.getRole().isCanViewOrgPatientPanel());
                logger.info("debug-x mark-9");

                ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
                logger.info("debug-x mark-10");

                // Redux
                int userInterfaceType = (user.getUserInterfaceType() == null) ? 0
                        : Integer.parseInt(user.getUserInterfaceType());
                int comUserInt = Integer.parseInt(configurationDAO.getProperty(communityId, "user.interfacetype", "0"));
                logger.info("debug-x mark-11");

                // If the client doesn't allow Redux...
                if (comUserInt == 0) {
                    userInterfaceType = 0;
                } else // If the client forces Redux...
                 if (comUserInt == 1) {
                        userInterfaceType = 1;
                    } else {
                        // If the client allows user choosing of Redux...
                        userInterfaceType = userInterfaceType + (comUserInt * 256);
                    }
                loginResult.setUserInterfaceType(userInterfaceType);
                logger.info("debug-x mark-12");

                // set user settings
                loginResult.setSettings(user.getSettings());
                logger.info("debug-x mark-13");

                // replaces SSO_LOGOUT_URL_KEY
                String logoutUrl = configurationDAO.getProperty(communityId, "sso.logout.url");
                loginResult.setLogoutUrl(logoutUrl);
                logger.info("debug-x mark-14");

                // Treat Restful SSO - Treat Fedlets replaces old
                // SSO_TREAT_URL_KEY
                String treatUrl = this.getAppUrl(applicationDAO, AppConfigConstants.CAREPLAN_APP_ID, communityId);
                loginResult.setTreatUrl(treatUrl);
                logger.info("debug-x mark-15");

                // replace SSO_TREAT_FEATURES_KEY
                String treatFeatures = configurationDAO.getProperty(communityId, "sso.treat.features");
                loginResult.setTreatFeatures(treatFeatures);
                logger.info("debug-x mark-16");

                // replace SSO_TREAT_NAME_KEY
                String treatName = configurationDAO.getProperty(communityId, "sso.treat.name");
                loginResult.setTreatName(treatName);
                logger.info("debug-x mark-17");

                // replace TREAT_RESTFUL_SSO_ENABLED_KEY
                boolean treatRestfulSSOEnabled = this.isAppRestfulSSOEnabled(applicationDAO,
                        AppConfigConstants.CAREPLAN_APP_ID, communityId);
                loginResult.setTreatRestfulSSOEnabled(treatRestfulSSOEnabled);
                logger.info("debug-x mark-18");

                // replace SSO_AVADO_URL_KEY
                String avadoUrl = getAppUrl(applicationDAO, AppConfigConstants.PATIENT_ENGAGEMENT_APP_ID, communityId);
                loginResult.setAvadoUrl(avadoUrl);
                logger.info("debug-x mark-19");

                // replace SSO_AVADO_FEATURES_KEY
                String avadoFeatures = configurationDAO.getProperty(communityId, "sso.avado.features");
                loginResult.setAvadoFeatures(avadoFeatures);
                logger.info("debug-x mark-20");

                // replace SSO_AVADO_NAME_KEY
                String avadoName = configurationDAO.getProperty(communityId, "sso.avado.name");
                loginResult.setAvadoName(avadoName);
                logger.info("debug-x mark-21");

                // replace SSO_AVADO_USEPOST_KEY
                String avadoUsePost = configurationDAO.getProperty(communityId, "sso.avado.usepost");
                loginResult.setAvadoUsePost(avadoUsePost);
                logger.info("debug-x mark-22");

                // replace SSO_AVADO_USER_LIST_KEY
//				String avadoUserList = configurationDAO.getProperty(communityId, "sso.avado.user.list", "");
//				loginResult.setAvadoUserList(avadoUserList);
                // replace SSO_SESSION_URL_KEY
                String sessionUrl = configurationDAO.getProperty(communityId, "sso.session.url");
                loginResult.setSessionUrl(sessionUrl);
                logger.info("debug-x mark-23");

                // replace SSO_SESSION_FEATURES_KEY
                String sessionFeatures = configurationDAO.getProperty(communityId, "sso.session.features");
                loginResult.setSessionFeatures(sessionFeatures);
                logger.info("debug-x mark-24");

                // replace SSO_SESSION_NAME_KEY
                String sessionName = configurationDAO.getProperty(communityId, "sso.session.name");
                loginResult.setSessionName(sessionName);
                logger.info("debug-x mark-25");

                // replace SSO_SESSION_GOTO_KEY
                String sessionGoto = configurationDAO.getProperty(communityId, "sso.session.goto");
                loginResult.setSessionGoto(sessionGoto);
                logger.info("debug-x mark-26");

                // replace SSO_PENTAHO_SERVER_KEY
                String ssoPentahoServer = configurationDAO.getProperty(communityId, "sso.pentaho.server");
                loginResult.setSsoPentahoServer(ssoPentahoServer);
                logger.info("debug-x mark-27");

                String pentahoLandingPage = configurationDAO.getProperty(communityId, "pentaho.landing.page");
                loginResult.setPentahoLandingPage(pentahoLandingPage);
                logger.info("debug-x mark-28");

                // get openAMinstance
                URLGenerator urlGenerator = new URLGenerator(communityId);
                URI authenticateURI = urlGenerator.getAuthenticateURI();
                realm = urlGenerator.getRealm();
                String openAMinstance = authenticateURI.toString().replaceAll("authenticate", "").replace('?', ' ');
                openAMinstance = openAMinstance.trim();
                loginResult.setOpenAMinstance(openAMinstance);
                logger.info("debug-x mark-29");

                // replace CAREBOOK_SUMMARY_URL_KEY
                String carebookSummaryURL = configurationDAO.getProperty(communityId, "sso.carebook.summary.URL");
                loginResult.setCarebookSummaryURL(carebookSummaryURL);
                logger.info("debug-x mark-30");

                // replace DISENROLLMENT_CODE_TITLE
                String disenrollmentCodeTitle = configurationDAO.getProperty(communityId, "disenrollment.code.title");
                loginResult.setDisenrollmentCodeTitle(disenrollmentCodeTitle);
                logger.info("debug-x mark-31");

                // replace PLCS_TITLE
                String plcsTitle = configurationDAO.getProperty(communityId, "plcs.title");
                loginResult.setPlcsTitle(plcsTitle);
                logger.info("debug-x mark-32");

                // Get map of all applications in Application Table
                List<Application> allApps = (List<Application>) application
                        .getAttribute(WebConstants.APPLICATION_LIST_KEY);
                List<ApplicationDTO> allAppDTOs = getAllApps(allApps);
                loginResult.setAllApps(allAppDTOs); // FIXME WHAT IS THIS FOR?
                logger.info("debug-x mark-33");

                // store login result in session. it will be used later for
                // auditing
                timestamp("before storeinSession", email, stopWatch);
                storeInSession(loginResult);
                storeInSession(password);
                storeUserInSession(user, loginResult.getCommunityId());
                timestamp("after storeinSession", email, stopWatch);
                logger.info("debug-x mark-34");

                // POLLING INTERVAL
                int pollingIntervalInSeconds = Integer
                        .parseInt(configurationDAO.getProperty(communityId, "polling.interval.in.seconds", "300"));
                loginResult.setPollingIntervalInSeconds(pollingIntervalInSeconds);
                logger.info("debug-x mark-35");

                boolean pollingEnabled = Boolean
                        .parseBoolean(configurationDAO.getProperty(communityId, "polling.enabled", "true"));
                logger.info("pollingEnabled=" + pollingEnabled);
                loginResult.setPoolingEnabled(pollingEnabled);
                logger.info("debug-x mark-36");

                logger.info("polling.interval.in.seconds=" + pollingIntervalInSeconds);

                timestamp("finish setting loginResult", email, stopWatch);
                logger.info("debug-x mark-37");

                //County field By Default it is true
                boolean countyEnabled = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "county.enabled","true"));
                logger.info("countyEnabled=" + countyEnabled);
                boolean countyRequired = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "county.required","true"));
                logger.info("countyRequired=" + countyRequired);
                loginResult.setCountyEnabled(countyEnabled);
                loginResult.setCountyRequired(countyRequired);

            } catch (NoResultException exc) {
                errorMessage = "Invalid user id: " + email;
                logger.log(Level.WARNING, errorMessage);
                loginResult.setAuthenticated(false);
                loginResult.setErrorMessage("Invalid UserID/Password.");
                long theCommunityId = loginResult.getCommunityId();
                audit(email, errorMessage, AuditType.USER_LOGGED_IN, false, theCommunityId);
                logger.info("debug-x mark-38");

                return loginResult;
            }

            timestamp("starting second part of  authentication", email, stopWatch);

            Integer failedAccessAttempts = user.getFailedAccessAttempts();

            if (failedAccessAttempts == null) {
                failedAccessAttempts = 0;
            }
            logger.info("debug-x mark-39");

            // add other data
            loginResult.setFirstName(user.getFirstName());
            loginResult.setLastName(user.getLastName());
            logger.info("debug-x mark-40");

            // check password
            timestamp("before calling OpenAM", email, stopWatch);

            if(StringUtils.isEmpty(ssoToken)) throw new IllegalArgumentException("SSO Token is null - user: "+ email);

            boolean authenticated = isAuthenticated(communityId, ssoToken);

            timestamp("after calling OpenAM", email, stopWatch);
            logger.info("debug-x mark-41");

            if (authenticated) {
                loginResult.setAuthenticated(true);
                // save tokenId for later
                loginResult.setToken(ssoToken);
                logger.info("Save tokenId in Login Result: -" + loginResult.getToken() + "-, user: " + email + ", commId: "+ communityId);

                // pass to /war/login/post_for_Avado.jsp
                storeTokenIdInSession(ssoToken);
                logger.info("debug-x mark-42");

                if (user.isDeleted()) {
                    loginResult.setErrorMessage("Your account is deleted. Please contact Administrator.");
                    loginResult.setAuthenticated(false);

                    audit(user, loginResult, "Account is deleted", AuditType.USER_LOGGED_IN, false);
                    logger.info("debug-x mark-43");

                    return loginResult;
                }

                if (!uco.getStatus().equals("ACTIVE")) {
                    loginResult.setErrorMessage("Your account is inactive. Please contact Administrator.");
                    loginResult.setAuthenticated(false);
                    audit(user, loginResult, "Account is inactive", AuditType.USER_LOGGED_IN, false);
                    logger.info("debug-x mark-44");

                    return loginResult;
                }

                //	if (!organization.getStatus().equalsIgnoreCase("ACTIVE")) {
                if (!communityOrganization.getStatus().equalsIgnoreCase("ACTIVE")) {
                    loginResult.setErrorMessage(
                            "Your account has been disabled due to the inactivation of your Organization.");
                    loginResult.setAuthenticated(false);
                    audit(user, loginResult, "Account is disabled to inactivation of organization",
                            AuditType.USER_LOGGED_IN, false);
                    logger.info("debug-x mark-45");
                    return loginResult;
                }

                // check days since last login
                int daysSinceLastLogin = getDaysSinceLastLogin(user);
                logger.info("debug-x mark-46");

                if (daysSinceLastLogin > MAX_DAYS_SINCE_LAST_LOGIN) {
                    loginResult.setErrorMessage(
                            "Your account has been disabled. The maximum days since last login has exceeded "
                            + MAX_DAYS_SINCE_LAST_LOGIN
                            + " days. Please contact your Local Administrator to enable your account.");
                    loginResult.setAuthenticated(false);
                    audit(user, loginResult, "Account is disabled. Maximum days since last login has exceeded "
                            + MAX_DAYS_SINCE_LAST_LOGIN + " days", AuditType.USER_LOGGED_IN, false);
                    return loginResult;
                }

                // check days since last password change
                int daysSinceLastPasswordChanged = getDaysSinceLastChange(user.getLastPasswordChangeDate(), new Date());
                if (daysSinceLastPasswordChanged > MAX_DAYS_SINCE_LAST_PASSSWORD_CHANGE) {
                    user.setMustChangePassword(WebConstants.YES);
                }

                // Handle for password reset
                if (user.isMustChangePassword()) {
                    loginResult.setErrorMessage("You must change your password.");
                    loginResult.setAuthenticated(true);
                    loginResult.setMustChangePassword(true);
                }
                logger.info("debug-x mark-47");

                // reset failed access attempts back to zero
                user.setFailedAccessAttempts(0);
                user.setLastSuccessfulLoginDate(new Date());
                timestamp("before userDAO update", email, stopWatch);
                userDAO.update(user);
                timestamp("after userDAO update", email, stopWatch);
                long userId = user.getUserId();
                logger.info("debug-x mark-48");

                // get a list of app keys for this user
                //
                // THIS IS LEGACY
                timestamp("before get Legacy Apps list", email, stopWatch);
                List<String> appKeys = getUserApplicationNames(userId);
                loginResult.setUserAppKeys(appKeys);
                logger.info("debug-x mark-49");

                // THIS IS NEW
                timestamp("after Legacy, before getUserApps list", email, stopWatch);
                List<ApplicationDTO> apps = getUserApps(userId, communityId);
                loginResult.setUserApps(apps);
                logger.info("debug-x mark-50");

                timestamp("after getUserApps, before audit log", email, stopWatch);
                audit(user, loginResult, "Log in successful.", AuditType.USER_LOGGED_IN);

                timestamp("after audit log,finished 2nd part of authentication", email, stopWatch);
                logger.info("debug-x mark-51");

            } else if (!uco.getStatus().equals("ACTIVE")) {
                loginResult.setErrorMessage("Your account is disabled. Please contact Administrator.");
                loginResult.setAuthenticated(false);
                audit(user, loginResult, "Account is disabled", AuditType.USER_LOGGED_IN, false);
                return loginResult;
            } else if (failedAccessAttempts < MAX_FAILED_ACCESS_ATTEMPTS) {
                Date today = new Date();
                logger.info("User last update: " + user.getLastUpdateDate());
                boolean resetFailedAccessAttempts = isMoreThan24hours(user.getLastUpdateDate(), today);
                logger.info("Reset fail access attempts:" + resetFailedAccessAttempts);

                if (resetFailedAccessAttempts) {
                    failedAccessAttempts = 0;
                }

                failedAccessAttempts++;
                loginResult.setErrorMessage("Invalid password. Failed attempt: " + failedAccessAttempts + "\n"
                        + " Your log-in will be disabled on the 5th failed attempt.");
                user.setFailedAccessAttempts(failedAccessAttempts);
                userDAO.update(user);
                logger.info("debug-x mark-52");

                audit(user, loginResult, "Invalid password. Failed attempt: " + failedAccessAttempts,
                        AuditType.USER_LOGGED_IN, false);
            } else {
                loginResult.setErrorMessage(
                        "Login Failed: Account Disabled Failed Login \n attempt: 5. Contact your administrator");
                user.setFailedAccessAttempts(0);
                uco.setStatus("INACTIVE");
                userDAO.update(user);
                logger.info("debug-x mark-53");

                audit(user, loginResult, "Login Failed: Account Disabled Failed Login \n attempt: 5",
                        AuditType.USER_LOGGED_IN, false);
            }

        } catch (Exception exc) {
            logger.info("debug-x mark-54");

            final String message = "Error during authentication: " + email;
            logger.logp(Level.SEVERE, getClass().getName(), "authenticate", message, exc);
            loginResult.setAuthenticated(false);
            loginResult.setErrorMessage(message);
        }
        logger.info("debug-x mark-55");

        logger.info("loginResult.isAuthenticated : " + loginResult.isAuthenticated() + ":CommunityId:"
                + loginResult.getCommunityId());

        timestamp("ready to exit authenticate()", email, stopWatch);
        stopWatch.stop();
        logger.info("debug-x mark-56");

        // 4.return authenticated user
        return loginResult;
    }

    public void timestamp(String message, String email, StopWatch stopWatch) {

        logger.info("==== Performance timestamp: " + message + " for " + email + ": " + stopWatch.getTime() / 1000.0 + " secs ====");
    }

    // JIRA 1122
    @Override
    public void logout(String portalUserId, String tokenId, Long communityId) {
        try {

            sSOUserDAO.logout(communityId, tokenId);
            // logoutPentaho(portalUserId, special, pentaho);
        } catch (Exception exc) {
            final String message = "Error during logout: " + portalUserId;
            logger.logp(Level.SEVERE, getClass().getName(), "logout", message, exc);
        } finally {
            // even in case of exceptions, just log the exception, but make sure
            // the session is invalidated
            HttpServletRequest request = getThreadLocalRequest();
            HttpSession session = request.getSession();
            if (null != session) {
                session.invalidate();
            }
            logger.info("User logged out:" + portalUserId);
        }
    }

    /*
	 * private void logoutPentaho(String portalUserId, String special, String
	 * pentaho) { String theUrl = pentaho + "/pentaho/Logout"; theUrl +=
	 * "?userId=" + portalUserId + "&password=" + special;
	 * System.out.println(theUrl); try { URL obj = new URL(theUrl);
	 * HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	 * 
	 * con.setRequestMethod("GET");
	 * System.out.println("Sending 'GET' request to URL : " + theUrl); int
	 * responseCode = con.getResponseCode();
	 * System.out.println("Response Code : " + responseCode); } catch (Exception
	 * x) { x.printStackTrace(); } }
     */
    private String getCountryCode(User user) {

        String countryCode = null;
        if (user.getMobileNumber() != null && user.getMobileNumber().length() != 10) {
            String number = user.getMobileNumber();
            String mobileNo = number.substring(number.length() - 10, number.length());
            countryCode = number.replace(mobileNo, "");

        }
        return countryCode;
    }

    /**
     * Simply validates ssoToken(OpenAM token) sent from UserManager via JWT.
     */
    protected boolean isAuthenticated(long communityId,  String ssoToken) {
        try {
            return sSOUserDAO.validate(communityId, ssoToken);

        } catch (Exception e) {
            logger.info("isAuthenticated failed");
            logger.info("Error validating OpenAM Token: " + e);
            logger.logp(Level.SEVERE, getClass().getName(), "authenticate", "Error authenticating", e);
            return false;
        }
    }

    protected boolean isMoreThan24hours(Date startDate, Date endDate) {

        int daysElapsed = DateUtils.daysBetween(startDate, endDate);

        logger.info("Days Elapsed: " + daysElapsed);

        return (daysElapsed >= 1);
    }

    private int getDaysSinceLastLogin(User user) {
        Date now = new Date();
        Date lastLoginDate = user.getLastSuccessfulLoginDate();
        int daysSinceLastLogin = DateUtils.daysBetween(lastLoginDate, now);
        return daysSinceLastLogin;
    }

    private void storeInSession(LoginResult loginResult) {
        HttpServletRequest request = getThreadLocalRequest();
        HttpSession session = request.getSession();
        session.setAttribute(WebConstants.LOGIN_RESULT_KEY, loginResult);
    }

    private boolean getEula(User user) {
        return user.getEulaAccepted() != null && user.getEulaAccepted() == Constants.YES_CHAR;
    }

    private boolean getSamhsa(User user, long communityId) {
        return user.getUserCommunityOrganization(communityId).getSamhsaAccepted() != null && user.getUserCommunityOrganization(communityId).getSamhsaAccepted().charValue() ==  Constants.NO_CHAR;
    }

    /**
     * Returns true if we have a valid portal user id
     *
     * @param portalUserId
     * @return
     */
    protected boolean isValidPortalUserId(String portalUserId, Long communityId) {

        boolean isValid = false;

        try {
            User user = userDAO.findUserByEmail(portalUserId, communityId);
            isValid = true;
        } catch (NoResultException exc) {
            isValid = false;
        }

        return isValid;
    }

    /**
     * User forgot their password. Create a new one and send to them.
     *
     * @param email
     * @param communityId
     * @param portalUserId
     * @return 
     * @throws com.gsihealth.dashboard.common.PortalException 
     */
    public boolean forgotPassword(String email,long communityId) throws PortalException {

        // convert email address to lower case
        email = email.toLowerCase();

        try {
            HttpServletRequest request = getThreadLocalRequest();
            // JIRA 629 - remove properties
            boolean isValid = securityServiceHelper.handleForgotPassword(email, request, communityId);

            return isValid;
        } catch (Exception exc) {
            String message = "Error sending email to: " + email + ". Make sure this is a valid email address.";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }
    }

    @Override
    public String changePassword(String email, String password) throws PortalException {

        try {

            // convert password to lower case
            email = email.toLowerCase();

            // get the user
            Long communityId = this.getCommunityIdFromSession();
            User user = userDAO.findUserByEmail(email, communityId);

            // set mustChangePassword
            user.setMustChangePassword(Constants.NO_CHAR);
            user.setAccountLockedOut(Constants.NO_CHAR);

            // update last password change
            user.setLastPasswordChangeDate(new Date());

            // update user in database
            userDAO.update(user);

            //JIRA 1853
            /*
             * UserManager now takes care of updating password across communities 
             * for an user. So, there is no need for us to do the looping.
             * One call is enough to take care of the same.
             */
            //for (UserCommunityOrganization uco : user.getUserCommunityOrganization()) {
            //JIRA 629 - remove properties
            return securityServiceHelper.changePassword(email, password, communityId, getTokenFromSession());
            //}

        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error changing password: " + email, exc);
            throw new IllegalArgumentException("Error changing password: " + email);
        }
    }

    private void storeUserInSession(User user, Long communityId) {
        HttpServletRequest request = getThreadLocalRequest();
        HttpSession session = request.getSession();
        session.setAttribute(WebConstants.USER_KEY, user);
        session.setAttribute(WebConstants.USER_ID_KEY, user.getEmail());
        session.setAttribute(WebConstants.REPORTING_ROLE_KEY, user.getReportingRole());
        session.setAttribute(WebConstants.USER_COMMUNITY_KEY, communityId);

    }

    private void storeTokenIdInSession(String tokenId) {
        HttpServletRequest request = getThreadLocalRequest();
        HttpSession session = request.getSession();
        session.setAttribute(WebConstants.SSO_TOKEN_KEY, tokenId);

    }

    private void storeInSession(String portalPassword) {
        HttpServletRequest request = getThreadLocalRequest();
        HttpSession session = request.getSession();
        session.setAttribute(WebConstants.PW_KEY, portalPassword);
    }

    private void audit(User user, LoginResult loginResult, String comment, String auditType) {
        audit(user, loginResult, comment, auditType, true);
    }

    private void audit(String email, String comment, String auditType, long communityId) {

        audit(email, comment, auditType, true, communityId);
    }

    private void audit(String email, String comment, String auditType, boolean actionSuccess, long communityId) {
        try {
            Audit audit = new Audit();
            audit.setUserEmail(email);
            audit.setActionComments(comment);
            audit.setActionType(auditType);
            audit.setCommunityId(communityId);

            String success = actionSuccess ? com.gsihealth.dashboard.common.Constants.YES
                    : com.gsihealth.dashboard.common.Constants.NO;
            audit.setActionSuccess(success);

            auditDAO.addAudit(audit);
        } catch (Exception exc) {
            String message = "Error AUDITING: " + exc.getMessage();
            logger.log(Level.SEVERE, message, exc);
        }

    }

    private void audit(User user, LoginResult loginResult, String comment, String auditType, boolean actionSuccess) {

        long communityId = loginResult.getCommunityId();
        try {
            Audit audit = AuditUtils.convertUser(loginResult, user);
            audit.setActionComments(comment);
            audit.setActionType(auditType);
            audit.setCommunityId(communityId);

            String success = actionSuccess ? com.gsihealth.dashboard.common.Constants.YES
                    : com.gsihealth.dashboard.common.Constants.NO;
            audit.setActionSuccess(success);

            auditDAO.addAudit(audit);
        } catch (Exception exc) {
            String message = "Error AUDITING user: " + user.getEmail() + ",   " + exc.getMessage();
            logger.log(Level.SEVERE, message, exc);
        }
    }

    private boolean isAppRestfulSSOEnabled(ApplicationDAO applicationDAO, long applicationId, long communityID) {

        // long
        // communityId=com.gsihealth.dashboard.server.dao.Constants.GSI_COMMUNITY_ID;
        Application theApp = applicationDAO.findApplication(applicationId, communityID);

        boolean isSsoEnabled = theApp.getIsSsoEnabled();

        return isSsoEnabled;
    }

    private String getAppUrl(ApplicationDAO applicationDAO, long applicationId, long communityID) {

        // long
        // communityId=com.gsihealth.dashboard.server.dao.Constants.GSI_COMMUNITY_ID;
        Application theApp = applicationDAO.findApplication(applicationId, communityID);

        String appUrl = theApp.getExternalApplicationUrl();

        return appUrl;
    }

    // work around for comminity id
    private List<ApplicationDTO> getUserApps(long userId, long communityID) {

        logger.info(" getUserApps ***** userId " + userId + " communityID..." + communityID);
        // get from database
        List<Application> apps = applicationDAO.getUserApplications(userId, communityID);

        if (!apps.isEmpty()) {
            logger.info(" getUserApps ***** apps.size " + apps.size());
        }

        // convert entity objects to DTOs
        List<ApplicationDTO> appDtos = UserApplicationUtils.convert(apps);

        return appDtos;
    }

    private List<ApplicationDTO> getAllApps(List<Application> apps) {

        // convert entity objects to DTOs
        List<ApplicationDTO> appDtos = UserApplicationUtils.convert(apps);
        if (!appDtos.isEmpty()) {
            logger.info("getAllApps appDtos.size:" + appDtos.size());
        }

        return appDtos;
    }

    public int getDaysSinceLastChange(Date old, Date now) {

        int days = 0;
        try {
            Date lastPasswordChangedDate = old;
            int daysSinceLastChangedPassword = DateUtils.daysBetween(lastPasswordChangedDate, now);
            days = daysSinceLastChangedPassword;
        } catch (NullPointerException npe) {
            // logger.log(Level.WARNING, "Nullpointer: Last password changed
            // date is null for: {0}{1}", new Object[]{user.getFirstName(),
            // user.getLastName()});
        }
        return days;
    }

    protected Long getCommunityIdFromSession() {
        HttpServletRequest request = getThreadLocalRequest();
        HttpSession session = request.getSession();
        return (Long) session.getAttribute(WebConstants.USER_COMMUNITY_KEY);
    }

    private String getTokenFromSession() {
        HttpServletRequest request = getThreadLocalRequest();
        HttpSession session = request.getSession();
        return (String) session.getAttribute(WebConstants.SSO_TOKEN_KEY);
    }
    
    public static void main(String args[]) {
        SecurityServiceImpl ss = new SecurityServiceImpl();
        ss.authenticate("su@test.com", "Test123#", Long.parseLong("1"),null);
    }

}
