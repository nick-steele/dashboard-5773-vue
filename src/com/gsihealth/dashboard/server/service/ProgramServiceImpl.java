package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.service.ProgramService;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.entity.dto.ProgramDTO;
import com.gsihealth.dashboard.entity.dto.ProgramSearchOptionsDTO;
import com.gsihealth.dashboard.entity.dto.ProgramStatusDTO;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.PatientProgramDAO;
import com.gsihealth.entity.Program;
import com.gsihealth.entity.ProgramStatus;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProgramServiceImpl extends BasePortalServiceServlet implements ProgramService {

    private Logger logger = Logger.getLogger(getClass().getName());
    PatientProgramDAO patientProgramDAO;

    @Override
    public void init() throws ServletException {

        logger.info("loading ProgramServiceImpl");
        ServletContext application = getServletContext();
        patientProgramDAO = (PatientProgramDAO) application.getAttribute(WebConstants.PATIENT_PROGRAM_DAO_KEY);

    }

    @Override
    public List<ProgramDTO> getSubordinatePrograms(long communityId) throws PortalException{
        return getPrograms(communityId, false);
    }

    @Override
    public List<ProgramDTO> getParentPrograms(long communityId) throws PortalException {
        return getPrograms(communityId, true);
    }

    @Override
    public List<ProgramStatusDTO> getProgramStatuses(long communityId) throws PortalException {
        List<ProgramStatus> programStatuses;
        try {
            programStatuses = patientProgramDAO.getProgramStatuses(communityId);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Failed to retrieve program statuses communityId="+communityId, e);
            throw new PortalException("Failed to retrieve program statuses.");
        }
        List<ProgramStatusDTO> statusDTOs = convertStatusToDTO(programStatuses);
        return statusDTOs;
    }


    /**
     *
     * @param communityId
     * @param parentId
     * Invoked when a parent is selected.
     * searchOption.consent = parent's consent, when the parent is simple OR, complex which requires consent
     * searchOption.statuses = statuses of the parent
     * searchOption.subordinatePrograms = all programs who have the parent with parentId.
     * @return searchOption of type ProgramSearchOptionsDTO -> true : returns actual consent values. false : returns 'No consent Required'. null : empty value.
     * @throws PortalException
     */
    @Override
    public ProgramSearchOptionsDTO filterProgramOptionsByParent(long communityId, long parentId) throws PortalException {
        try {
            ProgramSearchOptionsDTO searchOption = new ProgramSearchOptionsDTO();
            Program program = patientProgramDAO.getProgram(communityId, parentId);
            searchOption.setSubordinatePrograms(convertToDTO(patientProgramDAO.getPrograms(communityId, parentId)));
            searchOption.setStatuses(convertStatusToDTO(patientProgramDAO.getProgramStatuses(communityId, parentId)));
            if(program.getSimpleView() || (!program.getSimpleView() && program.getConsentRequired())){
                searchOption.setConsentRequired(program.getConsentRequired());
            }
            return searchOption;
        }catch (Exception e) {
            logger.log(Level.SEVERE, "Failed to retrieve program statuses communityId="+communityId, e);
            throw new PortalException("Failed to retrieve program statuses.");
        }
    }

    /**
     *
     * @param communityId
     * @param programId
     * Invoked when a child is selected.
     * searchOption.consent = parent's consent when the parent is simple. Child OR parent consent when complex program. Child consent when orphans.
     * searchOption.statuses = No status when simple parent. Child status when complex parent. Child status when orphans.
     * @return searchOption of type ProgramSearchOptionsDTO -> true : returns actual consent values. false : returns 'No consent Required'. null : empty value.
     * @throws PortalException
     */
    @Override
    public ProgramSearchOptionsDTO filterProgramOptionsByProgram(long communityId, long programId) throws PortalException {
        try {
            ProgramSearchOptionsDTO searchOption = new ProgramSearchOptionsDTO();
            Program program = patientProgramDAO.getProgram(communityId, programId);
            if (program.getParent() != null) {
                if (program.getParent().getSimpleView()) { //  simple subordinates
                    searchOption.setSimpleSubordinate(true);
                    searchOption.setConsentRequired(program.getParent().getConsentRequired());
                } else {  // complex subordinates
                    searchOption.setConsentRequired(program.getParent().getConsentRequired() || program.getConsentRequired());
                    searchOption.setStatuses(convertStatusToDTO(patientProgramDAO.getProgramStatuses(communityId, programId)));
                }
            } else { // orphans
                searchOption.setConsentRequired(program.getConsentRequired());
                searchOption.setStatuses(convertStatusToDTO(patientProgramDAO.getProgramStatuses(communityId, programId)));
            }
            return searchOption;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Failed to retrieve program statuses communityId=" + communityId, e);
            throw new PortalException("Failed to retrieve program statuses.");
        }
    }


    @Override
    public List<ProgramStatusDTO> getSystemProgramStatuses(long communityId) throws PortalException {
        List<ProgramStatus> programStatuses;
        try {
            programStatuses = patientProgramDAO.getSystemProgramStatuses(communityId);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Failed to retrieve program statuses communityId="+communityId, e);
            throw new PortalException("Failed to retrieve program statuses.");
        }
        return convertStatusToDTO(programStatuses);
    }

    private List<ProgramDTO> getPrograms(Long communityId, boolean isParent) throws PortalException {
        List<Program> programs;
        try {
            programs = patientProgramDAO.getPrograms(communityId, isParent);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Failed to retrieve programs communityId="+communityId +", isParent="+isParent , e);
            throw new PortalException("Failed to retrieve programs.");
        }
        List<ProgramDTO> programDTOS = convertToDTO(programs);
        return programDTOS;
    }

    private List<ProgramStatusDTO> convertStatusToDTO(List<ProgramStatus> programStatuses) {
        List<ProgramStatusDTO> statusDTOs = new ArrayList<ProgramStatusDTO>();
        for(ProgramStatus p : programStatuses){
            statusDTOs.add(new ProgramStatusDTO(p.getId(), p.getName()));
        }
        return statusDTOs;
    }


    private List<ProgramDTO> convertToDTO(List<Program> programs) {
        List<ProgramDTO> programDTOS = new ArrayList<ProgramDTO>();
        for(Program p : programs){
            programDTOS.add(new ProgramDTO(p.getId(), p.getName()));
        }
        return programDTOS;
    }


}
