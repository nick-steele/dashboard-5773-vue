/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.service.PatientProgramService;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.PatientProgramDTO;
import com.gsihealth.dashboard.server.CommunityContexts;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.CommunityDAO;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.dashboard.server.dao.NotificationAuditDAO;
import com.gsihealth.dashboard.server.dao.OrganizationDAO;
import com.gsihealth.dashboard.server.dao.OrganizationPatientConsentDAO;
import com.gsihealth.dashboard.server.dao.PatientEnrollmentDAO;
import com.gsihealth.dashboard.server.dao.PatientProgramDAO;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
//import com.gsihealth.dashboard.server.service.BasePatientService;
import com.gsihealth.dashboard.server.util.NotificationUtils;
import com.gsihealth.entity.PatientCommunityEnrollment;
import com.gsihealth.entity.PatientProgramName;
import com.gsihealth.entity.PatientProgramNameRest;
import com.gsihealth.entity.PatientStatus;
import com.gsihealth.entity.ProgramDeleteReason;
import com.gsihealth.entity.ProgramHealthHome;
import com.gsihealth.entity.ProgramHealthHomePK;
import com.gsihealth.entity.ProgramName;
import com.gsihealth.entity.ProgramNameHistory;
import com.gsihealth.entity.ProgramNamePK;
import com.gsihealth.entity.ProgramTerminationReason;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.PathParam;
import org.apache.commons.lang.time.StopWatch;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

/**
 *
 * 
 */


public class PatientProgramServiceImpl extends BasePatientService implements PatientProgramService { //AbstractFacade<PatientProgramName>{
    
    private ServletContext context;
    private Logger logger = Logger.getLogger(getClass().getName());

    private PatientProgramDAO patientProgramDAO;
    private PatientEnrollmentDAO patientEnrollmentDAO;
    private PatientManager patientManager;
    private OrganizationPatientConsentDAO organizationPatientConsentDAO;
    private NotificationAuditDAO notificationAuditDAO;

    private OrganizationDAO organizationDAO;
    private CommunityDAO communityDAO;

    private ConfigurationDAO configurationDAO;
    private HashMap<Long,CommunityContexts> communityContexts;
//    BasePatientService basePatientService = new BasePatientService();
//    
    
     /**
     * Initializes the service with DAOs
     */
    @Override
    public void init() throws ServletException {

        ServletContext application = getServletContext();
       
        patientProgramDAO = (PatientProgramDAO) application.getAttribute(WebConstants.PATIENT_PROGRAM_DAO_KEY);
        patientEnrollmentDAO = (PatientEnrollmentDAO) application.getAttribute(WebConstants.PATIENT_ENROLLMENT_DAO_KEY); //not saved
        patientManager = (PatientManager) application.getAttribute(WebConstants.PATIENT_MANAGER_KEY); //not saved
        communityContexts = (HashMap<Long,CommunityContexts>) application.getAttribute(WebConstants.COMMUNITY_SPECIFIC_DATA_KEY); 
        
        communityDAO = (CommunityDAO) application.getAttribute(WebConstants.COMMUNITY_DAO_KEY);        
        organizationPatientConsentDAO = (OrganizationPatientConsentDAO) application.getAttribute(WebConstants.ORGANIZATION_PATIENT_CONSENT_DAO_KEY);
        notificationAuditDAO = (NotificationAuditDAO) application.getAttribute(WebConstants.NOTIFICATION_AUDIT_DAO_KEY);
        organizationDAO = (OrganizationDAO) application.getAttribute(WebConstants.ORGANIZATION_DAO_KEY);   
        configurationDAO = (ConfigurationDAO) application.getAttribute(WebConstants.CONFIGURATION_DAO_KEY);
        
        
        
        // store a reference to THIS service in application context
        application.setAttribute(WebConstants.PATIENT_PROGRAM_SERVICE_KEY, this);  

        super.init();
    }

    
         
    public PatientProgramServiceImpl(){
      //   super(PatientProgramName.class);
      
    }            
 
    private EntityManagerFactory getEntityManagerFactory() throws NamingException {
        String persistenceUnitName = "dashboardPU";
        String persistenceUnitNameStandAlone = "dashboardPU_standalone";
        EntityManagerFactory emf = getEntityManagerFactory(persistenceUnitName, persistenceUnitNameStandAlone);
        return emf;
    }
    protected EntityManagerFactory getEntityManagerFactory(String persistenceUnitName, String persistenceUnitNameStandAlone) throws RuntimeException {
        // JPA setup...
        EntityManagerFactory emf = null;
        EntityManager manager = null;

        try {
            // look for datasource first
            logger.info("Looking up: " + persistenceUnitName);
            emf = Persistence.createEntityManagerFactory(persistenceUnitName);
            logger.info("emf = " + emf);
            manager = emf.createEntityManager();
            logger.info("manager = " + manager);
        } catch (Exception exc) {

            logger.info("Could not lookup: " + persistenceUnitName);

         
        }

        if (manager == null) {
            String msg = "Unable to get connection to database.";
            logger.severe("\n\n>>>>>>>>> ERROR:" + msg + " <<<<<<<<<<<<<\n\n");
            throw new RuntimeException(msg);
        }

        return emf;
}
    protected EntityManager getEntityManager(){
        System.out.println("In getEntityManager  ");
        try {
           
            return getEntityManagerFactory().createEntityManager();
           
        } catch (NamingException ex) {
            Logger.getLogger(PatientProgramService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
   
    
    public List<PatientProgramNameRest> getActivePatientPrograms(@PathParam("patId") BigInteger patientId, @PathParam("comId") BigInteger communityId) {
        EntityManager em = getEntityManager();
        try {
             List<PatientProgramNameRest> ret = new ArrayList();
            Class clas = PatientProgramNameRest.class;
            String sql1 = "SELECT pp.*, pn.value as PROGRAM_NAME, ps.value as PATIENT_STATUS_NAME FROM connect.patient_program_name pp\n" 
                    + " LEFT JOIN program_name pn ON pp.program_id=pn.id AND pn.community_id=?2\n" 
                    + " LEFT JOIN patient_status ps ON pp.patient_status=ps.id AND ps.community_id=?2\n"
                    + " WHERE pp.patient_id=?1 \n" 
                    + " AND pp.community_id=?2 \n"
                    + " AND pp.termination_reason IS NULL\n";

            Query query = em.createNativeQuery(sql1, clas).setParameter(1, patientId).setParameter(2, communityId);
            ret.addAll(query.getResultList());
           
            SimplePerson sp = patientManager.findPatient(Long.parseLong(patientId.toString()));
            return ret;
        }  catch(Exception e){
                    logger.info("ugh");
                    
        } finally {
            em.close();
            //update patient program list
            
        }
       
        return new ArrayList<PatientProgramNameRest>();
    }
    
    
  
    public List<PatientProgramNameRest> getAllPatientPrograms(@PathParam("patId") BigInteger patientId, @PathParam("comId") BigInteger communityId) {
        EntityManager em = getEntityManager();
        try {
             
           List<PatientProgramNameRest> ret = new ArrayList();
            Class clas = PatientProgramNameRest.class;
            String sql1 = "SELECT pp.*, pn.value as PROGRAM_NAME, ps.value as PATIENT_STATUS_NAME FROM connect.patient_program_name pp\n" 
                    + " LEFT JOIN program_name pn ON pp.program_id=pn.id AND pn.community_id=?2\n\n" 
                    + " LEFT JOIN patient_status ps ON pp.patient_status=ps.id AND ps.community_id=?2\n"
                    + " WHERE pp.patient_id=?1 \n" 
                    + " AND pp.community_id=?2 \n";                 
                    

            Query query = em.createNativeQuery(sql1, clas).setParameter(1, patientId).setParameter(2, communityId);
            ret.addAll(query.getResultList());
      
            return ret;
        } finally {
            em.close();
        }
    }

    
    @Override
    public String addProgram(Long userId,PatientProgramName entity) {
        
      
        EntityManager em = getEntityManager();
         EntityTransaction tx =null;
         int duplicateCount=0;
        try {
            
            tx = em.getTransaction();
            tx.begin();
             /*To check whether patient is enrolled with the same patient program name or not*/
            Query q = em.createQuery("select object(o) from PatientProgramName as o where o.programId=:theProgramId and  o.communityId=:theCommunityId and o.patientId=:thePatientId ");
            q.setParameter("theCommunityId", entity.getCommunityId());
            q.setParameter("thePatientId", entity.getPatientId());
            q.setParameter("theProgramId", entity.getProgramId());
          
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            
            List<PatientProgramName> duplicateEntity= (List<PatientProgramName>)q.getResultList();
           
          
          if(duplicateEntity!=null) {
              for(PatientProgramName ppn:duplicateEntity){
                        if(ppn.getTerminationReason()==null){
                            duplicateCount ++;
                            break;
                        }
                        else if(entity.getTerminationReason()!=null){
                            duplicateCount ++;
                            break;
                        }
                    }
          if(duplicateCount>0){
              tx.commit();
              return "duplicate";
          }}
//            super.create(entity);
            em.persist(entity);
           
//            long patId = entity.getPatientId();
              em.flush(); 
              em.clear();
//            // Add to program name history
            ProgramNameHistory pnh = getProgramNameHistory(entity,"Program Added ",userId);
            em.persist(pnh);
            tx.commit();
            
            sendPatientProgramInfo(entity);
           
        
        }
           
        catch (Exception exc) {
			logger.log(Level.SEVERE, "Error adding program: " , exc);
			
			if ((tx != null) && tx.isActive()) {
				tx.rollback();
			}		
		} 
       
        finally {
            if(em!=null && em.isOpen()){
            em.close();
            }
        }
         return "true";
    }
    
   @Override
    public String sendPatientProgramInfo(PatientProgramName entity) throws Exception {
       
        StopWatch stopWatch = new StopWatch();
        
        // create enrollment service helper
        CommunityContexts curentCommunityContext = communityContexts.get(entity.getCommunityId());
        patientManager = curentCommunityContext.getPatientManager();
        patientEnrollmentDAO = curentCommunityContext.getPatientEnrollmentDAO();
         
        EnrollmentServiceHelper enrollmentServiceHelper = buildEnrollmentServiceHelper();

        //Send out notifications
        //update patient program list
        logger.info("setting up pce info for id:"+ entity.getPatientId());
        PatientCommunityEnrollment pce = patientEnrollmentDAO.findPatientEnrollment(entity.getPatientId(),entity.getCommunityId()); 
        
        Map<String,PatientCommunityEnrollment> pces = new HashMap<String,PatientCommunityEnrollment>();
        pces.put(String.valueOf(entity.getPatientId()), pce);
        stopWatch.start();
        logger.info("convert pces to personDTOs... "+ (stopWatch.getTime() / 1000.0) + ", secs");
        List<PersonDTO> personDTOs = super.convertPatientEnrollmentsToPersonDTOs(entity.getCommunityId(), pces, pce.getOrgId());
        logger.info("finished convertingpersonDTOs... "+ (stopWatch.getTime() / 1000.0) + ", secs");
       
        if (personDTOs != null && entity != null) {
           PersonDTO personDTO = personDTOs.get(0); //FIX ME Quickly 

           PatientProgramDTO patientProgramDTO = convertTODTO(entity, entity.getPatientId());

           logger.info("sending patient-DEMO update msg with new patient program info from carebook... " + (stopWatch.getTime() / 1000.0) + ", secs");
           //personDTO.getPatientEnrollment().getPatientProgramName().add(patientProgramDTO); //not needed anymore -DAO reads in notification utils
           // run notification on background thread for patient updates
           enrollmentServiceHelper.sendPatientProgramInfoToNotificationManager(personDTO, personDTO.getPatientEnrollment().getCommunityId());
           logger.info("finish sending  patient-DEMO " + (stopWatch.getTime() / 1000.0) + ", secs");
           return "SUCCESS";
       } else {
           logger.info("personDTO and/or entity is null so no patient program info sent");
           return "personDTO and/or entity is null so no patient program info sent";
       }
    }
    
  
    

 
    @Override
    public String edit(@PathParam("userId") Long userId, PatientProgramName entity,@PathParam("isAddToHistory") boolean isAddtoHistory) {
          EntityManager em=getEntityManager();
          EntityTransaction tx =null;
        try {
           boolean flag = false;
            tx = em.getTransaction();
            tx.begin();  
            /*To check whether patient is enrolled with the same patient program name or not*/
//            String sql1 = "SELECT o.id from patient_program_name as o where o.program_id=?1 and o.patient_status=?2 and o.community_id=?3 and o.patient_id=?4";
//          Query q = em.createNativeQuery(sql1).setParameter(1, entity.getProgramId()).setParameter(2, entity.getPatientStatus()).setParameter(3, entity.getCommunityId()).setParameter(4, entity.getPatientId());
//          if(q.getResultList()!=null && q.getResultList().size()>0) {
//              for(int i=0; i<q.getResultList().size();i++) {
//                  String id = q.getResultList().get(i).toString();
//                  if(id.equals(entity.getPatientProgramNameId().toString())) {
//                      flag = true;
//                      break;
//                  }
//              } 
//              if(flag==false) {
//              tx.commit();
//              return "duplicate";
//              }
//          }
            em.merge(entity);
             em.flush();
             em.clear();            
             
//            em.find(PatientProgramName.class, entity.getPatientProgramNameId());
        // Add to program name history
             if(isAddtoHistory){
                 ProgramNameHistory pnh = getProgramNameHistory(entity, "Program  Updated",userId);
                  em.persist(pnh);
             }
        
       
        tx.commit();
        sendPatientProgramInfo(entity);
        } 
         catch (Exception exc) {
			logger.log(Level.SEVERE, "Error updating program: " , exc);
			
			if ((tx != null) && tx.isActive()) {
				tx.rollback();
			}			
		}
        finally {
          if(em!=null && em.isOpen()){
            em.close();
            }
        }
         return "true";
    }
  

  @Override
    public String remove(Long id,Long userId, int reasonId) {
        EntityManager em = getEntityManager();
         EntityTransaction tx =null;
        try {
            PatientProgramName entity = em.find(PatientProgramName.class, id);
            tx = em.getTransaction();
            tx.begin();
            em.remove(entity);            
//            em.flush();
//            em.clear();
//        // Add to program name history
//        ProgramNameHistory pnh = getProgramNameHistory(entity,"Program Deleted",userId);
//        String sql1 = "SELECT o.VALUE from program_delete_reason as o where o.id=?1 and o.community_id=?2";
//          Query q = em.createNativeQuery(sql1).setParameter(1, reasonId).setParameter(2, entity.getCommunityId());
//            pnh.setDeleteReason(q.getSingleResult().toString());
//        em.persist(pnh);
        tx.commit();     
        
        sendPatientProgramInfo(entity);
             
      
        
        } 
        catch (Exception exc) {
			logger.log(Level.SEVERE, "Error deleting program: " , exc);
			

			if ((tx != null) && tx.isActive()) {
				tx.rollback();
			}                       			
		}
        finally {
           if(em!=null && em.isOpen()){
            em.close();
            }
        }
        return "true";
    }


    private ProgramNameHistory getProgramNameHistory(PatientProgramName entity, String action, long userId){
          ProgramNameHistory pnh = new ProgramNameHistory();
       
            pnh.setUserId(userId);
            pnh.setCommunityId(entity.getCommunityId());            
            pnh.setActionDatetime(new Date());
         
            pnh.setPatientId(entity.getPatientId());
            pnh.setProgramEffectiveDate(entity.getProgramEffectiveDate());
            pnh.setCurrentProgramName(entity.getProgramName());
          
            pnh.setProgramEndDate(entity.getProgramEndDate());
            pnh.setProgramEndReason(entity.getTerminationReason());
            pnh.setStatus(entity.getPatientStatus());
            pnh.setAction(action);
            pnh.setParentProgramName(entity.getHealthHome());
            pnh.setStatusEffectiveDate(entity.getStatusEffectiveDate());
                       
            return pnh;
        
    }
    
   
   
    public List<ProgramName> getProgramNames( BigInteger communityId) {
        EntityManager em = getEntityManager();
        try {
            List<ProgramName> programList = new ArrayList();
            Class clas = ProgramName.class;
            String sql1 = "Select ob.id,ob.value,ob.community_id,ob.health_home_id \n"
                    + " FROM program_name ob WHERE ob.community_id =?1";

            Query query = em.createNativeQuery(sql1).setParameter(1, communityId);
            List<Object[]> list=query.getResultList();
            
            for(Object[] temp:list){
                ProgramName program=new ProgramName();
                ProgramNamePK prograPk=new ProgramNamePK();
                prograPk.setCommunityId((Long)temp[2]);
                prograPk.setId((Integer)temp[0]);
                program.setProgramNamePK(prograPk);
                program.setValue((String)temp[1]);
                ProgramHealthHome healthHome = new ProgramHealthHome();
                ProgramHealthHomePK healthHomePk = new ProgramHealthHomePK();
                healthHomePk.setId((Long)temp[3]);
                healthHomePk.setCommunityId((Long)temp[2]);
                healthHome.setProgramHealthHomePK(healthHomePk);
                program.setHealthHomeId(healthHome);
                programList.add(program);
            }
           
            return programList;
        } finally {
            em.close();
        }
    }
    
   
    
    public List<PatientStatus> getProgramStatus(@PathParam("comId") BigInteger communityId) {
        EntityManager em = getEntityManager();
        try {
            List<PatientStatus> list = new ArrayList();
            Class clas = PatientStatus.class;
            String sql1 = "SELECT * FROM patient_status WHERE community_id=?1";

            Query query = em.createNativeQuery(sql1, clas).setParameter(1, communityId);
            list.addAll(query.getResultList());
           
            return list;
        } finally {
            em.close();
        }
    }
    

  
    public List<ProgramTerminationReason> getProgramEndReason(@PathParam("comId") BigInteger communityId) {
        EntityManager em = getEntityManager();
        try {
            List<ProgramTerminationReason> list = new ArrayList();
            Class clas = ProgramTerminationReason.class;
            String sql1 = "SELECT * FROM program_termination_reason WHERE community_id=?1";

            Query query = em.createNativeQuery(sql1, clas).setParameter(1, communityId);
            list.addAll(query.getResultList());
           
            return list;
        } finally {
            em.close();
        }
    }
    
   
 
    public List<ProgramHealthHome> getProgramHealthHome( BigInteger communityId) {
        EntityManager em = getEntityManager();
        try {
            List<ProgramHealthHome> healthHomeList = new ArrayList();
//            Class clas = ProgramHealthHome.class;
            String sql1 = "SELECT o.id, o.health_home_value, o.community_id FROM program_health_home as o WHERE o.community_id=?1";
           
            Query query = em.createNativeQuery(sql1).setParameter(1, communityId);
            List<Object[]> list=query.getResultList();
            for(Object[] temp:list){               
                ProgramHealthHome healthHome = new ProgramHealthHome();
                ProgramHealthHomePK healthHomePk = new ProgramHealthHomePK();
                healthHomePk.setId((Long)temp[0]);
                healthHomePk.setCommunityId((Long)temp[2]);
                healthHome.setProgramHealthHomePK(healthHomePk);
                healthHome.setHealthHomeValue((String)temp[1]);
                healthHomeList.add(healthHome);
            }
           
            return healthHomeList;
        } finally {
            em.close();
        }
    }
    
    
    
    public List<ProgramDeleteReason> getProgramDeleteReason(BigInteger communityId) {
        EntityManager em = getEntityManager();
        try {
            List<ProgramDeleteReason> list = new ArrayList();
            Class clas = ProgramDeleteReason.class;
            String sql1 = "SELECT * FROM program_delete_reason WHERE community_id=?1";

            Query query = em.createNativeQuery(sql1, clas).setParameter(1, communityId);
            list.addAll(query.getResultList());
           
            return list;
        } finally {
            em.close();
        }
    }
    
     private EnrollmentServiceHelper buildEnrollmentServiceHelper() {
        NotificationUtils.setCommunityDAO(communityDAO);
        //JIRA 629 - remove properties
        EnrollmentServiceHelper enrollmentServiceHelper = new EnrollmentServiceHelper(patientManager,
                notificationAuditDAO, organizationPatientConsentDAO,
                patientEnrollmentDAO, organizationDAO);

        return enrollmentServiceHelper;
    }
      protected PersonDTO loadPersonDTO(SimplePerson sp,PatientCommunityEnrollment pce,
              long communityId, Long currentUserOrgId)
    {
        long patientId = Long.valueOf(sp.getLocalId());
        logger.info("patient id:" + patientId);

        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        int minorAge=Integer.parseInt(configurationDAO.getProperty(communityId, "minor.age", "18"));
        
       
        return super.convertPersonToDto(sp, sp.getLocalId(), pce, mapper, communityId, minorAge, currentUserOrgId);
    }

      //FIXME:  This can be refactored because BasePatientService is now super class
     /**
     * Convert entity objects to DTOs
     *
     * @param patientProgramNames
     * @return
     */
    protected List<PatientProgramDTO> convert(List<PatientProgramName> patientProgramNames, long patientId) {

        List<PatientProgramDTO> patientProgDTOs = new ArrayList<PatientProgramDTO>();

        for (PatientProgramName tempPatientProgramName : patientProgramNames) {
            PatientProgramDTO tempDTO = convertTODTO(tempPatientProgramName, patientId);
            patientProgDTOs.add(tempDTO);
        }

        return patientProgDTOs;
    }
    

     public PatientProgramDTO convertTODTO(PatientProgramName patientProgramName, long patientId) {
        PatientProgramDTO patientProgramDTO = new PatientProgramDTO();
        patientProgramDTO.setPatientProgramNameId(patientProgramName.getPatientProgramNameId());
        patientProgramDTO.setCommunityId(patientProgramName.getCommunityId());
        patientProgramDTO.setPatientId(patientId);
        patientProgramDTO.setHealthHome(patientProgramName.getHealthHome());
        patientProgramDTO.setPatientStatus(patientProgramName.getPatientStatus());
        patientProgramDTO.setProgramEffectiveDate(patientProgramName.getProgramEffectiveDate());
        patientProgramDTO.setProgramEndDate(patientProgramName.getProgramEndDate());
        patientProgramDTO.setProgramId(patientProgramName.getProgramId());
        patientProgramDTO.setTerminationReason(patientProgramName.getTerminationReason());
        patientProgramDTO.setStatusEffectiveDate(patientProgramName.getStatusEffectiveDate());
        patientProgramDTO.setOldRecord(true);
        return patientProgramDTO;
    }
     
}

