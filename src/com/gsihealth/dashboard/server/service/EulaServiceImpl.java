package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.service.EulaService;
import com.gsihealth.dashboard.common.Constants;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.entity.Audit;
import com.gsihealth.entity.Eula;
import com.gsihealth.entity.User;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.AuditDAO;
import com.gsihealth.dashboard.server.dao.EulaDAO;
import com.gsihealth.dashboard.server.dao.UserDAO;
import com.gsihealth.dashboard.server.util.AuditType;
import com.gsihealth.dashboard.server.util.AuditUtils;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Chad Darby
 */
public class EulaServiceImpl extends BasePortalServiceServlet implements EulaService {

    private Logger logger = Logger.getLogger(getClass().getName());
    private EulaDAO eulaDAO;
    private AuditDAO auditDAO;
    
    /**
     * Initializes the service with DAOs
     */
    @Override
    public void init() throws ServletException {
        logger.info("loading eulaServiceImpl");
        super.init();
        ServletContext application = getServletContext();

        eulaDAO = (EulaDAO) application.getAttribute(WebConstants.EULA_DAO_KEY);
        userDAO = (UserDAO) application.getAttribute(WebConstants.USER_DAO_KEY);
        auditDAO = (AuditDAO) application.getAttribute(WebConstants.AUDIT_DAO_KEY);
        logger.info("finished loading eulaServiceImpl");
    }

    @Override
    public String getActiveEulaContent(Long communityId) throws PortalException {


        try {
            Eula eula = eulaDAO.findActiveEula(communityId);
            
            return eula.getContent();
        } catch (Exception exc) {
            final String message = "Error retrieving EULA content.";
            logger.logp(Level.SEVERE, getClass().getName(), "getActiveEulaContent", message, exc);

            throw new PortalException(message);
        }

    }

    @Override
    public void updateUserEulaStatus(String email, Long community, boolean flag) throws PortalException {

        logger.info("START: updateUserEulaStatus()");

        try {
            User theUser = userDAO.findUserByEmail(email, community);

            if (flag) {
                theUser.setEulaAccepted(Constants.YES_CHAR);
            } else {
                theUser.setEulaAccepted(Constants.NO_CHAR);
            }

            theUser.setEulaDate(new Date());

            userDAO.update(theUser);

            audit(theUser, flag);
            logger.info("FINISHED: updateUserEulaStatus()");
        } catch (Exception exc) {
            final String message = "Error updating EULA status for: " + email;
            logger.logp(Level.SEVERE, getClass().getName(), "updateUserEulaStatus", message, exc);

            throw new PortalException(message);
        }
    }

    private void audit(User theUser, boolean flag) {
        HttpServletRequest httpServletRequest = getThreadLocalRequest();
        LoginResult loginResult = AuditUtils.getLoginResult(httpServletRequest);

        logger.info(">> inside audit: loginResult = " + loginResult);

        Audit audit = AuditUtils.convertUser(loginResult, theUser);

        audit.setCommunityId(loginResult.getCommunityId());
        String comment = getComment(theUser, flag);
        audit.setActionComments(comment);

        String actionType = getActionType(flag);
        audit.setActionType(actionType);

        audit.setActionSuccess(Constants.YES);

        auditDAO.addAudit(audit);
    }

    private String getComment(User theUser, boolean flag) {
        StringBuilder comment = new StringBuilder();

        comment.append("User: " + theUser.getFirstName() + " " + theUser.getLastName());
        if (flag) {
            comment.append(" accepted EULA.");
        } else {
            comment.append(" declined EULA.");
        }

        return comment.toString();
    }

    private String getActionType(boolean flag) {

        return flag ? AuditType.ACCEPTED_EULA : AuditType.DECLINED_EULA;
    }
}
