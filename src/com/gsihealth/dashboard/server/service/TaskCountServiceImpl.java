package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.service.TaskCountService;
import com.gsihealth.dashboard.common.ConfigurationConstants;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.dashboard.server.dao.UserDAO;
import com.gsihealth.dashboard.server.util.AuditUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import java.util.logging.Logger;
import javax.servlet.ServletException;

public class TaskCountServiceImpl extends BasePortalServiceServlet implements TaskCountService {

    private Logger logger = Logger.getLogger(getClass().getName());

    /**
     * Initializes the service with DAOs
     */
    @Override
    public void init() throws ServletException {
        super.init();
        userDAO = (UserDAO) getServletContext().getAttribute(WebConstants.USER_DAO_KEY);
    }

    @Override
    public int getOverdueTaskCount() throws PortalException {

        try {
            ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
            
            LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
            if (loginResult != null) {
                long communityId = loginResult.getCommunityId();
                String emailId = loginResult.getEmail();
                String tasksServerUrl = configurationDAO.getProperty(communityId, ConfigurationConstants.REDUX_REST_TASKS);
                tasksServerUrl += "usertask/count/" + emailId + "/" + communityId;

                ClientResponse cResponse = hitService(tasksServerUrl);

                if (cResponse.getResponseStatus().getStatusCode() != 200) {
                    logger.warning("Tasks Response code: " + cResponse.getResponseStatus().getStatusCode());
                    return -1;
                }
                
                String response = cResponse.getEntity(String.class);
                int taskCount = Integer.parseInt(response);
                return taskCount;
            }
        } catch (ClientHandlerException | UniformInterfaceException e) {
            logger.warning("Could not retrieve data from task service because of " + e.getClass().getName() + ": " + e.getMessage());
            return -1;
        }
        return -1;
    }

    private ClientResponse hitService(String serviceUrl) {
        WebResource webResource = Client.create().resource(serviceUrl);
        ClientResponse response = webResource.type("application/json").get(ClientResponse.class);
        return response;
    }

}
