package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.service.ReportService;
import com.gsihealth.dashboard.common.AccessLevelConstants;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.AggregatePatientCountDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;
import com.gsihealth.dashboard.entity.dto.PatientProgramDTO;
import com.gsihealth.dashboard.entity.dto.PatientStatusDTO;
import com.gsihealth.dashboard.entity.dto.ProgramHealthHomeDTO;
import com.gsihealth.dashboard.entity.dto.ProgramNameDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.gsihealth.dashboard.entity.dto.insurance.PayerClassDTO;
import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import com.gsihealth.dashboard.server.CommunityContexts;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.dao.CommunityCareteamDAO;
import com.gsihealth.dashboard.server.dao.PatientProgramDAO;
import com.gsihealth.dashboard.server.dao.ReportDAO;
import com.gsihealth.dashboard.server.dao.UserDAO;
import com.gsihealth.dashboard.server.dao.UserPatientConsentDAO;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import com.gsihealth.dashboard.server.reports.ReportUtils;
import com.gsihealth.dashboard.server.util.AuditUtils;
import com.gsihealth.dashboard.server.util.PropertyUtils;
import com.gsihealth.entity.CommunityCareteam;
import com.gsihealth.entity.PatientCommunityEnrollment;
import com.gsihealth.entity.PatientProgramSheet;
import com.gsihealth.entity.PatientReportSheet;
import com.gsihealth.entity.PatientStatus;
import com.gsihealth.entity.ProgramHealthHome;
import com.gsihealth.entity.ProgramName;
import com.gsihealth.entity.User;
import com.gsihealth.entity.UserCommunityOrganization;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.NoResultException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.dozer.MappingException;

/**
 *
 * @author Chad Darby
 */
public class ReportServiceImpl extends BasePortalServiceServlet implements ReportService {

    private Logger logger = Logger.getLogger(getClass().getName());
    private ReportDAO reportDAO;
    private CommunityCareteamDAO careteamDAO;
    private PatientProgramDAO patientProgramDAO;
    private Map<Long, ProgramHealthHome> programHealthHomeMap = null;
    private UserPatientConsentDAO userPatientConsentDAO;

    /**
     * Initializes the service with DAOs
     */
    @Override
    public void init() throws ServletException {
        logger.info("loading reportServiceImpl");
        super.init();
        ServletContext application = getServletContext();
        reportDAO = (ReportDAO) application.getAttribute(WebConstants.REPORT_DAO_KEY);
        userDAO = (UserDAO) application.getAttribute(WebConstants.USER_DAO_KEY);
        careteamDAO = (CommunityCareteamDAO) application.getAttribute(WebConstants.COMMUNITY_CARE_TEAM_DAO_KEY);
        patientProgramDAO = (PatientProgramDAO) application.getAttribute(WebConstants.PATIENT_PROGRAM_DAO_KEY);
        programHealthHomeMap = new HashMap<Long, ProgramHealthHome>();
        programHealthHomeMap = getProgramNameHealthHomeMap();
        userPatientConsentDAO = (UserPatientConsentDAO) application.getAttribute(WebConstants.USER_PATIENT_CONSENT_DAO_KEY);
        logger.info("finished loading reportServiceImpl");
    }

    /**
     * Get a list of users for report
     *
     * @return
     * @throws Exception
     */
    // @Override
    public List<UserDTO> getReportUsers() throws PortalException {

        try {
            // Step 1: Get users from DAO
            List<User> users = userDAO.findUserEntities();

            // Convert to DTOs, fully populated
            LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
            Long communityId = loginResult.getCommunityId();
            List<UserDTO> userDTOs = PropertyUtils.convertToDTOs(users, userCommunityOrganizationDAO, communityId);

            return userDTOs;
        } catch (Exception exc) {
            final String message = "Error loading users";
            logger.logp(Level.SEVERE, getClass().getName(), "getReportUsers", message, exc);
            throw new PortalException(message);
        }

    }

    @Override
    public AggregatePatientCountDTO getAggregatePatientCount(long communityId) throws PortalException {

        AggregatePatientCountDTO aggregatePatientCountDTO = null;
        int inactiveCount = 0;
        int assignedCount = 0;
        int pendingCount = 0;
        int enrolledCount = 0;

        try {
            try {
                inactiveCount = reportDAO.findPatientEnrollmentInActiveCount(communityId);
            } catch (Exception ex) {
                inactiveCount = 0;
            }
            try {
                pendingCount = reportDAO.findPatientEnrollmentPendingCount(communityId);
            } catch (Exception ex) {
                pendingCount = 0;
            }
            try {
                enrolledCount = reportDAO.findPatientEnrollmentCount(communityId);
            } catch (Exception ex) {
                enrolledCount = 0;
            }

            // total count for assigned patients
            try {
                assignedCount = reportDAO.findPatientAssignedCount(communityId);
            } catch (Exception ex) {
                assignedCount = 0;
            }

            aggregatePatientCountDTO = new AggregatePatientCountDTO((inactiveCount + pendingCount) + "", assignedCount + "", enrolledCount + "", "0", "0");

        } catch (Exception exc) {
            final String message = "Error aggregatePatientCount. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "aggregatePatientCount", message, exc);

            throw new PortalException("Error loading aggregate patient count.");
        }
        return aggregatePatientCountDTO;
    }

    @Override
    public SearchResults<PersonDTO> getEnrolledPatients(long communityId, long userId, long accessLevelId, int pageNum, int pageSize) throws PortalException {

        try {
            // get total count
            long start = System.currentTimeMillis();
            int totalCount = reportDAO.getTotalCountForEnrolledPatients(communityId, userId, accessLevelId);

            // get patients
            Map<String, PatientCommunityEnrollment> patientEnrollments = reportDAO.findEnrolledPatients(communityId, userId, accessLevelId, pageNum, pageSize);
            long end = System.currentTimeMillis();
            logger.info("Total:time taken getting data from connect MySql :" + (end - start));
            
            List<PersonDTO> patients = findGenericCandidatesEnrolledPatientsWithPatientIdAsKey(patientEnrollments);
           
            // build up search results
            SearchResults<PersonDTO> searchResults = new SearchResults(totalCount, patients);

            return searchResults;

        } catch (Exception exc) {
            final String message = "Error searching for enrolled patients.";
            logger.logp(Level.SEVERE, getClass().getName(), "findEnrolledPatients", message, exc);

            throw new PortalException(message);
        }
    }

    /*@Override
    public SearchResults<PersonDTO> getCandidatePatientsNotEnrolled(long communityId, long currentUserOrgId, int pageNum, int pageSize) throws PortalException {

        try {
            // get total count
            int totalCount = reportDAO.getTotalCountForCandidatePatientsNotEnrolledPatients(communityId, currentUserOrgId);

            // get patients
            Map<String, PatientCommunityEnrollment> patientEnrollments = reportDAO.findCandidatePatientsNotEnrolledPatients(communityId, currentUserOrgId, pageNum, pageSize);
            List<PersonDTO> patients = findGenericCandidatesEnrolledPatients(patientEnrollments);

            // build up search results
            SearchResults<PersonDTO> searchResults = new SearchResults(totalCount, patients);

            return searchResults;

        } catch (Exception exc) {
            final String message = "Error searching for candidates not enrolled.";
            logger.logp(Level.SEVERE, getClass().getName(), "getCandidatePatientsNotEnrolled", message, exc);

            throw new PortalException(message);
        }
    }*/

    private List<PersonDTO> findGenericCandidatesEnrolledPatients(Map<String, PatientCommunityEnrollment> patientEnrollments) throws Exception {
        long communityId = AuditUtils.getLoginResult(getThreadLocalRequest()).getCommunityId();
        List<PersonDTO> searchResults = new ArrayList<PersonDTO>();
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        // get the patient enrollment
        Set<Map.Entry<String, PatientCommunityEnrollment>> entries = patientEnrollments.entrySet();
        PatientManager patientManager = this.getPatientManager(communityId);
        for (Map.Entry<String, PatientCommunityEnrollment> tempEntry : entries) {
            String euid = tempEntry.getKey();
            PatientCommunityEnrollment tempPatientEnrollment = tempEntry.getValue();

            Person tempPerson = null;

            try {
                tempPerson = new Person(patientManager.getPatient(euid));

            } catch (NullPointerException e) {
                logger.log(Level.WARNING, "MDM do not have record for patient EUID:" + euid);
            }
            
            logger.fine("consented enrolled patient euid: " + euid);
            if (tempPerson != null && tempPerson.getSimplePerson() != null) {
                tempPerson.setPatientCommunityEnrollment(tempPatientEnrollment);

                String careteamName = getCareteamName(tempPatientEnrollment);
                tempPerson.setCareteam(careteamName);

                PersonDTO personDto = mapper.map(tempPerson, PersonDTO.class);

                // source
                String systemCode = null;
                try {
                    systemCode = patientManager.getSystemCode(euid);
                } catch (Exception ex) {
                    logger.warning("systemCode not found " + ex);
                }
                String source = ReportUtils.getUserFriendlySource(systemCode);
                personDto.setSource(source);
                searchResults.add(personDto);
            }
        }

        return searchResults;
    }

    private List<PersonDTO> findGenericCandidatesEnrolledPatientsWithPatientIdAsKey(Map<String, PatientCommunityEnrollment> patientEnrollments) throws Exception {
        long communityId = AuditUtils.getLoginResult(getThreadLocalRequest()).getCommunityId();
        String communityOid = AuditUtils.getLoginResult(getThreadLocalRequest()).getCommunityOid();
        List<PersonDTO> searchResults = new ArrayList<PersonDTO>();
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        PatientManager patientManager = this.getPatientManager(communityId);

        // get the patient enrollment
        Set<Map.Entry<String, PatientCommunityEnrollment>> entries = patientEnrollments.entrySet();
        List<String> patientIds = new ArrayList<>();
         if(patientEnrollments.keySet() != null ){
             patientIds.addAll(patientEnrollments.keySet());
        }
        long start = System.currentTimeMillis();
        Map<String, SimplePerson> simplePersonMapFromMPI = reportDAO.getPatientsFromMdm(patientManager,patientIds,communityOid);
        long end = System.currentTimeMillis();
        logger.info("Total:time taken getting data from MDM/Mrth :" + (end - start));
        
        for (Map.Entry<String, PatientCommunityEnrollment> tempEntry : entries) {
            String patientId = tempEntry.getKey();
            PatientCommunityEnrollment tempPatientEnrollment = tempEntry.getValue();

            Person tempPerson = null;

            try {
                tempPerson = new Person(simplePersonMapFromMPI.get(patientId));

            } catch (NullPointerException e) {
                logger.log(Level.WARNING, "MDM do not have record for patient patientId:" + patientId);
            }
            
            logger.fine("consented enrolled patient patientId: " + patientId);
            if (tempPerson != null ) {
                tempPerson.setPatientCommunityEnrollment(tempPatientEnrollment);

//                String careteamName = getCareteamName(tempPatientEnrollment);
//                tempPerson.setCareteam(careteamName);
                if (tempPatientEnrollment.getPatientCommunityCareteam() != null &&
                        tempPatientEnrollment.getPatientCommunityCareteam().getCommunityCareteam() != null ) {
                    tempPerson.setCareteam(tempPatientEnrollment.getPatientCommunityCareteam().getCommunityCareteam().getName());
                }

                PersonDTO personDto = mapper.map(tempPerson, PersonDTO.class);
              if (tempPatientEnrollment.getPatientSource() != null){
                personDto.setSource(tempPatientEnrollment.getPatientSource().getOrganization());
              }
              searchResults.add(personDto);
            }
        }

        return searchResults;
    }
    //add health Home 
    protected void setProgramHealthHome(Person tempPerson, PersonDTO personDto) {

        ProgramHealthHomeDTO programHealthHomeDTO = null;
        if (tempPerson.getPatientCommunityEnrollment().getProgramHealthHomeId() != 0) {
            programHealthHomeDTO = new ProgramHealthHomeDTO();
            ProgramHealthHome healthHome = programHealthHomeMap.get(tempPerson.getPatientCommunityEnrollment().getProgramHealthHomeId());
            programHealthHomeDTO.setHealthHomeName(healthHome.getHealthHomeValue());
            programHealthHomeDTO.setId(healthHome.getProgramHealthHomePK().getId());
            personDto.getPatientEnrollment().setProgramHealthHome(programHealthHomeDTO);

        }
    }

    private String getCareteamName(PatientCommunityEnrollment tempPatientEnrollment) {

        String careteamName = null;

        // get the patient id
        long patientId = 0;
        if (tempPatientEnrollment == null) {
            // skip this patient since patient enrollment is null
            return careteamName;
        } else {
            patientId = tempPatientEnrollment.getPatientId();
        }

        // check if patient id is in patient_community_careteam
        boolean exists = careteamDAO.doesPatientCommunityCareteamExist(patientId);

        // if so, then get care team id
        // - get care team name
        if (exists) {
            CommunityCareteam communityCareteam = careteamDAO.findCommunityCareteamForPatient(patientId);
            careteamName = communityCareteam.getName();
        }

        return careteamName;
    }

    /**
     * @deprecated @return @throws PortalException
     */
    @Override
    public int getTotalCountForReportUsers() throws PortalException {
        LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
        Long communityId = loginResult.getCommunityId();
        User loggedinUser = getUserFromSession();
        UserCommunityOrganization uco = loggedinUser.getUserCommunityOrganization(communityId);

        try {
            int count = userDAO.getTotalUserCount(uco, null);

            return count;
        } catch (Exception exc) {
            String message = "Error getting total count.";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }
    }

    @Override
    public SearchResults<UserDTO> getReportUsers(int pageNum, int pageSize) throws PortalException {
        LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
        Long communityId = loginResult.getCommunityId();
        User loggedinUser = getUserFromSession();
        UserCommunityOrganization uco = loggedinUser.getUserCommunityOrganization(communityId);

        try {
            int totalCount = userDAO.getTotalUserCount(uco, null);

            // get users
            List<User> users = userDAO.findUsers(uco, null, pageNum, pageSize);

            // Convert to DTOs, fully populated
            List<UserDTO> userDTOList = PropertyUtils.convertToDTOs(users, userCommunityOrganizationDAO, communityId);

            // build up search results
            SearchResults<UserDTO> searchResults = new SearchResults(totalCount, userDTOList);

            return searchResults;

        } catch (Exception ex) {
            String message = "Error retrieving users: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error retrieving users.");
        }
    }

    @Override
    public int getTotalCountForEnrolledPatients(long communityId, long userId, long accessLevelId) throws PortalException {
        try {
            return reportDAO.getTotalCountForEnrolledPatients(communityId, userId, accessLevelId);
        } catch (Exception ex) {
            String message = "Error retrieving enrolled patients: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error retrieving enrolled patients.");
        }
    }

    /*@Override
    public int getTotalCountForCandidatePatientsNotEnrolled(long communityId, long currentUserOrgId) throws PortalException {
        try {
            return reportDAO.getTotalCountForCandidatePatientsNotEnrolledPatients(communityId, currentUserOrgId);
        } catch (Exception ex) {
            String message = "Error retrieving not enrolled patients: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error retrieving not enrolled patients.");
        }
    }*/

    @Override
    public SearchResults<PersonDTO> getCandidatePatientsNotEnrolled(long communityId, int pageNum, int pageSize) throws PortalException {

        try {

            LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
            long accessLevelId = loginResult.getAccessLevelId();
            long userId = loginResult.getUserId();
            List<PatientReportSheet> nonConsentedPatientSheets = getNonConsentedPatientRecords(communityId, userId, accessLevelId, pageNum, pageSize);

            List<PersonDTO> personDto = converNonConsentedPatientToDTO(nonConsentedPatientSheets);
            SearchResults<PersonDTO> searchResults = new SearchResults(getTotalCountForCandidatePatientsNotEnrolled(communityId), personDto);

            return searchResults;

        } catch (Exception exc) {
            final String message = "Error searching for candidates not enrolled.";
            logger.logp(Level.SEVERE, getClass().getName(), "getCandidatePatientsNotEnrolled", message, exc);

            throw new PortalException(message);
        }


        /* try {
            // get total count
            int totalCount = reportDAO.getTotalCountForCandidatePatientsNotEnrolledPatients(communityId);

            // get patients
            Map<String, PatientCommunityEnrollment> patientEnrollments = reportDAO.findCandidatePatientsNotEnrolledPatients(communityId, pageNum, pageSize);
            List<PersonDTO> patients = findGenericCandidatesEnrolledPatients(patientEnrollments);

            // build up search results
            SearchResults<PersonDTO> searchResults = new SearchResults(totalCount, patients);

            return searchResults;

        } catch (Exception exc) {
            final String message = "Error searching for candidates not enrolled.";
            logger.logp(Level.SEVERE, getClass().getName(), "getCandidatePatientsNotEnrolled", message, exc);

            throw new PortalException(message);
        }*/
    }


    @Override
    public int getTotalCountForCandidatePatientsNotEnrolled(long communityId) throws PortalException {
        try {
            LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
            long accessLevelId = loginResult.getAccessLevelId();
            long userId = loginResult.getUserId();
            if (accessLevelId == WebConstants.POWER_USER_ACCESS_LEVEL_ID) {
                logger.info("  looking getTotalCountForCandidatePatientsNotEnrolled for power user : ");
                //  allPatientsList = filterPatientsWithDemographics(communityId, searchCriteria);
                return reportDAO.findTotalCountNonConsentedRecordsForPowerUser(communityId);
            } else {
                logger.info(" looking getTotalCountForCandidatePatientsNotEnrolled for non power user : ");
                return reportDAO.findTotalCountNonConsentedRecordsForNonPowerUser(communityId, userId);
//allPatientsList = filterdPatientsWithDemographics(communityId, userId, searchCriteria);
            }
        } catch (Exception ex) {
            String message = "Error retrieving not enrolled patients: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error retrieving not enrolled patients.");
        }
    }

    private LinkedHashMap<Long, ProgramHealthHome> getProgramNameHealthHomeMap() {

        List<ProgramHealthHome> programHealthHomeList = reportDAO.findProgramHealthHomeEntities();
        LinkedHashMap<Long, ProgramHealthHome> data = new LinkedHashMap<Long, ProgramHealthHome>();
        for (ProgramHealthHome phh : programHealthHomeList) {

            data.put(phh.getProgramHealthHomePK().getId(), phh);
        }
        return data;
    }

    public SearchResults<PersonDTO> getPatientPrograms(long userId, long accessLevelId, long currentUserOrgId, long communityId, SearchCriteria searchCriteria, int pageNum, int pageSize) throws PortalException {

        try {


            List<PatientProgramSheet> patientProgramSheets = getPatientProgramRecords(communityId, userId, accessLevelId, searchCriteria, pageNum, pageSize);
            logger.info("getPatientPrograms record count:" + patientProgramSheets.size());
            List<PersonDTO> personDto = converPatientProgramToDTO(patientProgramSheets);
            SearchResults<PersonDTO> searchResults = new SearchResults(getTotalCountOfPrograms(userId, accessLevelId, currentUserOrgId, communityId, searchCriteria), personDto);

            return searchResults;

        } catch (Exception exc) {
            final String message = "Error searching for getPatientPrograms";
            logger.logp(Level.SEVERE, getClass().getName(), "getPatientPrograms", message, exc);

            throw new PortalException(message);
        }

//        HttpSession session = getThreadLocalRequest().getSession();
   /*     try {
            int totalCount = 0;
            SearchResults<PersonDTO> searchResults = new SearchResults();
            if (searchCriteria != null) {
                searchResults = findPatientWithPrograms(communityId, userId, searchCriteria);
                System.out.println("Search Criteria not null");
            } else {

                System.out.println("Search Criteria null");
                boolean isPowerUser = accessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID;
                if (isPowerUser) {
                    totalCount = reportDAO.CountPatientsForPowerUser(communityId);
                } else {
                    totalCount = reportDAO.CountPatientsForNotPowerUser(userId, communityId);
                }
//          find generic patients from PCE
                List<PersonDTO> allPatientList = getPatientsFromPCE(userId, accessLevelId, currentUserOrgId, communityId, pageNum, pageSize);
//       
                // build up search results
                searchResults = new SearchResults(totalCount, allPatientList);

            }
            return searchResults;
//            return getFinalListOfRecords(session, pageNum, pageSize);

        } catch (Exception exc) {
            final String message = "Error searching for patients programs.";
            logger.logp(Level.SEVERE, getClass().getName(), "getPatientPrograms", message, exc);

            throw new PortalException(message);
        }*/
    }

    public List<PersonDTO> getPatientsFromPCE(long userId, long accessLevelId, long currentUserOrgId, long communityId, int pageNum, int pageSize) throws PortalException {
        try {
            List<PersonDTO> allPatientList = new ArrayList<PersonDTO>();
            Map<String, PatientCommunityEnrollment> patientEnrollmentsforNotEnrolled = new HashMap<String, PatientCommunityEnrollment>();

            boolean isPowerUser = accessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID;
            // get patients from PCE
            Map<String, PatientCommunityEnrollment> patientEnrollments = new HashMap<String, PatientCommunityEnrollment>();
            if (isPowerUser) {

                patientEnrollments = reportDAO.findTotalPatientsForPowerUser(communityId, pageNum, pageSize);
                List<PersonDTO> totalPatients = findGenericCandidatesEnrolledPatients(patientEnrollments);
                setPatientProgramAndStatus(totalPatients);
                return totalPatients;

            } else {
                patientEnrollments = reportDAO.findPatientsForNotPowerUser(userId, communityId, pageNum, pageSize);
                List<PersonDTO> patients = findGenericCandidatesEnrolledPatients(patientEnrollments);
                setPatientProgramAndStatus(patients);
                return patients;

            }

        } catch (Exception exc) {
            final String message = "Error searching for patients in PCE.";
            logger.logp(Level.SEVERE, getClass().getName(), "getPatiengetPatientsFromPCE", message, exc);

            throw new PortalException(message);
        }

    }
//     public SearchResults<PersonDTO> getFinalListOfRecords(HttpSession session, int pageNum, int pageSize) {
//
//        SearchResults<PersonDTO> results = (SearchResults<PersonDTO>) session.getAttribute("records");
////          SearchResults<PersonDTO> result=results;
//        int endIndex = (pageNum * pageSize) - 1;
//        int startIndex = (endIndex + 1) - pageSize;
//        
//        if (endIndex > results.getData().size() - 1) {
//            endIndex = results.getData().size() - 1;
//        }
//        List<PersonDTO> person = new ArrayList<PersonDTO>();
//
//        for (int i = startIndex; i <= endIndex; i++) {
//            person.add(results.getData().get(i));
//        }
//        SearchResults<PersonDTO> searchResults = new SearchResults(results.getTotalCount(), person);
//
//        return searchResults;
//    }

    public int getTotalCountOfPrograms(long userId, long accessLevelId, long orgId, long communityId, SearchCriteria searchCriteria) throws PortalException {

        try {
              PatientManager patientManager=getPatientManager(communityId);
            if (accessLevelId == WebConstants.POWER_USER_ACCESS_LEVEL_ID) {
                logger.info("  looking getTotalCountOfPrograms for power user : ");
                //  allPatientsList = filterPatientsWithDemographics(communityId, searchCriteria);
                return reportDAO.findTotalCountPatientProgramRecordsForPowerUser(communityId, patientManager,searchCriteria);
            } else {
                logger.info(" looking getTotalCountOfPrograms for non power user : ");
                return reportDAO.findTotalCountPatientProgramRecordsForNonPowerUser(communityId, userId,patientManager, searchCriteria);
//allPatientsList = filterdPatientsWithDemographics(communityId, userId, searchCriteria);
            }

        } catch (Exception ex) {
            String message = "Error getTotalCountOfPrograms: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error getTotalCountOfPrograms.");
        }

        /* try {
            int totalCount = 0;
            if (searchCriteria != null) {

                List<PersonDTO> patients = new ArrayList<PersonDTO>();

                boolean hasDemographics = EnrollmentUtils.hasDemographicData(searchCriteria);

                if (hasDemographics) {
                    logger.info(" if hasDemographics : ");

                    if (accessLevelId == WebConstants.POWER_USER_ACCESS_LEVEL_ID) {
                        logger.info(" hasDemographics looking patients for power user : ");
                        patients = filterPatientsWithDemographics(communityId, searchCriteria);
                    } else {
                        patients = filterPatientsWithDemographics(communityId, userId, searchCriteria);
                    }
                    totalCount = patients.size();
                    System.out.println("Total count>>" + patients.size());
                }
            } else {

                long start = System.currentTimeMillis();

                boolean isPowerUser = accessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID;
                // get patients from PCE
                Map<String, PatientCommunityEnrollment> patientEnrollments = new HashMap<String, PatientCommunityEnrollment>();
                if (isPowerUser) {
                    totalCount = reportDAO.CountPatientsForPowerUser(communityId);
                } else {
                    totalCount = reportDAO.CountPatientsForNotPowerUser(userId, communityId);
                }
                long end = System.currentTimeMillis();
                logger.info("Total associated program counts= " + totalCount + ":Total:time taken:" + (end - start));

            }
            return totalCount;
        } catch (Exception exc) {
            final String message = "Error getting total patient program count";
            logger.logp(Level.SEVERE, getClass().getName(), "getTotalCountOfPrograms", message, exc);

            throw new PortalException(message);
        }*/
    }

    public List<PersonDTO> getEnrolledPatients(long userId, long accessLevelId) throws PortalException {

        try {
            // get patients
            Map<String, PatientCommunityEnrollment> patientEnrollments = reportDAO.findEnrolledPatients(userId, accessLevelId);
            List<PersonDTO> patients = findGenericCandidatesEnrolledPatients(patientEnrollments);

            // build up search results
            return patients;

        } catch (Exception exc) {
            final String message = "Error searching for enrolled patients.";
            logger.logp(Level.SEVERE, getClass().getName(), "findEnrolledPatients", message, exc);

            throw new PortalException(message);
        }
    }

    public SearchResults<PersonDTO> findPatientWithPrograms(long communityId, long userId, SearchCriteria searchCriteria) throws PortalException {

        try {

            LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
            long accessLevelId = loginResult.getAccessLevelId();

            List<PatientProgramSheet> patientProgramSheets = getPatientProgramRecords(communityId, userId, accessLevelId, searchCriteria, -1, -1);


            List<PersonDTO> personDto = converPatientProgramToDTO(patientProgramSheets);
            SearchResults<PersonDTO> searchResults = new SearchResults(getTotalCountOfPrograms(userId, accessLevelId, 0, communityId, searchCriteria), personDto);

            return searchResults;

        } catch (Exception exc) {
            final String message = "Error searching for findPatientWithPrograms";
            logger.logp(Level.SEVERE, getClass().getName(), "findPatientWithPrograms", message, exc);

            throw new PortalException(message);
        }

        /*  logger.info(" findPatientWithPrograms : " + communityId);
        try {
            LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
            Long userAccessLevelId = loginResult.getAccessLevelId();

            List<PersonDTO> patients = new ArrayList<PersonDTO>();

            boolean hasDemographics = EnrollmentUtils.hasDemographicData(searchCriteria);

            if (hasDemographics) {
                logger.info(" if hasDemographics : ");

                if (userAccessLevelId == WebConstants.POWER_USER_ACCESS_LEVEL_ID) {
                    logger.info(" hasDemographics looking patients for power user : ");
                    patients = filterPatientsWithDemographics(communityId, searchCriteria);
                } else {
                    logger.info("hasDemographics looking patients for non power user : ");
                    patients = filterPatientsWithDemographics(communityId, userId, searchCriteria);
                }
            }
            setPatientProgramAndStatus(patients);
//else {
//                if (userAccessLevelId == WebConstants.POWER_USER_ACCESS_LEVEL_ID) {
//                    logger.info(" looking patients for power user : ");
//                    patients = filterEnrolledPatientsWithoutDemographics(userId, searchCriteria);
//                } else {
//                    logger.info(" looking patients for non power user : ");
//                    patients = filterEnrolledPatientsWithoutDemographics(userId, searchCriteria);
//                }
//            }

            logger.info(" findPatientWithPrograms for " + userId + " patient list size: " + patients.size());
            int totalCount = patients.size();
            System.out.println("Search result toatal count" + patients.size());
            SearchResults<PersonDTO> searchResults = new SearchResults(totalCount, patients);

            return searchResults;

        } catch (Exception exc) {
            final String message = "Error retrieving patients. " + searchCriteria.getLastName() + "" + searchCriteria.getFirstName();
            logger.logp(Level.SEVERE, getClass().getName(), "findPatientWithPrograms", message, exc);

            throw new PortalException("Error retrieving care team users.");
        }
*/
    }

    protected List<PersonDTO> filterPatientsWithDemographics(long communityId, SearchCriteria searchCriteria) throws MappingException, NumberFormatException, Exception {

        logger.info(" power user filterPatientsWithDemographics ReportServiceImpl  :" + communityId);

        List<PersonDTO> searchResults = new ArrayList<PersonDTO>();

        // setup
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        List<SimplePerson> results = this.getPatientManager(communityId).findDashboardPatient(searchCriteria);

        // get the patient enrollment
        for (SimplePerson sp : results) {
            String euid = sp.getPatientEuid();
            logger.info(" euid :" + euid + ":LASTNAME:" + sp.getLastName());
            Person tempPerson = new Person(sp);
            try {
                PatientCommunityEnrollment tempPatientEnrollment = reportDAO.findPatientEnrollment(euid, communityId);
                if (tempPatientEnrollment != null) {
                    tempPerson.setPatientCommunityEnrollment(tempPatientEnrollment);
                    PersonDTO personDto = mapper.map(tempPerson, PersonDTO.class);
                    searchResults.add(personDto);
                }
            } catch (NoResultException exc) {
                logger.warning("===========>   Did not find person in our local database..."
                        + "not including in search results " + exc);
            }
        }

        return searchResults;
    }


    /**
     * TODO - CLEAN UP DUPLICATION
     * @param communityId
     * @param userId
     * @param searchCriteria
     * @return
     * @throws MappingException
     * @throws NumberFormatException
     * @throws Exception 
     */
    protected List<PersonDTO> filterPatientsWithDemographics(long communityId, long userId, SearchCriteria searchCriteria) throws MappingException, NumberFormatException, Exception {
        logger.info(" filterPatientsWithDemographics ReportServiceImpl not poweruser  :");

        List<PersonDTO> searchResults = new ArrayList<PersonDTO>();

        // setup
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        List<SimplePerson> results = this.getPatientManager(communityId).findDashboardPatient(searchCriteria);

        // get the patient enrollment
        for (SimplePerson sp : results) {
            String euid = sp.getPatientEuid();
            // logger.info(" patientId :" + tempPerson.getEuid() + ":LASTNAME:" + tempPerson.getLastName() + ":SSN:" + tempPerson.getSsn());
            Person tempPerson = new Person(sp);
            try {
                PatientCommunityEnrollment tempPatientEnrollment = reportDAO.findPatientEnrollment(euid, communityId);
                if (tempPatientEnrollment != null) {
                    tempPerson.setPatientCommunityEnrollment(tempPatientEnrollment);
                    long patientId = tempPerson.getPatientCommunityEnrollment().getPatientId();
                    // do not include we don't have consent for patient
                    boolean hasConsent = userPatientConsentDAO.hasConsent(userId, patientId, communityId);
                    if (!hasConsent) {
                        logger.fine("SKIP:  user does not have consent. euid=" + euid + ", userId=" + userId + ", patientId=" + patientId);

                    } else {
                        tempPerson.setPatientCommunityEnrollment(tempPatientEnrollment);
                        PersonDTO personDto = mapper.map(tempPerson, PersonDTO.class);
                        searchResults.add(personDto);

                    }
                }

            } catch (NoResultException exc) {
                logger.warning("===========>   Did not find person in our local database...not including in search results");
            }
        }

        return searchResults;
    }

    public ProgramNameDTO convertTODTO(ProgramName programName) {
        ProgramNameDTO programDTO = new ProgramNameDTO();
        programDTO.setId(programName.getProgramNamePK().getId());
        programDTO.setValue(programName.getValue());

        return programDTO;
    }

    public PatientStatusDTO convert(PatientStatus patientStatus) {
        PatientStatusDTO statusDTO = new PatientStatusDTO();
        statusDTO.setId(patientStatus.getPatientStatusPK().getId());
        statusDTO.setValue(patientStatus.getValue());

        return statusDTO;
    }

    public void setPatientProgramAndStatus(List<PersonDTO> patientsList) {
        LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
        Long communityId = loginResult.getCommunityId();

        try {

            for (PersonDTO person : patientsList) {
                List<PatientProgramDTO> programList = person.getPatientEnrollment().getPatientProgramName();
                for (PatientProgramDTO program : programList) {
                    ProgramName programName = patientProgramDAO.getProgramName(program.getProgramId(), communityId);
                    ProgramNameDTO prog = convertTODTO(programName);
                    program.setProgram(prog);

                    PatientStatus status = patientProgramDAO.getPatientStatus(program.getPatientStatus(), communityId);
                    PatientStatusDTO patientStatus = convert(status);
                    program.setStatus(patientStatus);

                }
            }

        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error getting associated programs", exc);

        }
    }

    @Override
    public Long findPTSRecordsCount(long communityId, long userId, long accessLevelId) throws PortalException {
        try {
            long count;
            boolean isPowerUser = accessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID;
            boolean isSuperUser = accessLevelId == AccessLevelConstants.SUPER_USER_ACCESS_LEVEL_ID;
            if (isPowerUser) {
                count = reportDAO.findPTSCountForPowerUser(communityId);

            } else if (isSuperUser) {
                count = reportDAO.findPTSCountForSuperUser(communityId, userId);
            } else {
                count = reportDAO.findPTSCountForLocalAdminUser(communityId, userId);
            }
            logger.info("PTS Conditional Count :" + count);
            return count;
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error during getting pts records count ", e);
            throw new PortalException(e.getMessage());
        }
    }

    private List<PersonDTO> converNonConsentedPatientToDTO(List<PatientReportSheet> nonConsentedPatientSheets) {
        List<PersonDTO> dtoList = new ArrayList<>();
        if (nonConsentedPatientSheets != null && nonConsentedPatientSheets.size() > 0) {
            for (PatientReportSheet ncps : nonConsentedPatientSheets) {
                PersonDTO dTO = new PersonDTO();
                PatientEnrollmentDTO enrollmentDTO = new PatientEnrollmentDTO();
                PayerClassDTO payerClassDTO = new PayerClassDTO();
                PayerPlanDTO payerPlanDTO = new PayerPlanDTO();

                payerClassDTO.setName(ncps.getPrimaryPayerClass());
                payerPlanDTO.setName(ncps.getPrimaryPayerPlan());

                enrollmentDTO.setAcuityScore(ncps.getAcuityScore());
                enrollmentDTO.setStatus(ncps.getEnrollmentStatus());
                enrollmentDTO.setOrganizationName(ncps.getOrganization());
                enrollmentDTO.setPatientEuid(ncps.getPatientEuid());
                enrollmentDTO.setPatientId(ncps.getPatientId());
                enrollmentDTO.setPrimaryPayerClass(payerClassDTO);
                enrollmentDTO.setPrimaryPayerPlan(payerPlanDTO);
                enrollmentDTO.setPrimaryPayerMedicaidMedicareId(ncps.getmPayerID());
                enrollmentDTO.setProgramLevelConsentStatus(ncps.getProgramEnrollmentConsent());
                dTO.setPatientEnrollment(enrollmentDTO);
                dTO.setStreetAddress1(ncps.getAddressLine1());
                dTO.setStreetAddress2(ncps.getAddressLine2());
                dTO.setCity(ncps.getCity());

                dTO.setFirstName(ncps.getFirstName());

                dTO.setLastName(ncps.getLastName());
                dTO.setMiddleName(ncps.getMiddleName());

                dTO.setEuid(ncps.getPatientEuid());

                dTO.setSource(ncps.getSource());

                dTO.setSsn(ncps.getSsn());
                dTO.setState(ncps.getState());

                dTO.setZipCode(ncps.getZipCode());

                dtoList.add(dTO);
            }
        }
        return dtoList;
    }

    private List<PatientReportSheet> getNonConsentedPatientRecords(long communityId, long userId, long accessLevelId, int pageNum, int pageSize) throws PortalException{
        List<PatientReportSheet> nonConsentedPatientSheets;
        LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
        String communityOid=loginResult.getCommunityOid();
        PatientManager patientManager=getPatientManager(communityId);
     try{
                  
        if (accessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
            logger.info("Running service for PatientsNotEnrolled: Power User");
            logger.info("Getting patients...");
            nonConsentedPatientSheets = reportDAO.findNonConsentedRecordsForPowerUser(communityId, pageNum, pageSize);
            addDamographicDataToNonConsentedSheet(nonConsentedPatientSheets, patientManager, communityOid);
           
//  Map<String, PatientCommunityEnrollment> patientEnrollments = reportDAO.findAllCandidates(communityId);
            //  enrollmentServicesList = findGenericCandidatesEnrolledPatients(patientEnrollments);
        } else {
            logger.info("Running service for PatientsNotEnrolled: userId=" + userId);
            logger.info("Getting patients...");          
            nonConsentedPatientSheets = reportDAO.findNonConsentedRecordsForNonPowerUser(communityId, userId, pageNum, pageSize);
             addDamographicDataToNonConsentedSheet(nonConsentedPatientSheets, patientManager, communityOid);
            // Map<String, PatientCommunityEnrollment> patientEnrollments = reportDAO.findCandidates(communityId,organizationId);
            //enrollmentServicesList = findGenericCandidatesEnrolledPatients(patientEnrollments);
        }}
     catch (Exception ex) {
            String message = "Error retrieving not consented patients: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error retrieving not consented patients.");
        }
        return nonConsentedPatientSheets;
    }

    private List<PatientProgramSheet> getPatientProgramRecords(long communityId, long userId, long accessLevelId, SearchCriteria criteria, int pageNum, int pageSize)throws PortalException {
        List<PatientProgramSheet> patientProgramSheets;
        PatientManager patientManager=this.getPatientManager(communityId);
        LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
      try{
        if (accessLevelId == WebConstants.POWER_USER_ACCESS_LEVEL_ID) {
            logger.info("  looking getPatientProgramRecords for power user : ");
            //  allPatientsList = filterPatientsWithDemographics(communityId, searchCriteria);
            patientProgramSheets = reportDAO.findPatientProgramRecordsForPowerUser(communityId, criteria,patientManager, pageNum, pageSize);
            addDamographicDataToPatientProgramSheet(patientProgramSheets, patientManager, loginResult.getCommunityOid());
        } else {
            logger.info(" looking getPatientProgramRecords for non power user : ");
            patientProgramSheets = reportDAO.findPatientProgramRecordsForNonPowerUser(communityId, userId, criteria,patientManager, pageNum, pageSize);
            addDamographicDataToPatientProgramSheet(patientProgramSheets, patientManager, loginResult.getCommunityOid());
            //allPatientsList = filterdPatientsWithDemographics(communityId, userId, searchCriteria);
        }}
       catch (Exception ex) {
            String message = "Error retrieving patient programs: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error retrieving patient programs.");
        }
        return patientProgramSheets;
    }
    private Map<String, List<PatientProgramDTO>> getPatientProgramsGroupedByPatientId(List<PatientProgramSheet> patientProgramSheets) {
        Map<String, List<PatientProgramDTO>> map = new HashMap<String, List<PatientProgramDTO>>();

        for (PatientProgramSheet sheet : patientProgramSheets) {
            String key = sheet.getPatientID();
            PatientProgramDTO patientProgramDTO = new PatientProgramDTO();
            ProgramNameDTO nameDTO = new ProgramNameDTO();
            PatientStatusDTO statusDTO = new PatientStatusDTO();

            statusDTO.setValue(sheet.getPatientStatus());
            statusDTO.setTerminationReason(sheet.getProgramEndReason());
            nameDTO.setValue(sheet.getProgramName());

            patientProgramDTO.setProgram(nameDTO);
            patientProgramDTO.setStatus(statusDTO);

            patientProgramDTO.setHealthHome(sheet.getHealthHome());
            patientProgramDTO.setProgramEffectiveDate(sheet.getProgramEffective());
            patientProgramDTO.setProgramEndDate(sheet.getProgramEnd());
            patientProgramDTO.setStatusEffectiveDate(sheet.getStatusEffective());
            patientProgramDTO.setTerminationReason(sheet.getProgramEndReason());
            patientProgramDTO.setPatientId(Long.valueOf(sheet.getPatientID()));

            if (map.containsKey(key)) {
                List<PatientProgramDTO> list = map.get(key);
                list.add(patientProgramDTO);

            } else {
                List<PatientProgramDTO> list = new ArrayList<>();
                list.add(patientProgramDTO);
                map.put(key, list);
            }

        }

        return map;
    }
    private List<PersonDTO> converPatientProgramToDTO(List<PatientProgramSheet> patientProgramSheets) {
        List<PersonDTO> dtoList = new ArrayList<>();

        if (patientProgramSheets != null && patientProgramSheets.size() > 0) {
            Map<String, List<PatientProgramDTO>> patientProgramMap = getPatientProgramsGroupedByPatientId(patientProgramSheets);
            logger.info("converPatientProgramToDTO map count:" + patientProgramMap.size());
            for (PatientProgramSheet ncps : patientProgramSheets) {
                if (patientProgramMap.get(ncps.getPatientID()) != null) {
                    PersonDTO dTO = new PersonDTO();
                PatientEnrollmentDTO enrollmentDTO = new PatientEnrollmentDTO();


                enrollmentDTO.setPatientProgramName(patientProgramMap.get(ncps.getPatientID()));

                // enrollmentDTO.setStatus(ncps.getPatientStatus());
                // enrollmentDTO.setHealthHome(ncps.getHealthHome());

                enrollmentDTO.setPatientId(Long.valueOf(ncps.getPatientID()));


                dTO.setPatientEnrollment(enrollmentDTO);


                dTO.setFirstName(ncps.getFirstName());

                dTO.setLastName(ncps.getLastName());

                dTO.setDateOfBirth(ncps.getDob());

                dTO.setGender(ncps.getGender());

                    dtoList.add(dTO);
                    patientProgramMap.remove(ncps.getPatientID());
            }

            }
        }
        logger.info("converPatientProgramToDTO personDTO count:" + dtoList.size());
        return dtoList;
    }
    @Override
     protected PatientManager getPatientManager(Long communityId) {
        ServletContext servletContext = getServletContext();
        Map<Long, CommunityContexts> communityContexts
                = ((Map<Long, CommunityContexts>) servletContext.getAttribute(WebConstants.COMMUNITY_SPECIFIC_DATA_KEY));
        return communityContexts.get(communityId).getPatientManager();
    }
     /**
     * FIXME : Single entity class for all reports can be used. So, we don't
     * need three different methods for each report.
     */
    private void addDamographicDataToNonConsentedSheet(List<PatientReportSheet> nonConsentedPatientSheet,PatientManager patientManager, String communityOid) throws Exception {
        List<String> patientList = new ArrayList<>();

        logger.info("Getting patientIds in list..");
        for (PatientReportSheet temp : nonConsentedPatientSheet) {
            patientList.add(temp.getPatientId().toString());
        }
     
        logger.info("Start retrieving patient demographic data from MDM/Mirth with the help of patientIds...");
        Map<String, SimplePerson> demographicData=  reportDAO.getPatientsFromMdm(patientManager, patientList, communityOid);
        
//        Map<String, SimplePerson> demographicData = patientManager.getMappedPatientsDirectQuery(em, patientList, communityOid);

        ListIterator listItr = nonConsentedPatientSheet.listIterator();

        logger.info("Adding demographic data to nonConsentedPatientSheet..");
        while (listItr.hasNext()) {
            PatientReportSheet nonConsentedPatientSheetTemp = (PatientReportSheet) listItr.next();
            SimplePerson simplePerson = demographicData.get(nonConsentedPatientSheetTemp.getPatientId().toString());
            convertMdmSbrToSP(simplePerson, nonConsentedPatientSheetTemp);
        }
    }
      private void convertMdmSbrToSP(SimplePerson sbr, PatientReportSheet nonConsentedPatientSheet) throws Exception {
        if (sbr != null && nonConsentedPatientSheet != null) {
            nonConsentedPatientSheet.setFirstName(sbr.getFirstName());
            nonConsentedPatientSheet.setMiddleName(sbr.getMiddleName());
            nonConsentedPatientSheet.setLastName(sbr.getLastName());
            nonConsentedPatientSheet.setAddressLine1(sbr.getStreetAddress1());
            nonConsentedPatientSheet.setAddressLine2(sbr.getStreetAddress2());
            nonConsentedPatientSheet.setCity(sbr.getCity());
            nonConsentedPatientSheet.setState(sbr.getState());
            nonConsentedPatientSheet.setZipCode(sbr.getZipCode());
            nonConsentedPatientSheet.setSsn(sbr.getSSN());
        } else {
            logger.info("Some thing wrong with Id : ...Looks like we have patient_id in PCE,"
                    + " but corresponding lid does not present in sbyn.enterprise" + nonConsentedPatientSheet.getPatientId());
        }
    }
      /**
     * FIXME : Single entity class for all reports can be used. So, we don't
     * need three different methods of each report.
     */
    private void addDamographicDataToPatientProgramSheet(List<PatientProgramSheet> pps, PatientManager patientManager,String communityOid) throws Exception {
        List<String> patientList = new ArrayList<String>();

        logger.info("Getting patientIds in list..");
        for (PatientProgramSheet temp : pps) {
            patientList.add(temp.getPatientID().toString());
        }
        logger.info("Start retrieving patient demographic data from MDM/Mirth with the help of patientIds...");
//        Map<String, SimplePerson> demographicData = patientManager.getMappedPatientsDirectQuery(em, patientList, communityOid);
         Map<String, SimplePerson> demographicData=  reportDAO.getPatientsFromMdm(patientManager, patientList, communityOid);
        ListIterator listItr = pps.listIterator();

        logger.info("Adding demographic data to patientProgramSheet..");
        while (listItr.hasNext()) {
            PatientProgramSheet ppsTemp = (PatientProgramSheet) listItr.next();
            SimplePerson simplePerson = demographicData.get(ppsTemp.getPatientID().toString());
            if (simplePerson == null) {
                listItr.remove();
            } else {
                convertMdmSbrToSP(simplePerson, ppsTemp);
            }

        }
    }
    
     /**
     *
     * @param sbr
     * @param patientProgramSheet
     * @throws Exception This method sets the demographic data to non-consented
     * sheet
     */
    private void convertMdmSbrToSP(SimplePerson sbr, PatientProgramSheet patientProgramSheet) throws Exception {
        if (sbr != null && patientProgramSheet != null) {
            patientProgramSheet.setFirstName(sbr.getFirstName());
            patientProgramSheet.setLastName(sbr.getLastName());
            patientProgramSheet.setDob(sbr.getDateOfBirth());
            patientProgramSheet.setGender(sbr.getGenderCode());

        } else {
            logger.info("Some thing wrong with Id : ...Looks like we have patient_id in PCE,"
                    + " but corresponding lid does not present in sbyn.enterprise" + patientProgramSheet.getPatientID());
        }
    }


}
