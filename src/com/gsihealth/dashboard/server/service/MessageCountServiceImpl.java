/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.service.MessageCountService;
import com.gsihealth.dashboard.common.ConfigurationConstants;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.dashboard.server.util.AuditUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author User
 */
public class MessageCountServiceImpl extends BasePortalServiceServlet implements MessageCountService {

    String inputJson = "{\"serviceParam\": {\"tokenid\": \"$token$\",\"communityid\": $community$ ,\"userDirectAddress\": \"$email$\"  }}";

    private Logger logger = Logger.getLogger(getClass().getName());

    /**
     * Initializes the service with DAOs
     */
    @Override
    public void init() throws ServletException {
        super.init();

    }

   @Override
    public int getUnreadMailCount() throws PortalException {
        try {
            ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

            LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
         if(loginResult!=null){
            String tokenid = loginResult.getToken();
            long communityId = loginResult.getCommunityId();
            String emailId=loginResult.getDirectAddress();
            String messagesServerUrl = configurationDAO.getProperty(communityId, ConfigurationConstants.Message_SERVER);
            messagesServerUrl = messagesServerUrl.replace("syncUser", "");
             messagesServerUrl = messagesServerUrl + "fetchFolders/unreadInboxCount";
            String jsonInput = inputJson.replace("$token$", tokenid).replace("$community$", String.valueOf(communityId)).replace("$email$", emailId);
             ClientResponse cResponse = hitMailService(jsonInput, messagesServerUrl);
              logger.info("Mail Response code:"+cResponse.getResponseStatus().getStatusCode());
             if(cResponse!=null && cResponse.getResponseStatus().getStatusCode()!=200){
                 return -1;
             }
            String response = cResponse.getEntity(String.class);
            int mailCount= parseJson(response);
            logger.info("Mail Count:"+mailCount);
            return mailCount;
         }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
        return 0;

    }

    private int parseJson(String json) throws JSONException {
        int messageCount = 0;
        JSONObject obj = new JSONObject(json);
        JSONArray arr = obj.getJSONArray("folders");
        for (int i = 0; i < arr.length(); i++) {
            JSONObject obj1 = arr.getJSONObject(i);
            String fullName = obj1.getString("fullName");
            if (fullName.equalsIgnoreCase("INBOX")) {
                messageCount = obj1.getInt("unseeMessageCount");
            }
        }
        return messageCount;
    }

    private ClientResponse hitMailService(String jsonMessage, String serviceUrl) {
        Client client = Client.create();

        WebResource webResource = client
                .resource(serviceUrl);

        String input = jsonMessage;

        ClientResponse response = webResource.type("application/json")
                .post(ClientResponse.class, input);

        return response;

    }

}
