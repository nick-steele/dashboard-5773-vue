package com.gsihealth.dashboard.server.service;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.gsihealth.entity.User;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.CommunityContexts;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.AccessLevelDAO;
import com.gsihealth.dashboard.server.dao.ApplicationDAO;
import com.gsihealth.dashboard.server.dao.CommunityDAO;
import com.gsihealth.dashboard.server.dao.CommunityOrganizationDAO;
import com.gsihealth.dashboard.server.dao.FacilityTypeDAO;
import com.gsihealth.dashboard.server.dao.OrganizationDAO;
import com.gsihealth.dashboard.server.dao.PatientEnrollmentDAO;
import com.gsihealth.dashboard.server.dao.RoleDAO;
import com.gsihealth.dashboard.server.dao.SSOUserDAO;
import com.gsihealth.dashboard.server.dao.UserCommunityOrganizationDAO;
import java.util.logging.Logger;
import com.gsihealth.dashboard.server.dao.UserDAO;
import com.gsihealth.dashboard.server.hisp.HispClient;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import com.gsihealth.dashboard.server.service.integration.CommunityOrganizationService;
import com.gsihealth.dashboard.server.service.integration.UserService;
import com.gsihealth.dashboard.server.util.AuditUtils;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Collection of base data members and methods
 * 
 */
public class BasePortalServiceServlet extends RemoteServiceServlet {
  
    private Logger logger = Logger.getLogger(getClass().getName());

    protected AccessLevelDAO accessLevelDAO;
    protected ApplicationDAO applicationDAO;
    protected CommunityDAO communityDAO;
    protected CommunityOrganizationDAO communityOrganizationDAO;
    protected CommunityOrganizationService communityOrganizationService;
    protected FacilityTypeDAO facilityTypeDAO;
    protected Map<Long, CommunityContexts> communityContexts;
    protected OrganizationDAO organizationDAO;
    protected RoleDAO roleDAO;
    protected SSOUserDAO sSOUserDAO;
    protected UserCommunityOrganizationDAO userCommunityOrganizationDAO;
    protected UserDAO userDAO;
    protected UserService userService;

    public BasePortalServiceServlet() {
        
    }
    
    @Override
    public void init() throws ServletException {
        ServletContext application = getServletContext();
        communityContexts = 
        ((Map<Long, CommunityContexts>)application.getAttribute(WebConstants.COMMUNITY_SPECIFIC_DATA_KEY));
    }
    
    public LoginResult getAssociatedUser() {
    	return AuditUtils.getLoginResult(getThreadLocalRequest());
    }

    /**
     * @param userDAO the userDAO to set
     */
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    /**
     * @param accessLevelDAO the accessLevelDAO to set
     */
    public void setAccessLevelDAO(AccessLevelDAO accessLevelDAO) {
        this.accessLevelDAO = accessLevelDAO;
    }

    /**
     * @param roleDAO the roleDAO to set
     */
    public void setRoleDAO(RoleDAO roleDAO) {
        this.roleDAO = roleDAO;
    }
    
    /**
     * @param facilityTypeDAO the facilityTypeDAO to set
     */
    public void setFacilityTypeDAO(FacilityTypeDAO facilityTypeDAO) {
        this.facilityTypeDAO = facilityTypeDAO;
    }

    public void setCommunityDAO(CommunityDAO communityDAO) {
        this.communityDAO = communityDAO;
    }
    
    public void setCommunityOrganizationDAO(CommunityOrganizationDAO communityOrganizationDAO) {
        this.communityOrganizationDAO = communityOrganizationDAO;
    }

    public void setOrganizationDAO(OrganizationDAO organizationDAO) {
        this.organizationDAO = organizationDAO;
    }

    public void setUserCommunityOrganizationDAO(UserCommunityOrganizationDAO userCommunityOrganizationDAO) {
        this.userCommunityOrganizationDAO = userCommunityOrganizationDAO;
    }

    public void setSSOUserDAO(SSOUserDAO ssoUserDAO) {
        this.sSOUserDAO = ssoUserDAO;
    }
        
    public void setCommunityOrganizationService(CommunityOrganizationService communityOrganizationService) {
        this.communityOrganizationService = communityOrganizationService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public ApplicationDAO getApplicationDAO() {
        return applicationDAO;
    }

    public void setApplicationDAO(ApplicationDAO applicationDAO) {
        this.applicationDAO = applicationDAO;
    }

    
    @Override
    protected void checkPermutationStrongName() throws SecurityException {
        // noop override. workaround for SecurityException: Blocked request without GWT permutation header (XSRF attack?)
    }

    /**
     * Get a list of user app names
     * 
     */
    protected List<String> getUserApplicationNames(long userId) {

        // get communityID     
        long communityId = getLoginResult().getCommunityId();
        logger.info("BasePortalServiceServlet communityId:" + communityId);
        List<String> appKeys = applicationDAO.getUserApplicationNames(userId, communityId);
        return appKeys;
    }
    
// over loaded this method for user loader web service to get rid of login result
    protected List<String> getUserApplicationNames(long userId,long communityId) {
        logger.info("BasePortalServiceServlet communityId:" + communityId);
        List<String> appKeys = applicationDAO.getUserApplicationNames(userId, communityId);
        return appKeys;
    }
    // Courtesy of Chad Darby

    protected LoginResult getLoginResult() {
        LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());

        return loginResult;
    }
    
    protected User getUserFromSession() {
        HttpServletRequest request = getThreadLocalRequest();
        HttpSession session = request.getSession();

        User theUser = (User) session.getAttribute(WebConstants.USER_KEY);
        return theUser;

    }
    
    protected PatientManager getPatientManager(Long communityId){
        return communityContexts.get(communityId).getPatientManager();
    }
    
    protected PatientEnrollmentDAO getPatientEnrollmentDAO(Long communityId){
        return communityContexts.get(communityId).getPatientEnrollmentDAO();
    }
    
    protected HispClient getHispClient(Long communityId){
        return communityContexts.get(communityId).getHispClient();
    }
    
    protected Long getCommunityOrgId(Long communityId) {
        return communityContexts.get(communityId).getCommunityOrgId();
    }
  

}
