package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.service.AdminService;
import com.gsihealth.dashboard.common.*;
import com.gsihealth.dashboard.common.dto.UserFormReferenceData;
import com.gsihealth.dashboard.entity.dto.CommunityDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.authy.client.TwoFactorAuthClient;
import com.gsihealth.dashboard.server.dao.*;
import com.gsihealth.dashboard.server.hisp.HispClient;
import com.gsihealth.dashboard.server.hisp.HispClientUtils;
import com.gsihealth.dashboard.server.service.integration.UserService;
import com.gsihealth.dashboard.server.tasks.LoadAvailableUsersInCacheTimerTask;
import com.gsihealth.dashboard.server.tasks.Task;
import com.gsihealth.dashboard.server.util.*;
import com.gsihealth.entity.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;
import org.dozer.MappingException;
import org.gsihealth.wsn.GSIHealthNotificationMessage;
import org.gsihealth.wsn.NotificationMetadata;

import javax.persistence.NoResultException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @author Satyendra Singh
 */
public class AdminServiceImpl extends BasePortalServiceServlet implements AdminService {

    private static final int SUPER_USER = 1;
    private static final int LOCAL_ADMIN = 2;
    private static final int POWER_USER = 4;
    private final int MAX_DAYS_VALID_FOR_PASSWORD = 90;
    private Logger logger = Logger.getLogger(getClass().getName());

    private AuditDAO auditDAO;
    private MyPatientListDAO myPatientListDAO;
    private NotificationAuditDAO notificationAuditDAO;
    private OrganizationPatientConsentDAO organizationPatientConsentDAO;
    private ServletContext application;
    private SubcriptionManagerAlertServiceDAO subcriptionManagerAlertServiceDAO;
    private UserPatientConsentDAO userPatientConsentDAO;

    protected ExecutorService executor;
    protected TwoFactorAuthClient gsiclient;


    /**
     * Initializes the service with DAOs
     */
    @Override
    public void init() throws ServletException {
        super.init();
        application = getServletContext();

        // Retrieve the DAOs. they were created in the DataContextListener class
        // and stored in the servlet context
        setUserDAO((UserDAO) application.getAttribute(WebConstants.USER_DAO_KEY));
        setAccessLevelDAO((AccessLevelDAO) application.getAttribute(WebConstants.ACCESS_LEVEL_DAO_KEY));
        setRoleDAO((RoleDAO) application.getAttribute(WebConstants.ROLE_DAO_KEY));
        setFacilityTypeDAO((FacilityTypeDAO) application.getAttribute(WebConstants.FACILITY_TYPE_DAO_KEY));
        setCommunityOrganizationDAO((CommunityOrganizationDAO) application.getAttribute(WebConstants.COMMUNITY_ORGANIZATION_DAO_KEY));
        setUserCommunityOrganizationDAO((UserCommunityOrganizationDAO) application.getAttribute(WebConstants.USER_COMMUNITY_ORGANIZATION_DAO_KEY));
        setCommunityDAO((CommunityDAO) application.getAttribute(WebConstants.COMMUNITY_DAO_KEY));
        setOrganizationDAO((OrganizationDAO) application.getAttribute(WebConstants.ORGANIZATION_DAO_KEY));
        setSSOUserDAO((SSOUserDAO) application.getAttribute(WebConstants.SSO_USER_DAO_KEY));

        setUserService((UserService) application.getAttribute(WebConstants.USER_SERVICE_KEY));
        auditDAO = (AuditDAO) application.getAttribute(WebConstants.AUDIT_DAO_KEY);

        setApplicationDAO((ApplicationDAO) application.getAttribute(WebConstants.APPLICATION_DAO_KEY));

        notificationAuditDAO = (NotificationAuditDAO) application.getAttribute(WebConstants.NOTIFICATION_AUDIT_DAO_KEY);

        userPatientConsentDAO = (UserPatientConsentDAO) application.getAttribute(WebConstants.USER_PATIENT_CONSENT_DAO_KEY);
        organizationPatientConsentDAO = (OrganizationPatientConsentDAO) application.getAttribute(WebConstants.ORGANIZATION_PATIENT_CONSENT_DAO_KEY);

        subcriptionManagerAlertServiceDAO = (SubcriptionManagerAlertServiceDAO) application.getAttribute(WebConstants.SUBC_MANAGER_ALERT_KEY);

        myPatientListDAO = (MyPatientListDAO) application.getAttribute(WebConstants.MY_PATIENT_LIST_DAO_KEY);

        executor = (ExecutorService) application.getAttribute(WebConstants.THREAD_EXECUTOR_KEY);
        application.setAttribute(WebConstants.ADMIN_SERVICE_KEY, this);

    }

    @Override
    public void addOrUpdateUser(UserDTO userDTO, List<String> userApps, boolean checkForExistingEmailAddress,
                                boolean isExistingUser, boolean enableClientMfa, String authyApiKey, long communityId, boolean isThroughUserLoader) throws PortalException {
        if (isExistingUser) {
            this.updateUser(userDTO, userApps, checkForExistingEmailAddress, enableClientMfa,
                    authyApiKey, communityId, isThroughUserLoader);
        } else {
            this.addUser(userDTO, userApps, enableClientMfa, authyApiKey, communityId, isThroughUserLoader);
        }
    }

    @Override
    public void addUser(UserDTO userDTO, List<String> selectedApps, boolean enableClientMfa,
                        String authyApiKey, long communityId, boolean isThroughUserLoader) throws PortalException {

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        logger.info("addUser user: " + userDTO.getEmail() + ":communityId:" + communityId);

        User user = new User();

        try {
            // 1. Prepare User
//            User user = new User();

            convertUserDTOtoUser(userDTO, user, communityId);

            user.setAccountLockedOut(userDTO.getAccountLockedOut());
            user.setFailedAccessAttempts(userDTO.getFailedAccessAttempts());
            user.setMustChangePassword('Y');
            user.setDeleted('N');
            user.setReportingRole("user");
            user.setUserInterfaceType("0");
            /*
             * setting can_manage_power_user to 'N'
             */
            user.setCanManagePowerUser(WebConstants.NO);

            //direct address to user
            ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
            Community community = communityDAO.getCommunityById(communityId);
            OrganizationCwd organization = organizationDAO.findOrganization(new Long(userDTO.getOrganization()), communityId);

            CommunityOrganization communityOrganization = communityOrganizationDAO.findCommunityOrganization(community, organization);

            String userPassword = userDTO.getPassword();
            String directAddress = null;

            UserCommunityOrganization uco = this.populateUCOData(userDTO, user, community, organization,
                    new UserCommunityOrganization(), null);


            if (isMessagingAppAssigned(selectedApps)) {
                logger.info("creating a new direct email");
                directAddress = this.generateDirectAddress(communityId, user);
                uco.setDirectAddress(directAddress);
                String hispAddUsersEnabledStr = configurationDAO.getProperty(communityId, "hisp.add.users.enabled", "true"); // default to true if property is missing
                boolean hispAddUsersEnabled = Boolean.parseBoolean(hispAddUsersEnabledStr);

                if (hispAddUsersEnabled) {
                    String jamesServerVersion = configurationDAO.getProperty(communityId, "hisp.james.server.version", "james2"); // default to james2 if property is missing

                    if (StringUtils.equalsIgnoreCase(jamesServerVersion, HispClient.JAMES2_VERSION)) {
                        addUserToHisp(user, communityId, directAddress);
                    } else if (StringUtils.equalsIgnoreCase(jamesServerVersion, HispClient.JAMES3_VERSION)) {
                        addUserToHispJames3(user, communityId, directAddress);
                    } else {
                        throw new PortalException("Unknown james hisp version.");
                    }
                }
            }

            user.addUserCommunityOrganization(uco);

            if (!enableClientMfa) {
                user.setDisableUserMfa("true");
            }

            String tokenId = getToken(communityId);
            userService.createUser(sSOUserDAO, userCommunityOrganizationDAO, userDAO, organization, user, userPassword,
                    communityOrganization, tokenId, uco.getStatus());
            //make sure the admin token created earlier is logged out
            sSOUserDAO.logout(communityId, tokenId);

            // Assign Apps for new user (calls out to DAOs)
            logger.info("Assign Apps for new user");
            Long orgId = organization.getOrgId();
            logger.info("organization.getOrgId::" + orgId + "::communityId:" + communityId);
            List<OrganizationCommunityApplication> organizationCommunityApplications = applicationDAO.getOrganizationApplications(orgId, communityId);

            if (!organizationCommunityApplications.isEmpty()) {
                logger.info("organizationCommunityApplications ::" + organizationCommunityApplications.size());
                for (OrganizationCommunityApplication oca : organizationCommunityApplications) {
                    logger.info("OrganizationCommunityApplication Access LEVEL: ::" + oca.getOrganizationCommunityApplicationPK().getAccessLevelId() + ":RoleId ID :" + oca.getOrganizationCommunityApplicationPK().getRoleId());
                }
            }
            applicationDAO.assignAppsToUser(user, communityId, organizationCommunityApplications);

            // update user apps: for Messaging App Only
            logger.info("add legacy apps communityId :" + communityId);
            addApps_LegacyMessaging(userDTO.getEmail(), selectedApps, communityId);

            // handle org/user patient consent
            // addConsentForNewUser(userDTO);
            Runnable addConsentForNewUserTask = new AddConsentForNewUserTask(userDTO, communityId);
            executor.execute(addConsentForNewUserTask);

            // send alert for user created
            Task sendUserCreatedAlertTask = new SendUserCreatedAlertTask(communityId, userDTO, organization);
            sendUserCreatedAlertTask.run();

            // check for deactivated user
            if (userDTO.isUserDeactivated()) {
                Task sendUserDeactivatedAlertTask = new SendUserDeactivatedAlertTask(communityId, userDTO, organization);
                sendUserDeactivatedAlertTask.run();
            }

            String comment = "Successfully added user: " + userDTO.getEmail();

            logger.info("Audit entry to database");
            audit(user, comment, AuditType.ADDED_USER, communityId, isThroughUserLoader);
            saveSupervisorAudit(user, null, communityId, getLoginResult());

            Runnable createUserAlertFiltersTask = new CreateUserAlertFiltersTask(userDTO, communityId);
            executor.execute(createUserAlertFiltersTask);

            logger.info(comment);

        } catch (Exception ex) {
            String message = "Error adding user: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error adding user.");
        }

        stopWatch.stop();

        logger.info("Time to add a new user: " + stopWatch.getTime() / 1000.0 + " secs");
        // Register user to authy if MFA required
        registerUser(user, enableClientMfa, authyApiKey, userDTO.getCountryCode(), "add", communityId);
    }

    protected String generateDirectAddress(long communityId, User user) {
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        String directDomain = configurationDAO.getProperty(communityId, "hisp.james3.direct.domain");
        String james3DirectAddress = HispClientUtils.generateDirectAddress(user.getEmail(), directDomain);
        logger.info("got directAddress for user: " + user.getEmail() + " of " + james3DirectAddress);
        return james3DirectAddress;
    }

    @Override
    public void requestSms(String email, long communityId, String authyApikey, String countryCode) throws PortalException {
        logger.info("get user authyId from the database");
        try {
            int authyId = userDAO.getAuthyId(email, communityId);//get from database via email
            gsiclient = new TwoFactorAuthClient(authyApikey, countryCode);
            logger.info("AuthyId>>>>>>> " + authyId);
            com.authy.api.Hash sms = gsiclient.sendSMSRequest(authyId);
            if (sms.isOk()) {
                logger.info("REQUEST OK");

            } else if (sms.getError() != null) {
                System.out.println(sms.getError());
                throw new PortalException();

            }
        } catch (Exception ex) {
            String message = "Error getting authyId from DB " + ex.getMessage();
            ex.printStackTrace();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error getting authyId from DB or SMS not sent");
        }
    }

    @Override
    public void verifyToken(String token, String email, long communityId, String authyApiKey, String countryCode) throws PortalException {

        logger.info("get user authyId from the database");
        try {
            int authyId = userDAO.getAuthyId(email, communityId);//get from database via email
            User user = userDAO.findUserByEmail(email, communityId);
            gsiclient = new TwoFactorAuthClient(authyApiKey, countryCode);
            logger.info("AuthyId>>>>>>> " + authyId);
            com.authy.api.Token verifyToken = gsiclient.verifyUser(authyId, token);
            if (verifyToken.isOk()) {
                logger.info("USER OK");
                user.setLastMfaVerificationDate(new Date());
                userDAO.update(user);
            } else {
                System.out.println(verifyToken.getError());
                throw new PortalException();
            }
        } catch (Exception ex) {
            String message = "Error verifying the token " + ex.getMessage();
            ex.printStackTrace();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Verification code not recognized. Please re-enter the information or request a new code");
        }
    }

    private List<UserAlertFilter> getUserAlertFilterList(List<ApplicationSendsAlert> appSendsAlert, Long communityId, Long userId) {

        List<UserAlertFilter> userAlerts = new ArrayList<UserAlertFilter>();
        UserAlertFilter alertFilter = null;

        for (ApplicationSendsAlert alert : appSendsAlert) {
            alertFilter = new UserAlertFilter();
            alertFilter.setAppAlertID(alert.getApplicationAlertId());
            BigInteger communityIDInt = BigInteger.valueOf(communityId);
            alertFilter.setCommunityId(communityIDInt);
            alertFilter.setCreationDateTime(new Date());
            alertFilter.setShowAlert(true);
            alertFilter.setUserId(userId);
            userAlerts.add(alertFilter);
        }
        return userAlerts;

    }

    protected void addUserPatientConsent(List<OrganizationPatientConsent> orgPatientConsentList, long userId, long communityId) throws Exception {

        Date now = new Date();
        logger.info("addUserPatientConsent, loop thru orgPatientConsentList");
        // add user consent for each of those patients
        for (OrganizationPatientConsent temp : orgPatientConsentList) {
            long patientId = temp.getOrganizationPatientConsentPK().getPatientId();

            UserPatientConsent userPatientConsent = new UserPatientConsent(patientId, userId, communityId);

//            UserPatientConsent userPatientConsent = new UserPatientConsent(patientId, userId, Constants.GSI_COMMUNITY_ID);
            userPatientConsent.setConsentStatus(temp.getConsentStatus());

            userPatientConsentDAO.add(userPatientConsent, now);
        }
        logger.info("leaving addUserPatientConsent method");
    }

    /**
     * If user's status has changed AND is now inactive, send out deactivated
     * alerts
     *
     * @param userDTO
     * @param oldStatus
     * @param organization
     */
    protected void sendAlertForDeactivatedUser(long communityId, UserDTO userDTO, String oldStatus, OrganizationCwd organization, String token) throws Exception {
        // check for deactivated user
        String userStatus = userDTO.getStatus();
        boolean statusHasChanged = !StringUtils.equalsIgnoreCase(userStatus, oldStatus);
        boolean statusIsInactive = StringUtils.equalsIgnoreCase(userStatus, WebConstants.INACTIVE);

        if (statusHasChanged && statusIsInactive) {
            Task sendUserDeactivatedAlertTask = new SendUserDeactivatedAlertTask(communityId, userDTO, organization);
            sendUserDeactivatedAlertTask.run();
        }
        if (statusHasChanged) {
            //UM Call
            sSOUserDAO.setUserActive(communityId, token, userDTO.getEmail(), !statusIsInactive);
        }
    }

    /**
     * @param communityId
     * @return
     * @deprecated @param accessLevel
     */
    protected List<User> getLocalAdminUsers(AccessLevel accessLevel, long communityId) {

        List<User> users = new ArrayList<User>();
        User loggedinUser = getUserFromSession();

        Long orgId = loggedinUser.getUserCommunityOrganization(new Community(communityId)).getOrganizationCwd().getOrgId();
        logger.info("did we get an orgId: " + orgId);
        OrganizationCwd organization = organizationDAO.findOrganization(orgId, communityId);
        Community community = getUserCommunity(communityId);

        //FIXME REMOVE CO FROM THIS
        CommunityOrganization communityOrganization = communityOrganizationDAO.findCommunityOrganization(
                community, organization);

        List<UserCommunityOrganization> userCommunityOrganizationList = userCommunityOrganizationDAO.findUserCommunityOrganizationEntitiesByCommunityOrganization(
                communityOrganization);

        for (UserCommunityOrganization userCommunityOrganization : userCommunityOrganizationList) {
            List<User> foundUsers = userDAO.findUserByUserCommunityOrganizationAndAccessLevel(userCommunityOrganization, accessLevel);
            users.addAll(foundUsers);
        }

        return users;
    }

    /**
     * Returns true if message app is assigned
     *
     * @param apps
     * @return
     */
    protected boolean isMessagingAppAssigned(List<String> apps) {
        boolean result = apps.contains(AppKeyConstants.MESSAGING_APP_KEY);

        return result;
    }

    protected void addApps(List<String> selectedApps, long userId, long communityId) {
        // for each app, get the app id
        logger.info("laddApps *** userId:" + userId + ":communityId:" + communityId);
        for (String tempAppName : selectedApps) {
            Application theApp = applicationDAO.findApplication(tempAppName, communityId);

            long appId = theApp.getApplicationPK().getApplicationId();

            UserCommunityApplication userCommApp = new UserCommunityApplication();
            UserCommunityApplicationPK userCommunityApplicationPK = new UserCommunityApplicationPK();

            userCommunityApplicationPK.setApplicationId(appId);
            userCommunityApplicationPK.setCommunityId(communityId);
            userCommunityApplicationPK.setUserId(userId);
            userCommApp.setUserCommunityApplicationPK(userCommunityApplicationPK);
            userCommApp.setStartDate(new Date());

            // execute
            applicationDAO.addUserCommunityApplication(userCommApp);
        }
    }

    private void addApps_LegacyMessaging(String userEmail, List<String> selectedApps, long communityId) {

        logger.info("addApps_LegacyMessaging .communityId:" + communityId);
        if (selectedApps == null || selectedApps.isEmpty()) {
            logger.info("No apps selected.");
            return;
        }

        // get user id make sure user is in same community
        User theUser = userDAO.findUserByEmail(userEmail, communityId);
        long userId = theUser.getUserId();

        addApps(selectedApps, userId, communityId);
    }

    private void audit(User user, String comment, String auditType, long communityId, boolean isThroughUserLoader) {
        // audit

        System.out.println("BasePatientService Audit CommunityId *###" + communityId);
        try {
            LoginResult loginResult = null;
            if (isThroughUserLoader) {
                loginResult = buildLoginResultForUserLoader();
            } else {

                loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
            }
            audit(user, comment, auditType, communityId, loginResult);
        } catch (Exception ex) {
            String message = "Error AUDITING user: " + user.getEmail() + ",   " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
        }

    }

    private void audit(User user, String comment, String auditType, long communityId, LoginResult loginResult) {
        try {
            Audit audit = AuditUtils.convertUser(loginResult, user);
            audit.setActionComments(comment);
            audit.setActionType(auditType);
            audit.setActionSuccess(com.gsihealth.dashboard.common.Constants.YES);

            audit.setCommunityId(communityId);
            auditDAO.addAudit(audit);
        } catch (Exception ex) {
            String message = "Error AUDITING user: " + user.getEmail() + ",   " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
        }
    }

    private void saveSupervisorAudit(User user, Long oldId, long communityId, LoginResult loginResult) {
        UserCommunityOrganization uco = user.getUserCommunityOrganization(communityId);
        if (uco == null) {
            return;
        }
        User spvr = uco.getSupervisor();
        if ((spvr == null && oldId == null) || (spvr != null && Objects.equals(spvr.getUserId(), oldId))) {
            //no change no audit
            return;
        }

        // determine supervisor, audit type and action
        String auditType;
        String action;
        User supervisor;
        if (oldId == null) {
            auditType = AuditType.ADDED_SUPERVISOR;
            action = "added";
            supervisor = userDAO.findUser(spvr.getUserId());
        } else {
            if (spvr == null) {
                auditType = AuditType.REMOVED_SUPERVISOR;
                action = "removed";
                supervisor = userDAO.findUser(oldId);
            } else {
                auditType = AuditType.UPDATED_SUPERVISOR;
                action = "updated";
                supervisor = userDAO.findUser(spvr.getUserId());
            }
        }
        // build comment
        String supervisorFullName = supervisor.getFirstName() + " " + supervisor.getLastName();
        String comment = "Successfully " + action + " supervisor " + supervisorFullName
                + " (id = " + supervisor.getUserId() + ")"
                + " for user: " + user.getEmail();

        // create audit
        audit(user, comment, auditType, communityId, loginResult);
    }

    /**
     * FIXME - clean this up, there's common code with add
     *
     * @param userDTO
     * @param userApps
     * @param checkForExistingEmailAddress
     * @param communityId
     * @param isThroughUserLoader
     * @throws PortalException
     */
    @Override
    public String updateUser(UserDTO userDTO, List<String> userApps, boolean checkForExistingEmailAddress,
                             boolean enableClientMfa, String authyApiKey, long communityId, boolean isThroughUserLoader)
            throws PortalException {
        String changePwdToken = null;
        String oldStatus = null;
        Long oldAccessLevelId = null;
        Long oldRoleId = null;
        Long oldOrgId = null;
        Long oldSupervisorId = null;
        String oldPhone = null;

        String newStatus = userDTO.getStatus();
        String userEmail = userDTO.getEmail();
        LoginResult loginResult = null;
        if (isThroughUserLoader) {
            loginResult = buildLoginResultForUserLoader();
        } else {

            loginResult = getLoginResult();
        }
        logger.info("updateUser():  Updating user: " + userEmail
                + " current status = " + newStatus
                + ", by logged in user:" + loginResult.getEmail());

        logger.info("updateUser: newOrgId=" + userDTO.getOrgid());

        if (checkForExistingEmailAddress && isValidPortalUserId(userEmail)) {
            throw new PortalException("User ID " + userEmail + " already exists.");
        } else {
            try { //FIXME
                User user = getUserFromPersistenceContext(userDTO);
                if (user.getUserCommunityOrganization(communityId) != null
                        && user.getUserCommunityOrganization(communityId).getSupervisor() != null) {
                    oldSupervisorId = user.getUserCommunityOrganization(communityId).getSupervisor().getUserId();
                }
//                    oldPhone=user.getTelephoneNumber();
                Community community = getUserCommunity(communityId);
                UserCommunityOrganization uco = user.getUserCommunityOrganization(community);
                boolean userExistsInAnotherCommunity = false;
                long otherCommunityId = -1L;
                if (uco != null) { // existing user in current community
                    oldStatus = uco.getStatus();
                    oldAccessLevelId = uco.getAccessLevel().getAccessLevelPK().getAccessLevelId();
                    oldRoleId = uco.getRole().getRoleId();
                    oldOrgId = uco.getOrganizationCwd().getOrgId();
                } else { // existing user in new community FIXME this will need hisp stuff added
                    logger.fine("user " + user.getUserId() + " exists in another community, adding");
                    //get the other communityId before new uco is created
                    List<UserCommunityOrganization> ucoList = userCommunityOrganizationDAO.findUserCommunityOrganizationEntities(user.getUserId());
                    if (!ucoList.isEmpty()) {
                        otherCommunityId = ucoList.get(0).getCommunity().getCommunityId();
                    }
                    uco = new UserCommunityOrganization();
                    uco.setCommunity(community);
                    userExistsInAnotherCommunity = true;
                }

                user.setAccountLockedOut(userDTO.getAccountLockedOut());
                user.setFailedAccessAttempts(userDTO.getFailedAccessAttempts());
                convertUserDTOtoUser(userDTO, user, communityId);
                user.setDisableUserMfa(userDTO.getDisableUserMfa());
                this.populateUCOData(userDTO, user, community, null, uco, uco.getDirectAddress());

                // if user's password has been set, then they must change it on next login
                logger.fine(userEmail + " AdminserviceImpl: update user: must change Password? " + userDTO.getMustChangePassword());
                if (userDTO.isPasswordSet()) {
                    user.setMustChangePassword(WebConstants.YES);
                    resetUserPasswordDate(userDTO, user);
                } else {
                    user.setMustChangePassword(WebConstants.NO);
                }

                if (user.getEnabled() == null) {
                    user.setEnabled(WebConstants.YES);
                }

                // reset login date
                if (getDaysSinceLastChange(userDTO.getLastSuccessfulLoginDate(), user.getLastSuccessfulLoginDate()) >= 90) {
                    user.setLastSuccessfulLoginDate(userDTO.getLastSuccessfulLoginDate());
                    logger.fine(userEmail + " Days since last login:" + getDaysSinceLastChange(userDTO.getLastSuccessfulLoginDate(), new Date()));
                }

                this.resetFlagsForActiveStatus(userDTO, oldStatus, user);

                this.updateNullCanManagePowerUser(user);


                //1.2 Organization
                OrganizationCwd organization = organizationDAO.findOrganization(new Long(userDTO.getOrgid()), communityId);
                Long newOrgId = organization.getOrgId();

                logger.fine(userEmail + " old org: " + oldOrgId + " new org: " + newOrgId);
                logger.info("adminservicesImpl :updateUser:CommunityId:" + community.getCommunityId() + ":organization.getOrgId:" + organization.getOrgId());
                ;

                //Prepare
                //1.3 CommunityOrganization
                CommunityOrganization communityOrganization = new CommunityOrganization(community, organization);
                uco.setOrganizationCwd(organization);

                // save the user
                user.addUserCommunityOrganization(uco);
                Map<String, Object> map = updateUserPasswordDemogMessagingApp(userDTO, userEmail, user, communityOrganization, organization, userApps, userExistsInAnotherCommunity, otherCommunityId);
                changePwdToken = (String) map.get("changePwdToken");
                long userId = (Long) map.get("userId");

                if (userRoleOrAccessHasChanged(userDTO, oldRoleId, oldAccessLevelId)) {
                    updateAppsForRoleOrAccessChange(organization, communityOrganization, user);
                }
                sendAlertForDeactivatedUser(communityId, userDTO, oldStatus, organization, loginResult.getToken());

                // update patient consent
                logger.info("AdminServiceImpl.updateUser: oldOrgId=" + oldOrgId + ", newOrgId=" + newOrgId);

                if (!Objects.equals(oldOrgId, newOrgId)) {
                    logger.fine(userEmail + " old org different from new org, updating patient consent");
                    updatePatientConsentTask(userId, oldOrgId, newOrgId, communityId);
                }

                // if this user exists in another community, do the add stuff for this community
                if (userExistsInAnotherCommunity) {
                    createUserAlerts(userEmail, communityId, userDTO, organization);
                }

                // break supervisor relationship if user changes organization or user becomes inactive
                if ((!Objects.equals(oldStatus, userDTO.getStatus())
                        && WebConstants.INACTIVE.equalsIgnoreCase(userDTO.getStatus())) ||
                        !Objects.equals(oldOrgId, newOrgId)) {
                    updateSubordinate(communityId, user.getUserId(), loginResult);
                }

                saveUserAudit("Successfully updated user: ", user, AuditType.UPDATED_USER, communityId, isThroughUserLoader);
                saveSupervisorAudit(user, oldSupervisorId, communityId, loginResult);

            } catch (Exception exc) {
                final String message = "Error updating user: " + userDTO.getFirstName() + " " + userDTO.getLastName();
                logger.logp(Level.SEVERE, getClass().getName(), "updateUser", message, exc);
                throw new PortalException("Error updating " + userEmail);
            }
            // Register user to authy if MFA required
            User user = getUserFromPersistenceContext(userDTO);
            registerUser(user, enableClientMfa, authyApiKey, userDTO.getCountryCode(), "update", communityId);
        }
        return changePwdToken;
    }

    protected void createUserAlerts(String userEmail, long communityId, UserDTO userDTO, OrganizationCwd organization) {
        logger.info(userEmail + " exists in another community, running tasks to add them to community "
                + communityId);
        Runnable createUserAlertFiltersTask = new CreateUserAlertFiltersTask(userDTO, communityId);
        executor.execute(createUserAlertFiltersTask);
        Task sendUserCreatedAlertTask = new SendUserCreatedAlertTask(communityId, userDTO, organization);
        sendUserCreatedAlertTask.run();
    }

    public void registerUser(User user, boolean enableClientMfa, String authyApiKey, String countryCode, String mode, long communityId) throws PortalException {

        gsiclient = new TwoFactorAuthClient(authyApiKey, countryCode);
        if (mode.equals("update")) {
            boolean disableUserMfa = Boolean.parseBoolean(user.getDisableUserMfa());
            // Check for Multi-Factor Authentication if required  
            int authId = (user.getAuthyId() == null) ? 0 : user.getAuthyId().intValue();
            if (enableClientMfa && !disableUserMfa && (authId == 0)) {
                user.setDisableUserMfa("false");
                register(user);

            }
        } else if (enableClientMfa && mode.equals("add")) {
            user.setDisableUserMfa("false");
            register(user);
        }
    }

    public void register(User user) throws PortalException {
        try {
            String mobileNo = getUserMobileNumber(user);
            com.authy.api.User authUuser = gsiclient.registerUser(user.getEmail(), mobileNo);
            if (authUuser.isOk()) {
                BigInteger authyId = BigInteger.valueOf(authUuser.getId());
                logger.info("AuthyId >>>>>>" + authyId);
                //set authy_id that is to be stored to the DB
                user.setAuthyId(authyId);
                userDAO.update(user);
                logger.info("User Register Okay");

            } else if (authUuser.getError() != null) {
                System.out.println(authUuser.getError());
                throw new PortalException();
            }
        } catch (Exception ex) {
            final String message = "Add/Update user successful,but phone registration for 2FA fails " + user.getFirstName() + " " + user.getLastName();
            logger.logp(Level.SEVERE, getClass().getName(), "registerUser", message, ex);
            throw new PortalException("Add/Update user successful,but phone registration for 2FA fails" + user.getEmail());
        }
    }

    private String getUserMobileNumber(User user) {

        String mobileNo = null;
        if (user.getMobileNumber() != null && user.getMobileNumber().length() != 10) {
            String number = user.getMobileNumber();
            mobileNo = number.substring(number.length() - 10, number.length());

        } else {
            String number = user.getMobileNumber();
            mobileNo = number;
        }
        return mobileNo;
    }

    /**
     * @return
     */
    private Community getUserCommunity(long communityId) {
        Community community = communityDAO.getCommunityById(communityId);
        return community;
    }

    /**
     * update user demographic info, password and message app stuff in user
     * preferences window
     *
     * @param userDTO
     * @param userApps
     * @throws PortalException
     */
    @Override
    public String updateUserPreferences(UserDTO userDTO, List<String> userApps, long communityId) throws PortalException {
        String changePwdToken = null;
        String userEmail = userDTO.getEmail();
        User user = getUserFromPersistenceContext(userDTO);
        Long oldSupervisorId = null;
        if (user.getUserCommunityOrganization(communityId).getSupervisor() != null) {
            oldSupervisorId = user.getUserCommunityOrganization(communityId).getSupervisor().getUserId();
        }

        Community community = getUserCommunity(communityId);

        try {
            convertUserDTOtoUser(userDTO, user, communityId);
            OrganizationCwd organization = organizationDAO.findOrganization(new Long(userDTO.getOrganization()), communityId);
            CommunityOrganization communityOrganization = communityOrganizationDAO.findCommunityOrganization(community, organization);
            UserCommunityOrganization uco = user.getUserCommunityOrganization(communityId);
            this.populateUCOData(userDTO, user, community, organization, uco, uco.getDirectAddress());
            user.addUserCommunityOrganization(uco);
            //otherCommunityId will be used only if flag is true
            Map<String, Object> map = this.updateUserPasswordDemogMessagingApp(userDTO, userEmail, user, communityOrganization, organization, userApps, false, -1);
            changePwdToken = (String) map.get("changePwdToken");
            saveSupervisorAudit(user, oldSupervisorId, communityId, getLoginResult());
        } catch (Exception exc) {
            final String message = "Error updating user: " + userDTO.getFirstName() + " " + userDTO.getLastName();
            logger.logp(Level.SEVERE, getClass().getName(), "updateUser", message, exc);
            throw new PortalException("Error updating " + userEmail);
        }
        // Register user to authy if MFA required
//        registerUser(user, enableClientMfa, authyApiKey, countryCode, "update", communityId);
        return changePwdToken;
    }

    /**
     * update user demographic, password and message app including direct_address email
     * TODO returning userId, not strictly necessary
     *
     * @param userDTO
     * @param userEmail
     * @param user
     * @param communityOrganization
     * @param organization
     * @param userApps
     * @return
     * @throws Exception
     */
    private Map<String, Object> updateUserPasswordDemogMessagingApp(UserDTO userDTO, String userEmail, User user,
                                                                    CommunityOrganization communityOrganization, OrganizationCwd organization, List<String> userApps, boolean userExistsInAnotherCommunity, long otherCommunityId) throws Exception {
        Map<String, Object> map = new HashMap<>();
        //CheckPassword
        String userPassword = "";
        if (userDTO.isPasswordSet()) {
            userPassword = userDTO.getPassword();
            user.setLastPasswordChangeDate(new Date());
            logger.log(Level.FINE, "{0} password will be reset", userEmail);
        }

        long communityId = communityOrganization.getCommunity().getCommunityId();

        String directAddress = null;

        String tokenId = getToken(communityId);
        if (isMessagingAppAssigned(userApps)) { // FIXME anyway to avoid doing this twice?
            // have to do this or save the user twice
            directAddress = this.generateDirectAddress(communityId, user);
            user.getUserCommunityOrganization(communityId).setDirectAddress(directAddress);
        }

        String changePwdToken = userService.updateUser(sSOUserDAO, userCommunityOrganizationDAO, userDAO, user, userPassword,
                communityOrganization, tokenId, organization, userExistsInAnotherCommunity, otherCommunityId);
        //make sure the admin token created earlier is logged out
        sSOUserDAO.logout(communityId, tokenId);
        long userId = user.getUserId();
        map.put("changePwdToken", changePwdToken);
        map.put("userId", userId);

        logger.info("updateUserPasswordDemogMessagingApp:communityId:" + communityId);

        if (!applicationDAO.hasMessagingApp(userId, communityId) && isMessagingAppAssigned(userApps)) {
            logger.log(Level.INFO, "configuring {0} for hisp", userId);
            if (directAddress == null) {
                directAddress = this.generateDirectAddress(communityId, user);
            }

            this.configureJamesUser(user, communityId, directAddress);
            logger.log(Level.INFO, "finished adding {0} to hisp", userId);
        }
        updateUserMessagingApp(userDTO, userApps, communityId);
        return map;
    }

    private void saveUserAudit(String auditMessage, User user, String auditType, long communityId, boolean isThroughUserLoader) {
        String comment = auditMessage + user.getEmail();
        audit(user, comment, auditType, communityId, isThroughUserLoader);
        logger.info(comment);
    }

    private void resetUserPasswordDate(UserDTO userDTO, User user) {
        Date setDate = new Date();
        if (getDaysSinceLastChange(userDTO.getLastPasswordChangeDate(), setDate) >= 90) {
            user.setLastPasswordChangeDate(setDate);
            logger.info("Days since last password change:" + getDaysSinceLastChange(userDTO.getLastPasswordChangeDate(), setDate));
        }
        user.setMustChangePassword(WebConstants.YES);
    }

    /**
     * replace old apps (except for messaging) with new apps based on users
     * current role and access level
     *
     * @param organization
     * @param communityOrganization
     * @param user
     * @throws Exception
     */
    private void updateAppsForRoleOrAccessChange(OrganizationCwd organization, CommunityOrganization communityOrganization, User user) throws Exception {
        Long orgId = organization.getOrgId();
        Long communityId = communityOrganization.getCommunity().getCommunityId();

        // remove all old apps except messaging
        applicationDAO.removeOldAppsExceptForMessaging(user.getUserId(), communityId);

        // add new apps based on role and access level
        List<OrganizationCommunityApplication> organizationCommunityApplications = applicationDAO.getOrganizationApplications(orgId, communityId);
        applicationDAO.assignAppsToUser(user, communityId, organizationCommunityApplications);
    }

    private void updatePatientConsentTask(long userId, Long oldOrgId, Long newOrgId, Long communityId) {
        Runnable updatePatientConsentTask = new UpdatePatientConsentTask(userId, oldOrgId, newOrgId, communityId);
        executor.execute(updatePatientConsentTask);
    }

    private User getUserFromPersistenceContext(UserDTO userDTO) {
        User user = userDAO.findUser(userDTO.getUserId());

        return user;
    }

    /**
     * if new status is active and old status isn't, update last successful
     * login, last password change, zero out failed attempts and unlock user
     * acct
     *
     * @param userDTO
     * @param oldStatus
     * @param user
     */
    private void resetFlagsForActiveStatus(UserDTO userDTO, String oldStatus, User user) {
        // if status is set to ACTIVE, then reset flags
        // TODO: need better way to handle this

        String newStatus = userDTO.getStatus();
        if (!StringUtils.equals(oldStatus, newStatus) && StringUtils.equals(newStatus, "ACTIVE")) {
            Date now = new Date();
            user.setLastSuccessfulLoginDate(now);
            user.setLastPasswordChangeDate(now);
            user.setFailedAccessAttempts(0);
            user.setAccountLockedOut(com.gsihealth.dashboard.common.Constants.NO_CHAR);
        }
    }

    /**
     * updating can_manage_power_user column to 'No' if it's null
     */
    private void updateNullCanManagePowerUser(User user) {
        Character canManagePowerUser = user.getCanManagePowerUser();
        if (canManagePowerUser == null) {
            user.setCanManagePowerUser(WebConstants.NO);
        }
    }

    private void configureJamesUser(User user, long communityId, String directAddress) throws Exception {
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        //james user configuration
        String hispAddUsersEnabledStr = configurationDAO.getProperty(communityId, "hisp.add.users.enabled", "true"); // default to true if property is missing
        boolean hispAddUsersEnabled = Boolean.parseBoolean(hispAddUsersEnabledStr);

        if (hispAddUsersEnabled) {
            String jamesServerVersion = configurationDAO.getProperty(communityId, "hisp.james.server.version", "james2"); // default to james2 if property is missing

            if (StringUtils.equalsIgnoreCase(jamesServerVersion, HispClient.JAMES2_VERSION)) {
                addUserToHisp(user, communityId, directAddress);
            } else if (StringUtils.equalsIgnoreCase(jamesServerVersion, HispClient.JAMES3_VERSION)) {
                addUserToHispJames3(user, communityId, directAddress);
            } else {
                throw new PortalException("Unknown james hisp version.");
            }
        }
    }

    private String getToken(long communityId) throws Exception {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        String adminUserName = configurationDAO.getProperty(communityId, "sso.server.admin.username");
        String adminPassword = configurationDAO.getProperty(communityId, "sso.server.admin.password");

        //get fresh tokenId for update
        String tokenId = sSOUserDAO.authenticate(communityId, adminUserName, adminPassword);
        tokenId = StringUtils.trimToEmpty(tokenId);
        logger.info("token result from OpenAM:-" + tokenId + "-");

        return tokenId;
    }
//
//    private Map<String, String> buildSsoUserMapForUpdate(User user, UserDTO userDTO, OrganizationCwd organization) throws Exception {
//
//        Map<String, String> ssoUserMap = new LinkedHashMap<String, String>();
//
//        //Use Main info to identify  user for OpenAM
//        ssoUserMap.put("identity_name", encode(user.getEmail()));
//        ssoUserMap.put("identity_attribute_names=cn&identity_attribute_values_cn", encode(user.getFirstName()) + "%20" + encode(user.getLastName()));
//        ssoUserMap.put("identity_attribute_names=sn&identity_attribute_values_sn", encode(user.getLastName()));
//        ssoUserMap.put("identity_attribute_names=givenname&identity_attribute_values_givenname", encode(user.getFirstName()));
//        ssoUserMap.put("identity_attribute_names=mail&identity_attribute_values_mail", encode(user.getEmail()));
//
//        if (userDTO.isPasswordSet()) {
//            String userPassword = userDTO.getPassword();
//            ssoUserMap.put("identity_attribute_names=userpassword&identity_attribute_values_userpassword", encode(userPassword));
//        }
//
//        //custom Attributes
//        String middleName = getDefaultString(user.getMiddleName());
//        ssoUserMap.put("identity_attribute_names=middleInitial&identity_attribute_values_middleInitial", encode(middleName));
//
//        String telephoneNumber = getDefaultString(user.getTelephoneNumber());
//        ssoUserMap.put("&identity_attribute_names=telephoneNumber&identity_attribute_values_telephoneNumber,", encode(telephoneNumber));
//
//        String phoneExt = getDefaultString(user.getPhoneExt());
//        ssoUserMap.put("&identity_attribute_names=telephoneExtension&identity_attribute_values_telephoneExtension", encode(phoneExt));
//
//        ssoUserMap.put("&identity_attribute_names=signingCredentials&identity_attribute_values_signingCredentials", encode(user.getCredentials()));
//        ssoUserMap.put("&identity_attribute_names=customGroups&identity_attribute_values_customGroups", encode(String.valueOf(user.getUserCommunityOrganization().getAccessLevel().getAccessLevelId())));
//        ssoUserMap.put("&identity_attribute_names=lastUpdated&identity_attribute_values_lastUpdated", encode(user.getLastUpdateDate().toString()));
//
//        //OrganizationCwd OID (userDTO is the previous value, user is from the form
//        String oid = getDefaultString(organization.getOid());
//        ssoUserMap.put("&identity_attribute_names=organizationId&identity_attribute_values_organizationId", encode(oid));
//
//        return ssoUserMap;
//    }

    protected String encode(String value) throws Exception {
        if (value != null) {
            value = URLEncoder.encode(value, "UTF-8");
        } else {
            value = "";
        }
        return value;
    }

    /**
     * FIXME - use the mapper for this or move it to propertyUtils
     *
     * @param userDTO
     * @param user
     * @param communityId
     * @return
     * @throws Exception
     */
    private void convertUserDTOtoUser(UserDTO userDTO, User user, long communityId) throws Exception {
        user.setFirstName(userDTO.getFirstName());
        user.setMiddleName(userDTO.getMiddleName());
        user.setLastName(userDTO.getLastName());
        user.setDateOfBirth(userDTO.getDateOfBirth());
        user.setStreetAddress1(userDTO.getStreetAddress1());
        user.setStreetAddress2(userDTO.getStreetAddress2());
        user.setCity(userDTO.getCity());
        user.setState(userDTO.getState());
        user.setZipCode(userDTO.getZipCode());

        user.setTelephoneNumber(userDTO.getWorkPhone());
        user.setPhoneExt(userDTO.getWorkPhoneExt());
        if (userDTO.getCountryCode() != null) {
            user.setMobileNumber(userDTO.getCountryCode() + userDTO.getMobileNo());
        } else {
            user.setMobileNumber(userDTO.getMobileNo());
        }
        user.setEmail(userDTO.getEmail());
        user.setDeleted(userDTO.getIsDeleted());
        user.setFailedAccessAttempts(userDTO.getFailedAccessAttempts());
        user.setCreationDate(userDTO.getCreationDate());
        user.setEulaAccepted(userDTO.getEulaAccepted());
        user.setEulaDate(userDTO.getEulaDate());
        user.setEulaId(userDTO.getEulaId());
        user.setPrefix(userDTO.getPrefix());
        user.setNpi(userDTO.getNpi());
        user.setEnabled(userDTO.getEnabled());

    }

    private UserCommunityOrganization populateUCOData(UserDTO userDTO, User user,
                                                      Community community, OrganizationCwd organization, UserCommunityOrganization uco,
                                                      String directAddress) throws NumberFormatException {
        AccessLevel accessLevel = accessLevelDAO.findAccessLevel(new Long(userDTO.getAccessLevelId()), community.getCommunityId());
        Role role = roleDAO.findRole(new Long(userDTO.getSecurityRoleId()), community.getCommunityId());
        String status = userDTO.getStatus();
        String gender = userDTO.getGender();
        String credentials = userDTO.getCredentials();

        User supervisor = null;
        if (userDTO.getSupervisorId() != null) {
            supervisor = new User(userDTO.getSupervisorId());
        }

        uco.setSamhsaAccepted(userDTO.getSamhsaAccepted());
        PropertyUtils.populateUserUcoData(user, uco, community, organization, accessLevel, role,
                new Date(System.currentTimeMillis()), directAddress, status, gender, credentials, supervisor);
        return uco;
    }

//    /**
//     * Get a list of administrators
//     * @deprecated 
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public SearchResults<UserDTO> getUsers(long accessLevelId, int pageNum, int pageSize, long communityId) throws PortalException {
//        User loggedinUser = getUserFromSession();
//        UserCommunityOrganization uco = loggedinUser.getUserCommunityOrganization(communityId);
//        try {
//            // Step 1: Get users from DAO
//            List<User> users = new ArrayList<User>();
//
//            AccessLevel accessLevel = accessLevelDAO.findAccessLevel(accessLevelId);
//
//            int totalCount = 0;
//
//            if (accessLevelId == LOCAL_ADMIN) {
//                users = getLocalAdminUsers(accessLevel, communityId);
//                totalCount = users.size();
//
//                int startRecordNumber = Math.max(0, (pageNum - 1) * pageSize);
//                int endRecordNumber = Math.min(startRecordNumber + pageSize, totalCount);
//
//                users = users.subList(startRecordNumber, endRecordNumber);
//
//            } else if ((accessLevelId == SUPER_USER) || (accessLevelId == POWER_USER)) {
//                logger.fine("calling totalCount from adminServiceImpl.getUsers");
//                totalCount = userDAO.getTotalCount(communityId, accessLevel);
//                logger.info("adminServiceImpl.getUsers total count" + totalCount);
//                users = userDAO.findUsers(uco, , pageNum, pageSize);
//                logger.info("SUPER_USER getUsers totalCount >>>>>>>");
//            }
//
//
//            /*
//             * // make sure password is current, if not set the user to
//             * inactive Date now = new Date(); for (User user : users) { if
//             * (getDaysSinceLastChangedPassword(user, now) >
//             * MAX_DAYS_VALID_FOR_PASSWORD) { user.setStatus("INACTIVE");
//             * user.setMustChangePassword(WebConstants.YES);
//             * userDAO.update(user); } }
//             */
//            // Convert to DTOs, fully populated
//            List<UserDTO> userDTOList = PropertyUtils.convertToDTOs(users, userCommunityOrganizationDAO, communityId);
//
//            SearchResults<UserDTO> searchResults = new SearchResults(totalCount, userDTOList);
//
//            return searchResults;
//
//        } catch (Exception exc) {
//            final String message = "Error loading users.";
//            logger.logp(Level.SEVERE, getClass().getName(), "getUsers", message, exc);
//            throw new PortalException(message);
//        }
//
//    }

    /**
     * NOT USED?
     *
     * @param userDTO
     * @param token
     * @throws PortalException
     */
    @Override
    public void deleteUser(UserDTO userDTO, String token) throws PortalException {
        User user = new User();
        user.setDeleted('Y');
        long communityId = AuditUtils.getLoginResult(getThreadLocalRequest()).getCommunityId();
        try {
            convertUserDTOtoUser(userDTO, user, communityId);
            userDAO.update(user);
        } catch (Exception exc) {
            final String message = "Error deleting user " + userDTO.getFirstName() + " " + userDTO.getLastName();
            logger.logp(Level.SEVERE, getClass().getName(), "deleteUser", message, exc);
            throw new PortalException(message);
        }
    }

    protected List<OrganizationCwd> getOrganizations(long communityId) {
        return organizationDAO.findOrganizationEntities(communityId);
    }

    /**
     * get a list of all orgs, convert to dtos and map to org ids
     *
     * @param communityId
     * @return
     * @throws PortalException
     */
    @Override
    public List<OrganizationDTO> getOrganizationDTOs(long communityId) throws PortalException {
        List<OrganizationDTO> orgDTOs = null;
        try {
            List<OrganizationCwd> organizations = this.getOrganizations(communityId);
            orgDTOs = new ArrayList<>(organizations.size());
            logger.fine("mapping orgs to orgDtos for community " + communityId);
            for (OrganizationCwd each : organizations) {
                OrganizationDTO orgDTO = OrgUtils.convertOrgToDTO(each, communityId);
                logger.fine("orgDTO for userFormReferenceData, " + orgDTO.toString());
                orgDTOs.add(orgDTO);
            }
        } catch (Exception exc) {
            String message = "Error loading organizations.";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }

        return orgDTOs;
    }

    @Override
    public LinkedHashMap<String, String> getOrganization(long communityId) throws PortalException {
        LinkedHashMap<String, String> data = null;

        try {
            List<OrganizationCwd> organizations = this.getOrganizations(communityId);

            data = new LinkedHashMap<String, String>();

            for (OrganizationCwd temp : organizations) {
                String id = Long.toString(temp.getOrgId());
                String name = temp.getOrganizationCwdName();
                data.put(id, name);
            }
        } catch (Exception exc) {
            String message = "Error loading organizations.";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }

        return data;
    }

    @Override
    public LinkedHashMap<String, String> getAccessLevel(long communityId) throws PortalException {
        LinkedHashMap<String, String> data = null;

        try {
            List<AccessLevel> accessLevels = accessLevelDAO.findAccessLevelEntities(communityId);

            data = new LinkedHashMap<String, String>();

            for (AccessLevel temp : accessLevels) {
                String id = Long.toString(temp.getAccessLevelPK().getAccessLevelId());
                String name = temp.getAccessLevelDesc();

                data.put(id, name);
            }
        } catch (Exception exc) {
            String message = "Error loading Access Level names.";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }

        return data;
    }

    @Override
    public LinkedHashMap<String, String> getRoles(long communityId) throws PortalException {
        LinkedHashMap<String, String> data = null;

        try {
            List<Role> roles = roleDAO.findRoleEntities(communityId);

            data = new LinkedHashMap<String, String>();

            for (Role temp : roles) {
                String id = Long.toString(temp.getPrimaryRoleId());
                String name = temp.getRoleName();
                data.put(id, name);
            }
        } catch (Exception exc) {
            String message = "Error loading Role names.";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }

        return data;
    }

    @Override
    public LinkedHashMap<String, String> getFacilityTypes(long communityId) throws PortalException {
        LinkedHashMap<String, String> data = null;

        try {
            List<FacilityType> facilityTypes = facilityTypeDAO.findFacilityTypeEntities(communityId);

            data = new LinkedHashMap<String, String>();

            for (FacilityType temp : facilityTypes) {
                String id = Long.toString(temp.getFacilityTypePK().getFacilityTypeId());
                String name = temp.getFacilityTypeDesc();
                data.put(id, name);
            }
        } catch (Exception exc) {
            String message = "Error loading facility types.";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }

        return data;
    }

    /**
     * Returns true if we have a valid portal user id
     *
     * @param portalUserId
     * @return
     */
    protected boolean isValidPortalUserId(String portalUserId) {

        boolean isValid = false;

        try {
            User user = userDAO.findUserByEmail(portalUserId);
            isValid = true;
        } catch (NoResultException exc) {
            isValid = false;
        }

        return isValid;
    }

    /**
     * @param email
     * @return userDTO populated with user specific mappedOrgDTOs
     * @throws PortalException
     */
    @Override
    public UserDTO getDemographicUser(String email, Long communityId) throws PortalException {
        String token = AuditUtils.getLoginResult(getThreadLocalRequest()).getToken();
        User userByEmail = userDAO.findUserByEmail(email);
        User userExsistInOneOtherCommunity = userDAO.findUserExistInOtherCommunity(email, communityId);
        boolean exsistInOneOtherCommunity = userExsistInOneOtherCommunity != null;
        boolean exsistInThisCommunity = false;

        UserDTO userDTO = new UserDTO();

        String failMsg = null;
        try {
            boolean activeInThisCommunity = sSOUserDAO.isUserActiveInCommunity(communityId, token, email);
            boolean activeInOneOtherCommunity = sSOUserDAO.isUserActiveNotInOtherCommunity(communityId, token, email);
            if (userByEmail != null) {
                logger.info("found user " + userByEmail.getUserId());
                exsistInThisCommunity = userByEmail.getUserCommunityOrganization(communityId) != null;
                logger.info("exsistInThisCommunity"+ exsistInThisCommunity);
                userDTO = PropertyUtils.populateUserDTODemographicData(userByEmail);
            }
            logger.info("exsistInThisCommunity-"+ exsistInThisCommunity);
            logger.info("activeInOneOtherCommunity"+activeInOneOtherCommunity);
            logger.info("exsistInOneOtherCommunity"+exsistInOneOtherCommunity);
            logger.info("activeInThisCommunity"+activeInThisCommunity);
            //we now need to check across communities if user is active
            userDTO.setFound(userByEmail != null);
            userDTO.setUserExsistInOneOtherCommunity(exsistInOneOtherCommunity);
            userDTO.setUserExistInCommunity(exsistInThisCommunity);
            userDTO.setUserActiveInCommunity(activeInThisCommunity);
            userDTO.setUserActiveAtleastInOneOther(activeInOneOtherCommunity);
            userDTO.setInActiveInOtherCommunities(!sSOUserDAO.isUserActive(communityId, token, email));
            logger.info("Inactive in other communities "+ !sSOUserDAO.isUserActive(communityId, token, email));
        } catch (Exception exc) {
            if (failMsg == null) {
                failMsg = "Error retrieving user: " + email + ".";
            }
            logger.log(Level.SEVERE, failMsg, exc);
            throw new PortalException(failMsg);
        }


        return userDTO;
    }

    @Override
    public UserDTO getUser(String email, Long communityId) throws PortalException {

        try {
            User theUser = userDAO.findUserByEmail(email, communityId);
            if (getDaysSinceLastChange(theUser.getLastPasswordChangeDate(), new Date()) >= 90) {
                theUser.setMustChangePassword(WebConstants.YES);
            }

            UserDTO userDTO = PropertyUtils.populateUserDTO(theUser, communityId);

            return userDTO;
        } catch (Exception exc) {
            String message = "Error retrieving user: " + email + ".";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }
    }

    private String getDefaultString(String data) {
        return StringUtils.defaultString(data);
    }

    @Override
    public UserDTO getUser(long userId, Long communityId) throws PortalException {
        try {
            long start = System.currentTimeMillis();


            User theUser = userDAO.findUser(userId);
            UserDTO userDTO = PropertyUtils.populateUserDTO(theUser, communityId);
            long end = System.currentTimeMillis();

            logger.info("Total:time taken to get User Info : " + (end - start));
            return userDTO;
        } catch (Exception exc) {
            String message = "Error retrieving user: User ID=" + userId + ".";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }
    }

    private void addUserToHisp(User user, Long communityId, String directAddress) throws Exception {
        logger.info("add User To Hisp for James2 :");
        String email = user.getEmail();

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        String directDomain = configurationDAO.getProperty(communityId, "hisp.direct.domain");
        String password = configurationDAO.getProperty(communityId, "hisp.direct.defaultuserpassword", "password");

        getHispClient(communityId).createAccount(directAddress, password);
    }

    private void addUserToHispJames3(User user, long communityId, String directAddress) throws Exception {
        logger.info("add User To Hisp for James3 :");
        String email = user.getEmail();

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        String directDomain = configurationDAO.getProperty(communityId, "hisp.james3.direct.domain");
        String password = configurationDAO.getProperty(communityId, "hisp.james3.direct.defaultuserpassword", "password");

        getHispClient(communityId).createAccountForJames3(directAddress, password, directDomain);
    }

    private void updateUserMessagingApp(UserDTO userDTO, List<String> apps, long communityId) throws Exception {

        if (apps == null || apps.isEmpty()) {
            return;
        }

        // loop thru the selected apps
        // if not in the list, then add it
        List<String> appsFromDb = getUserApps_Legacy(userDTO, communityId);

        List<String> newAppsToAdd = new ArrayList<String>();

        // find list of new apps
        for (String tempAppName : apps) {
            if (!appsFromDb.contains(tempAppName)) {
                // let's add this app
                newAppsToAdd.add(tempAppName);
            }
        }

        if (!newAppsToAdd.isEmpty()) {
            long userId = userDTO.getUserId();
            addApps(newAppsToAdd, userId, communityId);
        }
    }

    private List<String> getUserApps_Legacy(UserDTO userDTO, long communityId) throws PortalException {

        long userId = userDTO.getUserId();

        List<String> appKeys = getUserApplicationNames(userId, communityId);

        return appKeys;
    }

    /**
     * @param accessLevelId
     * @param communityId
     * @return
     * @throws PortalException
     * @deprecated ?
     */
    public int getTotalCount(long accessLevelId, long communityId) throws PortalException {

        try {
            AccessLevel accessLevel = accessLevelDAO.findAccessLevel(accessLevelId, communityId);
            return userDAO.getTotalCount(communityId, accessLevel);
        } catch (Exception exc) {
            String message = "Error getting total count.";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }
    }

    /**
     * @param communityId
     * @return
     * @throws PortalException
     * @deprecated @param accessLevelId
     */
    public int getLocalAdminTotalCountForUsers(long accessLevelId, long communityId) throws PortalException {

        try {
            AccessLevel accessLevel = accessLevelDAO.findAccessLevel(accessLevelId, communityId);

            List<User> users = getLocalAdminUsers(accessLevel, communityId);
            int totalCount = users.size();

            return totalCount;
        } catch (Exception exc) {
            String message = "Error getting total count.";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }
    }

    protected final void addNotificationAudit(UserDTO userDTO, String notificationStatus,
                                              GSIHealthNotificationMessage constructedMessage, long communityId) {

        NotificationMetadata notificationMetadata = constructedMessage.getNotificationMetadata();

        String fromOid = notificationMetadata.getFrom();
        String toOid = notificationMetadata.getTo();

        NotificationAudit notificationAudit = new NotificationAudit();
        notificationAudit.setCreatedOn(new java.util.Date());
        notificationAudit.setFromOrgid(fromOid);
        notificationAudit.setToOrgid(toOid);
        notificationAudit.setEnrolledStatus(null);
        notificationAudit.setMessageStatus(notificationStatus);
        notificationAudit.setCommunityId(communityId);
        String alertMessageBody = constructedMessage.getNotificationMessage().getAlertNotificationMessage().getAlertMessage().getAlertMessageBody();
        notificationAudit.setMessage("Sending Alert: " + alertMessageBody);
        notificationAudit.setReasonCode(notificationMetadata.getReasonCode());

        //add Notification in NOTIFICATION_AUDIT Table
        notificationAuditDAO.create(notificationAudit);
    }

    @Override
    public UserFormReferenceData getUserFormReferenceData() throws PortalException {
        try {

            LoginResult loginResult = AuditUtils.getLoginResult(this.getThreadLocalRequest());

            long communityId = loginResult.getCommunityId();

            logger.fine("getUserFormReferenceData communityID..." + communityId);
            return getUserFormReferenceData(communityId);

        } catch (Exception exc) {
            String message = "Error getting user form reference data.";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }
    }

    private UserFormReferenceData getUserFormReferenceData(long communityId) throws PortalException {

        UserFormReferenceData userFormReferenceData = new UserFormReferenceData();
        logger.fine("getUserFormReferenceData communityID..." + communityId);

        userFormReferenceData.setOrganizationDTOs(this.getOrganizationDTOs(communityId));

        LinkedHashMap<String, String> accessLevels = this.getAccessLevel(communityId);
        userFormReferenceData.setAccessLevels(accessLevels);

        LinkedHashMap<String, String> roles = this.getRoles(communityId);
        userFormReferenceData.setRoles(roles);

        return userFormReferenceData;
    }

    private void addConsentForNewUser(UserDTO userDTO, long communityId) throws Exception {
        logger.info("Add consent for new user, find email");
        // get the user id from database, based on email
        User theUser = userDAO.findUserByEmail(userDTO.getEmail(), communityId);
        long userId = theUser.getUserId();
        logger.info("Add consent for new user, get org id");
        long organizationId = theUser.getUserCommunityOrganization(communityId).getOrganizationCwd().getOrgId();

        logger.info("Add consent for new user, find prg patient consent entities");
        // get a list of patient ids that have provide consent to the user's organization 
        List<OrganizationPatientConsent> orgPatientConsentList = organizationPatientConsentDAO.findOrganizationPatientConsentEntitiesByOrg(organizationId, communityId);

        logger.info("Add consent for new user, add user patient consent");
        // add user patient consent for the patient ids
        addUserPatientConsent(orgPatientConsentList, userId, communityId);

        logger.info("Add consent for new user, leaving addConsentForNewUser");
    }

    @Override
    public List<String> getUserApps(UserDTO userDTO) throws PortalException {

        long userId = userDTO.getUserId();

        List<String> appKeys = getUserApplicationNames(userId);

        return appKeys;
    }

    //Admin Search Section

    /**
     * @param searchCriteria
     * @return
     * @throws PortalException
     */
    @Override
    public int findTotalNoOfUser(SearchCriteria searchCriteria, long communityId) throws PortalException {
        int totalCount = 0;
        User loggedinUser = getUserFromSession();
        try {
            UserCommunityOrganization uco = loggedinUser.getUserCommunityOrganization(communityId);
            totalCount = userDAO.getTotalUserCount(uco, searchCriteria);

        } catch (Exception exc) {
            final String message = "Error getting total count for users: ";
            logger.logp(Level.SEVERE, getClass().getName(), "getTotalCountForCandidates", message, exc);
        }
        return totalCount;
    }

    /**
     * @param searchCriteria
     * @param accessLevelId
     * @param communityId
     * @return
     * @throws MappingException
     * @throws Exception
     * @deprecated ?
     */
    protected int filterTotalNoOfAdminUser(SearchCriteria searchCriteria, long accessLevelId, long communityId) throws MappingException, Exception {

        AccessLevel accessLevel = accessLevelDAO.findAccessLevel(accessLevelId, communityId);

        List<User> userList = null;

        if (accessLevelId == LOCAL_ADMIN) {
            userList = getLocalAdminUsers(accessLevel, communityId);
        } else if ((accessLevelId == SUPER_USER) || (accessLevelId == POWER_USER)) {
            userList = userDAO.findUsers(communityId, accessLevel);
        }

        List<User> tempUserList = userDAO.filterUsers(searchCriteria, userList);

        return tempUserList.size();
    }

    /**
     * @param searchCriteria
     * @param accessLevelId
     * @param communityId
     * @param pageNumber
     * @param pageSize
     * @return paginated set of users
     * @throws PortalException
     */
    @Override
    public SearchResults<UserDTO> findCandidates(SearchCriteria searchCriteria, long accessLevelId,
                                                 long communityId, int pageNumber, int pageSize) throws PortalException {
        //logger.fine("findCandidates  LastName:" + searchCriteria.getLastName() + "::communityId:" + communityId);
        User loggedinUser = getUserFromSession();
        UserCommunityOrganization uco = loggedinUser.getUserCommunityOrganization(communityId);
        SearchResults<UserDTO> searchResults = null;
        int totalCount = 0;
        try {
            List<User> users = userDAO.findUsers(uco, searchCriteria, pageNumber, pageSize);
            List<UserDTO> adminUserList = PropertyUtils.convertToDTOs(users, userCommunityOrganizationDAO, communityId);

            totalCount = this.findTotalNoOfUser(searchCriteria, communityId);
            logger.fine("findCandidates totalCount >>>>>>>" + totalCount);

            searchResults = new SearchResults<UserDTO>(totalCount, adminUserList);
            logger.fine("searchResults >>>>>>>" + searchResults);

        } catch (Exception exc) {
            final String message = "Error searching for users";
            logger.logp(Level.SEVERE, getClass().getName(), "findCandidates", message, exc);
        }

        return searchResults;
    }

    /**
     * @param accessLevelId
     * @param communityId
     * @return
     * @throws MappingException
     * @throws Exception
     * @deprecated @param searchCriteria
     */
    protected List<UserDTO> filterAdminUser(SearchCriteria searchCriteria, long accessLevelId, long communityId) throws MappingException, Exception {

        logger.fine("filterAdminUser  LastName:" + searchCriteria.getLastName() + "communityId " + communityId);

        List<UserDTO> searchResults = new ArrayList<UserDTO>();

        AccessLevel accessLevel = accessLevelDAO.findAccessLevel(accessLevelId, communityId);

        List<User> users = null;

        if (accessLevelId == LOCAL_ADMIN) {
            users = getLocalAdminUsers(accessLevel, communityId);
        } else if (accessLevelId == SUPER_USER) {
            users = userDAO.findUsers(communityId, accessLevel);
        }

        List<User> tempUserList = userDAO.filterUsers(searchCriteria, users);

        if (tempUserList != null) {

            searchResults = PropertyUtils.convertToDTOs(tempUserList, userCommunityOrganizationDAO);

        }
        return searchResults;
    }

    private boolean userRoleOrAccessHasChanged(UserDTO userDTO, Long oldRoleId, Long oldAccessLevelId) {

        Long dtoAccessLevelId = Long.parseLong(userDTO.getAccessLevelId());
        Long dtoRoleId = Long.parseLong(userDTO.getSecurityRoleId());

        return ((!Objects.equals(dtoAccessLevelId, oldAccessLevelId)) || (!Objects.equals(dtoRoleId, oldRoleId)));
    }

    private void updatePatientConsent(long userId, Long oldOrganizationId, Long newOrganizationId, Long communityId) throws Exception {

        // delete entries for user from old org
        List<OrganizationPatientConsent> oldOrgPatientConsentList = null;
        if (oldOrganizationId != null) {
            oldOrgPatientConsentList = organizationPatientConsentDAO
                    .findOrganizationPatientConsentEntitiesByOrg(oldOrganizationId, communityId);
            deleteUserPatientConsent(oldOrgPatientConsentList, userId, communityId);
        }


        // add entries to user for new org
        //
        // get a list of patient ids that have provide consent to the user's organization 
        List<OrganizationPatientConsent> newOrgPatientConsentList = organizationPatientConsentDAO.findOrganizationPatientConsentEntitiesByOrg(newOrganizationId, communityId);

        // add user patient consent for the patient ids
        addUserPatientConsent(newOrgPatientConsentList, userId, communityId);
    }

    private void deleteUserPatientConsent(List<OrganizationPatientConsent> orgPatientConsentList, long userId, long communityId) {

        // delete user consent for each of those patients
        for (OrganizationPatientConsent temp : orgPatientConsentList) {
            long patientId = temp.getOrganizationPatientConsentPK().getPatientId();

            UserPatientConsentPK pk = new UserPatientConsentPK(patientId, userId, communityId);
            //old code
            // UserPatientConsentPK pk = new UserPatientConsentPK(patientId, userId, Constants.GSI_COMMUNITY_ID);

            try {
                userPatientConsentDAO.destroy(pk);
            } catch (Exception exc) {
                logger.warning("Could not delete user patient consent. userId=" + userId + ", patientId=" + patientId + ": " + exc.getMessage());

            }
        }
    }

    private void createUserAlertFilters(UserDTO userDTO, Long communityId) throws Exception {
        StopWatch watch2 = new StopWatch();
        watch2.start();

        String email = userDTO.getEmail();

        logger.info("user alert filter creation for: " + email);
        User theUser = userDAO.findUserByEmail(email, communityId);

        List<ApplicationSendsAlert> appSendsAlert = subcriptionManagerAlertServiceDAO.getAppSendsAlerts(communityId);

        List<UserAlertFilter> userAlertFilterList = getUserAlertFilterList(appSendsAlert, communityId, theUser.getUserId());
        if (userAlertFilterList != null) {
            subcriptionManagerAlertServiceDAO.saveUserAlertFilter(userAlertFilterList);
            logger.info("Successfully create Application Alerts for User... " + email);
        }

        watch2.stop();
        logger.info("FINISHED user alert filter creation. user: " + email + ",  time = " + watch2.getTime() / 1000.0 + " secs");
    }

    @Override
    public List<String> getUserApps_Legacy(UserDTO userDTO) throws PortalException {
        long userId = userDTO.getUserId();

        List<String> appKeys = getUserApplicationNames(userId);

        return appKeys;
    }

    class UpdatePatientConsentTask implements Runnable {

        long userId;
        Long oldOrgId;
        Long newOrgId;
        Long communityId;

        public UpdatePatientConsentTask(long theUserId, Long theOldOrgId, Long theNewOrgId, Long theCommunityId) {
            userId = theUserId;
            oldOrgId = theOldOrgId;
            newOrgId = theNewOrgId;
            communityId = theCommunityId;
        }

        @Override
        public void run() {
            try {
                logger.info("running update patient consent in background....user id=" + userId);
                updatePatientConsent(userId, oldOrgId, newOrgId, communityId);
                logger.info("finished running update patient consent in background. user id=" + userId);

                logger.info("running synch my patient list in background....user id=" + userId);
                synchMyPatientList(userId, communityId);
                logger.info("finished synch my patient list in background. user id=" + userId);

            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Error updating patient consent. user id=" + userId, ex);
            }
        }

        private void synchMyPatientList(long userId, long communityId) {

            logger.info("starting: AdminServiceImpl.synchMyPatientList");

            // get an initial list of patients for this user's list
            List<Long> patientIdsFromMyList = myPatientListDAO.getPatientIdsForPatientList(userId, communityId);
            logger.info("AdminServiceImpl.myPatientSynch: patientIdsFromMyList=" + patientIdsFromMyList);

            // check if user has consent for these patients
            List<Long> patientIdsWithConsent = myPatientListDAO.getPatientIdsWithConsent(userId, communityId);
            logger.info("AdminServiceImpl.myPatientSynch: patientIdsWithConsent=" + patientIdsWithConsent);

            // this is the list of patients who we no longer have consent for
            patientIdsFromMyList.removeAll(patientIdsWithConsent);
            logger.info("AdminServiceImpl.myPatientSynch: list of patients who no longer have consent=" + patientIdsFromMyList);

            // remove the remaining unconsented patients from this users patient list
            myPatientListDAO.deleteFromPatientList(patientIdsFromMyList, userId, communityId);

            logger.info("AdminServiceImpl.myPatientSynch: removed patient ids=" + patientIdsFromMyList + ", for this userId: " + userId);

            logger.info("finished: AdminServiceImpl.synchMyPatientList");
        }
    }

    class CreateUserAlertFiltersTask implements Runnable {

        private UserDTO userDTO;
        private long communityId;

        CreateUserAlertFiltersTask(UserDTO theUserDTO, long theCommunityId) {
            userDTO = theUserDTO;
            communityId = theCommunityId;
        }

        @Override
        public void run() {
            final String email = userDTO.getEmail();

            try {
                logger.info("running CreateUserAlertFiltersTask in background.  user email=" + email);
                createUserAlertFilters(userDTO, communityId);
                logger.info("finished running CreateUserAlertFiltersTask in background. user email=" + email);
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Error running CreateUserAlertFiltersTask: user email=" + email, ex);
            }
        }
    }

    class AddConsentForNewUserTask implements Runnable {

        private UserDTO userDTO;
        long communityId;

        AddConsentForNewUserTask(UserDTO theUserDTO, long theCommunityId) {
            userDTO = theUserDTO;
            communityId = theCommunityId;
        }

        @Override
        public void run() {
            final String email = userDTO.getEmail();
            try {
                logger.info("running AddConsentForNewUserTask in background.  user email=" + email);
                addConsentForNewUser(userDTO, communityId);
                logger.info("finished running AddConsentForNewUserTask in background. user email=" + email);
            } catch (Exception ex) {
                logger.log(Level.SEVERE, "Error running AddConsentForNewUserTask: user email=" + email, ex);
            }
        }
    }

    class SendUserCreatedAlertTask extends Task {

        private UserDTO userDTO;
        private long communityId;

        SendUserCreatedAlertTask(long theCommunityId, UserDTO theUserDTO, OrganizationCwd theOrganization) {
            userDTO = theUserDTO;

            // copy org info to the DTO
            userDTO.setOrganization(theOrganization.getOrganizationCwdName());
            userDTO.setOid(theOrganization.getOid());
            communityId = theCommunityId;
        }

        @Override
        public void run() {
            sendUserCreatedAlert();
        }

        private void sendUserCreatedAlert() {

            GSIHealthNotificationMessage constructedMessage = null;
            String notificationStatus = null;

            try {

                ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

                String gsiOid = configurationDAO.getProperty(communityId, "gsihealth.notification.send.fromOID");

                // get alert logical id
                String alertLogicalId = AlertConstants.USER_CREATED_ALERT_ID;
                String userOid = userDTO.getOid();

                String alertMessageBody = userDTO.getFirstName() + " " + userDTO.getLastName() + " associated to "
                        + userDTO.getOrganization() + " " + userOid + " has been created within GSIHealthCoordinator.";

                // construct the message
                constructedMessage = NotificationUtils.constructUserAlertNotificationMessage(communityId, alertLogicalId, alertMessageBody, userOid, gsiOid);

                // send the message
                notificationStatus = NotificationUtils.sendAlert(communityId, constructedMessage);
            } catch (Throwable exc) {
                final String message = "Error Sending message to WebService for 'NotificationManager' ";
                logger.logp(Level.SEVERE, getClass().getName(), "addPatient", message, exc);
                notificationStatus = "FAILURE";
            } finally {
                // audit the message
                addNotificationAudit(userDTO, notificationStatus, constructedMessage, communityId);
            }

        }
    }

    class SendUserDeactivatedAlertTask extends Task {

        private UserDTO userDTO;
        private long communityId;

        SendUserDeactivatedAlertTask(long theCommunityId, UserDTO theUserDTO, OrganizationCwd theOrganization) {
            userDTO = theUserDTO;

            // copy org info to the DTO
            userDTO.setOrganization(theOrganization.getOrganizationCwdName());
            userDTO.setOid(theOrganization.getOid());
            communityId = theCommunityId;
        }

        @Override
        public void run() {
            sendUserDeactivatedAlert();
        }

        private void sendUserDeactivatedAlert() {

            GSIHealthNotificationMessage constructedMessage = null;
            String notificationStatus = null;

            try {
                ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

                String gsiOid = configurationDAO.getProperty(communityId, "gsihealth.notification.send.fromOID");

                // get alert logical id
                String alertLogicalId = AlertConstants.USER_INACTIVATED_ALERT_ID;
                String userOid = userDTO.getOid();

                // alert message body
                String alertMessageBody = userDTO.getFirstName() + " " + userDTO.getLastName() + " associated to "
                        + userDTO.getOrganization() + " " + userOid + " has been inactivated within GSIHealthCoordinator.";

                // construct the message
                constructedMessage = NotificationUtils.constructUserAlertNotificationMessage(communityId, alertLogicalId, alertMessageBody, userOid, gsiOid);

                // send the message
                notificationStatus = NotificationUtils.sendAlert(communityId, constructedMessage);
            } catch (Throwable exc) {
                final String message = "Error Sending message to WebService for 'NotificationManager' ";
                logger.logp(Level.SEVERE, getClass().getName(), "addPatient", message, exc);
                notificationStatus = "FAILURE";
            } finally {
                // audit the message
                addNotificationAudit(userDTO, notificationStatus, constructedMessage, communityId);
            }

        }
    }

    protected LinkedHashMap<String, String> getOrganizationNames(long communityId) throws PortalException {
        LinkedHashMap<String, String> data = null;

        try {
            List<OrganizationCwd> orgs = organizationDAO.findOrganizationEntities2(communityId);

            data = new LinkedHashMap<String, String>();

            for (OrganizationCwd temp : orgs) {
                String oid = Long.toString(temp.getOrgId());
                String orgName = temp.getOrganizationCwdName();
                data.put(oid, orgName);
            }
        } catch (Exception exc) {
            String message = "Error loading organization names";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }

        return data;
    }

    @Override
    public int getDaysSinceLastChange(Date old, Date now) {

        int days = 0;
        try {
            Date lastPasswordChangedDate = old;
            int daysSinceLastChangedPassword = DateUtils.daysBetween(lastPasswordChangedDate, now);
            days = daysSinceLastChangedPassword;
        } catch (NullPointerException npe) {
            //   logger.log(Level.WARNING, "Nullpointer: Last password changed date is null for: {0}{1}", new Object[]{user.getFirstName(), user.getLastName()});
        }
        return days;
    }

    /*
     * Method to provide Build Number,on every build execution..
     */
    @Override
    public String getBuildVersion() {
        String buildReleaseVersion = (String) application.getAttribute(WebConstants.APPLICATION_PROPERTIES_BUILD_VERSION_KEY);
        return buildReleaseVersion;
    }

    @Override
    public List<UserDTO> getUsers(Long communityId) throws PortalException {
        try {
            //Taking out from cached data...
            Map<Long, List<UserDTO>> usermap = (Map<Long, List<UserDTO>>) application.getAttribute(WebConstants.AVAILABLE_USERS_KEY);
            return usermap.get(communityId);

        } catch (Exception exc) {
            final String message = "Error getting total user count";
            logger.logp(Level.SEVERE, getClass().getName(), "getTotalCountOfInProviders", message, exc);

            throw new PortalException(message);
        }
    }

    private LoginResult buildLoginResultForUserLoader() {
        LoginResult theLoginResult = new LoginResult();

        theLoginResult.setAccessLevel("Super User");
        theLoginResult.setUserLevel("Administrator");
        theLoginResult.setUserLevelId(8);
        theLoginResult.setFirstName("Admin");
        theLoginResult.setLastName("Admin User Loader");
        theLoginResult.setEmail("userloader@gsihealth.com");
        theLoginResult.setUserId(999998);
        theLoginResult.setOrganizationName("MMC");

        return theLoginResult;
    }

    @Override
    public List<UserDTO> updateUserContexts(long communityId) {
        //When user is being update Application Context should be updated because many functionality is using data from AppContext.
        new LoadAvailableUsersInCacheTimerTask(application).run();

        List<UserDTO> userDTOs = null;
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        try { // we already have users in the app context, get it
            ServletContext application = getServletContext();
            Map<Long, List<UserDTO>> usermap = (Map<Long, List<UserDTO>>) application.getAttribute(WebConstants.AVAILABLE_USERS_KEY);
            userDTOs = usermap.get(communityId);
        } catch (Exception exc) {
            String message = "Error loading users.";
            logger.log(Level.SEVERE, message, exc);
        }
        stopWatch.stop();
        logger.info("it took " + stopWatch.getTime() / 1000.0 + " getUsers/2070");

        return userDTOs;
    }

    ;

    @Override
    public OrganizationDTO getOrganizationByOid(String oid, long communityId) throws PortalException {
        try {
            OrganizationCwd org = organizationDAO.findOrganization(oid, communityId);
            Community community = communityDAO.getCommunityById(communityId);

            try {
                CommunityOrganization co = communityOrganizationDAO.findCommunityOrganization(community, org);
                if (co != null) {
                    final String message = "The organization already exists in this community.";
                    logger.logp(Level.FINE, getClass().getName(), "getOrganizationByOid", message);
                    throw new PortalException(message);
                }
            } catch (javax.persistence.NoResultException e) {
                // do nothing
            }

            if (org != null)
                return OrgUtils.convertOrgToDTO(org, communityId);
            else
                return null;
        } catch (javax.persistence.NoResultException e) {
            return null;
        } catch (PortalException pe) {
            throw pe;
        } catch (Exception exc) {
            final String message = "Error getting Org by oid";
            logger.logp(Level.SEVERE, getClass().getName(), "getOrganizationByOid", message, exc);

            throw new PortalException(message);
        }

    }

    ;

    //JIRA 727
    public List<CommunityDTO> getCommunitiesForUser(Long userId) {
        List<CommunityDTO> communityDtoList = new ArrayList<CommunityDTO>();

        List<UserCommunityOrganization> userCommunityOrganizationList = userCommunityOrganizationDAO.findUserCommunityOrganizationEntities(userId);
        for (UserCommunityOrganization uco : userCommunityOrganizationList) {
            Community community = uco.getCommunity();
            CommunityDTO communityDto = new CommunityDTO();
            communityDto.setCommunityId(community.getCommunityId());
            communityDto.setCommunityName(community.getCommunityName());

            communityDtoList.add(communityDto);
        }
        return communityDtoList;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<UserDTO> getSupervisors(Long communityId, String orgId) throws PortalException {
        ServletContext application = getServletContext();

        Map<Long, List<UserDTO>> dtoMap = (Map<Long, List<UserDTO>>) application.getAttribute(WebConstants.AVAILABLE_USERS_KEY);
        List<UserDTO> users = dtoMap.get(communityId);

        //filter active user
        List<UserDTO> supervisors = new ArrayList<UserDTO>();
        for (UserDTO user : users) {
            if (user.getStatus().equals("ACTIVE") && orgId.equals(user.getOrgid())) {
                supervisors.add(user);
            }
        }

        return supervisors;
    }

    private class UpdateUserSubordinateTask implements Runnable {
        private Long userId;
        private Long communityId;
        private LoginResult loginResult;

        public UpdateUserSubordinateTask(Long communityId, Long userId, LoginResult loginResult) {
            this.userId = userId;
            this.communityId = communityId;
            this.loginResult = loginResult;
        }

        @Override
        public void run() {
            List<User> users = userDAO.findUserBySupervisor(userId, communityId);

            if (users != null) {
                userDAO.removeSupervisor(userId, communityId);

                for (User user : users) {
                    User updatedUser = userDAO.findUser(user.getUserId());
                    if (updatedUser.getUserCommunityOrganization(communityId).getSupervisor() == null) {
                        saveSupervisorAudit(updatedUser, userId, communityId, loginResult);
                    }
                }
            }
        }
    }

    private void updateSubordinate(Long communityId, Long userId, LoginResult loginResult) {
        UpdateUserSubordinateTask updateUserSubordinateTask = new UpdateUserSubordinateTask(communityId, userId, loginResult);
        executor.execute(updateUserSubordinateTask);
    }
}
