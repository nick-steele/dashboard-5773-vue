/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.service.SubcriptionManagerAlertService;
import com.gsihealth.dashboard.common.ApplicationInfo;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.entity.AlertRole;
import com.gsihealth.entity.ApplicationSendsAlert;
import com.gsihealth.entity.UserAlertFilter;
import com.gsihealth.dashboard.entity.dto.AlertFilterPreferenceDTO;
import com.gsihealth.dashboard.entity.dto.AlertPreferenceDTO;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.SubcriptionManagerAlertServiceDAO;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 *
 * @author Vinay
 */
public class SubcriptionManagerAlertServiceImpl extends BasePortalServiceServlet implements SubcriptionManagerAlertService {

    private Logger logger = Logger.getLogger(getClass().getName());
    private ServletContext application;
    private SubcriptionManagerAlertServiceDAO subcriptionManagerAlertServiceDAO;

    @Override
    public void init() throws ServletException {
        super.init();
        application = getServletContext();
        subcriptionManagerAlertServiceDAO = (SubcriptionManagerAlertServiceDAO) application.getAttribute(WebConstants.SUBC_MANAGER_ALERT_KEY);
    }

    /**
     * converts applicationInfoMap to an AlertPreferenceDTO list
     * 
     * @return List of AlertPreferenceDTO test data
     */
    @Override
    public List<AlertPreferenceDTO> getAlerts(Long communityId, Map<String, ApplicationInfo> applicationInfoMap, long userId) throws PortalException {

        List<AlertPreferenceDTO> alertDTOList = new ArrayList<AlertPreferenceDTO>();
        AlertPreferenceDTO alertprefDTO = null;
        try {

            Collection<ApplicationInfo> values = applicationInfoMap.values();

            for (ApplicationInfo temp : values) {
                alertprefDTO = new AlertPreferenceDTO();
                alertprefDTO.setAlertIcon(temp.getIconFileName());
                alertprefDTO.setApplicationID(temp.getApplicationId());
                alertprefDTO.setAlertName(temp.getScreenName());
                alertprefDTO.setCommunityID(communityId);
                if(hasAlerts(userId, alertprefDTO)) {
                    alertDTOList.add(alertprefDTO);
                }

            }

        } catch (Exception ex) {
            Logger.getLogger(SubcriptionManagerAlertServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }

        return alertDTOList;

    }
    
    /**
     * 
     * lets not show an alert category that has no alerts, check first
     * @param userId
     * @param alertPreferenceDTO
     * @return true if this alertPreferenceDTO has alerts associated with it
     */
    private boolean hasAlerts(long userId, AlertPreferenceDTO alertPreferenceDTO) {
        List<ApplicationSendsAlert> alertTypePrefList = 
                subcriptionManagerAlertServiceDAO.getEnabledAppSendsAlertList(alertPreferenceDTO.getApplicationID(), alertPreferenceDTO.getCommunityID());
        boolean hasAlerts = alertTypePrefList != null && !alertTypePrefList.isEmpty();
        logger.info("AlertPreferenceDTO " + alertPreferenceDTO.getAlertName() + " has alerts? " + hasAlerts);
        return hasAlerts;
    }

    /**
     * TODO - why all the nested ifs?
     * @param userId
     * @param roleTypeID
     * @param apdto
     * @return
     */
    public List<AlertFilterPreferenceDTO> getAlertFilterSelections(long userId, AlertPreferenceDTO apdto, Long communityId) {
        logger.info("getAlertFilterSelections for user is :" + userId + ":Role Type ID:" + apdto.getUserLevelId() + ":ApplicationID: " + apdto.getApplicationID() + ":AccessLevelID:" + apdto.getAccessLevelID());;
        // roleTypeID is  loginResult.getUserLevelId() 
        List<AlertFilterPreferenceDTO> alertFilterTypeDTOList = new ArrayList<AlertFilterPreferenceDTO>();
        AlertFilterPreferenceDTO alertFilterPrefDTO = null;
// get all enabled alerts for a specific applicationId
        List<ApplicationSendsAlert> alertTypePrefList = subcriptionManagerAlertServiceDAO.getEnabledAppSendsAlertList(apdto.getApplicationID(), communityId);
        if (alertTypePrefList != null) {
         
            for (ApplicationSendsAlert asa : alertTypePrefList) {
                //  logger.info("IsRoleBased :"+asa.getIsRoleBased() +"::APP ID::-"+ asa.getApplicationAlertId()+":AlertName:"+asa.getAlertName()+":ALERT TYPE ID:"+asa.getAlertTypeId().getAlertTypeId());
               
                    if (asa.getIsRoleBased()) {  // ... if it's role based, get all roles for this alert
                        Collection<AlertRole> alertRoleCollection = asa.getAlertRoleCollection();
                        if (alertRoleCollection != null) { 
                            List<AlertRole> alertRoleList = new ArrayList<>(alertRoleCollection);
                            for (AlertRole alertRole : alertRoleList) {

                                if (alertRole.getRoleId() == apdto.getUserLevelId()) { // ... if alert role Id matches user roleId
                                    //  logger.info(" alertRole.getRoleId() == roleTypeID :" + asa.getApplicationAlertId() + ":AlertName:" + asa.getAlertName());

                                    List<UserAlertFilter> userAlertFilterList = asa.getUserAlertFilterList(); // get all uaf for this alert
                                    if (userAlertFilterList != null) {

                                        for (UserAlertFilter uaf : userAlertFilterList) { // compare them to this user
                                            if (uaf.getUserId() == userId) { // if they match, do whatever this shit is
                                                alertFilterPrefDTO = gettingAllUserAlertValues(asa, uaf, apdto.getAccessLevelID(), apdto.getApplicationID());
                                                if (alertFilterPrefDTO.getStatus() != null) {
                                                    alertFilterTypeDTOList.add(alertFilterPrefDTO);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else { // other wise ...

                        List<UserAlertFilter> userAlertFilterList = asa.getUserAlertFilterList();
                        if (userAlertFilterList != null) { // get all uaf's for this alert

                            for (UserAlertFilter uaf : userAlertFilterList) {

                                if (uaf.getUserId() == userId) { // do whatever this shit is
                                    alertFilterPrefDTO = gettingAllUserAlertValues(asa, uaf, apdto.getAccessLevelID(), apdto.getApplicationID());
                                    if (alertFilterPrefDTO.getStatus() != null) {  
                                        alertFilterTypeDTOList.add(alertFilterPrefDTO);
                                    }

                                }
                            }
                        }
                    }

            }
        }
        return alertFilterTypeDTOList;
    }

    /**
     * populate AlertFilterPreferenceDTO with asa and uaf values
     * @param asa
     * @param uaf
     * @param accessLevelID
     * @param applicationID
     * @return 
     */
    private AlertFilterPreferenceDTO gettingAllUserAlertValues(ApplicationSendsAlert asa, UserAlertFilter uaf, long accessLevelID, long applicationID) {
        logger.info(":gettingAllUserAlertValuesElse accessLevelID: ---" + accessLevelID + ":applicationID:" + applicationID);;

        AlertFilterPreferenceDTO alertFilterPrefDTO = new AlertFilterPreferenceDTO();
        alertFilterPrefDTO.setApplicationID(asa.getApplicationId());
        alertFilterPrefDTO.setAlertSeverity(asa.getSeverity());
        alertFilterPrefDTO.setAlertName(asa.getAlertName());
        alertFilterPrefDTO.setCommunityID(uaf.getCommunityId().intValue());
        alertFilterPrefDTO.setUserID(uaf.getUserId());
        alertFilterPrefDTO.setStatus("ENABLED");
        if (asa.getNoFiltering()) {
            alertFilterPrefDTO.setStatus("DISABLED");
        }
        
        alertFilterPrefDTO.setCheckValue(uaf.getShowAlert());
        alertFilterPrefDTO.setApplicationAlertID(asa.getApplicationAlertId());
        alertFilterPrefDTO.setIsRoleBased(asa.getIsRoleBased());
        alertFilterPrefDTO.setNoFiltering(asa.getNoFiltering());
        return alertFilterPrefDTO;
    }

    /**
     * update selected record with database
     * TODO - fix references, should use showalert, not checkvalue
     * @param alertFilterUsers
     */
    @Override
    public void updateUserAlertFilter(List<AlertFilterPreferenceDTO> alertFilterUsers) {

        try {
            List<UserAlertFilter> userAlertFilterList = new ArrayList<UserAlertFilter>();
            UserAlertFilter userAlertFilter = null;
            
            for (AlertFilterPreferenceDTO afpdto : alertFilterUsers) {
                logger.info(afpdto.toString());
                userAlertFilter = new UserAlertFilter();
                BigInteger commIDbig = BigInteger.valueOf(afpdto.getCommunityID().intValue());
                userAlertFilter.setCommunityId(commIDbig);
                userAlertFilter.setAppAlertID(afpdto.getApplicationAlertID());
                userAlertFilter.setUserId(afpdto.getUserID());
                userAlertFilter.setShowAlert(afpdto.isCheckValue());
                if(afpdto.getNoFiltering()) {
                  userAlertFilter.setShowAlert(true);  
                }
                userAlertFilterList.add(userAlertFilter);

            }
            subcriptionManagerAlertServiceDAO.updateUserAlertFilter(userAlertFilterList);

        } catch (Exception ex) {
            Logger.getLogger(SubcriptionManagerAlertServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @deprecated 
     * @param alertFilter
     * @param asa
     * @param uaf 
     */
    private void getAlertFilterAttributes(AlertFilterPreferenceDTO alertFilter, ApplicationSendsAlert asa, UserAlertFilter uaf) {
        alertFilter.setApplicationID(asa.getApplicationId());
        alertFilter.setAlertSeverity(asa.getSeverity());
        alertFilter.setAlertName(asa.getAlertName());
        alertFilter.setCommunityID(uaf.getCommunityId().intValue());
        alertFilter.setUserID(uaf.getUserId());
        if (uaf.getShowAlert()) {
            alertFilter.setStatus("ENABLED");
        } else {
            alertFilter.setStatus("DISABLED");
        }
        alertFilter.setApplicationAlertID(asa.getApplicationAlertId());
        alertFilter.setIsRoleBased(asa.getIsRoleBased());
        alertFilter.setNoFiltering(asa.getNoFiltering());
    }
}
