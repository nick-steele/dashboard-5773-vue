package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.entity.OrganizationCwd;
import com.gsihealth.entity.PatientCommunityEnrollment;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.common.Person;

/**
 *
 * @author Chad.Darby
 */
public interface CareTeamPatientUpdater {
    
    public void runPatientUpdateProcess(PatientInfo patientInfo, Person person, PersonDTO personDTO, OrganizationCwd organization, long patientId, 
            String programLevelConsentStatus, LoginResult loginResult, PatientCommunityEnrollment patientEnrollment, 
            long communityCareteamId, String oldCareteamName) throws Exception;
    
}
