package com.gsihealth.dashboard.server.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.dozer.MappingException;
import org.gsihealth.wsn.GSIHealthNotificationMessage;
import org.gsihealth.wsn.NotificationMetadata;

import com.gsihealth.dashboard.client.service.OrganizationService;
import com.gsihealth.dashboard.common.AlertConstants;
import com.gsihealth.dashboard.common.ConfigurationConstants;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.entity.dto.CommunityOrganizationDTO;
import com.gsihealth.dashboard.entity.dto.OrganizationDTO;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.ApplicationDAO;
import com.gsihealth.dashboard.server.dao.AuditDAO;
import com.gsihealth.dashboard.server.dao.CommunityDAO;
import com.gsihealth.dashboard.server.dao.CommunityOrganizationDAO;
import com.gsihealth.dashboard.server.dao.CommunityOrganizationDAOImpl;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.dashboard.server.dao.FacilityTypeDAO;
import com.gsihealth.dashboard.server.dao.NotificationAuditDAO;
import com.gsihealth.dashboard.server.dao.OrganizationDAO;
import com.gsihealth.dashboard.server.dao.OrganizationDAOImpl;
import com.gsihealth.dashboard.server.dao.UserDAO;
import com.gsihealth.dashboard.server.service.integration.CommunityOrganizationService;
import com.gsihealth.dashboard.server.tasks.Task;
import com.gsihealth.dashboard.server.util.NotificationUtils;
import com.gsihealth.dashboard.server.util.OrgUtils;
import com.gsihealth.entity.Community;
import com.gsihealth.entity.CommunityOrganization;
import com.gsihealth.entity.FacilityType;
import com.gsihealth.entity.NotificationAudit;
import com.gsihealth.entity.OrganizationCwd;
import com.gsihealth.entity.User;

import static com.gsihealth.dashboard.common.Constants.YES_CHAR;

/**
 *
 * @author Satyendra Singh
 */
public class OrganizationServiceImpl extends BasePortalServiceServlet implements OrganizationService {
    private Logger logger = Logger.getLogger(getClass().getName());
    
    private NotificationAuditDAO notificationAuditDAO;
    private AuditDAO auditDAO;
    private ConfigurationDAO configurationDAO;
   
    /**
     * Initializes the service with DAOs
     */
    @Override
    public void init() throws ServletException {
        logger.info("loading OrganizationServiceImpl");
        ServletContext application = getServletContext();
        super.init();
        // Retrieve the DAOs. they were created in the DataContextListener class
        // and stored in the servlet context
        setOrganizationDAO((OrganizationDAO) application.getAttribute(WebConstants.ORGANIZATION_DAO_KEY));
        setFacilityTypeDAO((FacilityTypeDAO) application.getAttribute(WebConstants.FACILITY_TYPE_DAO_KEY));
        setCommunityDAO((CommunityDAO) application.getAttribute(WebConstants.COMMUNITY_DAO_KEY));
        setCommunityOrganizationDAO((CommunityOrganizationDAO) application.getAttribute(WebConstants.COMMUNITY_ORGANIZATION_DAO_KEY));
        setApplicationDAO((ApplicationDAO) application.getAttribute(WebConstants.APPLICATION_DAO_KEY));
        
        auditDAO = (AuditDAO) application.getAttribute(WebConstants.AUDIT_DAO_KEY);
        notificationAuditDAO = (NotificationAuditDAO) application.getAttribute(WebConstants.NOTIFICATION_AUDIT_DAO_KEY);
        
        setCommunityOrganizationService((CommunityOrganizationService) application.getAttribute(WebConstants.COMMUNITY_ORGANIZATION_SERVICE_KEY));

        userDAO = (UserDAO) application.getAttribute(WebConstants.USER_DAO_KEY);

        configurationDAO = (ConfigurationDAO)application.getAttribute(WebConstants.CONFIGURATION_DAO_KEY);
        logger.info("finished loading OrganizationServiceImpl");
    }

    @Override
    public void addOrg(long communityId, OrganizationDTO orgDTO, String token) throws PortalException {

        try {
            logger.fine("addOrganization: Entering");

            logger.fine("addOrganization: adding " + orgDTO.getOrganizationName());

            OrganizationCwd org = new OrganizationCwd();
            org = convertOrgDTOtoOrg(orgDTO, org);
            String facId = orgDTO.getFacilityId();
            Integer facilityTypeId = new Integer(facId);
            logger.fine("Facility Type ID " +facilityTypeId);
            Date now = new Date();
            org.setCreationDateTime(now);
            org.setLastUpdateDateTime(now);

            boolean isExistingOrg = false;
            boolean orgExisitsForCommunity = false;
            if(orgDTO.getOrganizationId() != null) {
            	org.setOrgId(orgDTO.getOrganizationId());
        	isExistingOrg = true;
            }
            validate(org);
            Community community = communityDAO.getCommunityById(communityId);
            
            if(isExistingOrg) {
	        	try {
	        		CommunityOrganization co = communityOrganizationDAO.findCommunityOrganization(community, org);
	        		if(co != null)
	        			orgExisitsForCommunity = true;
	        	} catch(javax.persistence.NoResultException e) {
	        		//if no community_org link is present, continue to create one
	        		CommunityOrganization co = new CommunityOrganization();
	        		co.setCommunity(community);
	        		co.setOrganizationCwd(org);
	        		co.setAutoConsent('N');
	        		co.setIsCmaCmo(YES_CHAR);       //Spira 8785
	                //5863 - fix to move status to comm_org level
	        		co.setStatus(orgDTO.getStatus());
	        		co.setFacilityTypeId(Integer.parseInt(orgDTO.getFacilityId()));
	        		communityOrganizationDAO.create(co);
	        		logger.fine("addOrg: successfully linked org to community " + orgDTO.getOrganizationName());
	        	}
            } else {
            	communityOrganizationService.createOrganization(communityOrganizationDAO, communityDAO, organizationDAO, org, communityId);
            	logger.fine("addOrg: successfully added " + orgDTO.getOrganizationName());
                //set facility type and update
                org = organizationDAO.findOrganization(org.getOid());
                CommunityOrganization co = communityOrganizationDAO.findCommunityOrganization(community, org);
                co.setFacilityTypeId(facilityTypeId);
                
                communityOrganizationDAO.update(co);
                logger.fine("co updated with facility type");
            }
            
            if(!orgExisitsForCommunity) {
                    //JIRA 870
                    //unfortunately, the recently added id is not updated back to the org object
                    //so, get the data from db with the oid
                    org = organizationDAO.findOrganization(org.getOid());
                    logger.info("OrganizationID:" + org.getOrgId());                    
                
	            // move this variable lookups outside of Task to relieve any DB timing issues
	            String fromOid = configurationDAO.getProperty(communityId, "gsihealth.notification.send.fromOID");
	            String toOid = configurationDAO.getProperty(communityId, "gsihealth.notification.send.toOID");
	
	            String communityIdOid = community.getCommunityOid();
	            
	            String sendOrganizationToTreatFlagStr = configurationDAO.getProperty(communityId, ConfigurationConstants.SEND_ORGANIZATION_MESSAGES_TO_TREAT); 
	            boolean sendOrganizationToTreatFlag = Boolean.parseBoolean(sendOrganizationToTreatFlagStr);
	
	            // add the template organizations
	            communityOrganizationService.addTemplateApplications(applicationDAO, communityOrganizationDAO, org,communityId);
	            logOrgDTO("after adding templates" ,orgDTO);
	            // alert notification for organization created.
	            //DASHBOARD-611 Send Dynamic Org Create/Update Message to TREAT       
	            Task sendOrgCreatedAlertTask = new SendOrgCreatedAlertTask(communityId, orgDTO,  fromOid, toOid, communityIdOid, sendOrganizationToTreatFlag);
	            sendOrgCreatedAlertTask.run();
	            logOrgDTO("after creating Alert and message send" ,orgDTO);
	            
	            // check for newly created deactivated organization
	            if (orgDTO.getStatus().equalsIgnoreCase(WebConstants.INACTIVE)) {
	                Task sendOrganizationDeactivatedAlertTask = new SendOrganizationActivationStatusChangeAlertTask(communityId, orgDTO);
	                sendOrganizationDeactivatedAlertTask.run();
	            }
        	}  
            
        } catch (Exception exc) {
            String message = exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "addOrg", message, exc);
            throw new PortalException(message);
        }

        logger.fine("addOrganization: Leaving");
    }

    @Override
    public void updateOrg(long communityId, OrganizationDTO orgDTO, String token) throws PortalException {

        logger.fine("updateOrganization: updating " + orgDTO.getOrganizationName());

        try {
            OrganizationCwd org = new OrganizationCwd();
            org.setLastUpdateDateTime(new Date(System.currentTimeMillis()));
            org = convertOrgDTOtoOrg(orgDTO, org);
            org.setOrgId(orgDTO.getOrganizationId());
             
            Integer facilityTypeId =  Integer.parseInt(orgDTO.getFacilityId());

            validate(org);
            com.gsihealth.entity.Community community = communityDAO.getCommunityById(communityId);
            
            
            
            OrganizationCwd orgForCheckStatus = organizationDAO.findOrganization(orgDTO.getOrganizationId(),communityId);
            String oldStatus = orgForCheckStatus.getCommunityOrganization(communityId).getStatus();  
            
            // move this variable lookups outside of Task to relieve any DB timing issues
            String fromOid = configurationDAO.getProperty(communityId, "gsihealth.notification.send.fromOID");
            String toOid = configurationDAO.getProperty(communityId, "gsihealth.notification.send.toOID");

            String communityIdOid = community.getCommunityOid();
            
            organizationDAO.update(org,communityId);
            //5863 - fix to move status to comm_org level
            CommunityOrganization co = communityOrganizationDAO.findCommunityOrganization(community, org);
            co.setStatus(orgDTO.getStatus());
            co.setFacilityTypeId(Integer.parseInt(orgDTO.getFacilityId()));
            communityOrganizationDAO.update(co);
            
            logger.fine("updateOrganization: successfully updated " + orgDTO.getOrganizationName());
            
            //TODO make only send out dynamic update message when key fields are changed
           //DASHBOARD-611 Send Dynamic Org Create/Update Message to TREAT       
            String sendOrganizationToTreatFlagStr = configurationDAO.getProperty(communityId, ConfigurationConstants.SEND_ORGANIZATION_MESSAGES_TO_TREAT); 
            boolean sendOrganizationToTreatFlag = Boolean.parseBoolean(sendOrganizationToTreatFlagStr);
        
            if (sendOrganizationToTreatFlag) {
                logger.fine("sending org update message to TREAT..");
                Task sendOrgUpdatedMessageToTREATTask = new SendOrgUpdatedMessageToTREATTask(communityId, orgDTO,fromOid, toOid, communityIdOid);
                sendOrgUpdatedMessageToTREATTask.run();
            }

            String newStatus = orgDTO.getStatus();
           
            // check for activation status change organization
            if (!oldStatus.equalsIgnoreCase(newStatus)) {

                // update user statuses also based on organiztion status
                List<User> listUser = new ArrayList<User>();
                listUser = userDAO.findUserEntities();
                 if (newStatus.equalsIgnoreCase(WebConstants.INACTIVE)) {
                    setUserStatus("INACTIVE", listUser, orgDTO, communityId);
                } else {
                    setUserStatus("ACTIVE", listUser, orgDTO, communityId);
                }

                Task sendOrganizationActivationStatusChangeAlertTask = new SendOrganizationActivationStatusChangeAlertTask(communityId, orgDTO, newStatus);
                sendOrganizationActivationStatusChangeAlertTask.run();
            }


        } catch (Exception exc) {
            //final String message = "Error updating Organization " + orgDTO.getOrganizationName();
            final String message = exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "updateOrg", message, exc);
            throw new PortalException(message);
        }
    }

    /**
     * Changes the user status for users in a given organization
     * 
     * @param status
     * @param listUser
     * @param orgDTO
     * @throws Exception 
     */
    protected void setUserStatus(String status, List<User> listUser, OrganizationDTO orgDTO, Long communityId) throws Exception {

        for (User user : listUser) {

            String message = "setting user status for: " + user.getEmail() + ", " + status;
            logger.fine(message);

            try {
                String tempOrgName = user.getUserCommunityOrganization(communityId).getOrganizationCwd().getOrganizationCwdName();
                String organizationName = orgDTO.getOrganizationName();

                if (organizationName.equals(tempOrgName)) {
                    user.getUserCommunityOrganization(communityId).setStatus(status);
                    userDAO.update(user);
                    logger.info("updated uco");
                }
            } catch (Exception exc) {
                logger.log(Level.WARNING, "Error " + message, exc);
            }
        }
    }

    private OrganizationCwd convertOrgDTOtoOrg(OrganizationDTO orgDTO, OrganizationCwd org) {

        //form parameters
        org.setOrganizationCwdName(orgDTO.getOrganizationName());
        org.setOid(orgDTO.getOID());

        //5863 - fix to move status to comm_org level
        
        org.setPhysicalStreetAddress1(orgDTO.getPhysicalStreetAddress1());
        org.setPhysicalStreetAddress2(orgDTO.getPhysicalStreetAddress2());
        org.setPhysicalCity(orgDTO.getPhysicalCity());
        org.setPhysicalAddressState(orgDTO.getPhysicalState());
        org.setPhysicalZipCode(orgDTO.getPhysicalZipCode());

        org.setTelephoneNumber(orgDTO.getWorkPhone());

        return org;
    }

    /**
     * Get a list of administrators
     *
     * @param communityId
     * @param pageNumber
     * @param pageSize
     * @return
     * @throws com.gsihealth.dashboard.common.PortalException
     */
    @Override
    public SearchResults<OrganizationDTO> getOrgs(long communityId, int pageNumber, int pageSize) throws PortalException {

        try {
            int totalCount = organizationDAO.getTotalCount(communityId);
            // Step 1: Get organizations from DAO
            List<OrganizationCwd> orgs = organizationDAO.findOrganizationEntities(communityId, pageNumber, pageSize);
            
//            //5863 - fix to move status to comm_org level
            // Convert to DTOs, fully populated
            List<OrganizationDTO> orgDTOs = OrgUtils.convertToDTOs(orgs,communityId);
            SearchResults searchResults = new SearchResults(totalCount, orgDTOs);
            return searchResults;
        } catch (Exception exc) {
            final String message = "Error loading organizations.";
            logger.logp(Level.SEVERE, getClass().getName(), "getorganizations", message, exc);
            throw new PortalException(message);
        }

    }

    public void deleteOrg(OrganizationDTO orgDTO, String token) throws PortalException {

        try {

            organizationDAO.permDelete(orgDTO.getOrganizationId());

        } catch (Exception exc) {
            final String message = "Error deleting Organization " + orgDTO.getOrganizationName();
            logger.logp(Level.SEVERE, getClass().getName(), "deleteOrganization", message, exc);
            throw new PortalException(message);
        }
    }

    @Override
    public LinkedHashMap<String, String> getOrganizationNames(long communityId) throws PortalException {
        LinkedHashMap<String, String> data = null;

        try {
            List<OrganizationCwd> orgs = organizationDAO.findOrganizationEntities2(communityId);

            data = new LinkedHashMap<String, String>();

            for (OrganizationCwd temp : orgs) {
                String oid = Long.toString(temp.getOrgId());
                String orgName = temp.getOrganizationCwdName();
                data.put(oid, orgName);
            }
        } catch (Exception exc) {
            String message = "Error loading organization names";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }

        return data;
    }
    
    public LinkedHashMap<String, String> getOrganizationNamesForSearchForm(long communityId) throws PortalException{
        LinkedHashMap<String, String> data = null;

        try {
            List<OrganizationCwd> orgs = organizationDAO.findOrganizationEntitiesForSearchForm(communityId);

            data = new LinkedHashMap<String, String>();

            for (OrganizationCwd temp : orgs) {
                String oid = Long.toString(temp.getOrgId());
                String orgName = temp.getOrganizationCwdName();
                data.put(oid, orgName);
            }
        } catch (Exception exc) {
            String message = "Error loading organization names";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }

        return data;
    }
    private void validate(OrganizationCwd theOrg) throws PortalException {

        if (!organizationDAO.isUniqueOrganizationName(theOrg)) {
            throw new PortalException("Error: Organization name already exists.");
        }

        if (!organizationDAO.isUniqueOid(theOrg)) {
            throw new PortalException("Error: OID already exists.");
        }

    }

    public String getOrganizationName(long orgId) throws PortalException {
        String orgName = null;

        try {
            orgName = organizationDAO.getOrganizationName(orgId);

        } catch (Exception exc) {
            String message = "Error loading organization name.";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }

        return orgName;
    }

    public int getTotalCount(long communityId) throws PortalException {
        try {
            int count = organizationDAO.getTotalCount(communityId);

            return count;
        } catch (Exception exc) {
            String message = "Error get organization counts.";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }
    }

    class SendOrgCreatedAlertTask extends Task {

        private OrganizationDTO organizationDTO;
        private long communityId;
        private String fromOid;
        private String toOid;
        private String communityIdOid;
        private Boolean sendOrganizationToTreatFlag;
        
        public SendOrgCreatedAlertTask(long theCommunityId, OrganizationDTO theOrganizationDTO,
                String theFromOid, String theToOid, String theCommunityIdOid, Boolean theSendOrganizationToTreatFlag) {
            organizationDTO = theOrganizationDTO;
            communityId = theCommunityId;
            fromOid = theFromOid;
            toOid = theToOid;
            communityIdOid = theCommunityIdOid;
            sendOrganizationToTreatFlag = theSendOrganizationToTreatFlag;
           
        }

        @Override
        public void run() {
            sendOrgCreatedAlert();
        }

        private void sendOrgCreatedAlert() {
            GSIHealthNotificationMessage constructedMessage = null;
            String notificationStatus = null;

            try {
                String alertLogicalId = AlertConstants.ORGANIZATION_CREATED_ALERT_ID;
                String alertMessageBody = organizationDTO.getOrganizationName() + " " + organizationDTO.getOID() + " has been added to GSIHealthCoordinator.";

                //construct the message
                constructedMessage = NotificationUtils.constructUserAlertNotificationMessage(communityId, alertLogicalId, alertMessageBody, organizationDTO.getOID());

                // send the message
                notificationStatus = NotificationUtils.sendAlert(communityId, constructedMessage);
                         
            } catch (Throwable exc) {
                final String message = constructedMessage.toString();
                logger.logp(Level.SEVERE, getClass().getName(), "addOrganizatio>>>>>>>", message, exc);
                notificationStatus = "FAILURE";

            } finally {
                // audit the message
                addNotificationAudit(organizationDTO, notificationStatus, constructedMessage, communityId);
            }
            
             //If Alert is successful, SEND ORG CREATED message to TREAT within same Task.
               
            if (sendOrganizationToTreatFlag && notificationStatus.equalsIgnoreCase("Success")) {
                constructedMessage = null;
                notificationStatus = null;

                try {
                    logger.fine("Pause Send Org Create thread");
                    Thread.sleep(10000); //pause to keep messages from overlapping
                    logger.fine("Resume Send Org Create thread");
                    
                    logOrgDTO("inside task run - before constructed message", organizationDTO);
                    constructedMessage = NotificationUtils.constructOrganizationCreatedNotificationMessage(communityIdOid, String.valueOf(organizationDTO.getOrganizationId()),
                            organizationDTO, fromOid, toOid);
                    // send the message
                    logOrgDTO("after constructed message", organizationDTO);
                    notificationStatus = NotificationUtils.sendOrganizationMessage(communityId, constructedMessage);
                    logOrgDTO("after status and message sent", organizationDTO);

                } catch (Throwable exc) {
                    final String message = constructedMessage.toString();
                    logger.logp(Level.SEVERE, getClass().getName(), "addOrganizatio>>>>>>>", message, exc);
                    notificationStatus = "FAILURE";

                } finally {
                    // audit the message                  
                    addNotificationAudit(organizationDTO, notificationStatus, constructedMessage, communityId);
                }
            }

        }
    }
    
     class SendOrgCreatedMessageToTREATTask extends Task {

        private OrganizationDTO organizationDTO;
        private long communityId;
        private String fromOid;
        private String toOid;
        private String communityIdOid;
        
        public SendOrgCreatedMessageToTREATTask(long theCommunityId, OrganizationDTO theOrganizationDTO,
                String theFromOid, String theToOid, String theCommunityIdOid) {
            organizationDTO = theOrganizationDTO;
            communityId = theCommunityId;
            fromOid = theFromOid;
            toOid = theToOid;
            communityIdOid = theCommunityIdOid;
            
        }

        @Override
        public void run() {
            sendOrgCreatedMessageToTREAT();
        }

         private void sendOrgCreatedMessageToTREAT() {
             GSIHealthNotificationMessage constructedMessage = null;
             String notificationStatus = null;

             try {
                 
                 //put communityIdOid into constructOrganizationCreatedNotificationMessage

                 //construct the message
                /*long communityId,
                  String orgId,
                  OrganizationDTO organizationDTO,
                  //NotificationUtils.NotificationReasonCode reasonCode,
                  String from,
                  String to */
                 logOrgDTO("inside task run - before constructed message", organizationDTO);
                 constructedMessage = NotificationUtils.constructOrganizationCreatedNotificationMessage(communityIdOid, String.valueOf(organizationDTO.getOrganizationId()),
                         organizationDTO, fromOid, toOid);
                 // send the message
                 logOrgDTO("after constructed message" ,organizationDTO);
                 notificationStatus = NotificationUtils.sendOrganizationMessage(communityId, constructedMessage);
                 logOrgDTO("after status and message sent" ,organizationDTO);

             } catch (Throwable exc) {
                 final String message = constructedMessage.toString();
                 logger.logp(Level.SEVERE, getClass().getName(), "addOrganizatio>>>>>>>", message, exc);
                 notificationStatus = "FAILURE";

             } finally {
                 // audit the message
                 addNotificationAudit(organizationDTO, notificationStatus, constructedMessage, communityId);
             }

         }
    }

     class SendOrgUpdatedMessageToTREATTask extends Task {

        private OrganizationDTO organizationDTO;
        private long communityId;
        private String fromOid;
        private String toOid;
        private String communityIdOid;
        
        public SendOrgUpdatedMessageToTREATTask(long theCommunityId, OrganizationDTO theOrganizationDTO,
           String theFromOid, String theToOid, String theCommunityIdOid) {
            organizationDTO = theOrganizationDTO;
            communityId = theCommunityId;
            fromOid = theFromOid;
            toOid = theToOid;
            communityIdOid = theCommunityIdOid;
           
        }

        @Override
        public void run() {
            sendOrgUpdatedMessageToTREAT();
        }

        private void sendOrgUpdatedMessageToTREAT() {
            GSIHealthNotificationMessage constructedMessage = null;
            String notificationStatus = null;

            try {

              
                //put communityIdOid into constructOrganizationCreatedNotificationMessage

                //construct the message
                /*long communityId,
                 String orgId,
                 OrganizationDTO organizationDTO,
                 //NotificationUtils.NotificationReasonCode reasonCode,
                 String from,
                 String to */
                logOrgDTO("inside task run - before constructed update message", organizationDTO);
                constructedMessage = NotificationUtils.constructOrganizationUpdatedNotificationMessage(communityIdOid,
                        organizationDTO, fromOid, toOid);
                logOrgDTO("after constructed update message" ,organizationDTO);
                // send the message
                notificationStatus = NotificationUtils.sendOrganizationMessage(communityId, constructedMessage);
                logOrgDTO("after status and update message sent" ,organizationDTO);

            } catch (Throwable exc) {
                final String message = constructedMessage.toString();
                logger.logp(Level.SEVERE, getClass().getName(), "updateOrganizatio>>>>>>>", message, exc);
                notificationStatus = "FAILURE";

            } finally {
                // audit the message
                addNotificationAudit(organizationDTO, notificationStatus, constructedMessage, communityId);
            }

        }
    }

    protected final void addNotificationAudit(OrganizationDTO organizationDTO, String notificationStatus, 
            GSIHealthNotificationMessage constructedMessage, long communityId) {

        NotificationMetadata notificationMetadata = constructedMessage.getNotificationMetadata();

        String fromOid = notificationMetadata.getFrom();
        String toOid = notificationMetadata.getTo();

        NotificationAudit notificationAudit = new NotificationAudit();
        notificationAudit.setCreatedOn(new java.util.Date());
        notificationAudit.setFromOrgid(fromOid);
        notificationAudit.setToOrgid(toOid);
        notificationAudit.setEnrolledStatus(null);
        notificationAudit.setMessageStatus(notificationStatus);
        notificationAudit.setCommunityId(communityId);

        if (constructedMessage.getNotificationMessage().getAlertNotificationMessage() != null) {
            String alertMessageBody = constructedMessage.getNotificationMessage().getAlertNotificationMessage().getAlertMessage().getAlertMessageBody();
            notificationAudit.setMessage("Sending Alert: " + alertMessageBody);
        } else {
            notificationAudit.setMessage("Sending Message: " + notificationMetadata.getReasonCode().toString());
        }
        
        notificationAudit.setReasonCode(notificationMetadata.getReasonCode());

        //add Notification in NOTIFICATION_AUDIT Table
        notificationAuditDAO.create(notificationAudit);
    }

    class SendOrganizationActivationStatusChangeAlertTask extends Task {

        private OrganizationDTO organizationDTO;
        private String newStatus;
        private long communityId;
        
        public SendOrganizationActivationStatusChangeAlertTask(long theCommunityId, OrganizationDTO theOrganizationDTO) {            
            this(theCommunityId, theOrganizationDTO, theOrganizationDTO.getStatus());
        }

        public SendOrganizationActivationStatusChangeAlertTask(long theCommunityId, OrganizationDTO theOrganizationDTO, String theNewStatus) {
            organizationDTO = theOrganizationDTO;
            newStatus = theNewStatus;
            
            communityId = theCommunityId;
        }

        @Override
        public void run() {
            sendOrgDeactivatedAlert();
        }

        private void sendOrgDeactivatedAlert() {
            GSIHealthNotificationMessage constructedMessage = null;
            String notificationStatus = null;

            String status;
            if (newStatus.equalsIgnoreCase("active")) {
                status = "activated";
            } else {
                status = "inactivated";
            }
            try {
                String alertLogicalId = AlertConstants.ORGANIZATION_DEACTIVATED_ALERT_ID;
                String alertMessageBody = organizationDTO.getOrganizationName() + " " + organizationDTO.getOID() + " has been " + status + " within GSIHealthCoordinator.";

                //construct the message
                constructedMessage = NotificationUtils.constructUserAlertNotificationMessage(communityId, alertLogicalId, alertMessageBody, organizationDTO.getOID());

                // send the message
                notificationStatus = NotificationUtils.sendAlert(communityId, constructedMessage);

            } catch (Throwable exc) {
                final String message = constructedMessage.toString();
                logger.logp(Level.SEVERE, getClass().getName(), "SendOrganizationDeactivatedAlertThread.run>>>>>>>", message, exc);
                notificationStatus = "FAILURE";

            } finally {
                // audit the message
                //addNotificationAudit(organizationDTO, notificationStatus, constructedMessage);
            }
        }
    }

    @Override
    public int findTotalNoOrgs(long communityId,SearchCriteria searchCriteria) throws PortalException {

        int totalCount = 0;

        try {
            List<OrganizationDTO> organizationList = new ArrayList<OrganizationDTO>();
            organizationList = filterOrganizationUser(communityId,searchCriteria);
            
            if (organizationList.size() > 0) {
                totalCount = organizationList.size();
            }
        } catch (Exception exc) {
            final String message = "Error searching for patients: " + searchCriteria.getOrgName();
            logger.logp(Level.SEVERE, getClass().getName(), "findPatient", message, exc);
        }

        return totalCount;
    }

    protected List<OrganizationDTO> filterOrganizationUser(long communityId,SearchCriteria searchCriteria) throws MappingException, Exception {
        logger.info("filterOrganizationUser getOrgName() " + searchCriteria.getOrgName());
        List<OrganizationDTO> searchResults = new ArrayList<OrganizationDTO>();
        List<OrganizationCwd> organizationList = organizationDAO.findOrganizationEntities(communityId);//getTotalCount();
        List<OrganizationCwd> tempOrgList = new ArrayList<OrganizationCwd>();

        if (organizationList != null) {
            for (OrganizationCwd orgs : organizationList) {
                if (searchCriteria.getOrgName() != null) {

                    if (StringUtils.equalsIgnoreCase(searchCriteria.getOrgName().trim(), orgs.getOrganizationCwdName())) {

                        tempOrgList.add(orgs);
                    }
                } else {
                    tempOrgList = organizationList;
                }
            }
        }

//        //5863 - fix to move status to comm_org level

        if (tempOrgList != null) {
            searchResults = OrgUtils.convertToDTOs(tempOrgList,communityId);
        }

        return searchResults;
    }

    public SearchResults<OrganizationDTO> findCandidates(long communityId,SearchCriteria searchCriteria) throws PortalException {

        SearchResults<OrganizationDTO> searchResults = null;
        int totalCount = 0;
        try {
            List<OrganizationDTO> organizationDTO = new ArrayList<OrganizationDTO>();

            organizationDTO = filterOrganizationUser(communityId,searchCriteria);
             
          List<OrganizationCwd> organizationList=organizationDAO.findOrganizationEntities(communityId);
          
           totalCount=organizationList.size();
           logger.info("findCandidates  totalCount:" + totalCount);
            // build results
            searchResults = new SearchResults<OrganizationDTO>(totalCount, organizationDTO);

        } catch (Exception exc) {
            final String message = "Error searching for Organization: " + searchCriteria.getOrgName();
            logger.logp(Level.SEVERE, getClass().getName(), "findOrganization", message, exc);
        } finally {
            return searchResults;
        }

    }

    public int getTotalCount(long communityId, int pageNumber, int pageSize) throws PortalException {
        
        int totalCount = 0;

        try {
            // Step 1: Get organizations from DAO
            List<OrganizationCwd> orgs = organizationDAO.findOrganizationEntities(communityId, pageNumber, pageSize);

            // Convert to DTOs, fully populated
            List<OrganizationDTO> orgDTOs = OrgUtils.convertToDTOs(orgs,communityId);

        } catch (Exception exc) {

            final String message = "Error searching for patients: ";
            logger.logp(Level.SEVERE, getClass().getName(), "findPatient", message, exc);
        }

        return totalCount;
    }
    private void logOrgDTO(String message, OrganizationDTO orgDTO)  {
     
    //Debugging timing issues
    logger.fine(message +"--->: " 
            + " OrgName: " + orgDTO.getOrganizationName()
            + " OrgOid: " + orgDTO.getOID());
    }
    
    
}
