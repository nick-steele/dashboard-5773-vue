/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.service.PatientActivityTrackerService;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.entity.PatientActivityTracker;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.PatientActivityTrackerDTO;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.PatientActivityTrackerDAO;
import com.gsihealth.dashboard.server.util.AuditUtils;
import com.gsihealth.entity.PatientActivityHistory;
import com.gsihealth.entity.PatientActivityNames;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

/**
 *
 * @author ssingh
 */
public class PatientActivityTrackerServiceImpl extends BasePortalServiceServlet implements PatientActivityTrackerService {

    private PatientActivityTrackerDAO activityTrackerDAO;
    private Logger logger = Logger.getLogger(getClass().getName());
    private final String UPDATE = "Update";
    private final String CREATE = "Create";
    private final String DELETE = "Delete";
    private final String ADD = "Add";

    /**
     * Initializes the service with DAOs
     */
    @Override
    public void init() throws ServletException {

        ServletContext application = getServletContext();
        super.init();
        activityTrackerDAO = (PatientActivityTrackerDAO) application.getAttribute(WebConstants.PATIENT_ACTIVIY_TRACKER_DAO_KEY);
    }

    @Override
    public List<PatientActivityTrackerDTO> getPatientActivities(long communityId, long patientId) throws PortalException {
        try {

            List<PatientActivityTracker> patientActivities = activityTrackerDAO.findPatientActivityEntitiesByCommunityId(patientId, communityId);

            // convert entity objects to dtos
            List<PatientActivityTrackerDTO> activityDtos = convert(patientActivities, patientId);

            return activityDtos;

        } catch (Exception exc) {
            final String message = "Error getting patient activities.";
            logger.logp(Level.SEVERE, getClass().getName(), "getPatientActivities", message, exc);

            throw new PortalException(message);
        }
    }

    /**
     * Convert entity objects to DTOs
     *
     * @param patientActivities
     * @return
     */
    private List<PatientActivityTrackerDTO> convert(List<PatientActivityTracker> patientActivities, long patientId) {

        List<PatientActivityTrackerDTO> activityTrackerDTOs = new ArrayList<PatientActivityTrackerDTO>();

        for (PatientActivityTracker tempActivity : patientActivities) {
            PatientActivityTrackerDTO tempDTO = convertTODTO(tempActivity, patientId);
            activityTrackerDTOs.add(tempDTO);
        }

        return activityTrackerDTOs;
    }

    private PatientActivityTrackerDTO convertTODTO(PatientActivityTracker patientActivity, long patientId) {
        PatientActivityTrackerDTO patientActivityDTO = new PatientActivityTrackerDTO();

        patientActivityDTO.setId(patientActivity.getId());
        patientActivityDTO.setActivityNameId(patientActivity.getActivityId());
        patientActivityDTO.setActivityValue(patientActivity.getActivityValue());
        patientActivityDTO.setEffectiveDate(patientActivity.getEffectiveDate());
        patientActivityDTO.setPatientId(patientActivity.getPatientId());
        patientActivityDTO.setCreationDate(patientActivity.getCreationDate());
        patientActivityDTO.setOldRecord(true);

        return patientActivityDTO;
    }

    /**
     * add or update patient activities DTOs to DB
     *
     * @param dtos
     * @return
     */
    @Override
    public void addUpdatePatientActivities(List<PatientActivityTrackerDTO> dtos, PersonDTO personDTO) throws PortalException {

        LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());

        try {
            List<PatientActivityTrackerDTO> recordsToUpdate = new ArrayList<PatientActivityTrackerDTO>();
            List<PatientActivityTrackerDTO> newRecords = new ArrayList<PatientActivityTrackerDTO>();

            for (PatientActivityTrackerDTO patientActivityDTO : dtos) {

                if (patientActivityDTO.getOperation().equalsIgnoreCase(UPDATE)) {

                    patientActivityDTO.setLastUpdatedDate(new Date());
                    recordsToUpdate.add(patientActivityDTO);

                } else if (patientActivityDTO.getOperation().equalsIgnoreCase(CREATE)) {

                    patientActivityDTO.setCreationDate(new Date());
                    patientActivityDTO.setLastUpdatedDate(new Date());
                    newRecords.add(patientActivityDTO);
                }
            }

            logger.info("converting dtos...");

            List<PatientActivityTracker> updateList = convertFromDtos(recordsToUpdate);
            List<PatientActivityTracker> createList = convertFromDtos(newRecords);
            if (!createList.isEmpty()) {
                logger.info("saving new patient activity list...");
                activityTrackerDAO.savePatientActivityList(personDTO.getPatientEnrollment().getPatientId(), createList);

                logger.info("adding to patient activity history");
                addToPatientActivityHistory(createList, personDTO, ADD);

            }

            if (!updateList.isEmpty()) {

                logger.info("saving updated patient activity list...");
                activityTrackerDAO.updatePatientActivityList(updateList);

                logger.info("adding to patient activity history...");
                addToPatientActivityHistory(updateList, personDTO, UPDATE);

            }


        } catch (Exception exc) {
            final String message = "Error saving patient activities  patient id=" + personDTO.getPatientEnrollment().getPatientId();
            logger.logp(Level.SEVERE, getClass().getName(), "addPatientActivityHelper", message, exc);

            throw new PortalException(message);
        }
    }

    private void addToPatientActivityHistory(List<PatientActivityTracker> patientActivities, PersonDTO personDTO, String action) throws PortalException {
        logger.info("Adding patient's activity info to patient_activity_history...");
        try {
            List<PatientActivityHistory> patientActivityHistoryList = new ArrayList<PatientActivityHistory>();
            for (PatientActivityTracker tempActivity : patientActivities) {
                tempActivity.setPatientId(personDTO.getPatientEnrollment().getPatientId());
                PatientActivityHistory temp = setToPatientActivityHistory(tempActivity, action);
                patientActivityHistoryList.add(temp);
            }
            activityTrackerDAO.addToPatientActivityHistory(patientActivityHistoryList);
        } catch (Exception exc) {
            final String message = "Error adding to patient activity history -- patient id=" + personDTO.getPatientEnrollment().getPatientId();
            logger.logp(Level.SEVERE, getClass().getName(), "addToPatientActivityHistory", message, exc);

            throw new PortalException(message);
        }
    }
    
   /**
     * convert patient activity Entities to DTOs
     *
     * @param dtos
     * @return
     */
    private  List<PatientActivityTracker> convertFromDtos(List<PatientActivityTrackerDTO> dtos) {

        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        List<PatientActivityTracker> patientActivities = new ArrayList<PatientActivityTracker>();

        for (PatientActivityTrackerDTO tempDto : dtos) {
            PatientActivityTracker tempActivity = mapper.map(tempDto, PatientActivityTracker.class);
            patientActivities.add(tempActivity);

        }

        return patientActivities;
    }

    @Override
    public void delete(long id, long communityId) throws PortalException {
        logger.info("Begin: ActivityTrackerServiceImpl.delete");       
        try {
            PatientActivityTracker patiientActivity = activityTrackerDAO.findById(id, communityId);
            // remove from db
            activityTrackerDAO.delete(id, communityId);
            // add to activity tracker history
            PatientActivityHistory temp = setToPatientActivityHistory(patiientActivity, DELETE);           
            activityTrackerDAO.addToPatientActivityHistory(temp);

        } catch (Exception exc) {
            final String message = "Error removing patient activity from list. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "delete", message, exc);
            throw new PortalException("Error removing patient activity from list.");
        } finally {
            logger.info("End: ActivityTrackerServiceImpl.delete");
        }

    }

    private PatientActivityHistory setToPatientActivityHistory(PatientActivityTracker pat, String action) throws Exception {
        try {
            PatientActivityHistory patientActivityHistory = new PatientActivityHistory();
            patientActivityHistory.setCommunityId(pat.getCommunityId());

            patientActivityHistory.setPatientId(pat.getPatientId());

            PatientActivityNames activityName = activityTrackerDAO.getActivityName(pat.getActivityId(), pat.getCommunityId());
            patientActivityHistory.setActivityName(activityName.getName());
            patientActivityHistory.setActivityValue(pat.getActivityValue());
            patientActivityHistory.setActionDate(new Date());
            // set activity effective date
            if (pat.getEffectiveDate() != null) {
                patientActivityHistory.setActivityEffectiveDate(pat.getEffectiveDate());
            }

            patientActivityHistory.setUserId(pat.getUserId());
            patientActivityHistory.setActionType(action);
            patientActivityHistory.setActionSuccess('Y');

            return patientActivityHistory;
        } catch (Exception e) {
            final String message = "Error finding the activity name." + e.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "setToPatientActivityHistory", message, e);
            throw new Exception(message);
        } finally {
            logger.info("End: ActivityTrackerServiceImpl.setToPatientActivityHistory");
        }

    }
}
