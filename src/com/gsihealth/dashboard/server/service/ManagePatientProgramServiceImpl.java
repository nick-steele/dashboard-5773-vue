package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.service.ManagePatientProgramService;
import com.gsihealth.dashboard.common.Constants;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.PatientProgramDTO;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.common.DuplicateException;
import com.gsihealth.dashboard.server.common.ValidationException;
import com.gsihealth.dashboard.server.dao.CommunityDAO;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.dashboard.server.dao.NotificationAuditDAO;
import com.gsihealth.dashboard.server.dao.OrganizationDAO;
import com.gsihealth.dashboard.server.dao.OrganizationPatientConsentDAO;
import com.gsihealth.dashboard.server.dao.PatientEnrollmentDAO;
import com.gsihealth.dashboard.server.dao.PatientProgramDAO;
import com.gsihealth.dashboard.server.dao.ProgramNameHistoryDAO;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import com.gsihealth.dashboard.server.util.AuditUtils;
import com.gsihealth.dashboard.server.util.DateUtils;
import com.gsihealth.dashboard.server.util.NotificationUtils;
import com.gsihealth.entity.PatientProgramName;
import com.gsihealth.entity.ProgramName;
import com.gsihealth.entity.ProgramNameHistory;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.apache.commons.lang.StringUtils;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

/**
 *
 * @author ssingh
 */
public class ManagePatientProgramServiceImpl extends BasePortalServiceServlet implements ManagePatientProgramService {

    private Logger logger = Logger.getLogger(getClass().getName());
    private PatientProgramDAO patientProgramDAO;
    private ProgramNameHistoryDAO programNameHistoryDAO;

    private OrganizationPatientConsentDAO organizationPatientConsentDAO;
    private NotificationAuditDAO notificationAuditDAO;
    private PatientEnrollmentDAO patientEnrollmentDAO;
    private OrganizationDAO organizationDAO;
    private CommunityDAO communityDAO;
    private ConfigurationDAO configurationDAO;
    private PatientManager patientManager;

    private final String UPDATE = "Update";
    private final String CREATE = "Create";

    @Override
    public void init() throws ServletException {
        super.init();
        ServletContext application = getServletContext();

        patientProgramDAO = (PatientProgramDAO) application.getAttribute(WebConstants.PATIENT_PROGRAM_DAO_KEY);
        programNameHistoryDAO = (ProgramNameHistoryDAO) application.getAttribute(WebConstants.PROGRAM_NAME_HISTORY_DAO_KEY);

        organizationPatientConsentDAO = (OrganizationPatientConsentDAO) application.getAttribute(WebConstants.ORGANIZATION_PATIENT_CONSENT_DAO_KEY);
        notificationAuditDAO = (NotificationAuditDAO) application.getAttribute(WebConstants.NOTIFICATION_AUDIT_DAO_KEY);
        patientEnrollmentDAO = (PatientEnrollmentDAO) application.getAttribute(WebConstants.PATIENT_ENROLLMENT_DAO_KEY);
        organizationDAO = (OrganizationDAO) application.getAttribute(WebConstants.ORGANIZATION_DAO_KEY);
        communityDAO = (CommunityDAO) application.getAttribute(WebConstants.COMMUNITY_DAO_KEY);
        patientManager = (PatientManager) application.getAttribute(WebConstants.PATIENT_MANAGER_KEY);
        configurationDAO = (ConfigurationDAO) application.getAttribute(WebConstants.CONFIGURATION_DAO_KEY);
        // store a reference to THIS service in application context
        application.setAttribute(WebConstants.MANAGE_PATIENT_PROGRAM_SERVICE_KEY, this);
    }

    public SearchResults<PatientProgramDTO> getPatientProgramList(long patientId, long communityId, int pageNumber, int pageSize) throws PortalException {
        try {
            int totalCount = patientProgramDAO.getPatientProgramsCountByCommunityId(patientId, communityId);
            System.out.println("getPatientProgramList totalCount =" + totalCount);

            List<PatientProgramName> patientProgramNames = patientProgramDAO.findPatientProgramNameEntitiesByCommunityId(patientId, communityId, pageNumber, pageSize);

            // convert entity objects to dtos
            List<PatientProgramDTO> programDTOs = convert(patientProgramNames, patientId);

            SearchResults<PatientProgramDTO> searchResults = new SearchResults<PatientProgramDTO>(totalCount, programDTOs);

            return searchResults;

        } catch (Exception exc) {
            final String message = "Error getting patient programs.";
            logger.logp(Level.SEVERE, getClass().getName(), "getPatientProgramList", message, exc);

            throw new PortalException(message);
        }
    }

    public int getTotalCountOfPrograms(long patientId, long communityId) throws PortalException {
        try {
            long start = System.currentTimeMillis();
            System.out.println("CommunityID =" + communityId);
            System.out.println("patientId =" + patientId);
            int totalCount = patientProgramDAO.getPatientProgramsCountByCommunityId(patientId, communityId);
            long end = System.currentTimeMillis();
            logger.info("Total associated program counts= " + totalCount + ":Total:time taken:" + (end - start));
            return totalCount;
        } catch (Exception exc) {
            final String message = "Error getting total patient program count";
            logger.logp(Level.SEVERE, getClass().getName(), "getTotalCountOfPrograms", message, exc);

            throw new PortalException(message);
        }
    }

    //JIRA - 1024
    public int getTotalCountOfActivePrograms(long patientId, long communityId) throws PortalException {
        try {
            long start = System.currentTimeMillis();
            int totalCount = patientProgramDAO.getActivePatientProgramsCountByCommunityId(patientId, communityId);
            long end = System.currentTimeMillis();
            logger.info("Total active associated program counts= " + totalCount + ":Total:time taken:" + (end - start));
            return totalCount;
        } catch (Exception exc) {
            final String message = "Error getting total active patient program count";
            logger.logp(Level.SEVERE, getClass().getName(), "getTotalCountOfActivePrograms", message, exc);

            throw new PortalException(message);
        }
    }

    /**
     * Convert entity objects to DTOs
     *
     * @param patientProgramNames
     * @return
     */
    protected List<PatientProgramDTO> convert(List<PatientProgramName> patientProgramNames, long patientId) {

        List<PatientProgramDTO> patientProgDTOs = new ArrayList<PatientProgramDTO>();

        for (PatientProgramName tempPatientProgramName : patientProgramNames) {
            PatientProgramDTO tempDTO = convertTODTO(tempPatientProgramName, patientId);
            patientProgDTOs.add(tempDTO);
        }

        return patientProgDTOs;
    }

    public PatientProgramDTO convertTODTO(PatientProgramName patientProgramName, long patientId) {
        PatientProgramDTO patientProgramDTO = new PatientProgramDTO();
        patientProgramDTO.setPatientProgramNameId(patientProgramName.getPatientProgramNameId());
        patientProgramDTO.setCommunityId(patientProgramName.getCommunityId());
        patientProgramDTO.setPatientId(patientId);
        patientProgramDTO.setHealthHome(patientProgramName.getHealthHome());
        patientProgramDTO.setPatientStatus(patientProgramName.getPatientStatus());
        patientProgramDTO.setProgramEffectiveDate(patientProgramName.getProgramEffectiveDate());
        patientProgramDTO.setProgramEndDate(patientProgramName.getProgramEndDate());
        patientProgramDTO.setProgramId(patientProgramName.getProgramId());
        patientProgramDTO.setTerminationReason(patientProgramName.getTerminationReason());
        patientProgramDTO.setStatusEffectiveDate(patientProgramName.getStatusEffectiveDate());
        patientProgramDTO.setOldRecord(true);
        return patientProgramDTO;
    }

    public void deletePrograms(List<PatientProgramDTO> dtos, PersonDTO personDTO) throws PortalException {

        LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());

        // create enrollment service helper
        EnrollmentServiceHelper enrollmentServiceHelper = buildEnrollmentServiceHelper();

        try {
            List<PatientProgramDTO> recordsToDelete = new ArrayList<PatientProgramDTO>();
            for (PatientProgramDTO programDTO : dtos) {

                if (programDTO.isDelete()) {
                    recordsToDelete.add(programDTO);
                }
                List<PatientProgramName> deletionList = convertFromDtos(recordsToDelete);
                if (!deletionList.isEmpty()) {
                    for (PatientProgramName temp : deletionList) {
                        long patId = temp.getPatientId();
                        long progId = temp.getProgramId();
                        long patProgId=temp.getPatientProgramNameId();
                        logger.info("Deleting selected patient programs list...");
                        patientProgramDAO.deleteEntities(patProgId,patId, progId, loginResult.getCommunityId());
                        addDeletionReasonToProgramNameHistory(deletionList, personDTO, loginResult);
                    }
                }
            }
            //update patient program list
            logger.info("sending patient DEMO update msg with deleted patient program info...");
            personDTO.getPatientEnrollment().getPatientProgramName().removeAll(recordsToDelete);
            // run notification on background thread for patient updates
            enrollmentServiceHelper.sendPatientProgramInfoToNotificationManager(personDTO, personDTO.getPatientEnrollment().getCommunityId());

        } catch (Exception exc) {
            final String message = "Error deleting patient programs  patient id=" + personDTO.getPatientEnrollment().getPatientId();
            logger.logp(Level.SEVERE, getClass().getName(), "addPatientPrograms", message, exc);

            throw new PortalException(message);
        }

    }


    protected static List<PatientProgramName> convertFromDtos(List<PatientProgramDTO> dtos) {

        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        List<PatientProgramName> progName = new ArrayList<PatientProgramName>();

        for (PatientProgramDTO tempDto : dtos) {
            PatientProgramName tempProg = mapper.map(tempDto, PatientProgramName.class);
            progName.add(tempProg);

        }

        return progName;
    }

    private void addToProgramNameHistory(List<PatientProgramName> enrollPrograms, PersonDTO personDTO, LoginResult loginResult) throws PortalException {
        logger.info("Adding patient's program name info to program_name_history...");
        try {
            for (PatientProgramName enrollProgram : enrollPrograms) {
                //ProgramNameHistory programNameHistory = new ProgramNameHistory();
                ProgramNameHistory programNameHistory = new ProgramNameHistory();
                programNameHistory.setCommunityId(loginResult.getCommunityId());
                logger.info("add/update:" + enrollProgram.getPatientProgramNameId());
                programNameHistory.setPatientProgramNameId(enrollProgram.getPatientProgramNameId());

                programNameHistory.setPatientId(personDTO.getPatientEnrollment().getPatientId());
                long progId = enrollProgram.getProgramId();
                ProgramName programName = patientProgramDAO.getProgramName(progId, loginResult.getCommunityId());
                programNameHistory.setCurrentProgramName(programName.getValue());

                // set program effective date
                if (enrollProgram.getProgramEffectiveDate() != null) {
                    programNameHistory.setProgramEffectiveDate(enrollProgram.getProgramEffectiveDate());
                }

                // set program end date
                if (enrollProgram.getProgramEndDate() != null) {
                    programNameHistory.setProgramEndDate(enrollProgram.getProgramEndDate());
                }
                programNameHistory.setUserId(loginResult.getUserId());
                programNameHistory.setAction("Updated Patient");

                programNameHistory.setDeleteReason(null);

                //change for JIRA 1036 - Vignesh T
                programNameHistory.setStatus(enrollProgram.getPatientStatus());
                programNameHistory.setStatusEffectiveDate(enrollProgram.getStatusEffectiveDate());
                programNameHistory.setParentProgramName(enrollProgram.getHealthHome());
                programNameHistory.setProgramEndReason(enrollProgram.getTerminationReason());

                // adding to DAO
                programNameHistoryDAO.addToProgramNameHistory(programNameHistory);
            }
        } catch (Exception exc) {
            final String message = "Error adding programs to program name history  patient id=" + personDTO.getPatientEnrollment().getPatientId();
            logger.logp(Level.SEVERE, getClass().getName(), "addPatientPrograms", message, exc);

            throw new PortalException(message);
        }
    }

    private void addDeletionReasonToProgramNameHistory(List<PatientProgramName> enrollPrograms, PersonDTO personDTO, LoginResult loginResult) throws PortalException {
        logger.info("Adding patient's program name info to program_name_history...");
        try {
            for (PatientProgramName enrollProgram : enrollPrograms) {
                //ProgramNameHistory programNameHistory = new ProgramNameHistory();
                ProgramNameHistory programNameHistory = new ProgramNameHistory();
                programNameHistory.setCommunityId(loginResult.getCommunityId());
                programNameHistory.setPatientProgramNameId(enrollProgram.getPatientProgramNameId());

                programNameHistory.setPatientId(personDTO.getPatientEnrollment().getPatientId());
                long progId = enrollProgram.getProgramId();
                ProgramName programName = patientProgramDAO.getProgramName(progId, loginResult.getCommunityId());
                programNameHistory.setCurrentProgramName(programName.getValue());

                // set program effective date
                if (enrollProgram.getProgramEffectiveDate() != null) {
                    programNameHistory.setProgramEffectiveDate(enrollProgram.getProgramEffectiveDate());
                }

                // set program end date
                if (enrollProgram.getProgramEndDate() != null) {
                    programNameHistory.setProgramEndDate(enrollProgram.getProgramEndDate());
                }
                programNameHistory.setUserId(loginResult.getUserId());
                programNameHistory.setAction("Updated Patient");

                programNameHistory.setDeleteReason(personDTO.getProgramDeleteReason());

                programNameHistory.setStatusEffectiveDate(enrollProgram.getStatusEffectiveDate());
                programNameHistory.setProgramEndReason(enrollProgram.getTerminationReason());
                programNameHistory.setParentProgramName(enrollProgram.getHealthHome());
                // adding to DAO
                programNameHistoryDAO.addToProgramNameHistory(programNameHistory);
            }
        } catch (Exception exc) {
            final String message = "Error adding programs to program name history  patient id=" + personDTO.getPatientEnrollment().getPatientId();
            logger.logp(Level.SEVERE, getClass().getName(), "addPatientPrograms", message, exc);

            throw new PortalException(message);
        }
    }

    @Override
    public void addPatientProgramFromPatientLoader(LoginResult loginResult, PatientProgramDTO patientProgramDTO, PersonDTO personDTO) throws PortalException {

        // put our dto in a list
        ArrayList<PatientProgramDTO> dtos = new ArrayList<PatientProgramDTO>();
        dtos.add(patientProgramDTO);

        addPatientProgramHelper(loginResult, dtos, personDTO);
    }

    @Override
    public void addPatientProgram(List<PatientProgramDTO> dtos, PersonDTO personDTO) throws PortalException {

        LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());
        addPatientProgramHelper(loginResult, dtos, personDTO);
    }


    private void addPatientProgramHelper(LoginResult loginResult, List<PatientProgramDTO> dtos, PersonDTO personDTO) throws PortalException {
        boolean addToHistoryFlag = false;
        long communityId = loginResult.getCommunityId();
        int duplicateCount=0;
        //Spira 7467
        boolean isStatusEffDateEditable = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "mpp.status.effective.date.editable"));
        
        // create enrollment service helper
        EnrollmentServiceHelper enrollmentServiceHelper = buildEnrollmentServiceHelper();

        try {
            List<PatientProgramDTO> recordsToUpdate = new ArrayList<PatientProgramDTO>();
            List<PatientProgramDTO> newRecords = new ArrayList<PatientProgramDTO>();
//            List<PatientProgramDTO>  recordsToDelete=new ArrayList<PatientProgramDTO>();

            boolean isDateChangedInUpdateByUser = false;
            for (PatientProgramDTO programDTO : dtos) {
                if (programDTO.getOperation().equalsIgnoreCase(UPDATE)) {
                    PatientProgramName existingRecord = patientProgramDAO.findEntity(personDTO.getPatientEnrollment().getPatientId(), programDTO.getPatientProgramNameId(), communityId);
                    if (programDTO.getStatusEffectiveDate() == null) {
                        programDTO.setStatusEffectiveDate(new Date());
                    }
                    //Spira 7467
                    isDateChangedInUpdateByUser = !DateUtils.equalsDatePortionOnly(existingRecord.getStatusEffectiveDate(), 
                                programDTO.getStatusEffectiveDate());
                    if((existingRecord.getPatientStatus() != programDTO.getPatientStatus()) 
                            && !isDateChangedInUpdateByUser) {
                        //date has to be set to current date if status is changed and date is not changed by user
                        programDTO.setStatusEffectiveDate(new Date());
                    }                    
                    recordsToUpdate.add(programDTO);

                } else if (programDTO.getOperation().equalsIgnoreCase(CREATE)) {
                    if (programDTO.getStatusEffectiveDate() == null) {
                        programDTO.setStatusEffectiveDate(new Date());
                    }
                    newRecords.add(programDTO);
                }
            }
            //Spira 7467
            //new records
            if(isStatusEffDateEditable) {
                validateForStatusEffectiveDate(newRecords, communityId);
            }
            //updated records
            if(isStatusEffDateEditable && isDateChangedInUpdateByUser) {
                validateForStatusEffectiveDate(recordsToUpdate, communityId);
            }
            
            logger.info("converting dtos...");

            List<PatientProgramName> updateList = convertFromDtos(recordsToUpdate);
            List<PatientProgramName> createList = convertFromDtos(newRecords);


            if (!createList.isEmpty()) {

                // check for duplicate program enrollment
                for (PatientProgramName prog : createList) {
                    long progId = prog.getProgramId();
                    List<PatientProgramName> duplicateEntity = patientProgramDAO.findDuplicateProgram(personDTO.getPatientEnrollment().getPatientId(), progId, communityId);
                    for(PatientProgramName ppn:duplicateEntity){
                        if(ppn.getTerminationReason()==null){
                            duplicateCount ++;
                        }
                    }

//             checkDuplicateProgramEnrollment(duplicateEntity,communityId);
                    if ((duplicateEntity != null)&& (duplicateCount==1)) {

                        ProgramName progName = patientProgramDAO.getProgramName(progId, communityId);
                        String dupProgMsg = Constants.DUPLICATE_PROGRAM_MSG + progName.getValue();
                        throw new DuplicateException(dupProgMsg);
                    }

                }

                logger.info("saving new patient programs list...");
                createList = patientProgramDAO.savePatientProgramList(personDTO.getPatientEnrollment().getPatientId(), createList);

                  //update patient program list
                logger.info("sending patient DEMO update msg with new patient program info...");

                if (personDTO.getPatientEnrollment().getPatientProgramName() == null) {
                        personDTO.getPatientEnrollment().setPatientProgramName(new ArrayList<PatientProgramDTO>());
                }
                
                personDTO.getPatientEnrollment().getPatientProgramName().addAll(newRecords);
                 // run notification on background thread for patient updates
                enrollmentServiceHelper.sendPatientProgramInfoToNotificationManager(personDTO, personDTO.getPatientEnrollment().getCommunityId());

                logger.info("adding to program name history");
                addToProgramNameHistory(createList, personDTO, loginResult);

            }

            if (!updateList.isEmpty()) {
                List<PatientProgramName> addToHistoryList = new ArrayList<PatientProgramName>();


                //check to update to programNameHistory
                for (PatientProgramName patientProgName : updateList) {

                    PatientProgramName oldProgramEntity = patientProgramDAO.findEntity(personDTO.getPatientEnrollment().getPatientId(),patientProgName.getPatientProgramNameId(),communityId);

                    Date oldProgramStatusEffectiveDate = oldProgramEntity.getStatusEffectiveDate();
                    Date newProgramStatusEffectiveDate = patientProgName.getStatusEffectiveDate();
                    Date oldProgramEndDate = oldProgramEntity.getProgramEndDate();
                    Date newProgramEndDate = patientProgName.getProgramEndDate();


                    if (oldProgramEntity.getProgramId() == patientProgName.getProgramId()) {

                        if (StringUtils.isNotBlank(patientProgName.getHealthHome()) && !(StringUtils.equalsIgnoreCase(patientProgName.getHealthHome(), oldProgramEntity.getHealthHome()))) {

                            logger.info("Old healthhome name is " + oldProgramEntity.getHealthHome());
                            logger.info("New healthhome name is " + patientProgName.getHealthHome());
                            addToHistoryFlag = true;
                        } else if (StringUtils.isBlank(oldProgramEntity.getHealthHome())) {
                            logger.info("New HealthHome is " + patientProgName.getHealthHome());
                            addToHistoryFlag = true;
                        } else if ((newProgramStatusEffectiveDate != null && oldProgramStatusEffectiveDate != null) && !(newProgramStatusEffectiveDate.equals(oldProgramStatusEffectiveDate))) {
                            logger.info("Adding to program name history due to change in Program Status Effective Date");
                            addToHistoryFlag = true;
                        }
                        // JIRA - 2784 Satyendra
                        // No need to capture all the events to program_name_history only events related
                        // to specific fields are captured for reporting purpose.

//                        else if ((newProgramEndDate != null && oldProgramEndDate != null) && ((newProgramEndDate.after(oldProgramEndDate)) || (oldProgramEndDate.after(newProgramEndDate)))) {
//                            logger.info("Adding to program name history due to change in Program End Date");
//                            addToHistoryFlag = true;
//                        }

//                        if ((newProgramEndDate == null && oldProgramEndDate != null) || (newProgramEffectiveDate == null && oldProgramEffectiveDate != null)) {
//                            logger.info("Adding to program name history due to changed in Program End/Effective Date");
//                            addToHistoryFlag = true;
//                        }

//                        if ((newProgramEndDate != null && oldProgramEndDate == null) || (newProgramEffectiveDate != null && oldProgramEffectiveDate == null)) {
//                            logger.info("Adding to program name history due to changed in Program End/Effective Date");
//                            addToHistoryFlag = true;
//                        }

                        //change for JIRA 1036 - Vignesh T
                        if(oldProgramEntity.getPatientStatus() != patientProgName.getPatientStatus()) {
                            logger.info("Adding to program name history due to change in Patient Status");
                            addToHistoryFlag = true;
                        }

                        //change for JIRA 1036 - Vignesh T
                        if((oldProgramEntity.getHealthHome() == null && patientProgName.getHealthHome() != null) ||
                        	(oldProgramEntity.getHealthHome() != null && patientProgName.getHealthHome() == null) ||
                        		(oldProgramEntity.getHealthHome() != null && !oldProgramEntity.getHealthHome().equalsIgnoreCase(patientProgName.getHealthHome()))
                        	) {
                            logger.info("Adding to program name history due to change in Parent Program Name");
                            addToHistoryFlag = true;
                        }

                        //change for JIRA 1036 - Vignesh T
//                        if(
//                        		(oldProgramEntity.getTerminationReason() == null && patientProgName.getTerminationReason() != null) ||
//                        			(oldProgramEntity.getTerminationReason() != null && patientProgName.getTerminationReason() == null) ||
//                        				(oldProgramEntity.getTerminationReason() != null && !oldProgramEntity.getTerminationReason().equalsIgnoreCase(patientProgName.getTerminationReason()))
//                        	) {
//                            logger.info("Adding to program name history due to change in Program End Reason");
//                            addToHistoryFlag = true;
//                        }
                    }

                    if (addToHistoryFlag) {

                        addToHistoryList.add(patientProgName);
                    }
                }
                logger.info("saving updated patient programs list...");
                patientProgramDAO.updatePatientProgramList(updateList);

                //update patient program list
                logger.info("reading back full updated patient program list");
                List<PatientProgramName> fullList = patientProgramDAO.findPatientProgramsList(personDTO.getPatientEnrollment().getPatientId(),
                         personDTO.getPatientEnrollment().getCommunityId());
                List<PatientProgramDTO> fullListDTO = convert(fullList, personDTO.getPatientEnrollment().getPatientId());
                logger.info("sending patient DEMO update msg with updated patient program info...");
                personDTO.getPatientEnrollment().getPatientProgramName().clear();
                personDTO.getPatientEnrollment().getPatientProgramName().addAll(fullListDTO); //DTO

                 // run notification on background thread for patient updates
                enrollmentServiceHelper.sendPatientProgramInfoToNotificationManager(personDTO, personDTO.getPatientEnrollment().getCommunityId());

                if (addToHistoryFlag) {

                    logger.info("adding to program name history...");
                    addToProgramNameHistory(addToHistoryList, personDTO, loginResult);
                }
                addToHistoryFlag=false;
            }


        } catch (DuplicateException exc) {
            final String message = exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "addPatientPrograms", message, exc);
            throw new PortalException(message);
        } catch (ValidationException exc) {
            final String message = exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "addPatientPrograms", message, exc);
            throw new PortalException(message);
        } catch (Exception exc) {
            final String message = "Error saving patient programs  patient id=" + personDTO.getPatientEnrollment().getPatientId();
            logger.logp(Level.SEVERE, getClass().getName(), "addPatientPrograms", message, exc);

            throw new PortalException(message);
        }
    }
    
    private void validateForStatusEffectiveDate(List<PatientProgramDTO> dtoList, long communityId) throws ValidationException {
        //Spira 7467
        logger.info("validating for status effective date");
        Date now = Calendar.getInstance().getTime();
        for (PatientProgramDTO dto : dtoList) {
            final String healthHome = dto.getHealthHome();
            final Date newStatusEffectiveDate = dto.getStatusEffectiveDate();
            if(DateUtils.compareDatePortionOnly(newStatusEffectiveDate, now) > 0) {
                //error
                final String message = "Status effective date cannot be greater than current date for:" + healthHome;
                logger.logp(Level.SEVERE, getClass().getName(), "validateForStatusEffectiveDate", message);
                throw new ValidationException(message);
            }
            final long currentStatus = dto.getPatientStatus();

            List<Long> statusList = programNameHistoryDAO.getDistinctStatusListExceptCurrent(
                    dto.getPatientId(), dto.getProgramId(), currentStatus, communityId);
            TreeSet<Date> statusDates = new TreeSet<>();
            for (Long st : statusList) {
                Date dateForStatus = programNameHistoryDAO.getMostRecentStatusEffectiveDateForStatus(dto.getPatientId(), dto.getProgramId(), st, communityId);
                logger.info("status:" + currentStatus + ", date:" + dateForStatus);
                statusDates.add(dateForStatus);
            }
            Date maxDate = null;
            if(!statusDates.isEmpty()) {
                maxDate = statusDates.last();
            }
            logger.info(healthHome + ":" + maxDate);
            if(null != maxDate && DateUtils.compareDatePortionOnly(newStatusEffectiveDate, maxDate) < 0) {
                final String message = "Status effective date cannot be lesser than highest of status effective date in history for:" + healthHome;
                logger.logp(Level.SEVERE, getClass().getName(), "validateForStatusEffectiveDate", message);
                throw new ValidationException(message);                
            }
        }
    }

    private EnrollmentServiceHelper buildEnrollmentServiceHelper() {
        NotificationUtils.setCommunityDAO(communityDAO);
        //JIRA 629 - remove properties
        EnrollmentServiceHelper enrollmentServiceHelper = new EnrollmentServiceHelper(patientManager,
                notificationAuditDAO, organizationPatientConsentDAO,
                patientEnrollmentDAO, organizationDAO);

        return enrollmentServiceHelper;
    }
}
