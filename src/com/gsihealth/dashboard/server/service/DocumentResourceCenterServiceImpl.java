package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.service.DocumentResourceCenterService;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.entity.DocumentResourceCenter;
import com.gsihealth.dashboard.entity.dto.DocumentResourceCenterDTO;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.dashboard.server.dao.DocumentResourceCenterDAO;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

/**
 *
 * @author User
 */
public class DocumentResourceCenterServiceImpl extends BasePortalServiceServlet implements DocumentResourceCenterService {

    private Logger logger = Logger.getLogger(getClass().getName());
    private ServletContext application;
    private DocumentResourceCenterDAO docResourceCenterDAO;
    private final long BYTES_PER_MB = 1048576;
    private long maxFileSizeInBytes;

    @Override
    public void init() throws ServletException {
        super.init();
        application = getServletContext();
        docResourceCenterDAO = (DocumentResourceCenterDAO) application.getAttribute(WebConstants.DOCUMENT_RESOURCE_CENTER_KEY);
    }

    public List<DocumentResourceCenterDTO> getDocuments(long communityId) throws PortalException {

        List<DocumentResourceCenterDTO> docDTO = new ArrayList<DocumentResourceCenterDTO>();

        try {
            List<DocumentResourceCenter> docList = docResourceCenterDAO.findDocumentEntities(communityId);

            ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

            // read property for max file, default to 10MB
            String maxFileSizeStr = configurationDAO.getProperty(communityId, "document.resource.center.max.file.size.in.mb", "10");

            // convert MB to bytes
            maxFileSizeInBytes = Long.parseLong(maxFileSizeStr) * BYTES_PER_MB;

            logger.info("document.resource.center.max.file.size.in.mb=" + maxFileSizeStr);
            logger.info("maxFileSizeInBytes=" + maxFileSizeInBytes);

            Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
            DocumentResourceCenterDTO documentResourceCenterDTO = null;

            for (DocumentResourceCenter doc : docList) {
                logger.info("doc Path: " + doc.getPathLinkToFile());

                // skip, if this is an invalid file
                if (isInvalidFile(doc)) {
                    continue;
                }

                // convert entity to DTO
                documentResourceCenterDTO = (DocumentResourceCenterDTO) mapper.map(doc, DocumentResourceCenterDTO.class);

                // add to the list
                docDTO.add(documentResourceCenterDTO);
            }

        } catch (Exception exc) {
            final String message = "Error retrieving documents";
            logger.logp(Level.SEVERE, getClass().getName(), "getDocuments", message, exc);
            throw new PortalException("Error retrieving documents");
        }

        return docDTO;

    }

    /**
     * Check if this is invalid file
     *
     * @param doc
     * @return true if file to large or file does not exist
     */
    private boolean isInvalidFile(DocumentResourceCenter doc) {

        File theFile = new File(doc.getPathLinkToFile());

        boolean fileNotExists = !theFile.exists();
        boolean fileTooLarge = theFile.length() >= maxFileSizeInBytes;
        boolean emptyFile = theFile.length() == 0;

        boolean isInvalid = fileNotExists || fileTooLarge || emptyFile;

        logger.info("isInvalidFile: " + doc.getPathLinkToFile() + ",  fileNotExists=" + fileNotExists + ",  fileTooLarge=" + fileTooLarge + ", emptyFile=" + emptyFile + ", isInvalid=" + isInvalid);

        return isInvalid;
    }
}