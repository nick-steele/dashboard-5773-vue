package com.gsihealth.dashboard.server.service;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.gsihealth.dashboard.client.service.PayerPlanAdminService;
import com.gsihealth.dashboard.common.PayerPlanSearchCriteria;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;
import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import com.gsihealth.entity.insurance.PayerPlan;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.common.DuplicateException;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.dao.Constants;
import com.gsihealth.dashboard.server.dao.OrganizationDAO;
import com.gsihealth.dashboard.server.dao.PatientEnrollmentDAO;
import com.gsihealth.dashboard.server.dao.PayerPlanDAO;
import com.gsihealth.dashboard.server.dao.util.PayerPlanDAOUtils;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

/**
 *
 * @author Chad Darby
 */
public class PayerPlanAdminServiceImpl extends RemoteServiceServlet implements PayerPlanAdminService {

    private Logger logger = Logger.getLogger(getClass().getName());
    private PayerPlanDAO payerPlanDAO;
    private OrganizationDAO organizationDAO;
    private PatientManager patientManager;
    private PatientEnrollmentDAO patientEnrollmentDAO;

    /**
     * Initializes the service with DAOs
     */
    @Override
    public void init() {

        ServletContext application = getServletContext();

        payerPlanDAO = (PayerPlanDAO) application.getAttribute(WebConstants.PAYER_PLAN_DAO_KEY);

        // Retrieve the patient manager. it was created in the DataContextListener class and stored in the servlet context
        patientManager = (PatientManager) application.getAttribute(WebConstants.PATIENT_MANAGER_KEY);

        patientEnrollmentDAO = (PatientEnrollmentDAO) application.getAttribute(WebConstants.PATIENT_ENROLLMENT_DAO_KEY);
        organizationDAO = (OrganizationDAO) application.getAttribute(WebConstants.ORGANIZATION_DAO_KEY);
    }

    @Override
    public SearchResults<PayerPlanDTO> findPayerPlans(int pageNumber, int pageSize) throws PortalException {
        try {
            int totalCount = payerPlanDAO.getPayerPlanTotalCount();

            // get data from db
            List<PayerPlan> payerPlans = payerPlanDAO.findPayerPlanEntities(pageNumber, pageSize);

            // convert entity objects to dtos
            List<PayerPlanDTO> payerPlansDTOs = convert(payerPlans);

            // build search results
            SearchResults<PayerPlanDTO> searchResults = new SearchResults<PayerPlanDTO>(totalCount, payerPlansDTOs);

            return searchResults;

        } catch (Exception exc) {
            final String message = "Error searching for payer plans.";
            logger.logp(Level.SEVERE, getClass().getName(), "findPayerPlans", message, exc);

            throw new PortalException(message);
        }
    }

    @Override
    public int getTotalCount() throws PortalException {
        int totalCountForCandidates = payerPlanDAO.getPayerPlanTotalCount();

        return totalCountForCandidates;
    }

    /**
     * Convert entity objects to DTOs
     * 
     * @param payerPlans
     * @return 
     */
    private List<PayerPlanDTO> convert(List<PayerPlan> payerPlans) {

        List<PayerPlanDTO> results = new ArrayList<PayerPlanDTO>();
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        for (PayerPlan tempPayerPlan : payerPlans) {
            PayerPlanDTO payerPlanDto = mapper.map(tempPayerPlan, PayerPlanDTO.class);
            results.add(payerPlanDto);
        }

        return results;
    }

    @Override
    /**
     * Map<planId, planName>
     */
    public LinkedHashMap<String, String> getPayerPlans() throws PortalException {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

        try {
            List<PayerPlan> payerPlans = payerPlanDAO.findPayerPlanEntities();

            for (PayerPlan temp : payerPlans) {
                String id = Long.toString(temp.getPayerPlanPK().getId());
                String name = temp.getName();
                data.put(id, name);
            }
            
            // move "Other" to the end;
            PayerPlanDAOUtils.movePayerPlanToEnd(data, Constants.OTHER_PAYER_PLAN_NAME);
            
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error loading payer plans.", exc);
            throw new PortalException(exc.getMessage());
        }

        return data;
    }

    @Override
    public int getTotalCount(PayerPlanSearchCriteria searchCriteria) throws PortalException {

        int totalCount = 0;

        try {
            totalCount = payerPlanDAO.getPayerPlanTotalCount(searchCriteria);

        } catch (Exception exc) {
            final String message = "Error get total count for payer plans";
            logger.logp(Level.SEVERE, getClass().getName(), "getTotalCount", message, exc);
        } finally {
            return totalCount;
        }
    }

    @Override
    public SearchResults<PayerPlanDTO> findPayerPlans(PayerPlanSearchCriteria searchCriteria, int pageNumber, int pageSize) throws PortalException {
        SearchResults<PayerPlanDTO> searchResults = null;

        try {
            // search for data
            List<PayerPlan> payerPlans = payerPlanDAO.findPayerPlanEntities(searchCriteria, pageNumber, pageSize);

            // convert entity to daos
            List<PayerPlanDTO> dtos = convert(payerPlans);

            // get the count
            int totalCount = payerPlanDAO.getPayerPlanTotalCount(searchCriteria);

            // build results
            searchResults = new SearchResults<PayerPlanDTO>(totalCount, dtos);

        } catch (Exception exc) {
            final String message = "Error searching for payer plans";
            logger.logp(Level.SEVERE, getClass().getName(), "findPayerPlans", message, exc);
        } finally {
            return searchResults;
        }
    }

    @Override
    public void addPayerPlan(PayerPlanDTO payerPlanDTO) throws PortalException {

        try {
            Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
            PayerPlan payerPlan = mapper.map(payerPlanDTO, PayerPlan.class);

            // check for duplicates
            if (payerPlanDAO.isDuplicate(payerPlan)) {
                throw new DuplicateException("Error. This has duplicate payer plan information: Payer Plan Name and/or MMIS ID.");
            }

            // add payer plan
            payerPlanDAO.add(payerPlan);

        } catch (DuplicateException exc) {
            logger.log(Level.SEVERE, "Duplicate payer plan.", exc);
            throw new PortalException(exc.getMessage());
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error adding payer plan.", exc);
            throw new PortalException(exc.getMessage());
        }
    }

    @Override
    public void updatePayerPlan(PayerPlanDTO payerPlanDTO) throws PortalException {

        try {
            Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
            PayerPlan payerPlan = mapper.map(payerPlanDTO, PayerPlan.class);

            // check for duplicates
            if (payerPlanDAO.isDuplicate(payerPlan)) {
                throw new DuplicateException("Error. This has duplicate payer plan information: Payer Plan Name and/or MMIS ID.");
            }

            // add payer plan
            payerPlanDAO.update(payerPlan);

        } catch (DuplicateException exc) {
            logger.log(Level.SEVERE, "Duplicate payer plan.", exc);
            throw new PortalException(exc.getMessage());
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error updating payer plan.", exc);
            throw new PortalException(exc.getMessage());
        }
    }

    @Override
    public boolean hasPatientsAssignedToThisPayerPlan(PayerPlanDTO payerPlanDTO) throws PortalException {
        try {
            Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
            PayerPlan payerPlan = mapper.map(payerPlanDTO, PayerPlan.class);

            // find patients assigned to this payer plan
            return payerPlanDAO.hasPatientsAssignedToThisPayerPlan(payerPlan);
            
        } catch (Exception exc) {
            String message = "Error finding patients assigned to this payer plan.";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }
    }

    @Override
    public void deletePayerPlan(PayerPlanDTO payerPlanDTO) throws PortalException {
        try {
            Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
            PayerPlan payerPlan = mapper.map(payerPlanDTO, PayerPlan.class);

            // delete payer plan
            payerPlanDAO.deletePayerPlan(payerPlan);

        } catch (Exception exc) {
            String message = "Error deleting payer plan.";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }
    }

    /**
     * Convert entity objects to DTOs
     * 
     * @param patients
     * @return 
     */
    private List<PersonDTO> convertPatients(List<Person> patients) {
        List<PersonDTO> dtos = new ArrayList<PersonDTO>();

        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        for (Person tempPerson : patients) {
            PersonDTO personDto = mapper.map(tempPerson, PersonDTO.class);
            dtos.add(personDto);
        }

        return dtos;
    }

    private void loadOrganizationNames(List<PersonDTO> patients) {
        for (PersonDTO tempPerson : patients) {
            
            PatientEnrollmentDTO tempPatientEnrollment = tempPerson.getPatientEnrollment();
            
            // get organization name
            long patientOrgId = tempPatientEnrollment.getOrgId();
            String orgName = organizationDAO.getOrganizationName(patientOrgId);
            
            // set the organization name
            tempPatientEnrollment.setOrganizationName(orgName);
        }
    }
}
