package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.common.Person;

/**
 *
 * @author Chad.Darby
 */
public interface EnrollmentPatientUpdater {
    
    public void runUpdatePatientProcessesSynchronous(LoginResult loginResult, Person thePerson, PersonDTO personDTO, String oldEnrollmentStatus, String newCareTeamId, String oldCareTeamId, String oldProgramLevelConsentStatus, String programLevelConsentStatus, Person oldPerson, long oldOrganizationId) throws NumberFormatException, PortalException;

    public void synchMyPatientList(long patientId, long communityId) throws PortalException;
    
}
