package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.common.ConsentConstants;
import com.gsihealth.dashboard.common.Constants;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.ReferenceData;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchPatientResults;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.*;
import com.gsihealth.entity.*;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.dao.*;
import com.gsihealth.dashboard.server.service.integration.PatientUserNotification;
import com.gsihealth.dashboard.server.util.AuditUtils;
import com.gsihealth.dashboard.server.util.EnrollmentUtils;
import com.gsihealth.dashboard.server.util.PropertyUtils;
import com.gsihealth.entity.UserPatient;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.TransformerUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.dozer.MappingException;

/**
 * Collection of base functionality for patients
 *
 * @author Chad Darby
 */
public class BasePatientService extends BasePortalServiceServlet {
    private Logger logger = Logger.getLogger(getClass().getName());
    
    protected AuditDAO auditDAO;
    protected ReportDAO reportDAO;
    protected CommunityCareteamDAO careteamDAO;
    protected Comparator personDTOComparator;
    protected NonHealthHomeProviderDAO nonHealthHomeProviderDAO;
    protected NotificationAuditDAO notificationAuditDAO;
    protected OrganizationPatientConsentDAO organizationPatientConsentDAO;  
    protected PatientEnrollmentHistoryDAO patientEnrollmentHistoryDAO; 
    protected PatientUserNotification patientUserNotification;
    protected UserPatientConsentDAO userPatientConsentDAO;
    private final String MINIOR_CONSTANT="Minor";
    protected ConfigurationDAO configurationDAO;
    
    @Override
    public void init() throws ServletException {

        ServletContext application = getServletContext();
        auditDAO = (AuditDAO) application.getAttribute(WebConstants.AUDIT_DAO_KEY);

        userDAO = (UserDAO) application.getAttribute(WebConstants.USER_DAO_KEY);

        careteamDAO = (CommunityCareteamDAO) application.getAttribute(WebConstants.COMMUNITY_CARE_TEAM_DAO_KEY);

        configurationDAO = (ConfigurationDAO) application.getAttribute(WebConstants.CONFIGURATION_DAO_KEY);
        //dynamic Consent      
        organizationPatientConsentDAO = (OrganizationPatientConsentDAO) application.getAttribute(WebConstants.ORGANIZATION_PATIENT_CONSENT_DAO_KEY);
        userPatientConsentDAO = (UserPatientConsentDAO) application.getAttribute(WebConstants.USER_PATIENT_CONSENT_DAO_KEY);

        notificationAuditDAO = (NotificationAuditDAO) application.getAttribute(WebConstants.NOTIFICATION_AUDIT_DAO_KEY);

        organizationDAO = (OrganizationDAO) application.getAttribute(WebConstants.ORGANIZATION_DAO_KEY);

        patientUserNotification = (PatientUserNotification) application.getAttribute(WebConstants.PATIENT_USER_NOTIFICATION);

        nonHealthHomeProviderDAO = (NonHealthHomeProviderDAO) application.getAttribute(WebConstants.NON_HEALTH_HOME_PROVIDER_DAO_KEY);
 
        patientEnrollmentHistoryDAO = (PatientEnrollmentHistoryDAO)application.getAttribute(WebConstants.PATIENT_ENROLLMENT_HISTORY_DAO_KEY);
        
        reportDAO = (ReportDAO) application.getAttribute(WebConstants.REPORT_DAO_KEY);       
        super.init();
    }


    public void setAuditDAO(AuditDAO auditDAO) {
        this.auditDAO = auditDAO;
    }

    public void setCareteamDAO(CommunityCareteamDAO careteamDAO) {
        this.careteamDAO = careteamDAO;
    }

    public void setNotificationAuditDAO(NotificationAuditDAO notificationAuditDAO) {
        this.notificationAuditDAO = notificationAuditDAO;
    }

    protected void audit(Person person, String comment, String auditType, LoginResult loginResult, String actionSuccess, String sendToOid) {

        long communityId=loginResult.getCommunityId();
        logger.info("BasePatientService Audit CommunityId *###"+communityId);
        try {
            // audit
            Audit audit = AuditUtils.convertPerson(loginResult, person);
            audit.setActionComments(comment);
            audit.setActionType(auditType);
            audit.setActionSuccess(actionSuccess);
            audit.setSentToOid(sendToOid);
           
            audit.setCommunityId(communityId);
            auditDAO.addAudit(audit);
        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error auditing for " + comment, exc);
        }
    }

    protected boolean isNewCareTeamAssigned(String newCareTeamIdStr, String oldCareTeamIdStr) {

        logger.info("isNewCareTeamAssigned: newCareTeamIdStr=" + newCareTeamIdStr);
        logger.info("isNewCareTeamAssigned: oldCareTeamIdStr=" + oldCareTeamIdStr);

        if (StringUtils.isBlank(newCareTeamIdStr)) {
            return false;
        }

        long newCareTeamId = Long.parseLong(newCareTeamIdStr);
        long oldCareTeamId = Long.parseLong(oldCareTeamIdStr);

        logger.info("isNewCareTeamAssigned: newCareTeamId=" + newCareTeamId);
        logger.info("isNewCareTeamAssigned: oldCareTeamId=" + oldCareTeamId);

        return (newCareTeamId > 0) && (newCareTeamId != oldCareTeamId);
    }

    protected void synchOrganizationPatientConsent(PatientEnrollmentDAO patientEnrollmentDAO, long patientId,long communityId,
            String programLevelConsentStatus, long patientOrg, String patientEnrollmentStatus, Long communiytOrgId) throws Exception {

        // get the orgs
        List<OrganizationPatientConsent> orgConsentList = organizationPatientConsentDAO.findOrganizationPatientConsentEntities(patientId,communityId);

        // convert to DTOs
        List<OrganizationPatientConsentDTO> orgConsentListDtos = OrganizationPatientConsentUtils.convert(orgConsentList);

        // update value for SWBHH based on PLCS TODO - whats this?
        for (OrganizationPatientConsentDTO temp : orgConsentListDtos) {
           
            boolean hasConsent = StringUtils.equalsIgnoreCase(programLevelConsentStatus, Constants.YES);
            long tempOrgId = temp.getOrganizationId();
            boolean isPatientsHomeOrg = (tempOrgId == patientOrg);
            logger.info("is this patients home org?: " + isPatientsHomeOrg );
            if (communiytOrgId!=null && tempOrgId == communiytOrgId) {
                //Add  OID  for Program Level Consent
                logger.info("this org is swb: ");
                if (hasConsent || isPatientsHomeOrg) {
                    logger.info("this org is swb: setting consent to permit for " + patientId);
                    temp.setConsentStatus(ConsentConstants.CONSENT_PERMIT);
                } else {
                    temp.setConsentStatus(ConsentConstants.CONSENT_DENY);
                }
            }
            //              else { // if consent is no or status is pending deny it - except for patient's home org
//                if((!hasConsent || StringUtils.equalsIgnoreCase(patientEnrollmentStatus, "Pending")) && !isPatientsHomeOrg){
//                    temp.setConsentStatus(ConsentConstants.CONSENT_DENY);
//                }
//            }
        }

        // Perform save        
        PatientCommunityEnrollment patientEnrollment = patientEnrollmentDAO.findPatientEnrollment(patientId);
        PatientConsentUtils.saveOrganizationPatientConsentList(organizationPatientConsentDAO, userPatientConsentDAO, orgConsentListDtos, patientEnrollment);
    }

    protected PatientRelationshipCode findPatientRelationship(long communityId, Long relationshipId) {

        Map<Long, String> patientRelationshipMap = getPatientRelationshipMap(communityId);

        PatientRelationshipCode patientRelationshipCode = new PatientRelationshipCode();
        PatientRelationshipCodePK patientRelationshipCodePK = new PatientRelationshipCodePK();
        patientRelationshipCodePK.setId(relationshipId);

        patientRelationshipCode.setPatientRelationshipCodePK(patientRelationshipCodePK);
        patientRelationshipCode.setValue(patientRelationshipMap.get(relationshipId));

        return patientRelationshipCode;
    }
    
    public SimplePerson getPatientById(Long communityId,Long patientId) throws Exception{
        return getPatientManager(communityId).findPatient(patientId);
    }

    protected void setPatientRelationship(long communityId, Person tempPerson, PersonDTO personDto) {
        //set relationship                                                      
        PatientEmergencyContact patientEmergencyContact = tempPerson.getPatientCommunityEnrollment().getPatientEmergencyContact();

        if (patientEmergencyContact != null) {

            if (patientEmergencyContact.getRelationship() != null) {

                PatientRelationshipCode patientRelationshipCode = findPatientRelationship(communityId, patientEmergencyContact.getRelationship());

                if ((patientRelationshipCode != null) && (StringUtils.isNotBlank(patientRelationshipCode.getValue()))) {

                    PatientEmergencyContactDTO patientEmergencyContactDto = personDto.getPatientEnrollment().getPatientEmergencyContact();
                    patientEmergencyContactDto.setPatientRelationshipDescription(patientRelationshipCode.getValue());
                    patientEmergencyContactDto.setPatientRelationshipId(patientRelationshipCode.getPatientRelationshipCodePK().getId());
                }
            }
        }
    }

    private Map<Long, String> getPatientRelationshipMap(Long communityId) {

        ServletContext application = getServletContext();

        // get reference data map of all community ids
        Map<Long, ReferenceData> referenceDataMap = ((Map<Long, ReferenceData>) application.getAttribute(WebConstants.REFERENCE_DATA_KEY));

        // get the reference data for this user's community id
        ReferenceData referenceData = (ReferenceData) referenceDataMap.get(communityId);

        return referenceData.getPatientRelationship();
    }

    // these methods are used by both enrollment and careteam, put them here
    /**
     * ok, lets stop the madness, One Ring to rule them all, One Ring to find
     * them, One Ring to bring them all and in the darkness bind them this
     * method returns a paginated list of patients AND sets the total patient
     * count It can be used for enrolled or non-enrolled patients - set the
     * search param power/non-power users - if currentUserOrgId is null, don't
     * set that search param, if searchCriteria is null, don't use it.
     *
     * @param communityId
     * @param searchCriteria
     * @param enrolledMode
     * @param currentUserOrgId
     * @param pageNumber
     * @param pageSize
     * @param userId
     * @return paginated list of patients meeting the contained params
     */
    protected SearchResults<PersonDTO> findGenericPatients(
            long communityId,
            SearchCriteria searchCriteria,
            boolean enrolledMode,
            Long currentUserOrgId,
            int pageNumber,
            int pageSize,
            long userId,
            boolean isMangeConsentWithoutEnrollment,
            boolean isAllOrgsSelfAssert,
            int maxPatientSearchResultsSize) throws PortalException {

        logger.info("lets find some patients - starting now ");
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        Integer limitPatientCount = maxPatientSearchResultsSize;

        String[] patientEuids = null;
        boolean hasDemographics = EnrollmentUtils.hasDemographicData(searchCriteria);
        // do we have any search criteria? TODO - got some code duplication between here and getPatientSearchTotalCount
        int patientCount = 0;
        List<PatientCommunityEnrollment> peList = null;
        List<PatientCommunityEnrollment> searchPatientList = null;
       
        if (searchCriteria != null) {
            logger.info("this search has specific criteria");
            if (hasDemographics) {
                patientEuids = findAndParsePatientMDMDemographicData(searchCriteria, communityId);
                limitPatientCount = null;
            }
        }

        Root<PatientCommunityEnrollment> pceRoot = getPatientEnrollmentDAO(communityId).getPatientCommunityEnrollmentRootForQuery();
        // let's generate the query
        Predicate searchExpression = getPatientEnrollmentDAO(communityId).criteriaBuilderForPatientSearch(pceRoot,
                searchCriteria, patientEuids, currentUserOrgId, communityId, enrolledMode, userId,isMangeConsentWithoutEnrollment,isAllOrgsSelfAssert);
        //out of network provider search   JIRA-
        /*if (searchCriteria != null && searchCriteria.getProviderType()!=null && searchCriteria.getProviderType().equalsIgnoreCase(Constants.OUT_PROVIDER) ){
            
                logger.info(" searchCriteria!=null..and also OutOfNetworkProviderId.");

                Predicate searchNHPPredicate = nonHealthHomeProviderDAO.criteriaBuilderForNHPPatientSearch(searchCriteria, communityId);
                peList = getPatientEnrollmentDAO(communityId).queryPatientEnrollmentBySearchExpression(searchExpression, limitPatientCount);

                List<NonHealthHomeProviderAssignedPatient> nhpList = nonHealthHomeProviderDAO.getNHPPatientSearchTotalCount(searchNHPPredicate, limitPatientCount);
                searchPatientList = getPatientEnrollmentList(peList, nhpList);
                patientCount=searchPatientList.size();
        
        }else {*/
            
            patientCount = getPatientEnrollmentDAO(communityId).getPatientSearchTotalCount(pceRoot, searchExpression, limitPatientCount);
            
            searchPatientList = getPatientEnrollmentDAO(communityId).queryPatientEnrollmentBySearchExpression(
                    searchExpression, pageNumber, pageSize, limitPatientCount);
            
        //}*/
        
        stopWatch.split();
        logger.fine("it took " + stopWatch.getSplitTime() / 1000.0 + "seconds to find " + patientCount 
                + " patients for user: " + userId + "\n patient list: " + searchPatientList);
       
        stopWatch.unsplit();
        Map<String, PatientCommunityEnrollment> pemap = this.convertPeListToMap(searchPatientList);
        logger.fine("Size of patientMap from pce = " + pemap.size());
        
        List<PersonDTO> personDTOs = null;
        try {
            //TODO some serious inefficiency going on here - going back to patientMDM db to get patient demo data
            personDTOs = this.convertPatientEnrollmentsToPersonDTOs(communityId, pemap,currentUserOrgId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "couldn't convert map to personDTO " , ex);
            throw new PortalException(ex.toString());
        }

        SearchResults<PersonDTO> searchResults = new SearchResults<PersonDTO>(patientCount, personDTOs);
        // determine if MDM limit exceeded
        boolean mdmMaxExceeded = patientCount >= maxPatientSearchResultsSize && hasDemographics;
        searchResults.setMdmMaxExceeded(mdmMaxExceeded);
        stopWatch.split();
        stopWatch.stop();
        logger.info("it took " + stopWatch.getSplitTime() / 1000.0 + " secs to map pce data back to patientMDM "
                + "and " + stopWatch.getTime() / 1000.0 + " secs to get results for complete patient search for user " + userId);
        
        return searchResults;
    }
    
        private SearchResults<PersonDTO> convertSearchResults(List<PatientCommunityEnrollment> searchPatientList,
                int patientCount,long communityId,long currentUserOrgId,int maxPatientSearchResultsSize,boolean hasDemographics,long userId) throws PortalException{
            
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
        Map<String, PatientCommunityEnrollment> pemap = this.convertPeListToMap(searchPatientList);
        logger.fine("Size of patientMap from pce = " + pemap.size());
        
        List<PersonDTO> personDTOs = null;
        try {
            //TODO some serious inefficiency going on here - going back to patientMDM db to get patient demo data
            personDTOs = this.convertPatientEnrollmentsToPersonDTOs(communityId, pemap,currentUserOrgId);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "couldn't convert map to personDTO " , ex);
            throw new PortalException(ex.toString());
        }
        
        SearchResults<PersonDTO> searchResults = new SearchResults<PersonDTO>(patientCount, personDTOs);
        // determine if MDM limit exceeded
        boolean mdmMaxExceeded = patientCount >= maxPatientSearchResultsSize && hasDemographics;
        searchResults.setMdmMaxExceeded(mdmMaxExceeded);
        stopWatch.split();
        stopWatch.stop();
        logger.info("it took " + stopWatch.getSplitTime() / 1000.0 + " secs to map pce data back to patientMDM "
                + "and " + stopWatch.getTime() / 1000.0 + " secs to get results for complete patient search for user " + userId);

        return searchResults;
        }
    
        protected SearchPatientResults countGenericPatients(long communityId, SearchCriteria searchCriteria, boolean enrolledMode, Long currentUserOrgId, long userId,
             boolean isMangeConsentWithoutEnrollment,boolean isAllOrgsSelfAssert,int maxPatientSearchResultsSize) throws PortalException {
        logger.info("performing a patient search for a patient count for user " + userId + " in org: " + currentUserOrgId+":communityId:"+communityId);
        String[] patientEuids = null;
        Predicate searchPredicate = null;
        int patientCount = 0;
        List<PatientCommunityEnrollment> searchPatientList = null;
        SearchResults<PersonDTO> searchResults = null;
        SearchPatientResults  patientSearchResults= new SearchPatientResults();
        Integer limitPatientCount = maxPatientSearchResultsSize;
        boolean isSearchingByDemographics = false;
        
        try {
            // do we have any search criteria?
            if (searchCriteria != null) {
                logger.info("this search has specific criteria");
                // any trips to mdm needed?
                isSearchingByDemographics = EnrollmentUtils.hasDemographicData(searchCriteria);
                if (isSearchingByDemographics) {
                    patientEuids = findAndParsePatientMDMDemographicData(searchCriteria, communityId);
                    limitPatientCount = null;
                }
            }

            Root<PatientCommunityEnrollment> pceRoot = getPatientEnrollmentDAO(communityId).getPatientCommunityEnrollmentRootForQuery();            
            // let's generate the query
            searchPredicate = getPatientEnrollmentDAO(communityId).criteriaBuilderForPatientSearch(pceRoot,
                    searchCriteria, patientEuids, currentUserOrgId, communityId, enrolledMode, userId,isMangeConsentWithoutEnrollment,isAllOrgsSelfAssert);


            /*if (searchCriteria != null && searchCriteria.getProviderType() != null && searchCriteria.getProviderType().equalsIgnoreCase(Constants.OUT_PROVIDER)) {
//                  if (searchCriteria.getProviderType().equalsIgnoreCase(Constants.OUT_PROVIDER)) {                           
                List<PatientCommunityEnrollment> peList = getPatientEnrollmentDAO(communityId).queryPatientEnrollmentBySearchExpression(
                        searchPredicate, limitPatientCount);

                //Predicate searchNHPPredicate = nonHealthHomeProviderDAO.criteriaBuilderForNHPPatientSearch(searchCriteria, communityId);
                //List<NonHealthHomeProviderAssignedPatient> nhpList = nonHealthHomeProviderDAO.getNHPPatientSearchTotalCount(searchNHPPredicate, limitPatientCount);
                //searchPatientList = getPatientEnrollmentList(peList, nhpList);
                patientCount = peList == null ? 0 : peList.size();
                
                patientSearchResults.setPatientCount(patientCount);
                logger.info("limitPatientCount######################"+limitPatientCount);
                searchResults = convertSearchResults(searchPatientList,patientCount,communityId,currentUserOrgId,maxPatientSearchResultsSize,
                        isSearchingByDemographics,userId);
                patientSearchResults.setSearchResults(searchResults);
                
            } else {*/

                patientCount = getPatientEnrollmentDAO(communityId).getPatientSearchTotalCount(
                        pceRoot, searchPredicate, limitPatientCount);
                patientSearchResults.setPatientCount(patientCount);
                logger.info("limitPatientCount @"+limitPatientCount);
            //}


        } catch (Exception e) {
            logger.log(Level.SEVERE, "bad things happened during countPatient " , e);
            throw new PortalException();
        }
        logger.info(patientCount + " found during patient search for a patient count for user " + userId);
        return patientSearchResults;
    }


    /**
     * use demographic criteria to get a list of patientMDM persons. TODO
     * ideally we would do all our PCE stuff and use the returned list of
     * patientEUIDs and demographic criteria to return a filtered list of
     * patientMDMs, but that ability doesn't exist in our patientMDM web service
     *
     * @param searchCriteria
     * @return array of patientEuids from patientMDM that have been filtered by
     * searchCriteria
     */
    protected String[] findAndParsePatientMDMDemographicData(SearchCriteria searchCriteria, Long communityId) {
        List<SimplePerson> patientsFromMDM;
        String[] patientEuids = null;
        try {
            logger.info("this search has demographics, lets do this the hard way");
            
            patientsFromMDM = getPatientManager(communityId).findPatient(searchCriteria);
            int patMDMSize = 0;
            if (patientsFromMDM != null) {
                patMDMSize = patientsFromMDM.size();
            }
            logger.info("number of patients from mdm search: " + patMDMSize);
            // build your searchCriteria query, if you have demographics, use the euids in a join
            patientEuids = this.parseEuidListFromMDMPatients(patientsFromMDM);
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "either no patients returned or you broke something: " , ex);
            patientsFromMDM = null;
        }
        return patientEuids;
    }

    protected String[] parseEuidListFromMDMPatients(List<SimplePerson> patientsFromMDM) {
        int listSize = patientsFromMDM.size();
        logger.info("parsing an mdm patient list with " + listSize + " patients in it");
        String[] patientEuids = new String[listSize];
        Collection<String> euidcollection = 
                CollectionUtils.collect(patientsFromMDM, TransformerUtils.invokerTransformer("getPatientEuid"));
        euidcollection.toArray(patientEuids);
        logger.info("finished parsing mdm patient list");
        return patientEuids;
    }

    /**
     * convert patientEnrollments to personDTOs, add any necessary mapped
     * children
     *
     * @param communityId
     * @param patientEnrollments
     * @return
     * @throws Exception
     */
    protected List<PersonDTO> convertPatientEnrollmentsToPersonDTOs(long communityId,
            Map<String, PatientCommunityEnrollment> patientEnrollments, Long currentUserOrgId) throws Exception {

        List<PersonDTO> searchResults = new ArrayList<PersonDTO>();
        List<String> patientIdsList = new ArrayList<>();
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
       int minorAge=Integer.parseInt(configurationDAO.getProperty(communityId, "minor.age", "18"));
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+minorAge);
        // get the patient enrollment
        Set<Map.Entry<String, PatientCommunityEnrollment>> entries = patientEnrollments.entrySet();
        if(patientEnrollments.keySet() != null ){
            patientIdsList.addAll(patientEnrollments.keySet());
        }
        String communityOid = this.getPatientEnrollmentDAO(communityId).getCommunityOid();
        logger.fine("communityOid for pce to dto convert: " + communityOid);
        logger.fine("Size of key Set = " + patientEnrollments.keySet());
        logger.info("Size of patientIdsList when add key set from map = " + patientIdsList.size());
        
        Map<String,SimplePerson> demographicDataOfPatients = reportDAO.getPatientsFromMdm(getPatientManager(communityId),patientIdsList, communityOid);
                logger.info("Size of map after call from mdm = " + demographicDataOfPatients.size());

        logger.fine("converting pce's to personDTO's, starting with " + patientEnrollments.size() + " pce's");

        for (Map.Entry<String, PatientCommunityEnrollment> tempEntry : entries) {
            String patientId = tempEntry.getKey();
            PatientCommunityEnrollment tempPatientEnrollment = tempEntry.getValue();
            
            try {
                if (tempPatientEnrollment != null) {
                    PersonDTO personDto = null;
                    if(demographicDataOfPatients.get(patientId)== null){
                        personDto = new PersonDTO();
                        personDto.setLocalId(patientId);
                        personDto.setNotFoundInMirth(true);
                        logger.fine("PATIENTID NOT IN MIRTH: " + patientId);
//                        throw new DemographicDataNotFoundException("Demographic data was not found for patient id " + patientId);
                    } else {

                        /** FIXME : Hitting DB calls in a loop can degrade performance.
                         * We can add a join in entity to fetch the data from other tables.
                         * So that we don't have to make extra calls in loop. This needs to be clean-up.
                         */
                        personDto = convertPersonToDto(demographicDataOfPatients.get(patientId), 
                                patientId, tempPatientEnrollment, mapper, communityId, minorAge, currentUserOrgId);
                    }
                    
                    if(personDto != null) {
                        searchResults.add(personDto);
                    }
                }
            } catch (Exception exc) {
                // something's wrong, skip this patient and move on to the next one
                String message = "Error converting pce to personDTO for patient id " + patientId;
                logger.log(Level.WARNING, message, exc);
            }
        }

        //Collections.sort(searchResults, personDTOComparator);
        logger.info("pce converted to personDTO searchResults size is " + searchResults.size());
        return searchResults;
    }

    public PersonDTO convertPersonToDto(SimplePerson sp, String patientId, 
            PatientCommunityEnrollment tempPatientEnrollment, Mapper mapper, long communityId, 
            int minorAge, Long currentUserOrgId) throws MappingException {
        Person tempPerson = new Person(sp);
        tempPerson.setPatientCommunityEnrollment(tempPatientEnrollment);
        
        // check if patient id is in patient_community_careteam
        //boolean exists = careteamDAO.doesPatientCommunityCareteamExist(tempPatientEnrollment.getPatientId());
        
        // if so, then get care team id
        /*if (exists) {
            CommunityCareteam communityCareteam = careteamDAO.findCommunityCareteamForPatient(tempPatientEnrollment.getPatientId());
            String careteamName = communityCareteam.getName();
            tempPerson.setCareteam(careteamName);
            logger.fine("patient " + patientId + " has a careteam " + careteamName + " - setting");
        }*/
        
        // get organization name
        long orgId = tempPatientEnrollment.getOrgId();
        String orgName = organizationDAO.getOrganizationName(orgId);
        tempPatientEnrollment.setOrganizationName(orgName);
        logger.fine("setting org " + orgName + " for " + patientId);
        PersonDTO personDto = null;
        if (tempPerson != null) {
            logger.fine("setting relationship, phh, enroll date for " + patientId);
            personDto = mapper.map(tempPerson, PersonDTO.class);
            //Spira 5438 : After conversion from Person to personDto setting date in string format.
            //String will not be changed after GWT RPC call. It will reach as it is on client.
            //Then it can be used on Patient Search Grid, Patient View and patient update.
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
            String dobAsString = df.format(tempPerson.getDateOfBirth());
            personDto.setDateOfBirthAsString(dobAsString);
            setPatientRelationship(communityId, tempPerson, personDto);
            
            //Program health home
            setProgramHealthHome(communityId, tempPerson, personDto);

            //set new Program
            setProgram(tempPatientEnrollment.getPatientProgramLinks(), personDto);

            //set new Careteam
            setCareteam(tempPatientEnrollment.getUserPatients(), personDto);
            
            // Enrollment status changed effective date
            Date enrollStatusChangeDate = patientEnrollmentHistoryDAO.getEnrollmentStatusChangeDate(personDto.getPatientEnrollment().getPatientId());
            personDto.setEnrollmentChangeDate(enrollStatusChangeDate);
            
            setPatientRelationship(communityId, tempPerson, personDto);
            setMinor(tempPerson,personDto,minorAge);
            // JIRA - 902 Self-assert access to patients
            if(currentUserOrgId!=null){
                setSelfAssertFlagForNonConsentedPatients(personDto,currentUserOrgId,communityId);
            }
        }
        return personDto;
    }



    protected PersonDTO loadPersonDTO(SimplePerson sp, long communityId, Long currentUserOrgId)
    {
        long patientId = Long.valueOf(sp.getLocalId());
        logger.info("patient id:" + patientId);

        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        int minorAge=Integer.parseInt(configurationDAO.getProperty(communityId, "minor.age", "18"));
        
        PatientCommunityEnrollment pce = getPatientEnrollmentDAO(communityId).findPatientEnrollment(
                patientId, communityId);
        return this.convertPersonToDto(sp, sp.getLocalId(), pce, mapper, communityId, minorAge, currentUserOrgId);
    }

    public void setSelfAssertFlagForNonConsentedPatients(PersonDTO personDto, long currentUserOrgId,long communityId){
        if (checkPatientOrgConsent(personDto,currentUserOrgId,communityId)){
            personDto.setSelfAssertEligible(true);
        }
    }
    public boolean checkPatientOrgConsent(PersonDTO personDto, long currentUserOrgId,long communityId){
        boolean notConsentedOrg=true;
        List<OrganizationPatientConsent> consentedList = organizationPatientConsentDAO.findOrganizationPatientConsentEntities(personDto.getPatientEnrollment().getPatientId(), communityId);
//         List<OrganizationPatientConsentDTO> consentedList=personDto.getPatientEnrollment().getConsentedOrgs();
        if(consentedList!=null){
            System.out.println("OPC List"+consentedList.size());
         for(OrganizationPatientConsent tempOpc:consentedList){
             if(tempOpc.getOrganizationPatientConsentPK().getOrganizationId()==currentUserOrgId){
                 notConsentedOrg=false;
                 break;
             }
         }}
        return notConsentedOrg;
    }
    
     public void setMinor(Person person,PersonDTO personDto,int minorAge){       	
                     if(PropertyUtils.isMinor(person.getDateOfBirth(),minorAge)){	
                                personDto.setMinor(MINIOR_CONSTANT);	
                            }	
                     else{	
                          personDto.setMinor("");	
                     }	
                }

    protected Map<String, PatientCommunityEnrollment> convertPeListToMap(List<PatientCommunityEnrollment> pelist) {
        Map<String, PatientCommunityEnrollment> map = new HashMap<String, PatientCommunityEnrollment>();
        logger.info(pelist.toString()+"pelist ");
        if (pelist != null) {
            for (PatientCommunityEnrollment each : pelist) {
                logger.info(each.getPatientId().toString()+"Converting pce ");
                map.put(each.getPatientId().toString(), each);
            }
        }
        logger.info(map.toString()+"pelist map");
        return map;
    }

    protected void setProgramHealthHome(long communityId,Person tempPerson, PersonDTO personDto) {

        if (tempPerson.getPatientCommunityEnrollment().getProgramHealthHomeId() != 0) {

            ProgramHealthHomeDTO programHealthHomeDTO = new ProgramHealthHomeDTO();
            ProgramHealthHome healthHome = getPatientEnrollmentDAO(communityId).getProgramHealthHome(communityId,tempPerson.getPatientCommunityEnrollment().getProgramHealthHomeId());
            programHealthHomeDTO.setHealthHomeName(healthHome.getHealthHomeValue());
            programHealthHomeDTO.setId(healthHome.getProgramHealthHomePK().getId());
            personDto.getPatientEnrollment().setProgramHealthHome(programHealthHomeDTO);

        }
    }

    private void setProgram(List<PatientProgramLink> patientProgramLinks, PersonDTO personDto) {

        if (patientProgramLinks == null ||
                patientProgramLinks.isEmpty()) return;
        List<PatientProgramLinkDTO> dtos = new ArrayList<PatientProgramLinkDTO>();
        Set<String> patientProgramSet = new HashSet<String>();
        for(PatientProgramLink ppl : patientProgramLinks){
            PatientProgramLinkDTO dto = new PatientProgramLinkDTO();
            //Not required now.
//            dto.setProgram(mapper.map(ppl.getProgram(), ProgramDTO.class));
            ProgramStatus status = ppl.getProgramStatus();
            String statusName = status.getName();
            dto.setProgramStatus(new ProgramStatusDTO(status.getId(), statusName));
            dtos.add(dto);
            // If it's a simpleSubordinate, we ignore the status and use it's parent's status
            if (!((ppl.getProgram().getParent() != null) && ppl.getProgram().getParent().getSimpleView())) {
                if (statusName != null) patientProgramSet.add(statusName.toUpperCase());
            }
        }
        personDto.getPatientEnrollment().setPatientProgramLinks(dtos);
        // If it's only a single status, we use that PatientStatus. Else, if there are multiple statuses, we use 'Various' as the status.
        if (!patientProgramSet.isEmpty()) {
            personDto.setPatientStatus(patientProgramSet.size() != 1 ? "Various" : patientProgramSet.iterator().next().toLowerCase());
        }
    }

    private void setCareteam(List<UserPatient> userPatients, PersonDTO personDto) {

        if (userPatients == null ||
                userPatients.isEmpty()) return;
        if(userPatients.get(0).getCommunityCareteamId() == null) {
            personDto.setCareteam(Constants.YES);
        }
        else {
            personDto.setCareteam(userPatients.get(0).getCommunityCareteam().getName());
        }
    }

    private List<PatientCommunityEnrollment> getPatientEnrollmentList(List<PatientCommunityEnrollment> peList, List<NonHealthHomeProviderAssignedPatient> nhpList) {

        List<PatientCommunityEnrollment> patientEnrollmentList = new ArrayList<PatientCommunityEnrollment>();

        for (NonHealthHomeProviderAssignedPatient nhhpap : nhpList) {
            for (PatientCommunityEnrollment enrollment : peList) {
                if ((enrollment.getPatientId() == nhhpap.getNonHealthHomeProviderAssignedPatientPK().getPatientId()) && (enrollment.getStatus().equalsIgnoreCase("Assigned"))) {
                    patientEnrollmentList.add(enrollment);

                }
            }
        }
        return patientEnrollmentList;
    } 

 }