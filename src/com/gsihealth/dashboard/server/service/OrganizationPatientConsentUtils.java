package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.common.ConsentConstants;
import com.gsihealth.entity.OrganizationPatientConsent;
import com.gsihealth.dashboard.entity.dto.OrganizationPatientConsentDTO;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

/**
 *
 * @author Chad Darby
 */
public class OrganizationPatientConsentUtils {
    
    /**
     * Convert entity objects to DTOs
     * 
     * @param providers
     * @return 
     */
    public static List<OrganizationPatientConsentDTO> convert(List<OrganizationPatientConsent> providers) {

        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        List<OrganizationPatientConsentDTO> orgDTOs = new ArrayList<OrganizationPatientConsentDTO>();

        for (OrganizationPatientConsent tempProvider : providers) {
            OrganizationPatientConsentDTO tempDTO = mapper.map(tempProvider, OrganizationPatientConsentDTO.class);
            orgDTOs.add(tempDTO);
        }

        return orgDTOs;
    }

}
