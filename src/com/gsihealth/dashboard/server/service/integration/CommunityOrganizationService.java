package com.gsihealth.dashboard.server.service.integration;

import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.entity.Community;
import com.gsihealth.entity.OrganizationCwd;
import com.gsihealth.entity.TemplateOrganizationCommunityApplication;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.dao.ApplicationDAO;
import com.gsihealth.dashboard.server.dao.CommunityDAO;
import com.gsihealth.dashboard.server.dao.CommunityOrganizationDAO;
import com.gsihealth.dashboard.server.dao.OrganizationDAO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author User
 */
public class CommunityOrganizationService {

    private Logger logger = Logger.getLogger(getClass().getName());
    private EntityManagerFactory emf = null;

    public CommunityOrganizationService() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public CommunityOrganizationService(EntityManagerFactory dashboardEmf) {
        this.emf = dashboardEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void createOrganization(CommunityOrganizationDAO communityOrganizationDAO,
            CommunityDAO communityDAO, OrganizationDAO orgDAO, OrganizationCwd org, long communityId) throws Exception {

        EntityManager em = null;
        Community community = null;

        try {
            em = getEntityManager();
            em.getTransaction().begin();

            //1. create Community 
            //Work around for communityId FIXME - why is this returning 0??
            //long communityId=getCommunityId();
            community = communityDAO.getCommunityById(communityId, em);
            community.getOrganizationCwds().add(org);
            em.merge(community);
            em.getTransaction().commit();

        } catch (Exception ex) {
            logger.log(Level.SEVERE, "error in service", ex);
            Throwable cause = ex;
            while (cause != null) {
                if (cause instanceof javax.validation.ConstraintViolationException) {
                    javax.validation.ConstraintViolationException cve = (javax.validation.ConstraintViolationException) cause;

                    for(javax.validation.ConstraintViolation violation : cve.getConstraintViolations()) {
                        logger.warning("violation error:" + violation.getMessage());
                        logger.warning(violation.getRootBeanClass() + " " + violation.getPropertyPath() + " " + violation.getMessage()
                                            + " " + violation.getRootBean() + ", invalid value: [" + violation.getInvalidValue() + "]");
                    }
                }
                cause = cause.getCause();                
            }
            em.getTransaction().rollback();
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Add the collection of template applications to the organization
     *
     * @param org
     */
    public void addTemplateApplications(ApplicationDAO applicationDAO, CommunityOrganizationDAO communityOrganizationDAO, OrganizationCwd org,
            Long communityId) throws Exception {
        logger.info("Adding template applications to the organization: orgId=" + org.getOrgId() + ", orgName=" + org.getOrganizationCwdName());

        //Work around for communityId
        logger.info("Community id: " + communityId );

        // get the template orgs the template_organization_community_application table
        List<TemplateOrganizationCommunityApplication> templateApps = applicationDAO.getTemplateOrganizationApplications(communityId);
//        List<TemplateOrganizationCommunityApplication> templateApps = applicationDAO.getTemplateOrganizationApplications(Constants.GSI_COMMUNITY_ID);

        // add them to the organization_community_application table
        long orgId = org.getOrgId();
        applicationDAO.assignAppsToOrganization(orgId, communityId, templateApps);

//        applicationDAO.assignAppsToOrganization(orgId, Constants.GSI_COMMUNITY_ID, templateApps);

        logger.info("Successfully added template applications to the organization.");
    }

}
