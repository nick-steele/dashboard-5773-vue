/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.service.integration;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;



/**
 * Performs Encryption and Decryption of Information
 * 
 * @author JavaDev
 * @version 1.0
 * @since April 2011
 * 
 * 
 */
public class Encryption {

	Cipher ecipher;
	Cipher dcipher;
        
        private static final String Encryption_Key="doNotChangeThis"; 

	/**
	 * parameterize constructor for encryptor file
	 * 
	 * @param key
	 * @param algorithm
	 */
	Encryption(SecretKey key, String algorithm) {
		try {
			ecipher = Cipher.getInstance(algorithm);
			dcipher = Cipher.getInstance(algorithm);
			ecipher.init(Cipher.ENCRYPT_MODE, key);
			dcipher.init(Cipher.DECRYPT_MODE, key);

		} catch (NoSuchPaddingException e) {
			System.out.println("EXCEPTION: NoSuchPaddingException");
		} catch (NoSuchAlgorithmException e) {
			System.out.println("EXCEPTION: NoSuchAlgorithmException");
		} catch (InvalidKeyException e) {
			System.out.println("EXCEPTION: InvalidKeyException");
		}
	}

	/**
	 * default constructor for encryptor file
	 * 
	 * @param key
	 * @param algorithm
	 */
	Encryption(String keys) {

		// 8-bytes Salt
		byte[] salt = { (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32,
				(byte) 0x56, (byte) 0x34, (byte) 0xE3, (byte) 0x03 };

		// Iteration count
		int iterationCount = 19;

		try {

			KeySpec keySpec = new PBEKeySpec(keys.toCharArray(), salt,
					iterationCount);
			SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES")
					.generateSecret(keySpec);

			ecipher = Cipher.getInstance(key.getAlgorithm());
			dcipher = Cipher.getInstance(key.getAlgorithm());
			AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt,
					iterationCount);

			ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
			dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);

		} catch (InvalidAlgorithmParameterException e) {
			System.out.println("EXCEPTION: InvalidAlgorithmParameterException");
		} catch (InvalidKeySpecException e) {
			System.out.println("EXCEPTION: InvalidKeySpecException");
		} catch (NoSuchPaddingException e) {
			System.out.println("EXCEPTION: NoSuchPaddingException");
		} catch (NoSuchAlgorithmException e) {
			System.out.println("EXCEPTION: NoSuchAlgorithmException");
		} catch (InvalidKeyException e) {
			System.out.println("EXCEPTION: InvalidKeyException");
		}
	}

	/**
	 * encrypting the input string
	 * 
	 * @param str
	 * @return encrypt string
	 */
	public String encrypt(String str) {
		try {
			// Encode the string into bytes using utf-8
			byte[] utf8 = str.getBytes("UTF8");

			// Encrypt
			byte[] enc = ecipher.doFinal(utf8);

			// Encode bytes to base64 to get a string
			return new sun.misc.BASE64Encoder().encode(enc);

		} catch (BadPaddingException e) {
		} catch (IllegalBlockSizeException e) {
		} catch (UnsupportedEncodingException e) {
		} catch (Exception e) {
		}
		return null;
	}

	/**
	 * decrypting the input string
	 * 
	 * @param str
	 * @return decrypt string
	 */
	public String decrypt(String str) {

		try {

			byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);

			byte[] utf8 = dcipher.doFinal(dec);

			return new String(utf8, "UTF8");

		} catch (BadPaddingException e) {
		} catch (IllegalBlockSizeException e) {
		} catch (UnsupportedEncodingException e) {
		} catch (IOException e) {
		}
		// Encryption Key used is not the same as the one used to encrypt first
		return "couldNotDecrypt";
	}

	/**
	 * encrypting the input string
	 * 
	 * @param str
	 * @return encrypt string
	 */
	public static synchronized String encryption(String str) {
		if (str == null || str.length() < 1)
			return "";

		String keys = Encryption_Key;
		Encryption citiXsysEncryptor = new Encryption(keys);
		return citiXsysEncryptor.encrypt(str);
	}

	/**
	 * decrypting the input string
	 * 
	 * @param str
	 * @return decrypt string
	 */
	public static synchronized String decryption(String str) {
		if (str == null || str.length() < 1)
			return "";

		String keys = Encryption_Key;
		if (keys == null) {
			throw new NullPointerException();
		}
		Encryption citiXsysEncryptor = new Encryption(keys);
		return citiXsysEncryptor.decrypt(str);

	}

	public static void main(String[] args) {
	//	System.out.println("1"+decryption("7Z9kohdld/g="));
		
	}

}
