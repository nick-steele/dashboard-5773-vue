package com.gsihealth.dashboard.server.service.integration;

import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.dashboard.server.dao.SSOUserDAO;
import com.gsihealth.dashboard.server.dao.UserCommunityOrganizationDAO;
import com.gsihealth.dashboard.server.dao.UserDAO;
import com.gsihealth.dashboard.server.hisp.HispClientUtils;
import com.gsihealth.dashboard.server.util.SSOUtils;
import com.gsihealth.entity.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.logging.Logger;

/**
 *
 * @author Vishal Kumar
 */
public class UserService {

    private Logger logger = Logger.getLogger(getClass().getName());
    private EntityManagerFactory emf = null;
    private SSOUtils ssoUtils = new SSOUtils();

    public UserService() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public UserService(EntityManagerFactory dashboardEmf) {
        this.emf = dashboardEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    //JIRA 629 - remove properties
    public void createUser(SSOUserDAO ssoUserDAO, UserCommunityOrganizationDAO userCommunityOrganizationDAO, UserDAO userDAO,
            OrganizationCwd organization, User user, String userPassword, CommunityOrganization communityOrganization, String tokenId, String status ) throws Exception {

        EntityManager em = null;

        try {
            em = getEntityManager();
            em.getTransaction().begin();

            //1. create user
            userDAO.create(user);
            Community community = communityOrganization.getCommunity();
            long communityId = community.getCommunityId();
            UserCommunityOrganization uco = user.getUserCommunityOrganization(community);
            
            // create user account on HISP/James 
            String email = user.getEmail();
            Long userId = user.getUserId();
            logger.info("Status to user before adding in CP: " + status);
            String directDomain = HispClientUtils.getDirectDomain(communityId);

            String directAddress = HispClientUtils.generateDirectAddress(email, directDomain);

            //Get OpenAM patient group
            String patientGroup = getOpenAmPatientGroup(communityId);

            StringBuilder ssoUser = ssoUtils.buildSsoUser(user, userPassword, directAddress, uco, organization, patientGroup, false);
            String encryptedPwd = ssoUserDAO.addUserToCP(communityId, tokenId, email, userId,status);
            if(userPassword.equals("")) {
                ssoUser.append(ssoUtils.getSsoStringForPassword(encryptedPwd));
            }
            ssoUserDAO.addUser(communityId, tokenId, ssoUser.toString());

            em.getTransaction().commit();

            logger.info("Successfully added user to OpenAM: " + user.getEmail());

        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public String getOpenAmPatientGroup(long communityId) {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        String patientGroup = configurationDAO.getProperty(communityId, "carebook.openam.patientgroup", "");

        return patientGroup;
    }

    /**
     * update user in userDAO (demog info) update sso user
     *
     * @param ssoUserDAO
     * @param userCommunityOrganizationDAO
     * @param userDAO
     * @param user
     * @param userPassword
     * @param communityOrganization
     * @param tokenId
     * @param organization
     * @throws Exception
     */
    //JIRA 629 - remove properties
    public String updateUser(SSOUserDAO ssoUserDAO, UserCommunityOrganizationDAO userCommunityOrganizationDAO, UserDAO userDAO,
            User user, String userPassword, CommunityOrganization communityOrganization, String tokenId, OrganizationCwd organization,boolean userExistsInAnotherCommunity, long otherCommunityId) throws Exception {

        EntityManager em = null;

        try {
            em = getEntityManager();
            em.getTransaction().begin(); 
            Community community = communityOrganization.getCommunity();
            Long communityId = community.getCommunityId();
            UserCommunityOrganization userCommunityOrganization = user.getUserCommunityOrganization(community);
            
            logger.info("uco in service update: " +userCommunityOrganization );
            // update user
            userDAO.update(user);

            //Update user Direct Address account on HISP/James
            String email = user.getEmail();
            Long userId = user.getUserId();
            String status = userCommunityOrganization.getStatus();
            String directDomain = HispClientUtils.getDirectDomain(communityId);
            String directAddress = HispClientUtils.generateDirectAddress(email, directDomain);

            //Get OpenAM patient group
            // SPIRA 4621 – leave patient group blank, 
            // so existing OpenAM groups will not be overwritten during update
            // OpenAM patientGroup will only be created createUser method
            String patientGroup = "";    //getOpenAmPatientGroup(communityId);

            //Build SSO User
            StringBuilder ssoUser = ssoUtils.buildSsoUser(user, userPassword, directAddress,
                    userCommunityOrganization, organization, patientGroup, userExistsInAnotherCommunity);

            //1.4 create user in Open AM SSO
            if (userExistsInAnotherCommunity) {
                logger.info("adding user in current community SSO");
                String encryptedPwd = ssoUserDAO.getEncryptedPassword(communityId, tokenId, email,userId,status);
                ssoUser.append(ssoUtils.getSsoStringForPassword(encryptedPwd));
                ssoUserDAO.addUser(communityId, tokenId, ssoUser.toString());
            }else{
                ssoUserDAO.updateUser(communityId, tokenId, ssoUser.toString());
            }
            //JIRA 1853
            String changePwdToken = null;
            if(!userPassword.equals("")) {
                changePwdToken = ssoUserDAO.changePassword(communityId, tokenId, email, userPassword);
                ssoUserDAO.unlockAccount(communityId, tokenId, email);
            }
            em.getTransaction().commit();

            logger.info("Successfully updated user: " + user.getEmail());
            
            return changePwdToken;

        } catch (Exception exc) {
            em.getTransaction().rollback();
            throw exc;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    
    public void createPatientUser(SSOUserDAO ssoUserDAO, UserCommunityOrganizationDAO userCommunityOrganizationDAO, UserDAO userDAO,
            OrganizationCwd organization, User user, String userPassword, CommunityOrganization communityOrganization, 
            String tokenId, String patientEngaRole, String directAddress, String patientUser) throws Exception {

        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>createPatientUser patientUser :" + patientUser + ":directAddress:" + directAddress);
        EntityManager em = null;

        try {
            em = getEntityManager();
            em.getTransaction().begin();

            //1.1 UserCommunityOrganization
            Community community = communityOrganization.getCommunity();
            Long communityId = community.getCommunityId();
            UserCommunityOrganization userCommunityOrganization = user.getUserCommunityOrganization(community);

            // create user account on HISP/James
            //  String email = user.getEmail();
            //String directDomain = HispClientUtils.getDirectDomain(props);
            //String directAddress = HispClientUtils.generateDirectAddress(email, directDomain);
            //String directAddress = HispClientUtils.generatePatientDirectAddress(patientUserName, directDomain,organization.getOrganizationCwdName());
            //Get OpenAM patient group
            String patientGroup = getOpenAmPatientGroup(communityId);
            logger.info("patientGroup >>>>>>>>>>>>>>>>: " + patientGroup);
            patientGroup = "";

            StringBuilder ssoUser = ssoUtils.buildSsoPatientUser(user, userPassword, directAddress, userCommunityOrganization, organization, patientGroup, patientEngaRole, patientUser);
            logger.info("createPatientUser ssoUser >>>>>>>>>>>>>>>>: " + ssoUser.toString());

            ssoUserDAO.addUser(communityId, tokenId, ssoUser.toString());

            em.getTransaction().commit();

            logger.info("Successfully added user to OpenAM: " + patientUser);

        } catch (Exception ex) {
            em.getTransaction().rollback();
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    //JIRA 629 - remove properties
    public void updatePatientUser(SSOUserDAO ssoUserDAO, UserCommunityOrganizationDAO userCommunityOrganizationDAO, UserDAO userDAO,
            User user, String userPassword, CommunityOrganization communityOrganization, String tokenId, OrganizationCwd organization, String patientEngaRole, String directAddress, String patientUser) throws Exception {
        Community community = communityOrganization.getCommunity();
        Long communityId = community.getCommunityId();

        logger.info(">>>>>>>>>>>updatePatientUser :patientUser:" + patientUser + ":directAddress:" + directAddress + ":Community ID:" + communityId);
        EntityManager em = null;

        try {
            em = getEntityManager();
            em.getTransaction().begin();

            //Get OpenAM patient group
            ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
            String patientGroup = configurationDAO.getProperty(communityId, "carebook.openam.patientgroup", "");

            //Build SSO User
            StringBuilder ssoUser = ssoUtils.buildSsoPatientUser(user, userPassword, directAddress,
                    user.getUserCommunityOrganization(community), organization, patientGroup, patientEngaRole, patientUser);

            logger.info(">>>>>>updatePatientUser ssoUser <<<<<<<<<<<<<<" + ssoUser);

            //1.4 create user in Open AM SSO
            ssoUserDAO.updateUser(communityId, tokenId, ssoUser.toString());
            em.getTransaction().commit();

            logger.info("Successfully Patient updated user: " + patientUser);

        } catch (Exception exc) {
            em.getTransaction().rollback();
            throw exc;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
