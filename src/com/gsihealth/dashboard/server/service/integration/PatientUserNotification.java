/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.service.integration;

import com.gsihealth.entity.OrganizationCwd;
import com.gsihealth.entity.PatientEngagementRole;
import com.gsihealth.entity.User;
import com.gsihealth.dashboard.server.communication.CommunicationManager;
import com.gsihealth.dashboard.server.util.SSOUtils;
import java.net.URI;
import java.net.URLEncoder;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author User
 */
public class PatientUserNotification {

    private Logger logger = Logger.getLogger(getClass().getName());
    private EntityManagerFactory emf = null;
    private CommunicationManager communicationManager = null;
    private SSOUtils ssoUtils = new SSOUtils();

    public static enum PatientStatus {

        Active, Inactive, Reactive
    };

    public PatientUserNotification() {
        communicationManager = new CommunicationManager();
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public PatientUserNotification(EntityManagerFactory dashboardEmf) {
        communicationManager = new CommunicationManager();
        this.emf = dashboardEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void messageAppNotification(User user, String tokenId, String url, PatientStatus patientStatus, OrganizationCwd organization, 
            PatientEngagementRole engagementRole,long communityId, Long accessLevelId, String gender) throws Exception {

        logger.info("<<<<<<<<<<<<<<<<<<<<<<<<<<< messageAppNotification >>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
        String messageAppNotification = prepareNotification(user, tokenId, url, patientStatus, organization, 
                engagementRole,communityId, accessLevelId, gender);
        logger.info("messageAppNotification >>>>>>>>>>>>>>>>: " + messageAppNotification);
        //messageAppNotification=messageAppNotification.replaceAll("=null", "=")
        URI uri = new URI(messageAppNotification);
        
       String status= communicationManager.callGET(uri).getClientResponseStatus().getReasonPhrase();
        System.out.println("::PatientUserNotification::status::"+status); 
       logger.info("after communicationManager.callGET: ");
    }

    public String prepareNotification(User user, String tokenId, String url, PatientStatus patientStatus, OrganizationCwd organization, 
            PatientEngagementRole engagementRole,long communityId, Long accessLevelId,
            String gender) throws Exception {

        StringBuilder msgAppNotification = new StringBuilder();

        msgAppNotification.append(url + semiColon());
        msgAppNotification.append("SSO_TOKEN=" + tokenId + semiColon());
        msgAppNotification.append("TITLE=" + encode(user.getTitle()) + semiColon());
        msgAppNotification.append("FIRST_NAME=" + encode(user.getFirstName()) + semiColon());
        msgAppNotification.append("LAST_NAME=" + encode(user.getLastName()) + semiColon());
        msgAppNotification.append("GENDER=" + encode(gender) + semiColon());
        String directAddress = user.getUserCommunityOrganization(communityId).getDirectAddress();
        msgAppNotification.append("EMAIL=" + encode(directAddress) + semiColon());
        msgAppNotification.append("DIRECT_ADDRESS=" + encode(directAddress) + semiColon());
        msgAppNotification.append("HOME_PHONE=" + encode(user.getPhoneExt()) + semiColon());

        msgAppNotification.append("MOBILE_NUMBER=" +encode( user.getMobileNumber()) + semiColon());

        if (patientStatus == patientStatus.Active || patientStatus == patientStatus.Reactive) {
            msgAppNotification.append("STATUS=" +encode( "Active") + semiColon());
        } else if (patientStatus == patientStatus.Inactive) {
            msgAppNotification.append("STATUS=" + encode("Inactive") + semiColon());
        }
        msgAppNotification.append("MUST_CHANGE_PASSWORD=" + "1" + semiColon());

      
        msgAppNotification.append("PHONE_EXT=" + encode(user.getPhoneExt()) + semiColon());
        msgAppNotification.append("STREET_ADDRESS_1=" + encode(user.getStreetAddress1()) + semiColon());
        msgAppNotification.append("STREET_ADDRESS_2=" + encode(user.getStreetAddress2()) + semiColon());
        msgAppNotification.append("CITY=" + encode(user.getCity()) + semiColon());

        msgAppNotification.append("STATE=" +encode( user.getState()) + semiColon());
        msgAppNotification.append("ZIP_CODE=" + encode(user.getZipCode()) + semiColon());

        msgAppNotification.append("ROLE_ID=" + engagementRole.getPatientEngagementRolePK().getRoleId() + semiColon());
        msgAppNotification.append("ACCESS_LEVEL_ID=" + accessLevelId + semiColon());
        msgAppNotification.append("MIDDLE_NAME=" + encode(user.getMiddleName()) + semiColon());

        msgAppNotification.append("PREFIX=" + encode(user.getPrefix()) + semiColon());
        

        msgAppNotification.append("PERSONAL_EMAIL=" + encode(user.getEmail()) + semiColon());
        msgAppNotification.append("ORGANIZATION_NAME=" +encode( organization.getOrganizationCwdName()) + semiColon());
        msgAppNotification.append("COMMUNITY_ID=" +encode( String.valueOf(communityId)) + semiColon());
        msgAppNotification.append("OID=" + encode(organization.getOid()));
        
        
        //msgAppNotification.append("NPI=" + user.getNpi() + semiColon());
        //msgAppNotification.append("FAILED_ACCESS_ATTEMPTS=" + user.getFailedAccessAttempts()+getComma());
        //msgAppNotification.append("CREDENCIALS=" + user.getCredentials()+semiColon());
        //  msgAppNotification.append("ENABLED=" + user.getEnabled());
        //msgAppNotification.append("DELETED=" + user.getDeleted()+getComma());
        //msgAppNotification.append("ACCOUNT_LOCKED_OUT=" + user.getAccountLockedOut()+getComma());
        //msgAppNotification.append("CREATION_DATE=" + user.getCreationDate()+semiColon());
        // msgAppNotification.append("LAST_UPDATE_DATE=" + new Date()+semiColon());
        //msgAppNotification.append("LAST_PASSWORD_CHANGE_DATE=" + user.getLastPasswordChangeDate()+getComma());
        // msgAppNotification.append("LAST_SUCCESSFUL_LOGIN_DATE=" + user.getLastSuccessfulLoginDate()+getComma());
        //msgAppNotification.append("LAST_SUCCESSFUL_CHANGE_DATE=" + user.getLastPasswordChangeDate()+getComma());
        //msgAppNotification.append("DATE_OF_BIRTH=" + user.getDateOfBirth()+semiColon());
        //  msgAppNotification.append("PASSWORD=" + user.getpgetCity());
        

        return msgAppNotification.toString();
    }

    private String semiColon() {
        return ";";
    }
    
     protected String encode(String value) throws Exception {
        if (value != null) {
            value = URLEncoder.encode(value, "UTF-8");
        } else {
            value = "";
        }
        return value;
    }
}
