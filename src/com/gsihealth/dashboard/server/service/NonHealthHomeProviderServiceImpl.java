package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.service.NonHealthHomeProviderService;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.Constants;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.entity.*;
import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderAssignedPatientDTO;
import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderDTO;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.common.DuplicateException;
import com.gsihealth.dashboard.server.dao.AuditDAO;
import com.gsihealth.dashboard.server.dao.NonHealthHomeProviderDAO;
import com.gsihealth.dashboard.server.dao.RoleDAO;
import com.gsihealth.dashboard.server.dao.UserDAO;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 *
 * @author Chad Darby
 */
public class NonHealthHomeProviderServiceImpl extends BasePortalServiceServlet implements NonHealthHomeProviderService {

    private Logger logger = Logger.getLogger(getClass().getName());
    private AuditDAO auditDAO;
    private NonHealthHomeProviderDAO nonHealthHomeProviderDAO;
    private UserDAO userDAO;

    /**
     * Initializes the service with DAOs
     */
    @Override
    public void init() throws ServletException {

        ServletContext application = getServletContext();
        super.init();
        nonHealthHomeProviderDAO = (NonHealthHomeProviderDAO) application.getAttribute(WebConstants.NON_HEALTH_HOME_PROVIDER_DAO_KEY);
        auditDAO = (AuditDAO) application.getAttribute(WebConstants.AUDIT_DAO_KEY);
        userDAO = (UserDAO) application.getAttribute(WebConstants.USER_DAO_KEY);
         setRoleDAO((RoleDAO) application.getAttribute(WebConstants.ROLE_DAO_KEY));
    }

    /**
     * Convert entity objects to DTOs
     *
     * @param providers
     * @return
     */
    protected List<NonHealthHomeProviderDTO> convert(List<NonHealthHomeProvider> providers, long patientId) {

        List<NonHealthHomeProviderDTO> providerDTOs = new ArrayList<NonHealthHomeProviderDTO>();

        for (NonHealthHomeProvider tempProvider : providers) {
            NonHealthHomeProviderDTO tempDTO = convertTODTO(tempProvider, patientId);
            providerDTOs.add(tempDTO);
        }

        return providerDTOs;
    }

    public int getTotalCountOfProviders(long patientId) throws PortalException {

        try {
            int totalCount = nonHealthHomeProviderDAO.getNonHealthHomeProviderCount(patientId);
            logger.info("Total provider counts= " + totalCount);
            return totalCount;
        } catch (Exception exc) {
            final String message = "Error getting total provider count";
            logger.logp(Level.SEVERE, getClass().getName(), "getTotalCountOfProviders", message, exc);

            throw new PortalException(message);
        }
    }

    public void addProvider(NonHealthHomeProviderDTO providerDTO, long patientId, long communityId) throws PortalException {

        try {

            // convert
            //Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
            NonHealthHomeProviderDTO tempDTO = providerDTO;
            long tempPatientId = patientId;
            NonHealthHomeProvider provider = convertDTO(tempDTO, communityId);

            if (isDuplicate(provider, communityId)) {
                throw new DuplicateException("Another provider already exists with the same email address.");
            }

            if (null != provider.getNPI()) {
                if (isDuplicateNPI(provider, communityId)) {
                    throw new DuplicateException("Another provider already exists with the same NPI.");
                }
            }


            // add to nonHealthHomeProvider
            logger.info("Adding provider...");
            long providerId = nonHealthHomeProviderDAO.add(provider);

            logger.info("Provider added successfully with id=" + providerId + " to non_health_home_provider table.");

            NonHealthHomeProviderPK tempPK = provider.getNonHealthHomeProviderPK();
            tempPK.setProviderId(providerId);


            //  if("PERMIT".equalsIgnoreCase(provider.getConsentStatus())) {
            //convert to assignedProvider
            NonHealthHomeProviderAssignedPatient providerAssignedPatient = convertToAssignedProvider(provider, tempPatientId);
            // add to assignprovider if consent as permit
            logger.info("adding the provider to non_health_home_provider_assigned table...");

            nonHealthHomeProviderDAO.assignProvider(providerAssignedPatient, false);

            logger.info("Provider added successfully to non_health_home_provider_assigned table.");

            provider = null;
            tempDTO = null;


        } catch (DuplicateException dpe) {
            final String message = "Error adding provider. " + dpe.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "addProvider", message, dpe);

            throw new PortalException(message);
        } catch (Exception exc) {
            final String message = "An error occurred. Error adding provider.";
            logger.logp(Level.SEVERE, getClass().getName(), "addProvider", message, exc);

            throw new PortalException(message);
        }
    }

    public void updateProvider(NonHealthHomeProviderDTO providerDTO, boolean checkForDuplicateEmail, boolean checkForDuplicateNPI, long communityId) throws PortalException {

        try {

            // convert
            //Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

            NonHealthHomeProvider provider = convertDTO(providerDTO, communityId);
            

            // check for duplicates
            if (checkForDuplicateEmail && isDuplicate(provider, communityId)) {
                throw new DuplicateException("Another provider already exists with the same email address.");
            }

            if (checkForDuplicateNPI && isDuplicateNPI(provider, communityId)) {
                throw new DuplicateException("Another provider already exists with the same NPI.");

            }

            // update
            nonHealthHomeProviderDAO.update(provider);
            logger.info("Provider updated successfully.");


        } catch (DuplicateException dpe) {
            final String message = "An error occurred. Error updating provider. " + dpe.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "updateProvider", message, dpe);
            throw new PortalException(message);
        } catch (Exception exc) {
            final String message = "An error occurred. Error updating provider.";
            logger.logp(Level.SEVERE, getClass().getName(), "updateProvider", message, exc);

            throw new PortalException(message);
        }
    }

    public void removeProvider(NonHealthHomeProviderDTO providerDTO, long communityId) throws PortalException {

        try {

            // convert
            //Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
            NonHealthHomeProvider provider = convertDTO(providerDTO, communityId);
            long providerId = provider.getNonHealthHomeProviderPK().getProviderId();
            // long communityId = providerDTO.getCommunityId();
            NonHealthHomeProviderPK nonHealthHomeProviderPK = new NonHealthHomeProviderPK();
            nonHealthHomeProviderPK.setCommunityId(communityId);
            nonHealthHomeProviderPK.setProviderId(providerId);

            // delete from non_health_home_provider_assigned_patient table.
            // get a list of records accosiated with this provider.
            List<NonHealthHomeProviderAssignedPatient> assignedProviders = nonHealthHomeProviderDAO.getProviderAssignedPatient(providerId, communityId);

            // remvoing one by one
            for (NonHealthHomeProviderAssignedPatient tempAssignedProvider : assignedProviders) {
                nonHealthHomeProviderDAO.unAssignProvider(tempAssignedProvider);
            }

            // now remove from non_health_home_provider table.
            nonHealthHomeProviderDAO.delete(nonHealthHomeProviderPK);
            logger.info("Provider removed successfully.");

        } catch (Exception exc) {
            final String message = "Error removing provider";
            logger.logp(Level.SEVERE, getClass().getName(), "removeProvider", message, exc);

            throw new PortalException(message);
        }
    }

    private void loadConsentObtainedByNames(List<NonHealthHomeProviderDTO> providerDTOs) {

        for (NonHealthHomeProviderDTO tempProvider : providerDTOs) {

            // get the user
            long userId = tempProvider.getConsentObtainedByUserId();
            User theUser = userDAO.findUser(userId);

            // set the name
            if (theUser != null) {
                String name = theUser.getFirstName() + " " + theUser.getLastName();
                tempProvider.setConsentObtainedByName(name);
            }
        }
    }

    private boolean isDuplicate(NonHealthHomeProvider provider, long communityId) {
        return nonHealthHomeProviderDAO.isDuplicateEmail(provider.getEmail(), communityId);
    }

    private boolean isDuplicateNPI(NonHealthHomeProvider provider, long communityId) {
        return nonHealthHomeProviderDAO.isDuplicateNPI(provider.getNPI(), communityId);
    }

    public List<NonHealthHomeProviderDTO> getProviderList(long patientId, long communityId) throws PortalException {


        List<NonHealthHomeProvider> providers = nonHealthHomeProviderDAO.findNonHealthHomeProviderEntities(patientId, communityId);
        // convert entity objects to dtos
        List<NonHealthHomeProviderDTO> providerDTOs = convert(providers, patientId);


        return providerDTOs;
    }

    public NonHealthHomeProvider convertDTO(NonHealthHomeProviderDTO providerDTO, long communityId) {
        NonHealthHomeProvider provider = new NonHealthHomeProvider();
        NonHealthHomeProviderPK providerPK = new NonHealthHomeProviderPK();

        providerPK.setCommunityId(providerDTO.getCommunityId());
        providerPK.setProviderId(providerDTO.getProviderId());

        provider.setNonHealthHomeProviderPK(providerPK);
        provider.setFirstName(providerDTO.getFirstName());
        provider.setLastName(providerDTO.getLastName());
        provider.setMiddleInitial(providerDTO.getMiddleInitial());
        provider.setEmail(providerDTO.getEmail());
        provider.setCity(providerDTO.getCity());
        provider.setState(providerDTO.getState());
        provider.setStreetAddress1(providerDTO.getStreetAddress1());
        provider.setStreetAddress2(providerDTO.getStreetAddress2());
        provider.setConsentDateTime(providerDTO.getConsentDateTime());
        provider.setConsentStatus(providerDTO.getConsentStatus());
        provider.setConsentObtainedByUserId(providerDTO.getConsentObtainedByUserId());
        provider.setCredentials(providerDTO.getCredentials());
        provider.setZipCode(providerDTO.getZipCode());
        provider.setTelephoneNumber(providerDTO.getTelephoneNumber());
        provider.setTitle(providerDTO.getTitle());

        //new field
        provider.setSpecialty(providerDTO.getSpecialty());
        provider.setDirectAddress(providerDTO.getDirectAddress());
        provider.setPrimaryPracitionerFacility(providerDTO.getPrimaryPracitionerFacility());
        provider.setNPI(providerDTO.getNPI());

        Role role = null;

        String roleStr = providerDTO.getRole();
        if (StringUtils.isNotBlank(roleStr)) {
            long roleId = Long.parseLong(roleStr);
            System.out.println("roleId::::"+roleId);
            role = roleDAO.findProviderRole(roleId, communityId);
            role.setCommunityId(communityId);
//            role.setRoleId(role.getPrimaryRoleId());//setPrimaryRoleId(roleId);
                      
        }
        
        provider.setRole(role);
        
        return provider;
    }

    public NonHealthHomeProviderDTO convertTODTO(NonHealthHomeProvider provider, long patientId) {
        NonHealthHomeProviderDTO providerDTO = new NonHealthHomeProviderDTO();

        providerDTO.setCommunityId(provider.getNonHealthHomeProviderPK().getCommunityId());
        providerDTO.setProviderId(provider.getNonHealthHomeProviderPK().getProviderId());
        providerDTO.setFirstName(provider.getFirstName());
        providerDTO.setLastName(provider.getLastName());
        providerDTO.setMiddleInitial(provider.getMiddleInitial());
        providerDTO.setEmail(provider.getEmail());
        providerDTO.setCity(provider.getCity());
        providerDTO.setState(provider.getState());
        providerDTO.setStreetAddress1(provider.getStreetAddress1());
        providerDTO.setStreetAddress2(provider.getStreetAddress2());



        providerDTO.setCredentials(provider.getCredentials());
        providerDTO.setZipCode(provider.getZipCode());
        providerDTO.setTelephoneNumber(provider.getTelephoneNumber());
        providerDTO.setTitle(provider.getTitle());

        // setting assigned status to No by default
        providerDTO.setAssignedStatus(Constants.NO);
        // set consent status to DENY unless assigned
        providerDTO.setConsentStatus(Constants.DENY);
        // set consent obtained by userid to blank
        //providerDTO.setConsentObtainedByUserId(0);

        // set attestation date captured when provider is added
        //providerDTO.setConsentDateTime(provider.getConsentDateTime());
        Role role = provider.getRole();
       
        if (role != null) {
            providerDTO.setRole(Long.toString(role.getRoleId()));
        }



        List<NonHealthHomeProviderAssignedPatient> assignedPatients = nonHealthHomeProviderDAO.getProviderAssignedPatient(provider.getNonHealthHomeProviderPK().getProviderId(), provider.getNonHealthHomeProviderPK().getCommunityId());

        if (!assignedPatients.isEmpty()) {
            for (NonHealthHomeProviderAssignedPatient tempAssignedPatient : assignedPatients) {

                if (patientId == tempAssignedPatient.getNonHealthHomeProviderAssignedPatientPK().getPatientId()) {
                    providerDTO.setAssignedStatus(Constants.YES);

                    providerDTO.setConsentStatus(tempAssignedPatient.getConsentStatus());
                    // set consent obtained by userid to the user who assigned the provider
                    providerDTO.setConsentObtainedByUserId(tempAssignedPatient.getConsentObtainedByUserId());
                    // set attestation date captured when provider is assigned
                    providerDTO.setConsentDateTime(tempAssignedPatient.getConsentDateTime());

                    role = tempAssignedPatient.getRole();
                    if (role != null) {
                        providerDTO.setRole(Long.toString(role.getRoleId()));

                    }
                }

            }
        }

        // new fields
        providerDTO.setSpecialty(provider.getSpecialty());
        providerDTO.setPrimaryPracitionerFacility(provider.getPrimaryPracitionerFacility());



        providerDTO.setDirectAddress(provider.getDirectAddress());
        providerDTO.setNPI(provider.getNPI());

        return providerDTO;
    }

    public List<NonHealthHomeProviderDTO> getProvidersByCommunityId(long communityId, long patientId) throws PortalException {
        try {
            int totalCount = nonHealthHomeProviderDAO.getNonHealthHomeProviderCountByCommunityId(communityId);

            List<NonHealthHomeProvider> providers = nonHealthHomeProviderDAO.findNonHealthHomeProviderEntitiesByCommunityId(communityId);

            // convert entity objects to dtos
            List<NonHealthHomeProviderDTO> providerDTOs = convert(providers, patientId);


            // load consent obtained by names
            loadConsentObtainedByNames(providerDTOs);

//            SearchResults<NonHealthHomeProviderDTO> searchResults = new SearchResults<NonHealthHomeProviderDTO>(totalCount, providerDTOs);

            return providerDTOs;

        } catch (Exception exc) {
            final String message = "Error getting providers.";
            logger.logp(Level.SEVERE, getClass().getName(), "getProviders", message, exc);

            throw new PortalException(message);
        }
    }

    public int getTotalCountOfProvidersByCommunityId(long communityId) throws PortalException {
        try {
            long start = System.currentTimeMillis();
            int totalCount = nonHealthHomeProviderDAO.getNonHealthHomeProviderCountByCommunityId(communityId);
            long end = System.currentTimeMillis();
            logger.info("Total provider counts= " + totalCount + ":Total:time taken:" + (end - start));
            return totalCount;
        } catch (Exception exc) {
            final String message = "Error getting total provider count";
            logger.logp(Level.SEVERE, getClass().getName(), "getTotalCountOfProviders", message, exc);

            throw new PortalException(message);
        }
    }

    public void assignProvider(NonHealthHomeProviderAssignedPatientDTO assignedPatientDTO) throws PortalException {
        logger.info("assignProvider assignProvider ");

        logger.info("ProviderId: " + assignedPatientDTO.getProviderId() + ":PatientDTO.getPatientId:" + assignedPatientDTO.getPatientId() + ":PatientDTO.getCommunityId:" + assignedPatientDTO.getCommunityId());;


        NonHealthHomeProviderAssignedPatient assignedPatient = new NonHealthHomeProviderAssignedPatient();
        NonHealthHomeProviderAssignedPatientPK assignedPatientPK = new NonHealthHomeProviderAssignedPatientPK();

        assignedPatientPK.setProviderId(assignedPatientDTO.getProviderId());
        assignedPatientPK.setPatientId(assignedPatientDTO.getPatientId());
        assignedPatientPK.setCommunityId(assignedPatientDTO.getCommunityId());

        assignedPatient.setConsentDateTime(assignedPatientDTO.getConsentDateTime());
        assignedPatient.setConsentObtainedByUserId(assignedPatientDTO.getConsentObtainedByUserId());
        assignedPatient.setConsentStatus(assignedPatientDTO.getConsentStatus());
        String roleID = assignedPatientDTO.getRoleId();

        if (roleID != null) {
          
         
         
           Role  role = roleDAO.findProviderRole(Long.parseLong(roleID), assignedPatientDTO.getCommunityId());
           role.setCommunityId(assignedPatientDTO.getCommunityId());
//           role.setRoleId(role.getPrimaryRoleId());
            

            role.setRoleName(role.getRoleName());
            assignedPatient.setRole(role);
            //assignedPatient.setProviderRole(Long.parseLong(roleID));
           
        }

        assignedPatient.setNonHealthHomeProviderAssignedPatientPK(assignedPatientPK);


        try {

            // check if assignedProviderRecord already exist then merge the record else add a new record
            boolean assignedProviderExist = nonHealthHomeProviderDAO.ifProviderAssignedExist(assignedPatientDTO.getProviderId(), assignedPatientDTO.getPatientId(), assignedPatientDTO.getCommunityId());
            logger.info("Assigned Provider record already exists= " + assignedProviderExist);

            nonHealthHomeProviderDAO.assignProvider(assignedPatient, assignedProviderExist);
            logger.info("Provider assigned successfully with status=" + assignedPatient.getConsentStatus());
        } catch (Exception exc) {
            final String message = "Error assigning provider to the patient";
            logger.logp(Level.SEVERE, getClass().getName(), "assignProvider", message, exc);

            throw new PortalException(message);
        }
    }

    public void unAssignProvider(NonHealthHomeProviderAssignedPatientDTO assignedPatientDTO) throws PortalException {
       
        NonHealthHomeProviderAssignedPatient assignedPatient = new NonHealthHomeProviderAssignedPatient();
        NonHealthHomeProviderAssignedPatientPK assignedPatientPK = new NonHealthHomeProviderAssignedPatientPK();

        assignedPatientPK.setProviderId(assignedPatientDTO.getProviderId());
        assignedPatientPK.setPatientId(assignedPatientDTO.getPatientId());
        assignedPatientPK.setCommunityId(assignedPatientDTO.getCommunityId());

        assignedPatient.setNonHealthHomeProviderAssignedPatientPK(assignedPatientPK);
        assignedPatient.setConsentDateTime(assignedPatientDTO.getConsentDateTime());
        assignedPatient.setConsentStatus(Constants.DENY);
        assignedPatient.setConsentObtainedByUserId(assignedPatientDTO.getConsentObtainedByUserId());
        
       NonHealthHomeProvider healthHomeProvider= nonHealthHomeProviderDAO.getProvidersByCommunityId(assignedPatientDTO.getCommunityId(),assignedPatientDTO.getProviderId());
       
       assignedPatient.setRole(healthHomeProvider.getRole());
       
       //assignedPatient.setProviderRole(healthHomeProvider.getRole().getRoleId());
       

        try {
            // check if assignedProviderRecord already exist then merge the record else add a new record
            boolean assignedProviderExist = nonHealthHomeProviderDAO.ifProviderAssignedExist(assignedPatientDTO.getProviderId(), assignedPatientDTO.getPatientId(), assignedPatientDTO.getCommunityId());

            nonHealthHomeProviderDAO.assignProvider(assignedPatient, assignedProviderExist);
            logger.info("Provider un-assigned successfully with status=" + assignedPatient.getConsentStatus());

        } catch (Exception exc) {
            final String message = "Error un-assigning provider from the patient";
            logger.logp(Level.SEVERE, getClass().getName(), "assignProvider", message, exc);

            throw new PortalException(message);
        }
    }

    public int getCountofPatientsAssignedToProvider(long providerId, long communityId) throws PortalException {
        try {
            int totalCount = nonHealthHomeProviderDAO.getCountForPatientsAssignedToProvider(providerId, communityId);
            logger.info("Total patients count assigned to provider with id=" + providerId + " is" + totalCount);
            return totalCount;
        } catch (Exception exc) {
            final String message = "Error getting total provider count";
            logger.logp(Level.SEVERE, getClass().getName(), "getTotalCountOfProviders", message, exc);

            throw new PortalException(message);
        }
    }

    public NonHealthHomeProviderAssignedPatient convertToAssignedProvider(NonHealthHomeProvider provider, long patientId) {
        NonHealthHomeProviderAssignedPatient assignedPatient = new NonHealthHomeProviderAssignedPatient();
        NonHealthHomeProviderAssignedPatientPK assignedPatientPK = new NonHealthHomeProviderAssignedPatientPK();
        
        assignedPatientPK.setCommunityId(provider.getNonHealthHomeProviderPK().getCommunityId());
        assignedPatientPK.setProviderId(provider.getNonHealthHomeProviderPK().getProviderId());
        assignedPatientPK.setPatientId(patientId);

        assignedPatient.setNonHealthHomeProviderAssignedPatientPK(assignedPatientPK);
        assignedPatient.setConsentDateTime(provider.getConsentDateTime());
        assignedPatient.setConsentStatus(provider.getConsentStatus());
        assignedPatient.setConsentObtainedByUserId(provider.getConsentObtainedByUserId());
        
        assignedPatient.setRole(provider.getRole());
        
        //assignedPatient.setProviderRole(provider.getProviderRole());

        return assignedPatient;
    }

    /**
     *
     * @param communityId
     * @return
     * @throws PortalException
     */
    public List<NonHealthHomeProviderDTO> getProvidersByCommunityId(long communityId) throws PortalException {
        try {
            List<NonHealthHomeProvider> list = nonHealthHomeProviderDAO.getProvidersByCommunityId(communityId);

            List<NonHealthHomeProviderDTO> listDTO = new ArrayList<NonHealthHomeProviderDTO>();
            NonHealthHomeProviderDTO healthHomeProviderDTO = null;

            for (NonHealthHomeProvider tempProvider : list) {
                //logger.info(":ProviderId:getProvidersByCommunity:"+tempProvider.getNonHealthHomeProviderPK().getProviderId());;

                healthHomeProviderDTO = new NonHealthHomeProviderDTO();
                healthHomeProviderDTO.setProviderId(tempProvider.getNonHealthHomeProviderPK().getProviderId());
                healthHomeProviderDTO.setLastName(tempProvider.getLastName());
                healthHomeProviderDTO.setFirstName(tempProvider.getFirstName());
                listDTO.add(healthHomeProviderDTO);
            }
            //logger.info("Total list getProvidersByCommunity :"+listDTO.size());
            return listDTO;

        } catch (Exception exc) {
            final String message = "Error getting total provider count";
            logger.logp(Level.SEVERE, getClass().getName(), "getTotalCountOfProviders", message, exc);

            throw new PortalException(message);
        }
    }
}
