package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.common.Constants;
import com.gsihealth.entity.User;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.dashboard.server.dao.SSOUserDAO;
import com.gsihealth.dashboard.server.dao.UserDAO;
import com.gsihealth.dashboard.server.service.integration.Encryption;
import com.gsihealth.dashboard.server.util.EmailUtils;
import com.gsihealth.dashboard.server.util.PasswordUtils;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

/**
 *
 * @author Chad Darby
 */
public class SecurityServiceHelper {

    private Logger logger = Logger.getLogger(getClass().getName());
    private UserDAO userDAO;
    private SSOUserDAO ssoUserDAO;

    public SecurityServiceHelper() {

    }

    public SecurityServiceHelper(UserDAO theUserDAO, SSOUserDAO theSsoUserDAO, Long communityId) {
        userDAO = theUserDAO;
        ssoUserDAO = theSsoUserDAO;
    }

    public SecurityServiceHelper(UserDAO theUserDAO, SSOUserDAO theSsoUserDAO) {
        logger.info("I'm not setting communityId, be careful how you use me");
        userDAO = theUserDAO;
        ssoUserDAO = theSsoUserDAO;
    }

    //JIRA 629 - remove properties
    public boolean handleForgotPassword(String portalUserId, HttpServletRequest request, Long communityId) throws Exception {
        boolean isValid = false;
        User user = isValidPortalUserId(portalUserId, communityId);

        if (user != null) {
            // generate new password
            String randPassword = PasswordUtils.generatePassword();

            // change password
            //JIRA 629 - remove properties
            //JIRA 1853
            /*
             * During forgot password, user will not be logged and so will not 
             * have a valid token. In this scenario, we have to generate the token using admin 
             * and use that
             */
            String tokenId = getToken(communityId);
            changePassword(portalUserId, randPassword, communityId, tokenId);
            
            //unlock account
            ssoUserDAO.unlockAccount(communityId, tokenId, portalUserId);

            setUserMustChangePasswordFlag(user);
            //JIRA 629 - remove properties
            sendEmailForForgotPassword(portalUserId, randPassword, request, communityId);
            isValid = true;
            logger.info("forgotPassword: Sent email to: " + portalUserId);
        }
        return isValid;
    }

    /**
     * Returns true if we have a valid portal user id
     *
     * @param portalUserId
     * @return
     */
    protected User isValidPortalUserId(String portalUserId, Long communityId) {

        try {
            return userDAO.findUserByEmail(portalUserId, communityId);
        } catch (NoResultException exc) {
            return null;
        }
    }

    protected void setUserMustChangePasswordFlag(User user) throws Exception {

        // send new pw on user
        // reset other fields
        user.setFailedAccessAttempts(0);
        user.setMustChangePassword(Constants.YES_CHAR);

        Date now = new Date();
        user.setLastSuccessfulLoginDate(now);
        user.setLastPasswordChangeDate(now);
        user.setAccountLockedOut(Constants.NO_CHAR);
        //user.setEndDate(null);

        // update user in database
        userDAO.update(user);
    }

    //JIRA 629 - remove properties
    protected void sendEmailForForgotPassword(String portalUserId, String randPassword, HttpServletRequest request, 
            Long communityId) throws MessagingException, IOException {
        // send email
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        String from = configurationDAO.getProperty(communityId, "mail.coordinator.from");

        String to = portalUserId;
        String subject = "GSI Health - Health Home Dashboard: Password Reset";
        String server = configurationDAO.getProperty(communityId, "forgot.password.email.link", "http://www.gsihealth.com");

        Properties velocityProps = new Properties();
        velocityProps.load(new FileReader("/gsihealth/dashboard/config/velocity.properties"));
        Velocity.init(velocityProps);

        VelocityContext context = new VelocityContext();
        context.put("server", server);
        context.put("portalUserId", portalUserId);
        context.put("randPassword", randPassword);

        Template template = null;

        template = Velocity.getTemplate("emailtemplate.vm");

        StringWriter sw = new StringWriter();

        template.merge(context, sw);
        EmailUtils emailUtils = new EmailUtils(communityId);
        emailUtils.sendMessage(from, to, subject, sw.toString());
    }

    protected Map<String, String> getSsoUserMap(String email, String password) throws Exception {
        Map<String, String> ssoUserMap = new LinkedHashMap<String, String>();

        //Use Main info to identify  user for OpenAM
        ssoUserMap.put("identity_name", encode(email));
        ssoUserMap.put("identity_attribute_names=userpassword&identity_attribute_values_userpassword", encode(password));

        return ssoUserMap;
    }

    protected String getSsoUserString(String email, String password) throws Exception {

        StringBuilder ssoUser = new StringBuilder();
        //Use Main info to identify  user for OpenAM
        ssoUser.append("&identity_name=" + encode(email));
        ssoUser.append("&identity_attribute_names=userpassword&identity_attribute_values_userpassword=" + encode(password));
         ssoUser.append("&identity_realm=%2F");

        return ssoUser.toString();
    }

    protected String encode(String value) throws Exception {

        value = URLEncoder.encode(value, "UTF-8");

        return value;
    }

    //JIRA 629 - remove properties
    public String changePassword(String portalUserId, String theNewPassword, Long communityId, String token) throws Exception {
        //JIRA 1853
        //String tokenId = getToken(communityId);

        //String ssoUser = getSsoUserString(portalUserId, theNewPassword);
        //String umUrl = configurationDAO.getProperty(communityId, "userManager.url");
        
        
        return ssoUserDAO.changePassword(communityId, token, portalUserId, theNewPassword);
    }

    //JIRA 629 - remove properties
    private String getToken(Long communityId) throws Exception {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        String adminUserName = configurationDAO.getProperty(communityId, "sso.server.admin.username");
        String adminPassword = configurationDAO.getProperty(communityId, "sso.server.admin.password");
        String tokenId = ssoUserDAO.authenticate(communityId, adminUserName, adminPassword);
        tokenId = StringUtils.trimToEmpty(tokenId);
        return tokenId;
    }

    //JIRA 629 - remove properties
    protected void sendPasswordEmailToPatient(String patientEmail, String patientUserName, String patientPassword, long communityId) throws MessagingException, IOException {

        logger.info("<<<< sendPasswordEmailToPatient :patientEmail:" + patientEmail + ":patientPassword:" + patientPassword + ":patientUserName:" + patientUserName);

        // send email
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        String from = configurationDAO.getProperty(communityId, "mail.coordinator.from");
        String to = patientEmail;
        String subject = "GSI Health - Patient Password";

        String server = configurationDAO.getProperty(communityId, "patient.messageapp.url", "http://www.gsihealth.com");

        String encryptedCommunityId = Encryption.encryption(String.valueOf(communityId));
        server = server + "?communityid=" + encryptedCommunityId;

        logger.info("<<<< server:" + server);

        Properties velocityProps = new Properties();
        velocityProps.load(new FileReader("/gsihealth/dashboard/config/velocity.properties"));
        Velocity.init(velocityProps);

        VelocityContext context = new VelocityContext();
        context.put("server", server);
        context.put("portalUserId", patientUserName);
        context.put("randPassword", patientPassword);

        Template template = null;

        template = Velocity.getTemplate("patientemailtemplate.vm");
        logger.info("<<<< templatetemplate:" + template);

        StringWriter sw = new StringWriter();

        template.merge(context, sw);
        EmailUtils emailUtils = new EmailUtils(communityId);
        emailUtils.sendMessage(from, to, subject, sw.toString());

        logger.info("<<<<After  sendPasswordEmailToPatient :patientEmail:" + patientEmail + ":patientPassword:" + patientPassword);

    }
}
