package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.service.AlertService;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.server.dao.*;
import com.gsihealth.entity.*;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.UserAlertDTO;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.util.AuditType;
import com.gsihealth.dashboard.server.util.AuditUtils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

/**
 *
 * @author Chad Darby
 */
public class AlertServiceImpl extends BasePortalServiceServlet implements AlertService {
    private Logger logger = Logger.getLogger(getClass().getName());
    
    private UserAlertDAO userAlertDAO;
    private AuditDAO auditDAO;
    
    private static final int MAX_PAGE_SIZE = 200;

    /**
     * Initializes the service with DAOs
     */
    @Override
    public void init() throws ServletException {

        ServletContext application = getServletContext();
        super.init();
        // Retrieve the DAOs. they were created in the DataContextListener class
        // and stored in the servlet context
        userAlertDAO = (UserAlertDAO) application.getAttribute(WebConstants.USER_ALERT_DAO_KEY);

        applicationDAO = (ApplicationDAO) application.getAttribute(WebConstants.APPLICATION_DAO_KEY);

        userDAO = (UserDAO) application.getAttribute(WebConstants.USER_DAO_KEY);

        auditDAO = (AuditDAO) application.getAttribute(WebConstants.AUDIT_DAO_KEY);

        userCommunityOrganizationDAO = (UserCommunityOrganizationDAO) application.getAttribute(WebConstants.USER_COMMUNITY_ORGANIZATION_DAO_KEY);
    }

    /**
     * Get alerts for user
     *
     * @param userId
     * @return
     * @throws PortalException
     */
    @Override
    public SearchResults<UserAlertDTO> getUserAlertsPartial(long userId) throws PortalException {

        try {
            Long communityId = this.getLoginResult().getCommunityId();
            logger.fine("Getting TOTAL COUNT for user alerts for userId: " + userId + " in community " +
                    communityId);

            int totalCount = userAlertDAO.getTotalCount(userId, communityId);

            // dummy data
            logger.fine("SKIPPING returning top 200 alerts for userId: " + userId);
            List<UserAlertDTO> alertDTOs = new ArrayList<UserAlertDTO>();

            // build up search results
            SearchResults<UserAlertDTO> alertSearchResults = new SearchResults<UserAlertDTO>(totalCount, alertDTOs);

//            updateAlertAppLastAccessTime(userId,communityId);
            return alertSearchResults;

        } catch (Exception ex) {
            String message = "Error retrieving alerts: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error retrieving alerts.");
        }
    }

    /**
     * Update the user alert
     *
     * @param userId
     * @param userAlertDTO
     * @throws PortalException
     */
    @Override
    public void updateUserAlert(UserAlertDTO userAlertDTO) throws PortalException {
        try {
            // convert
            UserAlert userAlert = convert(userAlertDTO);

            // update alerts in db
            userAlertDAO.update(userAlert);

        } catch (Exception ex) {
            String message = "Error updating alerts: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error updating alerts.");
        }
    }

    /**
     * Update a collection of user alerts
     *
     * @param userId
     * @param userAlertDTOs
     * @throws PortalException a
     */
    @Override
    public void updateUserAlerts(List<UserAlertDTO> userAlertDTOs) throws PortalException {

        for (UserAlertDTO temp : userAlertDTOs) {
            updateUserAlert(temp);
        }
    }

    /**
     * Convert entity objects to DTOs
     *
     * @param alerts
     * @return
     */
    protected List<UserAlertDTO> convert(List<UserAlert> alerts) {

        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        List<UserAlertDTO> alertDTOs = new ArrayList<UserAlertDTO>();

        for (UserAlert tempAlert : alerts) {
            UserAlertDTO tempDTO = mapper.map(tempAlert, UserAlertDTO.class);

            AlertMessage tempAlertMessage = tempAlert.getAlertMessageId();
            String applicationName = getApplicationName(tempAlertMessage);

            tempDTO.setApplicationName(applicationName);
            alertDTOs.add(tempDTO);
        }

        return alertDTOs;
    }

    /**
     * Convert DTO to entity
     *
     * @param userAlertDTO
     * @return
     */
    protected UserAlert convert(UserAlertDTO userAlertDTO) {
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        UserAlert userAlert = mapper.map(userAlertDTO, UserAlert.class);

        return userAlert;
    }

    public void setUserAlertDAO(UserAlertDAO userAlertDAO) {
        this.userAlertDAO = userAlertDAO;
    }

    /**
     * Get the application name
     */
    private String getApplicationName(AlertMessage tempAlertMessage) {

        long applicationId = tempAlertMessage.getApplicationId();
        Application application = findApplication(applicationId, tempAlertMessage.getCommunityId());

        String applicationName = application.getApplicationName();

        return applicationName;
    }

    /**
     * Search for user alerts
     *
     * @param fromDate
     * @param lastDate
     * @return
     * @throws PortalException
     */
    @Override
    public SearchResults<UserAlertDTO> searchUserAlertsByDate(long userId, Date fromDate, Date toDate, int pageNumber, int pageSize) throws PortalException {
        try {
            Long communityId = this.getLoginResult().getCommunityId();
            int totalCount = userAlertDAO.getTotalCountForSearchByDate(userId, communityId, fromDate, toDate);

            // get alerts from db
            List<UserAlert> alerts = userAlertDAO.searchUserAlertsByDate(userId, 
                    communityId, fromDate, toDate, pageNumber, pageSize);

            // convert entity objects to dtos
            List<UserAlertDTO> alertDTOs = convert(alerts);

            SearchResults<UserAlertDTO> alertSearchResults = new SearchResults<UserAlertDTO>(totalCount, alertDTOs);

            return alertSearchResults;
        } catch (Exception ex) {
            String message = "Error retrieving alerts: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error retrieving alerts.");
        }
    }

    @Override
    public SearchResults<UserAlertDTO> searchUserAlertsByPatientFirstName(long userId, String searchValue, int pageNumber, int pageSize) throws PortalException {
        try {
            Long communityId = this.getLoginResult().getCommunityId();
            int totalCount = userAlertDAO.getTotalCountForSearchByPatientFirstName(userId, searchValue, communityId);

            // get alerts from db
            List<UserAlert> alerts = userAlertDAO.searchUserAlertsByPatientFirstName(userId, searchValue, 
                    communityId, pageNumber, pageSize);

            // convert entity objects to dtos
            List<UserAlertDTO> alertDTOs = convert(alerts);

            SearchResults<UserAlertDTO> alertSearchResults = new SearchResults<UserAlertDTO>(totalCount, alertDTOs);

            return alertSearchResults;
        } catch (Exception ex) {
            String message = "Error retrieving alerts: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error retrieving alerts.");
        }
    }

    @Override
    public SearchResults<UserAlertDTO> searchUserAlertsByPatientId(long userId, String searchValue, int pageNumber, int pageSize) throws PortalException {
        try {
            Long communityId = this.getLoginResult().getCommunityId();
            int totalCount = userAlertDAO.getTotalCountForSearchByPatientId(userId, searchValue, communityId);

            // get alerts from db
            List<UserAlert> alerts = userAlertDAO.searchUserAlertsByPatientId(userId, searchValue, 
                    communityId, pageNumber, pageSize);

            // convert entity objects to dtos
            List<UserAlertDTO> alertDTOs = convert(alerts);

            // create search resuls
            SearchResults<UserAlertDTO> alertSearchResults = new SearchResults<UserAlertDTO>(totalCount, alertDTOs);


            return alertSearchResults;
        } catch (Exception ex) {
            String message = "Error retrieving alerts: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error retrieving alerts.");
        }
    }

    @Override
    public SearchResults<UserAlertDTO> searchUserAlertsByPatientLastName(long userId, String searchValue, int pageNumber, int pageSize) throws PortalException {
        try {
            Long communityId = this.getLoginResult().getCommunityId();
            int totalCount = userAlertDAO.getTotalCountForSearchByPatientLastName(userId, searchValue,
                    communityId);

            // get alerts from db
            List<UserAlert> alerts = userAlertDAO.searchUserAlertsByPatientLastName(userId, searchValue, 
                    communityId, pageNumber, pageSize);

            // convert entity objects to dtos
            List<UserAlertDTO> alertDTOs = convert(alerts);

            SearchResults<UserAlertDTO> alertSearchResults = new SearchResults<UserAlertDTO>(totalCount, alertDTOs);

            return alertSearchResults;
        } catch (Exception ex) {
            String message = "Error retrieving alerts: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error retrieving alerts.");
        }
    }

    @Override
    public SearchResults<UserAlertDTO> searchUserAlertsByApplication(long userId, String searchValue, int pageNumber, int pageSize) throws PortalException {
        try {
            Long communityId = this.getLoginResult().getCommunityId();
            int totalCount = userAlertDAO.getTotalCountForSearchByApplication(userId, searchValue, communityId);

            // get alerts from db
            List<UserAlert> alerts = userAlertDAO.searchUserAlertsByApplication(userId, searchValue, 
                    communityId, pageNumber, pageSize);

            // convert entity objects to dtos
            List<UserAlertDTO> alertDTOs = convert(alerts);

            SearchResults<UserAlertDTO> alertSearchResults = new SearchResults<UserAlertDTO>(totalCount, alertDTOs);

            return alertSearchResults;
        } catch (Exception ex) {
            String message = "Error retrieving alerts: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error retrieving alerts.");
        }
    }

    @Override
    public SearchResults<UserAlertDTO> searchUserAlertsBySeverity(long userId, String searchValue, int pageNumber, int pageSize) throws PortalException {
        try {
            Long communityId = this.getLoginResult().getCommunityId();
            int totalCount = userAlertDAO.getTotalCountForSearchBySeverity(userId, searchValue, communityId);

            // get alerts from db
            List<UserAlert> alerts = userAlertDAO.searchUserAlertsBySeverity(userId, searchValue, 
                    communityId, pageNumber, pageSize);

            // convert entity objects to dtos
            List<UserAlertDTO> alertDTOs = convert(alerts);

            SearchResults<UserAlertDTO> alertSearchResults = new SearchResults<UserAlertDTO>(totalCount, alertDTOs);


            return alertSearchResults;
        } catch (Exception ex) {
            String message = "Error retrieving alerts: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error retrieving alerts.");
        }
    }

    @Override
    public SearchResults<UserAlertDTO> getUserAlerts(long userId, int pageNumber, int pageSize) throws PortalException {

        try {
            logger.info("Getting user alserts for userId: " + userId + ", pageNumber=" + pageNumber + ", pageSize=" + pageSize);
            Long communityId = this.getLoginResult().getCommunityId();
            int totalCount = userAlertDAO.getTotalCount(userId, communityId);

            // get alerts from db
            List<UserAlert> alerts = userAlertDAO.findUserAlertEntities(userId, communityId, pageNumber, pageSize);

            // convert entity objects to dtos
            List<UserAlertDTO> alertDTOs = convert(alerts);

            // build up search results
            SearchResults<UserAlertDTO> alertSearchResults = new SearchResults<UserAlertDTO>(totalCount, alertDTOs);

            //audit alert accessed
            HttpSession session = getThreadLocalRequest().getSession();
            if (session.getAttribute("AuditUser") == null ||((Long) session.getAttribute("AuditUser")).longValue()!=userId) {
                audit(new User(userId), "User accessed Alert App Successfuly", AuditType.ALERT_APP_ACCESSED);
            }

            updateAlertAppLastAccessTime(userId,communityId);

            return alertSearchResults;

        } catch (Exception ex) {
            String message = "Error retrieving alerts: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error retrieving alerts.");
        }

    }

    @Override
    public int getTotalCount(long userId) throws PortalException {
        try {
            logger.info("Getting total count user alserts for userId: " + userId);
            Long communityId = this.getLoginResult().getCommunityId();
            // get alerts from db
            int count = userAlertDAO.getTotalCount(userId, communityId);

            return count;
        } catch (Exception ex) {
            String message = "Error retrieving alert count: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            throw new PortalException("Error retrieving alert count.");
        }
    }

    private Application findApplication(long applicationId, long communityId) {
        Application theApp = null;

        ServletContext context = getServletContext();

        List<Application> apps = (List<Application>) context.getAttribute(WebConstants.APPLICATION_LIST_KEY);

        for (Application app : apps) {
            if (app.getApplicationPK().getApplicationId() == applicationId
                    && app.getApplicationPK().getCommunityId() == communityId){
                theApp = app;
                break;
            }
        }

        return theApp;
    }

    private void audit(User user, String comment, String auditType) {
        // audit

        try {
            LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());

            Audit audit = AuditUtils.convertUser(loginResult, user);
            audit.setActionComments(comment);
            audit.setActionType(auditType);
            audit.setActionSuccess(com.gsihealth.dashboard.common.Constants.YES);
            audit.setCommunityId(loginResult.getCommunityId());

            auditDAO.addAudit(audit);
            getThreadLocalRequest().getSession().setAttribute("AuditUser", user.getUserId());
        } catch (Exception ex) {
            String message = "Error AUDITING user: " + user.getEmail() + ",   " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
        }

    }


    private void updateAlertAppLastAccessTime(long userId, long communityId)  {

        try{
            UserCommunityOrganization uco =
                    userCommunityOrganizationDAO.findUserCommunityOrganization(userId,communityId);
            uco.setLastAlertAccessDate(new Date());
            userCommunityOrganizationDAO.update(uco);

        }catch(Exception ex){
            logger.log(Level.SEVERE, "Error saving last alert access time", ex);
        }
    }

}
