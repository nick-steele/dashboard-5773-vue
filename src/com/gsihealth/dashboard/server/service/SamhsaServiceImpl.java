/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.service.SamhsaService;
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.common.Constants;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.entity.Audit;
import com.gsihealth.entity.Samhsa;
import com.gsihealth.entity.User;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.AuditDAO;
import com.gsihealth.dashboard.server.dao.SamhsaDAO;
import com.gsihealth.dashboard.server.dao.UserDAO;
import com.gsihealth.dashboard.server.dao.UserCommunityOrganizationDAO;
import com.gsihealth.dashboard.server.util.AuditType;
import com.gsihealth.dashboard.server.util.AuditUtils;
import com.gsihealth.entity.UserCommunityOrganization;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author rsharan
 */
public class SamhsaServiceImpl extends BasePortalServiceServlet implements SamhsaService {
    
    private Logger logger = Logger.getLogger(getClass().getName());
    private SamhsaDAO samhsaDAO;
    private AuditDAO auditDAO;
    
    
     public void init() throws ServletException {
         logger.info("loading samhsaServiceImpl");
        ServletContext application = getServletContext();
        super.init();
        samhsaDAO = (SamhsaDAO) application.getAttribute(WebConstants.SAMHSA_DAO_KEY);
        userDAO = (UserDAO) application.getAttribute(WebConstants.USER_DAO_KEY);
        userCommunityOrganizationDAO = (UserCommunityOrganizationDAO) application.getAttribute(WebConstants.USER_COMMUNITY_ORGANIZATION_DAO_KEY);
        auditDAO = (AuditDAO) application.getAttribute(WebConstants.AUDIT_DAO_KEY);
        logger.info("finished loading samhsaServiceImpl");
    }
    
      @Override
    public String getActiveSamhsaContent(Long communityId) throws PortalException {


        try {
            Samhsa samhsa = samhsaDAO.findActiveSamhsa(communityId);
            
            return samhsa.getContent();
        } catch (Exception exc) {
            final String message = "Error retrieving SAMHSA content.";
            logger.logp(Level.SEVERE, getClass().getName(), "getActiveSamhsaContent", message, exc);

            throw new PortalException(message);
        }

    }
@Override
    public void updateUserSamhsaStatus(String email, Long community, boolean flag) throws PortalException {

        logger.info("START: updateUserSamhsaStatus()");

        try { 
//            User theUser = userDAO.findUserByEmail(email, community);
//            HttpServletRequest httpServletRequest = getThreadLocalRequest();
//            LoginResult loginResult = AuditUtils.getLoginResult(httpServletRequest);
////        User theUser = userDAO.findUserByEmail(email,loginResult.getCommunityId());                
////        LoginResult loginResult = ApplicationContextUtils.getLoginResult();
//
////            Calendar lastLoginDate = Calendar.getInstance();
////
////            Calendar currentDate = Calendar.getInstance();
//            
//            Date currentDate = new java.util.Date();
//            Date lastLoginDate = loginResult.getLastLoginDate();       
//
//            try {
//            comparison = (lastLoginDate).compareTo (currentDate);
//            } catch (Exception e) {
//            }
//            
//
//                 int comparison =lastLoginDate.compareTo(currentDate);
//                 
//            if(flag && comparison==0 ) {
//              
//                theUser.setSamhsaAccepted(Constants.YES_CHAR);
//            } else {
//                theUser.setSamhsaAccepted(Constants.NO_CHAR);
//            }
//
//            theUser.setSamhsaDate(new Date());
//
//            userDAO.update(theUser);
//
//            audit(theUser, flag);
//            logger.info("FINISHED: updateUserSamhsaStatus()");
//            
            
     //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
             User theUser = userDAO.findUserByEmail(email, community);
             UserCommunityOrganization uco = theUser.getUserCommunityOrganization(community);
            Date currentDate = new Date();
            if (flag) {
                uco.setSamhsaAccepted(new Character(Constants.YES_CHAR));
                theUser.setLastSuccessfulLoginDate(currentDate);
            } else {
                uco.setSamhsaAccepted(new Character(Constants.NO_CHAR));
            }
             
            uco.setSamhsaDate(currentDate);
            userDAO.update(theUser);

            audit(theUser, flag);
            logger.info("FINISHED: updateUserSamhsaStatus()");
            
     //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
            
            
        } catch (Exception exc) {
            final String message = "Error updating SAMHSA status for: " + email;
            logger.logp(Level.SEVERE, getClass().getName(), "updateUserSamhsaStatus", message, exc);

            throw new PortalException(message);
        }
    }
    
       
      private void audit(User theUser, boolean flag) {
        HttpServletRequest httpServletRequest = getThreadLocalRequest();
        LoginResult loginResult = AuditUtils.getLoginResult(httpServletRequest);

        logger.info(">> inside audit: loginResult = " + loginResult);

        Audit audit = AuditUtils.convertUser(loginResult, theUser);

        audit.setCommunityId(loginResult.getCommunityId());
        String comment = getComment(theUser, flag);
        audit.setActionComments(comment);

        String actionType = getActionType(flag);
        audit.setActionType(actionType);

        audit.setActionSuccess(Constants.YES);

        auditDAO.addAudit(audit);
        }
        
      private String getComment(User theUser, boolean flag) {
        StringBuilder comment = new StringBuilder();

        comment.append("User: " + theUser.getFirstName() + " " + theUser.getLastName());
        if (flag) {
            comment.append(" accepted SAMHSA.");
        } else {
            comment.append(" declined SAMHSA.");
        }

        return comment.toString();
        }   
        
         
          private String getActionType(boolean flag) {

        return flag ? AuditType.ACCEPTED_SAMHSA : AuditType.DECLINED_SAMHSA;
    }       
    }
    
    
    
    
    
    

