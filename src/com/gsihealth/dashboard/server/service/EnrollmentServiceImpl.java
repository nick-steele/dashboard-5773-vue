package com.gsihealth.dashboard.server.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.client.common.PersonInfo;
import com.gsihealth.dashboard.client.service.EnrollmentService;
import com.gsihealth.dashboard.common.*;
import com.gsihealth.dashboard.common.Constants;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.common.*;
import com.gsihealth.dashboard.server.dao.*;
import com.gsihealth.dashboard.server.jms.CoordinatorBrokerContext;
import com.gsihealth.dashboard.server.jms.JmsConstants;
import com.gsihealth.dashboard.server.jms.JmsUtils;
import com.gsihealth.dashboard.server.partner.PixClient;
import com.gsihealth.dashboard.server.partner.PixClientImpl;
import com.gsihealth.dashboard.server.service.integration.PatientUserNotification;
import com.gsihealth.dashboard.server.service.integration.UserService;
import com.gsihealth.dashboard.server.util.*;
import com.gsihealth.entity.*;
import com.gsihealth.patientmdminterface.entity.DuplicateCheckProcessResult;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import com.gsihealth.patientmdminterface.util.PatientUtils;
import org.apache.commons.lang.time.StopWatch;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

import javax.jms.*;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

//
/**
 *
 * @author Chad Darby
 */
public class EnrollmentServiceImpl extends BasePatientService implements EnrollmentService, EnrollmentPatientUpdater {
    private Logger logger = Logger.getLogger(getClass().getName());
    
    private CareManagementOrganizationHistoryDAO careManagementOrganizationHistoryDAO;
    private DisenrollmentReasonDAO disenrollmentReasonDAO;
    private EnrollmentServiceHelper enrollmentServiceHelper;
    private List<String> searchStatuses;
    private MyPatientListDAO myPatientListDAO;
    private ProgramNameHistoryDAO programNameHistoryDAO;
    protected PixClient pixClient;
    protected CountyDAO countyDAO;
    private AcuityScoreHistoryDAO acuityScoreHistoryDAO;
    private TaskHistoryDAO taskHistoryDAO;
    private ConfigurationDAO configurationDAO;
    private static ServletContext context;
    private ConsentHistoryDAO consentHistoryDAO;
    
    public EnrollmentServiceImpl() {
    }

    /**
     * Initializes the service with DAOs
     */
    @Override
    public void init() throws ServletException {
        logger.info("loading enrollmentServiceImpl");
        searchStatuses = new ArrayList<String>();

        searchStatuses.add(WebConstants.PENDING);
        searchStatuses.add(WebConstants.INACTIVE);
        super.init();

        ServletContext application = getServletContext();
        context = application;
        disenrollmentReasonDAO = (DisenrollmentReasonDAO) application.getAttribute(WebConstants.DISENROLLMENT_DAO_DEY);

        programNameHistoryDAO = (ProgramNameHistoryDAO) application.getAttribute(WebConstants.PROGRAM_NAME_HISTORY_DAO_KEY);

        //pe
        accessLevelDAO = (AccessLevelDAO) application.getAttribute(WebConstants.ACCESS_LEVEL_DAO_KEY);

        sSOUserDAO = (SSOUserDAO) application.getAttribute(WebConstants.SSO_USER_DAO_KEY);
        userService = (UserService) application.getAttribute(WebConstants.USER_SERVICE_KEY);
        userCommunityOrganizationDAO = (UserCommunityOrganizationDAO) application.getAttribute(WebConstants.USER_COMMUNITY_ORGANIZATION_DAO_KEY);

        myPatientListDAO = (MyPatientListDAO) application.getAttribute(WebConstants.MY_PATIENT_LIST_DAO_KEY);
        careManagementOrganizationHistoryDAO=(CareManagementOrganizationHistoryDAO) application.getAttribute(WebConstants.CARE_MANAGEMENT_ORGANIZATION_HISTORY_DAO_KEY);
        acuityScoreHistoryDAO=(AcuityScoreHistoryDAO)application.getAttribute(WebConstants.ACUITY_SCORE_HISTORY_DAO_KEY);
        communityOrganizationDAO=(CommunityOrganizationDAO)application.getAttribute(WebConstants.COMMUNITY_ORGANIZATION_DAO_KEY);
        patientUserNotification = (PatientUserNotification) application.getAttribute(WebConstants.PATIENT_USER_NOTIFICATION);
        taskHistoryDAO = (TaskHistoryDAO)application.getAttribute(WebConstants.TASK_HISTORY_DAO_KEY);
        consentHistoryDAO = (ConsentHistoryDAO) application.getAttribute(WebConstants.CONSENT_HISTORY_DAO_KEY);
        countyDAO = (CountyDAO) application.getAttribute(WebConstants.COUNTY_DAO_KEY);
        configurationDAO=super.configurationDAO;
        
        setupComparator();

        setupPixClient();

        
        //JIRA 629 - remove properties
        enrollmentServiceHelper = new EnrollmentServiceHelper(communityContexts,
                auditDAO, careteamDAO, notificationAuditDAO, organizationPatientConsentDAO, organizationDAO,
                userPatientConsentDAO, userDAO, patientEnrollmentHistoryDAO, disenrollmentReasonDAO, programNameHistoryDAO,
                accessLevelDAO, sSOUserDAO, userService, userCommunityOrganizationDAO, pixClient,
                patientUserNotification,careManagementOrganizationHistoryDAO,acuityScoreHistoryDAO,
                communityOrganizationDAO,configurationDAO,consentHistoryDAO);


        // store a reference to THIS service in application context
        application.setAttribute(WebConstants.ENROLLMENT_SERVICE_KEY, this);

        // config the broker context
        CoordinatorBrokerContext coordinatorBrokerContext = CoordinatorBrokerContext.getInstance();
        coordinatorBrokerContext.setEnrollmentPatientUpdater(this);
        logger.info("finished loading enrollmentServiceImpl");
    }
    

    public void handleMinorPatientsMessages(SearchCriteria searchCriteria, 
            int minorAge, int numberOfDays, long communityId){

        try {
             boolean isManageConsentWithoutEnrollment=Boolean.parseBoolean(configurationDAO.getProperty(communityId, "manage.consent.withut.enrollment","false"));
             boolean isAllOrgSelfAssert=Boolean.parseBoolean(configurationDAO.getProperty(communityId, "all.orgs.self.assert","false"));
       
            logger.info("search for patients turning " + minorAge + " in " 
                    + numberOfDays + " in community " + communityId);
            SearchResults<PersonDTO> searchResults = this.findGenericPatients(communityId,
                    searchCriteria,
                    true,
                    null, 0, 0, -1,isManageConsentWithoutEnrollment,
                    isAllOrgSelfAssert,1000);
            for(PersonDTO each : searchResults.getData()){
                AlertUtils.sendPatientAgeOfMajorityAlert(communityId, each, 
                    minorAge, numberOfDays, AlertConstants.GSI_CARETEAM_APP);
            }
            // update task table with results
        } catch (PortalException ex) {
            logger.warning(" search for minor patients failed for: " + ex);
        }
    }

    /**
     * ok, lets stop the madness, One Ring to rule them all, One Ring to find
     * them, One Ring to bring them all and in the darkness bind them this
     * method returns a paginated list of patients AND sets the total patient
     * count It can be used for enrolled or non-enrolled patients - set the
     * search param power/non-power users - if currentUserOrgId is null, don't
     * set that search param, if searchCriteria is null, don't use it.
     *
     * @param communityId
     * @param searchCriteria
     * @param enrolledMode
     * @param currentUserOrgId
     * @param pageNumber
     * @param pageSize
     * @param userId
     * @return paginated list of patients meeting the contained params
     */
    @Override
    public SearchResults<PersonDTO> findPatients(
            long communityId,
            SearchCriteria searchCriteria,
            boolean enrolledMode,
            Long currentUserOrgId,
            int pageNumber,
            int pageSize,
            long userId) throws PortalException {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        int maxPatientSearchResultsSize = Integer.parseInt(configurationDAO.getProperty(communityId, "max.patient.search.results.size", "1000"));
        boolean isManageConsentWithoutEnrollment=Boolean.parseBoolean(configurationDAO.getProperty(communityId, "manage.consent.withut.enrollment","false"));
        boolean isAllOrgSelfAssert=Boolean.parseBoolean(configurationDAO.getProperty(communityId, "all.orgs.self.assert","false"));
       
        SearchResults<PersonDTO> searchResults = this.findGenericPatients(communityId, searchCriteria,
                enrolledMode,
                currentUserOrgId,
                pageNumber,
                pageSize,
                userId,isManageConsentWithoutEnrollment,
                isAllOrgSelfAssert, maxPatientSearchResultsSize);

        return searchResults;
    }

    @Override
    public PersonInfo parseStringToPerson(String data) {
        System.out.println(data);
        PersonInfo personInfo = new PersonInfo();
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            personInfo = mapper.readValue(data, new TypeReference<PersonInfo>(){});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return personInfo;
    }

    @Override
    public LinkedHashMap<String, String> getCounty(long communityId) throws PortalException {
        LinkedHashMap<String, String> data = null;

        try{
            List<County> county = countyDAO.findCountys(communityId);

            data = new LinkedHashMap<String, String>();
            for (County temp : county) {
                String id = Long.toString(temp.getCountyPK().getCountyId());
                String name = temp.getCountyName();
                data.put(id, name);
            }
        } catch (Exception exc) {
            String message = "Error loading county.";
            logger.log(Level.SEVERE, message, exc);
            throw new PortalException(message);
        }

        return data;

    }

    /**
     * TODO - too many db searches, any way to get rid of some of them?
     *
     * @param communityId
     * @param searchCriteria
     * @param enrolledMode
     * @param currentUserOrgId
     * @param userId
     * @return total number of patients that meet searchCriteria
     */
    @Override
    public SearchPatientResults countPatients(long communityId,
            SearchCriteria searchCriteria,
            boolean enrolledMode,
            Long currentUserOrgId,
            long userId) throws PortalException {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        int maxPatientSearchResultsSize = Integer.parseInt(configurationDAO.getProperty(communityId, "max.patient.search.results.size", "1000"));
        boolean isManageConsentWithoutEnrollment=Boolean.parseBoolean(configurationDAO.getProperty(communityId, "manage.consent.withut.enrollment","false"));
        boolean isAllOrgSelfAssert=Boolean.parseBoolean(configurationDAO.getProperty(communityId, "all.orgs.self.assert","false"));
       
        SearchPatientResults patientCount = this.countGenericPatients(communityId,
                searchCriteria,
                enrolledMode,
                currentUserOrgId,
                userId,
                isManageConsentWithoutEnrollment,
                isAllOrgSelfAssert,
                maxPatientSearchResultsSize);

        return patientCount;
    }

    public void setupPixClient() {
        pixClient = new PixClientImpl();
    }

    public void setupComparator() {
        personDTOComparator = new PersonDTOComparator();
    }

    protected LoginResult getLoginResult() {
        LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());

        return loginResult;
    }

    protected void runUpdatePatientProcessesForCareteamAssignment(LoginResult loginResult, Person thePerson, PersonDTO personDTO, String oldEnrollmentStatus, String newCareTeamId, String oldCareTeamId, String oldProgramLevelConsentStatus, String programLevelConsentStatus, Person oldPerson, long oldOrganizationId, PatientInfo patientInfo) throws NumberFormatException {

        logger.info("Begin: runUpdatePatientProcessesForCareteamAssignment");

        // add code to save careteam patient info

        //savePatientInfo(patientInfo);
        //check if patient user flag to save 
        if (!patientInfo.isSavePatientUserFlag()) {
            // set new careteam to patientInfo
             if(newCareTeamId != null && !newCareTeamId.isEmpty() && Long.parseLong(newCareTeamId) > 0 ){
                patientInfo.setCommunityCareteamId(Long.parseLong(newCareTeamId));
            }            
            savePatientInfo(patientInfo);
        }

        // standard update process
        runUpdatePatientProcessesAsync(loginResult, thePerson, personDTO, oldEnrollmentStatus, newCareTeamId, oldCareTeamId, oldProgramLevelConsentStatus, programLevelConsentStatus, oldPerson, oldOrganizationId);

        // check for care team alerts        
        String oldCareTeamName = "OLD";
        String newEnrollmentStatus = patientInfo.getPatientEnrollment().getStatus();

        long communityCareteamId = patientInfo.getCommunityCareteamId();

        long communityId = loginResult.getCommunityId();
        checkForCareteamAlerts(communityId, communityCareteamId, personDTO, oldEnrollmentStatus, newEnrollmentStatus, oldCareTeamName);

        logger.info("End: runUpdatePatientProcessesForCareteamAssignment");
    }

    protected void runUpdatePatientProcessesAsync(LoginResult loginResult, Person thePerson, PersonDTO personDTO, String oldEnrollmentStatus, String newCareTeamId, String oldCareTeamId, String oldProgramLevelConsentStatus, String programLevelConsentStatus, Person oldPerson, long oldOrganizationId) throws NumberFormatException {

        // send to JMS queue
        logger.info("Begin: runUpdatePatientProcessesAsync - Sending message to Coordinator JMS Queue");

        CoordinatorBrokerContext coordinatorBrokerContext = CoordinatorBrokerContext.getInstance();

        ConnectionFactory connectionFactory = coordinatorBrokerContext.getConnectionFactory();
        javax.jms.Queue queue = coordinatorBrokerContext.getEnrollmentPatientUpdateQueue();

        if (connectionFactory == null) {
            logger.warning(">>> Not sending message. Connection Factory is null.");
            return;
        }

        Connection connection = null;
        Session session = null;
        MessageProducer producer = null;

        try {

            connection = connectionFactory.createConnection();
            session = connection.createSession(false, javax.jms.Session.AUTO_ACKNOWLEDGE);
            producer = session.createProducer(queue);

            javax.jms.ObjectMessage objectMessage = session.createObjectMessage();
            populateObjectMessage(objectMessage, loginResult, thePerson, personDTO, oldEnrollmentStatus, newCareTeamId, oldCareTeamId, oldProgramLevelConsentStatus, programLevelConsentStatus, oldPerson, oldOrganizationId);

            // send message
            logger.info("Sending message to " + queue.getQueueName());
            producer.send(objectMessage);
            logger.info("Finished sending message to " + queue.getQueueName());
        } catch (Exception exc) {
            final String message = "Error sending patient update message to Coordinator Enrollment Patient Update queue";

            logger.logp(Level.SEVERE, getClass().getName(), "updatePatient", message, exc);
        } finally {
            JmsUtils.close(producer, session, connection);
        }

        logger.info("End: runUpdatePatientProcessesAsync - Sending message to Coordinator JMS Queue");

    }

    /**
     * FIXME - delete references to plcs
     * @param loginResult
     * @param thePerson
     * @param personDTO
     * @param oldEnrollmentStatus
     * @param newCareTeamId
     * @param oldCareTeamId
     * @param oldProgramLevelConsentStatus
     * @param programLevelConsentStatus
     * @param oldPerson
     * @param oldOrganizationId
     * @throws PortalException 
     */
    @Override
    public void runUpdatePatientProcessesSynchronous(LoginResult loginResult, Person thePerson, 
            PersonDTO personDTO, String oldEnrollmentStatus, String newCareTeamId, String oldCareTeamId, 
            String oldProgramLevelConsentStatus, String programLevelConsentStatus, Person oldPerson, 
            long oldOrganizationId) throws PortalException {

        logger.info("Begin: runUpdatePatientProcessesSynchronous");
        long communityId = loginResult.getCommunityId();
        long patientId = thePerson.getPatientCommunityEnrollment().getPatientId();

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        //  TODO: SKIP IF POWERUSER
        // 
        synchMyPatientList(patientId, communityId);

        stopWatch.stop();
        logger.info("[synchMyPatientList] Time to synchMyPatientList: " + stopWatch.getTime() / 1000.0 + " secs");
        
        // send update notification
        sendUpdateNotifications(loginResult, thePerson, personDTO, oldEnrollmentStatus, newCareTeamId, oldCareTeamId, oldProgramLevelConsentStatus, programLevelConsentStatus);

        logger.info("Begin sending alerts");

        // send alert for patient care team assigned
        boolean newCareTeamAssigned = isNewCareTeamAssigned(newCareTeamId, oldCareTeamId);

        if (newCareTeamAssigned) {
            // send alert for patient assigned to care team
            long communityCareteamId = Long.parseLong(newCareTeamId);

            enrollmentServiceHelper.sendPatientCareteamAssignedAlert(communityId, personDTO, communityCareteamId);
        }

        String newEnrollStatus = thePerson.getPatientCommunityEnrollment().getStatus();

        // send alert for enrollments patientUserActiveStatus changed
        if (newEnrollStatus != null && oldEnrollmentStatus!=null) {
            if (!(newEnrollStatus.equalsIgnoreCase(oldEnrollmentStatus))) {
                AlertUtils.sendPatientEnrolledStatusChangedAlert(communityId, personDTO, newEnrollStatus);
            }
        }

        // send alert for consent patientUserActiveStatus changed
        boolean hasConsentChanged = ConsentUtils.hasConsentChangedForBHIX(oldProgramLevelConsentStatus, programLevelConsentStatus);


        // send alert for patient demographics changed
        boolean demogChanged = EnrollmentUtils.hasDemographicDataChanged(thePerson, oldPerson);
        if (demogChanged) {
            AlertUtils.sendPatientDemographicsChangedAlert(communityId, personDTO);
        }

        // send alert for patient care team removed
        if (oldEnrollmentStatus != null && newEnrollStatus != null) {
            if ("ASSIGNED".equalsIgnoreCase(oldEnrollmentStatus) && !newEnrollStatus.equalsIgnoreCase(oldEnrollmentStatus) && (oldCareTeamId != null)) {

                long oldCareTeamIdLong = Long.parseLong(oldCareTeamId);
                boolean isValidOldCareteam = oldCareTeamIdLong > -1;

                if (isValidOldCareteam) {
                    // get old care team name
                    CommunityCareteam oldCareTeam = careteamDAO.findCommunityCareteam(oldCareTeamIdLong);
                    AlertUtils.sendPatientCareteamRemovedAlert(communityId, personDTO, oldCareTeam.getName());
                }
            }
        }

        logger.info("Finished sending alerts");

        logger.info("End: runUpdatePatientProcessesSynchronous");
    }
    
    private void handleConsentSynch(Person thePerson, String programLevelConsentStatus, long oldOrganizationId, long communityId) {
        // handle organization patient consent synch
        PatientCommunityEnrollment patientEnrollment = thePerson.getPatientCommunityEnrollment();

        long patientId = patientEnrollment.getPatientId();

        // update dynamic consent info
        long newOrganizationId = patientEnrollment.getOrgId();
        logger.info("handleConsentSynch for old org " + oldOrganizationId + " new org: " + newOrganizationId);
        try {
            // if diff orgs, then update consent
            if (newOrganizationId != oldOrganizationId) {
                PatientConsentUtils.updateOrganizationPatientConsent(organizationPatientConsentDAO, userPatientConsentDAO,
                        newOrganizationId, oldOrganizationId, patientId, communityId);
            }
            String patientEnrollmentStatus = patientEnrollment.getStatus();
            // synch consent
            synchOrganizationPatientConsent(this.getPatientEnrollmentDAO(communityId), patientId,communityId, programLevelConsentStatus, newOrganizationId,
                    patientEnrollmentStatus, this.getCommunityOrgId(communityId));
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Error during synch patient organization", ex);
        }

        logger.info("End: handleConsentSynch");
    }

    protected void sendUpdateNotifications(LoginResult loginResult, Person thePerson, PersonDTO personDTO, String oldEnrollmentStatus, String newCareTeamId, String oldCareTeamId, String oldProgramLevelConsentStatus, String programLevelConsentStatus) {
        try {
            logger.info("Begin: sendUpdateNotifications");

            // run PIX client for PIX Revise to BHIX: Point-to-Point
            enrollmentServiceHelper.sendPixClientMessage(thePerson, loginResult, WebConstants.PIX_REVISE);

            // run notification on background thread for patient updates
            enrollmentServiceHelper.sendUpdateNotificationToNotificationManager(personDTO, loginResult.getCommunityId());

            // send notification for care team and enrollment
            enrollmentServiceHelper.processCareteamAndEnrollmentNotificationsToNotificationManager(oldEnrollmentStatus, personDTO, newCareTeamId, oldCareTeamId, loginResult.getCommunityId(), loginResult.getCommunityOid());


        } catch (Exception exc) {
            logger.logp(Level.WARNING, getClass().getName(), "updatePatient", "Error sending notifications.", exc);
        }

        logger.info("End: sendUpdateNotifications");

    }

    private boolean isInvalidStatus(List<String> statuses, String tempStatus) {
        return !statuses.contains(tempStatus);
    }

    //JIRA 2523
    @Override
    public DuplicateCheckProcessResult checkForDuplicates(SimplePerson person, Long communityId) throws PortalException
    {
        try {
            return enrollmentServiceHelper.checkForDuplicates(person, communityId);
        }
        catch (Exception exc) {
            final String message = "Error adding patient. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "addPatient", message, exc);
            throw new PortalException("Error adding patient.");
        }
    }

    @Override
    public PersonDTO findPatient(SimplePerson sp, long communityId, Long currentUserOrgId) throws PortalException 
    {
        try {
            return super.loadPersonDTO(sp, communityId, currentUserOrgId);
        }
        catch (Exception exc) {
            final String message = "Error finding patient. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "findPatient", message, exc);
            throw new PortalException("Error retrieving patient.");
        }
    }
    
    @Override
    public PersonDTO addPatient(PersonDTO personDTO) throws PortalException {
        return addPatient(personDTO, Long.MIN_VALUE);
    }

    @Override
    public PersonDTO addPatient(PersonDTO personDTO, long communityCareteamId) throws PortalException {
    	long patientId = 0;
        try {
            LoginResult loginResult = AuditUtils.getLoginResult(getThreadLocalRequest());

            patientId = enrollmentServiceHelper.addPatientHelper(personDTO, communityCareteamId, loginResult).getPatientId();
            return findOnePatient(patientId, loginResult);
            
        } catch (DuplicateException exc) {
            final String message = exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "addPatient", message, exc);
            throw new PortalException(message);
        } catch (DuplicatePatientException exc) {
            final String message = exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "addPatient", message, exc);
            throw new PortalException(message);
        } catch (DuplicatePatientMedicaidMedicareIDException exc) {

            final String message = "Error adding patient. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "addPatient", message, exc);
            throw new PortalException(message);
        } catch (Exception exc) {
            final String message = "Error adding patient. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "addPatient", message, exc);
            throw new PortalException("Error adding patient.");
        }
    }

    @Override
    public void updatePatientFromPatientLoader(LoginResult thePatientLoaderLoginResult, PersonDTO personDTO, boolean checkEnableCareteam,
            boolean checkForDuplicate, boolean checkForDuplicateSsn, boolean primaryMedicaidcareIdBln,
            boolean secondaryMedicaidcareIdBln, String oldProgramLevelConsentStatus, String oldEnrollmentStatus,
            String newCareTeamId, String oldCareTeamId, long oldOrganizationId, boolean checkPatientUserUpdate,String oldAcuityScore) throws PortalException {
        updatePatient(thePatientLoaderLoginResult, personDTO, checkEnableCareteam, checkForDuplicate, checkForDuplicateSsn,
                primaryMedicaidcareIdBln, secondaryMedicaidcareIdBln, oldProgramLevelConsentStatus, oldEnrollmentStatus,
                newCareTeamId, oldCareTeamId, oldOrganizationId, null, checkPatientUserUpdate,oldAcuityScore);
    }

    @Override
    public void updatePatient(PersonDTO personDTO, boolean checkEnableCareteam, boolean checkForDuplicate, boolean checkForDuplicateSsn,
            boolean primaryMedicaidcareIdBln, boolean secondaryMedicaidcareIdBln, String oldProgramLevelConsentStatus,
            String oldEnrollmentStatus, String newCareTeamId, String oldCareTeamId, long oldOrganizationId,
            boolean checkPatientUserUpdate,String oldAcuityScore) throws PortalException {
        updatePatient(null, personDTO, checkEnableCareteam, checkForDuplicate, checkForDuplicateSsn, primaryMedicaidcareIdBln,
                secondaryMedicaidcareIdBln, oldProgramLevelConsentStatus, oldEnrollmentStatus, newCareTeamId, oldCareTeamId,
                oldOrganizationId, null, checkPatientUserUpdate,oldAcuityScore);
    }

    /**
     * FIXME get rid of all these separate update patients
     * @param thePatientLoaderLoginResult
     * @param personDTO
     * @param checkEnableCareteam
     * @param checkForDuplicate
     * @param checkForDuplicateSsn
     * @param primaryMedicaidcareIdBln
     * @param secondaryMedicaidcareIdBln
     * @param oldProgramLevelConsentStatus
     * @param oldEnrollmentStatus
     * @param newCareTeamId
     * @param oldCareTeamId
     * @param oldOrganizationId
     * @param patientInfo
     * @param checkPatientUserUpdate
     * @param oldAcuityScore
     * @throws PortalException 
     */
    protected void updatePatient(LoginResult thePatientLoaderLoginResult, PersonDTO personDTO, boolean checkEnableCareteam,
            boolean checkForDuplicate, boolean checkForDuplicateSsn, boolean primaryMedicaidcareIdBln, boolean secondaryMedicaidcareIdBln,
            String oldProgramLevelConsentStatus, String oldEnrollmentStatus, String newCareTeamId, String oldCareTeamId, long oldOrganizationId,
            PatientInfo patientInfo, boolean checkPatientUserUpdate,String oldAcuityScore) throws PortalException {

        LoginResult loginResult = thePatientLoaderLoginResult;
        
        if (thePatientLoaderLoginResult == null) {
            loginResult = getLoginResult();
        }
        Long communityId = loginResult.getCommunityId();

        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        Person updatedPerson = mapper.map(personDTO, Person.class);
        String dobstr = PatientUtils.convertDateToStr(updatedPerson.getDateOfBirth());
        updatedPerson.getSimplePerson().setBirthDateTime(dobstr);

        try {
            // get the old patient so we can check demogs
            SimplePerson sp = this.getPatientManager(communityId).getPatient(updatedPerson.getEuid());
            Person oldPerson = new Person(sp);

            enrollmentServiceHelper.updatePatientHelper(loginResult, personDTO, oldEnrollmentStatus, checkForDuplicate,
                    checkForDuplicateSsn, primaryMedicaidcareIdBln, secondaryMedicaidcareIdBln, checkEnableCareteam,
                    patientInfo, oldOrganizationId, updatedPerson,oldAcuityScore);

            logger.info("patientInfo=" + patientInfo);
            String newProgramLevelConsentStatus = updatedPerson.getPatientCommunityEnrollment().getProgramLevelConsentStatus();
            runUpdatePatientProcessesAsync(loginResult, updatedPerson, personDTO, oldEnrollmentStatus, newCareTeamId, oldCareTeamId,
                        oldProgramLevelConsentStatus, newProgramLevelConsentStatus, oldPerson, oldOrganizationId);

            logger.info("Updated patient " + personDTO.getPatientEnrollment().getPatientId() + " successfully.");

        } catch (DuplicateException exc) {
            final String message = exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "updatePatient", message, exc);
            throw new PortalException(message);
        } catch (DuplicatePatientException exc) {
            final String message = exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "updatePatient", message, exc);
            throw new PortalException(message, exc.getDuplicateCheckProcessResult());
        } catch (DuplicatePatientMedicaidMedicareIDException exc) {
            final String message = "Error updating patient. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "updatePatient", message, exc);
            throw new PortalException(message);
        } catch (Throwable exc) {
            final String message = "Error updating patient. " + exc.getMessage();
            logger.logp(Level.SEVERE, getClass().getName(), "updatePatient", message, exc);

            throw new PortalException("Error updating patient.");
        }
    }
    
    @Override
    public PersonDTO updatePatientBySelfAssertion(PersonDTO personDTO) throws PortalException {
        LoginResult loginResult = getLoginResult();
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        Person updatedPerson = mapper.map(personDTO, Person.class);
        PatientCommunityEnrollment pce = updatedPerson.getPatientCommunityEnrollment();
        Long userOrgId = this.getLoginResult().getOrganizationId();
        logger.info("user orgId: " + userOrgId + " for patient  " + updatedPerson.getPatientId());
        try{
            this.enrollmentServiceHelper.addAndUpdateOrganizationPatientConsent(null, userOrgId, pce, pce.getCommunityId());
        } catch (Exception e) {
           logger.logp(Level.SEVERE, getClass().getName(), "updatePatientBySelfAssertion", "updatePatientBySelfAssertion", e);
            throw new PortalException("Could not set self assertion for this patient"); 
        }

        String comment = "Update Patient via Self Assert: " + personDTO.getFirstName() + " " + personDTO.getLastName();
        audit(updatedPerson, comment, AuditType.UPDATE_PATIENT_VIA_SELF_ASSERT, loginResult, Constants.YES);

        return personDTO;

    }

    private List<PersonDTO> findGenericCandidatesEnrolledPatients(long communityId, Map<String, PatientCommunityEnrollment> patientEnrollments) throws Exception {

        List<PersonDTO> searchResults = new ArrayList<PersonDTO>();
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        // get the patient enrollment
        Set<Map.Entry<String, PatientCommunityEnrollment>> entries = patientEnrollments.entrySet();

        for (Map.Entry<String, PatientCommunityEnrollment> tempEntry : entries) {
            String euid = tempEntry.getKey();
            PatientCommunityEnrollment tempPatientEnrollment = tempEntry.getValue();

            Person tempPerson;

            try {
                tempPerson = new Person(this.getPatientManager(communityId).getPatient(euid));
            } catch (Exception exc) {
                // skip this patient
                String message = "Could not find patient ID in MDM: " + euid;
                logger.log(Level.SEVERE, message, exc);
                continue;
            }

            tempPerson.setPatientCommunityEnrollment(tempPatientEnrollment);

            // check if patient id is in patient_community_careteam
            long patientId = tempPatientEnrollment.getPatientId();

            boolean exists = careteamDAO.doesPatientCommunityCareteamExist(patientId);

            // if so, then get care team id
            // - get care team name
            if (exists) {
                CommunityCareteam communityCareteam = careteamDAO.findCommunityCareteamForPatient(patientId);

                String careteamName = communityCareteam.getName();
                tempPerson.setCareteam(careteamName);
            }

            // get organization name
            long orgId = tempPatientEnrollment.getOrgId();
            String orgName = organizationDAO.getOrganizationName(orgId);

            tempPatientEnrollment.setOrganizationName(orgName);

            PersonDTO personDto = mapper.map(tempPerson, PersonDTO.class);
            setPatientRelationship(communityId, tempPerson, personDto);

            //Program health home
            //setProgramHealthHome(tempPerson, personDto);

            searchResults.add(personDto);
        }

        Collections.sort(searchResults, personDTOComparator);

        if (searchResults != null) {
            logger.info("searchResults size is " + searchResults.size());
        }
        return searchResults;
    }

    private void audit(LoginResult loginResult, Person person, String comment, String auditType) {
        // audit
        audit(person, comment, auditType, loginResult);
    }

    private void audit(Person person, String comment, String auditType, LoginResult loginResult) {
        audit(person, comment, auditType, loginResult, Constants.YES);
    }

    private void audit(Person person, String comment, String auditType, LoginResult loginResult, String actionSuccess) {
        audit(person, comment, auditType, loginResult, actionSuccess, null);
    }

    public void updatePatientCareteam(long communityCareteamId, long patientId) throws PortalException {

        try {
            careteamDAO.update(communityCareteamId, patientId);
        } catch (Exception exc) {
            final String message = "Error updating patient care team.";
            logger.logp(Level.SEVERE, getClass().getName(), "updatePatientCareteam", message, exc);

            throw new PortalException(message);
        }
    }

    public void addPatientCareteam(long communityCareteamId, long patientId) throws PortalException {

        try {
            enrollmentServiceHelper.addPatientCareteam(communityCareteamId, patientId);
        } catch (Exception exc) {
            final String message = "Error adding patient care team.";
            logger.logp(Level.SEVERE, getClass().getName(), "addPatientCareteam", message, exc);

            throw new PortalException(message);
        }
    }

    public void removePatientCareteam(long patientId, long communityId) throws PortalException {
        try {
            careteamDAO.remove(patientId);
            removeProviderAssignedToPatient(patientId, communityId);
        } catch (Exception exc) {
            final String message = "Error updating patient care team.";
            logger.logp(Level.SEVERE, getClass().getName(), "removePatientCareteam", message, exc);

            throw new PortalException(message);
        }
    }


    @Override
    public void updatePatientForCareteamAssignment(PersonDTO thePerson, boolean checkEnableCareteam, boolean checkForDuplicate, boolean checkForDuplicateSsn, boolean primaryMedicaidcareId, boolean secondaryMedicaidcareId, String oldProgramLevelConsentStatus, String oldEnrollmentStatus, String careTeamIdNew, String careTeamIdOld, long oldOrganizationId, PatientInfo patientInfo, boolean checkPatientUserUpdate,String oldAcuityScore) throws PortalException {

        // do our work
        updatePatient(null, thePerson, checkEnableCareteam, checkForDuplicate, checkForDuplicateSsn, primaryMedicaidcareId, secondaryMedicaidcareId, oldProgramLevelConsentStatus, oldEnrollmentStatus, careTeamIdNew, careTeamIdOld, oldOrganizationId, patientInfo, checkPatientUserUpdate,oldAcuityScore);

    }

    private void checkForCareteamAlerts(long communityId, long communityCareteamId, PersonDTO personDTO, String oldEnrollmentStatus, String newEnrollmentStatus, String oldCareteamName) {

        logger.info("Begin: checkForCareteamAlerts");

        // send patient alert
        if (communityCareteamId > 0) {
            // send alert for patient assigned to care team
            enrollmentServiceHelper.sendPatientCareteamAssignedAlert(communityId, personDTO, communityCareteamId);
        }

        // send alert for patient care team removed
        if (oldEnrollmentStatus.equalsIgnoreCase("ASSIGNED") && !oldEnrollmentStatus.equalsIgnoreCase(newEnrollmentStatus)) {
            AlertUtils.sendPatientCareteamRemovedAlert(communityId, personDTO, oldCareteamName);
        }

        logger.info("End: checkForCareteamAlerts");
    }

    private void savePatientInfo(PatientInfo patientInfo) {

        logger.info("Begin: savePatientInfo");

        long communityCareteamId = patientInfo.getCommunityCareteamId();
        long patientId = patientInfo.getPatientId();
        String careteamName = patientInfo.getCareteamName();
              
         // Spira - 6835
            // if status is changed from assigned to enrolled the existing careteam should be updated with end date
            if(patientInfo.isCareTeamEnd()){
                System.out.println("updateCateTeamEndDate" );
                careteamDAO.updateCateTeamEndDate(patientId);
            } else {
            // remove old care team assignment
//            careteamDAO.remove(patientId);
             careteamDAO.update(communityCareteamId, patientId);
            
        }

        logger.info("End: savePatientInfo");
    }

    @Override
    public void checkIfDuplicatePatient(PersonDTO personDTO, long communityId) throws PortalException {
        String duplicatePatientOrg = null;
        String duplicateEnrollmentStatus = null;
        logger.info("Checking if the patient is duplicate...");
        PatientCommunityEnrollment duplicateEnrollment;

        try {
            Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

            Person thePerson = mapper.map(personDTO, Person.class);
            String duplicatePatientEUID = this.getPatientManager(communityId).getDuplicatePatientEuid(thePerson.getSimplePerson());

            if (duplicatePatientEUID != null) {
                duplicateEnrollment = this.getPatientEnrollmentDAO(communityId).findPatientEnrollment(duplicatePatientEUID);

                if (duplicateEnrollment != null) {
                    Long orgId = duplicateEnrollment.getOrgId();
                    duplicatePatientOrg = organizationDAO.findOrganization(orgId, communityId).getOrganizationCwdName();
                    duplicateEnrollmentStatus = duplicateEnrollment.getStatus();
                    logger.info("Patient is duplicate...");
                    throw new DuplicateException("No duplicate patient record found.");
                } else {
                    logger.info("Patient is not a duplicate...");
                    throw new PatientNotDuplicateException("No duplicate patient record found!");
                }

            } else {
                logger.info("Patient is not a duplicate...");
                throw new PatientNotDuplicateException("No duplicate patient record found!");
            }
        } catch (DuplicateException exc) {

            final String message = "A patient with very similar details already exists in the system. That patient is assigned to: " + duplicatePatientOrg + ".";

            logger.logp(Level.SEVERE, getClass().getName(), "updatePatient", message, exc);
            throw new PortalException(message);
        } catch (PatientNotDuplicateException exc) {

            final String message = "No duplicate patient record found.";
            logger.logp(Level.SEVERE, getClass().getName(), "updatePatient", message, exc);
            throw new PortalException(exc.getMessage());
        } catch (Exception exc) {

            throw new PortalException("Error updating patient.");

        }

    }

    private void populateObjectMessage(ObjectMessage objectMessage, LoginResult loginResult, Person thePerson, PersonDTO personDTO, String oldEnrollmentStatus, String newCareTeamId, String oldCareTeamId, String oldProgramLevelConsentStatus, String programLevelConsentStatus, Person oldPerson, long oldOrganizationId) throws JMSException {
        HashMap<String, Object> map = new HashMap<String, Object>();

        map.put(JmsConstants.LOGIN_RESULT_KEY, loginResult);
        map.put(JmsConstants.PERSON_KEY, thePerson);
        map.put(JmsConstants.PERSON_DTO_KEY, personDTO);
        map.put(JmsConstants.OLD_ENROLLMENT_STATUS_KEY, oldEnrollmentStatus);
        map.put(JmsConstants.NEW_CARE_TEAM_ID_KEY, newCareTeamId);
        map.put(JmsConstants.OLD_CARE_TEAM_ID_KEY, oldCareTeamId);
        map.put(JmsConstants.OLD_PROGRAM_LEVEL_CONSENT_STATUS_KEY, oldProgramLevelConsentStatus);
        map.put(JmsConstants.PROGRAM_LEVEL_CONSENT_STATUS_KEY, programLevelConsentStatus);
        map.put(JmsConstants.OLD_PERSON_KEY, oldPerson);
        map.put(JmsConstants.OLD_ORGANIZATION_ID_KEY, oldOrganizationId);

        objectMessage.setObject(map);
    }

    private void addToEnrollmentHistory(PatientCommunityEnrollment enrollment, long patientId, LoginResult loginResult) {

        logger.info("Adding patient's enrollment status info to patient_enrollment_history.....");

        PatientEnrollmentHistory enrollmentHistory = new PatientEnrollmentHistory();

        enrollmentHistory.setCommunityId(loginResult.getCommunityId());
        enrollmentHistory.setPatientId(patientId);
        enrollmentHistory.setEnrollmentChangeDate(new Date());
        enrollmentHistory.setEnrollmentStatus(enrollment.getStatus());
        enrollmentHistory.setUserId(loginResult.getUserId());
        enrollmentHistory.setPatientOrgId(enrollment.getOrgId());

        enrollmentHistory.setAction("modify");

        String reason = enrollment.getReasonForInactivation();
        if (reason != null) {
            enrollmentHistory.setReasonForInactivationId((disenrollmentReasonDAO.getDisenrollmentReason(loginResult.getCommunityId(), reason).getDisenrollmentReasonsPK().getId()));
        }

        patientEnrollmentHistoryDAO.addToEnrollmentHistory(enrollmentHistory);
    }

//    private void addToProgramNameHistory(PatientEnrollment enrollment, long patientId, LoginResult loginResult) {
//        logger.info("Adding patient's program name info to program_name_history...");
//
//        //ProgramNameHistory programNameHistory = new ProgramNameHistory();
//
//        long communityId = loginResult.getCommunityId();
//
//
//        ProgramNameHistory programNameHistory = new ProgramNameHistory();
//        programNameHistory.setCommunityId(communityId);
//
//
//        programNameHistory.setPatientId(patientId);
//        programNameHistory.setCurrentProgramName(enrollment.getProgramName());
//
//        // set program effective date
//        if (enrollment.getProgramNameEffectiveDate() != null) {
//            programNameHistory.setProgramEffectiveDate(enrollment.getProgramNameEffectiveDate());
//        }
//
//        // set program end date
//        if (enrollment.getProgramEndDate() != null) {
//            programNameHistory.setProgramEndDate(enrollment.getProgramEndDate());
//        }
//        programNameHistory.setUserId(loginResult.getUserId());
//        programNameHistory.setAction("Updated Patient");
//
//        // adding to DAO
//        programNameHistoryDAO.addToProgramNameHistory(programNameHistory);
//
//    }
    @Override
    public void checkIfDuplicatePatientModify(PersonDTO personDTO, boolean checkForDuplicateBln, long communityId) throws PortalException {

        String duplicatePatientOrg = null;
        String duplicateEnrollmentStatus = null;
        logger.info("Checking if the patient is duplicate...");
        PatientCommunityEnrollment duplicateEnrollment;

        try {
            Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

            Person thePerson = mapper.map(personDTO, Person.class);
            //Spira 7256
            String dobstr = PatientUtils.convertDateToStr(thePerson.getDateOfBirth());
            thePerson.getSimplePerson().setBirthDateTime(dobstr);
            
            String duplicatePatientEUID = this.getPatientManager(communityId).getDuplicatePatientEuid(thePerson.getSimplePerson());

            if (duplicatePatientEUID != null) {

                if (duplicatePatientEUID.equalsIgnoreCase(personDTO.getEuid())) {

                    throw new PatientNotDuplicateException("Patient is not a duplicate.");
                } else {
                    duplicateEnrollment = this.getPatientEnrollmentDAO(communityId).findPatientEnrollment(duplicatePatientEUID);

                    if (duplicateEnrollment != null) {
                        Long orgId = duplicateEnrollment.getOrgId();
                        duplicatePatientOrg = organizationDAO.findOrganization(orgId, communityId).getOrganizationCwdName();
                        duplicateEnrollmentStatus = duplicateEnrollment.getStatus();
                        logger.info("Patient is a duplicate...");
                        throw new DuplicateException("duplicate Patient.");
                    } else {
                        logger.info("Patient is not a duplicate...");
                        throw new PatientNotDuplicateException("No duplicate patient record found.");
                    }
                }
            } else {
                logger.info("Patient is not a duplicate...");
                throw new PatientNotDuplicateException("No duplicate patient record found.");
            }

        } catch (DuplicateException exc) {

            final String message = "A patient with very similar details already exists in the system. That patient is assigned to: " + duplicatePatientOrg + ".";

            logger.logp(Level.SEVERE, getClass().getName(), "updatePatient", message, exc);
            throw new PortalException(message);
        } catch (PatientNotDuplicateException exc) {

            final String message = "No duplicate patient record found.";
            logger.logp(Level.SEVERE, getClass().getName(), "updatePatient", message, exc);
            throw new PortalException(exc.getMessage());
        } catch (Exception exc) {

            //final String message = "Error updating patient. " + exc.getMessage();
            //logger.logp(Level.SEVERE, getClass().getName(), "updatePatient", message, exc);
            throw new PortalException("Error updating patient.");

        }
    }

    public int getTotalCountForZero() throws PortalException {
        return 0;
    }

    private void setPatientUserIsActive(String status, PersonDTO personDTO) {
        logger.info(">>>setPatientUserIsActive--personDTO---------------------");

        String patientUserActive = personDTO.getPatientUserActive();

        // don't process null data
        if (status == null || patientUserActive == null) {
            return;
        }

        if (status.equalsIgnoreCase("None") && patientUserActive.equalsIgnoreCase("None")) {
            personDTO.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_NONE);
        } else if (status.equalsIgnoreCase("None") && patientUserActive.equalsIgnoreCase("Active")) {
            personDTO.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_ACTIVE);
        } else if (status.equalsIgnoreCase("Active") && patientUserActive.equalsIgnoreCase("Inactive")) {
            personDTO.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_INACTIVE);
        } else if (status.equalsIgnoreCase("Active") && patientUserActive.equalsIgnoreCase("Active")) {
            personDTO.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_ACTIVE);
        } else if (status.equalsIgnoreCase("Inactive") && patientUserActive.equalsIgnoreCase("Active")) {
            personDTO.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_ACTIVE);
        } else if (status.equalsIgnoreCase("Inactive") && patientUserActive.equalsIgnoreCase("Inactive")) {
            personDTO.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_INACTIVE);
        } else if (status.equalsIgnoreCase("Active") && patientUserActive.equalsIgnoreCase("None")) {
            personDTO.setPatientUserActive(com.gsihealth.dashboard.client.util.Constants.PATINET_INACTIVE);
        }
    }

    public void removeProviderAssignedToPatient(long patientId, long communityId) throws PortalException {

        try {
            List<NonHealthHomeProviderAssignedPatient> nhhpap = nonHealthHomeProviderDAO.getProviderAssignedToPatient(patientId, communityId);
            for (NonHealthHomeProviderAssignedPatient nonHealthHomeProviderAssignedPatient : nhhpap) {
                nonHealthHomeProviderDAO.unAssignProvider(nonHealthHomeProviderAssignedPatient);
            }
        } catch (Exception exc) {
            final String message = "Error removing provider.";
            logger.logp(Level.SEVERE, getClass().getName(), "removeProviderAssignedToPatient", message, exc);

            throw new PortalException(message);
        }
    }

    @Override
    public void synchMyPatientList(long patientId, long communityId) throws PortalException {

        try {
            logger.info("started: EnrollmentServiceImpl.synchMyPatientList");

            // get list of all users with this patient on their list
            List<Long> userIds = myPatientListDAO.getUserIdsForPatientList(patientId, communityId);
            logger.info("EnrollmentServiceImpl.myPatientSynch: userIdsForPatientList=" + userIds);

            // find list of users that have consent
            List<Long> userIdsWithConsent = myPatientListDAO.getUserIdsWithConsent(patientId, communityId);
            logger.info("EnrollmentServiceImpl.myPatientSynch: userIdsWithConsent=" + userIdsWithConsent);

            // this is the list of users who no longer have consent
            userIds.removeAll(userIdsWithConsent);
            logger.info("EnrollmentServiceImpl.myPatientSynch: list of users who no longer have consent=" + userIds);

            // remove the remaining user ids from patient list
            myPatientListDAO.delete(userIds, patientId, communityId);

            logger.info("removed patient id=" + patientId + ", from these users: " + userIds);

            logger.info("finished: EnrollmentServiceImpl.synchMyPatientList");

        } catch (Exception exc) {
            final String message = "Error synching patient list.";
            logger.logp(Level.SEVERE, getClass().getName(), "synchMyPatientList", message, exc);

            throw new PortalException(message);
        }

    }
    
    public SearchResults<PersonDTO> findPatientsWithNoService(SearchResults<PersonDTO> results) {
        return results;
    }
    public static ServletContext getContext(){
        return context;
    }
    
    public PersonDTO findOnePatient(long patientId, LoginResult loginResult) throws PortalException {
        Long communityId = loginResult.getCommunityId();
        Long userOrgId = loginResult.getOrganizationId();
        Long userId = loginResult.getUserId();

        long userAccessLevelId = loginResult.getAccessLevelId();
        if (userAccessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
            userOrgId = null;
        }

        if (patientId <= 0) {
            return null;
        } else {
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setPatientID(Long.toString(patientId));
            searchCriteria.setIsCarebookPatientSearch(true);
            searchCriteria.setSearchAllPatients(true);
            
            SearchResults<PersonDTO> personList = findPatients(communityId, searchCriteria, false, userOrgId, -1, -1, userId);
            return personList.getData().get(0);
        }
		
    }
}
