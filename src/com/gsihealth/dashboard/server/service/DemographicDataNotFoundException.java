
package com.gsihealth.dashboard.server.service;

public class DemographicDataNotFoundException extends RuntimeException {
    public DemographicDataNotFoundException(String message) {
        super(message);
    }
}
