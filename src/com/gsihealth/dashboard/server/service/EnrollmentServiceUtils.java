package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.server.util.DateUtils;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Chad Darby
 */
public class EnrollmentServiceUtils {

    
    public static Date combineDate(Date consentDate, String time, String ampm) {

        System.out.println("consentDate: " + consentDate);
        System.out.println("time: " + time);
        System.out.println("ampm: " + ampm);
        
        Date resultDate = DateUtils.combineDateTime(consentDate, time, ampm);

        return resultDate;
    }
    
    public static String getNewPatientUserName(PersonDTO personDTO) {
        String patientUserName = null;

        String lastName = personDTO.getLastName();
        String firstName = personDTO.getFirstName();
        Date dob = personDTO.getDateOfBirth();
        int dobValue = getNumericValue(dob);
        
        patientUserName = firstName + "." + lastName + "_" + dobValue;
        patientUserName=patientUserName.trim().replaceAll(" ", "_");
        
        return patientUserName;

    }

    public static String getNewPatientUserName(SimplePerson personDTO) {
        String patientUserName = null;

        String lastName = personDTO.getLastName();
        String firstName = personDTO.getFirstName();
        Date dob = personDTO.getDateOfBirth();
        int dobValue = getNumericValue(dob);

        patientUserName = firstName + "." + lastName + "_" + dobValue;
        patientUserName = patientUserName.trim().
                replaceAll(" ", "_");

        return patientUserName;

    }
    
    
    public static String getPatientUserName(PatientInfo patientInfo) {
        String patientUserName = null;

        String lastName = patientInfo.getUserDTO().getLastName();
        String firstName = patientInfo.getUserDTO().getFirstName();
        Date dob = patientInfo.getUserDTO().getDateOfBirth();
        int dobValue = getNumericValue(dob);
        patientUserName = firstName + "." + lastName + "_" + dobValue;
        patientUserName=patientUserName.trim().replaceAll(" ", "_");
       
        return patientUserName;

    }

    private static int getNumericValue(Date dob) {

        SimpleDateFormat daySdf;
        SimpleDateFormat monthSdf;
        SimpleDateFormat yearSdf;

        daySdf = new SimpleDateFormat("dd");
        monthSdf = new SimpleDateFormat("MM");
        yearSdf = new SimpleDateFormat("yy");
        int day = Integer.parseInt(daySdf.format(dob));
        int month = Integer.parseInt(monthSdf.format(dob));
        int year = Integer.parseInt(yearSdf.format(dob));
        
        int dobValue = (month * day) + year;

        return dobValue;


    }
   
}
