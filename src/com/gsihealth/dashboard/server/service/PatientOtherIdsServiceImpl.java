/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.service.PatientOtherIdsService;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PatientOtherIdsDTO;
import com.gsihealth.entity.OrganizationCwd;
import com.gsihealth.entity.PatientOtherIds;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.OrganizationDAO;
import com.gsihealth.dashboard.server.dao.PatientOtherIdsDAO;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

/**
 *
 * @author beth.boose
 */
public class PatientOtherIdsServiceImpl extends BasePortalServiceServlet implements PatientOtherIdsService {

    private Logger logger = Logger.getLogger(getClass().getName());
    private PatientOtherIdsDAO patientOtherIdsDAO;

    @Override
    public void init() throws ServletException {
        ServletContext application = getServletContext();
        super.init();
        patientOtherIdsDAO = (PatientOtherIdsDAO) application.getAttribute(WebConstants.PATIENT_OTHER_ID_DAO_KEY);
        organizationDAO = (OrganizationDAO)application.getAttribute(WebConstants.ORGANIZATION_DAO_KEY);
    }

    public int getTotalCountOfOtherIds(long patientId, long communityId) throws PortalException {
        try {
            int totalCount = patientOtherIdsDAO.getPatientOtherIdsCount(patientId, communityId);
            return totalCount;
        } catch (Exception exc) {
            final String message = "Error getting total patient other ids count";
            logger.logp(Level.SEVERE, getClass().getName(), "getTotalCountOfOtherIds", message, exc);

            throw new PortalException(message);
        }
    }

    @Override
    public void addPatientOtherId(PatientOtherIdsDTO patientOtherIdsDTO) throws PortalException {
        try {
            PatientOtherIds patientOtherIds = this.convertFromDTOtoEntity(patientOtherIdsDTO);
            if (isDuplicate(patientOtherIds)) {
                throw new PortalException("Another patient already exists in the selected organization with this id.");
            }
            patientOtherIdsDAO.add(patientOtherIds);

        } catch (Exception exc) {
            final String message = "Could not add patient id: " + exc.getMessage();
            logger.severe(message);

            throw new PortalException("Could not add this patient id ");
        }
    }
    
    @Override
    public void updatePatientOtherId(PatientOtherIdsDTO patientOtherIdsDTO, boolean checkForDuplicates) throws PortalException {
        try {
            PatientOtherIds patientOtherIds = this.convertFromDTOtoEntity(patientOtherIdsDTO);
            this.patientOtherIdsDAO.update(patientOtherIds);
        } catch (Exception e) {
            final String message = "Error updating patient other id: " + e.getMessage();
            logger.severe(message);

            throw new PortalException("Could not update patient");
        }
    }

    @Override
    public void deletePatientOtherId(PatientOtherIdsDTO patientOtherIdsDTO) throws PortalException {
        try {
            PatientOtherIds patientOtherIds = this.convertFromDTOtoEntity(patientOtherIdsDTO);
            patientOtherIdsDAO.delete(patientOtherIds.getPatientOtherIdsId());
        } catch (Exception e) {
            final String message = "Error deleting patient other id" + e.getMessage();
            logger.severe(message);
            throw new PortalException("Could not delete this patient other id" );
        }
    }

    private PatientOtherIds convertFromDTOtoEntity(PatientOtherIdsDTO patientOtherIdsDTO) {
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        PatientOtherIds patientOtherIds = mapper.map(patientOtherIdsDTO, PatientOtherIds.class);
        return patientOtherIds;
    }

    @Override
    public SearchResults<PatientOtherIdsDTO> getPatientOtherIds(long patientId, int pageNumber, int pageSize,long communityId) throws PortalException {
        try {
            int totalCount = patientOtherIdsDAO.getPatientOtherIdsCount(patientId,communityId);
            logger.fine("getPatientOtherIds number of patients with this id: " + totalCount);
            List<PatientOtherIds> patientOtherIds = patientOtherIdsDAO.findPatientOtherIdsEntities(patientId, pageNumber, pageSize,communityId);

            List<PatientOtherIdsDTO> patientOtherIdsDTOs = convertFromEntityToDTO(patientOtherIds);

            SearchResults<PatientOtherIdsDTO> searchResults = new SearchResults<PatientOtherIdsDTO>(totalCount, patientOtherIdsDTOs);

            return searchResults;

        } catch (Exception exc) {
            final String message = "Error getting patientOtherIds.";
            logger.logp(Level.SEVERE, getClass().getName(), "getPatientOtherIds", message, exc);

            throw new PortalException(message);
        }
    }

    protected List<PatientOtherIdsDTO> convertFromEntityToDTO(List<PatientOtherIds> patientOtherIds) {

        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        List<PatientOtherIdsDTO> patientOtherIdsDTOs = new ArrayList<PatientOtherIdsDTO>();

        for (PatientOtherIds each : patientOtherIds) {
            PatientOtherIdsDTO tempDTO = mapper.map(each, PatientOtherIdsDTO.class);
            OrganizationCwd org = each.getOrganization();
            if (org != null && org.getOrganizationCwdName() != null) {
                logger.fine("setting org name for " + each.getPatientOtherId() + " is " + org.getOrganizationCwdName());
                tempDTO.setOrganizationName(org.getOrganizationCwdName());
                tempDTO.setPatientOtherOrgName(org.getOrganizationCwdName());
            }
            patientOtherIdsDTOs.add(tempDTO);
        }

        return patientOtherIdsDTOs;
    }

    /**
     * @return a map of all orgs with an OID
     */
    @Override
    public LinkedHashMap<String, String> getOrganizations(long communityId) throws PortalException {
        logger.fine("patientOtherIdsServiceImpl.getOrganizations start method");
        List<OrganizationCwd> orglist = this.organizationDAO.findOrganizationEntities(communityId);
        LinkedHashMap map = new LinkedHashMap<String, String>();
        try {
            for (OrganizationCwd temp : orglist) {
                String oid = temp.getOid();
                if (oid != null) {
                    String orgName = temp.getOrganizationCwdName();
                    logger.fine("patientOtherIdsServiceImpl.getOrganizations adding org name and oid for " + orgName);
                    map.put(oid, orgName);
                }
            }
        } catch (Exception e) {
            String message = "Error loading organization names ";
            logger.severe(message + e);
            throw new PortalException(message);
        }

        return map;
    }

    private boolean isDuplicate(PatientOtherIds patientOtherIds) {
        return patientOtherIdsDAO.isDuplicate(patientOtherIds);
    }

    @Override
    public boolean isDuplicate(PatientOtherIdsDTO patientOtherIdsDTO) {
        PatientOtherIds patientOtherIds = convertFromDTOtoEntity(patientOtherIdsDTO);
        return isDuplicate(patientOtherIds);
    }

}
