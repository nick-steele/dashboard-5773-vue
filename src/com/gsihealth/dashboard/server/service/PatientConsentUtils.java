/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.common.ConsentConstants;
import com.gsihealth.dashboard.entity.dto.OrganizationPatientConsentDTO;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.CommunityOrganizationDAO;
import com.gsihealth.dashboard.server.dao.OrganizationPatientConsentDAO;
import com.gsihealth.dashboard.server.dao.UserPatientConsentDAO;
import com.gsihealth.entity.OrganizationPatientConsent;
import com.gsihealth.entity.OrganizationPatientConsentPK;
import com.gsihealth.entity.PatientCommunityEnrollment;
import com.gsihealth.entity.User;
import com.gsihealth.entity.UserPatientConsent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.joda.time.DateTime;

/**
 *
 * @author Chad Darby
 */
public class PatientConsentUtils {

    protected static Logger logger = Logger.getLogger(PatientConsentUtils.class.getName());

    public static synchronized void saveOrganizationPatientConsentList(OrganizationPatientConsentDAO organizationPatientConsentDAO, UserPatientConsentDAO userPatientConsentDAO, long organizationId,
           String additionalConsentedOrganizations, CommunityOrganizationDAO communityOrganizationDAO, PatientCommunityEnrollment patientEnrollment) throws Exception {
        // for same org
        OrganizationPatientConsentDTO organizationPatientConsentDTO = getOrganizationPatientConsent(patientEnrollment, organizationId);

        List<OrganizationPatientConsentDTO> orgs = new ArrayList<OrganizationPatientConsentDTO>();
        orgs.add(organizationPatientConsentDTO);
               
        // for auto consented orgs
        List<Long> autoConsentOrgIds = communityOrganizationDAO.getAutoConsentOrganizationId(patientEnrollment.getCommunityId(), 'Y');

        if (autoConsentOrgIds != null) {
            for (long tempOrgId : autoConsentOrgIds) {
                if ("yes".equalsIgnoreCase(patientEnrollment.getProgramLevelConsentStatus())) {
                    OrganizationPatientConsentDTO organizationPatientConsentDto = getOrganizationPatientConsent(patientEnrollment, tempOrgId);
                    orgs.add(organizationPatientConsentDto);
                }
            }
        }
        
        //Batched Consented Orgs -- JIRA-719
        if (additionalConsentedOrganizations != null) {
            String batchTest = additionalConsentedOrganizations; //test values "3,5"; // Beta Medical,Delta Hospital"; 
            String[] batchConsentOrgIds = batchTest.split(",");
            for (int i = 0; i < batchConsentOrgIds.length; i++) {
                OrganizationPatientConsentDTO organizationPatientConsentDto
                        = getOrganizationPatientConsent(patientEnrollment, Long.parseLong(batchConsentOrgIds[i]));
                orgs.add(organizationPatientConsentDto);
            }
            addToOrganizationPatientConsentList(organizationPatientConsentDAO, userPatientConsentDAO, orgs, patientEnrollment);   
        } else {
            
            saveOrganizationPatientConsentList(organizationPatientConsentDAO, userPatientConsentDAO, orgs, patientEnrollment);
            
        }
        
        
    }

    public static synchronized void addToOrganizationPatientConsentList(OrganizationPatientConsentDAO organizationPatientConsentDAO, UserPatientConsentDAO userPatientConsentDAO,
            List<OrganizationPatientConsentDTO> orgDtos, PatientCommunityEnrollment patientEnrollment) throws Exception {
    
        //JIRA-719
        logger.info("converting dtos...");
        List<OrganizationPatientConsent> orgs = convertFromDtos(orgDtos);

        logger.info("adding to organization patient consent list...don't delete existing entries");
        organizationPatientConsentDAO.add(orgs);

        logger.info("processing user patient consent list...");
        processUserPatientConsentList(userPatientConsentDAO, patientEnrollment, orgs);
    }

    public static synchronized void saveOrganizationPatientConsentList(OrganizationPatientConsentDAO organizationPatientConsentDAO, UserPatientConsentDAO userPatientConsentDAO, List<OrganizationPatientConsentDTO> orgDtos, PatientCommunityEnrollment patientEnrollment) throws Exception {

        long patientId = patientEnrollment.getPatientId();
        
        // work around, if org dtos empty, then add one for this current user
        if (orgDtos.isEmpty()) {
            long organizationId = patientEnrollment.getOrgId();
            OrganizationPatientConsentDTO organizationPatientConsentDTO = getOrganizationPatientConsent(patientEnrollment, organizationId);            
            orgDtos.add(organizationPatientConsentDTO);
        }
               
       
        logger.info("converting dtos...");
        List<OrganizationPatientConsent> orgs = convertFromDtos(orgDtos);

        logger.info("saving organization patient consent list...");
        organizationPatientConsentDAO.saveOrganizationPatientConsentList(patientId, orgs);

        logger.info("processing user patient consent list...");
        processUserPatientConsentList(userPatientConsentDAO, patientEnrollment, orgs);
    }

    public static synchronized void addOrganizationPatientConsentList(OrganizationPatientConsentDAO organizationPatientConsentDAO, UserPatientConsentDAO userPatientConsentDAO, List<OrganizationPatientConsent> orgList, PatientCommunityEnrollment patientEnrollment) throws Exception {
     
         long patientId = patientEnrollment.getPatientId();
        logger.info("saving organization patient consent list...");
        organizationPatientConsentDAO.saveOrganizationPatientConsentList(patientId, orgList);

        logger.info("processing user patient consent list...");
        processUserPatientConsentList(userPatientConsentDAO, patientEnrollment, orgList);
    }

    private static void processUserPatientConsentList(UserPatientConsentDAO userPatientConsentDAO, PatientCommunityEnrollment patientEnrollment, List<OrganizationPatientConsent> orgs) throws Exception {

        long patientId = patientEnrollment.getPatientId();
        
        logger.info("\n\nstarting processUserPatientConsentList.  patientId=" + patientId);

        // delete all userPatientConsent entries for patientId
        logger.info("Deleting old consent entries...");
        userPatientConsentDAO.deleteOldConsentEntries(patientId);

        logger.info("Organizations to update count=" + orgs.size());

        if (!orgs.isEmpty()) {
            saveUserPatientConsentList(userPatientConsentDAO, orgs, patientEnrollment);
        }
        else {
            logger.info("No organizations found. Skipping adding new entries for user paitient consent.");
        }

        logger.info("finished processUserPatientConsentList.  patientId=" + patientId + "\n\n");
    }

    /**
     * Convert entity objects to DTOs
     *
     * @param dtos
     * @return
     */
    protected static List<OrganizationPatientConsent> convertFromDtos(List<OrganizationPatientConsentDTO> dtos) {

        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        List<OrganizationPatientConsent> orgs = new ArrayList<OrganizationPatientConsent>();

        for (OrganizationPatientConsentDTO tempDto : dtos) {
            OrganizationPatientConsent tempOrg = mapper.map(tempDto, OrganizationPatientConsent.class);
            orgs.add(tempOrg);
        }

        return orgs;
    }
    
    protected static OrganizationPatientConsent convertToEntity(OrganizationPatientConsentDTO dto){
       
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        OrganizationPatientConsent temp = mapper.map(dto, OrganizationPatientConsent.class);
        return temp;
    }

    public static synchronized void updateOrganizationPatientConsent(OrganizationPatientConsentDAO organizationPatientConsentDAO, 
            UserPatientConsentDAO userPatientConsentDAO, long newOrganizationId, long oldOrganizationId, long patientId, long communityId) throws Exception {

        // delete old organizationPatientConsent
        OrganizationPatientConsentPK pk = new OrganizationPatientConsentPK();
        pk.setOrganizationId(oldOrganizationId);
        pk.setPatientId(patientId);
        pk.setCommunityId(communityId);
        organizationPatientConsentDAO.delete(pk);

        // delete old user consent
        List<User> oldUsers = userPatientConsentDAO.getUsers(oldOrganizationId, communityId);

        //        List<User> oldUsers = userPatientConsentDAO.getUsers(oldOrganizationId, Constants.GSI_COMMUNITY_ID);
        userPatientConsentDAO.deleteOldConsentEntries(patientId, oldUsers, communityId);

        // save new org patient consent ... if it doesn't already exist
        if (!organizationPatientConsentDAO.hasConsent(patientId,communityId, newOrganizationId)) {
            OrganizationPatientConsent organizationPatientConsent = new OrganizationPatientConsent();
            pk = new OrganizationPatientConsentPK();
            pk.setOrganizationId(newOrganizationId);
            pk.setPatientId(patientId);
            pk.setCommunityId(communityId);
            organizationPatientConsent.setOrganizationPatientConsentPK(pk);
            organizationPatientConsent.setConsentStatus(ConsentConstants.CONSENT_PERMIT);
       
            organizationPatientConsentDAO.add(organizationPatientConsent);
        }
    }

    private static void saveUserPatientConsentList(UserPatientConsentDAO userPatientConsentDAO, List<OrganizationPatientConsent> orgs, PatientCommunityEnrollment patientEnrollment) throws Exception {

        // for each organization, get a list of all users of the organization
        List<UserPatientConsent> userPatientConsentList = new ArrayList<UserPatientConsent>();
        
        //Work around for communityId FIXME - NOT SAFE, FIX THIS!!!!! relies on patient for communityId, should be using user commId
        long communityId = patientEnrollment.getCommunityId();
        List<Long> userIds = userPatientConsentDAO.getUserIds(orgs,communityId); 
        long patientId = patientEnrollment.getPatientId();
        Date patientConsentDate = patientEnrollment.getConsentDateTime();
    
        if (patientConsentDate == null) {
            // set it to midnight of today
            DateTime midnight = new DateTime().toDateMidnight().toDateTime();
            patientConsentDate = midnight.toDate();
        }
        
        logger.info("userIds count = " + userIds.size());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        logger.info("DOING BIG SAVE (smaller now only exceptions): userPatientConsentList.size()=" + userPatientConsentList.size() + "\n\n");
        userPatientConsentDAO.add(userPatientConsentList); 

        stopWatch.stop();
        logger.info("Time to SAVE user patient consent list: " + stopWatch.getTime() / 1000.0 + " secs");
    }

    protected static OrganizationPatientConsentDTO getOrganizationPatientConsent(PatientCommunityEnrollment patientEnrollment, long organizationId) {
        long patientId = patientEnrollment.getPatientId();
        OrganizationPatientConsentDTO organizationPatientConsentDTO = new OrganizationPatientConsentDTO();
        organizationPatientConsentDTO.setPatientId(patientId);
        organizationPatientConsentDTO.setOrganizationId(organizationId);
        organizationPatientConsentDTO.setConsentStatus(ConsentConstants.CONSENT_PERMIT);
        organizationPatientConsentDTO.setCommunityId(patientEnrollment.getCommunityId());
        return organizationPatientConsentDTO;
    }
    
    /**
     * FIXME - using a special query
     * @param userId
     * @param communityId
     * @return 
     */
    public static String buildUserPatientConsentQuery(long userId,long communityId){
        String nativeSQL="SELECT * FROM patient_community_enrollment AS t0," 
            + WebConstants.USER_PATIENT_CONSENT_VIEW_KEY + " AS t1"
                    +" WHERE t1.user_id = " + userId
                    + " and (t0.patient_id=t1.patient_id)"
            + " and t0.PROGRAM_LEVEL_CONSENT_STATUS='Yes' "                   
            + " and t0.community_id = " + communityId;
           
        
        logger.info("===========buildUserPatientConsentQuery-"+nativeSQL);
        
                     
        // DASHBOARD-477
        // userPatientConsent table only contains DENY ,
        // so only compare organization patient consent and exclude any DENYs from user patient consent table
        // also need to list orgs that user is associated with.
        /*  String queryString =  "select object(patientEnrollment) from PatientCommunityEnrollment as patientEnrollment, OrganizationPatientConsent as o"
                    + " where patientEnrollment.status in ('Enrolled', 'Assigned')"
                    + " and patientEnrollment.patientEnrollmentPK.patientId=o.organizationPatientConsentPK.patientId"
                    + " and o.consentStatus='PERMIT'"
                    + " and o.organizationPatientConsentPK.organizationId in (select uco.organization.orgId"
                    + " from UserCommunityOrganization as uco where"
                    + " uco.user.userId="+ userId +")"                
                    + " and patientEnrollment.patientEnrollmentPK.patientId NOT IN (select userPatientConsent.userPatientConsentPK.patientId"
                    + " from UserPatientConsent as userPatientConsent where"
                    + " userPatientConsent.userPatientConsentPK.userId=" + userId
                    + " and userPatientConsent.consentStatus='DENY')";        
        */
        
          //original query from multiple methods in ReportDAOImpl.java and PatientEnrollmentDAOImpl.java
         /*
           queryString = "select object(patientEnrollment) from PatientCommunityEnrollment as patientEnrollment, UserPatientConsent as userPatientConsent"
                    + " where patientEnrollment.status in ('Enrolled', 'Assigned')"
                    + " and patientEnrollment.patientEnrollmentPK.patientId=userPatientConsent.userPatientConsentPK.patientId"
                    + " and userPatientConsent.userPatientConsentPK.userId=" + userId
                    + " and userPatientConsent.consentStatus='PERMIT'"; */
           
    
          return nativeSQL;
    }
       public static String buildTotalUserPatientConsentQuery(long userId,long communityId){
         //TODO - fix for Dashboard 609 refactor consent to use Database  View
//           String queryString =  "select object(patientEnrollment) from PatientCommunityEnrollment as patientEnrollment, OrganizationPatientConsent as o"
//                    + " where patientEnrollment.status in ('Enrolled', 'Assigned','Pending','Inacive',' ')"
//                    + " and patientEnrollment.patientEnrollmentPK.patientId=o.organizationPatientConsentPK.patientId"
//                    + " and o.consentStatus='PERMIT'"
//                    + " and o.organizationPatientConsentPK.organizationId in (select uco.organization.orgId"
//                    + " from UserCommunityOrganization as uco where"
//                    + " uco.user.userId="+ userId +")"                
//                    + " and patientEnrollment.patientEnrollmentPK.patientId NOT IN (select userPatientConsent.userPatientConsentPK.patientId"
//                    + " from UserPatientConsent as userPatientConsent where"
//                    + " userPatientConsent.userPatientConsentPK.userId=" + userId
//                    + " and userPatientConsent.consentStatus='DENY') and patientEnrollment.patientEnrollmentPK.communityId="+communityId
//                  + " and patientEnrollment.patientEnrollmentPK.patientId IN (select p.patientProgramNamePK.patientId from PatientProgramName as p)";   
//                return queryString;
            String nativeSQL="SELECT * FROM patient_community_enrollment AS t0," + WebConstants.USER_PATIENT_CONSENT_VIEW_KEY + " AS t1"
                    +" WHERE t1.user_id = " + userId
                    + " and (t0.patient_id=t1.patient_id)"
                    + " and t0.STATUS IN ('Enrolled', 'Assigned','Pending','Inactive','')"
                    + "and t0.PATIENT_ID IN (select p.patient_id from patient_program_name as p)"
                    + " and t0.community_id= "+communityId;
        
        logger.info("===========buildUserPatientConsentQuery-"+nativeSQL);
        
        return nativeSQL;
    }
    
    
      
       public static String buildNativeSQLForUsersWithConsent(long patientId){
        
       
        String nativeSQL="SELECT * FROM " + WebConstants.USER_PATIENT_CONSENT_VIEW_KEY
                +" WHERE patient_id" + patientId;
        logger.info("===========buildNativeSQLForUsersWithConsent-"+nativeSQL);
        return nativeSQL;
    }
    
        public static String buildNativeSQLForEnrolledConsentedPatientsCount(long userId){
        
         String nativeSQL = "SELECT COUNT(t0.PATIENT_ID) FROM patient_community_enrollment t0,"
                + "(select patient_id from " + WebConstants.USER_PATIENT_CONSENT_VIEW_KEY + " where user_id="+ userId+") AS t1"
                            + " WHERE t0.STATUS IN ('Enrolled', 'Assigned') AND (t0.PATIENT_ID = t1.patient_id)"; 
         logger.info("===========buildNativeSQLForEnrolledConsentedPatientsCount-"+nativeSQL);
        return nativeSQL;
    }
    
    // =============================================================
    
    
    public static String buildJpqlForHasConsent(long userId, long patientId, long communityId){
        
        //WORKS!!!
        String jpql = "select count(viewUserPatientConsent) from "+ WebConstants.USER_PATIENT_CONSENT_JPQL_VIEW_KEY
                    + " as viewUserPatientConsent"
                    +" where viewUserPatientConsent.viewUserPatientConsentPK.patientId = "+ patientId
                    +" and viewUserPatientConsent.viewUserPatientConsentPK.userId = " + userId
                    +" and viewUserPatientConsent.viewUserPatientConsentPK.communityId = "+ communityId;
                
        
        return jpql;
    }
    
    //========================================================================
    public static Query createNativeOrJPQLQuery(EntityManager em , String queryString) {
        
        Query q = null;
        if (StringUtils.contains(queryString, WebConstants.USER_PATIENT_CONSENT_VIEW_KEY)) {
            logger.info("==createNativeQuery-"+queryString);
            q = em.createNativeQuery(queryString);
          

        } else {
            q = em.createQuery(queryString);
           
        }
        return q;
    }
    
    public static Query createNativeOrJPQLQuery(EntityManager em , String queryString, Class targetClass) {
        
        Query q = null;
        if (StringUtils.contains(queryString, WebConstants.USER_PATIENT_CONSENT_VIEW_KEY)) {
            logger.info("==createNativeQuery-"+queryString);
            q = em.createNativeQuery(queryString,targetClass);
          

        } else {
            q = em.createQuery(queryString);
           
        }
        return q;
    }
}
