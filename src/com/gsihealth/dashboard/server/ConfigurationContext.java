package com.gsihealth.dashboard.server;

import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.dashboard.server.dao.SqlConfigurationDAO;

/**
 * Holder for configuration related items
 * 
 * @author Chad.Darby
 */
public class ConfigurationContext {

    private static ConfigurationContext instance;
    private ConfigurationDAO configurationDAO;
    private SqlConfigurationDAO sqlConfigurationDAO;
    
    private ConfigurationContext() {
        
    }
    
    public static ConfigurationContext getInstance() {

        if (instance == null) {
            instance = new ConfigurationContext();
        }
        
        return instance;
    }

    public ConfigurationDAO getConfigurationDAO() {
        return configurationDAO;
    }

    public void setConfigurationDAO(ConfigurationDAO configurationDAO) {
        this.configurationDAO = configurationDAO;
    } 

    /**
     * @return the sqlConfigurationDAO
     */
    public SqlConfigurationDAO getSqlConfigurationDAO() {
        return sqlConfigurationDAO;
    }

    /**
     * @param sqlConfigurationDAO the sqlConfigurationDAO to set
     */
    public void setSqlConfigurationDAO(SqlConfigurationDAO sqlConfigurationDAO) {
        this.sqlConfigurationDAO = sqlConfigurationDAO;
    }
}
