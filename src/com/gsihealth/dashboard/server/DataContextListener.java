package com.gsihealth.dashboard.server;

import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.util.AppConfigConstants;
import com.gsihealth.dashboard.rest.service.PatientMDMInterfaceFactory;
import com.gsihealth.dashboard.server.dao.*;
import com.gsihealth.entity.*;
import com.gsihealth.dashboard.server.common.NonexistentCommunityException;
import com.gsihealth.dashboard.server.hisp.HispClient;
import com.gsihealth.dashboard.server.hisp.HispClientImpl;
import com.gsihealth.dashboard.server.jmx.ThreadPoolMonitor;
import com.gsihealth.dashboard.server.jmx.ThreadPoolMonitorMBean;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import com.gsihealth.dashboard.server.patientmanagement.PatientManagerImpl;
import com.gsihealth.dashboard.server.service.integration.CommunityOrganizationService;
import com.gsihealth.dashboard.server.service.integration.PatientUserNotification;
import com.gsihealth.dashboard.server.service.integration.UserService;
import com.gsihealth.dashboard.server.tasks.LoadAvailableUsersInCacheTimerTask;
import com.gsihealth.dashboard.server.util.NotificationUtils;
import com.gsihealth.patientmdminterface.PatientInterface;
import com.gsihealth.patientmdminterface.PatientInterfaceFactory;
import com.gsihealth.patientmdminterface.util.PatientsPropertiesMapBuilder;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author Chad Darby
 */
public class DataContextListener implements ServletContextListener {

    private Logger logger = Logger.getLogger(getClass().getName());
    private Timer loadAvailableUsersInCacheTimer;
    private boolean devMode;
//	private Properties props;
    private List<Community> communities;
    private PatientMDMInterfaceFactory pif;

    private static EntityManagerFactory dashboardEmf = null;
    private static EntityManagerFactory gsiAlertingEmf = null;
    private static EntityManagerFactory patientMdmEmf = null;
    private static EntityManagerFactory carePlanEmf = null;

    @Override
    public void contextInitialized(ServletContextEvent event) {

        try {
            logger.info("Loading Dashboard application....");
            ServletContext application = event.getServletContext();

            loadBuildNumber(application);
            devMode = isDevMode();
            loadDashboardEntityManagerFactory();
            loadGsiAlertingEntityManagerFactory();
            loadPatientMdmEntityManagerFactory();
            loadCarePlanEntityManagerFactory();

            logger.info("Loading ConfigurationDAO");
            ConfigurationDAO configurationDAO = loadConfigurationDAO(dashboardEmf, application);

            logger.info("Loading SqlConfigurationDAO");
            SqlConfigurationDAO sqlConfigurationDAO = loadSqlConfigurationDAO(dashboardEmf, application);

            loadDAOs(application, dashboardEmf, gsiAlertingEmf, patientMdmEmf, carePlanEmf, configurationDAO);

            loadServices(application, dashboardEmf);

            cacheApplicationData(application);

            loadAvailableUsers(application);

            loadThreadExecutor(application, configurationDAO);

            loadScheduledTasks(application, this.communities);
            logger.info("\n\n\nDashboard application started successfully at " + new java.util.Date() + ".\n\n\n");

        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Errors encountered when starting dashboard application.", exc);
        }

    }
    
    

    /**
     * Helper method to create entity manager factory
     *
     * @return
     * @throws RuntimeException
     */
    protected void loadDashboardEntityManagerFactory() throws RuntimeException {
        String persistenceUnitName = "dashboardPU";
        String persistenceUnitNameStandAlone = "dashboardPU_standalone";
        if (dashboardEmf == null) {
            dashboardEmf = getEntityManagerFactory(persistenceUnitName, persistenceUnitNameStandAlone);
        }
    }

    /**
     * Helper method to create entity manager factory
     *
     * @return
     * @throws RuntimeException
     */
    protected void loadPatientMdmEntityManagerFactory() throws RuntimeException {
        String persistenceUnitName = "patientmdmPU";
        String persistenceUnitNameStandAlone = "patientmdmPU_standalone";
        if (patientMdmEmf == null) {
            patientMdmEmf = getEntityManagerFactory(persistenceUnitName, persistenceUnitNameStandAlone);
        }
    }

    /**
     * Helper method to create entity manager factory
     *
     * @return
     * @throws RuntimeException
     */
    protected void loadCarePlanEntityManagerFactory() throws RuntimeException {
        String persistenceUnitName = "careplanPU";
        String persistenceUnitNameStandAlone = "careplanPU_standalone";
        if (carePlanEmf == null) {
            carePlanEmf = getEntityManagerFactory(persistenceUnitName, persistenceUnitNameStandAlone);
        }
    }

    /**
     * Helper method to create entity manager factory
     *
     * @return
     * @throws RuntimeException
     */
    protected void loadGsiAlertingEntityManagerFactory() throws RuntimeException {
        String persistenceUnitName = "gsialertingPU";
        String persistenceUnitNameStandAlone = "gsialertingPU_standalone";
        if (gsiAlertingEmf == null) {
            gsiAlertingEmf = getEntityManagerFactory(persistenceUnitName, persistenceUnitNameStandAlone);
        }
    }

    /**
     * Get entity manager factory. Look for data source first, if not found then
     * use standalone
     *
     * @param persistenceUnitName
     * @param persistenceUnitNameStandAlone
     * @return
     * @throws RuntimeException
     */
    protected EntityManagerFactory getEntityManagerFactory(String persistenceUnitName,
            String persistenceUnitNameStandAlone) throws RuntimeException {
        // JPA setup...
        EntityManagerFactory emf = null;
        EntityManager manager = null;

        try {
            // look for datasource first
            logger.info("Looking up: " + persistenceUnitName);
            emf = Persistence.createEntityManagerFactory(persistenceUnitName);
            logger.info("emf = " + emf);
            manager = emf.createEntityManager();
            logger.info("manager = " + manager);
        } catch (Exception exc) {

            logger.info("Could not lookup: " + persistenceUnitName);

            if (devMode) {
                // fall back to standalone
                logger.info("\n\n>>>>>>>>> DEVMODE: FALLING BACK - Now trying: " + persistenceUnitNameStandAlone
                        + " <<<<<<<<<<<<<\n\n");
                emf = Persistence.createEntityManagerFactory(persistenceUnitNameStandAlone);
                manager = emf.createEntityManager();
            } else {
                logger.log(Level.SEVERE,
                        "\n\n>>>>>>>>> ERROR: Could not lookup: " + persistenceUnitName + " <<<<<<<<<<<<<\n\n", exc);
                throw new RuntimeException(exc);
            }
        }

        if (manager == null) {
            String msg = "Unable to get connection to database.";
            logger.severe("\n\n>>>>>>>>> ERROR:" + msg + " <<<<<<<<<<<<<\n\n");
            throw new RuntimeException(msg);
        }

        return emf;
    }

    @Override
    public void contextDestroyed(ServletContextEvent contextEvent) {

        logger.info("Unregistering mbean");
        ServletContext application = contextEvent.getServletContext();

        ThreadPoolMonitor threadPoolMonitor = (ThreadPoolMonitor) application
                .getAttribute(WebConstants.THREAD_POOL_MONITOR_KEY);
        threadPoolMonitor.unregisterBean();

        logger.info("Stopping loadAvailableUsersInCacheTimer task...");
        loadAvailableUsersInCacheTimer.cancel();
        logger.info("Stopped loadAvailableUsersInCacheTimer task.");
    }

    private Map<Long, CommunityContexts> loadCommunityRelatedStuff(List<Community> communities,
            ConfigurationDAO configurationDAO, EntityManagerFactory dashboardEmf, OrganizationDAO organizationDAO) throws Exception {
        // populate application level stuff that must be done by community
        Map<Long, CommunityContexts> communityContextMap = new HashMap<>(communities.size());
        pif = new PatientMDMInterfaceFactory();
        for (Community community : communities) {
            if (community != null && community.getCommunityId() != null) {
                try {
                    logger.info("loading communityContext for community " + community);
                    CommunityContexts communityContexts = populateSingleCommunityContext(community, configurationDAO,
                            dashboardEmf, organizationDAO);
                    communityContextMap.put(community.getCommunityId(), communityContexts);
                } catch (NonexistentCommunityException ex) {
                    logger.severe("failed populating a community - skipping" + ex);
                }
            }
        }
        return communityContextMap;
    }

    /**
     *
     * @param community
     * @param configurationDAO
     * @param dashboardEmf
     * @param organizationDAO
     * @return a communityContext with all community specific values populated -
     * be careful about default values
     * @throws Exception 
     */
    private CommunityContexts populateSingleCommunityContext(Community community, ConfigurationDAO configurationDAO,
            EntityManagerFactory dashboardEmf, OrganizationDAO organizationDAO) throws Exception {
        Long communityId = community.getCommunityId();
        CommunityContexts communityContexts = new CommunityContexts();
        String defaultCommunityProperty = "default.community.id";
//                String mpiEndPoint = configurationDAO.getProperty(defaultCommunityProperty, communityId, PatientInterface.ENDPOINT);
//                String gsiOid = configurationDAO.getProperty(defaultCommunityProperty, communityId, PatientInterface.GSI_OID);
//                String mpi = configurationDAO.getProperty(defaultCommunityProperty, communityId, PatientInterface.MPI);
//                String minorAge = configurationDAO.getProperty(defaultCommunityProperty, communityId, PatientInterface.MINOR_AGE);
//                //mirth
//                String mpiMirthDomain=configurationDAO.getProperty(defaultCommunityProperty, communityId,PatientMirthImpl.MIRTH_DOMAIN);
//                String mpiMirthIddomain=configurationDAO.getProperty(defaultCommunityProperty, communityId,PatientMirthImpl.MIRTH_ID_DOMAIN);
//                String mpiResultLimit= configurationDAO.getProperty(defaultCommunityProperty,communityId,PatientInterface.MPI_RESULT_LIMIT);
//                String mpiUserName=configurationDAO.getProperty(defaultCommunityProperty, communityId, PatientInterface.MPI_USERNAME);
//                String mpiPassword=configurationDAO.getProperty(defaultCommunityProperty, communityId, PatientInterface.MPI_PASSWORD);
//                
        //Map<String , String> mpiMap = this.populateMPIProperties(communityId, mpi,mpiEndPoint, gsiOid,minorAge,mpiMirthDomain,mpiMirthIddomain,mpiResultLimit,mpiUserName,mpiPassword);
        EntityManager em = configurationDAO.getEntityManager();
        PatientManager patientManager = loadMdm(PatientsPropertiesMapBuilder.getMapWithMPIProperties(em, communityId.intValue()));
        pif.addPatientInterface(communityId, getPatientInterface(em, communityId));
        communityContexts.setPatientManager(patientManager);
        // JIRA - 958 application load does not require SWB OID and
        // community_oid will be used to add patients to MPI and to send
        // messages i.e BHIX/PIX/XACML
        String commOid = community.getCommunityOid();
        System.out.println("COMMUNITY OID AT DCL" + commOid);
        long commOrgId = loadCommunityOidData(communityId, organizationDAO, commOid);
        communityContexts.setCommunityOrgId(commOrgId);
        System.out.println("COMMUNITY Org id AT DCL" + commOrgId);

        PatientEnrollmentDAO patientEnrollmentDAO = new PatientEnrollmentDAOImpl(patientManager, dashboardEmf,
                configurationDAO, commOid, communityId);
        communityContexts.setPatientEnrollmentDAO(patientEnrollmentDAO);

        HispClient hispClient = this.loadHispClient(communityId, configurationDAO, defaultCommunityProperty);
        communityContexts.setHispClient(hispClient);

        return communityContexts;
    }

    private void loadDAOs(ServletContext application, EntityManagerFactory dashboardEmf,
            EntityManagerFactory gsiAlertingEmf, EntityManagerFactory patientMdmEmf, EntityManagerFactory carePlanEmf,
            ConfigurationDAO configurationDAO) throws Exception {

        logger.info("Loading DAOs...");

        logger.info("Loading UserDAO");
        UserDAO userDAO = new UserDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.USER_DAO_KEY, userDAO);

        logger.info("Loading OrganizationDAO");
        OrganizationDAO organizationDAO = new OrganizationDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.ORGANIZATION_DAO_KEY, organizationDAO);

        logger.info("Loading CommunityDAO");
        CommunityDAO communityDAO = new CommunityDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.COMMUNITY_DAO_KEY, communityDAO);
        NotificationUtils.setCommunityDAO(communityDAO); // save in static
        // method for
        // notification
        // header


        logger.info("Loading TaskHistoryDAO");
        TaskHistoryDAO taskHistoryDAO = new TaskHistoryDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.TASK_HISTORY_DAO_KEY, taskHistoryDAO);

        logger.info("Loading AccessLevelDAO");
        AccessLevelDAO accessLevelDAO = new AccessLevelDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.ACCESS_LEVEL_DAO_KEY, accessLevelDAO);

        logger.info("Loading RoleDAO");
        RoleDAO roleDAO = new RoleDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.ROLE_DAO_KEY, roleDAO);


        communities = loadCommunities(communityDAO);
        logger.info("Loading community specific stuff");
        Map<Long, CommunityContexts> communityContexts = loadCommunityRelatedStuff(communities, configurationDAO,
                dashboardEmf, organizationDAO);
        application.setAttribute(WebConstants.COMMUNITY_SPECIFIC_DATA_KEY, communityContexts);

        logger.info("Loading FacilityTypeDAO");
        FacilityTypeDAO facilityTypeDAO = new FacilityTypeDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.FACILITY_TYPE_DAO_KEY, facilityTypeDAO);

        logger.info("Loading CountyDAO");
        CountyDAO countyDAO = new CountyDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.COUNTY_DAO_KEY, countyDAO);

        logger.info("Loading CommunityOrganizationDAO");
        CommunityOrganizationDAO communityOrganizationDAO = new CommunityOrganizationDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.COMMUNITY_ORGANIZATION_DAO_KEY, communityOrganizationDAO);

        logger.info("Loading UserCommunityOrganizationDAO");
        UserCommunityOrganizationDAO userCommunityOrganizationDAO = new UserCommunityOrganizationDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.USER_COMMUNITY_ORGANIZATION_DAO_KEY, userCommunityOrganizationDAO);

        logger.info("Loading CommunityCareteamDAO");
        CommunityCareteamDAO communityCareteamDAO = new CommunityCareteamDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.COMMUNITY_CARE_TEAM_DAO_KEY, communityCareteamDAO);

        logger.info("Loading ReferenceDataDAO");
        ReferenceDataDAO referenceDataDAO = new ReferenceDataDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.REFERENCE_DATA_DAO_KEY, referenceDataDAO);
        NotificationUtils.setReferenceDataDAO(referenceDataDAO);// save in static
        logger.info("\n\n>>>>>>>>>>>>>> referenceDataDAO=" + referenceDataDAO + "\n\n");

        logger.info("Loading ReportDAO");
        ReportDAO reportDAO = new ReportDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.REPORT_DAO_KEY, reportDAO);

        logger.info("Loading EulaDAO");
        EulaDAO eulaDAO = new EulaDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.EULA_DAO_KEY, eulaDAO);

        logger.info("Loading SamhsaDAO");
        SamhsaDAO samhsaDAO = new SamhsaDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.SAMHSA_DAO_KEY, samhsaDAO);

        logger.info("Loading AuditDAO");
        AuditDAO auditDAO = new AuditDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.AUDIT_DAO_KEY, auditDAO);

        logger.info("Loading SSOUserDAO");
        SSOUserDAO ssoUserDAO = new SSOUserDAOImpl();
        application.setAttribute(WebConstants.SSO_USER_DAO_KEY, ssoUserDAO);

        logger.info("Loading NotificationAuditDAO");
        NotificationAuditDAO notificationAuditDAO = new NotificationAuditDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.NOTIFICATION_AUDIT_DAO_KEY, notificationAuditDAO);

        logger.info("Loading ApplicationDAO");
        ApplicationDAO applicationDAO = new ApplicationDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.APPLICATION_DAO_KEY, applicationDAO);

        logger.info("Loading NonHealthHomeProviderDAO");
        NonHealthHomeProviderDAO nonHealthHomeProviderDAO = new NonHealthHomeProviderDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.NON_HEALTH_HOME_PROVIDER_DAO_KEY, nonHealthHomeProviderDAO);

        logger.info("Loading OrganizationPatientConsentDAO");
        OrganizationPatientConsentDAO organizationPatientConsentDAO = new OrganizationPatientConsentDAOImpl(
                dashboardEmf);
        application.setAttribute(WebConstants.ORGANIZATION_PATIENT_CONSENT_DAO_KEY, organizationPatientConsentDAO);

        logger.info("Loading UserPatientConsentDAO");
        UserPatientConsentDAO userPatientConsentDAO = new UserPatientConsentDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.USER_PATIENT_CONSENT_DAO_KEY, userPatientConsentDAO);

        logger.info("Loading ViewUserPatientConsentDAO");
        ViewUserPatientConsentDAO viewUserPatientConsentDAO = new ViewUserPatientConsentDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.VIEW_ISER_PATIENT_CONSENT_DAO_KEY, viewUserPatientConsentDAO);

        logger.info("Loading OrganizationMmisOidDAO");
        OrganizationMmisOidDAO organizationMmisOidDAO = new OrganizationMmisOidDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.ORGANIZATION_MMIS_OID_DAO_KEY, organizationMmisOidDAO);

        logger.info("Loading UserAlertDAO");
        UserAlertDAO userAlertDAO = new UserAlertDAOImpl(gsiAlertingEmf);
        application.setAttribute(WebConstants.USER_ALERT_DAO_KEY, userAlertDAO);

        logger.info("Loading CarePlanDAO");
        CarePlanDAO carePlanDAO = new CarePlanDAOImpl(carePlanEmf);
        application.setAttribute(WebConstants.CARE_PLAN_DAO_KEY, carePlanDAO);

        logger.info("Loading PayerPlanDAO");
        PayerPlanDAO payerPlanDAO = new PayerPlanDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.PAYER_PLAN_DAO_KEY, payerPlanDAO);

        logger.info("Loading DocumentResourceCenterDAO");
        DocumentResourceCenterDAO documentResourceCenterDAO = new DocumentResourceCenterDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.DOCUMENT_RESOURCE_CENTER_KEY, documentResourceCenterDAO);

        logger.info("Loading PatientEnrollmentHistoryDAO");
        PatientEnrollmentHistoryDAO patientEnrollmentHistoryDAO = new PatientEnrollmentHistoryDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.PATIENT_ENROLLMENT_HISTORY_DAO_KEY, patientEnrollmentHistoryDAO);

        logger.info("Loading AlertSearchApplicationInfoDAO");
        AlertSearchApplicationInfoDAO alertSearchApplicationInfoDAO = new AlertSearchApplicationInfoDAOImpl(
                dashboardEmf);
        application.setAttribute(WebConstants.ALERT_SEARCH_APP_INFO_DAO_KEY, alertSearchApplicationInfoDAO);

        logger.info("Loading PatientOtherIdsDAO");
        PatientOtherIdsDAO patientOtherIdsDAO = new PatientOtherIdsDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.PATIENT_OTHER_ID_DAO_KEY, patientOtherIdsDAO);

        logger.info("Loading DisenrollmentReasonDAO");
        DisenrollmentReasonDAO disenrollmentReasonDAO = new DisenrollmentReasonDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.DISENROLLMENT_DAO_DEY, disenrollmentReasonDAO);

        logger.info("Loading ConsenterDAO");
        ConsenterDAO consenterDAO = new ConsenterDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.CONSENTER_DAO_KEY, consenterDAO);

        logger.info("Loading SubcriptionManagerAlertServiceDAO");
        SubcriptionManagerAlertServiceDAO subcManagerAlertDAO = new SubcriptionManagerAlertServiceDAOImpl(
                gsiAlertingEmf);
        application.setAttribute(WebConstants.SUBC_MANAGER_ALERT_KEY, subcManagerAlertDAO);

        logger.info("Loading ProgramNameHistoryDAO");
        ProgramNameHistoryDAO programNameHistoryDAO = new ProgramNameHistoryDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.PROGRAM_NAME_HISTORY_DAO_KEY, programNameHistoryDAO);

        logger.info("Loading PatientProgramDAO");
        PatientProgramDAO patientProgramDAO = new PatientProgramDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.PATIENT_PROGRAM_DAO_KEY, patientProgramDAO);
        NotificationUtils.setPatientProgramDAO(patientProgramDAO); // save in static

        logger.info("Loading MyPatientListDAO");
        MyPatientListDAO myPatientListDAO = new MyPatientListDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.MY_PATIENT_LIST_DAO_KEY, myPatientListDAO);

        logger.info("Loading CareManagementOrganizationHistoryDAO");
        CareManagementOrganizationHistoryDAO careManagementOrgHistoryDAO = new CareManagementOrganizationHistoryDAOImpl(
                dashboardEmf);
        application.setAttribute(WebConstants.CARE_MANAGEMENT_ORGANIZATION_HISTORY_DAO_KEY,
                careManagementOrgHistoryDAO);

        logger.info("Loading AcuityScoreHistoryDAO");
        AcuityScoreHistoryDAO acuityScoreHistoryDAO = new AcuityScoreHistoryDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.ACUITY_SCORE_HISTORY_DAO_KEY, acuityScoreHistoryDAO);

        logger.info("Loading patientActivityTrackerDAO");
        PatientActivityTrackerDAO patientActivityTrackerDAO = new PatientActivityTrackerDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.PATIENT_ACTIVIY_TRACKER_DAO_KEY, patientActivityTrackerDAO);

        logger.info("Loading ConsentHistoryDAO");
        ConsentHistoryDAO consentHistoryDAO = new ConsentHistoryDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.CONSENT_HISTORY_DAO_KEY, consentHistoryDAO);

        logger.info("Finished loading DAOs...");
    }

    protected Properties loadPropertiesData(ServletContext application) throws Exception {
        Properties props = null;
        try {
            props = loadProperties(application);
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Unable to load application properties.", exc);
            throw exc;
        }
        return props;
    }

    /**
     * Helper method to load properties file
     *
     * @param application
     * @throws IOException
     */
    private Properties loadProperties(ServletContext application) throws IOException {
        String propsFileName = application.getInitParameter("dashboard.properties.file");

        Properties theProps = new Properties();
        logger.info("Loading properties file: " + propsFileName);
        theProps.load(new FileReader(propsFileName));
        logger.info("Properties loaded: " + theProps);
        application.setAttribute(WebConstants.APPLICATION_PROPERTIES_KEY, theProps);

        return theProps;
    }

    /**
     * Helper method to load properties file
     *
     * @param application
     * @throws IOException
     */
    private void loadBuildNumber(ServletContext application) throws IOException {
        String propsFileName = "/buildnumber.properties";

        Properties theProps = new Properties();
        theProps.load(getClass().getResourceAsStream(propsFileName));

        String buildNumber = theProps.getProperty("buildnumber");
        application.setAttribute(WebConstants.APPLICATION_PROPERTIES_BUILD_VERSION_KEY, buildNumber);

        logger.info("Build version: " + buildNumber);
    }

    private void loadServices(ServletContext application, EntityManagerFactory dashboardEmf) {
        logger.info("Loading Services...");

        logger.info("Loading CommunityOrganizationService");
        CommunityOrganizationService communityOrganizationService = new CommunityOrganizationService(dashboardEmf);
        application.setAttribute(WebConstants.COMMUNITY_ORGANIZATION_SERVICE_KEY, communityOrganizationService);

        logger.info("Loading UserService");
        UserService userService = new UserService(dashboardEmf);
        application.setAttribute(WebConstants.USER_SERVICE_KEY, userService);

        logger.info("Loading PatientUserNotification.");
        PatientUserNotification patientUserNotification = new PatientUserNotification(dashboardEmf);
        application.setAttribute(WebConstants.PATIENT_USER_NOTIFICATION, patientUserNotification);
    }

    /**
     * @param communityDAO
     * @return list of existing communities
     */
    private List<Community> loadCommunities(CommunityDAO communityDAO) {
        List<Community> communities = communityDAO.findEntities();
        return communities;
    }

    private PatientManager loadMdm(Map<String, String> mpiMap)
            throws NonexistentCommunityException {
        logger.info("Loading MDM for community " + mpiMap.get(PatientInterface.COMMUNITY_ID));
        PatientManager patientManager = null;

        try {

            patientManager = new PatientManagerImpl(mpiMap);
            patientManager.setEntityManager(dashboardEmf.createEntityManager());

        } catch (Exception ex) {
            String message = "=======>  UNABLE TO LOAD PATIENT MANAGER FOR COMMUNITY " + mpiMap.get(PatientInterface.COMMUNITY_ID);
            logger.log(Level.SEVERE, message, ex);
            throw new NonexistentCommunityException(message, ex);
        }
        return patientManager;

    }

//       private Map<String, String> populateMPIProperties(Long communityId,String mpi,String mpiEndPoint,String  gsiOid,String  minorAge,String mpiMirthDomain,String mpiMirthIddomain,String mpiResultLimit,String mpiUserName,String mpiPwd){
//		Map<String, String> mpiProps = new HashMap<>();
//		mpiProps.put(PatientInterface.COMMUNITY_ID, communityId.toString());
//		mpiProps.put(PatientInterface.ENDPOINT, mpiEndPoint);
//
//                mpiProps.put(PatientMirthImpl.MIRTH_DOMAIN,mpiMirthDomain);
//                mpiProps.put(PatientMirthImpl.MIRTH_ID_DOMAIN,mpiMirthIddomain);
//                mpiProps.put(PatientInterface.MPI_RESULT_LIMIT,mpiResultLimit);
//                mpiProps.put(PatientInterface.MPI_USERNAME,mpiUserName);
//                mpiProps.put(PatientInterface.MPI_PASSWORD,mpiPwd);
//		 
//		mpiProps.put(PatientInterface.GSI_OID, gsiOid);
//
//		mpiProps.put(PatientInterface.MINOR_AGE, minorAge);
//                
//                mpiProps.put(PatientInterface.MPI, mpi);
//                
//                
//		logger.info(mpiProps.toString());
//		return mpiProps;
//	}
    private HispClient loadHispClient(Long communityId, ConfigurationDAO configurationDAO,
            String defaultCommunityProperty) throws NonexistentCommunityException {
        logger.info("Loading HISP Client");

        String hispServerVersion = configurationDAO.getProperty(communityId, "hisp.james.server.version", "james2");
        logger.info("hisp.james.server.version=" + hispServerVersion);

        HispClient hispClient = null;
        String hispServer;
        int hispPort;
        String adminUserId;
        String password;

        if (StringUtils.equalsIgnoreCase(HispClient.JAMES2_VERSION, hispServerVersion)) {
            hispServer = configurationDAO.getProperty(defaultCommunityProperty, communityId, "hisp.server");
            hispPort = Integer
                    .parseInt(configurationDAO.getProperty(defaultCommunityProperty, communityId, "hisp.port"));
            adminUserId = configurationDAO.getProperty(defaultCommunityProperty, communityId, "hisp.admin.userid");
            password = configurationDAO.getProperty(defaultCommunityProperty, communityId, "hisp.admin.password");
        } else if (StringUtils.equalsIgnoreCase(HispClient.JAMES3_VERSION, hispServerVersion)) {
            hispServer = configurationDAO.getProperty(defaultCommunityProperty, communityId, "hisp.james3.server");
            hispPort = Integer
                    .parseInt(configurationDAO.getProperty(defaultCommunityProperty, communityId, "hisp.james3.port"));
            adminUserId = configurationDAO.getProperty(defaultCommunityProperty, communityId,
                    "hisp.james3.admin.userid");
            password = configurationDAO.getProperty(defaultCommunityProperty, communityId,
                    "hisp.james3.admin.password");
        } else {
            throw new IllegalArgumentException("Unknown HISP VERSION: hisp.james.server.version=" + hispServerVersion);
        }

        if (StringUtils.isBlank(hispServer) || StringUtils.isBlank(hispServer) || StringUtils.isBlank(adminUserId)
                || StringUtils.isBlank(password) || hispPort == 0) {
            throw new NonexistentCommunityException(
                    "missing configuration for hispClient for community " + communityId);
        }

        logger.info("hisp server for community " + communityId + " is " + hispServer);
        logger.info("hisp port for community " + communityId + " is " + hispPort);

        hispClient = new HispClientImpl(hispServer, hispPort, adminUserId, password);
        logger.info("Loaded HISP Client for community " + communityId);
        return hispClient;
    }

    private void cacheApplicationData(ServletContext servletContext) throws Exception {
        ApplicationDAO applicationDAO = (ApplicationDAO) servletContext.getAttribute(WebConstants.APPLICATION_DAO_KEY);

        List<Application> apps = applicationDAO.findApplications();

        logger.info("Loading apps into cache. count = " + apps.size());

        // add app list in context
        servletContext.setAttribute(WebConstants.APPLICATION_LIST_KEY, apps);
    }



    /**
     * @param application
     * @param configurationDAO
     */
    private long loadCommunityOidData(Long communityId, OrganizationDAO organizationDAO, String communityOid)
            throws NonexistentCommunityException {
        logger.info("Loading auto consent org data for community " + communityId);

        if (StringUtils.isBlank(communityOid)) {
            String message = "no community oid for community " + communityId;
            logger.log(Level.SEVERE, message);
            throw new NonexistentCommunityException(message);
        }
        logger.info("Loaded community org oid for community " + communityId);
        return organizationDAO.getOrganizationId(communityOid);
    }

    private String getTreatUrl(ApplicationDAO applicationDAO, long communityID) {
        return getAppUrl(applicationDAO, AppConfigConstants.CAREPLAN_APP_ID, communityID);
    }

    private boolean isTreatRestfulSSOEnabled(ApplicationDAO applicationDAO, long communityID) {
        return isAppRestfulSSOEnabled(applicationDAO, AppConfigConstants.CAREPLAN_APP_ID, communityID);
    }

    private boolean isCareBookClickToOpenEnabled(ApplicationDAO applicationDAO, long communityID) {
        return isAppClickToOpenEnabled(applicationDAO, AppConfigConstants.CAREBOOK_APP_ID, communityID);
    }

    private String getAvadoUrl(ApplicationDAO applicationDAO, long communityID) {
        return getAppUrl(applicationDAO, AppConfigConstants.PATIENT_ENGAGEMENT_APP_ID, communityID);
    }

    private String getMessagesUrl(ApplicationDAO applicationDAO, long communityID) {
        return getAppUrl(applicationDAO, AppConfigConstants.MESSAGES_APP_ID, communityID);
    }

    private String getAppUrl(ApplicationDAO applicationDAO, long applicationId, long communityID) {

        // long
        // communityId=com.gsihealth.dashboard.server.dao.Constants.GSI_COMMUNITY_ID;
        Application theApp = applicationDAO.findApplication(applicationId, communityID);

        String appUrl = theApp.getExternalApplicationUrl();

        return appUrl;
    }

    private boolean isAppRestfulSSOEnabled(ApplicationDAO applicationDAO, long applicationId, long communityID) {

        // long
        // communityId=com.gsihealth.dashboard.server.dao.Constants.GSI_COMMUNITY_ID;
        Application theApp = applicationDAO.findApplication(applicationId, communityID);

        boolean isSsoEnabled = theApp.getIsSsoEnabled();

        return isSsoEnabled;
    }

    private boolean isAppClickToOpenEnabled(ApplicationDAO applicationDAO, long applicationId, long communityID) {

        // long
        // communityId=com.gsihealth.dashboard.server.dao.Constants.GSI_COMMUNITY_ID;
        Application theApp = applicationDAO.findApplication(applicationId, communityID);

        boolean isAppClickToOpenEnabled = theApp.getIsClickToOpenEnabled();

        return isAppClickToOpenEnabled;
    }

    private boolean isDevMode() { // FIXME SHOULDN'T THIS BE IN THE DB OR PROP
        // FILE?
        // String val = props.getProperty("database.connection.devmode",
        // "false");
        String val = System.getenv("devmode");
        logger.info("database.connection.devmode=" + val);

        return Boolean.parseBoolean(val);
    }

    /**
     * For details on thread pool executor, see:
     * http://docs.oracle.com/javase/6/docs/api/java/util/concurrent/
     * ThreadPoolExecutor.html these are community independent - use default
     * communityId 1
     *
     * @param application
     * @param configurationDAO
     */
    private void loadThreadExecutor(ServletContext application, ConfigurationDAO configurationDAO) {

        int corePoolSize = getIntProperty(configurationDAO, "threadpool.corepoolsize", "5");
        int maximumPoolSize = getIntProperty(configurationDAO, "threadpool.maximumpoolsize", "10");
        int workQueueSize = getIntProperty(configurationDAO, "threadpool.workqueuesize", "50");
        int keepAliveTime = getIntProperty(configurationDAO, "threadpool.keepalivetime.seconds", "60");

        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<Runnable>(workQueueSize);
        Executor executor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, TimeUnit.SECONDS,
                workQueue, new RejectionHandler());

        logger.info("Adding executor to application context");
        application.setAttribute(WebConstants.THREAD_EXECUTOR_KEY, executor);

        logger.info("Creating thread pool monitor");
        ThreadPoolMonitorMBean monitor = new ThreadPoolMonitor((ThreadPoolExecutor) executor);

        logger.info("Adding monitor to application context");
        application.setAttribute(WebConstants.THREAD_POOL_MONITOR_KEY, monitor);

        logger.info("Thread pool executor created.");
    }

    protected int getIntProperty(Properties props, String propName, String defaultValue) throws NumberFormatException {
        String propValueStr = props.getProperty(propName, defaultValue);
        int propValue = Integer.parseInt(propValueStr);
        logger.info(propName + "=" + propValue);

        return propValue;
    }

    protected int getIntProperty(ConfigurationDAO configurationDAO, String propName, String defaultValue)
            throws NumberFormatException {
        Long defaultCommunityId = Long.parseLong(configurationDAO.getDefaultCommunityId());
        String propValueStr = configurationDAO.getProperty(defaultCommunityId, propName, defaultValue);
        int propValue = Integer.parseInt(propValueStr);
        logger.info(propName + "=" + propValue);

        return propValue;
    }

    protected String getPropertyForDefaultCommunity(ConfigurationDAO configurationDAO, Long communityId,
            String propertyName) {
        return configurationDAO.getProperty("default.community.id", communityId, propertyName);
    }

    public ConfigurationDAO loadConfigurationDAO(EntityManagerFactory dashboardEmf, ServletContext application) {

        ConfigurationDAO configurationDAO = new ConfigurationDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.CONFIGURATION_DAO_KEY, configurationDAO);

        ConfigurationContext.getInstance().setConfigurationDAO(configurationDAO);

        return configurationDAO;
    }

    public SqlConfigurationDAO loadSqlConfigurationDAO(EntityManagerFactory dashboardEmf, ServletContext application) {

        SqlConfigurationDAO sqlConfigurationDAO = new SqlConfigurationDAOImpl(dashboardEmf);
        application.setAttribute(WebConstants.SQL_CONFIGURATION_DAO_KEY, sqlConfigurationDAO);
        ConfigurationContext.getInstance().setSqlConfigurationDAO(sqlConfigurationDAO);

        return sqlConfigurationDAO;
    }

    private void startScheduledTasks(ServletContext application, Properties props) {

    }

    private PatientInterface getPatientInterface(EntityManager em, Long communityId) throws Exception {
        Map<String, String> mapWithMPIProperties = PatientsPropertiesMapBuilder.getMapWithMPIProperties(em, new Long(communityId).intValue());
        PatientInterface pmi = PatientInterfaceFactory.getMPInterface(mapWithMPIProperties.get(PatientInterface.MPI), mapWithMPIProperties);
        pmi.setEntityManager(dashboardEmf.createEntityManager());
        return pmi;
    }

    private void loadAvailableUsers(ServletContext application) throws Exception {
        // String loadAvailableUsersPeriodStr =
        // props.getProperty("load.available.users.in.cache.period.minutes",
        // "5");
        ConfigurationDAO configDAO = (ConfigurationDAO) application.getAttribute(WebConstants.CONFIGURATION_DAO_KEY);
        long communityId = Long.parseLong(configDAO.getDefaultCommunityId());
        String loadAvailableUsersPeriodStr = configDAO.getProperty(
                "load.available.users.in.cache.period.minutes", communityId,
                "load.available.users.in.cache.period.minutes");

        long period = Long.parseLong(loadAvailableUsersPeriodStr);

        loadAvailableUsersInCacheTimer = new Timer();
        loadAvailableUsersInCacheTimer.scheduleAtFixedRate(new LoadAvailableUsersInCacheTimerTask(application), 0,
                TimeUnit.MINUTES.toMillis(period));
    }

    private void loadScheduledTasks(ServletContext application, List<Community> communities) {
        logger.info("loading scheduled tasks?");
//		 ScheduledTasks scheduledTasks = new ScheduledTasks();
//		 scheduledTasks.scheduleTasks();
    }

    class RejectionHandler implements RejectedExecutionHandler {

        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            logger.warning("Rejecting task execution. Maximum pool size exceeded: " + executor.getMaximumPoolSize());
        }
    }

}
