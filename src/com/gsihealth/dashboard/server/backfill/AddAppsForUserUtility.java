package com.gsihealth.dashboard.server.backfill;

import com.gsihealth.dashboard.server.dao.util.DaoUtils;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Chad Darby
 */
public class AddAppsForUserUtility {

    // connect:@10.128.65.110:3306:
    private static final String url = "jdbc:mysql://x.x.x.x:3306/connect";
    private static final String user = "xxx";
    private static final String password = "xxx";
    private static String communityId;

    // private static final int UHC_ENROLLMENT_REPORT_APP_ID = 18;
    private static final int POPULATION_MANAGER_APP_ID = 15;
    
    public static void main(String[] args) {

        try {

            String fileName = "mmc-users-for-prod.txt";

            File theFile = new File(fileName);

            prompt();

            Connection myConn = getConnection();

            List<String> users = readUsers(theFile);

            addApps(myConn, users);

            close(myConn);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    private static Connection getConnection() throws Exception {

        System.out.println("> connecting to " + url + "   ....");

        Connection myConn = DriverManager.getConnection(url, user, password);

        System.out.println("connection successful");

        return myConn;

    }

    private static void close(Connection myConn) throws Exception {

        if (myConn != null) {
            myConn.close();
        }
    }

    private static void addApps(Connection myConn, List<String> users) throws Exception {

        // get the user id for each user
        Map<String, Long> userIds = getUserIds(myConn, users);

        String insertSql = "insert into user_community_application "
                + "(user_id, community_id, application_id, start_date) "
                + "values (%d, %d, %d, now())";

        Statement myInsertStmt = null;

        try {

            // now do real work
            myInsertStmt = myConn.createStatement();

            int count = 1;
            int totalUsers = userIds.size();

            Set<Map.Entry<String, Long>> dataMap = userIds.entrySet();

            for (Map.Entry<String, Long> temp : dataMap) {

                String email = temp.getKey();
                long userId = temp.getValue();

                /*
                System.out.println("Updating user_id: " + userId + ", email=" + email);
                System.out.println(count + " out of " + totalUsers + ",  " + getPercentage(count, totalUsers));
                */
                
                String finalInsertSql = String.format(insertSql, userId, communityId, POPULATION_MANAGER_APP_ID);
                
                System.out.println(finalInsertSql + ";");
                
                // myInsertStmt.execute(finalInsertSql);

                count++;
            }
        } finally {
            myInsertStmt.close();
        }

    }

    private static void prompt() {
        System.out.println("Performing add apps for:");
        System.out.println("jdbcUrl: " + url + "\n");

        System.out.println("Select community");

        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        if (StringUtils.isNumeric(input)) {
            communityId = input;
            return;
        } else {
            System.out.println("\nOperation cancelled.\n");
            System.exit(0);
        }
    }

    private static String getPercentage(int count, int size) {

        double val = (count / (float) size) * 100;

        return String.format("%.0f%%", val);
    }

    private static List<String> readUsers(File theFile) throws Exception {
        return FileUtils.readLines(theFile);
    }

    private static Map<String, Long> getUserIds(Connection myConn, List<String> users) throws SQLException {

        Statement userStmt = null;
        ResultSet userRs = null;
        Map<String, Long> userIds = new HashMap<String, Long>();

        try {

            String inClause = DaoUtils.buildInClauseQuoted(users);

            String userSql = "select user_id, email from user where email in " + inClause;

            // get count first
            userStmt = myConn.createStatement();

            userRs = userStmt.executeQuery(userSql);

            int count = 1;

            while (userRs.next()) {
                long userId = userRs.getLong("user_id");
                String email = userRs.getString("email");

                userIds.put(email, userId);
            }

            return userIds;

        } finally {
            userRs.close();
            userStmt.close();
        }
    }
}
