package com.gsihealth.dashboard.server.backfill;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * This is used for setting up mappings with new role ids
 * 
 * @author Chad Darby
 */
public class NewRoleBackFillUtility {

    private static final String url = "jdbc:mysql://10.110.210.90:3306/connect";
    // private static final String driver = "com.mysql.jdbc.Driver";
    private static final String user = "ghedevuser";
    private static final String password = "ghedevuser";

    private static int[] newRoleIds = {9, 10};

    
    public static void main(String[] args) {

        try {
            Connection myConn = getConnection();

            // backfill org apps
            backfillOrgApps(myConn, newRoleIds);

            close(myConn);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    private static Connection getConnection() throws Exception {

        System.out.println("connecting to " + url + "   ....");

        Connection myConn = DriverManager.getConnection(url, user, password);

        System.out.println("connection successful");

        return myConn;

    }

    private static void close(Connection myConn) throws Exception {

        if (myConn != null) {
            myConn.close();
        }
    }

    private static void backfillOrgApps(Connection myConn, int[] theRoleIds) throws Exception {

        // get the template apps
        List<TemplateApp> templateApps = getTemplateApps(myConn, theRoleIds);
        System.out.println("Found: " + templateApps.size());
        System.out.println(templateApps);

        // get the orgs id
        List<Long> orgIds = getOrgIds(myConn);
        System.out.println(orgIds);

        // now insert org apps
        insertOrgApps(myConn, orgIds, templateApps);
    }

    
    private static List<TemplateApp> getTemplateApps(Connection myConn, int[] roleIds) throws Exception {

        List<TemplateApp> templateApps = new ArrayList<TemplateApp>();

        int[] appIds = {2, 4, 5, 6, 9, 12};
        int[] accessLevelIds = {1, 2, 3};

        for (int theRoleId : roleIds) {

            for (int tempAppId : appIds) {

                for (int tempAccessLevelId : accessLevelIds) {
                    if ((tempAppId == 4) && (tempAccessLevelId == 3)) {
                        continue;
                    }

                    TemplateApp tempApp = null;

                    tempApp = new TemplateApp();
                    tempApp.setAppId(tempAppId);
                    tempApp.setCommunityId(1);
                    tempApp.setAccessLevelId(tempAccessLevelId);
                    tempApp.setRoleId(theRoleId);
                    templateApps.add(tempApp);
                }
            }
        }

        return templateApps;
    }

    private static List<Long> getOrgIds(Connection myConn) throws Exception {
        List<Long> orgIds = new ArrayList<Long>();

        String sql = "select org_id from organization_cwd";

        Statement myStmt = null;
        ResultSet myRs = null;

        try {
            myStmt = myConn.createStatement();
            myRs = myStmt.executeQuery(sql);

            while (myRs.next()) {
                orgIds.add(myRs.getLong("org_id"));
            }
        } finally {
            myRs.close();
            myStmt.close();
        }

        return orgIds;
    }

    private static void insertOrgApps(Connection myConn, List<Long> orgIds, List<TemplateApp> templateApps) throws Exception {

        String sql = "insert into organization_community_application "
                + "(organization_id, community_id, application_id, access_level_id, role_id) "
                + "values (%d, %d, %d, %d, %d)";

        Statement myStmt = null;

        try {
            myStmt = myConn.createStatement();

            for (long tempOrgId : orgIds) {

                System.out.println("Updating org id: " + tempOrgId);

                for (TemplateApp tempApp : templateApps) {
                    String finalSql = String.format(sql, tempOrgId, tempApp.getCommunityId(), tempApp.getAppId(), tempApp.getAccessLevelId(), tempApp.getRoleId());

                    myStmt.execute(finalSql);
                }
            }
        } finally {
            myStmt.close();
        }
    }
}
