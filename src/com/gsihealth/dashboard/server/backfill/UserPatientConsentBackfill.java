/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.backfill;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Chad Darby
 */
public class UserPatientConsentBackfill {

    private static final String url = "jdbc:mysql://x.x.x.x:3306/connect";
    // private static final String driver = "com.mysql.jdbc.Driver";
    private static final String user = "connect";
    private static final String password = "connect";

    public static void main(String[] args) {

        try {
            prompt();

            Connection myConn = getConnection();

            // backfill user patient consent
            backfillUserPatientConsent(myConn);
            
            // backfill org patient consent
            backfillOrganizationPatientConsent(myConn);

            close(myConn);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    private static Connection getConnection() throws Exception {

        System.out.println("connecting to " + url + "   ....");

        Connection myConn = DriverManager.getConnection(url, user, password);

        System.out.println("connection successful");

        return myConn;

    }

    private static void close(Connection myConn) throws Exception {

        if (myConn != null) {
            myConn.close();
        }
    }

    private static void backfillUserPatientConsent(Connection myConn) throws Exception {

        // get all users

        String orphanedPatientsSql = "select patient_id, org_id from `connect`.`patient_community_enrollment` \n"
                + "where\n"
                + "program_level_consent_status='Yes'\n"
                + "and\n"
                + "patient_id not in (select patient_id from  `connect`.`user_patient_consent`)";

        String insertSql = "insert into user_patient_consent "
                + "(patient_id, user_id, consent_status, start_date, community_id) "
                + "values (%d, %d, 'PERMIT', now(), 1)";

        Statement myInsertStmt = null;
        Statement userStmt = null;
        ResultSet userRs = null;

        try {
            myInsertStmt = myConn.createStatement();

            userStmt = myConn.createStatement();

            userRs = userStmt.executeQuery(orphanedPatientsSql);

            while (userRs.next()) {

                long patientId = userRs.getLong("patient_id");
                long orgId = userRs.getLong("org_id");
                System.out.println("Updating patient_id: " + patientId);

                List<Long> userIds = getUserIds(myConn, orgId);
                System.out.println("userIds = " + userIds);

                for (Long tempUserId : userIds) {
                    String finalInsertSql = String.format(insertSql, patientId, tempUserId);
                    myInsertStmt.execute(finalInsertSql);
                }
            }
        } finally {
            userRs.close();
            userStmt.close();
            myInsertStmt.close();
        }


    }

    private static void backfillOrganizationPatientConsent(Connection myConn) throws Exception {

        // get all users
        String orphanedPatientSql = "select patient_id, org_id from patient_community_enrollment \n"
                + "where\n"
                + "program_level_consent_status='Yes'\n"
                + "and\n"
                + "patient_id not in (select patient_id from  organization_patient_consent);";

        String insertSql = "insert into organization_patient_consent "
                + "(patient_id, organization_id, consent_status, start_date) "
                + "values (%d, %d, 'PERMIT', now())";

        Statement myInsertStmt = null;
        Statement userStmt = null;
        ResultSet userRs = null;

        try {
            myInsertStmt = myConn.createStatement();

            userStmt = myConn.createStatement();

            userRs = userStmt.executeQuery(orphanedPatientSql);

            while (userRs.next()) {

                long patientId = userRs.getLong("patient_id");
                long orgId = userRs.getLong("org_id");
                System.out.println("Updating patient_id: " + patientId);

                String finalInsertSql = String.format(insertSql, patientId, orgId);
                myInsertStmt.execute(finalInsertSql);
            }
        } finally {
            userRs.close();
            userStmt.close();
            myInsertStmt.close();
        }


    }

    private static void prompt() {
        System.out.println("Performing patient consent backfill for:");
        System.out.println("jdbcUrl: " + url + "\n");

        System.out.println("Are you sure yes/[no]?");

        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        if (StringUtils.startsWithIgnoreCase(input, "yes")) {
            return;
        } else {
            System.out.println("\nOperation cancelled.\n");
            System.exit(0);
        }
    }

    private static List<Long> getUserIds(Connection myConn, long orgId) throws Exception {
        List<Long> userIds = new ArrayList<Long>();

        String sql = "SELECT u.user_id FROM user u, user_community_organization uco, community_organization co, organization_cwd org_cwd"
                + " where"
                + " u.user_id=uco.user_id"
                + " and"
                + " uco.community_org_id=co.community_org_id"
                + " and"
                + " org_cwd.org_id=co.organization_id"
                + " and"
                + " co.organization_id=" + orgId;

        Statement myInsertStmt = null;
        Statement userStmt = null;
        ResultSet userRs = null;

        try {
            myInsertStmt = myConn.createStatement();

            userStmt = myConn.createStatement();

            userRs = userStmt.executeQuery(sql);

            while (userRs.next()) {

                long tempUserId = userRs.getLong("user_id");
                userIds.add(tempUserId);
            }
        } finally {
            userRs.close();
            userStmt.close();
            myInsertStmt.close();
        }

        return userIds;
    }
}
