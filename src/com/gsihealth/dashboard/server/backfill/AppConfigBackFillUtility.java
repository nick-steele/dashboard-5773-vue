package com.gsihealth.dashboard.server.backfill;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Chad Darby
 */
public class AppConfigBackFillUtility {

    private static final String url = "jdbc:mysql://x.x.x.x:3306/connect";
    private static final String user = "xxx";
    private static final String password = "xxx";

    //
    // Note: Before running this utility, you need to clean up previous app mappings
    //
    //          delete from `connect`.`organization_community_application`;
    //
    //          delete from connect.user_community_application where application_id not in (10, 11, 13, 14, 15, 16, 17, 18);
    //          -- 10: messaging app
    //          -- 11: patient engagement
    //          -- 14: carebook
    //
    public static void main(String[] args) {

        try {
            prompt();
            
            Connection myConn = getConnection();

            // backfill org apps
            backfillOrgApps(myConn);

            // backfill user apps
            backfillUserApps(myConn);

            close(myConn);
        } catch (Exception exc) {
            exc.printStackTrace();
        }
    }

    private static Connection getConnection() throws Exception {

        System.out.println("> connecting to " + url + "   ....");

        Connection myConn = DriverManager.getConnection(url, user, password);

        System.out.println("connection successful");

        return myConn;

    }

    private static void close(Connection myConn) throws Exception {

        if (myConn != null) {
            myConn.close();
        }
    }

    private static void backfillOrgApps(Connection myConn) throws Exception {

        // get the template apps
        List<TemplateApp> templateApps = getTemplateApps(myConn);
        System.out.println(templateApps);

        // get the orgs id
        List<Long> orgIds = getOrgIds(myConn);
        System.out.println(orgIds);

        // now insert org apps
        insertOrgApps(myConn, orgIds, templateApps);
    }

    private static void backfillUserApps(Connection myConn) throws Exception {

        // get the template apps
        List<TemplateApp> templateApps = getTemplateApps(myConn);

        // get all users

        String userSql = "select * from user";

        String userCountSql = "select count(*) from user";
        
        String insertSql = "insert into user_community_application "
                + "(user_id, community_id, application_id, start_date) "
                + "values (%d, %d, %d, now())";

        Statement myInsertStmt = null;
        Statement userStmt = null;
        ResultSet userRs = null;

        Statement userCountStmt = null;
        ResultSet userCountRs = null;
        
        try {
            
            // get count first
            userCountStmt = myConn.createStatement();

            userCountRs = userCountStmt.executeQuery(userCountSql);
            
            userCountRs.next();
            int totalUsers = userCountRs.getInt(1);
            
            // now do real work
            myInsertStmt = myConn.createStatement();

            userStmt = myConn.createStatement();

            userRs = userStmt.executeQuery(userSql);

            int count = 1;
            
            while (userRs.next()) {

                long userId = userRs.getLong("user_id");
                long accessLevelId = userRs.getLong("access_level_id");
                long roleId = userRs.getLong("role_id");

                System.out.println("Updating user_id: " + userId + ", access_level_id=" + accessLevelId + ", role_id=" + roleId);
                System.out.println(count + " out of " + totalUsers + ",  " + getPercentage(count, totalUsers));
                
                for (TemplateApp tempApp : templateApps) {

                    if ((tempApp.getAccessLevelId() == accessLevelId) && (tempApp.getRoleId() == roleId)) {
                        String finalInsertSql = String.format(insertSql, userId, tempApp.getCommunityId(), tempApp.getAppId());
                        myInsertStmt.execute(finalInsertSql);
                    }
                }
                
                count++;
            }
        } finally {
            userRs.close();
            userStmt.close();
            myInsertStmt.close();
        }


    }

    private static List<TemplateApp> getTemplateApps(Connection myConn) throws Exception {

        List<TemplateApp> templateApps = new ArrayList<TemplateApp>();

        String sql = "select * from template_organization_community_application";

        Statement myStmt = null;
        ResultSet myRs = null;

        try {
            myStmt = myConn.createStatement();
            myRs = myStmt.executeQuery(sql);

            while (myRs.next()) {
                TemplateApp tempApp = new TemplateApp();

                tempApp.setAppId(myRs.getLong("application_id"));
                tempApp.setCommunityId(myRs.getLong("community_id"));
                tempApp.setRoleId(myRs.getLong("role_id"));
                tempApp.setAccessLevelId(myRs.getLong("access_level_id"));

                templateApps.add(tempApp);
            }
        } finally {
            myRs.close();
            myStmt.close();
        }

        return templateApps;
    }

    private static List<Long> getOrgIds(Connection myConn) throws Exception {
        List<Long> orgIds = new ArrayList<Long>();

        String sql = "select org_id from organization_cwd";

        Statement myStmt = null;
        ResultSet myRs = null;

        try {
            myStmt = myConn.createStatement();
            myRs = myStmt.executeQuery(sql);

            while (myRs.next()) {
                orgIds.add(myRs.getLong("org_id"));
            }
        } finally {
            myRs.close();
            myStmt.close();
        }

        return orgIds;
    }

    private static void insertOrgApps(Connection myConn, List<Long> orgIds, List<TemplateApp> templateApps) throws Exception {

        String sql = "insert into organization_community_application "
                + "(organization_id, community_id, application_id, access_level_id, role_id) "
                + "values (%d, %d, %d, %d, %d)";

        Statement myStmt = null;

        int count = 1;
        
        try {
            myStmt = myConn.createStatement();

            for (long tempOrgId : orgIds) {
                
                System.out.println("Updating org id: " + tempOrgId);
                System.out.println(count + " out of " + orgIds.size() + ",  " + getPercentage(count, orgIds.size()));
    
                for (TemplateApp tempApp : templateApps) {
                    String finalSql = String.format(sql, tempOrgId, tempApp.getCommunityId(), tempApp.getAppId(), tempApp.getAccessLevelId(), tempApp.getRoleId());

                    myStmt.execute(finalSql);
                }
                
                count++;
            }
        } finally {
            myStmt.close();
        }
    }

    private static void prompt() {
        System.out.println("Performing app config backfill for:");
        System.out.println("jdbcUrl: " + url + "\n");
        
        System.out.println("Are you sure yes/[no]?");
        
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        
        if (StringUtils.startsWithIgnoreCase(input, "yes")) {
            return;
        }
        else {
            System.out.println("\nOperation cancelled.\n");
            System.exit(0);
        }
    }

    private static String getPercentage(int count, int size) {
        
        double val = (count / (float) size) * 100;
        
        return String.format("%.0f%%", val);        
    }
}
