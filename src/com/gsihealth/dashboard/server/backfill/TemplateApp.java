package com.gsihealth.dashboard.server.backfill;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

/**
 *
 * @author Chad Darby
 */
public class TemplateApp {
 
    private long appId;
    private long communityId;
    private long accessLevelId;
    private long roleId;

    public long getAppId() {
        return appId;
    }

    public void setAppId(long appId) {
        this.appId = appId;
    }

    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }

    public long getAccessLevelId() {
        return accessLevelId;
    }

    public void setAccessLevelId(long accessLevelId) {
        this.accessLevelId = accessLevelId;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }
    
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
