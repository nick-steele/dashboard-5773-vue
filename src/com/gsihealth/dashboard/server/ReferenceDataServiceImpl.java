package com.gsihealth.dashboard.server;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.gsihealth.dashboard.client.common.PersonInfo;
import com.gsihealth.dashboard.common.ApplicationInfo;
import com.gsihealth.dashboard.client.service.ReferenceDataService;
import com.gsihealth.dashboard.common.ClientApplicationProperties;
import com.gsihealth.dashboard.common.Constants;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.ReferenceData;
import com.gsihealth.dashboard.common.ValueHolder;
import com.gsihealth.dashboard.common.util.AppConfigConstants;
import com.gsihealth.dashboard.server.dao.*;
import com.gsihealth.entity.*;
import com.gsihealth.dashboard.entity.dto.ProgramNameDTO;
import com.gsihealth.entity.insurance.PayerClass;
import com.gsihealth.entity.insurance.PayerPlan;
import com.gsihealth.entity.BaseCode;
import com.gsihealth.entity.GenderCode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Loads reference data
 *
 * @author Chad Darby
 */
public class ReferenceDataServiceImpl extends RemoteServiceServlet implements ReferenceDataService {

    private Logger logger = Logger.getLogger(getClass().getName());
    private ReferenceDataDAO referenceDataDAO;
    private CountyDAO countyDAO;
    private Map<Long, ReferenceData> referenceDataMap;
    private AlertSearchApplicationInfoDAO alertSearchApplicationInfoDAO;
    private CommunityCareteamDAO careteamDAO;
    private PayerPlanDAO payerPlanDAO;
    private EulaDAO eulaDAO;
    private SamhsaDAO samhsaDAO;
    private AccessLevelDAO accessLevelDAO;
    private PatientProgramDAO patientProgramDAO;
    
    private Map<String, String> defaultEmrollmentButtons;

    /**
     * Initializes the service with DAOs
     */
    @Override
    public void init() throws ServletException {

        try {
            logger.info("Loading Reference Data...");

            ServletContext application = getServletContext();
            referenceDataDAO = (ReferenceDataDAO) application.getAttribute(WebConstants.REFERENCE_DATA_DAO_KEY);

            alertSearchApplicationInfoDAO = (AlertSearchApplicationInfoDAO) application.getAttribute(WebConstants.ALERT_SEARCH_APP_INFO_DAO_KEY);

            careteamDAO = (CommunityCareteamDAO) application.getAttribute(WebConstants.COMMUNITY_CARE_TEAM_DAO_KEY);

            payerPlanDAO = (PayerPlanDAO) application.getAttribute(WebConstants.PAYER_PLAN_DAO_KEY);

            eulaDAO = (EulaDAO) application.getAttribute(WebConstants.EULA_DAO_KEY);

            samhsaDAO = (SamhsaDAO) application.getAttribute(WebConstants.SAMHSA_DAO_KEY);

            accessLevelDAO = (AccessLevelDAO) application.getAttribute(WebConstants.ACCESS_LEVEL_DAO_KEY);

            patientProgramDAO = (PatientProgramDAO) application.getAttribute(WebConstants.PATIENT_PROGRAM_DAO_KEY);

            countyDAO = (CountyDAO) application.getAttribute(WebConstants.COUNTY_DAO_KEY);

            buildDefaultEnrollmentButtons();
            
            // load reference data
            referenceDataMap = new HashMap<Long, ReferenceData>();
            buildReferenceDataMap();
            

            application.setAttribute(WebConstants.REFERENCE_DATA_KEY, referenceDataMap);

            logger.info("Reference Data loaded successfully");
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error loading reference data.", exc);
            throw new ServletException(exc);
        }
    }

    protected LinkedHashMap<String, String> buildCodeMap(List<? extends BaseCode> codes) {
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();

        for (BaseCode tempCode : codes) {
            String key = tempCode.getCode();
            String value = tempCode.getValue();

            data.put(key, value);
        }

        return data;
    }

    protected String[] getPostalStateCodes(Long communityId) {
        return referenceDataDAO.getPostalStateCodes(communityId);
    }

    protected LinkedHashMap<String, String> getGenderCodes(Long communityId) throws PortalException {

        try {
            List<GenderCode> codes = referenceDataDAO.findGenderCodeEntities(communityId);
            LinkedHashMap<String, String> data = buildCodeMap(codes);

            return data;

        } catch (Exception exc) {
            final String message = "Error loading gender codes.";
            logger.logp(Level.SEVERE, getClass().getName(), "getGenderCodes", message, exc);

            throw new PortalException(message);
        }

    }

    protected String[] getProgramLevels(Long communityId) throws PortalException {

        try {
            List<ProgramLevel> codes = referenceDataDAO.findProgramLevelEntities(communityId);
            String[] data =  convertProgramLevel(codes);

            return data;

        } catch (Exception exc) {
            final String message = "Error loading program levels.";
            logger.logp(Level.SEVERE, getClass().getName(), "getProgramLevels", message, exc);

            throw new PortalException(message);
        }
    }

    protected LinkedHashMap<Integer,String> getProgramLevelMap(Long communityId) throws PortalException {

        try {
            List<ProgramLevel> codes = referenceDataDAO.findProgramLevelEntities(communityId);
            return convertProgramLevelMap(codes);

        } catch (Exception exc) {
            final String message = "Error loading program levels.";
            logger.logp(Level.SEVERE, getClass().getName(), "getProgramLevels", message, exc);

            throw new PortalException(message);
        }
    }

    private LinkedHashMap<Integer,String> convertProgramLevelMap(List<ProgramLevel> list) {

        LinkedHashMap<Integer,String> data = new LinkedHashMap<Integer,String>();
        for (ProgramLevel pl: list) {
            data.put(pl.getProgramLevelPK().getId(), pl.getValue());
        }
        return data;
    }


    private String[] convertProgramLevel(List<ProgramLevel> list) {

        String[] data = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            data[i] = list.get(i).getValue();
        }
        return data;
    }

    protected String[] getProgramNames(Long communityId) throws PortalException {
        try {
            logger.info("referenceDataDAO=" + referenceDataDAO);
            logger.info("communityId=" + communityId);

            String[] programNames = referenceDataDAO.getProgramNames(communityId);
            return programNames;
        } catch (Exception exc) {

            exc.printStackTrace();

            final String message = "Error loading program names.";
            logger.logp(Level.SEVERE, getClass().getName(), "getProgramNames", message, exc);

            throw new PortalException(message);
        }
    }

    private String[] convertList(List<String> data) {
        String[] theArray = data.toArray(new String[data.size()]);

        return theArray;
    }

    private String[] convert(List<ValueHolder> list) {

        String[] data = new String[list.size()];

        for (int i = 0; i < list.size(); i++) {
            ValueHolder tempValueHolder = list.get(i);
            data[i] = tempValueHolder.getValue();
        }

        return data;
    }

    protected String[] getUserCredentials(long communityId) throws PortalException {
        try {
            List<UserCredential> userCredentialList = referenceDataDAO.findUserCredentialEntities(communityId);
            String[] data = convertUserCredentials(userCredentialList);

            return data;
        } catch (Exception exc) {
            final String message = "Error loading user credentials.";
            logger.logp(Level.SEVERE, getClass().getName(), "getUserCredentials", message, exc);

            throw new PortalException(message);
        }
    }

    private String[] convertUserCredentials(List<UserCredential> list) {

        String[] data = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {

            data[i] = list.get(i).getCredentialName();
        }

        return data;
    }

    protected String[] getUserPrefixes(long communityId) throws PortalException {
        try {
            List<UserPrefix> codes = referenceDataDAO.findUserPrefixEntities(communityId);
            String[] data = convertUserPrefixes(codes);

            return data;
        } catch (Exception exc) {
            final String message = "Error loading user prefixes.";
            logger.logp(Level.SEVERE, getClass().getName(), "getUserPrefixes", message, exc);

            throw new PortalException(message);
        }
    }

    private String[] convertUserPrefixes(List<UserPrefix> list) {

        String[] data = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            data[i] = list.get(i).getPrefixName();
        }
        return data;
    }

    @Override
    public ClientApplicationProperties getClientApplicationProperties(Long communityId) throws PortalException {
        HttpServletRequest threadLocalRequest = this.getThreadLocalRequest();
        int sessionTimeoutInSeconds = 30;
        if (threadLocalRequest != null) {
            HttpSession session = threadLocalRequest.getSession();
            sessionTimeoutInSeconds = session.getMaxInactiveInterval();
        }
        logger.info("setting sessionTime out to: " + sessionTimeoutInSeconds + " seconds");
        ClientApplicationProperties clientApplicationProperties = referenceDataMap.get(communityId).getClientApplicationProperties();
        clientApplicationProperties.setSessionTimeoutInSeconds(sessionTimeoutInSeconds);
        return clientApplicationProperties;
    }



    private ClientApplicationProperties loadClientApplicationProperties(long communityId) {
        ClientApplicationProperties clientApplicationProperties = new ClientApplicationProperties();
        ServletContext application = getServletContext();
        ConfigurationDAO configurationDAO = (ConfigurationDAO) application.getAttribute(WebConstants.CONFIGURATION_DAO_KEY);

        String helpUrl = configurationDAO.getProperty(communityId, "help.url");
        String consentLanguage = configurationDAO.getProperty(communityId, "consent.language");

        String messageUrl = getMessageUrl(application, communityId);
        String carebookUrl = getCarebookUrl(application, communityId);
        String populationManagerUrl = getPopulationManagerUrl(application, communityId);
        String uhcEnrollmentReportUrl = getUhcEnrollmentReportUrl(application, communityId);
        String patientListUtilityUrl = getPatientUtilityListUrl(application, communityId);
        String TaskListAppUrl = getTaskListAppUrl(application, communityId);

        clientApplicationProperties.setHelpUrl(helpUrl);
        clientApplicationProperties.setMessageUrl(messageUrl);
        clientApplicationProperties.setCarebookUrl(carebookUrl);
        clientApplicationProperties.setPopulationManagerUrl(populationManagerUrl);
        clientApplicationProperties.setUhcEnrollmentReportUrl(uhcEnrollmentReportUrl);
        clientApplicationProperties.setPatientListUtilityURL(patientListUtilityUrl);
        clientApplicationProperties.setTaskListAppURL(TaskListAppUrl);

        clientApplicationProperties.setConsentLanguage(consentLanguage);

        // patient search
        boolean patientSearchEnabled = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "patient.search.enabled", "false"));
        clientApplicationProperties.setPatientSearchEnabled(patientSearchEnabled);
        
        //mirth 
        String mirthSearchPanelVisible = configurationDAO.getProperty(communityId,"mpi","mdm");
        clientApplicationProperties.setMirthSearchVisible(mirthSearchPanelVisible);
        
        // Intra-HH Community
        boolean intraHHCommunityVisible = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "intra.hh.community.field.visible", "true"));
        clientApplicationProperties.setIntraHHComunityFieldVisible(intraHHCommunityVisible);

        // GUI configs
        // header
        boolean headerLogoVisible = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "header.logo.visible", "false"));
        String headerLogoFileName = configurationDAO.getProperty(communityId, "header.logo.filename");
        int headerLogoWidth = Integer.parseInt(configurationDAO.getProperty(communityId, "header.logo.width", "0"));
        int headerLogoHeight = Integer.parseInt(configurationDAO.getProperty(communityId, "header.logo.height", "0"));
        String headerTagLine = configurationDAO.getProperty(communityId, "header.tagline", "Health Home Dashboard");
        String navLogoFilename = configurationDAO.getProperty(communityId, "nav.logo.filename", "images/redux/logos/default.png");
		// For Redux
        int userInterfaceType = Integer.parseInt(configurationDAO.getProperty(communityId, "user.interfacetype", "0"));
        clientApplicationProperties.setUserInterfaceType(userInterfaceType);
                                    
        clientApplicationProperties.setHeaderLogoVisible(headerLogoVisible);
        clientApplicationProperties.setHeaderLogoFileName(headerLogoFileName);
        clientApplicationProperties.setHeaderLogoWidth(headerLogoWidth);
        clientApplicationProperties.setHeaderLogoHeight(headerLogoHeight);
        clientApplicationProperties.setHeaderTagLine(headerTagLine);
        clientApplicationProperties.setNavLogoFilename(navLogoFilename);

        // logo
        boolean loginLogoVisible = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "login.logo.visible", "true"));
        String loginLogoFileName = configurationDAO.getProperty(communityId, "login.logo.filename", "gsihealthcoordinator.jpg");
        int loginLogoWidth = Integer.parseInt(configurationDAO.getProperty(communityId, "login.logo.width", "288"));
        int loginLogoHeight = Integer.parseInt(configurationDAO.getProperty(communityId, "login.logo.height", "38"));

        clientApplicationProperties.setLoginLogoVisible(loginLogoVisible);
        clientApplicationProperties.setLoginLogoFileName(loginLogoFileName);
        clientApplicationProperties.setLoginLogoHeight(loginLogoHeight);
        clientApplicationProperties.setLoginLogoWidth(loginLogoWidth);

        // privacy policy footer
        boolean privacyPolicyVisible = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "privacy.footer.visible", "false"));
        String privacyPolicyFooterFileName = configurationDAO.getProperty(communityId, "privacy.footer.filename");

        clientApplicationProperties.setPrivacyPolicyVisible(privacyPolicyVisible);
        clientApplicationProperties.setPrivacyPolicyFooterFileName(privacyPolicyFooterFileName);

        // patient form insurance validation
        boolean enrollmentInsuranceEffectiveDateValidationEnabled = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "enrollment.insurance.effective.date.validation.enabled", "true"));
        boolean enrollmentInsuranceMedicaidIdValidationEnabled = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "enrollment.insurance.medicaid.id.validation.enabled", "true"));

        clientApplicationProperties.setEnrollmentInsuranceEffectiveDateValidationEnabled(enrollmentInsuranceEffectiveDateValidationEnabled);
        clientApplicationProperties.setEnrollmentInsuranceMedicaidIdValidationEnabled(enrollmentInsuranceMedicaidIdValidationEnabled);

        //Enrollment App Program Name CBC Requiement
        String healthHomeLabel = configurationDAO.getProperty(communityId, "health.home.label");
        clientApplicationProperties.setHealthHomeLabel(healthHomeLabel);

        boolean programNameRequired = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "program.name.required", "true"));
        clientApplicationProperties.setProgramNameRequired(programNameRequired);

        // //Patient Search -Suppress Intitial Screen Load CBC Requiement
        boolean candidatesEnrollmentEnable = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "candidates.enrollment.disable.initial.load", "false"));
        clientApplicationProperties.setCandidatesEnrollmentDisableIntialLoad(candidatesEnrollmentEnable);

        boolean enrolledPatientEnable = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "enrolled.patient.disable.initial.load", "false"));
        clientApplicationProperties.setEnrolledPatientDisableIntialLoad(enrolledPatientEnable);

        boolean showPatientOtherIdsCrud = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "show.patient.other.ids.crud", "false"));
        clientApplicationProperties.setShowPatientOtherIdsCrud(showPatientOtherIdsCrud);

        boolean healthHomeRequired = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "health.home.required", "true"));
        clientApplicationProperties.setHealthHomeRequired(healthHomeRequired);

        // MAX_PATIENT_SEARCH_RESULTS_SIZE
        int maxPatientSearchResultsSize = Integer.parseInt(configurationDAO.getProperty(communityId, "max.patient.search.results.size", "1000"));
        clientApplicationProperties.setMaxPatientSearchResultsSize(maxPatientSearchResultsSize);

        // RPC Retry properties
        int rpcRetryMaxAttempts = Integer.parseInt(configurationDAO.getProperty(communityId, "rpc.retry.max.attempts", "5"));
        int rpcRetryIntervalInMillis = Integer.parseInt(configurationDAO.getProperty(communityId, "rpc.retry.interval.millis", "1000"));

        clientApplicationProperties.setRpcRetryMaxAttempts(rpcRetryMaxAttempts);
        clientApplicationProperties.setRpcRetryIntervalInMillis(rpcRetryIntervalInMillis);

        //patient engagement
        boolean patientEngagementEnable = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "patient.engagement.enable", "true"));
        clientApplicationProperties.setPatientEngagementEnable(patientEngagementEnable);

        // Add patient to care team Label
        String addPatientToCareTeamLabel = configurationDAO.getProperty(communityId, "patient.To.CareTeamLabel");
        clientApplicationProperties.setPatientToCareTeamLabel(addPatientToCareTeamLabel);

        //add patient to care team msg
        String addPatientToCareTeam = configurationDAO.getProperty(communityId, "add.patient.to.careteam");
        clientApplicationProperties.setAddPatientToCareTeamMsg(addPatientToCareTeam);

        //remove patient from careteam msg 
        String removePatientFromCareTeam = configurationDAO.getProperty(communityId, "remove.patient.to.careteam");
        clientApplicationProperties.setRemovePatientFromCareTeamMsg(removePatientFromCareTeam);

        //login form config
        String attestLabel = configurationDAO.getProperty(communityId, "attestation.label");
        clientApplicationProperties.setAttestLabel(attestLabel);

        String federalLink = configurationDAO.getProperty(communityId, "federal.link");
        clientApplicationProperties.setFederalLink(federalLink);

        String federalLinkMessage = configurationDAO.getProperty(communityId, "federal.link.message");
        clientApplicationProperties.setFederalMessage(federalLinkMessage);

        String articleLinkMessage = configurationDAO.getProperty(communityId, "article.link.message");
        clientApplicationProperties.setArticleLinkMessage(articleLinkMessage);

        String articleLink = configurationDAO.getProperty(communityId, "article.link");
        clientApplicationProperties.setArticleLink(articleLink);

        String nysLinkMessage = configurationDAO.getProperty(communityId, "nys.link.message");
        clientApplicationProperties.setNysLinkMessage(nysLinkMessage);

        String nysLink = configurationDAO.getProperty(communityId, "nys.link");
        clientApplicationProperties.setNysLink(nysLink);

        boolean loginAttributesEnable = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "login.attributes.enable", "true"));
        clientApplicationProperties.setLoginAttributesEnable(loginAttributesEnable);
        
        boolean requireMedicaidID = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "requireMedicaidID", "false"));
        clientApplicationProperties.setRequireMedicaidID(requireMedicaidID);
        
        boolean requireMedicareID = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "requireMedicareID", "false"));
        clientApplicationProperties.setRequireMedicareID(requireMedicareID);

        boolean manageConsent = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "manage.consent.withut.enrollment", "true"));
        clientApplicationProperties.setManageConsent(manageConsent);

        String programEndReasonLabel = configurationDAO.getProperty(communityId, "program.end.reason.label");
        clientApplicationProperties.setProgramEndReasonLabel(programEndReasonLabel);
        clientApplicationProperties.setCommunityId(communityId);
        boolean devmode = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "app.devmode", "false"));
        clientApplicationProperties.setDevmode(devmode);
        
        boolean enableClientMFA = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "enable.client.MFA", "false"));
        clientApplicationProperties.setEnableClientMFA(enableClientMFA);

        boolean allowSubsequentLogin= Boolean.parseBoolean(configurationDAO.getProperty(communityId, "allow.subsequent.login", "true"));
        clientApplicationProperties.setAllowSubsequentLogin(allowSubsequentLogin);
        
        String authyApiKey= configurationDAO.getProperty(communityId, "authy.api.key");
        clientApplicationProperties.setAuthyApiKey(authyApiKey);
        
        String patientProgramButtonText= configurationDAO.getProperty(communityId, "patient.program.button.text");
        clientApplicationProperties.setPatientProgramButtonText(patientProgramButtonText);
        
        String healthHomeProviderButtonText= configurationDAO.getProperty(communityId, "health.home.provider.button.text");
        clientApplicationProperties.setHealthHomeProviderButtonText(healthHomeProviderButtonText);
     
        /* Satyendra
        JIRA - 2491 Prevent users from inactivating patient in active programs.
        */
         boolean preventPatientInactivation = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "prevent.inactivation.when.active.programs", "false"));
         clientApplicationProperties.setPreventPatientInactivation(preventPatientInactivation);
       
         /*
         * Children HealthHome Configuration Parameters 
         * DASHBOARD-780
         * 
         * childrens.health.home
         * minor.age
         * minor.warning
         * disable.minor.direct.messaging
         * 
         * TODO: can.consent.enroll.minors (list of role ids)
         * 
         * DB: gsicdr.configuration (only for carebook code - handled by Ajay )
         * ==========================
         * carebook.lock.warning
         * carebook.break.reasons (may need a master table)
         */
      
        boolean childrensHealthHome = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "childrens.health.home"));
        clientApplicationProperties.setChildrensHealthHome(childrensHealthHome);
        
        int minorAge = Integer.parseInt(configurationDAO.getProperty(communityId, "minor.age", "18"));
        clientApplicationProperties.setMinorAge(minorAge);
        
        String minorWarning = configurationDAO.getProperty(communityId, "minor.warning");
        clientApplicationProperties.setMinorWarning(minorWarning);
        
        boolean disableMinorDirectMessaging = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "disable.minor.direct.messaging"));
        clientApplicationProperties.setDisableMinorDirectMessaging(disableMinorDirectMessaging);
         
        // JIRA - 890 Activity Tracker
        String activityTrackerWindowTitle = configurationDAO.getProperty(communityId, "activity.tracker.window.title");
        clientApplicationProperties.setActivityTrackerWindowTitle(activityTrackerWindowTitle);
        
        String activityTrackerButtonText = configurationDAO.getProperty(communityId, "activity.tracker.button.label");
        clientApplicationProperties.setActivityTrackerButtonText(activityTrackerButtonText);
        
        String activityNameText = configurationDAO.getProperty(communityId, "activity.name.label");
        clientApplicationProperties.setActivityNameText(activityNameText);
        
        String activityValueText = configurationDAO.getProperty(communityId, "activity.value.label");
        clientApplicationProperties.setActivityValueText(activityValueText);
        
        String reportGenerationDurationWindowTitle = configurationDAO.getProperty(communityId, "pts.date.range.window.title");
        clientApplicationProperties.setReportGenerationDurationWindowTitle(reportGenerationDurationWindowTitle);
        
        boolean patientSelfAssertedConsentAccess = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "patient.self.asserted.consent.access"));
        clientApplicationProperties.setPatientSelfAssertedConsentAccess(patientSelfAssertedConsentAccess);
        
        String patientSelfAssertWarningText = configurationDAO.getProperty(communityId, "patient.self.assert.warning.text");
        clientApplicationProperties.setPatientSelfAssertWarningText(patientSelfAssertWarningText);
                
        // JIRA 1024
        boolean warnConsentStatusChangeFlag = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "warn.consent.status.change.flag"));
        clientApplicationProperties.setWarnConsentStatusChangeFlag(warnConsentStatusChangeFlag);
        
        String warnConsentStatusChange = configurationDAO.getProperty(communityId, "warn.consent.status.change");
        clientApplicationProperties.setWarnConsentStatusChange(warnConsentStatusChange);

        String warnActivePrograms = configurationDAO.getProperty(communityId, "warn.active.programs");
        clientApplicationProperties.setWarnActivePrograms(warnActivePrograms);
        
        String warnConsentStatusChangeDialogTitle = configurationDAO.getProperty(communityId, "warn.consent.status.change.dialog.title");
        clientApplicationProperties.setWarnConsentStatusChangeDialogTitle(warnConsentStatusChangeDialogTitle);

        String warnActiveProgramsDialogTitle = configurationDAO.getProperty(communityId, "warn.active.programs.dialog.title");
        clientApplicationProperties.setWarnActiveProgramsDialogTitle(warnActiveProgramsDialogTitle);
        
        String warnActiveProgramsInactiveButtonText = configurationDAO.getProperty(communityId, "warn.active.programs.inactivate.button.text");
        clientApplicationProperties.setWarnActiveProgramsInactiveButtonText(warnActiveProgramsInactiveButtonText);
        
        boolean obfuscateSearchResultsForConsent = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "obfuscate.search.results.for.consent"));
        clientApplicationProperties.setObfuscateSearchResultsForConsent(obfuscateSearchResultsForConsent);
        
        String searchForAdditionalPatientsText = configurationDAO.getProperty(communityId, "search.for.additional.patients.text");
        clientApplicationProperties.setSearchForAdditionalPatientsText(searchForAdditionalPatientsText);
        
        String normalPatientsSearchText = configurationDAO.getProperty(communityId, "normal.patients.search.text");
        clientApplicationProperties.setNormalPatientsSearchText(normalPatientsSearchText);
        
        String searchForAdditionalPatientsInformationIconText = configurationDAO.getProperty(communityId, "search.for.additional.patients.information.icon.text");
        clientApplicationProperties.setSearchForAdditionalPatientsInformationIconText(searchForAdditionalPatientsInformationIconText);
        
        //JIRA 727
        boolean multiTenantLoginPageFlag = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "multitenant.login.page.flag"));
        clientApplicationProperties.setMultiTenantLoginPageFlag(multiTenantLoginPageFlag);

        String supervisorFormTitle = configurationDAO.getProperty(communityId, "supervisor.form.title");
        clientApplicationProperties.setSupervisorFormTitle(supervisorFormTitle);
        
        //JIRA 2523
        String daeCloseButtonText = configurationDAO.getProperty(communityId, "duplicate.already.exists.close.button.text");
        clientApplicationProperties.setDaeCloseButtonText(daeCloseButtonText);
        
        String daeDescLabelText = configurationDAO.getProperty(communityId, "duplicate.already.exists.desc.label.text");
        clientApplicationProperties.setDaeDescLabelText(daeDescLabelText);

        String daeSelectButtonText = configurationDAO.getProperty(communityId, "duplicate.already.exists.select.button.text");
        clientApplicationProperties.setDaeSelectButtonText(daeSelectButtonText);

        String daeTitleLabelText = configurationDAO.getProperty(communityId, "duplicate.already.exists.title.label.text");
        clientApplicationProperties.setDaeTitleLabelText(daeTitleLabelText);

        String dseAddNowButtonText = configurationDAO.getProperty(communityId, "duplicate.similar.exists.addnow.button.text");
        clientApplicationProperties.setDseAddNowButtonText(dseAddNowButtonText);

        String dseDescLabelText = configurationDAO.getProperty(communityId, "duplicate.similar.exists.desc.label.text");
        clientApplicationProperties.setDseDescLabelText(dseDescLabelText);

        String dseSelectButtonText = configurationDAO.getProperty(communityId, "duplicate.similar.exists.select.button.text");
        clientApplicationProperties.setDseSelectButtonText(dseSelectButtonText);

        String dseTitleLabelText = configurationDAO.getProperty(communityId, "duplicate.similar.exists.title.label.text");
        clientApplicationProperties.setDseTitleLabelText(dseTitleLabelText);

        //Spira 7137
        String dseUpdateDescLabelText = configurationDAO.getProperty(communityId, "duplicate.similar.exists.update.desc.label.text");
        clientApplicationProperties.setDseUpdateDescLabelText(dseUpdateDescLabelText);
        
        String dseUpdateSaveButtonText = configurationDAO.getProperty(communityId, "duplicate.similar.exists.update.save.button.text");
        clientApplicationProperties.setDseUpdateSaveButtonText(dseUpdateSaveButtonText);

        String dseUpdateCloseButtonText = configurationDAO.getProperty(communityId, "duplicate.similar.exists.update.close.button.text");
        clientApplicationProperties.setDseUpdateCloseButtonText(dseUpdateCloseButtonText);

        //Spira 7467
        boolean mppStatusEffectiveDateEditable = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "mpp.status.effective.date.editable"));
        clientApplicationProperties.setMppStatusEffectiveDateEditable(mppStatusEffectiveDateEditable);

        String mppStatusEffectiveDateMessage = configurationDAO.getProperty(communityId, "mpp.status.effective.date.message");
        clientApplicationProperties.setMppStatusEffectiveDateMessage(mppStatusEffectiveDateMessage);

        //DASHBOARD-4126
        Map<String, String> demographicButtonTextMap = null;
        String demographicButtonTextString = configurationDAO.getProperty(communityId, "homepage.demographic.bundle");
        
        if (demographicButtonTextString != null) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                demographicButtonTextMap = mapper.readValue(demographicButtonTextString, new TypeReference<Map<String, String>>(){});
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
        	demographicButtonTextMap = defaultEmrollmentButtons;            
        }
        
        demographicButtonTextMap = (demographicButtonTextMap == null) ? defaultEmrollmentButtons : demographicButtonTextMap;
        
        clientApplicationProperties.setDemographicSearchButton(demographicButtonTextMap.get("button.search"));
        clientApplicationProperties.setDemographicClearButton(demographicButtonTextMap.get("button.clear"));
        clientApplicationProperties.setDemographicSelectButton(demographicButtonTextMap.get("button.select"));
        clientApplicationProperties.setDemographicCheckDuplicateButton(demographicButtonTextMap.get("button.check_duplicate"));
        clientApplicationProperties.setDemographicSaveButton(demographicButtonTextMap.get("button.save"));
        clientApplicationProperties.setDemographicCancelButton(demographicButtonTextMap.get("button.cancel"));
        clientApplicationProperties.setDemographicModifyButton(demographicButtonTextMap.get("button.modify"));
        clientApplicationProperties.setDemographicEnrollmentButton(demographicButtonTextMap.get("button.enrollment"));
        clientApplicationProperties.setDemographicAddToPatientButton(demographicButtonTextMap.get("button.add_to_patient"));

        //DASHBOARD-3752
        String providerTypes = configurationDAO.getProperty(communityId, "careteam.provider_types");
        LinkedHashMap<String, String> providerTypeList = null;
        if (providerTypes != null) {
            try {
                ObjectMapper mapper = new ObjectMapper();
                providerTypeList = mapper.readValue(providerTypes, new TypeReference<LinkedHashMap<String, String>>() {
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            providerTypeList = new LinkedHashMap<String, String>();
            providerTypeList.put("_blank", "");
        }
        clientApplicationProperties.setProviderTypes(providerTypeList);
        
        return clientApplicationProperties;      
    }

    @Override
    public ReferenceData getReferenceData(Long communityId) throws PortalException {
        return referenceDataMap.get(communityId);
    }



    private ReferenceData loadReferenceData(Long communityId) throws Exception {
        logger.info("loadReferenceData .communityId.." + communityId);

        ReferenceData referenceData = new ReferenceData();

        referenceData.setPostalStateCodes(getPostalStateCodes(communityId));
        referenceData.setEnrollmentStatuses(getEnrollmentStatuses(communityId));
        referenceData.setGenderCodes(getGenderCodes(communityId));
        referenceData.setProgramLevels(getProgramLevels(communityId));
        referenceData.setProgramLevelMap(getProgramLevelMap(communityId));
        referenceData.setProgramNames(getProgramNames(communityId));
        referenceData.setUserCredentials(getUserCredentials(communityId));
        referenceData.setUserPrefixes(getUserPrefixes(communityId));

        referenceData.setEthnicCodes(getEthnicCodes(communityId));
        referenceData.setLanguageCodes(getLanguageCodes(communityId));
        referenceData.setRaceCodes(getRaceCodes(communityId));

        // set SWB_HH ORG ID
//        referenceData.setSouthwestBrooklynOrgId(getSouthwestBrooklynOrgId(communityId));

        // access level map
        referenceData.setAccessLevelMap(getAccessLevelMap(communityId));

        // application info map
        referenceData.setApplicationInfoMap(getApplicationInfoMap(communityId));

        // reasons for refusal
//        referenceData.setEnrollmentReasonsForInactivation(getEnrollmentReasonsForInactivation(communityId));
        // patient relationship
        referenceData.setPatientRelationship(getPatientRelationship(communityId));
        // disenrollment reasons
        referenceData.setAllDisenrollmentReasons(getDisenrollmentReasons(communityId));
        // Duplicate Careteam Roles
        referenceData.setDuplicateCareteamRoles(getDuplicateCareteamRoles(communityId));
        // Required Careteam Roles
        referenceData.setRequiredCareteamRoles(getRequiredCareteamRoles(communityId));

        //Payer Class
        referenceData.setPayerClass(getPayerClass(communityId));
        //Payer Plan
        referenceData.setPayerPlan(getPayerPlan(communityId));

        // EULA
        referenceData.setEula(getEula(communityId));

        //SMAHSA
        referenceData.setSamhsa(getSamhsa(communityId));

        //Program Health Home
        referenceData.setProgramHealthHome(getProgramHealthHome(communityId));
        //Program Health Home
        referenceData.setProgramNameHealthHome(getProgramNameHealthHome(communityId));
        
        //JIRA 1023
        //program name ids for health home
        referenceData.setProgramForHealthHomeCodes(getProgramForHealthHomeCodes(communityId));

        // role
        referenceData.setUserRole(getUserRoles(communityId));
        // InProvider Names
//        referenceData.setInProviderName(getInProviderName(communityId));
//        // OutProvider Names
//        referenceData.setOutProviderName(getOutProviderName(communityId));

        referenceData.setProgramNameMap(getProgramNameMap(communityId));
        // Patient Status
        referenceData.setPatientStatus(getPatientStatus(communityId));

        referenceData.setHealthHomeCodes(getHealthHomeCodes(communityId));

        referenceData.setTerminationReasonCodes(getTerminationReasonCodes(communityId));

        referenceData.setReasonForPatientStatus(getReasonForPatientStatus(communityId));

        referenceData.setHhRequiredForProgram(getHhRequiredForProgram(communityId));

//        referenceData.setHealthHomeForProgram(getHealthHomeForProgram(communityId));

        referenceData.setDeleteReasons(getDeleteReasons(communityId));

        referenceData.setStatusWithReason(getStatusWithReason(communityId));
        
        referenceData.setActivityNamesMap(getActivityNamesMap(communityId));

        referenceData.setClientApplicationProperties(loadClientApplicationProperties(communityId));

        referenceData.setCounty(getCounty(communityId));
       
        return referenceData;
    }

    private LinkedHashMap<String, String> getEthnicCodes(long communityId) throws PortalException {
        try {
            List<EthnicCode> codes = referenceDataDAO.findEthnicCodeEntities(communityId);
            LinkedHashMap<String, String> data = buildCodeMap(codes);

            return data;

        } catch (Exception exc) {
            final String message = "Error loading ethnic codes.";
            System.out.println(message + " " + exc);
            logger.logp(Level.SEVERE, getClass().getName(), "getEthnicCodes", message, exc);

            throw new PortalException(message);
        }
    }

    private LinkedHashMap<String, String> getLanguageCodes(long communityId) throws PortalException {
        try {
            List<LanguageCode> codes = referenceDataDAO.findLanguageCodeEntities(communityId);
            LinkedHashMap<String, String> data = buildCodeMap(codes);

            return data;

        } catch (Exception exc) {
            final String message = "Error loading language codes.";
            logger.logp(Level.SEVERE, getClass().getName(), "getLanguageCodes", message, exc);

            throw new PortalException(message);
        }
    }

    private LinkedHashMap<String, String> getRaceCodes(long communityId) throws PortalException {
        try {
            List<RaceCode> codes = referenceDataDAO.findRaceCodeEntities(communityId);
            LinkedHashMap<String, String> data = buildCodeMap(codes);

            return data;

        } catch (Exception exc) {
            final String message = "Error loading race codes.";
            logger.logp(Level.SEVERE, getClass().getName(), "getRaceCodes", message, exc);

            throw new PortalException(message);
        }
    }
    private LinkedHashMap<String, String> getCounty(long communityId) throws PortalException {
        try {
            List<County> codes = countyDAO.findCountys(communityId);
            LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
            for (County county : codes) {
                data.put(Long.toString(county.getCountyPK().getCountyId()), county.getCountyName());
            }

            return data;

        } catch (Exception exc) {
            final String message = "Error loading county.";
            logger.logp(Level.SEVERE, getClass().getName(), "county", message, exc);

            throw new PortalException(message);
        }
    }
    
         // JIRA - 958 SWBOID is not required, community OID is being used to add patients to MPI and to send PIX/XCAML messages
        
//    private long getSouthwestBrooklynOrgId(long communityId) {
//
//        ServletContext application = getServletContext();
//        Map<Long, CommunityContexts> map = (Map<Long, CommunityContexts>) application.getAttribute(Constants.COMMUNITY_SPECIFIC_DATA_KEY);
//        if (map.get(communityId).getSouthwestBrooklynOidOrg() != null) {
//            long swbOrgId = map.get(communityId).getSouthwestBrooklynOidOrg();
//            return swbOrgId;
//        }
//        return -1;       
//    }

    private String getMessageUrl(ServletContext application, long communityID) {
        return getExternalApplicationUrl(application, AppConfigConstants.MESSAGES_APP_ID, communityID);
    }

    private String getCarebookUrl(ServletContext application, long communityID) {
        return getExternalApplicationUrl(application, AppConfigConstants.CAREBOOK_APP_ID, communityID);
    }

    private String getPopulationManagerUrl(ServletContext application, long communityID) {
        return getExternalApplicationUrl(application, AppConfigConstants.POPULATION_MANAGER_APP_ID, communityID);
    }

    private String getUhcEnrollmentReportUrl(ServletContext application, long communityID) {
        return getExternalApplicationUrl(application, AppConfigConstants.UHC_ENROLLMENT_REPORT_APP_ID, communityID);
    }
    
    private String getPatientUtilityListUrl(ServletContext application, long communityID) {
        return getExternalApplicationUrl(application, AppConfigConstants.PATIENT_LIST_UTILITY_APP_ID, communityID);
    }
     
    private String getTaskListAppUrl(ServletContext application, long communityID) {
        return getExternalApplicationUrl(application, AppConfigConstants.TASK_LIST_APP_ID, communityID);
    }

    private String getExternalApplicationUrl(ServletContext application, long appId, long communityID) {
        ApplicationDAO applicationDAO = (ApplicationDAO) application.getAttribute(WebConstants.APPLICATION_DAO_KEY);

        //long communityId=com.gsihealth.dashboard.server.dao.Constants.GSI_COMMUNITY_ID;
        Application theApp = applicationDAO.findApplication(appId, communityID);

        // if the app is not null then get the URL, else return null
        String appUrl = theApp != null ? theApp.getExternalApplicationUrl() : null;

        return appUrl;
    }

    private Map<String, ApplicationInfo> getApplicationInfoMap(long communityId) throws Exception {

        Map<String, ApplicationInfo> applicationInfoMap = new LinkedHashMap<String, ApplicationInfo>();

        try {
            List<AlertSearchApplicationInfo> alertSearchApplicationInfoEntities = alertSearchApplicationInfoDAO.findAlertSearchApplicationInfoEntities(communityId);

            for (AlertSearchApplicationInfo temp : alertSearchApplicationInfoEntities) {

                if (temp.getApplication() != null) {

                    Application tempApp = temp.getApplication();
                    String screenName = tempApp.getScreenName();
                    String alertIconFileName = temp.getAlertIconFileName();
                    long appId = tempApp.getApplicationPK().getApplicationId();

                    applicationInfoMap.put(tempApp.getApplicationName(), new ApplicationInfo(alertIconFileName, screenName, appId));
                }
            }
        } catch (Exception exc) {
            final String message = "Error loading application info.";
            logger.logp(Level.SEVERE, getClass().getName(), "getApplicationInfoMap", message, exc);

            throw new PortalException(message);
        }

        return applicationInfoMap;
    }

    private LinkedHashMap<Long, String> getPatientRelationship(long communityId) {

        List<PatientRelationshipCode> relationshipList = referenceDataDAO.findPatientRelationshipEntities(communityId);

        LinkedHashMap<Long, String> data = new LinkedHashMap<Long, String>();
        for (PatientRelationshipCode tempPatientRelationshipCode : relationshipList) {
            data.put(tempPatientRelationshipCode.getPatientRelationshipCodePK().getId(), tempPatientRelationshipCode.getValue());
        }

        return data;
    }

    private String[] getDisenrollmentReasons(Long communityId) {
        List<DisenrollmentReasons> data = referenceDataDAO.getDisenrollmentReasons(communityId);
        List<String> listReasons = new ArrayList<String>();
        for (DisenrollmentReasons reason : data) {
            listReasons.add(reason.getReason());
        }

        String[] reasons = convertList(listReasons);

        return reasons;
    }

    protected String[] getEnrollmentStatuses(Long communityId) throws PortalException {

        try {
            List<EnrollmentStatus> codes = referenceDataDAO.findEnrollmentStatusEntities(communityId);
            String[] enrollmentCodes = new String[codes.size()];
            for (int i = 0; i < codes.size(); i++) {
                enrollmentCodes[i] = (String) codes.get(i).getValue();
            }

            return enrollmentCodes;

        } catch (Exception exc) {
            final String message = "Error loading enrollment status.";
            logger.logp(Level.SEVERE, getClass().getName(), "getEnrollmentStatuses", message, exc);

            throw new PortalException(message);
        }
    }

    private String[] getDuplicateCareteamRoles(Long communityId) {
        List<String> duplicateRoles = careteamDAO.getDuplicateRolesForCareteam(communityId);
        return duplicateRoles.toArray(new String[duplicateRoles.size()]);
    }

    private String[] getRequiredCareteamRoles(Long communityId) {
        List<String> requiredRoles = careteamDAO.getRequiredRolesForCareteam(communityId);
        return requiredRoles.toArray(new String[requiredRoles.size()]);
    }

    private LinkedHashMap<String, String> getPayerClass(long communityId) {

        List<PayerClass> PayerClassList = payerPlanDAO.getPayerClassEntities(communityId);

        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
        for (PayerClass tempPayerClass : PayerClassList) {
            data.put(Long.toString(tempPayerClass.getPayerClassPK().getId()), tempPayerClass.getName());
        }

        return data;
    }

    private LinkedHashMap<String, String> getPayerPlan(long communityId) {

        List<PayerPlan> PayerPlanList = payerPlanDAO.getPayerPlanEntities(communityId);

        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
        for (PayerPlan tempPayerPlan : PayerPlanList) {

            data.put(Long.toString(tempPayerPlan.getPayerPlanPK().getId()), tempPayerPlan.getName());
        }

        return data;
    }

    private LinkedHashMap<String, String> getEula(Long communityId) {
        Eula eula = eulaDAO.findActiveEula(communityId);
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
        if (eula != null) {
            data.put(Integer.toString(eula.getEulaPK().getId()), eula.getContent());
        }
        return data;

    }

    private LinkedHashMap<String, String> getSamhsa(Long communityId) {
        Samhsa samhsa = samhsaDAO.findActiveSamhsa(communityId);
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
        if (samhsa != null) {
            data.put(Integer.toString(samhsa.getSamhsaPK().getId()), samhsa.getContent());
        }
        return data;
    }

    private void buildReferenceDataMap() throws Exception {

        // get list of community ids
        List<Long> communityIds = referenceDataDAO.findCommunityIds();

        // load reference data for each community
        for (Long tempCommunityId : communityIds) {
            logger.info("buildReferenceDataMap for community: " + tempCommunityId);
            ReferenceData tempReferenceData = loadReferenceData(tempCommunityId);

            referenceDataMap.put(tempCommunityId, tempReferenceData);
        }
    }
    
    private void buildDefaultEnrollmentButtons() {
    	defaultEmrollmentButtons = new HashMap<String, String>();
    	defaultEmrollmentButtons.put("button.clear", "Clear");
    	defaultEmrollmentButtons.put("button.search", "Search");
    	defaultEmrollmentButtons.put("button.select", "Select");
    	defaultEmrollmentButtons.put("button.modify", "Modify");
    	defaultEmrollmentButtons.put("button.check_duplicate", "Check if Duplicate");
    	defaultEmrollmentButtons.put("button.save", "Save");
    	defaultEmrollmentButtons.put("button.cancel", "Cancel");
    	defaultEmrollmentButtons.put("button.enrollment", "Enrollment");
    	defaultEmrollmentButtons.put("button.add_to_patient", "Add To Patient");
    }

    private Map<Long, String> getAccessLevelMap(long communityId) throws Exception {
        Map<Long, String> accessLevelMap = new HashMap<Long, String>();

        try {
            List<AccessLevel> entities = accessLevelDAO.findAccessLevelEntities(communityId);

            for (AccessLevel temp : entities) {
                //mt access level
                accessLevelMap.put(temp.getAccessLevelPK().getAccessLevelId(), temp.getAccessLevelDesc());
            }
        } catch (Exception exc) {
            final String message = "Error loading access levels.";
            logger.logp(Level.SEVERE, getClass().getName(), "getAccessLevelMap", message, exc);

            throw new PortalException(message);
        }

        return accessLevelMap;
    }

    private LinkedHashMap<Long, String> getProgramHealthHome(long communityId) {

        List<ProgramHealthHome> healthHomeList = referenceDataDAO.findProgramHealthHomeEntities(communityId);

        LinkedHashMap<Long, String> data = new LinkedHashMap<Long, String>();

        for (ProgramHealthHome healthHome : healthHomeList) {
            data.put(healthHome.getProgramHealthHomePK().getId(), healthHome.getHealthHomeValue());
        }

        return data;
    }

    private LinkedHashMap<Long, String> getProgramNameMap(long communityId) {

        List<ProgramName> programNameList = referenceDataDAO.findProgramNameMapEntities(communityId);

        LinkedHashMap<Long, String> data = new LinkedHashMap<Long, String>();

        for (ProgramName programName : programNameList) {
            data.put(programName.getProgramNamePK().getId(), programName.getValue());
        }
        
        return data;
    }

    private LinkedHashMap<Long, ProgramNameDTO> getProgramNameHealthHome(long communityId) {
        logger.info("getProgramNameHealthHome.communityId..." + communityId);
        List<ProgramName> programNameHealthHomeList = referenceDataDAO.findProgramNameEntities(communityId);
        if (!programNameHealthHomeList.isEmpty()) {
            logger.info("programNameHealthHomeList.size..." + programNameHealthHomeList.size());
        }

        LinkedHashMap<Long, ProgramNameDTO> data = new LinkedHashMap<Long, ProgramNameDTO>();
        ProgramNameDTO programNameDTO = null;

        for (ProgramName programName : programNameHealthHomeList) {
            logger.fine("in each ProgramNamePK().getId..." + programName.getProgramNamePK().getId() + ":ProgramHealthHomePK().getId:" + programName.getHealthHomeId().getProgramHealthHomePK().getId());
            programNameDTO = new ProgramNameDTO();

            programNameDTO.setId((long) programName.getProgramNamePK().getId());
            programNameDTO.setValue(programName.getValue());
            programNameDTO.setProgramHealth(programName.getHealthHomeId().getProgramHealthHomePK().getId());

            data.put((long) programName.getProgramNamePK().getId(), programNameDTO);
        }

        return data;
    }

    private LinkedHashMap<Long, String> getUserRoles(long communityId) {

        List<Role> userRoleList = referenceDataDAO.getUserRolesEntities(communityId);

        LinkedHashMap<Long, String> data = new LinkedHashMap<Long, String>();

        for (Role role : userRoleList) {
            data.put(role.getRoleId(), role.getRoleName());
        }

        return data;
    }

    private LinkedHashMap<Long, String> getPatientStatus(long communityId) {

        List<PatientStatus> patientStatuses = referenceDataDAO.findPatientStatusEntities(communityId);

        LinkedHashMap<Long, String> data = new LinkedHashMap<Long, String>();

        for (PatientStatus patientStatus : patientStatuses) {
            data.put(patientStatus.getPatientStatusPK().getId(), patientStatus.getValue());
        }

        return data;
    }

    //JIRA 1023
    private LinkedHashMap<Long, String> getHealthHomeCodes(Long communityId) throws PortalException {
        try {
            return referenceDataDAO.getHealthHomes(communityId);
        } catch (Exception exc) {
            final String message = "Error loading health home vales.";
            logger.logp(Level.SEVERE, getClass().getName(), "getHealthHomeCodes", message, exc);

            throw new PortalException(message);
        }
    }
    
    //JIRA 1023
    private LinkedHashMap<Long, List<Long>> getProgramForHealthHomeCodes(Long communityId) throws PortalException {
        try {
            LinkedHashMap<Long, List<Long>> map = new LinkedHashMap<>();
            List<ProgramName> findProgramNameEntities = referenceDataDAO.findProgramNameEntities(communityId);
            for (ProgramName programName : findProgramNameEntities) {
                Long hhId = programName.getHealthHomeId().getProgramHealthHomePK().getId();
                if(!map.containsKey(hhId)) {
                    map.put(hhId, new ArrayList<Long>());
                }
                map.get(hhId).add(programName.getProgramNamePK().getId());
            }
            return map;
        } catch (Exception exc) {
            final String message = "Error loading program codes for health home.";
            logger.logp(Level.SEVERE, getClass().getName(), "getProgramForHealthHomeCodes", message, exc);

            throw new PortalException(message);
        }
    }

    private String[] getTerminationReasonCodes(Long communityId) throws PortalException {
        try {
            return referenceDataDAO.getTerminationReason(communityId);
        } catch (Exception exc) {
            final String message = "Error loading termination reason values.";
            logger.logp(Level.SEVERE, getClass().getName(), "getTerminationReasonCodes", message, exc);

            throw new PortalException(message);
        }
    }

    private LinkedHashMap<String, String[]> getReasonForPatientStatus(long communityId) {

        List<PatientStatus> patientStatuses = referenceDataDAO.findPatientStatusEntities(communityId);
        String[] terminationReasons = referenceDataDAO.getTerminationReason(communityId);
        String[] blankReason = new String[]{"", ""};
        LinkedHashMap<String, String[]> data = new LinkedHashMap<String, String[]>();

        for (PatientStatus patientStatus : patientStatuses) {
            if (patientStatus.getTerminationRequired().equalsIgnoreCase("false")) {
                data.put(patientStatus.getValue(), blankReason);
            } //            else if(patientStatus.getValue().equalsIgnoreCase("Controlled")){
            //                 data.put(patientStatus.getValue(), blankReason);  
            //            }
            else {
                data.put(patientStatus.getValue(), terminationReasons);
            }
        }
        return data;
    }

    private LinkedHashMap<Long, String> getHhRequiredForProgram(long communityId) {

        List<ProgramName> temp = referenceDataDAO.findProgramNameEntities(communityId);

        LinkedHashMap<Long, String> data = new LinkedHashMap<Long, String>();

        for (ProgramName programName : temp) {
            data.put(programName.getProgramNamePK().getId(), programName.getHhRequired());
        }

        return data;
    }

    protected String[] getDeleteReasons(Long communityId) throws PortalException {
        try {
            logger.info("referenceDataDAO=" + referenceDataDAO);
            logger.info("communityId=" + communityId);

            String[] programDeleteReasons = referenceDataDAO.getProgramDeleteReasons(communityId);
            return programDeleteReasons;
        } catch (Exception exc) {

            exc.printStackTrace();

            final String message = "Error loading Program Delete Reasons .";
            logger.logp(Level.SEVERE, getClass().getName(), "getDeleteReasons", message, exc);

            throw new PortalException(message);
        }
    }

    private LinkedHashMap<Long, String> getStatusWithReason(long communityId) {

        List<PatientStatus> patientStatuses = referenceDataDAO.findPatientStatusEntities(communityId);
        LinkedHashMap<Long, String> data = new LinkedHashMap<Long, String>();

        for (PatientStatus patientStatus : patientStatuses) {
            data.put(patientStatus.getPatientStatusPK().getId(), patientStatus.getTerminationRequired());
        }

        return data;
    }
  
     private LinkedHashMap<Long, String> getActivityNamesMap(long communityId) {

        List<PatientActivityNames> activityList = referenceDataDAO.findActivityNameEntities(communityId);

        LinkedHashMap<Long, String> data = new LinkedHashMap<Long, String>();

        for (PatientActivityNames activity : activityList) {
            data.put(activity.getId(), activity.getName());
        }

        return data;
    }
}
