/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server;

import com.gsihealth.dashboard.common.dto.UserFormReferenceData;
import com.gsihealth.dashboard.server.dao.PatientEnrollmentDAO;
import com.gsihealth.dashboard.server.hisp.HispClient;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import java.util.logging.Logger;

/**
 *
 * @author Beth.Boose
 */
public class CommunityContexts {

    private Logger logger = Logger.getLogger(getClass().getName());
    private PatientManager patientManager;
    private PatientEnrollmentDAO patientEnrollmentDAO;
    private Long communityOrgId;
    private HispClient hispClient;

    public HispClient getHispClient() {
        return hispClient;
    }

    public void setHispClient(HispClient hispClient) {
        this.hispClient = hispClient;
    }

    public Long getCommunityOrgId() {
        return communityOrgId;
    }

    public void setCommunityOrgId(Long communityOrgId) {
        this.communityOrgId = communityOrgId;
    }

    public PatientEnrollmentDAO getPatientEnrollmentDAO() {
        return patientEnrollmentDAO;
    }

    public void setPatientEnrollmentDAO(PatientEnrollmentDAO patientEnrollmentDAO) {
        this.patientEnrollmentDAO = patientEnrollmentDAO;
    }

    public PatientManager getPatientManager() {
        return patientManager;
    }

    public void setPatientManager(PatientManager patientManager) {
        this.patientManager = patientManager;
    }

}