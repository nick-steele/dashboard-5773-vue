package com.gsihealth.dashboard.server.alertgateway;

import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.entity.PatientCommunityEnrollment;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;
import com.gsihealth.dashboard.server.CommunityContexts;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.dao.PatientEnrollmentDAO;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import com.gsihealth.dashboard.server.util.AuditUtils;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Logger;
import javax.persistence.NoResultException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Chad Darby
 */
public class BaseAlertServlet extends HttpServlet {
    protected Logger logger = Logger.getLogger(getClass().getName());
    protected PatientManager patientManager;
    protected PatientEnrollmentDAO patientEnrollmentDAO;

    @Override
    public void init() throws ServletException {
        ServletContext application = getServletContext();
    }
   
    protected PersonDTO getPersonDTO(String euid, Long communityId) throws NoResultException, Exception {
        if(patientManager == null){
            logger.info("pulling a patientManager from the application context in BaseAlertServlet");
            CommunityContexts communityContexts = getCommunityContexts(communityId);
            patientManager = communityContexts.getPatientManager();
            patientEnrollmentDAO = communityContexts.getPatientEnrollmentDAO();
        }
        SimplePerson thePerson = patientManager.getPatient(euid);

        PersonDTO personDTO = new PersonDTO();
        personDTO.setFirstName(thePerson.getFirstName());
        personDTO.setLastName(thePerson.getLastName());
        
        // get patient enrollment
        PatientCommunityEnrollment patientEnrollment = patientEnrollmentDAO.findPatientEnrollment(euid);
        long patientId = patientEnrollment.getPatientId();
                
        PatientEnrollmentDTO patientEnrollmentDTO = new PatientEnrollmentDTO();
        patientEnrollmentDTO.setPatientId(patientId);
        
        personDTO.setPatientEnrollment(patientEnrollmentDTO);
        
        return personDTO;
    }
    
    protected CommunityContexts getCommunityContexts(Long communityId){
        logger.info("base alert service, get communityContexts for community " + communityId);
        ServletContext application = getServletContext();
        CommunityContexts communityContexts = 
        ((Map<Long, CommunityContexts>)application.getAttribute(WebConstants.COMMUNITY_SPECIFIC_DATA_KEY)).get(communityId);
        return communityContexts;
    }
    
}
