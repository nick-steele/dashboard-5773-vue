package com.gsihealth.dashboard.server.alertgateway;

import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.util.AlertUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Gateway interface for generating alerts for "patient updated". 
 * External system will send the patient EUID
 * 
 * @author Chad Darby
 */
public class PatientUpdatedAlertServlet extends BaseAlertServlet {

    private Logger logger = Logger.getLogger(getClass().getName());
    
    /** 
     * Processes requests for HTTP <code>GET</code> 
     * 
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        // get the EUID
        String euid = request.getParameter("euid");
        String fromOid = request.getParameter("fromOid");
        String patientId = request.getParameter("patientId");

        PrintWriter out = response.getWriter();

        try {
            HttpSession session = request.getSession();
            LoginResult loginResult = (LoginResult) session.getAttribute(WebConstants.LOGIN_RESULT_KEY);            
            long communityId = loginResult.getCommunityId();
                        
            // send the alert
            sendPatientUpdatedAlert(communityId, euid, fromOid, patientId);

            response.setContentType("text/html;charset=UTF-8");
            out.println("SUCCESS");

            logger.info("Sent patient update alert successfully. euid=" + euid);
            
        } catch (Exception exc) {
            out.println("FAILED: " + exc.getMessage());
            logger.log(Level.WARNING, "Error sending patient updated alert. euid=" + euid, exc);            
        } finally {
            out.close();
        }
        
    }

    private void sendPatientUpdatedAlert(long communityId, String euid, String fromOid, String patientId) throws Exception {
        logger.info("inside sendPatientUpdatedAlert.  euid=" + euid);
        
        // get the patient
        PersonDTO personDTO = getPersonDTO(euid, communityId);
        
        AlertUtils.sendPatientDemographicsChangedAlertFromTreat(communityId, personDTO, fromOid, patientId);        
    }
    
}
