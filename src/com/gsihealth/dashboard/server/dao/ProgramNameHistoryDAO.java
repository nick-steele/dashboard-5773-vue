/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.ProgramNameHistory;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Satyendra Singh
 */
public interface ProgramNameHistoryDAO {

    public void addToProgramNameHistory(ProgramNameHistory programNameHistory);

    public Long getLastProgramName(Long patientId);

    //Spira 6467
    public Date getMostRecentStatusEffectiveDateForStatus(long patientId, long programId, long status, long communityId);

    //Spira 7555
    public List<Long> getDistinctStatusListExceptCurrent(long patientId, long programId, long currentStatus, long communityId);    
}
