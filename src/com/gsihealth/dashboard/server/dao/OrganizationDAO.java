package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.entity.CommunityOrganization;
import com.gsihealth.entity.OrganizationCwd;
import java.util.List;

/**
 *
 * @author Satyendra Singh
 */
public interface OrganizationDAO {

    public void create(OrganizationCwd org);

    public OrganizationCwd createAndGetOrganization(OrganizationCwd org);
    
    public void update(OrganizationCwd org,long communityId) throws NonexistentEntityException, Exception;

    public OrganizationCwd findOrganization(Long id,long communityId);
    
    public OrganizationCwd findOrganization(String oid) throws Exception;
    
    public OrganizationCwd findOrganization(String oid, long communityId) throws Exception;
    
    public List<CommunityOrganization> findOrganizationsWithAllRelationships(long communityId);
    
    public List<OrganizationCwd> findOrganizationEntities(long communityId);
    
    public List<OrganizationCwd> findOrganizationEntities2(long communityId);
     
    public List<OrganizationCwd> findOrganizationEntitiesForSearchForm(long communityId);

    public List<OrganizationCwd> findOrganizationEntities(long communityId, int pageNumber, int pageSize);

    public void permDelete(Long id) throws NonexistentEntityException;

    public boolean isUniqueOrganizationName(OrganizationCwd theOrganization);

    public boolean isUniqueOid(OrganizationCwd theOrganization);

    public String getOrganizationName(long orgId);

    public int getTotalCount(long communityId);

    public long getOrganizationId(String oid);    
}
