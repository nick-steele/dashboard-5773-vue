package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.common.AppKeyConstants;
import com.gsihealth.dashboard.common.util.AppConfigConstants;
import com.gsihealth.entity.*;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;

/**
 *
 * @author Chad Darby
 */
public class ApplicationDAOImpl implements ApplicationDAO {

    private EntityManagerFactory emf = null;
     private Logger logger = Logger.getLogger(getClass().getName());
    

    public ApplicationDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public ApplicationDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    /**
     * Find application by id
     *
     * @param id
     * @return
     */

    
     public Application findApplication(Long id,long communityId) {
       
       EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Application as o where o.applicationPK.applicationId=:theApplicationId and o.applicationPK.communityId=:theCommunityId");
            q.setParameter("theApplicationId", id);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            

            return (Application) q.getSingleResult();
        } finally {
            em.close();
        }
    }
     
     

    /**
     * Find application by name
     *
     * @param applicationName
     * @return
     */
    @Override
    public Application findApplication(String applicationName ,long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Application as o where o.applicationName=:theAppName and o.applicationPK.communityId=:theCommunityId");
            q.setParameter("theAppName", applicationName);
             q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return (Application) q.getSingleResult();
        } finally {
            em.close();
        }
    }

    /**
     * Find application by name
     *
     * @param applicationName
     * @return
     */
    @Override
    public List<Application> findApplications() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Application as o");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    /**
     * Add user community application
     *
     * @param userCommunityApplication
     */
    @Override
    public void addUserCommunityApplication(UserCommunityApplication userCommunityApplication) {
        EntityManager em = null;

        try {
            em = getEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();

            em.persist(userCommunityApplication);

            tx.commit();

        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Add user community application
     *
     * @param userCommunityApplication
     */
    public void deleteUserCommunityApplication(UserCommunityApplication userCommunityApplication) throws NonexistentEntityException {
        EntityManager em = null;

        try {
            em = getEntityManager();
            EntityTransaction tx = em.getTransaction();
            tx.begin();

            UserCommunityApplication theTarget = null;
            UserCommunityApplicationPK id = userCommunityApplication.getUserCommunityApplicationPK();

            try {
                theTarget = em.getReference(UserCommunityApplication.class, id);
                theTarget.getUserCommunityApplicationPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The UserCommunityApplication with id " + id + " no longer exists.", enfe);
            }

            em.remove(theTarget);

            tx.commit();

        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public UserCommunityApplication findUserCommunityApplication(UserCommunityApplicationPK id) {
        EntityManager em = null;
        UserCommunityApplication result;

        try {
            em = getEntityManager();

            result = em.find(UserCommunityApplication.class, id);

        } finally {
            if (em != null) {
                em.close();
            }
        }

        return result;
    }

    @Override
    public List<Application> getUserApplications(long userId, long communityId) {
        List<Application> results = null;
        
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select o.application from UserCommunityApplication as o "
                    + " where o.userCommunityApplicationPK.userId=:theUserId and o.userCommunityApplicationPK.communityId=:theCommunityId "
                    + " order by o.application.displayOrder");

            q.setParameter("theUserId", userId);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            results = (List<Application>) q.getResultList();
            
            
            
            
        } finally {
            em.close();
        }

        return results;
    }

    @Override
    public boolean hasMessagingApp(long userId, long communityId) {
        boolean result = false;

        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from UserCommunityApplication as o, Application as app"
                    + " where o.userCommunityApplicationPK.userId=:theUserId and"
                    + " o.userCommunityApplicationPK.communityId=:theCommunityId and"
                    + " app.applicationName=:theMessagingAppKey and"
                    + " app.applicationPK.applicationId=o.userCommunityApplicationPK.applicationId and"
                    + " app.applicationPK.communityId=:theCommunityId ");

            q.setParameter("theUserId", userId);
            q.setParameter("theCommunityId", communityId);
            q.setParameter("theMessagingAppKey", AppKeyConstants.MESSAGING_APP_KEY);

            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            long count = ((Long) q.getSingleResult()).intValue();

            result = count > 0;
        } finally {
            em.close();
        }

        return result;
    }

    @Override
    public List<String> getUserApplicationNames(long userId, long communityId) {
        EntityManager em = getEntityManager();

        List<String> appNames = new ArrayList<String>();

        try {
            Query q = em.createQuery("select app.applicationName from UserCommunityApplication as o, Application as app"
                    + " where o.userCommunityApplicationPK.userId=:theUserId and"
                    + " o.userCommunityApplicationPK.communityId=:theCommunityId and"
                    + " app.applicationPK.applicationId=o.userCommunityApplicationPK.applicationId and"
                    + " app.applicationPK.communityId=:theCommunityId");

            q.setParameter("theUserId", userId);
            q.setParameter("theCommunityId", communityId);

            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            appNames = ((List<String>) q.getResultList());

        } finally {
            em.close();
        }

        return appNames;
    }

    @Override
    public List<OrganizationCommunityApplication> getOrganizationApplications(long orgId, long communityId) {
        logger.info("** getOrganizationApplications** orgId:"+orgId+":communityId:"+communityId);
        List<OrganizationCommunityApplication> results = null;

        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from OrganizationCommunityApplication as o "
                    + " where o.organizationCommunityApplicationPK.organizationId=:theOrgId "
                    + " and o.organizationCommunityApplicationPK.communityId=:theCommunityId");

            q.setParameter("theOrgId", orgId);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            results = (List<OrganizationCommunityApplication>) q.getResultList();
        } finally {
            em.close();
        }

        return results;
    }

    @Override
    public void assignAppsToOrganization(long orgId, long communityId, List<TemplateOrganizationCommunityApplication> templateApps) throws Exception {

        EntityManager em = getEntityManager();
        EntityTransaction transaction = em.getTransaction();

        try {
            transaction.begin();

            // assign template apps to the organization            
            for (TemplateOrganizationCommunityApplication templateApp : templateApps) {
                OrganizationCommunityApplication orgApp = convert(orgId, communityId, templateApp);
                em.persist(orgApp);
            }

            transaction.commit();

        } catch (Exception ex) {
            transaction.rollback();
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void assignAppsToUser(User user, long communityId, List<OrganizationCommunityApplication> orgApps) throws Exception {
        EntityManager em = getEntityManager();
        EntityTransaction transaction = em.getTransaction();

        try {

            transaction.begin();

            // assign apps to the user            
            for (OrganizationCommunityApplication orgApp : orgApps) {
                long userid = user.getUserId();
                logger.info("check app access in org: " + orgApp.getOrganizationCommunityApplicationPK().getOrganizationId() + "for user " + userid);
                if (userHasAccessToApplication(user, orgApp)) {

                    long appId = orgApp.getOrganizationCommunityApplicationPK().getApplicationId();
                    logger.info("user " + userid + "should get app " + appId);
                    UserCommunityApplication userCommunityApplication = new UserCommunityApplication();

                    UserCommunityApplicationPK pk = new UserCommunityApplicationPK();
                    pk.setApplicationId(appId);
                    pk.setCommunityId(communityId);

                    pk.setUserId(userid);
                    userCommunityApplication.setUserCommunityApplicationPK(pk);
                    userCommunityApplication.setStartDate(new Date());
                    em.merge(userCommunityApplication);

                }
            }

            transaction.commit();

        } catch (Exception ex) {
            transaction.rollback();
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Returns true if accessLevel and role matches
     *
     * @param user
     * @param organizationCommunityApplication
     * @return
     */
    protected boolean userHasAccessToApplication(User user, OrganizationCommunityApplication organizationCommunityApplication) {

        OrganizationCommunityApplicationPK organizationCommunityApplicationPK = organizationCommunityApplication.getOrganizationCommunityApplicationPK();
        Long communityId = organizationCommunityApplicationPK.getCommunityId();
        Long userAccessLevelId = user.getUserCommunityOrganization(communityId).getAccessLevel().getAccessLevelPK().getAccessLevelId();
        long appAccessLevelId = organizationCommunityApplicationPK.getAccessLevelId();

        logger.info(" userHasAccessToApplication userAccessLevelId:"+userAccessLevelId+":appAccessLevelId:"+appAccessLevelId);
        Long userRoleId = user.getUserCommunityOrganization(communityId).getRole().getRoleId();
        long appRoleId = organizationCommunityApplicationPK.getRoleId();
        
        logger.info(" userHasAccessToApplication userRoleId:"+userRoleId+":appRoleId:"+appRoleId);
        
        
        return ((userAccessLevelId == appAccessLevelId)
                && (userRoleId == appRoleId));
    }

    public List<TemplateOrganizationCommunityApplication> getTemplateOrganizationApplications(long communityId) {

        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from TemplateOrganizationCommunityApplication as o where o.templateOrganizationCommunityApplicationPK.communityId=:communityId");
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    private OrganizationCommunityApplication convert(long orgId, long communityId, TemplateOrganizationCommunityApplication templateApp) {

        OrganizationCommunityApplication orgCommunityApplication = new OrganizationCommunityApplication();

        OrganizationCommunityApplicationPK orgPK = new OrganizationCommunityApplicationPK();
        TemplateOrganizationCommunityApplicationPK templateOrganizationCommunityApplicationPK = templateApp.getTemplateOrganizationCommunityApplicationPK();

        orgPK.setCommunityId(communityId);
        orgPK.setOrganizationId(orgId);

        long applicationId = templateOrganizationCommunityApplicationPK.getApplicationId();
        orgPK.setApplicationId(applicationId);

        long accessLevelId = templateOrganizationCommunityApplicationPK.getAccessLevelId();
        orgPK.setAccessLevelId(accessLevelId);

        long roleId = templateOrganizationCommunityApplicationPK.getRoleId();
        orgPK.setRoleId(roleId);

        orgCommunityApplication.setOrganizationCommunityApplicationPK(orgPK);

        return orgCommunityApplication;
    }

    @Override
    public void removeOldAppsExceptForMessaging(Long userId, Long communityId) {

        EntityManager em = getEntityManager();
        try {

            EntityTransaction transaction = em.getTransaction();
            transaction.begin();

            Query q = em.createQuery("delete from UserCommunityApplication o WHERE o.userCommunityApplicationPK.userId =:theUserId"
                    + " AND o.userCommunityApplicationPK.communityId =:communityId"
                    + " AND o.userCommunityApplicationPK.applicationId <> :messageAppId");
            q.setParameter("theUserId", userId);
            q.setParameter("messageAppId", AppConfigConstants.MESSAGES_APP_ID);
            q.setParameter("communityId", communityId);

            int deleted = q.executeUpdate();

            transaction.commit();
        } finally {
            em.close();
        }

    }
}
