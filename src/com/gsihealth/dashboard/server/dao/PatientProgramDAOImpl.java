
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.*;
import com.gsihealth.dashboard.server.dao.exceptions.PreexistingEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.RollbackFailureException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.*;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import java.util.ArrayList;
import org.apache.commons.lang.time.StopWatch;

import static com.mirth.kana.util.KanaApp.getEntityManager;

/**
 *
 * @author ssingh
 */
public class PatientProgramDAOImpl implements PatientProgramDAO{

    @Override
    public List<PatientProgramLink> findPatientProgramLinks(long patientId, long communityId) throws Exception {

        EntityManager em = getEntityManager();
        try {

            Query q = em.createNamedQuery("PatientProgramLink.findByCommunityIdAndPatientId", PatientProgramLink.class).setParameter(1, communityId).setParameter(2, patientId);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    private EntityManagerFactory emf = null;
    private Logger logger = Logger.getLogger(getClass().getName());

    public PatientProgramDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public PatientProgramDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Deprecated
   public int getPatientProgramsCountByCommunityId(long patientId,long communityId) {
        EntityManager em = getEntityManager();
        try {
             
            Query q = em.createQuery("select count(o) from PatientProgramName as o where o.patientId=:thePatientId and o.communityId=:theCommunityId");
            q.setParameter("thePatientId", patientId);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
   
    //JIRA 1024
    @Deprecated
    public int getActivePatientProgramsCountByCommunityId(long patientId, long communityId) {
        EntityManager em = getEntityManager();
        try {
             
            Query q = em.createQuery("select count(o) from PatientProgramName as o where o.patientId=:thePatientId and o.communityId=:theCommunityId "
                    + " and o.terminationReason IS NULL");
            q.setParameter("thePatientId", patientId);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @Deprecated
   public List<PatientProgramName> findPatientProgramNameEntitiesByCommunityId(long patientId,long communityId, int pageNumber, int pageSize) {
        EntityManager em = getEntityManager();
        try {
            
            Query q = em.createQuery("select object(o) from PatientProgramName as o where o.communityId=:theCommunityId and o.patientId=:thePatientId");
            q.setParameter("theCommunityId", communityId);
            q.setParameter("thePatientId", patientId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);

            q.setFirstResult(startRecordNumber);
            q.setMaxResults(pageSize);

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Deprecated
    public List<PatientProgramName> savePatientProgramList(long patientId, List<PatientProgramName> data) throws Exception{

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

//        // delete all old entries
//        deleteOldEntries(patientId);
//        logger.info("Time to deleteOldEntries=" + stopWatch.getTime() / 1000.0 + " secs");
//
//        stopWatch.stop();
//        stopWatch.reset();
//
//        stopWatch.start();
//        logger.info("Old entries Deleted" + stopWatch.getTime() / 1000.0 + " secs");
        // add new entries
        data = add(data);

        stopWatch.stop();
        logger.info("Time to add new patient program list=" + stopWatch.getTime() / 1000.0 + " secs");
        
        return data;
    }

    @Deprecated
    public void deleteEntities(long patProgId,long patientId,long progId,long communityId) throws Exception{

        EntityManager em = getEntityManager();
        PatientProgramName patientProgramName=new PatientProgramName();
        try {
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
 
            Query query = em.createQuery("select object(o) from PatientProgramName as o where o.patientId = :thePatientId and o.programId=:theProgId and o.communityId=:theCommunityId and o.id=:thePatProgId");
            query.setParameter("thePatientId", patientId);
            query.setParameter("theProgId", progId);
            query.setParameter("theCommunityId", communityId);
            query.setParameter("thePatProgId", patProgId);
            
            query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            PatientProgramName items =(PatientProgramName)query.getSingleResult();
               if(items!=null){
                    em.remove(items);
                     transaction.commit();
        }
        }
                catch (Exception exc) {
                    logger.log(Level.WARNING, "Could not delete PatientProgramName: " + patientProgramName, exc);
                }            
                       
         finally {
            em.close();
        }

    }

    @Deprecated
    private List<PatientProgramName> add(List<PatientProgramName> patientProgramList) throws PreexistingEntityException, RollbackFailureException, Exception {

        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();
       
        List<PatientProgramName> mergedList = new ArrayList<>();
        try {

            utx.begin();

            for (PatientProgramName patientProgramName : patientProgramList) {
                PatientProgramName mergedObj = em.merge(patientProgramName);
                //merged object will have the id upon commit which is needed for history
                mergedList.add(mergedObj);
            }

            utx.commit();
        } catch (Exception ex) {
            //first log the exception here
            //otherwise we lose it in case there's a rollback exception
            logger.log(Level.SEVERE, "Exception in add", ex);
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }

        return mergedList;
    }

    @Deprecated
    public void updatePatientProgramList(List<PatientProgramName> patientProgramList) throws PreexistingEntityException, RollbackFailureException, Exception {

        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();
        

       try {
            utx.begin();

            for (PatientProgramName patientProgramName : patientProgramList) {
                em.merge(patientProgramName);
            }

            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
     }
    }

    @Deprecated
    public List<PatientProgramName> findPatientProgramsList(long patientId, long communityId)throws Exception{
        
        EntityManager em = getEntityManager();
        try {
            
            Query q = em.createQuery("select object(o) from PatientProgramName as o where o.communityId=:theCommunityId and o.patientId=:thePatientId");
            q.setParameter("theCommunityId", communityId);
            q.setParameter("thePatientId", patientId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Deprecated
    public ProgramName getProgramName(long progId,long communityId) throws Exception{
         EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from ProgramName as o where o.programNamePK.id=:theProgId and o.programNamePK.communityId=:theCommunityId");
            q.setParameter("theProgId", progId);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            
            return  (ProgramName)q.getSingleResult();
        } finally {
            em.close();
        }
        
    }

    @Deprecated
     public PatientProgramName findEntity(long patientId,Long id,long communityId) throws Exception{
         EntityManager em = getEntityManager();
        try {
            
            Query q = em.createQuery("select object(o) from PatientProgramName as o where o.communityId=:theCommunityId and o.patientId=:thePatientId and o.id=:theId");
            q.setParameter("theCommunityId", communityId);
            q.setParameter("thePatientId", patientId);
            q.setParameter("theId", id);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            
            return  (PatientProgramName)q.getSingleResult();
        } finally {
            em.close();
        }
}

@Deprecated
      public List<PatientProgramName> findDuplicateProgram(long patientId,long progId,long communityId) throws Exception{
         EntityManager em = getEntityManager();
//         PatientProgramName programEnrolled=null;
        List<PatientProgramName> list = null;
        try {
            
            Query q = em.createQuery("select object(o) from PatientProgramName as o where o.communityId=:theCommunityId and o.programId=:theProgramId and o.patientId=:thePatientId");
            q.setParameter("theCommunityId", communityId);
            q.setParameter("theProgramId", progId);
             q.setParameter("thePatientId", patientId);
         q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
          
         list = (List<PatientProgramName>) q.getResultList();
            
//            if(!list.isEmpty()){
//                 programEnrolled=(PatientProgramName)list;
//            }

         
            return  list;
        } finally {
            em.close();
        }
}
      
      public PatientStatus getPatientStatus(long statusId,long communityId) throws Exception{
         EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from PatientStatus as o where o.patientStatusPK.id=:theStatusId and o.patientStatusPK.communityId=:theCommunityId");
            q.setParameter("theStatusId", statusId);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            
            return  (PatientStatus)q.getSingleResult();
        } finally {
            em.close();
        }
        
    }

    @Override
    public Program getProgram(long communityId, long programId) throws Exception {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<Program> query =
                    em.createQuery("SELECT p from Program p where p.communityId=:communityId and p.id=:programId", Program.class)
                            .setParameter("communityId", communityId)
                            .setParameter("programId", programId)
                            .setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return query.getSingleResult();
        }finally {
            em.close();
        }
    }

    @Override
    public List<Program> getPrograms(long communityId, boolean isParent) throws Exception {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<Program> query =
                    em.createQuery("SELECT p from Program p where p.communityId=:communityId and p.isAParent=:isParent", Program.class)
                            .setParameter("communityId", communityId)
                            .setParameter("isParent", isParent)
                            .setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return query.getResultList();
        }finally {
            em.close();
        }
    }

    @Override
    public List<Program> getPrograms(long communityId, long parentId) throws Exception {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<Program> query =
                    em.createQuery("SELECT p from Program p where p.communityId=:communityId and p.parent.id=:parentId", Program.class)
                            .setParameter("communityId", communityId)
                            .setParameter("parentId", parentId)
                            .setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return query.getResultList();
        }finally {
            em.close();
        }
    }

    @Override
    public List<ProgramStatus> getProgramStatuses(long communityId) throws Exception {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<ProgramStatus> query =
                    em.createQuery("SELECT p from ProgramStatus p where p.communityId=:communityId and p.definitionType='USER'", ProgramStatus.class)
                            .setParameter("communityId", communityId)
                            .setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return query.getResultList();
        }finally {
            em.close();
        }
    }

    @Override
    public List<ProgramStatus> getSystemProgramStatuses(long communityId) throws Exception {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<ProgramStatus> query =
                    em.createQuery("SELECT p from ProgramStatus p where p.communityId=:communityId and p.definitionType='SYSTEM'", ProgramStatus.class)
                            .setParameter("communityId", communityId)
                            .setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return query.getResultList();
        }finally {
            em.close();
        }
    }

    @Override
    public List<ProgramStatus> getProgramStatuses(long communityId, long programId) throws Exception {
        EntityManager em = getEntityManager();
        try {
            TypedQuery<ProgramStatus> query =
                    em.createQuery("SELECT p from ProgramStatus p join p.programs pp where p.communityId=:communityId and pp.id=:programId", ProgramStatus.class)
                            .setParameter("communityId", communityId)
                            .setParameter("programId", programId)
                            .setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return query.getResultList();
        }finally {
            em.close();
        }
    }


}


