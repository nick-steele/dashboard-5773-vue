package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.server.dao.util.PayerPlanDAOUtils;
import com.gsihealth.dashboard.common.PayerPlanSearchCriteria;
import com.gsihealth.entity.insurance.PayerClass;
import com.gsihealth.entity.insurance.PayerPlan;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.RollbackFailureException;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;

/**
 *
 * @author Chad Darby
 */
public class PayerPlanDAOImpl implements PayerPlanDAO {
    public static final int MAX_RESULTS = 200;

    private EntityManagerFactory emf = null;
    private Logger logger = Logger.getLogger(getClass().getName());

    public PayerPlanDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public PayerPlanDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public void add(PayerPlan payerPlan) throws RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();
            em.persist(payerPlan);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void update(PayerPlan payerPlan) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();
            payerPlan = em.merge(payerPlan);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = payerPlan.getPayerPlanPK().getId();
                if (findPayerPlan(id) == null) {
                    throw new NonexistentEntityException("The payerPlan with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();
            PayerPlan payerPlan;
            try {
                payerPlan = em.getReference(PayerPlan.class, id);
                payerPlan.getPayerPlanPK().getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The payerPlan with id " + id + " no longer exists.", enfe);
            }
            em.remove(payerPlan);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<PayerPlan> findPayerPlanEntities() {
        return findPayerPlanEntities(true, -1, -1);
    }

    @Override
    public List<PayerPlan> findPayerPlanEntities(int pageNumber, int pageSize) {
        return findPayerPlanEntities(false, pageNumber, pageSize);
    }

    @Override
    public List<PayerClass> findPayerClassEntities() {
        
        EntityManager em = getEntityManager();
        //long communityId=1;
        try {
            Query q = em.createQuery("select object(o) from PayerClass as o order by o.name");
           
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
        
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    @Override
    public List<PayerClass> getPayerClassEntities(Long communityId) {
        
        EntityManager em = getEntityManager();
        
        try {
            Query q = em.createQuery("select object(o) from PayerClass as o where o.payerClassPK.communityId=:communityId order by o.name");
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
        
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    
    @Override
    public List<PayerPlan> getPayerPlanEntities(Long communityId) {
        
        EntityManager em = getEntityManager();
        
        try {
            Query q = em.createQuery("select object(o) from PayerPlan as o where o.payerPlanPK.communityId=:communityId order by o.name");
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
        
            return q.getResultList();
        } finally {
            em.close();
        }
    }


    private List<PayerPlan> findPayerPlanEntities(boolean all, int pageNumber, int pageSize) {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery("select object(o) from PayerPlan as o order by o.name");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            if (!all) {
                int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);

                q.setFirstResult(startRecordNumber);
                q.setMaxResults(pageSize);
            }

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public List<PayerPlan> findPayerPlanEntities(PayerPlanSearchCriteria searchCriteria, int pageNumber, int pageSize) {
        EntityManager em = getEntityManager();

        try {
            StringBuilder jpql = new StringBuilder("select object(o) from PayerPlan as o");

            String whereClause = PayerPlanDAOUtils.buildWhereClause(searchCriteria);
            jpql.append(whereClause);

            jpql.append(" order by o.name");

            Query q = em.createQuery(jpql.toString());
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);

            q.setFirstResult(startRecordNumber);
            q.setMaxResults(pageSize);

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public PayerPlan findPayerPlan(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PayerPlan.class, id);
        } finally {
            em.close();
        }
    }

    @Override
    public int getPayerPlanTotalCount() {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery("select count(o) from PayerPlan as o");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int count = ((Long) q.getSingleResult()).intValue();

            return count;
        } finally {
            em.close();
        }
    }

    @Override
    public int getPayerPlanTotalCount(PayerPlanSearchCriteria searchCriteria) {
        EntityManager em = getEntityManager();

        try {
            StringBuilder jpql = new StringBuilder("select count(o) from PayerPlan as o");

            String whereClause = PayerPlanDAOUtils.buildWhereClause(searchCriteria);

            jpql.append(whereClause);

            // execute query
            Query q = em.createQuery(jpql.toString());
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int count = ((Long) q.getSingleResult()).intValue();

            return count;
        } finally {
            em.close();
        }
    }

    @Override
    public boolean isDuplicate(PayerPlan payerPlan) {
        EntityManager em = getEntityManager();

        try {
            StringBuilder jpql = new StringBuilder("select count(o) from PayerPlan as o");

            String whereClause = PayerPlanDAOUtils.buildWhereClauseCheckForDuplicates(payerPlan);

            jpql.append(whereClause);

            // execute query
            Query q = em.createQuery(jpql.toString());
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int count = ((Long) q.getSingleResult()).intValue();

            return count > 0;
        } finally {
            em.close();
        }

    }

    @Override
    public boolean hasPatientsAssignedToThisPayerPlan(PayerPlan payerPlan) {
        EntityManager em = getEntityManager();

        try {
            StringBuilder jpql = new StringBuilder("select count(o) from PatientCommunityEnrollment as o where o.primaryPayerPlan=:thePayerPlan");

            Query q = em.createQuery(jpql.toString());
            q.setParameter("thePayerPlan", payerPlan);            

            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            q.setMaxResults(MAX_RESULTS);

            int count = ((Long) q.getSingleResult()).intValue();

            return count > 0;
        } finally {
            em.close();
        }

    }

    @Override
    public void deletePayerPlan(PayerPlan payerPlan) throws Exception {
        destroy(payerPlan.getPayerPlanPK().getId());
    }
}
