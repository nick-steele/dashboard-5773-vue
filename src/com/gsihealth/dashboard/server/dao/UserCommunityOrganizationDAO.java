/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.CommunityOrganization;
import com.gsihealth.entity.UserCommunityOrganization;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
// import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import java.util.List;

/**
 *
 * @author User
 */
public interface UserCommunityOrganizationDAO {
       
    public void create(UserCommunityOrganization userCommunityOrganization);

    public void update(UserCommunityOrganization userCommunityOrganization) throws NonexistentEntityException, Exception;

    public UserCommunityOrganization findUserCommunityOrganization(Long userId, Long communityId);

    public List<UserCommunityOrganization> findUserCommunityOrganizationEntities();

    public List<UserCommunityOrganization> findUserCommunityOrganizationEntitiesByCommunityOrganization(CommunityOrganization communityOrganization);

    public int getUserCommunityOrganizationCount();
    
    public List<UserCommunityOrganization> findUserCommunityOrganizationEntities(Long userId);
    
}
