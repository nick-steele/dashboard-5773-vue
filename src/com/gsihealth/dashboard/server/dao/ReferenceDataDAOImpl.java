package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.BaseCode;
import com.gsihealth.entity.CareteamRoleDuplicate;
import com.gsihealth.entity.CareteamRoleRequired;
import com.gsihealth.entity.DisenrollmentReasons;
import com.gsihealth.entity.EnrollmentStatus;
import com.gsihealth.entity.EthnicCode;
import com.gsihealth.entity.GenderCode;
import com.gsihealth.entity.LanguageCode;
import com.gsihealth.entity.NonHealthHomeProvider;
import com.gsihealth.entity.PatientActivityNames;
import com.gsihealth.entity.PatientRelationshipCode;
import com.gsihealth.entity.PatientStatus;
import com.gsihealth.entity.ProgramDeleteReason;
import com.gsihealth.entity.ProgramHealthHome;
import com.gsihealth.entity.ProgramLevel;
import com.gsihealth.entity.ProgramName;
import com.gsihealth.entity.ProgramTerminationReason;
import com.gsihealth.entity.RaceCode;
import com.gsihealth.entity.Role;
import com.gsihealth.entity.User;
import com.gsihealth.entity.UserCredential;
import com.gsihealth.entity.UserPrefix;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;

/**
 *
 * @author Chad Darby
 */
public class ReferenceDataDAOImpl implements ReferenceDataDAO {

    private EntityManagerFactory emf = null;
    private LinkedHashMap<String, String> postalStateCodes;

    public ReferenceDataDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
        loadPostalStateCodes();
    }

    public ReferenceDataDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
        loadPostalStateCodes();
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public List<GenderCode> findGenderCodeEntities(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from GenderCode as o where o.genderCodePK.communityId=:theCommunityId");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public List<RaceCode> findRaceCodeEntities(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from RaceCode as o where o.raceCodePK.communityId=:theCommunityId");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public List<LanguageCode> findLanguageCodeEntities(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from LanguageCode as o where o.languageCodePK.communityId=:theCommunityId "
                    + "ORDER BY o.value");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public List<EthnicCode> findEthnicCodeEntities(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from EthnicCode as o where o.ethnicCodePK.communityId=:theCommunityId");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public List<EnrollmentStatus> findEnrollmentStatusEntities(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("SELECT e FROM EnrollmentStatus as e where e.enrollmentStatusPK.communityId = :theCommunityId");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public List<ProgramLevel> findProgramLevelEntities(Long communityId) {
        EntityManager em = getEntityManager();

        try {

            Query q = em.createQuery("select object(o) from ProgramLevel as o where o.programLevelPK.communityId= :communityId");
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return q.getResultList();
        } finally {
            em.close();
        }


    }

    @Override
    public List<ProgramName> findProgramNameEntities(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from ProgramName as o where o.programNamePK.communityId= :communityId order by o.value");
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public List<UserCredential> findUserCredentialEntities(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from UserCredential as o where o.userCredentialPK.communityId= :communityId");
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public List<UserPrefix> findUserPrefixEntities(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from UserPrefix as o where o.userPrefixPK.communityId= :communityId");
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

//    @Override
//    public List<String> getEnrollmentReasonsForInactivation() {
//        return getReasons("enrollment_reason_for_inactivation");
//    }

    /**
     * Helper method to convert vector list to normal list
     *
     * @param theList
     * @return e
     */
    private List<String> convert(List<Vector> theList) {

        List<String> reasons = new ArrayList<String>();

        for (Vector temp : theList) {
            String theReason = (String) temp.get(0);
            reasons.add(theReason);
        }

        return reasons;
    }

    /**
     * Helper method to convert vector list to normal list
     *
     * @param theList
     * @return e
     */
    private List<Long> convertCommunityIds(List<Vector> theList) {

        List<Long> communityIds = new ArrayList<Long>();

        for (Vector temp : theList) {
            Long theId = (Long) temp.get(0);
            communityIds.add(theId);
        }

        return communityIds;
    }

    /**
     * Helper method to get reasons based on table name
     *
     * @param tableName
     * @return
     */
    private List<String> getReasons(String tableName) {
        List<String> reasons = new ArrayList<String>();

        EntityManager em = getEntityManager();
        try {
            String sql = "select reason from " + tableName + " order by reason";
            Query q = em.createNativeQuery(sql);
            //q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<Vector> theList = (List<Vector>) q.getResultList();
            reasons = convert(theList);

            return reasons;

        } finally {
            em.close();
        }
    }

    @Override
    public List<PatientRelationshipCode> findPatientRelationshipEntities(Long communityId) {

        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from PatientRelationshipCode as o where o.active='Y' and o.patientRelationshipCodePK.communityId =:theCommunityId order by o.displayOrder");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public List<DisenrollmentReasons> getDisenrollmentReasons(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("SELECT d FROM DisenrollmentReasons d WHERE d.disenrollmentReasonsPK.communityId = :theCommunityId");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public List<CareteamRoleDuplicate> getCareteamRoleDuplicates(Long communityId) {

        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("SELECT c FROM CareteamRoleDuplicate c WHERE c.careteamRoleDuplicatePK.communityId = :theCommunityId");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public List<CareteamRoleRequired> getCareteamRoleRequired(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("SELECT c FROM CareteamRoleRequired c WHERE c.careteamRoleRequiredPK.communityId = :communityId");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public List<Long> findCommunityIds() {
        EntityManager em = getEntityManager();

        try {
            String sql = "select community_id from Community";
            Query q = em.createNativeQuery(sql);
            //q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<Long> communityIds = q.getResultList();

            return communityIds;

        } finally {
            em.close();
        }
    }

    public String[] getPostalStateCodes(Long communityId) {
        Set<String> keys = postalStateCodes.keySet();

        String[] data = keys.toArray(new String[keys.size()]);

        Arrays.sort(data);

        return data;
    }

    private void loadPostalStateCodes() {

        postalStateCodes = new LinkedHashMap<String, String>();

        postalStateCodes.put("AL", "ALABAMA");
        postalStateCodes.put("AK", "ALASKA");
        postalStateCodes.put("AZ", "ARIZONA");
        postalStateCodes.put("AR", "ARKANSAS");
        postalStateCodes.put("CA", "CALIFORNIA");
        postalStateCodes.put("CO", "COLORADO");
        postalStateCodes.put("CT", "CONNECTICUT");
        postalStateCodes.put("DE", "DELAWARE");
        postalStateCodes.put("DC", "DISTRICT OF COLUMBIA");
        postalStateCodes.put("FL", "FLORIDA");
        postalStateCodes.put("GA", "GEORGIA");
        postalStateCodes.put("HI", "HAWAII");
        postalStateCodes.put("ID", "IDAHO");
        postalStateCodes.put("IL", "ILLINOIS");
        postalStateCodes.put("IN", "INDIANA");
        postalStateCodes.put("IA", "IOWA");
        postalStateCodes.put("KS", "KANSAS");
        postalStateCodes.put("KY", "KENTUCKY");
        postalStateCodes.put("LA", "LOUISIANA");
        postalStateCodes.put("ME", "MAINE");
        postalStateCodes.put("MD", "MARYLAND");
        postalStateCodes.put("MA", "MASSACHUSETTS");
        postalStateCodes.put("MI", "MICHIGAN");
        postalStateCodes.put("MN", "MINNESOTA");
        postalStateCodes.put("MS", "MISSISSIPPI");
        postalStateCodes.put("MO", "MISSOURI");
        postalStateCodes.put("MT", "MONTANA");
        postalStateCodes.put("NE", "NEBRASKA");
        postalStateCodes.put("NV", "NEVADA");
        postalStateCodes.put("NH", "NEW HAMPSHIRE");
        postalStateCodes.put("NJ", "NEW JERSEY");
        postalStateCodes.put("NM", "NEW MEXICO");
        postalStateCodes.put("NY", "NEW YORK");
        postalStateCodes.put("NC", "NORTH CAROLINA");
        postalStateCodes.put("ND", "NORTH DAKOTA");
        postalStateCodes.put("OH", "OHIO");
        postalStateCodes.put("OK", "OKLAHOMA");
        postalStateCodes.put("OR", "OREGON");
        postalStateCodes.put("PA", "PENNSYLVANIA");
        postalStateCodes.put("RI", "RHODE ISLAND");
        postalStateCodes.put("SC", "SOUTH CAROLINA");
        postalStateCodes.put("SD", "SOUTH DAKOTA");
        postalStateCodes.put("TN", "TENNESSEE");
        postalStateCodes.put("TX", "TEXAS");
        postalStateCodes.put("UT", "UTAH");
        postalStateCodes.put("VT", "VERMONT");
        postalStateCodes.put("VA", "VIRGINIA");
        postalStateCodes.put("WA", "WASHINGTON");
        postalStateCodes.put("WV", "WEST VIRGINIA");
        postalStateCodes.put("WI", "WISCONSIN");
        postalStateCodes.put("WY", "WYOMING");
    }

    public String[] getProgramNames(Long communityId) {

        List<ProgramName> codes = findProgramNameEntities(communityId);

        String[] data = convertProgramName(codes);
        return data;
    }

    private String[] convertProgramName(List<ProgramName> list) {

        String[] data = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            data[i] = list.get(i).getValue();
        }

        return data;
    }

     @Override
    public List<ProgramHealthHome> findProgramHealthHomeEntities(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from ProgramHealthHome as o where o.programHealthHomePK.communityId= :communityId order by o.healthHomeValue" );
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }
     
     
     @Override
    public List<ProgramName> findProgramNameMapEntities(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from ProgramName as o where o.programNamePK.communityId= :communityId order by o.value" );
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }
      public List<Role>  getUserRolesEntities(long communityId) {

         EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Role as o where o.communityId = :communityId  order by o.roleName" );
             q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    @Override
    public HashMap<String, String> getPreferredLanguages(Long communityId) {
        List<LanguageCode> entities = this.findLanguageCodeEntities(communityId);
        
        HashMap<String, String> result = convertCodes(entities);
        
        return result;
    }

    @Override
    public HashMap<String, String> getEthnics(Long communityId) {
        List<EthnicCode> entities = this.findEthnicCodeEntities(communityId);

        HashMap<String, String> result = convertCodes(entities);
        
        return result;    
    }

    @Override
    public HashMap<String, String> getRaces(Long communityId) {
        List<RaceCode> entities = this.findRaceCodeEntities(communityId);
        
        HashMap<String, String> result = convertCodes(entities);
        
        return result;
    }

    @Override
    public HashMap<String, String> getGenders(Long communityId) {
        List<GenderCode> entities = this.findGenderCodeEntities(communityId);
        
        HashMap<String, String> result = convertCodes(entities);
        
        return result;
    }
    
    /**
     * Helper method to convert codes to strings
     * 
     * @param entities
     * @return 
     */
    private HashMap<String, String> convertCodes(List<? extends BaseCode> entities) {
        
        HashMap<String, String> result = new HashMap<String, String>();
        for (BaseCode temp : entities) {
            String key = temp.getValue();
            String val = temp.getCode();
            
            result.put(key, val);
        }
        
        return result;
    }
    
     public List<User> getUsers() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from User as o");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);         
            
            return q.getResultList();
        } finally {
            em.close();
        }
    }

      public List<NonHealthHomeProvider> getProvidersByCommunityId(long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from NonHealthHomeProvider as o where o.nonHealthHomeProviderPK.communityId = :theCommunityId");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);         
            
            return q.getResultList();
        } finally {
            em.close();
        }
    }

//      public List<PatientStatus> getPatientStatus(Long communityId) {
//
//        List<PatientStatus> codes = findPatientStatusEntities(communityId);
//
//        String[] data = convertPatientStatus(codes);
//        return data;
//    }

    public List<PatientStatus> findPatientStatusEntities(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from PatientStatus as o where o.patientStatusPK.communityId= :communityId order by o.value");
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }
 
	//JIRA 1023
    public LinkedHashMap<Long, String>  getHealthHomes(Long communityId) {

        List<ProgramHealthHome> codes = findHealthHomeEntities(communityId);

        LinkedHashMap<Long, String> data = convertHealthHome(codes);
        return data;
    }
     
    public List<ProgramHealthHome> findHealthHomeEntities(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from ProgramHealthHome as o where o.programHealthHomePK.communityId= :communityId order by o.healthHomeValue");
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

	//JIRA 1023    
    private LinkedHashMap<Long, String> convertHealthHome(List<ProgramHealthHome> list) {

        LinkedHashMap<Long, String> data = new LinkedHashMap<Long, String> (list.size());
        for (ProgramHealthHome phh : list) {
            data.put(phh.getProgramHealthHomePK().getId(), phh.getHealthHomeValue());
        }

        return data;
    }
    
    public String[] getTerminationReason(Long communityId) {

        List<ProgramTerminationReason> codes = findTerminationReasonEntities(communityId);

        String[] data = convertReasons(codes);
        return data;
    }
    
    public List<ProgramTerminationReason> findTerminationReasonEntities(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            System.out.println("CommunityID ="+communityId);
            Query q = em.createQuery("select object(o) from ProgramTerminationReason as o where o.programTerminationReasonPK.communityId =:theCommunityId order by o.value");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
     private String[] convertReasons(List<ProgramTerminationReason> list) {

        String[] data = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            data[i] = list.get(i).getValue();
        }

        return data;
    }
     
     public String[] getProgramDeleteReasons(Long communityId) {

        List<ProgramDeleteReason> codes = findProgramDeleteReasonsEntities(communityId);

        String[] data = convertProgramDeleteReasons(codes);
        return data;
    }

    private String[] convertProgramDeleteReasons(List<ProgramDeleteReason> list) {

        String[] data = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            data[i] = list.get(i).getValue();
        }

        return data;
    }
 
    public List<ProgramDeleteReason> findProgramDeleteReasonsEntities(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from ProgramDeleteReason as o where o.programDeleteReasonPK.communityId=:theCommunityId order by o.value");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
     @Override
    public List<PatientActivityNames> findActivityNameEntities(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from PatientActivityNames as o where o.visible='Y' and o.communityId= :communityId order by o.name" );
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }
}
