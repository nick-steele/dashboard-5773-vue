package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.entity.UserAlert;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.RollbackFailureException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public interface UserAlertDAO {


    /**
     * Update an alert
     *
     * @param userAlert
     * @throws NonexistentEntityException
     * @throws RollbackFailureException
     * @throws Exception
     */
    public void update(UserAlert userAlert) throws NonexistentEntityException, RollbackFailureException, Exception;
    

    /**
     * 
     * @param userId
     * @param searchValue
     * @return 
     */
    public List<UserAlert> searchUserAlertsByApplication(long userId, String searchValue, Long communityId, int pageNumber, int pageSize);
    
    

    /**
     * Get total count of user alerts for this user
     * 
     * @param userId
     * @return 
     */
    public int getTotalCount(long userId, Long communityId);

    /**
     * Get the user alerts for this user, based on page number and page size
     * 
     * @param userId
     * @param pageNumber
     * @param pageSize
     * @return 
     */
    public List<UserAlert> findUserAlertEntities(long userId, Long communityId, int pageNumber, int pageSize);

    /**
     * Get total count for search by patient last name
     * 
     * @param userId
     * @param searchValue
     * @return 
     */
    public int getTotalCountForSearchByPatientLastName(long userId, String searchValue, Long communityId);

    /**
     * Get user alerts by last name
     * 
     * @param userId
     * @param searchValue
     * @param pageNumber
     * @param pageSize
     * @return 
     */
    public List<UserAlert> searchUserAlertsByPatientLastName(long userId, String searchValue, Long communityId, int pageNumber, int pageSize);

    /**
     * Get search results for patient by first name
     * 
     * @param userId
     * @param searchValue
     * @return 
     */
    public int getTotalCountForSearchByPatientFirstName(long userId, String searchValue, Long communityId);
    
    /**
     * Get search results for patient by first name
     * 
     * @param userId
     * @param searchValue
     * @param pageNumber
     * @param pageSize
     * @return 
     */
    public List<UserAlert> searchUserAlertsByPatientFirstName(long userId, String searchValue, Long communityId, int pageNumber, int pageSize);

    /**
     * Get total count for search by date
     * 
     * @param userId
     * @param fromDate
     * @param toDate
     * @return 
     */
    public int getTotalCountForSearchByDate(long userId, Long communityId, Date fromDate, Date toDate);
    
    /**
     * Get alerts for date range
     * 
     * @param userId
     * @param fromDate
     * @param toDate
     * @param pageNumber
     * @param pageSize
     * @return 
     */
    public List<UserAlert> searchUserAlertsByDate(long userId, Long communityId, Date fromDate, Date toDate, int pageNumber, int pageSize);
    
    /**
     * Get total count for alerts by application
     * 
     * @param userId
     * @param searchValue
     * @return 
     */
    public int getTotalCountForSearchByApplication(long userId, String searchValue, Long communityId);

    /**
     * Get total count for alerts by severity
     * 
     * @param userId
     * @param searchValue
     * @return 
     */
    public int getTotalCountForSearchBySeverity(long userId, String searchValue, Long communityId);

    /**
     * Search for user alerts by severity
     * 
     * @param userId
     * @param searchValue
     * @param pageNumber
     * @param pageSize
     * @return 
     */
    public List<UserAlert> searchUserAlertsBySeverity(long userId, String searchValue, Long communityId, int pageNumber, int pageSize);

    /**
     * Get total count for search by patient id
     * 
     * @param userId
     * @param searchValue
     * @return 
     */
    public int getTotalCountForSearchByPatientId(long userId, String searchValue, Long communityId);

    /**
     * Get search results by patient id
     * 
     * @param userId
     * @param searchValue
     * @param pageNumber
     * @param pageSize
     * @return 
     */
    public List<UserAlert> searchUserAlertsByPatientId(long userId, String searchValue, Long communityId, int pageNumber, int pageSize);
    
}
