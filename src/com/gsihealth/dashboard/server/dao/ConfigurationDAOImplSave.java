package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.Configuration;
import com.gsihealth.entity.ConfigurationPK;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import org.apache.commons.lang.StringUtils;

/**
 * FIXME IS THIS USED ANYWHERE?
 * @author Chad.Darby
 */
public class ConfigurationDAOImplSave implements ConfigurationDAO {

    private Logger logger = Logger.getLogger(getClass().getName());
    
    private EntityManagerFactory emf = null;

    public ConfigurationDAOImplSave() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public ConfigurationDAOImplSave(EntityManagerFactory theEmf) {
        emf = theEmf;
    }
    @Override
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    /**
     * Returns the value for this property. If it doesn't exist, returns the default value
     * 
     * @param communityId
     * @param configurationId
     * @param defaultValue
     * @return 
     */
    @Override
    public String getProperty(long communityId, String name, String defaultValue) {
    
        String value = null;
        
        ConfigurationPK primaryKey = new ConfigurationPK(communityId, name);
        Configuration config = findConfiguration(primaryKey);
         
        if (config == null) {
            logger.warning(name + " not found in Configuration table, using default value of " + defaultValue);
            return defaultValue;
        }
        else {
            value = config.getValue();
            logger.fine(name + " retrieved from configuration table for community " + communityId);
        }
        return value;
    }
    
    /**
     * ugly?  maybe -if the droids you seek are not available, check the default community, maybe they're there
     * @param defaultPropertyName
     * @param communityId
     * @param name
     * @return value for this property for communityId or value for default communityId or null if you're totally 
     * out of luck
     */
    @Override
    public String getProperty(String defaultPropertyName, Long communityId, String name){
        String property = this.getProperty(communityId, name);
        if(StringUtils.isBlank(property)){
            Long defaultCommunityId = Long.parseLong(this.getProperty(communityId, defaultPropertyName));
            property = this.getProperty(defaultCommunityId, name);
            logger.warning(name + " not found in Configuration table for community " + communityId
                    + " getting them from community: " + defaultCommunityId);
        }
        return property;
    }
    
    /**
     * Returns the value for this property. If it doesn't exist, returns null
     * 
     * @param communityId
     * @param configurationId
     * @return 
     */
    @Override
    public String getProperty(long communityId, String name) {
    
        String value = null;
        
        ConfigurationPK primaryKey = new ConfigurationPK(communityId, name);
        
        Configuration config = findConfiguration(primaryKey);
        if (config != null) {
            value = config.getValue();
            logger.fine(name + " retrieved from configuration table for community " + communityId);
        } else {
            logger.warning(name + " not found in configuration table for community " + communityId);
        }
        
        return value;
    }
    
    private Configuration findConfiguration(ConfigurationPK id) {
        EntityManager em = getEntityManager();

        Configuration theConfig = null;
        
        try {
            
            Query query = em.createQuery("select object(o) from Configuration as o where o.configurationPK = :theId and o.active='Y'");

            query.setParameter("theId", id);

            query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            query.setMaxResults(1);

            theConfig = (Configuration) query.getSingleResult();
                
            return theConfig;
        }
            catch (Exception exc) {
                return null;
            } finally {
            em.close();
        }
    }

    /**
     * @param name
     * @return all configs by this name
     */
    @Override
    public List<Configuration> findConfigurationsByName(String name) {
        EntityManager em = getEntityManager();
        List<Configuration> configurations = null;
        try {
            
            configurations = em.createNamedQuery("Configuration.findByName")
                    .setParameter("name", name)
                    .setParameter("active", 'Y')
                    .setHint(JPAConstants.REFRESH, JPAConstants.TRUE)
                    .getResultList();
        }
            catch (Exception exc) {
                logger.warning("configuration qry failed cuz: " + exc);
            } finally {
            em.close();
        }
        return configurations;
    }

    @Override
    public String getDefaultCommunityId() {
        return null;
    }

}
