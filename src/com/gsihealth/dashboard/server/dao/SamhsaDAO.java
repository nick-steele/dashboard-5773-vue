/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.Samhsa;

/**
 *
 * @author rsharan
 */
public interface SamhsaDAO {
    
    public Samhsa findActiveSamhsa(long communityId);
    
}
