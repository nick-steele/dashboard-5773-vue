package com.gsihealth.dashboard.server.dao;

/**
 *
 * @author Chad Darby
 */
public interface Constants {

   // public static final long GSI_COMMUNITY_ID = 1;

    public static final String OTHER_PAYER_PLAN_NAME = "Other";
    public static final Long POWER_USER = (long)4;
    public static final Long SUPER_USER = (long)1;
    
    public static final String MEDICAID ="Medicaid";
    public static final String MEDICARE ="Medicare";

    int PROGRAM_CONSENT_YES = 1;
    int PROGRAM_CONSENT_NO = 0;
    int PROGRAM_CONSENT_NOT_REQUIRED = -1;
}
