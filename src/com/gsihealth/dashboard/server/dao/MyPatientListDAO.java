package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.common.PatientIdList;
import com.gsihealth.entity.MyPatientInfo;
import com.gsihealth.entity.MyPatientInfoPK;
import com.gsihealth.dashboard.entity.dto.MyPatientInfoDTO;
import com.gsihealth.entity.ListTags;
import java.util.List;

/**
 *
 * @author Chad.Darby
 */
public interface MyPatientListDAO {

    public void add(MyPatientInfo myPatientInfo) throws Exception;

    public void delete(MyPatientInfoPK id) throws Exception;

    public MyPatientInfo findMyPatientInfo(MyPatientInfoPK id);

    public List<MyPatientInfo> findMyPatientInfoEntities(long userId, long communityId);

    public List<MyPatientInfoDTO> findMyPatientInfoList(long userId, long communityId);

    public void updateTags(long userId, long communityId, String type, String id);

    public String findMyPatientInfoListCount(long userId, long communityId, String level, String tags, List patients);

    public List<MyPatientInfoDTO> findMyPatientInfoList(long userId, long communityId, long start, long stop, String level, String tags, List patients);

    public List<ListTags> getListTags(long communityId) throws Exception;

    public void update(MyPatientInfo myPatientInfo) throws Exception;

    public boolean isAlreadyOnList(long userid, long patientId, long communityId);

    public boolean isListFull(long userid, long communityId);

    public List<Long> getUserIdsForPatientList(long patientId, long communityId);

    public List<Long> getUserIdsWithConsent(long patientId, long communityId);

    public void delete(List<Long> userIds, long patientId, long communityId) throws Exception;

    public boolean hasConsent(long userId, long patientId, long communityId);

    public List<Long> getPatientIdsForPatientList(long userId, long communityId);

    public List<Long> getPatientIdsWithConsent(long userId, long communityId);

    public void deleteFromPatientList(List<Long> patientIds, long userId, long communityId);

    public ListTags getTagByType(long communityId, String type) throws Exception;
}
