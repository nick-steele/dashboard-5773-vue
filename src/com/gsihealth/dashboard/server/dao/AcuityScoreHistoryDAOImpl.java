
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.AcuityScoreHistory;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author ssingh
 */
public class AcuityScoreHistoryDAOImpl implements AcuityScoreHistoryDAO{
    
     private EntityManagerFactory emf = null;
    
    public AcuityScoreHistoryDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public AcuityScoreHistoryDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
      @Override
    public void addToAcuityScoreHistory(AcuityScoreHistory acuityScoreHistory) {
        acuityScoreHistory.setAcuityScoreChangeDate(new Date());
       
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(acuityScoreHistory);
            em.getTransaction().commit();
             
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
