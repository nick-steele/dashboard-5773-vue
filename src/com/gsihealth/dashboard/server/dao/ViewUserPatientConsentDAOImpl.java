package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.common.ConsentConstants;
import com.gsihealth.entity.OrganizationPatientConsent;
import com.gsihealth.entity.User;
import com.gsihealth.entity.ViewUserPatientConsent;
import com.gsihealth.entity.ViewUserPatientConsentPK;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.PreexistingEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.RollbackFailureException;
import com.gsihealth.dashboard.server.service.PatientConsentUtils;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import org.apache.commons.lang.time.StopWatch;

/**
 *
 * @author Chad Darby
 */
public class ViewUserPatientConsentDAOImpl implements ViewUserPatientConsentDAO {
    
    /* DASHBOARD-477
     * Modify this class to only save user consent exceptions (DENY) 
     * All users in an organization have consent, unless the user patient consent table contains
     * DENY for a particular user/patient combination
     */

    private EntityManagerFactory emf = null;
    private Logger logger = Logger.getLogger(getClass().getName());
    
    private ConfigurationDAO configurationDAO;

    public ViewUserPatientConsentDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
        this.configurationDAO = new ConfigurationDAOImpl(emf);
    }

    public ViewUserPatientConsentDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
        this.configurationDAO = new ConfigurationDAOImpl(emf);
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public void add(ViewUserPatientConsent viewUserPatientConsent, Date startDate) throws PreexistingEntityException, RollbackFailureException, Exception {

        // Only persist consentStatus = DENY      
        if (hasViewUserPatientConsent(viewUserPatientConsent)) return;
        
            EntityManager em = getEntityManager();
            EntityTransaction utx = em.getTransaction();

            if (viewUserPatientConsent.getViewUserPatientConsentPK() == null) {
                viewUserPatientConsent.setViewUserPatientConsentPK(new ViewUserPatientConsentPK());
            }

       

            try {
                utx.begin();
                em.merge(viewUserPatientConsent);
                utx.commit();
            } catch (Exception ex) {
                try {
                    utx.rollback();
                } catch (Exception re) {
                    throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                }
                if (findViewUserPatientConsent(viewUserPatientConsent.getViewUserPatientConsentPK()) != null) {
                    throw new PreexistingEntityException("ViewUserPatientConsent " + viewUserPatientConsent + " already exists.", ex);
                }
                throw ex;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
    }

    @Override
    public void add(List<ViewUserPatientConsent> viewUserPatientConsentList) throws PreexistingEntityException, RollbackFailureException, Exception {

        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();

            for (ViewUserPatientConsent viewUserPatientConsent : viewUserPatientConsentList) {              
                // Only persist consentStatus = DENY      
                if (!hasViewUserPatientConsent(viewUserPatientConsent)) { // should this be after the null check
                    if (viewUserPatientConsent.getViewUserPatientConsentPK() == null) {
                        viewUserPatientConsent.setViewUserPatientConsentPK(new ViewUserPatientConsentPK());
                    }

                    em.merge(viewUserPatientConsent);
                }
            }

            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                // em.clear();
                em.close();
            }
        }
    }

    @Override
    public void update(ViewUserPatientConsent viewUserPatientConsent) throws NonexistentEntityException, RollbackFailureException, Exception {
            
            // Only persist if consentStatus = DENY 
            if (hasViewUserPatientConsent(viewUserPatientConsent)) return;
            
            EntityManager em = getEntityManager();
            EntityTransaction utx = em.getTransaction();

            try {
                utx.begin();
                viewUserPatientConsent = em.merge(viewUserPatientConsent);
                utx.commit();
            } catch (Exception ex) {
                try {
                    utx.rollback();
                } catch (Exception re) {
                    throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                }
                String msg = ex.getLocalizedMessage();
                if (msg == null || msg.length() == 0) {
                    ViewUserPatientConsentPK id = viewUserPatientConsent.getViewUserPatientConsentPK();
                    if (findViewUserPatientConsent(id) == null) {
                        throw new NonexistentEntityException("The viewUserPatientConsent with id " + id + " no longer exists.");
                    }
                }
                throw ex;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        }
    @Override
    public void destroy(ViewUserPatientConsentPK id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();
            ViewUserPatientConsent viewUserPatientConsent;

            try {
                viewUserPatientConsent = em.getReference(ViewUserPatientConsent.class, id);
                viewUserPatientConsent.getViewUserPatientConsentPK();
                em.remove(viewUserPatientConsent);
                utx.commit();
            } catch (EntityNotFoundException enfe) {
                logger.warning("The viewUserPatientConsent with id " + id + " no longer exists.");
            }
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<ViewUserPatientConsent> findViewUserPatientConsentEntities(long patientId, int pageNumber, int pageSize) {
        EntityManager em = getEntityManager();

        try {
           // Query q = em.createQuery("select object(o) from ViewUserPatientConsent as o where o.viewUserPatientConsentPK.patientId = :thePatientId");
            Query q = em.createNativeQuery(PatientConsentUtils.buildNativeSQLForUsersWithConsent(patientId));
         //   q.setParameter("thePatientId", patientId);
            //q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);

            q.setFirstResult(startRecordNumber);
            q.setMaxResults(pageSize);

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public ViewUserPatientConsent findViewUserPatientConsent(ViewUserPatientConsentPK id) {
        EntityManager em = getEntityManager();

        try {
            return em.find(ViewUserPatientConsent.class, id);
        } finally {
            em.close();
        }
    }

    @Override
    public int getViewUserPatientConsentCount(long patientId) {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createNativeQuery(PatientConsentUtils.buildNativeSQLForUsersWithConsent(patientId));

            //q.setParameter("thePatientId", patientId);
            //q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @Override
    public void deleteOldConsentEntries(long patientId) {

        EntityManager em = getEntityManager();

        try {
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            Query query = em.createQuery("select object(o) from ViewUserPatientConsent as o where o.viewUserPatientConsentPK.patientId = :thePatientId");
            query.setParameter("thePatientId", patientId);
            //query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            List<ViewUserPatientConsent> items = query.getResultList();

            for (ViewUserPatientConsent tempViewUserPatientConsent : items) {
                try {
                    tempViewUserPatientConsent.getViewUserPatientConsentPK();
                    em.remove(tempViewUserPatientConsent);
                } catch (Exception exc) {
                    logger.log(Level.WARNING, "Could not delete ViewUserPatientConsent: " + tempViewUserPatientConsent.getViewUserPatientConsentPK(), exc);
                }
            }

            stopWatch.stop();
            logger.info("Time for deleteOldConsentEntries=" + stopWatch.getTime() / 1000.0 + " secs");

            transaction.commit();

        } finally {
            em.close();
        }
    }

    @Override
    public void deleteOldConsentEntries(long patientId, List<User> oldUsers, long communityId) throws Exception {
         System.out.println("update communityId:" + communityId);

        for (User tempUser : oldUsers) {
            Long userId = tempUser.getUserId();
            ViewUserPatientConsentPK viewUserPatientConsentPK = new ViewUserPatientConsentPK(patientId, userId, communityId);

            destroy(viewUserPatientConsentPK);
        }

    }

    @Override
    public List<User> getUsers(long organizationId, long communityId) {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createNativeQuery("SELECT * FROM connect.user u JOIN connect.user_community_organization uco ON "
                    + "uco.user_id = u.user_id "
                    + "WHERE (u.deleted = 'N' or u.deleted is null) "
                    + "AND uco.community_id = ?1 "
                    + "AND uco.org_id = ?2 ", User.class)
                    .setParameter(1, communityId)
                    .setParameter(2, organizationId);
                    //.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }

    }

    @Override
    public boolean hasConsent(long userId, long patientId) {
        
       
        EntityManager em = getEntityManager();
        try {
            //long communityId = 1L;
        	long communityId = Long.parseLong(configurationDAO.getDefaultCommunityId());
            String jpql = PatientConsentUtils.buildJpqlForHasConsent(userId, patientId, communityId);
            
            Query q = em.createQuery(jpql);

            //q.setParameter("theUserId", userId);
            //q.setParameter("thePatientId", patientId);

            int count = ((Long) q.getSingleResult()).intValue();
            return count >= 1;   
        } finally {
            em.close();
        }
    }

    public boolean exists(ViewUserPatientConsentPK id) {
        ViewUserPatientConsent obj = this.findViewUserPatientConsent(id);

        return obj != null;
    }

    public List<Long> getUserIds(long organizationId, long communityId) {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createNativeQuery("SELECT u.user_id FROM connect.user u JOIN connect.user_community_organization uco ON "
                    + "uco.user_id = u.user_id "
                    + "WHERE (u.deleted = 'N' or u.deleted is null) "
                    + "AND uco.community_id = ?1 "
                    + "AND uco.org_id = ?2 ", User.class)
                    .setParameter(1, communityId)
                    .setParameter(2, organizationId);
                    //.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Long> getUserIds(List<OrganizationPatientConsent> orgs, long communityId) {
        EntityManager em = getEntityManager();

        try {
            String inClause = buildInClause(orgs);
            Query q = em.createNativeQuery("SELECT u.user_id FROM connect.user u JOIN connect.user_community_organization uco ON "
                    + "uco.user_id = u.user_id "
                    + "WHERE (u.deleted = 'N' or u.deleted is null) "
                    + "AND uco.community_id = ?1 "
                    + "AND uco.org_id in (?2) ")
                    .setParameter(1, communityId)
                    .setParameter(2, inClause);
                    //.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    protected static String buildInClause(List<OrganizationPatientConsent> theList) {
        StringBuilder result = new StringBuilder();

        result.append("(");
        boolean first = true;

        for (OrganizationPatientConsent temp : theList) {

            long organizationId = temp.getOrganizationPatientConsentPK().getOrganizationId();

            StringBuilder data = new StringBuilder();
            if (!first) {
                data.append(", ");
            } else {
                first = false;
            }

            result.append(data);
            result.append(organizationId);
        }

        result.append(")");

        return result.toString();
    }
    
    public boolean hasViewUserPatientConsent(ViewUserPatientConsent viewUserPatientConsent) {

        //Only persist userPatient Consent if consent status = deny 
        return viewUserPatientConsent.getConsentStatus().equalsIgnoreCase(ConsentConstants.CONSENT_PERMIT);
     
    }
}
