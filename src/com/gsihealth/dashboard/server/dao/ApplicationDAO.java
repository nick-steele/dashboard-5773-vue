package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.Application;
import com.gsihealth.entity.OrganizationCommunityApplication;
import com.gsihealth.entity.TemplateOrganizationCommunityApplication;
import com.gsihealth.entity.User;
import com.gsihealth.entity.UserCommunityApplication;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public interface ApplicationDAO {

    
    /**
     * Add user community application
     * 
     * @param userCommunityApplication 
     */
    public void addUserCommunityApplication(UserCommunityApplication userCommunityApplication);

    /**
     * Find application by id
     * 
     * @param id
     * @return 
     */
    public Application findApplication(Long id,long communityId);

    /**
     * Find application by name
     * @param applicationName
     * @return 
     */
    public Application findApplication(String applicationName,long communityId);

    /**
     * Get user apps names for a given user id
     * 
     * @param userId
     * @param communityId
     * @return 
     */
    public List<String> getUserApplicationNames(long userId, long communityId);
    
    /**
     * Returns true if the user is assigned the messaging app
     */
    public boolean hasMessagingApp(long userId, long communityId);

    /**
     * Returns a collection of applications
     */
    public List<Application> findApplications();

    public List<Application> getUserApplications(long userId, long communityId);
    
    public List<OrganizationCommunityApplication> getOrganizationApplications(long orgId, long communityId);
    
    public List<TemplateOrganizationCommunityApplication> getTemplateOrganizationApplications(long communityId);

    public void assignAppsToOrganization(long orgId, long communityId, List<TemplateOrganizationCommunityApplication> orgApps) throws Exception;

    public void assignAppsToUser(User user, long communityId, List<OrganizationCommunityApplication> orgApps) throws Exception;

    public void removeOldAppsExceptForMessaging(Long userId, Long communityId);
    
}
