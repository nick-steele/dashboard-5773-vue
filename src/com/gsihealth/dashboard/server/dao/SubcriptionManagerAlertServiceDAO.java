/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.ApplicationSendsAlert;
import com.gsihealth.entity.UserAlertFilter;
import java.util.List;

/**
 *
 * @author Vinay
 */
public interface SubcriptionManagerAlertServiceDAO {
    
    // public List<ApplicationSendsAlert> getAppSendsAlertList(Long applicationID, Long communityId);
    public List<ApplicationSendsAlert> getEnabledAppSendsAlertList(Long applicationID, Long communityId);
    public void updateUserAlertFilter(List<UserAlertFilter> userAlertFilterList)throws Exception;
    public List<ApplicationSendsAlert> getAppSendsAlerts(Long communityId)throws Exception;
    public void saveUserAlertFilter(List<UserAlertFilter> userAlertFilterList)throws Exception;
}
