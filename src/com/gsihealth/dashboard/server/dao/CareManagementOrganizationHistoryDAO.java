
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.CareManagementOrganizationHistory;

/**
 *
 * @author ssingh
 */
public interface CareManagementOrganizationHistoryDAO {
    
  public void addToCareManagementOrganizationhistory(CareManagementOrganizationHistory careManagementOrganizationHistory);
}
