/**
 * 
 */
package com.gsihealth.dashboard.server.dao;

import java.util.List;

import com.gsihealth.entity.NotificationAudit;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import java.util.Date;

/**
 * @author Vishal (Off.)
 *
 */
public interface NotificationAuditDAO {

    public void create(NotificationAudit notificationAudit);

    public void update(NotificationAudit notificationAudit) throws NonexistentEntityException, Exception;

    public NotificationAudit findNotificationAudit(Long id);

    public List<NotificationAudit> findAll();
    
  
}
