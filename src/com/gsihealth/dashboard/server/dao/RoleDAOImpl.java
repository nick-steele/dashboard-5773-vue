package com.gsihealth.dashboard.server.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.entity.Role;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;

/**
 *
 * @author Chad Darby
 */
public class RoleDAOImpl implements RoleDAO {

    private EntityManagerFactory emf = null;

    public RoleDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public RoleDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public List<Role> findRoleEntities(long communityId) {
        return findRoleEntities(true, -1, -1,communityId);
    }

    public List<Role> findRoleEntities(int maxResults, int firstResult) {
       // return findRoleEntities(false, maxResults, firstResult);
        
         EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(r) from Role as r order by r.roleName");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
       
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    
    private List<Role> findRoleEntities(boolean all, int maxResults, int firstResult,long communityId) {
        EntityManager em = getEntityManager();
        
        try {
            Query q = em.createQuery("select object(r) from Role as r where r.communityId=:theCommunityID");
            q.setParameter("theCommunityID", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    
    public Role findRole(Long id,long communityId) {
        
        System.out.println("************findRole id ::"+id+"--communityId::"+communityId);  
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Role as o where o.primaryRoleId=:theRoleId and o.communityId=:theCommunityId");
            q.setParameter("theRoleId", id);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
          

            return (Role) q.getSingleResult();
        } finally {
            em.close();
        }
    }
    
     public Role findProviderRole(Long id,long communityId) {
        
        System.out.println("************findProviderRole id ::"+id+"--communityId::"+communityId);  
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Role as o where o.roleId=:theRoleId and o.communityId=:theCommunityId");
            q.setParameter("theRoleId", id);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
          

            return (Role) q.getSingleResult();
        } finally {
            em.close();
        }
    }
}
