/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.PatientActivityHistory;
import com.gsihealth.entity.PatientActivityNames;
import com.gsihealth.entity.PatientActivityTracker;
import java.util.List;

/**
 *
 * @author ssingh
 */
public interface PatientActivityTrackerDAO {
    
   public List<PatientActivityTracker> findPatientActivityEntitiesByCommunityId(long patientId, long communityId);
   
   public PatientActivityTracker findEntity(long patientId,Long id,long communityId) throws Exception;
   
   public void savePatientActivityList(long patientId, List<PatientActivityTracker> data) throws Exception;
   
   public void updatePatientActivityList(List<PatientActivityTracker> data) throws Exception;
   
   public void addToPatientActivityHistory(List<PatientActivityHistory> data)throws Exception;
   
   public PatientActivityNames getActivityName(long activityId,long communityId)throws Exception;
   
    public boolean hasConsent(long userId, long patientId, long communityId);
   
    public void delete(long id,long communityId)  throws Exception;
    
    public PatientActivityTracker findById(long id,long communityId);
    
    public void addToPatientActivityHistory(PatientActivityHistory data)throws Exception;
}
