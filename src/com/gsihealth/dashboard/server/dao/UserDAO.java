package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.entity.AccessLevel;
import com.gsihealth.entity.User;
import com.gsihealth.entity.UserCommunityOrganization;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public interface UserDAO {

    public void create(User user);

    public void update(User user) throws NonexistentEntityException, Exception;

    public User findUser(Long id);
    public List<User> findUsers(long communityId, AccessLevel accessLevel, int pageNum, int pageSize);
    
    public List<User> findUsers(UserCommunityOrganization uco, SearchCriteria searchCriteria, int pageNumber, int pageSize);

    public List<User> findUsers(int pageNumber, int pageSize);
    
    public List<User> findUserEntities();

    public int getTotalCount(long communityId, AccessLevel accessLevel);

    public int getTotalCountForUserEntities();
    
    public int getTotalUserCount(UserCommunityOrganization uco, SearchCriteria searchCriteria);
    
    /**
     * This function finds a User of a Particular EmailID.
     * 
     * @param portalUserId is the Id used to login into Portal(Generally ones email id).
     * @return User
     */
    public User findUserByEmail(String portalUserId, long communityId);
    public User findUserByEmail(String portalUserId);
    public User findUserExistInOtherCommunity(String portalUserId, long communityId);
    public void permDelete(Long id) throws NonexistentEntityException;

    public List<User> findUserByUserCommunityOrganizationAndAccessLevel(UserCommunityOrganization userCommunityOrganization, AccessLevel findAccessLevel);
    
     public List<User> findUsers(long communityId, AccessLevel accessLevel);

    public List<User> filterUsers(SearchCriteria searchCriteria, List<User> users);
    
    public List<User> getTotalUsers(long communityId);
    
    public int getAuthyId(String email,long communityId);
    
    public List<User> findUsers(long communityId);
    
    public User findUser(Long id, long communityId);

    int removeSupervisor(long supervisorId, long communityId);

    List<User> findUserBySupervisor(long supervisorId, long communityId);

    User findByEmail(String email, long communityId);
}

