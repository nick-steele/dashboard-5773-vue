package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.Configuration;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Chad.Darby
 */
public interface ConfigurationDAO {

    /**
     * ugly?  maybe -if the droids you seek are not available, check the default community, maybe they're there
     * @param defaultPropertyName
     * @param communityId
     * @param name
     * @return value for this property for communityId or value for default communityId
     */
    public String getProperty(String defaultPropertyName, Long communityId, String name);
            
    /**
     * Returns the value for this property. If it doesn't exist, returns the default value
     *
     * @param communityId
     * @param configurationId
     * @param defaultValue
     * @return
     */
    public String getProperty(long communityId, String name, String defaultValue);

    /**
     * Returns the value for this property. If it doesn't exist, returns null
     *
     * @param communityId
     * @param configurationId
     * @return
     */
    public String getProperty(long communityId, String name);
    
    /**
     * @param name
     * @return all properties by name
     */
    public List<Configuration> findConfigurationsByName(String name); 
    
    
    public EntityManager getEntityManager();

    public String getDefaultCommunityId();
    
}
