
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.*;

import java.util.List;

/**
 *
 * @author ssingh
 */
public interface PatientProgramDAO {
    
//       public int getPatientProgramCount(long patientId);
    public List<PatientProgramName> findPatientProgramNameEntitiesByCommunityId(long patientId, long communityId, int pageNumber, int pageSize);

    public int getPatientProgramsCountByCommunityId(long patientId, long communityId);
    
    //JIRA 1024
    public int getActivePatientProgramsCountByCommunityId(long patientId, long communityId);

    public List<PatientProgramName> savePatientProgramList(long patientId, List<PatientProgramName> data) throws Exception;
    
    public List<PatientProgramName> findPatientProgramsList(long patientId, long communityId)throws Exception;
    
    public ProgramName getProgramName(long progId,long communityId)throws Exception;
    
    public void updatePatientProgramList(List<PatientProgramName> data) throws Exception;
    
    public PatientProgramName findEntity(long patientId,Long id,long communityId) throws Exception;
   
    public void deleteEntities(long patProgId,long patientId,long progId,long communityId)  throws Exception;
    
    public List<PatientProgramName> findDuplicateProgram(long patientId,long progId,long communityId) throws Exception;
   
    public PatientStatus getPatientStatus(long statusId,long communityId)throws Exception;

    public Program getProgram(long communityId, long programId) throws Exception;

    public List<Program> getPrograms(long communityId, boolean isParent) throws Exception;

    public List<Program> getPrograms(long communityId, long parentId) throws Exception;

    public List<ProgramStatus> getProgramStatuses(long communityId) throws Exception;

    List<ProgramStatus> getSystemProgramStatuses(long communityId) throws Exception;

    public List<ProgramStatus> getProgramStatuses(long communityId, long programId) throws Exception;

    List<PatientProgramLink> findPatientProgramLinks(long patientId, long communityId)throws Exception;


}
