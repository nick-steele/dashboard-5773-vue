package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.Audit;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Chad Darby
 */
public class AuditDAOImpl implements AuditDAO {

    private EntityManagerFactory emf = null;

    public AuditDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public AuditDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }
    
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void addAudit(Audit audit) {

        audit.setActionDateTime(new Date());
        
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(audit);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
//    public void addAudit(Audit audit) {
//
//        audit.setActionDateTime(new Date());
//        
//        EntityManager em = null;
//        try {
//            em = getEntityManager();
//            em.getTransaction().begin();
//            em.merge(audit);
//            em.getTransaction().commit();
//           
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }
    
    
}
