package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.FacilityType;
import com.gsihealth.entity.FacilityTypePK;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.entity.OrganizationCwd;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import com.gsihealth.entity.CommunityOrganization;

/**
 *
 * @author Satyendra Singh
 */
public class OrganizationDAOImpl implements OrganizationDAO {

	private EntityManagerFactory emf = null;
	private Logger logger = Logger.getLogger(getClass().getName());

	public OrganizationDAOImpl() {
		emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
	}

	public OrganizationDAOImpl(EntityManagerFactory theEmf) {
		emf = theEmf;
	}

	public EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

	public void create(OrganizationCwd org) {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			// em.persist(org);
			em.merge(org);
			em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public OrganizationCwd createAndGetOrganization(OrganizationCwd org) {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			em.persist(org);
			em.flush();
			em.getTransaction().commit();
		} catch (Exception exc) {
			logger.log(Level.SEVERE, "Error: " + exc.getMessage(), exc);
		} finally {
			if (em != null) {
				em.close();
			}
		}
		return org;
	}

	public void update(OrganizationCwd org, long communityId) throws NonexistentEntityException, Exception {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			OrganizationCwd persistentOrg = em.find(OrganizationCwd.class, org.getOrgId());

			persistentOrg.setOrganizationCwdName(org.getOrganizationCwdName());
			persistentOrg = em.merge(org);

			em.getTransaction().commit();
		} catch (Exception ex) {
			String msg = ex.getLocalizedMessage();
			if (msg == null || msg.length() == 0) {
				Long id = new Long("456");
				if (findOrganization(id, communityId) == null) {
					throw new NonexistentEntityException("The Organization with id " + id + " no longer exists.");
				}
			}
			throw ex;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	/**
	 * Permanent permDelete from the database
	 *
	 * @param id
	 * @throws NonexistentEntityException
	 */
	@Override
	public void permDelete(Long id) throws NonexistentEntityException {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			OrganizationCwd org;
			try {
				org = em.getReference(OrganizationCwd.class, id);
				org.getOrgId();
			} catch (EntityNotFoundException enfe) {
				throw new NonexistentEntityException("The organization with id " + id + " no longer exists.", enfe);
			}

			em.remove(org);
			em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public List<OrganizationCwd> findOrganizationEntities(long communityId, int pageNumber, int pageSize) {
		EntityManager em = getEntityManager();
		try {

			// now query
			Query q = em.createNamedQuery("OrganizationCwd.findByCommunityId")
					.setParameter("theCommunityId", communityId).setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

			int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);

			q.setFirstResult(startRecordNumber);
			q.setMaxResults(pageSize);

			List<OrganizationCwd> orgs = q.getResultList();

			// load facility types
			loadFacilityTypes(em, orgs, communityId);
                        logger.log(Level.INFO, "loaded faility type");
			return orgs;
		} finally {
			em.close();
		}
	}
        
        @Override
        public List<CommunityOrganization> findOrganizationsWithAllRelationships(long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("CommunityOrganization.findAllCommunityOrgsByOrg")
                    .setParameter("communityId", communityId).setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<CommunityOrganization> orgs = q.getResultList();
            return orgs;
        } finally {
            em.close();
        }
    }

	public List<OrganizationCwd> findOrganizationEntities(long communityId) {
		EntityManager em = getEntityManager();
		try {
			Query q = em.createNamedQuery("OrganizationCwd.findByCommunityId")
					.setParameter("theCommunityId", communityId).setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

			List<OrganizationCwd> orgs = q.getResultList();

			// load facility types
			loadFacilityTypes(em, orgs, communityId);

			return orgs;
		} finally {
			em.close();
		}
	}

	public List<OrganizationCwd> findOrganizationEntities2(long communityId) {
		EntityManager em = getEntityManager();
		try {
			Query q = em.createNamedQuery("OrganizationCwd.findByCommunityId2")
					.setParameter("theCommunityId", communityId).setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

			List<OrganizationCwd> orgs = q.getResultList();

			// load facility types
			loadFacilityTypes(em, orgs, communityId);

			return orgs;
		} finally {
			em.close();
		}
	}
        public List<OrganizationCwd> findOrganizationEntitiesForSearchForm(long communityId) {
		EntityManager em = getEntityManager();
		try {
			Query q = em.createNamedQuery("OrganizationCwd.findByCommunityId2")
					.setParameter("theCommunityId", communityId).setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

			List<OrganizationCwd> orgs = q.getResultList();

			return orgs;
		} finally {
			em.close();
		}
	}
	@Override
	public int getTotalCount(long communityId) {
		EntityManager em = getEntityManager();
		try {

			// now query
			String countOrgString = "select count(o) " + "FROM OrganizationCwd o " + "JOIN o.communities c "
					+ "WHERE c.communityId=:theCommunityId";
			Query orgQuery = em.createQuery(countOrgString).setParameter("theCommunityId", communityId)
					.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

			return ((Long) orgQuery.getSingleResult()).intValue();
		} finally {
			em.close();
		}
	}

	public OrganizationCwd findOrganization(Long id, long communityId) {
		EntityManager em = getEntityManager();
		try {
			OrganizationCwd theOrg = em.find(OrganizationCwd.class, id);
			loadFacilityType(em, theOrg, communityId);

			return theOrg;
		} finally {
			em.close();
		}
	}

	public boolean isUniqueOrganizationName(OrganizationCwd theOrg) {

		EntityManager em = getEntityManager();
		try {
			Query q = null;

			String organizationName = theOrg.getOrganizationCwdName();

			if (theOrg.getOrgId() == null) {
				q = em.createQuery(
						"select count(o) from OrganizationCwd as o where lower(o.organizationCwdName) = :nameParam");
				q.setParameter("nameParam", organizationName.toLowerCase());
			} else {
				q = em.createQuery(
						"select count(o) from OrganizationCwd as o where lower(o.organizationCwdName) = :nameParam and o.orgId <> :orgIdParam");
				q.setParameter("nameParam", organizationName.toLowerCase());
				q.setParameter("orgIdParam", theOrg.getOrgId());
			}

			q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

			long count = (Long) q.getSingleResult();

			return count == 0;
		} finally {
			em.close();
		}

	}

	@Override
	public boolean isUniqueOid(OrganizationCwd theOrg) {
		EntityManager em = getEntityManager();
		try {
			Query q = null;
			String oid = theOrg.getOid();

			if (theOrg.getOrgId() == null) {
				q = em.createQuery("select count(o) from OrganizationCwd as o where o.oid = :oidParam");
				q.setParameter("oidParam", oid);
			} else {
				q = em.createQuery(
						"select count(o) from OrganizationCwd as o where o.oid = :oidParam and o.orgId <> :orgIdParam");
				q.setParameter("oidParam", oid);
				q.setParameter("orgIdParam", theOrg.getOrgId());
			}

			q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

			long count = (Long) q.getSingleResult();

			return count == 0;
		} finally {
			em.close();
		}
	}

	@Override
	public String getOrganizationName(long orgId) {
		EntityManager em = getEntityManager();
		try {
			Query q = null;

			q = em.createQuery("select o.organizationCwdName from OrganizationCwd as o where o.orgId= :orgIdParam");
			q.setParameter("orgIdParam", orgId);

			q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

			String organizationName = null;

			try {
				organizationName = (String) q.getSingleResult();
			} catch (Exception exc) {
				logger.log(Level.WARNING, "Could not find organization name for orgId=" + orgId, exc);
				organizationName = "";
			}

			return organizationName;
		} finally {
			em.close();
		}
	}

	@Override
	public OrganizationCwd findOrganization(String oid) throws Exception {
		EntityManager em = getEntityManager();
		Query q = em.createQuery("select o from OrganizationCwd as o where o.oid = :oidParam");
		q.setParameter("oidParam", oid);
                logger.log(Level.INFO, "OID ",oid);
		q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

		OrganizationCwd org = (OrganizationCwd) q.getSingleResult();
		return org;
	}

	public OrganizationCwd findOrganization(String oid, long communityId) throws Exception {
		EntityManager em = getEntityManager();
		Query q = em.createQuery("select o from OrganizationCwd as o where o.oid = :oidParam");
		q.setParameter("oidParam", oid);
                logger.log(Level.INFO, "OID "+oid +"communityID"+ communityId);
		q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

		OrganizationCwd org = (OrganizationCwd) q.getSingleResult();
		loadFacilityType(em, org, communityId);
		return org;
	}

	@Override
	public long getOrganizationId(String oid) {
		EntityManager em = getEntityManager();
		try {
			Query q = null;

			q = em.createQuery("select o.orgId from OrganizationCwd as o where o.oid= :oidParam");
			q.setParameter("oidParam", oid);

			q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

			long orgId;

			try {
				orgId = (Long) q.getSingleResult();
			} catch (Exception exc) {
				logger.log(Level.WARNING, "Could not find organization id for oid=" + oid, exc);
				orgId = 0;
			}

			return orgId;
		} finally {
			em.close();
		}
	}

	private void loadFacilityTypes(EntityManager em, List<OrganizationCwd> orgs, long communityId) {

		for (OrganizationCwd tempOrg : orgs) {
			loadFacilityType(em, tempOrg, communityId);
		}
	}

	private void loadFacilityType(EntityManager em, OrganizationCwd tempOrg, long communityId) {
                int facilityId = 0;
                if(tempOrg.getCommunityOrganization(communityId)!= null){
                    facilityId=tempOrg.getCommunityOrganization(communityId).getFacilityTypeId();
                }
                if(facilityId != 0){
                    FacilityTypePK pk = new FacilityTypePK();
                    pk.setCommunityId((int) communityId);                    
                    pk.setFacilityTypeId(facilityId);
		FacilityType facType = em.find(FacilityType.class, pk);
		tempOrg.setFacilityTypeDesc(facType.getFacilityTypeDesc());
                }
	}
}
