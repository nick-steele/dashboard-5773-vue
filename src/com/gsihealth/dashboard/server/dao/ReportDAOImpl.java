package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchRange;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import com.gsihealth.dashboard.server.service.PatientConsentUtils;
import com.gsihealth.entity.*;
import com.gsihealth.entity.insurance.PayerClass;
import com.gsihealth.entity.insurance.PayerClassPK;
import com.gsihealth.entity.insurance.PayerPlan;
import com.gsihealth.entity.insurance.PayerPlanPK;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.TransformerUtils;
import org.apache.commons.lang.StringUtils;

import javax.persistence.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Chad Darby
 */
public class ReportDAOImpl implements ReportDAO {

    private Logger logger = Logger.getLogger(getClass().getName());
    private EntityManagerFactory emf = null;
    final String[] ACTION_TYPES = {"CARETEAM_ASSIGNED", "CARETEAM_UNASSIGNED", "CARETEAM_CHANGED"};

    public ReportDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public ReportDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public int findPatientEnrollmentInActiveCount(long communityId) {
        return findPatientCount(communityId, WebConstants.INACTIVE);
    }

    @Override
    public int findPatientEnrollmentPendingCount(long communityId) {
        return findPatientCount(communityId, WebConstants.PENDING);
    }

    @Override
    public int findPatientEnrollmentCount(long communityId) {
        return findPatientCount(communityId, "Enrolled");
    }

    @Override
    public int findPatientAssignedCount(long communityId) {
        return findPatientCount(communityId, "Assigned");
    }

    private Map<String, PatientCommunityEnrollment> buildMap(List<PatientCommunityEnrollment> patientEnrollments) {
        Map<String, PatientCommunityEnrollment> map = new HashMap<String, PatientCommunityEnrollment>();

        for (PatientCommunityEnrollment tempPatientEnrollment : patientEnrollments) {
            String euid = tempPatientEnrollment.getPatientEuid();
            map.put(euid, tempPatientEnrollment);
        }

        return map;
    }

    private Map<String, PatientCommunityEnrollment> buildMapWithPatientIdAsKey(List<PatientCommunityEnrollment> patientEnrollments) {
        Map<String, PatientCommunityEnrollment> map = new HashMap<String, PatientCommunityEnrollment>();

        for (PatientCommunityEnrollment tempPatientEnrollment : patientEnrollments) {
            String patientId = tempPatientEnrollment.getPatientId().toString();
            map.put(patientId, tempPatientEnrollment);
        }

        return map;
    }

    private int findPatientCount(long communityId, String status) {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery("select count(o) from PatientCommunityEnrollment as o where o.status = :statusParam and o.communityId=:theCommunityId");
            q.setParameter("statusParam", status);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int count = ((Long) q.getSingleResult()).intValue();
            logger.info("ReportDAOImpl: count for " + status + " is:" + count);

            return count;
        } finally {
            em.close();
        }
    }

    /**
     * Find all: candidates + enrolled patients.
     * <p>
     * Returns a map: key=euid, value=patientEnrollment
     *
     * @param communityId
     * @return
     */
    @Override
    public Map<String, PatientCommunityEnrollment> findAll(long communityId) {
        String queryString = "select object(o) from PatientCommunityEnrollment as o";
        return findGenericCandidateEnrolledPatient(queryString, communityId);
    }

    /**
     * Find candidates.
     * <p>
     * Returns a map: key=euid, value=patientEnrollment
     *
     * @param communityId
     * @param organizationId
     * @return
     */
    @Override
    public Map<String, PatientCommunityEnrollment> findCandidates(long communityId, long organizationId) {
        String queryString = "select object(o) from PatientCommunityEnrollment as o where o.status in ('Pending', 'Inactive', '') and o.orgId=" + organizationId + "and o.communityId=" + communityId;
        return findGenericCandidateEnrolledPatient(queryString, communityId);
    }

    @Override
    public Map<String, PatientCommunityEnrollment> findCandidatePatientsNotEnrolledPatients(long communityId, long organizationId, int pageNum, int pageSize) {
        String queryString = "select object(o) from PatientCommunityEnrollment as o where o.status in ('Pending', 'Inactive', '') and o.orgId=" + organizationId + "and o.communityId=" + communityId;
        return findGenericCandidateEnrolledPatient(queryString, pageNum, pageSize, communityId);
    }

    /**
     * Find enrolled patients.
     * <p>
     * Returns a map: key=euid, value=patientEnrollment
     *
     * @param communityId
     * @param userId
     * @param accessLevelId
     * @return
     */
    @Override
    public List<PatientReportSheet> findEnrolledPatients(long communityId, long userId, long accessLevelId) {
        String queryString = null;
        List<PatientReportSheet> consentedPatientSheet = new ArrayList<>();
        EntityManager em = getEntityManager();
        logger.log(Level.FINE, "ReportDAOImpl findEnrolledPatients..");
        // if power user, no restrictions on patient consent. else apply patient consent
        try {
            if (accessLevelId == WebConstants.POWER_USER_ACCESS_LEVEL_ID) {
//            queryString = "select object(o) from PatientCommunityEnrollment as o where  "
//                    + "o.programLevelConsentStatus='Yes' and o.communityId=" + communityId
//                    + " and o.patientEuid IS NOT NULL";
                String sqlQuery = buildQueryForConsentedRecordsForPowerUser();
                Query q = em.createNativeQuery(sqlQuery, PatientReportSheet.class).setParameter(1, communityId);

                q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

                logger.info("find patient: running query: " + sqlQuery);
                consentedPatientSheet = q.getResultList();

            } else {
                //DASHBOARD-477
//            queryString = PatientConsentUtils.buildUserPatientConsentQuery(userId, communityId);
                String sqlQuery = buildQueryForConsentedRecordsForNonPowerUser();
                Query q = em.createNativeQuery(sqlQuery, PatientReportSheet.class).setParameter(1, communityId).setParameter(2, userId);

                q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

                logger.info("find patient: running query: " + sqlQuery);
                consentedPatientSheet = q.getResultList();

            }
        } catch (Exception ex) {
            logger.warning("===========>   Did not find patients data in database..." + ex.getMessage());
            ex.printStackTrace();
        } finally {
            em.close();
        }

        return consentedPatientSheet;
//        return findGenericCandidateEnrolledPatientWithPatientIdAsKey(queryString, communityId);
    }

    /**
     * Find enrolled patients.
     * <p>
     * Returns a map: key=euid, value=patientEnrollment
     *
     * @param communityId
     * @param userId
     * @param accessLevelId
     * @param pageNumber
     * @param pageSize
     * @return
     */
    @Override
    public Map<String, PatientCommunityEnrollment> findEnrolledPatients(long communityId, long userId, long accessLevelId, int pageNumber, int pageSize) {
        String queryString = null;
        logger.log(Level.FINE, "ReportDAOImpl findEnrolledPatients with page params..");
        // if power user, no restrictions on patient consent. else apply patient consent
        if (accessLevelId == WebConstants.POWER_USER_ACCESS_LEVEL_ID) {
            queryString = "select object(o) from PatientCommunityEnrollment as o where o.programLevelConsentStatus='Yes' and o.communityId=" + communityId;
        } else {
            //DASHBOARD-477
            queryString = PatientConsentUtils.buildUserPatientConsentQuery(userId, communityId);
        }

        return findGenericCandidateEnrolledPatientWithPatientIdAsKey(queryString, pageNumber, pageSize, communityId);
    }

    /**
     * Helper method for patients. Returns enrolled patients for the report csv
     * <p>
     * Returns a map: key=euid, value=patientEnrollment
     *
     * @param queryString
     * @return
     */
    protected Map<String, PatientCommunityEnrollment> findGenericCandidateEnrolledPatient(String queryString, long communityId) {
        EntityManager em = getEntityManager();

        try {
            Query q = PatientConsentUtils.createNativeOrJPQLQuery(em, queryString, PatientCommunityEnrollment.class);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            logger.info("find patient: running query: " + queryString);
            List<PatientCommunityEnrollment> patientEnrollments = q.getResultList();

            logger.info("loading organization names");
            loadOrganizationNamesFromMap(patientEnrollments);

            logger.info("loading payerPlan and classes stuff");
            Map<Long, PayerPlan> payerPlansMap = this.loadPayerPlans(em, communityId);
            Map<Long, PayerClass> payerClassesMap = this.loadPayerClasses(em, communityId);
            this.assignPayerPlanAndPayerClass(patientEnrollments, payerPlansMap, payerClassesMap);

            logger.info("building map");
            Map<String, PatientCommunityEnrollment> map = buildMap(patientEnrollments);
            logger.info("finished building map");

            return map;
        } finally {
            em.close();
        }
    }

    /**
     * Helper method for patients. Returns enrolled patients for the report csv
     * <p>
     * Returns a map: key=euid, value=patientEnrollment
     *
     * @param queryString
     * @return
     */
    protected Map<String, PatientCommunityEnrollment> findGenericCandidateEnrolledPatientWithPatientIdAsKey(String queryString, long communityId) {
        EntityManager em = getEntityManager();

        try {
            Query q = PatientConsentUtils.createNativeOrJPQLQuery(em, queryString, PatientCommunityEnrollment.class);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            logger.info("find patient: running query: " + queryString);
            List<PatientCommunityEnrollment> patientEnrollments = q.getResultList();

            logger.info("loading organization names");
            loadOrganizationNamesFromMap(patientEnrollments);

            logger.info("loading payerPlan and classes stuff");
            Map<Long, PayerPlan> payerPlansMap = this.loadPayerPlans(em, communityId);
            Map<Long, PayerClass> payerClassesMap = this.loadPayerClasses(em, communityId);
            this.assignPayerPlanAndPayerClass(patientEnrollments, payerPlansMap, payerClassesMap);

            logger.info("building map");
            Map<String, PatientCommunityEnrollment> map = buildMapWithPatientIdAsKey(patientEnrollments);
            logger.info("finished building map");

            return map;
        } finally {
            em.close();
        }
    }

    /**
     * retrieve plans and payer classes from db
     */
    private Map<Long, PayerPlan> loadPayerPlans(EntityManager em, long communityId) {
        // run the query
        Query payerPlansQuery = em.createQuery("select object(o) from PayerPlan as o where o.payerPlanPK.communityId=:theCommunityId");
        payerPlansQuery.setParameter("theCommunityId", communityId);
        payerPlansQuery.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
        List<PayerPlan> payerPlans = (List<PayerPlan>) payerPlansQuery.getResultList();

        Map<Long, PayerPlan> payerPlansMap = new HashMap<>();
        // add them to the correct maps TODO joins?
        // payer plans
        for (PayerPlan tempPayerPlan : payerPlans) {
            PayerPlanPK payerPlanPK = tempPayerPlan.getPayerPlanPK();
            Long planId = payerPlanPK.getId();

            payerPlansMap.put(planId, tempPayerPlan);
        }
        return payerPlansMap;
    }

    public Map<Long, PayerClass> loadPayerClasses(EntityManager em, long communityId) {
        Query payerClassesQuery = em.createQuery("select object(o) from PayerClass as o where o.payerClassPK.communityId=:theCommunityId");
        payerClassesQuery.setParameter("theCommunityId", communityId);
        payerClassesQuery.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
        List<PayerClass> payerClasses = (List<PayerClass>) payerClassesQuery.getResultList();

        Map<Long, PayerClass> payerClassesMap = new HashMap<>();
        // payer classes
        for (PayerClass tempPayerClass : payerClasses) {
            PayerClassPK payerClassPK = tempPayerClass.getPayerClassPK();
            Long classId = payerClassPK.getId();

            payerClassesMap.put(classId, tempPayerClass);
        }
        return payerClassesMap;
    }

    /**
     * map payerPlans and payerClasses to each patientEnrollment object
     *
     * @param patientEnrollments
     */
    private void assignPayerPlanAndPayerClass(List<PatientCommunityEnrollment> patientEnrollments,
                                              Map<Long, PayerPlan> payerPlansMap, Map<Long, PayerClass> payerClassesMap) {

        for (PatientCommunityEnrollment each : patientEnrollments) {
            // load payer plans
            long primaryPayerPlanId = each.getPrimaryPayerPlanId();
            PayerPlan tempPayerPlan = payerPlansMap.get(primaryPayerPlanId);

            if (tempPayerPlan != null) {
                each.setPrimaryPayerPlan(tempPayerPlan);
            }

            // load payer classes
            long primaryPayerClassId = each.getPrimaryPayerClassId();
            PayerClass tempPayerClass = payerClassesMap.get(primaryPayerClassId);

            if (tempPayerClass != null) {
                each.setPrimaryPayerClass(tempPayerClass);
            }
        }
    }

    /**
     * Helper method for patients.
     * <p>
     * Returns a map: key=euid, value=patientEnrollment
     *
     * @param queryString
     * @return
     */
    protected Map<String, PatientCommunityEnrollment> findGenericCandidateEnrolledPatient(String queryString, int pageNumber, int pageSize, long communityId) {
        EntityManager em = getEntityManager();

        try {
            Query q = PatientConsentUtils.createNativeOrJPQLQuery(em, queryString, PatientCommunityEnrollment.class);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);

            q.setFirstResult(startRecordNumber);
            q.setMaxResults(pageSize);

            List<PatientCommunityEnrollment> patientEnrollments = q.getResultList();

            loadOrganizationNamesFromMap(patientEnrollments);

            logger.info("loading payerPlan and classes stuff");
            Map<Long, PayerPlan> payerPlansMap = this.loadPayerPlans(em, communityId);
            Map<Long, PayerClass> payerClassesMap = this.loadPayerClasses(em, communityId);
            this.assignPayerPlanAndPayerClass(patientEnrollments, payerPlansMap, payerClassesMap);

            Map<String, PatientCommunityEnrollment> map = buildMap(patientEnrollments);

            return map;
        } finally {
            em.close();
        }
    }

    /**
     * Helper method for patients.
     * <p>
     * Returns a map: key=euid, value=patientEnrollment
     *
     * @param queryString
     * @return
     */
    protected Map<String, PatientCommunityEnrollment> findGenericCandidateEnrolledPatientWithPatientIdAsKey(String queryString, int pageNumber, int pageSize, long communityId) {
        EntityManager em = getEntityManager();

        try {
            Query q = PatientConsentUtils.createNativeOrJPQLQuery(em, queryString, PatientCommunityEnrollment.class);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);

            q.setFirstResult(startRecordNumber);
            q.setMaxResults(pageSize);

            List<PatientCommunityEnrollment> patientEnrollments = q.getResultList();

            loadOrganizationNamesFromMap(patientEnrollments);

            logger.info("loading payerPlan and classes stuff");
            Map<Long, PayerPlan> payerPlansMap = this.loadPayerPlans(em, communityId);
            Map<Long, PayerClass> payerClassesMap = this.loadPayerClasses(em, communityId);
            this.assignPayerPlanAndPayerClass(patientEnrollments, payerPlansMap, payerClassesMap);

            Map<String, PatientCommunityEnrollment> map = buildMapWithPatientIdAsKey(patientEnrollments);

            return map;
        } finally {
            em.close();
        }
    }

    private Map<Long, String> getOrganizationNames() {

        EntityManager em = getEntityManager();
        Map<Long, String> orgNamesMap = new HashMap<Long, String>();

        int ORG_ID_INDEX = 0;
        int ORG_NAME_INDEX = 1;

        try {
            Query q = em.createQuery("select o.orgId, o.organizationCwdName from OrganizationCwd as o");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            // run the query
            List<Object[]> resultList = q.getResultList();

            // build the map
            for (Object[] tempRow : resultList) {
                Object orgIdObj = tempRow[ORG_ID_INDEX];
                Object orgNameObj = tempRow[ORG_NAME_INDEX];

                if (orgIdObj != null && orgNameObj != null) {
                    orgNamesMap.put(Long.parseLong(orgIdObj.toString()), orgNameObj.toString());
                }
            }

            return orgNamesMap;
        } finally {
            em.close();
        }
    }

    private void loadOrganizationNamesFromMap(List<PatientCommunityEnrollment> patientEnrollments) {

        Map<Long, String> orgNamesMap = getOrganizationNames();

        for (PatientCommunityEnrollment tempPatientEnrollment : patientEnrollments) {
            long orgId = tempPatientEnrollment.getOrgId();

            String organizationName = orgNamesMap.get(orgId);

            tempPatientEnrollment.setOrganizationName(organizationName);
        }
    }

    @Override
    public int getTotalCountForEnrolledPatients(long communityId, long userId, long accessLevelId) {
        String queryString = null;
        logger.log(Level.FINE, "ReportDAOImpl test getTotalCountForEnrolledPatients..");
        // if super user, no restrictions on patient consent. else apply patient consent
        if (accessLevelId == WebConstants.POWER_USER_ACCESS_LEVEL_ID) {
            queryString = "select count(o) from PatientCommunityEnrollment as o where o.programLevelConsentStatus='Yes' and o.communityId=" + communityId;
        } else {
            //build query and convert to count
            logger.log(Level.FINE, "ReportDAOImpl before convert to count " + queryString);
            queryString = PatientConsentUtils.buildUserPatientConsentQuery(userId, communityId).replaceFirst("SELECT.*\\* ", "SELECT COUNT(*) ");
            logger.log(Level.FINE, "ReportDAOImpl convert to count " + queryString);

        }

        return getTotalCount(queryString);
    }

    @Override
    public int getTotalCountForCandidatePatientsNotEnrolledPatients(long communityId, long organizationId) {
        String queryString = "select count(o) from PatientCommunityEnrollment as o where o.status in ('Pending', 'Inactive', '') and o.orgId=" + organizationId + " and o.communityId=" + communityId;

        return getTotalCount(queryString);
    }

    protected int getTotalCount(String queryString) {
        EntityManager em = getEntityManager();

        try {
            //DASHBOARD-609 Consent Refactor for SQL View
            Query q = null;
            int count = 0;
            if (StringUtils.contains(queryString, WebConstants.USER_PATIENT_CONSENT_VIEW_KEY)) {
                logger.log(Level.FINE, "==createNativeQuery..");
                q = em.createNativeQuery(queryString);
                count = ((Long) q.getSingleResult()).intValue();
                logger.info("count =" + count);

            } else {
                q = em.createQuery(queryString);
                q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
                count = ((Long) q.getSingleResult()).intValue();
            }

            return count;
        } finally {
            em.close();
        }
    }

    @Override
    public Map<String, PatientCommunityEnrollment> findEnrolledPatients(long communityId, long userId) {

        logger.log(Level.FINE, "ReportDAOImpl findEnrolledPatients with userid..");
        String queryString = PatientConsentUtils.buildUserPatientConsentQuery(userId, communityId);
        return findGenericCandidateEnrolledPatient(queryString, communityId);
    }

    @Override
    public Map<String, PatientCommunityEnrollment> findAllCandidates(long communityId) {
        String queryString = "select object(o) from PatientCommunityEnrollment as o where o.status in ('Pending', 'Inactive', '') and o.communityId=" + communityId;
        return findGenericCandidateEnrolledPatient(queryString, communityId);
    }

    @Override
    public Map<String, PatientCommunityEnrollment> findAllEnrolledPatients(long communityId) {
        String queryString = "select object(o) from PatientCommunityEnrollment as o where o.status in ('Enrolled', 'Assigned') and o.communityId=" + communityId;
        return findGenericCandidateEnrolledPatient(queryString, communityId);
    }

    @Override
    public Map<String, PatientCommunityEnrollment> findAllEnrolledPatientsOfOrg(long communityId, long orgId) {
        String queryString = "select object(o) from PatientCommunityEnrollment as o where o.status in ('Enrolled', 'Assigned') and o.orgId=" + orgId + "and o.communityId=" + communityId;
        return findGenericCandidateEnrolledPatient(queryString, communityId);
    }

    // for power user
    @Override
    public Map<String, PatientCommunityEnrollment> findCandidatePatientsNotEnrolledPatients(long communityId, int pageNum, int pageSize) {
        String queryString = "select object(o) from PatientCommunityEnrollment as o where o.status in ('Pending', 'Inactive', '') and o.communityId=" + communityId;
        return findGenericCandidateEnrolledPatient(queryString, pageNum, pageSize, communityId);
    }

    // for power user
    @Override
    public int getTotalCountForCandidatePatientsNotEnrolledPatients(long communityId) {
        String queryString = "select count(o) from PatientCommunityEnrollment as o where o.status in ('Pending', 'Inactive', '') and o.communityId=" + communityId;

        return getTotalCount(queryString);
    }

    public List<ProgramHealthHome> findProgramHealthHomeEntities() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from ProgramHealthHome as o ");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public Map<String, PatientCommunityEnrollment> findTotalPatientsForPowerUser(long communityId, int pageNum, int pageSize) {
        String queryString = null;
        logger.log(Level.FINE, "ReportDAOImpl findToatalPatients..");
        // if power user, no restrictions on patient consent. else apply patient consent

        queryString = "select object(o) from PatientCommunityEnrollment as o "
                + "where o.status in ('Enrolled', 'Assigned','Pending', 'Inactive', '') "
                + "and o.communityId=" + communityId + " "
                + "and o.patientId IN (select p.patientId from PatientProgramName as p)";

        return findGenericPatient(queryString, pageNum, pageSize);
    }

    protected Map<String, PatientCommunityEnrollment> findGenericPatient(String queryString, int pageNum, int pageSize) {
        EntityManager em = getEntityManager();

        try {
//            Query q = em.createQuery(queryString);
//             
//            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            Query q = PatientConsentUtils.createNativeOrJPQLQuery(em, queryString, PatientCommunityEnrollment.class);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            logger.info("find patient: running query: " + queryString);
//            List<PatientCommunityEnrollment> patientEnrollments = q.getResultList();

            int startRecordNumber = Math.max(0, (pageNum - 1) * pageSize);

            q.setFirstResult(startRecordNumber);
            q.setMaxResults(pageSize);
            List<PatientCommunityEnrollment> patientEnrollments = q.getResultList();

//            logger.info("loading organization names");
//            loadOrganizationNamesFromMap(patientEnrollments);
//            
//            logger.info("loading payerPlan and classes stuff");
//            this.loadPayerPlansAndPayerClasses();
//            this.assignPayerPlanAndPayerClass(patientEnrollments);
            System.out.println("patientEnrollments>>>" + patientEnrollments.size());
            logger.info("building map");
            Map<String, PatientCommunityEnrollment> map = buildMap(patientEnrollments);
            logger.info("finished building map");

            return map;
        } finally {
            em.close();
        }
    }

    @Override
    public Map<String, PatientCommunityEnrollment> findPatientsForNotPowerUser(long userId, long communityId, int pageNum, int pageSize) {
        String queryString = null;
        logger.info("ReportDAOImpl findPatientsForNotPowerUser..");
        //DASHBOARD-477
        queryString = PatientConsentUtils.buildTotalUserPatientConsentQuery(userId, communityId);

        return findGenericPatient(queryString, pageNum, pageSize);
    }

    /**
     * FIXME sql injection
     *
     * @param communityId
     * @return
     */
    public int CountPatientsForPowerUser(long communityId) {
        String queryString = "select count(o) from PatientCommunityEnrollment as o where o.status in ('Enrolled', 'Assigned','Pending', 'Inactive', '') "
                + "and o.communityId=" + communityId + " "
                + "and o.patientId IN (select p.patientId from PatientProgramName as p)";

        return getCount(queryString);
    }

    protected int getCount(String queryString) {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery(queryString);

            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int count = ((Long) q.getSingleResult()).intValue();

            return count;
        } finally {
            em.close();
        }
    }

    public int CountPatientsForNotPowerUser(long userId, long communityId) {
//           String queryString =  "select count(patientEnrollment) from PatientCommunityEnrollment as patientEnrollment, OrganizationPatientConsent as o"
//                    + " where patientEnrollment.status in ('Enrolled', 'Assigned','Pending','Inacive',' ')"
//                    + " and patientEnrollment.patientEnrollmentPK.patientId=o.organizationPatientConsentPK.patientId"
//                    + " and o.consentStatus='PERMIT'"
//                    + " and o.organizationPatientConsentPK.organizationId in (select uco.organization.orgId"
//                    + " from UserCommunityOrganization as uco where"
//                    + " uco.user.userId="+ userId +")"                
//                    + " and patientEnrollment.patientEnrollmentPK.patientId NOT IN (select userPatientConsent.userPatientConsentPK.patientId"
//                    + " from UserPatientConsent as userPatientConsent where"
//                    + " userPatientConsent.userPatientConsentPK.userId=" + userId
//                    + " and userPatientConsent.consentStatus='DENY') and patientEnrollment.patientEnrollmentPK.communityId="+communityId
//                    + " and patientEnrollment.patientEnrollmentPK.patientId IN (select p.patientProgramNamePK.patientId from PatientProgramName as p)";   
//                
        String queryString = PatientConsentUtils.buildTotalUserPatientConsentQuery(userId, communityId).replaceFirst("SELECT.*\\* ", "SELECT COUNT(*) ");

        logger.info("===========buildUserPatientConsentQuery-" + queryString);

        return getTotalCount(queryString);
    }

    @Override
    public Map<String, PatientCommunityEnrollment> findTotalPatientsForPowerUser(long communityId) {
        String queryString = null;
        logger.log(Level.FINE, "ReportDAOImpl findToatalPatients..");
        // if power user, no restrictions on patient consent. else apply patient consent

        queryString = "select object(o) from PatientCommunityEnrollment as o where o.status in ('Enrolled', 'Assigned','Pending', 'Inactive', '') and o.communityId=" + communityId + " and o.patientId IN (select p.patientId from PatientProgramName as p)";

        return findGenericPatients(queryString);
    }

    protected Map<String, PatientCommunityEnrollment> findGenericPatients(String queryString) {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery(queryString);

            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            logger.info("find patient: running query: " + queryString);
            List<PatientCommunityEnrollment> patientEnrollments = q.getResultList();

//            logger.info("loading organization names");
//            loadOrganizationNamesFromMap(patientEnrollments);
//            
//            logger.info("loading payerPlan and classes stuff");
//            this.loadPayerPlansAndPayerClasses();
//            this.assignPayerPlanAndPayerClass(patientEnrollments);
            logger.info("building map");
            Map<String, PatientCommunityEnrollment> map = buildMap(patientEnrollments);
            logger.info("finished building map");

            return map;
        } finally {
            em.close();
        }
    }

    @Override
    public Map<String, PatientCommunityEnrollment> findPatientsForNotPowerUser(long userId, long communityId) {
        String queryString = null;
        logger.log(Level.FINE, "ReportDAOImpl findPatientsForNotPowerUser..");
        //DASHBOARD-477
        queryString = PatientConsentUtils.buildTotalUserPatientConsentQuery(userId, communityId);

        return findGenericPatient(queryString);
    }

    @Override
    public PatientCommunityEnrollment findPatientEnrollment(String euid, long communityId) throws NoResultException {

        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery("select object(o) from PatientCommunityEnrollment as o where o.patientEuid = :theEuid and o.communityId=:theCommunityId and o.patientId IN (select p.patientId from PatientProgramName as p)");
            q.setParameter("theEuid", euid);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<PatientCommunityEnrollment> list = q.getResultList();

            PatientCommunityEnrollment tempPatientEnrollment = null;

            if (!list.isEmpty()) {
                tempPatientEnrollment = list.get(0);
            }

//            if (tempPatientEnrollment != null) {
//                assignPayerPlanAndPayerClass(tempPatientEnrollment);
//            }
            return tempPatientEnrollment;
        } finally {
            em.close();
        }
    }

    protected Map<String, PatientCommunityEnrollment> findGenericPatient(String queryString) {
        EntityManager em = getEntityManager();

        try {
            Query q = PatientConsentUtils.createNativeOrJPQLQuery(em, queryString, PatientCommunityEnrollment.class);

            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            logger.info("find patient: running query: " + queryString);
            List<PatientCommunityEnrollment> patientEnrollments = q.getResultList();

//            logger.info("loading organization names");
//            loadOrganizationNamesFromMap(patientEnrollments);
//            
//            logger.info("loading payerPlan and classes stuff");
//            this.loadPayerPlansAndPayerClasses();
//            this.assignPayerPlanAndPayerClass(patientEnrollments);
            logger.info("building map");
            Map<String, PatientCommunityEnrollment> map = buildMap(patientEnrollments);
            logger.info("finished building map");

            return map;
        } finally {
            em.close();
        }
    }

    @Override
    public List<PatientTrackingSheet> findPTSRecordsForPowerUser(long communityId, Long programId, SearchRange<Date> search, String sqlQuery) {
        EntityManager em = getEntityManager();
        List<PatientTrackingSheet> patientEnrollments = new ArrayList<PatientTrackingSheet>();
        try {
            String configQuery = sqlQuery;

            String subQuery = "";

            subQuery = "\n AND ( pce.created_date BETWEEN ?3 AND ?4 \n"
                    + " OR (ppl.most_recent_status_date IS NOT NULL AND ppl.most_recent_status_date  BETWEEN ?3 AND ?4 ) \n"
                    + " OR (cpr.max_encounter_start_date_time IS NOT NULL AND cpr.max_encounter_start_date_time BETWEEN ?3 AND ?4 ) "
                    + " OR ((uph.uph_max_date BETWEEN ?3 AND ?4 ) AND uph.action_type IS NOT NULL )"
                    + " OR (pph.adt IS NOT NULL AND pph.adt BETWEEN ?3 AND ?4 ))";

            // work-around to fix PTS for power user. the query is not picking the parameter value for 2
            if (search != null && sqlQuery != null) {
                configQuery = sqlQuery.replace("$subQuery$", subQuery).replace("$careteamAction$", buildActionTypeForPTSQuery());
                configQuery = configQuery.replace("?2", Long.toString(communityId));
                configQuery = configQuery.replace("?100", "'" + programId + "'");

                Query q = em.createNativeQuery(configQuery, PatientTrackingSheet.class);
                q = q.setParameter(3, search.getFrom()).setParameter(4, search.getTo());
                q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

                logger.info("find patient: running query: " + configQuery);
                patientEnrollments = q.getResultList();

            } else {
                configQuery = sqlQuery.replace("$subQuery$", "").replace("$careteamAction$", buildActionTypeForPTSQuery());
                configQuery = configQuery.replace("?2", Long.toString(communityId));
                configQuery = configQuery.replace("?100", "'" + programId + "'");

                Query q = em.createNativeQuery(configQuery, PatientTrackingSheet.class);
                q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

                logger.info("find patient: running query: " + configQuery);
                patientEnrollments = q.getResultList();

            }

        } catch (Exception ex) {
            logger.warning("===========>   Did not find patients data in database..." + ex.getMessage());
            ex.printStackTrace();

        } finally {
            em.close();
        }
        return patientEnrollments;
    }

    @Override
    public List<PatientTrackingSheet> findPTSRecordsForSuperUser(long communityId, Long programId, long userId, SearchRange<Date> search, String sqlQuery) {
        EntityManager em = getEntityManager();
        List<PatientTrackingSheet> patientEnrollments = new ArrayList<PatientTrackingSheet>();
        try {
            String configQuery = sqlQuery;

            String subQuery = "";
            subQuery = "\n AND ( pce.created_date BETWEEN ?4 AND ?5 \n"
                    + " OR (ppl.most_recent_status_date IS NOT NULL AND ppl.most_recent_status_date  BETWEEN ?4 AND ?5 ) \n"
                    + " OR (cpr.max_encounter_start_date_time IS NOT NULL AND cpr.max_encounter_start_date_time BETWEEN ?4 AND ?5 ) \n"
                    + " OR ((uph.uph_max_date BETWEEN ?4 AND ?5) AND uph.action_type IS NOT NULL)"
                    + " OR ( pph.adt IS NOT NULL AND pph.adt BETWEEN ?4 AND ?5 ))";

            configQuery = configQuery.replace("$careteamAction$", buildActionTypeForPTSQuery());
            if (search != null) {
                configQuery = configQuery.replace("$subQuery$", subQuery);
            } else {

                configQuery = configQuery.replace("$subQuery$", "");

            }
            configQuery = configQuery.replace("?2", Long.toString(communityId));
            configQuery = configQuery.replace("?3", Long.toString(userId));
            configQuery = configQuery.replace("?100", "'" + programId + "'");
            Query q = em.createNativeQuery(configQuery, PatientTrackingSheet.class);
            if (search != null) {
                q = q.setParameter(4, search.getFrom()).setParameter(5, search.getTo());
            }
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            logger.info("find patient: running query: " + configQuery);
            patientEnrollments = q.getResultList();

        } catch (Exception ex) {
            logger.warning("===========>   Did not find patients data in database..." + ex.getMessage());
            ex.printStackTrace();
        } finally {
            em.close();
        }
        return patientEnrollments;
    }

    @Override
    public List<PatientTrackingSheet> findPTSRecordsForLocalAdminUser(long communityId, Long programId, long userId, SearchRange<Date> search, String sqlQuery) {
        EntityManager em = getEntityManager();
        List<PatientTrackingSheet> patientEnrollments = new ArrayList<PatientTrackingSheet>();
        try {
            String configQuery = sqlQuery;

            String subQuery = "";
            subQuery = "\n AND ( pce.created_date BETWEEN ?4 AND ?5 \n"
                    + " OR (ppl.most_recent_status_date IS NOT NULL AND ppl.most_recent_status_date  BETWEEN ?4 AND ?5 ) \n"
                    + " OR (cpr.max_encounter_start_date_time IS NOT NULL AND cpr.max_encounter_start_date_time BETWEEN ?4 AND ?5 ) "
                    + " OR ((uph.uph_max_date BETWEEN ?4 AND ?5) AND uph.action_type IS NOT NULL)"
                    + " OR (pph.adt IS NOT NULL AND pph.adt BETWEEN ?4 AND ?5 ))";

            configQuery = configQuery.replace("$careteamAction$", buildActionTypeForPTSQuery());
            if (search != null) {
                configQuery = configQuery.replace("$subQuery$", subQuery);
            } else {

                configQuery = configQuery.replace("$subQuery$", "");

            }
            configQuery = configQuery.replace("?2", Long.toString(communityId));
            configQuery = configQuery.replace("?3", Long.toString(userId));
            configQuery = configQuery.replace("?100", "'" + programId + "'");
            Query q = em.createNativeQuery(configQuery, PatientTrackingSheet.class);//.setParameter(2, communityId).setParameter(3, userId).setParameter(100, programId);
            if (search != null) {
                q = q.setParameter(4, search.getFrom()).setParameter(5, search.getTo());
            }
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            logger.info("find patient: running query: " + configQuery);
            patientEnrollments = q.getResultList();

        } catch (Exception ex) {
            logger.warning("===========>   Did not find patients data in database..." + ex.getMessage());
            ex.printStackTrace();
        } finally {
            em.close();
        }
        return patientEnrollments;
    }

    @Override
    public Long findPTSCountForPowerUser(long communityId) {
        EntityManager em = getEntityManager();

        try {
            String sqlQuery = "SELECT COUNT(*) FROM connect.`patient_community_enrollment` pce \n"
                    //                    INNER JOIN patientmidm.sbyn_patientmdmsbr sp \n"
                    //                    + "    ON pce.patient_euid = sp.euid \n"
                    + "    WHERE pce.community_id=?1 ";
            Query q = em.createNativeQuery(sqlQuery).setParameter(1, communityId);

            //q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            logger.info("find patient: running query: " + sqlQuery);

            return (Long) q.getSingleResult();
        } finally {
            em.close();
        }
    }

    @Override
    public Long findPTSCountForSuperUser(long communityId, long userId) {
        EntityManager em = getEntityManager();

        try {
            String sqlQuery = "	SELECT COUNT(*) FROM connect.`patient_community_enrollment` pce INNER JOIN connect.organization_patient_consent opc \n"
                    + "    ON pce.patient_id = opc.patient_id \n"
                    + "    AND pce.community_id = opc.community_id \n"
                    + "    AND pce.community_id=?1\n"
                    + "    AND consent_status = 'Permit' \n"
                    + "  INNER JOIN connect.user_community_organization uco \n"
                    + "    ON opc.organization_id = uco.org_id \n"
                    + "    AND opc.community_id = uco.community_id\n"
                    + "  \n"
                    + "  INNER JOIN connect.user u \n"
                    + "    ON uco.user_id = u.user_id \n"
                    + "    AND u.user_id=?2\n"
                    //                    + "  INNER JOIN patientmidm.sbyn_patientmdmsbr sp \n"
                    //                    + "    ON pce.patient_euid = sp.euid \n"
                    + "    WHERE NOT EXISTS \n"
                    + "  (SELECT \n"
                    + "    'x' \n"
                    + "  FROM\n"
                    + "    connect.user_patient_consent upc \n"
                    + "  WHERE pce.patient_id = upc.patient_id \n"
                    + "    AND pce.community_id = upc.community_id \n"
                    + "    AND uco.user_id = upc.user_id \n"
                    + "    AND uco.community_id = upc.community_id)  ";
            Query q = em.createNativeQuery(sqlQuery).setParameter(1, communityId).setParameter(2, userId);
            ;

            logger.info("find patient: running query: " + sqlQuery);

            return (Long) q.getSingleResult();
        } finally {
            em.close();
        }
    }

    @Override
    public Long findPTSCountForLocalAdminUser(long communityId, long userId) {
        EntityManager em = getEntityManager();

        try {
            String sqlQuery = "SELECT COUNT(*) FROM connect.`patient_community_enrollment` pce INNER JOIN connect.user_community_organization uco \n"
                    + "    ON pce.org_id = uco.org_id \n"
                    + "    AND pce.community_id = uco.community_id \n"
                    + "    AND pce.community_id=?1\n"
                    + "  INNER JOIN connect.user u \n"
                    + "    ON uco.user_id = u.user_id\n"
                    + "    AND u.user_id=?2 \n"
                    //                    + "  INNER JOIN patientmidm.sbyn_patientmdmsbr sp \n"
                    //                    + "    ON pce.patient_euid = sp.euid \n"
                    + "    WHERE NOT EXISTS \n"
                    + "  (SELECT \n"
                    + "    'x' \n"
                    + "  FROM\n"
                    + "    connect.user_patient_consent upc \n"
                    + "  WHERE pce.patient_id = upc.patient_id \n"
                    + "    AND pce.community_id = upc.community_id \n"
                    + "    AND uco.user_id = upc.user_id \n"
                    + "    AND uco.community_id = upc.community_id) ";
            Query q = em.createNativeQuery(sqlQuery).setParameter(1, communityId).setParameter(2, userId);

            //q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            logger.info("find patient: running query: " + sqlQuery);

            return (Long) q.getSingleResult();
        } finally {
            em.close();
        }
    }

    @Override
    public List<PatientReportSheet> findNonConsentedRecordsForPowerUser(long communityId, int pageNum, int pageSize) {
        EntityManager em = getEntityManager();
        List<PatientReportSheet> nonConsentedPatientSheet = new ArrayList<>();
        try {
            String sqlQuery = buildQueryForNonConsentedRecordsForPowerUser();
            Query q = em.createNativeQuery(sqlQuery, PatientReportSheet.class).setParameter(1, communityId);

            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            if (pageNum != -1 && pageSize != -1) {
                int startRecordNumber = Math.max(0, (pageNum - 1) * pageSize);

                q.setFirstResult(startRecordNumber);
                q.setMaxResults(pageSize);
            }
            logger.info("find patient: running query: " + sqlQuery);
            nonConsentedPatientSheet = q.getResultList();

        } catch (Exception ex) {
            logger.warning("===========>   Did not find patients data in database..." + ex.getMessage());
            ex.printStackTrace();
        } finally {
            em.close();
        }
        return nonConsentedPatientSheet;
    }

    @Override
    public List<PatientReportSheet> findNonConsentedRecordsForNonPowerUser(long communityId, long userId, int pageNum, int pageSize) {
        EntityManager em = getEntityManager();
        List<PatientReportSheet> nonConsentedPatientSheet = new ArrayList<>();

        try {
            String sqlQuery = buildQueryForNonConsentedRecordsForNonPowerUser();
            Query q = em.createNativeQuery(sqlQuery, PatientReportSheet.class).setParameter(1, communityId).setParameter(2, userId);

            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            if (pageNum != -1 && pageSize != -1) {
                int startRecordNumber = Math.max(0, (pageNum - 1) * pageSize);

                q.setFirstResult(startRecordNumber);
                q.setMaxResults(pageSize);
            }
            logger.info("find patient: running query: " + sqlQuery);
            nonConsentedPatientSheet = q.getResultList();

        } catch (Exception ex) {
            logger.warning("===========>   Did not find patients data in database..." + ex.getMessage());
            ex.printStackTrace();
        } finally {
            em.close();
        }

        return nonConsentedPatientSheet;
    }

    /**
     * @param communityId
     * @param userId
     * @throws Exception This method sets the demographic data to non-consented
     *                   sheet
     */
    @Override
    public int findTotalCountNonConsentedRecordsForNonPowerUser(long communityId, long userId) {
        EntityManager em = getEntityManager();

        try {
            String sqlQuery = "SELECT COUNT(*) FROM( $subQuery$ ) AS num";

            String subQuery = buildQueryForNonConsentedRecordsForNonPowerUser();
            sqlQuery = sqlQuery.replace("$subQuery$", subQuery);

            Query q = em.createNativeQuery(sqlQuery).setParameter(1, communityId).setParameter(2, userId);

            logger.info("find patient count: running query: " + sqlQuery);

            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @Override
    public int findTotalCountNonConsentedRecordsForPowerUser(long communityId) {
        EntityManager em = getEntityManager();

        try {
            String sqlQuery = "SELECT COUNT(*) FROM( $subQuery$ ) AS num";
            String subQuery = buildQueryForNonConsentedRecordsForPowerUser();
            sqlQuery = sqlQuery.replace("$subQuery$", subQuery);

            Query q = em.createNativeQuery(sqlQuery).setParameter(1, communityId);

            //q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            logger.info("find patient count: running query: " + sqlQuery);
//            Vector resultSet = (Vector) q.getSingleResult();
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @Override
    public int findTotalCountPatientProgramRecordsForPowerUser(long communityId, PatientManager patientManager, SearchCriteria searchCriteria) {
        EntityManager em = getEntityManager();
        String sqlQuery = null;
        int result = 0;
        try {
            if (searchCriteria != null) {
                logger.info("this search has specific criteria");

                String[] patientEuids = findAndParsePatientMDMDemographicData(searchCriteria, patientManager, communityId);
                List<String> euidList = new ArrayList<String>(Arrays.asList(patientEuids));
                if (euidList.isEmpty()) {
                    return 0;
                }

                //sqlQuery = "SELECT COUNT(*) FROM( $subQuery$ GROUP BY pce.patient_id) AS num"; test
                sqlQuery = "SELECT COUNT(*) FROM( $subQuery$ ) AS num";
                String subQuery2 = buildQueryForPatientProgramRecordsSearchForPowerUser(euidList);

                sqlQuery = sqlQuery.replace("$subQuery$", subQuery2);
                Query q = em.createNativeQuery(sqlQuery).setParameter(1, communityId);

                result = ((Long) q.getSingleResult()).intValue();
            } else {
                sqlQuery = "SELECT COUNT(*) FROM( $subQuery$ ) AS num";
                String subQuery2 = buildQueryForPatientProgramRecordsForPowerUser();

                sqlQuery = sqlQuery.replace("$subQuery$", subQuery2);
                Query q = em.createNativeQuery(sqlQuery).setParameter(1, communityId);
                //            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

                logger.info("find patient count: running query: " + sqlQuery);
//            Vector resultSet = (Vector) q.getSingleResult();
                result = ((Long) q.getSingleResult()).intValue();
            }
            return result;
        } finally {
            em.close();
        }
    }

    @Override
    public int findTotalCountPatientProgramRecordsForNonPowerUser(long communityId, long userId, PatientManager patientManager, SearchCriteria searchCriteria) {
        EntityManager em = getEntityManager();
        String sqlQuery = null;
        int result = 0;
        try {
            if (searchCriteria != null) {
                logger.info("this search has specific criteria");

                String[] patientEuids = findAndParsePatientMDMDemographicData(searchCriteria, patientManager, communityId);
                List<String> euidList = new ArrayList<String>(Arrays.asList(patientEuids));
                if (euidList.isEmpty()) {
                    return 0;
                }

                //sqlQuery = "SELECT COUNT(*) FROM( $subQuery$ GROUP BY pce.patient_id) AS num";
                sqlQuery = "SELECT COUNT(*) FROM( $subQuery$ ) AS num";
                String subQuery2 = buildQueryForPatientProgramRecordsSearch(euidList);

                sqlQuery = sqlQuery.replace("$subQuery$", subQuery2);
                Query q = em.createNativeQuery(sqlQuery).setParameter(1, communityId).setParameter(2, userId);

                result = ((Long) q.getSingleResult()).intValue();
            } else {
                sqlQuery = "SELECT COUNT(*) FROM( $subQuery$ ) AS num";
                String subQuery2 = buildQueryForPatientProgramRecordsForNonPowerUser();

                // Query q = em.createNativeQuery(sqlQuery).setParameter(1, communityId);;
//            String subQuery = "";
//
//            if (searchCriteria != null) {
//                if (searchCriteria.getLastName() != null && !searchCriteria.getLastName().isEmpty()) {
//                    subQuery = subQuery + " AND lastname =?3";
//                }
//                if (searchCriteria.getFirstName() != null && !searchCriteria.getFirstName().isEmpty()) {
//                    subQuery = subQuery + " AND firstname =?4";
//                }
//            }
                sqlQuery = sqlQuery.replace("$subQuery$", subQuery2);
                Query q = em.createNativeQuery(sqlQuery).setParameter(1, communityId).setParameter(2, userId);
                result = ((Long) q.getSingleResult()).intValue();
//            if (searchCriteria != null) {
//                if (searchCriteria.getLastName() != null && !searchCriteria.getLastName().isEmpty()) {
//                    q.setParameter(3, searchCriteria.getLastName());
//                }
//                if (searchCriteria.getFirstName() != null && !searchCriteria.getFirstName().isEmpty()) {
//                    q.setParameter(4, searchCriteria.getFirstName());
//                }

            }
            logger.info("find patient count: running query: " + sqlQuery);
            return result;

        } finally {
            em.close();
        }
    }

    @Override
    public List<PatientProgramSheet> findPatientProgramRecordsForPowerUser(long communityId, SearchCriteria searchCriteria, PatientManager patientManager, int pageNum, int pageSize) {
        EntityManager em = getEntityManager();
        List<PatientProgramSheet> patientProgramSheet = new LinkedList<PatientProgramSheet>();
        try {

            if (searchCriteria != null) {
                logger.info("this search has specific criteria");

                String[] patientEuids = findAndParsePatientMDMDemographicData(searchCriteria, patientManager, communityId);
                List<String> euidList = new ArrayList<String>(Arrays.asList(patientEuids));
                if (euidList.isEmpty()) {
                    return patientProgramSheet;
                }

                String sqlQuery = buildQueryForPatientProgramRecordsSearchForPowerUser(euidList);

                Query q = em.createNativeQuery(sqlQuery, PatientProgramSheet.class).setParameter(1, communityId);

                q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
                if (pageNum != -1 && pageSize != -1) {
                    int startRecordNumber = Math.max(0, (pageNum - 1) * pageSize);

                    q.setFirstResult(startRecordNumber);
                    q.setMaxResults(pageSize);
                }
                logger.info("find patient: running query: " + sqlQuery);
                patientProgramSheet = q.getResultList();

            } else {
                String sqlQuery = buildQueryForPatientProgramRecordsForPowerUser();
                sqlQuery = sqlQuery;
                Query q = em.createNativeQuery(sqlQuery, PatientProgramSheet.class).setParameter(1, communityId);

                q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
                if (pageNum != -1 && pageSize != -1) {
                    int startRecordNumber = Math.max(0, (pageNum - 1) * pageSize);

                    q.setFirstResult(startRecordNumber);
                    q.setMaxResults(pageSize);
                }
                logger.info("find patient: running query: " + sqlQuery);
                patientProgramSheet = q.getResultList();

            }
        } catch (Exception ex) {
            logger.warning("===========>   Did not find patients data in database..." + ex.getMessage());
            ex.printStackTrace();

        } finally {
            em.close();
        }
        refinePatientProgramSheetList(patientProgramSheet);

        return patientProgramSheet;
    }

    private List<PatientProgramSheet> refinePatientProgramSheetList(List<PatientProgramSheet> patientProgramSheetList) {
        //PatientProgramSheet parentPPS = null;
        for (PatientProgramSheet patientProgramSheetElement : patientProgramSheetList) {
            String healthHome = patientProgramSheetElement.getHealthHome();
            String program = patientProgramSheetElement.getProgramName();
            //parent simple
             if (!StringUtils.isBlank(healthHome) && !StringUtils.isBlank(program)) {
                String splitted[] = healthHome.split(";", 2);
                if ("1".equals(splitted[0])) {
                    //first way
                    //if (searchCriteria != null) {
                    patientProgramSheetElement.setHealthHome(null);
                    //patientProgramSheetTest.add(patientProgramSheetElement);
                    /*}
                    else {
                        //second way
                        if (parentPPS.getProgramName() == null) {
                            parentPPS.setProgramName(patientProgramSheetElement.getProgramName());
                        } else {
                            String progList = parentPPS.getProgramName();
                            parentPPS.setProgramName(progList + ";" + patientProgramSheetElement.getProgramName());
                        }
                    }*/

                } else {
                    patientProgramSheetElement.setHealthHome(splitted[1]);
                    //patientProgramSheetTest.add(patientProgramSheetElement);
                }

            } /*else if (!StringUtils.isBlank(healthHome) && StringUtils.isBlank(program)) {
                //patientProgramSheetTest.add(patientProgramSheetElement);
                //parentPPS = patientProgramSheetElement;
            }
            else {
                //patientProgramSheetTest.add(patientProgramSheetElement);
            }*/
        }
        return patientProgramSheetList;
    }

    @Override
    public List<PatientProgramSheet> findPatientProgramRecordsForNonPowerUser(long communityId, long userId, SearchCriteria searchCriteria, PatientManager patientManager, int pageNum, int pageSize) {
        EntityManager em = getEntityManager();
        List<PatientProgramSheet> patientProgramSheet = new LinkedList<PatientProgramSheet>();
        try {

//        if (searchCriteria != null) {
//                if (searchCriteria.getLastName() != null && !searchCriteria.getLastName().isEmpty()) {
//                    subQuery = subQuery + " AND lastname =?3";
//        }
//                if (searchCriteria.getFirstName() != null && !searchCriteria.getFirstName().isEmpty()) {
//                    subQuery = subQuery + " AND firstname =?4";
//                }
//            }
            if (searchCriteria != null) {
                logger.info("this search has specific criteria");

                String[] patientEuids = findAndParsePatientMDMDemographicData(searchCriteria, patientManager, communityId);
                List<String> euidList = new ArrayList<String>(Arrays.asList(patientEuids));
                if (euidList.isEmpty()) {
                    return patientProgramSheet;
                }

                String sqlQuery = buildQueryForPatientProgramRecordsSearch(euidList);

                Query q = em.createNativeQuery(sqlQuery, PatientProgramSheet.class).setParameter(1, communityId).setParameter(2, userId);

                q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
                if (pageNum != -1 && pageSize != -1) {
                    int startRecordNumber = Math.max(0, (pageNum - 1) * pageSize);

                    q.setFirstResult(startRecordNumber);
                    q.setMaxResults(pageSize);
                }
                logger.info("find patient: running query: " + sqlQuery);
                patientProgramSheet = q.getResultList();

            } else {
                String sqlQuery = buildQueryForPatientProgramRecordsForNonPowerUser();
                String subQuery = "";

                sqlQuery = sqlQuery + subQuery;
                Query q = em.createNativeQuery(sqlQuery, PatientProgramSheet.class).setParameter(1, communityId).setParameter(2, userId);
//            if (searchCriteria != null) {
//                if (searchCriteria.getLastName() != null && !searchCriteria.getLastName().isEmpty()) {
//                    q.setParameter(3, searchCriteria.getLastName());
//                }
//                if (searchCriteria.getFirstName() != null && !searchCriteria.getFirstName().isEmpty()) {
//                    q.setParameter(4, searchCriteria.getFirstName());
//                }
//            }

                q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
                if (pageNum != -1 && pageSize != -1) {
                    int startRecordNumber = Math.max(0, (pageNum - 1) * pageSize);

                    q.setFirstResult(startRecordNumber);
                    q.setMaxResults(pageSize);
                }
                logger.info("find patient: running query: " + sqlQuery);
                patientProgramSheet = q.getResultList();

            }
        } catch (Exception ex) {
            logger.warning("===========>   Did not find patients data in database..." + ex.getMessage());
            ex.printStackTrace();

        } finally {
            em.close();
        }
        refinePatientProgramSheetList(patientProgramSheet);
        return patientProgramSheet;
    }

    private String buildQueryForPatientProgramRecordsForNonPowerUser() {
        String query = "SELECT DISTINCT \n"
                + "(@n := @n + 1) AS ID,\n"
                + "  pce.patient_id,\n"
                //+ "  pn.value AS program_name,\n"
                //+ "  p.program_name AS program_name,\n"
                + "  CASE WHEN p.is_a_parent IS true THEN null ELSE p.program_name END AS program_name,\n"
                //+ "  ppl.program_effective_date,\n"
                //+ "  ppl.program_end_date,\n"
                //+ "  ps.value AS patient_status,\n"
                + "  ps.program_status_name AS patient_status,\n"
                + "  ppl.status_effective_date,\n"
                //+ "  ppn.termination_reason,\n"
                + "  pt.value AS termination_reason,\n"
                //+ "  ppn.health_home \n"
                //+ "  p2.program_name AS health_home \n"
                + "  CASE WHEN p.IS_A_PARENT IS true THEN p.program_name ELSE concat(p2.SIMPLE_VIEW, ';', p2.program_name) END AS health_home \n"
                + "  \n"
                + "FROM\n"
                + "  connect.patient_community_enrollment pce \n"
                + "  CROSS JOIN (SELECT @n := 0) AS dummy \n"
                + "  INNER JOIN connect.view_user_patient_consent vupc \n"
                + "    ON (pce.patient_id = vupc.patient_id AND pce.community_id=vupc.community_id) \n"
                //+ "  INNER JOIN connect.patient_program_name ppn \n"
                //+ "    ON pce.patient_id = ppn.patient_id \n"
                + "  INNER JOIN connect.patient_program_link ppl \n"
                + "    ON pce.patient_id = ppl.patient_id \n"
                + "    AND pce.community_id = ppl.community_id \n"
                //+ "  INNER JOIN connect.program_name pn \n"
                //+ "    ON ppn.program_id = pn.id \n"
                //+ "    AND ppn.community_id = pn.community_id \n"
                + "  INNER JOIN connect.program p \n"
                + "    ON ppl.program_id = p.program_id \n"
                + "  LEFT OUTER JOIN connect.program p2 \n"
                + "    ON p.parent_id = p2.program_id \n"
                + "  LEFT OUTER JOIN connect.organization_cwd oc \n"
                + "    ON oc.org_id = pce.org_id \n"
                + "  LEFT OUTER JOIN connect.organization_mmis_oid omo \n"
                + "    ON oc.oid = omo.oid \n"
                //+ "  LEFT OUTER JOIN connect.patient_status ps \n"
                //+ "    ON ppn.patient_status = ps.id \n"
                //+ "    AND ppn.community_id=ps.community_id \n"
                + "  LEFT OUTER JOIN connect.program_status ps \n"
                + "    ON ppl.program_status_id = ps.program_status_id \n"
                + "  LEFT OUTER JOIN connect.program_termination_link ptl \n"
                + "    ON ppl.program_termination_link_id = ptl.program_termination_link_id \n"
                + "  LEFT OUTER JOIN connect.program_termination pt \n"
                + "    ON ptl.program_termination_id = pt.id \n"
                + "  WHERE "
                + "  pce.community_id=?1"
                + " AND  vupc.USER_ID=?2"
                //+ " ORDER BY pce.patient_id";
                + " ORDER BY pce.patient_id, \n"
                + " CASE WHEN ( p2.program_name is NOT NULL and p2.SIMPLE_VIEW is true ) THEN CONCAT(pce.patient_id,p2.program_name, '_F2') ELSE concat(pce.patient_id, p.program_name, '_F1') END \n";
        return query;
    }

    private String buildQueryForPatientProgramRecordsForPowerUser() {
        String query = "SELECT DISTINCT \n"
                + "  (@n := @n + 1) AS ID,\n"
                + "  pce.patient_id,\n"
                //+ "  p.program_name AS program_name,\n"
                + "  CASE WHEN p.is_a_parent IS true THEN null ELSE p.program_name END AS program_name,\n"
                //+ "  ppl.program_effective_date,\n"
                //+ "  ppl.program_end_date,\n"
                //+ "  ps.value AS patient_status,\n"
                + "  ps.program_status_name AS patient_status,\n"
                + "  ppl.status_effective_date,\n"
                //+ "  ppn.termination_reason,\n"
                + "  pt.value AS termination_reason,\n"
                //+ "  ppn.health_home \n"
                //+ "  p2.program_name AS health_home \n"
                + "  CASE WHEN p.IS_A_PARENT IS true THEN p.program_name ELSE concat(p2.SIMPLE_VIEW, ';', p2.program_name) END AS health_home \n"
                + "  FROM\n"
                + "  connect.patient_community_enrollment pce \n"
                + "    CROSS JOIN (SELECT @n := 0) AS dummy \n"
                //+ "  INNER JOIN connect.patient_program_name ppn \n"
                //+ "    ON pce.patient_id = ppn.patient_id \n"
                + "  INNER JOIN connect.patient_program_link ppl \n"
                + "    ON pce.patient_id = ppl.patient_id \n"
                + "    AND pce.community_id = ppl.community_id \n"
                //+ "  INNER JOIN connect.program_name pn \n"
                //+ "    ON ppn.program_id = pn.id \n"
                //+ "    AND ppn.community_id = pn.community_id \n"
                + "  INNER JOIN connect.program p \n"
                + "    ON ppl.program_id = p.program_id \n"
                + "  LEFT OUTER JOIN connect.program p2 \n"
                + "    ON p.parent_id = p2.program_id \n"
                + "  LEFT OUTER JOIN connect.organization_cwd oc \n"
                + "    ON oc.org_id = pce.org_id \n"
                + "  LEFT OUTER JOIN connect.organization_mmis_oid omo \n"
                + "    ON oc.oid = omo.oid \n"
                //+ "  LEFT OUTER JOIN connect.patient_status ps \n"
                //+ "    ON ppn.patient_status = ps.id \n"
                //+ " AND ppn.community_id=ps.community_id \n"
                + "  LEFT OUTER JOIN connect.program_status ps \n"
                + "    ON ppl.program_status_id = ps.program_status_id \n"
                + "  LEFT OUTER JOIN connect.program_termination_link ptl \n"
                + "    ON ppl.program_termination_link_id = ptl.program_termination_link_id \n"
                + "  LEFT OUTER JOIN connect.program_termination pt \n"
                + "    ON ptl.program_termination_id = pt.id \n"
                + "  \n"
                + "  WHERE "
                + " pce.community_id=?1"
                //+ " ORDER BY pce.patient_id";
                + " ORDER BY pce.patient_id, \n"
                + " CASE WHEN ( p2.program_name is NOT NULL and p2.SIMPLE_VIEW is true ) THEN CONCAT(pce.patient_id,p2.program_name, '_F2') ELSE concat(pce.patient_id, p.program_name, '_F1') END \n";
        return query;
    }

    private String buildQueryForNonConsentedRecordsForNonPowerUser() {
        String query = " SELECT  \n"
                + "  pce.patient_id,\n"
                //                + "  firstname first_name,\n"
                //                + "  \n"
                + "  pce.patient_euid,\n"
                + "   ps.organization source,\n"
                //                + "  CASE\n"
                //                + "    WHEN middlename = 'NULL_VALUE' \n"
                //                + "    THEN NULL \n"
                //                + "    ELSE middlename \n"
                //                + "  END middle_name,\n"
                //                + "  lastname last_name,\n"
                //                + "  CASE\n"
                //                + "    WHEN ssn IN (\n"
                //                + "      '000000000',\n"
                //                + "      '111111111',\n"
                //                + "      '999999999'\n"
                //                + "    ) \n"
                //                + "    THEN NULL \n"
                //                + "    ELSE ssn \n"
                //                + "  END ssn,\n"
                //                + "  addressline1,\n"
                //                + "  CASE\n"
                //                + "    WHEN addressline2 = 'NULL_VALUE' \n"
                //                + "    THEN NULL \n"
                //                + "    ELSE addressline2 \n"
                //                + "  END addressline2,\n"
                //                + "  sa.city,\n"
                //                + "  sa.statecode state,\n"
                //                + "  sa.postalcode zipcode,\n"
                + "  organization_name,\n"
                + "  \n"
                + "  acuity_score,\n"
                + "  Program_level_consent_status,\n"
                + "  pce.status,\n"
                + "  pc.name payer_class,\n"
                + "  pp.name payer_plan,\n"
                + "  primary_payer_medicaid_medicare_id m_payer_id\n"
                + "FROM\n"
                + "  connect.patient_community_enrollment pce \n"
                + "  INNER JOIN connect.user_community_organization uco \n"
                + "    ON (pce.org_id = uco.org_id AND pce.community_id=uco.community_id) \n"
                + "  INNER JOIN connect.user u \n"
                + "    ON uco.user_id = u.user_id \n"
                //                + "  INNER JOIN patientmidm.sbyn_enterprise se \n"
                //                + "    ON pce.patient_id = se.lid \n"
                //                + "  INNER JOIN patientmidm.sbyn_patientmdmsbr sp \n"
                //                + "    ON sp.euid = se.euid  \n"
                + "     LEFT OUTER JOIN `connect`.`patient_source` ps \n"
                + " ON (pce.patient_source_id=ps.patient_source_id AND pce.community_id=ps.community_id) \n"
                //                + "  LEFT OUTER JOIN patientmidm.sbyn_addresssbr sa \n"
                //                + "    ON sp.patientmdmid = sa.patientmdmid \n"
                + "  LEFT OUTER JOIN connect.organization_cwd oc \n"
                + "    ON oc.org_id = pce.org_id \n"
                + "  LEFT OUTER JOIN connect.payer_plan pp \n"
                + "    ON (primary_payer_plan = pp.id AND pce.community_id=pp.community_id) \n"
                + "  LEFT OUTER JOIN connect.payer_class pc \n"
                + "    ON (primary_payer_class = pc.id AND pce.community_id=pc.community_id) \n"
                + "WHERE (program_level_consent_status <> 'Yes' OR program_level_consent_status IS NULL) \n"
                //                + "  AND se.systemcode = '2.16.840.1.113883.3.1358' \n"
                + "  AND pce.community_id=?1"
                + "  AND u.user_id =?2 GROUP BY  patient_id";

        return query;
    }

    private String buildQueryForNonConsentedRecordsForPowerUser() {
        String query = " SELECT  \n"
                + "  pce.patient_id,\n"
                //                + "  firstname first_name,\n"
                //                + "  \n"
                + "  pce.patient_euid,\n"
                + "   ps.organization source,\n"
                //                + "  CASE\n"
                //                + "    WHEN middlename = 'NULL_VALUE' \n"
                //                + "    THEN NULL \n"
                //                + "    ELSE middlename \n"
                //                + "  END middle_name,\n"
                //                + "  lastname last_name,\n"
                //                + "  CASE\n"
                //                + "    WHEN ssn IN (\n"
                //                + "      '000000000',\n"
                //                + "      '111111111',\n"
                //                + "      '999999999'\n"
                //                + "    ) \n"
                //                + "    THEN NULL \n"
                //                + "    ELSE ssn \n"
                //                + "  END ssn,\n"
                //                + "  addressline1,\n"
                //                + "  CASE\n"
                //                + "    WHEN addressline2 = 'NULL_VALUE' \n"
                //                + "    THEN NULL \n"
                //                + "    ELSE addressline2 \n"
                //                + "  END addressline2,\n"
                //                + "  sa.city,\n"
                //                + "  sa.statecode state,\n"
                //                + "  sa.postalcode zipcode,\n"
                + "  organization_name,\n"
                + "  \n"
                + "  acuity_score,\n"
                + "  Program_level_consent_status,\n"
                + "  pce.status,\n"
                + "  pc.name payer_class,\n"
                + "  pp.name payer_plan,\n"
                + "  primary_payer_medicaid_medicare_id m_payer_id\n"
                + "FROM\n"
                + "  connect.patient_community_enrollment pce \n"
                //                + "  INNER JOIN patientmidm.sbyn_enterprise se \n"
                //                + "    ON pce.patient_id = se.lid \n"
                //                + "  INNER JOIN patientmidm.sbyn_patientmdmsbr sp \n"
                //                + "    ON sp.euid = se.euid \n"
                + "     LEFT OUTER JOIN `connect`.`patient_source` ps \n"
                + " ON (pce.patient_source_id=ps.patient_source_id AND pce.community_id=ps.community_id) \n"
                //                + "  LEFT OUTER JOIN patientmidm.sbyn_addresssbr sa \n"
                //                + "    ON sp.patientmdmid = sa.patientmdmid \n"
                + "  LEFT OUTER JOIN connect.organization_cwd oc \n"
                + "    ON oc.org_id = pce.org_id \n"
                + "  LEFT OUTER JOIN connect.payer_plan pp \n"
                + "    ON (primary_payer_plan = pp.id AND pce.community_id=pp.community_id)  \n"
                + "  LEFT OUTER JOIN connect.payer_class pc \n"
                + "    ON (primary_payer_class = pc.id AND pce.community_id=pc.community_id) \n"
                + "WHERE (program_level_consent_status <> 'Yes' OR program_level_consent_status IS NULL) \n"
                //                + "  AND se.systemcode = '2.16.840.1.113883.3.1358' \n"
                + "  AND pce.community_id=?1 GROUP BY  patient_id";

        return query;
    }

    @Override
    public Map<String, SimplePerson> getPatientsFromMdm(PatientManager patientManager, List<String> patientIds, String communityOid) {
        EntityManager em = getEntityManager();

        try {
            logger.info("ReportDAOImpl:getPatientsFromMdm: " + patientIds);
            return patientManager.getMappedPatientsDirectQuery(em, patientIds, communityOid);
        } finally {
            em.close();
        }
    }

    @Override
    public Program getProgram(Long progId, long communityId) throws Exception{
        EntityManager em = getEntityManager();
        try {
            String programQuery = "select p.program_id, p.program_name, p.community_id, p.parent_id, p.is_a_parent,"
                   +  " p.consent_required, p.simple_view, p.consenter_required, p.minor_consent_required, p.program_date_required, p.group_id"
                   + " from `connect`.`program` p where p.program_id = ?1 and p.community_id = ?2";
            Query q = em.createNativeQuery(programQuery).setParameter(1, progId.longValue()).setParameter(2, communityId);
            //q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            Object[] progData = (Object[]) q.getSingleResult();
            Program result = Program.builder().id((Long)progData[0]).name((String)progData[1]).communityId((Long)progData[2])
                    .isAParent((Boolean)progData[4]).simpleView((Boolean)progData[6]).build();

            return  result;
        } finally {
            em.close();
        }

    }

    /**
     * use demographic criteria to get a list of patientMDM persons.
     *
     * @param searchCriteria
     * @return array of patientEuids from patientMDM that have been filtered by
     * searchCriteria
     */
    protected String[] findAndParsePatientMDMDemographicData(SearchCriteria searchCriteria, PatientManager patientManager, Long communityId) {
        List<SimplePerson> patientsFromMDM;
        String[] patientEuids = null;
        try {
            logger.info("this search has demographics, lets do this the hard way");

            patientsFromMDM = patientManager.findPatient(searchCriteria);
            int patMDMSize = 0;
            if (patientsFromMDM != null) {
                patMDMSize = patientsFromMDM.size();
            }
            logger.info("number of patients from mdm search: " + patMDMSize);
            // build your searchCriteria query, if you have demographics, use the euids in a join
            patientEuids = this.parseEuidListFromMDMPatients(patientsFromMDM);
        } catch (Exception ex) {
            logger.info("either no patients returned or you broke something: " + ex);
            patientsFromMDM = null;
        }
        return patientEuids;
    }

    protected String[] parseEuidListFromMDMPatients(List<SimplePerson> patientsFromMDM) {
        int listSize = patientsFromMDM.size();
        logger.info("parsing an mdm patient list with " + listSize + " patients in it");
        String[] patientEuids = new String[listSize];
        Collection<String> euidcollection
                = CollectionUtils.collect(patientsFromMDM, TransformerUtils.invokerTransformer("getPatientEuid"));
        euidcollection.toArray(patientEuids);
        logger.info("finished parsing mdm patient list");
        return patientEuids;
    }

    private String buildQueryForPatientProgramRecordsSearch(List<String> euidList) {
        String euids = "(" + StringUtils.join(euidList, ",") + ")";
        String query = "SELECT DISTINCT \n"
                + "(@n := @n + 1) AS ID,\n"
                + "  pce.patient_id,\n"
                + "  pce.patient_euid,\n"
                //+ "  p.program_name AS program_name,\n"
                + "  CASE WHEN p.is_a_parent IS true THEN null ELSE p.program_name END AS program_name,\n"
                //+ "  ppl.program_effective_date,\n"
                //+ "  ppl.program_end_date,\n"
                + "  ps.program_status_name AS patient_status,\n"
                + "  ppl.status_effective_date,\n"
                + "  pt.value AS termination_reason,\n"
                //+ "  p2.program_name AS health_home \n"
                + "  CASE WHEN p.IS_A_PARENT IS true THEN p.program_name ELSE concat(p2.SIMPLE_VIEW, ';', p2.program_name) END AS health_home \n"
                + "  \n"
                + "FROM\n"
                + "  connect.patient_community_enrollment pce \n"
                + "  CROSS JOIN (SELECT @n := 0) AS dummy \n"
                + "  INNER JOIN connect.view_user_patient_consent vupc \n"
                + "    ON (pce.patient_id = vupc.patient_id AND pce.community_id=vupc.community_id) \n"
                //+ "  INNER JOIN connect.patient_program_name ppn \n"
                //+ "    ON pce.patient_id = ppn.patient_id \n"
                + "  INNER JOIN connect.patient_program_link ppl \n"
                + "    ON pce.patient_id = ppl.patient_id \n"
                + "    AND pce.community_id = ppl.community_id \n"
                // "  INNER JOIN connect.program_name pn \n"
                + "  INNER JOIN connect.program p \n"
                + "    ON ppl.program_id = p.program_id \n"
                + "  LEFT OUTER JOIN connect.program p2 \n"
                + "    ON p.parent_id = p2.program_id \n"
                + "  LEFT OUTER JOIN connect.organization_cwd oc \n"
                + "    ON oc.org_id = pce.org_id \n"
                + "  LEFT OUTER JOIN connect.organization_mmis_oid omo \n"
                + "    ON oc.oid = omo.oid \n"
                //+ "  LEFT OUTER JOIN connect.patient_status ps \n"
                + "  LEFT OUTER JOIN connect.program_status ps \n"
                + "    ON ppl.program_status_id = ps.program_status_id \n"
                + "  LEFT OUTER JOIN connect.program_termination_link ptl \n"
                + "    ON ppl.program_termination_link_id = ptl.program_termination_link_id \n"
                + "  LEFT OUTER JOIN connect.program_termination pt \n"
                + "    ON ptl.program_termination_id = pt.id \n"
                + "  WHERE"
                + "  \n"
                + "  pce.community_id=?1"
                + " AND  vupc.USER_ID=?2"
                + " AND pce.patient_euid IN " + euids
                + " ORDER BY pce.patient_id, \n"
                + " CASE WHEN ( p2.program_name is NOT NULL and p2.SIMPLE_VIEW is true ) THEN CONCAT(pce.patient_id,p2.program_name, '_F2') ELSE concat(pce.patient_id, p.program_name, '_F1') END \n";
        return query;
    }

    private String buildQueryForPatientProgramRecordsSearchForPowerUser(List<String> euidList) {
        String euids = "(" + StringUtils.join(euidList, ",") + ")";
        String query = "SELECT DISTINCT \n"
                + "(@n := @n + 1) AS ID,\n"
                + "  pce.patient_id,\n"
                + "  pce.patient_euid,\n"
                //+ "  p.program_name AS program_name,\n"
                + "  CASE WHEN p.is_a_parent IS true THEN null ELSE p.program_name END AS program_name,\n"
                //+ "  ppl.program_effective_date,\n"
                //+ "  ppl.program_end_date,\n"
                + "  ps.program_status_name AS patient_status,\n"
                + "  ppl.status_effective_date,\n"
                + "  pt.value AS termination_reason,\n"
                //+ "  p2.program_name AS health_home \n"
                + "  CASE WHEN p.IS_A_PARENT IS true THEN p.program_name ELSE concat(p2.SIMPLE_VIEW, ';', p2.program_name) END AS health_home \n"
                + "  \n"
                + "FROM\n"
                + "  connect.patient_community_enrollment pce \n"
                + "  CROSS JOIN (SELECT @n := 0) AS dummy \n"
                + "  INNER JOIN connect.patient_program_link ppl \n"
                //+ "  INNER JOIN connect.patient_program_name ppn \n"
                //+ "    ON pce.patient_id = ppn.patient_id \n"
                + "    ON pce.patient_id = ppl.patient_id \n"
                + "    AND pce.community_id = ppl.community_id \n"
                //+ "  INNER JOIN connect.program_name pn \n"
                //+ "    ON ppn.program_id = pn.id \n"
                //+ "    AND ppn.community_id = pn.community_id \n"
                + "  INNER JOIN connect.program p \n"
                + "    ON ppl.program_id = p.program_id \n"
                + "  LEFT OUTER JOIN connect.program p2 \n"
                + "    ON p.parent_id = p2.program_id \n"
                + "  LEFT OUTER JOIN connect.organization_cwd oc \n"
                + "    ON oc.org_id = pce.org_id \n"
                + "  LEFT OUTER JOIN connect.organization_mmis_oid omo \n"
                + "    ON oc.oid = omo.oid \n"
                //+ "  LEFT OUTER JOIN connect.patient_status ps \n"
                //+ "    ON ppn.patient_status = ps.id \n"
                //+ "    AND ppn.community_id=ps.community_id \n"
                + "  LEFT OUTER JOIN connect.program_status ps \n"
                + "    ON ppl.program_status_id = ps.program_status_id \n"
                + "  LEFT OUTER JOIN connect.program_termination_link ptl \n"
                + "    ON ppl.program_termination_link_id = ptl.program_termination_link_id \n"
                + "  LEFT OUTER JOIN connect.program_termination pt \n"
                + "    ON ptl.program_termination_id = pt.id \n"
                + "  WHERE "
                + "  \n"
                + "  pce.community_id=?1"
                + " AND pce.patient_euid IN " + euids
                + " ORDER BY pce.patient_id, \n"
                + " CASE WHEN ( p2.program_name is NOT NULL and p2.SIMPLE_VIEW is true ) THEN CONCAT(pce.patient_id,p2.program_name, '_F2') ELSE concat(pce.patient_id, p.program_name, '_F1') END \n";

        return query;
    }

    private String buildQueryForConsentedRecordsForNonPowerUser() {
        String query = "SELECT "
                + " pce.patient_id,\n"
                + " pce.patient_euid,ps.organization source,\n"
                + "  organization_name,\n"
                + "  acuity_score,\n"
                + "  Program_level_consent_status,\n"
                + "  pce.status,\n"
                + "  pc.name payer_class,\n"
                + "  pp.name payer_plan,\n"
                + "  primary_payer_medicaid_medicare_id m_payer_id,\n"
                + " cc.name careteam,\n"
                + " pce.patient_user_active\n"
                + " FROM"
                + " connect.patient_community_enrollment pce \n"

                + "  INNER JOIN `all_user_patient_consent` vupc \n"
                + "   ON (pce.patient_id=vupc.patient_id AND pce.community_id = vupc.community_id AND vupc.user_id=?2) \n"
                + " LEFT JOIN patient_community_careteam pcc \n"
                + " ON (pce.patient_id=pcc.patient_id AND pcc.end_date IS NULL) \n"
                + "  LEFT JOIN community_careteam cc \n"
                + " ON pcc.community_careteam_id=cc.community_careteam_id \n"
                + "     LEFT OUTER JOIN `connect`.`patient_source` ps \n"
                + " ON (pce.patient_source_id=ps.patient_source_id AND pce.community_id=ps.community_id) \n"
                + "  LEFT OUTER JOIN connect.organization_cwd oc \n"
                + "    ON oc.org_id = pce.org_id \n"
                + "  LEFT OUTER JOIN connect.payer_plan pp \n"
                + "   ON (primary_payer_plan = pp.id AND pce.community_id=pp.community_id) \n"
                + " LEFT OUTER JOIN connect.payer_class pc \n"
                + "  ON (primary_payer_class = pc.id AND pce.community_id=pc.community_id) \n"
                + " WHERE (program_level_consent_status = 'Yes') \n"
                + "  AND pce.community_id=?1 \n"
                + " GROUP BY  pce.patient_id \n";

        return query;
    }

    private String buildQueryForConsentedRecordsForPowerUser() {
        String query = "SELECT"
                + " pce.patient_id,\n"
                + " cc.name careteam,\n"
                + " pce.patient_euid, \n"
                + " ps.organization source, \n"
                + " organization_name, \n"
                + " pce.patient_user_active,\n"
                + " acuity_score,\n"
                + " Program_level_consent_status,\n"
                + " pce.status,\n"
                + " pc.name payer_class,"
                + " pp.name payer_plan,"
                + " primary_payer_medicaid_medicare_id m_payer_id \n"
                + " FROM"
                + " connect.patient_community_enrollment pce \n"
                + " LEFT JOIN patient_community_careteam pcc \n"
                + " ON (pce.patient_id=pcc.patient_id AND pcc.end_date IS NULL)\n"
                + " LEFT JOIN community_careteam cc \n"
                + " ON pcc.community_careteam_id=cc.community_careteam_id \n"
                + " LEFT OUTER JOIN `connect`.`patient_source` ps"
                + " ON (pce.patient_source_id=ps.patient_source_id AND pce.community_id=ps.community_id)\n"
                + " LEFT OUTER JOIN connect.organization_cwd oc \n"
                + "   ON oc.org_id = pce.org_id \n"
                + " LEFT OUTER JOIN connect.payer_plan pp \n"
                + "  ON (primary_payer_plan = pp.id AND pce.community_id=pp.community_id)\n"
                + " LEFT OUTER JOIN connect.payer_class pc \n"
                + "  ON (primary_payer_class = pc.id AND pce.community_id=pc.community_id) \n"
                + " WHERE (program_level_consent_status = 'Yes') \n"
                + "  AND pce.community_id=?1 \n"
                + " GROUP BY  pce.patient_id";
        return query;
    }

    private String buildActionTypeForPTSQuery() {
        String query = "(";
        for(int i = 0; i < ACTION_TYPES.length; i++) {
            query += "'" + ACTION_TYPES[i] + "'";
            query += (i == ACTION_TYPES.length - 1) ? ")" : ",";
        }
        return query;
    }
}
