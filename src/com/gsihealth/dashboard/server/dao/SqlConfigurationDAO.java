/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.SqlConfiguration;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author ssingh
 */
public interface SqlConfigurationDAO {
    
     /**
     * Returns the value for this property. If it doesn't exist, returns the default value
     *
     * @param communityId
     * @param name
     * @param defaultValue
     * @return
     */
    public String getProperty(long communityId, String name, String defaultValue);

    /**
     * Returns the value for this property. If it doesn't exist, returns null
     *
     * @param communityId
     * @param name
     * @return
     */
    public String getProperty(long communityId, String name);
    
    /**
     * @param name
     * @return all properties by name
     */
    public List<SqlConfiguration> findConfigurationsByName(String name); 
    
    
    public EntityManager getEntityManager();
}
