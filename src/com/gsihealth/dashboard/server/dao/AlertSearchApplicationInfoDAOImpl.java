package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.AlertSearchApplicationInfo;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.Persistence;

/**
 *
 * @author Chad Darby
 */
public class AlertSearchApplicationInfoDAOImpl implements AlertSearchApplicationInfoDAO  {

    private EntityManagerFactory emf = null;

    public AlertSearchApplicationInfoDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public AlertSearchApplicationInfoDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }
    
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public List<AlertSearchApplicationInfo> findAlertSearchApplicationInfoEntities(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from AlertSearchApplicationInfo as o where o.alertSearchByApplicationPK.communityId=:theCommunityId order by o.displayOrder");
            q.setParameter("theCommunityId", communityId);
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
}
