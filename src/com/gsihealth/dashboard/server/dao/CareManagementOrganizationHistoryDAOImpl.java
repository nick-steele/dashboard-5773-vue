
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.CareManagementOrganizationHistory;
import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author ssingh
 */
public class CareManagementOrganizationHistoryDAOImpl implements CareManagementOrganizationHistoryDAO{
    
    private EntityManagerFactory emf = null;
    
    public CareManagementOrganizationHistoryDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public CareManagementOrganizationHistoryDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    @Override
    public void addToCareManagementOrganizationhistory(CareManagementOrganizationHistory careManagementOrganizationHistory) {
        careManagementOrganizationHistory.setOrganizationChangeDate(new Date());
       
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(careManagementOrganizationHistory);
            em.getTransaction().commit();
             
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
