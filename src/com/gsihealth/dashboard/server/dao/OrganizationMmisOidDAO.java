package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.OrganizationMmisOid;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public interface OrganizationMmisOidDAO {

    /**
     * Find a specific entity by OID
     * 
     * @param oid
     * @return 
     */
    public OrganizationMmisOid findOrganizationMmisOid(String oid,long communityId);

    /**
     * Get a list of all entities
     */
    public List<OrganizationMmisOid> findOrganizationMmisOidEntities(long communityId);
    
}
