/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.OrganizationPatientConsent;
import com.gsihealth.entity.User;
import com.gsihealth.entity.ViewUserPatientConsent;
import com.gsihealth.entity.ViewUserPatientConsentPK;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.PreexistingEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.RollbackFailureException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public interface ViewUserPatientConsentDAO {

    public void add(ViewUserPatientConsent viewUserPatientConsent, Date startDate) throws PreexistingEntityException, RollbackFailureException, Exception;

    public void add(List<ViewUserPatientConsent> viewUserPatientConsent) throws PreexistingEntityException, RollbackFailureException, Exception;
    
    public void deleteOldConsentEntries(long patientId);

    public void destroy(ViewUserPatientConsentPK id) throws NonexistentEntityException, RollbackFailureException, Exception;

    public ViewUserPatientConsent findViewUserPatientConsent(ViewUserPatientConsentPK id);

    public List<ViewUserPatientConsent> findViewUserPatientConsentEntities(long patientId, int pageNumber, int pageSize);

    public int getViewUserPatientConsentCount(long patientId);

    public void update(ViewUserPatientConsent viewUserPatientConsent) throws NonexistentEntityException, RollbackFailureException, Exception;

    public List<User> getUsers(long organizationId, long communityId);
    
    public boolean hasConsent(long userId, long patientId);

    public void deleteOldConsentEntries(long patientId, List<User> oldUsers, long communityId) throws Exception;
    
    public boolean exists(ViewUserPatientConsentPK id);

    public List<Long> getUserIds(long organizationId, long GSI_COMMUNITY_ID);

    public List<Long> getUserIds(List<OrganizationPatientConsent> orgs, long GSI_COMMUNITY_ID);
    
}
