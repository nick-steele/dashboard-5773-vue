package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.UserAlert;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.RollbackFailureException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import java.util.logging.Level;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

/**
 *
 * @author Chad Darby
 */
public class UserAlertDAOImpl implements UserAlertDAO {

    private EntityManagerFactory emf = null;
    private Logger logger = Logger.getLogger(getClass().getName());

    public UserAlertDAOImpl() {
        emf = Persistence.createEntityManagerFactory("gsialertingPU_standalone");
    }

    public UserAlertDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    /**
     * Update an alert
     *
     * @param userAlert
     * @throws NonexistentEntityException
     * @throws RollbackFailureException
     * @throws Exception
     */
    @Override
    public void update(UserAlert userAlert) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();
            userAlert = em.merge(userAlert);
            utx.commit();

        } catch (Exception ex) {
            String message = "Error deleting alerts: " + ex.getMessage();
            logger.log(Level.SEVERE, message, ex);
            Throwable cause = ex;
            while (cause != null) {
                if (cause instanceof javax.validation.ConstraintViolationException) {
                    javax.validation.ConstraintViolationException cve = (javax.validation.ConstraintViolationException) cause;

                    for (javax.validation.ConstraintViolation violation : cve.getConstraintViolations()) {
                        logger.warning("violation error:" + violation.getMessage());
                        logger.warning(violation.getRootBeanClass() + " " + violation.getPropertyPath() + " " + violation.getMessage()
                                + " " + violation.getRootBean() + ", invalid value: [" + violation.getInvalidValue() + "]");
                    }
                }
                cause = cause.getCause();
            }

            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }

            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<UserAlert> searchUserAlertsByPatientFirstName(long userId, String searchValue, Long communityId,
            int pageNumber, int pageSize) {

        String field = "patientFirstName";
        return searchUserAlertsBy(field, userId, searchValue, communityId, pageNumber, pageSize);
    }

    @Override
    public List<UserAlert> searchUserAlertsByPatientId(long userId, String searchValue,
            Long communityId, int pageNumber, int pageSize) {

        String field = "patientId";
        return searchUserAlertsBy(field, userId, searchValue, communityId, pageNumber, pageSize);
    }

    /**
     * Move to the end of today: mm/dd/yy 23:59:59
     *
     * @param toDate
     * @return
     */
    protected Date moveToEndOfDay(Date toDate) {
        toDate = DateUtils.setHours(toDate, 23);
        toDate = DateUtils.setMinutes(toDate, 59);
        toDate = DateUtils.setSeconds(toDate, 59);

        return toDate;
    }

    /**
     * FIXME - sql injection waiting to happen
     *
     * @param field
     * @param userId
     * @param searchValue
     * @param pageNumber
     * @param pageSize
     * @return
     */
    protected List<UserAlert> searchUserAlertsBy(String field, long userId, String searchValue, Long communityId,
            int pageNumber, int pageSize) {
        EntityManager em = getEntityManager();

        if (StringUtils.isBlank(searchValue)) {
            throw new IllegalArgumentException("A search value is required.");
        }

        searchValue = searchValue.toLowerCase() + "%";

        try {
            String jpql = "select object(userAlert) from UserAlert as userAlert"
                    + " where"
                    + " userAlert.userId=:theUserId"
                    + " and userAlert.isVisible=true"
                    + " AND userAlert.communityId = :communityId"
                    + " and lower(userAlert.alertMessageId." + field + ") like :theSearchValue"
                    + " order by userAlert.creationDateTime desc";

            Query query = em.createQuery(jpql);
            query.setParameter("theUserId", userId);
            query.setParameter("theSearchValue", searchValue);
            query.setParameter("communityId", communityId);
            query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);

            query.setFirstResult(startRecordNumber);
            query.setMaxResults(pageSize);

            return query.getResultList();
        } finally {
            em.close();
        }
    }

    protected List<UserAlert> searchUserAlertsBy(String field, long userId, String searchValue, Long communityId) {
        EntityManager em = getEntityManager();

        if (StringUtils.isBlank(searchValue)) {
            throw new IllegalArgumentException("A search value is required.");
        }

        searchValue = searchValue.toLowerCase() + "%";

        try {
            String jpql = "select object(userAlert) from UserAlert as userAlert"
                    + " where"
                    + " userAlert.userId=:theUserId"
                    + " and userAlert.isVisible=true"
                    + " and userAlert.communityId = :communityId"
                    + " and lower(userAlert.alertMessageId." + field + ") like :theSearchValue"
                    + " order by userAlert.creationDateTime desc";

            Query query = em.createQuery(jpql);
            query.setParameter("theUserId", userId);
            query.setParameter("communityId", communityId);
            query.setParameter("theSearchValue", searchValue);
            query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return query.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public List<UserAlert> searchUserAlertsByPatientLastName(long userId, String searchValue,
            Long communityId, int pageNumber, int pageSize) {
        String field = "patientLastName";
        return searchUserAlertsBy(field, userId, searchValue, communityId, pageNumber, pageSize);
    }

    @Override
    public List<UserAlert> searchUserAlertsBySeverity(long userId, String searchValue,
            Long communityId, int pageNumber, int pageSize) {
        String field = "severity";
        return searchUserAlertsBy(field, userId, searchValue, communityId, pageNumber, pageSize);
    }

    @Override
    public List<UserAlert> searchUserAlertsByApplication(long userId, String searchValue,
            Long communityId, int pageNumber, int pageSize) {
        EntityManager em = getEntityManager();

        if (StringUtils.isBlank(searchValue)) {
            throw new IllegalArgumentException("A search value is required.");
        }

        long applicationId = Long.parseLong(searchValue);

        try {
            String jpql = "select object(userAlert) from UserAlert as userAlert"
                    + " where"
                    + " userAlert.userId=:theUserId"
                    + " and userAlert.isVisible=true"
                    + " and userAlert.communityId = :communityId"
                    + " and userAlert.alertMessageId.applicationId = :theSearchValue"
                    + " order by userAlert.creationDateTime desc";

            Query query = em.createQuery(jpql);
            query.setParameter("theUserId", userId);
            query.setParameter("communityId", communityId);
            query.setParameter("theSearchValue", applicationId);
            query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);

            query.setFirstResult(startRecordNumber);
            query.setMaxResults(pageSize);

            return query.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public List<UserAlert> searchUserAlertsByDate(long userId, Long communityId,
            Date fromDate, Date toDate, int pageNumber, int pageSize) {

        EntityManager em = getEntityManager();

        // truncate the date to remove the time (hours, mins, seconds)
        fromDate = DateUtils.truncate(fromDate, Calendar.DATE);
        toDate = moveToEndOfDay(toDate);

        try {
            String jpql = "select object(userAlert) from UserAlert as userAlert"
                    + " where"
                    + " userAlert.userId=:theUserId"
                    + " and userAlert.isVisible=true"
                    + " and userAlert.communityId = :communityId"
                    + " and userAlert.creationDateTime BETWEEN :theFromDate and :theToDate"
                    + " order by userAlert.creationDateTime desc";

            Query query = em.createQuery(jpql);
            query.setParameter("theUserId", userId);
            query.setParameter("communityId", communityId);
            query.setParameter("theFromDate", fromDate);
            query.setParameter("theToDate", toDate);
            query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);

            query.setFirstResult(startRecordNumber);
            query.setMaxResults(pageSize);

            return query.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public int getTotalCountForSearchByDate(long userId, Long communityId, Date fromDate, Date toDate) {

        EntityManager em = getEntityManager();

        // truncate the date to remove the time (hours, mins, seconds)
        fromDate = DateUtils.truncate(fromDate, Calendar.DATE);
        toDate = moveToEndOfDay(toDate);

        try {
            String jpql = "select count(userAlert) from UserAlert as userAlert"
                    + " where"
                    + " userAlert.userId=:theUserId"
                    + " and userAlert.isVisible=true"
                    + " and userAlert.communityId = :communityId"
                    + " and userAlert.creationDateTime BETWEEN :theFromDate and :theToDate";

            Query query = em.createQuery(jpql);
            query.setParameter("theUserId", userId);
            query.setParameter("communityId", communityId);
            query.setParameter("theFromDate", fromDate);
            query.setParameter("theToDate", toDate);
            query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int count = ((Long) query.getSingleResult()).intValue();

            return count;
        } finally {
            em.close();
        }
    }

    @Override
    public int getTotalCount(long userId, Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from UserAlert o"
                    + " where o.userId = :theUserId"
                    + " and o.isVisible = true"
                    + " and o.communityId = :communityId");
            q.setParameter("theUserId", userId);
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int count = ((Long) q.getSingleResult()).intValue();
            return count;

        } finally {
            em.close();
        }
    }

    @Override
    public List<UserAlert> findUserAlertEntities(long userId, Long communityId, int pageNumber, int pageSize) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from UserAlert as o"
                    + " where o.userId=:theUserId"
                    + " AND o.communityId = :communityId"
                    + " and o.isVisible=true order by o.creationDateTime desc");
            q.setParameter("theUserId", userId);
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);

            q.setFirstResult(startRecordNumber);
            q.setMaxResults(pageSize);

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public int getTotalCountForSearchByPatientLastName(long userId, String searchValue, Long communityId) {
        return getTotalCountForSearchBy("patientLastName", userId, searchValue, communityId);
    }

    @Override
    public int getTotalCountForSearchByPatientFirstName(long userId, String searchValue, Long communityId) {
        return getTotalCountForSearchBy("patientFirstName", userId, searchValue, communityId);
    }

    @Override
    public int getTotalCountForSearchByPatientId(long userId, String searchValue, Long communityId) {
        return getTotalCountForSearchBy("patientId", userId, searchValue, communityId);
    }

    @Override
    public int getTotalCountForSearchBySeverity(long userId, String searchValue, Long communityId) {
        return getTotalCountForSearchBy("severity", userId, searchValue, communityId);
    }

    protected int getTotalCountForSearchBy(String field, long userId, String searchValue, Long communityId) {
        EntityManager em = getEntityManager();

        if (StringUtils.isBlank(searchValue)) {
            throw new IllegalArgumentException("A search value is required.");
        }

        searchValue = searchValue.toLowerCase() + "%";

        try {
            String jpql = "select count(userAlert) from UserAlert as userAlert"
                    + " where"
                    + " userAlert.userId=:theUserId"
                    + " and userAlert.isVisible=true"
                    + " and userAlert.communityId = :communityId"
                    + " and lower(userAlert.alertMessageId." + field + ") like :theSearchValue";

            Query query = em.createQuery(jpql);
            query.setParameter("theUserId", userId);
            query.setParameter("theSearchValue", searchValue);
            query.setParameter("communityId", communityId);
            query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int count = ((Long) query.getSingleResult()).intValue();

            return count;
        } finally {
            em.close();
        }
    }

    @Override
    public int getTotalCountForSearchByApplication(long userId, String searchValue, Long communityId) {
        EntityManager em = getEntityManager();

        if (StringUtils.isBlank(searchValue)) {
            throw new IllegalArgumentException("A search value is required.");
        }

        long applicationId = Long.parseLong(searchValue);

        try {
            String jpql = "select count(userAlert) from UserAlert as userAlert"
                    + " where"
                    + " userAlert.userId=:theUserId"
                    + " and userAlert.isVisible=true"
                    + " and userAlert.communityId = :communityId"
                    + " and userAlert.alertMessageId.applicationId = :theSearchValue";

            Query query = em.createQuery(jpql);
            query.setParameter("theUserId", userId);
            query.setParameter("theSearchValue", applicationId);
            query.setParameter("communityId", communityId);
            query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int count = ((Long) query.getSingleResult()).intValue();

            return count;
        } finally {
            em.close();
        }
    }
}
