/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.Community;
import com.gsihealth.entity.CommunityOrganization;
import com.gsihealth.entity.OrganizationCwd;
import java.util.List;

/**
 *
 * @author User
 */
public interface CommunityOrganizationDAO {

    public void create(CommunityOrganization communityOrganization);
    
    public void update(CommunityOrganization communityOrganization);
    
    public CommunityOrganization findCommunityOrganization(long communityId, long orgId);
    
    public CommunityOrganization findCommunityOrganization(Community community, OrganizationCwd organization);
    
    public CommunityOrganization findCommunityOrganization(Long id);

    public List<CommunityOrganization> findCommunityOrganizationEntities();

    public int getCommunityOrganizationCount();
    
    public List<Long> getAutoConsentOrganizationId(long communityId, char autoConsentStatus);
}
