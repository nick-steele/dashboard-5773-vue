package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.DisenrollmentReasons;
import java.util.List;
import javax.persistence.NoResultException;

/**
 *
 * @author Satyendra Singh
 */
public interface DisenrollmentReasonDAO {
    
    public List<DisenrollmentReasons> getDisenrollmentReasons(Long communityId);
    
    public DisenrollmentReasons getDisenrollmentReason(Long communityId, String reason);
    
    public Long getDisenrollmentId(String reason) throws NoResultException;
}
