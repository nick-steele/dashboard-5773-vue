package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.server.SequenceNames;
import com.gsihealth.dashboard.server.common.PatientResult;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.PreexistingEntityException;
import com.gsihealth.dashboard.server.dao.util.DaoUtils;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import com.gsihealth.dashboard.server.service.PatientConsentUtils;
import com.gsihealth.dashboard.server.util.DateUtils;
import com.gsihealth.entity.*;
import com.gsihealth.entity.UserPatient;
import com.gsihealth.entity.insurance.PayerClass;
import com.gsihealth.entity.insurance.PayerClassPK;
import com.gsihealth.entity.insurance.PayerPlan;
import com.gsihealth.entity.insurance.PayerPlanPK;
import com.gsihealth.patientmdminterface.entity.DuplicateCheckProcessResult;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Chad Darby
 */
public class PatientEnrollmentDAOImpl implements PatientEnrollmentDAO {

    private PatientManager patientManager;
    private EntityManagerFactory emf = null;
    private Logger logger = Logger.getLogger(getClass().getName());
    private String communityOid;
    private Map<Long, PayerPlan> payerPlansMap;
    private Map<Long, PayerClass> payerClassesMap;
    private boolean nativeSQL = false;
    private Long communityId;
    private ConfigurationDAO configurationDAO;

    public PatientEnrollmentDAOImpl(PatientManager thePatientManager, ConfigurationDAO theConfigurationDAO) {
        patientManager = thePatientManager;
        this.communityId = patientManager.getCommunityId();
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
        configurationDAO = theConfigurationDAO;
        loadPayerPlansAndPayerClassesInCache();
    }

    public PatientEnrollmentDAOImpl(PatientManager thePatientManager, EntityManagerFactory theEmf,
                                    ConfigurationDAO theConfigurationDAO, String commOid, Long communityId) {
        patientManager = thePatientManager;
        emf = theEmf;
        configurationDAO = theConfigurationDAO;
        communityOid = commOid;
        this.communityId = communityId;
        loadPayerPlansAndPayerClassesInCache();
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    //JIRA 870 - needed to have one from clause
    @Override
    public Root<PatientCommunityEnrollment> getPatientCommunityEnrollmentRootForQuery() {
        EntityManager entityManager = this.getEntityManager();
        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<PatientCommunityEnrollment> cq = cb.createQuery(PatientCommunityEnrollment.class);
            return cq.from(PatientCommunityEnrollment.class);
        } finally {
            this.cleanUpPersistenceResources(entityManager);
        }
    }

	/*
     * protected long getNextPatientId() { long id = 0;
	 *
	 * EntityManager em = getEntityManager(); try { //String qryString =
	 * "UPDATE connect.control_values SET int_val = int_val + 1 WHERE value_id='next_patient_id'"
	 * ; Query query = em.createQuery(
	 * "select object(o) from PatientCommunityEnrollment as o order by o.patientId desc"
	 * ); query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
	 * query.setMaxResults(1);
	 *
	 * try { PatientCommunityEnrollment patientEnrollment =
	 * (PatientCommunityEnrollment) query.getSingleResult(); //FIXME WHAT THE
	 * WHAT???????? STOP THIS if (patientEnrollment != null) { id =
	 * patientEnrollment.getPatientId() + 1; } } catch (NoResultException exc) {
	 * // do nothing since we'll use the id = 0; }
	 *
	 * return id; } finally { em.close(); }
	 *
	 * }
	 */

    /**
     * TODO this can be more generic, parameterize the class as well as the
     * expression and you can use this more widely
     *
     * @param searchPredicate
     * @param limitPatientCount
     * @return row count for patientEnrollment using searchExpression
     */
    @Override
    //JIRA 860-1460 (1466)
    //Migrate EclipseLink Expression to pure JPA Criteria
    //public int getPatientSearchTotalCount(Expression searchExpression, Integer limitPatientCount) {
    public int getPatientSearchTotalCount(Root<PatientCommunityEnrollment> pceRoot,
                                          Predicate searchPredicate, Integer limitPatientCount) {

        logger.info(("lets count some patientEnrollment rows"));
        List reportRows = null;
        long count = 0;
        EntityManager entityManager = this.getEntityManager();
        try {
            logger.info("starting reportQuery stuff");
            //if (limitPatientCount != null) {
            //	reportQuery.setMaxRows(limitPatientCount);
            //}

            //String sqlStatement = reportQuery.getSQLString();
            //logger.info("the result of all our labors, the sql statement: " + sqlStatement);

            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<Long> cq = cb.createQuery(Long.class);
            cq.select(cb.count(pceRoot));
            cq.where(searchPredicate);
            TypedQuery<Long> tq = entityManager.createQuery(cq);
            if (limitPatientCount != null) {
                logger.info("setting max results:" + limitPatientCount);
                tq.setMaxResults(limitPatientCount);
            }
            count = tq.getSingleResult();

        } catch (Exception tle) {
            logger.log(Level.SEVERE, "count failed:", tle);
        } finally {
            this.cleanUpPersistenceResources(entityManager);
        }
        logger.info("counting done, returning " + count);
        return (int) count;
    }

    private void cleanUpPersistenceResources(EntityManager em) {
        if (em != null) {
            em.close();
        }
    }

    /**
     * @param searchPredicate
     * @param pageNumber
     * @param pageSize
     * @param limitPatientCount TODO - not using, this sets max for entire query, do we need
     *                          it when paginating? I dunno
     * @return a list of patientEnrollments filtered by query expression
     */
    @Override
    public List<PatientCommunityEnrollment> queryPatientEnrollmentBySearchExpression(Predicate searchPredicate,
                                                                                     int pageNumber, int pageSize, Integer limitPatientCount) {
        // lets get some results
        EntityManager entityManager = null;
        entityManager = this.getEntityManager();
        List<PatientCommunityEnrollment> peresults = null;
        try {
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<PatientCommunityEnrollment> cq = cb.createQuery(PatientCommunityEnrollment.class);
            cq.where(searchPredicate);
            TypedQuery<PatientCommunityEnrollment> tq = entityManager.createQuery(cq);
            if (pageSize != -1) {
                int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);
                logger.info("startRecordNumber: " + startRecordNumber);
                logger.info("pageSize: " + pageSize);
                tq = tq.setFirstResult(startRecordNumber);
                tq = tq.setMaxResults(pageSize);
            }

            peresults = tq.getResultList();

            if (peresults != null) {
                logger.fine("found " + peresults.size() + " patients during pce search");
                this.assignPayerPlanAndPayerClass(peresults);
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "oh noes, something went wrong with getting pe data: ", ex);
        } finally {
            this.cleanUpPersistenceResources(entityManager);
        }
        return peresults;
    }

    /**
     * TODO this method is ginormous, break it up into something less daunting
     * This is where the magic happens, dynamically build criteria query for
     * patient search check each pce fields in searchCriteria and add it to the
     * searchExpression if found
     *
     * @param searchCriteria - user generated search params
     * @param patientEuids   - until we find a better way, do the mdm patient demographic
     *                       search first
     * @param userOrgId      - logged on user's org id
     * @param communityId
     * @param enrolled       - enrolled status
     * @param userId
     * @return criteria query with all the above params included dynamically
     */
    //JIRA 860-1460 (1466)
    //Migrate EclipseLink Expression to pure JPA Criteria
    @Override
    //public Expression criteriaBuilderForPatientSearch(SearchCriteria searchCriteria, String[] patientEuids,
    public Predicate criteriaBuilderForPatientSearch(Root<PatientCommunityEnrollment> pce,
                                                     SearchCriteria searchCriteria, String[] patientEuids,
                                                     Long userOrgId, long communityId, boolean enrolled, long userId, boolean isMangeConsentWithoutEnrollment,
                                                     boolean isAllOrgsSelfAssert) {

        // aight, how long will all this crap take?
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        logger.info("---- criteriaBuilderForPatientSearch");
        //ExpressionBuilder pce = new ExpressionBuilder(PatientCommunityEnrollment.class);
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<PatientCommunityEnrollment> cq = cb.createQuery(PatientCommunityEnrollment.class);

            // lets build an expression, we always need community id, so lets start
            // there
            //Expression baseExpression = pce.get("community").get("communityId").equal(communityId);
            Predicate basePredicate = cb.equal(pce.get("community").get("communityId"), communityId);
            System.out.println("CommunityID>>>>" + communityId);

            // status needs to be set whether searchCriteria exists or not, if
            // search criteria isn't null and user needs to search by
            // status, narrow it down later on
            // JIRA - 842


            // everything else is negotiable
            // got patient demographics?
            if (searchCriteria != null) {
                logger.info("searchCriteria isn't null, user: " + userId + " patient search");
                String patientId = searchCriteria.getPatientID();

                // looking for a specific patient id
                if (patientId != null && !patientId.trim().isEmpty()) {

                    // strip leading issues: JIRA-3726
                    patientId = DaoUtils.stripLeadingZeros(patientId);

                    logger.info(patientId + " patient id selected for search");
                    //Expression patientIdQry = pce.get("patientId").equalsIgnoreCase(patientId);
                    //baseExpression = baseExpression.and(patientIdQry);
                    //equalsIgnoreCase done with upper
                    Predicate patientIdQry = cb.equal(cb.upper(pce.<String>get("patientId")), patientId.toUpperCase());
                    basePredicate = cb.and(basePredicate, patientIdQry);
                }

                if (patientEuids != null) {
                    logger.info("patient demographics exist, add a search expression for a total of " + patientEuids.length
                            + " patients" + " who meet the demographics criteria");
                    // patientEuids is empty, put something in there so mySql won't
                    // complain
                    if (patientEuids.length == 0) {
                        patientEuids = new String[]{"-1"};
                    }
                    //Expression euidExpr = pce.get("patientEuid").in(patientEuids);
                    //baseExpression = baseExpression.and(euidExpr);
                    Predicate euidPred = pce.get("patientEuid").in(patientEuids);
                    basePredicate = cb.and(basePredicate, euidPred);
                }

                // if there are patients with null or empty EUIDs, don't include in
                // the search
                //baseExpression = baseExpression.and(pce.get("patientEuid").notNull());
                //baseExpression = baseExpression.and(pce.get("patientEuid").notEqual(""));
                basePredicate = cb.and(basePredicate, pce.get("patientEuid").isNotNull());
                basePredicate = cb.and(basePredicate, cb.notEqual(pce.get("patientEuid"), ""));


                // primary or secondary insurance
                String mediCaidCareId = searchCriteria.getMediCaidCareId();
                if (mediCaidCareId != null && !mediCaidCareId.trim().isEmpty()) {
                    logger.info(mediCaidCareId + "  selected for medicaid search");
                    //Expression mediCaidQry = pce.get("primaryPayerMedicaidMedicareId").equalsIgnoreCase(mediCaidCareId)
                    //		.or(pce.get("secondaryPayerMedicaidMedicareId").equalsIgnoreCase(mediCaidCareId));
                    //baseExpression = baseExpression.and(mediCaidQry);
                    Predicate mediCaidQry = cb.or(cb.equal(cb.upper(pce.<String>get("primaryPayerMedicaidMedicareId")),
                            mediCaidCareId.toUpperCase()),
                            cb.equal(cb.upper(pce.<String>get("secondaryPayerMedicaidMedicareId")),
                                    mediCaidCareId.toUpperCase()));
                    basePredicate = cb.and(basePredicate, mediCaidQry);
                }

                // health home info

                basePredicate = getProgramPredicate(pce, communityId, searchCriteria, cb, cq, basePredicate);

                // Active/Inactive patient messaging
                String isActive = searchCriteria.getActivePatient();
                if (StringUtils.isNotBlank(isActive)) {
                    logger.info("Patient Messaging selected:" + isActive);
                    //Expression isActiveQry = pce.get("patientUserActive").equalsIgnoreCase(isActive);
                    //baseExpression = baseExpression.and(isActiveQry);
                    Predicate isActiveQry = cb.equal(cb.upper(pce.<String>get("patientUserActive")), isActive.toUpperCase());
                    basePredicate = cb.and(basePredicate, isActiveQry);
                }

                // patient org id - TODO this really should be a Long, not long, so
                // it can be nulled, otherwise an orgId can never be 0
                long organizationId = searchCriteria.getOrganizationId();
                if (organizationId > 0) {
                    logger.info(organizationId + " patient org id selected");
                    //Expression orgNameQry = pce.get("orgId").equal(organizationId);
                    //baseExpression = baseExpression.and(orgNameQry);
                    Predicate orgNameQry = cb.equal(pce.get("orgId"), organizationId);
                    basePredicate = cb.and(basePredicate, orgNameQry);
                }


                basePredicate = getCareteamPredicate(pce, communityId, searchCriteria, cb, cq, basePredicate);


            }
            /*
			 * if user is a power user userOrgId will be null user can look at any
			 * patient regardless of userConsent FIXME - is this wrong? might not be
			 * enrolled vs not enrolled if user isn't a power user: if status is
			 * pending, inactive or "" : user and patient must be in the same org if
			 * enrolled or assigned: user and patient in opc, user and patient NOT
			 * in upc do this last, hopefully work with a smaller pool of patients
			 *
			 * if enrolled is true means patient search for Careteam app if enrolled
			 * is false means patient search for Enrollment app
			 *
			 */
            logger.fine(userOrgId + " <-- if blank, this is a power user");
            if (userOrgId != null) {
                logger.info(userOrgId + " user isn't a power user, this is their orgId ");

                if ((searchCriteria == null)
                        || (searchCriteria != null && StringUtils.isBlank(searchCriteria.getProviderType()))) {

                    if (!enrolled) {
                        logger.info("patients for enrollment app.. ");
                        // when user wants to search additional patients on the
                        // basis of first_name and last_name with check box for
                        // search clicked
                        boolean addtionalPatients = searchCriteria != null && searchCriteria.getFirstName() != null &&
                                searchCriteria.getLastName() != null && searchCriteria.isSearchAllPatients();
                        basePredicate = getSearchExpressionForEnrollmentApp(cb, cq, basePredicate, pce, userOrgId, userId,
                                addtionalPatients, isMangeConsentWithoutEnrollment, isAllOrgsSelfAssert, communityId);
                    } else {
                        logger.info("patients for careteam app.. ");
                        basePredicate = cb.and(basePredicate, getSearchExpressionForCareTeamApp(cb, cq, pce, userOrgId, userId, communityId));
                    }


                    // ----REMOVE ABOVE THIS LINE--------------
					/*
					 * //DASHBOARD-609 refacter Consent to use DataBase View //TODO
					 * unit testing before deploying logger.info(
					 * "new consent logic for enrolled patients ");
					 * ExpressionBuilder viewConsentedPatients = new
					 * ExpressionBuilder(ViewUserPatientConsent.class); Expression
					 * consentedPatients =
					 * viewConsentedPatients.get("user_id").equal(userId); //be sure
					 * to add u.user_id to view baseExpression =
					 * baseExpression.and(consentedPatients);
					 */
                }
            }

            stopWatch.stop();
            logger.info("it took " + stopWatch.getTime() / 1000.0 + " secs to build this query");

            return basePredicate;
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error criteriaBuilderForPatientSearch: ", exc);
            return null;
        } finally {
            this.cleanUpPersistenceResources(em);
        }
    }

    private Predicate getCareteamPredicate(Root<PatientCommunityEnrollment> pce, long communityId, SearchCriteria searchCriteria, CriteriaBuilder cb, CriteriaQuery<PatientCommunityEnrollment> cq, Predicate basePredicate) {
        if (searchCriteria == null)
            return basePredicate;

        String careteamId = searchCriteria.getCareteamId();
        String providerId = searchCriteria.getNetworkProviderId();

        if (StringUtils.isBlank(careteamId) &&
                StringUtils.isBlank(providerId))
            return basePredicate;

        Subquery<Long> userPatientIdSubQuery = cq.subquery(Long.class);
        Root<UserPatient> userPatientRoot = userPatientIdSubQuery.from(UserPatient.class);
        userPatientIdSubQuery.select(userPatientRoot.get("patientCommunityEnrollment").<Long>get("patientId")).distinct(true);
        //Subquery<UserPatient> userPatientSubquery = cq.subquery(UserPatient.class);
        //Root<UserPatient> userPatientRoot = userPatientSubquery.from(UserPatient.class);
        //userPatientSubquery.select(userPatientRoot);
        List<Predicate> predicates = new ArrayList<Predicate>();

        if (careteamId != null && !careteamId.trim().isEmpty()) {
            logger.info(careteamId + " care team id selected");
            //Subquery<Long> subQuery = cq.subquery(Long.class);
                    /*Root<PatientCommunityCareteam> pcc = subQuery.from(PatientCommunityCareteam.class);
                    Predicate patientInPcc = cb.and(
                        cb.equal(pce.get("patientId"), pcc.get("patientCommunityCareteamPK").get("patientId")),
                        cb.equal(pcc.get("patientCommunityCareteamPK").get("communityCareteamId"), careteamId),
                        pcc.get("endDate").isNull());

                    basePredicate = cb.and(basePredicate, patientInPcc);*/

            if (careteamId.equalsIgnoreCase(com.gsihealth.dashboard.common.Constants.BLANK_CARETEAM_ID)) {
                predicates.add(cb.isNull(userPatientRoot.get("communityCareteamId")));
            }
            else {
                Join userPatientCareteamJoin = userPatientRoot.join("communityCareteam", JoinType.LEFT);
                predicates.add(cb.equal(userPatientCareteamJoin.get("communityCareteamId"), careteamId));
            }
        }

        logger.info("Provider Id " + providerId);
        if (providerId != null && !providerId.trim().isEmpty()) {
            if (searchCriteria.getProviderType().equalsIgnoreCase(com.gsihealth.dashboard.common.Constants.IN_PROVIDER)) {
                logger.info("In Network Provider is selected");
                                        /*
                                        *   Spira 7369 Code Fix for Predicate Query Based on PowerUser in all communities @Raj
                                        */
                /*Root<PatientCommunityCareteam> pcc = cq.from(PatientCommunityCareteam.class);
                Predicate patientInPcc ;
                final Predicate predicateWithoutPowerUser = cb.equal(pce.get("patientId"), pcc.get("patientCommunityCareteamPK").get("patientId"));
                if(userOrgId != null){
                    patientInPcc = cb.and(predicateWithoutPowerUser,
                            cb.equal(pce.get("orgId"), userOrgId));
                }else{
                    patientInPcc = cb.and(predicateWithoutPowerUser);
                }
                Root<UserCommunityCareteam> ucc = cq.from(UserCommunityCareteam.class);
                Predicate patientInUcc = cb.and(
                        cb.equal(ucc.get("userCommunityCareteamPK").get("communityCareteamId"), pcc.get("patientCommunityCareteamPK").get("communityCareteamId")),
                        pcc.get("endDate").isNull(),
                        cb.equal(ucc.get("userCommunityCareteamPK").get("userId"),providerId));
                basePredicate = cb.and(basePredicate, patientInPcc, patientInUcc);*/
                Join userPatientUserJoin = userPatientRoot.join("user", JoinType.LEFT);
                predicates.add(cb.and(cb.equal(userPatientUserJoin.get("userId"), providerId),
                        cb.equal(userPatientRoot.get("userType"), "INTERNAL")));
            }
            else if(searchCriteria.getProviderType().equalsIgnoreCase(com.gsihealth.dashboard.common.Constants.OUT_PROVIDER)) {
                //Fetch userPatientNHHPFetch = userPatientRoot.fetch("nonHealthHomeProvider", JoinType.LEFT);
                Join userPatientNHHPJoin = userPatientRoot.join("nonHealthHomeProvider", JoinType.LEFT);
                predicates.add(cb.and(cb.equal(userPatientNHHPJoin.get("nonHealthHomeProviderPK").get("providerId"), providerId),
                        cb.equal(userPatientNHHPJoin.get("nonHealthHomeProviderPK").get("communityId"), communityId),
                        cb.equal(userPatientRoot.get("userType"), "EXTERNAL")));
            }
        }

        //userPatientSubquery.where(toArray(predicates));
        //basePredicate = cb.and(basePredicate, cb.exists(userPatientSubquery));
        if(predicates.size() > 0) {
            userPatientIdSubQuery.where(toArray(predicates));
            Predicate patientInPcc = pce.get("patientId").in(userPatientIdSubQuery);
            basePredicate = cb.and(basePredicate, patientInPcc);
        }

        return basePredicate;
    }

    private Predicate getProgramPredicate(Root<PatientCommunityEnrollment> pce, long communityId, SearchCriteria searchCriteria, CriteriaBuilder cb, CriteriaQuery<PatientCommunityEnrollment> cq, Predicate basePredicate) {
        String parentProgramId = searchCriteria.getHealthHome();
        String subordinateProgramId = searchCriteria.getProgramNameId();
        String statusId = searchCriteria.getEnrollmentStatus();
        String consent = searchCriteria.getProgramLevelConsentStatus();

        if (StringUtils.isBlank(parentProgramId) &&
                StringUtils.isBlank(subordinateProgramId) &&
                StringUtils.isBlank(statusId) &&
                StringUtils.isBlank(consent))
            return basePredicate;

        Subquery<Long> patientIdQuery = cq.subquery(Long.class);
        Root<PatientProgramLink> patientProgramRoot = patientIdQuery.from(PatientProgramLink.class);
        patientIdQuery.select(patientProgramRoot.<Long>get("patientId"));

        Subquery<Long> sprogPatientIdQuery = cq.subquery(Long.class);
        Root<PatientProgramLink> sprogPatientProgramRoot = sprogPatientIdQuery.from(PatientProgramLink.class);
        sprogPatientIdQuery.select(sprogPatientProgramRoot.<Long>get("patientId"));
        Predicate sprogPatientInPcc = null;


        if (StringUtils.isNotBlank(parentProgramId)) {

            patientIdQuery = getParentProgramPredicate(cb, communityId, patientProgramRoot, patientIdQuery, subordinateProgramId, statusId, consent, parentProgramId);

        } else if (StringUtils.isNotBlank(subordinateProgramId)) {

            patientIdQuery = getSubProgramPredicate(cb, communityId, patientProgramRoot, patientIdQuery, subordinateProgramId, consent, statusId);
            sprogPatientIdQuery = getSingleProgramPredicate(cb, communityId, sprogPatientProgramRoot, sprogPatientIdQuery, subordinateProgramId, consent, statusId);
            sprogPatientInPcc = pce.get("patientId").in(sprogPatientIdQuery);
        } else {

            List<Predicate> predicates = new ArrayList<Predicate>();

            if (StringUtils.isNotBlank(statusId)) {
                logger.info(statusId + " status selected");
                Join<PatientProgramLink, ProgramStatus> statusList = patientProgramRoot.join("programStatus");
                predicates.add(cb.equal(statusList.get("id"), Long.parseLong(statusId)));
            }

			if (StringUtils.isNotBlank(consent)) {

				logger.info(consent + " consent selected");
				Join<PatientProgramLink, Program> programList = patientProgramRoot.join("program");
				Join<Program, Program> parentList  = programList.join("parent", JoinType.LEFT);

				Predicate consentPredicate = cb.and(
						cb.or(
								isComplexParentPredicate(cb, parentList),
								isSingle(cb, programList),
								isSimpleParentPredicate(cb, programList)
						),
                        getConsentPredicate(cb, consent, patientProgramRoot)
				);
				predicates.add(consentPredicate);
			}

            predicates.add(getCommunityIdPredicate(communityId, cb, patientProgramRoot));

            patientIdQuery.where(toArray(predicates));
        }

        Predicate patientInPcc = pce.get("patientId").in(patientIdQuery);
        if(sprogPatientInPcc != null) {
            basePredicate = cb.and(basePredicate, cb.or(patientInPcc, sprogPatientInPcc));
        }
        else {
            basePredicate = cb.and(basePredicate, patientInPcc);
        }
        return basePredicate;
    }

	private Predicate getConsentPredicate(CriteriaBuilder cb, String consent, Root<PatientProgramLink> patientProgramRoot) {
		Long consentId = Long.parseLong(consent);
		if(consentId==Constants.PROGRAM_CONSENT_NOT_REQUIRED){
            return cb.notEqual(patientProgramRoot.get("program").get("consentRequired"), true);
        }
		return cb.equal(patientProgramRoot.get("consent"), consentId);
	}

	private Predicate getCommunityIdPredicate(long communityId, CriteriaBuilder cb, Root<PatientProgramLink> patientProgramRoot) {
		return cb.equal(patientProgramRoot.get("community").get("communityId"), communityId);
	}




    /**
     * @param cb
     * @param communityId
     * @param patientProgramRoot
     * @param patientIdQuery
     * @param subordinateProgramId
     * @param consent
     * @param statusId             @return patientIdQuery
     *                             Configures patientIdQuery with program ID, status ID(opt.), consent(opt.)
     *                             Note separate queries are required for simple and complex programs
     */
    private Subquery<Long> getSubProgramPredicate(CriteriaBuilder cb, long communityId, Root<PatientProgramLink> patientProgramRoot, Subquery<Long> patientIdQuery, String subordinateProgramId, String consent, String statusId) {

        logger.info(subordinateProgramId + " subordinate program selected");

        Join<PatientProgramLink, Program> programList = patientProgramRoot.join("program");
        Join<Program, Program> parentList = programList.join("parent", JoinType.LEFT);


        List<Predicate> complexPredicates = new ArrayList<Predicate>();
        List<Predicate> simplePredicates = new ArrayList<Predicate>();

        //complex subordinate or single
        complexPredicates.add(cb.and(
                cb.equal(patientProgramRoot.get("program").get("id"), Long.parseLong(subordinateProgramId)),
                isComplexParentPredicate(cb, parentList)
        ));

        Subquery<Long> simpleParentQuery = patientIdQuery.subquery(Long.class);
        Root<Program> simpleParentRoot = simpleParentQuery.from(Program.class);
        simpleParentQuery
                .select(simpleParentRoot.<Long>get("parent"))
                .where(cb.and(
                        cb.equal(simpleParentRoot.get("id"), Long.parseLong(subordinateProgramId)),
                        cb.equal(simpleParentRoot.get("communityId"), patientProgramRoot.get("community").<Long>get("communityId"))
                ));

        //simple subordinate
        simplePredicates.add(cb.and(
                cb.equal(programList.get("id"), simpleParentQuery),
                isSimpleParentPredicate(cb, programList)
        ));


        if (StringUtils.isNotBlank(statusId)) {

            logger.info(statusId + " status selected");

            complexPredicates.add(cb.equal(patientProgramRoot.get("programStatus").<Long>get("id"), Long.parseLong(statusId)));

            Predicate simpleSubordinatePredicate = getSimpleSubordinatePredicate(cb, patientProgramRoot, patientIdQuery, subordinateProgramId, statusId);
            simplePredicates.add(simpleSubordinatePredicate);

        }

        if (StringUtils.isNotBlank(consent)) {

			logger.info(consent + " consent selected");
			Predicate consentPredicate = getConsentPredicate(cb,consent, patientProgramRoot);
			complexPredicates.add(consentPredicate);
			simplePredicates.add(consentPredicate);
		}


        patientIdQuery.where(
                cb.and(
                        getCommunityIdPredicate(communityId, cb, patientProgramRoot),
                        cb.or(
                                cb.and(toArray(complexPredicates)),
                                cb.and(toArray(simplePredicates))
                        )
                )
        );

        return patientIdQuery;
    }


    /**
     * @param cb
     * @param communityId
     * @param patientProgramRoot
     * @param patientIdQuery
     * @param subordinateProgramId
     * @param consent
     * @param statusId             @return patientIdQuery
     *                             Configures patientIdQuery with program ID, status ID(opt.), consent(opt.)
     *                             Note separate queries are required for single programs
     */
    private Subquery<Long> getSingleProgramPredicate(CriteriaBuilder cb, long communityId, Root<PatientProgramLink> patientProgramRoot, Subquery<Long> patientIdQuery, String subordinateProgramId, String consent, String statusId) {

        logger.info(subordinateProgramId + "single program selected");

        Join<PatientProgramLink, Program> programList = patientProgramRoot.join("program");

        List<Predicate> singlePredicates = new ArrayList<Predicate>();

        //single
        singlePredicates.add(cb.and(
                cb.equal(patientProgramRoot.get("program").get("id"), Long.parseLong(subordinateProgramId)),
                isSingle(cb, programList)
        ));

        if (StringUtils.isNotBlank(statusId)) {

            logger.info(statusId + " status selected");

            singlePredicates.add(cb.equal(patientProgramRoot.get("programStatus").<Long>get("id"), Long.parseLong(statusId)));
        }

        if (StringUtils.isNotBlank(consent)) {

            logger.info(consent + " consent selected");
            Predicate consentPredicate = getConsentPredicate(cb,consent, patientProgramRoot);
            singlePredicates.add(consentPredicate);
        }

        patientIdQuery.where(
                cb.and(
                        getCommunityIdPredicate(communityId, cb, patientProgramRoot),
                        cb.and(toArray(singlePredicates))
                )
        );

        return patientIdQuery;
    }


    /**
     * @param cb
     * @param programJoin
     * @return Predicate to check if a program is neither parent program nor subordinate program)
     */
    private Predicate isSingle(CriteriaBuilder cb, Join<?, Program> programJoin) {
        return cb.and(
                cb.isNull(programJoin.get("parent")),
                cb.notEqual(programJoin.<Boolean>get("isAParent"), true)
        );
    }

    /**
     * @param cb
     * @param programJoin
     * @return Predicate to check if a program is simple parent
     */
    private Predicate isSimpleParentPredicate(CriteriaBuilder cb, Join<?, Program> programJoin) {
        return cb.and(
                cb.isTrue(programJoin.<Boolean>get("isAParent")),
                cb.isTrue(programJoin.<Boolean>get("simpleView"))
        );
    }

    /**
     * @param cb
     * @param programJoin
     * @return Predicate to check if a program is complex parent
     */
    private Predicate isComplexParentPredicate(CriteriaBuilder cb, Join<?, Program> programJoin) {
        return cb.and(
                cb.isTrue(programJoin.<Boolean>get("isAParent")),
                cb.isFalse(programJoin.<Boolean>get("simpleView"))
        );
    }

    private Predicate[] toArray(List<Predicate> predicates) {
        return predicates.toArray(new Predicate[]{});
    }


    /**
     * @param cb
     * @param communityId
     * @param patientProgramRoot
     * @param patientIdQuery
     * @param subordinateProgramId
     * @param statusId
     * @param consent
     * @param parentProgramId      @return patientIdQuery
     *                             <p>
     *                             Configures patientIdQuery with parent program ID, program ID(opt.), status ID(opt.), consent(opt.)
     *                             Note separate queries are required for simple and complex programs
     */
    private Subquery<Long> getParentProgramPredicate(CriteriaBuilder cb, long communityId, Root<PatientProgramLink> patientProgramRoot, Subquery<Long> patientIdQuery, String subordinateProgramId, String statusId, String consent, String parentProgramId) {
        logger.info(parentProgramId + " parent program selected");

        Join<PatientProgramLink, Program> programList = patientProgramRoot.join("program");
        Join<Program, Program> parentList = programList.join("parent", JoinType.LEFT);

        List<Predicate> complexPredicates = new ArrayList<Predicate>();
        List<Predicate> simplePredicates = new ArrayList<Predicate>();

        //complex parent
        complexPredicates.add(cb.and(
                cb.equal(parentList.get("id"), Long.parseLong(parentProgramId)),
                isComplexParentPredicate(cb, parentList)
        ));

        //simple parent
        simplePredicates.add(cb.and(
                cb.equal(programList.get("id"), Long.parseLong(parentProgramId)),
                isSimpleParentPredicate(cb, programList)
        ));

        if (StringUtils.isNotBlank(subordinateProgramId)) {

            logger.info(subordinateProgramId + " sub program selected");

            //complex parent
            complexPredicates.add(cb.equal(programList.get("id"), Long.parseLong(subordinateProgramId)));
            Predicate simpleSubordinatePredicate = getSimpleSubordinatePredicate(cb, patientProgramRoot, patientIdQuery, subordinateProgramId, statusId);


            simplePredicates.add(simpleSubordinatePredicate);
        }

        if (StringUtils.isNotBlank(statusId)) {
            logger.info(statusId + " status selected");
            Predicate statusPredicate = cb.equal(patientProgramRoot.get("programStatus").<Long>get("id"), Long.parseLong(statusId));
            complexPredicates.add(statusPredicate);
            if (StringUtils.isBlank(subordinateProgramId)) simplePredicates.add(statusPredicate);
        }

        if(StringUtils.isNotBlank(consent)){
			logger.info(consent + " consent selected");
            Predicate consentPredicate = getConsentPredicate(cb,consent, patientProgramRoot);
			complexPredicates.add(consentPredicate);
			simplePredicates.add(consentPredicate);
		}

        patientIdQuery.where(cb.and(
                getCommunityIdPredicate(communityId, cb, patientProgramRoot),
                cb.or(cb.and(toArray(complexPredicates)), cb.and(toArray(simplePredicates))))
        );

        return patientIdQuery;
    }


	private Predicate getSimpleSubordinatePredicate(CriteriaBuilder cb, Root<PatientProgramLink> patientProgramRoot, Subquery<Long> patientIdQuery, String subordinateProgramId, String statusId) {
		//simple parent
		//check if a row exists for selected subordinate]
		Subquery<PatientProgramLink> simpleSubordinateQuery = patientIdQuery.subquery(PatientProgramLink.class);
		Root<PatientProgramLink> subordinateRoot = simpleSubordinateQuery.from(PatientProgramLink.class);
		simpleSubordinateQuery.select(subordinateRoot).where(
                cb.equal(subordinateRoot.get("patientId"), patientProgramRoot.get("patientId")),
                cb.equal(subordinateRoot.get("community").get("communityId"), patientProgramRoot.get("community").get("communityId")),
                cb.equal(subordinateRoot.get("program").get("id"), Long.parseLong(subordinateProgramId))
        );

        Predicate predicate = cb.exists(simpleSubordinateQuery);
        if (StringUtils.isNotBlank(statusId) && Integer.parseInt(statusId) == 1) predicate = cb.not(predicate);
        return predicate;
    }

    //JIRA 860-1460 (1466)
    //Migrate EclipseLink Expression to pure JPA Criteria
    //private Expression getConsentedPatientExpression(Expression pce, Long userOrgId, long userId, long communityId) {
    private Predicate getConsentedPatientExpression(CriteriaBuilder cb, CriteriaQuery<PatientCommunityEnrollment> cq, Root<PatientCommunityEnrollment> pce,
                                                    Long userOrgId, long userId, long communityId) {

        logger.info("consent logic for patients ");
        // user must have opc permission AND not be in upc

//		ExpressionBuilder viewConsentedPatients = new ExpressionBuilder(ViewUserPatientConsent.class); 
//                ReportQuery subOPCQuery = new ReportQuery(ViewUserPatientConsent.class, viewConsentedPatients);
//                subOPCQuery.addAttribute("opcPatientId", viewConsentedPatients.get("viewUserPatientConsentPK").get("patientId"));
//                subOPCQuery.setSelectionCriteria(viewConsentedPatients.get("viewUserPatientConsentPK").get("userId").equal(userId));
//                Expression consentedPatients = pce.get("patientId").in(subOPCQuery);

        Subquery<Long> subOPCQuery = cq.subquery(Long.class);
        Root<ViewUserPatientConsent> viewConsentedPatients = subOPCQuery.from(ViewUserPatientConsent.class);
        subOPCQuery.select(viewConsentedPatients.get("viewUserPatientConsentPK").<Long>get("patientId"));
        subOPCQuery.where(cb.equal(viewConsentedPatients.get("viewUserPatientConsentPK").get("userId"), userId));
        Predicate consentedPatients = pce.get("patientId").in(subOPCQuery);

        return consentedPatients;
    }

    //JIRA 860-1460 (1466)
    //Migrate EclipseLink Expression to pure JPA Criteria
    //private Expression getSearchExpressionForEnrollmentApp(Expression baseExpression, Expression pce, Long userOrgId,
    private Predicate getSearchExpressionForEnrollmentApp(CriteriaBuilder cb, CriteriaQuery<PatientCommunityEnrollment> cq, Predicate basePredicate, Root<PatientCommunityEnrollment> pce,
                                                          Long userOrgId, long userId, boolean addtionalPatients, boolean isMangeConsentWithoutEnrollment,
                                                          boolean isAllOrgsSelfAssert, long communityId) {

        if (addtionalPatients && isAllOrgsSelfAssert) {

            logger.info("all orgs patient search for BHH and UHC ");
            // Similar to power user search so no expression needs to be added.
            // Since it for all patients of one community

        } else if (addtionalPatients && !isAllOrgsSelfAssert) {

            logger.info("Additional patient search, but only of CMO and dynamically consented orgs for BHH and UHC");
            logger.info("consent logic for patients ");
            // user must have opc permission AND not be in upc
            Predicate expConsentedPatients = getConsentedPatientExpression(cb, cq, pce, userOrgId, userId, communityId);
            basePredicate = cb.and(basePredicate, expConsentedPatients,
                    cb.equal(pce.get("community").get("communityId"), communityId));
        } else if (!isMangeConsentWithoutEnrollment) {

            logger.info("Initial load of patients for BHH model");
            logger.info("consent logic for patients ");
            // user must have opc permission AND not be in upc
            Predicate expConsentedPatients = getConsentedPatientExpression(cb, cq, pce, userOrgId, userId, communityId);
            Predicate expUnknownInactive = cb.and(
                    cb.equal(pce.get("orgId"), userOrgId));
            basePredicate = cb.and(basePredicate, cb.or(expConsentedPatients, expUnknownInactive));
        } else {
            logger.info("Initial load of patients for UHC model");

            logger.info("consent logic for patients ");
            // user must have opc permission AND not be in upc
            Predicate expConsentedPatients = getConsentedPatientExpression(cb, cq, pce, userOrgId, userId, communityId);
            basePredicate = cb.and(basePredicate, expConsentedPatients,
                    cb.equal(pce.get("community").get("communityId"), communityId));
        }

        return basePredicate;
    }

    //JIRA 860-1460 (1466)
    //Migrate EclipseLink Expression to pure JPA Criteria
    //private Expression getSearchExpressionForCareTeamApp(Expression pce, Long userOrgId, long userId,
    private Predicate getSearchExpressionForCareTeamApp(CriteriaBuilder cb, CriteriaQuery<PatientCommunityEnrollment> cq, Root<PatientCommunityEnrollment> pce,
                                                        Long userOrgId, long userId,
                                                        long communityId) {

        Predicate expConsentedPatients = getConsentedPatientExpression(cb, cq, pce, userOrgId, userId, communityId);

        Predicate basePredicate = cb.and(expConsentedPatients,
                //pce.get("status").in("Enrolled", "Assigned"),
                cb.equal(pce.get("community").get("communityId"), communityId));

        return basePredicate;
    }
    // vpl removed synchronized

    @Override
    public DuplicateCheckProcessResult checkForDuplicates(SimplePerson person) throws Exception {
        return patientManager.checkForDuplicates(person);
    }

    @Override
    public PatientResult addPatient(Person thePerson, long communityId) throws PreexistingEntityException, Exception {

        com.gsihealth.entity.PatientCommunityEnrollment patientEnrollment = thePerson.getPatientCommunityEnrollment();

        Community community = new Community(communityId);
        patientEnrollment.setCommunity(community);
        patientEnrollment.setStartDate(new java.util.Date());
        patientEnrollment.setCommunityId(patientEnrollment.getCommunity().getCommunityId());

        EntityManager em = null;
        EntityTransaction transaction = null;

        try {
            StopWatch stopWatch = new StopWatch();
            stopWatch.start();
            em = getEntityManager();
            transaction = em.getTransaction();
            transaction.begin();

            if (community != null) {
                community = em.getReference(community.getClass(), community.getCommunityId());
                patientEnrollment.setCommunity(community);
            }

            // update insurance info
            patientEnrollment.setPrimaryPayerPlanId(thePerson.getPrimaryPayerPlanId());
            patientEnrollment.setSecondaryPayerPlanId(thePerson.getSecondaryPayerPlanId());

            patientEnrollment.setPrimaryPayerClassId(thePerson.getPrimaryPayerClassId());
            patientEnrollment.setSecondaryPayerClassId(thePerson.getSecondaryPayerClassId());

            //Spira 6823 - set the objects too
            long primaryPayerPlanId = patientEnrollment.getPrimaryPayerPlanId();
            PayerPlan tempPayerPlan = payerPlansMap.get(primaryPayerPlanId);

            if (tempPayerPlan != null) {
                patientEnrollment.setPrimaryPayerPlan(tempPayerPlan);
            }

            long secondaryPayerPlanId = patientEnrollment.getSecondaryPayerPlanId();
            tempPayerPlan = payerPlansMap.get(secondaryPayerPlanId);

            if (tempPayerPlan != null) {
                patientEnrollment.setSecondaryPayerPlan(tempPayerPlan);
            }

            // load payer classes
            long primaryPayerClassId = patientEnrollment.getPrimaryPayerClassId();
            PayerClass primaryPayerClass = payerClassesMap.get(primaryPayerClassId);

            if (primaryPayerClass != null) {
                patientEnrollment.setPrimaryPayerClass(primaryPayerClass);
            }

            long secondaryPayerClassId = patientEnrollment.getSecondaryPayerClassId();
            PayerClass secondaryPayerClass = payerClassesMap.get(secondaryPayerClassId);

            if (secondaryPayerClass != null) {
                patientEnrollment.setSecondaryPayerClass(secondaryPayerClass);
            }

            // Program Health Home
            // patientEnrollment.setProgramHealthHomeId(thePerson.getProgramHealthHomeId());
            // patient user changed password date
            patientEnrollment.setLastPasswordChangeDate(thePerson.getPatientLastPasswordChanged());

            // set program dates to noon
            setProgramDatesToNoon(patientEnrollment);

            //set medicaid/medicare
            setMedicaidMedicare(thePerson, patientEnrollment, primaryPayerClass, secondaryPayerClass);

            Long nextPatientId = getNextVal(SequenceNames.PATIENT_ID_SEQ, em);

            // set the local id
            String localId = Long.toString(nextPatientId);
            thePerson.setLocalId(localId);

            logger.info("saving person to MDM: patientId=" + nextPatientId);

            // add to MDM
            String euid = null;
            euid = patientManager.addPatient(thePerson.getSimplePerson());
            String mpi = patientManager.getWhoAmI();
            logger.info("saving " + localId + "in comm " + communityId + " to mpi: " + mpi);
            if (StringUtils.equalsIgnoreCase(mpi, "mdm")) {
                logger.info("I'm mdm, lets do a completely useless 2nd save!!!");
                patientManager.addPatient(thePerson.getSimplePerson(), communityOid);
            }

            logger.info("finished regular: saving person to MDM: patientId=" + nextPatientId);
            stopWatch.stop();

            logger.info("[profiling], Adding patient to MDM, " + (stopWatch.getTime() / 1000.0) + ", secs");

            if (StringUtils.isBlank(euid)) {
                throw new Exception("addPatient: Unable to store patient in MDM. Please check server logs.");
            }

            stopWatch.reset();
            stopWatch.start();

            // update euid before commit
            patientEnrollment.setPatientEuid(euid);
            patientEnrollment.setPatientId(nextPatientId);

            em.persist(patientEnrollment);
            em.flush();
            transaction.commit();

            stopWatch.stop();

            logger.info("[profiling], Adding patient to connect.pce, " + (stopWatch.getTime() / 1000.0) + ", secs");

            logger.info("after persist  patient enrollment");
            PatientResult patientResult = new PatientResult(nextPatientId, euid);

            return patientResult;
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error adding patient: ", exc);
            if (transaction != null)
                logger.info("Trans active?: " + transaction.isActive());
            else
                logger.info("Trans null");

            if ((transaction != null) && transaction.isActive()) {
                transaction.rollback();
            }

            throw exc;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private void setMedicaidMedicare(Person thePerson, PatientCommunityEnrollment patientEnrollment,
                                     PayerClass primaryPayerClass, PayerClass secondaryPayerClass) {
        boolean primMedicaidFlag, primMedicareFlag, secMedicaidFlag, secMedicareFlag;
        primMedicaidFlag = primMedicareFlag = secMedicaidFlag = secMedicareFlag = false;
        if (primaryPayerClass != null) {
            primMedicaidFlag = primaryPayerClass.getName().equalsIgnoreCase(Constants.MEDICAID);
            primMedicareFlag = primaryPayerClass.getName().equalsIgnoreCase(Constants.MEDICARE);
        }
        if (secondaryPayerClass != null) {
            secMedicaidFlag = secondaryPayerClass.getName().equalsIgnoreCase(Constants.MEDICAID);
            secMedicareFlag = secondaryPayerClass.getName().equalsIgnoreCase(Constants.MEDICARE);
        }
        if (primMedicaidFlag) {
            thePerson.setMedicaidId(patientEnrollment.getPrimaryPayerMedicaidMedicareId());
        } else if (primMedicareFlag) {
            thePerson.setMedicareId(patientEnrollment.getPrimaryPayerMedicaidMedicareId());
        }

        if (!primMedicaidFlag && secMedicaidFlag) {
            thePerson.setMedicaidId(patientEnrollment.getSecondaryPayerMedicaidMedicareId());
        }

        if (!primMedicareFlag && secMedicareFlag) {
            thePerson.setMedicareId(patientEnrollment.getSecondaryPayerMedicaidMedicareId());
        }
    }

    private Long getNextVal(SequenceNames seqName, EntityManager em) throws Exception {
        Query q = em.createNativeQuery("SELECT nextval('" + seqName + "') as next_sequence");
        //JIRA 860-1460 (1465)
        return (Long) q.getSingleResult();
    }

    @Override
    public void edit(PatientCommunityEnrollment patientEnrollment) throws NonexistentEntityException, Exception {

        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            patientEnrollment = em.merge(patientEnrollment);

            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                long id = patientEnrollment.getPatientId();
                if (findPatientEnrollment(id) == null) {
                    throw new NonexistentEntityException("The patientEnrollment with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void destroy(long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PatientCommunityEnrollment patientEnrollment;
            try {
                patientEnrollment = em.getReference(PatientCommunityEnrollment.class, id);
                patientEnrollment.getPatientId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The patientEnrollment with id " + id + " no longer exists.",
                        enfe);
            }
            Community community = patientEnrollment.getCommunity();
            if (community != null) {
                community.getPatientCommunityEnrollmentList().remove(patientEnrollment);
                community = em.merge(community);
            }
            em.remove(patientEnrollment);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PatientCommunityEnrollment findPatientEnrollment(String euid, Long communityId) throws NoResultException {

        EntityManager em = getEntityManager();

        try {
            Query q = em.createNamedQuery("PatientCommunityEnrollment.findByEuidAndCommunity")
                    .setParameter("euid", euid).setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<PatientCommunityEnrollment> list = q.getResultList();

            PatientCommunityEnrollment tempPatientEnrollment = null;

            if (!list.isEmpty()) {
                tempPatientEnrollment = list.get(0);
            }

            if (tempPatientEnrollment != null) {
                assignPayerPlanAndPayerClass(tempPatientEnrollment);
            }

            return tempPatientEnrollment;
        } finally {
            em.close();
        }
    }

    @Override
    public PatientCommunityEnrollment findPatientEnrollment(String euid) throws NoResultException {

        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery(
                    "select object(o) from PatientCommunityEnrollment as o where o.patientEuid = :theEuid");
            q.setParameter("theEuid", euid);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<PatientCommunityEnrollment> list = q.getResultList();

            PatientCommunityEnrollment tempPatientEnrollment = null;

            if (!list.isEmpty()) {
                tempPatientEnrollment = list.get(0);
            }

            if (tempPatientEnrollment != null) {
                assignPayerPlanAndPayerClass(tempPatientEnrollment);
            }

            return tempPatientEnrollment;
        } finally {
            em.close();
        }
    }

    protected int getPatientEnrollmentCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from PatientCommunityEnrollment as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @Override
    public Long findPatientId(String euid) throws NoResultException {

        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery(
                    "select object(o) from PatientCommunityEnrollment as o where o.patientEuid = :theEuid");
            q.setParameter("theEuid", euid);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            PatientCommunityEnrollment tempPatientEnrollment = (PatientCommunityEnrollment) q.getSingleResult();

            return tempPatientEnrollment.getPatientId();
        } finally {
            em.close();
        }
    }

    @Override
    public void updatePatient(Person thePerson) throws Exception {
        PatientCommunityEnrollment patientEnrollment = thePerson.getPatientCommunityEnrollment();

        EntityManager em = null;
        EntityTransaction transaction = null;

        try {
            em = getEntityManager();
            transaction = em.getTransaction();
            transaction.begin();

            // set local id = patientId
            String localId = Long.toString(patientEnrollment.getPatientId());
            thePerson.setLocalId(localId);

            logger.info("updating person to MDM: patientId=" + localId);

            // update to MDM
            String euid = patientManager.addPatient(thePerson.getSimplePerson());

            logger.info("Getting community id for patient...");
            long communityId = patientEnrollment.getCommunity().getCommunityId();
            logger.info("Reading BHIX configs for community id=" + communityId);

            PayerClass primaryPayerClass = payerClassesMap.get(thePerson.getPrimaryPayerClassId());
            PayerClass secondaryPayerClass = payerClassesMap.get(thePerson.getSecondaryPayerClassId());

            //set medicaid/medicare
            setMedicaidMedicare(thePerson, patientEnrollment, primaryPayerClass, secondaryPayerClass);

            // add to MDM with SW Brookyln OID TODO - does this need to be done
            // twice?
            patientManager.addPatient(thePerson.getSimplePerson(), communityOid);

            if (StringUtils.isBlank(euid)) {
                throw new Exception("addPatient: Unable to store patient in MDM. Please check server logs.");
            }

            patientEnrollment.setStartDate(new java.util.Date());

            // update insurance info
            patientEnrollment.setPrimaryPayerPlanId(thePerson.getPrimaryPayerPlanId());
            //Spira 6763
            //A new ManyToOne join has been added for the same column along with the direct id setter
            //this is triggering an insert into the related tables. 
            //Setting to null just before update to avoid this
            patientEnrollment.setPrimaryPayerPlan(null);
            patientEnrollment.setSecondaryPayerPlanId(thePerson.getSecondaryPayerPlanId());
            patientEnrollment.setSecondaryPayerPlan(null);

            patientEnrollment.setPrimaryPayerClassId(thePerson.getPrimaryPayerClassId());
            patientEnrollment.setPrimaryPayerClass(null);
            patientEnrollment.setSecondaryPayerClassId(thePerson.getSecondaryPayerClassId());
            patientEnrollment.setSecondaryPayerClass(null);

            // Program Health Home update
            // patientEnrollment.setProgramHealthHomeId(thePerson.getProgramHealthHomeId());
            // set patient last password changed date
            patientEnrollment.setLastPasswordChangeDate(thePerson.getPatientLastPasswordChanged());

            logger.info("updating patient enrollment: PatientUserName DAO=");

            // set program dates to noon
            setProgramDatesToNoon(patientEnrollment);

            // update euid
            logger.info("updating patient enrollment: patientId=" + localId);

            em.merge(patientEnrollment);
            em.flush();

            transaction.commit();

            logger.info("  Merge patient enrollment   ");
        } catch (Exception exc) {
            if ((transaction != null) && transaction.isActive()) {
                transaction.rollback();
            }

            throw exc;
        } finally {
            if (em != null) {
                em.close();
            }
        }

    }

    @Override
    public void updatePatientEnrollment(PatientCommunityEnrollment patientEnrollment) {

        EntityManager em = null;
        EntityTransaction transaction = null;

        try {
            em = getEntityManager();
            transaction = em.getTransaction();
            transaction.begin();

            // set program dates to noon
            setProgramDatesToNoon(patientEnrollment);

            // update euid
            logger.info("updating patient enrollment: patientId=" + patientEnrollment.getPatientId()
                    + ":PatientUserName(:" + patientEnrollment.getPatientLoginId());
            em.merge(patientEnrollment);
            em.flush();

            transaction.commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public int getTotalCountForCandidates(long organizationId) {
        String queryString = "select count(o) from PatientCommunityEnrollment as o"
                + " where o.status in ('Pending', 'Inactive', '') and (o.orgId = " + organizationId + ")";

        return getTotalCount(queryString);
    }
    // DASHBOARD-609 TODO this needs to be fixed for Patient Consent

    @Override
    public Map<String, PatientCommunityEnrollment> findCandidates(long organizationId, int pageNumber, int pageSize) {
        String queryString = "select object(o) from PatientCommunityEnrollment as o"
                + " where o.status in ('Pending', 'Inactive', '') and (o.orgId = " + organizationId + ")";

        return findGenericCandidateEnrolledPatient(queryString, pageNumber, pageSize, false);
    }

    @Override
    public List<String> findAllPatientEuids() {
        List<String> results = null;

        EntityManager em = getEntityManager();

        try {
            String queryString = "select o.patientEuid from PatientCommunityEnrollment as o";

            Query q = em.createQuery(queryString);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            results = q.getResultList();
        } finally {
            em.close();
        }

        return results;
    }

    // @Override
    // public Map<String, PatientCommunityEnrollment> findEnrolledPatients(long
    // userId, int pageNumber, int pageSize) {
    // //DASHBOARD-477 - keep only consent status=DENY in userPatientConsent DB
    // table
    // logger.log(Level.FINE, "PatientEnrollmentDAOImpl findEnrolledPatients
    // with userid and pageNumbers..");
    // String queryString =
    // PatientConsentUtils.buildUserPatientConsentQuery(userId);
    //
    // nativeSQL = true;
    // return findGenericCandidateEnrolledPatient(queryString, pageNumber,
    // pageSize, nativeSQL);
    // }
    protected Map<String, PatientCommunityEnrollment> findGenericCandidateEnrolledPatient(String queryString,
                                                                                          int pageNumber, int pageSize, boolean nativeSQL) {
        EntityManager em = getEntityManager();

        try {

            Map<String, PatientCommunityEnrollment> map = new HashMap<String, PatientCommunityEnrollment>();

            Query q = PatientConsentUtils.createNativeOrJPQLQuery(em, queryString, PatientCommunityEnrollment.class);

            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);

            q.setFirstResult(startRecordNumber);
            q.setMaxResults(pageSize);

            List<PatientCommunityEnrollment> patientEnrollments = q.getResultList();

            // load insurance info: payer plan and payer class
            assignPayerPlanAndPayerClass(patientEnrollments);

            // build the map
            for (PatientCommunityEnrollment tempPatientEnrollment : patientEnrollments) {
                String euid = tempPatientEnrollment.getPatientEuid();
                map.put(euid, tempPatientEnrollment);
            }

            return map;
        } finally {
            em.close();
        }
    }

    protected Map<String, PatientCommunityEnrollment> findGenericCandidateEnrolledPatient(String queryString) {
        EntityManager em = getEntityManager();

        try {

            Map<String, PatientCommunityEnrollment> map = new HashMap<String, PatientCommunityEnrollment>();
            Query q = PatientConsentUtils.createNativeOrJPQLQuery(em, queryString, PatientCommunityEnrollment.class);

            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<PatientCommunityEnrollment> patientEnrollments = q.getResultList();

            // load insurance info: payer plan and payer class
            assignPayerPlanAndPayerClass(patientEnrollments);

            for (PatientCommunityEnrollment tempPatientEnrollment : patientEnrollments) {
                String euid = tempPatientEnrollment.getPatientEuid();
                map.put(euid, tempPatientEnrollment);
            }

            return map;
        } finally {
            em.close();
        }
    }

    @Override
    public int getTotalCountFilterPatientsWithoutDemographics(SearchCriteria searchCriteria, List<String> statuses) {

        String queryString = getQueryStringFilterPatientsWithoutDemographics(searchCriteria, statuses, true);

        int count = getTotalCount(queryString);

        return count;
    }

    @Override
    public int getTotalCountFilterPatientsWithoutDemographics(SearchCriteria searchCriteria, List<String> statuses,
                                                              long currentUserOrgId) {

        String queryString = getQueryStringFilterPatientsWithoutDemographics(searchCriteria, statuses, currentUserOrgId,
                true);

        int count = getTotalCount(queryString);
        return count;
    }

    @Override
    public Map<String, PatientCommunityEnrollment> filterPatientsWithoutDemographics(SearchCriteria searchCriteria,
                                                                                     List<String> statuses, long currentUserOrgId) {

        String queryString = getQueryStringFilterPatientsWithoutDemographics(searchCriteria, statuses, currentUserOrgId,
                false);

        return findGenericCandidateEnrolledPatient(queryString);
    }

    @Override
    public int getTotalCountFilterEnrolledPatientsWithoutDemographics(long userId, SearchCriteria searchCriteria,
                                                                      List<String> statuses) {
        String queryString = getQueryStringFilterEnrolledPatientsWithoutDemographics(userId, searchCriteria, statuses,
                true);

        int count = getTotalCount(queryString);

        return count;
    }

    @Override
    public Map<String, PatientCommunityEnrollment> filterEnrolledPatientsWithoutDemographics(long userId,
                                                                                             SearchCriteria searchCriteria, List<String> statuses) {
        String queryString = getQueryStringFilterEnrolledPatientsWithoutDemographics(userId, searchCriteria, statuses,
                false);

        Map<String, PatientCommunityEnrollment> patients = findGenericCandidateEnrolledPatient(queryString);

        return patients;
    }

    protected String getQueryStringFilterEnrolledPatientsWithoutDemographics(long userId, SearchCriteria searchCriteria,
                                                                             List<String> statuses, boolean countFlag) throws NumberFormatException {
        String careteamIdStr = searchCriteria.getCareteamId();

        String inClauseForStatuses = DaoUtils.buildInClauseQuoted(statuses);

        StringBuilder queryString = new StringBuilder();

        if (countFlag) {
            queryString.append("select count(o) from PatientCommunityEnrollment as o");
        } else {
            queryString.append("select object(o) from PatientCommunityEnrollment as o");
        }

        // check to see if we need to add join for careteam id
        if (StringUtils.isNotBlank(careteamIdStr)) {
            queryString.append(", PatientCommunityCareteam as careteam");
        }
        queryString.append(" where o.status in " + inClauseForStatuses);

        // filter for consent with subquery for to reduce by UserPatientConsent
        // table by userid
        queryString.append(" and o.patientId IN (select userPatientConsent.userPatientConsentPK.patientId"
                + " from UserPatientConsent as userPatientConsent where");
        queryString.append(" userPatientConsent.userPatientConsentPK.userId=" + userId);
        queryString.append(" and userPatientConsent.consentStatus='PERMIT')");

        // enrollment status
        String enrollmentStatus = searchCriteria.getEnrollmentStatus();
        if (StringUtils.isNotBlank(enrollmentStatus)) {
            queryString.append(" and o.status = " + DaoUtils.quote(enrollmentStatus));
        }

        // PLCS
        String programLevelConsentStatus = searchCriteria.getProgramLevelConsentStatus();
        if (StringUtils.isNotBlank(programLevelConsentStatus)) {
            queryString.append(" and o.programLevelConsentStatus = " + DaoUtils.quote(programLevelConsentStatus));
        }

        // // program name
        // String programName = searchCriteria.getProgramName();
        // if (StringUtils.isNotBlank(programName)) {
        // queryString.append(" and o.programName = " +
        // DaoUtils.quote(programName));
        // }
        // medicaid
        // remove extra whitespace first
        String mediCaidCareId = StringUtils.remove(searchCriteria.getMediCaidCareId(), " ");

        if (StringUtils.isNotBlank(mediCaidCareId)) {
            queryString.append(" and (o.primaryPayerMedicaidMedicareId = " + DaoUtils.quote(mediCaidCareId));
            queryString.append(" or o.secondaryPayerMedicaidMedicareId = " + DaoUtils.quote(mediCaidCareId) + ")");
        }

        // search organization
        long searchOrganizationId = searchCriteria.getOrganizationId();
        if (searchOrganizationId > 0) {
            queryString.append(" and o.orgId = " + searchOrganizationId);
        }

        // care team
        if (StringUtils.isNotBlank(careteamIdStr)) {
            long careteamId = Integer.parseInt(careteamIdStr);

            queryString.append(" and o.patientId=careteam.patientCommunityCareteamPK.patientId"
                    + " and careteam.patientCommunityCareteamPK.communityCareteamId=" + careteamId);
        }

        // patient id
        String patientID = searchCriteria.getPatientID();
        if (StringUtils.isNotBlank(patientID)) {
            patientID = DaoUtils.stripLeadingZeros(patientID);
            queryString.append(" and o.patientId = " + patientID);
        }

        // Health Home id
        // String healthHomeId = searchCriteria.getHealthHomeId();
        // if (StringUtils.isNotBlank(healthHomeId)) {
        // queryString.append(" and o.programHealthHomeId = " + healthHomeId + "
        // ");
        // }
        // //Active/Inactive patient messaging
        String isActive = searchCriteria.getActivePatient();
        if (StringUtils.isNotBlank(isActive)) {
            queryString.append(" and o.patientUserActive = " + DaoUtils.quote(isActive));
        }

        return queryString.toString();
    }

    protected String getQueryStringFilterEnrolledPatientsWithoutDemographics(SearchCriteria searchCriteria,
                                                                             List<String> statuses, boolean countFlag) throws NumberFormatException {
        String careteamIdStr = searchCriteria.getCareteamId();

        String inClauseForStatuses = DaoUtils.buildInClauseQuoted(statuses);

        StringBuilder queryString = new StringBuilder();

        if (countFlag) {
            queryString.append("select count(o) from PatientCommunityEnrollment as o");
        } else {
            queryString.append("select object(o) from PatientCommunityEnrollment as o");
        }

        // check to see if we need to add join for careteam id
        if (StringUtils.isNotBlank(careteamIdStr)) {
            queryString.append(", PatientCommunityCareteam as careteam");
        }
        queryString.append(" where o.status in " + inClauseForStatuses);

        // enrollment status
        String enrollmentStatus = searchCriteria.getEnrollmentStatus();
        if (StringUtils.isNotBlank(enrollmentStatus)) {
            queryString.append(" and o.status = " + DaoUtils.quote(enrollmentStatus));
        }

        // PLCS
        String programLevelConsentStatus = searchCriteria.getProgramLevelConsentStatus();
        if (StringUtils.isNotBlank(programLevelConsentStatus)) {
            queryString.append(" and o.programLevelConsentStatus = " + DaoUtils.quote(programLevelConsentStatus));
        }

        // program name
        // String programName = searchCriteria.getProgramName();
        // if (StringUtils.isNotBlank(programName)) {
        // queryString.append(" and o.programName = " +
        // DaoUtils.quote(programName));
        // }
        // medicaid
        // remove extra whitespace first
        String mediCaidCareId = StringUtils.remove(searchCriteria.getMediCaidCareId(), " ");

        if (StringUtils.isNotBlank(mediCaidCareId)) {
            queryString.append(" and (o.primaryPayerMedicaidMedicareId = " + DaoUtils.quote(mediCaidCareId));
            queryString.append(" or o.secondaryPayerMedicaidMedicareId = " + DaoUtils.quote(mediCaidCareId) + ")");
        }

        // search organization
        long searchOrganizationId = searchCriteria.getOrganizationId();
        if (searchOrganizationId > 0) {
            queryString.append(" and o.orgId = " + searchOrganizationId);
        }

        // care team
        if (StringUtils.isNotBlank(careteamIdStr)) {
            long careteamId = Integer.parseInt(careteamIdStr);

            queryString.append(" and o.patientId=careteam.patientCommunityCareteamPK.patientId"
                    + " and careteam.patientCommunityCareteamPK.communityCareteamId=" + careteamId);
        }

        // patient id
        String patientID = searchCriteria.getPatientID();
        if (StringUtils.isNotBlank(patientID)) {
            patientID = DaoUtils.stripLeadingZeros(patientID);
            queryString.append(" and o.patientId = " + patientID);
        }

        // Health Home id
        // String healthHomeId = searchCriteria.getHealthHomeId();
        // if (StringUtils.isNotBlank(healthHomeId)) {
        // queryString.append(" and o.programHealthHomeId = " + healthHomeId + "
        // ");
        // }
        // Active/Inactive
        String isActive = searchCriteria.getActivePatient();
        if (StringUtils.isNotBlank(isActive)) {
            queryString.append(" and o.patientUserActive = " + DaoUtils.quote(isActive));
        }

        return queryString.toString();
    }

    @Override
    public int getTotalCountForEnrolledPatients(long userId) {
		/*
		 * String queryString =
		 * "select count(patientEnrollment) from PatientCommunityEnrollment as patientEnrollment, UserPatientConsent as userPatientConsent"
		 * + " where patientEnrollment.status in ('Enrolled', 'Assigned')" +
		 * " and patientEnrollment.patientEnrollmentPK.patientId=userPatientConsent.userPatientConsentPK.patientId"
		 * + " and userPatientConsent.userPatientConsentPK.userId=" + userId +
		 * " and userPatientConsent.consentStatus='PERMIT'";
		 */
        // DASHIBOARD-477
		/*
		 * String nativeQueryString =
		 * "SELECT COUNT(t0.PATIENT_ID) FROM patient_community_enrollment t0," +
		 * "(select patient_id from user_patient_consent where user_id="+ userId
		 * + " and consent_status ='PERMIT') AS t1" +
		 * " WHERE t0.STATUS IN ('Enrolled', 'Assigned') AND (t0.PATIENT_ID = t1.patient_id)"
		 * ;
		 */

        // DASHBOARD-609
        String nativeQueryString = PatientConsentUtils.buildNativeSQLForEnrolledConsentedPatientsCount(userId);

        boolean nativeSQL = true;

        return getTotalCount(nativeQueryString, nativeSQL);
    }

    protected int getTotalCount(String queryString, boolean nativeSQL) {

        if (nativeSQL) {
            EntityManager em = getEntityManager();

            try {
                Query q = em.createNativeQuery(queryString);
                //q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

                // even getSingleResult is still put in a List format. ex: [351]
                List<Object> countResult = (List) q.getSingleResult();
                // need to convert because countResult is a Long
                int count = Integer.parseInt(countResult.get(0).toString());
                logger.info("EnrolledPatientCount: " + count);
                return count;
            } finally {
                em.close();
            }
        } else {
            return getTotalCount(queryString);
        }
    }

    protected int getTotalCount(String queryString) {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery(queryString);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int count = ((Long) q.getSingleResult()).intValue();

            return count;
        } finally {
            em.close();
        }
    }

    protected String getQueryStringFilterPatientsWithoutDemographics(SearchCriteria searchCriteria,
                                                                     List<String> statuses, long currentUserOrgId, boolean countFlag) {

        String inClauseForStatuses = DaoUtils.buildInClauseQuoted(statuses);

        StringBuilder queryString = new StringBuilder();

        if (countFlag) {
            queryString.append("select count(o) from PatientCommunityEnrollment as o");
        } else {
            queryString.append("select object(o) from PatientCommunityEnrollment as o");
        }

        queryString.append(" where o.status in " + inClauseForStatuses);

        // enrollment status
        String enrollmentStatus = searchCriteria.getEnrollmentStatus();
        if (StringUtils.isNotBlank(enrollmentStatus)) {
            queryString.append(" and o.status = " + DaoUtils.quote(enrollmentStatus));
        }

        // PLCS
        String programLevelConsentStatus = searchCriteria.getProgramLevelConsentStatus();
        if (StringUtils.isNotBlank(programLevelConsentStatus)) {
            queryString.append(" and o.programLevelConsentStatus = " + DaoUtils.quote(programLevelConsentStatus));
        }

        // program name
        // String programName = searchCriteria.getProgramName();
        // if (StringUtils.isNotBlank(programName)) {
        // queryString.append(" and o.programName = " +
        // DaoUtils.quote(programName));
        // }
        // remove extra whitespace
        String mediCaidCareId = StringUtils.remove(searchCriteria.getMediCaidCareId(), " ");

        if (StringUtils.isNotBlank(mediCaidCareId)) {
            queryString.append(" and (o.primaryPayerMedicaidMedicareId = " + DaoUtils.quote(mediCaidCareId));
            queryString.append(" or o.secondaryPayerMedicaidMedicareId = " + DaoUtils.quote(mediCaidCareId) + ")");
        }

        // patient id
        String patientIdStr = searchCriteria.getPatientID();

        if (StringUtils.isNotBlank(patientIdStr)) {
            // remove leading zeroes
            String patientId = DaoUtils.stripLeadingZeros(patientIdStr);

            queryString.append(" and o.patientId = " + patientId);
        }

        // Health Home id
        // String healthHomeId = searchCriteria.getHealthHomeId();
        // if (StringUtils.isNotBlank(healthHomeId)) {
        // queryString.append(" and o.programHealthHomeId = " + healthHomeId + "
        // ");
        // }
        // only for this organization
        queryString.append("and (o.orgId = " + currentUserOrgId + ")");

        return queryString.toString();
    }

    protected String getQueryStringFilterPatientsWithoutDemographics(SearchCriteria searchCriteria,
                                                                     List<String> statuses, boolean countFlag) {

        String inClauseForStatuses = DaoUtils.buildInClauseQuoted(statuses);

        StringBuilder queryString = new StringBuilder();

        if (countFlag) {
            queryString.append("select count(o) from PatientCommunityEnrollment as o");
        } else {
            queryString.append("select object(o) from PatientCommunityEnrollment as o");
        }

        queryString.append(" where o.status in " + inClauseForStatuses);

        // enrollment status
        String enrollmentStatus = searchCriteria.getEnrollmentStatus();
        if (StringUtils.isNotBlank(enrollmentStatus)) {
            queryString.append(" and o.status = " + DaoUtils.quote(enrollmentStatus));
        }

        // PLCS
        String programLevelConsentStatus = searchCriteria.getProgramLevelConsentStatus();
        if (StringUtils.isNotBlank(programLevelConsentStatus)) {
            queryString.append(" and o.programLevelConsentStatus = " + DaoUtils.quote(programLevelConsentStatus));
        }

        // program name
        // String programName = searchCriteria.getProgramName();
        // if (StringUtils.isNotBlank(programName)) {
        // queryString.append(" and o.programName = " +
        // DaoUtils.quote(programName));
        // }
        // remove extra white space
        String mediCaidCareId = StringUtils.remove(searchCriteria.getMediCaidCareId(), " ");

        if (StringUtils.isNotBlank(mediCaidCareId)) {
            queryString.append(" and (o.primaryPayerMedicaidMedicareId = " + DaoUtils.quote(mediCaidCareId));
            queryString.append(" or o.secondaryPayerMedicaidMedicareId = " + DaoUtils.quote(mediCaidCareId) + ")");
        }
        String patientID = searchCriteria.getPatientID();

        if (StringUtils.isNotBlank(patientID)) {
            patientID = DaoUtils.stripLeadingZeros(patientID);
            queryString.append(" and o.patientId = " + DaoUtils.quote(patientID));
        }

        // Health Home id
        // String healthHomeId = searchCriteria.getHealthHomeId();
        // if (StringUtils.isNotBlank(healthHomeId)) {
        // queryString.append(" and o.programHealthHomeId = " + healthHomeId + "
        // ");
        // }
        return queryString.toString();
    }

    public PatientCommunityEnrollment findPatientEnrollment(long patientId) throws NoResultException {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery(
                    "select object(o) from PatientCommunityEnrollment as o where o.patientId = :thePatientId");
            q.setParameter("thePatientId", patientId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<PatientCommunityEnrollment> list = q.getResultList();

            PatientCommunityEnrollment tempPatientEnrollment = null;
            if (!list.isEmpty()) {
                tempPatientEnrollment = list.get(0);
            }

            assignPayerPlanAndPayerClass(tempPatientEnrollment);

            return tempPatientEnrollment;
        } finally {
            em.close();
        }
    }

    @Override
    public PatientCommunityEnrollment findPatientEnrollment(long patientId, Long communityId) throws NoResultException {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery(
                    "select object(o) from PatientCommunityEnrollment as o where o.patientId = :thePatientId"
                            + " AND o.communityId = :communityId");
            q.setParameter("thePatientId", patientId);
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<PatientCommunityEnrollment> list = q.getResultList();

            PatientCommunityEnrollment tempPatientEnrollment = null;
            if (!list.isEmpty()) {
                tempPatientEnrollment = list.get(0);
            }

            assignPayerPlanAndPayerClass(tempPatientEnrollment);

            return tempPatientEnrollment;
        } finally {
            em.close();
        }
    }

    // for getting total counts for a power user irrespective of an organization
    @Override
    public int getTotalCountForCandidates() {
        String queryString = "select count(o) from PatientCommunityEnrollment as o"
                + " where o.status in ('Pending', 'Inactive', '')";

        return getTotalCount(queryString);
    }

    // for getting total candidate patients for a power user irrespective of an
    // organization
    @Override
    public Map<String, PatientCommunityEnrollment> findCandidates(int pageNumber, int pageSize) {
        String queryString = "select object(o) from PatientCommunityEnrollment as o"
                + " where o.status in ('Pending', 'Inactive', '')";

        return findGenericCandidateEnrolledPatient(queryString, pageNumber, pageSize, false);
    }

    // for searching among total candidate patients for a power user
    // irrespective of an organization
    @Override
    public Map<String, PatientCommunityEnrollment> filterPatientsWithoutDemographics(SearchCriteria searchCriteria,
                                                                                     List<String> statuses) {
        String queryString = getQueryStringFilterPatientsWithoutDemographics(searchCriteria, statuses, false);

        return findGenericCandidateEnrolledPatient(queryString);
    }

    @Override
    public int getTotalCountForEnrolledPatients() {
        String queryString = "select count(patientEnrollment) from PatientCommunityEnrollment as patientEnrollment"
                + " where patientEnrollment.status in ('Enrolled', 'Assigned')";

        return getTotalCount(queryString);
    }

    @Override
    public int getTotalCountFilterEnrolledPatientsWithoutDemographics(SearchCriteria searchCriteria,
                                                                      List<String> statuses) {
        String queryString = getQueryStringFilterEnrolledPatientsWithoutDemographics(searchCriteria, statuses, true);

        int count = getTotalCount(queryString);
        return count;
    }

    @Override
    public Map<String, PatientCommunityEnrollment> findEnrolledPatients(int pageNumber, int pageSize) {
        String queryString = "select object(patientEnrollment) from PatientCommunityEnrollment as patientEnrollment"
                + " where patientEnrollment.status in ('Enrolled', 'Assigned')";

        return findGenericCandidateEnrolledPatient(queryString, pageNumber, pageSize, false);
    }

    @Override
    public Map<String, PatientCommunityEnrollment> filterEnrolledPatientsWithoutDemographics(
            SearchCriteria searchCriteria, List<String> statuses) {
        String queryString = getQueryStringFilterEnrolledPatientsWithoutDemographics(searchCriteria, statuses, false);

        Map<String, PatientCommunityEnrollment> patients = findGenericCandidateEnrolledPatient(queryString);

        return patients;
    }

    private PayerClass getPayerClassEntity(long id) {

        EntityManager em = getEntityManager();
        PayerClass payerClass = null;
        try {
            Query q = em.createQuery("select object(o) from PayerClass as o where o.payerClassPK.id=:id");
            q.setParameter("id", id);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            payerClass = (PayerClass) q.getSingleResult();
        } finally {
            em.close();
        }

        return payerClass;
    }

    /**
     * Load payer plans and payer classes into memory cache
     */
    private void loadPayerPlansAndPayerClassesInCache() {

        payerPlansMap = new HashMap<Long, PayerPlan>();
        payerClassesMap = new HashMap<Long, PayerClass>();

        EntityManager em = getEntityManager();

        try {
            // run the query
            Query payerPlansQuery = em.createQuery(
                    "select object(o) from PayerPlan as o" + " WHERE o.payerPlanPK.communityId = :communityId");
            payerPlansQuery.setParameter("communityId", this.communityId);
            payerPlansQuery.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            List<PayerPlan> payerPlans = (List<PayerPlan>) payerPlansQuery.getResultList();

            Query payerClassesQuery = em.createQuery(
                    "select object(o) from PayerClass as o" + " WHERE o.payerClassPK.communityId = :communityId");
            payerClassesQuery.setParameter("communityId", this.communityId);
            payerClassesQuery.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            List<PayerClass> payerClasses = (List<PayerClass>) payerClassesQuery.getResultList();

            // now put them in the cache
            // key: communityId + DELIMITER + payerId
            // payer plans
            for (PayerPlan tempPayerPlan : payerPlans) {
                PayerPlanPK payerPlanPK = tempPayerPlan.getPayerPlanPK();
                Long planId = payerPlanPK.getId();

                payerPlansMap.put(planId, tempPayerPlan);
            }

            // payer classes
            for (PayerClass tempPayerClass : payerClasses) {
                PayerClassPK payerClassPK = tempPayerClass.getPayerClassPK();
                Long classId = payerClassPK.getId();

                payerClassesMap.put(classId, tempPayerClass);
            }

        } finally {
            em.close();
        }
    }

    private void assignPayerPlanAndPayerClass(List<PatientCommunityEnrollment> patientEnrollments) {

        for (PatientCommunityEnrollment tempPatientEnrollment : patientEnrollments) {
            assignPayerPlanAndPayerClass(tempPatientEnrollment);
        }
    }

    private void assignPayerPlanAndPayerClass(PatientCommunityEnrollment tempPatientEnrollment) {
        // load payer plans
        long primaryPayerPlanId = tempPatientEnrollment.getPrimaryPayerPlanId();
        PayerPlan tempPayerPlan = payerPlansMap.get(primaryPayerPlanId);

        if (tempPayerPlan != null) {
            tempPatientEnrollment.setPrimaryPayerPlan(tempPayerPlan);
        }

        long secondaryPayerPlanId = tempPatientEnrollment.getSecondaryPayerPlanId();
        tempPayerPlan = payerPlansMap.get(secondaryPayerPlanId);

        if (tempPayerPlan != null) {
            tempPatientEnrollment.setSecondaryPayerPlan(tempPayerPlan);
        }

        // load payer classes
        long primaryPayerClassId = tempPatientEnrollment.getPrimaryPayerClassId();
        PayerClass tempPayerClass = payerClassesMap.get(primaryPayerClassId);

        if (tempPayerClass != null) {
            tempPatientEnrollment.setPrimaryPayerClass(tempPayerClass);
        }

        long secondaryPayerClassId = tempPatientEnrollment.getSecondaryPayerClassId();
        tempPayerClass = payerClassesMap.get(secondaryPayerClassId);

        if (tempPayerClass != null) {
            tempPatientEnrollment.setSecondaryPayerClass(tempPayerClass);
        }
    }

    public List<PatientCommunityEnrollment> getPrimaryPatientMedicaidMedicare(String medicaidMedicareId,
                                                                              long primaryPayerClassId, long communityId) {

        EntityManager em = getEntityManager();
        Query query = null;
        List<PatientCommunityEnrollment> list = null;

        try {
            query = em.createQuery(
                    "select object(o) from PatientCommunityEnrollment as o where o.primaryPayerMedicaidMedicareId = :primaryPayerMedicaidMedicareId and o.primaryPayerClassId = :primaryPayerClassId "
                            + " and o.communityId = :communityId");
            query.setParameter("primaryPayerMedicaidMedicareId", medicaidMedicareId);
            query.setParameter("primaryPayerClassId", primaryPayerClassId);
            query.setParameter("communityId", communityId);
            query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            list = (List) query.getResultList();

            if (list.isEmpty()) {
                query = em.createQuery(
                        "select object(o) from PatientCommunityEnrollment as o where o.secondaryPayerMedicaidMedicareId = :secondaryPayerMedicaidMedicareId and o.secondaryPayerClassId = :secondaryPayerClassId"
                                + " and o.communityId = :communityId");
                query.setParameter("secondaryPayerMedicaidMedicareId", medicaidMedicareId);
                query.setParameter("secondaryPayerClassId", primaryPayerClassId);
                query.setParameter("communityId", communityId);
                query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

                list = (List) query.getResultList();

            }
            return list;
        } finally {
            em.close();
        }
    }

    public List<PatientCommunityEnrollment> getSecondaryPatientMedicaidMedicare(String medicaidMedicareId,
                                                                                long secPayerClassId, long communityId) {

        EntityManager em = getEntityManager();
        Query query = null;
        List<PatientCommunityEnrollment> list = null;

        try {

            query = em.createQuery(
                    "select object(o) from PatientCommunityEnrollment as o where o.secondaryPayerMedicaidMedicareId = :secondaryPayerMedicaidMedicareId and  o.secondaryPayerClassId = :secondaryPayerClassId"
                            + " and o.communityId = :communityId");
            query.setParameter("secondaryPayerMedicaidMedicareId", medicaidMedicareId);
            query.setParameter("secondaryPayerClassId", secPayerClassId);
            query.setParameter("communityId", communityId);
            query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            list = (List) query.getResultList();

            if (list.isEmpty()) {
                query = em.createQuery(
                        "select object(o) from PatientCommunityEnrollment as o where o.primaryPayerMedicaidMedicareId = :primaryPayerMedicaidMedicareId and o.primaryPayerClassId = :primaryPayerClassId"
                                + " and o.communityId = :communityId");
                query.setParameter("primaryPayerMedicaidMedicareId", medicaidMedicareId);
                query.setParameter("primaryPayerClassId", secPayerClassId);
                query.setParameter("communityId", communityId);
                query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

                list = (List) query.getResultList();

            }

            return list;
        } finally {
            em.close();
        }
    }

    /**
     * @param id
     * @return ProgramHealthHome
     */
    public ProgramHealthHome getProgramHealthHome(long id) {

        EntityManager em = getEntityManager();
        ProgramHealthHome programHealthHome = null;
        try {
            Query q = em
                    .createQuery("select object(o) from ProgramHealthHome as o where o.programHealthHomePK.id =:id");
            q.setParameter("id", id);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            programHealthHome = (ProgramHealthHome) q.getSingleResult();
        } finally {
            em.close();
        }

        return programHealthHome;
    }

    /**
     * @param id
     * @param communityId
     * @return ProgramHealthHome
     */
    public ProgramHealthHome getProgramHealthHome(long communityId, long id) {

        EntityManager em = getEntityManager();
        ProgramHealthHome programHealthHome = null;
        ProgramHealthHomePK programHealthHomePK = new ProgramHealthHomePK(id, communityId);
        try {
            Query q = em.createNamedQuery("ProgramHealthHome.findByCompositeId").setParameter("id", programHealthHomePK)
                    .setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            programHealthHome = (ProgramHealthHome) q.getSingleResult();
        } finally {
            em.close();
        }

        return programHealthHome;
    }

    private void setProgramDatesToNoon(PatientCommunityEnrollment patientEnrollment) {

        // handle program end date
        Date programEndDate = patientEnrollment.getProgramEndDate();
        if (null != programEndDate) {
            Date noonProgramEndDate = DateUtils.getDateNoon(programEndDate);
            patientEnrollment.setProgramEndDate(noonProgramEndDate);
        }

        // handle program effective date
        Date programNameEffectiveDate = patientEnrollment.getProgramNameEffectiveDate();
        if (null != programNameEffectiveDate) {
            Date noonProgramNameEffectiveDate = DateUtils.getDateNoon(programNameEffectiveDate);
            patientEnrollment.setProgramNameEffectiveDate(noonProgramNameEffectiveDate);
        }

    }

    @Override
    public PatientCommunityEnrollment isDuplicateEmail(String email) {
        EntityManager em = getEntityManager();
        PatientCommunityEnrollment enrollment = null;
        List<PatientCommunityEnrollment> list = null;
        try {
            Query q = em.createQuery(
                    "select object(o) from PatientCommunityEnrollment as o where o.emailAddress = :theEmail"
                            + " AND o.communityId = :communityId");
            q.setParameter("theEmail", email);
            q.setParameter("communityId", this.communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            list = (List) q.getResultList();

            if (!list.isEmpty()) {
                enrollment = (PatientCommunityEnrollment) list.get(0);
            }

            return enrollment;
        } finally {
            em.close();
        }
    }

    @Override
    //JIRA 860-1460 (1466)
    //Migrate EclipseLink Expression to pure JPA Criteria
    //public List<PatientCommunityEnrollment> queryPatientEnrollmentBySearchExpression(Expression searchExpression,
    public List<PatientCommunityEnrollment> queryPatientEnrollmentBySearchExpression(Predicate searchPredicate,
                                                                                     Integer limitPatientCount) {
        // lets get some results
        EntityManager entityManager = null;
        entityManager = this.getEntityManager();
        List<PatientCommunityEnrollment> peresults = null;
        try {

            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<PatientCommunityEnrollment> cq = cb.createQuery(PatientCommunityEnrollment.class);
            cq.where(searchPredicate);
            TypedQuery<PatientCommunityEnrollment> tq = entityManager.createQuery(cq);
            peresults = tq.getResultList();

            if (peresults != null) {
                this.assignPayerPlanAndPayerClass(peresults);
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Exception in queryPatientEnrollmentBySearchExpression:", ex);
        } finally {
            this.cleanUpPersistenceResources(entityManager);
        }
        return peresults;
    }

    @Override
    public List<PatientSource> getPatientSourceList(long communityId) {

        EntityManager em = getEntityManager();
        Query query = null;

        try {
            query = em.createQuery("select object(o) from PatientSource as o where o.communityId = :theCommunityId");
            query.setParameter("theCommunityId", communityId);
            query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<PatientSource> list = (List) query.getResultList();

            return list;
        } finally {
            em.close();
        }
    }

    /**
     * Returns true if this is a valid patient id
     *
     * @param communityId
     * @param patientId
     * @return
     */
    public boolean isValidPatientId(long communityId, long patientId) {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery(
                    "select count(o) from PatientCommunityEnrollment as o where o.patientId = :thePatientId and o.communityId = :theCommunityId");
            q.setParameter("thePatientId", patientId);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            long count = ((Long) q.getSingleResult()).intValue();

            return count > 0;
        } finally {
            em.close();
        }

    }

    public String getCommunityOid() {
        return communityOid;
    }
}
