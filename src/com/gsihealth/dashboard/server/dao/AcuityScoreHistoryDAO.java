/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.AcuityScoreHistory;

/**
 *
 * @author ssingh
 */
public interface AcuityScoreHistoryDAO {
    
     public void addToAcuityScoreHistory(AcuityScoreHistory acuityScoreHistory);
}
