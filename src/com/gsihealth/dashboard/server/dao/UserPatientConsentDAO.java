/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.OrganizationPatientConsent;
import com.gsihealth.entity.User;
import com.gsihealth.entity.UserPatientConsent;
import com.gsihealth.entity.UserPatientConsentPK;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.PreexistingEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.RollbackFailureException;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public interface UserPatientConsentDAO {

    public void add(UserPatientConsent userPatientConsent, Date startDate) throws PreexistingEntityException, RollbackFailureException, Exception;

    public void add(List<UserPatientConsent> userPatientConsent) throws PreexistingEntityException, RollbackFailureException, Exception;
    
    public void deleteOldConsentEntries(long patientId);

    public void destroy(UserPatientConsentPK id) throws NonexistentEntityException, RollbackFailureException, Exception;

    public UserPatientConsent findUserPatientConsent(UserPatientConsentPK id);

    public List<UserPatientConsent> findUserPatientConsentEntities(long patientId, int pageNumber, int pageSize);

    public int getUserPatientConsentCount(long patientId);

    public void update(UserPatientConsent userPatientConsent) throws NonexistentEntityException, RollbackFailureException, Exception;

    public List<User> getUsers(long organizationId, long communityId);
    
    public boolean hasConsent(long userId, long patientId, long communityId);

    public void deleteOldConsentEntries(long patientId, List<User> oldUsers, long communityId) throws Exception;
    
    public boolean exists(UserPatientConsentPK id);

    public List<Long> getUserIds(long organizationId, long GSI_COMMUNITY_ID);

    public List<Long> getUserIds(List<OrganizationPatientConsent> orgs, long GSI_COMMUNITY_ID);
    
}
