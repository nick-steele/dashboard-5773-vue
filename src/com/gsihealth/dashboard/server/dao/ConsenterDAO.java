package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.Consenter;
import java.util.List;
import javax.persistence.NoResultException;

/**
 *
 * @author Edwin Montgomery
 */
public interface ConsenterDAO {
    
    public List<Consenter> getConsenters(Long communityId);
    
    public Consenter getConsenter(Long communityId, long code);
    
    public Long getConsenterCode(String description) throws NoResultException;
}
