package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.entity.PatientCommunityEnrollment;
import com.gsihealth.entity.PatientSource;
import com.gsihealth.entity.ProgramHealthHome;
import com.gsihealth.dashboard.server.common.PatientResult;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.patientmdminterface.entity.DuplicateCheckProcessResult;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import java.util.List;
import java.util.Map;
import javax.persistence.NoResultException;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Chad Darby
 */
public interface PatientEnrollmentDAO {

    public List<String> findAllPatientEuids();
    
    public void destroy(long id) throws NonexistentEntityException;

    public void edit(PatientCommunityEnrollment patientEnrollment) throws NonexistentEntityException, Exception;

    public PatientCommunityEnrollment findPatientEnrollment(String euid) throws NoResultException;
    public PatientCommunityEnrollment findPatientEnrollment(String euid, Long communityId) throws NoResultException;

    public PatientCommunityEnrollment findPatientEnrollment(long patientId) throws NoResultException;
    public PatientCommunityEnrollment findPatientEnrollment(long patientId, Long communityId) throws NoResultException;
    
    public Long findPatientId(String euid) throws NoResultException;

    //JIRA 2523
    public DuplicateCheckProcessResult checkForDuplicates(SimplePerson person) throws Exception;
    
    public PatientResult addPatient(Person thePerson,long communityId) throws Exception;

    //public long findCommunityOrgId(long communityId, long orgId);

    public void updatePatient(Person thePerson) throws Exception;

    public void updatePatientEnrollment(PatientCommunityEnrollment patientEnrollment);

    public int getTotalCountForCandidates(long organizationId);
    
    public Map<String, PatientCommunityEnrollment> findCandidates(long organizationId, int pageNumber, int pageSize);

    public int getTotalCountForEnrolledPatients(long userId);
    
//    public Map<String, PatientCommunityEnrollment> findEnrolledPatients(long userId, int pageNumber, int pageSize);

    public int getTotalCountFilterPatientsWithoutDemographics(SearchCriteria searchCriteria, List<String> statuses, long currentUserOrgId);
    
    public int getTotalCountFilterPatientsWithoutDemographics(SearchCriteria searchCriteria, List<String> statuses);
    
    public Map<String, PatientCommunityEnrollment> filterPatientsWithoutDemographics(SearchCriteria searchCriteria, List<String> statuses, long currentUserOrgId);

    public int getTotalCountFilterEnrolledPatientsWithoutDemographics(long userId, SearchCriteria searchCriteria, List<String> statuses);

    public Map<String, PatientCommunityEnrollment> filterEnrolledPatientsWithoutDemographics(long userId, SearchCriteria searchCriteria, List<String> statuses);
    
    public int getTotalCountForCandidates();
    
    public Map<String, PatientCommunityEnrollment> findCandidates(int pageNumber, int pageSize);
    
    public Map<String, PatientCommunityEnrollment> filterPatientsWithoutDemographics(SearchCriteria searchCriteria, List<String> statuses);
    
    public int getTotalCountForEnrolledPatients();
    
    public int getTotalCountFilterEnrolledPatientsWithoutDemographics(SearchCriteria searchCriteria, List<String> statuses);
    
    public Map<String, PatientCommunityEnrollment> findEnrolledPatients(int pageNumber, int pageSize);
    
    public Map<String, PatientCommunityEnrollment> filterEnrolledPatientsWithoutDemographics(SearchCriteria searchCriteria, List<String> statuses);
    
  //  public List<PatientCommunityEnrollment> getPatientMedicaidMedicare(String medicaidMedicareId);
    
    public List<PatientCommunityEnrollment> getPrimaryPatientMedicaidMedicare(String medicaidMedicareId,long primaryPayerClassId,long communityId);
    
    public List<PatientCommunityEnrollment> getSecondaryPatientMedicaidMedicare(String medicaidMedicareId,long secPayerClassId,long communityId);
    
    public ProgramHealthHome getProgramHealthHome(long id);
    
    public ProgramHealthHome getProgramHealthHome(long communityId,long id);
    
    
    /**
     * @param searchPredicate
     * @param pageNumber
     * @param pageSize
     * @param limitPatientCount
     * @return a list of patientEnrollments filtered by query expression
     */
    public List<PatientCommunityEnrollment> queryPatientEnrollmentBySearchExpression(
            Predicate searchPredicate, 
            int pageNumber, 
            int pageSize,
            Integer limitPatientCount);
    
    //JIRA 870 - needed to have one from clause
    public Root<PatientCommunityEnrollment> getPatientCommunityEnrollmentRootForQuery();

    /**
     * @param searchPredicate
     * @param maxPatients - if there's no demographics, throttle pce size
     * @return row count for patientEnrollment using searchExpression
     */
    public int getPatientSearchTotalCount(Root<PatientCommunityEnrollment> pceRoot, 
            Predicate searchPredicate, Integer maxPatients);
    /**
     * build criteria query for patient search
     * @param searchCriteria - user generated search params 
     * @param patientEuids - until we find a better way, do the mdm patient demographic search first
     * @param userOrgId - logged on user's org id
     * @param communityId
     * @param enrolled - enrolled status
     * @param userId 
     * @return criteria query with all the above params included dynamically
     */
    public Predicate criteriaBuilderForPatientSearch(
            Root<PatientCommunityEnrollment> pceRoot,
            SearchCriteria searchCriteria,
            String[] patientEuids,
            Long userOrgId,
            long communityId,
            boolean enrolled,
            long userId,
            boolean isMangeConsentWithoutEnrollment,
            boolean isAllOrgsSelfAssert);

    public PatientCommunityEnrollment isDuplicateEmail(String email);
    
    public List<PatientCommunityEnrollment> queryPatientEnrollmentBySearchExpression(Predicate searchPredicate,
                    Integer limitPatientCount);

    public List<PatientSource> getPatientSourceList(long communityId);

    public boolean isValidPatientId(long communityId, long patientId);
    
    public String getCommunityOid();
    
}


