package com.gsihealth.dashboard.server.dao;

//classes used for Entity Creation in persistence.xml
import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.entity.*;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.hisp.HispClientUtils;
import com.gsihealth.dashboard.server.patientmanagement.PatientManagerImpl;
import com.gsihealth.dashboard.server.util.SSOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Chad Darby
 */
public class SSOUserMigrationUtility {

    private static final String defaultOid = "2.16.840.1.113883.3.1358";
    private Properties props;
    private Logger logger = Logger.getLogger(getClass().getName());
    private SSOUserDAO ssoUserDAO;
    private UserDAO userDAO;
    private OrganizationDAO organizationDAO;
    private CommunityDAO communityDAO;
    private CommunityOrganizationDAO communityOrganizationDAO;
    private PatientManagerImpl patientManager;
    private PatientEnrollmentDAO patientEnrollmentDAO;
    
    private static final String DEFAULT_PASSWORD = "Test123#";

    public SSOUserMigrationUtility(String propsFileName) throws Exception {
        props = new Properties();

        logger.info("Loading properties file: " + propsFileName);
        props.load(new FileInputStream(propsFileName));
        logger.info("Loaded: " + props);

        ssoUserDAO = new SSOUserDAOImpl();
        //Add to fetch user data from DB
        userDAO = new UserDAOImpl();
        organizationDAO = new OrganizationDAOImpl();
        communityDAO = new CommunityDAOImpl();
        communityOrganizationDAO = new CommunityOrganizationDAOImpl();
        
        //prep for MDM sync
        String mdmEndpoint = props.getProperty("patient.mdm.endpoint");
        String gsiHealthOID = props.getProperty("gsihealth.oid");
        logger.info("mdmEndpoint:" + mdmEndpoint);
        logger.info("gsiHealthOID:" + gsiHealthOID); 
//        patientManager = new PatientManagerImpl(mdmEndpoint, gsiHealthOID);
//       patientEnrollmentDAO = new PatientEnrollmentDAOImpl(patientManager,props);
    }

    public void run(String userAccountsFileName) throws Exception {
        List<String> userAccounts = FileUtils.readLines(new File(userAccountsFileName));


        String token = getToken();

        for (String temp : userAccounts) {
            String[] data = temp.split(",");

            String firstName = data[0].trim();
            String lastName = data[1].trim();
            String email = data[2].trim();
            
            String ssoUserString = buildSsoUserString(firstName, lastName, email);
            System.out.println("Adding user account to OpenAM: " + temp);
            ssoUserDAO.addUser(1, token, ssoUserString);
            System.out.println("Success!\n");

            Thread.sleep(100);
        }
        ssoUserDAO.logout(1, token);        
    }
    
    public void patientMDMSync() throws Exception {
        
        // need to get euid for all patients in patient_community_enrollment        
        List<String> euids = patientEnrollmentDAO.findAllPatientEuids();
        
        int syncCounter=1;
        Long patientID = 0L;
        
        System.out.println("Person size: " + euids.size());
        
         for (String tempEuid : euids) {
             
             Person patient = new Person(patientManager.getPatient(tempEuid));
             
       //     if ((syncCounter % 7) == 0) {

                try {
                    patientID  = patientEnrollmentDAO.findPatientId(patient.getEuid()); 
                  
                    System.out.println("=========================");
                    System.out.println(syncCounter + ". Patient EUID and patient ID  "
                            + patient.getEuid() + " - " + patientID );

                    //get patientEnrollment
                    PatientCommunityEnrollment patientEnrollment = patientEnrollmentDAO.findPatientEnrollment(patient.getEuid());
                   
                    System.out.println("--- Patient Enrollment Status: " + patientEnrollment.getStatus());
                    // *** remember to uncomment to sync mdm records
                    //     patient.setPatientCommunityEnrollment(patientEnrollment);
                    //     patientEnrollmentDAO.updatePatient(patient);


                } catch (Exception e) {
                    System.out.println(e.toString() + "********** Skipped Bad Record for Patient EUID: "
                            + patient.getEuid() + " - " + patientID);

                }
                Thread.sleep(200);
           // }
            syncCounter++;
        }
    }

    public void update() throws Exception{
        
         String token = getToken();
         
         List<User> users = userDAO.findUserEntities();
         
         System.out.println("There are " +users.size() + " users");
         
                 
        long communityId= ApplicationContextUtils.getLoginResult().getCommunityId();
        Community loggedOnUserCommunity = new Community(communityId);
        
         int syncCounter=1;
         for (User user:users) {
             
           
           if (syncCounter==1) { 
             //1.2 Organization
             OrganizationCwd organization;
             Long orgId = 1L;
             UserCommunityOrganization userCommunityOrganization = user.getUserCommunityOrganization(loggedOnUserCommunity);
             
             if (userCommunityOrganization != null){
                organization = userCommunityOrganization.getOrganizationCwd();
             } else {  //FIXME does this need accesslvl and role stuff?       
                //Prepare
                userCommunityOrganization = new UserCommunityOrganization();
                //1.1 Community
                Community community = communityDAO.getCommunityById(communityId);
                userCommunityOrganization.setCommunity(community);
                
                organization = organizationDAO.findOrganization(orgId,communityId);
                userCommunityOrganization.setOrganizationCwd(organization);

                userCommunityOrganization.setStartDate(new Date(System.currentTimeMillis()));
                userCommunityOrganization.setUser(user);
                user.addUserCommunityOrganization(userCommunityOrganization);
             }
            

             // create user account on HISP/James
             String email = user.getEmail();
             String directDomain = props.getProperty("hisp.direct.domain");
             String directAddress="";
             
             try {
                if (email.indexOf("@")!=-1){
                    directAddress = HispClientUtils.generateDirectAddress(email, directDomain);
                }
                
            //Get OpenAM patient group
            String patientGroup = props.getProperty("carebook.openam.patientgroup","");
            
             //Build SSO User            
                 SSOUtils ssoUtils = new SSOUtils();
                 StringBuilder ssoUser = ssoUtils.buildSsoUser(user, "", directAddress,
                         userCommunityOrganization, organization, patientGroup, true);
                 String tokenId = getToken();
                 System.out.println("ssoUser: " + ssoUser.toString());

                 //1.4 create user in Open AM SSO
                 System.out.println("Updating user account: " + email + "..");
                 ssoUserDAO.updateUser(1, tokenId, ssoUser.toString());
                 ssoUserDAO.logout(1, token);
                 
                 System.out.println("Success!\n");
             } catch (Exception e) {
                 System.out.println(e.getMessage() + "- Skipped Bad Record for User: " + email);

             }
            

             Thread.sleep(200);
             
            } //end if statement  -- comment out this if statement sync all users
            syncCounter++;
            
        } 
         
    }
    private String buildSsoUserString(String firstName, String lastName, String email) throws Exception {
        StringBuilder ssoUserString = new StringBuilder();

        String encodedEmail = encode(email);
        
        ssoUserString.append("&identity_name=" + encodedEmail);
        ssoUserString.append("&identity_attribute_names=cn&identity_attribute_values_cn=" + encode(firstName)+"%20"+encode(lastName));
        ssoUserString.append("&identity_attribute_names=sn&identity_attribute_values_sn=" + encode(firstName));
        ssoUserString.append("&identity_attribute_names=userpassword&identity_attribute_values_userpassword=" + encode(DEFAULT_PASSWORD));
        ssoUserString.append("&identity_realm=%2F&identity_type=user");

        return ssoUserString.toString();
    }

    private String getToken() throws Exception {
        String adminUserName = props.getProperty("sso.server.admin.username");
        String adminPassword = props.getProperty("sso.server.admin.password");

        String tokenId = ssoUserDAO.authenticate(1, adminUserName, adminPassword); //"amadmin","adminadmin");
        tokenId = StringUtils.trimToEmpty(tokenId);
        logger.info("token id = " + tokenId);

        return tokenId;
    }

    protected String encode(String value) throws Exception {
        if (value != null) {
            value = URLEncoder.encode(value, "UTF-8");
        } else {
            value = "";
        }
        return value;
    }

    public static void main(String[] args) throws Exception {
        
        // DEV
       String propsFileName = "propfiles-temp/dev-dashboard.properties";
       //String userAccountsFileName = "propfiles-temp/dev-users.txt";
        

        // QA
      //  String propsFileName = "propfiles-temp/qa-dashboard.properties";
      //  String userAccountsFileName = "propfiles-temp/qa-users.txt.csv";
        
        //Staging
        //String propsFileName = "propfiles-temp/staging-dashboard.properties";
        // String userAccountsFileName = "propfiles-temp/qa-users.txt.csv";
        
        //Production
        //String propsFileName = "propfiles-temp/production-dashboard.properties";
        //String userAccountsFileName = "propfiles-temp/qa-users.txt.csv";

        SSOUserMigrationUtility util = new SSOUserMigrationUtility(propsFileName);
        
        // util.run(userAccountsFileName);
        //Synchronize openAM
        //**** Uncomment next line util.update() to sync database ***
        //util.update();
        
        //Sync MDM - not needed just to refresh OpenAM
        //** uncomment to run sync
       // util.patientMDMSync();

        // System.out.println("Plain name: " + util.getPlainName("leroy.jones.superuser@test.com"));

    }

    protected String getPlainName(String email) {

        String data = null;
        if (email.contains("@")) {
            data = email.split("@")[0];
        }
        else {
            data = email;
        }

        System.out.println("data = " + data);
        
        data = data.replaceAll("\\.", " ");

        return data;
    }


}
