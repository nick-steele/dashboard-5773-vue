package com.gsihealth.dashboard.server.dao;

import java.util.Map;

/**
 *
 * @author Vishal (Off.)
 */
public interface SSOUserDAO {
    
    public void addUser(long communityId, String tokenId, String ssoUser) throws Exception;

    public void deleteUser(long communityId, String tokenId, String ssoUser) throws Exception;
    
    public boolean validate( long communityId,String tokenId) throws Exception;

    public void updateUser(long communityId, String tokenId, String ssoUser) throws Exception;
    
    public String authenticate(long communityId, String user, String Password) throws Exception;
    
    public String authenticate(long communityId, Map ssoUserAccess)  throws Exception;
    
    //JIRA 1122
    public void logout(long communityId, String tokenId)  throws Exception;
    
    public String avadoPOST(long communityId,String tokenId) throws Exception;

    //JIRA 1853
    public String changePassword(Long communityId, String tokenId, String userId, String newPassword) throws Exception;

    //Spira 6537
    public String getEncryptedPassword(Long communityId, String token, String userEmail,Long userId, String status) throws Exception;

    public String addUserToCP(Long communityId, String token, String userEmail,Long userId, String status) throws Exception;
    
    public boolean isUserActive(Long communityId, String token, String userEmail) throws Exception;

    public boolean isUserActiveInCommunity (Long communityId, String token, String userEmail) throws Exception;

    public boolean isUserActiveNotInOtherCommunity (Long communityId, String token, String userEmail) throws Exception;

    public void setUserActive(Long communityId, String token, String userEmail, boolean active) throws Exception;
    
    public void unlockAccount(Long communityId, String token, String userEmail) throws Exception;
}
