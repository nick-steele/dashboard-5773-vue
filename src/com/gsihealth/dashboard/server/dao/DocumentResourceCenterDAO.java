/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.DocumentResourceCenter;
import java.util.List;

/**
 *
 * @author User
 */
public interface DocumentResourceCenterDAO {
    
    
    public List<DocumentResourceCenter> findDocumentEntities(long communityId);
}
