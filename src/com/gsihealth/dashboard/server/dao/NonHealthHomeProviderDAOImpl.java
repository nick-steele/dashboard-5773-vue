package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.entity.NonHealthHomeProvider;
import com.gsihealth.entity.NonHealthHomeProviderAssignedPatient;
import com.gsihealth.entity.NonHealthHomeProviderPK;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.PreexistingEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.RollbackFailureException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.*;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import java.util.logging.Level;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.lang.time.StopWatch;

/**
 *
 * @author Chad Darby
 */
public class NonHealthHomeProviderDAOImpl implements NonHealthHomeProviderDAO {

    private EntityManagerFactory emf = null;
    private Logger logger = Logger.getLogger(getClass().getName());

    public NonHealthHomeProviderDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public NonHealthHomeProviderDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public synchronized long add(NonHealthHomeProvider nonHealthHomeProvider) throws PreexistingEntityException, RollbackFailureException, Exception {

        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        // get next providerId
        long providerId = getNextProviderId();

        NonHealthHomeProviderPK tempPK = nonHealthHomeProvider.getNonHealthHomeProviderPK();
        tempPK.setProviderId(providerId);
        nonHealthHomeProvider.setNonHealthHomeProviderPK(tempPK);

        try {
            utx.begin();
            em.persist(nonHealthHomeProvider);

            utx.commit();

            return providerId;
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void update(NonHealthHomeProvider nonHealthHomeProvider) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();
            nonHealthHomeProvider = em.merge(nonHealthHomeProvider);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = nonHealthHomeProvider.getNonHealthHomeProviderPK().getProviderId();
                if (findNonHealthHomeProvider(nonHealthHomeProvider.getNonHealthHomeProviderPK()) == null) {
                    throw new NonexistentEntityException("The nonHealthHomeProvider with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void delete(NonHealthHomeProviderPK id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();
            NonHealthHomeProvider nonHealthHomeProvider;
            try {
                nonHealthHomeProvider = em.getReference(NonHealthHomeProvider.class, id);
                nonHealthHomeProvider.getNonHealthHomeProviderPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The nonHealthHomeProvider with id " + id + " no longer exists.", enfe);
            }
            em.remove(nonHealthHomeProvider);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public NonHealthHomeProvider findNonHealthHomeProvider(NonHealthHomeProviderPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(NonHealthHomeProvider.class, id);
        } finally {
            em.close();
        }
    }

    public int getNonHealthHomeProviderCount(long patientId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from NonHealthHomeProvider as o where o.patientId = :thePatientId");
            q.setParameter("thePatientId", patientId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @Override
    public boolean isDuplicateEmail(String email, long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from NonHealthHomeProvider as o where o.email = :theEmail and o.nonHealthHomeProviderPK.communityId = :communityId");
            q.setParameter("theEmail", email);
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int count = ((Long) q.getSingleResult()).intValue();

            return count > 0;
        } finally {
            em.close();
        }
    }

    public List<NonHealthHomeProvider> findNonHealthHomeProviderEntities(long patientId, long communityId) {
        EntityManager em = getEntityManager();
        try {
            List<NonHealthHomeProviderAssignedPatient> assignedPatients = getProviderAssignedPatientByPatientId(patientId,communityId);

            long providerId;
            List<NonHealthHomeProvider> healthHomeProviders = new ArrayList<NonHealthHomeProvider>();
            NonHealthHomeProviderPK healthHomeProviderPK = new NonHealthHomeProviderPK();
            NonHealthHomeProvider homeProvider;
            for (NonHealthHomeProviderAssignedPatient temp : assignedPatients) {

                providerId = temp.getNonHealthHomeProviderAssignedPatientPK().getProviderId();
                healthHomeProviderPK.setCommunityId(communityId);
                healthHomeProviderPK.setProviderId(providerId);

                homeProvider = findNonHealthHomeProvider(healthHomeProviderPK);

                healthHomeProviders.add(homeProvider);
            }


            return healthHomeProviders;
        } finally {
            em.close();
        }
    }

    public int getNonHealthHomeProviderCountByCommunityId(long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from NonHealthHomeProvider as o where o.nonHealthHomeProviderPK.communityId = :theCommunityId");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @Override
    public List<NonHealthHomeProvider> findNonHealthHomeProviderEntitiesByCommunityId(long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from NonHealthHomeProvider as o where o.nonHealthHomeProviderPK.communityId = :theCommunityId");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

//            int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);
//
//            q.setFirstResult(startRecordNumber);
//            q.setMaxResults(pageSize);

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public void assignProvider(NonHealthHomeProviderAssignedPatient providerAssignedPatient, boolean recordAlreadyExists) throws PreexistingEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();

            if (recordAlreadyExists) {
                em.merge(providerAssignedPatient);
            } else {
                em.persist(providerAssignedPatient);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<NonHealthHomeProviderAssignedPatient> getProviderAssignedPatient(long providerId, long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from NonHealthHomeProviderAssignedPatient as o where o.nonHealthHomeProviderAssignedPatientPK.providerId = :theProviderId and o.nonHealthHomeProviderAssignedPatientPK.communityId = :theCommunityId");
            q.setParameter("theProviderId", providerId);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);


            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<NonHealthHomeProviderAssignedPatient> getProviderAssignedPatientByPatientId(long patientId,long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from NonHealthHomeProviderAssignedPatient as o where o.nonHealthHomeProviderAssignedPatientPK.patientId = :thePatientId and o.nonHealthHomeProviderAssignedPatientPK.communityId=:theCommunityId and o.consentStatus= :theConsentStatus");
            q.setParameter("thePatientId", patientId);
            q.setParameter("theCommunityId", communityId);
            q.setParameter("theConsentStatus", "PERMIT");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);


            return q.getResultList();
        } finally {
            em.close();
        }
    }

    // this method is not being called from unAssignProvider() method from service class, instead from removeProvider()
    public void unAssignProvider(NonHealthHomeProviderAssignedPatient assignedPatient) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();
            NonHealthHomeProviderAssignedPatient homeProviderAssignedPatient;
            try {
                homeProviderAssignedPatient = em.getReference(NonHealthHomeProviderAssignedPatient.class, assignedPatient.getNonHealthHomeProviderAssignedPatientPK());
                homeProviderAssignedPatient.getNonHealthHomeProviderAssignedPatientPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The nonHealthHomeProvider with id " + assignedPatient.getNonHealthHomeProviderAssignedPatientPK() + " no longer exists.", enfe);
            }
            em.remove(homeProviderAssignedPatient);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public int getCountForPatientsAssignedToProvider(long providerId, long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from NonHealthHomeProviderAssignedPatient as o where o.nonHealthHomeProviderAssignedPatientPK.providerId = :theProviderId and o.nonHealthHomeProviderAssignedPatientPK.communityId= :theCommunityId and o.consentStatus= :theConsentStatus");
            q.setParameter("theProviderId", providerId);
            q.setParameter("theCommunityId", communityId);
            q.setParameter("theConsentStatus", "PERMIT");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    private long getNextProviderId() {
        long id = 0;

        EntityManager em = getEntityManager();
        try {
            Query query = em.createQuery("select o.nonHealthHomeProviderPK.providerId from NonHealthHomeProvider as o order by o.nonHealthHomeProviderPK.providerId desc");
            query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            query.setMaxResults(1);

            try {
                id = (Long) query.getSingleResult() + 1;

            } catch (NoResultException exc) {
                // do nothing since we'll use the id = 0;
            }

            return id;
        } finally {
            em.close();
        }

    }

    public boolean ifProviderAssignedExist(long providerId, long patientId, long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from NonHealthHomeProviderAssignedPatient as o where o.nonHealthHomeProviderAssignedPatientPK.providerId = :theProviderId and o.nonHealthHomeProviderAssignedPatientPK.communityId= :theCommunityId and o.nonHealthHomeProviderAssignedPatientPK.patientId= :thePatientId");
            q.setParameter("theProviderId", providerId);
            q.setParameter("theCommunityId", communityId);
            q.setParameter("thePatientId", patientId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return ((Long) q.getSingleResult()).intValue() == 1;
        } finally {
            em.close();
        }
    }

    @Override
    public boolean isDuplicateNPI(String NPI, long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from NonHealthHomeProvider as o where o.NPI = :theNPI and o.nonHealthHomeProviderPK.communityId = :communityId");
            q.setParameter("theNPI", NPI);
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int count = ((Long) q.getSingleResult()).intValue();

            return count > 0;
        } finally {
            em.close();
        }
    }

    /**
     * TODO this can be more generic, parameterize the class as well as the
     * expression and you can use this more widely
     *
     * @param searchExpression
     * @param limitPatientCount
     * @return row count for patientEnrollment using searchExpression
     */
    //JIRA 860-1460 (1467)
    //Migrate EclipseLink Expression to pure JPA Criteria    
    @Override
    //public List<NonHealthHomeProviderAssignedPatient> getNHPPatientSearchTotalCount(Expression searchNHPExpression, Integer limitPatientCount) {    
    public List<NonHealthHomeProviderAssignedPatient> getNHPPatientSearchTotalCount(Predicate searchNHPPredicate, Integer limitPatientCount) {
        logger.info(("lgetNHPPatientSearchTotalCount ---- rows"));
        
        int count = 0;
        List<NonHealthHomeProviderAssignedPatient> peresults = null;
       
        EntityManager entityManager = this.getEntityManager();
        try {
            
            logger.info("#### ReportQuery   ReportQuery  --->");
            //ReadAllQuery readAllQuery = new ReadAllQuery(NonHealthHomeProviderAssignedPatient.class);
            //readAllQuery.setSelectionCriteria(searchNHPExpression);
            //readAllQuery.dontMaintainCache();
            
            CriteriaBuilder cb = entityManager.getCriteriaBuilder();
            CriteriaQuery<NonHealthHomeProviderAssignedPatient> cq = cb.createQuery(NonHealthHomeProviderAssignedPatient.class);
            cq.where(searchNHPPredicate);
            TypedQuery<NonHealthHomeProviderAssignedPatient> tq = entityManager.createQuery(cq);
            peresults = tq.getResultList();
            if(!peresults.isEmpty()){
                count=peresults.size();
                
            }
            
                    
        } catch (Exception tle) {
            logger.log(Level.SEVERE, "count failed: " , tle);
        } finally {
            if (entityManager != null) {
                entityManager.close();
            }
        }
        logger.info("counting done, returning " + count);
        return peresults;
    }

    //JIRA 860-1460 (1467)
    //Migrate EclipseLink Expression to pure JPA Criteria
    @Override
    //public Expression criteriaBuilderForNHPPatientSearch(SearchCriteria searchCriteria, long communityId) {    
    public Predicate criteriaBuilderForNHPPatientSearch(SearchCriteria searchCriteria, long communityId) {

       
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        
        EntityManager em = getEntityManager();
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<NonHealthHomeProviderAssignedPatient> cq = cb.createQuery(NonHealthHomeProviderAssignedPatient.class);
            Root<NonHealthHomeProviderAssignedPatient> nhp = cq.from(NonHealthHomeProviderAssignedPatient.class);

            // lets build an expression, we always need community id, so lets start there
            //Expression baseExpression = nhp.get("nonHealthHomeProviderAssignedPatientPK").get("communityId").equal(communityId);
            Predicate basePredicate = cb.equal(nhp.get("nonHealthHomeProviderAssignedPatientPK").get("communityId"), communityId);

            Long providerID=Long.parseLong(searchCriteria.getNetworkProviderId());

            //Expression providerIdQry = nhp.get("nonHealthHomeProviderAssignedPatientPK").get("providerId").equal(providerID);
            //baseExpression = baseExpression.and(providerIdQry);
            Predicate providerIdPred = cb.equal(nhp.get("nonHealthHomeProviderAssignedPatientPK").get("providerId"), providerID);
            basePredicate = cb.and(basePredicate, providerIdPred);

            //Expression statusQry = nhp.get("consentStatus").in(new String[]{"PERMIT"});
            //baseExpression = baseExpression.and(statusQry);
            Predicate statusPred = nhp.get("consentStatus").in("PERMIT");
            basePredicate = cb.and(basePredicate, statusPred);

            stopWatch.stop();

            return basePredicate;
        }
        finally {
            em.close();
        }
    }

    /**
     *
     * @param communityId
     * @return
     */
     public List<NonHealthHomeProvider> getProvidersByCommunityId(long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from NonHealthHomeProvider as o where o.nonHealthHomeProviderPK.communityId = :theCommunityId order by o.lastName");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);         
            
            return q.getResultList();
        } finally {
            em.close();
        }
    }

       /**
     *
     * @param communityId
     * @return
     */
     public NonHealthHomeProvider getProvidersByCommunityId(long communityId,long providerId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from NonHealthHomeProvider as o where o.nonHealthHomeProviderPK.communityId = :theCommunityId and o.nonHealthHomeProviderPK.providerId=:theProviderId ");
            q.setParameter("theCommunityId", communityId);
            q.setParameter("theProviderId", providerId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);         
            
           // List<NonHealthHomeProvider> 
            
            return (NonHealthHomeProvider)q.getSingleResult();
        } finally {
            em.close();
        }
    }

      public List<NonHealthHomeProviderAssignedPatient> getProviderAssignedToPatient(long patientId, long communityId) {
     
        List<NonHealthHomeProviderAssignedPatient> ProviderAssignedPToPatient= getProviderAssignedPatientByPatientId(patientId, communityId);          
        return ProviderAssignedPToPatient;
        
    }
}
