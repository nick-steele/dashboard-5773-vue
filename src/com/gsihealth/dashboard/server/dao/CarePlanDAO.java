/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.server.reports.EncounterDate;
import com.gsihealth.dashboard.server.reports.EncounterMode;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

/**
 *
 * @author Chad Darby
 */
public interface CarePlanDAO {

    public Map<Long, EncounterDate> findEncounterDates(long communityId);
    
    public Map<Long, Date> findInitialAssessmentDates(long communityId);

    //JIRA 629
    public Map<Long, EncounterMode> findEncounterModeCounts(long communityId);
        
}
