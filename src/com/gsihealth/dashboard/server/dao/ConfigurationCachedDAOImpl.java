package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.Configuration;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author Chad.Darby
 */
public class ConfigurationCachedDAOImpl implements ConfigurationDAO {

    private Logger logger = Logger.getLogger(getClass().getName());
    private static HashMap<Long, HashMap> configuration = new HashMap();

    private EntityManagerFactory emf = null;
    private String defaultCommunityId;

    public ConfigurationCachedDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
        loadConfiguration();
    }

    public ConfigurationCachedDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
        loadConfiguration();

    }

    @Override
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    /**
     * Returns the value for this property. If it doesn't exist, returns the
     * default value
     *
     * @param communityId
     * @param configurationId
     * @param defaultValue
     * @return
     */
    @Override
    public String getProperty(long communityId, String name, String defaultValue) {

        String value = null;

        HashMap<String, String> comCache =  configuration.get(communityId);
        if(comCache==null){
                        logger.warning("community " + communityId + " not found in Configuration table, using default value of " + defaultValue);
            return defaultValue;
        }
        value=comCache.get(name);
        if (value == null) {
            logger.warning(name + " not found in Configuration table, using default value of " + defaultValue);
            return defaultValue;
        } 
        return value;
    }

    /**
     * ugly? maybe -if the droids you seek are not available, check the default
     * community, maybe they're there
     *
     * @param defaultPropertyName
     * @param communityId
     * @param name
     * @return value for this property for communityId or value for default
     * communityId or null if you're totally out of luck
     */
    @Override
    public String getProperty(String defaultPropertyName, Long communityId, String name) {
        
        
         String value = null;
         

        HashMap<String, String> comCache =  configuration.get(communityId);
        if(comCache==null){
            logger.warning("community " + communityId + " not found in Configuration table, using 1");
            comCache =  configuration.get(new Long(1));
        }
        value=comCache.get(name);
        if (value == null) {
            logger.warning(name + " not found in Configuration table");
            
        } 
        return value;
        
    }

    /**
     * Returns the value for this property. If it doesn't exist, returns null
     *
     * @param communityId
     * @param configurationId
     * @return
     */
    @Override
    public String getProperty(long communityId, String name) {

        String value = null;
         

        HashMap<String, String> comCache =  configuration.get(communityId);
        if(comCache==null){
            logger.warning("community " + communityId + " not found in Configuration table");
            return value;
        }
        value=comCache.get(name);
        if (value == null) {
            logger.warning(name + " not found in Configuration table");
            
        } 
        return value;
        
       
    }

    private void loadConfiguration() {
        EntityManager em = getEntityManager();

        try {
            if (configuration.isEmpty()) {
                System.out.println("LOADING CONFIGURATION CACHE!!!!");

                Query query = em.createQuery("select c from Configuration c ORDER BY c.configurationPK.communityId");

                query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

                List<Configuration> list = query.getResultList();
                Iterator<Configuration> iconf = list.iterator();
                Long comstart = new Long(0);
                HashMap<String, String> comCache =  null;
                while (iconf.hasNext()) {
                    Configuration conf = iconf.next();
                    Long comtest = conf.getConfigurationPK().getCommunityId();

                    if (!comtest.equals(comstart)) {
                         comCache= new HashMap();
                        configuration.put(comtest,comCache);
                        comstart = comtest;
                    }
                    String name = conf.getConfigurationPK().getName();
                    String value = conf.getValue();
                    comCache.put(name,value);
                     if(this.defaultCommunityId == null && name.equalsIgnoreCase("default.community.id")){
                        this.defaultCommunityId = value;
                        logger.info("setting default community id to " + value); // FIXME for the love of all that's holy temporary workaround til full mt
                    }
                }

            }
        } catch (Exception exc) {
            exc.printStackTrace();
        } finally {
            em.close();
        }
    }

    /**
     * @param name
     * @return all configs by this name
     */
    @Override
    public List<Configuration> findConfigurationsByName(String name) {
        EntityManager em = getEntityManager();
        List<Configuration> configurations = null;
        try {

            configurations = em.createNamedQuery("Configuration.findByName")
                    .setParameter("name", name)
                    .setParameter("active", "Y")
                    .setHint(JPAConstants.REFRESH, JPAConstants.TRUE)
                    .getResultList();
        } catch (Exception exc) {
            logger.warning("configuration qry failed cuz: " + exc);
        } finally {
            em.close();
        }
        return configurations;
    }

    @Override
    public String getDefaultCommunityId() {
        return defaultCommunityId;
    }

}
