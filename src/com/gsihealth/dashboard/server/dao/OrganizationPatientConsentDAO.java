package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.OrganizationPatientConsent;
import com.gsihealth.entity.OrganizationPatientConsentPK;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.PreexistingEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.RollbackFailureException;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public interface OrganizationPatientConsentDAO {

    public void add(OrganizationPatientConsent organizationPatientConsent) throws PreexistingEntityException, RollbackFailureException, Exception;

    public void add(List<OrganizationPatientConsent> organizationPatientConsent) throws PreexistingEntityException, RollbackFailureException, Exception;
 
    public void delete(OrganizationPatientConsentPK id) throws NonexistentEntityException, RollbackFailureException, Exception;

    public List<OrganizationPatientConsent> findOrganizationPatientConsentEntities(long patientId,long communityId, int pageNumber, int pageSize);

    public List<OrganizationPatientConsent> findOrganizationPatientConsentEntities(long patientId,long communityId);
    
    public int getOrganizationPatientConsentCount(long patientId,long communityId);

    public void update(OrganizationPatientConsent organizationPatientConsent) throws NonexistentEntityException, RollbackFailureException, Exception;
    
    public void saveOrganizationPatientConsentList(long patientId, List<OrganizationPatientConsent> data) throws Exception;

    public boolean hasConsent(long patientId,long communityId, long userOrganizationId);

    public List<OrganizationPatientConsent> findOrganizationPatientConsentEntitiesByOrg(long organizationId, Long communityId);
    
}
