package com.gsihealth.dashboard.server.dao;


import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchRange;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import com.gsihealth.entity.*;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.persistence.NoResultException;

/**
 *
 * @author Chad Darby
 */
public interface ReportDAO {

    public int findPatientEnrollmentInActiveCount(long communityId);

    public int findPatientEnrollmentPendingCount(long communityId);

    public int findPatientEnrollmentCount(long communityId);

    /**
     * Find all: candidates + enrolled patients.
     *
     * Returns a map: key=euid, value=patientEnrollment
     *
     * @param queryString
     * @return
     */
    public Map<String, PatientCommunityEnrollment> findAll(long communityId);

    /**
     * Find candidates.
     *
     * Returns a map: key=euid, value=patientEnrollment
     *
     * @return
     */
    public Map<String, PatientCommunityEnrollment> findCandidates(long communityId,long currentUserOrgId);

    /**
     * Find candidates.
     *
     * Returns a map: key=euid, value=patientEnrollment
     *
     * @return
     */
    public Map<String, PatientCommunityEnrollment> findAllCandidates(long communityId);
    
    /**
     * Find enrolled.
     *
     * Returns a map: key=euid, value=patientEnrollment
     *
     * @return
     */
    public Map<String, PatientCommunityEnrollment> findAllEnrolledPatients(long communityId);
    
    /**
     * Find enrolled.
     *
     * Returns a map: key=euid, value=patientEnrollment
     *
     * @return
     */
    public Map<String, PatientCommunityEnrollment> findAllEnrolledPatientsOfOrg(long communityId,long currentUserOrgId);
    
    /**
     * Find enrolled patients.
     *
     * Returns a map: key=euid, value=patientEnrollment
     *
     * @param queryString
     * @return
     */
    public   List<PatientReportSheet> findEnrolledPatients(long communityId,long userId, long accessLevelId);

    /**
     * Find enrolled patients.
     *
     * Returns a map: key=euid, value=patientEnrollment
     *
     * @param queryString
     * @return
     */
    public Map<String, PatientCommunityEnrollment> findEnrolledPatients(long communityId,long currentUserOrgId);
    
    public int getTotalCountForEnrolledPatients(long communityId,long userId, long accessLevelId);

    public Map<String, PatientCommunityEnrollment> findEnrolledPatients(long communityId,long userId, long accessLevelId, int pageNum, int pageSize);

    public int getTotalCountForCandidatePatientsNotEnrolledPatients(long communityId,long currentUserOrgId);

    public Map<String, PatientCommunityEnrollment> findCandidatePatientsNotEnrolledPatients(long communityId,long currentUserOrgId, int pageNum, int pageSize);
    
    public int findPatientAssignedCount(long communityId);
    
    public Map<String, PatientCommunityEnrollment> findCandidatePatientsNotEnrolledPatients(long communityId,int pageNum, int pageSize);
    
    public int getTotalCountForCandidatePatientsNotEnrolledPatients(long communityId);
    
    //Healrh Home field of report
    public List<ProgramHealthHome> findProgramHealthHomeEntities();
    
    public Map<String, PatientCommunityEnrollment> findTotalPatientsForPowerUser(long communityId, int pageNum, int pageSize);

    public Map<String, PatientCommunityEnrollment> findTotalPatientsForPowerUser(long communityId);

    public Map<String, PatientCommunityEnrollment> findPatientsForNotPowerUser(long userId, long communityId, int pageNum, int pageSize);

    public Map<String, PatientCommunityEnrollment> findPatientsForNotPowerUser(long userId, long communityId);

    public int CountPatientsForPowerUser(long communityId);

    public int CountPatientsForNotPowerUser(long userId, long communityId);

    public PatientCommunityEnrollment findPatientEnrollment(String euid, long communityId) throws NoResultException;

    public List<PatientTrackingSheet> findPTSRecordsForPowerUser(long communityId, Long programId, SearchRange<Date> search, String sqlQuery);

    public List<PatientReportSheet> findNonConsentedRecordsForPowerUser(long communityId,int pageNum, int pageSize);

    public List<PatientReportSheet> findNonConsentedRecordsForNonPowerUser(long communityId, long userId,int pageNum, int pageSize);

    public List<PatientTrackingSheet> findPTSRecordsForSuperUser(long communityId, Long programId, long userId,SearchRange<Date> search, String sqlQuery);

    public List<PatientTrackingSheet> findPTSRecordsForLocalAdminUser(long communityId, Long programId, long userId,SearchRange<Date> search, String sqlQuery);

    public Long findPTSCountForPowerUser(long communityId);

    public Long findPTSCountForSuperUser(long communityId, long userId);

    public Long findPTSCountForLocalAdminUser(long communityId, long userId);

    public List<PatientProgramSheet> findPatientProgramRecordsForPowerUser(long communityId, SearchCriteria searchCriteria,PatientManager patientManager, int pageNum, int pageSize);

    public List<PatientProgramSheet> findPatientProgramRecordsForNonPowerUser(long communityId, long userId, SearchCriteria searchCriteria,PatientManager patientManager,int pageNum, int pageSize);

    public int findTotalCountNonConsentedRecordsForNonPowerUser(long communityId, long userId);

    public int findTotalCountNonConsentedRecordsForPowerUser(long communityId);

    public int findTotalCountPatientProgramRecordsForPowerUser(long communityId,PatientManager patientManager, SearchCriteria searchCriteria);

    public int findTotalCountPatientProgramRecordsForNonPowerUser(long communityId, long userId,PatientManager patientManager, SearchCriteria searchCriteria);
    
    public Map<String, SimplePerson> getPatientsFromMdm(PatientManager patientManager,List<String> patientIds,String communityOid);

    public Program getProgram(Long progId, long communityId)throws Exception;
}
