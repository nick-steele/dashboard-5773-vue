package com.gsihealth.dashboard.server.dao.util;

import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Chad Darby
 */
public class DaoUtils {

    /**
     * Builds a collection for the IDs:  output: "(id1, id2, id3, ...)"
     *
     * @param theList
     * @return
     */
    public static String buildInClause(List<?> theList) {
        return build(theList, false);
    }

    public static String buildInClauseQuoted(List<?> theList) {
        return build(theList, true);
    }

    protected static String build(List<?> theList, boolean quoted) {
        StringBuilder result = new StringBuilder();

        result.append("(");
        boolean first = true;
        for (Object temp : theList) {

            StringBuilder data = new StringBuilder();
            if (!first) {
                data.append(", ");
            } else {
                first = false;
            }

            result.append(data);

            if (quoted) {
                // add quotes
                result.append("'");
                result.append(temp);
                result.append("'");
            } else {
                result.append(temp);
            }
        }

        result.append(")");

        return result.toString();
    }

    public static String quote(String data) {

        StringBuilder result = new StringBuilder();
        result.append("'");
        result.append(data);
        result.append("'");

        return result.toString();
    }

    public static String stripLeadingZeros(String data) {
        String actual;
        
        if (data.length() > 1) {
            actual = StringUtils.stripStart(data, "0");
        }
        else {
            actual = data;
        }

        return actual;
    }
}
