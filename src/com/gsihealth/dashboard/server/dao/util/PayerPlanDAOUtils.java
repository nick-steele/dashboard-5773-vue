package com.gsihealth.dashboard.server.dao.util;

import com.gsihealth.dashboard.common.PayerPlanSearchCriteria;
import com.gsihealth.entity.insurance.PayerPlan;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Chad Darby
 */
public class PayerPlanDAOUtils {

    public static String buildWhereClause(PayerPlanSearchCriteria searchCriteria) {

        StringBuilder whereClause = new StringBuilder();

        // build up query string based on search criteria
        String payerPlanId = searchCriteria.getPayerPlanId();
        String mmisId = searchCriteria.getMmisid();

        boolean hasPayerPlanId = StringUtils.isNotBlank(payerPlanId);
        boolean hasMmisId = StringUtils.isNotBlank(mmisId);

        if (hasPayerPlanId || hasMmisId) {
            whereClause.append(" where");

            if (hasPayerPlanId) {
                whereClause.append((" o.id=" + payerPlanId));
            }

            if (hasPayerPlanId && hasMmisId) {
                whereClause.append(" and");
            }

            if (hasMmisId) {
                whereClause.append((" o.mmisId='" + mmisId + "'"));
            }
        }

        return whereClause.toString();
    }

    public static String buildWhereClauseCheckForDuplicates(PayerPlan payerPlan) {

        StringBuilder whereClause = new StringBuilder();

        // build up query string based on search criteria
        Long payerPlanId = null;
        String mmisId = null;
        String name = null;

        System.out.println("payerPlan=" + payerPlan);

        if (payerPlan != null) {
            
            if (payerPlan.getPayerPlanPK() != null) {
                payerPlanId = payerPlan.getPayerPlanPK().getId();
            }
            
            mmisId = payerPlan.getMmisId();
            name = payerPlan.getName();
        }

        boolean hasPayerPlanId = payerPlanId != null;
        boolean hasMmisId = StringUtils.isNotBlank(mmisId);
        boolean hasName = StringUtils.isNotBlank(name);

        if (hasName || hasMmisId) {
            whereClause.append(" where");

            whereClause.append(" (");

            if (hasName) {
                whereClause.append("o.name=" + DaoUtils.quote(name));
            }

            if (hasName && hasMmisId) {
                whereClause.append(" or ");
            }

            if (hasMmisId) {
                whereClause.append("o.mmisId=" + DaoUtils.quote(mmisId));
            }

            whereClause.append(")");

            if (hasPayerPlanId) {
                whereClause.append((" and o.id<>" + payerPlanId));
            }
        }

        return whereClause.toString();
    }

    public static void movePayerPlanToEnd(LinkedHashMap<String, String> data, String payerPlanName) {

        // remove the payerPlanName
        Set<Entry<String, String>> entrySet = data.entrySet();
        String theKey = null;

        boolean foundPlanName = false;

        for (Entry<String, String> tempEntry : entrySet) {

            if (StringUtils.equalsIgnoreCase(tempEntry.getValue(), payerPlanName)) {
                theKey = tempEntry.getKey();
                entrySet.remove(tempEntry);
                foundPlanName = true;
                break;
            }
        }

        // if we found the planName, then
        if (foundPlanName) {
            data.put(theKey, payerPlanName);
        }
    }
}
