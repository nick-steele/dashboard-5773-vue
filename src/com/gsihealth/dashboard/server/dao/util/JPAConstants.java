package com.gsihealth.dashboard.server.dao.util;

/**
 * JIRA 870 (JPA)
 * Copy the constants over from EclipseLink to remove the direct dependency on the EclipseLink API
 * @author Ranga
 */
public class JPAConstants 
{
    public static final String REFRESH = "eclipselink.refresh";
    public static final String TRUE = "True";
}
