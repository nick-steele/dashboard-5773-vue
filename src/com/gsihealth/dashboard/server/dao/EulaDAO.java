package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.Eula;

/**
 *
 * @author Chad Darby
 */
public interface EulaDAO {

    public Eula findActiveEula(Long communityId);

}
