/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.Community;
import com.gsihealth.entity.CommunityOrganization;
import com.gsihealth.entity.OrganizationCwd;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;

/**
 *
 * @author User
 */
public class CommunityOrganizationDAOImpl implements CommunityOrganizationDAO{

    private EntityManagerFactory emf = null;

    public CommunityOrganizationDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public CommunityOrganizationDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CommunityOrganization communityOrganization) {
    
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            em.persist(communityOrganization);

            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }    
    }

	public void update(CommunityOrganization communityOrganization) {
		EntityManager em = null;
		try {
			em = getEntityManager();
			em.getTransaction().begin();
			
			em.merge(communityOrganization);

			em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	public CommunityOrganization findCommunityOrganization(Community community, OrganizationCwd organization) {
        EntityManager em = getEntityManager();
        CommunityOrganization theCommunityOrganization = null;
        try {
            Query q = em.createQuery("select object(o) from CommunityOrganization as o "
                    + "where o.community= :community and o.organizationCwd= :organization");
            q.setParameter("community", community);
            q.setParameter("organization", organization);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            theCommunityOrganization = (CommunityOrganization) q.getSingleResult();
        } finally {
            em.close();
        }
        return theCommunityOrganization;
    }

	public CommunityOrganization findCommunityOrganization(long communityId, long orgId) {
        EntityManager em = getEntityManager();
        CommunityOrganization theCommunityOrganization = null;
        try {
        	Query q = em.createNamedQuery("CommunityOrganization.findCommunityOrgs")
						.setParameter("communityId", communityId)
						.setParameter("orgId", orgId)
						.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
        	
            theCommunityOrganization = (CommunityOrganization) q.getSingleResult();
        } finally {
            em.close();
        }
        return theCommunityOrganization;
    }

	public List<CommunityOrganization> findCommunityOrganizationEntities() {
        return findCommunityOrganizationEntities(true, -1, -1);
    }

    public List<CommunityOrganization> findCommunityOrganizationEntities(int maxResults, int firstResult) {
        return findCommunityOrganizationEntities(false, maxResults, firstResult);
    }

    private List<CommunityOrganization> findCommunityOrganizationEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from User as o where o.isDeleted = 'N' or o.isDeleted is null");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CommunityOrganization findCommunityOrganization(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CommunityOrganization.class, id);
        } finally {
            em.close();
        }
    }

    public int getCommunityOrganizationCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from User as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
 @Override
    public List<Long> getAutoConsentOrganizationId(long communityId, char autoConsentStatus){
        
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select o.organizationCwd.orgId from CommunityOrganization as o where o.community.communityId=:theCommunity and o.autoConsent=:theAutoConsent");
            q.setParameter("theCommunity", communityId);
            q.setParameter("theAutoConsent", autoConsentStatus);
            return (List<Long>)q.getResultList();
        } finally {
            em.close();
        } 
    }

}
