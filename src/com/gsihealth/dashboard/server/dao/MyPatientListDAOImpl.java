package com.gsihealth.dashboard.server.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gsihealth.dashboard.common.PatientIdList;
import com.gsihealth.dashboard.common.Patient;
import com.gsihealth.dashboard.common.PatientDemogList;
import com.gsihealth.dashboard.common.ViewPatientDemog;
import com.gsihealth.dashboard.entity.dto.ListTagDTO;
import com.gsihealth.dashboard.entity.dto.MyPatientInfoDTO;
import com.gsihealth.dashboard.rest.service.JacksonContextResolver;
import com.gsihealth.dashboard.rest.service.PatientMDMInterfaceFactory;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.RollbackFailureException;
import com.gsihealth.dashboard.server.dao.util.DaoUtils;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import com.gsihealth.entity.Community;
import com.gsihealth.entity.ListTags;
import com.gsihealth.entity.MyPatientInfo;
import com.gsihealth.entity.MyPatientInfoPK;
import com.gsihealth.entity.PatientCommunityCareteam;
import com.gsihealth.entity.PatientListTags;
import com.gsihealth.entity.UserPatientListTags;
import com.gsihealth.patientmdminterface.PatientInterface;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import com.gsihealth.patientmdminterface.util.PatientUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import org.apache.commons.lang.time.StopWatch;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author Chad.Darby
 */
public class MyPatientListDAOImpl implements Serializable, MyPatientListDAO {

    private EntityManagerFactory emf = null;
    private Logger logger = Logger.getLogger(getClass().getName());

    public MyPatientListDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public MyPatientListDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    private EntityManager getEntityManager() {

        return emf.createEntityManager();
    }

    private PatientInterface getPmi(long communityId) {
        PatientInterface pmi = PatientMDMInterfaceFactory.getPatientInterface(communityId);

        return pmi;
    }

    @Override
    public void add(MyPatientInfo myPatientInfo) throws Exception {

        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        Collection<UserPatientListTags> tags = myPatientInfo.getUserPatientListTagsCollection();
        Collection<UserPatientListTags> ptags = new ArrayList();
        Collection<UserPatientListTags> uptags = new ArrayList();
        for (UserPatientListTags tag : tags) {

            String test = getListTagById(em, tag.getMyPatientList().getMyPatientInfoPK().getCommunityId(), tag.getTagId().getTagId()).getTagType();
            if (test.equals("EVENT") || test.equals("MINOR")) {
                ptags.add(tag);

            } else {
                uptags.add(tag);
            }

        }

        tags.clear();

        try {
            utx.begin();
            myPatientInfo.setPatientUpdateDateTime(new Date());
            em.persist(myPatientInfo);
            addPatientTags(em, ptags);
            addUserPatientTags(em, uptags);
            utx.commit();
        } catch (Exception ex) {
            try {
                ex.printStackTrace();
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the merge transaction.", re);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void update(MyPatientInfo myPatientInfo) throws Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        Collection<UserPatientListTags> tags = myPatientInfo.getUserPatientListTagsCollection();
        Collection<UserPatientListTags> ptags = new ArrayList();
        Collection<UserPatientListTags> uptags = new ArrayList();
        for (UserPatientListTags tag : tags) {

            String test = getListTagById(em, tag.getMyPatientList().getMyPatientInfoPK().getCommunityId(), tag.getTagId().getTagId()).getTagType();
            if (test.equals("EVENT") || test.equals("MINOR")) {
                ptags.add(tag);

            } else {
                uptags.add(tag);
            }

        }

        tags.clear();

        try {
            utx.begin();
            myPatientInfo.setPatientUpdateDateTime(new Date());
            em.merge(myPatientInfo);
            addPatientTags(em, ptags);
            addUserPatientTags(em, uptags);
            utx.commit();
        } catch (Exception ex) {
            try {
                ex.printStackTrace();
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the merge transaction.", re);
            }

        } finally {
            if (em != null) {
                em.close();
            }
        }

    }

    @Override
    public void delete(MyPatientInfoPK id) throws Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();
        try {
            utx.begin();
            MyPatientInfo myPatientInfo;
            try {
                myPatientInfo = em.getReference(MyPatientInfo.class, id);
                myPatientInfo.getMyPatientInfoPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The myPatientInfo with id " + id + " no longer exists.", enfe);
            }
            em.remove(myPatientInfo);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public MyPatientInfo findMyPatientInfo(MyPatientInfoPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(MyPatientInfo.class, id);
        } finally {
            em.close();
        }
    }

    @Override
    public List<MyPatientInfo> findMyPatientInfoEntities(long userId, long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from MyPatientInfo as o where o.myPatientInfoPK.userId = :theUserId and o.myPatientInfoPK.communityId = :theComnunityId");

            q.setParameter("theUserId", userId);
            q.setParameter("theComnunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public void updateTags(long userId, long communityId, String type, String careteamId) {
        EntityManager em = getEntityManager();

        updateTags(em, communityId, type, careteamId);
    }

    private void addPatientTags(EntityManager em, Collection<UserPatientListTags> tags) {

        PatientListTags plt = null;
        try {

            for (UserPatientListTags tag : tags) {
                plt = new PatientListTags();
                plt.setCommunityId(tag.getMyPatientList().getMyPatientInfoPK().getCommunityId());
                plt.setPatientId(tag.getMyPatientList().getMyPatientInfoPK().getPatientId());
                plt.setTagId(tag.getTagId());

                if (!patientTagIsPresent(plt)) {

                    em.persist(plt);

                }

            }

        } catch (Exception ex) {
            logger.severe(" addPatientTags commit failure for " + plt.toString());

            throw ex;
        }
    }

    private void addUserPatientTags(EntityManager em, Collection<UserPatientListTags> tags) {

        UserPatientListTags tag = null;
        try {

            for (UserPatientListTags itag : tags) {
                tag = itag;
                if (!userPatientTagIsPresent(itag)) {

                    em.persist(itag);

                }

            }

        } catch (Exception ex) {
            logger.severe(" addUserPatientTags commit failure for " + tag.toString());

            throw ex;
        }
    }

    private boolean patientTagIsPresent(PatientListTags plt) {
        EntityManager em = getEntityManager();

        boolean result = false;
        try {
            String sql = "select count(*) from connect.patient_list_tags where"
                    + " patient_id = " + plt.getPatientId()
                    + " and community_id = " + plt.getCommunityId()
                    + " and tag_id  =  " + plt.getTagId().getTagId();

            Query query = em.createNativeQuery(sql);
            //patientQuery.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            long count = (Long) query.getSingleResult();
            result = count > 0;

        } finally {
            em.close();
        }

        return result;

    }

    private boolean userPatientTagIsPresent(UserPatientListTags uplt) {
        EntityManager em = getEntityManager();

        boolean result = false;
        try {
            String sql = "select count(*) from connect.user_patient_list_tags where"
                    + " user_id = " + uplt.getMyPatientList().getMyPatientInfoPK().getUserId()
                    + " and patient_id = " + uplt.getMyPatientList().getMyPatientInfoPK().getPatientId()
                    + " and community_id = " + uplt.getMyPatientList().getMyPatientInfoPK().getCommunityId()
                    + " and tag_id  =  " + uplt.getTagId().getTagId();

            Query query = em.createNativeQuery(sql);
            //patientQuery.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            long count = (Long) query.getSingleResult();
            result = count > 0;

        } finally {
            em.close();
        }

        return result;

    }

    private List<MyPatientInfo> findUserPatientInfoEntities(long userId, long communityId, long start, long stop, String level, String tags, PatientIdList filterPatients) {
        EntityManager em = getEntityManager();
        List<MyPatientInfo> ret = new ArrayList();
        try {

            if (!hasUserPatientTags(em, communityId, tags)) {
                return ret;
            }
            //     updateTags(em, userId, communityId, level);
            String users = Long.toString(userId);

            String select = "SELECT DISTINCT m.* FROM my_patient_list m INNER JOIN patient_community_enrollment pc ON (m.patient_id = pc.patient_id ) ";
            String where = " WHERE  m.community_id = " + communityId + " AND m.user_id IN(" + users + ") ";
            String jtemplate = " INNER JOIN user_patient_list_tags X ON (m.user_id = X.user_id AND m.patient_id = X.patient_id AND m.community_id = X.community_id) ";
            String ojtemplate = " LEFT OUTER JOIN user_patient_list_tags X ON (m.user_id = X.user_id AND m.patient_id = X.patient_id AND m.community_id = X.community_id) ";
            String atemplate = " AND X.tag_id = Y ";
            String ntemplate = " AND X.tag_id is NULL ";
            String sqltags = "";
            String ands = "";
            String consent = "";

            if (!isPowerUser(communityId, userId)) {
                consent = " INNER JOIN VIEW_USER_PATIENT_CONSENT v ON (v.patient_id = m.patient_id and v.user_id = m.user_id and v.community_id = m.community_id) ";
            }

            if (filterPatients != null && !filterPatients.isEmpty()) {
                String filter = "";
                for (Patient id : filterPatients) {
                    filter = filter + id.getId() + ",";
                }
                filter = filter.substring(0, filter.length() - 1);
                where = where + " and m.patient_id in (" + filter + ") ";
            }
            if (tags.equals("null")) {
                sqltags = ojtemplate;
                ands = ntemplate;
            } else if (!tags.equals("all")) {
                String[] subs = tags.split(",");
                for (String ind : subs) {
                    if (!ind.equals("null")) {
                        String test = getListTagById(em, communityId, new Long(ind)).getTagType();
                        if (!test.equals("EVENT") && !test.equals("MINOR")) {
                            String jsub = "X" + ind;
                            sqltags = sqltags + jtemplate.replaceAll("X", jsub);

                            ands = ands + atemplate.replaceAll("X", jsub).replaceAll("Y", ind);
                        }
                    }

                }
            }

            String sql = select + consent + sqltags + where + ands;
            sql += " order by m.PATIENT_UPDATE_DATE_TIME desc ";

            System.out.println(sql);
            Query q = em.createNativeQuery(sql, MyPatientInfo.class);

            int to = new Long(stop).intValue();
            int from = new Long(start).intValue();
            q.setMaxResults(to - from + 1);
            q.setFirstResult(from);

            ret = q.getResultList();
            //vpl    addDemogs(em, communityId, ret);
        } catch (Exception x) {
            x.printStackTrace();
        } finally {
            em.close();
            return ret;
        }
    }

    private List<MyPatientInfo> findPatientInfoEntities(long userId, long communityId, String level, String tags, PatientIdList filterPatients) {
        EntityManager em = getEntityManager();
        List<MyPatientInfo> ret = new ArrayList();
        // if (!hasPatientTags(em, communityId, tags)) {
        //     return ret;
        // }
        try {

            String select = "SELECT DISTINCT m.* FROM patient_list_tags m INNER JOIN my_patient_list pc ON (m.patient_id = pc.patient_id and m.community_id = pc.community_id) ";
            String where = " WHERE  m.community_id = " + communityId + " AND pc.user_id IN(" + userId + ") ";
            String jtemplate = " INNER JOIN patient_list_tags X ON ( m.patient_id = X.patient_id AND m.community_id = X.community_id) ";
            String ojtemplate = " LEFT OUTER JOIN patient_list_tags X ON ( m.patient_id = X.patient_id AND m.community_id = X.community_id) ";
            String atemplate = " AND X.tag_id = Y ";
            String ntemplate = " AND X.tag_id is NULL ";
            String sqltags = "";
            String ands = "";
            String consent = "";

            if (!isPowerUser(communityId, userId)) {
                consent = " INNER JOIN VIEW_USER_PATIENT_CONSENT v ON (v.patient_id = m.patient_id  and v.community_id = m.community_id and v.user_id = " + userId + " )";
            }

            if (filterPatients != null && !filterPatients.isEmpty()) {
                String filter = "";
                for (Patient id : filterPatients) {
                    filter = filter + id.getId() + ",";
                }
                filter = filter.substring(0, filter.length() - 1);
                where = where + " and m.patient_id in (" + filter + ") ";
            }
            if (tags.equals("null")) {
                sqltags = ojtemplate;
                ands = ntemplate;
            } else if (!tags.equals("all")) {
                String[] subs = tags.split(",");
                for (String ind : subs) {
                    if (!ind.equals("null")) {
                        String test = getListTagById(em, communityId, new Long(ind)).getTagType();
                        if (test.equals("EVENT") || test.equals("MINOR")) {
                            String jsub = "X" + ind;
                            sqltags = sqltags + jtemplate.replaceAll("X", jsub);

                            ands = ands + atemplate.replaceAll("X", jsub).replaceAll("Y", ind);
                        }
                    }
                }

            }

            String sql = select + consent + sqltags + where + ands;
            sql += " order by pc.PATIENT_UPDATE_DATE_TIME desc ";

            System.out.println(sql);
            Query q = em.createNativeQuery(sql, PatientListTags.class);

            //      int to = new Long(stop).intValue();
            //      int from = new Long(start).intValue();
            //      q.setMaxResults(to - from + 1);
            //       q.setFirstResult(from);
            List<PatientListTags> patients = q.getResultList();
            LinkedHashMap<Long, List<PatientListTags>> map = new LinkedHashMap();
            for (PatientListTags patient : patients) {
                List<PatientListTags> ltags = new ArrayList();
                Long patientId = patient.getPatientId();
                if (map.containsKey(patientId)) {
                    ltags = map.get(patientId);
                } else {
                    map.put(patientId, ltags);
                }
                ltags.add(patient);

            }
            for (Long patientId : map.keySet()) {

                MyPatientInfo mpi = new MyPatientInfo(userId, patientId, communityId);
                ArrayList<UserPatientListTags> upltl = new ArrayList();
                mpi.setUserPatientListTagsCollection(upltl);
                List<PatientListTags> pltags = map.get(patientId);
                for (PatientListTags plt : pltags) {
                    UserPatientListTags uplt = new UserPatientListTags();
                    uplt.setMyPatientList(mpi);
                    uplt.setTagId(plt.getTagId());
                    upltl.add(uplt);
                }
                ret.add(mpi);
            }

            //vpl addDemogs(em, communityId, ret);
        } catch (Exception x) {
            x.printStackTrace();
        } finally {
            em.close();
            return ret;
        }
    }

    private int mergeType(long communityId, String tags) {
        EntityManager em = getEntityManager();
        int type1 = 0;
        int type2 = 0;
        if (tags.equals("all")) {
            return 0;
        }
        if (tags.equals(" null")) {
            return 3;
        }
        String[] subs = tags.split(",");
        for (String ind : subs) {
            if (!ind.equals("null")) {
                String test = getListTagById(em, communityId, new Long(ind)).getTagType();
                if (test.equals("EVENT") || test.equals("MINOR")) {
                    type2 = 1;
                } else {
                    type1 = 1;
                }
            }

        }
        int ret = 0;
        if ((type1 + type2) == 2) {
            ret = 3;
        } else if (type1 == 1) {
            ret = 1;
        } else if (type2 == 1) {
            ret = 2;
        }
        return ret;
    }

    private boolean hasPatientTags(EntityManager em, Long communityId, String tags) {
        if (tags.equals("null")) {
            return false;
        } else if (tags.equals("all")) {
            return true;
        } else {
            String[] subs = tags.split(",");
            for (String ind : subs) {
                if (!ind.equals("null")) {
                    String test = getListTagById(em, communityId, new Long(ind)).getTagType();
                    if (test.equals("EVENT") || test.equals("MINOR")) {
                        return true;

                    }
                }
            }
        }
        return false;
    }

    private boolean hasUserPatientTags(EntityManager em, Long communityId, String tags) {
        if (tags.equals("null")) {
            return true;
        } else if (tags.equals("all")) {
            return true;
        } else {
            String[] subs = tags.split(",");
            for (String ind : subs) {
                if (!ind.equals("null")) {
                    String test = getListTagById(em, communityId, new Long(ind)).getTagType();
                    if (!test.equals("EVENT") && !test.equals("MINOR")) {
                        return true;

                    }
                }
            }
        }
        return false;
    }

    private List<MyPatientInfo> findPatientTagOrgEntities(long userId, long communityId, long start, long stop, String level, String tags, PatientIdList filterPatients) {
        EntityManager em = getEntityManager();
        List<MyPatientInfo> ret = new ArrayList();
        try {

            String orgId = getOrgId(communityId, userId);

            String select = "SELECT DISTINCT m.* FROM patient_list_tags m INNER JOIN patient_community_enrollment pc ON (m.patient_id = pc.patient_id ) ";
            String where = " WHERE  m.community_id = " + communityId + " AND pc.org_id IN(" + orgId + ") ";
            String jtemplate = " INNER JOIN patient_list_tags X ON ( m.patient_id = X.patient_id AND m.community_id = X.community_id) ";
            String ojtemplate = " LEFT OUTER JOIN patient_list_tags X ON ( m.patient_id = X.patient_id AND m.community_id = X.community_id) ";
            String atemplate = " AND X.tag_id = Y ";
            String ntemplate = " AND X.tag_id is NULL ";
            String sqltags = "";
            String ands = "";
            String consent = "";

            if (!isPowerUser(communityId, userId)) {
                consent = " INNER JOIN  ORGANIZATION_PATIENT_CONSENT o ON (o.patient_id = m.patient_id and o.organization_id = " + orgId + " and o.community_id = m.community_id) ";
            }

            if (filterPatients != null && !filterPatients.isEmpty()) {
                String filter = "";
                for (Patient id : filterPatients) {
                    filter = filter + id.getId() + ",";
                }
                filter = filter.substring(0, filter.length() - 1);
                where = where + " and m.patient_id in (" + filter + ") ";
            }
            if (tags.equals("null")) {
                sqltags = ojtemplate;
                ands = ntemplate;
            } else if (!tags.equals("all")) {
                String[] subs = tags.split(",");
                for (String ind : subs) {
                    if (!ind.equals("null")) {
                        String jsub = "X" + ind;
                        sqltags = sqltags + jtemplate.replaceAll("X", jsub);

                        ands = ands + atemplate.replaceAll("X", jsub).replaceAll("Y", ind);
                    }
                }
            }

            String sql = select + consent + sqltags + where + ands;
            sql += " order by pc.LAST_MODIFIED_DATE desc ";

            System.out.println(sql);
            Query q = em.createNativeQuery(sql, PatientListTags.class);

            int to = new Long(stop).intValue();
            int from = new Long(start).intValue();
            q.setMaxResults(to - from + 1);
            q.setFirstResult(from);
            List<PatientListTags> patients = q.getResultList();
            LinkedHashMap<Long, List<PatientListTags>> map = new LinkedHashMap();
            for (PatientListTags patient : patients) {
                List<PatientListTags> ltags = new ArrayList();
                Long patientId = patient.getPatientId();
                if (map.containsKey(patientId)) {
                    ltags = map.get(patientId);
                } else {
                    map.put(patientId, ltags);
                }
                ltags.add(patient);

            }
            for (Long patientId : map.keySet()) {

                MyPatientInfo mpi = new MyPatientInfo(userId, patientId, communityId);
                ArrayList<UserPatientListTags> upltl = new ArrayList();
                mpi.setUserPatientListTagsCollection(upltl);
                List<PatientListTags> pltags = map.get(patientId);
                for (PatientListTags plt : pltags) {
                    UserPatientListTags uplt = new UserPatientListTags();
                    uplt.setMyPatientList(mpi);
                    uplt.setTagId(plt.getTagId());
                    upltl.add(uplt);
                }

                ret.add(mpi);
            }

            //addDemogs(em, communityId, ret);vpl
        } catch (Exception x) {
            x.printStackTrace();
        } finally {
            em.close();
            return ret;
        }
    }

    private List<MyPatientInfo> findPatientTagOrgEntitiesCount(long userId, long communityId, String level, String tags, PatientIdList filterPatients) {
        EntityManager em = getEntityManager();
        List<MyPatientInfo> ret = new ArrayList();
        try {

            String orgId = getOrgId(communityId, userId);

            String select = "SELECT DISTINCT m.* FROM patient_list_tags m INNER JOIN patient_community_enrollment pc ON (m.patient_id = pc.patient_id ) ";
            String where = " WHERE  m.community_id = " + communityId + " AND pc.org_id IN(" + orgId + ") ";
            String jtemplate = " INNER JOIN patient_list_tags X ON ( m.patient_id = X.patient_id AND m.community_id = X.community_id) ";
            String ojtemplate = " LEFT OUTER JOIN patient_list_tags X ON ( m.patient_id = X.patient_id AND m.community_id = X.community_id) ";
            String atemplate = " AND X.tag_id = Y ";
            String ntemplate = " AND X.tag_id is NULL ";
            String sqltags = "";
            String ands = "";
            String consent = "";

            if (!isPowerUser(communityId, userId)) {
                consent = " INNER JOIN  ORGANIZATION_PATIENT_CONSENT o ON (o.patient_id = m.patient_id and o.organization_id = " + orgId + " and o.community_id = m.community_id) ";
            }

            if (filterPatients != null && !filterPatients.isEmpty()) {
                String filter = "";
                for (Patient id : filterPatients) {
                    filter = filter + id.getId() + ",";
                }
                filter = filter.substring(0, filter.length() - 1);
                where = where + " and m.patient_id in (" + filter + ") ";
            }
            if (tags.equals("null")) {
                sqltags = ojtemplate;
                ands = ntemplate;
            } else if (!tags.equals("all")) {
                String[] subs = tags.split(",");
                for (String ind : subs) {
                    if (!ind.equals("null")) {
                        String jsub = "X" + ind;
                        sqltags = sqltags + jtemplate.replaceAll("X", jsub);

                        ands = ands + atemplate.replaceAll("X", jsub).replaceAll("Y", ind);
                    }
                }
            }

            String sql = select + consent + sqltags + where + ands;
            sql += " order by pc.LAST_MODIFIED_DATE desc ";

            System.out.println(sql);
            Query q = em.createNativeQuery(sql, PatientListTags.class);

            List<PatientListTags> patients = q.getResultList();
            LinkedHashMap<Long, List<PatientListTags>> map = new LinkedHashMap();
            for (PatientListTags patient : patients) {
                List<PatientListTags> ltags = new ArrayList();
                Long patientId = patient.getPatientId();
                if (map.containsKey(patientId)) {
                    ltags = map.get(patientId);//ltags is slightly useless
                } else {
                    map.put(patientId, ltags);
                }
                ltags.add(patient);

            }
            for (Long patientId : map.keySet()) {

                MyPatientInfo mpi = new MyPatientInfo(userId, patientId, communityId);

                ret.add(mpi);
            }

        } catch (Exception x) {
            x.printStackTrace();
        } finally {
            em.close();
            return ret;
        }
    }

    private void updateTags(EntityManager em, long communityId, String type, String careteamId) {

        if (type.equals("MINOR")) {
            //implement for all
            minorTagRemoval(em, communityId);
            minorTagInserts(em, communityId);
        } else if (type.equals("EVENT")) {
            //implement for all
            eventExpiredTagRemoval(em, communityId);
        } else if (type.equals("CARETEAM")) {

            logger.log(Level.INFO, "MADE IT INTO CARETEAM TAG UPDATE");
            careTeamTagRemoval(em, Long.parseLong(careteamId), communityId);
            careTeamTagInserts(em, Long.parseLong(careteamId), communityId);
        }

    }

    private void minorTagRemoval(EntityManager em, long communityId) {
        try {

            String delete = "delete from patient_list_tags ";
            String tag = Long.toString(getMinorTag(em, communityId).getTagId());

            String where = " WHERE community_id = " + communityId;

            String ands = " and tag_id = " + tag;
            String sql = delete + where + ands;
            System.out.println(sql);
            EntityTransaction utx = em.getTransaction();
            try {
                utx.begin();
                em.createNativeQuery(sql).executeUpdate();
                utx.commit();
            } catch (Exception ex) {
                try {
                    ex.printStackTrace();
                    utx.rollback();
                } catch (Exception re) {
                    throw new RollbackFailureException("An error occurred attempting to roll back the delete transaction " + sql, re);
                }
            }
        } catch (Exception x) {
            x.printStackTrace();
        }

    }

    private void eventExpiredTagRemoval(EntityManager em, long communityId) {
        try {
            String delete = "delete from patient_list_tags ";
            String tag = Long.toString(getEventTag(em, communityId).getTagId());
            String exp = getEventExpire(em, communityId);

            String where = " WHERE community_id = " + communityId + " and DATEDIFF(NOW(),LAST_MODIFIED_DATE_TIME) > " + exp;

            String ands = " and tag_id = " + tag;
            String sql = delete + where + ands;
            System.out.println(sql);
            EntityTransaction utx = em.getTransaction();
            try {
                utx.begin();
                em.createNativeQuery(sql).executeUpdate();
                utx.commit();
            } catch (Exception ex) {
                try {
                    ex.printStackTrace();
                    utx.rollback();
                } catch (Exception re) {
                    throw new RollbackFailureException("An error occurred attempting to roll back the delete transaction " + sql, re);
                }
            }

        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    private void careTeamTagRemoval(EntityManager em, long careTeamId, long communityId) {
        try {

            String delete = "delete from user_patient_list_tags ";
            String tag = Long.toString(getCareTeamTag(em, communityId).getTagId());

            String where = " WHERE community_id = " + communityId + " AND patient_id IN("
                    + "select patient_id from patient_community_careteam where community_careteam_id = " + careTeamId + ")";

            String ands = " and tag_id = " + tag;
            String sql = delete + where + ands;
            System.out.println(sql);
            EntityTransaction utx = em.getTransaction();
            try {
                utx.begin();
                em.createNativeQuery(sql).executeUpdate();
                utx.commit();
            } catch (Exception ex) {
                try {
                    ex.printStackTrace();
                    utx.rollback();
                } catch (Exception re) {
                    throw new RollbackFailureException("An error occurred attempting to roll back the delete transaction " + sql, re);
                }
            }

        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    private HashMap<Long,Date> getPatientsByCareteam(EntityManager em, Long careTeamId) {

        HashMap<Long,Date>  ret = new HashMap();
        String usql = "select *  from connect.patient_community_careteam  "
                + "where community_careteam_id  = " + careTeamId + " and  (END_DATE is NULL OR END_DATE > NOW())";

        Query q = em.createNativeQuery(usql,PatientCommunityCareteam.class);
        List<PatientCommunityCareteam> list = q.getResultList();
        for(PatientCommunityCareteam pcc :list){
            Date test = pcc.getStartDate();
            if(test==null){
				test=new Date();
			}
            ret.put(pcc.getPatientCommunityCareteamPK().getPatientId(),test);
        }
        return ret;
    }

    private List<Long> getUsersByCareteam(EntityManager em, Long careTeamId) {

        String usql = "select user_id from  connect.user_community_careteam  "
                + "where community_careteam_id  = " + careTeamId + " and  (END_DATE is NULL OR END_DATE > NOW())";

        Query q = em.createNativeQuery(usql);
        return q.getResultList();

    }

    private void addDemogs(EntityManager em, long comId, List<MyPatientInfo> mpis) {
        PatientInterface pmi = getPmi(comId);
        ArrayList<String> pats = new ArrayList();
        for (MyPatientInfo pat : mpis) {
            pats.add(new Long(pat.getMyPatientInfoPK().getPatientId()).toString());
        }
        String oid = getCommunityOid(em, comId);

        Map<String, SimplePerson> demogs = pmi.getMappedPatientsDirectQuery(em, pats, oid);
        for (MyPatientInfo pat : mpis) {
            String patId = (new Long(pat.getMyPatientInfoPK().getPatientId()).toString());
            SimplePerson sp = demogs.get(patId);
            pat.setFirstName(sp.getFirstName());
            pat.setLastName(sp.getLastName());
            pat.setDateOfBirth(sp.getDateOfBirth());
            pat.setGender(sp.getGenderCode());
        }
    }

    private HashMap<Long, Integer> getAges(EntityManager em, long comId) {

        String oid = getCommunityOid(em, comId);

        List<String> pats = getAllPatients(em, comId);
        PatientInterface pmi = getPmi(comId);
        Map<String, SimplePerson> demogs = pmi.getMappedPatientsDirectQuery(em, pats, oid);
        HashMap<Long, Integer> ret = new HashMap();
        Set patients = demogs.keySet();
        Iterator ip = patients.iterator();
        while (ip.hasNext()) {
            String spid = (String) ip.next();
            Long pid = Long.valueOf(spid);
            SimplePerson sp = (SimplePerson) demogs.get(spid);
            if (sp != null) {
                Date dob = sp.getDateOfBirth();
                Integer age = new Double(PatientUtils.getAge(dob)).intValue();
                ret.put(pid, age);
            } else {
                ret.put(pid, 0);
                logger.log(Level.WARNING, " patient not returned from mdm :" + pid);
            }
        }
        return ret;
    }

    private List<String> getAllPatients(EntityManager em, Long comId) {
        List<String> pats = new ArrayList();

        String sql = "SELECT patient_id from connect.patient_community_enrollment where community_id = " + comId;

        Query q = em.createNativeQuery(sql);
        q.setParameter(1, comId);
        pats = q.getResultList();

        return pats;
    }

   private void careTeamTagInserts(EntityManager em, Long careTeamId, Long communityId) {
        HashMap<Long, Date> patientIds = getPatientsByCareteam(em, careTeamId);
        List<Long> userIds = getUsersByCareteam(em, careTeamId);
        Long tagId = getCareTeamTag(em, communityId).getTagId();
        EntityTransaction utx = em.getTransaction();
        try {
            utx.begin();
            for (Long userId : userIds) {
                for (Long patientId : patientIds.keySet()) {
                    Date date = patientIds.get(patientId);
                    
                  //  String sql1 = "INSERT INTO connect.MY_PATIENT_LIST(PATIENT_ID , USER_ID,COMMUNITY_ID,PATIENT_UPDATE_DATE_TIME) VALUES(?1,?2,?3,?4)"
                  //          + " ON DUPLICATE KEY UPDATE PATIENT_UPDATE_DATE_TIME = PATIENT_UPDATE_DATE_TIME";
                    
                  String sql1 = "INSERT INTO connect.MY_PATIENT_LIST(PATIENT_ID , USER_ID,COMMUNITY_ID,PATIENT_UPDATE_DATE_TIME) VALUES(?1,?2,?3,?4)"
                            + " ON DUPLICATE KEY UPDATE PATIENT_UPDATE_DATE_TIME = CASE WHEN PATIENT_UPDATE_DATE_TIME < ?4 " 
                            + " THEN ?4  ELSE PATIENT_UPDATE_DATE_TIME END";

                    Query q = em.createNativeQuery(sql1);
                    q.setParameter(1, patientId);
                    q.setParameter(2, userId);
                    q.setParameter(3, communityId);
                    q.setParameter(4, date);

                    q.executeUpdate();

                    String sql2 = "INSERT INTO connect.USER_PATIENT_LIST_TAGS(PATIENT_ID , USER_ID,COMMUNITY_ID, TAG_ID, STATUS) VALUES(?1,?2,?3,?4,'ENABLED')";

                    q = em.createNativeQuery(sql2);
                    q.setParameter(1, patientId);
                    q.setParameter(2, userId);
                    q.setParameter(3, communityId);
                    q.setParameter(4, tagId);
                    q.executeUpdate();
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                ex.printStackTrace();
                utx.rollback();
            } catch (Exception re) {
                logger.severe(" failure inserting careteam tags");
            }
        }

    }


    private void minorTagInserts(EntityManager em, long comId) {
        EntityTransaction utx = em.getTransaction();
        HashMap<Long, Integer> ages = getAges(em, comId);
        int minorAge = Integer.parseInt(getMinorAge(em, comId));
        Long mtagId = getMinorTag(em, comId).getTagId();
        // String tempTable = "PATIENT_AGE_" + new Date().getTime() + Thread.currentThread().getId();

        try {
            utx.begin();
            String values = " VALUES";

            Iterator<Long> ip = ages.keySet().iterator();
            while (ip.hasNext()) {
                Long pid = ip.next();
                Integer age = ages.get(pid);
                if (age < minorAge) {
                    values += "(" + pid + "," + comId + "," + mtagId + "),";
                }
            }
            values = values.substring(0, values.length() - 1);
            String sql5 = "INSERT INTO  PATIENT_LIST_TAGS ( PATIENT_ID, COMMUNITY_ID, TAG_ID) "
                    + values;

            logger.info(sql5);
            Query q = em.createNativeQuery(sql5);

            q.executeUpdate();

            utx.commit();

        } catch (Exception ex) {

            try {
                ex.printStackTrace();
                utx.rollback();
            } catch (Exception re) {
                logger.log(Level.SEVERE, "An error occurred attempting to roll back the transaction.");
            }
        }

    }

    private String getPatientManagerPath(EntityManager em, long comId) {

        String sql = "select VALUE from connect.configuration where name = 'patient.manager.url' and community_id =?1";
        Query q = em.createNativeQuery(sql);
        q.setParameter(1, comId);
        return (String) q.getResultList().get(0);

    }

    private String getMinorAge(EntityManager em, long comId) {

        String sql = "select VALUE from connect.configuration where name = 'minor.age' and community_id =?1";
        Query q = em.createNativeQuery(sql);
        q.setParameter(1, comId);
        return (String) q.getResultList().get(0);

    }

    private String getUserEmail(EntityManager em, long userId) {

        String sql = "select email from connect.user where user_id = ?1 ";
        Query q = em.createNativeQuery(sql);
        q.setParameter(1, userId);

        return (String) q.getResultList().get(0);
    }

    private ListTags getMinorTag(EntityManager em, long comId) {

        String sql = "select * from connect.List_Tags where tag_type = 'MINOR' and community_id =?1";
        Query q = em.createNativeQuery(sql, ListTags.class
        );
        q.setParameter(1, comId);
        return (ListTags) q.getResultList().get(0);

    }

    private ListTags getCareTeamTag(EntityManager em, long comId) {

        String sql = "select * from connect.List_Tags where tag_type = 'CARETEAM' and community_id =?1";
        Query q = em.createNativeQuery(sql, ListTags.class);
        q.setParameter(1, comId);
        return (ListTags) q.getResultList().get(0);

    }

    private ListTags getEventTag(EntityManager em, long comId) {

        String sql = "select * from connect.List_Tags where tag_type = 'EVENT' and community_id =?1";
        Query q = em.createNativeQuery(sql, ListTags.class
        );
        q.setParameter(1, comId);
        return (ListTags) q.getResultList().get(0);

    }

    private ListTags getListTagById(EntityManager em, long comId, long tagId) {

        String sql = "select * from connect.List_Tags where community_id =?1 and tag_id = ?2";
        Query q = em.createNativeQuery(sql, ListTags.class);
        q.setParameter(1, comId);
        q.setParameter(2, tagId);
        return (ListTags) q.getResultList().get(0);

    }

    private String getCommunityOid(EntityManager em, long comId) {

        String sql = "select * from connect.community where  community_id =?1";
        Query q = em.createNativeQuery(sql, Community.class
        );
        q.setParameter(1, comId);
        return ((Community) q.getResultList().get(0)).getCommunityOid();

    }

    private String getEventExpire(EntityManager em, long comId) {

        String sql = "select VALUE from connect.configuration where name = 'critical.tag.expiration.days' and community_id =?1";
        Query q = em.createNativeQuery(sql);
        q.setParameter(1, comId);
        return (String) q.getResultList().get(0);

    }

    @Override
    public List<ListTags> getListTags(long communityId) throws Exception {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery("select m from ListTags as m where  m.communityId = :theComnunityId", ListTagDTO.class
            );

            q.setParameter("theComnunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public List<MyPatientInfoDTO> findMyPatientInfoList(long userId, long communityId) {
        List<MyPatientInfoDTO> myPatientInfoList = new ArrayList<MyPatientInfoDTO>();

        List<MyPatientInfo> entities = findMyPatientInfoEntities(userId, communityId);
        for (MyPatientInfo patient : entities) {
            long patientId = patient.getMyPatientInfoPK().getPatientId();
            char flagged = patient.getFlagged();

            MyPatientInfoDTO myPatientInfo = new MyPatientInfoDTO(userId, patientId, communityId, flagged, null, null, null);
            Collection<UserPatientListTags> utags = patient.getUserPatientListTagsCollection();
            List<ListTagDTO> list = myPatientInfo.getTags();
            for (UserPatientListTags utag : utags) {
                ListTags tag = utag.getTagId();
                ListTagDTO lto = new ListTagDTO();
                lto.setTagId(tag.getTagId());
                lto.setTagName(tag.getTagName());
                lto.setTagType(tag.getTagType());
                lto.setTagAbbr(tag.getTagAbbr());
                list.add(lto);
            }
            myPatientInfoList.add(myPatientInfo);
        }

        return myPatientInfoList;
    }

    /*@Override
    public String findMyPatientInfoListCount(long userId, long communityId, String level, String tags, List patients) {

            List<MyPatientInfoDTO> test =  findMyPatientInfoList(userId,  communityId, start, stop,  level, tags,patients);
            return Integer.toString(test.size());
    }*/
    @Override
    public String findMyPatientInfoListCount(long userId, long communityId, String level, String tags, List patients) {

        String fname = null;
        String lname = null;
        String name = null;
        String search = null;
        if (patients != null) {
            List<LinkedHashMap> temp = (List<LinkedHashMap>) patients;
            for (LinkedHashMap ptemp : temp) {
                name = (String) ptemp.get("name");
                fname = (String) ptemp.get("firstname");
                lname = (String) ptemp.get("name");
                search = (String) ptemp.get("search");
            }
        }
        if (search == null && name != null) {
            search = name;
        }
        PatientIdList filter = null;
        if (search != null) {
            PatientDemogList test = getPMResponse(communityId, search, userId);

            if (test != null && test.size() > 0) {
                filter = new PatientIdList();
                for (ViewPatientDemog vpd : test) {
                    String id = new Long(vpd.getPatientId()).toString();
                    Patient p = new Patient();
                    p.setId(id);
                    filter.add(p);
                }
            } else {
                return "0";// return empty since search returned none
            }
        }
        List<MyPatientInfo> entities = null;
        List<MyPatientInfo> entities2 = null;

        String tag1 = tags;
        String tag2 = tags;
        if (level.equals("user")) {
            int mergeType = mergeType(communityId, tags);
            if (mergeType == 1) {
                tag2 = "all";
            }
            if (mergeType == 2) {
                tag1 = "all";
            }
            entities = findUserPatientInfoEntities(userId, communityId, 0, 100000, level, tags, filter);
            entities2 = findPatientInfoEntities(userId, communityId, level, tags, filter);

            if (mergeType == 0) {
                entities = unionEntitiesCount(entities, entities2);
            } else if (mergeType == 1) {
                entities = mergeEntitiesCount(entities, entities2);
            } else if (mergeType == 2) {
                entities = mergeEntitiesCount(entities2, entities);
            } else if (mergeType == 3) {
                entities = intersectEntitiesCount(entities, entities2);
            }
        } else if (level.equals("org")) {
            entities = findPatientTagOrgEntitiesCount(userId, communityId, level, tags, filter);
        }

        return Integer.toString(entities.size());
    }

    @Override
    public List<MyPatientInfoDTO> findMyPatientInfoList(long userId, long communityId, long start, long stop, String level, String tags, List patients) {
        List<MyPatientInfoDTO> myPatientInfoList = new ArrayList<MyPatientInfoDTO>();
        String thread = Thread.currentThread().getName();
        StopWatch stopWatch = new StopWatch();
        StopWatch totalWatch = new StopWatch();
        stopWatch.start();
        totalWatch.start();
        String fname = null;
        String lname = null;
        String name = null;
        String search = null;
        if (patients != null) {
            List<LinkedHashMap> temp = (List<LinkedHashMap>) patients;
            for (LinkedHashMap ptemp : temp) {
                name = (String) ptemp.get("name");
                fname = (String) ptemp.get("firstname");
                lname = (String) ptemp.get("name");
                search = (String) ptemp.get("search");
            }
        }
        if (search == null && name != null) {
            search = name;
        }
        double timesql = stopWatch.getTime();
        logger.info("************* Thread "+ thread +" SW1= "+ timesql);
      //  stopWatch.reset();
     //   stopWatch.start();
        PatientIdList filter = null;
        if (search != null) {
            PatientDemogList test = getPMResponse(communityId, search, userId);

            if (test != null && test.size() > 0) {
                filter = new PatientIdList();
                for (ViewPatientDemog vpd : test) {
                    String id = new Long(vpd.getPatientId()).toString();
                    Patient p = new Patient();
                    p.setId(id);
                    filter.add(p);
                }
            } else {
                return myPatientInfoList;// return empty since search returned none
            }
        }
        timesql = stopWatch.getTime();
        logger.info("************* Thread "+ thread +" SW2= "+ timesql);
      //  stopWatch.reset();
      //  stopWatch.start();
        List<MyPatientInfo> entities = null;
        List<MyPatientInfo> entities2 = null;
        String tag1 = tags;
        String tag2 = tags;
        if (level.equals("user")) {
            int mergeType = mergeType(communityId, tags);
            if (mergeType == 1) {
                tag2 = "all";
            }
            if (mergeType == 2) {
                tag1 = "all";
            }
            timesql = stopWatch.getTime();
           logger.info("************* Thread "+ thread +" SW3= "+ timesql);
       //     stopWatch.reset();
         //   stopWatch.start();
            entities = findUserPatientInfoEntities(userId, communityId, start, stop, level, tag1, filter);
            timesql = stopWatch.getTime();
             logger.info("************* Thread "+ thread +" SW4= "+ timesql);
        //    stopWatch.reset();
         //   stopWatch.start();
            entities2 = findPatientInfoEntities(userId, communityId, level, tag2, filter);
            timesql = stopWatch.getTime();
            logger.info("************* Thread "+ thread +" SW5= "+ timesql);
        //    stopWatch.reset();
         //   stopWatch.start();
            if (mergeType == 0) {
                entities = unionEntities(entities, entities2);//vpl
            } else if (mergeType == 1) {
                entities = mergeEntities(entities, entities2);
            } else if (mergeType == 2) {
                entities = mergeEntities(entities2, entities);
            } else if (mergeType == 3) {
                entities = intersectEntities(entities, entities2);
            }
            timesql = stopWatch.getTime();
            logger.info("************* Thread "+ thread +" SW6= "+ timesql + " for merge type " + mergeType);
          //  stopWatch.reset();
          //  stopWatch.start();
        } else if (level.equals("org")) {
            entities = findPatientTagOrgEntities(userId, communityId, start, stop, level, tags, filter);
        }

        addDemogs(getEntityManager(), communityId, entities);
        timesql = stopWatch.getTime();
        logger.info("************* Thread "+ thread +" MPLDemog= "+ timesql);
      //  stopWatch.reset();
   //     stopWatch.start();
        for (MyPatientInfo patient : entities) {
            long muserId = patient.getMyPatientInfoPK().getUserId();
            long patientId = patient.getMyPatientInfoPK().getPatientId();
            char flagged = patient.getFlagged();

            MyPatientInfoDTO myPatientInfo = new MyPatientInfoDTO(muserId, patientId, communityId, flagged, null, null, null);
            Collection<UserPatientListTags> utags = patient.getUserPatientListTagsCollection();
            String pfname = patient.getFirstName();
            String plname = patient.getLastName();

            myPatientInfo.setFirstName(pfname);
            myPatientInfo.setLastName(plname);
            myPatientInfo.setGender(patient.getGender());
            myPatientInfo.setDob(patient.getDateOfBirth());
            List<ListTagDTO> list = myPatientInfo.getTags();
            for (UserPatientListTags utag : utags) {
                ListTags tag = utag.getTagId();
                ListTagDTO lto = new ListTagDTO();
                lto.setTagId(tag.getTagId());
                lto.setTagName(tag.getTagName());
                lto.setTagType(tag.getTagType());
                lto.setTagAbbr(tag.getTagAbbr());
                list.add(lto);
            }
            myPatientInfoList.add(myPatientInfo);
        }
        timesql = stopWatch.getTime();
        logger.info("************* Thread "+ thread +" SW7= "+ timesql);
        stopWatch.stop();
        long total = totalWatch.getTime();
        logger.info("************ Thread "+ thread +" MPL TOTAL time " + total);
        totalWatch.stop();

        return myPatientInfoList;

    }

    private PatientIdList getPatientIds(List<MyPatientInfo> entities) {
        PatientIdList filter = null;

        filter = new PatientIdList();
        for (MyPatientInfo mpi : entities) {
            String id = new Long(mpi.getMyPatientInfoPK().getPatientId()).toString();
            Patient p = new Patient();
            p.setId(id);
            filter.add(p);
        }
        return filter;

    }

    private List<MyPatientInfo> mergeEntities(List<MyPatientInfo> entities1, List<MyPatientInfo> entities2) {
        ArrayList<MyPatientInfo> intersect = new ArrayList();
        for (MyPatientInfo mpi1 : entities1) {
            MyPatientInfo sup = new MyPatientInfo(mpi1.getMyPatientInfoPK().getUserId(), mpi1.getMyPatientInfoPK().getPatientId(), mpi1.getMyPatientInfoPK().getCommunityId());
            sup.setFirstName(mpi1.getFirstName());
            sup.setLastName(mpi1.getLastName());
            sup.setDateOfBirth(mpi1.getDateOfBirth());
            sup.setGender(mpi1.getGender());
            Collection<UserPatientListTags> list = new ArrayList();
            sup.setUserPatientListTagsCollection(list);
            intersect.add(sup);
        }

        for (MyPatientInfo mpi : intersect) {

            for (MyPatientInfo mpi1 : entities1) {
                if (mpi.equals(mpi1)) {
                    Collection<UserPatientListTags> list1 = mpi1.getUserPatientListTagsCollection();
                    Collection<UserPatientListTags> list = mpi.getUserPatientListTagsCollection();
                    list.addAll(list1);
                }
            }
            for (MyPatientInfo mpi2 : entities2) {
                if (mpi.equals(mpi2)) {
                    Collection<UserPatientListTags> list2 = mpi2.getUserPatientListTagsCollection();
                    Collection<UserPatientListTags> list = mpi.getUserPatientListTagsCollection();
                    list.addAll(list2);
                }
            }

        }

        return intersect;

    }

    private List<MyPatientInfo> intersectEntities(List<MyPatientInfo> entities1, List<MyPatientInfo> entities2) {
        ArrayList<MyPatientInfo> intersect = new ArrayList();
        for (MyPatientInfo mpi1 : entities1) {
            MyPatientInfo sup = new MyPatientInfo(mpi1.getMyPatientInfoPK().getUserId(), mpi1.getMyPatientInfoPK().getPatientId(), mpi1.getMyPatientInfoPK().getCommunityId());
            sup.setFirstName(mpi1.getFirstName());
            sup.setLastName(mpi1.getLastName());
            sup.setDateOfBirth(mpi1.getDateOfBirth());
            sup.setGender(mpi1.getGender());
            Collection<UserPatientListTags> list = new ArrayList();
            sup.setUserPatientListTagsCollection(list);
            if (entities2.isEmpty()) {
                if (!intersect.contains(sup)) {
                    intersect.add(sup);
                }
            } else if (entities2.contains(sup)) {
                if (!intersect.contains(sup)) {
                    intersect.add(sup);
                }
            }

        }

        for (MyPatientInfo mpi2 : entities2) {
            MyPatientInfo sup = new MyPatientInfo(mpi2.getMyPatientInfoPK().getUserId(), mpi2.getMyPatientInfoPK().getPatientId(), mpi2.getMyPatientInfoPK().getCommunityId());

            sup.setFirstName(mpi2.getFirstName());
            sup.setLastName(mpi2.getLastName());
            sup.setDateOfBirth(mpi2.getDateOfBirth());
            sup.setGender(mpi2.getGender());
            Collection<UserPatientListTags> list = new ArrayList();
            sup.setUserPatientListTagsCollection(list);

            if (entities1.isEmpty()) {
                if (!intersect.contains(sup)) {
                    intersect.add(sup);
                }
            } else if (entities1.contains(sup)) {
                if (!intersect.contains(sup)) {
                    intersect.add(sup);
                }
            }
        }
        for (MyPatientInfo mpi : intersect) {

            for (MyPatientInfo mpi1 : entities1) {
                if (mpi.equals(mpi1)) {
                    Collection<UserPatientListTags> list1 = mpi1.getUserPatientListTagsCollection();
                    Collection<UserPatientListTags> list = mpi.getUserPatientListTagsCollection();
                    list.addAll(list1);
                }
            }
            for (MyPatientInfo mpi2 : entities2) {
                if (mpi.equals(mpi2)) {
                    Collection<UserPatientListTags> list2 = mpi2.getUserPatientListTagsCollection();
                    Collection<UserPatientListTags> list = mpi.getUserPatientListTagsCollection();
                    list.addAll(list2);
                }
            }

        }

        return intersect;

    }

    private List<MyPatientInfo> unionEntities(List<MyPatientInfo> entities1, List<MyPatientInfo> entities2) {
        ArrayList<MyPatientInfo> union = new ArrayList();
        for (MyPatientInfo mpi1 : entities1) {
            MyPatientInfo sup = new MyPatientInfo(mpi1.getMyPatientInfoPK().getUserId(), mpi1.getMyPatientInfoPK().getPatientId(), mpi1.getMyPatientInfoPK().getCommunityId());
            sup.setFirstName(mpi1.getFirstName());
            sup.setLastName(mpi1.getLastName());
            sup.setDateOfBirth(mpi1.getDateOfBirth());
            sup.setGender(mpi1.getGender());
            Collection<UserPatientListTags> list = new ArrayList();
            sup.setUserPatientListTagsCollection(list);

            if (!union.contains(sup)) {
                union.add(sup);
            }

        }

        for (MyPatientInfo mpi2 : entities2) {
            MyPatientInfo sup = new MyPatientInfo(mpi2.getMyPatientInfoPK().getUserId(), mpi2.getMyPatientInfoPK().getPatientId(), mpi2.getMyPatientInfoPK().getCommunityId());

            sup.setFirstName(mpi2.getFirstName());
            sup.setLastName(mpi2.getLastName());
            sup.setDateOfBirth(mpi2.getDateOfBirth());
            sup.setGender(mpi2.getGender());
            Collection<UserPatientListTags> list = new ArrayList();
            sup.setUserPatientListTagsCollection(list);

            if (!union.contains(sup)) {
                union.add(sup);
            }

        }
        // get two selections of tags by user/commnity, paired down py patients

        for (MyPatientInfo mpi : union) {

            for (MyPatientInfo mpi1 : entities1) {
                if (mpi.equals(mpi1)) {
                    Collection<UserPatientListTags> list1 = mpi1.getUserPatientListTagsCollection();
                    Collection<UserPatientListTags> list = mpi.getUserPatientListTagsCollection();
                    list.addAll(list1);
                }
            }
            for (MyPatientInfo mpi2 : entities2) {
                if (mpi.equals(mpi2)) {
                    Collection<UserPatientListTags> list2 = mpi2.getUserPatientListTagsCollection();
                    Collection<UserPatientListTags> list = mpi.getUserPatientListTagsCollection();
                    list.addAll(list2);
                }
            }

        }

        return union;

    }

    private List<MyPatientInfo> intersectEntitiesCount(List<MyPatientInfo> entities1, List<MyPatientInfo> entities2) {
        ArrayList<MyPatientInfo> intersect = new ArrayList();
        for (MyPatientInfo mpi1 : entities1) {
            MyPatientInfo sup = new MyPatientInfo(mpi1.getMyPatientInfoPK().getUserId(), mpi1.getMyPatientInfoPK().getPatientId(), mpi1.getMyPatientInfoPK().getCommunityId());
            sup.setFirstName(mpi1.getFirstName());
            sup.setLastName(mpi1.getLastName());
            sup.setDateOfBirth(mpi1.getDateOfBirth());
            sup.setGender(mpi1.getGender());
            Collection<UserPatientListTags> list = new ArrayList();
            sup.setUserPatientListTagsCollection(list);
            if (entities2.isEmpty()) {
                if (!intersect.contains(sup)) {
                    intersect.add(sup);
                }
            } else if (entities2.contains(sup)) {
                if (!intersect.contains(sup)) {
                    intersect.add(sup);
                }
            }

        }

        return intersect;

    }

    private List<MyPatientInfo> mergeEntitiesCount(List<MyPatientInfo> entities1, List<MyPatientInfo> entities2) {
        ArrayList<MyPatientInfo> intersect = new ArrayList();
        for (MyPatientInfo mpi1 : entities1) {
            MyPatientInfo sup = new MyPatientInfo(mpi1.getMyPatientInfoPK().getUserId(), mpi1.getMyPatientInfoPK().getPatientId(), mpi1.getMyPatientInfoPK().getCommunityId());
            sup.setFirstName(mpi1.getFirstName());
            sup.setLastName(mpi1.getLastName());
            sup.setDateOfBirth(mpi1.getDateOfBirth());
            sup.setGender(mpi1.getGender());
            Collection<UserPatientListTags> list = new ArrayList();
            sup.setUserPatientListTagsCollection(list);
            intersect.add(sup);

        }

        return intersect;

    }

    private List<MyPatientInfo> unionEntitiesCount(List<MyPatientInfo> entities1, List<MyPatientInfo> entities2) {
        ArrayList<MyPatientInfo> union = new ArrayList();
        for (MyPatientInfo mpi1 : entities1) {
            MyPatientInfo sup = new MyPatientInfo(mpi1.getMyPatientInfoPK().getUserId(), mpi1.getMyPatientInfoPK().getPatientId(), mpi1.getMyPatientInfoPK().getCommunityId());
            sup.setFirstName(mpi1.getFirstName());
            sup.setLastName(mpi1.getLastName());
            sup.setDateOfBirth(mpi1.getDateOfBirth());
            sup.setGender(mpi1.getGender());
            Collection<UserPatientListTags> list = new ArrayList();
            sup.setUserPatientListTagsCollection(list);

            if (!union.contains(sup)) {
                union.add(sup);
            }

        }

        for (MyPatientInfo mpi2 : entities2) {
            MyPatientInfo sup = new MyPatientInfo(mpi2.getMyPatientInfoPK().getUserId(), mpi2.getMyPatientInfoPK().getPatientId(), mpi2.getMyPatientInfoPK().getCommunityId());

            sup.setFirstName(mpi2.getFirstName());
            sup.setLastName(mpi2.getLastName());
            sup.setDateOfBirth(mpi2.getDateOfBirth());
            sup.setGender(mpi2.getGender());
            Collection<UserPatientListTags> list = new ArrayList();
            sup.setUserPatientListTagsCollection(list);

            if (!union.contains(sup)) {
                union.add(sup);
            }

        }

        return union;

    }

    @Override
    public boolean isAlreadyOnList(long userid, long patientId, long communityId
    ) {
        EntityManager em = getEntityManager();
        try {
            MyPatientInfoPK pk = new MyPatientInfoPK(userid, patientId, communityId);

            // em.find returns null if not found
            MyPatientInfo myPatientInfo = em.find(MyPatientInfo.class, pk);

            boolean found = myPatientInfo != null;

            return found;
        } finally {
            em.close();
        }
    }

    @Deprecated
    @Override
    public boolean isListFull(long userId, long communityId
    ) {
        EntityManager em = getEntityManager();

        try {
            ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

            int maxSize = Integer.parseInt(configurationDAO.getProperty(communityId, "mypatientlist.maxsize", "50"));

            Query q = em.createQuery("select count(o) from MyPatientInfo as o where o.myPatientInfoPK.userId = :theUserId and o.myPatientInfoPK.communityId = :theCommunityId");
            q.setParameter("theUserId", userId);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int count = ((Long) q.getSingleResult()).intValue();

            return count >= maxSize;

        } finally {
            em.close();
        }
    }

    @Override
    public List<Long> getUserIdsForPatientList(long patientId, long communityId
    ) {

        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select o.myPatientInfoPK.userId from MyPatientInfo as o where o.myPatientInfoPK.patientId = :thePatientId and o.myPatientInfoPK.communityId = :theComnunityId");

            q.setParameter("thePatientId", patientId);
            q.setParameter("theComnunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public List<Long> getUserIdsWithConsent(long patientId, long communityId
    ) {

        EntityManager em = getEntityManager();

        List<Long> userIds = new ArrayList<Long>();

        try {
            String sql = "select user_id from view_user_patient_consent where"
                    + " patient_id=" + patientId
                    + " and community_id=" + communityId;

            Query patientQuery = em.createNativeQuery(sql);
            //JIRA 870
            //patientQuery.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<Long> theList = patientQuery.getResultList();

            for (Long tempUserId : theList) {
                userIds.add(tempUserId);
            }
        } finally {
            em.close();
        }

        return userIds;
    }

    @Override
    public void delete(List<Long> userIds, long patientId, long communityId) throws Exception {

        // convert user ids to in clause
        String inClause = DaoUtils.buildInClause(userIds);

        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();
            String jpql = "delete from MyPatientInfo as o where o.myPatientInfoPK.patientId = :thePatientId and o.myPatientInfoPK.communityId = :theComnunityId and o.myPatientInfoPK.userId in " + inClause;
            Query q = em.createQuery(jpql);

            q.setParameter("thePatientId", patientId);
            q.setParameter("theComnunityId", communityId);

            q.executeUpdate();
            utx.commit();

            logger.info("mypatientsynch: executing jpql=" + jpql);
        } catch (Exception exc) {
            try {
                if (utx.isActive()) {
                    utx.rollback();
                }
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
        } finally {
            em.close();
        }
    }

    @Override
    public boolean hasConsent(long userId, long patientId, long communityId
    ) {

        EntityManager em = getEntityManager();

        boolean result = false;

        logger.info("Checking consent for: userId=" + userId + ", patientId=" + patientId + ", communityId=" + communityId);

        try {
            String sql = "select count(*) from view_user_patient_consent where"
                    + " user_id=" + userId
                    + " and patient_id=" + patientId
                    + " and community_id=" + communityId;

            Query patientQuery = em.createNativeQuery(sql);
            //patientQuery.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            long count = (Long) patientQuery.getSingleResult();

            logger.info("count =" + count);

            result = count > 0;

            logger.info("hasConsent=" + result);

        } finally {
            em.close();
        }

        return result;
    }

    @Override
    public List<Long> getPatientIdsForPatientList(long userId, long communityId
    ) {

        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select o.myPatientInfoPK.patientId from MyPatientInfo as o where o.myPatientInfoPK.userId = :theUserId and o.myPatientInfoPK.communityId = :theComnunityId");

            q.setParameter("theUserId", userId);
            q.setParameter("theComnunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return q.getResultList();
        } finally {
            em.close();
        }

    }

    @Override
    public List<Long> getPatientIdsWithConsent(long userId, long communityId
    ) {
        EntityManager em = getEntityManager();

        List<Long> patientIds = new ArrayList<Long>();

        try {
            String sql = "select patient_id from view_user_patient_consent where"
                    + " user_id=" + userId
                    + " and community_id=" + communityId;

            Query patientQuery = em.createNativeQuery(sql);
            //patientQuery.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<Vector> theList = (List<Vector>) patientQuery.getResultList();

            for (Vector tempRow : theList) {
                long tempUserId = (Long) tempRow.get(0);

                patientIds.add(tempUserId);
            }
        } finally {
            em.close();
        }

        return patientIds;
    }

    @Override
    public void deleteFromPatientList(List<Long> patientIds, long userId, long communityId
    ) {

        // convert user ids to in clause
        String inClause = DaoUtils.buildInClause(patientIds);

        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();
            String jpql = "delete from MyPatientInfo as o where o.myPatientInfoPK.userId = :theUserId and o.myPatientInfoPK.communityId = :theComnunityId and o.myPatientInfoPK.patientId in " + inClause;
            Query q = em.createQuery(jpql);

            q.setParameter("theUserId", userId);
            q.setParameter("theComnunityId", communityId);

            q.executeUpdate();
            utx.commit();

            logger.info("mypatientsynch: executing jpql=" + jpql);
        } catch (Exception exc) {
            try {
                if (utx.isActive()) {
                    utx.rollback();
                }
            } catch (Exception re) {
                logger.log(Level.SEVERE, "An error occurred attempting to roll back the transaction.", re);
            }
        } finally {
            em.close();
        }
    }

    private String getOrgId(long comId, long userId) {
        EntityManager em = getEntityManager();
        String sql = " SELECT distinct CONVERT(ORG_ID,CHAR) from connect.user_community_organization uco"
                + " WHERE uco.community_id = ?1"
                + " AND uco.user_Id = ?2"
                + " AND (uco.END_DATE IS NULL OR uco.END_DATE > NOW()) AND (uco.START_DATE < NOW())";

        Query q = em.createNativeQuery(sql)
                .setParameter(1, comId)
                .setParameter(2, userId);
        String ret = (String) q.getResultList().get(0);
        return ret;
    }

    private boolean isPowerUser(long comId, long userId) {
        EntityManager em = getEntityManager();
        boolean ret = false;
        try {
            String sql = "SELECT * from connect.user_community_organization uco"
                    + " WHERE uco.community_id = ?1"
                    + " AND uco.access_level_id IN(4)"
                    + " AND uco.user_Id = ?2"
                    + " AND (uco.END_DATE IS NULL OR uco.END_DATE > NOW()) AND (uco.START_DATE < NOW())";

            Query query = em.createNativeQuery(sql)
                    .setParameter(1, comId)
                    .setParameter(2, userId);

            List qret = query.getResultList();
            if (qret != null && !qret.isEmpty()) {
                ret = true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return ret;
    }

    /*public String postResponse(EventInfo event, String path) throws Exception {
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpPost request = new HttpPost(path);
            ObjectMapper mapper = JacksonContextResolver.createCombinedObjectMapper();

//Object to JSON in String
            String send = mapper.writeValueAsString(event);
            StringEntity params = new StringEntity(send);

            request.addHeader("content-type", "application/json");
            request.setEntity(params);
            HttpResponse result = httpClient.execute(request);
            String json = EntityUtils.toString(result.getEntity(), "UTF-8");
            System.out.println(json);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;

    }*/
    public PatientDemogList getPMResponse(long comId, String search, long userId) {

        String json = null;
        PatientDemogList ret = null;
        EntityManager em = getEntityManager();
        String userEmail = getUserEmail(em, userId);
        search = search.trim();

        //String pmUrl = "http://10.110.210.59:8080/PatientManager-1.0/patient/patientInfo/";
        String pmUrl = getPatientManagerPath(em, comId);
        if (search.indexOf(" ") > 0) {
            search = search.replaceAll(" ", "%20");
        }

        String path = pmUrl + "demog/" + comId + "/" + search + "/" + userEmail;
        logger.info("Patient Manager path " + path);
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpGet request = new HttpGet(path);
            ObjectMapper mapper = JacksonContextResolver.createCombinedObjectMapper();

//Object to JSON in String
            //StringEntity params = new StringEntity(send);
            request.addHeader("content-type", "application/json");
            // request.setEntity(params);
            HttpResponse result = httpClient.execute(request);
            json = EntityUtils.toString(result.getEntity(), "UTF-8");
            System.out.println(json);
            ret
                    = mapper.readValue(json, PatientDemogList.class
                    );

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            return ret;
        }

    }

    @Override
    public ListTags getTagByType(long communityId, String type) throws Exception {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery("select m from ListTags as m where m.communityId = :theComnunityId AND m.tagType = :tagType", ListTags.class);

            q.setParameter("theComnunityId", communityId);
            q.setParameter("tagType", type);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<ListTags> result = q.getResultList();

            if (result == null || result.size() == 0) {
                return null;
            }

            return result.get(0);
        } finally {
            em.close();
        }
    }

}
