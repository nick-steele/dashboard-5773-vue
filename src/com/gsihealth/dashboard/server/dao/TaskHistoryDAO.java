/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.TaskHistory;

/**
 *
 * @author Beth.Boose
 */
public interface TaskHistoryDAO {
    public void addTaskHistory(TaskHistory taskHistory);
    public TaskHistory getTaskHistory(Long communityId, String taskName);
}
