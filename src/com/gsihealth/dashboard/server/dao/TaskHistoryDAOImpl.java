/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.TaskHistory;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;

/**
 *
 * @author Beth.Boose
 */
public class TaskHistoryDAOImpl implements TaskHistoryDAO {
    private EntityManagerFactory emf = null;
    private final Logger logger = Logger.getLogger(getClass().getName());

    public TaskHistoryDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public TaskHistoryDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }
    
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    @Override
    public void addTaskHistory(TaskHistory taskHistory) {
        EntityManager em = null;
        try {
            logger.info("persisting taskHistory: " + taskHistory.toString());
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(taskHistory);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    @Override
    public TaskHistory getTaskHistory(Long communityId, String taskName) {
        EntityManager em = getEntityManager();
        Query q = em.createQuery("SELECT t FROM TaskHistory t where t.communityId = :communityId"
                + " AND t.taskName = :taskName ORDER BY t.createdDate DESC")
                .setParameter("taskName", taskName)
                .setParameter("communityId", communityId)
                .setMaxResults(1)
                .setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
        List<TaskHistory> th = q.getResultList();
        TaskHistory taskHistory = null;
        if(q.getResultList().size() > 0) {
           taskHistory = (TaskHistory)q.getResultList().get(0); 
        }
        return taskHistory;
    }
    
    
}
