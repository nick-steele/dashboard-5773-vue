package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.entity.NonHealthHomeProvider;
import com.gsihealth.entity.NonHealthHomeProviderAssignedPatient;
import com.gsihealth.entity.NonHealthHomeProviderPK;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.PreexistingEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.RollbackFailureException;
import java.util.List;
import javax.persistence.criteria.Predicate;

/**
 *
 * @author Chad Darby
 */
public interface NonHealthHomeProviderDAO {

    public long add(NonHealthHomeProvider nonHealthHomeProvider) throws PreexistingEntityException, RollbackFailureException, Exception;

    public void delete(NonHealthHomeProviderPK id) throws NonexistentEntityException, RollbackFailureException, Exception;

    public void update(NonHealthHomeProvider nonHealthHomeProvider) throws NonexistentEntityException, RollbackFailureException, Exception;

    public NonHealthHomeProvider findNonHealthHomeProvider(NonHealthHomeProviderPK id);

   // public List<NonHealthHomeProvider> findNonHealthHomeProviderEntities(long patientId, long communityId , int pageNumber, int pageSize);
    
    public int getNonHealthHomeProviderCount(long patientId);

    public boolean isDuplicateEmail(String email, long communityId);
    
    public List<NonHealthHomeProvider> findNonHealthHomeProviderEntities(long patientId, long communityId);
    
    public int getNonHealthHomeProviderCountByCommunityId(long communityId);
    
    public List<NonHealthHomeProvider> findNonHealthHomeProviderEntitiesByCommunityId(long communityId);
    
    public void assignProvider(NonHealthHomeProviderAssignedPatient providerAssignedPatient, boolean recordAlreadyExists) throws PreexistingEntityException, RollbackFailureException, Exception;;
    
    public List<NonHealthHomeProviderAssignedPatient> getProviderAssignedPatient(long providerId, long communityId);
    
    public void unAssignProvider(NonHealthHomeProviderAssignedPatient assignedPatient) throws NonexistentEntityException, RollbackFailureException, Exception;
    
    public int getCountForPatientsAssignedToProvider(long providerId, long communityId);
    
    public boolean ifProviderAssignedExist(long providerId, long patientId, long communityId);
    
    public boolean isDuplicateNPI(String NPI, long communityId);
    
    /**
     * @param searchExpression
     * @param maxPatients - if there's no demographics, throttle pce size
     * @return row count for patientEnrollment using searchExpression
     */
    //JIRA 860-1460 (1467)
    //public List<NonHealthHomeProviderAssignedPatient> getNHPPatientSearchTotalCount(Expression searchExpression, Integer maxPatients);
    public List<NonHealthHomeProviderAssignedPatient> getNHPPatientSearchTotalCount(Predicate searchPredicate, Integer maxPatients);
    
    /**
     * build criteria query for patient search
     * @param searchCriteria - user generated search params 
     * @param patientEuids - until we find a better way, do the mdm patient demographic search first
     * @param userOrgId - logged on user's org id
     * @param communityId
     * @param enrolled - enrolled status
     * @param userId 
     * @return criteria query with all the above params included dynamically
     */
    //JIRA 860-1460 (1467)
    //public Expression criteriaBuilderForNHPPatientSearch(SearchCriteria searchCriteria,long communityId);
    public Predicate criteriaBuilderForNHPPatientSearch(SearchCriteria searchCriteria, long communityId);
    
    public List<NonHealthHomeProvider> getProvidersByCommunityId(long communityId);
    
    public NonHealthHomeProvider getProvidersByCommunityId(long communityId,long providerId);
    
    public List<NonHealthHomeProviderAssignedPatient> getProviderAssignedToPatient(long patientId, long communityId);
    
}
