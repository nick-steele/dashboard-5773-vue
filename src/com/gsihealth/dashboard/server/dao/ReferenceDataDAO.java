package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.PatientActivityNames;
import com.gsihealth.entity.CareteamRoleDuplicate;
import com.gsihealth.entity.CareteamRoleRequired;
import com.gsihealth.entity.DisenrollmentReasons;
import com.gsihealth.entity.EnrollmentStatus;
import com.gsihealth.entity.EthnicCode;
import com.gsihealth.entity.GenderCode;
import com.gsihealth.entity.LanguageCode;
import com.gsihealth.entity.NonHealthHomeProvider;
import com.gsihealth.entity.PatientRelationshipCode;
import com.gsihealth.entity.PatientStatus;
import com.gsihealth.entity.ProgramHealthHome;
import com.gsihealth.entity.ProgramLevel;
import com.gsihealth.entity.ProgramName;
import com.gsihealth.entity.RaceCode;
import com.gsihealth.entity.Role;
import com.gsihealth.entity.UserCredential;
import com.gsihealth.entity.UserPrefix;
import com.gsihealth.entity.User;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public interface ReferenceDataDAO {

    public List<Long> findCommunityIds();

    public List<GenderCode> findGenderCodeEntities(Long communityId);

    public List<RaceCode> findRaceCodeEntities(Long communityId);

    public List<EthnicCode> findEthnicCodeEntities(Long communityId);

    public List<LanguageCode> findLanguageCodeEntities(Long communityId);

    public List<ProgramName> findProgramNameEntities(Long communityId);

    public List<EnrollmentStatus> findEnrollmentStatusEntities(Long communityId);

    public List<ProgramLevel> findProgramLevelEntities(Long communityId);

    public List<UserCredential> findUserCredentialEntities(Long communityId);

    public List<UserPrefix> findUserPrefixEntities(Long communityId);

//    public List<String> getEnrollmentReasonsForInactivation();

    public List<PatientRelationshipCode> findPatientRelationshipEntities(Long communityId);

    public List<DisenrollmentReasons> getDisenrollmentReasons(Long communityId);

    public List<CareteamRoleDuplicate> getCareteamRoleDuplicates(Long communityId);

    public List<CareteamRoleRequired> getCareteamRoleRequired(Long communityId);

    public String[] getPostalStateCodes(Long communitId);

    public String[] getProgramNames(Long communityId);

    public List<ProgramHealthHome> findProgramHealthHomeEntities(Long communityId);
    
    public List<ProgramName> findProgramNameMapEntities (Long communityId);

    public List<Role> getUserRolesEntities(long communityId);

    public HashMap<String, String> getPreferredLanguages(Long communityId);

    public HashMap<String, String> getEthnics(Long communityId);

    public HashMap<String, String> getRaces(Long communityId);

    public HashMap<String, String> getGenders(Long communityId);
    
    public List<User> getUsers();
    
    public List<NonHealthHomeProvider> getProvidersByCommunityId(long communityId);
    
    public List<PatientStatus> findPatientStatusEntities(Long communityId);
    
	//JIRA 1023
    public LinkedHashMap<Long, String> getHealthHomes(Long communityId);
    
    public String[] getTerminationReason(Long communityId);
    
    public String[] getProgramDeleteReasons(Long communityId);
    
    public List<PatientActivityNames> findActivityNameEntities(Long communityId);
}
