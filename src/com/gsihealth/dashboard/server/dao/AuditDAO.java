package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.Audit;

/**
 *
 * @author Chad Darby
 */
public interface AuditDAO {

   // public void add(Audit audit);
     public void addAudit(Audit audit);

}
