package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.client.util.ApplicationContextUtils;
import com.gsihealth.dashboard.common.ConsentConstants;
import com.gsihealth.entity.OrganizationPatientConsent;
import com.gsihealth.entity.User;
import com.gsihealth.entity.UserPatientConsent;
import com.gsihealth.entity.UserPatientConsentPK;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.PreexistingEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.RollbackFailureException;
import com.gsihealth.dashboard.server.service.PatientConsentUtils;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import org.apache.commons.lang.time.StopWatch;

/**
 *
 * @author Chad Darby
 */
public class UserPatientConsentDAOImpl implements UserPatientConsentDAO {
    
    /* DASHBOARD-477
     * Modify this class to only save user consent exceptions (DENY) 
     * All users in an organization have consent, unless the user patient consent table contains
     * DENY for a particular user/patient combination
     */

    private EntityManagerFactory emf = null;
    private Logger logger = Logger.getLogger(getClass().getName());

    public UserPatientConsentDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public UserPatientConsentDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public void add(UserPatientConsent userPatientConsent, Date startDate) throws PreexistingEntityException, RollbackFailureException, Exception {

        // Only persist consentStatus = DENY      
        if (hasUserPatientConsent(userPatientConsent)) return;
        
            EntityManager em = getEntityManager();
            EntityTransaction utx = em.getTransaction();

            if (userPatientConsent.getUserPatientConsentPK() == null) {
                userPatientConsent.setUserPatientConsentPK(new UserPatientConsentPK());
            }

            userPatientConsent.setStartDate(startDate);

            try {
                utx.begin();
                em.merge(userPatientConsent);
                utx.commit();
            } catch (Exception ex) {
                try {
                    utx.rollback();
                } catch (Exception re) {
                    throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                }
                if (findUserPatientConsent(userPatientConsent.getUserPatientConsentPK()) != null) {
                    throw new PreexistingEntityException("UserPatientConsent " + userPatientConsent + " already exists.", ex);
                }
                throw ex;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
    }

    @Override
    public void add(List<UserPatientConsent> userPatientConsentList) throws PreexistingEntityException, RollbackFailureException, Exception {

        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();

            for (UserPatientConsent userPatientConsent : userPatientConsentList) {              
                // Only persist consentStatus = DENY      
                if (!hasUserPatientConsent(userPatientConsent)) { // should this be after the null check
                    if (userPatientConsent.getUserPatientConsentPK() == null) {
                        userPatientConsent.setUserPatientConsentPK(new UserPatientConsentPK());
                    }

                    em.merge(userPatientConsent);
                }
            }

            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                // em.clear();
                em.close();
            }
        }
    }

    @Override
    public void update(UserPatientConsent userPatientConsent) throws NonexistentEntityException, RollbackFailureException, Exception {
            
            // Only persist if consentStatus = DENY 
            if (hasUserPatientConsent(userPatientConsent)) return;
            
            EntityManager em = getEntityManager();
            EntityTransaction utx = em.getTransaction();

            try {
                utx.begin();
                userPatientConsent = em.merge(userPatientConsent);
                utx.commit();
            } catch (Exception ex) {
                try {
                    utx.rollback();
                } catch (Exception re) {
                    throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
                }
                String msg = ex.getLocalizedMessage();
                if (msg == null || msg.length() == 0) {
                    UserPatientConsentPK id = userPatientConsent.getUserPatientConsentPK();
                    if (findUserPatientConsent(id) == null) {
                        throw new NonexistentEntityException("The userPatientConsent with id " + id + " no longer exists.");
                    }
                }
                throw ex;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        }
    @Override
    public void destroy(UserPatientConsentPK id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();
            UserPatientConsent userPatientConsent;

            try {
                userPatientConsent = em.getReference(UserPatientConsent.class, id);
                userPatientConsent.getUserPatientConsentPK();
                em.remove(userPatientConsent);
                utx.commit();
            } catch (EntityNotFoundException enfe) {
                logger.warning("The userPatientConsent with id " + id + " no longer exists.");
            }
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<UserPatientConsent> findUserPatientConsentEntities(long patientId, int pageNumber, int pageSize) {
        EntityManager em = getEntityManager();

        try {
           // Query q = em.createQuery("select object(o) from UserPatientConsent as o where o.userPatientConsentPK.patientId = :thePatientId");
            Query q = em.createNativeQuery(PatientConsentUtils.buildNativeSQLForUsersWithConsent(patientId));
         //   q.setParameter("thePatientId", patientId);
            //q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);

            q.setFirstResult(startRecordNumber);
            q.setMaxResults(pageSize);

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public UserPatientConsent findUserPatientConsent(UserPatientConsentPK id) {
        EntityManager em = getEntityManager();

        try {
            return em.find(UserPatientConsent.class, id);
        } finally {
            em.close();
        }
    }

    @Override
    public int getUserPatientConsentCount(long patientId) {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createNativeQuery(PatientConsentUtils.buildNativeSQLForUsersWithConsent(patientId));

            //q.setParameter("thePatientId", patientId);
            //q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @Override
    public void deleteOldConsentEntries(long patientId) {

        EntityManager em = getEntityManager();

        try {
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();

            StopWatch stopWatch = new StopWatch();
            stopWatch.start();

            Query query = em.createQuery("select object(o) from UserPatientConsent as o where o.userPatientConsentPK.patientId = :thePatientId");
            query.setParameter("thePatientId", patientId);
            query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            List<UserPatientConsent> items = query.getResultList();

            for (UserPatientConsent tempUserPatientConsent : items) {
                try {
                    tempUserPatientConsent.getUserPatientConsentPK();
                    em.remove(tempUserPatientConsent);
                } catch (Exception exc) {
                    logger.log(Level.WARNING, "Could not delete UserPatientConsent: " + tempUserPatientConsent.getUserPatientConsentPK(), exc);
                }
            }

            stopWatch.stop();
            logger.info("Time for deleteOldConsentEntries=" + stopWatch.getTime() / 1000.0 + " secs");

            transaction.commit();

        } finally {
            em.close();
        }
    }

    @Override
    public void deleteOldConsentEntries(long patientId, List<User> oldUsers, long communityId) throws Exception {
         System.out.println("update communityId:" + communityId);

        for (User tempUser : oldUsers) {
            Long userId = tempUser.getUserId();
            UserPatientConsentPK userPatientConsentPK = new UserPatientConsentPK(patientId, userId, communityId);

            destroy(userPatientConsentPK);
        }

    }

    @Override
    public List<User> getUsers(long organizationId, long communityId) {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createNativeQuery("SELECT * FROM connect.user u JOIN connect.user_community_organization uco ON "
                    + "uco.user_id = u.user_id "
                    + "WHERE (u.deleted = 'N' or u.deleted is null) "
                    + "AND uco.community_id = ?1 "
                    + "AND uco.org_id = ?2 ", User.class)
                    .setParameter(1, communityId)
                    .setParameter(2, organizationId);
                    //.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }

    }

    @Override
    public boolean hasConsent(long userId, long patientId, long communityId) {
        //TODO refactor add communityId to hasConsent method
        
        //DASHBOARD-609 use database view
        EntityManager em = getEntityManager();
        try {       
           String jpql = PatientConsentUtils.buildJpqlForHasConsent(userId, patientId, communityId);
          
            
            Query q = em.createQuery(jpql);
            
            //q.setParameter("theUserId", userId);
            //q.setParameter("thePatientId", patientId);

            int count = ((Long) q.getSingleResult()).intValue();
            logger.fine("jpql count=" + count);
            return count >= 1;   //back to permits with the view 
        } finally {
            em.close();
        }
    }

    public boolean exists(UserPatientConsentPK id) {
        UserPatientConsent obj = this.findUserPatientConsent(id);

        return obj != null;
    }

    public List<Long> getUserIds(long organizationId, long communityId) {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createNativeQuery("SELECT u.user_id FROM connect.user u JOIN connect.user_community_organization uco ON "
                    + "uco.user_id = u.user_id "
                    + "WHERE (u.deleted = 'N' or u.deleted is null) "
                    + "AND uco.community_id = ?1 "
                    + "AND uco.org_id = ?2 ", User.class)
                    .setParameter(1, communityId)
                    .setParameter(2, organizationId);
                    //.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Long> getUserIds(List<OrganizationPatientConsent> orgs, long communityId) {
        EntityManager em = getEntityManager();

        try {
            String inClause = buildInClause(orgs);
            Query q = em.createNativeQuery("SELECT u.user_id FROM connect.user u JOIN connect.user_community_organization uco ON "
                    + "uco.user_id = u.user_id "
                    + "WHERE (u.deleted = 'N' or u.deleted is null) "
                    + "AND uco.community_id = ?1 "
                    + "AND uco.org_id in (?2) ")
                    .setParameter(1, communityId)
                    .setParameter(2, inClause);
                    //.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    protected static String buildInClause(List<OrganizationPatientConsent> theList) {
        StringBuilder result = new StringBuilder();

        result.append("(");
        boolean first = true;

        for (OrganizationPatientConsent temp : theList) {

            long organizationId = temp.getOrganizationPatientConsentPK().getOrganizationId();

            StringBuilder data = new StringBuilder();
            if (!first) {
                data.append(", ");
            } else {
                first = false;
            }

            result.append(data);
            result.append(organizationId);
        }

        result.append(")");

        return result.toString();
    }
    
    public boolean hasUserPatientConsent(UserPatientConsent userPatientConsent) {

        //Only persist userPatient Consent if consent status = deny 
        return userPatientConsent.getConsentStatus().equalsIgnoreCase(ConsentConstants.CONSENT_PERMIT);
     
    }
}
