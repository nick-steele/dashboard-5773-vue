package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.County;

import java.util.List;

/**
 * Created by rsundar on 10/10/2017.
 */
public interface CountyDAO {

    public List<County> findCountys(long communityId);

    public County findcounty(long communityId, int countyId);
}
