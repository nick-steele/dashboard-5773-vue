package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.CommunityCareteam;
import com.gsihealth.entity.NotificationCareteamRole;
import com.gsihealth.entity.PatientCommunityCareteam;
import com.gsihealth.entity.PatientCommunityCareteamPK;
import com.gsihealth.entity.PatientEngagementRole;
import com.gsihealth.entity.PatientCommunityEnrollment;
import com.gsihealth.entity.Role;
import com.gsihealth.entity.User;
import com.gsihealth.entity.UserCommunityCareteam;
import com.gsihealth.entity.UserCommunityCareteamPK;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.gsihealth.dashboard.server.dao.exceptions.IllegalOrphanException;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.PreexistingEntityException;
import java.util.List;
import javax.persistence.NoResultException;

/**
 *
 * @author Chad Darby
 */
public interface CommunityCareteamDAO {

    public void create(CommunityCareteam communityCareteam);

    public void create(UserCommunityCareteam userCommunityCareteam) throws PreexistingEntityException, Exception;

    public void create(PatientCommunityCareteam patientCommunityCareteam) throws PreexistingEntityException, Exception;

    public void destroyCommunityCareteam(Long id) throws IllegalOrphanException, NonexistentEntityException;

    public void destroy(UserCommunityCareteamPK id) throws NonexistentEntityException;

    public void destroy(PatientCommunityCareteamPK id) throws NonexistentEntityException;

    public void edit(CommunityCareteam communityCareteam, List<UserDTO> users,long communityId) throws IllegalOrphanException, NonexistentEntityException, Exception;

    public void edit(UserCommunityCareteam userCommunityCareteam) throws NonexistentEntityException, Exception;

    public void edit(PatientCommunityCareteam patientCommunityCareteam) throws NonexistentEntityException, Exception;

    public CommunityCareteam findCommunityCareteam(Long id);

    //JIRA 1178 - Spira 5201
    public CommunityCareteam findCommunityCareteam(String name, Long communityId);

    public List<CommunityCareteam> findCommunityCareteamEntities(Long communityId);

    public PatientCommunityCareteam findPatientCommunityCareteam(PatientCommunityCareteamPK id);

    public List<PatientCommunityCareteam> findPatientCommunityCareteamEntities();

    public UserCommunityCareteam findUserCommunityCareteam(UserCommunityCareteamPK id);

    public boolean doesPatientCommunityCareteamExist(Long patientId);

    public CommunityCareteam findCommunityCareteamForPatient(Long patientId);

    public void update(long communityCareteamId, long patientId);

    public List<User> getUsersForCareteam(long communityCareteamId);

    public List<User> getAvailableUsers();

    public List<User> getAvailableUsers(String role, List<Long> userIdsToExclude, Long communityId);

    public int getTotalCountForAvailableUsers(String role, List<Long> userIdsToExclude, Long communityId);
    
    //JIRA 1178 - Spira 5201
    public boolean doesCareteamNameExistForOtherTeam(long careteamId, String careteamName, Long communityId);

    //JIRA 1178 - Spira 5201
    public boolean doesCareteamNameExistForOtherTeam(String careteamName, Long communityId);
    
    //JIRA 1178 - Spira 5201
    public void create(CommunityCareteam careteam, List<UserDTO> users, Long communityId) throws Exception;

    public void remove(long patientId);

    public boolean hasPatientsAssignedToCareteam(long communityCareteamId);

    public boolean hasOtherPatientsAssignedToCareteam(long communityCareteamId, long patientId);

    public List<String> getRequiredRolesForCareteam(long communityId);

    public List<String> getDuplicateRolesForCareteam(long communityId);
    
    public List<NotificationCareteamRole> getNotificationCareteamRoles(Long communityId);
    
    public List<String> getPatientEuidsAssignedToCareteam(long communityCareteamId);

    public List<Role> getUserRoles(long communityId);
    
    //pe
    public PatientCommunityEnrollment findPatientEnrollment(String euid) throws NoResultException;
    
    public void activateDeactivatePatientUserAccount(PatientCommunityEnrollment patientEnrollment) throws Exception;
    
    public PatientEngagementRole findPatientEngagementRole(long communityId) throws NoResultException;
    
    public List<PatientCommunityEnrollment> findPatientEnrollmentList(String emailAddress) throws NoResultException;
    
    public void associatedPatientCommunityCareteam(long communityCareteamId, long patientId,String patientActive)throws Exception;
 
    public List<PatientCommunityEnrollment> findPatientEnrollmentWithDirectAddress(String directAddress) throws NoResultException ;
    
    public boolean isSameCareTeamAssigned(long communityCareteamId, long patientId);
	
    public PatientCommunityCareteam findAssociatedPatientFlag(Long communityCareteamId,Long patientId);
    
    public void updateCateTeamEndDate(long patientId);
}
