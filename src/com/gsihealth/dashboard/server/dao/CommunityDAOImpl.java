/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.Community;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;

/**
 *
 * @author User
 */
public class CommunityDAOImpl implements CommunityDAO{
    
    private EntityManagerFactory emf = null;

    public CommunityDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public CommunityDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public Community getCommunityById(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Community.class, id);
        } finally {
            em.close();
        }
    }
    
    @Override
    public Community getCommunityById(Long id, EntityManager em){
        return em.find(Community.class, id);
    }

    @Override
    public List<Community> findEntities() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Community as o order by o.communityId");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return q.getResultList();
        } finally {
            em.close();
        }              
    }
        
}
