package com.gsihealth.dashboard.server.dao;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.communication.CommunicationManager;
import com.gsihealth.dashboard.server.util.URLGenerator;
import com.sun.jersey.api.client.ClientResponse;

import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Vishal (Off.)
 */
public class SSOUserDAOImpl implements SSOUserDAO {

    public static final int SUCCESS = 200;
    public static final String AMUSER = "amAdmin";
    private CommunicationManager communicationManager = null;
    
    //JIRA 1853
    private static final String UPD_PWD_STR = "/updatePasswords/{0}/{1}/{2}/{3}";
    private static final String IS_ACTIVE_USR_COMMUNITY_STR = "/isUserActiveInCommunity/{0}/{1}/{2}";
    private static final String IS_ACTIVE_USR_COMMUNITY_OTHER_STR = "/isUserActiveNotInOtherCommunity/{0}/{1}/{2}";
    private static final String IS_ACTIVE_USR_STR = "/isUserActive/{0}/{1}/{2}";
    private static final String GET_ENC_PWD_STR = "/addCPUserCommunity/{0}/{1}/{2}/{3}/{4}";
    private static final String SET_USR_STATUS_STR = "/setUserStatus/{0}/{1}/{2}/{3}";
    private static final String UNLOCK_ACT_STR = "/unlockAccount/{0}/{1}";
    
    private static final MessageFormat UPD_PWD_FMT = new MessageFormat(UPD_PWD_STR);
    private static final MessageFormat IS_ACTIVE_USR_FMT = new MessageFormat(IS_ACTIVE_USR_STR);
    private static final MessageFormat IS_ACTIVE_USR_COMMUNITY_FMT = new MessageFormat(IS_ACTIVE_USR_COMMUNITY_STR);
    private static final MessageFormat IS_ACTIVE_USR_COMMUNITY_OTHER_FMT = new MessageFormat(IS_ACTIVE_USR_COMMUNITY_OTHER_STR);
    private static final MessageFormat GET_ENC_PWD_FMT = new MessageFormat(GET_ENC_PWD_STR);
    private static final MessageFormat SET_USR_STATUS_FMT = new MessageFormat(SET_USR_STATUS_STR);
    private static final MessageFormat UNLOCK_ACT_FMT = new MessageFormat(UNLOCK_ACT_STR);

    public SSOUserDAOImpl() {
        communicationManager = new CommunicationManager();
    }
    
    @Override
    public void addUser(long communityId, String tokenId, String ssoUser)  throws Exception {
        
        URLGenerator urlGenerator = new URLGenerator(communityId);
        URI createURI = urlGenerator.getCreateURI(tokenId, ssoUser);
        System.out.println("CREATE USER  " + createURI.toString());
        communicationManager.callGET(createURI);
    }

    public void deleteUser(long communityId, String tokenId, String ssoUser)  throws Exception {
        URLGenerator urlGenerator = new URLGenerator(communityId);
        URI deleteURI = urlGenerator.getDeleteURI(tokenId, ssoUser);
        System.out.println("DELETE USER  " +deleteURI.toString());
        communicationManager.callGET(deleteURI);
    }

    public void updateUser(long communityId, String tokenId, String ssoUser)  throws Exception {
        URLGenerator urlGenerator = new URLGenerator(communityId);
        URI updateURI = urlGenerator.getUpdateURI(tokenId, ssoUser);
        System.out.println("UPDATE USER  "+updateURI.toString());
        communicationManager.callGET(updateURI);
    }
    
    public boolean validate( long communityId, String tokenId)  throws Exception {
        URLGenerator urlGenerator = new URLGenerator(communityId);
        URL validateURL = urlGenerator.getValidateURI(tokenId);
        System.out.println("VALIDATE TOKEN  "+validateURL.toString());
        String clientResponse = communicationManager.callValidatePOST(validateURL);
        boolean tokenValid = clientResponse.equals("boolean=true");
        return tokenValid;
    }

    public String authenticate(long communityId, String user, String password)  throws Exception {
        
        Map<String, String> ssoUserAccess = new LinkedHashMap<String, String>();
        ssoUserAccess.put("username", user);
        ssoUserAccess.put("password", password);
        

        String result = authenticate(communityId, ssoUserAccess);

        return result;
    }

    /**
     * FIXME openam admin user is hardcoded - should use the configuration table
     * @param communityId
     * @param ssoUserAccess
     * @return
     * @throws Exception 
     */
    public String authenticate(long communityId, Map ssoUserAccess) throws Exception {
        URLGenerator urlGenerator = new URLGenerator(communityId);
        URI authenticateURI = urlGenerator.getAuthenticateURI();
         String realm = urlGenerator.getRealm();
         String name =(String) ssoUserAccess.get("username");
        
        // if name !=  admin, name is trying to log in and you need to add a realm, otherwise, you're adding someone and it needs to be done at top level realm
        if(realm != null && (!AMUSER.equalsIgnoreCase(name))){ 
            ssoUserAccess.put("uri", "realm=" + realm);
        }
        ClientResponse clientResponse = communicationManager.callLoginPOST(authenticateURI, ssoUserAccess);

        if (clientResponse.getStatus() == SUCCESS) {
            String result = clientResponse.getEntity(String.class);
            if (result.contains("=")) {
                String token = result.split("=")[1];

                return token;
            }
        }

        return "User Missing";
    }
    
    //JIRA 1122
    public void logout(long communityId, String tokenId)  throws Exception
    {
        Map<String, String> logoutMap = new LinkedHashMap<String, String>();
        logoutMap.put("subjectid", tokenId);
        
        URLGenerator urlGenerator = new URLGenerator(communityId);
        URI logoutURI = urlGenerator.getLogoutURI();
        
        communicationManager.callLogoutPOST(logoutURI, logoutMap);
    }
    
    public String avadoPOST(long communityId, String tokenId) throws Exception {
        URLGenerator urlGenerator = new URLGenerator(communityId);
        URI authenticateURI = urlGenerator.getAvadoURI();

        Map<String, String> ssoUserAccess = new LinkedHashMap<String, String>();
        ssoUserAccess.put("tokenid", tokenId);
        
        ClientResponse clientResponse = communicationManager.callPOST(authenticateURI, ssoUserAccess);

        if (clientResponse.getStatus() == SUCCESS) {
            String result = clientResponse.getEntity(String.class);
            if (result.contains("=")) {
                String token = result.split("=")[1];

                return token;
            }
        }

        return "Can't connect to Avado";
    }

    //JIRA 1853
    @Override
    public String changePassword(Long communityId, String token, String userEmail, String newPassword) throws Exception {
        //updateUser(communityId, tokenId, ssoUser);
        //get from db
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();        
        String umURL = configurationDAO.getProperty(communityId, "user.manager.server");

        String url = umURL + UPD_PWD_FMT.format(new String[] {userEmail, token, communityId.toString(), URLEncoder.encode(newPassword, "UTF-8")});
        System.out.println("UM call:" + url);
        URI uri = new URI(url);
        //PUT call
        ClientResponse resp = communicationManager.callPUT(uri, null);
        
        String respStr = resp.getEntity(String.class);
        System.out.println("changePassword response:" + respStr);
        
        Gson gson = new Gson();
        JsonElement element = gson.fromJson (respStr, JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        JsonElement jsonRespObj = jsonObj.get("response");

        if(jsonRespObj != null) {
            String jsonResp = jsonRespObj.getAsString();
            System.out.println("response:" + jsonResp);            
            if(jsonResp != null && jsonResp.startsWith("failure")) {
                throw new Exception("Failure in call to UserManager:" + jsonResp);
            }
        }
        String jsonToken = null;
        JsonElement jsonTokenObj = jsonObj.get("token");
        if(jsonTokenObj != null) {
            jsonToken = jsonTokenObj.getAsString();
            System.out.println("token:" + jsonToken);
        }
        
        return jsonToken;        
    }
    
    @Override
    public String addUserToCP(Long communityId, String token, String userEmail,Long userId,String status) throws Exception {
        //for now, the call is same as below and we will ignore the response
        //later on if the call is extracted out separatel in UM, we can change this
        return getEncryptedPassword(communityId, token, userEmail,userId, status);
    }
    
    //Spria 6537 - based on Vince solution
    @Override
    public String getEncryptedPassword(Long communityId, String token, String userEmail,Long userId,String status) throws Exception {
        //get from db
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();        
        String umURL = configurationDAO.getProperty(communityId, "user.manager.server");

        String url = umURL + GET_ENC_PWD_FMT.format(new String[] {userEmail, token, communityId.toString(),userId.toString(),status});
        System.out.println("UM call:" + url);
        URI uri = new URI(url);
        //GET call
        ClientResponse resp = communicationManager.callGET(uri, "application/json");
        String respStr = resp.getEntity(String.class);
        System.out.println("getEncryptedPassword response:" + respStr);
        
        Gson gson = new Gson();
        JsonElement element = gson.fromJson (respStr, JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        String jsonResp = jsonObj.get("response").getAsString();
        System.out.println("json pwd:" + jsonResp);
        if(jsonResp.startsWith("failure")) {
            throw new Exception("Failure in call to UserManager:" + jsonResp);
        }
        return jsonResp;
    }

    @Override
    public boolean isUserActive(Long communityId, String token, String userEmail) throws Exception
    {
        //get from db
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();        
        String umURL = configurationDAO.getProperty(communityId, "user.manager.server");

        String url = umURL + IS_ACTIVE_USR_FMT.format(new String[] {userEmail, token, communityId.toString()});
        System.out.println("UM call:" + url);
        URI uri = new URI(url);
        //GET call
        ClientResponse resp = communicationManager.callGET(uri, "application/json");
        String respStr = resp.getEntity(String.class);
        System.out.println("isActive response:" + respStr);
        
        Gson gson = new Gson();
        JsonElement element = gson.fromJson (respStr, JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        return jsonObj.get("response").getAsBoolean();
    }
    
    @Override
    public void setUserActive(Long communityId, String token, String userEmail, boolean active) throws Exception
    {
        String status = active ? "enabled" : "disabled";
        //get from db
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();        
        String umURL = configurationDAO.getProperty(communityId, "user.manager.server");

        String url = umURL + SET_USR_STATUS_FMT.format(new String[] {userEmail, token, communityId.toString(), status});
        System.out.println("UM call:" + url);
        URI uri = new URI(url);
        //PUT call
        ClientResponse resp = communicationManager.callPUT(uri, null);
        System.out.println("setUserActive response:" + resp.toString());        
    }
    
    //Spira 8057
    @Override
    public void unlockAccount(Long communityId, String token, String userEmail) throws Exception {
        //get from db
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();        
        String umURL = configurationDAO.getProperty(communityId, "user.manager.server");

        String url = umURL + UNLOCK_ACT_FMT.format(new String[] {userEmail, communityId.toString()});
		String jsonToken = "{\"token\": \"" + token + "\"}";
        System.out.println("unlockAccount UM call:" + url);
        URI uri = new URI(url);
        //PUT call
        ClientResponse resp = communicationManager.callPUTJson(uri, jsonToken);
        
        String respStr = resp.getEntity(String.class);
        System.out.println("unlockAccount response:" + respStr);
        
        Gson gson = new Gson();
        JsonElement element = gson.fromJson (respStr, JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        System.out.println("unlockAccount response:" + resp.toString());
    }

    @Override
    public boolean isUserActiveInCommunity(Long communityId, String token, String userEmail) throws Exception {
        //get from db
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        String umURL = configurationDAO.getProperty(communityId, "user.manager.server");

        String url = umURL + IS_ACTIVE_USR_COMMUNITY_FMT.format(new String[] {userEmail, token, communityId.toString()});
        System.out.println("UM call:" + url);
        URI uri = new URI(url);
        //GET call
        ClientResponse resp = communicationManager.callGET(uri, "application/json");
        String respStr = resp.getEntity(String.class);
        System.out.println("isActive response:" + respStr);

        Gson gson = new Gson();
        JsonElement element = gson.fromJson (respStr, JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        return jsonObj.get("response").getAsBoolean();
    }

    @Override
    public boolean isUserActiveNotInOtherCommunity(Long communityId, String token, String userEmail) throws Exception {
        //get from db
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        String umURL = configurationDAO.getProperty(communityId, "user.manager.server");

        String url = umURL + IS_ACTIVE_USR_COMMUNITY_OTHER_FMT.format(new String[] {userEmail, token, communityId.toString()});
        System.out.println("UM call:" + url);
        URI uri = new URI(url);
        //GET call
        ClientResponse resp = communicationManager.callGET(uri, "application/json");
        String respStr = resp.getEntity(String.class);
        System.out.println("isActive response:" + respStr);

        Gson gson = new Gson();
        JsonElement element = gson.fromJson (respStr, JsonElement.class);
        JsonObject jsonObj = element.getAsJsonObject();
        return jsonObj.get("response").getAsBoolean();
    }
}
