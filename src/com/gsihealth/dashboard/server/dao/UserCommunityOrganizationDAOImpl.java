/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.CommunityOrganization;
import com.gsihealth.entity.UserCommunityOrganization;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author User
 */
public class UserCommunityOrganizationDAOImpl implements UserCommunityOrganizationDAO {

    private EntityManagerFactory emf = null;

    public UserCommunityOrganizationDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public UserCommunityOrganizationDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public void create(UserCommunityOrganization userCommunityOrganization) {

        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            em.persist(userCommunityOrganization);

            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void update(UserCommunityOrganization userCommunityOrganization) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.merge(userCommunityOrganization);

            em.getTransaction().commit();

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<UserCommunityOrganization> findUserCommunityOrganizationEntities() {
        return findUserCommunityOrganizationEntities(true, -1, -1);
    }

    public List<UserCommunityOrganization> findUserCommunityOrganizationEntities(int maxResults, int firstResult) {
        return findUserCommunityOrganizationEntities(false, maxResults, firstResult);
    }

    private List<UserCommunityOrganization> findUserCommunityOrganizationEntities(CommunityOrganization communityOrganization, boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Long commId = communityOrganization.getCommunity().getCommunityId();
            Long orgId = communityOrganization.getOrganizationCwd().getOrgId();
            
            Query q = em.createNamedQuery("UserCommunityOrganization.findByOrganizationAndCommunity");
            q.setParameter("communityId", commId);
            q.setParameter("organizationId", orgId);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    private List<UserCommunityOrganization> findUserCommunityOrganizationEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from UserCommunityOrganization as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    @Override
    public List<UserCommunityOrganization> findUserCommunityOrganizationEntities(Long userId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("UserCommunityOrganization.findByUser")
                    .setParameter("userId", userId);
            
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public UserCommunityOrganization findUserCommunityOrganization(Long userId, Long communityId) {
        EntityManager em = getEntityManager();
        try {
        Query q = em.createNamedQuery("UserCommunityOrganization.findByUserAndCommunity")
                .setParameter("userId", userId)
                .setParameter("communityId", communityId);
        
            return (UserCommunityOrganization)q.getSingleResult();
        } finally {
            em.close();
        }
    }

    @Override
    public int getUserCommunityOrganizationCount() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<UserCommunityOrganization> findUserCommunityOrganizationEntitiesByCommunityOrganization(CommunityOrganization communityOrganization) {
        return findUserCommunityOrganizationEntities(communityOrganization, true, -1, -1);
    }
}
