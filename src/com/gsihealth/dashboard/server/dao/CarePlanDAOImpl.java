package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.server.ConfigurationContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.util.List;
import com.gsihealth.dashboard.server.reports.EncounterDate;
import com.gsihealth.dashboard.server.reports.EncounterMode;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;
import java.util.logging.Logger;
import javax.persistence.Persistence;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;

/**
 *
 * @author Chad Darby
 */
public class CarePlanDAOImpl implements CarePlanDAO {

    private Logger logger = Logger.getLogger(getClass().getName());
    private EntityManagerFactory emf = null;

    public CarePlanDAOImpl() {
        emf = Persistence.createEntityManagerFactory("careplanPU_standalone");
    }

    public CarePlanDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public Map<Long, EncounterDate> findEncounterDates(long communityId) {
        Map<Long, EncounterDate> encounterDates = new HashMap<Long, EncounterDate>();

        EntityManager em = getEntityManager();

        try {
            String sql = "select  patient_id, min(cpe.encounter_start_date_time) ,  max(cpe.encounter_start_date_time)"
                    + " from care_plan_encounter cpe , care_plan cp"
                    + " where cpe.care_plan_id = cp.care_plan_id and cp.community_id = " + communityId
                    + " group by patient_id";

            Query patientQuery = em.createNativeQuery(sql);
            //patientQuery.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<Vector> theList = (List<Vector>) patientQuery.getResultList();

            for (Vector tempRow : theList) {
                long patientId = (Long) tempRow.get(0);
                Date earliestDate = (Date) tempRow.get(1);
                Date latestDate = (Date) tempRow.get(2);

                EncounterDate tempEncounterDate = new EncounterDate();
                tempEncounterDate.setEarliestDateOfContact(earliestDate);
                tempEncounterDate.setLatestDateOfContact(latestDate);

                encounterDates.put(patientId, tempEncounterDate);
            }
        } finally {
            em.close();
        }

        return encounterDates;
    }

    @Override
    public Map<Long, Date> findInitialAssessmentDates(long communityId) {
        Map<Long, Date> initialAssessmentDates = new HashMap<Long, Date>();

        EntityManager em = getEntityManager();

        try {
            String sql = "select patient_id, min(ai.assessment_date) from assessment_info ai, care_plan cp"
                    + " where ai.care_plan_id = cp.care_plan_id"
                    + " and cp.community_id=" + communityId
                    + " group by patient_id";

            Query patientQuery = em.createNativeQuery(sql);
            //patientQuery.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<Vector> theList = (List<Vector>) patientQuery.getResultList();

            for (Vector tempRow : theList) {
                long patientId = (Long) tempRow.get(0);
                Date tempAssessmentDate = (Date) tempRow.get(1);

                initialAssessmentDates.put(patientId, tempAssessmentDate);
            }
        } finally {
            em.close();
        }

        return initialAssessmentDates;
    }

    //JIRA 629
    @Override
    public Map<Long, EncounterMode> findEncounterModeCounts(long communityId) {
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        // load encounter types from database configs
        Set<String> PERSON_ENCOUNTERS = loadEncountersConfiguration("person.encounters", configurationDAO, communityId);
        logger.info("Person encounter mappings from dashboard prop file: " + PERSON_ENCOUNTERS);
        Set<String> PHONE_ENCOUNTERS = loadEncountersConfiguration("phone.encounters", configurationDAO, communityId);
        logger.info("Phone encounter mappings from dashboard prop file: " + PHONE_ENCOUNTERS);
        Set<String> MAIL_ENCOUNTERS = loadEncountersConfiguration("mail.encounters", configurationDAO, communityId);
        logger.info("Mail encounter mappings from dashboard prop file: " + MAIL_ENCOUNTERS);

        Map<Long, EncounterMode> encounterModeCounts = new HashMap<Long, EncounterMode>();

        EntityManager em = getEntityManager();

        try {
            String sql = "select patient_id, encounter_mode,count(care_plan_encounter_id) from care_plan_encounter cpe , care_plan cp"
                    + " where cpe.care_plan_id = cp.care_plan_id and cp.community_id=" + communityId
                    + " group by patient_id,encounter_mode";

            Query patientQuery = em.createNativeQuery(sql);
            //patientQuery.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<Vector> theList = (List<Vector>) patientQuery.getResultList();

            for (Vector tempRow : theList) {
                // read row data
                long patientId = (Long) tempRow.get(0);
                String encounterModeDescription = (String) tempRow.get(1);
                Long longCount = (Long) tempRow.get(2);

                int count;
                if (longCount != null) {
                    count = longCount.intValue();
                } else {
                    continue;
                }

                // get encounter mode from the map, add one if needed
                EncounterMode tempEncounterMode = getEncounterMode(patientId, encounterModeCounts);

                // handle phone
                if (PHONE_ENCOUNTERS.contains(encounterModeDescription)) {
                    tempEncounterMode.addPhoneCount(count);
                } else if (PERSON_ENCOUNTERS.contains(encounterModeDescription)) {
                    // handle person
                    tempEncounterMode.addPersonCount(count);
                } else if (MAIL_ENCOUNTERS.contains(encounterModeDescription)) {
                    // handle mail
                    tempEncounterMode.addMailCount(count);
                }
            }
        } finally {
            em.close();
        }

        return encounterModeCounts;
    }

    /**
     * Get the encounter mode from map, create a new one if needed
     *
     * @param patientId
     * @param encounterModeCounts
     * @return
     */
    private EncounterMode getEncounterMode(long patientId, Map<Long, EncounterMode> encounterModeCounts) {
        EncounterMode tempEncounterMode = null;

        if (encounterModeCounts.containsKey(patientId)) {
            tempEncounterMode = encounterModeCounts.get(patientId);
        } else {
            tempEncounterMode = new EncounterMode();
            encounterModeCounts.put(patientId, tempEncounterMode);
        }

        return tempEncounterMode;
    }

    private Set<String> loadEncountersConfiguration(String encounterPropName, ConfigurationDAO configurationDAO, long communityId) {
        // reading properties
        
        String[] strArrayEncounters = configurationDAO.getProperty(communityId, encounterPropName).split(",");

        // adding as list
        return new HashSet<>(Arrays.asList(strArrayEncounters));
    }
}
