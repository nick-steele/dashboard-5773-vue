/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.DisenrollmentReasons;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;

/**
 *
 * @author Satyendra Singh
 */
public class DisenrollmentReasonDAOImpl implements DisenrollmentReasonDAO {

    private EntityManagerFactory emf = null;

    public DisenrollmentReasonDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public DisenrollmentReasonDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public List<DisenrollmentReasons> getDisenrollmentReasons(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select d from DisenrollmentReasons as d where d.disenrollmentReasonsPK.communityId= :theCommunityId");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public DisenrollmentReasons getDisenrollmentReason(Long communityId, String reason) {
        EntityManager em = getEntityManager();
        DisenrollmentReasons disenrollmentReasons = null;
        try {


            Query q = em.createQuery("SELECT d FROM DisenrollmentReasons as d WHERE d.reason = :reason and d.disenrollmentReasonsPK.communityId= :theCommunityId");
            q.setParameter("reason", reason);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            disenrollmentReasons = (DisenrollmentReasons) q.getSingleResult();

            return disenrollmentReasons;
        } finally {
            em.close();
        }
    }

    @Override
    public Long getDisenrollmentId(String reason)  throws NoResultException {
        EntityManager em = getEntityManager();
        DisenrollmentReasons disenrollmentReasons = null;
        try {


Query q = em.createQuery("SELECT d FROM DisenrollmentReasons as d WHERE d.reason = :reason");
            q.setParameter("reason", reason);
            //q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            disenrollmentReasons = (DisenrollmentReasons) q.getSingleResult();

            return disenrollmentReasons.getDisenrollmentReasonsPK().getId();

        } finally {
            em.close();
        }
    }
}
