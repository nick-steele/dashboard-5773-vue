/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.DocumentResourceCenter;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class DocumentResourceCenterDAOImpl implements DocumentResourceCenterDAO {

    private EntityManagerFactory emf = null;
    private Logger logger = Logger.getLogger(getClass().getName());

    public DocumentResourceCenterDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public DocumentResourceCenterDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public List<DocumentResourceCenter> findDocumentEntities(long communityId) {

        List<DocumentResourceCenter> docResourceCenterList = null;
        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery("select object(o) from DocumentResourceCenter as o where o.documentResourceCenterPK.communityId=:theCommunityId order by o.documentTitle");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            docResourceCenterList = q.getResultList();
        } finally {
            em.close();
        }

        return docResourceCenterList;
    }
}
