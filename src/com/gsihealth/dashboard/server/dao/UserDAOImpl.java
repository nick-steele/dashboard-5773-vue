package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.util.DaoUtils;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import com.gsihealth.entity.AccessLevel;
import com.gsihealth.entity.User;
import com.gsihealth.entity.UserCommunityOrganization;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.StopWatch;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chad Darby
 */
public class UserDAOImpl implements UserDAO {

    private EntityManagerFactory emf = null;
    private final Logger logger = Logger.getLogger(getClass().getName());

    public UserDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public UserDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public void create(User user) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            Date now = new Date();

            user.setCreationDate(now);
            user.setLastUpdateDate(now);
    
            em.persist(user);

            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * update user table - demog info
     * @param user
     * @throws NonexistentEntityException
     * @throws Exception 
     */
    @Override
    public void update(User user) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
                       
            User persistentUser= findUser(user.getUserId());
            user.setLastUpdateDate(new Date());
            logger.info("persisting user: " + user.toString());
            // force load of data from db
            persistentUser.setFirstName(user.getFirstName());
            em.merge(user);

            em.getTransaction().commit();
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /**
     * Permanent permDelete from the database
     * 
     * @param id
     * @throws NonexistentEntityException
     */
    @Override
    public void permDelete(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            User user;
            try {
                user = em.getReference(User.class, id);
                user.getUserId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The user with id " + id + " no longer exists.", enfe);
            }

            em.remove(user);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }


    @Override
    public List<User> findUserEntities() {
        return findUserEntities(true, -1, -1);
    }

    private List<User> findUserEntities(boolean all, int pageNumber, int pageSize) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from User as o where o.isDeleted = 'N' OR o.isDeleted IS NULL order by o.lastName ");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            if (!all) {
                int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);

                q.setFirstResult(startRecordNumber);
                q.setMaxResults(pageSize);
            }

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<User> findUserEntities(long communityId, AccessLevel accessLevel) {
        return findUserEntities(communityId,accessLevel, true, -1, -1);
    }

    public List<User> findUserEntities(UserCommunityOrganization userCommunityOrganization, AccessLevel accessLevel) {
        return findUserEntities(userCommunityOrganization, accessLevel, true, -1, -1);
    }

    public List<User> findUserEntities(long communityId, AccessLevel accessLevel, int pageNumber, int pageSize) {

        return findUserEntities(communityId,accessLevel, false, pageNumber, pageSize);
    }

    private List<User> findUserEntities(long communityId, AccessLevel accessLevel, boolean all, int pageNumber, int pageSize) {
        EntityManager em = getEntityManager();

        try {
            Long accessLevelId = accessLevel.getAccessLevelPK().getAccessLevelId();
            logger.info("logged on user's community id is " + communityId + " searching for accessLvlId: " + accessLevelId);
            
            Query q = em.createNamedQuery("User.findByCommunityAndAccessLevel")
                    .setParameter("communityId", communityId)
                    .setParameter("accessLevelId", accessLevelId)
                    .setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            if (!all) {

                int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);

                q.setFirstResult(startRecordNumber);
                q.setMaxResults(pageSize);
            }

            List<User> resultList = q.getResultList();


            return resultList;
        } finally {
            em.close();
        }
    }

    @Override
    public int getTotalCount(long communityId, AccessLevel accessLevel) {
        int totalCount = this.findUserEntities(communityId,accessLevel).size();
        logger.fine("userDAOImpl.getTotalCount is: " + totalCount);
        return totalCount;
    }
    
    @Override
    public int getTotalUserCount(UserCommunityOrganization uco, SearchCriteria searchCriteria) {
        EntityManager em = getEntityManager();
        Long count = 0L;

        //JIRA 860-1460 (1467)
        //Migrate EclipseLink Expression to pure JPA Criteria
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<User> userRoot = cq.from(User.class);
        Predicate finalQuery = this.buildUserListQuery(uco, searchCriteria, userRoot, cb);
        try {
            //ReportQuery reportQuery = new ReportQuery(User.class, finalQuery);
            //reportQuery.addCount();
            //reportQuery.useDistinct();            
            cq.select(cb.countDistinct(userRoot));
            cq.where(finalQuery);

            TypedQuery<Long> q = em.createQuery(cq);
            count = q.getSingleResult();

            logger.fine("total reportQuery count is " + count);
            
        } catch (Exception tle) {
            logger.log(Level.SEVERE, "count failed" , tle);
        } finally {
            em.close();
        }
        return count.intValue(); 
    }
    
    /**
     * use this to build the query to find users and user count
     * @return
     */
    private Predicate buildUserListQuery(UserCommunityOrganization uco, SearchCriteria searchCriteria,
            Root<User> userRoot, CriteriaBuilder cb) {
        Long loggedOnUserCommunityId = uco.getCommunity().getCommunityId();
        Long loggedOnUseraccessLevelId = uco.getAccessLevel().getAccessLevelPK().getAccessLevelId();
        Long loggedOnUserorgId = uco.getOrganizationCwd().getOrgId();

        //JIRA 860-1460 (1467)
        //Migrate EclipseLink Expression to pure JPA Criteria
        //ExpressionBuilder exp = new ExpressionBuilder(User.class);

        // if you have no criteria, check community and not deleted - we'll look at accessLevel later
        //Expression baseExpression = (exp.get("isDeleted").isNull().or(exp.get("isDeleted").equalsIgnoreCase("N")));
        //Expression ucoExpression = exp.anyOf("userCommunityOrganization");
        //baseExpression = baseExpression.and(ucoExpression.get("community").get("communityId").equal(loggedOnUserCommunityId));
      
        // if you have no criteria, check community and not deleted - we'll look at accessLevel later        
        Predicate delPred = cb.or(userRoot.get("isDeleted").isNull(), cb.equal(userRoot.get("isDeleted"), "n"), cb.equal(userRoot.get("isDeleted"), "n"));
        Join<User, UserCommunityOrganization> ucoJoin = userRoot.join("userCommunityOrganization");
        Predicate ucoPred = cb.equal(ucoJoin.get("community").get("communityId"), loggedOnUserCommunityId);
        Predicate basePredicate = cb.and(delPred, ucoPred);

        // take care of accesslevel, superusers and powerusers can see anyone, so no need to add them
        if(loggedOnUseraccessLevelId.equals(Constants.SUPER_USER) || loggedOnUseraccessLevelId.equals(Constants.POWER_USER)){
            // don't need to add criteria, they can see everybody
        } else {
            // lcl admin - can see all users in own org 
            logger.fine("I'm local admin, only search in my own org ");
            //Expression access = ucoExpression.get("organizationCwd").get("orgId").equal(loggedOnUserorgId);
            //baseExpression = baseExpression.and(access);
            Predicate access = cb.equal(ucoJoin.get("organizationCwd").get("orgId"), loggedOnUserorgId);
            basePredicate = cb.and(basePredicate, access);
        }  
        
        // now lets do some search criteria
        if(searchCriteria != null){
            logger.fine("we have search criteria ");
            // looking for a last name?
            String lastname = searchCriteria.getLastName();
            if(!StringUtils.isBlank(lastname)) {
                logger.fine("looking for a last name: " + lastname);
                //Expression last = exp.get("lastName").toUpperCase().like("%"+lastname.toUpperCase()+"%");
                //baseExpression = baseExpression.and(last);
                Predicate last = cb.like(cb.upper(userRoot.<String>get("lastName")), "%" + lastname.toUpperCase() + "%");
                basePredicate = cb.and(basePredicate, last);
            }
            // looking for a first name?
            String firstname = searchCriteria.getFirstName();
            if(!StringUtils.isBlank(firstname)) {
                logger.fine("looking for a first name: " + firstname);
                //Expression first = exp.get("firstName").toUpperCase().like("%"+firstname.toUpperCase()+"%");
                //baseExpression = baseExpression.and(first);
                Predicate first = cb.like(cb.upper(userRoot.<String>get("firstName")), "%" + firstname.toUpperCase() + "%");
                basePredicate = cb.and(basePredicate, first);
            }
            
            // you see where this is going right?
            String email = searchCriteria.getEmail();
            if(!StringUtils.isBlank(email)) {
                logger.fine("looking for an email: " + email);
                //Expression emailExp = exp.get("email").equalsIgnoreCase(email);
                //baseExpression = baseExpression.and(emailExp);
                Predicate emailPred = cb.equal(cb.lower(userRoot.<String>get("email")), email.toLowerCase());
                basePredicate = cb.and(basePredicate, emailPred);
            }
        }

         return basePredicate;
    }
    
    /**
     * FIXME - this will always only return one user whats the point?
     * @param userCommunityOrganization
     * @param accessLevel
     * @param all
     * @param maxResults
     * @param firstResult
     * @return 
     */
    private List<User> findUserEntities(UserCommunityOrganization userCommunityOrganization, AccessLevel accessLevel, boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();

        try {
             Query q = em.createNamedQuery("User.findByCommunityAndAccessLevel")
                    .setParameter("communityId", userCommunityOrganization.getCommunity().getCommunityId())
                    .setParameter("accessLevelId", accessLevel.getAccessLevelPK().getAccessLevelId())
                    .setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }

            List<User> resultList = q.getResultList();
            return resultList;
        } finally {
            em.close();
        }
    }

    public User findUser(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(User.class, id);
        } finally {
            em.close();
        }
    }
 
    @Override
    public User findUserByEmail(String email, long communityId) {

        User theUser = null;

        EntityManager em = getEntityManager();

        try {
            logger.info("logged on user's community id is " + communityId);
           
            Query qry = em.createNamedQuery("User.findByCommunityAndEmail")
                    .setParameter("communityId", communityId)
                    .setParameter("email", email.toLowerCase())
                    .setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            theUser = (User)qry.getSingleResult();
        } finally {
            em.close();
        }
        
        return theUser;
    }
    
    @Override
    public User findUserByEmail(String email) {
        User theUser = null;
        EntityManager em = getEntityManager();

        try {
            Query qry = em.createNamedQuery("User.findByEmail")
                    .setParameter("email", email.toLowerCase())
                    .setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            List<User> results = qry.getResultList();
            if(results.size() > 0){
                theUser = results.get(0);
            }
            
        } finally {
            em.close();
        }
        
        return theUser;
    }
    public User findUserExistInOtherCommunity(String email, long communityId){
        User theUser = null;

        EntityManager em = getEntityManager();

        try {
            logger.info("logged on user's community id is " + communityId);

            Query qry = em.createNamedQuery("User.findUserExistInOtherCommunity")
                    .setParameter("communityId", communityId)
                    .setParameter("email", email.toLowerCase())
                    .setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            List<User> results = qry.getResultList();
            if(results.size() > 0){
                theUser = results.get(0);
            }
        } finally {
            em.close();
        }

        return theUser;
    }
    public User findUser(Long id, long communityId) {
        User theUser = null;

        EntityManager em = getEntityManager();

        try {
            logger.info("logged on user's community id is " + communityId);
           
            Query qry = em.createNamedQuery("User.findByUserAndCommunity")
                    .setParameter("communityId", communityId)
                    .setParameter("userId", id)
                    .setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            theUser = (User)qry.getSingleResult();
        } finally {
            em.close();
        }
        
        return theUser;
    }

    /**
     * @deprecated ?
     * @param communityId
     * @param accessLevel
     * @param pageNum
     * @param pageSize
     * @return 
     */
    @Override
    public List<User> findUsers(long communityId, AccessLevel accessLevel, int pageNum, int pageSize) {

        List<User> users = findUserEntities(communityId,accessLevel, pageNum, pageSize);

        return users;
    }

    public List<User> findUserByUserCommunityOrganizationAndAccessLevel(UserCommunityOrganization userCommunityOrganization, AccessLevel accessLevel) {
        List<User> admins = findUserEntities(userCommunityOrganization, accessLevel);

        return admins;
    }

    public List<User> findUsers(int pageNumber, int pageSize) {

        return this.findUserEntities(false, pageNumber, pageSize);

    }

    /**
     * @deprecated 
     * @return 
     */
    public int getTotalCountForUserEntities() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from User as o where o.isDeleted = 'N' or o.isDeleted is null order by o.lastName");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int count = ((Long) q.getSingleResult()).intValue();

            return count;
        } finally {
            em.close();
        }
    }

    @Override
    public List<User> findUsers(UserCommunityOrganization uco, SearchCriteria searchCriteria,
            int pageNumber, int pageSize) {
        EntityManager em = getEntityManager();
        List<User> users = null;
        try {
            logger.fine("findUsers - pageNumber = " + pageNumber + " pageSize = " + pageSize);
            int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);
            logger.fine("findUsers - startRecordNumber = " + startRecordNumber);

            //JIRA 860-1460 (1467)
            //Migrate EclipseLink Expression to pure JPA Criteria
            /*
            Expression finalQuery = this.buildUserListQueryExpression(uco, searchCriteria);
            readAllQuery.addAscendingOrdering("lastName");
            if(pageNumber >= 0){
                readAllQuery.setFirstResult(startRecordNumber);
                readAllQuery.setMaxRows();
            }
            */

            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<User> cq = cb.createQuery(User.class);
            Root<User> userRoot = cq.from(User.class);
            Predicate finalQuery = this.buildUserListQuery(uco, searchCriteria, userRoot, cb);
            
            cq.where(finalQuery);
            cq.orderBy(cb.asc(userRoot.get("lastName")));

            TypedQuery<User> tq = em.createQuery(cq);
            if(pageNumber >= 0) {
                tq = tq.setFirstResult(startRecordNumber)
                       .setMaxResults(pageSize);
            }
            users = tq.getResultList();

            if(users != null){
                logger.fine("returned users list size: " + users.size());
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "oh noes, something went wrong with getting users: ", ex);
        } finally {
            em.close();
        }
        
        return users;
    }
    
    public List<User> findUsers(long communityId, AccessLevel accessLevel) {

        List<User> users = findUserEntities(communityId,accessLevel);
        return users;
    }
    
    @Override
    public List<User> filterUsers(SearchCriteria searchCriteria, List<User> users) {
        
        EntityManager em = getEntityManager();

        try {
            String queryString = getQueryStringFilterUsers(searchCriteria, users);
            Query q = em.createQuery(queryString);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return q.getResultList();
        } finally {
            em.close();
        }

    }
    
    /**
     * @deprecated
     * @param searchCriteria
     * @param users
     * @return 
     */
    protected String getQueryStringFilterUsers(SearchCriteria searchCriteria, List<User> users) {

        List<Long> userIds = buildUserIds(users);
        
        String inClauseForUserIds = DaoUtils.buildInClause(userIds);

        StringBuilder queryString = new StringBuilder();

        queryString.append("select object(o) from User as o");
        queryString.append(" where o.userId in " + inClauseForUserIds);

        // first name
        String firstName = searchCriteria.getFirstName();
        if (StringUtils.isNotBlank(firstName)) {
            queryString.append(" and o.firstName = " + DaoUtils.quote(firstName));
        }

        // last name
        String lastName = searchCriteria.getLastName();
        if (StringUtils.isNotBlank(lastName)) {
            queryString.append(" and o.lastName = " + DaoUtils.quote(lastName));
        }

        // email name
        String email = searchCriteria.getEmail();
        if (StringUtils.isNotBlank(email)) {
            queryString.append(" and o.email = " + DaoUtils.quote(email));
        }
        
        queryString.append(" order by o.lastName, o.firstName");
        
        return queryString.toString();
    }    

    private List<Long> buildUserIds(List<User> users) {
        List<Long> userIds = new ArrayList<Long>();
        
        for (User tempUser : users) {
            userIds.add(tempUser.getUserId());
        }
        
        return userIds;
    }
    
    @Override
    public List<User> getTotalUsers(long communityId) {
        EntityManager em = getEntityManager();
        try {
            logger.info("logged on user's community id is " + communityId);
            Query qry = em.createNamedQuery("User.findByCommunity")
                    .setParameter("communityId", communityId)
                    .setHint(JPAConstants.REFRESH, JPAConstants.TRUE); 
            return qry.getResultList();
        } finally {
            em.close();
        }
    }

    // Not need these as User patient consent stuff is changed, as it stores only DENY
    
//    @Override
//    public Expression criteriaBuilderForUserPatientSearch(SearchCriteria searchCriteria, long loggedOnUserCommunityId) {
//
//       
//        StopWatch stopWatch = new StopWatch();
//        stopWatch.start();
//        ExpressionBuilder up = new ExpressionBuilder(UserPatientConsent.class);
//
//        // lets build an expression, we always need community id, so lets start there
//        Expression baseExpression = up.get("userPatientConsentPK").get("loggedOnUserCommunityId").equal(loggedOnUserCommunityId);
//
//        Long providerID=Long.parseLong(searchCriteria.getNetworkProviderId());
//       
//        Expression providerIdQry = up.get("userPatientConsentPK").get("userId").equal(providerID);
//        baseExpression = baseExpression.and(providerIdQry);
//        
//        
//        Expression statusQry = up.get("consentStatus").in(new String[]{"DENY"});
//        baseExpression = baseExpression.and(statusQry);
//        
//
//        stopWatch.stop();
//        
//        
//        return baseExpression;
//    }
    
//    @Override
//    public List<UserPatientConsent> getUserPatientSearchTotalCount(Expression searchUPCExpression, Integer limitPatientCount) {
//        logger.info(("lgetUPCPatientSearchTotalCount ---- rows"));
//        
//        int count = 0;
//        List<UserPatientConsent> peresults = null;
//       
//        EntityManager entityManager = this.getEntityManager();
//        try {
//            logger.info("#### ReportQuery   ReportQuery  --->");
//            ReadAllQuery readAllQuery = new ReadAllQuery(UserPatientConsent.class);
//            readAllQuery.setSelectionCriteria(searchUPCExpression);
//            readAllQuery.dontMaintainCache();
//                      
//            Session session = this.getActiveSession(entityManager);
//
//            readAllQuery.prepareForExecution();
//            peresults = (List) session.executeQuery(readAllQuery);
//            if(!peresults.isEmpty()){
//                count=peresults.size();
//                
//            }
//            
//                    
//        } catch (Exception tle) {
//            logger.severe("count failed: " + tle);
//        } finally {
//            if (entityManager != null) {
//                entityManager.close();
//            }
//        }
//        logger.info("counting done, returning " + count);
//        return peresults;
//    }
    
     @Override
     public int getAuthyId(String email,long communityId){
          EntityManager em = getEntityManager();
        try {
            logger.info("logged on user's community id is " + communityId);
            Query qry = em.createNamedQuery("User.findByCommunityAndEmail")
                    .setParameter("communityId", communityId)
                    .setParameter("email", email.toLowerCase())
                    .setHint(JPAConstants.REFRESH, JPAConstants.TRUE); 
              User theUser = (User)qry.getSingleResult();
            int id = (theUser.getAuthyId() == null) ? 0 : theUser.getAuthyId().intValue();
             logger.info("User authyId " + id);
            return id;
        } finally {
            em.close();
        }
    
     }

    public List<User> findUsers(long communityId) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("SELECT uco.user FROM UserCommunityOrganization uco "
                    + "WHERE uco.community.communityId = :theCommunityId "
                    + "order by uco.user.lastName");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            stopWatch.stop();
            logger.info("it took " + stopWatch.getTime() / 1000.0 + " findUsers userDaoImpl/654");
            return q.getResultList();
        } finally {
            em.close();
        }

    }
    
//    public static void main(String[] args) {
//    	UserDAOImpl userDaoImpl = new UserDAOImpl();
//    	User user=userDaoImpl.findUserByEmail("ss1@test.com");
//    	System.out.println(user);
//    /*CommunityDAOImpl dao = new CommunityDAOImpl();
//    	Community list = dao.getCommunityById(1L);
//    	
//    	for(OrganizationCwd co : list.getOrganizationCwds())
//    			System.out.println("output:"+co.getOrgId());*/
//    	
//    	
//    }

    @Override
    public int removeSupervisor(long supervisorId, long communityId) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();

            Query q = em.createQuery("UPDATE UserCommunityOrganization uco SET uco.supervisor = null "
                    + "WHERE uco.supervisor.userId = :supervisorId AND uco.community.communityId = :communityId");
            q.setParameter("supervisorId", supervisorId);
            q.setParameter("communityId", communityId);

            int result =  q.executeUpdate();

            em.getTransaction().commit();
            return result;
        } finally {
            em.close();
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> findUserBySupervisor(long supervisorId, long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("User.findBySupervisor");
            q.setParameter("supervisorId", supervisorId);
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public User findByEmail(String email, long communityId) {
        User user = null;

        EntityManager em = getEntityManager();

        try {
           Query qry = em.createNamedQuery("User.findByCommunityAndEmail")
                    .setParameter("communityId", communityId)
                    .setParameter("email", email.toLowerCase());
            user = (User)qry.getSingleResult();
        } finally {
            em.close();
        }

        return user;
    }
}
