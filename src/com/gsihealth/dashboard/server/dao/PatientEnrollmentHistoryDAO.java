/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.PatientEnrollmentHistory;
import java.util.Date;

/**
 *
 * @author Satyendra Singh
 */
public interface PatientEnrollmentHistoryDAO {
    
    public void addToEnrollmentHistory(PatientEnrollmentHistory patientEnrollmentHistory);
    
    public Long getLastDisenrollmentReasonForInactivation(Long patientId);
    
    public Date getEndDate(Long patientId);
    
    public Date getEnrollmentStatusChangeDate(Long patientId);
}
