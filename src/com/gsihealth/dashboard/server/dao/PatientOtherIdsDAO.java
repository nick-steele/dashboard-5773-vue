/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gsihealth.dashboard.server.dao;
import com.gsihealth.entity.PatientOtherIds;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.PreexistingEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.RollbackFailureException;
import java.util.List;

/**
 *
 * @author beth.boose
 */
public interface PatientOtherIdsDAO {
    public void add(PatientOtherIds patientOtherIds) throws PreexistingEntityException, RollbackFailureException, Exception;

    public void delete(long id) throws NonexistentEntityException, RollbackFailureException, Exception;

    public void update(PatientOtherIds patientOtherIds) throws NonexistentEntityException, RollbackFailureException, Exception;

    /** get a specific patient other id */
    public PatientOtherIds findPatientOtherIds(Long idId);

    public List<PatientOtherIds> findPatientOtherIdsEntities(long patientId, int pageNumber, int pageSize,long communityId);
//    
    public int getPatientOtherIdsCount(long patientId,long communityId);

    public boolean isDuplicate(PatientOtherIds provider);
}
