package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.gsihealth.dashboard.server.dao.exceptions.IllegalOrphanException;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.PreexistingEntityException;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import com.gsihealth.entity.*;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Chad Darby
 */
public class CommunityCareteamDAOImpl implements CommunityCareteamDAO {

    private EntityManagerFactory emf = null;
    private Logger logger = Logger.getLogger(getClass().getName());

    public CommunityCareteamDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public CommunityCareteamDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(CommunityCareteam communityCareteam) {
        EntityManager em = null;

        try {
            em = getEntityManager();
            em.getTransaction().begin();


            em.persist(communityCareteam);

            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(CommunityCareteam communityCareteam, List<UserDTO> users, long communityId) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = getEntityManager();
        EntityTransaction transaction = em.getTransaction();

        try {  //FIXME - ok, fix this mess!!!!
            transaction.begin();

            Long communityCareteamId = communityCareteam.getCommunityCareteamId();

            // update careteam name
            communityCareteam = em.merge(communityCareteam);

            List<User> oldUsers = getUsersForCareteam(communityCareteamId);

            // setup data structures
            Map<Long, UserDTO> newUsersMap = new HashMap<Long, UserDTO>();
            for (UserDTO tempUser : users) {
                newUsersMap.put(tempUser.getUserId(), tempUser);
            }

            Set<Long> newUserIdsSet = new HashSet(newUsersMap.keySet());

            Set<Long> oldUserIdsSet = new HashSet<Long>();
            for (User tempUser : oldUsers) {
                //user multi tenant work
                oldUserIdsSet.add(tempUser.getUserId());
            }

            Set<Long> originalOldUserIdsSet = new HashSet(oldUserIdsSet);

            // delete all user_community_care_teamname
            oldUserIdsSet.removeAll(newUserIdsSet);
//FIXME - why? if you're going to delete all old users, just search and and delete, no need to delete one at a time
            for (Long tempUserId : oldUserIdsSet) {
                UserCommunityCareteamPK pk = new UserCommunityCareteamPK();
                pk.setCommunityCareteamId(communityCareteamId);
                pk.setUserId(tempUserId);
                pk.setCommunityId(communityId);
                
                destroy(em, pk);
            }
            logger.info("after destroy UserCommunityCareteam ");
            // add the new entries
            newUserIdsSet.removeAll(originalOldUserIdsSet);

            for (Long tempUserId : newUserIdsSet) {
                UserDTO tempUser = newUsersMap.get(tempUserId);
                UserCommunityCareteam userCommunityCareteam = new UserCommunityCareteam();

                UserCommunityCareteamPK pk = new UserCommunityCareteamPK();
                pk.setCommunityCareteamId(communityCareteamId);
                pk.setUserId(tempUser.getUserId());
                pk.setCommunityId(communityId); 
                userCommunityCareteam.setUserCommunityCareteamPK(pk);
                userCommunityCareteam.setStartDate(new Date());
                

                em.merge(userCommunityCareteam);
            }

            transaction.commit();
        } catch (Exception exc) {

            if (transaction.isActive()) {
                transaction.rollback();
            }

            throw exc;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroyCommunityCareteam(Long id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            CommunityCareteam communityCareteam;
            try {
                communityCareteam = em.getReference(CommunityCareteam.class, id);
                communityCareteam.getCommunityCareteamId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The communityCareteam with id " + id + " no longer exists.", enfe);
            }

            List<String> illegalOrphanMessages = null;
            List<UserCommunityCareteam> userCommunityCareteamListOrphanCheck = communityCareteam.getUserCommunityCareteamList();
            for (UserCommunityCareteam userCommunityCareteamListOrphanCheckUserCommunityCareteam : userCommunityCareteamListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CommunityCareteam (" + communityCareteam + ") cannot be destroyed since the UserCommunityCareteam " + userCommunityCareteamListOrphanCheckUserCommunityCareteam + " in its userCommunityCareteamList field has a non-nullable communityCareteam field.");
            }

            List<PatientCommunityCareteam> patientCommunityCareteamListOrphanCheck = communityCareteam.getPatientCommunityCareteamList();
            for (PatientCommunityCareteam patientCommunityCareteamListOrphanCheckPatientCommunityCareteam : patientCommunityCareteamListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This CommunityCareteam (" + communityCareteam + ") cannot be destroyed since the PatientCommunityCareteam " + patientCommunityCareteamListOrphanCheckPatientCommunityCareteam + " in its patientCommunityCareteamList field has a non-nullable communityCareteam field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(communityCareteam);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<CommunityCareteam> findCommunityCareteamEntities(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("CommunityCareteam.findByCommId")
                    .setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public List<NotificationCareteamRole> getNotificationCareteamRoles(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from NotificationCareteamRole as o where o.notificationCareteamRolePK.communityId= :communityId");
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public CommunityCareteam findCommunityCareteam(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(CommunityCareteam.class, id);
        } finally {
            em.close();
        }
    }

    public int getCommunityCareteamCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from CommunityCareteam as o");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    //JIRA 1178 - Spira 5201
    public CommunityCareteam findCommunityCareteam(String name, Long communityId) {
        EntityManager em = getEntityManager();
        try {
            logger.info("careteam name = " + name);

            Query q = em.createQuery("select object(o) from CommunityCareteam as o where o.communityId = :communityIdParam and o.name = :nameParam");
            q.setParameter("communityIdParam", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            q.setParameter("nameParam", name);
            return (CommunityCareteam) q.getSingleResult();
        } finally {
            em.close();
        }
    }

    //JIRA 1178 - Spira 5201
    public CommunityCareteam findCommunityCareteam(EntityManager em, String name, Long communityId) throws Exception {
        Query q = em.createQuery("select object(o) from CommunityCareteam as o where o.communityId = :communityIdParam and o.name = :nameParam");
        q.setParameter("communityIdParam", communityId);
        q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

        q.setParameter("nameParam", name);
        return (CommunityCareteam) q.getSingleResult();
    }

    public void create(UserCommunityCareteam userCommunityCareteam) throws PreexistingEntityException, Exception {

        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            em.persist(userCommunityCareteam);

            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUserCommunityCareteam(userCommunityCareteam.getUserCommunityCareteamPK()) != null) {
                throw new PreexistingEntityException("UserCommunityCareteam " + userCommunityCareteam + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(UserCommunityCareteam userCommunityCareteam) throws NonexistentEntityException, Exception {
        userCommunityCareteam.getUserCommunityCareteamPK().setCommunityCareteamId(userCommunityCareteam.getCommunityCareteam().getCommunityCareteamId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            userCommunityCareteam = em.merge(userCommunityCareteam);

            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                UserCommunityCareteamPK id = userCommunityCareteam.getUserCommunityCareteamPK();
                if (findUserCommunityCareteam(id) == null) {
                    throw new NonexistentEntityException("The userCommunityCareteam with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }


    public void destroy(EntityManager em, UserCommunityCareteamPK id) throws NonexistentEntityException {
        UserCommunityCareteam userCommunityCareteam = null;

        logger.info("destroy :UserCommunityCareteamPK:" + id.getCommunityCareteamId() + ":Commid:" + id.getCommunityId() + ":User Id:" + id.getUserId());

        try {
            Query q = em.createQuery("select object(o) from UserCommunityCareteam as o where o.userCommunityCareteamPK.communityCareteamId = :theCommCareTeamId and  o.userCommunityCareteamPK.communityId=:theCommunityId and o.userCommunityCareteamPK.userId=:theUserId");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            q.setParameter("theCommCareTeamId", id.getCommunityCareteamId());
            q.setParameter("theCommunityId", id.getCommunityId());
            q.setParameter("theUserId", id.getUserId());
            List<UserCommunityCareteam> teams = q.getResultList();

            if (!teams.isEmpty()) {
                userCommunityCareteam = teams.get(0);
            }
        } catch (EntityNotFoundException enfe) {
            throw new NonexistentEntityException("The UserCommunityCareteam with id " + id + " no longer exists.", enfe);
        }
        em.remove(userCommunityCareteam);


    }

    public void destroy(UserCommunityCareteamPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            UserCommunityCareteam userCommunityCareteam;

            try {
                userCommunityCareteam = em.getReference(UserCommunityCareteam.class, id);
                userCommunityCareteam.getUserCommunityCareteamPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The userCommunityCareteam with id " + id + " no longer exists.", enfe);
            }

            em.remove(userCommunityCareteam);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public UserCommunityCareteam findUserCommunityCareteam(UserCommunityCareteamPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(UserCommunityCareteam.class, id);
        } finally {
            em.close();
        }
    }

    public int getUserCommunityCareteamCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from UserCommunityCareteam as o");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public void create(PatientCommunityCareteam patientCommunityCareteam) throws PreexistingEntityException, Exception {

        EntityManager em = null;

        try {
            em = getEntityManager();
            em.getTransaction().begin();

            patientCommunityCareteam.setStartDate(new Date());

            em.persist(patientCommunityCareteam);

            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPatientCommunityCareteam(patientCommunityCareteam.getPatientCommunityCareteamPK()) != null) {
                throw new PreexistingEntityException("PatientCommunityCareteam " + patientCommunityCareteam + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PatientCommunityCareteam patientCommunityCareteam) throws NonexistentEntityException, Exception {
        EntityManager em = null;

        try {
            em = getEntityManager();
            em.getTransaction().begin();

            PatientCommunityCareteam persistentPatientCommunityCareteam = em.find(PatientCommunityCareteam.class, patientCommunityCareteam.getPatientCommunityCareteamPK());

            patientCommunityCareteam = em.merge(patientCommunityCareteam);

            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                PatientCommunityCareteamPK id = patientCommunityCareteam.getPatientCommunityCareteamPK();
                if (findPatientCommunityCareteam(id) == null) {
                    throw new NonexistentEntityException("The patientCommunityCareteam with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(PatientCommunityCareteamPK id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            PatientCommunityCareteam patientCommunityCareteam;
            try {
                patientCommunityCareteam = em.getReference(PatientCommunityCareteam.class, id);
                patientCommunityCareteam.getPatientCommunityCareteamPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The patientCommunityCareteam with id " + id + " no longer exists.", enfe);
            }

            em.remove(patientCommunityCareteam);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PatientCommunityCareteam> findPatientCommunityCareteamEntities() {
        return findPatientCommunityCareteamEntities(true, -1, -1);
    }

    public List<PatientCommunityCareteam> findPatientCommunityCareteamEntities(int maxResults, int firstResult) {
        return findPatientCommunityCareteamEntities(false, maxResults, firstResult);
    }

    private List<PatientCommunityCareteam> findPatientCommunityCareteamEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from PatientCommunityCareteam as o");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PatientCommunityCareteam findPatientCommunityCareteam(PatientCommunityCareteamPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PatientCommunityCareteam.class, id);
        } finally {
            em.close();
        }
    }

    public int getPatientCommunityCareteamCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from PatientCommunityCareteam as o");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public boolean doesPatientCommunityCareteamExist(Long patientId) {

        EntityManager em = getEntityManager();
        try {
            PatientCommunityCareteam patientCommunityCareteam = null;

            Query q = em.createQuery("select count(o) from PatientCommunityCareteam as o where o.patientCommunityCareteamPK.patientId = :patientIdParam and o.endDate IS NULL");
            q.setParameter("patientIdParam", patientId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            long count = (Long) q.getSingleResult();

            logger.info(":doesPatientCommunityCareteamExist for patientId " + patientId + "?" + count);

            return count > 0;
        } finally {
            em.close();
        }
    }

    public CommunityCareteam findCommunityCareteamForPatient(Long patientId) {
        CommunityCareteam communityCareteam = null;

        EntityManager em = getEntityManager();
        try {
            PatientCommunityCareteam patientCommunityCareteam = null;

            Query q = em.createQuery("select object(o) from PatientCommunityCareteam as o where o.patientCommunityCareteamPK.patientId = :patientIdParam and o.endDate IS NULL");
            q.setParameter("patientIdParam", patientId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<PatientCommunityCareteam> teams = q.getResultList();

            if (!teams.isEmpty()) {
                patientCommunityCareteam = teams.get(0);
            } else {
                return null;
            }

            long communityCareteamId = patientCommunityCareteam.getPatientCommunityCareteamPK().getCommunityCareteamId();
            Query q2 = em.createQuery("select object(o) from CommunityCareteam as o where o.communityCareteamId = :communityCareteamIdParam");
            q2.setParameter("communityCareteamIdParam", communityCareteamId);
            q2.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            communityCareteam = (CommunityCareteam) q2.getSingleResult();

            return communityCareteam;
        } finally {
            em.close();
        }
    }

    public void update(long communityCareteamId, long patientId) {

        logger.info("========>>>     update update   communityCareteamId = " + communityCareteamId + ":>>:" + patientId);

        EntityManager em = getEntityManager();
        try {

            boolean sameCareTeamAssigned = isSameCareTeamAssigned(communityCareteamId, patientId);

            logger.info("========>>>       sameCareTeamAssigned = " + sameCareTeamAssigned);
            if (sameCareTeamAssigned) {
                // the same care team so no need to update
                                
                return;
            }

            EntityTransaction transaction = em.getTransaction();
            transaction.begin();

            // get rid of old one(s)
            if (doesPatientCommunityCareteamExist(patientId)) {
                
                Query q = em.createQuery("select object(o) from PatientCommunityCareteam as o where o.patientCommunityCareteamPK.patientId = :patientIdParam");
                q.setParameter("patientIdParam", patientId);
                q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
                List<PatientCommunityCareteam> oldPatientCommunityCareteams = (List<PatientCommunityCareteam>) q.getResultList();
                
                for (PatientCommunityCareteam temp : oldPatientCommunityCareteams) {
//                    em.remove(temp);
                    if(temp.getEndDate()==null){
                       temp.setEndDate(new Date());
                        
                    }                  
                   em.merge(temp);
                }
            }
//
//            PatientCommunityCareteam patientAssociation = findPatientCommunityCareteamByPatientID(patientId);
            // add new one
            PatientCommunityCareteam patientCommunityCareteam = new PatientCommunityCareteam();

            PatientCommunityCareteamPK pk = new PatientCommunityCareteamPK();
            pk.setCommunityCareteamId(communityCareteamId);
            pk.setPatientId(patientId);
            patientCommunityCareteam.setPatientCommunityCareteamPK(pk);

            //set value for association
//            if (patientAssociation != null) {

//                if (patientAssociation.getIsAssociate() != null) {
                    patientCommunityCareteam.setIsAssociate('Y');
                    patientCommunityCareteam.setStartDate(new Date());
//                    patientCommunityCareteam.setEndDate(patientAssociation.getEndDate());
                    logger.info("========>>>   patientCommunityCareteam.getIsAssociate().charValue = " + patientCommunityCareteam.getIsAssociate().charValue());

//                } else {
//                    patientCommunityCareteam.setIsAssociate(null);
//                }
//            }
            logger.info("========>>>patientCommunityCareteam.getIsAssociate = " + patientCommunityCareteam.getIsAssociate());

            em.persist(patientCommunityCareteam);

            transaction.commit();

        } finally {
            em.close();
        }

    }

    public PatientCommunityEnrollment findPatientEnrollment(long patientId) throws NoResultException {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery("select object(o) from PatientCommunityEnrollment as o where o.patientId = :thePatientId");
            q.setParameter("thePatientId", patientId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<PatientCommunityEnrollment> list = q.getResultList();

            PatientCommunityEnrollment tempPatientEnrollment = null;
            if (!list.isEmpty()) {
                tempPatientEnrollment = list.get(0);
            }



            return tempPatientEnrollment;
        } finally {
            em.close();
        }
    }

    public List<User> getUsersForCareteam(long communityCareteamId) {
        List<User> users = new ArrayList<User>();

        EntityManager em = getEntityManager();
        try {

            // get all user ids from user_community_careteam
            Query q = em.createQuery("select o.userCommunityCareteamPK.userId from UserCommunityCareteam as o where o.userCommunityCareteamPK.communityCareteamId = :communityCareteamIdParam");
            q.setParameter("communityCareteamIdParam", communityCareteamId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            List<Long> userCommunityCareteams = q.getResultList();

            // for each user id, get the user object
            for (Long userId : userCommunityCareteams) {
                User tempUser = em.find(User.class, userId);

                users.add(tempUser);
            }

        } finally {
            em.close();
        }

        return users;
    }

    public List<User> getAvailableUsers() {
        List<User> users = new ArrayList<User>();

        EntityManager em = getEntityManager();

        try {
            Query q = em.createNamedQuery("User.findNotDeleted");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            users = q.getResultList();

            return users;
        } finally {
            em.close();
        }
    }

    //JIRA 1178 - Spira 5201
    public boolean doesCareteamNameExistForOtherTeam(long careteamId, String careteamName, Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from CommunityCareteam as o where o.communityCareteamId <> :communityCareteamIdParam and o.communityId = :communityIdParam and o.name = :careteamNameParam");
            q.setParameter("communityCareteamIdParam", careteamId);
            q.setParameter("careteamNameParam", careteamName);
            q.setParameter("communityIdParam", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            long count = (Long) q.getSingleResult();

            return count > 0;
        } finally {
            em.close();
        }
    }

    //JIRA 1178 - Spira 5201
    public boolean doesCareteamNameExistForOtherTeam(String careteamName, Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from CommunityCareteam as o where o.communityId = :communityIdParam and o.name = :careteamNameParam");
            q.setParameter("careteamNameParam", careteamName);
            q.setParameter("communityIdParam", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            long count = (Long) q.getSingleResult();

            return count > 0;
        } finally {
            em.close();
        }
    }

    //JIRA 1178 - Spira 5201
    public void create(CommunityCareteam careteam, List<UserDTO> users, Long communityId) throws Exception {
        EntityManager em = getEntityManager();
        EntityTransaction transaction = em.getTransaction();

        try {
            transaction.begin();

            em.persist(careteam);

            careteam = findCommunityCareteam(em, careteam.getName(), communityId);

            for (UserDTO tempUser : users) {
                UserCommunityCareteam userCommunityCareteam = new UserCommunityCareteam();

                UserCommunityCareteamPK pk = new UserCommunityCareteamPK();
                pk.setCommunityCareteamId(careteam.getCommunityCareteamId());
                pk.setUserId(tempUser.getUserId());
                pk.setCommunityId(careteam.getCommunityId());
                userCommunityCareteam.setUserCommunityCareteamPK(pk);
                userCommunityCareteam.setStartDate(new Date());
                em.merge(userCommunityCareteam);
            }

            transaction.commit();
        } catch (Exception exc) {
            transaction.rollback();

            throw exc;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public boolean isSameCareTeamAssigned(long communityCareteamId, long patientId) {
        EntityManager em = getEntityManager();

        try {
            boolean isSame = false;
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();
            Query q = em.createQuery("select object(o) from PatientCommunityCareteam as o where o.patientCommunityCareteamPK.patientId = :patientIdParam");
                q.setParameter("patientIdParam", patientId);
                q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
                List<PatientCommunityCareteam> oldPatientCommunityCareteams = (List<PatientCommunityCareteam>) q.getResultList();
                
                for (PatientCommunityCareteam temp : oldPatientCommunityCareteams) {
//                    em.remove(temp);

                    if (temp.getPatientCommunityCareteamPK().
                            getCommunityCareteamId() != communityCareteamId && temp.
                                    getEndDate() == null) {
                       temp.setEndDate(new Date());                                               
                    }                  
                   em.merge(temp);
                   em.flush();
                   em.clear();
                }
            PatientCommunityCareteamPK pk = new PatientCommunityCareteamPK();
            pk.setCommunityCareteamId(communityCareteamId);
            pk.setPatientId(patientId);

            PatientCommunityCareteam patientCommunityCareteam = em.find(PatientCommunityCareteam.class, pk);
            //Spira - 6835
            //if same care team is reassigned then new start date is provided and old dates are removed
            if(patientCommunityCareteam!=null && patientCommunityCareteam.getEndDate()!=null){
                patientCommunityCareteam.setEndDate(null);
                patientCommunityCareteam.setStartDate(new Date());
                em.merge(patientCommunityCareteam);
            }
            transaction.commit();
            isSame = patientCommunityCareteam != null;

            return isSame;
        } finally {
            em.close();
        }

    }

    public void remove(long patientId) {
        EntityManager em = getEntityManager();

        try {

            EntityTransaction transaction = em.getTransaction();
            transaction.begin();

            // get rid of old one(s)
            if (doesPatientCommunityCareteamExist(patientId)) {
                Query q = em.createQuery("select object(o) from PatientCommunityCareteam as o where o.patientCommunityCareteamPK.patientId = :patientIdParam");
                q.setParameter("patientIdParam", patientId);
                q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

                List<PatientCommunityCareteam> oldPatientCommunityCareteams = (List<PatientCommunityCareteam>) q.getResultList();

                // delete old care teams
                if (oldPatientCommunityCareteams != null) {
                    for (PatientCommunityCareteam tempCareTeam : oldPatientCommunityCareteams) {
                        em.remove(tempCareTeam);
                    }
                }
            }

            transaction.commit();
        } finally {
            em.close();
        }
    }

    @Override
    public boolean hasPatientsAssignedToCareteam(long communityCareteamId) {
        EntityManager em = getEntityManager();

        try {
            String jpql = "select count(o) from PatientCommunityCareteam as o"
                    + " where o.patientCommunityCareteamPK.communityCareteamId = :communityCareteamIdParam";

            Query q = em.createQuery(jpql);
            q.setParameter("communityCareteamIdParam", communityCareteamId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            long count = (Long) q.getSingleResult();

            return count > 1;

        } finally {
            em.close();
        }
    }

    @Override
    public List<String> getPatientEuidsAssignedToCareteam(long communityCareteamId) {
        EntityManager em = getEntityManager();

        try {

            String jpql = "select enrollment.patientEuid from PatientCommunityCareteam as careteam, PatientCommunityEnrollment as enrollment"
                    + " where careteam.patientCommunityCareteamPK.communityCareteamId = :communityCareteamIdParam"
                    + " and careteam.patientCommunityCareteamPK.patientId = enrollment.patientId";

            Query q = em.createQuery(jpql);
            q.setParameter("communityCareteamIdParam", communityCareteamId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<String> euids = (List<String>) q.getResultList();

            return euids;

        } finally {
            em.close();
        }
    }

    @Override
    public boolean hasOtherPatientsAssignedToCareteam(long communityCareteamId, long patientId) {
        EntityManager em = getEntityManager();

        try {
            String jpql = "select count(o) from PatientCommunityCareteam as o"
                    + " where o.patientCommunityCareteamPK.communityCareteamId = :communityCareteamIdParam"
                    + " and"
                    + " o.patientCommunityCareteamPK.patientId <> :patientIdParam";

            Query q = em.createQuery(jpql);
            q.setParameter("patientIdParam", patientId);
            q.setParameter("communityCareteamIdParam", communityCareteamId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            long count = (Long) q.getSingleResult();

            return count > 0;

        } finally {
            em.close();
        }
    }

    /*
     * Legacy method- old one, need to be replaced Composite Primary Key
     */
    @Override
    public List<String> getDuplicateRolesForCareteam(long communityId) {
        EntityManager em = getEntityManager();

        List<String> duplicateRoles;

        try {
            String jpql = "SELECT c.role.roleName FROM CareteamRoleDuplicate c "
                    + "WHERE c.careteamRoleDuplicatePK.communityId = :theCommunityId";
            Query q = em.createQuery(jpql);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            duplicateRoles = q.getResultList();
            return duplicateRoles;

        } finally {
            em.close();
        }

    }

    @Override
    public List<String> getRequiredRolesForCareteam(long communityId) {
        EntityManager em = getEntityManager();
        List<String> requiredRoles;

        try {
            String jpql = "select o.role.roleName from CareteamRoleRequired o "
                    + "WHERE o.careteamRoleRequiredPK.communityId = :theCommunityId";
            Query q = em.createQuery(jpql);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            requiredRoles = q.getResultList();
            return requiredRoles;
        } finally {
            em.close();
        }
    }

    /**
     * FIXME - sql injection waiting to happen, use criteria query
     * @param role
     * @param userIdsToExclude
     * @param pageNumber
     * @param pageSize
     * @return 
     */
    //JIRA 860-1460 (1465)
    //Migrate EclipseLink Expression to pure JPA Criteria    
    @Override
    public List<User> getAvailableUsers(String role, List<Long> userIdsToExclude, Long communityId) {
        EntityManager em = getEntityManager();
        List<User> users = null;
        try {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<User> cq = cb.createQuery(User.class);
            Root<User> userRoot = cq.from(User.class);
            
            Predicate finalQuery = buildAvailableUsersQueryExpression(cb, userRoot, communityId, role, userIdsToExclude);
//            int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);
            //ReadAllQuery readAllQuery = new ReadAllQuery(User.class);
            //readAllQuery.setSelectionCriteria(finalQuery);
            //readAllQuery.addAscendingOrdering("lastName");
            //readAllQuery.dontMaintainCache();
//            readAllQuery.setFirstResult(startRecordNumber);
//            readAllQuery.setMaxRows(startRecordNumber + pageSize);
            
            cq.where(finalQuery);
            cq.orderBy(cb.asc(userRoot.get("lastName")));

            TypedQuery<User> tq = em.createQuery(cq);
            users = tq.getResultList();
            
        } catch (Exception ex) {
            logger.log(Level.SEVERE, "oh noes, something went wrong with getting users: ", ex);
        } finally {
            em.close();
        }
        
        return users;
    }

    /**
     * 
     * @param communityId
     * @param role this is supposed to be a number, but some lazy bastage didnt want to make the code changes
     * @param userIdsToExclude
     * @return 
     */
    //JIRA 860-1460 (1465)
    //Migrate EclipseLink Expression to pure JPA Criteria
    //private Expression buildAvailableUsersQueryExpression(Long communityId, String role, List<Long> userIdsToExclude) {    
    private Predicate buildAvailableUsersQueryExpression(CriteriaBuilder cb, Root<User> userRoot, 
            Long communityId, String role, List<Long> userIdsToExclude) {
        //ExpressionBuilder exp = new ExpressionBuilder(User.class);
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Join<User, UserCommunityOrganization> ucoJoin = userRoot.join("userCommunityOrganization");
        
        // no criteria, just get an active user in selected community
        //Expression finalQuery = exp.anyOf("userCommunityOrganization").get("status").equalsIgnoreCase("ACTIVE")
          //      .and(exp.anyOf("userCommunityOrganization").get("community").get("communityId").equal(communityId));
        Predicate finalQuery = cb.and(cb.equal(cb.upper(ucoJoin.<String>get("status")), "ACTIVE"),
                cb.equal(ucoJoin.get("community").get("communityId"), communityId));
        // add a role if it exists
        if(role != null && !role.equalsIgnoreCase("all")){
            logger.info("adding qry exp for selected user role");
            Long rolenum = Long.parseLong(role);
            //Expression addRole = exp.anyOf("userCommunityOrganization").get("role").get("primaryRoleId").equal(rolenum);
            //finalQuery = finalQuery.and(addRole);
            Predicate addRole = cb.equal(ucoJoin.get("role").get("primaryRoleId"), rolenum);
            finalQuery = cb.and(finalQuery, addRole);
        }
        // add users to exclude if they exist
        if(userIdsToExclude != null && !userIdsToExclude.isEmpty()){
            //Expression excludeUsers = exp.get("userId").notIn(userIdsToExclude.toArray());
            //finalQuery = finalQuery.and(excludeUsers);
            Predicate excludeUsers = cb.not(userRoot.get("userId").in(userIdsToExclude));
            finalQuery = cb.and(finalQuery, excludeUsers);
        }
        return finalQuery;
    }

    //JIRA 860-1460 (1465)
    //Migrate EclipseLink Expression to pure JPA Criteria    
    @Override
    public int getTotalCountForAvailableUsers(String role, List<Long> userIdsToExclude, Long communityId) {
        EntityManager em = getEntityManager();
        Long count = 0L;
        List reportRows = null;
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<User> userRoot = cq.from(User.class);
        //Expression finalQuery = buildAvailableUsersQueryExpression(communityId, role, userIdsToExclude);
        Predicate finalQuery = buildAvailableUsersQueryExpression(cb, userRoot, communityId, role, userIdsToExclude);
        try {
//            ReportQuery reportQuery = new ReportQuery(User.class, finalQuery);
//            reportQuery.addCount();
//            reportQuery.setSelectionCriteria(finalQuery);
//            reportQuery.dontMaintainCache();
//            reportQuery.setShouldReturnSingleValue(true);
//            reportQuery.setShouldReturnWithoutReportQueryResult(true);
            
            cq.select(cb.countDistinct(userRoot));
            cq.where(finalQuery);

            TypedQuery<Long> q = em.createQuery(cq);
            count = q.getSingleResult();
            
        } catch (Exception tle) {
            logger.log(Level.SEVERE, "count failed: " , tle);
        } finally {
            em.close();
        }
        return count.intValue();
    }

    @Override
    public List<Role> getUserRoles(long communityId) {
        List<Role> userRoles = new ArrayList<Role>();

        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery("select o from Role as o where o.communityId=:theCommunityId order by o.roleName");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            userRoles = q.getResultList();

            return userRoles;
        } finally {
            em.close();
        }
    }

    //pe
    public PatientCommunityEnrollment findPatientEnrollment(String euid) throws NoResultException {

        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery("select object(o) from PatientCommunityEnrollment as o where o.patientEuid = :theEuid");
            q.setParameter("theEuid", euid);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            PatientCommunityEnrollment enrollment = (PatientCommunityEnrollment) q.getSingleResult();
            return enrollment;
        } finally {
            em.close();
        }
    }

    /**
     * activate & Deactivate Patient User Account
     *
     * @param patientEnrollment
     * @throws NoResultException
     */
    public void activateDeactivatePatientUserAccount(PatientCommunityEnrollment patientEnrollment) throws NoResultException {
        logger.info("  activateDeactivatePatientUserAccount DAO Active Flag: " + patientEnrollment.getPatientUserActive() + ":LastPasswordChangeDate:" + patientEnrollment.getLastPasswordChangeDate());

        EntityManager em = getEntityManager();
        EntityTransaction transaction = em.getTransaction();

        try {
            transaction.begin();
            PatientCommunityEnrollment enrollment = findPatientEnrollment(patientEnrollment.getPatientEuid());
            enrollment.setPatientUserActive(patientEnrollment.getPatientUserActive());
            enrollment.setLastPasswordChangeDate(patientEnrollment.getLastPasswordChangeDate());
            enrollment.setStatus(patientEnrollment.getStatus());
            enrollment.setReasonForInactivation(patientEnrollment.getReasonForInactivation());
            em.merge(enrollment);

            transaction.commit();
        } catch (NoResultException exc) {
            transaction.rollback();

            throw exc;
        } finally {
            em.close();
        }
    }
    /*
     * find the Patient Engagement Role
     */

    public PatientEngagementRole findPatientEngagementRole(long communityId) throws NoResultException {

        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from PatientEngagementRole as o where o.patientEngagementRolePK.communityId=:theCommunityId order by o.roleName");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<PatientEngagementRole> list = q.getResultList();

            PatientEngagementRole patientEngagementRole = null;
            if (!list.isEmpty()) {
                patientEngagementRole = list.get(0);
            }

            return patientEngagementRole;
        } finally {
            em.close();
        }

    }

    public List<PatientCommunityEnrollment> findPatientEnrollmentList(String emailAddress) throws NoResultException {

        logger.info("  findPatientEnrollmentList DAO emailAddress: " + emailAddress);

        EntityManager em = getEntityManager();
        try {

            Query q = em.createQuery("select object(o) from PatientCommunityEnrollment as o where o.emailAddress = :emailAddress");
            q.setParameter("emailAddress", emailAddress);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<PatientCommunityEnrollment> duplicatePatientEmails = q.getResultList();

            return duplicatePatientEmails;

        } finally {
            em.close();
        }
    }

    private PatientCommunityCareteam getPatientCommunityCareteam(long communityCareteamId, long patientId) {
        EntityManager em = getEntityManager();

        try {

            PatientCommunityCareteamPK pk = new PatientCommunityCareteamPK();
            pk.setCommunityCareteamId(communityCareteamId);
            pk.setPatientId(patientId);

            PatientCommunityCareteam patientCommunityCareteam = em.find(PatientCommunityCareteam.class, pk);

            return patientCommunityCareteam;
        } finally {
            em.close();
        }

    }

    /**
     *
     * @param communityCareteamId
     * @param patientId
     * @param patientActive
     */
    public void associatedPatientCommunityCareteam(long communityCareteamId, long patientId, String patientActive) {

        logger.info(" >>>>>>> associatedPatientCommunityCareteam communityCareteamId: " + communityCareteamId + "::patientId:" + patientId + ":patientActive:" + patientActive);
        EntityManager em = getEntityManager();
        try {

            boolean sameCareTeamAssigned = isSameCareTeamAssigned(communityCareteamId, patientId);

            EntityTransaction transaction = em.getTransaction();
            transaction.begin();

            logger.info("========>>>       sameCareTeamAssigned = " + sameCareTeamAssigned);
            if (sameCareTeamAssigned) {
                // the same care team so no need to update

                PatientCommunityCareteam communityCareteam = getPatientCommunityCareteam(communityCareteamId, patientId);

                if (patientActive.equalsIgnoreCase("Active")) {
                    communityCareteam.setIsAssociate('Y');
                } else {
                    communityCareteam.setIsAssociate('N');
                }

                em.merge(communityCareteam);
                logger.info("After merge same care team for Assocication");

            } else {


                // get rid of old one(s)
                if (doesPatientCommunityCareteamExist(patientId)) {
                    Query q = em.createQuery("select object(o) from PatientCommunityCareteam as o where o.patientCommunityCareteamPK.patientId = :patientIdParam");
                    q.setParameter("patientIdParam", patientId);
                    q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
                    List<PatientCommunityCareteam> oldPatientCommunityCareteams = (List<PatientCommunityCareteam>) q.getResultList();

                    for (PatientCommunityCareteam temp : oldPatientCommunityCareteams) {
//                    em.remove(temp);
                    if(temp.getEndDate()==null){
                       temp.setEndDate(new Date());
                      
                    }
                  
                    em.merge(temp);
                }
                }

                logger.info("DAO Prepare new Care team for Association --------------------------------");
                // add new one
                PatientCommunityCareteam patientCommunityCareteam = new PatientCommunityCareteam();

                PatientCommunityCareteamPK pk = new PatientCommunityCareteamPK();
                pk.setCommunityCareteamId(communityCareteamId);
                pk.setPatientId(patientId);

                patientCommunityCareteam.setPatientCommunityCareteamPK(pk);

                if (patientActive.equalsIgnoreCase("Active")) {
                    patientCommunityCareteam.setIsAssociate('Y');
                    patientCommunityCareteam.setStartDate(new Date());
                } else {
                    patientCommunityCareteam.setIsAssociate('N');
                    patientCommunityCareteam.setStartDate(new Date());
                }


                em.persist(patientCommunityCareteam);
                logger.info("========>>>      After persist associated PatientCommunityCareteam = ");

            }
            transaction.commit();

        } finally {
            em.close();
        }

    }

    /**
     *
     * @param directAddress
     * @return
     * @throws NoResultException
     */
    public List<PatientCommunityEnrollment> findPatientEnrollmentWithDirectAddress(String directAddress) throws NoResultException {

        logger.info("  findPatientEnrollmentWithDirectAddress DAO directAddress: " + directAddress);

        EntityManager em = getEntityManager();
        try {

            Query q = em.createQuery("select object(o) from PatientCommunityEnrollment as o where o.directAddress = :directAddress");
            q.setParameter("directAddress", directAddress);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<PatientCommunityEnrollment> duplicateDirectAddress = q.getResultList();

            return duplicateDirectAddress;

        } finally {
            em.close();
        }
    }

    /**
     *
     * @param communityCareteamId
     * @param patientId
     * @return
     */
    public PatientCommunityCareteam findAssociatedPatientFlag(Long communityCareteamId, Long patientId) {
        logger.info("  findAssociatedPatientFlag: communityCareteamId: " + communityCareteamId + ":patientId:" + patientId);
        EntityManager em = getEntityManager();
        List<PatientCommunityCareteam> list;
        PatientCommunityCareteam patientCommunityCareteam = null;

        try {


            Query q = em.createQuery("select object(o) from PatientCommunityCareteam as o where o.patientCommunityCareteamPK.patientId = :patientIdParam and "
                    + "o.communityCareteam.communityCareteamId= :communityCareteamIdParam");
            q.setParameter("patientIdParam", patientId);
            q.setParameter("communityCareteamIdParam", communityCareteamId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            list = (List<PatientCommunityCareteam>) q.getResultList();

            if (!list.isEmpty()) {
                patientCommunityCareteam = (PatientCommunityCareteam) list.get(0);
                logger.info("  patientCommunityCareteam: getIsAssociate:" + patientCommunityCareteam.getIsAssociate());

            }
            logger.info("  patientCommunityCareteam: " + patientCommunityCareteam);
            return patientCommunityCareteam;
        } finally {
            em.close();
        }
    }

    public PatientCommunityCareteam findPatientCommunityCareteamByPatientID(Long patientId) {
        logger.info("  findAssociatedPatientFlag:  " + ":patientId:" + patientId);
        EntityManager em = getEntityManager();
        List<PatientCommunityCareteam> list;
        PatientCommunityCareteam patientCommunityCareteam = null;

        try {


            Query q = em.createQuery("select object(o) from PatientCommunityCareteam as o where o.patientCommunityCareteamPK.patientId = :patientIdParam and o.endDate IS NULL");
            q.setParameter("patientIdParam", patientId);

            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            list = (List<PatientCommunityCareteam>) q.getResultList();

            if (!list.isEmpty()) {
                patientCommunityCareteam = (PatientCommunityCareteam) list.get(0);
                logger.info("  patientCommunityCareteam: getIsAssociate:" + patientCommunityCareteam.getIsAssociate());

            }
            logger.info("  patientCommunityCareteam: " + patientCommunityCareteam);
            return patientCommunityCareteam;
        } finally {
            em.close();
        }
    }
    
    
    private void cleanUpPersistenceResources(EntityManager em) {
        if (em != null) {
            em.close();
        }
    }
    
    @Override
     public void updateCateTeamEndDate(long patientId) {

        logger.info("========>>>     update CareTeam end date " + patientId);

        EntityManager em = getEntityManager();
        try {

            EntityTransaction transaction = em.getTransaction();
            transaction.begin();

            if (doesPatientCommunityCareteamExist(patientId)) {
                
                Query q = em.createQuery("select object(o) from PatientCommunityCareteam as o where o.patientCommunityCareteamPK.patientId = :patientIdParam");
                q.setParameter("patientIdParam", patientId);
                q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
                List<PatientCommunityCareteam> oldPatientCommunityCareteams = (List<PatientCommunityCareteam>) q.getResultList();
                
                for (PatientCommunityCareteam temp : oldPatientCommunityCareteams) {
//                    em.remove(temp);
                    if(temp.getEndDate()==null){
                       temp.setEndDate(new Date());
                        em.merge(temp);
                    }                   
                   
                }
            }
             transaction.commit();
             
        } finally {
            em.close();
        }

    }

}
