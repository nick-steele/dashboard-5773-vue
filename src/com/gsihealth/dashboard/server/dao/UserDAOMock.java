package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.entity.AccessLevel;
import com.gsihealth.entity.User;
import com.gsihealth.entity.UserCommunityOrganization;
import com.gsihealth.entity.UserPatientConsent;

import javax.persistence.criteria.Predicate;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public class UserDAOMock implements UserDAO {

    public void create(User user) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void update(User user) throws NonexistentEntityException, Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public User findUser(Long id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public User findUser(Long id, long communityId) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<User> findUserEntities() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int getUserCount() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public User findUserByEmail(String portalUserId) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void permDelete(Long id) throws NonexistentEntityException {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getUserPassword(String email) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<User> findUser(AccessLevel accessLevel) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<User> findUserByUserCommunityOrganizationAndAccessLevel(UserCommunityOrganization userCommunityOrganization, AccessLevel findAccessLevel) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<User> findUsers(long communityId, AccessLevel accessLevel, int pageNumber, int pageSize) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int getTotalCount(long communityId, AccessLevel accessLevel) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<User> findUsers(int pageNumber, int pageSize) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int getTotalCountForUserEntities() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<User> findUsers(long communityId, AccessLevel accessLevel) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<User> filterUsers(SearchCriteria searchCriteria, List<User> users) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<User> getTotalUsers(){
        throw new UnsupportedOperationException("Not supported yet.");
    }

     public List<UserPatientConsent> getUserPatientSearchTotalCount(Predicate searchPredicate, Integer maxPatients){
         throw new UnsupportedOperationException("Not supported yet.");
     }

    public Predicate criteriaBuilderForUserPatientSearch(SearchCriteria searchCriteria,long communityId){
         throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public User findUserByEmail(String portalUserId, long communityId) {
        throw new UnsupportedOperationException("Implement me stupid");
    }

    @Override
    public List<User> getTotalUsers(long communityId) {
        throw new UnsupportedOperationException("Implement me stupid");
    }

    @Override
    public List<User> findUsers(UserCommunityOrganization uco, SearchCriteria searchCriteria, int pageNumber, int pageSize) {
        throw new UnsupportedOperationException("Implement me stupid");
    }

    @Override
    public int getTotalUserCount(UserCommunityOrganization uco, SearchCriteria searchCriteria) {
        throw new UnsupportedOperationException("Implement me stupid");
    }

     @Override
      public int getAuthyId(String email,long communityId) {
        throw new UnsupportedOperationException("Implement me stupid");
    }
      @Override
    public List<User> findUsers(long communityId){
        throw new UnsupportedOperationException("Implement me stupid");
    }

    @Override
    public int removeSupervisor(long supervisorId, long communityId) {
        throw new UnsupportedOperationException("Implement me stupid");
    }

    @Override
    public List<User> findUserBySupervisor(long supervisorId, long communityId) {
        throw new UnsupportedOperationException("Implement me stupid");
    }

    @Override
    public User findByEmail(String email, long communityId) {
        throw new UnsupportedOperationException("Not support this method");
    }

    @Override
    public User findUserExistInOtherCommunity(String portalUserId, long communityId) {
        throw new UnsupportedOperationException("Not support this method");
    }
}
