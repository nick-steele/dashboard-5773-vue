package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.common.PayerPlanSearchCriteria;
import com.gsihealth.entity.insurance.PayerClass;
import com.gsihealth.entity.insurance.PayerPlan;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.RollbackFailureException;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public interface PayerPlanDAO {

    public void add(PayerPlan payerPlan) throws RollbackFailureException, Exception;

    public void destroy(Long id) throws NonexistentEntityException, RollbackFailureException, Exception;

    public PayerPlan findPayerPlan(Long id);

    public List<PayerPlan> findPayerPlanEntities();

    public List<PayerPlan> findPayerPlanEntities(int pageNumber, int pageSize);

    public List<PayerPlan> findPayerPlanEntities(PayerPlanSearchCriteria searchCriteria, int pageNumber, int pageSize);
    
    public int getPayerPlanTotalCount();

    public int getPayerPlanTotalCount(PayerPlanSearchCriteria searchCriteria);
    
    public void update(PayerPlan payerPlan) throws NonexistentEntityException, RollbackFailureException, Exception;

    public List<PayerClass> findPayerClassEntities();

    public boolean isDuplicate(PayerPlan payerPlan);

    public boolean hasPatientsAssignedToThisPayerPlan(PayerPlan payerPlan);

    public void deletePayerPlan(PayerPlan payerPlan) throws Exception;
    
    public List<PayerClass> getPayerClassEntities(Long communityId);
    
    public List<PayerPlan> getPayerPlanEntities(Long communityId);
}
