/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.gsihealth.entity.PatientEnrollmentHistory;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;

/**
 *
 * @author Satyendra Singh
 */
public class PatientEnrollmentHistoryDAOImpl implements PatientEnrollmentHistoryDAO {

    private EntityManagerFactory emf = null;

    public PatientEnrollmentHistoryDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public PatientEnrollmentHistoryDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void addToEnrollmentHistory(PatientEnrollmentHistory patientEnrollmentHistory) {
        patientEnrollmentHistory.setEnrollmentChangeDate(new Date());
       
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(patientEnrollmentHistory);
            em.getTransaction().commit();
             
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
   

    public Long getLastDisenrollmentReasonForInactivation(Long patientId) {
        EntityManager em = getEntityManager();
        try {
            String sql = "SELECT reason_for_inactivation FROM connect.patient_enrollment_history WHERE enrollment_status='inactive' AND patient_id="+patientId+ " ORDER BY enrollment_status_change_date DESC";
            Query patientQuery = em.createNativeQuery(sql);
            //patientQuery.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            
            // returning last record of inactive patient 
            List<Vector> theList = (List<Vector>) patientQuery.getResultList();
            Long id = 0L;
            if(theList.size()!=0) {
            Vector vct= theList.get(0);
            id = (Long) vct.get(0);
            }
            return id;
        } finally {
            em.close();
        }
    }

    public Date getEndDate(Long patientId) {
        EntityManager em = getEntityManager();
        Date endDate = null;
        try {
            String sql = "SELECT MAX(enrollment_status_change_date) FROM connect.patient_enrollment_history WHERE enrollment_status='inactive' AND patient_id=" + patientId;
            Query patientQuery = em.createNativeQuery(sql);
            //patientQuery.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<Vector> theList = (List<Vector>) patientQuery.getResultList();

            for (Vector tempRow : theList) {
                endDate = (Date) tempRow.get(0);

            }

        } finally {
            em.close();
        }

        return endDate;
    }
    
     public Date getEnrollmentStatusChangeDate(Long patientId) {
        EntityManager em = getEntityManager();
        Date enrollEffectDate = null;
        try {
            String sql = "SELECT MAX(enrollment_status_change_date) FROM connect.patient_enrollment_history WHERE patient_id=" + patientId;
            Query patientQuery = em.createNativeQuery(sql);
            //patientQuery.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            enrollEffectDate = (Date) patientQuery.getSingleResult();

        } finally {
            em.close();
        }

        return enrollEffectDate;
    }
}
