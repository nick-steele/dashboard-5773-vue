/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import com.gsihealth.entity.SqlConfiguration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author ssingh
 */
public class SqlConfigurationDAOImpl implements SqlConfigurationDAO{
    
    private Logger logger = Logger.getLogger(getClass().getName());
    private static HashMap<Long, HashMap> sqlConfiguration = new HashMap();

    private EntityManagerFactory emf = null;

    public SqlConfigurationDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
        loadConfiguration();
    }

    public SqlConfigurationDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
        loadConfiguration();

    }

    @Override
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    private void loadConfiguration() {
        EntityManager em = getEntityManager();

        try {
            if (sqlConfiguration.isEmpty()) {
                System.out.println("LOADING SQL CONFIGURATION CACHE!!!!");

                Query query = em.createQuery("select o from SqlConfiguration o ORDER BY o.sqlConfigurationPK.communityid");

                query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

                List<SqlConfiguration> list = query.getResultList();
                Iterator<SqlConfiguration> iconf = list.iterator();
                Long comstart = new Long(0);
                HashMap<String, String> storeToCache =  null;
                while (iconf.hasNext()) {
                    SqlConfiguration conf = iconf.next();
                    Long test = conf.getSqlConfigurationPK().getCommunityid();

                    if (!test.equals(comstart)) {
                         storeToCache= new HashMap();
                        sqlConfiguration.put(test,storeToCache);
                        comstart = test;
                    }
                    String name = conf.getSqlConfigurationPK().getName();
                    String value = conf.getQuery();
                    storeToCache.put(name,value);
                }

            }
        } catch (Exception exc) {
            exc.printStackTrace();
        } finally {
            em.close();
        }
    }
    
     /**
     * Returns the value for this property. If it doesn't exist, returns the
     * default value
     *
     * @param communityId
     * @param name
     * @param defaultValue
     * @return
     */
    @Override
    public String getProperty(long communityId, String name, String defaultValue) {

        String value = null;

        HashMap<String, String> comCache =  sqlConfiguration.get(communityId);
        if(comCache==null){
                        logger.warning("community " + communityId + " not found in SqlConfiguration table, using default value of " + defaultValue);
            return defaultValue;
        }
        value=comCache.get(name);
        if (value == null) {
            logger.warning(name + " not found in SqlConfiguration table, using default value of " + defaultValue);
            return defaultValue;
        } 
        return value;
    }
    
    /**
     * Returns the value for this property. If it doesn't exist, returns null
     *
     * @param communityId
     * @param name
     * @return
     */
    @Override
    public String getProperty(long communityId, String name) {

        String value = null;
         

        HashMap<String, String> comCache =  sqlConfiguration.get(communityId);
        if(comCache==null){
            logger.warning("community " + communityId + " not found in SqlConfiguration table");
            return value;
        }
        value=comCache.get(name);
        if (value == null) {
            logger.warning(name + " not found in SqlConfiguration table");
            
        } 
        return value;
          
    }
    
    /**
     * @param name
     * @return all configs by this name
     */
    @Override
    public List<SqlConfiguration> findConfigurationsByName(String name) {
        EntityManager em = getEntityManager();
        List<SqlConfiguration> configurations = null;
        try {

            configurations = em.createNamedQuery("Configuration.findByName")
                    .setParameter("name", name)                  
                    .setHint(JPAConstants.REFRESH, JPAConstants.TRUE)
                    .getResultList();
        } catch (Exception exc) {
            logger.warning("SqlConfiguration qry failed : " + exc);
        } finally {
            em.close();
        }
        return configurations;
    }

}
