/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;


import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import com.gsihealth.entity.Consenter;
import com.gsihealth.entity.ConsenterPK;

/**
 *
 * @author Edwin Montgomery
 */
public class ConsenterDAOImpl implements ConsenterDAO {

    private EntityManagerFactory emf = null;

    public ConsenterDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public ConsenterDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public List<Consenter> getConsenters(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Consenter as o where o.consenterPK.communityId=:theCommunityId");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }
    
    
    public Consenter findConsenter(long communityId, long code) {
        
        ConsenterPK id = new ConsenterPK();
        id.setCommunityId((long) communityId);
        id.setCode(code);
        
        EntityManager em = getEntityManager();
        try {
            return em.find(Consenter.class, id);
        } finally {
            em.close();
        }
    }

    @Override
    public Consenter getConsenter(Long communityId, long code) {
        EntityManager em = getEntityManager();
        Consenter consenter = null;
        try {


            Query q = em.createQuery("SELECT object(o) FROM Consenter as o WHERE o.code=:code and o.consenterPK.communityId=:theCommunityId");
            q.setParameter("code", code);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            consenter = (Consenter) q.getSingleResult();

            return consenter;
        } finally {
            em.close();
        }
    }

    @Override
    public Long getConsenterCode(String description)  throws NoResultException {
        EntityManager em = getEntityManager();
        Consenter consenter = null;
        try {


Query q = em.createQuery("SELECT d FROM Consenter as d WHERE d.description = :description");
            q.setParameter("description", description);
            //q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            consenter = (Consenter) q.getSingleResult();

            return consenter.getConsenterPK().getCode(); 

        } finally {
            em.close();
        }
    }
}
