package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.Eula;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;

/**
 *
 * @author Chad Darby
 */
public class EulaDAOImpl implements EulaDAO {

    private EntityManagerFactory emf = null;

    public EulaDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public EulaDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public Eula findActiveEula(Long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select o from Eula o where o.active='Y' and o.eulaPK.communityId= :theCommunityId order by o.datetimeModified desc");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<Eula> list = (List<Eula>) q.getResultList();
            Eula eula = null;
            if (!list.isEmpty()) {
                eula = list.get(0);
            }

            return eula;
        } finally {
            em.close();
        }
    }
}
