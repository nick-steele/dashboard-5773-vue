package com.gsihealth.dashboard.server.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.entity.AccessLevel;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;

/**
 *
 * @author Chad Darby
 */
public class AccessLevelDAOImpl implements AccessLevelDAO {

    private EntityManagerFactory emf = null;

    public AccessLevelDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public AccessLevelDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public List<AccessLevel> findAccessLevelEntities(long communityId) {
        return findAccessLevelEntities(true, -1, -1,communityId);
    }

    public List<AccessLevel> findAccessLevelEntities(int maxResults, int firstResult,long communityId) {
        return findAccessLevelEntities(false, maxResults, firstResult,communityId);
    }

    private List<AccessLevel> findAccessLevelEntities(boolean all, int maxResults, int firstResult,long communityId) {
        
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(a) from AccessLevel as a where a.accessLevelPK.communityId=:commId ");
            q.setParameter("commId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

  
    @Override
    public AccessLevel findAccessLevel(Long id, Long communityId) {
       
       EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from AccessLevel as o where o.accessLevelPK.accessLevelId=:theAccessLevelId and o.accessLevelPK.communityId=:theCommunityId");
            q.setParameter("theAccessLevelId", id);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
           
            return (AccessLevel) q.getSingleResult();
            
        } finally {
            em.close();
        }
    }

  
    
    
}
