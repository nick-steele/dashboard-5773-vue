package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.OrganizationMmisOid;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.RollbackFailureException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;

/**
 *
 * @author Chad Darby
 */
public class OrganizationMmisOidDAOImpl implements OrganizationMmisOidDAO {

    private EntityManagerFactory emf = null;

    public OrganizationMmisOidDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public OrganizationMmisOidDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(OrganizationMmisOid organizationMmisOid) throws RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();
        
        try {
            utx.begin();
            em.persist(organizationMmisOid);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(OrganizationMmisOid organizationMmisOid) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();
        try {
            utx.begin();
            organizationMmisOid = em.merge(organizationMmisOid);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = organizationMmisOid.getOrganizationMmisOidPK().getId();
                if (findOrganizationMmisOid(id) == null) {
                    throw new NonexistentEntityException("The organizationMmisOid with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();
            OrganizationMmisOid organizationMmisOid;
            try {
                organizationMmisOid = em.getReference(OrganizationMmisOid.class, id);
                organizationMmisOid.getOrganizationMmisOidPK().getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The organizationMmisOid with id " + id + " no longer exists.", enfe);
            }
            em.remove(organizationMmisOid);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<OrganizationMmisOid> findOrganizationMmisOidEntities(long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from OrganizationMmisOid as o where o.organizationMmisOidPK.communityId=:theCommunityId");
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public OrganizationMmisOid findOrganizationMmisOid(String oid, long communityId) {
        EntityManager em = getEntityManager();
        OrganizationMmisOid organizationMmisOid = null;
        
        try {
            Query q = em.createQuery("select object(o) from OrganizationMmisOid as o where o.oid=:theOid and o.organizationMmisOidPK.communityId=:theCommunityId");
            q.setParameter("theOid", oid);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            
            List<OrganizationMmisOid> results = q.getResultList();
            
            if (!results.isEmpty()) {
                organizationMmisOid = results.get(0);
            }
            
            return organizationMmisOid;
        } finally {
            em.close();
        }
    }
    
    public OrganizationMmisOid findOrganizationMmisOid(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(OrganizationMmisOid.class, id);
        } finally {
            em.close();
        }
    }
    
}
