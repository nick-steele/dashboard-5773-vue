package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import com.gsihealth.entity.County;
import com.gsihealth.entity.CountyPK;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by rsundar on 10/10/2017.
 */
public class CountyDAOImpl implements CountyDAO {
    private EntityManagerFactory emf = null;

    public CountyDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public CountyDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public List<County> findCountys(long communityId) {
        EntityManager em = getEntityManager();
        try{
        Query q = em.createQuery("select object(c) from County as c where c.countyPK.communityId=:theCommunityId");
        q.setParameter("theCommunityId", (int) communityId);
        q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
        return q.getResultList();
    } finally {
        em.close();
    }
    }

    @Override
    public County findcounty(long communityId, int countyId) {
        CountyPK id = new CountyPK();
        id.setCommunityId((int) communityId);
        id.setCountyId(countyId);
        EntityManager em = getEntityManager();
        try {
            return em.find(County.class, id);

        }finally {
            em.close();
        }
    }
}
