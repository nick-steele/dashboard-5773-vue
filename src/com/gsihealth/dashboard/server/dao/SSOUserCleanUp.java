package com.gsihealth.dashboard.server.dao;

//classes used for Entity Creation in persistence.xml
import com.gsihealth.entity.User;

import java.io.File;
import java.io.FileInputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

/**
 * Deletes users from OpenAM that are not present in the Connect.users DB table
 * 
 * @author Edwin Montgomery
 */
public class SSOUserCleanUp {

    
    private Properties props;
    private Logger logger = Logger.getLogger(getClass().getName());
    private SSOUserDAO ssoUserDAO;
    private UserDAO userDAO;
  
    private static final String DEFAULT_PASSWORD = "Test123#";

    public SSOUserCleanUp(String propsFileName) throws Exception {
        props = new Properties();

        logger.info("Loading properties file: " + propsFileName);
        props.load(new FileInputStream(propsFileName));
        logger.info("Loaded: " + props);

        ssoUserDAO = new SSOUserDAOImpl();
        //Add to fetch user data from DB
        userDAO = new UserDAOImpl();
      
       
    }

    public void run(String userAccountsFileName) throws Exception {
        List<String> userAccounts = FileUtils.readLines(new File(userAccountsFileName));


        String token = getToken();

        for (String temp : userAccounts) {
            String[] data = temp.split(",");

            String firstName = data[0].trim();
            String lastName = data[1].trim();
            String email = data[2].trim();
            
            String ssoUserString = buildSsoUserString(firstName, lastName, email);
            System.out.println("Adding user account to OpenAM: " + temp);
            ssoUserDAO.addUser(1, token, ssoUserString);
            System.out.println("Success!\n");

            Thread.sleep(100);
        }
        ssoUserDAO.logout(1, token);        
    }
    
      public void cleanOpenAMUserList(String openAMuserAccountsFileName) throws Exception {
        
        System.out.println("Getting OpenAM token...");
        String token = getToken();
        // Read identity list from ssoadm 
        List<String> openAMuserAccounts = FileUtils.readLines(new File(openAMuserAccountsFileName));
        
        //Get Connect User List
        List<User> connectUsers = userDAO.findUserEntities();
        System.out.println("There are " +connectUsers.size() + " users in the Connect DB");
        System.out.println("There are " +openAMuserAccounts.size() + " users in the OpenAM");
        
        ArrayList<String> validConnectUserIds = new ArrayList();     
        for (User validConnectUser : connectUsers)
            {
                validConnectUserIds.add(validConnectUser.getEmail());
                System.out.println("Valid Connect UserIds --"+validConnectUser.getEmail()+"--");
            }   
        System.out.println("There are " +validConnectUserIds.size() + " Connect UserIds in the Connect DB");

        int syncCounter=1;
        int orphanCounter = 1;
        int openAMInternalAccountCounter=1;
        

        for (String temp : openAMuserAccounts) {
            String[] data = temp.split(" ");
            String openAMaccount = data[0].trim();
            
            //If users not in Connect.DB then delete from OpenAM 
            if(!validConnectUserIds.contains(openAMaccount)) {
                String ssoUserString = buildSsoDeleteUserString(openAMaccount);
                if (openAMaccount.contains("@"))
                    {
                    System.out.println(syncCounter+ ": Deleting orphan user account from OpenAM: " + openAMaccount);
                    //ssoUserDAO.deleteUser(props, token, ssoUserString);
                    System.out.println("Success!\n");
                    orphanCounter++;
                    }
                else 
                    {
                    System.out.println(syncCounter+": Not deleting because this looks like an internal OpenAM account: " +openAMaccount);
                    openAMInternalAccountCounter++;
                    }
            } else {
                System.out.println(syncCounter + ": User exists in Connect DB: " + openAMaccount);
            }
            
            syncCounter++;

            Thread.sleep(100);
        }
        System.out.println("===== Number of OpenAM Orphan Accounts Deleted: "+ orphanCounter + " =======");
        System.out.println("===== Number of internal OpenAM  Accounts preserved: "+ openAMInternalAccountCounter + " =======");
    }
     
 
    private String buildSsoUserString(String firstName, String lastName, String email) throws Exception {
        StringBuilder ssoUserString = new StringBuilder();

        String encodedEmail = encode(email);
        
        ssoUserString.append("&identity_name=" + encodedEmail);
        ssoUserString.append("&identity_attribute_names=cn&identity_attribute_values_cn=" + encode(firstName)+"%20"+encode(lastName));
        ssoUserString.append("&identity_attribute_names=sn&identity_attribute_values_sn=" + encode(firstName));
        ssoUserString.append("&identity_attribute_names=userpassword&identity_attribute_values_userpassword=" + encode(DEFAULT_PASSWORD));
        ssoUserString.append("&identity_realm=%2F&identity_type=user");

        return ssoUserString.toString();
    }
    
    private String buildSsoDeleteUserString(String email) throws Exception {
        StringBuilder ssoUserString = new StringBuilder();

        String encodedEmail = encode(email);
        
        ssoUserString.append("&identity_name=" + encodedEmail);
        ssoUserString.append("&identity_attribute_names=userpassword&identity_attribute_values_userpassword=" + encode(DEFAULT_PASSWORD));
        ssoUserString.append("&identity_realm=%2F&identity_type=user");

        return ssoUserString.toString();
    }

    private String getToken() throws Exception {
        String adminUserName = props.getProperty("sso.server.admin.username");
        String adminPassword = props.getProperty("sso.server.admin.password");

        String tokenId = ssoUserDAO.authenticate(1, adminUserName, adminPassword); //"amadmin","adminadmin");
        tokenId = StringUtils.trimToEmpty(tokenId);
        logger.info("token id = " + tokenId);

        return tokenId;
    }

    protected String encode(String value) throws Exception {
        if (value != null) {
            value = URLEncoder.encode(value, "UTF-8");
        } else {
            value = "";
        }
        return value;
    }

    public static void main(String[] args) throws Exception {
        
        // DEV
       String propsFileName = "propfiles-temp/dev-dashboard.properties";
       String userAccountsFileName = "propfiles-temp/openam-dev-users.txt";
        

       // QA
       // String propsFileName = "propfiles-temp/qa-dashboard.properties";
       // String userAccountsFileName = "propfiles-temp/openam-qa-users.txt";
        
       //Staging
       //String propsFileName = "propfiles-temp/staging-dashboard.properties";
       // String userAccountsFileName = "propfiles-temp/qa-users.txt.csv";
        
        //Production
        // String propsFileName = "propfiles-temp/production-dashboard.properties";
        // String userAccountsFileName = "propfiles-temp/qa-users.txt.csv";

        SSOUserCleanUp util = new SSOUserCleanUp(propsFileName);
        
        //remove openAM users that don't exisit in Connect Database
        util.cleanOpenAMUserList(userAccountsFileName);
        
     
    }

    protected String getPlainName(String email) {

        String data = null;
        if (email.contains("@")) {
            data = email.split("@")[0];
        }
        else {
            data = email;
        }

        System.out.println("data = " + data);
        
        data = data.replaceAll("\\.", " ");

        return data;
    }


}
