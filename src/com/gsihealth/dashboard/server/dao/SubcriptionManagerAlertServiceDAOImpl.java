/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.ApplicationSendsAlert;
import com.gsihealth.entity.UserAlertFilter;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;

/**
 *
 * @author Vinay
 */
public class SubcriptionManagerAlertServiceDAOImpl implements SubcriptionManagerAlertServiceDAO {

    private EntityManagerFactory emf = null;
    private Logger logger = Logger.getLogger(getClass().getName());

    public SubcriptionManagerAlertServiceDAOImpl() {
        emf = Persistence.createEntityManagerFactory("gsialertingPU_standalone");
    }

    public SubcriptionManagerAlertServiceDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public List<ApplicationSendsAlert> getEnabledAppSendsAlertList(Long applicationID, Long communityId) {
        EntityManager em = getEntityManager();
        List<ApplicationSendsAlert> appAlertList = null;
        try {
            Query q = em.createQuery("select object(o) from ApplicationSendsAlert as o where o.applicationId =:applicationId "
                    + "AND o.communityId = :communityId "
                    + "AND o.status=:status ");
            q.setParameter("communityId", communityId);
            q.setParameter("applicationId", applicationID);
            q.setParameter("status", "ENABLED");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            appAlertList = q.getResultList();
        } catch(Exception ex) {
            logger.severe("getEnabledappSendsAlertList fail for " + applicationID + " " + ex);
        }
        finally {
            em.close();
        }

        return appAlertList;
    }
    
    /**
     * TODO - implement this to scale down the nested if's in subcriptionManagerAlertServiceImpl
     * @param userId
     * @param communityId
     * @return a list of alerts filtered by status and user alert filters
     */ 
//    private List<ApplicationSendsAlert> getAppSendsAlertListPerUAF(long userId, long communityId){
//    
//    }


    /**
     *
     * @param userAlertFilterList
     * @throws Exception
     */
    @Override
    public void updateUserAlertFilter(List<UserAlertFilter> userAlertFilterList) throws Exception {
       
        EntityManager em = getEntityManager();
        EntityTransaction transaction = null;

        try {
            for (UserAlertFilter alertFilter : userAlertFilterList) {

                transaction = em.getTransaction();
                transaction.begin();

                UserAlertFilter userAlertFilter = getUserAlertFilter(alertFilter.getUserId(), alertFilter.getAppAlertID(),
                        alertFilter.getCommunityId());
                if (userAlertFilter != null) {
                   
                    ApplicationSendsAlert appAlertFilter = getApplicationSendsAlert(alertFilter.getAppAlertID());
                    userAlertFilter.setApplicationAlertId(appAlertFilter);

                    userAlertFilter.setShowAlert(alertFilter.getShowAlert());
                    userAlertFilter.setCommunityId(alertFilter.getCommunityId());
                    userAlertFilter.setLastUpdateDateTime(new Date());
                    em.merge(userAlertFilter);
                    logger.info("merge UserAlertFilter  Application Alert ID =" + 
                            userAlertFilter.getApplicationAlertId().getApplicationAlertId()  
                            + ":for UserId:" + userAlertFilter.getUserId() + ":ShowAlert:" + userAlertFilter.getShowAlert());
                }
                transaction.commit();
              
            }
        } catch (Exception exc) {
            transaction.rollback();

            throw exc;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    private UserAlertFilter getUserAlertFilter(long userId, long applicationAlertId, BigInteger communityId) {
         logger.info("getUserAlertFilter method DAO userId :"+userId+":applicationAlertId:"+applicationAlertId);
        EntityManager em = getEntityManager();
        UserAlertFilter userAlertFilter = null;

        try {
            Query q = em.createQuery("select object(o) from UserAlertFilter as o "
                    + "where o.userId =:userId and o.applicationAlertId.applicationAlertId=:applicationAlertId"
                    + " AND o.communityId = :communityId");
            q.setParameter("userId", userId);
            q.setParameter("applicationAlertId", applicationAlertId);
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            userAlertFilter = (UserAlertFilter) q.getSingleResult();

        } finally {
            em.close();
        }

        return userAlertFilter;
    }

    private ApplicationSendsAlert getApplicationSendsAlert(long applicationAlertId) {
        EntityManager em = getEntityManager();
        ApplicationSendsAlert appSendsAlert = null;

        try {
            Query q = em.createQuery("select object(o) from ApplicationSendsAlert as o where o.applicationAlertId =:applicationAlertId");
            q.setParameter("applicationAlertId", applicationAlertId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            appSendsAlert = (ApplicationSendsAlert) q.getSingleResult();

        } finally {
            em.close();
        }

        return appSendsAlert;
    }


    @Override
    public List<ApplicationSendsAlert> getAppSendsAlerts(Long communityId) {
        EntityManager em = getEntityManager();
        List<ApplicationSendsAlert> appAlertList = null;
        try {
            Query q = em.createQuery("select object(o) from ApplicationSendsAlert as o where o.status =:status"
                    + " AND o.communityId = :communityId");
            q.setParameter("status", "ENABLED");
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            appAlertList = q.getResultList();
        } finally {
            em.close();
        }

        return appAlertList;
    }

    /**
     *
     * @param userAlertFilterList
     * @throws Exception
     */
    public void saveUserAlertFilter(List<UserAlertFilter> userAlertFilterList) throws Exception {

        EntityManager em = getEntityManager();
        EntityTransaction transaction = null;

        try {
            for (UserAlertFilter alertFilter : userAlertFilterList) {

                transaction = em.getTransaction();
                transaction.begin();

                ApplicationSendsAlert appAlertFilter = getApplicationSendsAlert(alertFilter.getAppAlertID());
              
                alertFilter.setApplicationAlertId(appAlertFilter);
                alertFilter.setShowAlert(alertFilter.getShowAlert());
                alertFilter.setCommunityId(alertFilter.getCommunityId());
                alertFilter.setLastUpdateDateTime(new Date());
                //logger.info("persist UserAlertFilter  Application Alert ID =" + alertFilter.getApplicationAlertId().getApplicationAlertId() + ":CommunityId:" + alertFilter.getCommunityId() + ":UserId:" + alertFilter.getUserId() + ":ShowAlert:" + alertFilter.getShowAlert());
                em.persist(alertFilter);
                transaction.commit();
                
            }
        } catch (Exception exc) {
            transaction.rollback();

            throw exc;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
