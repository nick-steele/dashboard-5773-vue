/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.Samhsa;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;

/**
 *
 * @author rsharan
 */
public class SamhsaDAOImpl implements SamhsaDAO{
    
       private EntityManagerFactory emf = null;
       
        public SamhsaDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }
        
        public SamhsaDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }
        
         public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
         
          @Override
    public Samhsa findActiveSamhsa(long communityId) {
        EntityManager em = getEntityManager();
        try {
             Query q = em.createQuery("select o from Samhsa o where o.active='Y' and o.samhsaPK.communityId= :theCommunityId order by o.datetimeModified desc");
//            Query q = em.createQuery("select s  from Samhsa s where s.active='Y' and s.community_id= :theCommunity_id order by s.datetimeModified desc");
//             Query q = em.createNativeQuery("select *  from Samhsa o where o.active='Y' and o.community_Id= theCommunity_Id order by o.datetimeModified desc");
            q.setParameter("theCommunityId", communityId);
          
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<Samhsa> list = (List<Samhsa>) q.getResultList();
            Samhsa samhsa = null;
            if (!list.isEmpty()) {
                samhsa = list.get(0);
            }

            return samhsa;
        } finally {
            em.close();
        }
    }
    
    
    
    
}
