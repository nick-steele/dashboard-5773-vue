/**
 * 
 */
package com.gsihealth.dashboard.server.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import com.gsihealth.entity.NotificationAudit;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Vishal (Off.)
 *
 */
public class NotificationAuditDAOImpl implements NotificationAuditDAO {

    private EntityManagerFactory emf = null;
    private Logger logger = Logger.getLogger(getClass().getName());

    public NotificationAuditDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public NotificationAuditDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public void create(NotificationAudit notificationAudit) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();

            em.merge(notificationAudit);

            em.getTransaction().commit();

        } catch (Exception ex) {
            logger.log(Level.SEVERE, "Error: " + ex.getMessage(), ex);

        } finally {

            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void update(NotificationAudit notificationAudit) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.merge(notificationAudit);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = new Long("456");
                if (findNotificationAudit(id) == null) {
                    throw new NonexistentEntityException("The Organization with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }

    }

    @Override
    public List<NotificationAudit> findAll() {
        return findNotificationAuditEntities(true, -1, -1);
    }

    @Override
    public NotificationAudit findNotificationAudit(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(NotificationAudit.class, id);
        } finally {
            em.close();
        }
    }

    public List<NotificationAudit> findNotificationAuditEntities() {
        return findNotificationAuditEntities(true, -1, -1);
    }

    public List<NotificationAudit> findNotificationAuditEntities(int maxResults, int firstResult) {
        return findNotificationAuditEntities(false, maxResults, firstResult);
    }

    private List<NotificationAudit> findNotificationAuditEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(na) from NotificationAudit as na order by na.createdOn");
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

   
}
