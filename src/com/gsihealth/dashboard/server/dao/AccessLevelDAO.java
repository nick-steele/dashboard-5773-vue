package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.AccessLevel;
import java.util.List;

/**
 *
 * @author Satyendra Singh
 */
public interface AccessLevelDAO {


    public AccessLevel findAccessLevel(Long id, Long communityId);

    public List<AccessLevel> findAccessLevelEntities(long communityId);
       
}
