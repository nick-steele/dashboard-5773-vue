/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author vlewis
 */
@Entity
public class UserPatient implements Serializable{
@Id
@Column(name = "USE_ID")
private Long userId;
        
@Column(name = "PATIENT_ID")
private Long patientId;

public UserPatient(){
    
}
public UserPatient(Long userId, Long patientId){
    this.userId = userId;
    this.patientId = patientId;
    
}
        /**
         * @return the userId
         */
        public Long getUserId() {
            return userId;
        }

        /**
         * @param userId the userId to set
         */
        public void setUserId(Long userId) {
            this.userId = userId;
        }

        /**
         * @return the patientId
         */
        public Long getPatientId() {
            return patientId;
        }

        /**
         * @param patientId the patientId to set
         */
        public void setPatientId(Long patientId) {
            this.patientId = patientId;
        }
      
    }
