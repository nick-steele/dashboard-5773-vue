package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.Role;
import java.util.List;

/**
 *
 * @author Satyendra Singh
 */
public interface RoleDAO {


    public Role findRole(Long id,long communityId);

    public List<Role> findRoleEntities(long  communityId);
    
    public Role findProviderRole(Long id,long communityId);
       
}
