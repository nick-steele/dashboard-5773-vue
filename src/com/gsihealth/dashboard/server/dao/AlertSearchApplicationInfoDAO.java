package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.AlertSearchApplicationInfo;
import java.util.List;

/**
 *
 * @author Chad Darby
 */
public interface AlertSearchApplicationInfoDAO {

    public List<AlertSearchApplicationInfo> findAlertSearchApplicationInfoEntities(Long communityId);
    
}
