package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.FacilityType;
import java.util.List;

/**
 *
 * @author Satyendra Singh
 */
public interface FacilityTypeDAO {

    public FacilityType findFacilityType(long communityId, int facilityTypeId);

    public List<FacilityType> findFacilityTypeEntities(long communityId);

    public void create(FacilityType tempFacilityType);

}
