/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import com.gsihealth.entity.ProgramNameHistory;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Satyendra Singh
 */
public class ProgramNameHistoryDAOImpl implements ProgramNameHistoryDAO {

    private EntityManagerFactory emf = null;

    public ProgramNameHistoryDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public ProgramNameHistoryDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

//    public void addToProgramNameHistory(ProgramNameHistory programNameHistory) {
//        EntityManager em = null;
//        try {
//            em = getEntityManager();
//            em.getTransaction().begin();
//            em.persist(programNameHistory);
//            em.getTransaction().commit();
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }
    
     public void addToProgramNameHistory(ProgramNameHistory programNameHistory) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.merge(programNameHistory);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }


    public Long getLastProgramName(Long patientId) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    //Spira 6467
    @Override
    public Date getMostRecentStatusEffectiveDateForStatus(long patientId, long programId, long status, long communityId)
    {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("SELECT pnh.statusEffectiveDate FROM ProgramNameHistory pnh where pnh.patientId = :patientId "
                    + " AND pnh.status = :status "
                    + " AND pnh.statusEffectiveDate IS NOT NULL "
                    + " AND pnh.communityId= :communityId "
                    + " AND pnh.currentProgramName = "
                    + "(select pn.value from ProgramName pn where pn.programNamePK.id=:programNameId and pn.programNamePK.communityId=:communityId) "
                    + " AND (pnh.patientProgramNameId NOT IN (SELECT pnhsq.patientProgramNameId FROM ProgramNameHistory pnhsq WHERE " 
                    + " pnhsq.patientId = :patientId " 
                    + " AND pnhsq.communityId= :communityId "
                    + " AND pnhsq.deleteReason IS NOT NULL AND pnhsq.patientProgramNameId IS NOT NULL) OR pnh.patientProgramNameId IS NULL )"
                    + " ORDER BY pnh.actionDatetime DESC ");
            q.setParameter("patientId", patientId);
            q.setParameter("programNameId", programId);
            q.setParameter("status", status);
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            //get the first row only
            q.setMaxResults(1);
            
            return  (Date)q.getSingleResult();
        } finally {
            em.close();
        }        
    }

    //Spira 7555
    @Override    
    public List<Long> getDistinctStatusListExceptCurrent(long patientId, long programId, long currentStatus, long communityId)
    {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("SELECT DISTINCT(pnh.status) FROM ProgramNameHistory pnh where pnh.patientId = :patientId "
                    + " AND pnh.status <> :currentStatus "
                    + " AND pnh.statusEffectiveDate IS NOT NULL "
                    + " AND pnh.communityId= :communityId "
                    + " AND pnh.currentProgramName = "
                    + "(select pn.value from ProgramName pn where pn.programNamePK.id=:programNameId and pn.programNamePK.communityId=:communityId) "
                    + " AND (pnh.patientProgramNameId NOT IN (SELECT pnhsq.patientProgramNameId FROM ProgramNameHistory pnhsq WHERE " 
                    + " pnhsq.patientId = :patientId " 
                    + " AND pnhsq.communityId= :communityId "
                    + " AND pnhsq.deleteReason IS NOT NULL AND pnhsq.patientProgramNameId IS NOT NULL) OR pnh.patientProgramNameId IS NULL )");
            q.setParameter("patientId", patientId);
            q.setParameter("programNameId", programId);
            q.setParameter("currentStatus", currentStatus);
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            
            return q.getResultList();
        } finally {
            em.close();
        }                
    }
    
}
