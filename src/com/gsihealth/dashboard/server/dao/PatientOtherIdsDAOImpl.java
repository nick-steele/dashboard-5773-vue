/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.PatientOtherIds;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.PreexistingEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.RollbackFailureException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import java.util.Date;

/**
 *
 * @author beth.boose
 */
public class PatientOtherIdsDAOImpl implements PatientOtherIdsDAO {

    private EntityManagerFactory emf = null;

    public PatientOtherIdsDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public PatientOtherIdsDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void add(PatientOtherIds patientOtherIds) throws Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();
        patientOtherIds.setCreationDateTime(new Date());
        patientOtherIds.setLastUpdateDateTime(new Date());
        try {
            utx.begin();
            em.persist(patientOtherIds);
            utx.commit();
            
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    /** delete a single entry by id (not patientId) */
    public void delete(long idId) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();
        try {
            utx.begin();
            PatientOtherIds patientOtherIds;
            try {
                patientOtherIds = em.getReference(PatientOtherIds.class, idId);
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("Could not delete this patient id: " + enfe);
            }
            em.remove(patientOtherIds);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void update(PatientOtherIds patientOtherIds) throws Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();
        patientOtherIds.setLastUpdateDateTime(new Date());
        try {
            utx.begin();
            patientOtherIds = em.merge(patientOtherIds);
            utx.commit();
        } catch (Exception ex) {
            throw new Exception(ex);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public PatientOtherIds findPatientOtherIds(Long idId) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PatientOtherIds.class, idId);
        } finally {
            em.close();
        }
    }

    public List<PatientOtherIds> findPatientOtherIdsEntities(long patientId, int pageNumber, int pageSize,long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("PatientOtherIds.findByPatientCommunity");
            q.setParameter("patientId", patientId);
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            
            int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);

            q.setFirstResult(startRecordNumber);
            q.setMaxResults(pageSize);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public int getPatientOtherIdsCount(long patientId, long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from PatientOtherIds as o "
                    + "where o.patientId = :patientId "
                    + "and o.communityId=:communityId");
            q.setParameter("patientId", patientId);
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            
            int count = ((Long) q.getSingleResult()).intValue();
            return count;
        } finally {
            em.close();
        }
    }

    /**
     * if patientId, community id and other org id are the same, these are duplicates
     * TODO - this is kind of misleading, they're not dupes, just a patient can't have more than one id
     * in an org
     * @param patientOtherIds
     * @return true is this patient already exists for this org oid
     */
    public boolean isDuplicate(PatientOtherIds patientOtherIds) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from PatientOtherIds as o where o.patientId = :patientId"
                    + " and o.patientOtherOrgOid = :patientOtherOrgOid "
                    + " and o.communityId = :communityId");
            q.setParameter("patientId", patientOtherIds.getPatientId());
            q.setParameter("patientOtherOrgOid", patientOtherIds.getPatientOtherOrgOid());
            q.setParameter("communityId", patientOtherIds.getCommunityId());
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            
            int count = ((Long) q.getSingleResult()).intValue();

            return count > 0;
        } finally {
            em.close();
        }
    }

}
