package com.gsihealth.dashboard.server.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.entity.FacilityType;
import com.gsihealth.entity.FacilityTypePK;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;

/**
 *
 * @author Satyendra Singh
 */
public class FacilityTypeDAOImpl implements FacilityTypeDAO {

    private EntityManagerFactory emf = null;

    public FacilityTypeDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public FacilityTypeDAOImpl(EntityManagerFactory theEmf) {
        emf = theEmf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public List<FacilityType> findFacilityTypeEntities(long communityId) {

        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(f) from FacilityType as f where f.facilityTypePK.communityId=:theCommunityId");
            q.setParameter("theCommunityId", (int) communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public FacilityType findFacilityType(long communityId, int facilityTypeId) {
        
        FacilityTypePK id = new FacilityTypePK();
        id.setCommunityId((int) communityId);
        id.setFacilityTypeId(facilityTypeId);
        
        EntityManager em = getEntityManager();
        try {
            return em.find(FacilityType.class, id);
        } finally {
            em.close();
        }
    }

    public void create(FacilityType tempFacilityType) {
        EntityManager em = getEntityManager();
        try {
            em.getTransaction().begin();

            em.merge(tempFacilityType);
            
            em.getTransaction().commit();
        
        } finally {
            em.close();
        }
    }
}
