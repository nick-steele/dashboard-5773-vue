/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.PatientActivityTracker;
import com.gsihealth.dashboard.server.dao.exceptions.PreexistingEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.RollbackFailureException;
import com.gsihealth.entity.PatientActivityHistory;
import com.gsihealth.entity.PatientActivityNames;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import org.apache.commons.lang.time.StopWatch;

/**
 *
 * @author ssingh
 */
public class PatientActivityTrackerDAOImpl implements PatientActivityTrackerDAO {

    private EntityManagerFactory emf = null;
    private Logger logger = Logger.getLogger(getClass().getName());

    public PatientActivityTrackerDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public PatientActivityTrackerDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public List<PatientActivityTracker> findPatientActivityEntitiesByCommunityId(long patientId, long communityId) {
        EntityManager em = getEntityManager();
        try {

            Query q = em.createQuery("select object(o) from PatientActivityTracker as o where o.communityId=:theCommunityId and o.patientId=:thePatientId");
            q.setParameter("theCommunityId", communityId);
            q.setParameter("thePatientId", patientId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return (List<PatientActivityTracker>) q.getResultList();
        } finally {
            em.close();
        }
    }

    @Override
    public void savePatientActivityList(long patientId, List<PatientActivityTracker> data) throws Exception {

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        add(data);

        stopWatch.stop();
        logger.info("Time to add new patient activity list=" + stopWatch.getTime() / 1000.0 + " secs");
    }

    private void add(List<PatientActivityTracker> activityList) throws PreexistingEntityException, RollbackFailureException, Exception {

        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();

            for (PatientActivityTracker patientActivityTracker : activityList) {
                em.merge(patientActivityTracker);
            }

            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PatientActivityTracker findEntity(long patientId, Long id, long communityId) throws Exception {
        EntityManager em = getEntityManager();
        try {

            Query q = em.createQuery("select object(o) from PatientActivityTracker as o where o.communityId=:theCommunityId and o.patientId=:thePatientId and o.id=:theId");
            q.setParameter("theCommunityId", communityId);
            q.setParameter("thePatientId", patientId);
            q.setParameter("theId", id);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return (PatientActivityTracker) q.getSingleResult();
        } finally {
            em.close();
        }
    }

    @Override
    public void updatePatientActivityList(List<PatientActivityTracker> patientActivityList) throws PreexistingEntityException, RollbackFailureException, Exception {

        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();


        try {
            utx.begin();

            for (PatientActivityTracker patientActivity : patientActivityList) {
                em.merge(patientActivity);
            }

            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void addToPatientActivityHistory(List<PatientActivityHistory> patientActivityHistoryList) throws PreexistingEntityException, RollbackFailureException, Exception {
         EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();

            for (PatientActivityHistory patientActivityHistory : patientActivityHistoryList) {
                em.merge(patientActivityHistory);
            }

            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PatientActivityNames getActivityName(long activityId, long communityId) throws Exception {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from PatientActivityNames as o where o.id=:theId and o.communityId=:theCommunityId");
            q.setParameter("theId", activityId);
            q.setParameter("theCommunityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return (PatientActivityNames) q.getSingleResult();
        } finally {
            em.close();
        }

    }

    @Override
    public boolean hasConsent(long userId, long patientId, long communityId) {

        EntityManager em = getEntityManager();

        boolean result = false;

        logger.info("Checking consent for: userId=" + userId + ", patientId=" + patientId + ", communityId=" + communityId);

        try {
            String sql = "select count(*) from view_user_patient_consent where"
                    + " user_id=" + userId
                    + " and patient_id=" + patientId
                    + " and community_id=" + communityId;

            Query patientQuery = em.createNativeQuery(sql);
            //patientQuery.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            Vector resultSet = (Vector) patientQuery.getSingleResult();

            long count = (Long) resultSet.get(0);

            logger.info("count =" + count);

            result = count > 0;

            logger.info("hasConsent=" + result);

        } finally {
            em.close();
        }

        return result;
    }

    @Override
    public void delete(long id, long communityId) throws Exception {
        EntityManager em = getEntityManager();
        PatientActivityTracker patientActivity = new PatientActivityTracker();
        try {
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();

            Query query = em.createQuery("select object(o) from PatientActivityTracker as o where o.id=:theId and o.communityId=:theCommunityId");
            query.setParameter("theId", id);
            query.setParameter("theCommunityId", communityId);

            query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            PatientActivityTracker items = (PatientActivityTracker) query.getSingleResult();
            if (items != null) {
                em.remove(items);
                transaction.commit();
            }
        } catch (Exception exc) {
            logger.log(Level.WARNING, "Could not delete PatientActivityTracker: " + patientActivity, exc);
        } finally {
            em.close();
        }

    }

    @Override
    public PatientActivityTracker findById(long id, long communityId) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PatientActivityTracker.class, id);
        } finally {
            em.close();
        }
    }
    @Override
     public void addToPatientActivityHistory(PatientActivityHistory data)throws Exception{
          EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.merge(data);
            em.getTransaction().commit();
        } catch (Exception exc) {
            logger.log(Level.WARNING, "Could not persist PatientActivityHistory: " + data, exc);
        } finally {
            if (em != null) {
                em.close();
            }
        }
     }
}
