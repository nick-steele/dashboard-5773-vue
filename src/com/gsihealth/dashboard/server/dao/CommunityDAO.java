package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.Community;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author User
 */
public interface CommunityDAO{

    public Community getCommunityById(Long aLong);
    public Community getCommunityById(Long aLong, EntityManager em);

    public List<Community> findEntities();
    
}
