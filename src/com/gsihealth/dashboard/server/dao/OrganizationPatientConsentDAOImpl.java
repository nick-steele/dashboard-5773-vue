package com.gsihealth.dashboard.server.dao;

import com.gsihealth.entity.OrganizationPatientConsent;
import com.gsihealth.entity.OrganizationPatientConsentPK;
import com.gsihealth.dashboard.server.dao.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.PreexistingEntityException;
import com.gsihealth.dashboard.server.dao.exceptions.RollbackFailureException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import org.apache.commons.lang.time.StopWatch;

/**
 *
 * @author Chad Darby
 */
public class OrganizationPatientConsentDAOImpl implements OrganizationPatientConsentDAO {

    private EntityManagerFactory emf = null;
    private Logger logger = Logger.getLogger(getClass().getName());

    public OrganizationPatientConsentDAOImpl() {
        emf = Persistence.createEntityManagerFactory("dashboardPU_standalone");
    }

    public OrganizationPatientConsentDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public void add(OrganizationPatientConsent organizationPatientConsent) throws PreexistingEntityException, RollbackFailureException, Exception {

        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        if (organizationPatientConsent.getOrganizationPatientConsentPK() == null) {
            organizationPatientConsent.setOrganizationPatientConsentPK(new OrganizationPatientConsentPK());
        }

        // set start date to now
        organizationPatientConsent.setStartDate(new Date());

        try {
            utx.begin();
            em.merge(organizationPatientConsent);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findOrganizationPatientConsent(organizationPatientConsent.getOrganizationPatientConsentPK()) != null) {
                throw new PreexistingEntityException("OrganizationPatientConsent " + organizationPatientConsent + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void add(List<OrganizationPatientConsent> organizationPatientConsentList) throws PreexistingEntityException, RollbackFailureException, Exception {

        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        Date now = new Date();

        try {
            utx.begin();

            for (OrganizationPatientConsent organizationPatientConsent : organizationPatientConsentList) {
                if (organizationPatientConsent.getOrganizationPatientConsentPK() == null) {
                    organizationPatientConsent.setOrganizationPatientConsentPK(new OrganizationPatientConsentPK());
                }

                // set start date to now
                organizationPatientConsent.setStartDate(now);

                em.merge(organizationPatientConsent);
            }

            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void update(OrganizationPatientConsent organizationPatientConsent) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();
            organizationPatientConsent = em.merge(organizationPatientConsent);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                OrganizationPatientConsentPK id = organizationPatientConsent.getOrganizationPatientConsentPK();
                if (findOrganizationPatientConsent(id) == null) {
                    throw new NonexistentEntityException("The organizationPatientConsent with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void delete(OrganizationPatientConsentPK id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();

        try {
            utx.begin();
            OrganizationPatientConsent organizationPatientConsent;



            try {
                organizationPatientConsent = em.getReference(OrganizationPatientConsent.class, id);
                organizationPatientConsent.getOrganizationPatientConsentPK();

                em.remove(organizationPatientConsent);

                utx.commit();
            } catch (EntityNotFoundException enfe) {
                logger.warning("The organizationPatientConsent with id " + id + " no longer exists.");
            }

        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<OrganizationPatientConsent> findOrganizationPatientConsentEntities(long patientId,long communityId, int pageNumber, int pageSize) {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery("select object(o) from OrganizationPatientConsent as o where o.organizationPatientConsentPK.patientId = :thePatientId");
            q.setParameter("thePatientId", patientId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int startRecordNumber = Math.max(0, (pageNumber - 1) * pageSize);

            q.setFirstResult(startRecordNumber);
            q.setMaxResults(pageSize);

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public OrganizationPatientConsent findOrganizationPatientConsent(OrganizationPatientConsentPK id) {
        EntityManager em = getEntityManager();


        try {
            return em.find(OrganizationPatientConsent.class, id);
        } finally {
            em.close();
        }
    }

    @Override
    public int getOrganizationPatientConsentCount(long patientId,long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from OrganizationPatientConsent as o where o.organizationPatientConsentPK.patientId = :thePatientId");
            q.setParameter("thePatientId", patientId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public List<OrganizationPatientConsent> findOrganizationPatientConsentEntities(long patientId,long communityId) {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery("select object(o) from OrganizationPatientConsent as o where o.organizationPatientConsentPK.patientId = :thePatientId");
            q.setParameter("thePatientId", patientId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public void saveOrganizationPatientConsentList(long patientId, List<OrganizationPatientConsent> data) throws Exception {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        // add new entries
        add(data);
        stopWatch.stop();
        logger.info("Time to add new organization patient consent list=" + stopWatch.getTime() / 1000.0 + " secs");
    }

    private void deleteOldConsentEntries(long patientId) {

        EntityManager em = getEntityManager();

        try {
            EntityTransaction transaction = em.getTransaction();
            transaction.begin();

            Query query = em.createQuery("select object(o) from OrganizationPatientConsent as o where o.organizationPatientConsentPK.patientId = :thePatientId");
            query.setParameter("thePatientId", patientId);
            query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<OrganizationPatientConsent> items = query.getResultList();

            for (OrganizationPatientConsent tempOrganizationPatientConsent : items) {
                try {
                    tempOrganizationPatientConsent.getOrganizationPatientConsentPK();
                    em.remove(tempOrganizationPatientConsent);
                } catch (Exception exc) {
                    logger.log(Level.WARNING, "Could not delete UserOrganizationPatientConsentPatientConsent: " + tempOrganizationPatientConsent.getOrganizationPatientConsentPK(), exc);
                }            
            }

            transaction.commit();
        } finally {
            em.close();
        }

    }

    public boolean hasConsent(long patientId,long communityId, long userOrganizationId) {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery("select count(o) from OrganizationPatientConsent as o"
                    + " where o.organizationPatientConsentPK.patientId = :thePatientId"
                    + " and o.organizationPatientConsentPK.organizationId= :theUserOrganizationId"
                    + " and o.consentStatus='PERMIT'");

            q.setParameter("thePatientId", patientId);
            q.setParameter("theUserOrganizationId", userOrganizationId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            int count = ((Long) q.getSingleResult()).intValue();

            boolean hasConsent = count > 0;

            // System.out.printf("hasConsent(): patientId=%d,  organizationId=%d,  hasConsent=%b\n", patientId, userOrganizationId, hasConsent);

            return hasConsent;

        } finally {
            em.close();
        }
    }

    @Override
    public List<OrganizationPatientConsent> 
        findOrganizationPatientConsentEntitiesByOrg(long organizationId, Long communityId) {
        EntityManager em = getEntityManager();

        try {
            Query q = em.createQuery("select object(o) from OrganizationPatientConsent as o "
                    + "WHERE o.organizationPatientConsentPK.organizationId = :theOrganizationId"
                    + " AND o.organizationPatientConsentPK.communityId = :communityId");
            q.setParameter("theOrganizationId", organizationId);
            q.setParameter("communityId", communityId);
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            return q.getResultList();
        } finally {
            em.close();
        }
    }
}
