package com.gsihealth.dashboard.server.servlet;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;


/**
 * Downloads documents from document center
 * 
 */
public class DocumentCenterServlet extends HttpServlet {

    private Logger logger = Logger.getLogger(getClass().getName());
    String document = null;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        File theFile = null;
        String fileName = null;

        try {
            // read file name param
            document = request.getParameter("param");

            // make sure file is valid
            if (document != null) {
                theFile = new File(document);
                fileName = theFile.getName();
            }
            else {
                throw new Exception("File name not given in parameter.");
            }

            // set headers
            response.setContentType("application/octet-stream");
            response.setHeader("Content-disposition", "attachment;filename=\"" + fileName + "\"");
          
            // get data streams
            ServletOutputStream outputToBrowser = response.getOutputStream();
            BufferedInputStream inputFromFile = new BufferedInputStream(new FileInputStream(theFile));

            // send the file to browser
            IOUtils.copyLarge(inputFromFile, outputToBrowser);
            
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error downloading file: " + document, e);
            throw new ServletException("Error downloading file", e);            
        }
    }
}
