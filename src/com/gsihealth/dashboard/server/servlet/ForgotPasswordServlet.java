/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.servlet;

import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.SSOUserDAO;
import com.gsihealth.dashboard.server.dao.UserDAO;
import com.gsihealth.dashboard.server.dao.UserDAOImpl;
import com.gsihealth.dashboard.server.service.SecurityServiceHelper;
import com.gsihealth.dashboard.server.util.PasswordUtils;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Chad Darby
 */
public class ForgotPasswordServlet extends HttpServlet {

    private Logger logger = Logger.getLogger(getClass().getName());
    private SecurityServiceHelper securityServiceHelper;

    @Override
    public void init() throws ServletException {

        ServletContext application = getServletContext();

        UserDAO userDAO = (UserDAOImpl) application.getAttribute(WebConstants.USER_DAO_KEY);
        SSOUserDAO ssoUserDAO = (SSOUserDAO) application.getAttribute(WebConstants.SSO_USER_DAO_KEY);

        securityServiceHelper = new SecurityServiceHelper(userDAO, ssoUserDAO);
    }

    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String message = null;
        String email = request.getParameter("user_email");
        Long communityId = Long.parseLong(request.getParameter(WebConstants.USER_COMMUNITY_KEY));
        boolean isError = false;

        try {
            //JIRA 629 - remove properties
            boolean isValid = securityServiceHelper.handleForgotPassword(email, request, communityId);

            if (isValid) {
                message = "An email has been sent to <b>" + email + "</b>.";
            } else {
                message = "Sorry. We can not find the user ID: <b>" + email + "</b>.";
            }
        } catch (Exception exc) {
            message = "Error sending email to: " + email + ". Make sure this is a valid email address.";
            logger.log(Level.SEVERE, message, exc);

            isError = true;
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/forgot_password_confirmation.jsp");
        request.setAttribute("message", message);
        request.setAttribute("isError", isError);

        dispatcher.forward(request, response);
    }


}
