package com.gsihealth.dashboard.server.webservice.mypatientlist;

import com.gsihealth.dashboard.client.service.MyPatientListService;
import com.gsihealth.dashboard.entity.dto.MyPatientInfoDTO;
import com.gsihealth.dashboard.server.WebConstants;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

/**
 *
 * @author Chad.Darby
 */
@WebService(serviceName = "MyPatientListWebService")
public class MyPatientListWebService {

    private Logger logger = Logger.getLogger(getClass().getName());
    
    @Resource
    WebServiceContext webServiceContext;

    @WebMethod
    public void add(long userid, long patientId, long communityId) {

        try {
            MyPatientListService myPatientListService = getMyPatientListService();

            MyPatientInfoDTO myPatientInfo = new MyPatientInfoDTO(userid, patientId, communityId);

            myPatientListService.add(myPatientInfo);
        } catch (Exception exc) {
            String message = "Error adding patient to My Patient List";
            logger.log(Level.WARNING, message, exc);
            throw new RuntimeException(message);
        }
    }

    @WebMethod
    public boolean isAlreadyOnList(long userid, long patientId, long communityId) {

        try {
            MyPatientListService myPatientListService = getMyPatientListService();

            return myPatientListService.isAlreadyOnList(userid, patientId, communityId);
            
        } catch (Exception exc) {
            String message = "Error calling: isAlreadyOnList";
            logger.log(Level.WARNING, message, exc);
            throw new RuntimeException(message);
        }
    }

    @WebMethod
    public boolean isListFull(long userid, long communityId) {
        try {
            MyPatientListService myPatientListService = getMyPatientListService();

            return myPatientListService.isListFull(userid, communityId);
            
        } catch (Exception exc) {
            String message = "Error calling: isListFull";
            logger.log(Level.WARNING, message, exc);
            throw new RuntimeException(message);
        }
    }

    protected MyPatientListService getMyPatientListService() {
        ServletContext application = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
        MyPatientListService myPatientListService = (MyPatientListService) application.getAttribute(WebConstants.MY_PATIENT_LIST_SERVICE_KEY);
        
        return myPatientListService;
    }
}
