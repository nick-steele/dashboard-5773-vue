package com.gsihealth.dashboard.server.loader.webservice;

import java.util.HashMap;

/**
 * Simple class that wraps a hashmap. When using JAX-WS, you can't return a
 * hashmap directly, you must wrap it.
 * 
 * For details, see StackOverFlow: http://stackoverflow.com/questions/13782797/jax-ws-exception-return-is-not-a-valid-property
 * 
 * @author Chad Darby
 */
public class HashMapWrapper implements java.io.Serializable {

    private HashMap<String, String> theMap;

    public HashMapWrapper(HashMap<String, String> theMap) {
        this.theMap = theMap;
    }

    public HashMap<String, String> getTheMap() {
        return theMap;
    }

    public void setTheMap(HashMap<String, String> theMap) {
        this.theMap = theMap;
    }
    
}