package com.gsihealth.dashboard.server.loader.webservice;

import com.gsihealth.dashboard.client.service.EnrollmentService;
import com.gsihealth.dashboard.client.service.ManagePatientProgramService;
import com.gsihealth.dashboard.common.Constants;
import com.gsihealth.dashboard.common.ReferenceData;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.*;
import com.gsihealth.dashboard.entity.dto.insurance.PayerClassDTO;
import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import com.gsihealth.dashboard.server.CommunityContexts;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.common.DuplicatePatientException;
import com.gsihealth.dashboard.server.common.NewPatientLoadResult;
import com.gsihealth.dashboard.server.common.PatientLoadResult;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.dao.*;
import com.gsihealth.dashboard.server.hisp.HispClient;
import com.gsihealth.dashboard.server.loader.webservice.dto.PatientStatusHolder;
import com.gsihealth.dashboard.server.loader.webservice.dto.ProgramNameHolder;
import com.gsihealth.dashboard.server.partner.PixClient;
import com.gsihealth.dashboard.server.partner.PixClientImpl;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import com.gsihealth.dashboard.server.service.EnrollmentServiceHelper;
import com.gsihealth.dashboard.server.service.integration.PatientUserNotification;
import com.gsihealth.dashboard.server.service.integration.UserService;
import com.gsihealth.dashboard.server.util.OrgUtils;
import com.gsihealth.entity.*;
import com.gsihealth.entity.insurance.PayerClass;
import com.gsihealth.entity.insurance.PayerPlan;
import com.gsihealth.patientmdminterface.entity.DuplicateCheckProcessResult;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import com.gsihealth.patientmdminterface.util.PatientUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrTokenizer;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang.time.StopWatch;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Chad Darby
 */
@WebService
public class LoaderWebService {

    private static final int PATIENT_LOADER_USER_ID = 999999;
    private Logger logger = Logger.getLogger(getClass().getName());
    private AuditDAO auditDAO;
    private CommunityDAO communityDAO;
    private CommunityCareteamDAO careteamDAO;
    private NotificationAuditDAO notificationAuditDAO;
    private OrganizationPatientConsentDAO organizationPatientConsentDAO;
    private OrganizationDAO organizationDAO;
    // private PatientEnrollmentDAOImpl patientEnrollmentDAO;
    private Map<Long, CommunityContexts> communityContexts;
    private UserPatientConsentDAO userPatientConsentDAO;
    private UserDAO userDAO;
    private PayerPlanDAO payerPlanDAO;
    private PatientEnrollmentHistoryDAO patientEnrollmentHistoryDAO;
    private DisenrollmentReasonDAO disenrollmentReasonDAO;
    private PixClient pixClient;
    private EnrollmentServiceHelper enrollmentServiceHelper;
    private List<String> clientAddressWhiteList;
    private ProgramNameHistoryDAO programNameHistoryDAO;
    private ReferenceDataDAO referenceDataDAO;
    private AccessLevelDAO accessLevelDAO;
    private SSOUserDAO sSOUserDAO;
    private UserService userService;
    private UserCommunityOrganizationDAO userCommunityOrganizationDAO;
    private CareManagementOrganizationHistoryDAO careManagementOrganizationHistoryDAO;
    private AcuityScoreHistoryDAO acuityScoreHistoryDAO;
    private CommunityOrganizationDAO commuityOrganizationDAO;
    private ConfigurationDAO configurationDAO;
    private PatientUserNotification patientUserNotification;
    private ConsentHistoryDAO consentHistoryDAO;
    private DateFormat timeFormatter = new SimpleDateFormat("hh:mm");
    private DateFormat ampmFormatter = new SimpleDateFormat("a");

    @Resource
    WebServiceContext webServiceContext;
    private boolean daosSetup;

    public LoaderWebService() {
    }

    @WebMethod
    public List<CommunityInfo> getCommunities() {

        setupDaos();

        // if (isDisabled()) {
        //     throw new IllegalArgumentException("disabled");
        // }

        List<CommunityInfo> commInfoList = new ArrayList<CommunityInfo>();

        List<Community> comms = communityDAO.findEntities();

        for (Community temp : comms) {
            commInfoList.add(new CommunityInfo(temp.getCommunityName(), temp.getCommunityId()));
        }

        return commInfoList;
    }

    private String getCommunityOid(Long communityId) {
        this.setupDaos(); // if it's already setup, won't run again
        Community community = this.communityDAO.getCommunityById(communityId);
        String communityOid = null;
        if (community != null) {
            communityOid = community.getCommunityOid();
        }
        logger.fine("community oid for communityId " + communityId + " = " + communityOid);
        return communityOid;
    }

    /**
     * Returns organization names and ids
     *
     * @param token
     */
    @WebMethod
    public List<OrganizationDTO> getOrganizations(String token, long communityId) {

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        List<OrganizationDTO> dtos = new ArrayList<OrganizationDTO>();


        try {
            // get orgs
            List<OrganizationCwd> orgs = organizationDAO.findOrganizationEntities(communityId);

            // Convert to DTOs, fully populated
            dtos = OrgUtils.convertToDTOs(orgs, communityId);
        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error loading orgs", exc);
            throw new RuntimeException("Error loading orgs");
        }

        return dtos;
    }

    /**
     * Returns patient source ids
     *
     * @param token
     */
    @WebMethod
    public List<PatientSourceDTO> getPatientSourceIds(String token, long communityId) {

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        List<PatientSourceDTO> dtos = new ArrayList<PatientSourceDTO>();

        try {
            // get orgs
            List<PatientSource> patientSourceList = this.getPatientEnrollmentDAO(communityId).getPatientSourceList(communityId);

            // Convert to DTOs, fully populated
            dtos = this.convertPatientSource(patientSourceList);

        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error loading patient source", exc);
            throw new RuntimeException("Error loading patient source.");
        }

        return dtos;
    }

    /**
     * Returns payer class names and ids
     *
     * @param token
     * @return Map
     */
    @WebMethod
    public List<PayerClassDTO> getPayerClasses(String token, long communityId) {

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        List<PayerClassDTO> payerClassIds = new ArrayList<PayerClassDTO>();

        try {
            // get payer classes
            List<PayerClass> payerClassList = payerPlanDAO.getPayerClassEntities(communityId);

            // convert to DTOs
            payerClassIds = convert(payerClassList);
        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error loading payer classes", exc);
            throw new RuntimeException("Error loading payer classes");
        }

        return payerClassIds;
    }

    @WebMethod
    public boolean isValidPatientId(String token, long communityId, long patientId) {

        boolean valid = false;

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        try {
            // check the patient id
            valid = this.getPatientEnrollmentDAO(communityId).isValidPatientId(communityId, patientId);

            return valid;
        } catch (Exception exc) {
            logger.log(Level.WARNING, "Invalid patient ID: " + patientId, exc);
            throw new RuntimeException("Invalid patient ID: " + patientId);
        }

    }

    /**
     * Returns payer plan names and ids
     *
     * @param token
     * @return Map
     */
    @WebMethod
    public List<PayerPlanDTO> getPayerPlans(String token, long communityId) {

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        List<PayerPlanDTO> payerPlanIds = new ArrayList<PayerPlanDTO>();

        try {
            // get payer plans
            List<PayerPlan> payerPlanList = payerPlanDAO.getPayerPlanEntities(communityId);

            // convert to DTOs
            payerPlanIds = convertPayerPlans(payerPlanList);
        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error loading payer plans", exc);
            throw new RuntimeException("Error loading payer plans");
        }

        return payerPlanIds;
    }

    /**
     * Return postal codes
     *
     * @param token
     * @param communityId
     * @return
     */
    @WebMethod
    public String[] getPostalStateCodes(String token, long communityId) {

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        String[] postalStateCodes = null;

        try {
            // get payer plans
            postalStateCodes = referenceDataDAO.getPostalStateCodes(communityId);

        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error loading postal state codes", exc);
            throw new RuntimeException("Error loading postal state codes");
        }

        return postalStateCodes;
    }

    /**
     * Return program names
     *
     * @param token
     * @param communityId
     * @return
     */
    @WebMethod
    public String[] getProgramNames(String token, long communityId) {

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        String[] programNames = null;

        try {
            // get program names
            programNames = referenceDataDAO.getProgramNames(communityId);

        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error loading program names", exc);
            throw new RuntimeException("Error loading program names");
        }

        return programNames;
    }

    /**
     * Return ethnic
     *
     * @param token
     * @param communityId
     * @return
     */
    @WebMethod
    public String[] getEthnicCodes(String token, long communityId) {

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        String[] ethnicCodes = null;

        try {
            // get ethnic codes
            ethnicCodes = referenceDataDAO.getEthnics(communityId).values().toArray(new String[0]);

        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error loading ethnic codes", exc);
            throw new RuntimeException("Error loading ethnic codes");
        }

        return ethnicCodes;
    }

    /**
     * Return races
     *
     * @param token
     * @param communityId
     * @return
     */
    @WebMethod
    public String[] getRaceCodes(String token, long communityId) {

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        String[] raceCodes = null;

        try {
            // get program names
            raceCodes = referenceDataDAO.getRaces(communityId).values().toArray(new String[0]);

        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error loading race codes", exc);
            throw new RuntimeException("Error loading race codes");
        }

        return raceCodes;
    }

    /**
     * Return em relationship codes
     *
     * @param token
     * @param communityId
     * @return
     */
    @WebMethod
    public String[] getEmergencyRelationshipCodes(String token, long communityId) {

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        String[] codes = null;

        try {
            List<PatientRelationshipCode> relationshipList = referenceDataDAO.findPatientRelationshipEntities(communityId);

            codes = new String[relationshipList.size()];

            int counter = 0;
            for (PatientRelationshipCode tempPatientRelationshipCode : relationshipList) {
                codes[counter] = Long.toString(tempPatientRelationshipCode.getPatientRelationshipCodePK().getId());
                counter++;
            }

        } catch (Exception exc) {
            String message = "Error loading patient relationship codes";
            logger.log(Level.WARNING, message, exc);
            throw new RuntimeException(message);
        }

        return codes;
    }

    /**
     * Return language codes
     *
     * @param token
     * @param communityId
     * @return
     */
    @WebMethod
    public String[] getLanguageCodes(String token, long communityId) {

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        String[] languageCodes = null;

        try {
            // get program names
            languageCodes = referenceDataDAO.getPreferredLanguages(communityId).values().toArray(new String[0]);

        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error loading language codes", exc);
            throw new RuntimeException("Error loading language codes");
        }

        return languageCodes;
    }

    /**
     * Get gender codes
     *
     * @param token
     * @param communityId
     * @return
     */
    @WebMethod
    public String[] getGenderCodes(String token, long communityId) {

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        String[] genderCodes = null;

        try {
            // get program names
            genderCodes = referenceDataDAO.getGenders(communityId).values().toArray(new String[0]);

        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error loading gender codes", exc);
            throw new RuntimeException("Error loading gender codes");
        }

        return genderCodes;
    }

    /**
     * Return preferred lang codes
     *
     * @param token
     * @param communityId
     * @return
     */
    @WebMethod
    public List<ProgramNameHolder> getPreferredLanguageCodes(String token, long communityId) {

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        List<ProgramNameHolder> programNameHolders = new ArrayList<ProgramNameHolder>();

        try {
            // get program names
            List<ProgramName> programNames = referenceDataDAO.findProgramNameEntities(communityId);

            // now build a list of holder objects
            programNameHolders = buildProgramNameHolders(programNames);

        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error loading preferred language codes", exc);
            throw new RuntimeException("Error loading preferred language codes");
        }

        return programNameHolders;
    }

    /**
     * Return health home names
     *
     * @param token
     * @param communityId
     * @return
     */
    @WebMethod
    public List<ProgramHealthHomeDTO> getHealthHomeNames(String token, long communityId) {

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        List<ProgramHealthHomeDTO> healthHomeNames = null;

        try {
            // get program health home names
            List<ProgramHealthHome> data = referenceDataDAO.findProgramHealthHomeEntities(communityId);

            // convert names to string array
            healthHomeNames = convertHealthHome(data);

        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error loading health home names", exc);
            throw new RuntimeException("Error loading health home names");
        }

        return healthHomeNames;
    }

    /**
     * Return program names
     *
     * @param token
     * @param communityId
     * @return
     */
    @WebMethod
    public List<ProgramNameHolder> getProgramNameHolders(String token, long communityId) {

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        List<ProgramNameHolder> programNameHolders = new ArrayList<ProgramNameHolder>();

        try {
            // get program names
            List<ProgramName> programNames = referenceDataDAO.findProgramNameEntities(communityId);

            // now build a list of holder objects
            programNameHolders = buildProgramNameHolders(programNames);

        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error loading program names holders", exc);
            throw new RuntimeException("Error loading program names holders");
        }

        return programNameHolders;
    }

    @WebMethod
    public List<PatientStatusHolder> getPatientStatusHolders(String token, long communityId) {

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        List<PatientStatusHolder> patientStatusHolders = new ArrayList<PatientStatusHolder>();

        try {
            // get patient statuses
            List<PatientStatus> patientStatuses = referenceDataDAO.findPatientStatusEntities(communityId);

            // now build a list of holder objects
            patientStatusHolders = buildPatientStatusHolders(patientStatuses);

        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error loading patient status holders", exc);
            throw new RuntimeException("Error loading patient status holders");
        }

        return patientStatusHolders;
    }

    @WebMethod
    public String[] getTerminationReasons(String token, long communityId) {

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        String[] terminateReasons = null;

        try {
            // get termination reasons
            terminateReasons = referenceDataDAO.getTerminationReason(communityId);

        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error loading termination reasons", exc);
            throw new RuntimeException("Error loading termination reasons");
        }

        return terminateReasons;
    }

    protected void performSecurityTest(long communityId) throws IllegalArgumentException {

        String clientAddress = getClientAddress();
        logger.info("PatientLoader web service request from, Client Address=" + clientAddress);

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        boolean whiteListEnabled = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "patientloader.webservice.client.address.whitelist.enabled", "true"));

        if (whiteListEnabled) {
            String clientAddressWhiteListStr = configurationDAO.getProperty(communityId, "patientloader.webservice.client.address.whitelist");
            clientAddressWhiteList = buildClientAddressWhiteList(clientAddressWhiteListStr);

            // check ip address
            if (isNotValidClientAddress(clientAddress)) {
                throw new IllegalArgumentException("Invalid client address: PatientLoader web service rejected call due to invalid address. client address=" + clientAddress);
            }
        }
    }

    protected String getClientAddress() {
        String clientAddress = null;

        MessageContext messageContext = webServiceContext.getMessageContext();
        HttpServletRequest request = (HttpServletRequest) messageContext.get(MessageContext.SERVLET_REQUEST);

        logger.info("dumping header names/values");
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String tempHeaderName = headerNames.nextElement();
            logger.info(tempHeaderName + ": " + request.getHeader(tempHeaderName));
        }

        logger.info("dumping parameter names/values");
        Enumeration<String> paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String tempParamName = paramNames.nextElement();
            logger.info(tempParamName + ": " + request.getParameter(tempParamName));
        }

        clientAddress = request.getRemoteAddr();
        logger.info("clientAddress=" + clientAddress);

        return clientAddress;
    }

    @WebMethod
    public PatientLoadResult loadPatient(PersonDTO person, String token, boolean runInBackground, long communityId) throws PatientLoadException {
        return doLoadPatient(person, token, runInBackground, communityId, true, null);
    }

    @WebMethod
    public NewPatientLoadResult loadPatientWithWrapperExp(PersonDTO person, String token, boolean runInBackground, long
            communityId, boolean createSimilarDup, String email) {
        NewPatientLoadResult newPatientLoadResult = new NewPatientLoadResult();
        try{
            PatientLoadResult patientLoadResult = doLoadPatient(person, token, runInBackground, communityId,
                    createSimilarDup, email);
            newPatientLoadResult.setPatientId(patientLoadResult.getPatientId());
            newPatientLoadResult.setCommunityId(patientLoadResult.getCommunityId());
        }
        catch (PatientLoadException ple) {
            logger.log(Level.WARNING, ple.getMessage(), ple);
            newPatientLoadResult.setIsError(true);
            newPatientLoadResult.setErrorMessage(ple.getMessage());
        }
        return newPatientLoadResult;
    }

    private PatientLoadResult doLoadPatient (PersonDTO person, String token, boolean runInBackground, long
            communityId, boolean createSimilarDup, String email) throws PatientLoadException {
        StopWatch totalStopWatch = new StopWatch();
        totalStopWatch.start();

        StopWatch tempStopWatch = new StopWatch();
        tempStopWatch.start();

        setupDaos();

        tempStopWatch.stop();
        logger.info("[profiling], LoaderWebService: setupDaos, " + (tempStopWatch.getTime() / 1000.0) + ", secs");

        tempStopWatch.reset();
        tempStopWatch.start();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        tempStopWatch.stop();
        logger.info("[profiling], LoaderWebService: isDisabled, " + (tempStopWatch.getTime() / 1000.0) + ", secs");

        tempStopWatch.reset();
        tempStopWatch.start();

        performSecurityTest(communityId);

        tempStopWatch.stop();
        logger.info("[profiling], LoaderWebService: performSecurityTest, " + (tempStopWatch.getTime() / 1000.0) + ", secs");

        if (person == null) {
            throw new IllegalArgumentException("Invalid data");
        }

        // now do your work
        try {
            User loadedBy = findUserByEmail(email, communityId);
            person = verifyAndUpdate(person, loadedBy);

            // 4728: fixed null patient relationship id
            LoginResult loginResult = buildLoginResult(communityId, loadedBy);


            tempStopWatch.reset();
            tempStopWatch.start();

            DuplicateCheckProcessResult result = enrollmentServiceHelper.checkForDuplicates(person.getSimplePerson(),
                    communityId);
            PatientLoadResult patientLoadResult = null;
            if (DuplicateCheckProcessResult.ProcessDecision.NONE_EXISTS.equals(result.getProcessDecision())) {
                patientLoadResult = enrollmentServiceHelper.addPatientHelper(person, Long.MIN_VALUE, loginResult, runInBackground);
            } else if (DuplicateCheckProcessResult.ProcessDecision.SIMILAR_EXISTS.equals(result.getProcessDecision()) &&
                    createSimilarDup) {
                patientLoadResult = enrollmentServiceHelper.addPatientHelper(person, Long.MIN_VALUE, loginResult, runInBackground);
            } else if ((DuplicateCheckProcessResult.ProcessDecision.SIMILAR_EXISTS.equals(result.getProcessDecision())) &&
                    !createSimilarDup) {
                throw new PatientLoadException("similar patient with duplicate " + createSimilarDup);
            } else if (DuplicateCheckProcessResult.ProcessDecision.ALREADY_EXISTS.equals(result.getProcessDecision())) {
                throw new PatientLoadException("exist patient");
            }

            tempStopWatch.stop();
            logger.info("[profiling], LoaderWebService: calling enrollmentServiceHelper.addPatientHelper, " + (tempStopWatch.getTime() / 1000.0) + ", secs");

            totalStopWatch.stop();
            logger.info("[profiling], LoaderWebService: TOTAL - loadPatient, " + (totalStopWatch.getTime() / 1000.0) + ", secs");

            return patientLoadResult;
        } catch (Exception exc) {
            throw new PatientLoadException("Error loading patient: " + exc.getMessage(), exc);
        }
    }

    @WebMethod
    public NewPatientLoadResult addPatientProgramWithWrapperExp(PatientProgramDTO patientProgramDTO, PersonDTO
            person, String token, long communityId, String email) {
        NewPatientLoadResult newPatientLoadResult = new NewPatientLoadResult();
        try {
            doAddPatientProgram(patientProgramDTO, person, token, communityId, email);
        }
        catch (PatientLoadException ple) {
            logger.log(Level.WARNING, ple.getMessage(), ple);
            newPatientLoadResult.setErrorMessage(ple.getMessage());
            newPatientLoadResult.setIsError(true);
        }
        return newPatientLoadResult;
    }

    @WebMethod
    public void addPatientProgram(PatientProgramDTO patientProgramDTO, PersonDTO
            person, String token, long communityId) throws PatientLoadException {
        doAddPatientProgram(patientProgramDTO, person, token, communityId, null);
    }

    private void doAddPatientProgram(PatientProgramDTO patientProgramDTO, PersonDTO person, String token, long
            communityId, String email) throws PatientLoadException {

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        if (person == null) {
            throw new IllegalArgumentException("Invalid data");
        }

        setupDaos();

        // now do your work
        try {
            User loadedBy = findUserByEmail(email, communityId);

            LoginResult loginResult = buildLoginResult(communityId, loadedBy);
            // get the program service
            ManagePatientProgramService managePatientProgramService = getManagePatientProgramService();

            managePatientProgramService.addPatientProgramFromPatientLoader(loginResult, patientProgramDTO, person);

            logger.info("added patient program");

        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error adding patient program", exc);
            throw new PatientLoadException(exc.getMessage());
        }
    }

    @WebMethod
    public void updatePatient(PersonDTO personDTO, String token, boolean runInBackground, long communityId) throws PatientLoadException {
        doUpdatePatient(personDTO, token, runInBackground, communityId, null);
    }

    @WebMethod
    public NewPatientLoadResult updatePatientWithWrapperExp(PersonDTO personDTO, String token, boolean runInBackground, long
            communityId, String email) {
        NewPatientLoadResult newPatientLoadResult = new NewPatientLoadResult();
        try {
            doUpdatePatient(personDTO, token, runInBackground, communityId, email);
        }
        catch (PatientLoadException ple) {
            logger.log(Level.WARNING, ple.getMessage(), ple);
            newPatientLoadResult.setIsError(true);
            newPatientLoadResult.setErrorMessage(ple.getMessage());
        }
        return newPatientLoadResult;
    }

    private void doUpdatePatient(PersonDTO personDTO, String token, boolean runInBackground, long communityId, String
            email)
            throws PatientLoadException {


        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(communityId);

        if (personDTO == null) {
            throw new IllegalArgumentException("Invalid data");
        }

        setupDaos();

        // now do your work
        try {
            User loadedBy = findUserByEmail(email, communityId);
            personDTO = verifyAndUpdate(personDTO, loadedBy);

            LoginResult loginResult = buildLoginResult(communityId, loadedBy);
            // get the enrollment service
            EnrollmentService enrollmentService = getEnrollmentService();

            // get the patient from DB first
            long patientId = personDTO.getPatientEnrollment().getPatientId();
            logger.info("4733: FIRST FROM DB - patient id = " + patientId);

            PatientCommunityEnrollment patientEnrollmentFromDB = this.getPatientEnrollmentDAO(communityId).findPatientEnrollment(patientId);

            logger.info("4733: FIRST FROM DB - patientEnrollmentFromDB.getPatientEmergencyContact() = " + patientEnrollmentFromDB.getPatientEmergencyContact());

            long oldOrganizationId = patientEnrollmentFromDB.getOrgId();

            String patientEuid = patientEnrollmentFromDB.getPatientEuid();
            SimplePerson sp = this.getPatientManager(communityId).getPatient(patientEuid);
            Person personFromDB = new Person(sp);

            personFromDB.setPatientCommunityEnrollment(patientEnrollmentFromDB);

            // convert to DTOs
            Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
            PersonDTO personDTOFromDB = mapper.map(personFromDB, PersonDTO.class);

            // now set the patient relationship
            setPatientRelationship(communityId, personFromDB, personDTOFromDB);

            logger.info("4733: AFTER CONVERSION TO DTO - personDTOFromDB.getPatientEnrollment().getPatientEmergencyContact() = " + personDTOFromDB.getPatientEnrollment().getPatientEmergencyContact());
            logger.info("4733: AFTER CONVERSION TO DTO - personDTOFromDB.getPatientEnrollment().getPatientEmergencyContact().getPatientRelationshipId()" + personDTOFromDB.getPatientEnrollment().getPatientEmergencyContact().getPatientRelationshipId());

            // set consent date time
            populateConsentTime(personDTOFromDB);

            // set non empty fields
            setNonEmptyFields(personDTO, personDTOFromDB);

            // set the boolean flags
            boolean checkEnableCareteam = false;
            boolean checkForDuplicate = false;
            String oldProgramLevelConsentStatus = patientEnrollmentFromDB.getProgramLevelConsentStatus();
            String oldEnrollmentStatus = patientEnrollmentFromDB.getStatus();

            String newCareTeamId = personFromDB.getCareteam();
            String oldCareTeamId = newCareTeamId;

            String oldAcuityScore = patientEnrollmentFromDB.getAcuityScore();

            boolean checkPatientUserUpdate = true;

            // if pereson info has changed from, check for duplicate patient
            if (hasPersonInfoFromClientChanged(personDTO)) {
                checkForDuplicatePatient(personDTOFromDB, communityId);
            }

            // if SSN is not blank, then check for duplicate SSN
            if (StringUtils.isNotBlank(personDTO.getSsn())) {
                checkForDuplicateSsn(personDTOFromDB, communityId);
            }

            PatientEnrollmentDTO patientEnrollmentFromClient = personDTO.getPatientEnrollment();

            // if Primary Medicaid/Medicare ID is not blank then check for duplicate
            if (StringUtils.isNotBlank(patientEnrollmentFromClient.getPrimaryPayerMedicaidMedicareId())) {
                checkForDuplicatePrimaryMedicaidId(personDTOFromDB, communityId);
            }

            // if Secondary Medicaid/Medicare ID is not blank then check for duplicate
            if (StringUtils.isNotBlank(patientEnrollmentFromClient.getSecondaryPayerMedicaidMedicareId())) {
                checkForDuplicateSecondaryMedicaidId(personDTOFromDB, communityId);
            }

            boolean checkForDuplicateSsn = false;
            boolean primaryMedicaidcareIdBln = false;
            boolean secondaryMedicaidcareIdBln = false;

            enrollmentService.updatePatientFromPatientLoader(loginResult, personDTOFromDB,
                    checkEnableCareteam, checkForDuplicate, checkForDuplicateSsn,
                    primaryMedicaidcareIdBln, secondaryMedicaidcareIdBln,
                    oldProgramLevelConsentStatus, oldEnrollmentStatus,
                    newCareTeamId, oldCareTeamId,
                    oldOrganizationId,
                    checkPatientUserUpdate, oldAcuityScore);

            logger.info("updated patient");

        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error loading patient", exc);
            throw new PatientLoadException(exc.getMessage());
        }
    }

    private LoginResult buildLoginResult(long communityId, User loadedBy) {
        LoginResult theLoginResult = new LoginResult();

        if (loadedBy != null) {
            theLoginResult.setFirstName(loadedBy.getFirstName());
            theLoginResult.setLastName(loadedBy.getLastName());
            theLoginResult.setEmail(loadedBy.getEmail());
            theLoginResult.setUserId(loadedBy.getUserId());
        } else {
            theLoginResult.setAccessLevel("Super User");
            theLoginResult.setUserLevel("Administrator");
            theLoginResult.setUserLevelId(8);
            theLoginResult.setFirstName("Admin");
            theLoginResult.setLastName("Admin Patient Loader");
            theLoginResult.setEmail("patientloader@gsihealth.com");
            theLoginResult.setUserId(PATIENT_LOADER_USER_ID);
            theLoginResult.setOrganizationName("MMC");
        }

        theLoginResult.setCommunityId(communityId);
        String communityOid = this.getCommunityOid(communityId);
        theLoginResult.setCommunityOid(communityOid);

        return theLoginResult;
    }

    private void setupDaos() {

        // if daos setup already just return
        if (daosSetup) {
            return;
        }

        ServletContext application = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);

        auditDAO = (AuditDAO) application.getAttribute(WebConstants.AUDIT_DAO_KEY);

        careteamDAO = (CommunityCareteamDAO) application.getAttribute(WebConstants.COMMUNITY_CARE_TEAM_DAO_KEY);

        notificationAuditDAO = (NotificationAuditDAO) application.getAttribute(WebConstants.NOTIFICATION_AUDIT_DAO_KEY);

        organizationPatientConsentDAO = (OrganizationPatientConsentDAO) application.getAttribute(WebConstants.ORGANIZATION_PATIENT_CONSENT_DAO_KEY);

        organizationDAO = (OrganizationDAO) application.getAttribute(WebConstants.ORGANIZATION_DAO_KEY);

        userPatientConsentDAO = (UserPatientConsentDAO) application.getAttribute(WebConstants.USER_PATIENT_CONSENT_DAO_KEY);

        userDAO = (UserDAO) application.getAttribute(WebConstants.USER_DAO_KEY);

        payerPlanDAO = (PayerPlanDAO) application.getAttribute(WebConstants.PAYER_PLAN_DAO_KEY);

        patientEnrollmentHistoryDAO = (PatientEnrollmentHistoryDAO) application.getAttribute(WebConstants.PATIENT_ENROLLMENT_HISTORY_DAO_KEY);

        disenrollmentReasonDAO = (DisenrollmentReasonDAO) application.getAttribute(WebConstants.DISENROLLMENT_DAO_DEY);

        programNameHistoryDAO = (ProgramNameHistoryDAO) application.getAttribute(WebConstants.PROGRAM_NAME_HISTORY_DAO_KEY);

        communityDAO = (CommunityDAO) application.getAttribute(WebConstants.COMMUNITY_DAO_KEY);

        referenceDataDAO = (ReferenceDataDAO) application.getAttribute(WebConstants.REFERENCE_DATA_DAO_KEY);

        accessLevelDAO = (AccessLevelDAO) application.getAttribute(WebConstants.ACCESS_LEVEL_DAO_KEY);

        sSOUserDAO = (SSOUserDAO) application.getAttribute(WebConstants.SSO_USER_DAO_KEY);
        careManagementOrganizationHistoryDAO = (CareManagementOrganizationHistoryDAO) application.getAttribute(WebConstants.CARE_MANAGEMENT_ORGANIZATION_HISTORY_DAO_KEY);
        acuityScoreHistoryDAO = (AcuityScoreHistoryDAO) application.getAttribute(WebConstants.ACUITY_SCORE_HISTORY_DAO_KEY);
        userService = (UserService) application.getAttribute(WebConstants.USER_SERVICE_KEY);
        userCommunityOrganizationDAO = (UserCommunityOrganizationDAO) application.getAttribute(WebConstants.USER_COMMUNITY_ORGANIZATION_DAO_KEY);
        commuityOrganizationDAO = (CommunityOrganizationDAO) application.getAttribute(WebConstants.COMMUNITY_ORGANIZATION_DAO_KEY);
        configurationDAO = (ConfigurationDAO) application.getAttribute(WebConstants.CONFIGURATION_DAO_KEY);
        patientUserNotification = (PatientUserNotification) application.getAttribute(WebConstants.PATIENT_USER_NOTIFICATION);
        consentHistoryDAO = (ConsentHistoryDAO) application.getAttribute(WebConstants.CONSENT_HISTORY_DAO_KEY);
        //JIRA 629 - remove properties
        pixClient = new PixClientImpl();

        communityContexts =
                ((Map<Long, CommunityContexts>) application.getAttribute(WebConstants.COMMUNITY_SPECIFIC_DATA_KEY));

        //JIRA 629 - remove properties
        enrollmentServiceHelper = new EnrollmentServiceHelper(communityContexts,
                auditDAO, careteamDAO, notificationAuditDAO, organizationPatientConsentDAO, organizationDAO,
                userPatientConsentDAO, userDAO, patientEnrollmentHistoryDAO,
                disenrollmentReasonDAO, programNameHistoryDAO,
                accessLevelDAO, sSOUserDAO, userService,
                userCommunityOrganizationDAO, pixClient, patientUserNotification,
                careManagementOrganizationHistoryDAO, acuityScoreHistoryDAO, commuityOrganizationDAO, configurationDAO,
                consentHistoryDAO);
        daosSetup = true;
        logger.info("DAOs setup complete");
    }

    private boolean isNotValidClientAddress(String clientAddress) {

        logger.info("clientAddressWhiteList=" + clientAddressWhiteList);
        logger.info("clientAddress=" + clientAddress);
        logger.info("!clientAddressWhiteList.contains(clientAddress)=" + !clientAddressWhiteList.contains(clientAddress));

        return !clientAddressWhiteList.contains(clientAddress);
    }

    /**
     * Parses a comma delimited list of client ip addresses Converts it to a
     * list
     *
     * @param clientAddressWhiteListStr
     * @return
     */
    private List<String> buildClientAddressWhiteList(String clientAddressWhiteListStr) {
        clientAddressWhiteList = new ArrayList<String>();

        if (StringUtils.isNotBlank(clientAddressWhiteListStr)) {

            StrTokenizer tokenizer = new StrTokenizer(clientAddressWhiteListStr, ",");
            clientAddressWhiteList = tokenizer.getTokenList();
        }

        return clientAddressWhiteList;
    }

    private boolean isDisabled(long communityId) {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        String enabledStr = configurationDAO.getProperty(communityId, "patientloader.webservice.enabled", "false");

        boolean enabled = Boolean.parseBoolean(enabledStr);

        return !enabled;
    }

    private List<PayerClassDTO> convert(List<PayerClass> payerClassList) {
        List<PayerClassDTO> results = new ArrayList<PayerClassDTO>();

        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        for (PayerClass tempPayerClass : payerClassList) {
            PayerClassDTO payerClassDto = mapper.map(tempPayerClass, PayerClassDTO.class);

            results.add(payerClassDto);
        }

        return results;
    }

    private List<PatientSourceDTO> convertPatientSource(List<PatientSource> patientSourceList) {
        List<PatientSourceDTO> results = new ArrayList<PatientSourceDTO>();

        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        for (PatientSource temp : patientSourceList) {
            PatientSourceDTO payerClassDto = mapper.map(temp, PatientSourceDTO.class);

            results.add(payerClassDto);
        }

        return results;
    }

    private List<PayerPlanDTO> convertPayerPlans(List<PayerPlan> payerPlanList) {
        List<PayerPlanDTO> results = new ArrayList<PayerPlanDTO>();

        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        for (PayerPlan tempPayerPlan : payerPlanList) {
            PayerPlanDTO payerPlanDto = mapper.map(tempPayerPlan, PayerPlanDTO.class);

            results.add(payerPlanDto);
        }

        return results;
    }

    private List<ProgramHealthHomeDTO> convertHealthHome(List<ProgramHealthHome> data) {
        // convert to array list

        logger.info("health home entities = " + data);

        List<ProgramHealthHomeDTO> healthHomeNames = new ArrayList<ProgramHealthHomeDTO>();

        for (ProgramHealthHome tempProgramHealthHome : data) {
            String name = tempProgramHealthHome.getHealthHomeValue();
            Long id = tempProgramHealthHome.getProgramHealthHomePK().getId();

            logger.info("name=" + name + ",  id=" + id);
            ProgramHealthHomeDTO tempDTO = new ProgramHealthHomeDTO();
            tempDTO.setHealthHomeName(name);
            tempDTO.setId(id);

            healthHomeNames.add(tempDTO);
        }

        logger.info("healthHomeNames=" + healthHomeNames);
        return healthHomeNames;
    }

    private List<ProgramNameHolder> buildProgramNameHolders(List<ProgramName> programNames) {
        List<ProgramNameHolder> programNameHolders = new ArrayList<ProgramNameHolder>();

        // now loop thru program names and get the health home
        for (ProgramName tempProgramName : programNames) {
            long programNameId = tempProgramName.getProgramNamePK().getId();
            String programName = tempProgramName.getValue();
            String healthHome = tempProgramName.getHealthHomeId().getHealthHomeValue();
            long communityId = tempProgramName.getProgramNamePK().getCommunityId();
            boolean hhRequired = Boolean.parseBoolean(tempProgramName.getHhRequired());
            programNameHolders.add(new ProgramNameHolder(programNameId, programName, healthHome, hhRequired, communityId));
        }

        return programNameHolders;
    }

    /**
     * Get handle to enrollment service
     *
     * @return
     */
    private EnrollmentService getEnrollmentService() {
        ServletContext application = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
        EnrollmentService enrollmentService = (EnrollmentService) application.getAttribute(WebConstants.ENROLLMENT_SERVICE_KEY);

        return enrollmentService;
    }

    private ManagePatientProgramService getManagePatientProgramService() {
        ServletContext application = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
        ManagePatientProgramService managePatientProgramService = (ManagePatientProgramService) application.getAttribute(WebConstants.MANAGE_PATIENT_PROGRAM_SERVICE_KEY);

        return managePatientProgramService;
    }

    private void populateConsentTime(PersonDTO personDTO) {

        PatientEnrollmentDTO patientEnrollmentDTO = personDTO.getPatientEnrollment();

        Date consentDate = patientEnrollmentDTO.getConsentDateTime();

        if (consentDate != null) {
            String consentTime = timeFormatter.format(consentDate);
            personDTO.setConsentTime(consentTime);

            String ampm = ampmFormatter.format(consentDate);
            personDTO.setConsentAMPM(ampm);
        }
    }

    /**
     * Only set the non empty fields
     *
     * @param person
     * @param personDTOFromDB
     */
    private void setNonEmptyFields(PersonDTO person, PersonDTO personDTOFromDB) {

        // SPIRA: 4859 - UPDATE FN, LN, DOB, Gender

        // first name
        String firstName = person.getFirstName();
        if (StringUtils.isNotBlank(firstName)) {
            personDTOFromDB.setFirstName(firstName);
        }

        // last name
        String lastName = person.getLastName();
        if (StringUtils.isNotBlank(lastName)) {
            personDTOFromDB.setLastName(lastName);
        }

        // dob
        Date dob = person.getDateOfBirth();
        if (dob != null) {
            personDTOFromDB.setDateOfBirth(dob);
        }

        // gender
        String gender = person.getGender();
        if (StringUtils.isNotBlank(gender)) {
            personDTOFromDB.setGender(gender);
        }

        // middle name
        String middleName = person.getMiddleName();
        if (StringUtils.isNotBlank(middleName)) {
            personDTOFromDB.setMiddleName(middleName);
        }

        // address 1
        String address1 = person.getStreetAddress1();
        if (StringUtils.isNotBlank(address1)) {
            personDTOFromDB.setStreetAddress1(address1);
        }

        // address 2
        String address2 = person.getStreetAddress2();
        if (StringUtils.isNotBlank(address2)) {
            personDTOFromDB.setStreetAddress2(address2);
        }

        // city
        String city = person.getCity();
        if (StringUtils.isNotBlank(city)) {
            personDTOFromDB.setCity(city);
        }

        // state
        String state = person.getState();
        if (StringUtils.isNotBlank(state)) {
            personDTOFromDB.setState(state);
        }

        // zip
        String zip = person.getZipCode();
        if (StringUtils.isNotBlank(zip)) {
            personDTOFromDB.setZipCode(zip);
        }

        // phone
        String phone = person.getTelephone();
        if (StringUtils.isNotBlank(phone)) {
            personDTOFromDB.setTelephone(phone);
        }

        //JIRA 719 Batch Consent  - SPIRA 5693 fix for update mode
        String additionalConsentedOrganizations = person.getAdditionalConsentedOrganizations();
        if (StringUtils.isNotBlank(additionalConsentedOrganizations)) {
            personDTOFromDB.setAdditionalConsentedOrganizations(additionalConsentedOrganizations);
        }

        // Patient Enrollment
        PatientEnrollmentDTO patientEnrollment = person.getPatientEnrollment();
        PatientEnrollmentDTO patientEnrollmentDTOFromDB = personDTOFromDB.getPatientEnrollment();

        // primary email
        String email = patientEnrollment.getEmailAddress();
        if (StringUtils.isNotBlank(email)) {
            patientEnrollmentDTOFromDB.setEmailAddress(email);
        }

        // organization
        String orgName = patientEnrollment.getOrganizationName();
        if (StringUtils.isNotBlank(orgName)) {
            long orgId = patientEnrollment.getOrgId();

            patientEnrollmentDTOFromDB.setOrganizationName(orgName);
            patientEnrollmentDTOFromDB.setOrgId(orgId);
        }

        // primary payer class
        PayerClassDTO primaryPayerClass = patientEnrollment.getPrimaryPayerClass();
        if (primaryPayerClass != null && StringUtils.isNotBlank(primaryPayerClass.getName())) {
            patientEnrollmentDTOFromDB.setPrimaryPayerClass(primaryPayerClass);
        }

        // secondary payer class
        PayerClassDTO secondaryPayerClass = patientEnrollment.getSecondaryPayerClass();
        if (secondaryPayerClass != null && StringUtils.isNotBlank(secondaryPayerClass.getName())) {
            patientEnrollmentDTOFromDB.setSecondaryPayerClass(secondaryPayerClass);
        }

        // primary medicaid id
        String primaryMedicaidId = patientEnrollment.getPrimaryPayerMedicaidMedicareId();
        if (StringUtils.isNotBlank(primaryMedicaidId)) {
            patientEnrollmentDTOFromDB.setPrimaryPayerMedicaidMedicareId(primaryMedicaidId);
        }

        // secondary medicaid id
        String secondaryMedicaidId = patientEnrollment.getSecondaryPayerMedicaidMedicareId();
        if (StringUtils.isNotBlank(secondaryMedicaidId)) {
            patientEnrollmentDTOFromDB.setSecondaryPayerMedicaidMedicareId(secondaryMedicaidId);
        }

        // PLCS
        String plcs = patientEnrollment.getProgramLevelConsentStatus();
        if (StringUtils.isNotBlank(plcs)) {
            patientEnrollmentDTOFromDB.setProgramLevelConsentStatus(plcs);
            if (patientEnrollment.getConsentDateTime() != null) {
                patientEnrollmentDTOFromDB.setConsentDateTime(patientEnrollment.getConsentDateTime());
            }

            if (patientEnrollment.getConsentObtainedByUserId() > 0) {
                patientEnrollmentDTOFromDB.setConsentObtainedByUserId(patientEnrollment.getConsentObtainedByUserId());
            }
        }

        // Enrollment Status
        String enrollmentStatus = patientEnrollment.getStatus();
        if (StringUtils.isNotBlank(enrollmentStatus)) {
            patientEnrollmentDTOFromDB.setStatus(enrollmentStatus);
        }


// CM mrn
        String mrn = patientEnrollment.getPatientMrn();
        if (StringUtils.isNotBlank(mrn)) {
            patientEnrollmentDTOFromDB.setPatientMrn(mrn);
        }

        // health home
        String healthHome = patientEnrollment.getHealthHome();
        if (StringUtils.isNotBlank(healthHome)) {
            patientEnrollmentDTOFromDB.setHealthHome(healthHome);
        }

        // program
        String program = patientEnrollment.getProgramName();
        if (StringUtils.isNotBlank(program)) {
            patientEnrollmentDTOFromDB.setProgramName(program);
        }

        // ssn
        String ssn = person.getSsn();
        if (StringUtils.isNotBlank(ssn)) {
            personDTOFromDB.setSsn(ssn);
        }

        // ethnicity
        String ethnic = person.getEthnic();
        if (StringUtils.isNotBlank(ethnic)) {
            personDTOFromDB.setEthnic(ethnic);
        }

        // race
        String race = person.getRace();
        if (StringUtils.isNotBlank(race)) {
            personDTOFromDB.setRace(race);
        }

        // preferred language
        String language = person.getLanguage();
        if (StringUtils.isNotBlank(language)) {
            personDTOFromDB.setLanguage(language);
        }

        // Emergency Contact
        PatientEmergencyContactDTO patientEmergencyContact = patientEnrollment.getPatientEmergencyContact();
        PatientEmergencyContactDTO patientEmergencyContactDTOFromDB = patientEnrollmentDTOFromDB.getPatientEmergencyContact();

        // em first name
        String emFirstName = patientEmergencyContact.getFirstName();
        if (StringUtils.isNotBlank(emFirstName)) {
            patientEmergencyContactDTOFromDB.setFirstName(emFirstName);
        }

        // em middle name
        String emMiddleName = patientEmergencyContact.getMiddleName();
        if (StringUtils.isNotBlank(emMiddleName)) {
            patientEmergencyContactDTOFromDB.setMiddleName(emMiddleName);
        }

        // em last name
        String emLastName = patientEmergencyContact.getLastName();
        if (StringUtils.isNotBlank(emLastName)) {
            patientEmergencyContactDTOFromDB.setLastName(emLastName);
        }

        // em phone
        String emTelephone = patientEmergencyContact.getTelephone();
        if (StringUtils.isNotBlank(emTelephone)) {
            patientEmergencyContactDTOFromDB.setTelephone(emTelephone);
        }

        // em mail
        String emEmailAddress = patientEmergencyContact.getEmailAddress();
        if (StringUtils.isNotBlank(emEmailAddress)) {
            patientEmergencyContactDTOFromDB.setEmailAddress(emEmailAddress);
        }

        // em relationship
        Long emRelationshipId = patientEmergencyContact.getPatientRelationshipId();

        if (emRelationshipId != null && emRelationshipId > 0) {
            patientEmergencyContactDTOFromDB.setPatientRelationshipId(emRelationshipId);
        }

        // program effective date
        Date programEffectiveDate = patientEnrollment.getProgramNameEffectiveDate();
        if (programEffectiveDate != null) {
            patientEnrollmentDTOFromDB.setProgramNameEffectiveDate(programEffectiveDate);
        }

        // program end date
        Date programEndDate = patientEnrollment.getProgramEndDate();
        if (programEndDate != null) {
            patientEnrollmentDTOFromDB.setProgramEndDate(programEndDate);
        }

        // primary payer plan
        PayerPlanDTO primaryPayerPlan = patientEnrollment.getPrimaryPayerPlan();
        if (primaryPayerPlan != null) {
            patientEnrollmentDTOFromDB.setPrimaryPayerPlan(primaryPayerPlan);
        }

        // secondary payer plan
        PayerPlanDTO secondaryPayerPlan = patientEnrollment.getSecondaryPayerPlan();
        if (secondaryPayerPlan != null) {
            patientEnrollmentDTOFromDB.setSecondaryPayerPlan(secondaryPayerPlan);
        }

        long patientSourceId = patientEnrollment.getPatientSourceId();
        if (patientSourceId > 0) {
            patientEnrollmentDTOFromDB.setPatientSourceId(patientSourceId);
        }
        
        //county
        String county = person.getCounty();
        if (StringUtils.isNotBlank(county)) {
            personDTOFromDB.setCounty(county);
        }        
    }


    private List<PatientStatusHolder> buildPatientStatusHolders(List<PatientStatus> patientStatuses) {

        List<PatientStatusHolder> result = new ArrayList<PatientStatusHolder>();

        for (PatientStatus tempPatientStatus : patientStatuses) {
            long id = tempPatientStatus.getPatientStatusPK().getId();
            String value = tempPatientStatus.getValue();

            PatientStatusHolder tempHolder = new PatientStatusHolder(id, value);
            result.add(tempHolder);
        }

        return result;
    }

    protected void setPatientRelationship(long communityId, Person tempPerson, PersonDTO personDto) {
        //set relationship
        PatientEmergencyContact patientEmergencyContact = tempPerson.getPatientCommunityEnrollment().getPatientEmergencyContact();

        if (patientEmergencyContact != null) {

            if (patientEmergencyContact.getRelationship() != null) {

                PatientRelationshipCode patientRelationshipCode = findPatientRelationship(communityId, patientEmergencyContact.getRelationship());

                if ((patientRelationshipCode != null) && (StringUtils.isNotBlank(patientRelationshipCode.getValue()))) {

                    PatientEmergencyContactDTO patientEmergencyContactDto = personDto.getPatientEnrollment().getPatientEmergencyContact();
                    patientEmergencyContactDto.setPatientRelationshipDescription(patientRelationshipCode.getValue());
                    patientEmergencyContactDto.setPatientRelationshipId(patientRelationshipCode.getPatientRelationshipCodePK().getId());
                }
            }
        }
    }

    protected PatientRelationshipCode findPatientRelationship(long communityId, Long relationshipId) {

        Map<Long, String> patientRelationshipMap = getPatientRelationshipMap(communityId);

        PatientRelationshipCode patientRelationshipCode = new PatientRelationshipCode();
        PatientRelationshipCodePK patientRelationshipCodePK = new PatientRelationshipCodePK();
        patientRelationshipCodePK.setId(relationshipId);

        patientRelationshipCode.setPatientRelationshipCodePK(patientRelationshipCodePK);
        patientRelationshipCode.setValue(patientRelationshipMap.get(relationshipId));

        return patientRelationshipCode;
    }

    private Map<Long, String> getPatientRelationshipMap(Long communityId) {

        ServletContext application = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);

        // get reference data map of all community ids
        Map<Long, ReferenceData> referenceDataMap = ((Map<Long, ReferenceData>) application.getAttribute(WebConstants.REFERENCE_DATA_KEY));

        // get the reference data for this user's community id
        ReferenceData referenceData = (ReferenceData) referenceDataMap.get(communityId);

        return referenceData.getPatientRelationship();
    }

    protected PatientEnrollmentDAO getPatientEnrollmentDAO(Long communityId) {
        return communityContexts.get(communityId).getPatientEnrollmentDAO();
    }

    protected PatientManager getPatientManager(Long communityId) {
        return communityContexts.get(communityId).getPatientManager();
    }

    protected HispClient getHispClient(Long communityId) {
        return communityContexts.get(communityId).getHispClient();
    }

    private void checkForDuplicateSsn(PersonDTO thePerson, long communityId) throws DuplicatePatientException, Exception {

        String euid = thePerson.getEuid();
        logger.info("euid = " + euid);

        if (!StringUtils.isBlank(thePerson.getSsn())) {

            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setSsn(thePerson.getSsn());
            List<SimplePerson> personsList = getPatientManager(communityId).findPatient(searchCriteria);

            for (SimplePerson tempPerson : personsList) {

                // make sure our ID is not there
                logger.info("euid = " + euid);
                logger.info("tempPerson.getEuid() = " + tempPerson.getPatientEuid());

                if (StringUtils.equals(euid, tempPerson.getPatientEuid())) {
                    continue;
                } else {
                    String duplicatePatientEuid = tempPerson.getPatientEuid();
                    String ssnMsg = getSSNMessage(duplicatePatientEuid, communityId);

                    throw new DuplicatePatientException(ssnMsg);
                }
            }
        }
    }

    private String getSSNMessage(String duplicatePatientEUID, long communityId) {
        String ssnMsg = null;

        if (duplicatePatientEUID != null) {
            PatientCommunityEnrollment duplicateEnrollment = getPatientEnrollmentDAO(communityId).findPatientEnrollment(duplicatePatientEUID);

            if (duplicateEnrollment != null) {
                Long orgId = duplicateEnrollment.getOrgId();
                String patientOrg = organizationDAO.findOrganization(orgId, communityId).getOrganizationCwdName();

                String status = duplicateEnrollment.getStatus();
                ssnMsg = Constants.SSN_MSG + patientOrg + Constants.LASTMSG + status + ".";
            }
        }

        return ssnMsg;
    }

    private void checkForDuplicatePrimaryMedicaidId(PersonDTO personDTOFromDB, long communityId) throws Exception {

        // get primary payer class and medicaid id
        PatientEnrollmentDTO patientEnrollment = personDTOFromDB.getPatientEnrollment();
        String euid = patientEnrollment.getPatientEuid();

        String medicaidId = patientEnrollment.getPrimaryPayerMedicaidMedicareId();
        long payerClassId = patientEnrollment.getPrimaryPayerClass().getId();

        List<PatientCommunityEnrollment> resultsFromDB = getPatientEnrollmentDAO(communityId).getPrimaryPatientMedicaidMedicare(medicaidId, payerClassId, communityId);

        logger.info("checkForDuplicatePrimaryMedicaidId: search results found: " + resultsFromDB.size());

        // check to see if it exists in database ... excluding our own id
        for (PatientCommunityEnrollment tempPatientEnrollment : resultsFromDB) {

            logger.info("checkForDuplicatePrimaryMedicaidId: euid = " + euid);
            logger.info("checkForDuplicatePrimaryMedicaidId: tempPatientEnrollment.getPatientEuid() = " + tempPatientEnrollment.getPatientEuid());

            // make sure our ID is not there
            if (StringUtils.equals(euid, tempPatientEnrollment.getPatientEuid())) {
                continue;
            } else {
                // if exists then this is a duplicate
                // construct error message
                // throw exception
                String duplicatePatientEuid = tempPatientEnrollment.getPatientEuid();
                String dupeMsg = "Duplicate Primary Medicaid/Medicare ID";

                throw new DuplicatePatientException(dupeMsg);
            }
        }

    }

    private void checkForDuplicateSecondaryMedicaidId(PersonDTO personDTOFromDB, long communityId) throws Exception {

        // get primary payer class and medicaid id
        PatientEnrollmentDTO patientEnrollment = personDTOFromDB.getPatientEnrollment();
        String euid = patientEnrollment.getPatientEuid();

        String medicaidId = patientEnrollment.getSecondaryPayerMedicaidMedicareId();
        long payerClassId = patientEnrollment.getSecondaryPayerClass().getId();

        List<PatientCommunityEnrollment> resultsFromDB = getPatientEnrollmentDAO(communityId).getSecondaryPatientMedicaidMedicare(medicaidId, payerClassId, communityId);

        logger.info("checkForDuplicateSecondaryMedicaidId: search results found: " + resultsFromDB.size());

        // check to see if it exists in database ... excluding our own id
        for (PatientCommunityEnrollment tempPatientEnrollment : resultsFromDB) {

            logger.info("checkForDuplicateSecondaryMedicaidId: euid = " + euid);
            logger.info("checkForDuplicateSecondaryMedicaidId: tempPatientEnrollment.getPatientEuid() = " + tempPatientEnrollment.getPatientEuid());

            // make sure our ID is not there
            if (StringUtils.equals(euid, tempPatientEnrollment.getPatientEuid())) {
                continue;
            } else {
                // if exists then this is a duplicate
                // construct error message
                // throw exception
                String duplicatePatientEuid = tempPatientEnrollment.getPatientEuid();
                String dupeMsg = "Duplicate Secondary Medicaid/Medicare ID";

                throw new DuplicatePatientException(dupeMsg);
            }
        }

    }

    /**
     * Check if the values are set, if so return true
     */
    private boolean hasPersonInfoFromClientChanged(PersonDTO personDTO) {

        // check to see if the values are set ... if so then
        return (StringUtils.isNotBlank(personDTO.getFirstName())
                || StringUtils.isNotBlank(personDTO.getLastName())
                || StringUtils.isNotBlank(personDTO.getGender())
                || personDTO.getDateOfBirth() != null);
    }

    private void checkForDuplicatePatient(PersonDTO personDTOFromDB, long communityId) throws Exception {

        logger.info("start checkForDuplicatePatient");

        PatientEnrollmentDTO patientEnrollment = personDTOFromDB.getPatientEnrollment();
        String euid = patientEnrollment.getPatientEuid();

        // create empty person for search criteria
        SimplePerson thePerson = new SimplePerson();
        thePerson.setFirstName(StringUtils.trim(personDTOFromDB.getFirstName()));
        thePerson.setLastName(StringUtils.trim(personDTOFromDB.getLastName()));
        thePerson.setDateOfBirth(personDTOFromDB.getDateOfBirth());
        //Fix for new MDM code
        thePerson.setBirthDateTime(PatientUtils.convertDateToStr(personDTOFromDB.getDateOfBirth()));
        thePerson.setGenderCode(personDTOFromDB.getGenderCode());

        List<SimplePerson> searchResults = getPatientManager(communityId).findPatient(thePerson);

        for (SimplePerson tempPerson : searchResults) {
            boolean euidMatches = StringUtils.equals(euid, tempPerson.getPatientEuid());
            boolean matches = PatientUtils.personEqualsIgnoreCase(thePerson, tempPerson);
            logger.info("euid: " + euid);
            logger.info("tempPerson.getEuid(): " + tempPerson.getPatientEuid());

            logger.info("StringUtils.equals(euid, tempPerson.getEuid(): " + euidMatches);

            logger.info("personEqualsIgnoreCase(thePerson, tempPerson) = " + matches);

            // make sure our ID is not there
            if (euidMatches) {
                continue;
            } else {
                if (matches) {

                    PatientCommunityEnrollment dupPatientEnrollment
                            = getPatientEnrollmentDAO(communityId).findPatientEnrollment(tempPerson.getPatientEuid());
                    Long orgId = dupPatientEnrollment.getOrgId();
                    String dupPatientOrg = organizationDAO.findOrganization(orgId, communityId).getOrganizationCwdName();

                    String dupPatientStatus = dupPatientEnrollment.getStatus();
                    logger.log(Level.INFO, "Duplicate patient org: {0}", dupPatientOrg);

                    if (dupPatientOrg != null) {
                        throw new DuplicatePatientException("Error updating Patient. A patient with very similar details already exists in the system. That patient is assigned to: " + dupPatientOrg + " with an Enrollment Status of " + dupPatientStatus + ".");
                    } else {
                        throw new DuplicatePatientException("Error updating Patient. This is a duplicate patient. A patient with very similar details already exists in the system.");
                    }
                }
            }
        }

        logger.info("end checkForDuplicatePatient");
    }

    public static boolean personEqualsIgnoreCase(Person sourcePerson, Person targetPerson) {

        boolean firstNameEquals = StringUtils.equalsIgnoreCase(sourcePerson.getFirstName(), targetPerson.getFirstName());
        boolean lastNameEquals = StringUtils.equalsIgnoreCase(sourcePerson.getLastName(), targetPerson.getLastName());
        boolean genderEquals = StringUtils.equalsIgnoreCase(sourcePerson.getGender(), targetPerson.getGender());
        boolean dobSameDay = DateUtils.isSameDay(sourcePerson.getDateOfBirth(), targetPerson.getDateOfBirth());

        return (firstNameEquals && lastNameEquals && genderEquals && dobSameDay);
    }

    private PersonDTO verifyAndUpdate(PersonDTO personDTO, User loadedBy) {
        PatientEnrollmentDTO enrollment = personDTO.getPatientEnrollment();
        if (enrollment != null) {
            if ("Yes".equalsIgnoreCase(enrollment.getProgramLevelConsentStatus())) {
                if (enrollment.getConsentObtainedByUserId() <= 0) {
                    enrollment.setConsentObtainedByUserId(loadedBy.getUserId());
                }

                if (enrollment.getConsentDateTime() == null) {
                    enrollment.setConsentDateTime(new Date());
                }
            }
        }

        return personDTO;
    }

    private User findUserByEmail(String email, long community) {
        if (email == null) return null;

        return userDAO.findByEmail(email, community);
    }
}
