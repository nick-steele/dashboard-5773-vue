package com.gsihealth.dashboard.server.loader.webservice;

import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import java.io.IOException;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;

/** DON'T ENABLE WITHOUT ADDRESSING THE DEFAULT COMMUNITY ID !!!!!!!!!!!!!
 * >> DISABLED FOR NOW ... seems as though the JAX-WS client still needs access
 * >> to the WSDL file at startup. Need to find a work around.
 * 
 * This filter will hide the Patient Loader Web Service WSDL. This prevents
 * someone from viewing the WSDL with: LoaderWebServiceService?wsdl
 *
 * This is configured via the property: patientloader.webservice.hide.wsdl This
 * property defaults to true if missing
 *
 * @author Chad.Darby
 */
// @WebFilter(filterName = "HideLoaderWebServiceWsdlFilter", urlPatterns = {"/LoaderWebServiceService"})
public class HideLoaderWebServiceWsdlFilter implements Filter {

    private Logger logger = Logger.getLogger(getClass().getName());
    private static final long WORK_AROUND_COMMUNITY_ID = 1;
    
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String queryString = httpServletRequest.getQueryString();
        
        logger.fine("FILTER: pathInfo=" + httpServletRequest.getPathInfo());
        logger.fine("FILTER: queryString=" + queryString);
                      
        if (StringUtils.containsIgnoreCase(queryString, "wsdl")) {
            // reading properties
            ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
            boolean hideWsdl = Boolean.parseBoolean(configurationDAO.getProperty(WORK_AROUND_COMMUNITY_ID, "patientloader.webservice.hide.wsdl", "true"));
            logger.fine("FILTER: hideWsdl=" + hideWsdl);

            if (hideWsdl) {
                return; //the filter chain stops and request does not get processed
            } else {
                chain.doFilter(request, response);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
    }
}
