package com.gsihealth.dashboard.server.loader.webservice;

/**
 *
 * @author Ajay
 */
public class UserLoadException extends Exception {

    public UserLoadException(Throwable cause) {
        super(cause);
    }

    public UserLoadException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserLoadException(String message) {
        super(message);
    }

    public UserLoadException() {
    }
     
}
