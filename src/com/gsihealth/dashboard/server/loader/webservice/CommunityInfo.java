package com.gsihealth.dashboard.server.loader.webservice;

/**
 *
 * @author Chad Darby
 */
public class CommunityInfo  {
    
    private String communityName;
    private long communityId;

    public CommunityInfo(String communityName, long communityId) {
        this.communityName = communityName;
        this.communityId = communityId;
    }
    
    public CommunityInfo() {
      
    }
    
    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }    
}
