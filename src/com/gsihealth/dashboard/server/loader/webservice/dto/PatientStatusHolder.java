package com.gsihealth.dashboard.server.loader.webservice.dto;

/**
 *
 * @author Chad.Darby
 */
public class PatientStatusHolder implements java.io.Serializable {
    
    private long id;
    private String status;

    public PatientStatusHolder(long id, String status) {
        this.id = id;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}


