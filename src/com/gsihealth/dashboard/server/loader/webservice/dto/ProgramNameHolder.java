package com.gsihealth.dashboard.server.loader.webservice.dto;

/**
 *
 * @author Chad Darby
 */
public class ProgramNameHolder implements java.io.Serializable {
    
    private long programNameId;
    private String programName;
    private String healthHome;
    private long communityId;
    private boolean hhRequired;
    
    public ProgramNameHolder(long programNameId, String programName, String healthHome, boolean hhRequired, long communityId) {
        this.programNameId = programNameId;
        this.programName = programName;
        this.healthHome = healthHome;
        this.hhRequired = hhRequired;
        this.communityId = communityId;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }

    public String getHealthHome() {
        return healthHome;
    }

    public void setHealthHome(String healthHome) {
        this.healthHome = healthHome;
    }

    public long getProgramNameId() {
        return programNameId;
    }

    public void setProgramNameId(long programNameId) {
        this.programNameId = programNameId;
    }    

    public boolean isHhRequired() {
        return hhRequired;
    }

    public void setHhRequired(boolean hhRequired) {
        this.hhRequired = hhRequired;
    }
    
}
