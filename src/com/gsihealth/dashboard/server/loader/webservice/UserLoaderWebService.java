/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.loader.webservice;

import com.gsihealth.dashboard.client.service.AdminService;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.common.UserLoadResult;
import com.gsihealth.dashboard.server.dao.AccessLevelDAO;
import com.gsihealth.dashboard.server.dao.AuditDAO;
import com.gsihealth.dashboard.server.dao.CommunityDAO;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.dashboard.server.dao.OrganizationDAO;
import com.gsihealth.dashboard.server.dao.ReferenceDataDAO;
import com.gsihealth.dashboard.server.dao.SSOUserDAO;
import com.gsihealth.dashboard.server.dao.UserCommunityOrganizationDAO;
import com.gsihealth.dashboard.server.dao.UserDAO;
import com.gsihealth.dashboard.server.hisp.HispClient;
import com.gsihealth.dashboard.server.service.integration.UserService;
import com.gsihealth.dashboard.server.util.PropertyUtils;
import com.gsihealth.entity.AccessLevel;
import com.gsihealth.entity.Community;
import com.gsihealth.entity.GenderCode;
import com.gsihealth.entity.OrganizationCwd;
import com.gsihealth.entity.Role;
import com.gsihealth.entity.User;
import com.gsihealth.entity.UserCredential;
import com.gsihealth.entity.UserPrefix;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrTokenizer;
import org.apache.commons.lang.time.StopWatch;

/**
 *
 * @author Ajay
 */
@WebService(serviceName = "UserLoaderWebService")
public class UserLoaderWebService {

    private static final int USER_LOADER_USER_ID = 999998;
    private Logger logger = Logger.getLogger(getClass().getName());

    private AuditDAO auditDAO;
    private CommunityDAO communityDAO;

    private OrganizationDAO organizationDAO;

    private UserDAO userDAO;

    private List<String> clientAddressWhiteList;

    private ReferenceDataDAO referenceDataDAO;
    private AccessLevelDAO accessLevelDAO;
    private SSOUserDAO sSOUserDAO;
    private UserService userService;
    private UserCommunityOrganizationDAO userCommunityOrganizationDAO;
    private HispClient hispClient;
    private ConfigurationDAO configDao;

    private DateFormat timeFormatter = new SimpleDateFormat("hh:mm");
    private DateFormat ampmFormatter = new SimpleDateFormat("a");
    private boolean enableClientMFA;
    private String authyApiKey;
    //  private long WORK_AROUND_COMMUNITY_ID = 1;
    @Resource
    WebServiceContext webServiceContext;

    public UserLoaderWebService() {
    }

    @WebMethod
    public UserLoadResult addUsers(UserDTO userDTO,long communityId, List<String> userApps, String token) throws UserLoadException {
        UserLoadResult loadResult = new UserLoadResult();
        boolean isExitingUser=false;
        StopWatch totalStopWatch = new StopWatch();
        totalStopWatch.start();

        StopWatch tempStopWatch = new StopWatch();
        tempStopWatch.start();

        setupDaos();
        getPropertiesFromConfigurationDAO(communityId);
        tempStopWatch.stop();
        logger.info("[profiling], LoaderWebService: setupDaos, " + (tempStopWatch.getTime() / 1000.0) + ", secs");

        tempStopWatch.reset();
        tempStopWatch.start();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        tempStopWatch.stop();
        logger.info("[profiling], LoaderWebService: isDisabled, " + (tempStopWatch.getTime() / 1000.0) + ", secs");

        tempStopWatch.reset();
        tempStopWatch.start();

        performSecurityTest(userDTO, token, communityId);

        tempStopWatch.stop();
        logger.info("[profiling], LoaderWebService: performSecurityTest, " + (tempStopWatch.getTime() / 1000.0) + ", secs");

        if (userDTO == null) {
            throw new IllegalArgumentException("Invalid data");
        }

        // now do your work
        try {
            // 4728: fixed null patient relationship id

           // LoginResult loginResult = buildLoginResult();
            // loginResult.setCommunityId(communityId);
            tempStopWatch.reset();
            tempStopWatch.start();
            //  setSession();

         /*   for (CommunityInfo community:getCommunities()){
                if(isUserEmailAlreadyExist(token, community.getCommunityId(), userDTO.getEmail())){
                    isExitingUser=true;
                    break;
                }
            }*/



          /*  if(isValidPortalUserId(userDTO.getEmail())){
                isExitingUser=true;
            }*/
            AdminService adminService = getAdminService();
         //   adminService.addOrUpdateUser(userDTO, userApps,false, isExitingUser,enableClientMFA,authyApiKey,communityId, true);
 
            adminService.addUser(userDTO, userApps,enableClientMFA,authyApiKey,communityId, true);

            tempStopWatch.stop();
            logger.info("[profiling], LoaderWebService: calling enrollmentServiceHelper.addPatientHelper, " + (tempStopWatch.getTime() / 1000.0) + ", secs");

            totalStopWatch.stop();
            logger.info("[profiling], LoaderWebService: TOTAL - loaduser, " + (totalStopWatch.getTime() / 1000.0) + ", secs");
            logger.info("Added user");
            loadResult.setEmail(userDTO.getEmail());
            loadResult.setCommunityId(communityId);
            return loadResult;
        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error loading user", exc);
            throw new UserLoadException(exc.getMessage());
        } finally {
            //closeSession();
        }

    }
    
    protected void getPropertiesFromConfigurationDAO(long communityId){
        
        enableClientMFA=Boolean.parseBoolean(configDao.getProperty(communityId, "enable.client.MFA", "false"));
        authyApiKey= configDao.getProperty(communityId, "authy.api.key");
        
    }

    @WebMethod
    public UserLoadResult updateUsers(UserDTO userDTO, List<String> userApps, boolean checkForExistingEmailAddress,long communityId, String token) throws UserLoadException {
        UserLoadResult loadResult = new UserLoadResult();
        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        performSecurityTest(userDTO, token, communityId);

        if (userDTO == null) {
            throw new IllegalArgumentException("Invalid data");
        }

        setupDaos();
        getPropertiesFromConfigurationDAO(communityId);
        // now do your work
        try {
           // LoginResult loginResult = buildLoginResult();
            //  loginResult.setCommunityId(communityId);

            User user = userDAO.findUser(userDTO.getUserId());
            if (user == null) {
                throw new RuntimeException("Invalid User Id");
            }
            // setSession();
            AdminService adminService = getAdminService();
            // get the patient from DB first
             userDTO.setEulaAccepted(user.getEulaAccepted());
            adminService.updateUser(userDTO, userApps, checkForExistingEmailAddress,enableClientMFA, authyApiKey,communityId, true);

            logger.info("updated user");
            loadResult.setEmail(userDTO.getEmail());
            loadResult.setCommunityId(communityId);
            return loadResult;

        } catch (Exception exc) {
            logger.log(Level.WARNING, "Error loading user", exc);
            throw new UserLoadException(exc.getMessage());
        } finally {
            //closeSession();
        }

    }

    @WebMethod
    public List<String> getPrefix(long communityId, String token) {
        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }
        setupDaos();
        String empty = "";
        performSecurityTest(empty, token, communityId);
        try {
            List<UserPrefix> daoResult = referenceDataDAO.findUserPrefixEntities(communityId);
            List<String> resultList = new ArrayList<String>();
            for (UserPrefix userPrefix : daoResult) {
                resultList.add(userPrefix.getPrefixName());
            }
            return resultList;
        } catch (Exception e) {
            logger.log(Level.WARNING, "Error loading prefix", e);
            throw new RuntimeException("Error loading prefix");
        }

    }

    @WebMethod
    public List<String> getGender(long communityId, String token) {

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }
        setupDaos();
        String empty = "";
        performSecurityTest(empty, token, communityId);
        try {

            List<GenderCode> daoResult = referenceDataDAO.findGenderCodeEntities(communityId);
            List<String> resultList = new ArrayList<String>();
            for (GenderCode ob : daoResult) {
                resultList.add(ob.getCode());
            }
            return resultList;
        } catch (Exception e) {
            logger.log(Level.WARNING, "Error loading gender", e);
            throw new RuntimeException("Error loading gender");
        }

    }

    @WebMethod
    public List<String> getOrganization(long communityId, String token) {
        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }
        setupDaos();
        String empty = "";
        performSecurityTest(empty, token, communityId);
        try {

            List<OrganizationCwd> daoResult = organizationDAO.findOrganizationEntities(communityId);
            List<String> resultList = new ArrayList<String>();
            for (OrganizationCwd ob : daoResult) {
                resultList.add(String.valueOf(ob.getOrgId()));
            }
            return resultList;
        } catch (Exception e) {
            logger.log(Level.WARNING, "Error loading organization", e);
            throw new RuntimeException("Error loading organization");
        }

    }

    @WebMethod
    public List<String> getUserLevel(long communityId, String token) {
        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }
        setupDaos();
        String empty = "";
        performSecurityTest(empty, token, communityId);
        try {

            List<Role> daoResult = referenceDataDAO.getUserRolesEntities(communityId);
            List<String> resultList = new ArrayList<String>();
            for (Role ob : daoResult) {
                resultList.add(String.valueOf(ob.getPrimaryRoleId()));
            }
            return resultList;
        } catch (Exception e) {
            logger.log(Level.WARNING, "Error loading user level", e);
            throw new RuntimeException("Error loading user level");
        }

    }

    @WebMethod
    public List<String> getAccessLevel(long communityId, String token) {

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }
        setupDaos();
        String empty = "";
        performSecurityTest(empty, token, communityId);
        try {

            List<AccessLevel> daoResult = accessLevelDAO.findAccessLevelEntities(communityId);
            List<String> resultList = new ArrayList<String>();
            for (AccessLevel ob : daoResult) {
                resultList.add(String.valueOf(ob.getAccessLevelPK().getAccessLevelId()));
            }
            return resultList;
        } catch (Exception e) {

            logger.log(Level.WARNING, "Error loading access level", e);
            throw new RuntimeException("Error loading access level");
        }

    }

    @WebMethod
    public List<String> getCredential(long communityId, String token) {

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }
        setupDaos();
        String empty = "";
        performSecurityTest(empty, token, communityId);
        try {

            List<UserCredential> daoResult = referenceDataDAO.findUserCredentialEntities(communityId);
            List<String> resultList = new ArrayList<String>();
            for (UserCredential ob : daoResult) {
                resultList.add(String.valueOf(ob.getCredentialName()));
            }
            return resultList;
        } catch (Exception e) {

            logger.log(Level.WARNING, "Error loading credentials", e);
            throw new RuntimeException("Error loading credentails");
        }

    }

    @WebMethod
    public String[] getState(long communityId, String token) {

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }
        setupDaos();
        String empty = "";
        performSecurityTest(empty, token, communityId);
        try {
            return referenceDataDAO.getPostalStateCodes(communityId);
        } catch (Exception e) {
            logger.log(Level.WARNING, "Error loading states", e);
            throw new RuntimeException("Error loading states");
        }

    }

    @WebMethod
    public List<CommunityInfo> getCommunities() {

        setupDaos();

        /*if (isDisabled(communityId)) {
         throw new IllegalArgumentException("disabled");
         }*/
        try {
            List<CommunityInfo> commInfoList = new ArrayList<CommunityInfo>();

            List<Community> comms = communityDAO.findEntities();

            for (Community temp : comms) {
                commInfoList.add(new CommunityInfo(temp.getCommunityName(), temp.getCommunityId()));
            }

            return commInfoList;
        } catch (Exception e) {
            logger.log(Level.WARNING, "Error loading community", e);
            throw new RuntimeException("Error loading community");
        }

    }

    @WebMethod
    public UserDTO getUser(long communityId, long userId) {

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        String empty = "";
        //   performSecurityTest(empty, token);

        try {
            // check the patient id
            User theUser = userDAO.findUser(userId);
            UserDTO userDTO = PropertyUtils.populateUserDTO(theUser, communityId);

            return userDTO;

        } catch (Exception exc) {
            logger.log(Level.WARNING, "Invalid userId ID: " + userId, exc);
            throw new RuntimeException("Invalid userId ID: " + userId);
        }

    }

    @WebMethod
    public boolean isValidUserId(String token, long communityId, long userId) {

        boolean valid = false;

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        String empty = "";
        performSecurityTest(empty, token, communityId);

        try {
            // check the patient id
            valid = userDAO.findUser(userId) == null ? false : true;

            return valid;
        } catch (Exception exc) {
            logger.log(Level.WARNING, "Invalid userId ID: " + userId, exc);
            throw new RuntimeException("Invalid userId ID: " + userId);
        }

    }
    
     @WebMethod
    public boolean isUserEmailAlreadyExist(String token, long communityId, String email) {

        boolean valid = false;

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        String empty = "";
        performSecurityTest(empty, token, communityId);

        try {
            
            valid = userDAO.findUserByEmail(email, communityId)==null?false:true;
            return valid;
        } catch (Exception exc) {
            logger.log(Level.WARNING,  exc.getMessage());
            return valid;
        }

    }
    
     @WebMethod
    public boolean isMFAEnabled(String token, long communityId) {

      

        setupDaos();

        if (isDisabled(communityId)) {
            throw new IllegalArgumentException("disabled");
        }

        String empty = "";
        performSecurityTest(empty, token, communityId);

        try {
            
            getPropertiesFromConfigurationDAO(communityId);
            return enableClientMFA;
        } catch (Exception exc) {
            logger.log(Level.WARNING,  exc.getMessage());
            return false;
        }

    }
    

    private boolean isDisabled(long communityId) {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        String enabledStr = configurationDAO.getProperty(communityId, "userloader.webservice.enabled", "false");

        boolean enabled = Boolean.parseBoolean(enabledStr);

        return !enabled;
    }

    protected void performSecurityTest(long communityId) throws IllegalArgumentException {

        String clientAddress = getClientAddress();
        logger.info("UserLoader web service request from, Client Address=" + clientAddress);

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        boolean whiteListEnabled = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "userloader.webservice.client.address.whitelist.enabled", "true"));

        if (whiteListEnabled) {
            String clientAddressWhiteListStr = configurationDAO.getProperty(communityId, "userloader.webservice.client.address.whitelist");
            clientAddressWhiteList = buildClientAddressWhiteList(clientAddressWhiteListStr);

            // check ip address
            if (isNotValidClientAddress(clientAddress)) {
                logger.warning("UserLoader web service rejected call due to invalid address. client address=" + clientAddress);
                throw new IllegalArgumentException("Invalid client address");
            }
        }

    }

    protected void performSecurityTest(String empty, String token, long communityId) throws IllegalArgumentException {

        String clientAddress = getClientAddress();
        logger.info("UserLoader web service request from, Client Address=" + clientAddress);

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        boolean whiteListEnabled = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "userloader.webservice.client.address.whitelist.enabled", "true"));

        if (whiteListEnabled) {
            String clientAddressWhiteListStr = configurationDAO.getProperty(communityId, "userloader.webservice.client.address.whitelist");
            clientAddressWhiteList = buildClientAddressWhiteList(clientAddressWhiteListStr);

            // check ip address
            if (isNotValidClientAddress(clientAddress)) {
                logger.warning("UserLoader web service rejected call due to invalid address. client address=" + clientAddress);
                throw new IllegalArgumentException("Invalid client address");
            }
        }

    }

    protected void performSecurityTest(UserDTO person, String token, long communityId) throws IllegalArgumentException {

        String clientAddress = getClientAddress();
        logger.info("UserLoader web service request from, Client Address=" + clientAddress);

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

        boolean whiteListEnabled = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "userloader.webservice.client.address.whitelist.enabled", "true"));

        if (whiteListEnabled) {
            String clientAddressWhiteListStr = configurationDAO.getProperty(communityId, "userloader.webservice.client.address.whitelist");
            clientAddressWhiteList = buildClientAddressWhiteList(clientAddressWhiteListStr);

            // check ip address
            if (isNotValidClientAddress(clientAddress)) {
                logger.warning("UserLoader web service rejected call due to invalid address. client address=" + clientAddress);
                throw new IllegalArgumentException("Invalid client address");
            }
        }

    }

    protected String getClientAddress() {
        String clientAddress = null;

        MessageContext messageContext = webServiceContext.getMessageContext();
        HttpServletRequest request = (HttpServletRequest) messageContext.get(MessageContext.SERVLET_REQUEST);

        logger.info("dumping header names/values");
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String tempHeaderName = headerNames.nextElement();
            logger.info(tempHeaderName + ": " + request.getHeader(tempHeaderName));
        }

        logger.info("dumping parameter names/values");
        Enumeration<String> paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            String tempParamName = paramNames.nextElement();
            logger.info(tempParamName + ": " + request.getParameter(tempParamName));
        }

        clientAddress = request.getRemoteAddr();
        logger.info("clientAddress=" + clientAddress);

        return clientAddress;
    }

    protected AdminService getAdminService() {
        ServletContext application = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);
        AdminService adminService = (AdminService) application.getAttribute(WebConstants.ADMIN_SERVICE_KEY);

        return adminService;
    }

    private void setupDaos() {

        // if daos setup already just return
        if (referenceDataDAO != null) {
            return;
        }

        ServletContext application = (ServletContext) webServiceContext.getMessageContext().get(MessageContext.SERVLET_CONTEXT);

       // patientManager = (PatientManager) application.getAttribute(WebConstants.PATIENT_MANAGER_KEY);

       // patientEnrollmentDAO = (PatientEnrollmentDAOImpl) application.getAttribute(WebConstants.PATIENT_ENROLLMENT_DAO_KEY);
        auditDAO = (AuditDAO) application.getAttribute(WebConstants.AUDIT_DAO_KEY);

        //careteamDAO = (CommunityCareteamDAO) application.getAttribute(WebConstants.COMMUNITY_CARE_TEAM_DAO_KEY);
        //notificationAuditDAO = (NotificationAuditDAO) application.getAttribute(WebConstants.NOTIFICATION_AUDIT_DAO_KEY);
       // organizationPatientConsentDAO = (OrganizationPatientConsentDAO) application.getAttribute(WebConstants.ORGANIZATION_PATIENT_CONSENT_DAO_KEY);
        organizationDAO = (OrganizationDAO) application.getAttribute(WebConstants.ORGANIZATION_DAO_KEY);

       // userPatientConsentDAO = (UserPatientConsentDAO) application.getAttribute(WebConstants.USER_PATIENT_CONSENT_DAO_KEY);
        userDAO = (UserDAO) application.getAttribute(WebConstants.USER_DAO_KEY);

        //payerPlanDAO = (PayerPlanDAO) application.getAttribute(WebConstants.PAYER_PLAN_DAO_KEY);
       // patientEnrollmentHistoryDAO = (PatientEnrollmentHistoryDAO) application.getAttribute(WebConstants.PATIENT_ENROLLMENT_HISTORY_DAO_KEY);
      //  disenrollmentReasonDAO = (DisenrollmentReasonDAO) application.getAttribute(WebConstants.DISENROLLMENT_DAO_DEY);
       // programNameHistoryDAO = (ProgramNameHistoryDAO) application.getAttribute(WebConstants.PROGRAM_NAME_HISTORY_DAO_KEY);
        communityDAO = (CommunityDAO) application.getAttribute(WebConstants.COMMUNITY_DAO_KEY);

        referenceDataDAO = (ReferenceDataDAO) application.getAttribute(WebConstants.REFERENCE_DATA_DAO_KEY);

        accessLevelDAO = (AccessLevelDAO) application.getAttribute(WebConstants.ACCESS_LEVEL_DAO_KEY);

        sSOUserDAO = (SSOUserDAO) application.getAttribute(WebConstants.SSO_USER_DAO_KEY);
        userService = (UserService) application.getAttribute(WebConstants.USER_SERVICE_KEY);
        userCommunityOrganizationDAO = (UserCommunityOrganizationDAO) application.getAttribute(WebConstants.USER_COMMUNITY_ORGANIZATION_DAO_KEY);
        hispClient = (HispClient) application.getAttribute(WebConstants.HISP_CLIENT_KEY);
        configDao=(ConfigurationDAO)application.getAttribute(WebConstants.CONFIGURATION_DAO_KEY);
     //   patientUserNotification = (PatientUserNotification) application.getAttribute(WebConstants.PATIENT_USER_NOTIFICATION);
        // pixClient = new PixClientImpl(props);

        logger.info("DAOs setup complete");
    }

    private boolean isNotValidClientAddress(String clientAddress) {

        logger.info("clientAddressWhiteList=" + clientAddressWhiteList);
        logger.info("clientAddress=" + clientAddress);
        logger.info("!clientAddressWhiteList.contains(clientAddress)=" + !clientAddressWhiteList.contains(clientAddress));

        return !clientAddressWhiteList.contains(clientAddress);
    }

    /**
     * Parses a comma delimited list of client ip addresses Converts it to a
     * list
     *
     * @param clientAddressWhiteListStr
     * @return
     */
    private List<String> buildClientAddressWhiteList(String clientAddressWhiteListStr) {
        clientAddressWhiteList = new ArrayList<String>();

        if (StringUtils.isNotBlank(clientAddressWhiteListStr)) {

            StrTokenizer tokenizer = new StrTokenizer(clientAddressWhiteListStr, ",");
            clientAddressWhiteList = tokenizer.getTokenList();
        }

        return clientAddressWhiteList;
    }

    private LoginResult buildLoginResult() {
        LoginResult theLoginResult = new LoginResult();

        theLoginResult.setAccessLevel("Super User");
        theLoginResult.setUserLevel("Administrator");
        theLoginResult.setUserLevelId(8);
        theLoginResult.setFirstName("Admin");
        theLoginResult.setLastName("Admin User Loader");
        theLoginResult.setEmail("userloader@gsihealth.com");
        theLoginResult.setUserId(USER_LOADER_USER_ID);
        theLoginResult.setOrganizationName("MMC");

        return theLoginResult;
    }

    /* private void setSession() {
     HttpServletRequest req = (HttpServletRequest) this.webServiceContext.getMessageContext()
     .get(MessageContext.SERVLET_REQUEST);

     req.getSession().setAttribute(WebConstants.LOGIN_RESULT_KEY, buildLoginResult());
     }

     private void closeSession() {
     HttpServletRequest req = (HttpServletRequest) this.webServiceContext.getMessageContext()
     .get(MessageContext.SERVLET_REQUEST);
     req.getSession().invalidate();
     }*/

     protected boolean isValidPortalUserId(String portalUserId) {

        boolean isValid = false;

        try {
            User user = userDAO.findUserByEmail(portalUserId);
            isValid = true;
        } catch (Exception exc) {
            isValid = false;
        }

        return isValid;
    }
}
