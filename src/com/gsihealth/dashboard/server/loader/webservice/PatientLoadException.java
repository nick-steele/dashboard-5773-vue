package com.gsihealth.dashboard.server.loader.webservice;

/**
 *
 * @author Chad Darby
 */
public class PatientLoadException extends Exception {

    public PatientLoadException(Throwable cause) {
        super(cause);
    }

    public PatientLoadException(String message, Throwable cause) {
        super(message, cause);
    }

    public PatientLoadException(String message) {
        super(message);
    }

    public PatientLoadException() {
    }
     
}
