package com.gsihealth.dashboard.server.loader.webservice;

import com.gsihealth.dashboard.common.dto.PersonDTO;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import org.jasypt.util.text.BasicTextEncryptor;
import org.joda.time.DateTime;

/**
 *
 * @author Chad Darby
 */
public class TokenUtils {

    private final static String key = "bFLajjHZc3DV";
    private BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
    private DateFormat dateFormatter;
    private Logger logger = Logger.getLogger(getClass().getName());

    public TokenUtils(String theKey) {
        textEncryptor = new BasicTextEncryptor();
        textEncryptor.setPassword(theKey);

        dateFormatter = new SimpleDateFormat("yyyy@MMM@dd");
    }

    public TokenUtils() {
        this(key);
    }

    public boolean isValidToken(PersonDTO person, String token) {

        Date today = new Date();
        Date yesterday = getYesterday(today);
        Date tomorrow = getTomorrow(today);

        String yesterdayText = null;
        String tomorrowText = null;
        String todayText = null;

        if (person != null) {
            String text = person.getLastName() + person.getFirstName() + person.getZipCode();
            
            yesterdayText = text + dateFormatter.format(yesterday);
            tomorrowText = text + dateFormatter.format(tomorrow);
            todayText = text + dateFormatter.format(today);
        }

        boolean result = validate(token, todayText) || validate(token, yesterdayText) || validate(token, tomorrowText);

        return result;
    }

    private Date getYesterday(Date theDate) {
        Date result = new DateTime(theDate).minusDays(1).toDate();

        return result;
    }

    private Date getTomorrow(Date theDate) {
        Date result = new DateTime(theDate).plusDays(1).toDate();

        return result;
    }
    
    public boolean isValidToken(String text, String token) {

        Date today = new Date();
        Date yesterday = getYesterday(today);
        Date tomorrow = getTomorrow(today);

        String yesterdayText = null;
        String tomorrowText = null;
        String todayText = null;
        
        if (text != null) {
            yesterdayText = text + dateFormatter.format(yesterday);
            tomorrowText = text + dateFormatter.format(tomorrow);
            todayText = text + dateFormatter.format(today);
        }

        boolean result = validate(token, todayText) || validate(token, yesterdayText) || validate(token, tomorrowText);

        return result;
    }

    protected boolean validate(String token, String text) {
        boolean result;

        try {
            String decryptToken = textEncryptor.decrypt(token);

            result = StringUtils.equals(text, decryptToken);
        } catch (Exception exc) {
            result = false;
            logger.warning("Could not decrypt token.");
        }

        return result;
    }
}
