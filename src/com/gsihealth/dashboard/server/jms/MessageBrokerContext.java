package com.gsihealth.dashboard.server.jms;

import com.gsihealth.dashboard.client.util.StringUtils;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Hashtable;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author Chad.Darby
 */
public class MessageBrokerContext {
   private Logger logger = Logger.getLogger(getClass().getName()); 
    private ConnectionFactory connectionFactory;
    private Queue queue;
     private ConnectionFactory xdsConnectionFactory;
    private Queue xdsQueue;
    
    private static MessageBrokerContext instance;
    
    private MessageBrokerContext() {
        
    }
    
    public static MessageBrokerContext getInstance() {
        if (instance == null) {
            instance = new MessageBrokerContext();
        }
        
        return instance;
    }

    private MessageBrokerContext(String serverIP) {
        this.createConnectionFactoryAndQueue(serverIP);
    }
    
    public static MessageBrokerContext getInstance(String serverIP) {
        if (instance == null) {
            instance = new MessageBrokerContext(serverIP);
            
        }
        
        return instance;
    }

    

    /**
     * gotta do this the old fashioned way - roll your own
     * @param remoteBindingsFileLocation location of the binding file
     */
    private void createConnectionFactoryAndQueue(String remoteBindingsFileLocation) {
        if(StringUtils.isBlank(remoteBindingsFileLocation)) {
            logger.warning("missing a valid binding file - this will not end well");
            return;
        }
        
        // TODO prolly a better way to do this, but use relative pathing
        Path currentRelativePath = Paths.get("");
        String currPath = "file:///" + currentRelativePath.toAbsolutePath().toString();
        remoteBindingsFileLocation = currPath.concat(remoteBindingsFileLocation);
        logger.info("remote bindings file location: " + remoteBindingsFileLocation);
        
        try {
            Context jndiContext = this.getAConnectionFactory(remoteBindingsFileLocation);
            connectionFactory = (ConnectionFactory)jndiContext.lookup("jms/NotificationConnectionFactory");
            logger.info("created a connectionFactory: " + connectionFactory);
            //connectionFactory.setProperty(com.sun.messaging.ConnectionConfiguration.imqAddressList,imqAddressListString);
            connectionFactory.createConnection("guest", "guest"); //FIXME - CHANGE THE PASSWORD IN THE BROKER
            queue = (Queue)jndiContext.lookup("jms/NotificationQueue");
        } catch (JMSException jMSException) {
            logger.severe("your pitiful attempts at creating a connection factory amuse me " + jMSException);
        } catch (NamingException ex) {
            logger.severe("you have chosen ... poorly " + ex);
       }
        
    }
    
    /**
     * do the context lookup bit first to set the connection factory
     * Uses jndi lookup that has been previously configured on the selected remote coordinator server
     * @param serverIP 
     */
    private Context getAConnectionFactory(String remoteBindingsFileLocation) throws NamingException {
        Hashtable env = new Hashtable();
        env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.fscontext.RefFSContextFactory");
        env.put(Context.PROVIDER_URL, remoteBindingsFileLocation); //FIXME THIS NEEDS TO BE A PARAM 
        return new InitialContext(env);
        
    }
    
    public ConnectionFactory getConnectionFactory() {
        return connectionFactory;
    }

    public void setConnectionFactory(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    public Queue getQueue() {
        return queue;
    }

    public void setQueue(Queue queue) {
        this.queue = queue;
    }

    private String parseIPAddyFromEndpoint(String endpoint) {
        String ipPattern
                = "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";
        String ipAddress = null;

        Pattern pattern = Pattern.compile(ipPattern);
        Matcher matcher = pattern.matcher(endpoint);
        if (matcher.find()) {
            ipAddress = matcher.group();
            logger.info("parsed endpoint " + endpoint + " to ip addy: " + ipAddress);
        } else {
            logger.warning("endpoint " + endpoint + " doesn't contain a valid ip address");
        }
        return ipAddress;
    }

    public ConnectionFactory getXdsConnectionFactory() {
        return xdsConnectionFactory;
    }

    public void setXdsConnectionFactory(ConnectionFactory xdsConnectionFactory) {
        this.xdsConnectionFactory = xdsConnectionFactory;
    }

    public Queue getXdsQueue() {
        return xdsQueue;
    }

    public void setXdsQueue(Queue xdsQueue) {
        this.xdsQueue = xdsQueue;
    }

}
