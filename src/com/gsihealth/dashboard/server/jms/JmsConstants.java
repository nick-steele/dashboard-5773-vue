package com.gsihealth.dashboard.server.jms;

/**
 *
 * @author Chad.Darby
 */
public interface JmsConstants {
    
    public final static String LOGIN_RESULT_KEY = "LOGIN_RESULT_KEY";
    public final static String PERSON_KEY = "PERSON_KEY";
    public final static String PERSON_DTO_KEY = "PERSON_DTO_KEY";
    public final static String OLD_ENROLLMENT_STATUS_KEY = "OLD_ENROLLMENT_STATUS_KEY";
    public final static String NEW_CARE_TEAM_ID_KEY = "NEW_CARE_TEAM_ID_KEY";
    public final static String OLD_CARE_TEAM_ID_KEY = "OLD_CARE_TEAM_ID_KEY";
    
    public final static String OLD_PROGRAM_LEVEL_CONSENT_STATUS_KEY = "OLD_PROGRAM_LEVEL_CONSENT_STATUS_KEY";
    public final static String PROGRAM_LEVEL_CONSENT_STATUS_KEY = "PROGRAM_LEVEL_CONSENT_STATUS_KEY";
    public final static String OLD_PERSON_KEY = "OLD_PERSON_KEY";
    public final static String OLD_ORGANIZATION_ID_KEY = "OLD_ORGANIZATION_ID_KEY";
    
    public final static String PATIENT_INFO_KEY = "PATIENT_INFO_KEY";
    public final static String ORGANIZATION_KEY = "ORGANIZATION_KEY";
    public final static String PATIENT_ID_KEY = "PATIENT_ID_KEY";
    public final static String PATIENT_ENROLLMENT_KEY = "PATIENT_ENROLLMENT_KEY";
    public final static String COMMUNITY_CARE_TEAM_ID_KEY = "COMMUNITY_CARE_TEAM_ID_KEY";
    public final static String OLD_CARE_TEAM_NAME_KEY = "OLD_CARE_TEAM_NAME_KEY";
    
}
