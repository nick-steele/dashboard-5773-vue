package com.gsihealth.dashboard.server.jms;

import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.service.EnrollmentPatientUpdater;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

/**
 *
 * @author Chad.Darby
 */
@MessageDriven(mappedName = "jms/Enrollment_PatientUpdate_Queue", activationConfig = {
    @ActivationConfigProperty(propertyName = "acknowledgeMode",
            propertyValue = "Auto-acknowledge"),
    @ActivationConfigProperty(propertyName = "destinationType",
            propertyValue = "javax.jms.Queue")
})
public class EnrollmentPatientUpdateMessageListener implements MessageListener {

    private Logger logger = Logger.getLogger(getClass().getName());

    public void onMessage(Message message) {

        logger.info("EnrollmentPatientUpdateMessageListener: BEGIN onMessage");

        if (message instanceof ObjectMessage) {
            ObjectMessage objectMessage = (ObjectMessage) message;
            try {
                HashMap<String, Object> map = (HashMap<String, Object>) objectMessage.getObject();

                // read data from the map
                LoginResult loginResult = (LoginResult) map.get(JmsConstants.LOGIN_RESULT_KEY);
                Person thePerson = (Person) map.get(JmsConstants.PERSON_KEY);
                PersonDTO thePersonDTO = (PersonDTO) map.get(JmsConstants.PERSON_DTO_KEY);

                String oldEnrollmentStatus = (String) map.get(JmsConstants.OLD_ENROLLMENT_STATUS_KEY);
                String newCareTeamId = (String) map.get(JmsConstants.NEW_CARE_TEAM_ID_KEY);
                String oldCareTeamId = (String) map.get(JmsConstants.OLD_CARE_TEAM_ID_KEY);
                String oldProgramLevelConsentStatus = (String) map.get(JmsConstants.OLD_PROGRAM_LEVEL_CONSENT_STATUS_KEY);
                String programLevelConsentStatus = (String) map.get(JmsConstants.PROGRAM_LEVEL_CONSENT_STATUS_KEY);

                Person oldPerson = (Person) map.get(JmsConstants.OLD_PERSON_KEY);
                long oldOrganizationId = (Long) map.get(JmsConstants.OLD_ORGANIZATION_ID_KEY);

                // get handle to updater code
                CoordinatorBrokerContext coordinatorBrokerContext = CoordinatorBrokerContext.getInstance();
                EnrollmentPatientUpdater enrollmentPatientUpdater = coordinatorBrokerContext.getEnrollmentPatientUpdater();

                // now call the method
                enrollmentPatientUpdater.runUpdatePatientProcessesSynchronous(loginResult, thePerson, thePersonDTO,
                        oldEnrollmentStatus, newCareTeamId, oldCareTeamId,
                        oldProgramLevelConsentStatus, programLevelConsentStatus,
                        oldPerson, oldOrganizationId);

            }
            catch (Exception ex) {
                logger.log(Level.SEVERE, "Eror processing message: EnrollmentPatientUpdateMessageListener", ex);                
            }
        } else {
            logger.warning("Invalid message type in EnrollmentPatientUpdate queue");
        }

        logger.info("EnrollmentPatientUpdateMessageListener: END onMessage");
    }
}
