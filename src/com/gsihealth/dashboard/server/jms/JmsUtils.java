package com.gsihealth.dashboard.server.jms;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;

/**
 *
 * @author Chad.Darby
 */
public class JmsUtils {

    public static void close(MessageProducer producer, Session session, Connection connection) {
     
        if (producer != null) {
            try {
                producer.close();
            } catch (JMSException ex) {
                Logger.getLogger(JmsUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (session != null) {
            try {
                session.close();
            } catch (JMSException ex) {
                Logger.getLogger(JmsUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        if (connection != null) {
            try {
                connection.close();
            } catch (JMSException ex) {
                Logger.getLogger(JmsUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
}
