package com.gsihealth.dashboard.server.jms;

import com.gsihealth.dashboard.server.service.CareTeamPatientUpdater;
import com.gsihealth.dashboard.server.service.EnrollmentPatientUpdater;
import javax.jms.ConnectionFactory;
import javax.jms.Queue;

/**
 *
 * @author Chad.Darby
 */
public class CoordinatorBrokerContext {
   
    private ConnectionFactory connectionFactory;
    private Queue enrollmentPatientUpdateQueue;
    private Queue careTeamPatientUpdateQueue;
    
    private EnrollmentPatientUpdater enrollmentPatientUpdater;
    private CareTeamPatientUpdater careTeamPatientUpdater;
    
    private static CoordinatorBrokerContext instance;
    
    private CoordinatorBrokerContext() {
        
    }
    
    public static CoordinatorBrokerContext getInstance() {
        if (instance == null) {
            instance = new CoordinatorBrokerContext();
        }
        
        return instance;
    }

    public ConnectionFactory getConnectionFactory() {
        return connectionFactory;
    }

    public void setConnectionFactory(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    public Queue getEnrollmentPatientUpdateQueue() {
        return enrollmentPatientUpdateQueue;
    }

    public void setEnrollmentPatientUpdateQueue(Queue enrollmentPatientUpdateQueue) {
        this.enrollmentPatientUpdateQueue = enrollmentPatientUpdateQueue;
    }

    public Queue getCareTeamPatientUpdateQueue() {
        return careTeamPatientUpdateQueue;
    }

    public void setCareTeamPatientUpdateQueue(Queue careTeamPatientUpdateQueue) {
        this.careTeamPatientUpdateQueue = careTeamPatientUpdateQueue;
    }

    public EnrollmentPatientUpdater getEnrollmentPatientUpdater() {
        return enrollmentPatientUpdater;
    }

    public void setEnrollmentPatientUpdater(EnrollmentPatientUpdater enrollmentPatientUpdater) {
        this.enrollmentPatientUpdater = enrollmentPatientUpdater;
    }

    public CareTeamPatientUpdater getCareTeamPatientUpdater() {
        return careTeamPatientUpdater;
    }

    public void setCareTeamPatientUpdater(CareTeamPatientUpdater careTeamPatientUpdater) {
        this.careTeamPatientUpdater = careTeamPatientUpdater;
    }    
}
