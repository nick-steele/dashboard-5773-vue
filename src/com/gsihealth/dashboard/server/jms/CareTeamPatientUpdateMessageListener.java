package com.gsihealth.dashboard.server.jms;

import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.entity.OrganizationCwd;
import com.gsihealth.entity.PatientCommunityEnrollment;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.service.CareTeamPatientUpdater;
import com.gsihealth.dashboard.server.service.CareteamServiceImpl;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

/**
 *
 * @author Chad.Darby
 */
@MessageDriven(mappedName = "jms/CareTeam_PatientUpdate_Queue", activationConfig = {
    @ActivationConfigProperty(propertyName = "acknowledgeMode",
            propertyValue = "Auto-acknowledge"),
    @ActivationConfigProperty(propertyName = "destinationType",
            propertyValue = "javax.jms.Queue")
})
public class CareTeamPatientUpdateMessageListener implements MessageListener {

    private Logger logger = Logger.getLogger(getClass().getName());

    public void onMessage(Message message) {

        logger.info("CareTeam_PatientUpdate_Queue: BEGIN onMessage");

        if (message instanceof ObjectMessage) {
            ObjectMessage objectMessage = (ObjectMessage) message;
            try {
                HashMap<String, Object> map = (HashMap<String, Object>) objectMessage.getObject();

                // read data from the map
                PatientInfo patientInfo = (PatientInfo) map.get(JmsConstants.PATIENT_INFO_KEY);
                
                OrganizationCwd organization = (OrganizationCwd) map.get(JmsConstants.ORGANIZATION_KEY);
                long patientId = (Long) map.get(JmsConstants.PATIENT_ID_KEY);
                Person thePerson = (Person) map.get(JmsConstants.PERSON_KEY);
                PersonDTO thePersonDTO = (PersonDTO) map.get(JmsConstants.PERSON_DTO_KEY);
                
                String programLevelConsentStatus = (String) map.get(JmsConstants.PROGRAM_LEVEL_CONSENT_STATUS_KEY);
                LoginResult loginResult = (LoginResult) map.get(JmsConstants.LOGIN_RESULT_KEY);

                PatientCommunityEnrollment patientEnrollment = (PatientCommunityEnrollment) map.get(JmsConstants.PATIENT_ENROLLMENT_KEY);
                long communityCareTeamId = (Long) map.get(JmsConstants.COMMUNITY_CARE_TEAM_ID_KEY);
                String oldCareTeamName = (String) map.get(JmsConstants.OLD_CARE_TEAM_NAME_KEY);

                // get handle to updater code
                CoordinatorBrokerContext coordinatorBrokerContext = CoordinatorBrokerContext.getInstance();
                CareTeamPatientUpdater careTeamPatientUpdater = coordinatorBrokerContext.getCareTeamPatientUpdater();
                
                // now call the method
                careTeamPatientUpdater.runPatientUpdateProcess(patientInfo, thePerson, thePersonDTO, organization, patientId, programLevelConsentStatus, 
                        loginResult, patientEnrollment, communityCareTeamId, oldCareTeamName);
                                
            } catch (Exception ex) {
                Logger.getLogger(CareteamServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            logger.warning("Invalid message type in CareteamPatientUpdate queue");
        }

        logger.info("CareTeam_PatientUpdate_Queue: END onMessage");
    }
}
