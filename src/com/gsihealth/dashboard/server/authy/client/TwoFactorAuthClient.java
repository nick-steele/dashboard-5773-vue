
package com.gsihealth.dashboard.server.authy.client;
import com.authy.*;
import com.authy.api.*;
import java.util.HashMap;
import java.util.Map;
/**
 *
 * @author ssingh
 */
public class TwoFactorAuthClient {
    
//https://github.com/authy/authy-java
    private AuthyApiClient client = null;
    private Users users = null;
    private Tokens tokens = null;
    private String countryCode;
    private String key;

    public TwoFactorAuthClient(String key, String countryCode) {

        this.key = key;
        this.countryCode = countryCode;
        client = new AuthyApiClient(key);//GSIHealth Coordinator
        users = client.getUsers();
        tokens = client.getTokens();

    }

    public User registerUser(String email, String cellphone) {

        User user;
        if (countryCode != null) {
            user = users.createUser(email, cellphone, countryCode);
        } else {
            user = users.createUser(email, cellphone);
        }
        return user;
    }

    public Token verifyUser(int authyId, String userToken) {
        Map<String, String> options = new HashMap<String, String>();
        options.put("force", "true");
        Token verification = tokens.verify(authyId, userToken,options);

        return verification;
    }
    public Hash sendSMSRequest(int authyId){
        Map<String, String> options = new HashMap<String, String>();
        options.put("force", "true");
        com.authy.api.Hash resp = users.requestSms(authyId,options);
        return resp;
    }
    public Hash deleteUser(int authyId){
        Hash resp = users.deleteUser(authyId);
        return resp;
    }

}
