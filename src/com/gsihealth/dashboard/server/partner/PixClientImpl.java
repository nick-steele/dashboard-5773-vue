package com.gsihealth.dashboard.server.partner;

import com.gsihealth.dashboard.server.common.BHIXDevModeException;
import com.gsihealth.adapter.service.AdapterServiceObject;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.logging.Logger;
import javax.jms.ConnectionFactory;
import javax.jms.Queue;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Chad Darby
 */
public class PixClientImpl implements PixClient {

    private Logger logger = Logger.getLogger(getClass().getName());
    private static SimpleDateFormat dateOut = new SimpleDateFormat("yyyyMMddHHmmss");
    private Adapter adapter;
//    private String southwestBrooklynOid;
    private boolean sendToBhixFlag;
    private String pixEndpointUrl;
    
    //JIRA 629
    public PixClientImpl() {
        adapter = new Adapter();
    }

    @Override
    public void send(Person thePerson, String transactionType, long communityId, String communityOid, String messageId, String endPoint, ConnectionFactory xdsConnectionFactory, Queue xdsQueue) throws Exception {
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        String defaultCommunityProperty = "default.community.id";
//        southwestBrooklynOid = configurationDAO.getProperty(defaultCommunityProperty, communityId, "auto.consent.org.oid");
        pixEndpointUrl = configurationDAO.getProperty(defaultCommunityProperty, communityId, "bhix.pix.endpoint.url");

        String sendToBhixFlagStr = configurationDAO.getProperty(defaultCommunityProperty, communityId, "dashboard.outbound.communication");
        sendToBhixFlag = Boolean.parseBoolean(sendToBhixFlagStr);
        
        logger.info("\n\nBHIX FLAG = " + sendToBhixFlag + "\n\n");
        
        if (sendToBhixFlag) {
            sendMessage(thePerson, transactionType,communityOid,messageId,endPoint,xdsConnectionFactory,xdsQueue);
            logger.fine("wait for it ..");
            this.sleepForABit(1000);
        } else {
            String message = "Message not sent because of configuration: dashboard.outbound.communication=false or the configuration is missing.";
            throw new BHIXDevModeException(message);
        }
    }
    
    private boolean sleepForABit(long howlong){
         try {
             Thread.sleep(howlong);
         } catch (InterruptedException ex) {
             logger.info("sleep interrupted");
         }
        return true;
    }

    protected void sendMessage(Person thePerson, String transactionType,String communityOid, String messageId, String endPoint, ConnectionFactory xdsConnectionFactory, Queue xdsQueue) throws Exception {
        AdapterServiceObject populatedAdapter = new AdapterServiceObject();

        populatedAdapter.setBirthTime(dateOut.format(thePerson.getDateOfBirth()));
        populatedAdapter.setEuid(thePerson.getEuid());
        populatedAdapter.setGenderCode(thePerson.getGender());
        populatedAdapter.setLid(thePerson.getLocalId());
        populatedAdapter.setPatientName(thePerson.getFirstName());
        populatedAdapter.setPatientMiddle(StringUtils.defaultString(thePerson.getMiddleName()));
        populatedAdapter.setPatientFamily(thePerson.getLastName());
        populatedAdapter.setTelephone(StringUtils.defaultString(thePerson.getTelephone()));

        if (StringUtils.isBlank(thePerson.getStreetAddress2())) {
            populatedAdapter.setStreetAddress(thePerson.getStreetAddress1());
        } else {
            populatedAdapter.setStreetAddress(thePerson.getStreetAddress1()
                    + " " + thePerson.getStreetAddress2());
        }

        populatedAdapter.setCity(thePerson.getCity());
        populatedAdapter.setState(thePerson.getState());
        populatedAdapter.setPostalCode(thePerson.getZipCode());
        populatedAdapter.setSsn(thePerson.getSsn());

        populatedAdapter.setSystemCode(communityOid); //"1.3.6.1.4.1.21367.13.20.5180");

        //These settings are not needed for BHIX Demographic Update
        populatedAdapter.setSender(""); //("doctorx@florida.directproject.org");
        populatedAdapter.setTransactionType(transactionType); //pixquery");
        populatedAdapter.setPixEndpoint(pixEndpointUrl);
        
        populatedAdapter.setXdrFrom(""); //("ranjan@medalliesdirectstag.com");
        populatedAdapter.setXdrTo(""); //("john.blair@medalliesdirectstag.com");

        String response = adapter.send(populatedAdapter,communityOid,messageId,endPoint,xdsConnectionFactory,xdsQueue);

        logger.info(("PIX Client response: " + response));
        // look for something positive in the ack, otherwise fail it 
        if (!StringUtils.containsIgnoreCase(response, "SUCCESS")) {
            throw new Exception("Send failed. Check web service logs for details.");
        }
    }
}
