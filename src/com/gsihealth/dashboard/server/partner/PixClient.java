package com.gsihealth.dashboard.server.partner;

import com.gsihealth.dashboard.server.common.Person;
import javax.jms.ConnectionFactory;
import javax.jms.Queue;

/**
 *
 * @author Chad Darby
 */
public interface PixClient {
    
    public void send(Person person, String transactionType, long communityId, String communityOid, String messageId, String endPoint, ConnectionFactory xdsConnectionFactory, Queue xdsQueue) throws Exception;

    
    
}
