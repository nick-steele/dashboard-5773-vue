package com.gsihealth.dashboard.server.partner;

import com.gsihealth.adapter.service.AdapterService;
import com.gsihealth.adapter.service.AdapterServiceImpl;
import com.gsihealth.adapter.service.AdapterServiceObject;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.ConnectionFactory;
import javax.jms.Queue;

/**
 *
 * @author edwin
 */
public class Adapter {

    private Logger logger = Logger.getLogger(getClass().getName());
    
    private AdapterService adapterService;

    public Adapter() {
        adapterService = new AdapterServiceImpl();
    }

    /**
     *  send populated Adapter to EndPoint specified in XML file
     * 
     * @param populatedAdapter
     * @return 
     */
    public String send(AdapterServiceObject populatedAdapter,String communityOid, String messageId, String endPoint, ConnectionFactory xdsConnectionFactory, Queue xdsQueue) {

        //send populated adapter to target in ccd;
        String ccd = "";
        populatedAdapter.setCcd(ccd);

        String response = "";

        try {
            response = adapterService.getAdapterRequest(populatedAdapter, communityOid, messageId, endPoint, xdsConnectionFactory, xdsQueue);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error: " + e.getMessage(), e);
            response = "fail";
        }

        return response;
    }

    private String staticAdapterTest() {

        //CCD is not needed in BHIX PIX Adapter
        String ccd = "";

        AdapterServiceObject adapterServiceObject = new AdapterServiceObject();

        adapterServiceObject.setBirthTime("19771123");
        adapterServiceObject.setCcd(ccd);
        adapterServiceObject.setCity("Brooklyn");
        adapterServiceObject.setEuid("0000014010");
        adapterServiceObject.setGenderCode("M");
        adapterServiceObject.setLid("1234612");
        adapterServiceObject.setPatientFamily("PathakZZZZ");
        adapterServiceObject.setPostalCode("11228");
        adapterServiceObject.setPatientName("PathakZZZZ");
        adapterServiceObject.setSender("doctorx@florida.directproject.org");
        adapterServiceObject.setState("NY");
        adapterServiceObject.setStreetAddress("XX 86th Street");
        adapterServiceObject.setSystemCode("1.3.6.1.4.1.21367.13.20.5180");
        adapterServiceObject.setTransactionType("pixquery");
        adapterServiceObject.setXdrFrom("ranjan@medalliesdirectstag.com");
        adapterServiceObject.setXdrTo("john.blair@medalliesdirectstag.com");

        AdapterService adapterService = null;
        String response = "";
        try {
            adapterService = new AdapterServiceImpl();
            //response = adapterService.getAdapterRequest(adapterServiceObject);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error: " + e.getMessage(), e);
        }

        return response;
    }
}
