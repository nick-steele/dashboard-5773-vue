package com.gsihealth.dashboard.server.dataloaderutils;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.List;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author Chad Darby
 */
public class UserCommunityAppsDataGenerator {
   
    public static void main(String[] args) throws Exception {

        String file = "samp_user_ids.txt";
        String outputFileName = "output.txt";
        
        PrintWriter out = new PrintWriter(new FileWriter(outputFileName));
        
        // get a list of user ids 
        List<String> data = IOUtils.readLines(new FileReader(file));
        
        // generate inserts for apps 1-9
        for (String temp : data) {
            
            for (int i=1; i <= 9; i++) {
                String message = String.format("insert into connect.user_community_application values (%s, %d, 1, NOW(), null);", temp, i);
                System.out.println(message);
                out.println(message);
            }
        }
        
        out.flush();
        out.close();
    }
    
}
