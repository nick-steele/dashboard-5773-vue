package com.gsihealth.dashboard.server.common;

/**
 *
 * @author Ajay
 */
public class UserLoadResult implements java.io.Serializable {

    private long userId;
    private long communityId;
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserLoadResult() {
    }
    
    public UserLoadResult(long userId, long communityId) {
        this.userId = userId;
        this.communityId = communityId;
    }
    
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }    
}
