package com.gsihealth.dashboard.server.common;

import com.gsihealth.entity.PatientCommunityEnrollment;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import java.util.Date;

/**
 *
 * @author Chad Darby
 */
public class Person implements java.io.Serializable {
    SimplePerson simplePerson;

    private long patientId;
    
    private String careteam;
    
    private String consent;
    
    private long primaryPayerPlanId;
    private long secondaryPayerPlanId;
    
    private long primaryPayerClassId;
    private long secondaryPayerClassId;
    
    private long programHealthHomeId;
    private PatientCommunityEnrollment patientCommunityEnrollment;
    
    private Date patientLastPasswordChanged;
    
    
    public Person() {
        simplePerson = new SimplePerson();
    }
    
    public Person(SimplePerson sp){
        if (sp != null) {
            this.simplePerson = sp;
        } else {
            this.simplePerson = new SimplePerson();
        }
    }

    public Person(SimplePerson simplePerson, PatientCommunityEnrollment patientCommunityEnrollment) {
        if (simplePerson != null) {
        this.simplePerson = simplePerson;
        }else{
            this.simplePerson = new SimplePerson();
        }
        this.patientCommunityEnrollment = patientCommunityEnrollment;
    }
    
    

    public Person(String euid, long patientId, String firstName, String lastName, String middleName, Date dateOfBirth, String gender, 
            String streetAddress1, String streetAddress2, String city, String state, String zipCode, String telephone, String telephoneExtension, 
            String careteam, String consent, String title, String phoneBus, String mothermn, String languageCode, String age, String ageUnits, 
            String genderCode, String county, String country, String localId, String localOrg, String ssn, String driversLicense, String nationality, 
            String language, String dateOfdeath, String mStatus, String motherName, String race, String ethnic,  
            long primaryPayerPlanId, long secondaryPayerPlanId, long primaryPayerClassId, long secondaryPayerClassId, long programHealthHomeId, 
            PatientCommunityEnrollment patientEnrollment, Date patientLastPasswordChanged,String minor, String email) {
        this.patientId = patientId;
        this.simplePerson = new SimplePerson();
        this.simplePerson.setPatientEuid(euid);
        
        this.simplePerson.setFirstName(firstName);
        this.simplePerson.setLastName(lastName);
        this.simplePerson.setMiddleName(middleName);
        this.simplePerson.setDateOfBirth(dateOfBirth);
        this.simplePerson.setGenderCode(gender);
        this.simplePerson.setStreetAddress1(streetAddress1);
        this.simplePerson.setStreetAddress2(streetAddress2);
        this.simplePerson.setCity(city);
        this.simplePerson.setState(state);
        this.simplePerson.setZipCode(zipCode);
        this.simplePerson.setPhoneHome(telephone);
        this.simplePerson.setTelephoneExtension(telephoneExtension);
        
        this.simplePerson.setPhoneBus(phoneBus);
        this.simplePerson.setMothermn(mothermn);
        this.simplePerson.setLanguageCode(languageCode);
        this.simplePerson.setAge(age);
        this.simplePerson.setAgeUnits(ageUnits);
        this.simplePerson.setGenderCode(genderCode);
        this.simplePerson.setMinor(minor) ;
        this.simplePerson.setCounty(county);
        this.simplePerson.setCountry(country);
        this.simplePerson.setLocalId(localId);
        this.simplePerson.setLocalOrg(localOrg);
        this.simplePerson.setSSN(ssn);
        this.simplePerson.setLicenseNum(driversLicense);
        this.simplePerson.setNationality(nationality);
        this.simplePerson.setLanguage(language);
        this.simplePerson.setDateOfdeath(dateOfdeath);
        this.simplePerson.setmStatus(mStatus);
        this.simplePerson.setMotherName(motherName);
        this.simplePerson.setRace(race);
        this.simplePerson.setEthnicityCode(ethnic);
        this.simplePerson.setTitle(title);
        this.simplePerson.setEmail(email);
        
        this.careteam = careteam;
        this.consent = consent;
        
        this.primaryPayerPlanId = primaryPayerPlanId;
        this.secondaryPayerPlanId = secondaryPayerPlanId;
        this.primaryPayerClassId = primaryPayerClassId;
        this.secondaryPayerClassId = secondaryPayerClassId;
        this.programHealthHomeId = programHealthHomeId;
        this.patientCommunityEnrollment = patientEnrollment;
        this.patientLastPasswordChanged = patientLastPasswordChanged;
    }
    
    public SimplePerson getSimplePerson() {
        return simplePerson;
    }

    public void setSimplePerson(SimplePerson simplePerson) {
        this.simplePerson = simplePerson;
    }

    public String getCity() {
        return  this.simplePerson.getCity();
    }

    public void setCity(String city) {
        this.simplePerson.setCity(city);
    }

    public Date getDateOfBirth() {
        return  this.simplePerson.getDateOfBirth();
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.simplePerson.setDateOfBirth(dateOfBirth);
    }

    public String getFirstName() {
        return  this.simplePerson.getFirstName();
    }

    public void setFirstName(String firstName) {
        this.simplePerson.setFirstName(firstName);
    }

    public String getGender() {
        return  this.simplePerson.getGenderCode();
    }

    public void setGender(String gender) {
        this.simplePerson.setGenderCode(gender);
    }

    public String getLastName() {
        return  this.simplePerson.getLastName();
    }

    public void setLastName(String lastName) {
        this.simplePerson.setLastName(lastName);
    }

    public String getState() {
        return  this.simplePerson.getState();
    }

    public void setState(String state) {
        this.simplePerson.setState(state);
    }

    public String getStreetAddress1() {
        return  this.simplePerson.getStreetAddress1();
    }

    public void setStreetAddress1(String streetAddress1) {
        this.simplePerson.setStreetAddress1(streetAddress1);
    }

    public String getStreetAddress2() {
        return  this.simplePerson.getStreetAddress2() ;
    }

    public void setStreetAddress2(String streetAddress2) {
        this.simplePerson.setStreetAddress2(streetAddress2);
    }

    public String getZipCode() {
        return  this.simplePerson.getZipCode();
    }

    public void setZipCode(String zipCode) {
        this.simplePerson.setZipCode(zipCode);
    }

    public String getEuid() {
        return  this.simplePerson.getPatientEuid();
    }

    public void setEuid(String euid) {
        this.simplePerson.setPatientEuid(euid);
    }

    public String getMiddleName() {
        return  this.simplePerson.getMiddleName();
    }

    public void setMiddleName(String middleName) {
        this.simplePerson.setMiddleName(middleName);
    }

    public String getTelephone() {
        return  this.simplePerson.getPhoneHome();
    }

    public void setTelephone(String telephone) {
        this.simplePerson.setPhoneHome(telephone);
    }
    
       public String getMinor() {
        return  this.simplePerson.getMinor();
    }

    public void setMinor(String minor) {
        this.simplePerson.setMinor(minor);
    }

    //Add getter and setters from Simple Person Class

    public String getAge() {
        return  this.simplePerson.getAge();
    }

    public void setAge(String age) {
        this.simplePerson.setAge(age);
    }

    public String getAgeUnits() {
        return  this.simplePerson.getAgeUnits();
    }

    public void setAgeUnits(String ageUnits) {
        this.simplePerson.setAgeUnits(ageUnits);
    }

    public String getCountry() {
        return  this.simplePerson.getCountry();
    }

    public void setCountry(String country) {
        this.simplePerson.setCountry(country);
    }

    public String getCounty() {
        return  this.simplePerson.getCounty();
    }

    public void setCounty(String county) {
        this.simplePerson.setCounty(county);
    }

    public String getDateOfdeath() {
        return  this.simplePerson.getDateOfdeath();
    }

    public void setDateOfdeath(String dateOfdeath) {
        this.simplePerson.setDateOfdeath(dateOfdeath);
    }


    public String getGenderCode() {
        return  this.simplePerson.getGenderCode();
    }

    public void setGenderCode(String genderCode) {
        this.simplePerson.setGenderCode(genderCode);
    }

    public String getLanguage() {
        return  this.simplePerson.getLanguage();
    }

    public void setLanguage(String language) {
        this.simplePerson.setLanguage(language);
    }

    public String getLanguageCode() {
        return  this.simplePerson.getLanguageCode();
    }

    public void setLanguageCode(String languageCode) {
        this.simplePerson.setLanguageCode(languageCode);
    }

    public String getLocalId() {
        return  this.simplePerson.getLocalId();
    }

    public void setLocalId(String localId) {
        this.simplePerson.setLocalId(localId);
    }

    public String getLocalOrg() {
        return  this.simplePerson.getLocalOrg();
    }

    public void setLocalOrg(String localOrg) {
        this.simplePerson.setLocalOrg(localOrg);
    }

    public String getmStatus() {
        return  this.simplePerson.getmStatus();
    }

    public void setmStatus(String mStatus) {
        this.simplePerson.setmStatus(mStatus);
    }

    public String getMotherName() {
        return  this.simplePerson.getMotherName();
    }

    public void setMotherName(String motherName) {
        this.simplePerson.setMotherName(motherName);
    }

    public String getMothermn() {
        return  this.simplePerson.getMothermn();
    }

    public void setMothermn(String mothermn) {
        this.simplePerson.setMothermn(mothermn);
    }

    public String getNationality() {
        return  this.simplePerson.getNationality();
    }

    public void setNationality(String nationality) {
        this.simplePerson.setNationality(nationality);
    }


    public String getPhoneBus() {
        return  this.simplePerson.getPhoneBus();
    }

    public void setPhoneBus(String phoneBus) {
        this.simplePerson.setPhoneBus(phoneBus);
    }

    public String getRace() {
        return  this.simplePerson.getRace();
    }

    public void setRace(String race) {
        this.simplePerson.setRace(race);
    }

    public String getSsn() {
        return  this.simplePerson.getSSN();
    }

    public void setSsn(String ssn) {
        this.simplePerson.setSSN(ssn);
    }

    public String getTitle() {
        return  this.simplePerson.getTitle();
    }

    public void setTitle(String title) {
        this.simplePerson.setTitle(title);
    }

    public String getDriversLicense() {
        return  this.simplePerson.getLicenseNum();
    }

    public void setDriversLicense(String driversLicense) {
        this.simplePerson.setLicenseNum(driversLicense);
    }
    
    public String getEmail() {
    	return this.simplePerson.getEmail();
    }
    
    public void setEmail(String email) {
    	this.simplePerson.setEmail(email);
    }

    public String getCareteam() {
        return careteam;
    }

    public void setCareteam(String careteam) {
        this.careteam = careteam;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public PatientCommunityEnrollment getPatientCommunityEnrollment() {
        return patientCommunityEnrollment;
    }

    public void setPatientCommunityEnrollment(PatientCommunityEnrollment patientEnrollment) {
        this.patientCommunityEnrollment = patientEnrollment;
    }

    public String getConsent() {
        return consent;
    }

    public void setConsent(String consent) {
        this.consent = consent;
    }

    public String getTelephoneExtension() {
        return  this.simplePerson.getTelephoneExtension();
    }

    public void setTelephoneExtension(String telephoneExtension) {
        this.simplePerson.setTelephoneExtension(telephoneExtension);
    }

    public String getEthnic() {
         return  this.simplePerson.getEthnicityCode() ;
    }

    public void setEthnic(String ethnic) {
        this.simplePerson.setEthnicityCode(ethnic);
    }

    public String getMedicareId() {
        return this.simplePerson.getMedicareId();
    }

    public void setMedicareId(String id) {
    	this.simplePerson.setMedicareId(id);
    }

    public String getMedicaidId() {
        return this.simplePerson.getMedicaidId();
    }

    public void setMedicaidId(String id) {
    	this.simplePerson.setMedicaidId(id);
    }
    
    public long getPrimaryPayerPlanId() {
        return primaryPayerPlanId;
    }

    public void setPrimaryPayerPlanId(long primaryPayerPlanId) {
        this.primaryPayerPlanId = primaryPayerPlanId;
    }

    public long getSecondaryPayerPlanId() {
        return secondaryPayerPlanId;
    }

    public void setSecondaryPayerPlanId(long secondaryPayerPlanId) {
        this.secondaryPayerPlanId = secondaryPayerPlanId;
    }

    public long getPrimaryPayerClassId() {
        return primaryPayerClassId;
    }

    public void setPrimaryPayerClassId(long primaryPayerClassId) {
        this.primaryPayerClassId = primaryPayerClassId;
    }

    public long getSecondaryPayerClassId() {
        return secondaryPayerClassId;
    }

    public void setSecondaryPayerClassId(long secondaryPayerClassId) {
        this.secondaryPayerClassId = secondaryPayerClassId;
    }

    /**
     * @return the programHealthHomeId
     */
    public long getProgramHealthHomeId() {
        return programHealthHomeId;
    }

    /**
     * @param programHealthHomeId the programHealthHomeId to set
     */
    public void setProgramHealthHomeId(long programHealthHomeId) {
        this.programHealthHomeId = programHealthHomeId;
    }

    public Date getPatientLastPasswordChanged() {
        return patientLastPasswordChanged;
    }

    public void setPatientLastPasswordChanged(Date patientLastPasswordChanged) {
        this.patientLastPasswordChanged = patientLastPasswordChanged;
    }

    @Override
    public String toString() {
        return "Person{" + "simplePerson=" + simplePerson + ", patientId=" + patientId + '}';
    }
    
    


}
