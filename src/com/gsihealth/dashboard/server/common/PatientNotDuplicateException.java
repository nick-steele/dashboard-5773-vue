/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.common;

/**
 *
 * @author User
 */

public class PatientNotDuplicateException extends Exception {

    public PatientNotDuplicateException(Throwable cause) {
        super(cause);
    }

    public PatientNotDuplicateException(String message, Throwable cause) {
        super(message, cause);
    }

    public PatientNotDuplicateException(String message) {
        super(message);
    }

    public PatientNotDuplicateException() {
    }

    
}
