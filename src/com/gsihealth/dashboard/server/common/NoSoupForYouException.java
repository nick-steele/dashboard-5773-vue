/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.common;

import com.gsihealth.dashboard.common.PortalException;

/**
 *
 * @author Beth.Boose
 */
public class NoSoupForYouException extends PortalException {

    public NoSoupForYouException() {
    }

    public NoSoupForYouException(String message) {
        super(message);
    } 
}
