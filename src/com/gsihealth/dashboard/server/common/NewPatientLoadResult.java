package com.gsihealth.dashboard.server.common;

/**
 *
 * @author Chad.Darby
 */
public class NewPatientLoadResult implements java.io.Serializable {

    private long patientId;
    private long communityId;
    private String errorMessage;
    private boolean isError;

    public NewPatientLoadResult() {
    }

    public NewPatientLoadResult(long patientId, long communityId) {
        this.patientId = patientId;
        this.communityId = communityId;
    }
    
    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean getIsError() {
        return isError;
    }

    public void setIsError(boolean isError) {
        this.isError = isError;
    }
}
