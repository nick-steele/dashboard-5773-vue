package com.gsihealth.dashboard.server.common;

/**
 *
 * @author Chad Darby
 */
public class DuplicateException extends Exception {

    public DuplicateException(Throwable cause) {
        super(cause);
    }

    public DuplicateException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicateException(String message) {
        super(message);
    }

    public DuplicateException() {
    }
}
