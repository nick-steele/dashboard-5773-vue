/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.common;

/**
 *
 * @author Beth.Boose
 */
public class NonexistentCommunityException extends Exception {
    public NonexistentCommunityException(String message) {
        super(message);
    }

    public NonexistentCommunityException(Throwable cause) {
        super(cause);
    }

    public NonexistentCommunityException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
