/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.common;

/**
 *
 * @author Vinay
 */
public class PatientEngDuplicateEmailAddressException extends Exception{
    
    
    
    public PatientEngDuplicateEmailAddressException(String message) {
        super(message);
    }

    public PatientEngDuplicateEmailAddressException() {
    }
}
