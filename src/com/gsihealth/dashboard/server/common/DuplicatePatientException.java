package com.gsihealth.dashboard.server.common;

import com.gsihealth.patientmdminterface.entity.DuplicateCheckProcessResult;

/**
 *
 * @author Chad Darby
 */
public class DuplicatePatientException extends Exception {

    private DuplicateCheckProcessResult duplicateCheckProcessResult;

    public DuplicatePatientException(Throwable cause) {
        super(cause);
    }

    public DuplicatePatientException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicatePatientException(String message) {
        super(message);
    }

    public DuplicatePatientException(String message, DuplicateCheckProcessResult duplicateCheckProcessResult) {
        super(message);
        this.duplicateCheckProcessResult = duplicateCheckProcessResult;
    }

    public DuplicateCheckProcessResult getDuplicateCheckProcessResult() {
        return duplicateCheckProcessResult;
    }

}
