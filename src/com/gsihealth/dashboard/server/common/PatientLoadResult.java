package com.gsihealth.dashboard.server.common;

/**
 *
 * @author Chad.Darby
 */
public class PatientLoadResult implements java.io.Serializable {

    private long patientId;
    private long communityId;

    public PatientLoadResult() {
    }
    
    public PatientLoadResult(long patientId, long communityId) {
        this.patientId = patientId;
        this.communityId = communityId;
    }
    
    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }

    public long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(long communityId) {
        this.communityId = communityId;
    }    
}
