package com.gsihealth.dashboard.server.common;

/**
 *
 * @author Chad Darby
 */
public class PatientResult {

    private long patientId;
    private String patientEuid;

    public PatientResult() {
    }

    public PatientResult(long patientId, String patientEuid) {
        this.patientId = patientId;
        this.patientEuid = patientEuid;
    }

    public String getPatientEuid() {
        return patientEuid;
    }

    public void setPatientEuid(String patientEuid) {
        this.patientEuid = patientEuid;
    }

    public long getPatientId() {
        return patientId;
    }

    public void setPatientId(long patientId) {
        this.patientId = patientId;
    }
}
