package com.gsihealth.dashboard.server.common;

/**
 * Exception for BHIX dev mode messaging
 * 
 * @author Chad Darby
 */
public class BHIXDevModeException extends Exception {

    public BHIXDevModeException(String message) {
        super(message);
    }

    public BHIXDevModeException(Throwable cause) {
        super(cause);
    }

    public BHIXDevModeException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
