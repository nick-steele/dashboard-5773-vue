/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.common;

/**
 *
 * @author Vinay
 * This exception is for check duplicate  Medicaid Medicare ID
 */
public class DuplicatePatientMedicaidMedicareIDException extends Exception {

    private String ssn;
    private String orgName;
    private String status;
    private long primaryMedicaidMedicareID;
    private long secMedicaidMedicareID;

    public DuplicatePatientMedicaidMedicareIDException() {
    }

    public DuplicatePatientMedicaidMedicareIDException(String ssn, long primaryMedicaidMedicareID, long secMedicaidMedicareID, String orgName, String status) {
        this.ssn = ssn;
        this.primaryMedicaidMedicareID = primaryMedicaidMedicareID;
        this.secMedicaidMedicareID = secMedicaidMedicareID;
        this.orgName = orgName;
        this.status = status;
    }

    public DuplicatePatientMedicaidMedicareIDException(Throwable cause) {
        super(cause);
    }

    public DuplicatePatientMedicaidMedicareIDException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicatePatientMedicaidMedicareIDException(String message) {
        super(message);
    }

    public String getSsn() {
        return ssn;
    }

    public String getOrgName() {
        return orgName;
    }

    public String getStatus() {
        return status;
    }

    public long getPrimaryMedicaidMedicareID() {
        return primaryMedicaidMedicareID;
    }

    public long getSecMedicaidMedicareID() {
        return secMedicaidMedicareID;
    }
}
