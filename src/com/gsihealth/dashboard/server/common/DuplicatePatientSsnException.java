package com.gsihealth.dashboard.server.common;

/**
 *
 * @author Chad Darby
 */
public class DuplicatePatientSsnException extends Exception {

    public DuplicatePatientSsnException(Throwable cause) {
        super(cause);
    }

    public DuplicatePatientSsnException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicatePatientSsnException(String message) {
        super(message);
    }

    public DuplicatePatientSsnException() {
    }

    
}
