package com.gsihealth.dashboard.server.hisp;

import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author Chad Darby
 */
public class HispClientUtils {

    /**
     * Generate a direct address for the user
     *
     * @param email
     * @param directDomain
     * @return
     */
    public static String generateDirectAddress(String email, String directDomain) {
        String result = null;

        String[] parts = email.split("@");
        String part1 = parts[0];

        // now work on second half after the "@" symbol
        int position = parts[1].lastIndexOf(".");

        String secondHalf = parts[1].substring(0, position);

        // replace "." with "_"
        secondHalf = secondHalf.replace(".", "_");

        result = part1 + "_" + secondHalf + "@" + directDomain;

        return result.toLowerCase();
    }

    /**
     * For JAMES, the user name is not a complete email address. Rather, all
     * email addresses of the form <user>@<domain> will be delivered to this
     * account by default. So here, we'll just strip off the user name before
     * the
     * @ symbol
     *
     * @param directAddress
     * @return
     */
    public static String getUserName(String directAddress) {

        String[] data = directAddress.split("@");
        String user = data[0];

        return user;
    }

    /*
     public static String generateJames3DirectAddress(String email, String directDomain) {
     String result = null;

     String[] parts = email.split("@");
     String part1 = parts[0];
     result = part1 + "@" + directDomain;

     return result;
     }
     */
    public static String buildJmxUrl(String host, int port) {
        String template = "service:jmx:rmi://%1$s/jndi/rmi://%1$s:%2$d/jmxrmi";

        String result = String.format(template, host, port);

        return result;
    }

    public static String getDirectDomain(long communityId) throws PortalException {

        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        
        String directDomain = null;
        String jamesServerVersion = configurationDAO.getProperty(communityId, "hisp.james.server.version", "james3");

        // get the correct direct domain based on James version
        if (StringUtils.equalsIgnoreCase(jamesServerVersion, HispClient.JAMES2_VERSION)) {
            directDomain = configurationDAO.getProperty(communityId, "hisp.direct.domain");
        } else if (StringUtils.equalsIgnoreCase(jamesServerVersion, HispClient.JAMES3_VERSION)) {
            directDomain = configurationDAO.getProperty(communityId, "hisp.james3.direct.domain");
        } else {
            throw new PortalException("Unknown james hisp version.");
        }

        return directDomain;
    }

    /**
     * Generate a direct address for the user
     *
     * @param email
     * @param directDomain
     * @return
     */

    public static String generatePatientDirectAddress(String patientUserName, String directDomain, String patiemtOrgName) {
        String result = null;

        result = patientUserName + "_" + patiemtOrgName + "@" + directDomain;
        
        return result.toLowerCase();
    }

    /**
     * Generate a Unique direct address for the user
     *
     * @param email
     * @param directDomain
     * @return
     */
    public static String generatePatientUniqueDirectAddress(String patientUserName, String directDomain, String patiemtOrgName, int num) {
        String result = null;

        result = patientUserName + num + "_" + patiemtOrgName + "@" + directDomain;
       
        return result.toLowerCase();
    }
}
