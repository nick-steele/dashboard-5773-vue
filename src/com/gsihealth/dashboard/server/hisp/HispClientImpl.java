package com.gsihealth.dashboard.server.hisp;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.Socket;

import java.util.logging.Logger;
import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import org.apache.james.user.api.UsersRepositoryManagementMBean;

/**
 * Client that communicates with the HISP/JAMES server
 *
 * @author Chad Darby
 */
public class HispClientImpl implements HispClient {

    public static final String ERROR = "Error";
    private static final String NO_SUCH_USER = "No such user";
    private String mailServer;
    private int adminPort;
    private String adminUserId;
    private String adminPassword;
    private Logger logger = Logger.getLogger(getClass().getName());
    //James 3 components
    private String USERSREPOSITORY_OBJECT_NAME = "org.apache.james:type=component,name=usersrepository";
    private String DOMAIN_OBJ_NAME = "org.apache.james:type=component,name=domainlist";
    private String jmxUrlStr;

    /**
     * Constructor
     *
     * @param mailServer
     * @param adminPort
     * @param adminUserId
     * @param adminPassword
     */
    public HispClientImpl(String mailServer, int adminPort, String adminUserId, String adminPassword) {
        this.mailServer = mailServer;
        this.adminPort = adminPort;
        this.adminUserId = adminUserId;
        this.adminPassword = adminPassword;
    }

    /**
     * Send commands to login into the JAMES server
     *
     * @param in
     * @param out
     * @throws Exception
     */
    private void login(Reader in, PrintWriter out) throws Exception {

        int c = 0;

        // send user id
        while ((c = in.read()) != ':') {
            System.out.print((char) c);
        }
        System.out.print((char) c);
        c = in.read();
        System.out.print((char) c);
        out.print(adminUserId + "\r\n"); // also tried simply \n or \r
        out.flush();
        Thread.sleep(1000);

        // send admin password
        while ((c = in.read()) != ':') {
            System.out.print((char) c);
        }
        System.out.print((char) c);
        c = in.read();
        System.out.print((char) c);
        out.print(adminPassword + "\r\n");
        out.flush();
    }

    /**
     * Send commands to read up to JAMES prompt
     *
     * @param in
     * @param out
     * @throws Exception
     */
    private void moveToCommandPrompt(Reader in, PrintWriter out) throws Exception {
        int c = 0;

        Thread.sleep(1000);
        String test = "";
        while (test.indexOf("commands") < 0) {
            c = in.read();
            char[] b = new char[1];
            b[0] = (char) c;
            String s = new String(b);
            test += s;
            System.out.print((char) c);
        }
        c = in.read();
        System.out.print((char) c);

    }

    /**
     * Create an account for the user
     *
     * @param directAddress
     * @param password
     * @throws Exception
     */
    public void createAccount(String directAddress, String password) throws Exception {

        String user = HispClientUtils.getUserName(directAddress);
        logger.info("createAccountForJames2 user=" + user);

        String ret = null;
        Socket socket = new Socket(mailServer, adminPort);
        socket.setKeepAlive(true);
        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

        login(in, out);

        moveToCommandPrompt(in, out);

        int c = 0;

        out.print("adduser " + user + " " + password + "\r\n");
        out.flush();
        Thread.sleep(1000);

        String test = "";
        String errorString = "Error adding user " + user;
        while (!test.contains("added") && !test.contains(errorString)) {
            c = in.read();
            char[] b = new char[1];
            b[0] = (char) c;
            String s = new String(b);
            test += s;
        }
        ret = test;

        logger.info("James 2 RESULT = " + ret);

        // close up the session
        out.print("quit\r\n");
        out.flush();
        Thread.sleep(1000);

        socket.close();

        if (ret.contains(ERROR)) {
            throw new Exception(ret);
        }

        logger.info("User: " + user + " added successfully with James 2");
    }

    /**
     * Create an account for the james 3 user
     *
     * @param user
     * @param password
     * @param domain
     * @throws Exception
     */
    public void createAccountForJames3(String user, String password, String domain) throws Exception {
        logger.info("createAccountForJames3 user=" + user + ",  domain=" + domain);

        MBeanServerConnection mbeanServerConn = getMBeanServerConnection();

        ObjectName objname = new ObjectName(USERSREPOSITORY_OBJECT_NAME);

        UsersRepositoryManagementMBean userRepository = MBeanServerInvocationHandler.newProxyInstance(
                mbeanServerConn, objname, UsersRepositoryManagementMBean.class, true);

        userRepository.addUser(user, password);
        logger.info("User: " + user + " added successfully with James 3");
    }

    private MBeanServerConnection getMBeanServerConnection() throws Exception {

        if (jmxUrlStr == null) {
            jmxUrlStr = HispClientUtils.buildJmxUrl(mailServer, adminPort);
        }

        logger.info("Connecting to JMX URL: " + jmxUrlStr);

        JMXServiceURL jmxUrl = new JMXServiceURL(jmxUrlStr);
        JMXConnector jmxConnector = JMXConnectorFactory.connect(jmxUrl, null);
        MBeanServerConnection connection = jmxConnector.getMBeanServerConnection();

        logger.info("Connection successful to JMX URL: " + jmxUrlStr);

        return connection;
    }
}
