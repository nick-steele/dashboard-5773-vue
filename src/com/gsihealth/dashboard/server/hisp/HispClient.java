/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.hisp;

/**
 *
 * @author Chad Darby
 */
public interface HispClient {

    public static final String JAMES2_VERSION = "james2";

    public static final String JAMES3_VERSION = "james3";
    
    public void createAccount(String directAddress, String password) throws Exception;

    public void createAccountForJames3(String user, String password, String domain) throws Exception;
}