package com.gsihealth.dashboard.server.jmx;

import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.management.MBeanServer;
import javax.management.ObjectName;

/**
 * Thread pool monitor MBean
 *
 * @author Chad Darby
 */
public class ThreadPoolMonitor implements ThreadPoolMonitorMBean {

    private static final String THREAD_POOL_MONITOR_BEAN_NAME = "com.gsihealth.dashboard.server.jmx:type=ThreadPoolMonitor";
    private final ThreadPoolExecutor threadPoolExecutor;
    private Logger logger = Logger.getLogger(getClass().getName());

    public ThreadPoolMonitor(ThreadPoolExecutor theThreadPoolExecutor) {
        threadPoolExecutor = theThreadPoolExecutor;
        registerBean();
    }

    public void registerBean() {
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        try {
            ObjectName objectName = new ObjectName(THREAD_POOL_MONITOR_BEAN_NAME);

            if (!mbs.isRegistered(objectName)) {
                logger.info("Registering MBean: " + THREAD_POOL_MONITOR_BEAN_NAME);
                mbs.registerMBean(this, objectName);
                logger.info("Successfully registered MBean.");
            }
            else {
                logger.info("MBean already registered: " + THREAD_POOL_MONITOR_BEAN_NAME + ". So we're not going to register again.");
            }
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error registering JMX MBean", exc);
        }
    }

    public void unregisterBean() {
        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
        try {
            ObjectName objectName = new ObjectName(THREAD_POOL_MONITOR_BEAN_NAME);

            if (mbs.isRegistered(objectName)) {
                logger.info("Unregistering mbean: " + THREAD_POOL_MONITOR_BEAN_NAME);
                mbs.unregisterMBean(objectName);
                logger.info("Successfully unregistered MBean.");
            }
            else {
                logger.info("MBean NOT registered: " + THREAD_POOL_MONITOR_BEAN_NAME + ". So, we're not going to unregister.");                
            }
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error unregistering JMX MBean", exc);
        }
    }

    @Override
    public int getCorePoolSize() {
        return threadPoolExecutor.getCorePoolSize();
    }

    @Override
    public void setCorePoolSize(int corePoolSize) {
        threadPoolExecutor.setCorePoolSize(corePoolSize);
    }

    @Override
    public int getPoolSize() {
        return threadPoolExecutor.getPoolSize();
    }

    @Override
    public int getMaximumPoolSize() {
        return threadPoolExecutor.getMaximumPoolSize();
    }

    @Override
    public void setMaximumPoolSize(int maximumPoolSize) {
        threadPoolExecutor.setMaximumPoolSize(maximumPoolSize);
    }

    @Override
    public int getLargestPoolSize() {
        return threadPoolExecutor.getLargestPoolSize();
    }

    @Override
    public int getActiveCount() {
        return threadPoolExecutor.getActiveCount();
    }

    @Override
    public long getTaskCount() {
        return threadPoolExecutor.getTaskCount();
    }

    @Override
    public long getCompletedTaskCount() {
        return threadPoolExecutor.getCompletedTaskCount();
    }

    @Override
    public long getKeepAliveTime() {
        return threadPoolExecutor.getKeepAliveTime(TimeUnit.MILLISECONDS);
    }

    @Override
    public boolean isShutdown() {
        return threadPoolExecutor.isShutdown();
    }

    @Override
    public int getWorkQueueSize() {
        return threadPoolExecutor.getQueue().size();
    }

    @Override
    public int getWorkQueueRemainingCapacity() {
        return threadPoolExecutor.getQueue().remainingCapacity();
    }
    
    @Override
    public void flushWorkQueue() {
        int count = threadPoolExecutor.getQueue().drainTo(new ArrayList<Runnable>());
        logger.info("Flushed pool. Removed " + count + " tasks");
    } 
}