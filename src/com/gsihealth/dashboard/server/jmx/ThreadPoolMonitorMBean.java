package com.gsihealth.dashboard.server.jmx;

/**
 *
 * @author Chad Darby
 */
public interface ThreadPoolMonitorMBean {

    public String NAME = ThreadPoolMonitorMBean.class.getName();

    public int getCorePoolSize();

    public void setCorePoolSize(int corePoolSize);

    public int getPoolSize();

    public int getMaximumPoolSize();

    public void setMaximumPoolSize(int maximumPoolSize);

    public int getLargestPoolSize();

    public int getWorkQueueSize();

    public int getWorkQueueRemainingCapacity();
    
    public int getActiveCount();

    public long getTaskCount();

    public long getCompletedTaskCount();

    public long getKeepAliveTime();

    public boolean isShutdown();
    
    public void flushWorkQueue();
}