package com.gsihealth.dashboard.server.reports;

import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.WebConstants;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Make sure user is logged in
 * 
 * @author Chad Darby
 */
public class ReportsSecurityFilter implements Filter {

    public void init(FilterConfig fc) throws ServletException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {

        HttpSession session = ((HttpServletRequest) request).getSession();
        
        LoginResult loginResult = (LoginResult) session.getAttribute(WebConstants.LOGIN_RESULT_KEY);
        
        // check if user is logged in
        if (loginResult != null && loginResult.isAuthenticated()) {
            filterChain.doFilter(request, response);
        }
        else {
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/errors/not_logged_in.html");
            requestDispatcher.forward(request, response);
        }        
    }

    public void destroy() {
    }
    
}
