package com.gsihealth.dashboard.server.reports;

import com.gsihealth.dashboard.common.dto.PersonDTO;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 * @author Chad Darby
 */
public class DOHPatientTrackingUtils {

    private static Logger logger = Logger.getLogger(DOHPatientTrackingUtils.class.getName());

    /**
     * Builds a hash map for the patients
     *
     * @param allPatientsList
     * @return
     */
    public static Map<Long, PersonDTO> buildPatientMap(List<PersonDTO> allPatientsList) {

        Map<Long, PersonDTO> patientMap = new HashMap<Long, PersonDTO>();

        for (PersonDTO tempPerson : allPatientsList) {
            long patientId = tempPerson.getPatientEnrollment().getPatientId();
            patientMap.put(patientId, tempPerson);
        }

        return patientMap;
    }

    public static void updateEncounterDates(long patientId, PatientTrackingReportData patientTrackingReportData, Map<Long, EncounterDate> encounterDatesMap) {

        if (encounterDatesMap.containsKey(patientId)) {
            EncounterDate encounterDate = encounterDatesMap.get(patientId);

            patientTrackingReportData.setEarliestContactDate(encounterDate.getEarliestDateOfContact());
            patientTrackingReportData.setLatestContactDate(encounterDate.getLatestDateOfContact());
        }
    }

    public static void updateInitialAssessmentDates(long patientId, PatientTrackingReportData patientTrackingReportData, Map<Long, Date> initialAssessmentDatesMap) {
        
        if (initialAssessmentDatesMap.containsKey(patientId)) {            
            Date theDate = initialAssessmentDatesMap.get(patientId);
            patientTrackingReportData.setInitialAssessmentDate(theDate);
        }
    }

    public static void updateEncounterModeCounts(long patientId, PatientTrackingReportData patientTrackingReportData, Map<Long, EncounterMode> encounterModeCountsMap) {
        if (encounterModeCountsMap.containsKey(patientId)) {
            EncounterMode encounterMode = encounterModeCountsMap.get(patientId);
            
            int personCount = encounterMode.getPersonCount();            
            patientTrackingReportData.setPersonCount(personCount);

            int phoneCount = encounterMode.getPhoneCount();            
            patientTrackingReportData.setPhoneCount(phoneCount);

            int mailCount = encounterMode.getMailCount();            
            patientTrackingReportData.setMailCount(mailCount);            
        }
    }

    public static void updateCareManagementMmisId(PersonDTO person, PatientTrackingReportData patientTrackingReportData, Map<Long, String> mmisMap) {
        
        long orgId = person.getPatientEnrollment().getOrgId();
        
        if (mmisMap.containsKey(orgId)) {
            String mmisId = mmisMap.get(orgId);
            
            patientTrackingReportData.setCareManagementMmisId(mmisId);
        }
    }
    
}
