package com.gsihealth.dashboard.server.reports;

/**
 *
 * @author Chad Darby
 */
public interface ReportConstants {

    public static final String ADMIN_REPORTS_DIR_KEY = "reports.admin.dir";
    public static final String PATIENT_REPORTS_DIR_KEY = "reports.patient.dir";
    public static final String PATIENT_TRACKING_SHEET_REPORTS_DIR_KEY = "reports.patient.tracking.sheet.dir";
    public static final String REPORTS_DIR="/WEB-INF/reports/patient/";
    public static final String ADMIN_REPORTS_DIR="/WEB-INF/reports/admin/";
    public static final String PATIENT_TRACKING_REPORTS_DIR="/WEB-INF/reports/patient-tracking-sheet/";
}
