package com.gsihealth.dashboard.server.reports;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Chad Darby
 */
public class ReportsCleanupTimerTask extends TimerTask {

    private Logger logger = Logger.getLogger(getClass().getName());
    private List<File> reportDirectories;
    
    public ReportsCleanupTimerTask(List<File> theReportDirectories) {
    
        reportDirectories = theReportDirectories;
    }
    
    @Override
    public void run() {
        
        logger.info("Starting ReportsCleanupTimerTask");
        
        for (File tempDir : reportDirectories) {
            try {
                logger.info("Cleaning directory: " + tempDir);
                FileUtils.cleanDirectory(tempDir);
            } catch (IOException exc) {
                logger.log(Level.SEVERE, "Error cleaning reports dir: " + tempDir, exc);
            }
        }
        
        logger.info("Finished ReportsCleanupTimerTask");    
    }
    
}
