package com.gsihealth.dashboard.server.reports;

/**
 *
 * @author Chad Darby
 */
public class EncounterMode {

    private int phoneCount;
    private int personCount;
    private int mailCount;

    public int getPhoneCount() {
        return phoneCount;
    }

    public void setPhoneCount(int phoneCount) {
        this.phoneCount = phoneCount;
    }

    public int getPersonCount() {
        return personCount;
    }

    public void setPersonCount(int personCount) {
        this.personCount = personCount;
    }

    public int getMailCount() {
        return mailCount;
    }

    public void setMailCount(int mailCount) {
        this.mailCount = mailCount;
    }
    
    public void addMailCount(int val) {
        mailCount += val;
    }
    
    public void addPhoneCount(int val) {
        phoneCount += val;
    }
    
    public void addPersonCount(int val) {
        personCount += val;
    }
}
