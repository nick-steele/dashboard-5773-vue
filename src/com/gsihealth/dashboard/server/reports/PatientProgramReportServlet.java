package com.gsihealth.dashboard.server.reports;

import com.gsihealth.dashboard.client.reports.PatientProgramReportPanel;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.AccessLevelConstants;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.util.EncryptUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.CommunityContexts;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.CommunityCareteamDAO;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.dashboard.server.dao.PatientProgramDAO;
import com.gsihealth.dashboard.server.dao.ReportDAO;
import com.gsihealth.dashboard.server.dao.UserPatientConsentDAO;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import com.gsihealth.dashboard.server.util.PatientProgramTemplate;
import com.gsihealth.entity.PatientProgramSheet;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ssingh
 */
public class PatientProgramReportServlet extends HttpServlet {

    private ReportDAO reportDAO;
    private PatientManager patientManager;
    private Logger logger = Logger.getLogger(getClass().getName());
    private String reportsDir;
    private CommunityCareteamDAO careteamDAO;
    private PatientProgramDAO patientProgramDAO;
    private PatientProgramReportPanel patientProgramReportPanel;
    private UserPatientConsentDAO userPatientConsentDAO;
    private static String FIRST_NAME_PLACE_HOLDER = "-First Name-";
    private static final String LAST_NAME_PLACE_HOLDER = "*-Last Name-";
    private SearchCriteria searchCriteria;
    private long communityId;

    @Override
    public void init() throws ServletException {
        ServletContext application = getServletContext();
        reportDAO = (ReportDAO) getServletContext().getAttribute(WebConstants.REPORT_DAO_KEY);
        careteamDAO = (CommunityCareteamDAO) getServletContext().getAttribute(WebConstants.COMMUNITY_CARE_TEAM_DAO_KEY);
        patientProgramDAO = (PatientProgramDAO) application.getAttribute(WebConstants.PATIENT_PROGRAM_DAO_KEY);
        userPatientConsentDAO = (UserPatientConsentDAO) application.getAttribute(WebConstants.USER_PATIENT_CONSENT_DAO_KEY);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        logger.info("Starting Enrolled Patient Program  Report...");

        try {

            // get communityId
            HttpSession session = request.getSession();
            LoginResult loginResult = (LoginResult) session.getAttribute(WebConstants.LOGIN_RESULT_KEY);
            communityId = loginResult.getCommunityId();
            String communityOid=loginResult.getCommunityOid();
            patientManager = this.getPatientManager(communityId);
            logger.info("Parsing the arguments");
            String args = request.getParameter("p");
            String decodedArgs = EncryptUtils.decode(args);

            String[] decodedArgsArray = decodedArgs.split(",");

            if (decodedArgsArray.length != 5) {
                throw new IllegalArgumentException("Invalid arguments");
            }
            Long orgId = Long.parseLong(decodedArgsArray[0]);
            Long userId = Long.parseLong(decodedArgsArray[1]);
            String accessLevelStr = decodedArgsArray[2];
            String lastName = decodedArgsArray[3];
            String firstName = decodedArgsArray[4];
            long accessLevelId = Long.parseLong(accessLevelStr);
            
             ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
            boolean healthHomeRequired = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "health.home.required", "false"));
            String healthHomeLabel = configurationDAO.getProperty(communityId, "health.home.label", "Health Home");
            String endReasonLabel = configurationDAO.getProperty(communityId, "program.end.reason.label", "Program End");
            
            if (StringUtils.isNotBlank(lastName) && StringUtils.equals(lastName, LAST_NAME_PLACE_HOLDER)) {
                lastName = "";

            }

            if (StringUtils.isNotBlank(firstName) && StringUtils.equals(firstName, FIRST_NAME_PLACE_HOLDER)) {
                firstName = "";

            }
            if (!lastName.isEmpty()) {
                searchCriteria = new SearchCriteria();
                searchCriteria.setLastName(lastName);
                if (!firstName.isEmpty()) {
                    searchCriteria.setFirstName(firstName);
                }

            } else {
                searchCriteria = null;
            }

            logger.info("Running report for Patient Enrolled in Programs: userId=" + userId + ", accessLevelId=" + accessLevelId);

            reportsDir = ReportUtils.getReportsDir(communityId, ReportConstants.PATIENT_REPORTS_DIR_KEY);
            logger.info("reports DIR = " + reportsDir);


            /*
             * create template
             */
            logger.info("Loading Patient Program template...");
            PatientProgramTemplate programTemplate = new PatientProgramTemplate(healthHomeRequired,healthHomeLabel,endReasonLabel);

            // programTemplate.createHeaderColumns();

            logger.info("Getting patients...");
            boolean isPowerUser = accessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID;

            //  List<PersonDTO> allPatientsList = new ArrayList<PersonDTO>();
            List<PatientProgramSheet> patientProgramSheets = null;
            //   if (searchCriteria != null) {
            //     logger.info("Search Criteria is not null");

            //   boolean hasDemographics = EnrollmentUtils.hasDemographicData(searchCriteria);

            // if (hasDemographics) {
            //   logger.info(" if hasDemographics : ");

                    if (accessLevelId == WebConstants.POWER_USER_ACCESS_LEVEL_ID) {
                        logger.info("  looking patients for power user : ");
                        //  allPatientsList = filterPatientsWithDemographics(communityId, searchCriteria);
                        patientProgramSheets = reportDAO.findPatientProgramRecordsForPowerUser(communityId, searchCriteria, patientManager, -1, -1);
                        addDamographicDataToPatientProgramSheet(patientProgramSheets, patientManager, communityOid);
                    } else {
                        logger.info(" looking patients for non power user : ");
                        patientProgramSheets = reportDAO.findPatientProgramRecordsForNonPowerUser(communityId, userId, searchCriteria, patientManager, -1, -1);
                        addDamographicDataToPatientProgramSheet(patientProgramSheets, patientManager, communityOid);
                        //allPatientsList = filterdPatientsWithDemographics(communityId, userId, searchCriteria);
                    }
                    //  setPatientProgramAndStatus(allPatientsList);

            // }
            /*  } else {
                logger.info("Search Criteria is null");
                Map<String, PatientCommunityEnrollment> patientEnrollments = new HashMap<String, PatientCommunityEnrollment>();

                if (isPowerUser) {
                    patientEnrollments = reportDAO.findTotalPatientsForPowerUser(communityId);
                    allPatientsList = findGenericCandidatesEnrolledPatients(patientEnrollments);
                    setPatientProgramAndStatus(allPatientsList);

                } else {
                    patientEnrollments = reportDAO.findPatientsForNotPowerUser(userId, communityId);
                    allPatientsList = findGenericCandidatesEnrolledPatients(patientEnrollments);
                    setPatientProgramAndStatus(allPatientsList);

                }
            }*/
            logger.info(" No. of patients: " + patientProgramSheets.size());
            /*
             * populate the template with patients
             */
            logger.info("Populating template...");
            programTemplate.populate(patientProgramSheets);

            // create directory if needed
//            String pathToReportDir = getServletContext().getRealPath(reportsDir);
            File path = new File(reportsDir);
            if (!path.exists()) {
                path.mkdirs();
            }

            /* 
             * save file to reports directory
             */
            String fileName = "patient_program_sheet_report_" + ReportUtils.getTimeStamp() + ".xlsx";
            String pathToReport =reportsDir + fileName;

            logger.info("Saving file: " + pathToReport);
            programTemplate.getWorkbook().write(new FileOutputStream(pathToReport));

            /*
             * forward client to report
             */
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.addHeader("content-disposition",
                    "attachment; filename=" + fileName);
            logger.info("Forwarding to client");
            RequestDispatcher requestDispatcher = request.getRequestDispatcher(ReportConstants.REPORTS_DIR + fileName);
            requestDispatcher.forward(request, response);

            logger.info("Loading completed.");

        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error generating report", exc);
            throw new ServletException("Error generating report", exc);
        }
    }
//
//    private List<PersonDTO> findGenericCandidatesEnrolledPatients(Map<String, PatientCommunityEnrollment> patientEnrollments) throws Exception {
//
//        List<PersonDTO> searchResults = new ArrayList<PersonDTO>();
//        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
//
//        // get the patient enrollment
//        Set<Map.Entry<String, PatientCommunityEnrollment>> entries = patientEnrollments.entrySet();
//
//        for (Map.Entry<String, PatientCommunityEnrollment> tempEntry : entries) {
//            String euid = tempEntry.getKey();
//            PatientCommunityEnrollment tempPatientEnrollment = tempEntry.getValue();
//
//            Person tempPerson = new Person(patientManager.getPatient(euid));
//            if (tempPerson != null) {
//                tempPerson.setPatientCommunityEnrollment(tempPatientEnrollment);
//
//                // check if patient id is in patient_community_careteam
//                long patientId = tempPatientEnrollment.getPatientId();
//                boolean exists = careteamDAO.doesPatientCommunityCareteamExist(patientId);
//
//                // if so, then get care team id
//                // - get care team name
//                if (exists) {
//                    CommunityCareteam communityCareteam = careteamDAO.findCommunityCareteamForPatient(patientId);
//                    String careteamName = communityCareteam.getName();
//                    tempPerson.setCareteam(careteamName);
//                }
//
//                // passed all the filters...so add to the search results
//                PersonDTO personDto = mapper.map(tempPerson, PersonDTO.class);
//
//                searchResults.add(personDto);
//            }
//        }
//        return searchResults;
//    }
//
//    protected List<PersonDTO> filterPatientsWithDemographics(long communityId, SearchCriteria searchCriteria) throws MappingException, NumberFormatException, Exception {
//
//        logger.info(" power user filterEnrolledPatientsWithDemographics CareteamServiceImpl  :" + communityId);
//
//        List<PersonDTO> searchResults = new ArrayList<PersonDTO>();
//
//        // setup
//        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
//
//        List<SimplePerson> results = patientManager.findDashboardPatient(searchCriteria);
//
//        // get the patient enrollment
//        for (SimplePerson sp : results) {
//            String euid = sp.getPatientEuid();
//            logger.info(" euid :" + sp.getPatientEuid() + ":LASTNAME:" + sp.getLastName());
//            Person person;
//            try {
//                PatientCommunityEnrollment pce = reportDAO.findPatientEnrollment(euid, communityId);
//                if (pce != null) {
//                    person = new Person(sp, pce);
//                    PersonDTO personDto = mapper.map(sp, PersonDTO.class);
//                    searchResults.add(personDto);
//                }
//
//            } catch (NoResultException exc) {
//
//                logger.warning("===========>   Did not find person in our local database...not including in search results");
//            }
//        }
//
//        return searchResults;
//    }
//
//    protected List<PersonDTO> filterdPatientsWithDemographics(long communityId, long userId, SearchCriteria searchCriteria) throws MappingException, NumberFormatException, Exception {
//
//        logger.info(" filterEnrolledPatientsWithDemographics CareteamServiceImpl not poweruser  :");
//
//        List<PersonDTO> searchResults = new ArrayList<PersonDTO>();
//
//        // setup
//        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
//
//        List<SimplePerson> results = patientManager.findDashboardPatient(searchCriteria);
//
//        // get the patient enrollment
//        for (SimplePerson sp : results) {
//            String euid = sp.getPatientEuid();
//            try {
//                PatientCommunityEnrollment pce = reportDAO.findPatientEnrollment(euid, communityId);
//                if ((pce != null)) {
//                    long patientId = pce.getPatientId();
//                    // do not include we don't have consent for patient
//                    boolean hasConsent = userPatientConsentDAO.hasConsent(userId, patientId, communityId);
//                    if (!hasConsent) {
//                        logger.fine("SKIP:  user does not have consent. euid=" + euid + ", userId="
//                                + userId + ", patientId=" + patientId);
//                    } else {
//                        Person person = new Person(sp, pce);
//                        PersonDTO personDto = mapper.map(person, PersonDTO.class);
//                        searchResults.add(personDto);
//                    }
//                }
//            } catch (NoResultException exc) {
//                logger.warning("===========>   Did not find person in our local database..."
//                        + "not including in search results: " + exc);
//            }
//        }
//
//        return searchResults;
//    }
//
//    private void saveFile(PatientProgramTemplate programTemplate, String pathToFile) throws IOException {
//
//        FileOutputStream out = new FileOutputStream(pathToFile);
//        programTemplate.getWorkbook().write(out);
//    }
//
//    public ProgramNameDTO convertTODTO(ProgramName programName) {
//        ProgramNameDTO programDTO = new ProgramNameDTO();
//        programDTO.setId(programName.getProgramNamePK().getId());
//        programDTO.setValue(programName.getValue());
//
//        return programDTO;
//    }
//
//    public PatientStatusDTO convert(PatientStatus patientStatus) {
//        PatientStatusDTO statusDTO = new PatientStatusDTO();
//        statusDTO.setId(patientStatus.getPatientStatusPK().getId());
//        statusDTO.setValue(patientStatus.getValue());
//
//        return statusDTO;
//    }
//
//    public void setPatientProgramAndStatus(List<PersonDTO> allPatientsList) {
//
//        try {
//            for (PersonDTO person : allPatientsList) {
//                List<PatientProgramDTO> programList = person.getPatientEnrollment().getPatientProgramName();
//                for (PatientProgramDTO program : programList) {
//                    ProgramName programName = patientProgramDAO.getProgramName(program.getProgramId(), communityId);
//                    ProgramNameDTO prog = convertTODTO(programName);
//                    program.setProgram(prog);
//
//                    PatientStatus status = patientProgramDAO.getPatientStatus(program.getPatientStatus(), communityId);
//                    PatientStatusDTO patientStatus = convert(status);
//                    program.setStatus(patientStatus);
//                }
//            }
//        } catch (Exception exc) {
//            logger.log(Level.SEVERE, "Error getting associated programs", exc);
//
//        }
//    }
//
    protected PatientManager getPatientManager(Long communityId){
        ServletContext servletContext = getServletContext();
        Map<Long, CommunityContexts> communityContexts =
        ((Map<Long, CommunityContexts>)servletContext.getAttribute(WebConstants.COMMUNITY_SPECIFIC_DATA_KEY));
        return communityContexts.get(communityId).getPatientManager();
    }
    
        /**
     * FIXME : Single entity class for all reports can be used. So, we don't
     * need three different methods of each report.
     */
    private void addDamographicDataToPatientProgramSheet(List<PatientProgramSheet> pps, PatientManager patientManager,String communityOid) throws Exception {
        List<String> patientList = new ArrayList<String>();

        logger.info("Getting patientIds in list..");
        for (PatientProgramSheet temp : pps) {
            patientList.add(temp.getPatientID().toString());
        }
        logger.info("Start retrieving patient demographic data from MDM/Mirth with the help of patientIds...");
//        Map<String, SimplePerson> demographicData = patientManager.getMappedPatientsDirectQuery(em, patientList, communityOid);
         Map<String, SimplePerson> demographicData=  reportDAO.getPatientsFromMdm(patientManager, patientList, communityOid);
        ListIterator listItr = pps.listIterator();

        logger.info("Adding demographic data to patientProgramSheet..");
        while (listItr.hasNext()) {
            PatientProgramSheet ppsTemp = (PatientProgramSheet) listItr.next();
            SimplePerson simplePerson = demographicData.get(ppsTemp.getPatientID().toString());
            if (simplePerson == null) {
                listItr.remove();
            } else {
                convertMdmSbrToSP(simplePerson, ppsTemp);
            }

        }
    }
    
     /**
     *
     * @param sbr
     * @param patientProgramSheet
     * @throws Exception This method sets the demographic data to non-consented
     * sheet
     */
    private void convertMdmSbrToSP(SimplePerson sbr, PatientProgramSheet patientProgramSheet) throws Exception {
        if (sbr != null && patientProgramSheet != null) {
            patientProgramSheet.setFirstName(sbr.getFirstName());
            patientProgramSheet.setLastName(sbr.getLastName());
            patientProgramSheet.setDob(sbr.getDateOfBirth());
            patientProgramSheet.setGender(sbr.getGenderCode());

        } else {
            logger.info("Some thing wrong with Id : ...Looks like we have patient_id in PCE,"
                    + " but corresponding lid does not present in sbyn.enterprise" + patientProgramSheet.getPatientID());
        }
    }
}
