/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.reports;

import java.util.Date;

/**
 *
 * @author Chad Darby
 */
public class EncounterDate {

    private Date earliestDateOfContact;
    private Date latestDateOfContact;

    public Date getEarliestDateOfContact() {
        return earliestDateOfContact;
    }

    public void setEarliestDateOfContact(Date earliestDateOfContact) {
        this.earliestDateOfContact = earliestDateOfContact;
    }

    public Date getLatestDateOfContact() {
        return latestDateOfContact;
    }

    public void setLatestDateOfContact(Date latestDateOfContact) {
        this.latestDateOfContact = latestDateOfContact;
    }
        
}
