package com.gsihealth.dashboard.server.reports;

import com.gsihealth.entity.User;
import com.gsihealth.entity.UserCommunityOrganization;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.UserDAO;
import java.io.File;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.IOUtils;

/**
 * @author Satyendra Singh
 *
 * Handling export get requests
 */
public class UserReportServlet extends HttpServlet {

    private Logger logger = Logger.getLogger(getClass().getName());
    private String reportsDir;
    private UserDAO userDAO;

    @Override
    public void init() throws ServletException {
        userDAO = (UserDAO) getServletContext().getAttribute(WebConstants.USER_DAO_KEY);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            // get communityId
            HttpSession session = request.getSession();
            LoginResult loginResult = (LoginResult) session.getAttribute(WebConstants.LOGIN_RESULT_KEY);
            long communityId = loginResult.getCommunityId();
            User loggedOnUser = (User) session.getAttribute(WebConstants.USER_KEY);
            UserCommunityOrganization uco = loggedOnUser.getUserCommunityOrganization(communityId);
          
            StringBuilder data = new StringBuilder();
            logger.info("Starting UserReportServlet");
            
            logger.info("Getting users...");
            List<User> userList = userDAO.findUsers(uco, null, -1, -1);

            reportsDir = ReportUtils.getReportsDir(communityId, ReportConstants.ADMIN_REPORTS_DIR_KEY);
            
            // display headers
            data.append("User ID/Email,First Name,Middle Name,Last Name,Address1,Address2,City,State,ZipCode,Organization,Access Level,User Role,Last Access Date");
            data.append(IOUtils.LINE_SEPARATOR);

            
            // display each user
            logger.info("Building data line for each user...");
            for (User user : userList) {
                StringBuilder userData = getUserData(user, communityId);
                data.append(userData);
                data.append(IOUtils.LINE_SEPARATOR);
            }

            // create directory if needed
//            String pathToReportDir = getServletContext().getRealPath(reportsDir);            
            File path = new File(reportsDir);
            if (!path.exists()) {
                path.mkdirs();
            }
            
            // save file to PTS reports directory
            // get a random seed
            int seed = ReportUtils.getSeed();
            String fileName = "user_list_report_" + ReportUtils.getTimeStamp() + "_" + seed + ".csv";
            String pathToReport = reportsDir + fileName;

            logger.info("Saving file: " + pathToReport);
            ReportUtils.saveFile(data.toString(), pathToReport);

            // forward client to report
            logger.info("Forwarding to client");
            RequestDispatcher requestDispatcher = request.getRequestDispatcher(ReportConstants.ADMIN_REPORTS_DIR + fileName);
            requestDispatcher.forward(request, response);

            logger.info("User Report complete");
            
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error generating report", e);
            throw new ServletException("File was not generated", e);
        }
    }

    protected StringBuilder getUserData(User user, Long communityId) {

        StringBuilder userData = new StringBuilder();
        UserCommunityOrganization uco = user.getUserCommunityOrganization(communityId);
        String accessLevelDesc = "unavailable";
        String roleName = "unavailable";
        String organizationName = "unavailable";
        String email = ReportUtils.getDefaultStringEscapeCsv(user.getEmail());
        String firstName = ReportUtils.getDefaultStringEscapeCsv(user.getFirstName());
        String middleName = ReportUtils.getDefaultStringEscapeCsv(user.getMiddleName());
        String lastName = ReportUtils.getDefaultStringEscapeCsv(user.getLastName());
        String address1 = ReportUtils.getDefaultStringEscapeCsv(user.getStreetAddress1());
        String address2 = ReportUtils.getDefaultStringEscapeCsv(user.getStreetAddress2());
        String city = ReportUtils.getDefaultStringEscapeCsv(user.getCity());
        String state = ReportUtils.getDefaultStringEscapeCsv(user.getState());
        String zipCode = ReportUtils.getDefaultStringEscapeCsv(user.getZipCode());
        if (uco != null) {
            organizationName = uco.getOrganizationCwd().getOrganizationCwdName();
            accessLevelDesc = ReportUtils.getDefaultStringEscapeCsv(uco.getAccessLevel().getAccessLevelDesc());
            roleName = ReportUtils.getDefaultStringEscapeCsv(uco.getRole().getRoleName());
        }
        Object lastLoginDate = (user.getLastSuccessfulLoginDate() == null) ? "" : user.getLastSuccessfulLoginDate();

        userData.append(email+",");
        userData.append(firstName + ",");
        userData.append(middleName + ",");
        userData.append(lastName + ",");
        userData.append(address1 + ",");
        userData.append(address2 + ",");
        userData.append(city + ",");
        userData.append(state + ",");
        userData.append(zipCode + ",");
        userData.append(organizationName + ",");
        userData.append(accessLevelDesc + ",");
        userData.append(roleName + ",");
        userData.append(lastLoginDate);

        return userData;
    }

    
}
