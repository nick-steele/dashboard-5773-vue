package com.gsihealth.dashboard.server.reports;

import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.common.util.EncryptUtils;
import com.gsihealth.dashboard.common.util.SsnUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;
import com.gsihealth.dashboard.entity.dto.ProgramHealthHomeDTO;
import com.gsihealth.dashboard.server.CommunityContexts;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.dao.CommunityCareteamDAO;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.dashboard.server.dao.ReportDAO;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import com.gsihealth.entity.PatientCommunityEnrollment;
import com.gsihealth.entity.PatientReportSheet;
import com.gsihealth.entity.ProgramHealthHome;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.apache.commons.lang.time.StopWatch;

/**
 * @author Satyendra Singh
 *
 * Handling export get requests
 */
public class EnrolledPatientsReportServlet extends HttpServlet {

    private ReportDAO reportDAO;
    private CommunityCareteamDAO careteamDAO;
    private PatientManager patientManager;
    private Logger logger = Logger.getLogger(getClass().getName());
    private String reportsDir;
    private Map<Long, ProgramHealthHome> programHealthHomeMap = null;
    private boolean patientEngagementEnable;

    @Override
    public void init() throws ServletException {
        ServletContext application = getServletContext();

        careteamDAO = (CommunityCareteamDAO) getServletContext().getAttribute(WebConstants.COMMUNITY_CARE_TEAM_DAO_KEY);
        reportDAO = (ReportDAO) getServletContext().getAttribute(WebConstants.REPORT_DAO_KEY);
        programHealthHomeMap = new HashMap<Long, ProgramHealthHome>();
        programHealthHomeMap = getProgramNameHealthHomeMap();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        logger.info("Starting Enrolled Patients Report...");

        try {

            // get communityId
            HttpSession session = request.getSession();
            LoginResult loginResult = (LoginResult) session.getAttribute(WebConstants.LOGIN_RESULT_KEY);
            long communityId = loginResult.getCommunityId();
            patientManager = this.getPatientManager(communityId);
            logger.info("Parsing the arguments");
            String args = request.getParameter("p");
            String decodedArgs = EncryptUtils.decode(args);

            String[] decodedArgsArray = decodedArgs.split(",");

            if (decodedArgsArray.length != 2) {
                throw new IllegalArgumentException("Invalid arguments");
            }

            Long userId = Long.parseLong(decodedArgsArray[0]);
            String accessLevelStr = decodedArgsArray[1];
            long accessLevelId = Long.parseLong(accessLevelStr);

            logger.info("Running report for EnrolledPatients: userId=" + userId + ", accessLevelId=" + accessLevelId);

            ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
            
            patientEngagementEnable = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "patient.engagement.enable"));
            reportsDir = ReportUtils.getReportsDir(communityId, ReportConstants.PATIENT_REPORTS_DIR_KEY);
            logger.info("reports DIR = " + reportsDir);

            // CSV
            StringBuilder data = new StringBuilder();
           List<PatientReportSheet> consentedPatientSheets = null;
            logger.info("Getting patients...");
            consentedPatientSheets = reportDAO.findEnrolledPatients(communityId,userId, accessLevelId);
            addDamographicDataToConsentedSheet(consentedPatientSheets, patientManager, loginResult.getCommunityOid());
//            List<PersonDTO> enrollmentServicesList = findGenericCandidatesEnrolledPatients(patientEnrollments , loginResult.getCommunityOid());

            logger.info("Building report data...");
            if (patientEngagementEnable) {
                data.append("Patient ID,First Name,Middle Name,Last Name,SSN,Address1,Address2,City,State,ZipCode,Organization,Source,Acuity Score,Enrollment Status,Care Team,Patient Messaging,Primary Payer Class,Primary Payer Plan,Medicaid/Medicare/Payer ID");
            } else {
                data.append("Patient ID,First Name,Middle Name,Last Name,SSN,Address1,Address2,City,State,ZipCode,Organization,Source,Acuity Score,Enrollment Status,Care Team,Primary Payer Class,Primary Payer Plan,Medicaid/Medicare/Payer ID");
            }
            data.append(IOUtils.LINE_SEPARATOR);

            //2. Iterate and get deails of user in comma separation
//            for (PersonDTO personDTO : enrollmentServicesList) {
//                data.append(getPatientData(personDTO));
//                data.append(IOUtils.LINE_SEPARATOR);
//            }
       for (PatientReportSheet consentedPatientSheet : consentedPatientSheets) {
                data.append(getPatientData(consentedPatientSheet));
                data.append(IOUtils.LINE_SEPARATOR);
            }

            // save file to reports directory
            // get a random seed
            int seed = ReportUtils.getSeed();
            String fileName = "consented_patients_report_" + ReportUtils.getTimeStamp() + "_" + seed + ".csv";
            
            // create directory if needed
            logger.info("reportsDir" +reportsDir);

            File path = new File(reportsDir);
            if (!path.exists()) {
                path.mkdirs();
            }

//            String pathToReport = getServletContext().getRealPath(reportsDir + fileName);
            String pathToReport = reportsDir + fileName;
            logger.info("Saving file: " + pathToReport);
            logger.fine("file contents");
            logger.fine(data.toString());
            ReportUtils.saveFile(data.toString(), pathToReport);
           
            // forward client to report
            logger.info("Forwarding to client");
            RequestDispatcher requestDispatcher = request.getRequestDispatcher(ReportConstants.REPORTS_DIR + fileName);
            requestDispatcher.forward(request, response);

            logger.info("Enrolled Patients Report complete");

        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error generating report", e);
            e.printStackTrace();
            throw new ServletException("File was not generated", e);
        }
    }

    protected String getSource(PatientEnrollmentDTO patientEnrollment) {
        // source
        String euid = patientEnrollment.getPatientEuid();
        String systemCode = null;
        try {
            systemCode = patientManager.getSystemCode(euid);
        } catch (Exception ex ) {
            logger.warning("systemCode not found " + ex);
        }
        String source = ReportUtils.getUserFriendlySource(systemCode);
        return source;
    }

//    private List<PersonDTO> findGenericCandidatesEnrolledPatients(Map<String, PatientCommunityEnrollment> patientEnrollments , String communityOid) throws Exception {
//
//        List<PersonDTO> searchResults = new ArrayList<PersonDTO>();
//        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
//
//        // get the patient enrollment
//        Set<Map.Entry<String, PatientCommunityEnrollment>> entries = patientEnrollments.entrySet();
//        StopWatch tempStopWatch = new StopWatch();
//        tempStopWatch.start();
//        logger.info("################# Web service calls start ##############");
//        List<String> patientIds = new ArrayList<>();
//        if(patientEnrollments.keySet() != null){
//            patientIds.addAll(patientEnrollments.keySet());
//        }
//       
//        Map<String, SimplePerson> simplePersonMapFromMPI = reportDAO.getPatientsFromMdm(patientManager,patientIds,communityOid);
//        
//        
//        for (Map.Entry<String, PatientCommunityEnrollment> tempEntry : entries) {
//            String euid = tempEntry.getKey();
//            PatientCommunityEnrollment tempPatientEnrollment = tempEntry.getValue();
//
//
//            Person tempPerson = null;
//
//
//            try {
//                //double tempAge =  PropertyUtils.getAge( simplePersonMapFromMPI.get(euid).getDateOfBirth());
//                tempPerson = new Person(simplePersonMapFromMPI.get(euid));
//                //tempPerson.setAge(tempAge);
//
//            } catch (Exception ex) {
//                ex.printStackTrace();
//                logger.log(Level.WARNING, "MDM do not have record for patient EUID:" + euid);
//
//            }
//            if (tempPerson != null ) {
//                tempPerson.setPatientCommunityEnrollment(tempPatientEnrollment);
//                // if so, then get care team id
//                // - get care team name
//                if (tempPatientEnrollment.getPatientCommunityCareteam() != null &&
//                        tempPatientEnrollment.getPatientCommunityCareteam().getCommunityCareteam() != null ) {
//                    tempPerson.setCareteam(tempPatientEnrollment.getPatientCommunityCareteam().getCommunityCareteam().getName());
//                }
//                PersonDTO personDto = mapper.map(tempPerson, PersonDTO.class);
//                searchResults.add(personDto);
//            }
//        }
//        tempStopWatch.stop();
//        logger.info("################# Web service calls end ##############  "  + (tempStopWatch.getTime() / 1000.0) + ", secs");
//        return searchResults;
//    }

    private StringBuilder getPatientData(PatientReportSheet consentedSheet) {
        StringBuilder data = new StringBuilder();

//        PatientEnrollmentDTO patientEnrollment = personDTO.getPatientEnrollment();

        data.append(ReportUtils.getDefaultStringEscapeCsv(String.valueOf(consentedSheet.getPatientId())) + ",");
        data.append(ReportUtils.getDefaultStringEscapeCsv(consentedSheet.getFirstName()) + ",");
        data.append(ReportUtils.getDefaultStringEscapeCsv(consentedSheet.getMiddleName()) + ",");
        data.append(ReportUtils.getDefaultStringEscapeCsv(consentedSheet.getLastName()) + ",");

        String theSsn = consentedSheet.getSsn();
        if (!StringUtils.isBlank(theSsn)) {
            theSsn = SsnUtils.getMaskedSsn(theSsn);
        }

        data.append(ReportUtils.getDefaultStringEscapeCsv(theSsn) + ",");

        data.append(ReportUtils.getDefaultStringEscapeCsv(consentedSheet.getAddressLine1()) + ",");
        data.append(ReportUtils.getDefaultStringEscapeCsv(consentedSheet.getAddressLine2()) + ",");
        data.append(ReportUtils.getDefaultStringEscapeCsv(consentedSheet.getCity()) + ",");
        data.append(ReportUtils.getDefaultStringEscapeCsv(consentedSheet.getState()) + ",");
        data.append(ReportUtils.getDefaultStringEscapeCsv(consentedSheet.getZipCode()) + ",");

        data.append(ReportUtils.getDefaultStringEscapeCsv(consentedSheet.getOrganization()) + ",");
       
        data.append(ReportUtils.getDefaultStringEscapeCsv(consentedSheet.getSource()) + ",");
        data.append(ReportUtils.getDefaultStringEscapeCsv(consentedSheet.getAcuityScore())+",");
        data.append(ReportUtils.getDefaultStringEscapeCsv(consentedSheet.getEnrollmentStatus()) + ",");
        data.append(ReportUtils.getDefaultStringEscapeCsv(consentedSheet.getCareteam()) + ",");

        if (patientEngagementEnable) {
            data.append(ReportUtils.getDefaultStringEscapeCsv(consentedSheet.getPatientUserActive()) + ",");
        }

//        String payerClassName = ReportUtils.getPayerClassName(personDTO);
        data.append(ReportUtils.getDefaultStringEscapeCsv(consentedSheet.getPrimaryPayerClass()) + ",");

//        String payerPlanName = ReportUtils.getPayerPlanName(personDTO);
        data.append(ReportUtils.getDefaultStringEscapeCsv(consentedSheet.getPrimaryPayerPlan()) + ",");

//        String medicaidMedicarePayerID = ReportUtils.getMedicaidMedicarePayerID(personDTO);
        data.append(ReportUtils.getDefaultStringEscapeCsv(consentedSheet.getmPayerID()) + ",");

        //health Home
//
//        String HealthHomeName = ReportUtils.getHealthHome(personDTO);
//        data.append(ReportUtils.getDefaultStringEscapeCsv(HealthHomeName) + ",");

//        // program Name
//        String programName = personDTO.getPatientCommunityEnrollment().getProgramName();
//        data.append(ReportUtils.getDefaultStringEscapeCsv(programName) + ",");
//
//
//
//        // program effective date
//        Date programEffectiveDate = personDTO.getPatientCommunityEnrollment().getProgramNameEffectiveDate();
//        if (programEffectiveDate != null) {
//            data.append(ReportUtils.getDefaultStringEscapeCsv(getStringProgramDate(programEffectiveDate)) + ",");
//        }
//        // program end date
//        Date programEndDate = personDTO.getPatientCommunityEnrollment().getProgramEndDate();
//        if (programEndDate != null) {
//            data.append(ReportUtils.getDefaultStringEscapeCsv(getStringProgramDate(programEndDate)) + ",");
//        }



        return data;
    }

    private static String getStringProgramDate(Date programDate) {
        SimpleDateFormat programDateFormatter = new SimpleDateFormat("MM/dd/yyyy");
        String programDateStr = programDateFormatter.format(programDate);
        return programDateStr;
    }

    private LinkedHashMap<Long, ProgramHealthHome> getProgramNameHealthHomeMap() {

        List<ProgramHealthHome> programHealthHomeList = reportDAO.findProgramHealthHomeEntities();
        LinkedHashMap<Long, ProgramHealthHome> data = new LinkedHashMap<Long, ProgramHealthHome>();
        for (ProgramHealthHome phh : programHealthHomeList) {
            data.put(phh.getProgramHealthHomePK().getId(), phh);
        }
        return data;
    }

    //add health Home 
    protected void setProgramHealthHome(Person tempPerson, PersonDTO personDto) {

        ProgramHealthHomeDTO programHealthHomeDTO = null;
        if (tempPerson.getPatientCommunityEnrollment().getProgramHealthHomeId() != 0) {
            programHealthHomeDTO = new ProgramHealthHomeDTO();
            ProgramHealthHome healthHome = programHealthHomeMap.get(tempPerson.getPatientCommunityEnrollment().getProgramHealthHomeId());
            programHealthHomeDTO.setHealthHomeName(healthHome.getHealthHomeValue());

            programHealthHomeDTO.setId(healthHome.getProgramHealthHomePK().getId());
            personDto.getPatientEnrollment().setProgramHealthHome(programHealthHomeDTO);

        }
    }
    
    protected PatientManager getPatientManager(Long communityId){
        ServletContext servletContext = getServletContext();
        Map<Long, CommunityContexts> communityContexts = 
        ((Map<Long, CommunityContexts>)servletContext.getAttribute(WebConstants.COMMUNITY_SPECIFIC_DATA_KEY));
        return communityContexts.get(communityId).getPatientManager();
    }
    
     /**
     * FIXME : Single entity class for all reports can be used. So, we don't
     * need three different methods of each report.
     */
    private void addDamographicDataToConsentedSheet(List<PatientReportSheet> consentedPatientSheet,PatientManager patientManager, String communityOid) throws Exception {
        List<String> patientList = new ArrayList<>();

        logger.info("Getting patientIds in list..");
        for (PatientReportSheet temp : consentedPatientSheet) {
            patientList.add(temp.getPatientId().toString());
        }
         logger.info("Start retrieving patient demographic data from MDM/Mirth with the help of patientIds...");
      Map<String, SimplePerson> demographicData=  reportDAO.getPatientsFromMdm(patientManager, patientList, communityOid);

//        Map<String, SimplePerson> demographicData = patientManager.getMappedPatientsDirectQuery(em, patientList, communityOid);

        ListIterator listItr = consentedPatientSheet.listIterator();

        logger.info("Adding demographic data to consentedPatientSheet..");
        while (listItr.hasNext()) {
            PatientReportSheet nonConsentedPatientSheetTemp = (PatientReportSheet) listItr.next();
            SimplePerson simplePerson = demographicData.get(nonConsentedPatientSheetTemp.getPatientId().toString());
            convertMdmSbrToSP(simplePerson, nonConsentedPatientSheetTemp);
        }
    }
      private void convertMdmSbrToSP(SimplePerson sbr, PatientReportSheet consentedPatientSheet) throws Exception {
        if (sbr != null && consentedPatientSheet != null) {
            consentedPatientSheet.setFirstName(sbr.getFirstName());
            consentedPatientSheet.setMiddleName(sbr.getMiddleName());
            consentedPatientSheet.setLastName(sbr.getLastName());
            consentedPatientSheet.setAddressLine1(sbr.getStreetAddress1());
            consentedPatientSheet.setAddressLine2(sbr.getStreetAddress2());
            consentedPatientSheet.setCity(sbr.getCity());
            consentedPatientSheet.setState(sbr.getState());
            consentedPatientSheet.setZipCode(sbr.getZipCode());
            consentedPatientSheet.setSsn(sbr.getSSN());
        } else {
            logger.info("Some thing wrong with Id : ...Looks like we have patient_id in PCE,"
                    + " but corresponding lid does not present in sbyn.enterprise" + consentedPatientSheet.getPatientId());
        }
    }
}
