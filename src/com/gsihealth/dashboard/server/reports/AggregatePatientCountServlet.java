package com.gsihealth.dashboard.server.reports;

import com.gsihealth.dashboard.common.Constants;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.ReportDAO;
import java.io.File;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.IOUtils;

/**
 * Aggregate patient counts
 * 
 * @author Satyendra Singh 
 */
public class AggregatePatientCountServlet extends HttpServlet {

    private Logger logger = Logger.getLogger(getClass().getName());
    private ReportDAO reportDAO;
    private String reportsDir;
    
    @Override
    public void init() throws ServletException {
        ServletContext application = getServletContext();

        reportDAO = (ReportDAO) application.getAttribute(WebConstants.REPORT_DAO_KEY);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        logger.info("Starting  AggregatePatientCountServlet ...");

        try {
            // get communityId
            HttpSession session = request.getSession();
            LoginResult loginResult = (LoginResult) session.getAttribute(WebConstants.LOGIN_RESULT_KEY);
            long communityId = loginResult.getCommunityId();
            
            StringBuilder data = new StringBuilder();

            reportsDir = ReportUtils.getReportsDir(communityId, ReportConstants.PATIENT_REPORTS_DIR_KEY);
        
            //1. get data
            logger.info("Getting patient counts data...");

            int enrollmentCandidates = reportDAO.findPatientEnrollmentInActiveCount(communityId) + reportDAO.findPatientEnrollmentPendingCount(communityId);
            int assignedPatients = reportDAO.findPatientAssignedCount(communityId);
            int enrolledPatients = reportDAO.findPatientEnrollmentCount(communityId);
            
            int totalPatientsWithCompletedCarePlan = 0;
            int totalPatientsWithCompletedAssessment = 0;
            
            data.append("Candidates for Enrollment" + "," + "Patients Assigned to a Care Team"+ ","+ "Patients Enrolled and No Care Team Assignment" + "," + "Patients with a Completed Care Plan" + "," + "Patients with a Completed Assessment");
            data.append(IOUtils.LINE_SEPARATOR);

            //2. Get details
            logger.info("Generating report data...");
            data.append(enrollmentCandidates + "," + assignedPatients+ ","+ enrolledPatients + "," + totalPatientsWithCompletedCarePlan + "," + totalPatientsWithCompletedAssessment);
            data.append(IOUtils.LINE_SEPARATOR);

            // create directory if needed
//            String pathToReportDir = getServletContext().getRealPath(reportsDir);            
            File path = new File(reportsDir);
            if (!path.exists()) {
                path.mkdirs();
            }
            
            // save file to reports directory
            // get a random seed
            int seed = ReportUtils.getSeed();
            String fileName = "total_patient_count_report_" + ReportUtils.getTimeStamp() + "_" + seed + ".csv";
            String pathToReport = reportsDir + fileName;

            logger.info("Saving file: " + pathToReport);
            ReportUtils.saveFile(data.toString(), pathToReport);

            // forward client to report
            logger.info("Forwarding to client");
            RequestDispatcher requestDispatcher = request.getRequestDispatcher(ReportConstants.REPORTS_DIR + fileName);
            requestDispatcher.forward(request, response);

            logger.info("Aggregate Patient Count Report complete");
            
            
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Error generating report", e);
            throw new ServletException("File was not generated", e);
        }

    }
}
