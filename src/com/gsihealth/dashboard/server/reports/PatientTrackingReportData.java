package com.gsihealth.dashboard.server.reports;

import java.util.Date;

/**
 *
 * @author Chad Darby
 */
public class PatientTrackingReportData {
    
    private Date earliestContactDate;
    private Date latestContactDate;
    private Date initialAssessmentDate;
    
    private int mailCount;
    private int phoneCount;
    private int personCount;

    private String careManagementMmisId;
    
    public PatientTrackingReportData() {        
    }

    public Date getEarliestContactDate() {
        return earliestContactDate;
    }

    public void setEarliestContactDate(Date earliestContactDate) {
        this.earliestContactDate = earliestContactDate;
    }

    public Date getInitialAssessmentDate() {
        return initialAssessmentDate;
    }

    public void setInitialAssessmentDate(Date initialAssessmentDate) {
        this.initialAssessmentDate = initialAssessmentDate;
    }

    public Date getLatestContactDate() {
        return latestContactDate;
    }

    public void setLatestContactDate(Date latestContactDate) {
        this.latestContactDate = latestContactDate;
    }

    public int getMailCount() {
        return mailCount;
    }

    public void setMailCount(int mailCount) {
        this.mailCount = mailCount;
    }

    public int getPersonCount() {
        return personCount;
    }

    public void setPersonCount(int personCount) {
        this.personCount = personCount;
    }

    public int getPhoneCount() {
        return phoneCount;
    }

    public void setPhoneCount(int phoneCount) {
        this.phoneCount = phoneCount;
    }

    public String getCareManagementMmisId() {
        return careManagementMmisId;
    }

    public void setCareManagementMmisId(String careManagementMmisId) {
        this.careManagementMmisId = careManagementMmisId;
    }

    
}
