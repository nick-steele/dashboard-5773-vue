package com.gsihealth.dashboard.server.reports;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gsihealth.entity.*;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

import com.gsihealth.dashboard.common.AccessLevelConstants;
import com.gsihealth.dashboard.common.SearchRange;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.common.util.EncryptUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;


import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;
import com.gsihealth.dashboard.entity.dto.ProgramHealthHomeDTO;
import com.gsihealth.dashboard.entity.dto.insurance.PayerClassDTO;
import com.gsihealth.dashboard.server.CommunityContexts;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.dao.*;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import com.gsihealth.dashboard.server.util.ClientTemplate;
import com.gsihealth.dashboard.server.util.NewClientTemplate;
import com.gsihealth.dashboard.server.util.PersonDTOComparator;
import com.gsihealth.dashboard.server.util.PropertyUtils;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import java.io.File;
import java.text.SimpleDateFormat;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.time.StopWatch;

/**
 * @author Satyendra Singh
 *
 * Handling export get requests
 */
public class DOHPatientTrackingSheetReportServlet extends HttpServlet {
    private PatientManager patientManager;
    private OrganizationDAO organizationDAO;
    private OrganizationMmisOidDAO organizationMmisOidDAO;
    private Logger logger = Logger.getLogger(getClass().getName());
    private ReportDAO reportDAO;
    private CarePlanDAO carePlanDAO;
    private PatientEnrollmentHistoryDAO patientEnrollmentHistoryDAO;
    private PersonDTOComparator personDTOComparator;
    private String ptsReportsDir;
    private String disenrollmentCodeTitle;
    private DisenrollmentReasonDAO disenrollmentReasonDAO;
    private UserDAO userDAO;
    private Map<Long, ProgramHealthHome> programHealthHomeMap = null;
    private final String MINIOR_CONSTANT="Minor";
    private ConfigurationDAO configDAO;

    @Override
    public void init() throws ServletException {
        ServletContext servletContext = getServletContext();
        organizationDAO = (OrganizationDAO) servletContext.getAttribute(WebConstants.ORGANIZATION_DAO_KEY);
        organizationMmisOidDAO = (OrganizationMmisOidDAO) servletContext.getAttribute(WebConstants.ORGANIZATION_MMIS_OID_DAO_KEY);
        reportDAO = (ReportDAO) servletContext.getAttribute(WebConstants.REPORT_DAO_KEY);
        carePlanDAO = (CarePlanDAO) servletContext.getAttribute(WebConstants.CARE_PLAN_DAO_KEY);
        patientEnrollmentHistoryDAO = (PatientEnrollmentHistoryDAO) servletContext.getAttribute(WebConstants.PATIENT_ENROLLMENT_HISTORY_DAO_KEY);
        personDTOComparator = new PersonDTOComparator();
        disenrollmentReasonDAO = (DisenrollmentReasonDAO) servletContext.getAttribute(WebConstants.DISENROLLMENT_DAO_DEY);
        userDAO = (UserDAO) servletContext.getAttribute(WebConstants.USER_DAO_KEY);
        configDAO=(ConfigurationDAO)servletContext.getAttribute(WebConstants.CONFIGURATION_DAO_KEY);
//        disenrollmentCodeTitle = (String) getServletContext().getAttribute(WebConstants.DISENROLLMENT_CODE_TITLE);

        programHealthHomeMap = new HashMap<Long, ProgramHealthHome>();
        programHealthHomeMap=getProgramNameHealthHomeMap();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
           
        try {
            /*
             * Parse the args
             * the parameter p has the arguments that are encoded as hex.
             * each value is command delimited
             */
            Date fromDate=null;
            Date toDate=null;
            SearchRange<Date> search=null;

            logger.info("Parsing the arguments");
            String args = request.getParameter("p");
             String fromDateText = request.getParameter("fromDate");
              String toDateText = request.getParameter("toDate");
            String decodedArgs = EncryptUtils.decode(args);

            String[] decodedArgsArray = decodedArgs.split(",");

            if (decodedArgsArray.length != 5) {
                throw new IllegalArgumentException("Invalid arguments");
            }
            
            if(fromDateText!=null&&!fromDateText.isEmpty() && toDateText!=null&&!toDateText.isEmpty()){
                SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
           fromDate= format.parse(fromDateText +  " 00:00:00");
           toDate= format.parse(toDateText  + " 23:59:59" );
             search=new SearchRange<Date>(fromDate, toDate);
            }

            Long organizationId = Long.parseLong(decodedArgsArray[0]);
            long accessLevelId = Long.parseLong(decodedArgsArray[1]);
            String oid = decodedArgsArray[2];
            Long userId = Long.parseLong(decodedArgsArray[3]);
            long communityId=Long.parseLong(decodedArgsArray[4]);
            System.out.println("DOHPatient Tracking Sheet CommunityId"+communityId);

            /* 
             * Hiding the column NYSID for UHC
             */         
            ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
            boolean uhcNYSIDFlag = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "patient.tracking.sheet.hide.nysid.column", "false"));
            boolean CHHFlag = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "childrens.health.home"));
           int minorAge=Integer.parseInt(configurationDAO.getProperty(communityId, "minor.age", "18"));
            logger.info("NYSID Flag:" + uhcNYSIDFlag);
            disenrollmentCodeTitle=configurationDAO.getProperty(communityId, "disenrollment.code.title");
            String primaryCMALabel = configurationDAO.getProperty(communityId, "organization.primary.cma.label");
                        
            ptsReportsDir = ReportUtils.getReportsDir(communityId, ReportConstants.PATIENT_TRACKING_SHEET_REPORTS_DIR_KEY);
            
            User userFromDAO = userDAO.findUser(userId);
            Long userCommunityId = userFromDAO.getUserCommunityOrganization(communityId).getCommunity().getCommunityId();
            patientManager = this.getPatientManager(communityId);
            logger.info("decodedArgs: organizationId=" + organizationId + ", accessLevelId=" + accessLevelId + ", oid=" + oid + ", userId=" + userId + ",communityId" + communityId);
            HttpSession session = request.getSession();
            LoginResult loginResult = (LoginResult) session.getAttribute(WebConstants.LOGIN_RESULT_KEY);
            String communityOid=loginResult.getCommunityOid();
            
            SqlConfigurationDAO sqlConfigurationDAO = ConfigurationContext.getInstance().getSqlConfigurationDAO();
            String powerUserQuery = sqlConfigurationDAO.getProperty(communityId, "power.user.pts.query");
            String superUserQuery = sqlConfigurationDAO.getProperty(communityId, "super.user.pts.query");
            String localAdminQuery = sqlConfigurationDAO.getProperty(communityId, "local.admin.user.pts.query");
            String configProgramId = configurationDAO.getProperty(communityId, "program.id");
            Long programId = Long.parseLong(configProgramId);
            /*
             * create template
             */
            logger.info("Loading DOHPatientTracking template...");

            /*
             * Using new constructor of NewClientTemplate for passing disenrollmentCodeTitle for PTS column Disenrollment Reason Codes
             */
            NewClientTemplate clientTemplate = new NewClientTemplate(uhcNYSIDFlag,CHHFlag, disenrollmentCodeTitle, primaryCMALabel);

//            clientTemplate.createTopColums();
            clientTemplate.createHeaderColumns();

            /* 
             * getting OrganizationMMIS oid
             */
            logger.info("Getting OrganizationMMIS...");
            OrganizationCwd theOrg = organizationDAO.findOrganization(organizationId,communityId);
            String orgName = theOrg.getOrganizationCwdName();

            String mmisId = "";
            OrganizationMmisOid organizationMmisOid = organizationMmisOidDAO.findOrganizationMmisOid(oid,communityId);
            if (organizationMmisOid != null) {
                mmisId = organizationMmisOid.getMmisId();
            }

            Map<String, PatientCommunityEnrollment> candidatePatients = new HashMap<String, PatientCommunityEnrollment>();
            Map<String, PatientCommunityEnrollment> enrolledPatients = new HashMap<String, PatientCommunityEnrollment>();
            List<PatientTrackingSheet> ptsRecords=new ArrayList<PatientTrackingSheet>();
            

            /*
             * Get patients: enrolled and candidates
             * LOCAL ADMIN or SUPER USER or POWER USER
             */
            boolean isPowerUser = accessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID;
            boolean isSuperUser = accessLevelId == AccessLevelConstants.SUPER_USER_ACCESS_LEVEL_ID;

            logger.info("Getting patients...");
            if(search!=null){ 
            logger.info("search Criteria"+search.getFrom()+"---"+search.getTo());
            }
            if (isPowerUser) {
                ptsRecords=reportDAO.findPTSRecordsForPowerUser(communityId, programId, search, powerUserQuery);
                addDamographicDataToPTS(ptsRecords, patientManager, minorAge, communityOid);
                /*enrolledPatients = reportDAO.findAllEnrolledPatients(communityId);
                candidatePatients = reportDAO.findAllCandidates(communityId);*/
            } 
            else if (isSuperUser) {
              
                ptsRecords=reportDAO.findPTSRecordsForSuperUser(communityId, programId, userId, search, superUserQuery);
                addDamographicDataToPTS(ptsRecords, patientManager, minorAge, communityOid);
                /* enrolledPatients = reportDAO.findEnrolledPatients(communityId,userId);
                candidatePatients = reportDAO.findCandidates(communityId,organizationId);*/
            }
            else {
                /*
                 * For Local Admin
                 */
               
                ptsRecords=reportDAO.findPTSRecordsForLocalAdminUser(communityId, programId, userId, search, localAdminQuery);
                addDamographicDataToPTS(ptsRecords, patientManager, minorAge, communityOid);
                /*enrolledPatients = reportDAO.findAllEnrolledPatientsOfOrg(communityId,organizationId);
                candidatePatients = reportDAO.findCandidates(communityId,organizationId);*/
            }
            
            /*List<PersonDTO> enrolledPatientsServiceList = findGenericPatients(enrolledPatients,minorAge);
            List<PersonDTO> candidatePatientsServiceList = findGenericPatients(candidatePatients,minorAge);*/

            // set end date and disenrollment reason code for inactive
            //setEndDateAndDisenrollmentReasonCode(userCommunityId, candidatePatientsServiceList);

            /* 
             * combine the patients
             */
            //List<PersonDTO> allPatientsList = new ArrayList<PersonDTO>();

            //allPatientsList.addAll(enrolledPatientsServiceList);
            //allPatientsList.addAll(candidatePatientsServiceList);
            
            // set Enrollment Status Effective Date
            //setEnrollStatusEffectDateAndOrg(allPatientsList); 

            /* 
             * get TREAT careplan data for all patients and other data elements
             */
            //logger.info("Getting TREAT careplan data for patients...");
            //Map<Long, PatientTrackingReportData> patientTrackingReportDataMap = buildPatientTrackingReportDataMap(allPatientsList,communityId);

            /* 
             * sort the patients
             */
            //logger.info("Sorting patients...");
            //Collections.sort(allPatientsList, personDTOComparator);

            /*
             * populate the template with patients
             */
            Program selectedProg = reportDAO.getProgram(programId, communityId);
            logger.info("PTS Record count:-"+ptsRecords.size());
            logger.info("Populating template...");
            clientTemplate.populate(selectedProg, ptsRecords, orgName, mmisId);

            // create directory if needed
//            String pathToReportDir = getServletContext().getRealPath(ptsReportsDir);            
            File path = new File(ptsReportsDir);
            if (!path.exists()) {
                path.mkdirs();
            }
            
            /* 
             * save file to PTS reports directory
             */
            String fileName = "patient_tracking_sheet_report_" + ReportUtils.getTimeStamp() + ".xlsx";
            String pathToReport = ptsReportsDir + fileName;

            logger.info("Saving file: " + pathToReport);
            saveFile(clientTemplate, pathToReport);

            /*
             * forward client to report
             */
            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.addHeader("content-disposition",
                    "attachment; filename=" + fileName);
            logger.info("Forwarding to client");
            RequestDispatcher requestDispatcher = request.getRequestDispatcher(ReportConstants.PATIENT_TRACKING_REPORTS_DIR + fileName);
            requestDispatcher.forward(request, response);

            logger.info("Loading completed.");

        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error generating report", exc);
            throw new ServletException("Error generating report", exc);
        }
    }

    /**
     * Find care management ids
     *
     * key: org id (not oid) value: mmis id
     *
     * @param communityId
     * @return
     */
    private Map<Long, String> findCareManagementMmisId(long communityId) {
        Map<Long, String> data = new HashMap<Long, String>();

        List<OrganizationMmisOid> organizationMmis = organizationMmisOidDAO.findOrganizationMmisOidEntities(communityId);

        List<OrganizationCwd> orgs = organizationDAO.findOrganizationEntities(communityId);

        // build map based on oids
        Map<String, OrganizationCwd> orgMap = buildOrgMap(orgs);

        Map<Long, String> mmisMap = buildMmisMap(organizationMmis, orgMap);

        return mmisMap;
    }

    private List<PersonDTO> findGenericPatients(Map<String, PatientCommunityEnrollment> patientEnrollments, int minorAge) throws Exception {

        List<PersonDTO> searchResults = new ArrayList<PersonDTO>();
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        // get the patient enrollment
        Set<Map.Entry<String, PatientCommunityEnrollment>> entries = patientEnrollments.entrySet();

        for (Map.Entry<String, PatientCommunityEnrollment> tempEntry : entries) {
            String euid = tempEntry.getKey();
            PatientCommunityEnrollment tempPatientEnrollment = tempEntry.getValue();

            logger.fine("\n\nDOH:  patientManager.getPatient() euid=" + euid);

            try {
                Person tempPerson = new Person(patientManager.getPatient(euid));
//              To set date for PLCS = NULL                 
                if ((tempPatientEnrollment.getProgramLevelConsentStatus()==null)||(!tempPatientEnrollment.getProgramLevelConsentStatus().equalsIgnoreCase("YES"))) {
//                 if (!tempPatientEnrollment.getProgramLevelConsentStatus().equalsIgnoreCase("YES")) {    
                tempPatientEnrollment.setConsentDateTime(null);
                }
                tempPerson.setPatientCommunityEnrollment(tempPatientEnrollment);
                PersonDTO personDto = mapper.map(tempPerson, PersonDTO.class);
                // set payer class
                setPayerClass(tempPatientEnrollment, personDto);
                // if patient is Minor
                setMinor(tempPerson,personDto,minorAge);
//                //Health Home
//                setProgramHealthHome(tempPerson, personDto);
                searchResults.add(personDto);
            } catch (Exception exc) {
                // skip this patient
                logger.log(Level.WARNING, "Error retrieving patient. EUID=" + euid, exc);
            }
        }

        return searchResults;
    }

    private void setMinor(Person person,PersonDTO personDto,int minorAge){
        
         if(PropertyUtils.isMinor(person.getDateOfBirth(),minorAge)){
                    personDto.setMinor(MINIOR_CONSTANT);
                }
         else{
              personDto.setMinor("");
         }
    }
    /**
     * Build a map of report data for each patient
     *
     * key: patientId value: report data for this patient
     *
     * @param allPatientsList
     * @return
     */
    private Map<Long, PatientTrackingReportData> buildPatientTrackingReportDataMap(List<PersonDTO> allPatientsList,long communityId) {
        Map<Long, PatientTrackingReportData> reportDataMap = new HashMap<Long, PatientTrackingReportData>();

       System.out.println("buildPatientTrackingReportDataMap communityId:"+communityId);

        /* 
         *LOAD DATA FROM DATABASE INTO HASH MAPS
         */

        // patient map
        logger.info("loading patient map...");
        Map<Long, PersonDTO> patientMap = DOHPatientTrackingUtils.buildPatientMap(allPatientsList);

        // encounter dates
        logger.info("loading encounter dates map...");
        Map<Long, EncounterDate> encounterDatesMap = carePlanDAO.findEncounterDates(communityId);

        // initial assessment dates
        logger.info("load assessment dates map...");
        Map<Long, Date> initialAssessmentDatesMap = carePlanDAO.findInitialAssessmentDates(communityId);

        // encounter mode counts
        logger.info("loading encounter modes map...");
        // passing props file for encounter Types
        //JIRA 629
        Map<Long, EncounterMode> encounterModeCountsMap = carePlanDAO.findEncounterModeCounts(communityId);

        logger.info("loading care management mmis map...");
        Map<Long, String> mmisMap = this.findCareManagementMmisId(communityId);

        //
        // UPDATE REPORT DATA BASED ON HASHMAPS
        //      

        logger.info("updating report data for all patients...");
        Set<Map.Entry<Long, PersonDTO>> patientMapEntries = patientMap.entrySet();
        for (Map.Entry<Long, PersonDTO> tempPatientMapEntry : patientMapEntries) {

            long patientId = tempPatientMapEntry.getKey();
            PersonDTO person = tempPatientMapEntry.getValue();

            PatientTrackingReportData patientTrackingReportData = new PatientTrackingReportData();

            // update encounter dates
            DOHPatientTrackingUtils.updateEncounterDates(patientId, patientTrackingReportData, encounterDatesMap);

            // update initial assessment dates
            DOHPatientTrackingUtils.updateInitialAssessmentDates(patientId, patientTrackingReportData, initialAssessmentDatesMap);

            // update encounter mode counts: person, phone and mail
            DOHPatientTrackingUtils.updateEncounterModeCounts(patientId, patientTrackingReportData, encounterModeCountsMap);

            // update care management mmis id
            DOHPatientTrackingUtils.updateCareManagementMmisId(person, patientTrackingReportData, mmisMap);

            // store it in the report data map
            reportDataMap.put(patientId, patientTrackingReportData);
        }

        return reportDataMap;
    }

    private Map<String, OrganizationCwd> buildOrgMap(List<OrganizationCwd> orgs) {
        Map<String, OrganizationCwd> theMap = new HashMap<String, OrganizationCwd>();

        for (OrganizationCwd tempOrg : orgs) {
            String oid = tempOrg.getOid();

            theMap.put(oid, tempOrg);
        }

        return theMap;
    }

    /**
     * Build a map for mmis ids
     *
     * key: org id (not oid) value: mmis id
     *
     * @param organizationMmis
     * @param orgMap
     * @return
     */
    private Map<Long, String> buildMmisMap(List<OrganizationMmisOid> organizationMmisList, Map<String, OrganizationCwd> orgMap) {

        Map<Long, String> mmisMap = new HashMap<Long, String>();

        for (OrganizationMmisOid tempOrganizationMmisOid : organizationMmisList) {
            String oid = tempOrganizationMmisOid.getOid();
            String mmisId = tempOrganizationMmisOid.getMmisId();

            if (orgMap.containsKey(oid)) {
                OrganizationCwd theOrg = orgMap.get(oid);

                long orgId = theOrg.getOrgId();

                mmisMap.put(orgId, mmisId);
            }
        }

        return mmisMap;
    }

    private void saveFile(ClientTemplate clientTemplate, String pathToFile) throws IOException {

        FileOutputStream out = new FileOutputStream(pathToFile);
        clientTemplate.getWorkbook().write(out);
    }

    private void saveFile(NewClientTemplate clientTemplate, String pathToFile) throws IOException {

        FileOutputStream out = new FileOutputStream(pathToFile);
        clientTemplate.getWorkbook().write(out);
    }

    private void setPayerClass(PatientCommunityEnrollment enrollment, PersonDTO personDTO) {
        PayerClassDTO primarPayerClassDTO = new PayerClassDTO();
        PayerClassDTO secondayPayerClasDTO = new PayerClassDTO();
        PatientEnrollmentDTO enrollmentDTO = personDTO.getPatientEnrollment();

        if (enrollment.getPrimaryPayerClassId() > 0) {

            primarPayerClassDTO.setId(enrollment.getPrimaryPayerClassId());
            enrollmentDTO.setPrimaryPayerClass(primarPayerClassDTO);
            personDTO.setPatientEnrollment(enrollmentDTO);
        }
        if (enrollment.getSecondaryPayerClassId() > 0) {

            secondayPayerClasDTO.setId(enrollment.getSecondaryPayerClassId());
            enrollmentDTO.setSecondaryPayerClass(secondayPayerClasDTO);
            personDTO.setPatientEnrollment(enrollmentDTO);
        }
    }

    /**
     * Create a map of disenrollment reason codes.
     * 
     * @param communityId
     * @return 
     */
    private Map<String, Long> getDisenrollmentReasons(long communityId) {
        Map<String, Long> results = new HashMap<String, Long>();

        List<DisenrollmentReasons> reasons = disenrollmentReasonDAO.getDisenrollmentReasons(communityId);

        for (DisenrollmentReasons tempDisenrollmentReason : reasons) {

            long id = tempDisenrollmentReason.getDisenrollmentReasonsPK().getId();
            String reason = tempDisenrollmentReason.getReason();

            results.put(reason, id);
        }

        return results;
    }

    /**
     * End Date for inactive patients (date of inactivation)
     */
    private void setEndDateAndDisenrollmentReasonCode(Long userCommunityId, List<PersonDTO> candidatePatientsServiceList) {
        logger.info("Setting end date and disenrollment reason code for inactive patients");

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        Map<String, Long> disenrollmentReasonsMap = getDisenrollmentReasons(userCommunityId);
               
        for (PersonDTO pat : candidatePatientsServiceList) {
            PatientEnrollmentDTO tempPatientEnrollment = pat.getPatientEnrollment();

            if (WebConstants.INACTIVE.equalsIgnoreCase(tempPatientEnrollment.getStatus())) {
                // set end date
                Date endDate = patientEnrollmentHistoryDAO.getEndDate(tempPatientEnrollment.getPatientId());
                pat.setEndDate(endDate);

                // set disenrollment reason code
                Long disenrollmentReasonCodeId = disenrollmentReasonsMap.get(tempPatientEnrollment.getReasonForInactivation());

                tempPatientEnrollment.setDisenrollmentReasonCodeId(disenrollmentReasonCodeId);
            }
        }

        stopWatch.stop();

        logger.info("Finished Setting end date and disenrollment reason code for inactive patients");
        logger.info("Time to set end dates: " + stopWatch.getTime() + " ms");
    }
    
    private LinkedHashMap<Long, ProgramHealthHome> getProgramNameHealthHomeMap() {

        List<ProgramHealthHome> programHealthHomeList = reportDAO.findProgramHealthHomeEntities();
        LinkedHashMap<Long, ProgramHealthHome> data = new LinkedHashMap<Long, ProgramHealthHome>();
        for (ProgramHealthHome phh : programHealthHomeList) {
            data.put(phh.getProgramHealthHomePK().getId(), phh);
        }
        return data;
    }
    
     //add health Home 
      protected void setProgramHealthHome(Person tempPerson, PersonDTO personDto) {
                        
           ProgramHealthHomeDTO programHealthHomeDTO=null;
        if (tempPerson.getPatientCommunityEnrollment().getProgramHealthHomeId() != 0) {
            programHealthHomeDTO = new ProgramHealthHomeDTO();
            ProgramHealthHome healthHome= programHealthHomeMap.get(tempPerson.getPatientCommunityEnrollment().getProgramHealthHomeId());
            programHealthHomeDTO.setHealthHomeName(healthHome.getHealthHomeValue());
            programHealthHomeDTO.setId(healthHome.getProgramHealthHomePK().getId());
            programHealthHomeDTO.setHealthHomeMmisId(healthHome.getHealthHomeMmisId());
            personDto.getPatientEnrollment().setProgramHealthHome(programHealthHomeDTO);
           
        }
    }
      
    private void setEnrollStatusEffectDateAndOrg(List<PersonDTO> allPatientsList){
      
        for (PersonDTO patient : allPatientsList) {
            PatientEnrollmentDTO tempPatientEnrollment = patient.getPatientEnrollment();
            Date enrollmentChangeDate = patientEnrollmentHistoryDAO.getEnrollmentStatusChangeDate(tempPatientEnrollment.getPatientId());
            patient.setEnrollmentChangeDate(enrollmentChangeDate);
            long orgID = tempPatientEnrollment.getOrgId();
            String orgName = organizationDAO.getOrganizationName(orgID);
            patient.setLocalOrg(orgName);
        }
    }
    
    protected PatientManager getPatientManager(Long communityId){
        ServletContext servletContext = getServletContext();
        Map<Long, CommunityContexts> communityContexts = 
        ((Map<Long, CommunityContexts>)servletContext.getAttribute(WebConstants.COMMUNITY_SPECIFIC_DATA_KEY));
        return communityContexts.get(communityId).getPatientManager();
    }
    
    /**
     * FIXME : Single entity class for all reports can be used. So, we don't
     * need three different methods of each report.
     */
    private void addDamographicDataToPTS(List<PatientTrackingSheet> pts, PatientManager patientManager,int minorAge, String communityOid) throws Exception {
        List<String> patientList = new ArrayList<String>();

        logger.info("Getting patientIds in list..");
        for (PatientTrackingSheet temp : pts) {
            patientList.add(temp.getDashboardMRN().toString());
        }
        logger.info("Start retrieving patient demographic data from MDM/Mirth with the help of patientIds...");
         Map<String, SimplePerson> demographicData=  reportDAO.getPatientsFromMdm(patientManager, patientList, communityOid);
//        Map<String, SimplePerson> demographicData = patientManager.getMappedPatientsDirectQuery(em, patientList, communityOid);

        ListIterator listItr = pts.listIterator();

        logger.info("Adding demographic data to patatient tracking sheet..");
        while (listItr.hasNext()) {
            PatientTrackingSheet patientTrackingSheet = (PatientTrackingSheet) listItr.next();
            SimplePerson simplePerson = demographicData.get(patientTrackingSheet.getDashboardMRN().toString());
            convertMdmSbrToSP(simplePerson, patientTrackingSheet, minorAge);
        }
    }

    /**
     *
     * @param sbr
     * @param pts
     * @throws Exception This method sets the demographic data to non-consented
     * sheet
     */
    private void convertMdmSbrToSP(SimplePerson sbr, PatientTrackingSheet pts, int minorAge) throws Exception {
        if (sbr != null && pts != null) {
            pts.setFirstName(sbr.getFirstName());
            pts.setDob(sbr.getDateOfBirth());
            pts.setGender(sbr.getGenderCode());
            if (PropertyUtils.isMinor(sbr.getDateOfBirth(), minorAge)) {
                pts.setMinor("Minor");
            }
            pts.setLastName(sbr.getLastName());
            pts.setAddressLine1(sbr.getStreetAddress1());
            pts.setAddressLine2(sbr.getStreetAddress2());
            pts.setCity(sbr.getCity());
            pts.setState(sbr.getState());
            pts.setZipCode(sbr.getZipCode());
            pts.setPrimePhone(sbr.getPhoneHome());
        } else {
            logger.info("Some thing wrong with Id : ...Looks like we have patient_id in PCE,"
                    + " but corresponding lid does not present in sbyn.enterprise" + pts.getDashboardMRN());
        }
    }
    
}
