package com.gsihealth.dashboard.server.reports;

import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.ProgramHealthHomeDTO;
import com.gsihealth.dashboard.entity.dto.insurance.PayerClassDTO;
import com.gsihealth.dashboard.entity.dto.insurance.PayerPlanDTO;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;

/**
 *
 * @author Chad Darby
 */
public class ReportUtils {

    private static final String GSIHEALTH_OID = "2.16.840.1.113883.3.1358";
    private static final String HINEXT_OID = "2.16.840.1.113883.3.1391";
    private static final DateFormat dateFormatter = new SimpleDateFormat("MMddyyyy_HHmmss");
    
    public static String getUserFriendlySource(String systemCode) {

        String result = null;

        if (StringUtils.equals(GSIHEALTH_OID, systemCode)) {
            result = "Dashboard";
        } else if (StringUtils.equals(HINEXT_OID, systemCode)) {
            result = "Care Plan";
        }
        else {
            result = "";
        }

        return result;
    }
    
    public static String getTimeStamp() {
        return dateFormatter.format(new Date());
    }

    public static String getDefaultStringEscapeCsv(String data) {
        String theData = StringUtils.defaultString(data);
        
        theData = StringEscapeUtils.escapeCsv(theData);
        
        return theData;
    }
   
    public static String getPayerClassName(PersonDTO personDTO) {

        String result = "";

        PayerClassDTO payerClass = personDTO.getPatientEnrollment().getPrimaryPayerClass();

        if (payerClass != null) {
            result = payerClass.getName();
        }

        return result;
    }

    public static String getPayerPlanName(PersonDTO personDTO) {
        String result = "";

        PayerPlanDTO payerPlan = personDTO.getPatientEnrollment().getPrimaryPayerPlan();

        if (payerPlan != null) {
            result = payerPlan.getName();
        }

        return result;
    }

    public static String getMedicaidMedicarePayerID(PersonDTO personDTO) {
        String result = "";
                if(personDTO.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId()!=null){
                    result =  personDTO.getPatientEnrollment().getPrimaryPayerMedicaidMedicareId();
                }
       

        return result;
    }
    
    /**
     * Save a text file to the given directory
     * 
     * @param data
     * @param pathToReport
     * @throws Exception 
     */
    public static void saveFile(String data, String pathToReport) throws Exception {

        FileWriter out = null;

        try {
            out = new FileWriter(pathToReport);

            IOUtils.write(data, out);
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }
    
    public static String getReportsDir(long communityId, String reportKey) {
        
        ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();
        
        // get reports dir
        String reportsDir = configurationDAO.getProperty(communityId, reportKey);
        
        System.out.println("READING REPORT DIR FROM DB - reports DIR = " + reportsDir);
        
        if (!reportsDir.endsWith("/")) {
            reportsDir += "/";
        }
        
        return reportsDir;
    }

    public static int getSeed() {
        return new Random().nextInt(Integer.MAX_VALUE);
    }
    
    
    
    public static String getHealthHome(PersonDTO personDTO) {

        String result = "";
        ProgramHealthHomeDTO programHealthHome = personDTO.getPatientEnrollment().getProgramHealthHome();
        if (programHealthHome != null) {
          result = programHealthHome.getHealthHomeName();
        }
        return result;
    }
}
