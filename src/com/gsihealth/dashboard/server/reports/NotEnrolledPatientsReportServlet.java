package com.gsihealth.dashboard.server.reports;

import com.gsihealth.dashboard.common.AccessLevelConstants;
import com.gsihealth.dashboard.common.util.EncryptUtils;
import com.gsihealth.dashboard.common.util.SsnUtils;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.CommunityContexts;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.dashboard.server.dao.ReportDAO;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import com.gsihealth.entity.PatientReportSheet;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

/**
 * @author Satyendra Singh
 * 
 * Handling export get requests
 */
public class NotEnrolledPatientsReportServlet extends HttpServlet {

    private ReportDAO reportDAO;
    private PatientManager patientManager;
    private String reportsDir;
    private Logger logger = Logger.getLogger(getClass().getName());
    private String plcsTitle;
    // private Map<Long, ProgramHealthHome> programHealthHomeMap = null;
    private ConfigurationDAO configDao; 
    
    @Override
    public void init() throws ServletException {
        ServletContext application = getServletContext();
        reportDAO = (ReportDAO) getServletContext().getAttribute(WebConstants.REPORT_DAO_KEY);
        configDao=(ConfigurationDAO)getServletContext().getAttribute(WebConstants.CONFIGURATION_DAO_KEY);
             
        //  programHealthHomeMap = new HashMap<Long, ProgramHealthHome>();
        //programHealthHomeMap=getProgramNameHealthHomeMap();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        logger.info("Starting  NotEnrolledPatientsReportServlet ...");
       
        try {
            // get communityId
            HttpSession session = request.getSession();
            LoginResult loginResult = (LoginResult) session.getAttribute(WebConstants.LOGIN_RESULT_KEY);
            long communityId = loginResult.getCommunityId();
            patientManager = this.getPatientManager(communityId);
            plcsTitle= configDao.getProperty(communityId, "plcs.title");
            
            logger.info("Parsing the arguments");
            String args = request.getParameter("p");
            String decodedArgs = EncryptUtils.decode(args);

            String[] decodedArgsArray = decodedArgs.split(",");

            if (decodedArgsArray.length != 2) {
                throw new IllegalArgumentException("Invalid arguments");
            }

            String organizationIdStr = decodedArgsArray[0];
            long organizationId = Long.parseLong(organizationIdStr);

            // access level for power user handle
            long accessLevelId = Long.parseLong(decodedArgsArray[1]);
            
            String communityOid=loginResult.getCommunityOid();

            reportsDir = ReportUtils.getReportsDir(communityId, ReportConstants.PATIENT_REPORTS_DIR_KEY);
            logger.log(Level.INFO, "reports DIR = {0}", reportsDir);
            
            
            // List<PersonDTO> enrollmentServicesList = null;
            List<PatientReportSheet> nonConsentedPatientSheets = null;
            if (accessLevelId == AccessLevelConstants.POWER_USER_ACCESS_LEVEL_ID) {
                logger.info("Running report for PatientsNotEnrolled: Power User");
                logger.info("Getting patients...");
                nonConsentedPatientSheets = reportDAO.findNonConsentedRecordsForPowerUser(communityId,-1, -1);
                addDamographicDataToNonConsentedSheet(nonConsentedPatientSheets, patientManager, communityOid);
                //  Map<String, PatientCommunityEnrollment> patientEnrollments = reportDAO.findAllCandidates(communityId);
                //  enrollmentServicesList = findGenericCandidatesEnrolledPatients(patientEnrollments);
            } else {
                logger.info("Running report for PatientsNotEnrolled: organizationId=" + organizationId);
                logger.info("Getting patients...");
                
                nonConsentedPatientSheets = reportDAO.findNonConsentedRecordsForNonPowerUser(communityId, loginResult.getUserId(),-1, -1);
                addDamographicDataToNonConsentedSheet(nonConsentedPatientSheets, patientManager, communityOid);
                // Map<String, PatientCommunityEnrollment> patientEnrollments = reportDAO.findCandidates(communityId,organizationId);
                //enrollmentServicesList = findGenericCandidatesEnrolledPatients(patientEnrollments);
            }


            logger.info("Building report data...");
            StringBuilder data = new StringBuilder();

            data.append("Patient ID,First Name,Middle Name,Last Name,SSN,Address1,Address2,City,State,ZipCode,Organization,Source,Acuity Score,"+plcsTitle+",Enrollment Status,Primary Payer Class,Primary Payer Plan,Medicaid/Medicare/Payer ID");
            data.append(IOUtils.LINE_SEPARATOR);

            //2. Iterate and get deails of user in comma sepration
            for (PatientReportSheet nonConsentedPatientSheet : nonConsentedPatientSheets) {
                data.append(getPatientData(nonConsentedPatientSheet));
                data.append(IOUtils.LINE_SEPARATOR);
            }

            // create directory if needed
//            String pathToReportDir = getServletContext().getRealPath(reportsDir);            
            File path = new File(reportsDir);
            if (!path.exists()) {
                path.mkdirs();
            }
            
            // save file to reports directory
            // get a random seed
            int seed = ReportUtils.getSeed();
            String fileName = "patients_not_consented_report_" + ReportUtils.getTimeStamp() + "_" + seed + ".csv";
            String pathToReport = reportsDir + fileName;

            logger.info("Saving file: " + pathToReport);
            ReportUtils.saveFile(data.toString(), pathToReport);

            // forward client to report
            logger.info("Forwarding to client");
            RequestDispatcher requestDispatcher = request.getRequestDispatcher(ReportConstants.REPORTS_DIR + fileName);
            requestDispatcher.forward(request, response);

            logger.info("Not Enrolled Patients Report complete");

        } catch (Exception e) {
            throw new ServletException("File was not generated", e);
        }
    }

    protected StringBuilder getPatientData(PatientReportSheet nonConsentedPatientSheet) {
        StringBuilder data = new StringBuilder();

        //  PatientEnrollmentDTO patientEnrollment = personDTO.getPatientEnrollment();

        data.append(ReportUtils.getDefaultStringEscapeCsv(String.valueOf(nonConsentedPatientSheet.getPatientId()))).append(",");
        data.append(ReportUtils.getDefaultStringEscapeCsv(nonConsentedPatientSheet.getFirstName())).append(",");
        data.append(ReportUtils.getDefaultStringEscapeCsv(nonConsentedPatientSheet.getMiddleName())).append(",");
        data.append(ReportUtils.getDefaultStringEscapeCsv(nonConsentedPatientSheet.getLastName())).append(",");

        String theSsn = nonConsentedPatientSheet.getSsn();
        if (!StringUtils.isBlank(theSsn)) {
            theSsn = SsnUtils.getMaskedSsn(theSsn);
        }

        data.append(ReportUtils.getDefaultStringEscapeCsv(theSsn)).append(",");

        data.append(ReportUtils.getDefaultStringEscapeCsv(nonConsentedPatientSheet.getAddressLine1())).append(",");
        data.append(ReportUtils.getDefaultStringEscapeCsv(nonConsentedPatientSheet.getAddressLine2())).append(",");
        data.append(ReportUtils.getDefaultStringEscapeCsv(nonConsentedPatientSheet.getCity())).append(",");
        data.append(ReportUtils.getDefaultStringEscapeCsv(nonConsentedPatientSheet.getState())).append(",");
        data.append(ReportUtils.getDefaultStringEscapeCsv(nonConsentedPatientSheet.getZipCode())).append(",");
        
        data.append(ReportUtils.getDefaultStringEscapeCsv(nonConsentedPatientSheet.getOrganization())).append(",");

        // source
     //   String source = getSource(nonConsentedPatientSheet);
        data.append(ReportUtils.getDefaultStringEscapeCsv(nonConsentedPatientSheet.getSource())).append(",");
        
        data.append(ReportUtils.getDefaultStringEscapeCsv(nonConsentedPatientSheet.getAcuityScore())).append(",");

        data.append(ReportUtils.getDefaultStringEscapeCsv(nonConsentedPatientSheet.getProgramEnrollmentConsent())).append(",");
        data.append(ReportUtils.getDefaultStringEscapeCsv(nonConsentedPatientSheet.getEnrollmentStatus())).append(",");

        // String payerClassName = ReportUtils.getPayerClassName(personDTO);
        data.append(ReportUtils.getDefaultStringEscapeCsv(nonConsentedPatientSheet.getPrimaryPayerClass())).append(",");

        //String payerPlanName = ReportUtils.getPayerPlanName(personDTO);
        data.append(ReportUtils.getDefaultStringEscapeCsv(nonConsentedPatientSheet.getPrimaryPayerPlan())).append(",");

        //String medicaidMedicarePayerID = ReportUtils.getMedicaidMedicarePayerID(personDTO);
     
        data.append(ReportUtils.getDefaultStringEscapeCsv(nonConsentedPatientSheet.getmPayerID())).append(",");
        
//        //health Home
//         String HealthHomeName = ReportUtils.getHealthHome(personDTO);
//         data.append(ReportUtils.getDefaultStringEscapeCsv(HealthHomeName)+ "," );
         
//        // program effective date
//        Date programEffectiveDate = personDTO.getPatientCommunityEnrollment().getProgramNameEffectiveDate();
//        if (programEffectiveDate != null) {
//            data.append(ReportUtils.getDefaultStringEscapeCsv(getStringProgramDate(programEffectiveDate)) + ",");
//        } 
//        // program end date
//        Date programEndDate = personDTO.getPatientCommunityEnrollment().getProgramEndDate();
//        if (programEndDate != null) {
//            data.append(ReportUtils.getDefaultStringEscapeCsv(getStringProgramDate(programEndDate)) + ",");
//        } 
//       
        
        return data;
    }

    private static String getStringProgramDate(Date programDate) {
        SimpleDateFormat programDateFormatter = new SimpleDateFormat("MM/dd/yyyy");
        String programDateStr = programDateFormatter.format(programDate);
        return programDateStr;
    }
    protected String getSource(PatientReportSheet nonConsentedPatientSheet) {
        // source
        String euid = nonConsentedPatientSheet.getPatientEuid();
        String systemCode = null;
        try {
            systemCode = patientManager.getSystemCode(euid);
        } catch (Exception ex ) {
            logger.warning("systemCode not found " + ex);
        }
        String source = ReportUtils.getUserFriendlySource(systemCode);
        return source;
    }

    protected PatientManager getPatientManager(Long communityId) {
        ServletContext servletContext = getServletContext();
        Map<Long, CommunityContexts> communityContexts
                = ((Map<Long, CommunityContexts>) servletContext.getAttribute(WebConstants.COMMUNITY_SPECIFIC_DATA_KEY));
        return communityContexts.get(communityId).getPatientManager();
    }
//
//    private List<PersonDTO> findGenericCandidatesEnrolledPatients(Map<String, PatientCommunityEnrollment> patientEnrollments) throws Exception {
//
//        List<PersonDTO> searchResults = new ArrayList<PersonDTO>();
//        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
//
//        // get the patient enrollment
//        Set<Map.Entry<String, PatientCommunityEnrollment>> entries = patientEnrollments.entrySet();
//
//        for (Map.Entry<String, PatientCommunityEnrollment> tempEntry : entries) {
//            String euid = tempEntry.getKey();
//            PatientCommunityEnrollment tempPatientEnrollment = tempEntry.getValue();
//
//            Person tempPerson = patientManager.getPatient(euid);
//            if (tempPerson != null) {
//                tempPerson.setPatientCommunityEnrollment(tempPatientEnrollment);
//
//                // passed all the filters...so add to the search results
//                PersonDTO personDto = mapper.map(tempPerson, PersonDTO.class);
//                setProgramHealthHome(tempPerson, personDto);
//                searchResults.add(personDto);
//            } else {
//                logger.warning("could not find patient matching euid " + euid + " skipping");
//            }
//        }
//
//        return searchResults;
//    }
//
//     private LinkedHashMap<Long, ProgramHealthHome> getProgramNameHealthHomeMap() {
//
//        List<ProgramHealthHome> programHealthHomeList = reportDAO.findProgramHealthHomeEntities();
//        LinkedHashMap<Long, ProgramHealthHome> data = new LinkedHashMap<Long, ProgramHealthHome>();
//        for (ProgramHealthHome phh : programHealthHomeList) {
//
//            data.put(phh.getProgramHealthHomePK().getId(), phh);
//        }
//        return data;
//    }
//
//     //add health Home
//      protected void setProgramHealthHome(Person tempPerson, PersonDTO personDto) {
//
//          ProgramHealthHomeDTO programHealthHomeDTO;
//        if (tempPerson.getPatientCommunityEnrollment().getProgramHealthHomeId() != 0) {
//            programHealthHomeDTO = new ProgramHealthHomeDTO();
//            ProgramHealthHome healthHome= programHealthHomeMap.get(tempPerson.getPatientCommunityEnrollment().getProgramHealthHomeId());
//            programHealthHomeDTO.setHealthHomeName(healthHome.getHealthHomeValue());
//            programHealthHomeDTO.setId(healthHome.getProgramHealthHomePK().getId());
//            personDto.getPatientEnrollment().setProgramHealthHome(programHealthHomeDTO);
//
//        }
//    }
//
    /**
     * FIXME : Single entity class for all reports can be used. So, we don't
     * need three different methods of each report.
     */
    private void addDamographicDataToNonConsentedSheet(List<PatientReportSheet> nonConsentedPatientSheet,PatientManager patientManager, String communityOid) throws Exception {
        List<String> patientList = new ArrayList<>();

        logger.info("Getting patientIds in list..");
        for (PatientReportSheet temp : nonConsentedPatientSheet) {
            patientList.add(temp.getPatientId().toString());
        }
         logger.info("Start retrieving patient demographic data from MDM/Mirth with the help of patientIds...");
      Map<String, SimplePerson> demographicData=  reportDAO.getPatientsFromMdm(patientManager, patientList, communityOid);

//        Map<String, SimplePerson> demographicData = patientManager.getMappedPatientsDirectQuery(em, patientList, communityOid);

        ListIterator listItr = nonConsentedPatientSheet.listIterator();

        logger.info("Adding demographic data to nonConsentedPatientSheet..");
        while (listItr.hasNext()) {
            PatientReportSheet nonConsentedPatientSheetTemp = (PatientReportSheet) listItr.next();
            SimplePerson simplePerson = demographicData.get(nonConsentedPatientSheetTemp.getPatientId().toString());
            convertMdmSbrToSP(simplePerson, nonConsentedPatientSheetTemp);
        }
    }
      private void convertMdmSbrToSP(SimplePerson sbr, PatientReportSheet nonConsentedPatientSheet) throws Exception {
        if (sbr != null && nonConsentedPatientSheet != null) {
            nonConsentedPatientSheet.setFirstName(sbr.getFirstName());
            nonConsentedPatientSheet.setMiddleName(sbr.getMiddleName());
            nonConsentedPatientSheet.setLastName(sbr.getLastName());
            nonConsentedPatientSheet.setAddressLine1(sbr.getStreetAddress1());
            nonConsentedPatientSheet.setAddressLine2(sbr.getStreetAddress2());
            nonConsentedPatientSheet.setCity(sbr.getCity());
            nonConsentedPatientSheet.setState(sbr.getState());
            nonConsentedPatientSheet.setZipCode(sbr.getZipCode());
            nonConsentedPatientSheet.setSsn(sbr.getSSN());
        } else {
            logger.info("Some thing wrong with Id : ...Looks like we have patient_id in PCE,"
                    + " but corresponding lid does not present in sbyn.enterprise" + nonConsentedPatientSheet.getPatientId());
        }
    }
}
