/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.tasks;

import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.entity.TaskHistory;
import java.util.Date;
import java.util.logging.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 *
 * @author Beth.Boose
 */
public class PatientTagProcessing {

    private Logger logger = Logger.getLogger(getClass().getName());
    private Long communityId;
    private String taskName;
    private String path;

  

    public PatientTagProcessing(ConfigurationDAO configurationDAO, Long communityId, String taskName) {
        this.taskName = taskName;
        this.communityId = communityId;
        path = configurationDAO.getProperty(communityId, "patient.list.server");
        
    }

 

    public TaskHistory createTaskHistory(Date lastRan, Date created, Character success) {
        TaskHistory taskHistory = new TaskHistory();
        taskHistory.setApplication("NA");
        taskHistory.setCommunityId(communityId);
        taskHistory.setCreatedDate(created);
        taskHistory.setLastRan(lastRan);
        taskHistory.setSuccess(success);
        taskHistory.setTaskName(taskName);
        return taskHistory;
    }

    public void processPatientTags(Long communityId) throws Exception{
        
         String path1 = path + "/tag/-1/" + communityId + "/MINOR/-1" ;
         sendGet(path1);
         String path2 = path + "/tag/-1/" + communityId + "/EVENT/-1" ;
         sendGet(path2);
         String path3 = path + "/tag/-1/" + communityId + "/INCARCERATED/-1" ;
         sendGet(path3);
    }

     public void sendGet(String path) throws Exception {
        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            HttpGet request = new HttpGet(path);
            
            request.addHeader("content-type", "application/json");
           
            HttpResponse result = httpClient.execute(request);
            
            System.out.println(result.toString());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
       

    }
    
     
    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }


}
