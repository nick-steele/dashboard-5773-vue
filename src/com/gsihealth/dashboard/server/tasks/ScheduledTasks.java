/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.tasks;

import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.ConfigurationConstants;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.CommunityDAO;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.entity.Community;

import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedScheduledExecutorService;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Beth.Boose
 */
@WebListener
public class ScheduledTasks implements ServletContextListener {

    private Logger logger = Logger.getLogger(getClass().getName());

    @Resource(mappedName = "concurrent/__defaultManagedScheduledExecutorService")
    private ManagedScheduledExecutorService mses;

    private ServletContext servletContext;
    private List<Community> communities;

//    public ScheduledTasks(ServletContext servletContext, List<Community> communities) {
//        this.servletContext = servletContext;
//        this.communities = communities;
//    }
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.info("context initialized, where your mses at?");
        if (this.mses == null) {
            throw new CancellationException("RUN WILL ROBINSON RUN !!!! managedScheduledExecutorService is null"
                    + "- check your resource");
        }
        // get context for access to dao's
        this.servletContext = sce.getServletContext();
        if (mses == null) {
            throw new CancellationException("RUN WILL ROBINSON RUN !!!! servletContext is null"
                    + "- find one");
        }
        this.communities = ((CommunityDAO) this.servletContext.getAttribute(WebConstants.COMMUNITY_DAO_KEY))
                .findEntities();
        if (this.communities == null) {
            throw new CancellationException("RUN WILL ROBINSON RUN !!!! communities is null"
                    + "- is dataContextListener up?");
        }
        this.scheduleTasks();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        logger.info("context destroyed, die mses die");
        // this.shutdownAndAwaitTermination();
    }

    /**
     * add your tasks here
     */
    private void scheduleTasks() {
        try { // FIXME, PUT ALL THIS IN A CLASS, USE AN INTERFACE
            for (Community each : this.communities) {
                Long communityId = each.getCommunityId();
                String[] parseInterval
                        = this.getScheduledInterval(ConfigurationConstants.MINOR_AGE_MSG_TASK_RUN, each.getCommunityId());
                if (parseInterval != null && parseInterval.length > 1) {
                    long time = Long.parseLong(parseInterval[0]);
                    TimeUnit tu = TimeUnit.valueOf(parseInterval[1]);
                    long interval = tu.toMillis(time);
                    Date nextScheduledTime = new Date(System.currentTimeMillis() + interval);
                    logger.fine("next scheduled date for msgtask in community "
                            + each.getCommunityId() + " is " + nextScheduledTime);

                    mses.schedule(new SendMessagesTask(servletContext, communityId),
                            new SendMessagesTrigger(servletContext, nextScheduledTime, communityId,
                                    interval, SendMessagesTask.IDENTITY_NAME));
                }
               
                //parseInterval = new String[]{"1", "HOURS"};
                if (parseInterval != null && parseInterval.length > 1) {
                    long time = Long.parseLong(parseInterval[0]);
                    TimeUnit tu = TimeUnit.valueOf(parseInterval[1]);
                    
                    long interval = tu.toMillis(time);
                  
                    Date nextScheduledTime = new Date();
                    logger.info("next scheduled date for patient list task in community "
                            + each.getCommunityId() + " is " + nextScheduledTime);

                    mses.schedule(new PatientTagTask(servletContext, communityId),
                            new PatientTagTaskTrigger(servletContext, nextScheduledTime, communityId,
                                    interval, PatientTagTask.IDENTITY_NAME));
                   
                }

            }
        } catch (Throwable t) {
            logger.log(Level.SEVERE ,"bad things happened with scheduled task startup " , t.getMessage());

        }
    }

    /**
     * get how often this task should run
     *
     * @param configName
     * @return how often this task should run from configuration
     */
    private String[] getScheduledInterval(String configName, Long communityId) {
        ConfigurationDAO configurationDAO
                = (ConfigurationDAO) servletContext.getAttribute(WebConstants.CONFIGURATION_DAO_KEY);
        String nextScheduledInterval = configurationDAO.getProperty(communityId, configName);
        String[] parseInterval = null;
        try {
            if (StringUtils.isNotBlank(nextScheduledInterval)) {
                logger.fine("for community " + communityId + " next scheduled interval is: " + nextScheduledInterval);
                parseInterval = nextScheduledInterval.split(" ");
                logger.fine("parse interval for community " + communityId + " is " + parseInterval);
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE ,"getting parsed interval failed for " + configName + " for community " + communityId
                    + " failed " , e);
            parseInterval = null;
        }
        return parseInterval;
    }

    /**
     * call this when the server shuts down or app redeployed
     */
    private void shutdownAndAwaitTermination() {
        this.mses.shutdown(); // Disable new tasks from being submitted
        try {
            // Wait a while for existing tasks to terminate
            if (!this.mses.awaitTermination(10, TimeUnit.SECONDS)) {
                this.mses.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                if (!this.mses.awaitTermination(10, TimeUnit.SECONDS)) {
                    logger.warning("managedScheduledExecutorService did not terminate");
                }
            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            this.mses.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }

}
