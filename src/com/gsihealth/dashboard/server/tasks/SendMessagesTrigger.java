/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.tasks;

import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.ConfigurationConstants;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.dashboard.server.dao.TaskHistoryDAO;
import com.gsihealth.dashboard.server.dao.TaskHistoryDAOImpl;
import com.gsihealth.entity.TaskHistory;
import org.joda.time.DateTime;

import javax.enterprise.concurrent.LastExecution;
import javax.enterprise.concurrent.Trigger;
import javax.servlet.ServletContext;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 *
 * @author Beth.Boose
 */
public class SendMessagesTrigger implements Trigger {

    private Logger logger = Logger.getLogger(getClass().getName());

    private ServletContext servletContext;
    private Long communityId;
    private Date nextrun;
    private long interval;
    private String taskName;

    public SendMessagesTrigger(ServletContext servletContext, Date nextrun, Long communityId, long interval,
        String taskName) {
        this.servletContext = servletContext;
        this.nextrun = nextrun;
        this.communityId = communityId;
        this.interval = interval;
        this.taskName = taskName;
    }

    /**
     * calc this from database entries, next run time should be
     * task_history.last_ran(successful) + minor.age.task.run unless that value
     * is null, then use now() + minor.age.task.run edge case - check
     * lastExecutionInfo against taskhistory
     *
     * @param lastExecutionInfo
     * @param taskScheduledTime
     * @return
     */
    @Override
    public Date getNextRunTime(LastExecution lastExecutionInfo, Date taskScheduledTime) {
        // get a fresh interval
        this.calculateInterval();
        if (lastExecutionInfo != null) { // use now + interval
            logger.fine("last time this task started was: " + lastExecutionInfo.getRunStart());
            DateTime lastRan = new DateTime(lastExecutionInfo.getRunStart());
            logger.fine("Before adding last ran to intreval for SendMessage Trigger " + this.interval);
            this.nextrun = lastRan.plus(this.interval).toDate();
            logger.fine("Next run for SendMessage Trigger " + this.nextrun);
        } else { // get last ran from taskHistory
            
            Date lastRanFromTaskHistory = this.getLastStoredRun();
            if(lastRanFromTaskHistory != null) {
                logger.fine("no last execution info available for this task - get last execution from taskHistory"+lastRanFromTaskHistory);
                DateTime runnext = new DateTime(lastRanFromTaskHistory).plus(this.interval);
                this.nextrun = runnext.toDate();
                logger.fine("Get last execution from taskHistory to run Next"+this.nextrun);
            } else { // otherwise add interval to the current time

                this.nextrun = new Date(System.currentTimeMillis() + this.interval);
                logger.fine("no history - use current time + interval"+this.nextrun);
            }
        }

        return this.nextrun;
    }

    /**
     * how long should I wait to run?
     * get the interval from configuration
     */
    private void calculateInterval() {
        ConfigurationDAO configurationDAO
                = (ConfigurationDAO) servletContext.getAttribute(WebConstants.CONFIGURATION_DAO_KEY);
        String nextScheduledInterval = configurationDAO
                .getProperty(communityId, ConfigurationConstants.MINOR_AGE_MSG_TASK_RUN);
        if (StringUtils.isNotBlank(nextScheduledInterval)) {
            String[] parseInterval = nextScheduledInterval.split(" ");
            long time = Long.parseLong(parseInterval[0]);
            TimeUnit tu = TimeUnit.valueOf(parseInterval[1]);
            this.interval = tu.toMillis(time);
            logger.fine("got a fresh trigger time from db");
        }
    }

    /**
     * if
     *
     * @param lastExecutionInfo
     * @param scheduledRunTime
     * @return
     */
    @Override
    public boolean skipRun(LastExecution lastExecutionInfo, Date scheduledRunTime) {
        boolean skip = false;
        if (lastExecutionInfo != null) {
            logger.fine("working out skip info, last time this task for community " + communityId + " started was: " + lastExecutionInfo.getRunStart());
            DateTime joda = new DateTime(lastExecutionInfo.getRunStart());
            logger.fine("Intreval for Send Message Trigger"+this.interval);
            joda = joda.plus(this.interval);
            Date lastRanPlusInterval = joda.toDate();
            logger.fine("LastRanPlusIntreval for Send Message Trigger"+lastRanPlusInterval);
            // if last time this ran + interval is after right now, don't run
            skip = lastRanPlusInterval.after(new Date(System.currentTimeMillis()));
        } else {
            logger.fine("no last execution info available for this task for community " + communityId + ", get it from the database"); //TODO
            // skip is already false, go ahead and run it
        }

        logger.fine("should this run be skipped for community " +communityId+ " : " + skip);
        return skip;
    }

    private Date calculateNextRunTime() {
        return null;
    }

    /**
     * get last time this task ran from taskHistory if it exists
     * @return 
     */
    private Date getLastStoredRun() {
       TaskHistoryDAO taskHistoryDAO = (TaskHistoryDAOImpl)
               this.servletContext.getAttribute(WebConstants.TASK_HISTORY_DAO_KEY);
       TaskHistory lastTaskRan = taskHistoryDAO.getTaskHistory(communityId, this.taskName);
       Date dateLastRanFromTaskHistory = null;
       if(lastTaskRan != null){
           dateLastRanFromTaskHistory = lastTaskRan.getLastRan();
           logger.info(taskName + " was last ran on (from taskHistory) " + dateLastRanFromTaskHistory);
       }
       return dateLastRanFromTaskHistory;
    }

}
