/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.tasks;

import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.server.service.EnrollmentServiceImpl;
import com.gsihealth.dashboard.server.util.DateUtils;
import com.gsihealth.entity.TaskHistory;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Beth.Boose
 */
public class MinorMessageProcessing {

    private Logger logger = Logger.getLogger(getClass().getName());
    private Long communityId;
    private String taskName;
    private List<Integer> parseIntervals;
    private int minorAge;
    private String sendingApp;

    public MinorMessageProcessing(Long communityId, String taskName, String application) {
        this.communityId = communityId;
        this.taskName = taskName;
        this.sendingApp = application;
    }

    /**
     *
     * @param minorAgeSchedule
     * @param split regex to split minorAgeSchedule on
     * @return minorAgeSchedule as an interval of millis
     * @throws Exception
     */
    public List<Integer> parseMinorAgeScheduleIntoIntervals(String minorAgeSchedule, String split) throws Exception {
        try {
            logger.fine("processing a schedule string to intervals for "
                    + minorAgeSchedule + " split on " + split);
            String[] parseArr = minorAgeSchedule.split(split);
            this.parseIntervals = new ArrayList<>(parseArr.length);
            for (String each : parseArr) {
                Integer interval = Integer.parseInt(each);
                logger.fine("processing each parse array and get intrevals" + (Object) parseArr);
                this.parseIntervals.add(interval);
            }
            Collections.sort(parseIntervals);
        } catch (Exception e) {
            throw new Exception("could not parse schedule interval ", e);
        }
        return this.parseIntervals;
    }

    public Long convertTimeToInterval(int time) throws Exception {
        Long interval = TimeUnit.DAYS.toMillis(time);
        return interval;
    }

    @SuppressWarnings("LoggerStringConcat")
    public String calculateBirthdayToSearchFor(long interval, int minorAge) throws Exception {
        String birthdayToSearchFor = null;
        try {
            birthdayToSearchFor = DateUtils.calculateBirthdayToSearchFor(interval, minorAge);
            logger.fine("calculated " + birthdayToSearchFor + " for " + minorAge);
        } catch (ParseException pe) {
            throw new Exception("Could not calculate birthday to search for: ", pe);
        }
        return birthdayToSearchFor;
    }

    public TaskHistory createTaskHistory(Date lastRan, Date created, Character success) {
        TaskHistory taskHistory = new TaskHistory();
        taskHistory.setApplication(sendingApp);
        taskHistory.setCommunityId(communityId);
        taskHistory.setCreatedDate(created);
        taskHistory.setLastRan(lastRan);
        taskHistory.setSuccess(success);
        taskHistory.setTaskName(taskName);
        return taskHistory;
    }

    public void convertAndSetMinorAge(String age) throws NumberFormatException {
        if (age != null) {
            this.minorAge = Integer.parseInt(age);
        }
    }

    public SearchCriteria populateSearchCriteria(String birthdayToSearchFor) {
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setDateOfBirthStr(birthdayToSearchFor);
        return searchCriteria;
    }

    /**
     * task schedule can check for any number of days in advance of day X if a
     * patient's birthday falls between closestToBday and farthestFromBday
     * checks, return closestToBday, don't count today, if they reach minorAge
     * today, they're already adults
     *
     *
     * @return closestToBday scheduled time or -1 if out of bounds
     */
    public int findMinimumNumberOfDaysThisPatientMatches(Date patientDOB) throws Exception {
        int time = -1;
        int max = Collections.max(parseIntervals);
        int min = Collections.min(parseIntervals);
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("MM/dd/yyyy");
        String soonestBdayToSearchFor = DateUtils.calculateBirthdayToSearchFor(
                this.convertTimeToInterval(max), this.minorAge);
        DateTime closestToBday = dtfOut.parseDateTime(soonestBdayToSearchFor);
        String latestBdayToSearchFor = DateUtils.calculateBirthdayToSearchFor(
                this.convertTimeToInterval(min), this.minorAge);
        DateTime farthestFromBday = dtfOut.parseDateTime(latestBdayToSearchFor);
        DateTime dob = new DateTime(patientDOB);
        logger.fine("soonest: " + closestToBday + " patient dob " + dob + " latest: " + farthestFromBday);

        if (new Interval(farthestFromBday, closestToBday).contains(dob)) { // if patientDOB is between scheduled dates, use max
            time = max;
            logger.fine("patient with dob of " + patientDOB + " has a birthday that is between "
                    + closestToBday + " and " + farthestFromBday + " days from now");
        }
        return time;
    }

    /**
     * calculate birthday to search for based on time, populate searchCriteria
     * and send messages for a single time interval
     *
     * @param enrollmentService
     * @param time
     * @throws Exception
     */
    public void processSingleInterval(EnrollmentServiceImpl enrollmentService, int time) throws Exception {
        Long interval = this.convertTimeToInterval(time);
        String birthdayToSearchFor = this.calculateBirthdayToSearchFor(interval, this.minorAge);
        SearchCriteria searchCriteria = this.populateSearchCriteria(birthdayToSearchFor);
        logger.info("processing msg for " + this.minorAge + " interval " + time + " community " + this.communityId);
        logger.info("Search Criteria for Processing Major Alerts"+searchCriteria.getDateOfBirthStr()+"Enrolment service :"+enrollmentService);
            enrollmentService.handleMinorPatientsMessages(searchCriteria, this.minorAge, time, this.communityId);
    }

    public Character processAllIntervals(EnrollmentServiceImpl enrollmentService) {
        Character success = 'N';
        for (Integer time : this.parseIntervals) {
            try {
                this.processSingleInterval(enrollmentService, time);
                success = 'Y';  // as long as they don't all fail, it's a success
            } catch (Exception ex) {
                logger.log(Level.SEVERE,"failed to process message for task: " + this.taskName + " at "
                        + time + " interval in community " + this.communityId + " " , ex);
            }
        }
        return success;
    }

    public boolean isPatientAMinor(Date personDOB, int minorAge) {
        logger.fine("checkig patient with dob " + personDOB + " against minor age of " + minorAge);
        LocalDate dob = new LocalDate(personDOB);
        LocalDate now = new LocalDate();
        int age = Years.yearsBetween(dob, now).getYears();
        boolean isAMinor = age < minorAge;
        logger.fine("patient born on " + personDOB + " is " + age + " years old today. are they a mionr? " + isAMinor);
        return isAMinor;
    }

    public int getMinorAge() {
        return this.minorAge;
    }

    public void setMinorAge(int minorAge) {
        this.minorAge = minorAge;
    }

    public List<Integer> getParseIntervals() {
        return parseIntervals;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getSendingApp() {
        return sendingApp;
    }

    public void setSendingApp(String sendingApp) {
        this.sendingApp = sendingApp;
    }

}
