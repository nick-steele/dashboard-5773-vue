/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.tasks;

import com.gsihealth.dashboard.common.AlertConstants;
import com.gsihealth.entity.TaskHistory;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.CommunityDAO;
import com.gsihealth.dashboard.server.dao.ConfigurationDAO;
import com.gsihealth.dashboard.server.dao.TaskHistoryDAO;
import com.gsihealth.dashboard.server.dao.TaskHistoryDAOImpl;
import com.gsihealth.entity.Community;
import static com.mirth.kana.result.Result.success;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.logging.Logger;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.concurrent.ManagedTask;
import javax.enterprise.concurrent.ManagedTaskListener;
import javax.servlet.ServletContext;
import org.joda.time.DateTime;

/**
 * time driven message sending
 *
 * @author Beth.Boose
 */
public class PatientTagTask implements Runnable, ManagedTask, ManagedTaskListener {

    private static final Logger logger = Logger.getLogger("PatientTagTask");
    public static final String IDENTITY_NAME = "patientTagTask";

    private final ServletContext servletContext;
    private Map<String, String> executionProperties;
    private Long communityId;

    public PatientTagTask(ServletContext application, Long communityId) {
        this.servletContext = application;
        this.communityId = communityId;
        this.getExecutionProperties().put(ManagedTask.IDENTITY_NAME, IDENTITY_NAME);

    }

    /**
     * 1. check configuration table for parameters - approaching age, how far in
     * advance to run 2. search patient demog for patients that meet these
     * parameters 3. send alert messages for these patients
     */
    @Override
    public void run() {
        logger.info(IDENTITY_NAME + " is running");
        ConfigurationDAO configurationDAO;
        TaskHistoryDAO taskHistoryDAO;
        CommunityDAO communityDAO;

        Date now = DateTime.now().toDate();
        

        try {
            configurationDAO = (ConfigurationDAO) servletContext.getAttribute(WebConstants.CONFIGURATION_DAO_KEY);
            taskHistoryDAO = (TaskHistoryDAOImpl) servletContext.getAttribute(WebConstants.TASK_HISTORY_DAO_KEY);
            communityDAO = (CommunityDAO) servletContext.getAttribute(WebConstants.COMMUNITY_DAO_KEY);
            PatientTagProcessing ptp = new PatientTagProcessing(configurationDAO, communityId, IDENTITY_NAME);
            ptp.processPatientTags(communityId);
            TaskHistory taskHistory = ptp.createTaskHistory(now, now, 'Y');
            taskHistoryDAO.addTaskHistory(taskHistory);

        } catch (Exception ex) {
            logger.warning(IDENTITY_NAME + " entire task failed for " + ex);
            // lets try to save the failure
            try {
                TaskHistory taskHistory = new TaskHistory(AlertConstants.GSI_CARE_COORDINATION_APP, now,
                        now, IDENTITY_NAME, communityId);
                taskHistory.setSuccess('N');

                taskHistoryDAO = (TaskHistoryDAOImpl) servletContext.getAttribute(WebConstants.TASK_HISTORY_DAO_KEY);
                taskHistoryDAO.addTaskHistory(taskHistory);
            } catch (Exception es) {
                logger.warning("couldn't even save the failure, I give up " + es);
            }
        }

    }

    @Override
    public ManagedTaskListener getManagedTaskListener() {
        return this;
    }

    @Override
    public Map<String, String> getExecutionProperties() {
        if (this.executionProperties == null) {
            this.executionProperties = new HashMap<>();
        }
        return this.executionProperties;
    }

    @Override
    public void taskSubmitted(Future<?> future, ManagedExecutorService executor, Object task) {
        logger.info(IDENTITY_NAME + " has been submitted");
    }

    @Override
    public void taskAborted(Future<?> future, ManagedExecutorService executor, Object task, Throwable exception) {
        logger.warning(IDENTITY_NAME + " has been aborted");
    }

    /**
     * this is a function of ManagedTaskListener
     *
     * @param future
     * @param executor
     * @param task
     * @param exception
     */
    @Override
    public void taskDone(Future<?> future, ManagedExecutorService executor, Object task, Throwable exception) {
        logger.fine(IDENTITY_NAME + " has been completed");
    }

    @Override
    public void taskStarting(Future<?> future, ManagedExecutorService executor, Object task) {
        logger.info(IDENTITY_NAME + " is starting now");
    }

}
