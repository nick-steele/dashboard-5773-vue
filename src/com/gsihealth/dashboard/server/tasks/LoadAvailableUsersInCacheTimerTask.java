package com.gsihealth.dashboard.server.tasks;

import com.gsihealth.entity.User;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.CommunityCareteamDAO;
import com.gsihealth.dashboard.server.dao.UserCommunityOrganizationDAO;
import com.gsihealth.dashboard.server.dao.UserCommunityOrganizationDAOImpl;
import com.gsihealth.dashboard.server.util.PropertyUtils;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;

public class LoadAvailableUsersInCacheTimerTask extends TimerTask {

    private Logger logger = Logger.getLogger(getClass().getName());
    private ServletContext application;
    private Date createdDate;

    public LoadAvailableUsersInCacheTimerTask(ServletContext theApplication) {
        application = theApplication;
        createdDate = new Date();
    }

    @Override
    public void run() {

        try {
            logger.fine("Starting LoadAvailableUsersInCacheTimerTask: createdDate=" + createdDate);

            // get from database
            CommunityCareteamDAO careteamDAO = (CommunityCareteamDAO) application.getAttribute(WebConstants.COMMUNITY_CARE_TEAM_DAO_KEY);
            List<User> users = careteamDAO.getAvailableUsers();
            
            UserCommunityOrganizationDAO userCommunityOrganizationDAO
                = (UserCommunityOrganizationDAO) application.getAttribute(WebConstants.USER_COMMUNITY_ORGANIZATION_DAO_KEY);
            // convert to dtos
            Map<Long, List<UserDTO>> userDTOMap = PropertyUtils.convertToDTOMap(users, userCommunityOrganizationDAO);

            // store in servlet context / cache
            application.setAttribute(WebConstants.AVAILABLE_USERS_KEY, userDTOMap);

            logger.fine("Finished LoadAvailableUsersInCacheTimerTask");
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "LoadAvailableUsersInCacheTimerTask", exc);
        }
    }
}
