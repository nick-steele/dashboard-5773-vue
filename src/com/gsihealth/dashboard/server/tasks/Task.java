package com.gsihealth.dashboard.server.tasks;

/**
 * Base class for all tasks
 * 
 * @author Chad.Darby
 */
public abstract class Task {
    
    /**
     * The action to be performed by this task
     */
    public abstract void run();
}
