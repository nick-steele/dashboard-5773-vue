/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.rest.service;

import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.gsihealth.dashboard.entity.dto.OrgCareteamUserDTO;
import com.gsihealth.entity.User;
import javax.persistence.Persistence;

/**
 *
 * @author vlewis
 */
@Path("user")
public class UserRESTFacade {

    private static EntityManagerFactory emf = null;

    private Logger logger = Logger.getLogger(getClass().getName());

    private EntityManagerFactory getEntityManagerFactory() throws NamingException {
        String persistenceUnitName = "dashboardPU";
        String persistenceUnitNameStandAlone = "dashboardPU_standalone";
        if (emf == null) {
            emf = getEntityManagerFactory(persistenceUnitName, persistenceUnitNameStandAlone);
        }
        return emf;
    }

    protected EntityManagerFactory getEntityManagerFactory(String persistenceUnitName, String persistenceUnitNameStandAlone) throws RuntimeException {
        // JPA setup...
        EntityManagerFactory emf = null;
    
        try {
            // look for datasource first
            logger.info("Looking up: " + persistenceUnitName);
            emf = Persistence.createEntityManagerFactory(persistenceUnitName);
            logger.info("emf = " + emf);
           
        } catch (Exception exc) {

            logger.info("Could not lookup: " + persistenceUnitName);

        }
        return emf;
    }

    
 
 
    private UserJpaController getJpaController() {
        try {
             return new UserJpaController(getEntityManagerFactory());
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    public UserRESTFacade() {
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    public Response create(User entity) {
        try {
            getJpaController().create(entity);
            return Response.created(URI.create(entity.getUserId().toString())).build();
        } catch (Exception ex) {
            return Response.notModified(ex.getMessage()).build();
        }
    }

    @PUT
    @Consumes({MediaType.APPLICATION_JSON})
    public Response edit(User entity) {
        try {
            getJpaController().edit(entity);
            return Response.ok().build();
        } catch (Exception ex) {
            return Response.notModified(ex.getMessage()).build();
        }
    }

    @POST
    @Consumes({MediaType.APPLICATION_JSON})
    @Path("/update")
    public Response update(Map<String, String> values) {
        try {
            Long userId = Long.valueOf(values.get("userId"));
            User user = getJpaController().findUser(userId);
            user.setUserInterfaceType(values.get("userInterfaceType"));
            getJpaController().edit(user);
            return Response.ok().build();
        } catch (Exception ex) {
            return Response.notModified(ex.getMessage()).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response remove(@PathParam("id") Long id) {
        try {
            getJpaController().destroy(id);
            return Response.ok().build();
        } catch (Exception ex) {
            return Response.notModified(ex.getMessage()).build();
        }
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public User find(@PathParam("id") Long id) {
        return getJpaController().findUser(id);
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<User> findAll() {
        return getJpaController().findUserEntities();
    }

    @GET
    @Path("{max}/{first}")
    @Produces({MediaType.APPLICATION_JSON})
    public List<User> findRange(@PathParam("max") Integer max, @PathParam("first") Integer first) {
        return getJpaController().findUserEntities(max, first);
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String count() {
        return String.valueOf(getJpaController().getUserCount());
    }

    @GET
    @Path("/{communityId}/{userId}/subordinates")
    @Produces(MediaType.APPLICATION_JSON)
    public List<User> findSubordinate(@PathParam("userId") long userId, @PathParam("communityId") long communityId) {
        return getJpaController().findUserBySupervisor(userId, communityId);
    }

    @GET
    @Path("/{communityId}/{userId}/{patientId}/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<OrgCareteamUserDTO> findUserSameOrganizationOrCareteam(@PathParam("communityId") long communityId,
            @PathParam("userId") long userId,
            @PathParam("patientId") long patientId,
            @PathParam("name") String name) {
        return getJpaController().findUserInSameOrganizationOrCareteam(communityId, userId, patientId, name);
    }

}
