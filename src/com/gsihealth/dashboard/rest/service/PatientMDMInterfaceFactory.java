/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.rest.service;

import com.gsihealth.patientmdminterface.PatientInterface;
import java.util.HashMap;

/**
 *
 * @author vlewis
 */
public class PatientMDMInterfaceFactory {
    private static HashMap<Long,PatientInterface> map = new HashMap();
    
    public void addPatientInterface(long communityId,PatientInterface pi){
        map.put(communityId, pi);
    }
    public static PatientInterface getPatientInterface(long communityId){
        return map.get(communityId);
    }
}
