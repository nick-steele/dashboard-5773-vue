/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.rest.service;

import com.gsihealth.dashboard.rest.service.exceptions.NonexistentEntityException;
import com.gsihealth.dashboard.rest.service.exceptions.PreexistingEntityException;
import com.gsihealth.dashboard.rest.service.exceptions.RollbackFailureException;
import com.gsihealth.entity.Configuration;
import com.gsihealth.entity.ConfigurationPK;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityTransaction;
import javax.transaction.UserTransaction;

/**
 *
 * @author vlewis
 */
public class ConfigurationJpaController implements Serializable {

    public ConfigurationJpaController( EntityManagerFactory emf) {
    
        this.emf = emf;
    }
  
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Configuration configuration) throws PreexistingEntityException, RollbackFailureException, Exception {
        if (configuration.getConfigurationPK() == null) {
            configuration.setConfigurationPK(new ConfigurationPK());
        }
                EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();
        try {
            utx.begin();
            em = getEntityManager();
            em.persist(configuration);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            if (findConfiguration(configuration.getConfigurationPK()) != null) {
                throw new PreexistingEntityException("Configuration " + configuration + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Configuration configuration) throws NonexistentEntityException, RollbackFailureException, Exception {
              EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();
        try {
            utx.begin();
            em = getEntityManager();
            configuration = em.merge(configuration);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                ConfigurationPK id = configuration.getConfigurationPK();
                if (findConfiguration(id) == null) {
                    throw new NonexistentEntityException("The configuration with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(ConfigurationPK id) throws NonexistentEntityException, RollbackFailureException, Exception {
               EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();
        try {
            utx.begin();
            em = getEntityManager();
            Configuration configuration;
            try {
                configuration = em.getReference(Configuration.class, id);
                configuration.getConfigurationPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The configuration with id " + id + " no longer exists.", enfe);
            }
            em.remove(configuration);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Configuration> findConfigurationByCommunity(int comId) {
        EntityManager em = getEntityManager();
        try {
            Class clas = Configuration.class;
            String sql =  "SELECT * FROM connect.configuration where community_id = " + comId;
            Query query = em.createNativeQuery(sql, clas);

            return query.getResultList();
        } finally {
            em.close();
        }
    }

    public List<Configuration> findConfigurationEntities() {
        return findConfigurationEntities(true, -1, -1);
    }

    public List<Configuration> findConfigurationEntities(int maxResults, int firstResult) {
        return findConfigurationEntities(false, maxResults, firstResult);
    }

    private List<Configuration> findConfigurationEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from Configuration as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Configuration findConfiguration(ConfigurationPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Configuration.class, id);
        } finally {
            em.close();
        }
    }

    public int getConfigurationCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from Configuration as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

}
