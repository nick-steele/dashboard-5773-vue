/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.rest.service;

import com.gsihealth.entity.ConfigurationPK;
import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.ws.rs.core.PathSegment;
import javax.transaction.UserTransaction;
import javax.naming.InitialContext;
import com.gsihealth.entity.Configuration;
import java.net.URI;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author vlewis
 */
@Path("configuration")
public class ConfigurationRESTFacade {
 private Logger logger = Logger.getLogger(getClass().getName());
 
  private static EntityManagerFactory emf = null;

    private ConfigurationPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;communityId=communityIdValue;name=nameValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        com.gsihealth.entity.ConfigurationPK key = new com.gsihealth.entity.ConfigurationPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> communityId = map.get("communityId");
        if (communityId != null && !communityId.isEmpty()) {
            key.setCommunityId(new java.lang.Long(communityId.get(0)));
        }
        java.util.List<String> name = map.get("name");
        if (name != null && !name.isEmpty()) {
            key.setName(name.get(0));
        }
        return key;
    }

    private EntityManagerFactory getEntityManagerFactory() throws NamingException {
        String persistenceUnitName = "dashboardPU";
        String persistenceUnitNameStandAlone = "dashboardPU_standalone";
        if (emf == null) {
            emf = getEntityManagerFactory(persistenceUnitName, persistenceUnitNameStandAlone);
        }
        return emf;
    }

    protected EntityManagerFactory getEntityManagerFactory(String persistenceUnitName, String persistenceUnitNameStandAlone) throws RuntimeException {
        // JPA setup...
        EntityManagerFactory emf = null;
    
        try {
            // look for datasource first
            logger.info("Looking up: " + persistenceUnitName);
            emf = Persistence.createEntityManagerFactory(persistenceUnitName);
            logger.info("emf = " + emf);
           
        } catch (Exception exc) {

            logger.info("Could not lookup: " + persistenceUnitName);

        }
        return emf;
    }

    private ConfigurationJpaController getJpaController() {
        try {
          
            return new ConfigurationJpaController(getEntityManagerFactory());
        } catch (NamingException ex) {
            throw new RuntimeException(ex);
        }
    }

    public ConfigurationRESTFacade() {
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON})
    public Response create(Configuration entity) {
        try {
            getJpaController().create(entity);
            return Response.created(URI.create(entity.getConfigurationPK().getCommunityId() + "," + entity.getConfigurationPK().getName().toString())).build();
        } catch (Exception ex) {
            return Response.notModified(ex.getMessage()).build();
        }
    }

    @PUT
    @Consumes({ MediaType.APPLICATION_JSON})
    public Response edit(Configuration entity) {
        try {
            getJpaController().edit(entity);
            return Response.ok().build();
        } catch (Exception ex) {
            return Response.notModified(ex.getMessage()).build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response remove(@PathParam("id") PathSegment id) {
        try {
            com.gsihealth.entity.ConfigurationPK key = getPrimaryKey(id);
            getJpaController().destroy(key);
            return Response.ok().build();
        } catch (Exception ex) {
            return Response.notModified(ex.getMessage()).build();
        }
    }

    
    @GET
    @Path("{comId}")
    @Produces({ MediaType.APPLICATION_JSON})
    public List<Configuration> findByCommunity(@PathParam("comId") Integer comId) {
        return getJpaController().findConfigurationByCommunity(comId);
    }
    
    @GET
    @Produces({ MediaType.APPLICATION_JSON})
    public List<Configuration> findAll() {
        return getJpaController().findConfigurationEntities();
    }

    @GET
    @Path("{max}/{first}")
    @Produces({ MediaType.APPLICATION_JSON})
    public List<Configuration> findRange(@PathParam("max") Integer max, @PathParam("first") Integer first) {
        return getJpaController().findConfigurationEntities(max, first);
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String count() {
        return String.valueOf(getJpaController().getConfigurationCount());
    }
    
}
