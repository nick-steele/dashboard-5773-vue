/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.rest.service;

import com.gsihealth.dashboard.client.service.PatientProgramService;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.PatientProgramDTO;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.common.ValidationException;
import com.gsihealth.dashboard.server.dao.ProgramNameHistoryDAO;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import com.gsihealth.dashboard.server.service.BasePatientService;
import com.gsihealth.dashboard.server.util.DateUtils;
import com.gsihealth.entity.PatientCommunityEnrollment;
import com.gsihealth.entity.PatientProgramName;
import com.gsihealth.entity.PatientProgramNameRest;
import com.gsihealth.entity.PatientStatus;
import com.gsihealth.entity.ProgramDeleteReason;
import com.gsihealth.entity.ProgramHealthHome;
import com.gsihealth.entity.ProgramHealthHomePK;
import com.gsihealth.entity.ProgramName;
import com.gsihealth.entity.ProgramNameHistory;
import com.gsihealth.entity.ProgramNamePK;
import com.gsihealth.entity.ProgramTerminationReason;
import com.gsihealth.patientmdminterface.entity.SimplePerson;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;

/**
 *
 * @author ssingh
 */

@Path("/patientprogram")
public class PatientProgramRESTFacade extends BasePatientService implements PatientProgramService { 
    
    @Context
    private ServletContext context;
    private Logger logger = Logger.getLogger(getClass().getName());
      
     /**
     * Initializes the service with DAOs
     */
    @Override
    public void init() throws ServletException {

    }

    
         
    public PatientProgramRESTFacade(){
       
    }            
 
    private EntityManagerFactory getEntityManagerFactory() throws NamingException {
        String persistenceUnitName = "dashboardPU";
        String persistenceUnitNameStandAlone = "dashboardPU_standalone";
        EntityManagerFactory emf = getEntityManagerFactory(persistenceUnitName, persistenceUnitNameStandAlone);
        return emf;
    }
    protected EntityManagerFactory getEntityManagerFactory(String persistenceUnitName, String persistenceUnitNameStandAlone) throws RuntimeException {
        // JPA setup...
        EntityManagerFactory emf = null;
        EntityManager manager = null;

        try {
            // look for datasource first
            logger.info("Looking up: " + persistenceUnitName);
            emf = Persistence.createEntityManagerFactory(persistenceUnitName);
            logger.info("emf = " + emf);
            manager = emf.createEntityManager();
            logger.info("manager = " + manager);
        } catch (Exception exc) {

            logger.info("Could not lookup: " + persistenceUnitName);

         
        }

        if (manager == null) {
            String msg = "Unable to get connection to database.";
            logger.severe("\n\n>>>>>>>>> ERROR:" + msg + " <<<<<<<<<<<<<\n\n");
            throw new RuntimeException(msg);
        }

        return emf;
}
    protected EntityManager getEntityManager(){
        System.out.println("In getEntityManager  ");
        try {
           
            return getEntityManagerFactory().createEntityManager();
           
        } catch (NamingException ex) {
            Logger.getLogger(PatientProgramRESTFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @GET
    @Path("/active/{patId}/{comId}")
    @Produces({"application/json"})
    public List<PatientProgramNameRest> getActivePatientPrograms(@PathParam("patId") BigInteger patientId, @PathParam("comId") BigInteger communityId){
        EntityManager em = getEntityManager();
        try {
             List<PatientProgramNameRest> ret = new ArrayList();
            Class clas = PatientProgramNameRest.class;
            String sql1 = "SELECT pp.*, pn.value as PROGRAM_NAME, ps.value as PATIENT_STATUS_NAME FROM connect.patient_program_name pp\n" 
                    + " LEFT JOIN program_name pn ON pp.program_id=pn.id AND pn.community_id=?2\n" 
                    + " LEFT JOIN patient_status ps ON pp.patient_status=ps.id AND ps.community_id=?2\n"
                    + " WHERE pp.patient_id=?1 \n" 
                    + " AND pp.community_id=?2 \n"
                    + " AND pp.termination_reason IS NULL\n";

            Query query = em.createNativeQuery(sql1, clas).setParameter(1, patientId).setParameter(2, communityId);
            ret.addAll(query.getResultList());
            return ret; 
        } catch (Exception e) {
            logger.info("get Active patient program exception: "+ e.getMessage());
            e.printStackTrace();
        }finally {
            em.close();
        }
       
        return new ArrayList<PatientProgramNameRest>();
    }
    
    @GET
    @Path("/all/{patId}/{comId}")
    @Produces({"application/json"})
    public List<PatientProgramNameRest> getAllPatientPrograms(@PathParam("patId") BigInteger patientId, @PathParam("comId") BigInteger communityId) {
        EntityManager em = getEntityManager();
        try {
             
           List<PatientProgramNameRest> ret = new ArrayList();
            Class clas = PatientProgramNameRest.class;
            String sql1 = "SELECT pp.*, pn.value as PROGRAM_NAME, ps.value as PATIENT_STATUS_NAME FROM connect.patient_program_name pp\n" 
                    + " LEFT JOIN program_name pn ON pp.program_id=pn.id AND pn.community_id=?2\n\n" 
                    + " LEFT JOIN patient_status ps ON pp.patient_status=ps.id AND ps.community_id=?2\n"
                    + " WHERE pp.patient_id=?1 \n" 
                    + " AND pp.community_id=?2 \n";                 
                    

            Query query = em.createNativeQuery(sql1, clas).setParameter(1, patientId).setParameter(2, communityId);
            ret.addAll(query.getResultList());
      
            return ret;
        } finally {
            em.close();
        }
    }

    @POST   
    @Path("add/{userId}")
    @Consumes({"application/json"})
    @Override
    public String addProgram(@PathParam("userId") Long userId,PatientProgramName entity) {

        EntityManager em = getEntityManager();
         EntityTransaction tx =null;
         int duplicateCount=0;
         String message = null;
        try {
            //FIX ME: We need to remove duplicacy of this below method . Its duplicated of ManagePatientProgramServiceImpl
            validateForStatusEffectiveDate(entity);
            tx = em.getTransaction();
            tx.begin();
             /*To check whether patient is enrolled with the same patient program name or not*/
            Query q = em.createQuery("select object(o) from PatientProgramName as o where o.programId=:theProgramId and  o.communityId=:theCommunityId and o.patientId=:thePatientId ");
            q.setParameter("theCommunityId", entity.getCommunityId());
            q.setParameter("thePatientId", entity.getPatientId());
            q.setParameter("theProgramId", entity.getProgramId());
          
            q.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);
            
            List<PatientProgramName> duplicateEntity= (List<PatientProgramName>)q.getResultList();
           
          
          if(duplicateEntity!=null) {
              for(PatientProgramName ppn:duplicateEntity){
                        if(ppn.getTerminationReason()==null){
                            duplicateCount ++;
                            break;
                        }
                        else if(entity.getTerminationReason()!=null){
                            duplicateCount ++;
                            break;
                        }
                    }
          if(duplicateCount>0){
              tx.commit();
              return "duplicate";
          }}
//            super.create(entity);
            em.persist(entity);
           
//            long patId = entity.getPatientId();
              em.flush(); 
              em.clear();
//            // Add to program name history
            ProgramNameHistory pnh = getProgramNameHistory(entity,"Program Added ",userId);
            em.persist(pnh);
            tx.commit();
            
             PatientProgramService patientProgramService = getPatientProgramService();
           
             String result = patientProgramService.sendPatientProgramInfo(entity);
             
             logger.info("Results of add patient program send message: " + result);
             message = "true";
        }catch (ValidationException exception) {
			
            message = exception.getMessage();
        }
           
        catch (Exception exc) {
			logger.log(Level.SEVERE, "Error adding program: " , exc);
			
			if ((tx != null) && tx.isActive()) {
				tx.rollback();
			}
                        message = exc.getMessage();
		} 
       
        finally {
            if(em!=null && em.isOpen()){
            em.close();
            }
        }
         return message;
    }
 
    @PUT
    @Path("update/{userId}/{isAddToHistory}")
    @Consumes({"application/json"})
    @Override
    public String edit(@PathParam("userId") Long userId, PatientProgramName entity,@PathParam("isAddToHistory") boolean isAddtoHistory) {
        EntityManager em = null;
        EntityTransaction tx = null;
        String message = null;
        try {
//FIX ME: We need to remove duplicacy of this below method . Its duplicated of ManagePatientProgramServiceImpl
            validateForStatusEffectiveDate(entity);
            em = getEntityManager();
            tx = em.getTransaction();
            tx.begin();  


            em.merge(entity);
             em.flush();
             em.clear();            
             
        // Add to program name history
            if (isAddtoHistory) {
                ProgramNameHistory pnh = getProgramNameHistory(entity, "Program  Updated", userId);
                  em.persist(pnh);
             }
        
        tx.commit();
        
         PatientProgramService patientProgramService = getPatientProgramService();
           
        String result = patientProgramService.sendPatientProgramInfo(entity);
             
        logger.info("Results of Edit  patient program send message: " + result);
            message = "true";
        } catch (ValidationException exception) {
			
            message = exception.getMessage();
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error updating program: ", exc);

			if ((tx != null) && tx.isActive()) {
				tx.rollback();
			}			
            message = exc.getMessage();
        } finally {
            if (em != null && em.isOpen()) {
            em.close();
            }

}
        return message;
  
    }
    @DELETE
    @Path("delete/{id}/{userId}/{reasonId}")
    @Override
    public String remove(@PathParam("id") Long id,@PathParam("userId") Long userId,@PathParam("reasonId") int reasonId) {
        EntityManager em = getEntityManager();
         EntityTransaction tx =null;
        try {
            PatientProgramName entity = em.find(PatientProgramName.class, id);
            tx = em.getTransaction();
            tx.begin();
            em.remove(entity);            
          em.flush();
           em.clear();
        // Add to program name history
            ProgramNameHistory pnh = getProgramNameHistory(entity, "Program Deleted", userId);
            String sql1 = "SELECT o.VALUE from program_delete_reason as o where o.id=?1 and o.community_id=?2";
            Query q = em.createNativeQuery(sql1).setParameter(1, reasonId).setParameter(2, entity.getCommunityId());
            pnh.setDeleteReason(q.getSingleResult().toString());

            String sql2 = "SELECT o.VALUE from program_name as o where o.id=?1 and o.community_id=?2";
            Query q2 = em.createNativeQuery(sql2).setParameter(1, entity.getProgramId()).setParameter(2, entity.getCommunityId());
            pnh.setCurrentProgramName(q2.getSingleResult().toString());

            em.persist(pnh);
        tx.commit(); 
        
        PatientProgramService patientProgramService = getPatientProgramService();
           
        String result = patientProgramService.sendPatientProgramInfo(entity);
             
        logger.info("Results of Remove  patient program send message: " + result);
        
        } 
        catch (Exception exc) {
			logger.log(Level.SEVERE, "Error deleting program: " , exc);
			

			if ((tx != null) && tx.isActive()) {
				tx.rollback();
			}                       			
		}
        finally {
           if(em!=null && em.isOpen()){
            em.close();
            }
        }
        return "true";
    }


    private ProgramNameHistory getProgramNameHistory(PatientProgramName entity, String action, long userId){
          ProgramNameHistory pnh = new ProgramNameHistory();
       
            pnh.setUserId(userId);
            pnh.setCommunityId(entity.getCommunityId());            
            pnh.setActionDatetime(new Date());
         
            pnh.setPatientId(entity.getPatientId());
            pnh.setProgramEffectiveDate(entity.getProgramEffectiveDate());
            pnh.setCurrentProgramName(entity.getProgramName());
          
            pnh.setProgramEndDate(entity.getProgramEndDate());
            pnh.setProgramEndReason(entity.getTerminationReason());
            pnh.setStatus(entity.getPatientStatus());
            pnh.setAction(action);
            pnh.setParentProgramName(entity.getHealthHome());
            pnh.setStatusEffectiveDate(entity.getStatusEffectiveDate());
            pnh.setPatientProgramNameId(entity.getPatientProgramNameId());
                       
            return pnh;
        
    }
    
    @GET
    @Path("/programs/{comId}")
    @Produces({"application/json"})
    public List<ProgramName> getProgramNames(@PathParam("comId") BigInteger communityId) {
        EntityManager em = getEntityManager();
        try {
            List<ProgramName> programList = new ArrayList();
            String sql1 = "Select ob.id,ob.value,ob.community_id,ob.health_home_id \n"
                    + " FROM program_name ob WHERE ob.community_id =?1";

            Query query = em.createNativeQuery(sql1).setParameter(1, communityId);
            List<Object[]> list=query.getResultList();
            
            for(Object[] temp:list){
                ProgramName program=new ProgramName();
                ProgramNamePK prograPk=new ProgramNamePK();
                prograPk.setCommunityId((Long)temp[2]);
                prograPk.setId((Integer)temp[0]);
                program.setProgramNamePK(prograPk);
                program.setValue((String)temp[1]);
                ProgramHealthHome healthHome = new ProgramHealthHome();
                ProgramHealthHomePK healthHomePk = new ProgramHealthHomePK();
                healthHomePk.setId((Long)temp[3]);
                healthHomePk.setCommunityId((Long)temp[2]);
                healthHome.setProgramHealthHomePK(healthHomePk);
                program.setHealthHomeId(healthHome);
                programList.add(program);
            }
           
            return programList;
        } finally {
            em.close();
        }
    }
    
    @GET
    @Path("/programstatus/{comId}")
    @Produces({"application/json"})
    public List<PatientStatus> getProgramStatus(@PathParam("comId") BigInteger communityId) {
        EntityManager em = getEntityManager();
        try {
            List<PatientStatus> list = new ArrayList();
            Class clas = PatientStatus.class;
            String sql1 = "SELECT * FROM patient_status WHERE community_id=?1";

            Query query = em.createNativeQuery(sql1, clas).setParameter(1, communityId);
            list.addAll(query.getResultList());
           
            return list;
        } finally {
            em.close();
        }
    }
    
    @GET
    @Path("/programendreason/{comId}")
    @Produces({"application/json"})
    public List<ProgramTerminationReason> getProgramEndReason(@PathParam("comId") BigInteger communityId) {
        EntityManager em = getEntityManager();
        try {
            List<ProgramTerminationReason> list = new ArrayList();
            Class clas = ProgramTerminationReason.class;
            String sql1 = "SELECT * FROM program_termination_reason WHERE community_id=?1";

            Query query = em.createNativeQuery(sql1, clas).setParameter(1, communityId);
            list.addAll(query.getResultList());
           
            return list;
        } finally {
            em.close();
        }
    }
    
    @GET
    @Path("/programhealthhome/{comId}")
    @Produces({"application/json"})
    public List<ProgramHealthHome> getProgramHealthHome(@PathParam("comId") BigInteger communityId) {
        EntityManager em = getEntityManager();
        try {
            List<ProgramHealthHome> healthHomeList = new ArrayList();
            String sql1 = "SELECT o.id, o.health_home_value, o.community_id FROM program_health_home as o WHERE o.community_id=?1";
           
            Query query = em.createNativeQuery(sql1).setParameter(1, communityId);
            List<Object[]> list=query.getResultList();
            for(Object[] temp:list){               
                ProgramHealthHome healthHome = new ProgramHealthHome();
                ProgramHealthHomePK healthHomePk = new ProgramHealthHomePK();
                healthHomePk.setId((Long)temp[0]);
                healthHomePk.setCommunityId((Long)temp[2]);
                healthHome.setProgramHealthHomePK(healthHomePk);
                healthHome.setHealthHomeValue((String)temp[1]);
                healthHomeList.add(healthHome);
            }
           
            return healthHomeList;
        } finally {
            em.close();
        }
    }
    
    @GET
    @Path("/progdeletereason/{comId}")
    @Produces({"application/json"})
    public List<ProgramDeleteReason> getProgramDeleteReason(@PathParam("comId") BigInteger communityId) {
        EntityManager em = getEntityManager();
        try {
            List<ProgramDeleteReason> list = new ArrayList();
            Class clas = ProgramDeleteReason.class;
            String sql1 = "SELECT * FROM program_delete_reason WHERE community_id=?1";

            Query query = em.createNativeQuery(sql1, clas).setParameter(1, communityId);
            list.addAll(query.getResultList());
           
            return list;
        } finally {
            em.close();
        }
    }
    
    protected PersonDTO loadPersonDTO(SimplePerson sp,PatientCommunityEnrollment pce,
              long communityId, Long currentUserOrgId)
    {
        long patientId = Long.valueOf(sp.getLocalId());
        logger.info("patient id:" + patientId);

        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        int minorAge=Integer.parseInt(configurationDAO.getProperty(communityId, "minor.age", "18"));
        
       
        return super.convertPersonToDto(sp, sp.getLocalId(), pce, mapper, communityId, minorAge, currentUserOrgId);
    }

      //FIXME:  This can be refactored because BasePatientService is now super class
     /**
     * Convert entity objects to DTOs
     *
     * @param patientProgramNames
     * @return
     */
    protected List<PatientProgramDTO> convert(List<PatientProgramName> patientProgramNames, long patientId) {

        List<PatientProgramDTO> patientProgDTOs = new ArrayList<PatientProgramDTO>();

        for (PatientProgramName tempPatientProgramName : patientProgramNames) {
            PatientProgramDTO tempDTO = convertTODTO(tempPatientProgramName, patientId);
            patientProgDTOs.add(tempDTO);
        }

        return patientProgDTOs;
    }
    
   
     public PatientProgramDTO convertTODTO(PatientProgramName patientProgramName, long patientId) {
        PatientProgramDTO patientProgramDTO = new PatientProgramDTO();
        patientProgramDTO.setPatientProgramNameId(patientProgramName.getPatientProgramNameId());
        patientProgramDTO.setCommunityId(patientProgramName.getCommunityId());
        patientProgramDTO.setPatientId(patientId);
        patientProgramDTO.setHealthHome(patientProgramName.getHealthHome());
        patientProgramDTO.setPatientStatus(patientProgramName.getPatientStatus());
        patientProgramDTO.setProgramEffectiveDate(patientProgramName.getProgramEffectiveDate());
        patientProgramDTO.setProgramEndDate(patientProgramName.getProgramEndDate());
        patientProgramDTO.setProgramId(patientProgramName.getProgramId());
        patientProgramDTO.setTerminationReason(patientProgramName.getTerminationReason());
        patientProgramDTO.setStatusEffectiveDate(patientProgramName.getStatusEffectiveDate());
        patientProgramDTO.setOldRecord(true);
        return patientProgramDTO;
    }
     
     protected static List<PatientProgramName> convertFromDtos(List<PatientProgramDTO> dtos) {

        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        List<PatientProgramName> progName = new ArrayList<PatientProgramName>();

        for (PatientProgramDTO tempDto : dtos) {
            PatientProgramName tempProg = mapper.map(tempDto, PatientProgramName.class);
            progName.add(tempProg);
            
        }

        return progName;
    }
      
    protected PatientProgramService getPatientProgramService() {

        PatientProgramService patientProgramService = (PatientProgramService) context.getAttribute(WebConstants.PATIENT_PROGRAM_SERVICE_KEY);

        return patientProgramService;
    }
    
    @Override
    public String sendPatientProgramInfo(PatientProgramName entity) throws Exception{
       
        logger.info("is part ever reached?");
        SimplePerson sp = super.getPatientById(entity.getCommunityId(), entity.getPatientId());
        String result = getPatientProgramService().sendPatientProgramInfo(entity);
    
        return result;
    } 

    private void validateForStatusEffectiveDate(PatientProgramName entity) throws ValidationException {
        ProgramNameHistoryDAO programNameHistoryDAO = (ProgramNameHistoryDAO) context.getAttribute(WebConstants.PROGRAM_NAME_HISTORY_DAO_KEY);
//Spira 7467
        logger.info("validating for status effective date");
        Date now = Calendar.getInstance().getTime();

        final String healthHome = entity.getHealthHome();
        final Date newStatusEffectiveDate = entity.getStatusEffectiveDate();
        if (DateUtils.compareDatePortionOnly(newStatusEffectiveDate, now) > 0) {
            //error
            final String message = "Status effective date cannot be greater than current date for:" + healthHome;
            logger.logp(Level.SEVERE, getClass().getName(), "validateForStatusEffectiveDate", message);
            throw new ValidationException(message);
}
        final long currentStatus = entity.getPatientStatus();
        final long communityId = entity.getCommunityId();

        List<Long> statusList = programNameHistoryDAO.getDistinctStatusListExceptCurrent(entity.getPatientId(), entity.getProgramId(), currentStatus, communityId);
        TreeSet<Date> statusDates = new TreeSet<>();
        for (Long st : statusList) {
            Date dateForStatus = programNameHistoryDAO.getMostRecentStatusEffectiveDateForStatus(entity.getPatientId(), entity.getProgramId(), st, communityId);
            logger.info("status:" + currentStatus + ", date:" + dateForStatus);
            statusDates.add(dateForStatus);
        }
        Date maxDate = null;
        if (!statusDates.isEmpty()) {
            maxDate = statusDates.last();
        }
        logger.info(healthHome + ":" + maxDate);
        if (null != maxDate && DateUtils.compareDatePortionOnly(newStatusEffectiveDate, maxDate) < 0) {
            final String message = "Status effective date cannot be lesser than highest of status effective date in history for:" + healthHome;
            logger.logp(Level.SEVERE, getClass().getName(), "validateForStatusEffectiveDate", message);
            throw new ValidationException(message);
        }
    }
}

