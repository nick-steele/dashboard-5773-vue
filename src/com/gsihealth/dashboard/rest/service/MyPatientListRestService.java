package com.gsihealth.dashboard.rest.service;

import com.gsihealth.dashboard.client.service.MyPatientListService;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.common.PortalException;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.ListTagDTO;
import com.gsihealth.dashboard.entity.dto.MyPatientInfoDTO;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.service.BasePatientService;

import javax.jws.WebMethod;
import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.gsihealth.dashboard.client.util.Constants.COMMUNITY_TAG_LIST_MAP;
import static com.gsihealth.dashboard.client.util.Constants.TAGTYPE_USER_ADDED;
import com.gsihealth.dashboard.entity.dto.PatientListLabelDTO;
import org.apache.commons.lang.time.StopWatch;

/**
 * @deprecated 
 * @author Beth.Boose
 */
@Path("patientList")
public class MyPatientListRestService extends BasePatientService implements MyPatientListService {

    @Context
    private ServletContext context;
    private Logger logger = Logger.getLogger(getClass().getName());

    @POST
    @Consumes({"application/json"})
    public void add(MyPatientInfoDTO myPatientInfoDTO) throws PortalException {
         try {
            MyPatientListService mpls = getMyPatientListService();
            if(!mpls.isAlreadyOnList(myPatientInfoDTO.getUserId(), myPatientInfoDTO.getPatientId(), myPatientInfoDTO.getCommunityId())){
                mpls.add(myPatientInfoDTO);
            }else{
                mpls.update(myPatientInfoDTO);
            }

         } catch (Exception exc) {
            String message = "Error adding patient to My Patient List";
            logger.log(Level.WARNING, message, exc);
            throw new RuntimeException(message);
        }
    }

    @POST
    @Path("addWithManualTag")
    @Consumes({"application/json"})
    public void addWithManualTag(MyPatientInfoDTO myPatientInfoDTO) throws PortalException {
        setManualTag(myPatientInfoDTO, myPatientInfoDTO.getCommunityId());
        add(myPatientInfoDTO);
    }

    private void setManualTag(MyPatientInfoDTO myPatientInfoDTO, long communityId){
        // Map<Long, Map<String, Long>> communityTagMaps = (Map<Long, Map<String, Long>>)
        //         ApplicationContext.getInstance().getAttribute(COMMUNITY_TAG_LIST_MAP);
        //
        // if(communityTagMaps==null){
        //     logger.log(Level.WARNING, "CommunityTagMaps were not initialized.");
        //     return;}
        //
        // Map<String, Long> communityTagMap = communityTagMaps.get(communityId);
        // if(communityTagMap==null){
        //     logger.log(Level.WARNING, "for community "+communityId+" was not found.");
        //     return;
        // }
        //
        // Long manuallyAddedTagId = communityTagMap.get(TAGTYPE_USER_ADDED);
        //
        // if(manuallyAddedTagId == null) {
        //     logger.log(Level.WARNING, "USER tag was not found for community "+ communityId);
        //     return;
        // }

        MyPatientListService myPatientListService = getMyPatientListService();

        try {
            ListTagDTO tagDTO = myPatientListService.getTagByType(communityId, TAGTYPE_USER_ADDED);
            if (tagDTO != null) {
                myPatientInfoDTO.getTags().add(tagDTO);
            }
        } catch(Exception ex) {
            logger.log(Level.WARNING, ex.getMessage());
        }
    }

/*  @PUT
    @Consumes({"application/json"})*/
    @Deprecated
    public void update(MyPatientInfoDTO myPatientInfo) throws PortalException {
        try {
            MyPatientListService myPatientListService = getMyPatientListService();

            myPatientListService.update(myPatientInfo);

        } catch (Exception exc) {
            String message = "Error calling: isAlreadyOnList";
            logger.log(Level.WARNING, message, exc);
            throw new RuntimeException(message);
        }

    }

    @DELETE
    @Path("delete/{userId}/{comId}/{patientId}")
    public void delete(@PathParam("userId") long userId, @PathParam("comId") long comId,  @PathParam("patientId") long patientId) throws PortalException {
        try {
            MyPatientListService myPatientListService = getMyPatientListService();

            MyPatientInfoDTO myPatientInfoDTO = new MyPatientInfoDTO( userId,  patientId,  comId);
            myPatientListService.delete(myPatientInfoDTO);

        } catch (Exception exc) {
            String message = "Error calling: isAlreadyOnList";
            logger.log(Level.WARNING, message, exc);
            throw new RuntimeException(message);
        }
    }

    @GET
    @Path("{userId}/{comId}")
    @Produces({"application/json"})
    @WebMethod
    public List<MyPatientInfoDTO> getMyPatientList(@PathParam("userId") long userId, @PathParam("comId") long communityId) throws PortalException {
       List<MyPatientInfoDTO> ret =  new ArrayList();

        try {
            MyPatientListService myPatientListService = getMyPatientListService();

            ret = myPatientListService.getMyPatientList(userId, communityId);

        } catch (Exception exc) {
            String message = "Error calling: getMyPatientList";
            logger.log(Level.WARNING, message, exc);
            throw new RuntimeException(message);
        }
        return ret;
    }
    @GET
    @Path("tag/{userId}/{comId}/{type}/{id}")
    @Produces({"application/json"})
    @WebMethod
    public void updateTags(@PathParam("userId") long userId, @PathParam("comId") long communityId, @PathParam("type") String type, @PathParam("id") String id)throws PortalException {
         try {
            MyPatientListService myPatientListService = getMyPatientListService();

            myPatientListService.updateTags(userId, communityId, type, id);

        } catch (Exception exc) {
            String message = "Error calling: getMyPatientList";
            logger.log(Level.WARNING, message, exc);
            throw new RuntimeException(message);
        }

    }
    @GET
    @Path("count/{userId}/{comId}/{level}/{tags}")
    @Produces({"application/json"})
    @WebMethod
    public String getMyPatientListCount(@PathParam("userId") long userId, @PathParam("comId") long communityId, @PathParam("level") String level, @PathParam("tags") String tags) throws PortalException {
      String ret = null;

        try {
            MyPatientListService myPatientListService = getMyPatientListService();

            ret = myPatientListService.getMyPatientListCount(userId, communityId,level, tags,null);

        } catch (Exception exc) {
            String message = "Error calling: getMyPatientList";
            logger.log(Level.WARNING, message, exc);
            throw new RuntimeException(message);
        }
        return ret;
    }

    @POST
    @Path("count/{userId}/{comId}/{level}/{tags}")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @WebMethod
    public String getMyPatientListCount(@PathParam("userId") long userId, @PathParam("comId") long communityId, @PathParam("level") String level, @PathParam("tags") String tags, List patients) throws PortalException {
      String ret = null;

        try {
            MyPatientListService myPatientListService = getMyPatientListService();

            ret = myPatientListService.getMyPatientListCount(userId, communityId,level, tags, patients);

        } catch (Exception exc) {
            String message = "Error calling: getMyPatientList";
            logger.log(Level.WARNING, message, exc);
            throw new RuntimeException(message);
        }
        return ret;
    }


     @GET
    @Path("{userId}/{comId}/{start}/{stop}/{level}/{tags}")
    @Produces({"application/json"})
    @WebMethod
    public List<MyPatientInfoDTO> getMyPatientListScroll(@PathParam("userId") long userId, @PathParam("comId") long communityId, @PathParam("start") long start, @PathParam("stop") long stop, @PathParam("level") String level, @PathParam("tags") String tags ) throws PortalException {
       StopWatch totalWatch = new StopWatch();
       totalWatch.start();
        List<MyPatientInfoDTO> ret =  new ArrayList();

        try {
            MyPatientListService myPatientListService = getMyPatientListService();

            ret = myPatientListService.getMyPatientListScroll(userId, communityId, start, stop, level, tags,null);

        } catch (Exception exc) {
            String message = "Error calling: getMyPatientList";
            logger.log(Level.WARNING, message, exc);
            throw new RuntimeException(message);
        }
         long total =totalWatch.getTime();
        logger.info("!!!!!!!!!!!!!!!!MPL SERVICE time " + total);
        totalWatch.stop();
        return ret;
    }
    @POST
    @Path("{userId}/{comId}/{start}/{stop}/{level}/{tags}")
    @Produces({"application/json"})
    @Consumes({"application/json"})
    @WebMethod
    public List<MyPatientInfoDTO> getMyPatientListScroll(@PathParam("userId") long userId, @PathParam("comId") long communityId, @PathParam("start") long start, @PathParam("stop") long stop, @PathParam("level") String level, @PathParam("tags") String tags, List patients ) throws PortalException {
       List<MyPatientInfoDTO> ret =  new ArrayList();

        try {
            MyPatientListService myPatientListService = getMyPatientListService();

            ret = myPatientListService.getMyPatientListScroll(userId, communityId, start, stop, level, tags, patients);

        } catch (Exception exc) {
            String message = "Error calling: getMyPatientList";
            logger.log(Level.WARNING, message, exc);
            throw new RuntimeException(message);
        }
        return ret;
    }


  /*  @GET
    @Path("patient/{patientId}/{communityId}")
    @Produces({"application/json"})
    @WebMethod
    public PersonDTO getPatient(@PathParam("patientId")long patientId,@PathParam("communityId") long communityId) throws PortalException {
        PersonDTO ret =  null;

        try {
            MyPatientListService myPatientListService = getMyPatientListService();

            ret = myPatientListService.getPatient(patientId, communityId);

        } catch (Exception exc) {
            String message = "Error calling: isAlreadyOnList";
            logger.log(Level.WARNING, message, exc);
            throw new RuntimeException(message);
        }
        return ret;
    }*/
    @Deprecated
    public PersonDTO getPatient(@PathParam("patientId")long patientId,@PathParam("communityId") long communityId) throws PortalException {
        PersonDTO ret =  null;
        return ret;
    }

    @GET
    @Path("tags/{comId}")
    @Produces({"application/json"})
    @WebMethod
    public List<ListTagDTO> getListTags( @PathParam("comId") long communityId) throws PortalException {
       List<ListTagDTO> ret =  new ArrayList();

        try {
            MyPatientListService myPatientListService = getMyPatientListService();

            ret = myPatientListService.getListTags(communityId);

        } catch (Exception exc) {
            String message = "Error calling: getMyPatientList";
            logger.log(Level.WARNING, message, exc);
            throw new RuntimeException(message);
        }
        return ret;
    }
     public void delete(MyPatientInfoDTO mplDTO){

     }

    @GET
    @Path("isOn/{userId}/{patientId}/{comId}")
    @Produces({"application/json"})
    public boolean isAlreadyOnList(@PathParam("userId") long userid, @PathParam("patientId")  long patientId, @PathParam("comId") long communityId) throws PortalException {
         boolean ret = false;
         try {
            MyPatientListService myPatientListService = getMyPatientListService();

            ret = myPatientListService.isAlreadyOnList(userid, patientId, communityId);

        } catch (Exception exc) {
            String message = "Error calling: isAlreadyOnList";
            logger.log(Level.WARNING, message, exc);
            throw new RuntimeException(message);
        }
        return ret;
    }

    @Deprecated
    public boolean isListFull(long userid, long communityId) throws PortalException {
       boolean ret = false;
         try {
            MyPatientListService myPatientListService = getMyPatientListService();

            ret = myPatientListService.isListFull(userid, communityId);

        } catch (Exception exc) {
            String message = "Error calling: isAlreadyOnList";
            logger.log(Level.WARNING, message, exc);
            throw new RuntimeException(message);
        }
        return ret;
    }
 @GET
    @Path("hasConsent/{userId}/{patientId}/{comId}")
    @Produces({"application/json"})
    public boolean hasConsent(@PathParam("userId") long userId, @PathParam("patientId")  long patientId, @PathParam("comId") long communityId) throws PortalException {
       boolean ret = false;
         try {
            MyPatientListService myPatientListService = getMyPatientListService();

            ret = myPatientListService.hasConsent(userId, patientId, communityId);

        } catch (Exception exc) {
            String message = "Error calling: isAlreadyOnList";
            logger.log(Level.WARNING, message, exc);
            throw new RuntimeException(message);
        }
        return ret;
    }

    public String getConfigurationValue(long communityId, String defaultValue, String key, String subKey) {
        MyPatientListService myPatientListService = getMyPatientListService();

        return myPatientListService.getConfigurationValue(communityId, defaultValue, key, subKey);
    }
   protected MyPatientListService getMyPatientListService() {

        MyPatientListService myPatientListService = (MyPatientListService) context.getAttribute(WebConstants.MY_PATIENT_LIST_SERVICE_KEY);

        return myPatientListService;
    }

    public ListTagDTO getTagByType(long communityId, String type) throws Exception {
        return null;
    }

    @Override
    public PatientListLabelDTO myPatientListSelectionDisplayOptions(long userid, long patientId, long communityId) throws PortalException {
        throw new UnsupportedOperationException("com.gsihealth.dashboard.rest.service.MyPatientListRestService.myPatientListSelectionDisplayOptions - Implement me stupid");
    }
}
