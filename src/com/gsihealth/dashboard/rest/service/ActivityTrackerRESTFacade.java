/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.rest.service;

import com.gsihealth.entity.PatientActivityHistory;
import com.gsihealth.entity.PatientActivityNames;
import com.gsihealth.entity.PatientActivityTracker;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author ssingh
 */
@Path("activity")
public class ActivityTrackerRESTFacade {
    
    private Logger logger = Logger.getLogger(getClass().getName());
    
    public ActivityTrackerRESTFacade(){
        
    }
    
    private EntityManagerFactory getEntityManagerFactory() throws NamingException {
      String persistenceUnitName = "dashboardPU";
        String persistenceUnitNameStandAlone = "dashboardPU_standalone";
        EntityManagerFactory emf = getEntityManagerFactory(persistenceUnitName, persistenceUnitNameStandAlone);
        return emf;
    }
    protected EntityManagerFactory getEntityManagerFactory(String persistenceUnitName, String persistenceUnitNameStandAlone) throws RuntimeException {
        // JPA setup...
        EntityManagerFactory emf = null;
        EntityManager manager = null;

        try {
            // look for datasource first
            logger.info("Looking up: " + persistenceUnitName);
            emf = Persistence.createEntityManagerFactory(persistenceUnitName);
            logger.info("emf = " + emf);
            manager = emf.createEntityManager();
            logger.info("manager = " + manager);
        } catch (Exception exc) {

            logger.info("Could not lookup: " + persistenceUnitName);

         
        }

        if (manager == null) {
            String msg = "Unable to get connection to database.";
            logger.severe("\n\n>>>>>>>>> ERROR:" + msg + " <<<<<<<<<<<<<\n\n");
            throw new RuntimeException(msg);
        }

        return emf;
    }
    
    protected EntityManager getEntityManager(){
        System.out.println("In getEntityManager  ");
        try {
           
            return getEntityManagerFactory().createEntityManager();
           
        } catch (NamingException ex) {
            Logger.getLogger(PatientProgramRESTFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @GET
    @Path("/get/{patId}/{comId}")
    @Produces({"application/json"})
    public List<PatientActivityTracker> getActivePatientActivities(@PathParam("patId") BigInteger patientId, @PathParam("comId") BigInteger communityId) {
        EntityManager em = getEntityManager();
        try {
            List<PatientActivityTracker> ret = new ArrayList();
            String sql1 = "SELECT pat.*,pan.name FROM patient_activity_tracker pat\n" +
"INNER JOIN patient_activity_names pan on pat.activity_name_id=pan.id AND pan.visible='Y'\n" +
"WHERE pat.patient_id=?1 AND pat.community_id=?2 ";

            Query query = em.createNativeQuery(sql1).setParameter(1, patientId).setParameter(2, communityId);
             List<Object[]> list=query.getResultList();
                for(Object[] temp:list){
                   PatientActivityTracker activities= new PatientActivityTracker();
                   activities.setId((Long)temp[0]);
                   activities.setUserId((Long)temp[1]);
                   activities.setPatientId((Long)temp[2]);
                   activities.setActivityId((Long)temp[3]);
                   activities.setActivityValue((String)temp[4]);
                   activities.setCreationDate((Date)temp[5]);
                   activities.setLastUpdatedDate((Date)temp[6]);
                   activities.setCommunityId((Long)temp[7]);
                   activities.setEffectiveDate((Date)temp[8]);
                   activities.setActivityName((String)temp[9]);
                   ret.add(activities);
                }
     
            return ret;
        } finally {
            em.close();
        }
    }
   
    @GET
    @Path("/activityname/{comId}/{activityNameId}")
    @Produces({"application/json"})
    public  PatientActivityNames findActivityName(@PathParam("comId") BigInteger communityId , @PathParam("activityNameId") BigInteger activityNameId){
        EntityManager em = getEntityManager();
                    List<PatientActivityNames> ret = new ArrayList<>();

        try {
            String sql1 = "SELECT * FROM patient_activity_names \n"
                    + " WHERE community_id=?1 and visible ='Y' AND id=?2 \n";

            Query query = em.createNativeQuery(sql1).setParameter(1,  communityId).setParameter(2,activityNameId);
            List<Object[]> list=query.getResultList();
                for(Object[] temp:list){
                   PatientActivityNames activity= new PatientActivityNames();
                   activity.setId((Long)temp[0]);
                   activity.setName((String)temp[1]);
                   activity.setCommunityId((Long)temp[3]);
                 ret.add(activity);
                 
                }
            return ret.get(0);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            em.close();
        }
        return ret.get(0);
    }
    
    @GET
    @Path("/activitynames/{comId}")
    @Produces({"application/json"})
    public  List<PatientActivityNames> findActivityNames(@PathParam("comId") BigInteger communityId){
        EntityManager em = getEntityManager();
                    List<PatientActivityNames> ret = new ArrayList<>();

        try {
            String sql1 = "SELECT * FROM patient_activity_names \n"
                    + " WHERE community_id=?1 and visible ='Y' \n";

            Query query = em.createNativeQuery(sql1).setParameter(1,  communityId);
            List<Object[]> list=query.getResultList();
                for(Object[] temp:list){
                   PatientActivityNames activity= new PatientActivityNames();
                   activity.setId((Long)temp[0]);
                   activity.setName((String)temp[1]);
                   activity.setCommunityId((Long)temp[3]);
                 ret.add(activity);
                 
                }
            return ret;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            em.close();
        }
        return ret;
    }
    
    
    //---------------------Add --------------------- Update--------------------- Delete---------------------//
    @POST
    @Path("add")
    @Consumes({"application/json"})
    public String addPatientActivity(PatientActivityTracker entity) {
        EntityManager em = getEntityManager();
        EntityTransaction tx = null;
        try {
            tx = em.getTransaction();
            tx.begin();
            em.persist(entity);
            em.flush();
            em.clear();
            // Add patient activity into history
            PatientActivityHistory pnh = getActivityHistory(entity, "Add");
            em.persist(pnh);
            tx.commit();
        } catch (Exception exc) {
            logger.log(Level.SEVERE, "Error adding patient activity: ", exc);
            if ((tx != null) && tx.isActive()) {
                tx.rollback();
            }
        } finally {
            if (em != null && em.isOpen()) {
                em.close();
            }
        }
        return "success";
    }
    
      private PatientActivityHistory getActivityHistory(PatientActivityTracker entity, String actionType){
          PatientActivityHistory pah = new PatientActivityHistory();
            pah.setActionDate(new Date());
            pah.setActionSuccess('Y');
            pah.setActionType(actionType);
            pah.setActivityEffectiveDate(entity.getEffectiveDate());
            pah.setActivityName(entity.getActivityName());
            pah.setActivityValue(entity.getActivityValue());
            pah.setCommunityId(entity.getCommunityId());  
            pah.setPatientId(entity.getPatientId());
            pah.setUserId(entity.getUserId());
            return pah;
        
    }
      
    @PUT
    @Path("update")
    @Consumes({"application/json"})
    public String edit(PatientActivityTracker entity) {
          EntityManager em=getEntityManager();
          EntityTransaction tx =null;
        try {
           
            tx = em.getTransaction();
            tx.begin();           
            em.merge(entity);
            em.flush();
            em.clear();
        // Add to patient activity history
        PatientActivityHistory pah = getActivityHistory(entity, "Update");
        em.persist(pah);
        tx.commit();
        } 
         catch (Exception exc) {
			logger.log(Level.SEVERE, "Error updating patient activity: " , exc);
			
			if ((tx != null) && tx.isActive()) {
				tx.rollback();
			}			
		}
        finally {
          if(em!=null && em.isOpen()){
            em.close();
            }
        }
         return "success";
    }
  
    @DELETE
    @Path("delete/{id}")
    public String remove(@PathParam("id") Long id) {
        EntityManager em = getEntityManager();
         EntityTransaction tx =null;
        try {
            PatientActivityTracker entity = em.find(PatientActivityTracker.class, id);
            entity.setActivityName(findActivityName(BigInteger.valueOf(entity.getCommunityId()),BigInteger.valueOf(entity.getActivityId())).getName());
            tx = em.getTransaction();
            tx.begin();
            em.remove(entity);            
            em.flush();
            em.clear();
        // Add to patient activity history
        PatientActivityHistory pah = getActivityHistory(entity,"Delete");
        em.persist(pah);
        tx.commit();            
        
        } 
        catch (Exception exc) {
			logger.log(Level.SEVERE, "Error deleting patient activity: " , exc);
			

			if ((tx != null) && tx.isActive()) {
				tx.rollback();
			}                       			
		}
        finally {
           if(em!=null && em.isOpen()){
            em.close();
            }
        }
        return "success";
    }
    
    @GET
    @Path("/getActivitiesCount/{patId}/{comId}")
    @Produces({"text/plain"})
    public String getPatientActivitiesCount(@PathParam("patId") BigInteger patientId, @PathParam("comId") BigInteger communityId) {
        EntityManager em = getEntityManager();
        int count=0;
        try {
            
            String sql1 = "SELECT COUNT(*) FROM patient_activity_tracker pat\n" +
"INNER JOIN patient_activity_names pan on pat.activity_name_id=pan.id AND pan.visible='Y'\n" +
"WHERE pat.patient_id=?1 AND pat.community_id=?2 ";

            Query query = em.createNativeQuery(sql1).setParameter(1, patientId).setParameter(2, communityId);
            count =((Long)query.getSingleResult()).intValue();
            
            return String.valueOf(count);
        } finally {
            em.close();
        }
    }
}
