package com.gsihealth.dashboard.rest.service;

import com.gsihealth.dashboard.common.Constants;
import com.gsihealth.dashboard.common.util.AppConfigConstants;
import com.gsihealth.dashboard.entity.dto.ApplicationDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.ConfigurationContext;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.dao.*;
import com.gsihealth.dashboard.server.service.BasePatientService;
import com.gsihealth.dashboard.server.util.*;
import com.gsihealth.entity.*;
import org.apache.commons.lang.StringUtils;

import javax.jws.WebMethod;
import javax.persistence.NoResultException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Path("login")
public class LoginResultService extends BasePatientService {

    private static final int MAX_DAYS_SINCE_LAST_LOGIN = 90;
    private static final int MAX_DAYS_SINCE_LAST_PASSSWORD_CHANGE = 90;
    private static final int MAX_FAILED_ACCESS_ATTEMPTS = 5;

    @Context
    HttpServletRequest request;

    @Context
    ServletContext context;

    private Logger logger = Logger.getLogger(getClass().getName());

    private String tokenId = "";

    private String realm = null;

    @GET
    @Produces({"application/json"})
    @WebMethod
    public LoginResult getUser() {
        HttpSession session = request.getSession();
        LoginResult loginResult = (LoginResult) session.getAttribute(WebConstants.LOGIN_RESULT_KEY);
        // not a valid user, blow it all away
        if (loginResult != null && !loginResult.isAuthenticated()) {
            loginResult = null;
        }
        return loginResult;
    }

    public static final class LoginObject {

        private String email;
        private String password;
        private Long communityId;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public Long getCommunityId() {
            return communityId;
        }

        public void setCommunityId(Long communtyId) {
            this.communityId = communtyId;
        }

    }

    @POST
    @Produces({"application/json"})
    @Consumes({MediaType.APPLICATION_JSON})
    @WebMethod
    public LoginResult authenticate(LoginObject object) {

        String email = null;
        String errorMessage = null;

        LoginResult loginResult = new LoginResult();

        try {

            email = object.getEmail();
            String password = object.getPassword();
            Long communityId = object.getCommunityId();

            logger.info("authenticate():  email=" + email);

            // Get user
            User user = null;
            UserCommunityOrganization uco = null;
            OrganizationCwd organization = null;

            try {

                //ServletContext application = getServletContext();
                //ServletContext application = context;
                // convert email address to lowercase
                email = email.toLowerCase();

                //Inject Dao
                userDAO = (UserDAOImpl) context.getAttribute(WebConstants.USER_DAO_KEY);
                applicationDAO = (ApplicationDAOImpl) context.getAttribute(WebConstants.APPLICATION_DAO_KEY);
                auditDAO = (AuditDAOImpl) context.getAttribute(WebConstants.AUDIT_DAO_KEY);
                sSOUserDAO = (SSOUserDAOImpl) context.getAttribute(WebConstants.SSO_USER_DAO_KEY);

                //1.Find User by portal id.  
                user = userDAO.findUserByEmail(email, communityId);
                uco = user.getUserCommunityOrganization(communityId);

                organization = uco.getOrganizationCwd();

                loginResult.setEmail(user.getEmail());
                loginResult.setSpecial(password);
                loginResult.setDirectAddress(uco.getDirectAddress());
                int authId = (user.getAuthyId() == null) ? 0 : user.getAuthyId().intValue();
                loginResult.setAuthyId(authId);
                loginResult.setDisableUserMfa(user.getDisableUserMfa());
                loginResult.setLastLoginDate(user.getLastSuccessfulLoginDate());

                loginResult.setUserLevel(uco.getRole().getRoleName());

                loginResult.setUserLevelId(uco.getRole().getPrimaryRoleId());

                loginResult.setAccessLevel(uco.getAccessLevel().getAccessLevelDesc());

                loginResult.setAccessLevelId(uco.getAccessLevel().getAccessLevelPK().getAccessLevelId());
                loginResult.setUserId(user.getUserId());
                loginResult.setCanManagePowerUser(user.getCanManagePowerUser());
                loginResult.setReportingRole(user.getReportingRole());
                logger.info("REPORTING_ROLE=" + user.getReportingRole() + "--");

                String organizationName = organization.getOrganizationCwdName();
                loginResult.setOrganizationName(organizationName);

                long organizationId = organization.getOrgId();
                loginResult.setOrganizationId(organizationId);

                String oid = organization.getOid();
                loginResult.setOid(oid);

                logger.info("set loginResult for " + email + " communityId to " + communityId);
                loginResult.setCommunityId(communityId);

                //Community OID & Name
                String communityOid = uco.getCommunity().getCommunityOid();
                String communityName = uco.getCommunity().getCommunityName();
                loginResult.setCommunityOid(communityOid);
                loginResult.setCommunityName(communityName);
                logger.info("set loginResult for " + email + " community OID to " + communityOid);
                logger.info("set loginResult for " + email + " community Name to " + communityName);
                

                boolean hasEula = getEula(user);
                loginResult.setEula(hasEula);

                boolean hasSamhsa = getSamhsa(user,communityId);
                loginResult.setSamhsa(hasSamhsa);

                //DASHBOARD - 716 MFA
                String countryCode = getCountryCode(user);
                loginResult.setCountryCode(countryCode);

                loginResult.setLastMfaVerificationDate(user.getLastMfaVerificationDate());

                //DASHBOARD-780 Children's Health Home
                loginResult.setCanConsentEnrollMinors(uco.getRole().isCanConsentEnrollMinors());

                ConfigurationDAO configurationDAO = ConfigurationContext.getInstance().getConfigurationDAO();

                // Redux
                int userInterfaceType = (user.getUserInterfaceType() == null) ? 0 : Integer.parseInt(user.getUserInterfaceType());
                int comUserInt = Integer.parseInt(configurationDAO.getProperty(communityId, "user.interfacetype", "0"));

                // If the client doesn't allow Redux...
                if (comUserInt == 0) {
                    userInterfaceType = 0;
                } else // If the client forces Redux...
                {
                    if (comUserInt == 1) {
                        userInterfaceType = 1;
                    } else {
                        // If the client allows user choosing of Redux...
                        userInterfaceType = userInterfaceType + (comUserInt * 256);
                    }
                }
                loginResult.setUserInterfaceType(userInterfaceType);

                //set user settings
                loginResult.setSettings(user.getSettings());

                // replaces SSO_LOGOUT_URL_KEY
                String logoutUrl = configurationDAO.getProperty(communityId, "sso.logout.url");
                loginResult.setLogoutUrl(logoutUrl);

                //Treat Restful SSO - Treat Fedlets replaces old SSO_TREAT_URL_KEY
                String treatUrl = this.getAppUrl(applicationDAO, AppConfigConstants.CAREPLAN_APP_ID, communityId);
                loginResult.setTreatUrl(treatUrl);

                // replace SSO_TREAT_FEATURES_KEY
                String treatFeatures = configurationDAO.getProperty(communityId, "sso.treat.features");
                loginResult.setTreatFeatures(treatFeatures);

                // replace SSO_TREAT_NAME_KEY
                String treatName = configurationDAO.getProperty(communityId, "sso.treat.name");
                loginResult.setTreatName(treatName);

                // replace TREAT_RESTFUL_SSO_ENABLED_KEY
                boolean treatRestfulSSOEnabled = this.isAppRestfulSSOEnabled(applicationDAO, AppConfigConstants.CAREPLAN_APP_ID,
                        communityId);
                loginResult.setTreatRestfulSSOEnabled(treatRestfulSSOEnabled);

                // replace SSO_AVADO_URL_KEY
                String avadoUrl = getAppUrl(applicationDAO, AppConfigConstants.PATIENT_ENGAGEMENT_APP_ID, communityId);
                loginResult.setAvadoUrl(avadoUrl);

                // replace SSO_AVADO_FEATURES_KEY
                String avadoFeatures = configurationDAO.getProperty(communityId, "sso.avado.features");
                loginResult.setAvadoFeatures(avadoFeatures);

                // replace SSO_AVADO_NAME_KEY
                String avadoName = configurationDAO.getProperty(communityId, "sso.avado.name");
                loginResult.setAvadoName(avadoName);

                // replace SSO_AVADO_USEPOST_KEY
                String avadoUsePost = configurationDAO.getProperty(communityId, "sso.avado.usepost");
                loginResult.setAvadoUsePost(avadoUsePost);

                // replace SSO_AVADO_USER_LIST_KEY
                String avadoUserList = configurationDAO.getProperty(communityId, "sso.avado.user.list", "");
                loginResult.setAvadoUserList(avadoUserList);

                // replace SSO_SESSION_URL_KEY
                String sessionUrl = configurationDAO.getProperty(communityId, "sso.session.url");
                loginResult.setSessionUrl(sessionUrl);

                // replace SSO_SESSION_FEATURES_KEY
                String sessionFeatures = configurationDAO.getProperty(communityId, "sso.session.features");
                loginResult.setSessionFeatures(sessionFeatures);

                // replace SSO_SESSION_NAME_KEY
                String sessionName = configurationDAO.getProperty(communityId, "sso.session.name");
                loginResult.setSessionName(sessionName);

                // replace SSO_SESSION_GOTO_KEY
                String sessionGoto = configurationDAO.getProperty(communityId, "sso.session.goto");
                loginResult.setSessionGoto(sessionGoto);

                // replace SSO_PENTAHO_SERVER_KEY
                String ssoPentahoServer = configurationDAO.getProperty(communityId, "sso.pentaho.server");
                loginResult.setSsoPentahoServer(ssoPentahoServer);

                String pentahoLandingPage = configurationDAO.getProperty(communityId, "pentaho.lnading.page");
                loginResult.setPentahoLandingPage(pentahoLandingPage);

                // get openAMinstance
                URLGenerator urlGenerator = new URLGenerator(communityId);
                URI authenticateURI = urlGenerator.getAuthenticateURI();
                realm = urlGenerator.getRealm();
                String openAMinstance = authenticateURI.toString().replaceAll("authenticate", "").replace('?', ' ');
                openAMinstance = openAMinstance.trim();
                loginResult.setOpenAMinstance(openAMinstance);

                // replace CAREBOOK_SUMMARY_URL_KEY
                String carebookSummaryURL = configurationDAO.getProperty(communityId, "sso.carebook.summary.URL");
                loginResult.setCarebookSummaryURL(carebookSummaryURL);

                // replace DISENROLLMENT_CODE_TITLE
                String disenrollmentCodeTitle = configurationDAO.getProperty(communityId, "disenrollment.code.title");
                loginResult.setDisenrollmentCodeTitle(disenrollmentCodeTitle);

                // replace PLCS_TITLE
                String plcsTitle = configurationDAO.getProperty(communityId, "plcs.title");
                loginResult.setPlcsTitle(plcsTitle);

                //Get map of all applications in Application Table
                List<Application> allApps = (List<Application>) context.getAttribute(WebConstants.APPLICATION_LIST_KEY);
                List<ApplicationDTO> allAppDTOs = getAllApps(allApps);
                loginResult.setAllApps(allAppDTOs); //FIXME WHAT IS THIS FOR?

                // store login result in session. it will be used later for auditing
                storeInSession(loginResult);
                storeInSession(password);
                storeUserInSession(user, loginResult.getCommunityId());

                // POLLING INTERVAL
                int pollingIntervalInSeconds = Integer.parseInt(configurationDAO.getProperty(communityId, "polling.interval.in.seconds", "300"));
                loginResult.setPollingIntervalInSeconds(pollingIntervalInSeconds);

                boolean pollingEnabled = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "polling.enabled", "true"));
                logger.info("pollingEnabled=" + pollingEnabled);
                loginResult.setPoolingEnabled(pollingEnabled);

                logger.info("polling.interval.in.seconds=" + pollingIntervalInSeconds);

                //County field By Default it is true
                boolean countyEnabled = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "county.enabled","true"));
                logger.info("countyEnabled=" + countyEnabled);
                boolean countyRequired = Boolean.parseBoolean(configurationDAO.getProperty(communityId, "county.required","true"));
                logger.info("countyRequired=" + countyRequired);
                loginResult.setCountyEnabled(countyEnabled);
                loginResult.setCountyRequired(countyRequired);

            } catch (NoResultException exc) {
                errorMessage = "Invalid user id: " + email;
                logger.log(Level.WARNING, errorMessage);
                loginResult.setAuthenticated(false);
                loginResult.setErrorMessage("Invalid UserID/Password.");
                long theCommunityId = loginResult.getCommunityId();
                audit(email, errorMessage, AuditType.USER_LOGGED_IN, false, theCommunityId);

                return loginResult;
            }

            Integer failedAccessAttempts = user.getFailedAccessAttempts();

            if (failedAccessAttempts == null) {
                failedAccessAttempts = 0;
            }

            // add other data
            loginResult.setFirstName(user.getFirstName());
            loginResult.setLastName(user.getLastName());

            // check password
            boolean passwordMatch = isPasswordMatch(communityId, email, password, realm);

            if (passwordMatch) {
                loginResult.setAuthenticated(true);
                //save tokenId for later
                loginResult.setToken(tokenId);
                logger.info("Save tokenId in Login Result: -" + loginResult.getToken() + "-");

                //pass to /war/login/post_for_Avado.jsp
                storeTokenIdInSession(tokenId);

                if (user.isDeleted()) {
                    loginResult.setErrorMessage("Your account is deleted. Please contact Administrator.");
                    loginResult.setAuthenticated(false);

                    audit(user, loginResult, "Account is deleted", AuditType.USER_LOGGED_IN, false);
                    return loginResult;
                }

                if (!uco.getStatus().equals("ACTIVE")) {
                    loginResult.setErrorMessage("Your account is inactive. Please contact Administrator.");
                    loginResult.setAuthenticated(false);
                    audit(user, loginResult, "Account is inactive", AuditType.USER_LOGGED_IN, false);
                    return loginResult;
                }

                if (!organization.getCommunityOrganization(communityId).getStatus().equalsIgnoreCase("ACTIVE")) {
                    loginResult.setErrorMessage("Your account has been disabled due to the inactivation of your Organization.");
                    loginResult.setAuthenticated(false);
                    audit(user, loginResult, "Account is disabled to inactivation of organization", AuditType.USER_LOGGED_IN, false);
                    return loginResult;
                }

                // check days since last login
                int daysSinceLastLogin = getDaysSinceLastLogin(user);

                if (daysSinceLastLogin > MAX_DAYS_SINCE_LAST_LOGIN) {
                    loginResult.setErrorMessage("Your account has been disabled. The maximum days since last login has exceeded " + MAX_DAYS_SINCE_LAST_LOGIN + " days. Please contact your Local Administrator to enable your account.");
                    loginResult.setAuthenticated(false);
                    audit(user, loginResult, "Account is disabled. Maximum days since last login has exceeded " + MAX_DAYS_SINCE_LAST_LOGIN + " days", AuditType.USER_LOGGED_IN, false);
                    return loginResult;
                }

                // check days since last password change
                int daysSinceLastPasswordChanged = getDaysSinceLastChange(user.getLastPasswordChangeDate(), new Date());
                if (daysSinceLastPasswordChanged > MAX_DAYS_SINCE_LAST_PASSSWORD_CHANGE) {
                    user.setMustChangePassword(WebConstants.YES);
                }

                // Handle for password reset
                if (user.isMustChangePassword()) {
                    loginResult.setErrorMessage("You must change your password.");
                    loginResult.setAuthenticated(true);
                    loginResult.setMustChangePassword(true);
                }

                //reset failed access attempts back to zero
                user.setFailedAccessAttempts(0);
                user.setLastSuccessfulLoginDate(new Date());
                userDAO.update(user);

                long userId = user.getUserId();

                // get a list of app keys for this user
                //
                //  THIS IS LEGACY
                logger.info("BasePortalServiceServlet communityId:" + communityId);
                List<String> appKeys = applicationDAO.getUserApplicationNames(userId, communityId);

                loginResult.setUserAppKeys(appKeys);

                // THIS IS NEW
                List<ApplicationDTO> apps = getUserApps(userId, communityId);
                loginResult.setUserApps(apps);

                audit(user, loginResult, "Log in successful.", AuditType.USER_LOGGED_IN);

            } else if (!uco.getStatus().equals("ACTIVE")) {
                loginResult.setErrorMessage("Your account is disabled. Please contact Administrator.");
                loginResult.setAuthenticated(false);
                audit(user, loginResult, "Account is disabled", AuditType.USER_LOGGED_IN, false);
                return loginResult;
            } else if (failedAccessAttempts < MAX_FAILED_ACCESS_ATTEMPTS) {
                Date today = new Date();
                logger.info("User last update: " + user.getLastUpdateDate());
                boolean resetFailedAccessAttempts = isMoreThan24hours(user.getLastUpdateDate(), today);
                logger.info("Reset fail access attempts:" + resetFailedAccessAttempts);

                if (resetFailedAccessAttempts) {
                    failedAccessAttempts = 0;
                }

                failedAccessAttempts++;
                loginResult.setErrorMessage("Invalid password. Failed attempt: " + failedAccessAttempts + "\n" + " Your log-in will be disabled on the 5th failed attempt.");
                user.setFailedAccessAttempts(failedAccessAttempts);
                userDAO.update(user);

                audit(user, loginResult, "Invalid password. Failed attempt: " + failedAccessAttempts, AuditType.USER_LOGGED_IN, false);
            } else {
                loginResult.setErrorMessage("Login Failed: Account Disabled Failed Login \n attempt: 5. Contact your administrator");
                user.setFailedAccessAttempts(0);
                uco.setStatus("INACTIVE");
                userDAO.update(user);
                audit(user, loginResult, "Login Failed: Account Disabled Failed Login \n attempt: 5", AuditType.USER_LOGGED_IN, false);
            }

        } catch (Exception exc) {
            final String message = "Error during authentication: " + email;
            logger.logp(Level.SEVERE, getClass().getName(), "authenticate", message, exc);
            loginResult.setAuthenticated(false);
            loginResult.setErrorMessage(message);
        }

        logger.info("loginResult.isAuthenticated : " + loginResult.isAuthenticated() + ":CommunityId:" + loginResult.getCommunityId());

        //4.return authenticated user
        return loginResult;

    }

    private String getCountryCode(User user) {

        String countryCode = null;
        if (user.getMobileNumber() != null && user.getMobileNumber().length() != 10) {
            String number = user.getMobileNumber();
            String mobileNo = number.substring(number.length() - 10, number.length());
            countryCode = number.replace(mobileNo, "");

        }
        return countryCode;
    }

    protected boolean isPasswordMatch(long communityId, String portalUserId, String portalPassword, String realm) {
        try {
            logger.info("isPasswordMatch");
            logger.info("Get token from OpenAM");
            tokenId = sSOUserDAO.authenticate(communityId, portalUserId, portalPassword);
            tokenId = StringUtils.trimToEmpty(tokenId);
            logger.info("token result from OpenAM:-" + tokenId + "-");

            boolean passwordMatch = !tokenId.isEmpty();
            logger.info("passwordMatch=" + passwordMatch);

            return passwordMatch;

        } catch (Exception e) {
            logger.info("passwordMatch failed");
            logger.info("Error getting token from OpenAM: " + e);
            logger.logp(Level.SEVERE, getClass().getName(), "authenticate", "Error authenticating", e);
            return false;
        }
    }

    protected boolean isMoreThan24hours(Date startDate, Date endDate) {

        int daysElapsed = DateUtils.daysBetween(startDate, endDate);

        logger.info("Days Elapsed: " + daysElapsed);

        return (daysElapsed >= 1);
    }

    private int getDaysSinceLastLogin(User user) {
        Date now = new Date();
        Date lastLoginDate = user.getLastSuccessfulLoginDate();
        int daysSinceLastLogin = DateUtils.daysBetween(lastLoginDate, now);
        return daysSinceLastLogin;
    }

    private void storeInSession(LoginResult loginResult) {
        //HttpServletRequest request = getThreadLocalRequest();
        HttpSession session = request.getSession();
        session.setAttribute(WebConstants.LOGIN_RESULT_KEY, loginResult);
    }

    private boolean getEula(User user) {
        return user.getEulaAccepted() != null && user.getEulaAccepted() == Constants.YES_CHAR;
    }

    private boolean getSamhsa(User user,long communityId) {
        return user.getUserCommunityOrganization(communityId).getSamhsaAccepted() != null && user.getUserCommunityOrganization(communityId).getSamhsaAccepted().charValue() ==  Constants.NO_CHAR;
    }

    private void storeUserInSession(User user, Long communityId) {
        //HttpServletRequest request = getThreadLocalRequest();
        HttpSession session = request.getSession();
        session.setAttribute(WebConstants.USER_KEY, user);
        session.setAttribute(WebConstants.USER_ID_KEY, user.getEmail());
        session.setAttribute(WebConstants.REPORTING_ROLE_KEY, user.getReportingRole());
        session.setAttribute(WebConstants.USER_COMMUNITY_KEY, communityId);

    }

    private void storeTokenIdInSession(String tokenId) {
        //HttpServletRequest request = getThreadLocalRequest();
        HttpSession session = request.getSession();
        session.setAttribute(WebConstants.SSO_TOKEN_KEY, tokenId);

    }

    private void storeInSession(String portalPassword) {
        //HttpServletRequest request = getThreadLocalRequest();
        HttpSession session = request.getSession();
        session.setAttribute(WebConstants.PW_KEY, portalPassword);
    }

    private void audit(User user, LoginResult loginResult, String comment, String auditType) {
        audit(user, loginResult, comment, auditType, true);
    }

    private void audit(String email, String comment, String auditType, long communityId) {

        audit(email, comment, auditType, true, communityId);
    }

    private void audit(String email, String comment, String auditType, boolean actionSuccess, long communityId) {
        try {
            Audit audit = new Audit();
            audit.setUserEmail(email);
            audit.setActionComments(comment);
            audit.setActionType(auditType);
            audit.setCommunityId(communityId);

            String success = actionSuccess ? com.gsihealth.dashboard.common.Constants.YES : com.gsihealth.dashboard.common.Constants.NO;
            audit.setActionSuccess(success);

            auditDAO.addAudit(audit);
        } catch (Exception exc) {
            String message = "Error AUDITING: " + exc.getMessage();
            logger.log(Level.SEVERE, message, exc);
        }

    }

    private void audit(User user, LoginResult loginResult, String comment, String auditType, boolean actionSuccess) {

        long communityId = loginResult.getCommunityId();
        try {
            Audit audit = AuditUtils.convertUser(loginResult, user);
            audit.setActionComments(comment);
            audit.setActionType(auditType);
            audit.setCommunityId(communityId);

            String success = actionSuccess ? com.gsihealth.dashboard.common.Constants.YES : com.gsihealth.dashboard.common.Constants.NO;
            audit.setActionSuccess(success);

            auditDAO.addAudit(audit);
        } catch (Exception exc) {
            String message = "Error AUDITING user: " + user.getEmail() + ",   " + exc.getMessage();
            logger.log(Level.SEVERE, message, exc);
        }
    }

    private boolean isAppRestfulSSOEnabled(ApplicationDAO applicationDAO, long applicationId, long communityID) {

        //long communityId=com.gsihealth.dashboard.server.dao.Constants.GSI_COMMUNITY_ID;
        Application theApp = applicationDAO.findApplication(applicationId, communityID);

        boolean isSsoEnabled = theApp.getIsSsoEnabled();

        return isSsoEnabled;
    }

    private String getAppUrl(ApplicationDAO applicationDAO, long applicationId, long communityID) {

        //   long communityId=com.gsihealth.dashboard.server.dao.Constants.GSI_COMMUNITY_ID;
        Application theApp = applicationDAO.findApplication(applicationId, communityID);

        String appUrl = theApp.getExternalApplicationUrl();

        return appUrl;
    }

    //work around for comminity id 
    private List<ApplicationDTO> getUserApps(long userId, long communityID) {

        logger.info(" getUserApps ***** userId " + userId + " communityID..." + communityID);
        // get from database
        List<Application> apps = applicationDAO.getUserApplications(userId, communityID);

        if (!apps.isEmpty()) {
            logger.info(" getUserApps ***** apps.size " + apps.size());
        }

        // convert entity objects to DTOs
        List<ApplicationDTO> appDtos = UserApplicationUtils.convert(apps);

        return appDtos;
    }

    private List<ApplicationDTO> getAllApps(List<Application> apps) {

        // convert entity objects to DTOs
        List<ApplicationDTO> appDtos = UserApplicationUtils.convert(apps);
        if (!appDtos.isEmpty()) {
            logger.info("getAllApps appDtos.size:" + appDtos.size());
        }

        return appDtos;
    }

    public int getDaysSinceLastChange(Date old, Date now) {

        int days = 0;
        try {
            Date lastPasswordChangedDate = old;
            int daysSinceLastChangedPassword = DateUtils.daysBetween(lastPasswordChangedDate, now);
            days = daysSinceLastChangedPassword;
        } catch (NullPointerException npe) {
            //   logger.log(Level.WARNING, "Nullpointer: Last password changed date is null for: {0}{1}", new Object[]{user.getFirstName(), user.getLastName()});
        }
        return days;
    }

    protected Long getCommunityIdFromSession() {
        //HttpServletRequest request = getThreadLocalRequest();
        HttpSession session = request.getSession();
        return (Long) session.getAttribute(WebConstants.USER_COMMUNITY_KEY);
    }
}
