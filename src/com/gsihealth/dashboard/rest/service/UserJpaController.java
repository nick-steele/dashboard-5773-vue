/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.rest.service;

import com.gsihealth.dashboard.entity.dto.OrgCareteamUserDTO;
import com.gsihealth.dashboard.server.dao.util.JPAConstants;
import com.gsihealth.entity.User;
import com.gsihealth.entity.UserCommunityCareteam;
import com.gsihealth.entity.UserCommunityOrganization;
import com.gsihealth.dashboard.common.util.CharacterUtils;
import com.gsihealth.entity.UserPatient;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.transaction.UserTransaction;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author vlewis
 */
public class UserJpaController implements Serializable {

    public UserJpaController( EntityManagerFactory emf) {

        this.emf = emf;
    }

    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(User user) throws Exception {
         EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();
        try {
            utx.begin();

            em.persist(user);

            utx.commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new Exception("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(User user) throws Exception {
        EntityManager em = getEntityManager();
        EntityTransaction utx = em.getTransaction();
        try {
            //   utx.begin();
            utx.begin();
            user = em.merge(user);
            utx.commit();


        } catch (Exception ex) {
            ex.printStackTrace();
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new Exception("An error occurred attempting to roll back the transaction.", re);
            }

            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws Exception {
        throw new Exception("Not implemented");
    }

    public List<User> findUserEntities() {
        return findUserEntities(true, -1, -1);
    }

    public List<User> findUserEntities(int maxResults, int firstResult) {
        return findUserEntities(false, maxResults, firstResult);
    }

    private List<User> findUserEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select object(o) from User as o");
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public User findUser(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(User.class, id);
        } finally {
            em.close();
        }
    }

    public int getUserCount() {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createQuery("select count(o) from User as o");
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    @SuppressWarnings("unchecked")
    public List<User> findUserBySupervisor(long supervisorId, long communityId) {
        EntityManager em = getEntityManager();
        try {
            Query q = em.createNamedQuery("User.findBySupervisor");
            q.setParameter("supervisorId", supervisorId);
            q.setParameter("communityId", communityId);
            return q.getResultList();
        } finally {
            em.close();
        }
    }



    public List<OrgCareteamUserDTO> findUserInSameOrganizationOrCareteam(long communityId, long userId, long patientId, String name)
    {
        name = CharacterUtils.escapeMetaCharacters(name);

        List<OrgCareteamUserDTO> users = new ArrayList<>();
        EntityManager em = getEntityManager();
        try {
            StringBuilder jpqlBuilder = new StringBuilder();
            jpqlBuilder.append("SELECT uco from UserCommunityOrganization uco INNER JOIN FETCH uco.organizationCwd ");
            jpqlBuilder.append("WHERE uco.user.userId = :userId AND uco.community.communityId = :communityId");

            Query query = em.createQuery(jpqlBuilder.toString());
            query.setParameter("userId", userId);
            query.setParameter("communityId", communityId);
            List<UserCommunityOrganization> userCommOrgs = query.getResultList();
            if (userCommOrgs == null || userCommOrgs.size() == 0) {
                return users;
            }
            List<Long> userCommOrgIds = new ArrayList<Long>(userCommOrgs.size());
            Iterator<UserCommunityOrganization> iUserCommOrg = userCommOrgs.iterator();
            List<Long> orgIds = new ArrayList<Long>(userCommOrgIds.size());
            while (iUserCommOrg.hasNext()) {
                orgIds.add(iUserCommOrg.next().getOrganizationCwd().getOrgId());
            }

            List<Long> userIdCareteam = null;

            jpqlBuilder = new StringBuilder();
            jpqlBuilder.append("SELECT up FROM UserPatient up ");
            jpqlBuilder.append("WHERE up.communityCareteamId IN ");
            jpqlBuilder.append("(SELECT DISTINCT(up.communityCareteamId) FROM UserPatient up WHERE up.userId = :user_id)");
//                jpqlBuilder.append("WHERE up.patientId = :patientId ");
//                jpqlBuilder.append("AND up.communityId = :communityId ");
//                jpqlBuilder.append("AND up.userType = 'INTERNAL'");

            query = em.createQuery(jpqlBuilder.toString());
            query.setParameter("user_id", userId);

            List<UserPatient> userPatients = query.getResultList();
            if (userPatients != null && userPatients.size() > 0) {
                userIdCareteam =  new ArrayList<Long>(userPatients.size());
                for(UserPatient userPatient : userPatients) {
                    userIdCareteam.add(userPatient.getUserId());
                }
            }


            StringBuilder sqlBuilder = new StringBuilder();
            sqlBuilder.append("SELECT u.user_id as userId, u.first_name as firstName, u.last_name as lastName, ");
            sqlBuilder.append("u.email as email, oc.org_id as orgId, oc.organization_name as organization ");
            sqlBuilder.append("FROM connect.user u ");
            sqlBuilder.append("JOIN connect.user_community_organization uco ");
            sqlBuilder.append("ON u.user_id = uco.user_id ");
            sqlBuilder.append("JOIN connect.organization_cwd oc ");
            sqlBuilder.append("ON oc.org_id = uco.org_id ");
            sqlBuilder.append("WHERE uco.community_id = ?1 ");
            if (orgIds != null && orgIds.size() > 0) {
                sqlBuilder.append("AND uco.org_id in ( ");
                for(int i = 0; i < orgIds.size(); i++) {
                    sqlBuilder.append(orgIds.get(i).toString());
                    if(i < orgIds.size() - 1) {
                        sqlBuilder.append(",");
                    }
                }
                sqlBuilder.append(") ");
            }
            sqlBuilder.append("AND (uco.END_DATE IS NULL OR uco.END_DATE > NOW()) AND (uco.START_DATE < NOW()) ");
            sqlBuilder.append("AND ( lower(concat(u.last_name, ' ', u.first_name)) regexp ?2 ");
            sqlBuilder.append("OR lower(concat(u.first_name, ' ', u.last_name)) regexp ?2 )");
            sqlBuilder.append("AND u.deleted = 'N' ");
            if (userIdCareteam == null || userIdCareteam.size() == 0) {
                sqlBuilder.append("limit 30");
            }
            else {
                sqlBuilder.append("limit 15 ");
                sqlBuilder.append("UNION ");
                sqlBuilder.append("SELECT u.user_id as userId, u.first_name as firstName, u.last_name as lastName, ");
                sqlBuilder.append("u.email as email, oc.org_id as orgId, oc.organization_name as organization ");
                sqlBuilder.append("FROM connect.user u ");
                sqlBuilder.append("JOIN connect.user_community_organization uco ");
                sqlBuilder.append("ON u.user_id = uco.user_id ");
                sqlBuilder.append("JOIN connect.organization_cwd oc ");
                sqlBuilder.append("ON oc.org_id = uco.org_id ");
                sqlBuilder.append("WHERE uco.community_id = ?3 ");
                sqlBuilder.append("AND u.user_id in ( ");
                for (int i = 0; i < userIdCareteam.size(); i++) {
                    sqlBuilder.append(userIdCareteam.get(i).toString());
                    if (i < userIdCareteam.size() - 1) {
                        sqlBuilder.append(",");
                    }
                }
                sqlBuilder.append(") ");
                sqlBuilder.append("AND ( lower(concat(u.last_name, ' ', u.first_name)) regexp ?4 ");
                sqlBuilder.append("OR lower(concat(u.first_name, ' ', u.last_name)) regexp ?4 ) ");
                sqlBuilder.append("AND u.deleted = 'N' ");
                sqlBuilder.append("limit 15");
            }
            StringBuilder nameLikeBuilder = new StringBuilder();
            String[] splitted;
            if (name.trim().isEmpty()) {
                nameLikeBuilder.append("^.");
            }
            else {
                splitted = name.trim().split("[,\\s]");
                nameLikeBuilder.append(".*");
                for(int i = 0; i < splitted.length; i++) {
                    if(splitted[i].trim().isEmpty()) {
                        continue;
                    }
                    nameLikeBuilder.append(splitted[i]);
                    if (i < splitted.length - 1) {
                        nameLikeBuilder.append("[ ]+");
                    }
                    else {
                        nameLikeBuilder.append("[ ]*");
                    }
                }
            }

            String nameLike = nameLikeBuilder.toString();
            query = em.createNativeQuery(sqlBuilder.toString())
                    .setParameter(1, communityId)
                    .setParameter(2, nameLike);
            if (userIdCareteam != null && userIdCareteam.size() > 0) {
                query.setParameter(3, communityId)
                        .setParameter(4, nameLike);
            }
            //query.setHint(JPAConstants.REFRESH, JPAConstants.TRUE);

            List<Object[]> resultList = query.getResultList();
            for (int i = 0; i< resultList.size(); i++) {
                Object[] values = (Object[]) resultList.get(i);
                OrgCareteamUserDTO orgCareteamUserDTO = new OrgCareteamUserDTO();
                orgCareteamUserDTO.setUserId((Long)values[0]);
                orgCareteamUserDTO.setFirstName(values[1].toString());
                orgCareteamUserDTO.setLastName(values[2].toString());
                orgCareteamUserDTO.setEmail(values[3].toString());
                orgCareteamUserDTO.setOrgId((Long)values[4]);
                orgCareteamUserDTO.setOrganization(values[5].toString());
                users.add(orgCareteamUserDTO);
            }

        } finally {
            em.close();
        }
        return users;
    }


}
