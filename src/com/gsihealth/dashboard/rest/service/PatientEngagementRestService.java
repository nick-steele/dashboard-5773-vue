/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.rest.service;

import com.gsihealth.dashboard.client.service.CareteamService;
import com.gsihealth.dashboard.server.WebConstants;
import com.gsihealth.dashboard.server.service.BasePatientService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebMethod;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

/**
 *
 * @author akumar
 */
@Path("/patientengagement")
public class PatientEngagementRestService
        extends BasePatientService {
    @Context
    private ServletContext context;
    private Logger logger = Logger.getLogger(getClass().
            getName());

    /**
     * Initializes the service with DAOs
     */
    public PatientEngagementRestService() {

    }

    @GET
    @Path("directmessaging/{comId}/{patientId}/{isDirectMessagingRequired}")
    @Produces({"application/json"})
    @WebMethod
    public String performDirectMessaging(@PathParam("patientId") long patientId,
            @PathParam("comId") long communityId,
            @PathParam("isDirectMessagingRequired") boolean isDirectMessagingRequired) {
        String ret = null;

        try {
            CareteamService careteamService = getCareteamService();

            ret = careteamService.
                    performPatientEngagement(communityId, patientId,
                            isDirectMessagingRequired);

        } catch (Exception exc) {
            String message = "Error calling: careteamService";
            logger.log(Level.SEVERE, message, exc);
            throw new RuntimeException(message);
        }
        return ret;
    }

    protected CareteamService getCareteamService() {

        CareteamService careteamService = (CareteamService) context.
                getAttribute(WebConstants.CARETEAM_SERVICE_KEY);

        return careteamService;
    }
}
