/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.common.SearchResults;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.server.dao.PatientEnrollmentDAO;
import com.gsihealth.dashboard.server.patientmanagement.PatientManager;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Beth.Boose
 */
public class EnrollmentServiceImplTest {
    
    public EnrollmentServiceImplTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of init method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testInit() throws Exception {
        System.out.println("init");
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.init();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of findPatients method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testFindPatients() throws Exception {
        System.out.println("findPatients");
        long communityId = 0L;
        SearchCriteria searchCriteria = null;
        boolean enrolledMode = false;
        Long currentUserOrgId = null;
        int pageNumber = 0;
        int pageSize = 0;
        long userId = 0L;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        SearchResults<PersonDTO> expResult = null;
        SearchResults<PersonDTO> result = instance.findPatients(communityId, searchCriteria, enrolledMode, currentUserOrgId, pageNumber, pageSize, userId);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of countPatients method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testCountPatients() throws Exception {
        System.out.println("countPatients");
        long communityId = 0L;
        SearchCriteria searchCriteria = null;
        boolean enrolledMode = false;
        Long currentUserOrgId = null;
        long userId = 0L;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        int expResult = 0;
        int result = instance.countPatients(communityId, searchCriteria, enrolledMode, currentUserOrgId, userId);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setupPixClient method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testSetupPixClient() {
        System.out.println("setupPixClient");
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.setupPixClient();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setupComparator method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testSetupComparator() {
        System.out.println("setupComparator");
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.setupComparator();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of filterPatientsWithDemographics method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testFilterPatientsWithDemographics_4args() throws Exception {
        System.out.println("filterPatientsWithDemographics");
        long communityId = 0L;
        SearchCriteria searchCriteria = null;
        List<String> statuses = null;
        long currentUserOrgId = 0L;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        List<PersonDTO> expResult = null;
        List<PersonDTO> result = instance.filterPatientsWithDemographics(communityId, searchCriteria, statuses, currentUserOrgId);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of filterPatientsWithDemographics method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testFilterPatientsWithDemographics_3args() throws Exception {
        System.out.println("filterPatientsWithDemographics");
        long communityId = 0L;
        SearchCriteria searchCriteria = null;
        List<String> statuses = null;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        List<PersonDTO> expResult = null;
        List<PersonDTO> result = instance.filterPatientsWithDemographics(communityId, searchCriteria, statuses);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLoginResult method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testGetLoginResult() {
        System.out.println("getLoginResult");
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        LoginResult expResult = null;
        LoginResult result = instance.getLoginResult();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of runUpdatePatientProcessesForCareteamAssignment method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testRunUpdatePatientProcessesForCareteamAssignment() {
        System.out.println("runUpdatePatientProcessesForCareteamAssignment");
        LoginResult loginResult = null;
        Person thePerson = null;
        PersonDTO personDTO = null;
        String oldEnrollmentStatus = "";
        String newCareTeamId = "";
        String oldCareTeamId = "";
        String oldProgramLevelConsentStatus = "";
        String programLevelConsentStatus = "";
        Person oldPerson = null;
        long oldOrganizationId = 0L;
        PatientInfo patientInfo = null;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.runUpdatePatientProcessesForCareteamAssignment(loginResult, thePerson, personDTO, oldEnrollmentStatus, newCareTeamId, oldCareTeamId, oldProgramLevelConsentStatus, programLevelConsentStatus, oldPerson, oldOrganizationId, patientInfo);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of runUpdatePatientProcessesAsync method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testRunUpdatePatientProcessesAsync() {
        System.out.println("runUpdatePatientProcessesAsync");
        LoginResult loginResult = null;
        Person thePerson = null;
        PersonDTO personDTO = null;
        String oldEnrollmentStatus = "";
        String newCareTeamId = "";
        String oldCareTeamId = "";
        String oldProgramLevelConsentStatus = "";
        String programLevelConsentStatus = "";
        Person oldPerson = null;
        long oldOrganizationId = 0L;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.runUpdatePatientProcessesAsync(loginResult, thePerson, personDTO, oldEnrollmentStatus, newCareTeamId, oldCareTeamId, oldProgramLevelConsentStatus, programLevelConsentStatus, oldPerson, oldOrganizationId);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of runUpdatePatientProcessesSynchronous method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testRunUpdatePatientProcessesSynchronous() {
        System.out.println("runUpdatePatientProcessesSynchronous");
        LoginResult loginResult = null;
        Person thePerson = null;
        PersonDTO personDTO = null;
        String oldEnrollmentStatus = "";
        String newCareTeamId = "";
        String oldCareTeamId = "";
        String oldProgramLevelConsentStatus = "";
        String programLevelConsentStatus = "";
        Person oldPerson = null;
        long oldOrganizationId = 0L;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.runUpdatePatientProcessesSynchronous(loginResult, thePerson, personDTO, oldEnrollmentStatus, newCareTeamId, oldCareTeamId, oldProgramLevelConsentStatus, programLevelConsentStatus, oldPerson, oldOrganizationId);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of sendUpdateNotifications method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testSendUpdateNotifications() {
        System.out.println("sendUpdateNotifications");
        LoginResult loginResult = null;
        Person thePerson = null;
        PersonDTO personDTO = null;
        String oldEnrollmentStatus = "";
        String newCareTeamId = "";
        String oldCareTeamId = "";
        String oldProgramLevelConsentStatus = "";
        String programLevelConsentStatus = "";
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.sendUpdateNotifications(loginResult, thePerson, personDTO, oldEnrollmentStatus, newCareTeamId, oldCareTeamId, oldProgramLevelConsentStatus, programLevelConsentStatus);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addPatient method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testAddPatient_PersonDTO() throws Exception {
        System.out.println("addPatient");
        PersonDTO personDTO = null;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.addPatient(personDTO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addPatient method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testAddPatient_PersonDTO_long() throws Exception {
        System.out.println("addPatient");
        PersonDTO personDTO = null;
        long communityCareteamId = 0L;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.addPatient(personDTO, communityCareteamId);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of updatePatientFromPatientLoader method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testUpdatePatientFromPatientLoader() throws Exception {
        System.out.println("updatePatientFromPatientLoader");
        LoginResult thePatientLoaderLoginResult = null;
        PersonDTO personDTO = null;
        boolean checkEnableCareteam = false;
        boolean checkForDuplicate = false;
        boolean checkForDuplicateSsn = false;
        boolean primaryMedicaidcareIdBln = false;
        boolean secondaryMedicaidcareIdBln = false;
        String oldProgramLevelConsentStatus = "";
        String oldEnrollmentStatus = "";
        String newCareTeamId = "";
        String oldCareTeamId = "";
        long oldOrganizationId = 0L;
        boolean checkPatientUserUpdate = false;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.updatePatientFromPatientLoader(thePatientLoaderLoginResult, personDTO, checkEnableCareteam, checkForDuplicate, checkForDuplicateSsn, primaryMedicaidcareIdBln, secondaryMedicaidcareIdBln, oldProgramLevelConsentStatus, oldEnrollmentStatus, newCareTeamId, oldCareTeamId, oldOrganizationId, checkPatientUserUpdate);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of updatePatient method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testUpdatePatient_12args() throws Exception {
        System.out.println("updatePatient");
        PersonDTO personDTO = null;
        boolean checkEnableCareteam = false;
        boolean checkForDuplicate = false;
        boolean checkForDuplicateSsn = false;
        boolean primaryMedicaidcareIdBln = false;
        boolean secondaryMedicaidcareIdBln = false;
        String oldProgramLevelConsentStatus = "";
        String oldEnrollmentStatus = "";
        String newCareTeamId = "";
        String oldCareTeamId = "";
        long oldOrganizationId = 0L;
        boolean checkPatientUserUpdate = false;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
//        instance.updatePatient(personDTO, checkEnableCareteam, checkForDuplicate, checkForDuplicateSsn, 
//                primaryMedicaidcareIdBln, secondaryMedicaidcareIdBln, oldProgramLevelConsentStatus, 
//                oldEnrollmentStatus, newCareTeamId, oldCareTeamId, oldOrganizationId, checkPatientUserUpdate);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of updatePatient method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testUpdatePatient_14args() throws Exception {
        System.out.println("updatePatient");
        LoginResult thePatientLoaderLoginResult = null;
        PersonDTO personDTO = null;
        boolean checkEnableCareteam = false;
        boolean checkForDuplicate = false;
        boolean checkForDuplicateSsn = false;
        boolean primaryMedicaidcareIdBln = false;
        boolean secondaryMedicaidcareIdBln = false;
        String oldProgramLevelConsentStatus = "";
        String oldEnrollmentStatus = "";
        String newCareTeamId = "";
        String oldCareTeamId = "";
        long oldOrganizationId = 0L;
        PatientInfo patientInfo = null;
        boolean checkPatientUserUpdate = false;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.updatePatient(thePatientLoaderLoginResult, personDTO, checkEnableCareteam, checkForDuplicate, checkForDuplicateSsn, primaryMedicaidcareIdBln, secondaryMedicaidcareIdBln, oldProgramLevelConsentStatus, oldEnrollmentStatus, newCareTeamId, oldCareTeamId, oldOrganizationId, patientInfo, checkPatientUserUpdate);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of updatePatientCareteam method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testUpdatePatientCareteam() throws Exception {
        System.out.println("updatePatientCareteam");
        long communityCareteamId = 0L;
        long patientId = 0L;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.updatePatientCareteam(communityCareteamId, patientId);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of addPatientCareteam method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testAddPatientCareteam() throws Exception {
        System.out.println("addPatientCareteam");
        long communityCareteamId = 0L;
        long patientId = 0L;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.addPatientCareteam(communityCareteamId, patientId);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of removePatientCareteam method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testRemovePatientCareteam() throws Exception {
        System.out.println("removePatientCareteam");
        long patientId = 0L;
        long communityId = 0L;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.removePatientCareteam(patientId, communityId);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setPatientEnrollmentDAO method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testSetPatientEnrollmentDAO() {
        System.out.println("setPatientEnrollmentDAO");
        PatientEnrollmentDAO patientEnrollmentDAO = null;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.setPatientEnrollmentDAO(patientEnrollmentDAO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setPatientManager method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testSetPatientManager() {
        System.out.println("setPatientManager");
        PatientManager patientManager = null;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.setPatientManager(patientManager);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of updatePatientForCareteamAssignment method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testUpdatePatientForCareteamAssignment() throws Exception {
        System.out.println("updatePatientForCareteamAssignment");
        PersonDTO thePerson = null;
        boolean checkEnableCareteam = false;
        boolean checkForDuplicate = false;
        boolean checkForDuplicateSsn = false;
        boolean primaryMedicaidcareId = false;
        boolean secondaryMedicaidcareId = false;
        String oldProgramLevelConsentStatus = "";
        String oldEnrollmentStatus = "";
        String careTeamIdNew = "";
        String careTeamIdOld = "";
        long oldOrganizationId = 0L;
        PatientInfo patientInfo = null;
        boolean checkPatientUserUpdate = false;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.updatePatientForCareteamAssignment(thePerson, checkEnableCareteam, checkForDuplicate, checkForDuplicateSsn, primaryMedicaidcareId, secondaryMedicaidcareId, oldProgramLevelConsentStatus, oldEnrollmentStatus, careTeamIdNew, careTeamIdOld, oldOrganizationId, patientInfo, checkPatientUserUpdate);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of checkIfDuplicatePatient method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testCheckIfDuplicatePatient() throws Exception {
        System.out.println("checkIfDuplicatePatient");
        PersonDTO personDTO = null;
        long communityId = 0L;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.checkIfDuplicatePatient(personDTO, communityId);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of checkIfDuplicatePatientModify method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testCheckIfDuplicatePatientModify() throws Exception {
        System.out.println("checkIfDuplicatePatientModify");
        PersonDTO personDTO = null;
        boolean checkForDuplicateBln = false;
        long communityId = 0L;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.checkIfDuplicatePatientModify(personDTO, checkForDuplicateBln, communityId);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getTotalCountForZero method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testGetTotalCountForZero() throws Exception {
        System.out.println("getTotalCountForZero");
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        int expResult = 0;
        int result = instance.getTotalCountForZero();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of removeProviderAssignedToPatient method, of class EnrollmentServiceImpl.
     */
    @Test
    public void testRemoveProviderAssignedToPatient() throws Exception {
        System.out.println("removeProviderAssignedToPatient");
        long patientId = 0L;
        long communityId = 0L;
        EnrollmentServiceImpl instance = new EnrollmentServiceImpl();
        instance.removeProviderAssignedToPatient(patientId, communityId);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
