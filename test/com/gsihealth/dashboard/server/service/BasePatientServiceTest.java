/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.service;

import com.gsihealth.dashboard.server.dao.AuditDAO;
import com.gsihealth.dashboard.server.dao.CommunityCareteamDAO;
import com.gsihealth.dashboard.server.dao.NotificationAuditDAO;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Beth.Boose
 */
public class BasePatientServiceTest {
    
    public BasePatientServiceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of init method, of class BasePatientService.
     */
    @Test
    public void testInit() throws Exception {
        System.out.println("init");
        BasePatientService instance = new BasePatientService();
        instance.init();
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setAuditDAO method, of class BasePatientService.
     */
    @Test
    public void testSetAuditDAO() {
        System.out.println("setAuditDAO");
        AuditDAO auditDAO = null;
        BasePatientService instance = new BasePatientService();
        instance.setAuditDAO(auditDAO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setCareteamDAO method, of class BasePatientService.
     */
    @Test
    public void testSetCareteamDAO() {
        System.out.println("setCareteamDAO");
        CommunityCareteamDAO careteamDAO = null;
        BasePatientService instance = new BasePatientService();
        instance.setCareteamDAO(careteamDAO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setNotificationAuditDAO method, of class BasePatientService.
     */
    @Test
    public void testSetNotificationAuditDAO() {
        System.out.println("setNotificationAuditDAO");
        NotificationAuditDAO notificationAuditDAO = null;
        BasePatientService instance = new BasePatientService();
        instance.setNotificationAuditDAO(notificationAuditDAO);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
