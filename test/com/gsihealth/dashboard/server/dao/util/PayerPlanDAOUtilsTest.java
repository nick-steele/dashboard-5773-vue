package com.gsihealth.dashboard.server.dao.util;

import java.util.LinkedHashMap;
import com.gsihealth.dashboard.entity.insurance.PayerPlan;
import com.gsihealth.dashboard.client.util.StringUtils;
import com.gsihealth.dashboard.common.PayerPlanSearchCriteria;
import com.gsihealth.dashboard.entity.insurance.PayerPlanPK;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class PayerPlanDAOUtilsTest {

    public PayerPlanDAOUtilsTest() {
    }

    @Test
    public void buildWhereClause_noSearchCriteria() {
        // setup
        PayerPlanSearchCriteria searchCriteria = new PayerPlanSearchCriteria();

        // execute
        String actual = PayerPlanDAOUtils.buildWhereClause(searchCriteria);

        // assert
        assertTrue(StringUtils.isBlank(actual));
    }

    @Test
    public void buildWhereClause_onlyPayerPlanId() {
        // setup
        String theId = "9999";

        PayerPlanSearchCriteria searchCriteria = new PayerPlanSearchCriteria();
        searchCriteria.setPayerPlanId(theId);

        // execute
        String actual = PayerPlanDAOUtils.buildWhereClause(searchCriteria);

        // assert
        String expected = " where o.id=" + theId;
        assertEquals(expected, actual);
    }

    @Test
    public void buildWhereClause_onlyMmisId() {
        // setup
        String theMmisId = "12345";

        PayerPlanSearchCriteria searchCriteria = new PayerPlanSearchCriteria();
        searchCriteria.setMmisid(theMmisId);

        // execute
        String actual = PayerPlanDAOUtils.buildWhereClause(searchCriteria);

        // assert
        String expected = " where o.mmisId='" + theMmisId + "'";
        assertEquals(expected, actual);
    }

    @Test
    public void buildWhereClause_bothSearchPayerPlanIdAndMmisId() {
        // setup
        String theMmisId = "12345";
        String theId = "9999";

        PayerPlanSearchCriteria searchCriteria = new PayerPlanSearchCriteria();
        searchCriteria.setMmisid(theMmisId);
        searchCriteria.setPayerPlanId(theId);

        // execute
        String actual = PayerPlanDAOUtils.buildWhereClause(searchCriteria);

        // assert
        String expected = " where o.id=" + theId + " and o.mmisId='" + theMmisId + "'";
        assertEquals(expected, actual);
    }

    @Test
    public void buildWhereClauseCheckForDuplicates_bothNameAndMmisId_newObject() {
        // setup
        String name = "Cigna";
        String theMmisId = "12345";

        PayerPlan payerPlan = new PayerPlan();
        payerPlan.setMmisId(theMmisId);
        payerPlan.setName(name);

        // execute
        String actual = PayerPlanDAOUtils.buildWhereClauseCheckForDuplicates(payerPlan);

        // assert
        String expected = " where (o.name=" + DaoUtils.quote(name) + " or o.mmisId=" + DaoUtils.quote(theMmisId) + ")";

        assertEquals(expected, actual);
    }

    @Test
    public void buildWhereClauseCheckForDuplicates_nameOnly_newObject() {
        // setup
        String name = "Cigna";

        PayerPlan payerPlan = new PayerPlan();
        payerPlan.setName(name);

        // execute
        String actual = PayerPlanDAOUtils.buildWhereClauseCheckForDuplicates(payerPlan);

        // assert
        String expected = " where (o.name=" + DaoUtils.quote(name) + ")";

        assertEquals(expected, actual);
    }

    @Test
    public void buildWhereClauseCheckForDuplicates_mmisIdOnly_newObject() {
        // setup
        String mmisId = "12345";

        PayerPlan payerPlan = new PayerPlan();
        payerPlan.setMmisId(mmisId);

        // execute
        String actual = PayerPlanDAOUtils.buildWhereClauseCheckForDuplicates(payerPlan);

        // assert
        String expected = " where (o.mmisId=" + DaoUtils.quote(mmisId) + ")";

        assertEquals(expected, actual);
    }

    @Test
    public void buildWhereClauseCheckForDuplicates_bothNameAndMmisId_existingObject() {
        // setup
        String name = "Cigna";
        String theMmisId = "12345";
        Long id = 9L;

        PayerPlan payerPlan = new PayerPlan();
        payerPlan.setMmisId(theMmisId);
        payerPlan.setName(name);
        payerPlan.setPayerPlanPK(new PayerPlanPK(id, 1L));

        // execute
        String actual = PayerPlanDAOUtils.buildWhereClauseCheckForDuplicates(payerPlan);

        // assert
        String expected = " where (o.name=" + DaoUtils.quote(name) + " or o.mmisId=" + DaoUtils.quote(theMmisId)  + ")" + " and o.id<>" + id;

        assertEquals(expected, actual);
    }

    @Test
    public void buildWhereClauseCheckForDuplicates_nameOnly_existingObject() {
        // setup
        String name = "Cigna";
        Long id = 9L;

        PayerPlan payerPlan = new PayerPlan();
        payerPlan.setName(name);
        payerPlan.setPayerPlanPK(new PayerPlanPK(id, 1L));

        // execute
        String actual = PayerPlanDAOUtils.buildWhereClauseCheckForDuplicates(payerPlan);

        // assert
        String expected = " where (o.name=" + DaoUtils.quote(name)  + ")" + " and o.id<>" + id;

        assertEquals(expected, actual);
    }

    @Test
    public void buildWhereClauseCheckForDuplicates_mmisIdOnly_existingObject() {
        // setup
        String mmisId = "12345";
        Long id = 9L;

        PayerPlan payerPlan = new PayerPlan();
        payerPlan.setMmisId(mmisId);
        payerPlan.setPayerPlanPK(new PayerPlanPK(id, 1L));

        // execute
        String actual = PayerPlanDAOUtils.buildWhereClauseCheckForDuplicates(payerPlan);

        // assert
        String expected = " where (o.mmisId=" + DaoUtils.quote(mmisId)  + ")" + " and o.id<>" + id;

        assertEquals(expected, actual);
    }

    @Test
    public void movePayerPlanToEnd_beginStartOfList() {

        String expected = "Other";

        // setup
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
        data.put("211", expected);
        data.put("a", "aaa");
        data.put("g", "ggg");
        data.put("z", "zzz");

        // execute
        PayerPlanDAOUtils.movePayerPlanToEnd(data, "Other");

        // assert
        String last = getLastValue(data);

        assertEquals(expected, last);
    }

    protected String getLastValue(LinkedHashMap<String, String> data) {
        return data.values().toArray(new String[0])[data.size()-1];
    }

    @Test
    public void movePayerPlanToEnd_beginMiddleOfList() {
        String expected = "Other";

        // setup
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
        data.put("a", "aaa");
        data.put("211", expected);
        data.put("g", "ggg");
        data.put("z", "zzz");

        // execute
        PayerPlanDAOUtils.movePayerPlanToEnd(data, "Other");

        // assert
        String last = getLastValue(data);

        assertEquals(expected, last);
    }

    @Test
    public void movePayerPlanToEnd_beginEndOfList() {
        String expected = "Other";

        // setup
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
        data.put("a", "aaa");
        data.put("g", "ggg");
        data.put("z", "zzz");
        data.put("211", expected);

        // execute
        PayerPlanDAOUtils.movePayerPlanToEnd(data, "Other");

        // assert
        String last = getLastValue(data);

        assertEquals(expected, last);
    }

    @Test
    public void movePayerPlanToEnd_otherDoesNotExist() {
        String expected = "zzz";

        // setup
        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
        data.put("a", "aaa");
        data.put("g", "ggg");
        data.put("z", expected);

        // execute
        PayerPlanDAOUtils.movePayerPlanToEnd(data, "Other");

        // assert
        String last = getLastValue(data);

        assertEquals(expected, last);
    }
}
