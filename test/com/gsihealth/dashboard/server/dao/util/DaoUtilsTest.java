package com.gsihealth.dashboard.server.dao.util;

import java.util.Arrays;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class DaoUtilsTest {
    
    public DaoUtilsTest() {
    }

    @Test
    public void buildInClause_OneElement() {
        // setup
        Long[] data = {1l};
        
        // execute
        String actual = DaoUtils.buildInClause(Arrays.asList(data));
        
        // assert
        String expected = "(1)";
        
        assertEquals(expected, actual);
    }

    @Test
    public void buildInClause_TwoElements() {
        // setup
        Long[] data = {1l, 2l};
        
        // execute
        String actual = DaoUtils.buildInClause(Arrays.asList(data));
        
        // assert
        String expected = "(1, 2)";
        
        assertEquals(expected, actual);
    }

    @Test
    public void buildInClause_ThreeElements() {
        // setup
        Long[] data = {1l, 2l, 3l};
        
        // execute
        String actual = DaoUtils.buildInClause(Arrays.asList(data));
        
        // assert
        String expected = "(1, 2, 3)";
        
        assertEquals(expected, actual);
    }

    @Test
    public void buildInClause_Quoted_OneElement() {
        // setup
        String[] data = {"alpha"};
        
        // execute
        String actual = DaoUtils.buildInClauseQuoted(Arrays.asList(data));
        
        // assert
        String expected = "('alpha')";
        
        assertEquals(expected, actual);
    }
    
    @Test
    public void buildInClause_Quoted_ThreeElements() {
        // setup
        String[] data = {"alpha", "bravo", "charlie"};
        
        // execute
        boolean quoted = true;
        String actual = DaoUtils.buildInClauseQuoted(Arrays.asList(data));
        
        // assert
        String expected = "('alpha', 'bravo', 'charlie')";
        
        assertEquals(expected, actual);
    }
    
}
