/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.common.Constants;
import com.gsihealth.dashboard.entity.PatientEnrollment;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class ConsentUtilsTest {

    public ConsentUtilsTest() {
    }

    @Test
    public void computeSendConsentFlagForBHIX_NoChange_to_Consent_Flag_Yes_Yes() {

        // setup
        String oldProgramLevelConsentStatus = Constants.YES;
        String newProgramLevelConsentStatus = Constants.YES;
        
        // execute
        boolean actual = ConsentUtils.hasConsentChangedForBHIX(oldProgramLevelConsentStatus, newProgramLevelConsentStatus);
        
        // assert
        boolean expected = false;
        assertEquals(expected, actual);                
    }

    @Test
    public void computeSendConsentFlagForBHIX_NoChange_to_Consent_Flag_No_No() {

        // setup
        String oldProgramLevelConsentStatus = Constants.NO;
        String newProgramLevelConsentStatus = Constants.NO;
        
        // execute
        boolean actual = ConsentUtils.hasConsentChangedForBHIX(oldProgramLevelConsentStatus, newProgramLevelConsentStatus);
        
        // assert
        boolean expected = false;
        assertEquals(expected, actual);                
    }
    
    @Test
    public void computeSendConsentFlagForBHIX_Change_to_Consent_Flag_Null_to_Yes() {

        // setup
        String oldProgramLevelConsentStatus = null;
        String newProgramLevelConsentStatus = Constants.YES;
        
        // execute
        boolean actual = ConsentUtils.hasConsentChangedForBHIX(oldProgramLevelConsentStatus, newProgramLevelConsentStatus);
        
        // assert
        boolean expected = true;
        assertEquals(expected, actual);                
    }

    @Test
    public void computeSendConsentFlagForBHIX_Change_to_Consent_Flag_Null_to_No() {

        // setup
        String oldProgramLevelConsentStatus = null;
        String newProgramLevelConsentStatus = Constants.NO;
        
        // execute
        boolean actual = ConsentUtils.hasConsentChangedForBHIX(oldProgramLevelConsentStatus, newProgramLevelConsentStatus);
        
        // assert
        boolean expected = false;
        assertEquals(expected, actual);                
    }
    
    @Test
    public void computeSendConsentFlagForBHIX_Change_to_Consent_Flag_No_to_Unknown() {

        // setup
        String oldProgramLevelConsentStatus = Constants.NO;
        String newProgramLevelConsentStatus = Constants.UNKNOWN;
        
        // execute
        boolean actual = ConsentUtils.hasConsentChangedForBHIX(oldProgramLevelConsentStatus, newProgramLevelConsentStatus);
        
        // assert
        boolean expected = false;
        assertEquals(expected, actual);                
    }

    @Test
    public void computeSendConsentFlagForBHIX_Change_to_Consent_Flag_No_to_Null() {

        // setup
        String oldProgramLevelConsentStatus = Constants.NO;
        String newProgramLevelConsentStatus = null;
        
        // execute
        boolean actual = ConsentUtils.hasConsentChangedForBHIX(oldProgramLevelConsentStatus, newProgramLevelConsentStatus);
        
        // assert
        boolean expected = false;
        assertEquals(expected, actual);                
    }

    @Test
    public void computeSendConsentFlagForBHIX_Change_to_Consent_Flag_Null_to_Unknown() {

        // setup
        String oldProgramLevelConsentStatus = null;
        String newProgramLevelConsentStatus = Constants.UNKNOWN;
        
        // execute
        boolean actual = ConsentUtils.hasConsentChangedForBHIX(oldProgramLevelConsentStatus, newProgramLevelConsentStatus);
        
        // assert
        boolean expected = false;
        assertEquals(expected, actual);                
    }

    @Test
    public void computeSendConsentFlagForBHIX_Change_to_Consent_Flag_Unknown_to_Null() {

        // setup
        String oldProgramLevelConsentStatus = Constants.UNKNOWN;
        String newProgramLevelConsentStatus = null;
        
        // execute
        boolean actual = ConsentUtils.hasConsentChangedForBHIX(oldProgramLevelConsentStatus, newProgramLevelConsentStatus);
        
        // assert
        boolean expected = false;
        assertEquals(expected, actual);                
    }
    
    @Test
    public void computeSendConsentFlagForBHIX_Change_to_Consent_Flag_Unknown_to_No() {

        // setup
        String oldProgramLevelConsentStatus = Constants.UNKNOWN;
        String newProgramLevelConsentStatus = Constants.NO;
        
        // execute
        boolean actual = ConsentUtils.hasConsentChangedForBHIX(oldProgramLevelConsentStatus, newProgramLevelConsentStatus);
        
        // assert
        boolean expected = false;
        assertEquals(expected, actual);                
    }
    
}
