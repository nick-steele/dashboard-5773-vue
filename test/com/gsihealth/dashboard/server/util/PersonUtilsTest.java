/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.server.common.Person;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class PersonUtilsTest {

    public PersonUtilsTest() {
    }


    @Test
    public void testPersonEquals_AllEquals() throws Exception {
        Person sourcePerson = createPerson("John", "Smith", "M", "07/07/1973");
        Person targetPerson = createPerson("John", "Smith", "M", "07/07/1973");

        boolean expected = true;
        boolean actual = PersonUtils.equalsIgnoreCase(sourcePerson, targetPerson);

        assertEquals(expected, actual);
    }

    @Test
    public void testPersonEquals_DiffFirstName() throws Exception {
        Person sourcePerson = createPerson("Bill", "Smith", "M", "07/07/1973");
        Person targetPerson = createPerson("John", "Smith", "M", "07/07/1973");

        boolean expected = false;
        boolean actual = PersonUtils.equalsIgnoreCase(sourcePerson, targetPerson);

        assertEquals(expected, actual);
    }

    @Test
    public void testPersonEquals_DiffLastName() throws Exception {
        Person sourcePerson = createPerson("John", "Smith", "M", "07/07/1973");
        Person targetPerson = createPerson("John", "Doe", "M", "07/07/1973");

        boolean expected = false;
        boolean actual = PersonUtils.equalsIgnoreCase(sourcePerson, targetPerson);

        assertEquals(expected, actual);
    }

    @Test
    public void testPersonEquals_DiffGender() throws Exception {
        Person sourcePerson = createPerson("John", "Smith", "M", "07/07/1973");
        Person targetPerson = createPerson("John", "Smith", "F", "07/07/1973");

        boolean expected = false;
        boolean actual = PersonUtils.equalsIgnoreCase(sourcePerson, targetPerson);

        assertEquals(expected, actual);
    }

    @Test
    public void testPersonEquals_DiffDob() throws Exception {
        Person sourcePerson = createPerson("John", "Smith", "M", "07/07/1979");
        Person targetPerson = createPerson("John", "Smith", "F", "07/07/1973");

        boolean expected = false;
        boolean actual = PersonUtils.equalsIgnoreCase(sourcePerson, targetPerson);

        assertEquals(expected, actual);
    }

    private Person createPerson(String firstName, String lastName, String gender, String dobStr) throws Exception {
        Person person = new Person();

        person.setFirstName(firstName);
        person.setLastName(lastName);
        person.setGender(gender);
        
        SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
        Date dob = (Date) formatter.parse(dobStr);
        person.setDateOfBirth(dob);
        
        return person;
    }
}
