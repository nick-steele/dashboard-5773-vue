/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.entity.Audit;
import com.gsihealth.dashboard.entity.User;
import com.gsihealth.dashboard.entity.dto.LoginResult;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class AuditUtilsTest {

    public AuditUtilsTest() {
    }

    @Test
    public void convertLoginResultAndUser() {

        // setup
        LoginResult loginResult = new LoginResult();
        loginResult.setAccessLevel("alpha");
        loginResult.setEmail("foo@test.com");
        loginResult.setFirstName("John");
        loginResult.setLastName("Doe");
        loginResult.setOrganizationName("acme inc");
        loginResult.setUserLevel("admin");

        User user = new User();
        user.setFirstName("Paul");
        user.setLastName("Pierce");

        Audit expectedAudit = new Audit();
        expectedAudit.setUserAccessLevel(loginResult.getAccessLevel());
        expectedAudit.setUserEmail(loginResult.getEmail());
        expectedAudit.setUserFirstName(loginResult.getFirstName());
        expectedAudit.setUserLastName(loginResult.getLastName());
        expectedAudit.setUserId(loginResult.getUserId());
        expectedAudit.setUserOrganizationName(loginResult.getOrganizationName());
        expectedAudit.setUserRole(loginResult.getUserLevel());

        expectedAudit.setTargetUserFirstName(user.getFirstName());
        expectedAudit.setTargetUserLastName(user.getLastName());
        
        // execute
        Audit actualAudit = AuditUtils.convertUser(loginResult, user);
        
        // assert
        assertEquals(expectedAudit.getTargetUserFirstName(), actualAudit.getTargetUserFirstName());
        assertEquals(expectedAudit.getTargetUserLastName(), actualAudit.getTargetUserLastName());
        assertEquals(expectedAudit.getTargetUserOrganizationName(), actualAudit.getTargetUserOrganizationName());

        assertEquals(expectedAudit.getUserAccessLevel(), actualAudit.getUserAccessLevel());
        assertEquals(expectedAudit.getUserRole(), actualAudit.getUserRole());
        assertEquals(expectedAudit.getUserFirstName(), actualAudit.getUserFirstName());
        assertEquals(expectedAudit.getUserId(), actualAudit.getUserId());
        assertEquals(expectedAudit.getUserEmail(), actualAudit.getUserEmail());
        assertEquals(expectedAudit.getUserOrganizationName(), actualAudit.getUserOrganizationName());        
    }

}