package com.gsihealth.dashboard.server.util;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class MessageUtilsTest {

    @Test
    public void sanitize_ForMessage_PatientCreation() {
        // setup
        String message = "<ns3:AlertMessageBody>Todd Matthews, Patient ID: 130 has been added to GSIHealthCoordinator.</ns3:AlertMessageBody>";
        String expected = "<ns3:AlertMessageBody>Patient ID: 130 has been added to GSIHealthCoordinator.</ns3:AlertMessageBody>";

        // execute
        String actual = MessageUtils.sanitize(message);

        // assert
        assertEquals(expected, actual);
    }

    @Test
    public void sanitize_ForMessage_DemographicUpdated() {
        // setup
        String message = "<ns3:AlertMessageBody>The patient demographic information has been updated for Todd Matthews, Patient ID: 130</ns3:AlertMessageBody>";
        String expected = "<ns3:AlertMessageBody>The patient demographic information has been updated for Patient ID: 130</ns3:AlertMessageBody>";

        // execute
        String actual = MessageUtils.sanitize(message);

        // assert
        assertEquals(expected, actual);
    }

    @Test
    public void sanitize_ForMessage_EnrollmentStatusUpdated() {
        // setup
        String message = "<ns3:AlertMessageBody>The enrollment status for Patient: Todd Matthews, Patient ID: 130 has been updated to Enrolled</ns3:AlertMessageBody>";
        String expected = "<ns3:AlertMessageBody>The enrollment status for Patient ID: 130 has been updated to Enrolled</ns3:AlertMessageBody>";

        // execute
        String actual = MessageUtils.sanitize(message);

        // assert
        assertEquals(expected, actual);
    }
    
    
    // patient consent changed
    @Test
    public void sanitize_ForMessage_ConsentChanged_Revoked() {
        // setup
        String message = "<ns3:AlertMessageBody>Todd Matthews, Patient ID: 130 has revoked consent</ns3:AlertMessageBody>";
        String expected = "<ns3:AlertMessageBody>Patient ID: 130 has revoked consent</ns3:AlertMessageBody>";

        // execute
        String actual = MessageUtils.sanitize(message);

        // assert
        assertEquals(expected, actual);
    }

    @Test
    public void sanitize_ForMessage_ConsentChanged_Provide() {
        // setup
        String message = "<ns3:AlertMessageBody>Todd Matthews, Patient ID: 130 has provided consent</ns3:AlertMessageBody>";
        String expected = "<ns3:AlertMessageBody>Patient ID: 130 has provided consent</ns3:AlertMessageBody>";

        // execute
        String actual = MessageUtils.sanitize(message);

        // assert
        assertEquals(expected, actual);
    }

    // care team assigned
    @Test
    public void sanitize_ForMessage_Careteam_Assigned() {
        // setup
        String message = "<ns3:AlertMessageBody>Todd Matthews, Patient ID: 130 has been assigned to Brownsville Careteam</ns3:AlertMessageBody>";
        String expected = "<ns3:AlertMessageBody>Patient ID: 130 has been assigned to Brownsville Careteam</ns3:AlertMessageBody>";

        // execute
        String actual = MessageUtils.sanitize(message);

        // assert
        assertEquals(expected, actual);
    }

    @Test
    public void sanitize_ForMessage_Careteam_Removed() {
        // setup
        String message = "<ns3:AlertMessageBody>Todd Matthews, Patient ID: 130 has been removed from Brownsville Careteam</ns3:AlertMessageBody>";
        String expected = "<ns3:AlertMessageBody>Patient ID: 130 has been removed from Brownsville Careteam</ns3:AlertMessageBody>";

        // execute
        String actual = MessageUtils.sanitize(message);

        // assert
        assertEquals(expected, actual);
    }
    

}
