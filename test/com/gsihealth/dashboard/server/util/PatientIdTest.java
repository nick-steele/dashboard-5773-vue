package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.server.dao.util.DaoUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test suite for stripping leading zeros from a patient ID
 *
 * QA_Enrollment App /Care Team App search Criteria_when the user try to search
 * the newly added patient by prefixing zero(0854 instead of 854) before the
 * search value in "Patient ID" field , search results are not retrieved .
 * [IN:003379]
 *
 * @author Chad Darby
 */
public class PatientIdTest {

    @Test
    public void testSingleLeadingZero() {
        String data = "01234";
        String expected = "1234";

        String actual = DaoUtils.stripLeadingZeros(data);

        assertEquals(expected, actual);
    }

    @Test
    public void testMultipleLeadingZeroWithAlpha() {
        String data = "0001234a";
        String expected = "1234a";

        String actual = DaoUtils.stripLeadingZeros(data);

        assertEquals(expected, actual);
    }

    @Test
    public void testInteriorZero() {
        String data = "101234";
        String expected = "101234";

        String actual = DaoUtils.stripLeadingZeros(data);

        assertEquals(expected, actual);
    }

    @Test
    public void testMultipleLeadingZeroWithNoAlpha() {
        String data = "000002829839";
        String expected = "2829839";

        String actual = DaoUtils.stripLeadingZeros(data);

        assertEquals(expected, actual);
    }

    @Test
    public void testSingleZero() {
        String data = "0";
        String expected = "0";

        String actual = DaoUtils.stripLeadingZeros(data);
        
        assertEquals(expected, actual);
    }

    @Test
    public void testAllLeadingZero() {
        String data = "0000009";
        String expected = "9";

        String actual = DaoUtils.stripLeadingZeros(data);

        assertEquals(expected, actual);
    }

    @Test
    public void testAllLeadingZeroSingleAlpha() {
        String data = "01234";
        String expected = "1234";

        String actual = DaoUtils.stripLeadingZeros(data);

        assertEquals(expected, actual);
    }
    
     @Test
    public void testFilterOutOtherParams(){
        String patientId= "70727?commid=2";
        String fix = "70727";
        
        if (StringUtils.isBlank(patientId)) {
            patientId = "-1";
        } else {
        patientId = patientId.substring(0, patientId.indexOf('?'));
         }
            System.out.println("PatiendId: "+ patientId);
        assertEquals(fix, patientId);
    }
     @Test
    public void testFilterNulls(){
        String patientId= null;
        String fix = "70727";
        
         if (StringUtils.isBlank(patientId)) {
            patientId = "-1";
        } else {
        patientId = patientId.substring(0, patientId.indexOf('?'));
         }
             System.out.println("Null PatiendId: "+ patientId);
        assertEquals("-1",patientId);
    }
    
    @Test
    public void testBlanks(){
        String patientId= " ";
        String fix = "70727";
       
         if (StringUtils.isBlank(patientId)) {
            patientId = "-1";
        } else {
        patientId = patientId.substring(0, patientId.indexOf('?'));
         }
         System.out.println("Blank PatiendId: "+ patientId);
         assertEquals("-1",patientId);
    }
    
  
    public void cleanPatientId(String fix, String patientId ){
        
        
        
    }

}