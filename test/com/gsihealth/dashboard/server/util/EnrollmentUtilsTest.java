/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.common.SearchCriteria;
import com.gsihealth.dashboard.server.WebConstants;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class EnrollmentUtilsTest {

    public EnrollmentUtilsTest() {
    }

    @Test
    public void computeSendEnrollmentStatus_Null_to_Assigned() {
        // setup
        String oldEnrollmentStatus = null;
        String newEnrollmentStatus = WebConstants.ASSIGNED;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Null_to_Enrolled() {
        // setup
        String oldEnrollmentStatus = null;
        String newEnrollmentStatus = WebConstants.ENROLLED;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Null_to_Pending() {
        // setup
        String oldEnrollmentStatus = null;
        String newEnrollmentStatus = WebConstants.PENDING;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = false;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Null_to_Inactive() {
        // setup
        String oldEnrollmentStatus = null;
        String newEnrollmentStatus = WebConstants.INACTIVE;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = false;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Pending_to_Inactive() {
        // setup
        String oldEnrollmentStatus = WebConstants.PENDING;
        String newEnrollmentStatus = WebConstants.INACTIVE;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = false;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Inactive_to_Pending() {
        // setup
        String oldEnrollmentStatus = WebConstants.INACTIVE;
        String newEnrollmentStatus = WebConstants.PENDING;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = false;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Enrolled_to_Assigned() {
        // setup
        String oldEnrollmentStatus = WebConstants.ENROLLED;
        String newEnrollmentStatus = WebConstants.ASSIGNED;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = false;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Assigned_to_Enrolled() {
        // setup
        String oldEnrollmentStatus = WebConstants.ASSIGNED;
        String newEnrollmentStatus = WebConstants.ENROLLED;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = false;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Pending_to_Enrolled() {
        // setup
        String oldEnrollmentStatus = WebConstants.PENDING;
        String newEnrollmentStatus = WebConstants.ENROLLED;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Inactive_to_Enrolled() {
        // setup
        String oldEnrollmentStatus = WebConstants.INACTIVE;
        String newEnrollmentStatus = WebConstants.ENROLLED;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Pending_to_Assigned() {
        // setup
        String oldEnrollmentStatus = WebConstants.PENDING;
        String newEnrollmentStatus = WebConstants.ASSIGNED;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Inactive_to_Assigned() {
        // setup
        String oldEnrollmentStatus = WebConstants.INACTIVE;
        String newEnrollmentStatus = WebConstants.ASSIGNED;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Assigned_to_Pending() {
        // setup
        String oldEnrollmentStatus = WebConstants.ASSIGNED;
        String newEnrollmentStatus = WebConstants.PENDING;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Assigned_to_Inactive() {
        // setup
        String oldEnrollmentStatus = WebConstants.ASSIGNED;
        String newEnrollmentStatus = WebConstants.INACTIVE;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Enrolled_to_Inactive() {
        // setup
        String oldEnrollmentStatus = WebConstants.ENROLLED;
        String newEnrollmentStatus = WebConstants.INACTIVE;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Enrolled_to_Pending() {
        // setup
        String oldEnrollmentStatus = WebConstants.ENROLLED;
        String newEnrollmentStatus = WebConstants.PENDING;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Pending_to_Pending() {
        // setup
        String oldEnrollmentStatus = WebConstants.PENDING;
        String newEnrollmentStatus = WebConstants.PENDING;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = false;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Inactive_to_Inactive() {
        // setup
        String oldEnrollmentStatus = WebConstants.INACTIVE;
        String newEnrollmentStatus = WebConstants.INACTIVE;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = false;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Enrolled_to_Enrolled() {
        // setup
        String oldEnrollmentStatus = WebConstants.ENROLLED;
        String newEnrollmentStatus = WebConstants.ENROLLED;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = false;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Assigned_to_Assigned() {
        // setup
        String oldEnrollmentStatus = WebConstants.ASSIGNED;
        String newEnrollmentStatus = WebConstants.ASSIGNED;

        // execute
        boolean actual = EnrollmentUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus);

        // assert
        boolean expected = false;

        assertEquals(expected, actual);
    }

    @Test
    public void hasDemographicData_None() {
        
        SearchCriteria searchCriteria = new SearchCriteria();
        
        boolean actual = EnrollmentUtils.hasDemographicData(searchCriteria);
        
        boolean expected = false;
        
        assertEquals(expected, actual);
    }
    
    @Test
    public void hasDemographicData_FirstName() {
        
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setFirstName("foo");
        
        boolean actual = EnrollmentUtils.hasDemographicData(searchCriteria);
        
        boolean expected = true;
        
        assertEquals(expected, actual);
    }

    @Test
    public void hasDemographicData_LastName() {
        
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setLastName("foo");
        
        boolean actual = EnrollmentUtils.hasDemographicData(searchCriteria);
        
        boolean expected = true;
        
        assertEquals(expected, actual);
    }

    @Test
    public void hasDemographicData_MiddleName() {
        
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setMiddleName("foo");
        
        boolean actual = EnrollmentUtils.hasDemographicData(searchCriteria);
        
        boolean expected = true;
        
        assertEquals(expected, actual);
    }
    
    @Test
    public void hasDemographicData_Gender() {
        
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setGender("F");
        
        boolean actual = EnrollmentUtils.hasDemographicData(searchCriteria);
        
        boolean expected = true;
        
        assertEquals(expected, actual);
    }

    @Test
    public void hasDemographicData_DateOfBirth() {
        
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setDateOfBirthStr("foo");
        
        boolean actual = EnrollmentUtils.hasDemographicData(searchCriteria);
        
        boolean expected = true;
        
        assertEquals(expected, actual);
    }

    @Test
    public void hasDemographicData_Address1() {
        
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAddress1("foo");
        
        boolean actual = EnrollmentUtils.hasDemographicData(searchCriteria);
        
        boolean expected = true;
        
        assertEquals(expected, actual);
    }
    
    @Test
    public void hasDemographicData_Address2() {
        
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setAddress2("foo");
        
        boolean actual = EnrollmentUtils.hasDemographicData(searchCriteria);
        
        boolean expected = true;
        
        assertEquals(expected, actual);
    }
    
    
    @Test
    public void hasDemographicData_City() {
        
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setCity("foo");
        
        boolean actual = EnrollmentUtils.hasDemographicData(searchCriteria);
        
        boolean expected = true;
        
        assertEquals(expected, actual);
    }
    
    @Test
    public void hasDemographicData_State() {
        
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setState("foo");
        
        boolean actual = EnrollmentUtils.hasDemographicData(searchCriteria);
        
        boolean expected = true;
        
        assertEquals(expected, actual);
    }
    
    @Test
    public void hasDemographicData_ZipCode() {
        
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setZipCode("foo");
        
        boolean actual = EnrollmentUtils.hasDemographicData(searchCriteria);
        
        boolean expected = true;
        
        assertEquals(expected, actual);
    }
    
    @Test
    public void hasDemographicData_Phone() {
        
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setPhone("foo");
        
        boolean actual = EnrollmentUtils.hasDemographicData(searchCriteria);
        
        boolean expected = true;
        
        assertEquals(expected, actual);
    }
    
}
