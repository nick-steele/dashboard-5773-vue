package com.gsihealth.dashboard.server.util;

import java.text.DateFormat;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class DateUtilsTest {

    public DateUtilsTest() {
    }

    @Test
    public void testDaysBetweenDate_OneDay() throws Exception {

        DateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

        Date endDate = formatter.parse("2/23/2012");

        Date startDate = formatter.parse("2/22/2012");

        int actual = DateUtils.daysBetween(startDate, endDate);

        int expected = 1;
        assertEquals(expected, actual);
    }

    @Test
    public void testDaysBetweenDate_OneWeek() throws Exception {

        DateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

        Date endDate = formatter.parse("2/23/2012");

        Date startDate = formatter.parse("2/16/2012");

        int actual = DateUtils.daysBetween(startDate, endDate);

        int expected = 7;
        assertEquals(expected, actual);
    }

    @Test
    public void testDaysBetweenDate_SameDate() throws Exception {

        DateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

        Date endDate = formatter.parse("2/22/2012");

        Date startDate = formatter.parse("2/22/2012");

        int actual = DateUtils.daysBetween(startDate, endDate);

        int expected = 0;
        assertEquals(expected, actual);
    }

    @Test
    public void testDaysBetweenDate_Negative() throws Exception {

        DateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");

        Date endDate = formatter.parse("2/22/2012");

        Date startDate = formatter.parse("2/24/2012");

        int actual = DateUtils.daysBetween(startDate, endDate);

        int expected = 2;
        assertEquals(expected, actual);
    }

    @Test
    public void testDaysBetweenDate_23Hours() throws Exception {

        DateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy h:mm a");

        Date endDate = formatter.parse("2/22/2012 9:00 am");

        Date startDate = formatter.parse("2/21/2012 10:00 am");

        int actual = DateUtils.daysBetween(startDate, endDate);

        int expected = 0;
        assertEquals(expected, actual);
    }

    @Test
    public void testDaysBetweenDate_25Hours() throws Exception {

        DateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy h:mm a");

        Date endDate = formatter.parse("2/22/2012 9:00 am");

        Date startDate = formatter.parse("2/21/2012 8:00 am");

        int actual = DateUtils.daysBetween(startDate, endDate);

        int expected = 1;
        assertEquals(expected, actual);
    }

    @Test
    public void testCombineDateTime_AM() throws Exception {
        DateFormat dateFormatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
        DateFormat dateTimeFormatter = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

        Date theDate = dateFormatter.parse("2/22/2012");
        String time = "9:17";
        String ampm = "AM";
        
        Date expectedDate = dateTimeFormatter.parse("2/22/2012 9:17 AM");
        
        Date resultDate = DateUtils.combineDateTime(theDate, time, ampm);
        
        assertEquals(expectedDate, resultDate);        
    }
    
    @Test
    public void testCombineDateTime_PM() throws Exception {
        DateFormat dateFormatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
        DateFormat dateTimeFormatter = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

        Date theDate = dateFormatter.parse("2/22/2012");
        String time = "9:17";
        String ampm = "PM";
        
        Date expectedDate = dateTimeFormatter.parse("2/22/2012 9:17 PM");
        
        Date resultDate = DateUtils.combineDateTime(theDate, time, ampm);
        
        assertEquals(expectedDate, resultDate);        
    }   
    
    @Test
    public void testCombineDateTime_12Noon() throws Exception {
        DateFormat dateFormatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
        DateFormat dateTimeFormatter = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

        Date theDate = dateFormatter.parse("2/22/2012");
        String time = "12:00";
        String ampm = "PM";
        
        Date expectedDate = dateTimeFormatter.parse("2/22/2012 12:00 PM");
        
        Date resultDate = DateUtils.combineDateTime(theDate, time, ampm);
        
        assertEquals(expectedDate, resultDate);        
    }   

    @Test
    public void testCombineDateTime_12AM() throws Exception {
        DateFormat dateFormatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
        DateFormat dateTimeFormatter = new java.text.SimpleDateFormat("MM/dd/yyyy hh:mm a");

        Date theDate = dateFormatter.parse("2/22/2012");
        String time = "00:00";
        String ampm = "AM";
        
        Date expectedDate = dateTimeFormatter.parse("2/22/2012 00:00 AM");
        
        Date resultDate = DateUtils.combineDateTime(theDate, time, ampm);
        
        assertEquals(expectedDate, resultDate);        
    }   
  
    @Test
    public void testGetDateNoon_lateNight() throws Exception {

        DateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy h:mm a");

        Date theDate = formatter.parse("2/22/2012 11:59 pm");
        
        Date expected = formatter.parse("2/22/2012 12:00 pm");
        
        Date actual = DateUtils.getDateNoon(theDate);
        
        assertEquals(expected, actual);
    }

    @Test
    public void testGetDateNoon_morning() throws Exception {

        DateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy h:mm a");

        Date theDate = formatter.parse("2/22/2012 9:12 am");
        
        Date expected = formatter.parse("2/22/2012 12:00 pm");
        
        Date actual = DateUtils.getDateNoon(theDate);
        
        assertEquals(expected, actual);
    }

    @Test
    public void testGetDateNoon_aroundNoon() throws Exception {

        DateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy h:mm a");

        Date theDate = formatter.parse("2/22/2012 12:05 pm");
        
        Date expected = formatter.parse("2/22/2012 12:00 pm");
        
        Date actual = DateUtils.getDateNoon(theDate);
        
        assertEquals(expected, actual);
    }
    
 }