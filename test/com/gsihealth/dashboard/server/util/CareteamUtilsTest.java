/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.util;

import com.gsihealth.dashboard.client.common.PatientInfo;
import com.gsihealth.dashboard.server.WebConstants;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class CareteamUtilsTest {

    public CareteamUtilsTest() {
    }

    @Test
    public void computeSendEnrollmentStatus_Pending_to_Inactive() {
        // setup
        String oldEnrollmentStatus = WebConstants.PENDING;
        String newEnrollmentStatus = WebConstants.INACTIVE;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setEnrollmentStatusChanged(true);

        // execute
        boolean actual = CareteamUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus, patientInfo);

        // assert
        boolean expected = false;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Inactive_to_Pending() {
        // setup
        String oldEnrollmentStatus = WebConstants.INACTIVE;
        String newEnrollmentStatus = WebConstants.PENDING;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setEnrollmentStatusChanged(true);

        // execute
        boolean actual = CareteamUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus, patientInfo);

        // assert
        boolean expected = false;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Enrolled_to_Assigned() {
        // setup
        String oldEnrollmentStatus = WebConstants.ENROLLED;
        String newEnrollmentStatus = WebConstants.ASSIGNED;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setEnrollmentStatusChanged(true);

        // execute
        boolean actual = CareteamUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus, patientInfo);

        // assert
        boolean expected = false;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Assigned_to_Enrolled() {
        // setup
        String oldEnrollmentStatus = WebConstants.ASSIGNED;
        String newEnrollmentStatus = WebConstants.ENROLLED;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setEnrollmentStatusChanged(true);

        // execute
        boolean actual = CareteamUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus, patientInfo);

        // assert
        boolean expected = false;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Pending_to_Enrolled() {
        // setup
        String oldEnrollmentStatus = WebConstants.PENDING;
        String newEnrollmentStatus = WebConstants.ENROLLED;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setEnrollmentStatusChanged(true);

        // execute
        boolean actual = CareteamUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus, patientInfo);

        // assert
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Inactive_to_Enrolled() {
        // setup
        String oldEnrollmentStatus = WebConstants.INACTIVE;
        String newEnrollmentStatus = WebConstants.ENROLLED;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setEnrollmentStatusChanged(true);

        // execute
        boolean actual = CareteamUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus, patientInfo);

        // assert
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Pending_to_Assigned() {
        // setup
        String oldEnrollmentStatus = WebConstants.PENDING;
        String newEnrollmentStatus = WebConstants.ASSIGNED;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setEnrollmentStatusChanged(true);

        // execute
        boolean actual = CareteamUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus, patientInfo);

        // assert
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Inactive_to_Assigned() {
        // setup
        String oldEnrollmentStatus = WebConstants.INACTIVE;
        String newEnrollmentStatus = WebConstants.ASSIGNED;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setEnrollmentStatusChanged(true);

        // execute
        boolean actual = CareteamUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus, patientInfo);

        // assert
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Assigned_to_Pending() {
        // setup
        String oldEnrollmentStatus = WebConstants.ASSIGNED;
        String newEnrollmentStatus = WebConstants.PENDING;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setEnrollmentStatusChanged(true);

        // execute
        boolean actual = CareteamUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus, patientInfo);

        // assert
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Assigned_to_Inactive() {
        // setup
        String oldEnrollmentStatus = WebConstants.ASSIGNED;
        String newEnrollmentStatus = WebConstants.INACTIVE;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setEnrollmentStatusChanged(true);

        // execute
        boolean actual = CareteamUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus, patientInfo);

        // assert
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Enrolled_to_Inactive() {
        // setup
        String oldEnrollmentStatus = WebConstants.ENROLLED;
        String newEnrollmentStatus = WebConstants.INACTIVE;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setEnrollmentStatusChanged(true);

        // execute
        boolean actual = CareteamUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus, patientInfo);

        // assert
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Enrolled_to_Pending() {
        // setup
        String oldEnrollmentStatus = WebConstants.ENROLLED;
        String newEnrollmentStatus = WebConstants.PENDING;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setEnrollmentStatusChanged(true);

        // execute
        boolean actual = CareteamUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus, patientInfo);

        // assert
        boolean expected = true;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Pending_to_Pending() {
        // setup
        String oldEnrollmentStatus = WebConstants.PENDING;
        String newEnrollmentStatus = WebConstants.PENDING;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setEnrollmentStatusChanged(false);

        // execute
        boolean actual = CareteamUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus, patientInfo);

        // assert
        boolean expected = false;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Inactive_to_Inactive() {
        // setup
        String oldEnrollmentStatus = WebConstants.INACTIVE;
        String newEnrollmentStatus = WebConstants.INACTIVE;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setEnrollmentStatusChanged(false);

        // execute
        boolean actual = CareteamUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus, patientInfo);

        // assert
        boolean expected = false;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Enrolled_to_Enrolled() {
        // setup
        String oldEnrollmentStatus = WebConstants.ENROLLED;
        String newEnrollmentStatus = WebConstants.ENROLLED;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setEnrollmentStatusChanged(false);

        // execute
        boolean actual = CareteamUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus, patientInfo);

        // assert
        boolean expected = false;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendEnrollmentStatus_Assigned_to_Assigned() {
        // setup
        String oldEnrollmentStatus = WebConstants.ASSIGNED;
        String newEnrollmentStatus = WebConstants.ASSIGNED;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setEnrollmentStatusChanged(false);

        // execute
        boolean actual = CareteamUtils.computeSendEnrollmentStatusFlagForBHIX(oldEnrollmentStatus, newEnrollmentStatus, patientInfo);

        // assert
        boolean expected = false;

        assertEquals(expected, actual);
    }

    @Test
    public void computeSendCareteamFlag_Changed_Careteam_and_EnrollmentStatus_to_Assigned() {
        
        // setup
        String newEnrollmentStatus = WebConstants.ASSIGNED;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setEnrollmentStatusChanged(true);
        patientInfo.setCareteamChanged(true);
        
        // execute
        boolean actual = CareteamUtils.computeSendCareteamFlagForBHIX(patientInfo, newEnrollmentStatus);
        
        // assert
        boolean expected = true;
        
        assertEquals(expected, actual);
    }

    @Test
    public void computeSendCareteamFlag_Changed_Careteam_Assigned_and_No_Change_To_EnrollmentStatus() {
        
        // setup
        String newEnrollmentStatus = WebConstants.ASSIGNED;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setOldEnrollmentStatus(WebConstants.ASSIGNED);
        patientInfo.setEnrollmentStatusChanged(false);
        patientInfo.setCareteamChanged(true);
        
        // execute
        boolean actual = CareteamUtils.computeSendCareteamFlagForBHIX(patientInfo, newEnrollmentStatus);
        
        // assert
        boolean expected = true;
        
        assertEquals(expected, actual);
    }

    @Test
    public void computeSendCareteamFlag_No_Change_to_Careteam_and_No_Change_To_EnrollmentStatus() {
        
        // setup
        String newEnrollmentStatus = WebConstants.ASSIGNED;
        PatientInfo patientInfo = new PatientInfo();
        patientInfo.setOldEnrollmentStatus(WebConstants.ASSIGNED);
        patientInfo.setEnrollmentStatusChanged(false);
        patientInfo.setCareteamChanged(false);
        
        // execute
        boolean actual = CareteamUtils.computeSendCareteamFlagForBHIX(patientInfo, newEnrollmentStatus);
        
        // assert
        boolean expected = false;
        
        assertEquals(expected, actual);
    }
    
}
