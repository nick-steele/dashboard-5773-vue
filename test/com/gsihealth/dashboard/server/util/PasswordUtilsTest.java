/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.util;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class PasswordUtilsTest {
    
    public PasswordUtilsTest() {
    }

    @Test
    public void generatePassword() {
     
        // setup
        String regex = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%=:\\?]).{8,12})";
        
        // execute
        String thePassword = PasswordUtils.generatePassword();
        
        // assert
        assertTrue(thePassword.matches(regex));
        
    }
}
