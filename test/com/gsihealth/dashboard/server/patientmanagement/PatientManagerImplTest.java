/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gsihealth.dashboard.server.patientmanagement;

import java.util.ArrayList;
import java.util.List;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.gsihealth.dashboard.server.common.Person;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class PatientManagerImplTest {

    public PatientManagerImplTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void isDuplicate_True() throws Exception {
        PatientManagerMock patientManager = new PatientManagerMock();

        List<Person> testData = getTestData();
        patientManager.setData(testData);
        
        boolean expected = true;

        Person person = createPerson("doug", "tippman", "07/07/1907", "M");
        boolean actual = patientManager.isDuplicate(person);

        assertEquals(expected, actual);
    }

    @Test
    public void isDuplicate_False_Name() throws Exception {
        boolean expected = false;

        PatientManagerMock patientManager = new PatientManagerMock();

        List<Person> testData = getTestData();
        patientManager.setData(testData);


        Person person = createPerson("bill", "tippman", "07/07/1907", "M");
        boolean actual = patientManager.isDuplicate(person);

        assertEquals(expected, actual);
    }

    @Test
    public void isDuplicate_False_DOB() throws Exception {
        boolean expected = false;

        PatientManagerMock patientManager = new PatientManagerMock();

        List<Person> testData = getTestData();
        patientManager.setData(testData);


        Person person = createPerson("doug", "tippman", "07/07/1999", "M");
        boolean actual = patientManager.isDuplicate(person);

        assertEquals(expected, actual);
    }
    
    @Test
    public void isDuplicate_False_Gender() throws Exception {
        boolean expected = false;

        PatientManagerMock patientManager = new PatientManagerMock();

        List<Person> testData = getTestData();
        patientManager.setData(testData);


        Person person = createPerson("doug", "tippman", "07/07/1907", "F");
        boolean actual = patientManager.isDuplicate(person);

        assertEquals(expected, actual);
    }

    @Test
    public void isDuplicateSsn_False() throws Exception {
        boolean expected = false;

        PatientManagerMock patientManager = new PatientManagerMock();

        List<Person> testData = getTestData();
        patientManager.setData(testData);


        Person person = createPerson("doug", "tippman", "07/07/1907", "F", "111112222");
        boolean actual = patientManager.isDuplicateSsn(person.getSsn());

        assertEquals(expected, actual);
    }

    @Test
    public void isDuplicateSsn_True() throws Exception {
        boolean expected = true;

        PatientManagerMock patientManager = new PatientManagerMock();

        List<Person> testData = getTestData();
        patientManager.setData(testData);


        Person person = createPerson("doug", "tippman", "07/07/1907", "F", "123456789");
        boolean actual = patientManager.isDuplicateSsn(person.getSsn());

        assertEquals(expected, actual);
    }

    @Test
    public void isDuplicateSsn_Null() throws Exception {
        boolean expected = false;

        PatientManagerMock patientManager = new PatientManagerMock();

        List<Person> testData = getTestData();
        patientManager.setData(testData);


        Person person = createPerson("doug", "tippman", "07/07/1907", "F", null);
        boolean actual = patientManager.isDuplicateSsn(person.getSsn());

        assertEquals(expected, actual);
    }

    @Test
    public void isDuplicateSsn_EmptyString() throws Exception {
        boolean expected = false;

        PatientManagerMock patientManager = new PatientManagerMock();

        List<Person> testData = getTestData();
        patientManager.setData(testData);


        Person person = createPerson("doug", "tippman", "07/07/1907", "F", "");
        boolean actual = patientManager.isDuplicateSsn(person.getSsn());

        assertEquals(expected, actual);
    }
    
    private List<Person> getTestData() throws Exception {
        List<Person> persons = new ArrayList<Person>();

        persons.add(createPerson("Doug", "Tippman", "07/07/1907",  "M", "123456789"));
        persons.add(createPerson("Doug", "Wilkerson", "07/05/1907", "M", "111334444"));
        persons.add(createPerson("Roger", "Tippman", "4/07/1907", "M", "222334444"));
        persons.add(createPerson("Anna", "Wilkerson", "07/05/1912", "F", "333334444"));
        
        return persons;
    }

    private Person createPerson(String firstName, String lastName, String dobStr, String gender) throws Exception {
        Person person = new Person();
        person.setFirstName(firstName);
        person.setLastName(lastName);

        SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
        Date dob = (Date) formatter.parse(dobStr);
        person.setDateOfBirth(dob);

        person.setGender(gender);

        return person;
    }
    
    private Person createPerson(String firstName, String lastName, String dobStr, String gender, String ssn) throws Exception {
        Person person = new Person();
        person.setFirstName(firstName);
        person.setLastName(lastName);

        SimpleDateFormat formatter = new java.text.SimpleDateFormat("MM/dd/yyyy");
        Date dob = (Date) formatter.parse(dobStr);
        person.setDateOfBirth(dob);

        person.setGender(gender);
        person.setSsn(ssn);
        
        return person;
    }
    
}