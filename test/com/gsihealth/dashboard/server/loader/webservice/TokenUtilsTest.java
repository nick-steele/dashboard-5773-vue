/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.loader.webservice;

import org.jasypt.util.text.BasicTextEncryptor;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class TokenUtilsTest {

    public TokenUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testBasicPerson() {
        String key = "alpha";
        TokenUtils tokenUtils = new TokenUtils(key);

        PersonDTO dto = new PersonDTO();
        dto.setFirstName("Larry");
        dto.setLastName("Mitchell");

        String token = getToken(dto, key);

        assertTrue(tokenUtils.isValidToken(dto, token));
    }

    @Test
    public void testNullPerson() {
        String key = "alpha";
        TokenUtils tokenUtils = new TokenUtils(key);

        PersonDTO dto = null;
        String token = getToken(dto, key);

        assertFalse(tokenUtils.isValidToken(dto, token));
    }

    @Test
    public void testBasicPerson_BadToken() {
        String key = "alpha";
        TokenUtils tokenUtils = new TokenUtils(key);

        PersonDTO dto = new PersonDTO();
        dto.setFirstName("Larry");
        dto.setLastName("Mitchell");

        String token = "bad token";

        assertFalse(tokenUtils.isValidToken(dto, token));
    }

    @Test
    public void testBasicPerson_PersonModifiedAfterToken() {
        String key = "alpha";
        TokenUtils tokenUtils = new TokenUtils(key);

        PersonDTO dto = new PersonDTO();
        dto.setFirstName("Larry");
        dto.setLastName("Mitchell");

        String token = getToken(dto, key);

        dto.setLastName("Mitchelp");
        assertFalse(tokenUtils.isValidToken(dto, token));
    }

    @Test
    public void testString() {
        String key = "alpha";
        TokenUtils tokenUtils = new TokenUtils(key);

        String text = "HowdyDoody";
        String token = getToken(text, key);

        assertTrue(tokenUtils.isValidToken(text, token));
    }

    @Test
    public void testString_BadToken() {
        String key = "alpha";
        TokenUtils tokenUtils = new TokenUtils(key);

        String text = "HowdyDoody";
        String token = "bad token";

        assertFalse(tokenUtils.isValidToken(text, token));
    }

    @Test
    public void testString_NullToken() {
        String key = "alpha";
        TokenUtils tokenUtils = new TokenUtils(key);

        String text = null;
        String token = getToken(text, key);

        assertFalse(tokenUtils.isValidToken(text, token));
    }
    
    private String getToken(PersonDTO person, String key) {
        String result = null;

        BasicTextEncryptor encryptor = new BasicTextEncryptor();
        encryptor.setPassword(key);

        DateFormat dateFormatter = new SimpleDateFormat("yyyy@MMM@dd");

        if (person != null) {
            result = person.getLastName() + person.getFirstName() + person.getZipCode();
            result += dateFormatter.format(new Date());
        } else {
            result = "";
        }

        return encryptor.encrypt(result);
    }

    private String getToken(String text, String key) {
        String result = null;

        BasicTextEncryptor encryptor = new BasicTextEncryptor();
        encryptor.setPassword(key);

        DateFormat dateFormatter = new SimpleDateFormat("yyyy@MMM@dd");

        if (text != null) {
            result = text;
            result += dateFormatter.format(new Date());
        } else {
            result = "";
        }

        return encryptor.encrypt(result);
    }

}
