/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.server.hisp;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class HispClientUtilsTest {
    
    public HispClientUtilsTest() {
    }

    @Test
    public void generateDirectAddress_WithSingleDomain() {
        // setup
        String email = "pmore@mypractice.com";
        String directDomain = "direct.gsihealth.com";
        
        // execute
        String actual = HispClientUtils.generateDirectAddress(email, directDomain);
        
        // assert
        String expected = "pmore_mypractice@" + directDomain;
        
        assertEquals(expected, actual);
    }

    @Test
    public void generateDirectAddress_WithSubDomains() {
        // setup
        String email = "jbleeker@ubdom.dom.com";
        String directDomain = "direct.gsihealth.com";
        
        // execute
        String actual = HispClientUtils.generateDirectAddress(email, directDomain);
        
        // assert
        String expected = "jbleeker_ubdom_dom@" + directDomain;
        
        assertEquals(expected, actual);
    }

    // @Test
    public void addUser() throws Exception {
        // setup
        // String email = "beta@udub.com";
        String directDomain = "direct.gsihealth.com";
        
        HispClient hispClient = new HispClientImpl("localhost", 4555, "root", "root");
        
        String email = "";
        for (int i=0; i < 512; i++) {
              email += "a";
        }
        
        email += "@udub.com";
        
        String directAddress = HispClientUtils.generateDirectAddress(email, directDomain);

        // execute
        // hispClient.createAccount(directAddress, "foobar");
        hispClient.createAccount("johnDoe@test.com", "foobar");
        
        // assert
        
        // assertEquals(expected, actual);
        
    }
    
    @Test
    public void testBuildJmxUrl() {
        String expected = "service:jmx:rmi://myserver/jndi/rmi://myserver:8888/jmxrmi";
        
        String actual = HispClientUtils.buildJmxUrl("myserver", 8888);
        
        assertEquals(expected, actual);
        
    }
}
