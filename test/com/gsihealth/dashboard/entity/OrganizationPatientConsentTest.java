package com.gsihealth.dashboard.entity;

import com.gsihealth.dashboard.entity.dto.OrganizationPatientConsentDTO;
import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderDTO;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class OrganizationPatientConsentTest {
    
    public OrganizationPatientConsentTest() {
    }

    @Test
    public void testConversion() {

        long patientId = 25;
        long organizationId = 1969;
        
        String consentStatus = "PERMIT";
        
        // setup
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        OrganizationPatientConsent consent = new OrganizationPatientConsent();
        
        OrganizationPatientConsentPK orgPK = new OrganizationPatientConsentPK();
        orgPK.setPatientId(patientId);
        orgPK.setOrganizationId(organizationId);
        consent.setOrganizationPatientConsentPK(orgPK);
        
        consent.setConsentStatus(consentStatus);
        
        // execute
        OrganizationPatientConsentDTO dto = mapper.map(consent, OrganizationPatientConsentDTO.class);

        // assert
        assertEquals(consent.getConsentStatus(), dto.getConsentStatus());

        assertEquals(consent.getOrganizationPatientConsentPK().getPatientId(), dto.getPatientId());
        assertEquals(consent.getOrganizationPatientConsentPK().getOrganizationId(), dto.getOrganizationId());        
    }
}
