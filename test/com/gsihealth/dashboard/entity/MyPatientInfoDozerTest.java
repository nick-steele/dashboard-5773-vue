package com.gsihealth.dashboard.entity;

import com.gsihealth.dashboard.entity.dto.MyPatientInfoDTO;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad.Darby
 */
public class MyPatientInfoDozerTest {

    @Test
    public void testConversionEntityToDTO() {

        long patientId = 25;
        long userId = 1969;
        long communityId = 1;
        Character flagged = 'Y';
        String firstName = "John";
        String lastName = "Doe";
        String cmOrganizationName = "Alpha Test";
        String enrollmentStatus = "Enrolled";
        
        // setup
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        MyPatientInfo myPatientInfo = new MyPatientInfo(userId, patientId, communityId, flagged, firstName, lastName, cmOrganizationName, enrollmentStatus);
                        
        // execute
        MyPatientInfoDTO dto = mapper.map(myPatientInfo, MyPatientInfoDTO.class);

        // assert
        MyPatientInfoPK myPatientInfoPK = myPatientInfo.getMyPatientInfoPK();
        assertEquals(myPatientInfoPK.getCommunityId(), dto.getCommunityId());
        assertEquals(myPatientInfoPK.getPatientId(), dto.getPatientId());
        assertEquals(myPatientInfoPK.getUserId(), dto.getUserId());
        
        assertEquals(myPatientInfo.getFlagged(), dto.getFlagged());
        assertEquals(myPatientInfo.getFirstName(), dto.getFirstName());
        assertEquals(myPatientInfo.getLastName(), dto.getLastName());
        assertEquals(myPatientInfo.getCmOrganizationName(), dto.getCmOrganizationName());
        assertEquals(myPatientInfo.getEnrollmentStatus(), dto.getEnrollmentStatus());
    }

    @Test
    public void testConversionDTOToEntity() {

        long patientId = 25;
        long userId = 1969;
        long communityId = 1;
        Character flagged = 'Y';
        String firstName = "John";
        String lastName = "Doe";
        String cmOrganizationName = "Alpha Test";
        String enrollmentStatus = "Enrolled";
        String euid = "0001222";
        
        // setup
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        MyPatientInfoDTO dto = new MyPatientInfoDTO(userId, patientId, communityId, flagged, firstName, lastName, cmOrganizationName, enrollmentStatus, euid);
                  
        // execute
        MyPatientInfo myPatientInfo = mapper.map(dto, MyPatientInfo.class);

        // assert
        MyPatientInfoPK myPatientInfoPK = myPatientInfo.getMyPatientInfoPK();
        assertEquals(dto.getCommunityId(), myPatientInfoPK.getCommunityId());
        assertEquals(dto.getPatientId(), myPatientInfoPK.getPatientId());
        assertEquals(dto.getUserId(), myPatientInfoPK.getUserId());
        
        assertEquals(dto.getFlagged(), myPatientInfo.getFlagged());
        assertEquals(dto.getFirstName(), myPatientInfo.getFirstName());
        assertEquals(dto.getLastName(), myPatientInfo.getLastName());
        assertEquals(dto.getCmOrganizationName(), myPatientInfo.getCmOrganizationName());
        assertEquals(dto.getEnrollmentStatus(), myPatientInfo.getEnrollmentStatus());
    }

    
}