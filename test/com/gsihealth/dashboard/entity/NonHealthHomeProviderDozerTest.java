package com.gsihealth.dashboard.entity;

import com.gsihealth.dashboard.entity.dto.NonHealthHomeProviderDTO;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 *
 * @author Chad Darby
 */
public class NonHealthHomeProviderDozerTest {
    
    @Test
    public void testConversion() {

        long patientId = 25;
        long providerId = 1969;
        
        String firstName = "John";
        String lastName = "Doe";
        
        // setup
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        NonHealthHomeProvider provider = new NonHealthHomeProvider();
        NonHealthHomeProviderPK providerPK = new NonHealthHomeProviderPK();
        providerPK.setProviderId(providerId);
        providerPK.setCommunityId(1);
        provider.setNonHealthHomeProviderPK(providerPK);
        
        
        provider.setFirstName(firstName);
        provider.setLastName(lastName);
        
        // execute
        NonHealthHomeProviderDTO dto = convertTODTO(provider);

        // assert
        assertEquals(provider.getFirstName(), dto.getFirstName());
        assertEquals(provider.getLastName(), dto.getLastName());

    //    assertEquals(provider.getPatientId(), dto.getPatientId());
        assertTrue(provider.getNonHealthHomeProviderPK().getProviderId() == dto.getProviderId());
    }
    
    public NonHealthHomeProviderDTO convertTODTO(NonHealthHomeProvider provider) {
        NonHealthHomeProviderDTO providerDTO = new NonHealthHomeProviderDTO();
        
        providerDTO.setCommunityId(provider.getNonHealthHomeProviderPK().getCommunityId());
        providerDTO.setProviderId(provider.getNonHealthHomeProviderPK().getProviderId());
        providerDTO.setFirstName(provider.getFirstName());
        providerDTO.setLastName(provider.getLastName());
        providerDTO.setMiddleInitial(provider.getMiddleInitial());
        providerDTO.setEmail(provider.getEmail());
        providerDTO.setCity(provider.getCity());
        providerDTO.setState(provider.getState());
        providerDTO.setStreetAddress1(provider.getStreetAddress1());
        providerDTO.setStreetAddress2(provider.getStreetAddress2());
        providerDTO.setConsentDateTime(provider.getConsentDateTime());
        providerDTO.setConsentStatus(provider.getConsentStatus());
        providerDTO.setConsentObtainedByUserId(provider.getConsentObtainedByUserId());
        providerDTO.setCredentials(provider.getCredentials());
        providerDTO.setZipCode(provider.getZipCode());
        providerDTO.setTelephoneNumber(provider.getTelephoneNumber());
        providerDTO.setTitle(provider.getTitle());
        
        return providerDTO;
    }
    
}
