package com.gsihealth.dashboard.common.util;

import com.gsihealth.dashboard.common.util.SsnUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class SsnUtilsTest {
    
    public SsnUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Test(expected=java.lang.IllegalArgumentException.class)
    public void getMaskedSsn_testNull() {
        
        String ssn = null;
        
        String actual = SsnUtils.getMaskedSsn(ssn);
    }

    @Test(expected=java.lang.IllegalArgumentException.class)
    public void getMaskedSsn_testEmptyString() {
        
        String ssn = "";
        
        String actual = SsnUtils.getMaskedSsn(ssn);
    }

    @Test(expected=java.lang.IllegalArgumentException.class)
    public void getMaskedSsn_testInvalidLength() {
        
        String ssn = "123";
        
        String actual = SsnUtils.getMaskedSsn(ssn);
    }

    @Test
    public void getMaskedSsn() {
        
        // setup
        String ssn = "123456789";
        
        // execute
        String actual = SsnUtils.getMaskedSsn(ssn);
        
        // assert
        String expected = "###-##-6789";
        
        assertEquals(expected, actual);
    }
    
}
