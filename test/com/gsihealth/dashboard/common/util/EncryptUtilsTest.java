/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.common.util;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class EncryptUtilsTest {
    
    public EncryptUtilsTest() {
    }
    
    
    @Test
    public void testEncode() throws Exception {
        // setup
        String data = "The quick brown fox jumped over the fence. His email is redfox@aol.com";
        
        // execute
        String actual = EncryptUtils.encode(data);
        
        // assert
        String expected = "54686520717569636b2062726f776e20666f78206a756d706564206f766572207468652066656e63652e2048697320656d61696c20697320726564666f7840616f6c2e636f6d";
        assertEquals(expected, actual);        
    }
    
    @Test
    public void testEncode_null() throws Exception {
        // setup
        String data = null;
        
        // execute
        String actual = EncryptUtils.encode(data);
        
        // assert
        assertNull(actual);        
    }
    
    @Test
    public void testDecode() {
        // setup
        String data = "54686520717569636b2062726f776e20666f78206a756d706564206f766572207468652066656e63652e2048697320656d61696c20697320726564666f7840616f6c2e636f6d";
        
        // execute       
        String actual = EncryptUtils.decode(data);
        
        // assert
        String expected = "The quick brown fox jumped over the fence. His email is redfox@aol.com";
        
        assertEquals(expected, actual);
    }

    @Test
    public void testDecode_null() throws Exception {
        // setup
        String data = null;
        
        // execute
        String actual = EncryptUtils.decode(data);
        
        // assert
        assertNull(actual);        
    }

}