package com.gsihealth.dashboard.common;

import com.gsihealth.dashboard.server.common.Person;
import com.gsihealth.dashboard.common.dto.PersonDTO;
import com.gsihealth.dashboard.entity.PatientEnrollmentPK;
import com.gsihealth.dashboard.entity.Community;
import java.util.Date;
import com.gsihealth.dashboard.entity.PatientEnrollment;
import com.gsihealth.dashboard.entity.dto.PatientEnrollmentDTO;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class PersonDTODozerTest {

    public PersonDTODozerTest() {
    }

    @Test
    public void testPerson() {

        // setup
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        PatientEnrollmentPK patientEnrollmentPK = new PatientEnrollmentPK();
        patientEnrollmentPK.setCommunityId(1L);
        patientEnrollmentPK.setPatientId(14L);

        PatientEnrollment patientEnrollment = new PatientEnrollment();
        patientEnrollment.setPatientEnrollmentPK(patientEnrollmentPK);
        patientEnrollment.setPatientEuid("1234567");
        patientEnrollment.setStartDate(new Date());
        //patientEnrollment.setProgramName("HEAL 17");
        patientEnrollment.setStatus("Enrolled");

        Person person = new Person();
        person.setFirstName("John");
        person.setLastName("Doe");
        person.setDateOfBirth(new Date());
        person.setPatientEnrollment(patientEnrollment);
        
        // execute
        PersonDTO dto = mapper.map(person, PersonDTO.class);

        // assert
        assertEquals(person.getFirstName(), dto.getFirstName());
        assertEquals(person.getLastName(), dto.getLastName());

        assertEquals(person.getPatientEnrollment().getPatientEuid(), dto.getPatientEnrollment().getPatientEuid());
        assertEquals(person.getPatientEnrollment().getStartDate(), dto.getPatientEnrollment().getStartDate());
    }

    @Test
    public void testPatientEnrollment() {

        // setup
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        PatientEnrollmentPK patientEnrollmentPK = new PatientEnrollmentPK();
        patientEnrollmentPK.setCommunityId(1L);
        patientEnrollmentPK.setPatientId(14L);
        
        PatientEnrollment patientEnrollment = new PatientEnrollment();
        patientEnrollment.setPatientEnrollmentPK(patientEnrollmentPK);
        patientEnrollment.setPatientEuid("1234567");
        patientEnrollment.setStartDate(new Date());
        //patientEnrollment.setProgramName("HEAL 17");
        patientEnrollment.setStatus("Enrolled");
        
        // execute
        PatientEnrollmentDTO dto = mapper.map(patientEnrollment, PatientEnrollmentDTO.class);

        // assert
        assertEquals(patientEnrollment.getPatientEuid(), dto.getPatientEuid());
        assertEquals(patientEnrollment.getStartDate(), dto.getStartDate());
        //assertEquals(patientEnrollment.getProgramName(), dto.getProgramName());
        assertEquals(patientEnrollment.getStatus(), dto.getStatus());
    }


    @Test
    public void testPatientEnrollment_copyCommunity() {

        // setup
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        PatientEnrollment patientEnrollment = new PatientEnrollment();
        patientEnrollment.setPatientEuid("1234567");

        Community comm = new Community();
        comm.setCommunityId(1l);
        comm.setCommunityName("Test");
        patientEnrollment.setCommunity(comm);

        // execute
        PatientEnrollmentDTO dto = mapper.map(patientEnrollment, PatientEnrollmentDTO.class);

        // assert
        
        assertEquals(patientEnrollment.getCommunity().getCommunityId(), dto.getCommunityDTO().getCommunityId());
        assertEquals(patientEnrollment.getCommunity().getCommunityName(), dto.getCommunityDTO().getCommunityName());
    }

    @Test
    public void testPatientEnrollment_PatientEnrollmentPK_fromEntityToDTO() {

        // setup
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        PatientEnrollment patientEnrollment = new PatientEnrollment();
        patientEnrollment.setPatientEuid("1234567");

        PatientEnrollmentPK patientEnrollmentPK = new PatientEnrollmentPK();
        patientEnrollmentPK.setPatientId(55L);
        patientEnrollmentPK.setCommunityId(23L);
        patientEnrollment.setPatientEnrollmentPK(patientEnrollmentPK);
        
        // execute
        PatientEnrollmentDTO dto = mapper.map(patientEnrollment, PatientEnrollmentDTO.class);

        // assert

        assertEquals(patientEnrollment.getPatientEnrollmentPK().getCommunityId(), dto.getCommunityId());
        assertEquals(patientEnrollment.getPatientEnrollmentPK().getPatientId(), dto.getPatientId());
    }

    @Test
    public void testPatientEnrollment_PatientEnrollmentPK_fromDTOtoEntity() {

        // setup
        Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();

        PatientEnrollmentDTO patientEnrollmentDTO = new PatientEnrollmentDTO();
        patientEnrollmentDTO.setPatientId(55L);
        patientEnrollmentDTO.setCommunityId(34L);
        patientEnrollmentDTO.setPatientMrn("12345");

        // execute
        PatientEnrollment patientEnrollment = mapper.map(patientEnrollmentDTO, PatientEnrollment.class);

        // assert

        assertEquals(patientEnrollmentDTO.getCommunityId(), patientEnrollment.getPatientEnrollmentPK().getCommunityId());
        assertEquals(patientEnrollmentDTO.getPatientId(), patientEnrollment.getPatientEnrollmentPK().getPatientId());
        assertEquals(patientEnrollmentDTO.getPatientMrn(), patientEnrollment.getPatientMrn());
    }

    
}