/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gsihealth.dashboard.client.util;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class StringUtilsTest {

    public StringUtilsTest() {
    }

    @Test
    public void testEquals_BothNull() {

        String s1 = null;
        String s2 = null;

        assertTrue(StringUtils.equals(s1, s2));
    }

    @Test
    public void testEquals_FirstStringNullSecondStringNotNull() {

        String s1 = null;
        String s2 = "abc";

        assertFalse(StringUtils.equals(s1, s2));
    }

    @Test
    public void testEquals_FirstStringNotNullSecondStringNull() {

        String s1 = "abc";
        String s2 = null;

        assertFalse(StringUtils.equals(s1, s2));
    }

    @Test
    public void testEquals_BothSameStringDiffCase() {

        String s1 = new String("abc");
        String s2 = new String("ABC");

        assertFalse(StringUtils.equals(s1, s2));
    }

    @Test
    public void testEquals_BothSameStringSameCase() {

        String s1 = new String("abc");
        String s2 = new String("abc");

        assertTrue(StringUtils.equals(s1, s2));
    }

///

    @Test
    public void testEqualsIgnoreCase_BothNull() {

        String s1 = null;
        String s2 = null;

        assertTrue(StringUtils.equalsIgnoreCase(s1, s2));
    }

    @Test
    public void testEqualsIgnoreCase_FirstStringNullSecondStringNotNull() {

        String s1 = null;
        String s2 = "abc";

        assertFalse(StringUtils.equalsIgnoreCase(s1, s2));
    }

    @Test
    public void testEqualsIgnoreCase_FirstStringNotNullSecondStringNull() {

        String s1 = "abc";
        String s2 = null;

        assertFalse(StringUtils.equalsIgnoreCase(s1, s2));
    }

    @Test
    public void testEqualsIgnoreCase_BothSameString() {

        String s1 = new String("abc");
        String s2 = new String("abc");

        assertTrue(StringUtils.equalsIgnoreCase(s1, s2));
    }

    @Test
    public void testEqualsIgnoreCase_BothSameStringDifferentCase() {

        String s1 = new String("abc");
        String s2 = new String("ABC");

        assertTrue(StringUtils.equalsIgnoreCase(s1, s2));
    }

    @Test
    public void testDefaultIfEmpty_StringIsNull() {

        // setup
        String s1 = null;
        String s2 = "foo";

        String expected = "foo";
        
        // execute
        String actual = StringUtils.defaultIfEmpty(s1, s2);

        // assert
        assertEquals(expected, actual);
    }

    @Test
    public void testDefaultIfEmpty_StringIsEmpty() {

        // setup
        String s1 = "";
        String s2 = "foo";

        String expected = "foo";
        
        // execute
        String actual = StringUtils.defaultIfEmpty(s1, s2);

        // assert
        assertEquals(expected, actual);
    }
    
    @Test
    public void testDefaultIfEmpty_StringIsNotNull() {

        // setup
        String s1 = "alpha";
        String s2 = "foo";

        String expected = "alpha";
        
        // execute
        String actual = StringUtils.defaultIfEmpty(s1, s2);

        // assert
        assertEquals(expected, actual);
    }

    @Test
    public void testDefaultIfEmpty_StringIsEmptyDefaultIsNull() {

        // setup
        String s1 = "";
        String s2 = null;

        String expected = null;
        
        // execute
        String actual = StringUtils.defaultIfEmpty(s1, s2);

        // assert
        assertEquals(expected, actual);
    }
    
}