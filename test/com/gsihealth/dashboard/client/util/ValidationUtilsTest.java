package com.gsihealth.dashboard.client.util;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class ValidationUtilsTest {

    @Test
    public void testIsValidPassword_EmptyStringWithSpace() {
        String password = " ";

        boolean actual = ValidationUtils.isValidPassword(password);

        assertFalse(actual);
    }

    @Test
    public void testIsValidPassword_EmptyString() {
        String password = "";

        boolean actual = ValidationUtils.isValidPassword(password);

        assertTrue(actual);
    }

    @Test
    public void testIsValidPassword_Yes() {
        String password = "Test123#";

        boolean actual = ValidationUtils.isValidPassword(password);

        assertTrue(actual);
    }

    @Test
    public void testIsValidPassword_LongPassword() {
        String password = "abcdef12345Test123#";

        boolean actual = ValidationUtils.isValidPassword(password);

        assertTrue(actual);
    }

    @Test
    public void testIsValidPassword_No() {
        String password = "abc1234";

        boolean actual = ValidationUtils.isValidPassword(password);

        assertFalse(actual);
    }

    @Test
    public void testIsValidPassword_MissingUppercase() {
        String password = "test123#";

        boolean actual = ValidationUtils.isValidPassword(password);

        assertFalse(actual);
    }

    @Test
    public void testIsValidPassword_MissingLowercase() {
        String password = "TEST123#";

        boolean actual = ValidationUtils.isValidPassword(password);

        assertFalse(actual);
    }

    @Test
    public void testIsValidPassword_MissingDigit() {
        String password = "TESTtest#";

        boolean actual = ValidationUtils.isValidPassword(password);

        assertFalse(actual);
    }

    @Test
    public void testIsValidPassword_HasWhiteSpace() {
        String password = "Test12 3#";

        boolean actual = ValidationUtils.isValidPassword(password);

        assertFalse(actual);
    }

    @Test
    public void testIsValidPassword_NotMinimumLength() {
        String password = "Test12#";

        boolean actual = ValidationUtils.isValidPassword(password);

        assertFalse(actual);
    }

    @Test
    public void testIsValidPassword_MissingSpecialSymbol() {
        String password = "Test1234";

        boolean actual = ValidationUtils.isValidPassword(password);

        assertFalse(actual);
    }

}
