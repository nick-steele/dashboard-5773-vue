package com.gsihealth.dashboard.client.careteam;

import java.util.Set;
import com.gsihealth.dashboard.entity.dto.UserDTO;
import java.util.ArrayList;
import java.util.List;
import com.gsihealth.dashboard.client.util.ApplicationContext;
import com.gsihealth.dashboard.client.util.Constants;
import java.util.LinkedHashMap;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class CareteamUtilsTest {

    public static final String[] REQUIRED_USER_LEVELS = {"Care Manager", "Care Navigator"};
    public static final String[] DUPLICATE_ROLES_FOR_CAREATEAM = {"Care Manager", "Care Navigator", "Wrap Around Service Provider", "Staff Extender"};

    public CareteamUtilsTest() {
    }

    @Test
    public void testGetCareteamId_Found() {

        // SETUP
        ApplicationContext context = ApplicationContext.getInstance();

        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
        data.put("1", "Alpha");
        data.put("2", "Beta");
        data.put("3", "Charlie");

        context.putAttribute(Constants.CARE_TEAM_CODES_KEY, data);

        // execute
        long actual = CareteamUtils.getCareteamId("Beta");

        // assert
        long expected = 2;

        assertEquals(expected, actual);
    }

    @Test(expected = java.lang.IllegalArgumentException.class)
    public void testGetCareteamId_NotFound() {

        // SETUP
        ApplicationContext context = ApplicationContext.getInstance();

        LinkedHashMap<String, String> data = new LinkedHashMap<String, String>();
        data.put("1", "Alpha");
        data.put("2", "Beta");
        data.put("3", "Charlie");

        context.putAttribute(Constants.CARE_TEAM_CODES_KEY, data);

        // execute
        long actual = CareteamUtils.getCareteamId("@@@@@@@");
    }

    @Test
    public void validateUserLevels_AllValid() {

        // Primary Care Physician, Psychiatrist, Therapist, Care Manager, Care Navigator

        // setup
        List<UserDTO> users = new ArrayList<UserDTO>();
        users.add(new UserDTO(1, "Primary Care Physician", "1"));
        users.add(new UserDTO(2, "Psychiatrist", "2"));
        users.add(new UserDTO(3, "Therapist", "3"));
        users.add(new UserDTO(4, "Care Manager", "4"));
        users.add(new UserDTO(5, "Care Navigator", "5"));

        // execute
        boolean actual = CareteamUtils.validateSelectedUserLevels(REQUIRED_USER_LEVELS, DUPLICATE_ROLES_FOR_CAREATEAM, users);

        // assert
        boolean expected = true;
        assertEquals(expected, actual);
    }

    @Test
    public void validateUserLevels_MissingOne() {

        // Primary Care Physician, Psychiatrist, Therapist, Care Manager, Care Navigator

        // setup
        List<UserDTO> users = new ArrayList<UserDTO>();
        users.add(new UserDTO(1, "Primary Care Physician", "1"));

        // execute
        boolean actual = CareteamUtils.validateSelectedUserLevels(REQUIRED_USER_LEVELS, DUPLICATE_ROLES_FOR_CAREATEAM, users);

        // assert
        boolean expected = false;
        assertEquals(expected, actual);
    }

    @Test
    public void validateUserLevels_MissingAll() {

        // Primary Care Physician, Psychiatrist, Therapist, Care Manager, Care Navigator

        // setup
        List<UserDTO> users = new ArrayList<UserDTO>();
        users.add(new UserDTO(1, "a", "98"));
        users.add(new UserDTO(2, "b", "99"));

        // execute
        boolean actual = CareteamUtils.validateSelectedUserLevels(REQUIRED_USER_LEVELS, DUPLICATE_ROLES_FOR_CAREATEAM, users);

        // assert
        boolean expected = false;
        assertEquals(expected, actual);
    }

    @Test
    public void validateUserLevels_Duplicates() {

        // Primary Care Physician, Psychiatrist, Therapist, Care Manager, Care Navigator

        // setup
        List<UserDTO> users = new ArrayList<UserDTO>();
        users.add(new UserDTO(1, "Primary Care Physician", "1"));
        users.add(new UserDTO(2, "Psychiatrist", "2"));
        users.add(new UserDTO(3, "Therapist", "3"));
        users.add(new UserDTO(4, "Care Manager", "4"));
        users.add(new UserDTO(5, "Care Navigator", "5"));
        users.add(new UserDTO(12, "Care Navigator", "5"));

        // execute
        boolean actual = CareteamUtils.validateSelectedUserLevels(REQUIRED_USER_LEVELS, DUPLICATE_ROLES_FOR_CAREATEAM, users);

        // assert
        boolean expected = true;
        assertEquals(expected, actual);
    }

    @Test
    public void validateUserLevels_Duplicates_StaffExtenders() {

        // Primary Care Physician, Psychiatrist, Therapist, Care Manager, Care Navigator

        // setup
        List<UserDTO> users = new ArrayList<UserDTO>();
        users.add(new UserDTO(1, "Primary Care Physician", "1"));
        users.add(new UserDTO(2, "Psychiatrist", "2"));
        users.add(new UserDTO(3, "Therapist", "3"));
        users.add(new UserDTO(4, "Care Manager", "4"));
        users.add(new UserDTO(5, "Care Navigator", "5"));
        users.add(new UserDTO(12, "Staff Extender", "6"));
        users.add(new UserDTO(18, "Staff Extender", "6"));

        // execute
        boolean actual = CareteamUtils.validateSelectedUserLevels(REQUIRED_USER_LEVELS, DUPLICATE_ROLES_FOR_CAREATEAM, users);

        // assert
        boolean expected = true;
        assertEquals(expected, actual);
    }

    @Test
    public void getDuplicateUserLevels_Supported_Dupes() {

        // Primary Care Physician, Psychiatrist, Therapist, Care Manager, Care Navigator

        String[] duplicateRoles = {"Care Manager", "Care Navigator", "Wrap Around Service Provider", "Staff Extender"};

        // setup
        List<UserDTO> users = new ArrayList<UserDTO>();
        users.add(new UserDTO(1, "Primary Care Physician", "1"));
        users.add(new UserDTO(2, "Psychiatrist", "2"));
        users.add(new UserDTO(3, "Therapist", "3"));
        users.add(new UserDTO(4, "Care Manager", "4"));
        users.add(new UserDTO(4, "Care Manager", "4"));
        users.add(new UserDTO(4, "Staff Extender", "6"));
        users.add(new UserDTO(4, "Staff Extender", "6"));
        users.add(new UserDTO(4, "Wrap Around Service Provider", "7"));
        users.add(new UserDTO(4, "Wrap Around Service Provider", "7"));
        users.add(new UserDTO(5, "Care Navigator", "5"));
        users.add(new UserDTO(12, "Care Navigator", "5"));

        // execute
        Set<String> dupes = CareteamUtils.getDuplicateUserLevels(duplicateRoles, users);

        // assert
        assertTrue(dupes.isEmpty());
    }

    @Test
    public void getDuplicateUserLevels_Unsupported_Dupes() {

        // Primary Care Physician, Psychiatrist, Therapist, Care Manager, Care Navigator

        String[] duplicateRoles = {"Care Manager", "Care Navigator", "Wrap Around Service Provider", "Staff Extender"};

        // setup
        List<UserDTO> users = new ArrayList<UserDTO>();
        users.add(new UserDTO(1, "Primary Care Physician", "1"));
        users.add(new UserDTO(2, "Psychiatrist", "2"));
        users.add(new UserDTO(3, "Therapist", "3"));
        users.add(new UserDTO(4, "Care Manager", "4"));
        users.add(new UserDTO(5, "Care Navigator", "5"));
        users.add(new UserDTO(12, "Care Navigator", "5"));
        users.add(new UserDTO(6, "Psychiatrist", "2"));

        // execute
        Set<String> dupes = CareteamUtils.getDuplicateUserLevels(duplicateRoles, users);

        // assert
        assertFalse(dupes.isEmpty());
    }

    @Test
    public void validateUserLevels_EmptyUsers() {

        // Primary Care Physician, Psychiatrist, Therapist, Care Manager, Care Navigator

        // setup
        List<UserDTO> users = new ArrayList<UserDTO>();

        // execute
        boolean actual = CareteamUtils.validateSelectedUserLevels(REQUIRED_USER_LEVELS, DUPLICATE_ROLES_FOR_CAREATEAM, users);

        // assert
        boolean expected = false;
        assertEquals(expected, actual);
    }

    @Test
    public void getMissingLevels_MissingOne() {

        // Primary Care Physician, Psychiatrist, Therapist, Care Manager, Care Navigator

        // setup
        List<UserDTO> users = new ArrayList<UserDTO>();
        users.add(new UserDTO(4, "Care Manager"));

        // execute
        Set<String> missing = CareteamUtils.getMissingUserLevels(REQUIRED_USER_LEVELS, users);

        // assert
        int expectedSize = 1;

        assertEquals(expectedSize, missing.size());
    }

    @Test
    public void getMissingLevels_None() {

        // Primary Care Physician, Psychiatrist, Therapist, Care Manager, Care Navigator

        // setup
        List<UserDTO> users = new ArrayList<UserDTO>();
        users.add(new UserDTO(1, "Primary Care Physician"));
        users.add(new UserDTO(2, "Psychiatrist"));
        users.add(new UserDTO(3, "Therapist"));
        users.add(new UserDTO(4, "Care Manager"));
        users.add(new UserDTO(5, "Care Navigator"));

        // execute
        Set<String> missing = CareteamUtils.getMissingUserLevels(REQUIRED_USER_LEVELS, users);

        // assert
        int expectedSize = 0;

        assertEquals(expectedSize, missing.size());
    }

    @Test
    public void getMissingLevels_Two() {

        // Primary Care Physician, Psychiatrist, Therapist, Care Manager, Care Navigator

        // setup
        List<UserDTO> users = new ArrayList<UserDTO>();

        // execute
        Set<String> missing = CareteamUtils.getMissingUserLevels(REQUIRED_USER_LEVELS, users);

        // assert
        int expectedSize = 2;

        assertEquals(expectedSize, missing.size());
    }

    @Test
    public void getMissingLevels_All() {

        // Primary Care Physician, Psychiatrist, Therapist, Care Manager, Care Navigator

        // setup
        List<UserDTO> users = new ArrayList<UserDTO>();

        // execute
        Set<String> missing = CareteamUtils.getMissingUserLevels(REQUIRED_USER_LEVELS, users);

        // assert
        int expectedSize = REQUIRED_USER_LEVELS.length;

        assertEquals(expectedSize, missing.size());
    }

    @Test
    public void hasInactiveUsers_OneInactiveUser() {


        // setup
        List<UserDTO> users = new ArrayList<UserDTO>();
        UserDTO tempUser = null;

        tempUser = new UserDTO();
        tempUser.setStatus(Constants.USER_STATUS_ACTIVE);
        users.add(tempUser);

        tempUser = new UserDTO();
        tempUser.setStatus(Constants.USER_STATUS_INACTIVE);
        users.add(tempUser);

        tempUser = new UserDTO();
        tempUser.setStatus(Constants.USER_STATUS_ACTIVE);
        users.add(tempUser);

        // execute
        boolean actual = CareteamUtils.hasInactiveUsers(users);

        // assert
        boolean expected = true;
        assertEquals(expected, actual);
    }

    @Test
    public void hasInActiveUsers_AllActiveUsers() {


        // setup
        List<UserDTO> users = new ArrayList<UserDTO>();
        UserDTO tempUser = null;

        tempUser = new UserDTO();
        tempUser.setStatus(Constants.USER_STATUS_ACTIVE);
        users.add(tempUser);

        tempUser = new UserDTO();
        tempUser.setStatus(Constants.USER_STATUS_ACTIVE);
        users.add(tempUser);

        // execute
        boolean actual = CareteamUtils.hasInactiveUsers(users);

        // assert
        boolean expected = false;
        assertEquals(expected, actual);
    }

    @Test
    public void hasInActiveUsers_AllInactiveUsers() {

        // setup
        List<UserDTO> users = new ArrayList<UserDTO>();
        UserDTO tempUser = null;

        tempUser = new UserDTO();
        tempUser.setStatus(Constants.USER_STATUS_INACTIVE);
        users.add(tempUser);

        tempUser = new UserDTO();
        tempUser.setStatus(Constants.USER_STATUS_INACTIVE);
        users.add(tempUser);

        // execute
        boolean actual = CareteamUtils.hasInactiveUsers(users);

        // assert
        boolean expected = true;
        assertEquals(expected, actual);
    }
}
