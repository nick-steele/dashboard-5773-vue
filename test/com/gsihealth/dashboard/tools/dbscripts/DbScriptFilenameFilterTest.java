/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsihealth.dashboard.tools.dbscripts;

import java.io.File;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Chad Darby
 */
public class DbScriptFilenameFilterTest {
    
    public DbScriptFilenameFilterTest() {
    }

            
    @Test
    public void testAccept() {
        // setup
        File dir = new File(".");
        String name = "db-updates-03-program-name-table.sql";
        DbScriptFilenameFilter instance = new DbScriptFilenameFilter();

        boolean expResult = true;
        
        // execute
        boolean result = instance.accept(dir, name);

        // assert
        assertEquals(expResult, result);
    }

        
    @Test
    public void testAccept_Double() {
        // setup
        File dir = new File(".");
        String name = "db-updates-12-race-code-table.sql";
        DbScriptFilenameFilter instance = new DbScriptFilenameFilter();

        boolean expResult = true;
        
        // execute
        boolean result = instance.accept(dir, name);

        // assert
        assertEquals(expResult, result);
    }
    
    @Test
    public void testFail_noDigits() {
        // setup
        File dir = new File(".");
        String name = "db-updates-program-name-table.sql";
        DbScriptFilenameFilter instance = new DbScriptFilenameFilter();

        boolean expResult = false;
        
        // execute
        boolean result = instance.accept(dir, name);

        // assert
        assertEquals(expResult, result);
    }

    @Test
    public void testFail_noBeginLabel() {
        // setup
        File dir = new File(".");
        String name = "coordinator-program-name-table.sql";
        DbScriptFilenameFilter instance = new DbScriptFilenameFilter();

        boolean expResult = false;
        
        // execute
        boolean result = instance.accept(dir, name);

        // assert
        assertEquals(expResult, result);
    }

    @Test
    public void testFail_badFileExtension() {
        // setup
        File dir = new File(".");
        String name = "db-updates-03-program-name-table.bat";
        DbScriptFilenameFilter instance = new DbScriptFilenameFilter();

        boolean expResult = false;
        
        // execute
        boolean result = instance.accept(dir, name);

        // assert
        assertEquals(expResult, result);
    }
    
}