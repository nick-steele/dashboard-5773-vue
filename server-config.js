module.exports = {
	"env": {
		"endpoints"		: {
			"configuration"	: "http://localhost:8080/",
			"userManager" : "http://10.218.110.104:8080/UserManager-1.0/"
		},
		"vars"			: {
			"NEW_RELIC_LICENSE_KEY" : "a8b791a4f064b1cb32297d0ecf2aec646d3320b7",
			"NEW_RELIC_APP_NAME"	: "LocalDev EventRouter",
			"NEW_RELIC_CAPTURE_PARAMS" : true
		}
	}
};