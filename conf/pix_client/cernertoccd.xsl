<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:tns="C:/gsihealth/cdr.xsd"  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"  version="2.0" xmlns:ns1="C:/gsihealth/cdr.xsd" xmlns:xsi="urn:hl7-org:v3 CDA.xsd" xmlns="urn:hl7-org:v3">
    <xsl:variable name="facOid" select="'2.16.840.1.113883.3.1282'"/>
    <xsl:variable name="facName" select="'Palomar Pomorado Health'"/>
    <xsl:variable name="transDate" select="current-dateTime()"/>
    <xsl:variable name="facTest" select="'GGGG'"/>
    <xsl:variable name="localType" select="'local'"/>
    <xsl:variable name="zeros" select="'00'"/>

    <xsl:template match='/'>        

        <xsl:apply-templates select='CCDData' />
    </xsl:template>
    <xsl:template match='CCDData'>       

        <xsl:element name="ClinicalDocument" >
            <xsl:namespace name="xsi" select="'http://www.w3.org/2001/XMLSchema-instance'"/>


            <xsl:element name="typeId">
                <xsl:attribute name="root">2.16.840.1.113883.1.3</xsl:attribute>
                <xsl:attribute name="extension">POCD_HD000040</xsl:attribute>
            </xsl:element>
            <xsl:element name="templateId">
                <xsl:attribute name="root">2.16.840.1.113883.10.20.1</xsl:attribute><!-- CCD v1.0 Templates Root -->
            </xsl:element>
            <xsl:element name="templateId">
                <xsl:attribute name="root">2.16.840.1.113883.3.88.11.32.1</xsl:attribute><!-- CCD v1.0 Templates Root -->
            </xsl:element>

            <xsl:element name="id">
                <xsl:attribute name="root">6858a017-39c1-4153-bbd4-eaedac72a0e7</xsl:attribute>
            </xsl:element>
            <xsl:element name="code">
                <xsl:attribute name="code">34133-9</xsl:attribute>
                <xsl:attribute name="codeSystem">2.16.840.1.113883.6.1</xsl:attribute>
                <xsl:attribute name="displayName">Summarization of episode note</xsl:attribute>
            </xsl:element>
            <xsl:element name="title">Continuity of Care Document</xsl:element>
            <xsl:element name="effectiveTime">
                <xsl:attribute name="value">
                    <xsl:value-of select='concat(replace(format-dateTime($transDate, "[Y0001][M01][D01][H01][m01][s01][z]"),"GMT",""),$zeros)'/>
                   
                </xsl:attribute>
            </xsl:element>
            <xsl:element name="confidentialityCode">
                <xsl:attribute name="code">N</xsl:attribute>
                <xsl:attribute name="codeSystem">2.16.840.1.113883.5.25</xsl:attribute>
            </xsl:element>
            <xsl:element name="languageCode">
                <xsl:attribute name="code">en-US</xsl:attribute>
            </xsl:element>

            <xsl:element name="recordTarget">
                <xsl:for-each select="PersonInfo">
                    <xsl:element name="patientRole"><!-- Root OID is the Personal Physicians HealthCare NPI. Extension is John"s medical record number. -->
                        <xsl:element name="id">
                            <xsl:attribute name="extension">
                                <xsl:value-of select="PersonId"/>
                            </xsl:attribute>
                            <xsl:attribute name="root">
                                <xsl:value-of select="$facOid"/>
                            </xsl:attribute>
                        </xsl:element>
                        <xsl:element name="id">
                            <xsl:attribute name="extension">
                                <xsl:value-of select="$facOid"/>
                                <xsl:value-of select="ID"/>
                            </xsl:attribute>
                            <xsl:attribute name="root">
                                <xsl:value-of select="$facOid"/>
                            </xsl:attribute>
                        </xsl:element>
                        <xsl:for-each select="Addresses/Address">
                            <xsl:variable name="type" select="AddressType/Meaning"/>
              
                            <xsl:if test="$type='HOME'">   
                                <xsl:element name="addr">
                                    <xsl:attribute name="use">H</xsl:attribute>
                                    <xsl:element name="streetAddressLine">
                                        <xsl:value-of select="Street1"/> 
                                        <xsl:value-of select="Street2"/>
                                        <xsl:value-of select="Street2"/>
                                    </xsl:element>
                                    <xsl:element name="city">
                                        <xsl:value-of select="City"/>
                                    </xsl:element>
                                    <xsl:element name="state">
                                        <xsl:value-of select="State/Meaning"/>
                                    </xsl:element>
                                    <xsl:element name="postalCode">
                                        <xsl:value-of select="Zipcode"/>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:if>       
                        </xsl:for-each>
                        <xsl:element name="patient">
                            
                            <xsl:variable name="name" select="FullName"/>
                            <xsl:if test="$name !=''">
                                <xsl:variable name="tokenizedSample" select="tokenize($name,' ')"/> 
                                <xsl:variable name="name1">
                                    <xsl:for-each select="$tokenizedSample">                                            
                                        <xsl:if test="position()=1">                                                  
                                            <xsl:value-of select="normalize-space(replace(.,',',''))"/> 
                                        </xsl:if>                                              
                                    </xsl:for-each>
                                </xsl:variable>
                                <xsl:variable name="name2">
                                    <xsl:for-each select="$tokenizedSample">                                              
                                        <xsl:if test="position()=2">                                                 
                                            <xsl:value-of select="normalize-space(replace(.,',',''))"/> 
                                        </xsl:if>                                              
                                    </xsl:for-each>
                                </xsl:variable>                                      
                                <xsl:variable name="name3">
                                    <xsl:for-each select="$tokenizedSample">                                              
                                        <xsl:if test="position()=3">                                                    
                                            <xsl:value-of select="normalize-space(replace(.,',',''))"/> 
                                        </xsl:if>                                              
                                    </xsl:for-each>
                                </xsl:variable>
                                <xsl:variable name="name4">
                                    <xsl:for-each select="$tokenizedSample">                                              
                                        <xsl:if test="position()=4">                                                  
                                            <xsl:value-of select="normalize-space(replace(.,',',''))"/> 
                                        </xsl:if>                                             
                                    </xsl:for-each>
                                </xsl:variable>
                            
                            
                                <xsl:element name="name">
                                    <xsl:element name="given">
                                        <xsl:value-of select="$name2"/>
                                    </xsl:element>
                                    <xsl:element name="given">
                                        <xsl:value-of select="$name3"/>
                                    </xsl:element>
                                    <xsl:element name="family">
                                        <xsl:value-of select="$name1"/>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:if>  
                            <xsl:element name="administrativeGenderCode">
                                <xsl:attribute name="code">
                                    <xsl:value-of select="Gender/Meaning"/>
                                </xsl:attribute>
                                <xsl:attribute name="codeSystem">2.16.840.1.113883.5.1</xsl:attribute>
                            </xsl:element>
                            <xsl:element name="birthTime">
                                <xsl:attribute name="value">                         
                                    <xsl:variable name="t" select="BirthDateTime"/>
                                    <xsl:value-of select='concat(replace(format-dateTime($t, "[Y0001][M01][D01][H01][m01][s01][z]"),"GMT",""),$zeros)'/>
                                </xsl:attribute>
                            </xsl:element>
                            <xsl:element name="raceCode">
                                <xsl:attribute name="code">
                                    <xsl:value-of select="Race/Value"/>
                                </xsl:attribute>
                                <xsl:attribute name="codeSystem">2.16.840.1.113883.6.238</xsl:attribute>
                            </xsl:element>
                            <!--xsl:element name="languageCommunication"> fix this in cerner, needs to be within personinfo
                                <xsl:element name="languageCode">
                                    <xsl:attribute name="code">
                                        <xsl:value-of select="Language/Meaning"/>
                                    </xsl:attribute>
                                </xsl:element>
                            </xsl:element-->
                        </xsl:element>
                        <xsl:element name="providerOrganization">
                            <xsl:element name="id">
                                <xsl:attribute name="root">
                                    <xsl:value-of select="$facOid"/>
                                </xsl:attribute>
                            </xsl:element>
                            <xsl:element name="name">
                                <xsl:value-of select="$facName"/>
                            </xsl:element>
                        </xsl:element>
                    </xsl:element>
                </xsl:for-each>
            </xsl:element>
            <xsl:for-each select="Providers/Provider">
                <xsl:variable name="ptc" select="PrsnlReltnType/Meaning"/>
                <xsl:variable name="edate" select="EVENT_START_DATE_TIME"/>
                <!--xsl:if test="$ptc='author'"-->
                <xsl:element name="author">
                    <xsl:element name="time">
                        <xsl:attribute name="value">
                            <xsl:value-of select='concat(replace(format-dateTime($transDate, "[Y0001][M01][D01][H01][m01][s01][z]"),"GMT",""),$zeros)'/>
                        </xsl:attribute>
                    </xsl:element>
                    <xsl:element name="assignedAuthor"><!-- OID is Dr. Flier"s NPI -->
                        <xsl:element name="id">
                            <xsl:attribute name="root">
                                <xsl:value-of select="$facOid"/>
                            </xsl:attribute>   
                            <xsl:attribute name="extension">
                                <xsl:value-of select="Prsnl/PrsnlId"/>
                            </xsl:attribute>
                        </xsl:element>
                        <xsl:variable name="softTest" select="SOFTWARE_NAME"/>
                        <xsl:variable name="lastTest" select="Prsnl/FullName"/>
                        <xsl:if test="$lastTest !=''">
                            <xsl:element name="assignedPerson">
                                
                                <xsl:variable name="name" select="Prsnl/FullName"/>
                                <xsl:if test="$name !=''">
                                    <xsl:variable name="tokenizedSample" select="tokenize($name,' ')"/> 
                                    <xsl:variable name="name1">
                                        <xsl:for-each select="$tokenizedSample">                                            
                                            <xsl:if test="position()=1">                                                  
                                                <xsl:value-of select="normalize-space(replace(.,',',''))"/> 
                                            </xsl:if>                                              
                                        </xsl:for-each>
                                    </xsl:variable>
                                    <xsl:variable name="name2">
                                        <xsl:for-each select="$tokenizedSample">                                              
                                            <xsl:if test="position()=2">                                                 
                                                <xsl:value-of select="normalize-space(replace(.,',',''))"/> 
                                            </xsl:if>                                              
                                        </xsl:for-each>
                                    </xsl:variable>                                      
                                    <xsl:variable name="name3">
                                        <xsl:for-each select="$tokenizedSample">                                              
                                            <xsl:if test="position()=3">                                                    
                                                <xsl:value-of select="normalize-space(replace(.,',',''))"/> 
                                            </xsl:if>                                              
                                        </xsl:for-each>
                                    </xsl:variable>
                                    <xsl:variable name="name4">
                                        <xsl:for-each select="$tokenizedSample">                                              
                                            <xsl:if test="position()=4">                                                  
                                                <xsl:value-of select="normalize-space(replace(.,',',''))"/> 
                                            </xsl:if>                                             
                                        </xsl:for-each>
                                    </xsl:variable>
                                
                                    <xsl:element name="name">
                                                                   
                                        <xsl:element name="given">
                                            <xsl:value-of select="$name2"/>
                                        </xsl:element>
                                        <xsl:element name="given">
                                            <xsl:value-of select="$name3"/>
                                        </xsl:element>
                                        <xsl:element name="family">
                                            <xsl:value-of select="$name1"/>
                                        </xsl:element>
                                        <xsl:element name="suffix">
                                            <xsl:value-of select="$name4"/>
                                        </xsl:element>     
                                    </xsl:element>
                                </xsl:if>
                            </xsl:element>
                            <xsl:element name="representedOrganization"><!-- OID is the Personal Physicians HealthCare NPI -->
                                <xsl:element name="id">
                                    <xsl:attribute name="root">
                                        <xsl:value-of select="$facOid"/>
                                    </xsl:attribute>   
                                    <xsl:attribute name="extension">
                                        <xsl:value-of select="Prsnl/PrsnlId"/>
                                    </xsl:attribute>
                                </xsl:element>
                                <xsl:element name="name">
                                    <xsl:value-of select="$facName"/>
                                </xsl:element>

                            </xsl:element>
                        </xsl:if>
                        <xsl:if test="$softTest !=''">
                            <xsl:element name="assignedAuthoringDevice">
                                <xsl:element name="softwareName">
                                    <xsl:element name="displayName">
                                        <xsl:value-of select="SOFTWARE_NAME"/>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:element name="representedOrganization"><!-- OID is the Personal Physicians HealthCare NPI -->
                                <xsl:element name="id">
                                    <xsl:attribute name="root">
                                        <xsl:value-of select="$facOid"/>
                                    </xsl:attribute>
                                </xsl:element>
                                <xsl:element name="name">
                                    <xsl:value-of select="$facName"/>
                                </xsl:element>

                            </xsl:element>
                        </xsl:if>
                    </xsl:element>
                </xsl:element>
                <!--/xsl:if-->
            </xsl:for-each>

            <!--xsl:element name="legalAuthenticator"> not really needed
                <xsl:element name="time">
                    <xsl:attribute name="value">
                        <xsl:value-of select='concat(replace(format-dateTime($transDate, "[Y0001][M01][D01][H01][m01][s01][z]"),"GMT",""),$zeros)'/>
                    </xsl:attribute>
                </xsl:element>
                <xsl:element name="signatureCode">
                    <xsl:attribute name="code">S</xsl:attribute>
                </xsl:element>
                <xsl:element name="assignedEntity">
                    <xsl:element name="id">
                        <xsl:attribute name="nullFlavor">NI</xsl:attribute>
                    </xsl:element>
                    <xsl:element name="representedOrganization"> 
                        <xsl:element name="id">
                            <xsl:attribute name="root">
                                <xsl:value-of select="$facOid"/>
                            </xsl:attribute>
                        </xsl:element>
                        <xsl:element name="name">
                            <xsl:value-of select="$facName"/>
                        </xsl:element>

                    </xsl:element>
                </xsl:element>
            </xsl:element-->


            <xsl:element name="custodian">
                <xsl:element name="assignedCustodian">
                    <xsl:element name="representedCustodianOrganization"><!-- OID is the Personal Physicians HealthCare NPI -->
                        <xsl:element name="id">
                            <xsl:attribute name="root">
                                <xsl:value-of select="$facOid"/>
                            </xsl:attribute>
                        </xsl:element>
                        <xsl:element name="name">
                            <xsl:value-of select="$facName"/>
                        </xsl:element>

                    </xsl:element>
                </xsl:element>
            </xsl:element>

            
            <xsl:element name="component">
                <xsl:element name="structuredBody">
                    <!-- ********************************************************Problems section********************************************************-->

                    <xsl:element name="component">
                        <xsl:element name="section">
                            <xsl:element name="templateId">
                                <xsl:attribute name="root">2.16.840.1.113883.10.20.1.11</xsl:attribute>
                            </xsl:element>

                            <xsl:element name="code">
                                <xsl:attribute name="code">11450-4</xsl:attribute>
                                <xsl:attribute name="codeSystem">2.16.840.1.113883.6.1</xsl:attribute>

                            </xsl:element>

                            <xsl:element name="title">Problems</xsl:element>
                            <xsl:element name="text">
                                <xsl:element name="table">
                                    <xsl:attribute name="border" >1</xsl:attribute>
                                    <xsl:attribute name="width">100%</xsl:attribute>
                                    <xsl:element name="thead">
                                        <xsl:element name="tr">
                                            <xsl:element name="th"> Condition </xsl:element>
                                            <xsl:element name="th"> Condition Date</xsl:element>
                                            <xsl:element name="th">Condition Status</xsl:element>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="tbody">
                                        
                                        <xsl:for-each select="Diagnoses/Diagnosis">
                                            <xsl:element name="tr">
                                                <xsl:element name="td">
                                                    <xsl:value-of select="DiagnosisDisplay"/>
                                                </xsl:element>
                                                <xsl:element name="td">
                                                    <xsl:value-of select="DiagnosisDateTime"/>
                                                </xsl:element>
                                                <xsl:element name="td">completed</xsl:element>
                                            </xsl:element>
                                        </xsl:for-each>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>
                            <xsl:for-each select="Diagnoses/Diagnosis">
                                <xsl:element name="entry">
                                    <xsl:attribute name="typeCode">DRIV</xsl:attribute>
                                    <xsl:element name="act">
                                        <xsl:attribute name="classCode">ACT</xsl:attribute>
                                        <xsl:attribute name="moodCode">EVN</xsl:attribute>
                                        <xsl:element name="templateId">
                                            <xsl:attribute name="root">2.16.840.1.113883.10.20.1.27</xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="id">
                                            <xsl:attribute name="root">ec8a6ff8-ed4b-4f7e-82c3-e98e58b45de7</xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="code">
                                            <xsl:attribute name="nullFlavor">NA</xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="entryRelationship">
                                            <xsl:attribute name="typeCode">SUBJ</xsl:attribute>
                                            <xsl:element name="observation">
                                                <xsl:attribute name="classCode">OBS</xsl:attribute>
                                                <xsl:attribute name="moodCode">EVN</xsl:attribute>
                                                <xsl:element name="templateId">
                                                    <xsl:attribute name="root">2.16.840.1.113883.10.20.1.28</xsl:attribute>
                                                </xsl:element>
                                                <xsl:element name="id">
                                                    <xsl:variable name="dia" select="OnsetDateTime/text()|SourceIdentifier/text()"/>
                                                    <!--xsl:variable name="uid2" select="util2:nameUUIDFromBytes($dia)"/-->
                                                    <!--xsl:variable name="uid2" select="util:randomUUID()"/-->   
                                                    <xsl:variable name="uid2" select="'c6f88323-67ad-11db-bd13-0800200c9a66'"/>   
                                                    <xsl:attribute name="root"> 
                                                        <xsl:value-of select="$uid2"/>
                                                    </xsl:attribute>
                                                </xsl:element>                                        
                                                <xsl:element name="code">
                                                    <xsl:attribute name="code">                                                      
                                                        <xsl:value-of select="'64572001'"/>                                                      
                                                    </xsl:attribute>
                                                    <xsl:attribute name="codeSystemName">                                                      
                                                        <xsl:value-of select="'SNOWMED-CT'"/>                                                      
                                                    </xsl:attribute>
                                                    <xsl:attribute name="codeSystem">
                                                        <xsl:value-of select="'2.16.840.1.113883.6.96'"/>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="displayName">                                                      
                                                        <xsl:value-of select="'Condition'"/>                                                      
                                                    </xsl:attribute>
                                                </xsl:element>
                                                
                                                <xsl:element name="text">
                                                    <xsl:element name="reference">
                                                        <xsl:attribute name="value">
                                                            <xsl:value-of select="DiagnosisDisplay"/>
                                                        </xsl:attribute>
                                                    </xsl:element>
                                                </xsl:element>
                                                <xsl:element name="statusCode">
                                                    <xsl:attribute name="code">completed</xsl:attribute>
                                                </xsl:element>
                                                <xsl:element name="effectiveTime">
                                                    <xsl:element name="low">
                                                        <xsl:attribute name="value">
                                                            <xsl:variable name="t" select="DiagnosisDateTime"/>
                                                            <xsl:value-of select='concat(replace(format-dateTime($t, "[Y0001][M01][D01][H01][m01][s01][z]"),"GMT",""),$zeros)'/>
                                                        </xsl:attribute>
                                                    </xsl:element>
                                                </xsl:element>

                                                <xsl:element name="value">
                                                    <xsl:attribute name="xsi:type">CD</xsl:attribute>
                                                    <xsl:attribute name="code">
                                                        <xsl:value-of select="DiagnosisId"/>
                                                    </xsl:attribute>
                                                    
                                                    
                                                    
                                                    
                                                    <xsl:attribute name="codeSystem">
                                                        <xsl:variable name="diagType" select="Nomenclature/NomenclatureId"/>                                                        
                                                        <xsl:if test="$diagType='ICD9'">
                                                            <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.1'"/>
                                                            <xsl:value-of select="$newdiagType"/>
                                                        </xsl:if>
                                                        <xsl:if test="$diagType='SNOMED'">
                                                            <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.96'"/>
                                                            <xsl:value-of select="$newdiagType"/>
                                                        </xsl:if>
                                                        <xsl:if test="$diagType='SNOMED-CT'">
                                                            <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.96'"/>
                                                            <xsl:value-of select="$newdiagType"/>
                                                        </xsl:if>  
                                                        <xsl:if test="$diagType='9211655'">
                                                            <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.96'"/>
                                                            <xsl:value-of select="$newdiagType"/>
                                                        </xsl:if>                                                
                                                    </xsl:attribute>
                                                    <xsl:attribute name="displayName">
                                                        <xsl:value-of select="DiagnosisDisplay"/>
                                                    </xsl:attribute>
                                                </xsl:element>
                                               
                                            </xsl:element>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:for-each>
                        </xsl:element>
                    </xsl:element>


                    <!-- ********************************************************Vitals section********************************************************-->

                    <xsl:element name="component">
                        <xsl:element name="section">
                            <xsl:element name="templateId">
                                <xsl:attribute name="root">2.16.840.1.113883.10.20.1.16</xsl:attribute>
                            </xsl:element>

                            <xsl:element name="code">
                                <xsl:attribute name="code">30954-2</xsl:attribute>
                                <xsl:attribute name="codeSystem">2.16.840.1.113883.6.1</xsl:attribute>
                            </xsl:element>
                            <xsl:element name="title">Vitals</xsl:element>
                            <xsl:element name="text">
                                <xsl:element name="table">
                                    <xsl:attribute name="border" >1</xsl:attribute>
                                    <xsl:attribute name="width">100%</xsl:attribute>
                                    <xsl:element name="thead">
                                        <xsl:element name="tr">
                                            <xsl:element name="th"> Test Date </xsl:element>
                                            <xsl:element name="th"> Test Name</xsl:element>
                                            <xsl:element name="th"> Units</xsl:element>
                                            <xsl:element name="th"> Results</xsl:element>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="tbody">
                                        
                                        <xsl:for-each select="Vitals/Vital">
                                            <xsl:element name="tr">
                                                <xsl:element name="td">
                                                    <xsl:value-of select="DateTime"/>
                                                </xsl:element>
                                                <xsl:element name="td">
                                                    <xsl:value-of select="VitalName"/>
                                                </xsl:element>
                                                <xsl:element name="td">
                                                    <xsl:value-of select="ResultUnit"/>
                                                </xsl:element>
                                                <xsl:element name="td">
                                                    <xsl:value-of select="ResultValue"/>
                                                </xsl:element>
                                            </xsl:element>
                                        </xsl:for-each>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>

                            <xsl:for-each-group select="Vitals/Vital" group-by="DateTime">
                                <xsl:element name="entry">
                                    <xsl:attribute name="typeCode">DRIV</xsl:attribute>
                                    <xsl:element name="organizer">
                                        <xsl:attribute name="classCode">CLUSTER</xsl:attribute>
                                        <xsl:attribute name="moodCode">EVN</xsl:attribute>
                                        <xsl:element name="templateId">
                                            <xsl:attribute name="root">2.16.840.1.113883.10.20.1.35</xsl:attribute><!-- aggregation-->
                                        </xsl:element>
                                        <xsl:element name="id">
                                            <xsl:variable name="tr" select="DateTime/text()" />
                                                        <!--xsl:variable name="uid" select="util:nameUUIDFromBytes($tr)"/-->
                                                      <!--xsl:variable name="uid" select="util:randomUUID()"/-->   
                                            <xsl:variable name="uid" select="'c6f88323-67ad-11db-bd13-0800200c9a66'"/>                                                      
                                            <xsl:attribute name="root"> 
                                                <xsl:value-of select="$uid"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="code">
                                            <xsl:attribute name="code">
                                                <xsl:value-of select="'46680005'"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="codeSystem">

                                                <xsl:variable name="codeType" select="'SNOMED'"/>

                                                <xsl:if test="$codeType='LOINC'">
                                                    <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.1'"/>
                                                    <xsl:value-of select="$newdiagType"/>
                                                </xsl:if>
                                                <xsl:if test="$codeType='SNOMED'">
                                                    <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.96'"/>
                                                    <xsl:value-of select="$newdiagType"/>
                                                </xsl:if>
                                                <xsl:if test="$codeType='SNOMED-CT'">
                                                    <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.96'"/>
                                                    <xsl:value-of select="$newdiagType"/>
                                                </xsl:if>

                                            </xsl:attribute>
                                            <xsl:attribute name="displayName">
                                                <xsl:value-of select="'Vital Signs'"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="statusCode">
                                            <xsl:attribute name="code">
                                                <xsl:value-of select="'completed'"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="effectiveTime">
                                            <xsl:attribute name="value">
                                                <xsl:variable name="t" select="DateTime"/>
                                                <xsl:value-of select='concat(replace(format-dateTime($t, "[Y0001][M01][D01][H01][m01][s01][z]"),"GMT",""),$zeros)'/>
                                            </xsl:attribute>
                                        </xsl:element>

                                        <xsl:for-each select="current-group()">
                                            <xsl:element name="component">
                                                <xsl:element name="observation">
                                                    <xsl:attribute name="classCode">OBS</xsl:attribute>
                                                    <xsl:attribute name="moodCode">EVN</xsl:attribute>
                                                    <xsl:element name="templateId">
                                                        <xsl:attribute name="root">2.16.840.1.113883.10.20.1.31</xsl:attribute>
                                                    </xsl:element>
                                                    <xsl:element name="id">
                                                        <xsl:variable name="tr" select="DateTime/text()|VitalId/text()" />
                                                        <!--xsl:variable name="uid" select="util:nameUUIDFromBytes($tr)"/-->
                                                       <!--xsl:variable name="uid" select="util:randomUUID()"/-->   
                                                        <xsl:variable name="uid" select="'c6f88323-67ad-11db-bd13-0800200c9a66'"/>    
                                                        <!--xsl:attribute name="root">57d07056-bd97-4c90-891d-eb716d3170c8</xsl:attribute-->
                                                        <xsl:attribute name="root"> 
                                                            <xsl:value-of select="$uid"/>
                                                        </xsl:attribute>
                                                    </xsl:element>
                                                    
                                                    <xsl:variable name="vc" select="VitalID"/>
                                                    
                                                    
                                                    
                                                    <xsl:element name="code">
                                                        <xsl:attribute name="code">
                                                            
                                                            <xsl:if test="$vc='bp-dia'">
                                                                <xsl:variable name="newcodeType" select="'271650006'"/>
                                                                <xsl:value-of select="$newcodeType"/>
                                                            </xsl:if>
                                                            <xsl:if test="$vc='bp-sys'">
                                                                <xsl:variable name="newcodeType" select="'271649006'"/>
                                                                <xsl:value-of select="$newcodeType"/>
                                                            </xsl:if>
                                                            <xsl:if test="$vc='temp'">
                                                                <xsl:variable name="newcodeType" select="'386725007'"/>
                                                                <xsl:value-of select="$newcodeType"/>
                                                            </xsl:if>                                                            
                                                            <xsl:if test="$vc='pulse'">
                                                                <xsl:variable name="newcodeType" select="'78564009'"/>
                                                                <xsl:value-of select="$newcodeType"/>
                                                            </xsl:if>
                                                            <xsl:if test="$vc='spo2'">
                                                                <xsl:variable name="newcodeType" select="'113080007'"/>
                                                                <xsl:value-of select="$newcodeType"/>
                                                            </xsl:if>   
                                                            <xsl:if test="$vc='resp'">
                                                                <xsl:variable name="newcodeType" select="'86290005'"/>
                                                                <xsl:value-of select="$newcodeType"/>
                                                            </xsl:if> 
                                                            <xsl:if test="$vc='814232'">
                                                                <xsl:variable name="newcodeType" select="'46680005'"/>
                                                                <xsl:value-of select="$newcodeType"/>
                                                            </xsl:if>                                                                
                                                        </xsl:attribute>
                                                        <xsl:attribute name="codeSystem">

                                                            <xsl:variable name="codeType" select="'SNOMED'"/>
                                                            <xsl:if test="$codeType='LOINC'">
                                                                <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.1'"/>
                                                                <xsl:value-of select="$newdiagType"/>
                                                            </xsl:if>
                                                            <xsl:if test="$codeType='SNOMED'">
                                                                <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.96'"/>
                                                                <xsl:value-of select="$newdiagType"/>
                                                            </xsl:if>
                                                            <xsl:if test="$codeType='SNOMED-CT'">
                                                                <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.96'"/>
                                                                <xsl:value-of select="$newdiagType"/>
                                                            </xsl:if>

                                                        </xsl:attribute>
                                                        <xsl:attribute name="displayName">
                                                            <xsl:value-of select="VitalName"/>
                                                        </xsl:attribute>
                                                    </xsl:element>
                                                    <xsl:element name="statusCode">
                                                        <xsl:attribute name="code">
                                                            <xsl:value-of select="'completed'"/>
                                                        </xsl:attribute>
                                                    </xsl:element>
                                                    <xsl:element name="effectiveTime">
                                                        <xsl:attribute name="value">
                                                            <xsl:variable name="t" select="DateTime"/>
                                                            <xsl:value-of select='concat(replace(format-dateTime($t, "[Y0001][M01][D01][H01][m01][s01][z]"),"GMT",""),$zeros)'/>
                                                        </xsl:attribute>
                                                    </xsl:element>

                                                    <xsl:variable name="resUnit" select="ResultUnit"/>
                                                    <xsl:variable name="resVal" select="ResultValue"/>
                                                    <xsl:if test="$resUnit !=''">   
                                                        <xsl:variable name="unitless" select='normalize-space(replace($resVal,$resUnit,""))'/>                                                    
                                                        <xsl:element name="value">
                                                            <xsl:attribute name="xsi:type">
                                                                <xsl:value-of select="'PQ'"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="value">
                                                                <xsl:value-of select="$unitless"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="unit">
                                                                <xsl:value-of select="$resUnit"/>
                                                            </xsl:attribute>
                                                        </xsl:element>
                                                    </xsl:if>
                                                    <xsl:element name="interpretationCode">
                                                        <xsl:attribute name="code">N</xsl:attribute>
                                                        <xsl:attribute name="codeSystem">2.16.840.1.113883.5.83</xsl:attribute>
                                                    </xsl:element>
                                                </xsl:element>
                                            </xsl:element>
                                        </xsl:for-each>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:for-each-group>
                        </xsl:element>
                    </xsl:element>
               <!-- ********************************************************Results section********************************************************-->

                    <xsl:element name="component">
                        <xsl:element name="section">
                            <xsl:element name="templateId">
                                <xsl:attribute name="root">2.16.840.1.113883.10.20.1.14</xsl:attribute>
                            </xsl:element>

                            <xsl:element name="code">
                                <xsl:attribute name="code">30954-2</xsl:attribute>
                                <xsl:attribute name="codeSystem">2.16.840.1.113883.6.1</xsl:attribute>
                            </xsl:element>
                            <xsl:element name="title">Results</xsl:element>
                            <xsl:element name="text">
                                <xsl:element name="table">
                                    <xsl:attribute name="border" >1</xsl:attribute>
                                    <xsl:attribute name="width">100%</xsl:attribute>
                                    <xsl:element name="thead">
                                        <xsl:element name="tr">
                                            <xsl:element name="th"> Test Date </xsl:element>
                                            <xsl:element name="th"> Test Name</xsl:element>
                                            <xsl:element name="th"> Units</xsl:element>
                                            <xsl:element name="th"> Results</xsl:element>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="tbody">
                                   
                                        <xsl:for-each select="TEST_RESULTS">
                                            <xsl:element name="tr">
                                                <xsl:element name="td">
                                                    <xsl:value-of select="REPORT_DATE_TIME"/>
                                                </xsl:element>
                                                <xsl:element name="td">
                                                    <xsl:value-of select="OBSERVATION_NAME"/>
                                                </xsl:element>
                                                <xsl:element name="td">
                                                    <xsl:value-of select="RESULT_UNIT"/>
                                                </xsl:element>
                                                <xsl:element name="td">
                                                    <xsl:value-of select="RESULT"/>
                                                </xsl:element>
                                            </xsl:element>
                                        </xsl:for-each>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>

                            <xsl:for-each-group select="TEST_RESULTS" group-by="ORDERED_TEST_CODE">
                                <xsl:element name="entry">
                                    <xsl:attribute name="typeCode">DRIV</xsl:attribute>
                                    <xsl:element name="organizer">
                                        <xsl:attribute name="classCode">BATTER</xsl:attribute>
                                        <xsl:attribute name="moodCode">EVN</xsl:attribute>
                                        <xsl:element name="templateId">
                                            <xsl:attribute name="root">2.16.840.1.113883.10.20.1.32</xsl:attribute><!--results organizer-->
                                        </xsl:element>
                                        <xsl:element name="id">
                                            <xsl:variable name="tr" select="COLLECTION_DATE_TIME/text()|ORDERED_TEST_CODE/text()" />
                                                        <!--xsl:variable name="uid" select="util:nameUUIDFromBytes($tr)"/-->
                                                     <!--xsl:variable name="uid" select="util:randomUUID()"/-->   
                                            <xsl:variable name="uid" select="'c6f88323-67ad-11db-bd13-0800200c9a66'"/>    
                                                       
                                            <xsl:attribute name="root"> 
                                                <xsl:value-of select="$uid"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="code">
                                            <xsl:attribute name="code">
                                                <xsl:value-of select="ORDERED_TEST_CODE"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="codeSystem">

                                                <xsl:variable name="codeType" select="ORDERED_TEST_CODE_TYPE"/>

                                                <xsl:if test="$codeType='LOINC'">
                                                    <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.1'"/>
                                                    <xsl:value-of select="$newdiagType"/>
                                                </xsl:if>
                                                <xsl:if test="$codeType='SNOMED'">
                                                    <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.96'"/>
                                                    <xsl:value-of select="$newdiagType"/>
                                                </xsl:if>
                                                <xsl:if test="$codeType='SNOMED-CT'">
                                                    <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.96'"/>
                                                    <xsl:value-of select="$newdiagType"/>
                                                </xsl:if>

                                            </xsl:attribute>
                                            <xsl:attribute name="displayName">
                                                <xsl:value-of select="ORDERED_TEST_NAME"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="statusCode">
                                            <xsl:attribute name="code">
                                                <xsl:value-of select="RESULT_STATUS"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="effectiveTime">
                                            <xsl:attribute name="value">
                                                <xsl:value-of select="COLLECTION_DATE_TIME"/>
                                            </xsl:attribute>
                                        </xsl:element>

                                        <xsl:for-each select="current-group()">
                                            <xsl:element name="component">
                                                <xsl:element name="observation">
                                                    <xsl:attribute name="classCode">OBS</xsl:attribute>
                                                    <xsl:attribute name="moodCode">EVN</xsl:attribute>
                                                    <xsl:element name="templateId">
                                                        <xsl:attribute name="root">2.16.840.1.113883.10.20.1.31</xsl:attribute>
                                                    </xsl:element>
                                                    <xsl:element name="id">
                                                        <xsl:variable name="tr" select="REPORT_DATE_TIME/text()|OBSERVATION_CODE/text()" />
                                                        <!--xsl:variable name="uid" select="util:nameUUIDFromBytes($tr)"/-->
                                                         <!--xsl:variable name="uid" select="util:randomUUID()"/-->   
                                                        <xsl:variable name="uid" select="'c6f88323-67ad-11db-bd13-0800200c9a66'"/>    
                                                        <!--xsl:attribute name="root">57d07056-bd97-4c90-891d-eb716d3170c8</xsl:attribute-->
                                                        <xsl:attribute name="root"> 
                                                            <xsl:value-of select="$uid"/>
                                                        </xsl:attribute>
                                                    </xsl:element>
                                                    <xsl:element name="code">
                                                        <xsl:attribute name="code">
                                                            <xsl:value-of select="OBSERVATION_CODE"/>
                                                        </xsl:attribute>
                                                        <xsl:attribute name="codeSystem">

                                                            <xsl:variable name="codeType" select="OBSERVATION_CODE_TYPE"/>
                                                            <xsl:if test="$codeType='LOINC'">
                                                                <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.1'"/>
                                                                <xsl:value-of select="$newdiagType"/>
                                                            </xsl:if>
                                                            <xsl:if test="$codeType='SNOMED'">
                                                                <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.96'"/>
                                                                <xsl:value-of select="$newdiagType"/>
                                                            </xsl:if>
                                                            <xsl:if test="$codeType='SNOMED-CT'">
                                                                <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.96'"/>
                                                                <xsl:value-of select="$newdiagType"/>
                                                            </xsl:if>

                                                        </xsl:attribute>
                                                        <xsl:attribute name="displayName">
                                                            <xsl:value-of select="OBSERVATION_NAME"/>
                                                        </xsl:attribute>
                                                    </xsl:element>
                                                    <xsl:element name="statusCode">
                                                        <xsl:attribute name="code">
                                                            <xsl:value-of select="RESULT_STATUS"/>
                                                        </xsl:attribute>
                                                    </xsl:element>
                                                    <xsl:element name="effectiveTime">
                                                        <xsl:attribute name="value">
                                                            <xsl:value-of select="REPORT_DATE_TIME"/>
                                                        </xsl:attribute>
                                                    </xsl:element>
                                                    <xsl:element name="value">
                                                        <xsl:attribute name="xsi:type">
                                                            <xsl:value-of select="RESULT_TYPE"/>
                                                        </xsl:attribute>
                                                        <xsl:attribute name="value">
                                                            <xsl:value-of select="RESULT"/>
                                                        </xsl:attribute>
                                                        <xsl:attribute name="unit">
                                                            <xsl:value-of select="RESULT_UNIT"/>
                                                        </xsl:attribute>
                                                    </xsl:element>
                                                    <xsl:element name="interpretationCode">
                                                        <xsl:attribute name="code">N</xsl:attribute>
                                                        <xsl:attribute name="codeSystem">2.16.840.1.113883.5.83</xsl:attribute>
                                                    </xsl:element>
                                                </xsl:element>
                                            </xsl:element>
                                        </xsl:for-each>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:for-each-group>
                        </xsl:element>
                    </xsl:element>
                    <!-- ********************************************************Medications section********************************************************-->

                    <xsl:element name="component">
                        <xsl:element name="section">
                            <xsl:element name="templateId">
                                <xsl:attribute name="root">2.16.840.1.113883.10.20.1.8</xsl:attribute>
                            </xsl:element>

                            <xsl:element name="code">
                                <xsl:attribute name="code">10160-0</xsl:attribute>
                                <xsl:attribute name="codeSystem">2.16.840.1.113883.6.1</xsl:attribute>
                            </xsl:element>
                            <xsl:element name="title">Medications</xsl:element>
                            <xsl:element name="text">
                                <xsl:element name="table">
                                    <xsl:attribute name="border" >1</xsl:attribute>
                                    <xsl:attribute name="width">100%</xsl:attribute>
                                    <xsl:element name="thead">
                                        <xsl:element name="tr">
                                            <xsl:element name="th"> Start Date </xsl:element>
                                            <xsl:element name="th"> Med Name</xsl:element>
                                            <xsl:element name="th"> Quantity</xsl:element>
                                            <xsl:element name="th"> Units</xsl:element>
                                            <xsl:element name="th"> Freq</xsl:element>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="tbody">
                                        
                                        <xsl:for-each select="Medications/Medication">
                                        
                                            <xsl:element name="tr">
                                                <xsl:element name="td">
                                                    <xsl:value-of select="StartDate"/>
                                                </xsl:element>
                                                <xsl:element name="td">
                                                    <xsl:value-of select="ProductName"/>
                                                </xsl:element>
                                                <xsl:element name="td">
                                                    <xsl:value-of select="Quantity"/>
                                                </xsl:element>
                                                <xsl:element name="td">
                                                    <xsl:value-of select="Directions"/>
                                                </xsl:element>
                                                    
                                            </xsl:element>
                                    
                                        </xsl:for-each>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>

                            <xsl:for-each select="Medications/Medication">
                                
                                <xsl:element name="entry">
                                    <xsl:attribute name="typeCode">DRIV</xsl:attribute>
                                    <xsl:element name="substanceAdministration">
                                        <xsl:attribute name="classCode">SBADM</xsl:attribute>
                                        <xsl:attribute name="moodCode">EVN</xsl:attribute>
                                        <xsl:element name="templateId">
                                            <xsl:attribute name="root">2.16.840.1.113883.10.20.1.24</xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="id">
                                            <xsl:variable name="med" select="StartDate/text()|ProductName/text()" />
                                            <!--xsl:variable name="uid3" select="util:nameUUIDFromBytes($med)"/-->
                                             <!--xsl:variable name="uid3" select="util:randomUUID()"/-->   
                                            <xsl:variable name="uid3" select="'c6f88323-67ad-11db-bd13-0800200c9a66'"/>    
                                            <xsl:attribute name="root"> 
                                                <xsl:value-of select="$uid3"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="statusCode">
                                            <xsl:attribute name="code">
                                                <xsl:value-of select="'completed'"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="effectiveTime">
                                            <xsl:attribute name="xsi:type">IVL_TS</xsl:attribute>
                                            <xsl:element name="low">
                                                <xsl:attribute name="value">
                                                    <xsl:variable name="t" select="StartDate"/>
                                                    <xsl:value-of select='concat(replace(format-dateTime($t, "[Y0001][M01][D01][H01][m01][s01][z]"),"GMT",""),$zeros)'/>
                                                </xsl:attribute>
                                            </xsl:element>
                                            <xsl:variable name="t" select="StopDate"/>
                                            <xsl:if test="$t!=''">
                                                <xsl:element name="high">
                                                    <xsl:attribute name="value">                                               
                                                        <xsl:value-of select='concat(replace(format-dateTime($t, "[Y0001][M01][D01][H01][m01][s01][z]"),"GMT",""),$zeros)'/>                                           
                                                    </xsl:attribute>
                                                </xsl:element>
                                            </xsl:if>
                                        </xsl:element>
                                        <xsl:variable name="dir" select="Directions"/>
                                        <xsl:variable name="tokenizedSample" select="tokenize($dir,',')"/> 
                                        <xsl:variable name="dir1">
                                            <xsl:for-each select="$tokenizedSample">                                            
                                                <xsl:if test="position()=1">                                                  
                                                    <xsl:value-of select="normalize-space(.)"/> 
                                                </xsl:if>                                              
                                            </xsl:for-each>
                                        </xsl:variable>
                                        <xsl:variable name="dir2">
                                            <xsl:for-each select="$tokenizedSample">                                              
                                                <xsl:if test="position()=2">                                                 
                                                    <xsl:value-of select="normalize-space(.)"/> 
                                                </xsl:if>                                              
                                            </xsl:for-each>
                                        </xsl:variable>
                                        
                                        <xsl:variable name="dir3">
                                            <xsl:for-each select="$tokenizedSample">                                              
                                                <xsl:if test="position()=3">                                                    
                                                    <xsl:value-of select="normalize-space(.)"/> 
                                                </xsl:if>                                              
                                            </xsl:for-each>
                                        </xsl:variable>
                                        <xsl:variable name="dir4">
                                            <xsl:for-each select="$tokenizedSample">                                              
                                                <xsl:if test="position()=4">                                                  
                                                    <xsl:value-of select="normalize-space(.)"/> 
                                                </xsl:if>                                             
                                            </xsl:for-each>
                                        </xsl:variable>
                                                                            
                                        <xsl:element name="effectiveTime">
                                            <xsl:attribute name="xsi:type">PIVL_TS</xsl:attribute>
                                            <xsl:element name="period">
                                                <xsl:attribute name="value">
                                                    <xsl:value-of select="$dir4"/>
                                                </xsl:attribute>
                                                <!--xsl:attribute name="unit">
                                                    <xsl:value-of select="concat($dir1,$dir2)"/>
                                                </xsl:attribute-->
                                            </xsl:element>
                                        </xsl:element>
                                        <xsl:if test="ROUTE_CODE!=''">
                                            <xsl:element name="routeCode">
                                                <xsl:attribute name="code">
                                                    <xsl:value-of select="ROUTE_CODE"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="codeSystem">
                                                    <xsl:value-of select="ROUTE_CODE_SYSTEM"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="codeSystemName">
                                                    <xsl:value-of select="ROUTE_CODE_SYSTEM_NAME"/>
                                                </xsl:attribute>
                                                <xsl:attribute name="displayName">
                                                    <xsl:value-of select="ROUTE_CODE_DISPLAY_NAME"/> 
                                                </xsl:attribute>
                                            </xsl:element>
                                        </xsl:if>
                                        <xsl:element name="doseQuantity">
                                            <xsl:attribute name="value">
                                                <xsl:value-of select="$dir3"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="unit">
                                                <xsl:value-of select="concat($dir1,$dir2)"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="consumable">
                                            <xsl:element name="manufacturedProduct">
                                                <xsl:element name="templateId">
                                                    <xsl:attribute name="root">2.16.840.1.113883.10.20.1.53</xsl:attribute>
                                                </xsl:element>
                                                <xsl:element name="manufacturedMaterial">
                                                    <xsl:element name="code">
                                                        <xsl:attribute name="code">
                                                            <xsl:value-of select="ProductName"/>
                                                        </xsl:attribute>
                                                        
                                                        <xsl:attribute name="codeSystem">                                                         
                                                            <xsl:value-of select="$localType"/>
                                                        </xsl:attribute>
                                                        <xsl:attribute name="displayName">
                                                            <xsl:value-of select="ProductName"/>
                                                        </xsl:attribute>
                                                    </xsl:element>
                                                </xsl:element>
                                            </xsl:element>
                                        </xsl:element>
                                        
                                        
                                        <xsl:variable name="aorder" select="ORDER_NUMBER"/>

                                        <xsl:for-each select="../SUBSTANCE_ORDER">

                                            <xsl:variable name="order" select="ORDER_NUMBER"/>
                                            <xsl:if test="$aorder=$order">
                                                <xsl:element name="entryRelationship">
                                                    <xsl:attribute name="typeCode">REFR</xsl:attribute>
                                                    <xsl:element name="supply">
                                                        <xsl:attribute name="classCode">SPLY</xsl:attribute>
                                                        <xsl:attribute name="moodCode">EVN</xsl:attribute>
                                                        <xsl:element name="templateId">
                                                            <xsl:attribute name="root">2.16.840.1.113883.10.20.1.34</xsl:attribute>
                                                        </xsl:element>
                                                        <xsl:element name="templateId">
                                                            <xsl:attribute name="root">1.3.6.1.4.1.19376.1.5.3.1.4.7.3</xsl:attribute>
                                                        </xsl:element>
                                                        <xsl:element name="id">
                                                            <xsl:variable name="uid" select="'c6f88323-67ad-11db-bd13-0800200c9a66'"/>    
                                                        <!--xsl:attribute name="root">57d07056-bd97-4c90-891d-eb716d3170c8</xsl:attribute-->
                                                            <xsl:attribute name="root"> 
                                                                <xsl:value-of select="$uid"/>
                                                            </xsl:attribute>
                                                                <!--xsl:attribute name="extension"><xsl:value-of select="MATERIAL_CODE"/></xsl:attribute-->
                                                        </xsl:element>
                                                        <xsl:element name="quantity">
                                                            <xsl:attribute name="value"> 
                                                                <xsl:value-of select="DOSE_QUANTITY"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="unit">
                                                                <xsl:value-of select="DOSE_UNIT"/>
                                                            </xsl:attribute>
                                                        </xsl:element>
                                                    </xsl:element>
                                                    <xsl:element name="author">
                                                        <xsl:element name="time">
                                                            <xsl:attribute name="value">
                                                                <xsl:value-of select="ORDER_DATE_TIME"/>
                                                            </xsl:attribute>
                                                        </xsl:element>
                                                        <xsl:element name="assignedAuthor">
                                                            <xsl:element name="id">
                                                                <xsl:attribute name="root">
                                                                    <xsl:value-of select="$facOid"/>
                                                                </xsl:attribute>
                                                            </xsl:element>
                                                        </xsl:element>
                                                    </xsl:element>
                                                </xsl:element>
                                            </xsl:if>

                                        </xsl:for-each>
                                    </xsl:element>
                                </xsl:element>
                               
                            </xsl:for-each>
                        </xsl:element>
                    </xsl:element>

                    <!-- ********************************************************Immunization section********************************************************-->

                    <xsl:element name="component">
                        <xsl:element name="section">
                            <xsl:element name="templateId">
                                <xsl:attribute name="root">2.16.840.1.113883.10.20.1.6</xsl:attribute>
                            </xsl:element>

                            <xsl:element name="code">
                                <xsl:attribute name="code">11369-6</xsl:attribute>
                                <xsl:attribute name="codeSystem">2.16.840.1.113883.6.1</xsl:attribute>
                            </xsl:element>
                            <xsl:element name="title">Immunizations</xsl:element>
                            <xsl:element name="text">
                                <xsl:element name="table">
                                    <xsl:attribute name="border" >1</xsl:attribute>
                                    <xsl:attribute name="width">100%</xsl:attribute>
                                    <xsl:element name="thead">
                                        <xsl:element name="tr">
                                            <xsl:element name="th"> Start Date </xsl:element>
                                            <xsl:element name="th"> Vaccine Name</xsl:element>
                                            <xsl:element name="th"> Quantity</xsl:element>
                                            <xsl:element name="th"> Units</xsl:element>

                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="tbody">
                                       
                                        <xsl:for-each select="SUBSTANCE_ADMIN">
                                            <xsl:if test="IS_IMMUNIZATION='1'">
                                                <xsl:element name="tr">
                                                    <xsl:element name="td">
                                                        <xsl:value-of select="ADMIN_START_DATE_TIME"/>
                                                    </xsl:element>
                                                    <xsl:element name="td">
                                                        <xsl:value-of select="MATERIAL_CODE_DISPLAY_NAME"/>
                                                    </xsl:element>
                                                    <xsl:element name="td">
                                                        <xsl:value-of select="DOSE_QUANTITY"/>
                                                    </xsl:element>
                                                    <xsl:element name="td">
                                                        <xsl:value-of select="DOSE_UNIT"/>
                                                    </xsl:element>

                                                </xsl:element>
                                            </xsl:if>
                                        </xsl:for-each>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>

                            <xsl:for-each select="SUBSTANCE_ADMIN">
                                <xsl:if test="IS_IMMUNIZATION='1'">
                                    <xsl:element name="entry">
                                        <xsl:attribute name="typeCode">DRIV</xsl:attribute>
                                        <xsl:element name="substanceAdministration">
                                            <xsl:attribute name="classCode">SBADM</xsl:attribute>
                                            <xsl:attribute name="moodCode">EVN</xsl:attribute>
                                            <xsl:element name="templateId">
                                                <xsl:attribute name="root">2.16.840.1.113883.10.20.1.24</xsl:attribute>
                                            </xsl:element>
                                            <xsl:element name="id">
                                                <xsl:variable name="uid" select="'c6f88323-67ad-11db-bd13-0800200c9a66'"/>    
                                                  <!--xsl:attribute name="root">57d07056-bd97-4c90-891d-eb716d3170c8</xsl:attribute-->
                                                <xsl:attribute name="root"> 
                                                    <xsl:value-of select="$uid"/>
                                                </xsl:attribute>
                                            </xsl:element>
                                            <xsl:element name="statusCode">
                                                <xsl:attribute name="code">
                                                    <xsl:value-of select="STATUS_CODE"/>
                                                </xsl:attribute>
                                            </xsl:element>
                                            <xsl:element name="effectiveTime">
                                                <xsl:attribute name="xsi:type">IVL_TS</xsl:attribute>
                                                <xsl:element name="center">
                                                    <xsl:attribute name="value">
                                                        <xsl:value-of select="ADMIN_START_DATE_TIME"/>
                                                    </xsl:attribute>
                                                </xsl:element>
                                            </xsl:element>
                                            <xsl:if test="ROUTE_CODE!=''">
                                                <xsl:element name="routeCode">
                                                    <xsl:attribute name="code">
                                                        <xsl:value-of select="ROUTE_CODE"/>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="codeSystem">
                                                        <xsl:value-of select="ROUTE_CODE_SYSTEM"/>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="codeSystemName">
                                                        <xsl:value-of select="ROUTE_CODE_SYSTEM_NAME"/>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="displayName">
                                                        <xsl:value-of select="ROUTE_CODE_DISPLAY_NAME"/> 
                                                    </xsl:attribute>
                                                </xsl:element>
                                            </xsl:if>
                                            <xsl:element name="consumable">
                                                <xsl:element name="manufacturedProduct">
                                                    <xsl:element name="templateId">
                                                        <xsl:attribute name="root">2.16.840.1.113883.10.20.1.53</xsl:attribute>
                                                    </xsl:element>
                                                    <xsl:element name="manufacturedMaterial">
                                                        <xsl:element name="code">
                                                            <xsl:attribute name="code">
                                                                <xsl:value-of select="MATERIAL_CODE"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="codeSystem">

                                                                <xsl:variable name="codeType" select="MATERIAL_CODE_SYSTEM"/>
                                                                <xsl:value-of select="$codeType"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="displayName">
                                                                <xsl:value-of select="MATERIAL_CODE_DISPLAY_NAME"/>
                                                            </xsl:attribute>
                                                        </xsl:element>
                                                    </xsl:element>
                                                </xsl:element>
                                            </xsl:element>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:element>
                    </xsl:element>
                    <!-- ********************************************************Allergy section********************************************************-->

                    <xsl:element name="component">
                        <xsl:element name="section">
                            <xsl:element name="templateId">
                                <xsl:attribute name="root">2.16.840.1.113883.10.20.1.2</xsl:attribute>
                            </xsl:element>

                            <xsl:element name="code">
                                <xsl:attribute name="code">48765-2</xsl:attribute>
                                <xsl:attribute name="codeSystem">2.16.840.1.113883.6.1</xsl:attribute>
                            </xsl:element>
                            <xsl:element name="title">Allergies, Adverse Reactions, Alerts</xsl:element>
                            <xsl:element name="text">
                                <xsl:element name="table">
                                    <xsl:attribute name="border" >1</xsl:attribute>
                                    <xsl:attribute name="width">100%</xsl:attribute>
                                    <xsl:element name="thead">
                                        <xsl:element name="tr">
                                            <xsl:element name="th"> Type</xsl:element>
                                            <xsl:element name="th"> Substance</xsl:element>
                                            <xsl:element name="th"> Reaction</xsl:element>
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="tbody">
                                        
                                        <xsl:for-each select="Allergies/Allergy">
                                            <xsl:element name="tr">
                                                <xsl:element name="td">
                                                    <xsl:value-of select="ReactionClass/Meaning"/>
                                                </xsl:element>
                                                <xsl:element name="td">
                                                    <xsl:value-of select="Allergen"/>
                                                </xsl:element>
                                                <xsl:element name="td">
                                                    <xsl:value-of select="ReactionClass/Meaning"/>
                                                </xsl:element>
                                            </xsl:element>
                                        </xsl:for-each>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>

                            <xsl:for-each select="Allergies/Allergy">
                                <xsl:element name="entry">
                                    <xsl:attribute name="typeCode">DRIV</xsl:attribute>
                                    <xsl:element name="act">
                                        <xsl:attribute name="classCode">ACT</xsl:attribute>
                                        <xsl:attribute name="moodCode">EVN</xsl:attribute>
                                        <xsl:element name="templateId">
                                            <xsl:attribute name="root">2.16.840.1.113883.10.20.1.27</xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="id">
                                            <xsl:variable name="alg" select="Allergen/text()" />
                                            <!--xsl:variable name="uid4" select="util:nameUUIDFromBytes($alg)"/-->  
                                                       <!--xsl:variable name="uid4" select="util:randomUUID()"/-->   
                                            <xsl:variable name="uid4" select="'c6f88323-67ad-11db-bd13-0800200c9a66'"/>    
                                            
                                            <xsl:attribute name="root"> 
                                                <xsl:value-of select="$uid4"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="code">
                                            <xsl:attribute name="nullFlavor">NA</xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="entryRelationship">
                                            <xsl:attribute name="typeCode">SUBJ</xsl:attribute>
                                            <xsl:element name="observation">
                                                <xsl:attribute name="classCode">OBS</xsl:attribute>
                                                <xsl:attribute name="moodCode">EVN</xsl:attribute>
                                                <xsl:element name="templateId">
                                                    <xsl:attribute name="root">2.16.840.1.113883.10.20.1.18</xsl:attribute>
                                                </xsl:element>
                                                <xsl:element name="id">
                                                    <xsl:variable name="alg1" select="Allergen/text()" />
                                                    <!--xsl:variable name="uid5" select="util:nameUUIDFromBytes($alg1)"/-->  
                                                  <!--xsl:variable name="uid5" select="util:randomUUID()"/-->   
                                                    <xsl:variable name="uid5" select="'c6f88323-67ad-11db-bd13-0800200c9a66'"/>    
                                                    <xsl:attribute name="root"> 
                                                        <xsl:value-of select="$uid5"/>
                                                    </xsl:attribute>
                                                </xsl:element>
                                                <xsl:element name="code">
                                                    <xsl:attribute name="code">ASSERTION</xsl:attribute>
                                                    <xsl:attribute name="codeSystem">2.16.840.1.113883.5.4</xsl:attribute>
                                                </xsl:element>
                                                <xsl:element name="statusCode">
                                                    <xsl:attribute name="code">
                                                        <xsl:value-of select="'completed'"/>
                                                    </xsl:attribute>
                                                </xsl:element>
                                                <xsl:element name="value">
                                                    <xsl:attribute name="xsi:type">CD</xsl:attribute>
                                                    <xsl:attribute name="code">
                                                        <xsl:value-of select="ReactionClass/Meaning"/>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="codeSystem">
                                                        <xsl:value-of select="$localType"/>
                                                    </xsl:attribute>
                                                    <xsl:attribute name="displayName">
                                                        <xsl:value-of select="ReactionClass/Display"/>
                                                    </xsl:attribute>
                                                </xsl:element>
                                                <xsl:element name="participant">
                                                    <xsl:attribute name="typeCode">CSM</xsl:attribute>

                                                    <xsl:element name="participantRole">
                                                        <xsl:attribute name="classCode">MANU</xsl:attribute>
                                                        <xsl:element name="playingEntity">
                                                            <xsl:attribute name="classCode">MMAT</xsl:attribute>
                                                            <xsl:element name="code">
                                                                <xsl:attribute name="code">
                                                                    <xsl:value-of select="Allergen"/>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="codeSystem">
                                                                    <xsl:value-of select="$localType"/>
                                                                </xsl:attribute>
                                                                <xsl:attribute name="displayName">
                                                                    <xsl:value-of select="Allergen"/>
                                                                </xsl:attribute>
                                                            </xsl:element>
                                                        </xsl:element>
                                                    </xsl:element>
                                                </xsl:element>
                                                <!--xsl:element name="entryRelationship">
                                                    <xsl:attribute name="typeCode">MSFT</xsl:attribute>
                                                    <xsl:attribute name="inversionInd">true</xsl:attribute>
                                                    <xsl:element name="observation">
                                                        <xsl:attribute name="classCode">OBS</xsl:attribute>
                                                        <xsl:attribute name="moodCode">EVN</xsl:attribute>
                                                        <xsl:element name="templateId">
                                                            <xsl:attribute name="root">2.16.840.1.113883.10.20.1.54'</xsl:attribute>
                                                        </xsl:element>
                                                        <xsl:element name="code">
                                                            <xsl:attribute name="code">ASSERTION</xsl:attribute>
                                                            <xsl:attribute name="codeSystem">2.16.840.1.113883.5.4</xsl:attribute>
                                                        </xsl:element>

                                                        <xsl:element name="statusCode">
                                                            <xsl:attribute name="code">
                                                                <xsl:value-of select="STATUS_CODE"/>
                                                            </xsl:attribute>
                                                        </xsl:element>
                                                        <xsl:element name="value">
                                                            <xsl:attribute name="xsi:type">CD</xsl:attribute>
                                                            <xsl:attribute name="code">  
                                                                <xsl:value-of select="REACTION_CODE"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="codeSystem">
                                                                <xsl:value-of select="REACTION_CODE_SYSTEM"/>
                                                            </xsl:attribute>
                                                            <xsl:attribute name="displayName">
                                                                <xsl:value-of select="REACTION_DISPALY_NAME"/>
                                                            </xsl:attribute>
                                                        </xsl:element>
                                                    </xsl:element>
                                                </xsl:element-->
                                            </xsl:element>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:for-each>
                        </xsl:element>
                    </xsl:element>


                    <!-- ********************************************************Procedure section********************************************************-->

                    <xsl:element name="component">
                        <xsl:element name="section">
                            <xsl:element name="templateId">
                                <xsl:attribute name="root">2.16.840.1.113883.10.20.1.12</xsl:attribute>
                            </xsl:element>

                            <xsl:element name="code">
                                <xsl:attribute name="code">47519-4</xsl:attribute>
                                <xsl:attribute name="codeSystem">2.16.840.1.113883.6.1</xsl:attribute>
                            </xsl:element>
                            <xsl:element name="title">Procedures</xsl:element>
                            <xsl:element name="text">
                                <xsl:element name="table">
                                    <xsl:attribute name="border" >1</xsl:attribute>
                                    <xsl:attribute name="width">100%</xsl:attribute>
                                    <xsl:element name="thead">
                                        <xsl:element name="tr">
                                            <xsl:element name="th"> Procedure</xsl:element>
                                            <xsl:element name="th"> Dater</xsl:element>
                                          
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="tbody">
                                      
                                        <xsl:for-each select="Procedures/Procedure">
                                            <xsl:element name="tr">
                                                <xsl:element name="td">
                                                    <xsl:value-of select="ProcedureName"/>
                                                </xsl:element>
                                                <xsl:element name="td">
                                                    <xsl:value-of select="DateTime"/>
                                                </xsl:element>
                                            </xsl:element>
                                        </xsl:for-each>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>

                            <xsl:for-each select="Procedures/Procedure">
                                <xsl:element name="entry">
                                    <xsl:attribute name="typeCode">DRIV</xsl:attribute>
                                    <xsl:element name="procedure">
                                        <xsl:attribute name="classCode">PROC</xsl:attribute>
                                        <xsl:attribute name="moodCode">EVN</xsl:attribute>
                                        <xsl:element name="templateId">
                                            <xsl:attribute name="root">2.16.840.1.113883.10.20.1.29</xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="id">
                                            <xsl:variable name="pro" select="ProcedureId/text()|DateTime/text()" />
                                            <!--xsl:variable name="uid6" select="util:nameUUIDFromBytes($pro)"/-->  
                                                       <!--xsl:variable name="uid6" select="util:randomUUID()"/-->   
                                            <xsl:variable name="uid6" select="'c6f88323-67ad-11db-bd13-0800200c9a66'"/>    
                                            <xsl:attribute name="root"> 
                                                <xsl:value-of select="$uid6"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="code">
                                            <xsl:attribute name="code">
                                                <xsl:value-of select="ProcedureId"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="codeSystem">

                                                <xsl:variable name="codeType" select="'SNOMED'"/>
                                                <xsl:if test="$codeType='LOINC'">
                                                    <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.1'"/>
                                                    <xsl:value-of select="$newdiagType"/>
                                                </xsl:if>
                                                <xsl:if test="$codeType='SNOMED'">
                                                    <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.96'"/>
                                                    <xsl:value-of select="$newdiagType"/>
                                                </xsl:if>
                                                <xsl:if test="$codeType='SNOMED-CT'">
                                                    <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.96'"/>
                                                    <xsl:value-of select="$newdiagType"/>
                                                </xsl:if>

                                            </xsl:attribute>
                                            <xsl:attribute name="displayName">
                                                <xsl:value-of select="ProcedureName"/>
                                            </xsl:attribute>
                                            <xsl:element name="originalText">
                                                <xsl:value-of select="ProcedureName"/>
                                            </xsl:element>
                                        </xsl:element>
                                        <xsl:element name="statusCode">
                                            <xsl:attribute name="code">
                                                <xsl:value-of select="'completed'"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="effectiveTime">
                                            <xsl:attribute name="value">
                                                
                                                <xsl:variable name="t" select="DateTime"/>
                                                <xsl:value-of select='concat(replace(format-dateTime($t, "[Y0001][M01][D01][H01][m01][s01][z]"),"GMT",""),$zeros)'/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        
                                        <xsl:element name="performer">
                                            <xsl:element name="assignedEntity">
                                                <xsl:element name="assignedPerson">
                                                    <xsl:element name="name">                                           
                                                        <xsl:value-of select="Provider"/>                                            
                                                    </xsl:element>
                                                </xsl:element>
                                            </xsl:element>
                                        </xsl:element>
                                    
                                    </xsl:element>
                                </xsl:element>
                            </xsl:for-each>
                        </xsl:element>
                    </xsl:element>
    <!-- ********************************************************Encounters section********************************************************-->

                    <xsl:element name="component">
                        <xsl:element name="section">
                            <xsl:element name="templateId">
                                <xsl:attribute name="root">2.16.840.1.113883.10.20.1.3</xsl:attribute>
                            </xsl:element>

                            <xsl:element name="code">
                                <xsl:attribute name="code">46240-8</xsl:attribute>
                                <xsl:attribute name="codeSystem">2.16.840.1.113883.6.1</xsl:attribute>
                            </xsl:element>
                            <xsl:element name="title">Encounters</xsl:element>
                            <xsl:element name="text">
                                <xsl:element name="table">
                                    <xsl:attribute name="border" >1</xsl:attribute>
                                    <xsl:attribute name="width">100%</xsl:attribute>
                                    <xsl:element name="thead">
                                        <xsl:element name="tr">
                                            <xsl:element name="th"> Encounter</xsl:element>
                                           
                                            <xsl:element name="th"> Date</xsl:element>
                                          
                                        </xsl:element>
                                    </xsl:element>
                                    <xsl:element name="tbody">
                                       
                                        <xsl:for-each select="Encounter">
                                            <xsl:element name="tr">
                                                <xsl:element name="td">
                                                    <xsl:value-of select="EncounterType/Meaning"/>
                                                </xsl:element>

                                                <xsl:element name="td">
                                                    <xsl:value-of select="EncounterDataTime"/>
                                                   
                                                </xsl:element>
                                            </xsl:element>
                                        </xsl:for-each>
                                    </xsl:element>
                                </xsl:element>
                            </xsl:element>

                            <xsl:for-each select="Encounter">
                                <xsl:element name="entry">
                                    <xsl:attribute name="typeCode">DRIV</xsl:attribute>
                                    <xsl:element name="encounter">
                                        <xsl:attribute name="classCode">ENC</xsl:attribute>
                                        <xsl:attribute name="moodCode">EVN</xsl:attribute>
                                        <xsl:element name="templateId">
                                            <xsl:attribute name="root">2.16.840.1.113883.10.20.1.21</xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="id">
                                            <xsl:variable name="enc" select="EncounterId/text()|EncounterDataTime/text()" />
                                            <!--xsl:variable name="uid7" select="util:nameUUIDFromBytes($enc)"/-->  
                                                       <!--xsl:variable name="uid7" select="util:randomUUID()"/-->   
                                            <xsl:variable name="uid7" select="'c6f88323-67ad-11db-bd13-0800200c9a66'"/>    
                                            <xsl:attribute name="root"> 
                                                <xsl:value-of select="$uid7"/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="code">
                                            <xsl:attribute name="code">
                                                <xsl:value-of select="EncounterType/Meaning"/>
                                            </xsl:attribute>
                                            <xsl:attribute name="codeSystem">

                                                <xsl:variable name="codeType" select="'SNOMED'"/>
                                                <xsl:if test="$codeType='LOINC'">
                                                    <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.1'"/>
                                                    <xsl:value-of select="$newdiagType"/>
                                                </xsl:if>
                                                <xsl:if test="$codeType='SNOMED'">
                                                    <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.96'"/>
                                                    <xsl:value-of select="$newdiagType"/>
                                                </xsl:if>
                                                <xsl:if test="$codeType='SNOMED-CT'">
                                                    <xsl:variable name="newdiagType" select="'2.16.840.1.113883.6.96'"/>
                                                    <xsl:value-of select="$newdiagType"/>
                                                </xsl:if>

                                            </xsl:attribute>
                                            <xsl:attribute name="displayName">
                                                <xsl:value-of select="EncounterType/Display"/>
                                            </xsl:attribute>
                                            <xsl:element name="originalText">
                                                <xsl:value-of select="EncounterType/Display"/>
                                            </xsl:element>
                                        </xsl:element>
                                       
                                        <xsl:element name="effectiveTime">
                                            <xsl:attribute name="value">
                                                <xsl:variable name="t" select="EncounterDataTime"/>
                                                <xsl:value-of select='concat(replace(format-dateTime($t, "[Y0001][M01][D01][H01][m01][s01][z]"),"GMT",""),$zeros)'/>
                                            </xsl:attribute>
                                        </xsl:element>
                                        <xsl:element name="participant">
                                            <xsl:attribute name="typeCode">LOC</xsl:attribute>
                                            <xsl:element name="templateId">
                                                <xsl:attribute name="root">2.16.840.1.113883.10.20.1.45</xsl:attribute>
                                            </xsl:element>
                                            <xsl:element name="participantRole">
                                                <xsl:attribute name="classCode">SDLOC</xsl:attribute>
                                                <xsl:element name="id">                                         
                                                    <xsl:variable name="uid8" select="'2.16.840.1.113883.19.5'"/> <!-- ??? -->
                                                    <xsl:attribute name="root"> 
                                                        <xsl:value-of select="$uid8"/>
                                                    </xsl:attribute>
                                                </xsl:element>                                                
                                                <xsl:element name="playingEntity">
                                                    <xsl:attribute name="classCode">PLC</xsl:attribute>
                                                    <xsl:element name="name">
                                                        <xsl:value-of select="$facName"/>
                                                    </xsl:element>
                                                </xsl:element>
                                            </xsl:element>
                                        </xsl:element>
                                        <xsl:element name="performer">
                                            <xsl:element name="assignedEntity">
                                                <xsl:element name="assignedPerson">
                                                    <xsl:element name="name">                                           
                                                        <xsl:value-of select="EncounterProvider"/>                                            
                                                    </xsl:element>
                                                </xsl:element>
                                            </xsl:element>
                                        </xsl:element>
                                    
                                    </xsl:element>
                                </xsl:element>
                            </xsl:for-each>
                        </xsl:element>
                    </xsl:element>
        <!-- ***************************************************************************-->
                </xsl:element>
            </xsl:element>
        </xsl:element>

    </xsl:template>
</xsl:stylesheet>
