===================================
These files customize the look and feel of OpenAM for the 
Maimonides Health Home Dashboard
===================================

These files should be placed in the glassfish deployment
directory for OpenAM. Use the directory structure in this folder as a guide.
Be careful NOT TO OVERWRITE THE EXISTING FOLDERS of the same name in the OpenAM
deployment; because they contain other files needed by OpenAM that are not included
in this repository 



For the Development Server the Glassfish root directory
for the openam_s952 project is located :

D:\Glassfish2.1\domains\domain1\applications\j2ee-modules\openam_s952\

For the QA Server the Glassfish root directory
for the openam_s952 project is located :


D:\Glassfish2.1\domains\domain1\applications\j2ee-modules\openam_s952\


/images/PrimaryProductName.png is a copy of mmc_logo.jpg reduced
 slighty to 250 pixels wide. This removes the default ForgeRock logo from the 
application error pages